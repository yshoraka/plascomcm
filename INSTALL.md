PlasComCM uses Autoconf and Automake to generate the necessary Makefile(s) for
the target platform.  The code critically depends on an available MPI
implementation and an user-selected optional dependence on HDF5.

Autotools and version dependencies
==================================

The build process uses autoconf, automake, and libtools to create the necessary
Makefile appropriate for your traget platform.  As PlasComCM is a Fortran
application, the support for F90+ code in the autotools is weak and strongly
dependent on version numbers.  At present the code depends on Autoconf >= 2.69
and Automake >= 1.12.3.  If your target platform has older versions of either
one of these, the sh ./autogen.sh script below will fail.  In this case you
need to download and build newer versions of them, as follows

Autoconf
--------

```
wget http://ftp.gnu.org/gnu/autoconf/autoconf-2.69.tar.gz
tar xzf autoconf-2.69.tar.gz
cd autoconf-2.69
./configure [--prefix=$HOME]
make
make install
```

Automake
--------

```
wget http://ftp.gnu.org/gnu/automake/automake-1.15.tar.gz
tar xzf automake-1.15.tar.gz
cd automake-1.15
./configure [--prefix=$HOME]
make
make install
```

Don't forget to update your PATH to find the newer versions of autoconf and
automake first.  (If you included the `--prefix=$HOME` option then the auto*
binaries will be in `$HOME/bin`.)

Optional dependencies
=====================

HDF5
----

```
wget https://www.hdfgroup.org/ftp/HDF5/releases/hdf5-1.8.14/src/hdf5-1.8.14.tar.gz
tar xzf hdf5-1.8.14.tar.gz
cd hdf5-1.8.14
./configure --enable-fortran --enable-parallel --prefix=$HOME/hdf5-1.8.14
make
make install
```

Cantera
-------

Requires SCons, Python, Cython, and NumPy
```
git clone https://github.com/Cantera/cantera.git cantera-src
cd cantera-src
scons build prefix=$HOME/cantera python_package=full use_sundials=n debug=false
scons install
```

Building the code
=================

Issue the following commands

```
sh ./autogen.sh
./configure --help # to see a list of options
./configure [--with-hdf5=/path/to/h5pcc] # to enable HDF5
make
```

Installing the code
===================

If you choose, you can install the executable using

```
make install
```

The default location is `/usr/local/bin` but that can adjusted per the options in
`./configure --help`.

Running the code
================

The build will create one executable, plascomcm, located in the root directory.
Depending on your system you may be able to run with one MPI rank using the
command

```
./plascomcm
```

while on other system you will need to issue commands such as

```
  $ mpiexec -n <number of MPI ranks> plascomcm
```

Additional details on the code can be found in the manual, docs/Manual.pdf
