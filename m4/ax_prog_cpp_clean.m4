# Copyright (c) 2015, University of Illinois at Urbana-Champaign, XPACC
# License: MIT, http://opensource.org/licenses/MIT
#
# Find a C preprocessor and options that does not add output lines to
# the processed file (e.g., does not add #line etc.)
# Approach:
#   See if CPP works as is.
#   See if we can find a cpp
AC_DEFUN([AX_PROG_CPP_CLEAN],[
   AC_REQUIRE([AC_PROG_CPP])
   cat >>conftest.c <<_ACEOF
   This is a sample ONE//bar
_ACEOF
   isNoisy=yes
   AC_MSG_CHECKING([whether $CPP adds lines to output])
   if test "X$CPP" != "X" ; then
       if $CPP $CPPFLAGS -DONE=1 conftest.c >conftest.tout 2>&1 & \
          grep 'This is a sample 1//bar' conftest.tout >/dev/null 2>&1 ; then 
          if grep '#' conftest.tout >/dev/null 2>&1 ; then
              :
          else
              QCPP=$CPP
              isNoisy=no
          fi
       fi
   fi
   AC_MSG_RESULT([$isNoisy])
   if test $isNoisy = "yes" ; then
       # Find a quiet option for CPP, or a more flexible cpp
       AC_MSG_CHECKING([for a CPP that does not add to output])
       for QCPP in "$CC -P" "cpp" "cpp -P" "cpp -P -C" ; do
          if $QCPP -DONE=1 conftest.c >conftest.tout 2>&1 ; then
             if grep '#' conftest.tout >/dev/null 2>&1 ; then
                 :
             elif grep 'This is a sample 1//bar' conftest.tout >/dev/null 2>&1 ; then
                isNoisy=no
                break
             # else did not handle cpp operation correctly, do keep looking
             fi
          fi
       done
       if test $isNoisy = "no" ; then
           AC_MSG_RESULT([$QCPP])
       else
           AC_MSG_RESULT([unavailable])
           unset QCPP
      fi
   fi
   rm -f conftest.c conftest.txt conftest.*
])
