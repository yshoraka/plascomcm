# Copyright (c) 2015, University of Illinois at Urbana-Champaign, XPACC
# License: MIT, http://opensource.org/licenses/MIT
#
# Set a standard for dependency support for Fortran
# Optional argument is a list of subdirectories into which the DEPDIR
# should be placed
AC_DEFUN([AX_FC_DEPEND],[
AC_REQUIRE([AM_MAKE_INCLUDE])
abs_f90deps_dir=$(cd $srcdir/confdir && pwd)
FC_DEPCOMP=$abs_f90deps_dir/f90deps
AC_SUBST([FC_DEPCOMP])
# Create a dummy dependency file so that we can unconditionally include it
# Use the DEPDIR set by Automake's dependency commands.  If those are not
# active (for example, the package uses no C code), set the default as
# .deps
if test -z "$AMDEP_TRUE" -a -z "$AMDEP_FALSE" ; then
    AMDEP_TRUE=
    AMDEP_FALSE='#'
    AC_SUBST([AMDEP_TRUE])
    AC_SUBST([AMDEP_FALSE])
fi
if test -z "$DEPDIR" ; then
    DEPDIR=.deps
    AC_SUBST([DEPDIR])
fi
AC_MSG_NOTICE([FC_DEPCOMP = $FC_DEPCOMP])

# Create a dummpy list, but in the src directory.
ifelse([$1],[],[DEPSUBDIRS="."],[DEPSUBDIRS="$1"])
AC_CONFIG_COMMANDS([FCDependencies],[
if test -z "$DEPDIR" ; then DEPDIR=".deps" ; fi
for dir in $DEPSUBDIRS ; do
  if test ! -d $dir ; then mkdir $dir ; fi
  if test ! -d $dir/$DEPDIR ; then mkdir $dir/$DEPDIR ; fi
  if test ! -f $dir/$DEPDIR/_fclist.Po ; then echo "# dummy" > $dir/$DEPDIR/_fclist ; fi
done
],[DEPDIR=$DEPDIR;DEPSUBDIRS="$DEPSUBDIRS"])
])
