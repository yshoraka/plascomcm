# Based on HDF5 macro, available at
# ===========================================================================
#        http://www.gnu.org/software/autoconf-archive/ax_lib_hdf5.html
# ===========================================================================
#
# SYNOPSIS
#
#   AX_LIB_CANTERA([])
#
# DESCRIPTION
#
#   This macro provides tests of the availability of Cantera library.
#
#   The macro adds a --with-cantera option accepting one of three values:
#
#     no   - do not check for the Cantera library.
#     yes  - do check for Cantera library in standard locations.
#     path - complete path to Cantera
#
#   If Cantera is successfully found, this macro calls
#
#     AC_SUBST(CANTERA_DIR)
#     AC_CONDITIONAL(HAVE_CANTERA)
#
#   and sets with_cantera="yes".
#
#   If Cantera is disabled or not found, this macros sets with_cantera="no".
#
#   Your configuration script can test $with_cantera to take any further
#   actions. CANTERA_{C,CPP,LD}FLAGS may be used when building with C or C++.
#   CANTERA_F{FLAGS,LIBS} should be used when building Fortran applications.
#
#   To use the macro, one would code the following in "configure.ac"
#   before AC_OUTPUT:
#
#     AX_LIB_CANTERA()
#
#   One could test $with_cantera for the outcome or display it as follows
#
#     echo "Cantera support:  $with_cantera"
#
# LICENSE
#
#   Copyright (c) 2009 Timothy Brown <tbrown@freeshell.org>
#   Copyright (c) 2010 Rhys Ulerich <rhys.ulerich@gmail.com>
#
#   Copying and distribution of this file, with or without modification, are
#   permitted in any medium without royalty provided the copyright notice
#   and this notice are preserved. This file is offered as-is, without any
#   warranty.

#serial 11

AC_DEFUN([AX_LIB_CANTERA], [

AC_MSG_CHECKING([for Cantera])

dnl Set defaults to blank
CANTERA_DIR=
CANTERA_FCFLAGS=
CANTERA_LIBS=

dnl Add a default --with-cantera configuration option.
AC_ARG_WITH([cantera],
  AS_HELP_STRING(
    [--with-cantera=[no/PATH]],
    [Location of Cantera configuration]
  ),
  [if test "$withval" = "no"; then
     with_cantera="no"
   else
     with_cantera="yes"
     CANTERA_DIR="$withval"
   fi],
  [with_cantera="no"]
)

if test "$with_cantera" = "yes"; then

  CANTERA_VAR_FILE="$CANTERA_DIR/include/cantera/Cantera.mak"

  dnl Find Cantera compilation/linking flags
  printf '%s\n' "include $CANTERA_VAR_FILE" > Makefile_config_cantera
  printf '%s\n' "getCANTERA_FORTRAN_MODS:" >> Makefile_config_cantera
  printf '\t%s\n' "echo \$(CANTERA_FORTRAN_MODS)" >> Makefile_config_cantera
  printf '%s\n' "getCANTERA_FORTRAN_LIBS:" >> Makefile_config_cantera
  printf '\t%s\n' "echo \$(CANTERA_FORTRAN_LIBS)" >> Makefile_config_cantera
  CANTERA_FORTRAN_MODS=$(make -s -f Makefile_config_cantera getCANTERA_FORTRAN_MODS)
  CANTERA_FORTRAN_LIBS=$(make -s -f Makefile_config_cantera getCANTERA_FORTRAN_LIBS)
  rm -f Makefile_config_cantera

  dnl Hack to work around issue in some versions of Cantera (CANTERA_FORTRAN_LIBS contains
  dnl SUNDIALS -lcvode even if Cantera is built without it)
  LDFLAGS_TEMP="$LDFLAGS"
  LDFLAGS="$LDFLAGS_TEMP -lcvode"
  AC_LANG_PUSH([Fortran])
  AC_LINK_IFELSE([AC_LANG_SOURCE([[
    program sundialstest
    end program sundialstest
  ]])], [have_sundials=yes], [have_sundials=no])
  AC_LANG_POP([Fortran])
  LDFLAGS="$LDFLAGS_TEMP"

  CANTERA_FCFLAGS=-I"$CANTERA_FORTRAN_MODS"

  if test "$have_sundials" = "yes"; then
    CANTERA_LIBS="$CANTERA_FORTRAN_LIBS"
  else
    CANTERA_LIBS=
    for lib in $CANTERA_FORTRAN_LIBS; do
      if ! test $lib = "-lcvode"; then
        CANTERA_LIBS="$CANTERA_LIBS $lib"
      fi
    done
  fi

  dnl Test if Fortran program containing Cantera code can be compiled/linked successfully
  FCFLAGS_TEMP="$FCFLAGS"
  LIBS_TEMP="$LIBS"
  FCFLAGS="$FCFLAGS_TEMP $CANTERA_FCFLAGS"
  LIBS="$LIBS_TEMP $CANTERA_LIBS"
  AC_LANG_PUSH([Fortran])
  AC_LINK_IFELSE([AC_LANG_SOURCE([[
    program canteratest
      use cantera
    end program canteratest
  ]])], [cantera_works=yes], [cantera_works=no])
  AC_LANG_POP([Fortran])
  FCFLAGS="$FCFLAGS_TEMP"
  LIBS="$LIBS_TEMP"

  if test "$cantera_works" = "yes"; then
    AC_MSG_RESULT([$CANTERA_DIR])
  else
    with_cantera=no
    AC_MSG_RESULT([no])
    AC_MSG_FAILURE([cannot build a basic Cantera program])
  fi

else

  AC_MSG_RESULT([no])

fi

AC_SUBST([CANTERA_DIR])
AC_SUBST([CANTERA_FCFLAGS])
AC_SUBST([CANTERA_LIBS])
AM_CONDITIONAL([HAVE_CANTERA], [test x$with_cantera = xyes])
AS_IF([test "$with_cantera" = "yes"],
  [HAVE_CANTERA=1],
  [HAVE_CANTERA=0]
)
AC_SUBST([HAVE_CANTERA])
if test "$with_cantera" = "yes"; then
  AC_DEFINE(HAVE_CANTERA, 1, "Defined if building with Cantera")
fi

])
