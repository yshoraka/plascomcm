dnl Macro to add --enable-coverage option (disabled by default) and add
dnl appropriate compiler flags to permit usage of gcov if that option is
dnl enabled.  If WRAPPER_xFLAGS variables are set then the flags will also be
dnl added to those variables.
dnl
dnl Sets "pac_cv_use_coverage=yes" and AC_DEFINEs USE_COVERAGE if coverage was
dnl successfully enabled.  Also creates an AM_CONDITIONAL with the name
dnl "BUILD_COVERAGE" that is true iff pac_cv_use_coverage=yes.
dnl
dnl
dnl Assumes that all of the compiler macros have already been invoked
dnl (AC_PROG_CC and friends).
dnl

dnl
dnl New defintion
dnl usage:
dnl   AX_ENABLE_COVERAGE([CC FC CXX])
dnl
dnl Note: different compilers may require different versions of gcov.
dnl If there is any doubt, set the GCOV environment variable to the
dnl absolute path for the correct version of gcov.
AC_DEFUN([AX_ENABLE_COVERAGE],[
AC_REQUIRE([AC_PROG_CC])
ax_cv_use_coverage=no
AC_ARG_VAR([GCOV],[name/path for the gcov utility])
AC_ARG_ENABLE([coverage],
              [AS_HELP_STRING([--enable-coverage],
                              [Turn on coverage analysis using gcc and gcov])],
              [],[enable_coverage=no])

if test "$enable_coverage" = "yes" ; then
    AC_CHECK_PROGS([GCOV],[gcov])
    ifelse([$1],[],[ax_langs="CC FC CXX"],[ax_langs="$1"])
    for lang in $ax_langs ; do
    case $lang in
    CC)
        if test "$ac_cv_prog_gcc" = "yes" ; then
            CFLAGS="$CFLAGS -fprofile-arcs -ftest-coverage"
        else
           AC_MSG_WARN([--enable-coverage only supported for GCC])
        fi
        ;;
    CXX)
        if test "$ac_cv_cxx_compiler_gnu" = "yes" ; then
            CXXFLAGS="$CXXFLAGS -fprofile-arcs -ftest-coverage"
        else
            AC_MSG_WARN([--enable-coverage only supported for GCC])
        fi
        ;;
    FC)
        # The Autoconf test for whether the Fortran compiler is GNU
        # is incorrect and will falsely claim that a compiler is GNU.
        # See the AX_FC_IS_GNU test for a more accurate test.
        if test "$ax_fc_compiler_gnu" = "yes" ; then
             FCFLAGS="$FCFLAGS -fprofile-arcs -ftest-coverage"
             # gcov doesn't understand line marker directives, so we must omit them
             FPPFLAGS="$FPPFLAGS -P"
        else
            AC_MSG_WARN([--enable-coverage only supported for GFORTRAN])
        fi
	;;
    *)
        AC_MSG_ERROR([Unrecognized compiler for [AX_COVERAGE]])
        ;;
    esac
    done
    AC_LANG([C])
    # A partial test whether we need to include -lgcov
    ax_gcov_needs_lgcov=no
    AC_MSG_CHECKING([whether coverage has -lgcov])
    AC_CHECK_LIB([gcov],[exit],[ax_gcov_needs_lgcov=yes])
    AC_MSG_RESULT([$ax_gcov_needs_lgcov])
    if test "X$ax_gcov_needs_lgcov" = "Xyes" ; then
        LIBS="$LIBS -lgcov"
    fi
    # On some platforms (e.g., Mac Darwin), we must also *link*
    # with the -fprofile-args -ftest-coverage option.
    AC_MSG_CHECKING([whether compilation with coverage analysis enabled works])
    AC_LINK_IFELSE([AC_LANG_SOURCE([int main(int argc, char **argv){return 1;}])],
                   [AC_MSG_RESULT([yes])],
                   [AC_MSG_RESULT([no])
                    AC_MSG_ERROR([Unable to link programs when coverage analysis enabled])])

    # Test for the routines that we need to use to ensure that the
    # data files are (usually) written out
    # FIXME: Some versions of Linux provide usleep, but it rounds times
    # up to the next second (!)
    AC_CHECK_FUNCS([usleep])

    # NOTE: using a "pac_cv_" prefix but not caching because of xFLAGS "side effects"
    ax_cv_use_coverage=yes
    AC_DEFINE([USE_COVERAGE],[1],[Define if performing coverage tests])
fi
if test "X$ax_cv_use_coverage" = "Xyes"; then
  BUILD_COVERAGE=1
else
  BUILD_COVERAGE=0
fi
AC_SUBST([BUILD_COVERAGE])
AM_CONDITIONAL([BUILD_COVERAGE],[test "X$ax_cv_use_coverage" = "Xyes"])
])

