# Copyright (c) 2015, University of Illinois at Urbana-Champaign, XPACC
# License: MIT, http://opensource.org/licenses/MIT
#
dnl===========================================================================
dnl Macro to configure for pFUnit support
dnl
dnl Author: Matt Smith (mjsmith6@illinois.edu)
dnl Date: 2/23/2015
dnl
dnl Usage:
dnl
dnl Call AX_LIB_PFUNIT with either "mpi" or "nompi". Adds a configure option
dnl --with-pfunit accepting "yes" (in which case it will check for the
dnl presence of pFUnit via the $PFUNIT environment variable) or "no" (in
dnl which case the check is skipped).
dnl
dnl When called, this macro defines the variable with_pfunit, having the
dnl value "yes" if pFUnit is found, and "no" if not. If pFUnit is found,
dnl it creates the following:
dnl
dnl Header definitions:
dnl   HAVE_PFUNIT
dnl
dnl Substitutions:
dnl   PFUNIT_FPPFLAGS
dnl   PFUNIT_FCFLAGS
dnl   PFUNIT_LDFLAGS
dnl   PFUNIT_LIBS
dnl   PFUNIT_DRIVER
dnl   PFPARSE
dnl===========================================================================

AC_DEFUN([AX_LIB_PFUNIT], [

  AS_IF(
    [test "m4_normalize(m4_default([$1],[]))" = "mpi"], [pfunit_mpi="yes"],
    [test "m4_normalize(m4_default([$1],[]))" = "nompi"], [pfunit_mpi="no"],
    [AC_MSG_ERROR([Unrecognized argument to AX[]_LIB_PFUNIT within configure.ac. Argument 1 must be 'mpi' or 'nompi'.])]
  )

  AC_ARG_WITH([pfunit], [AS_HELP_STRING([--with-pfunit], [Use pFUnit testing library])], [],
    [with_pfunit="yes"])

  PFUNIT_FPPFLAGS="-I$PFUNIT/include"
  AS_IF([test "$pfunit_mpi" = "yes"], [PFUNIT_FPPFLAGS="$PFUNIT_FPPFLAGS -DUSE_MPI"], [])

  PFUNIT_FCFLAGS="-I$PFUNIT/mod"

  PFUNIT_LDFLAGS="-L$PFUNIT/lib"
  PFUNIT_LIBS="-lpfunit"

  PFUNIT_DRIVER="$PFUNIT/include/driver.F90"
  PFPARSE="$PFUNIT/bin/pFUnitParser.py"

  AS_IF(
    [test "$with_pfunit" != "yes" && test "$with_pfunit" != "no"],
    [AC_MSG_ERROR([Unrecognized value for argument to --with-pfunit. Argument must be 'yes' or 'no'.])]
  )

  AS_IF([test "$with_pfunit" = "yes"],
    [

      AC_MSG_CHECKING([for pFUnit])
      AS_IF([test -n "$PFUNIT"],
        [
          AC_MSG_RESULT([$PFUNIT])
          AC_DEFINE([HAVE_PFUNIT], [1], [Defined if pFUnit is supported.])
        ],
        [
          AC_MSG_RESULT([no])
          AC_MSG_WARN([Unable to locate pFUnit installation. Will not be able to build unit tests.])
          with_pfunit="no"
        ]
      )
    ],
    []
  )

  AS_IF([test "$with_pfunit" = "yes"],
    [
      AC_DEFINE([HAVE_PFUNIT], [1], [Define if pFUnit is supported.])
      AC_SUBST([PFUNIT_FPPFLAGS])
      AC_SUBST([PFUNIT_FCFLAGS])
      AC_SUBST([PFUNIT_LDFLAGS])
      AC_SUBST([PFUNIT_LIBS])
      AC_SUBST([PFUNIT_DRIVER])
      AC_SUBST([PFPARSE])
    ],
    []
  )

])
