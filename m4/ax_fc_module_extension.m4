# Copyright (c) 2015, University of Illinois at Urbana-Champaign, XPACC
# License: MIT, http://opensource.org/licenses/MIT
#
# The autoconf module extension macro fails to try to force the compiler
# to create a module, even though the documentation notes that some
# special options may be needed.  This macro doesn't make that mistake.
AC_DEFUN([AX_FC_MODULE_EXTENSION],[
AC_REQUIRE([AC_FC_MODULE_EXTENSION])
if test "X$FC_MODEXT" = "X" ; then
    # The standard test failed to find the module extension.  Try with a few
    # commandline options
    AC_MSG_CHECKING([for option to force Fortran to create module files])
    AC_LANG([Fortran])
    # Run test in a separate directory to ensure that the ls check for
    # the module extension won't find another file that matches the check
    if test ! -d conftest.dir ; then mkdir conftest.dir ; fi
    cd conftest.dir
    ax_FCFLAGS_save=$FCFLAGS
    for ax_flag in '-Am' '-e m' ; do
        FCFLAGS="$ax_FCFLAGS_save $ax_flag"
        AC_COMPILE_IFELSE([
      module conftest_module
      contains
      subroutine conftest_routine
      write(*,'(a)') 'gotcha!'
      end subroutine
      end module
],[
  ax_cv_fc_module_ext=`ls | sed -n 's,conftest_module\.,,p'`
  if test x$ac_cv_fc_module_ext = x; then
     ax_cv_fc_module_ext=`ls | sed -n 's,CONFTEST_MODULE\.,,p'`
  fi],[])
        if test "X$ax_cv_fc_module_ext" != "X" ; then
            FC_MODARG=$ax_flag ; break
        fi
    done
    FCFLAGS=$ax_FCFLAGS_save
    cd ..
    rm -rf conftest.dir
    if test "X$ax_cv_fc_module_ext" = "X" ; then
        AC_MSG_RESULT([unknown])
    else
        FC_MODEXT=$ax_cv_fc_module_ext
        AC_MSG_RESULT([$FC_MODARG])
        AC_MSG_NOTICE([Fortran module extension with $FC_MODARG is $FC_MODEXT])
    fi
fi
# Return to C (not sure why this is needed)
# AC_LANG([C])
])
