AC_DEFUN([AX_LIB_BOXMG], [

AC_MSG_CHECKING([for BoxMG])

AC_ARG_WITH([boxmg],
  AS_HELP_STRING(
    [--with-boxmg=[no/PATH]],
    [Location of BoxMG installation]
  ),
  [if test "$withval" = "no"; then
     with_boxmg="no"
   else
     with_boxmg="yes"
     BOXMG_DIR="$withval"
	 BOXMG_LIB="$BOXMG_DIR/lib/libboxmg-2d.a $BOXMG_DIR/lib/libboxmg-common.a"
	 BOXMG_CFLAGS="-I$BOXMG_DIR/include -DWITH_BOXMG"
   fi],
   [with_boxmg="no"]
)

AC_MSG_RESULT([$with_boxmg])


AC_SUBST([BOXMG_DIR])
AM_CONDITIONAL([HAVE_BOXMG], [test x$with_boxmg = xyes])
])
