#
# Hack of HDF5-written M4 macro to work with PETSc
# Original version of HDF5 code available at
# ===========================================================================
#        http://www.gnu.org/software/autoconf-archive/ax_lib_hdf5.html
# ===========================================================================
#
# SYNOPSIS
#
#   AX_LIB_PETSC([])
#
# DESCRIPTION
#
#   This macro provides tests of the availability of PETSc library.
#
#   The macro adds a --with-petsc option accepting one of three values:
#
#     no   - do not check for the PETSc library.
#     yes  - do check for PETSc library in standard locations.
#     path - complete path to PETSc
#
#   If PETSc is successfully found, this macro calls
#
#     AC_SUBST(PETSC_DIR)
#     AC_CONDITIONAL(HAVE_PETSC)
#
#   and sets with_petsc="yes".
#
#   If PETSc is disabled or not found, this macros sets with_petsc="no".
#
#   Your configuration script can test $with_petsc to take any further
#   actions. PETSC_{C,CPP,LD}FLAGS may be used when building with C or C++.
#   PETSC_F{FLAGS,LIBS} should be used when building Fortran applications.
#
#   To use the macro, one would code the following in "configure.ac"
#   before AC_OUTPUT:
#
#     dnl Check for HDF5 support
#     AX_LIB_PETSC()
#
#   One could test $with_petsc for the outcome or display it as follows
#
#     echo "PETSc support:  $with_petsc"
#
# LICENSE
#
#   Copyright (c) 2009 Timothy Brown <tbrown@freeshell.org>
#   Copyright (c) 2010 Rhys Ulerich <rhys.ulerich@gmail.com>
#
#   Copying and distribution of this file, with or without modification, are
#   permitted in any medium without royalty provided the copyright notice
#   and this notice are preserved. This file is offered as-is, without any
#   warranty.

#serial 11

AC_DEFUN([AX_LIB_PETSC], [

AC_MSG_CHECKING([for PETSc])

dnl Set defaults to blank
PETSC_DIR=
PETSC_LIBS=
PETSC_CFLAGS=
PETSC_FCFLAGS=
PETSC_FPPFLAGS=

dnl Add a default --with-petsc configuration option.
AC_ARG_WITH([petsc],
  AS_HELP_STRING(
    [--with-petsc=[no/PATH]],
    [Location of PETSc configuration]
  ),
  [if test "$withval" = "no"; then
     with_petsc="no"
   else
     with_petsc="yes"
     PETSC_DIR="$withval"
   fi],
  [with_petsc="no"]
)

if test "$with_petsc" = "yes"; then

  dnl Check if petscvariables is in the new (post-3.6) location or the old location
  PETSC_VAR_FILE_NEW="${PETSC_DIR}/lib/petsc/conf/petscvariables"
  PETSC_VAR_FILE_OLD="${PETSC_DIR}/conf/petscvariables"

  if test -f "$PETSC_VAR_FILE_NEW" -o -f "$PETSC_VAR_FILE_OLD"; then

    dnl If using PETSc version >= 3.6, Fortran header files are in a 'petsc' subdirectory
    if test -f "$PETSC_VAR_FILE_NEW"; then
      petsc_namespaced_headers=1
      PETSC_VAR_FILE="$PETSC_VAR_FILE_NEW"
    else
      petsc_namespaced_headers=0
      PETSC_VAR_FILE="$PETSC_VAR_FILE_OLD"
    fi

    dnl Find PETSc compilation/linking flags
    printf '%s\n' "include $PETSC_VAR_FILE" > Makefile_config_petsc
    printf '%s\n' "getPETSC_FC_INCLUDES:" >> Makefile_config_petsc
    printf '\t%s\n' "echo \$(PETSC_FC_INCLUDES)" >> Makefile_config_petsc
    printf '%s\n'  "getPETSC_CC_INCLUDES:" >> Makefile_config_petsc
    printf '\t%s\n' "echo \$(PETSC_CC_INCLUDES)" >> Makefile_config_petsc
    printf '%s\n' "getlinklibs:" >> Makefile_config_petsc
    printf '\t%s\n' "echo \$(PETSC_LIB)" >> Makefile_config_petsc
    PETSC_LIBS=`make -s -f Makefile_config_petsc getlinklibs 2>>config.log`
    PETSC_CFLAGS=`make -s -f Makefile_config_petsc getPETSC_CC_INCLUDES 2>>config.log`
    PETSC_FCFLAGS=`make -s -f Makefile_config_petsc getPETSC_FC_INCLUDES 2>>config.log`
    PETSC_FPPFLAGS=`make -s -f Makefile_config_petsc getPETSC_FC_INCLUDES 2>>config.log`
    rm -f Makefile_config_petsc

    dnl Flag to indicate which include directory to point to
    if test "$petsc_namespaced_headers" -eq 1; then
      PETSC_CFLAGS="$PETSC_CFLAGS -DPETSC_NAMESPACED_HEADERS"
      PETSC_FPPFLAGS="$PETSC_FPPFLAGS -DPETSC_NAMESPACED_HEADERS"
    fi

    dnl Test if Fortran program containing PETSc code can be compiled/linked successfully
    FPPFLAGS_TEMP="$FPPFLAGS"
    FCFLAGS_TEMP="$FCFLAGS"
    LIBS_TEMP="$LIBS"
    FPPFLAGS="$FPPFLAGS_TEMP $PETSC_FPPFLAGS"
    FCFLAGS="$FCFLAGS_TEMP $PETSC_FCFLAGS"
    LIBS="$LIBS_TEMP $PETSC_LIBS"
    echo "program petsctest
#ifdef PETSC_NAMESPACED_HEADERS
#include <petsc/finclude/petscsys.h>
#else
#include <finclude/petscsys.h>
#endif
      PetscErrorCode :: ierr
      call PetscInitialize(\"PETScOptions.txt\", ierr)
      call PetscFinalize(ierr)
    end program petsctest" > .conf_petsctest.fpp
    petsctest_source=`$FPP $FPPFLAGS .conf_petsctest.fpp 2>> config.log || true`
    rm -f .conf_petsctest*
    AC_LANG_PUSH([Fortran])
    AC_LINK_IFELSE([AC_LANG_SOURCE([[$petsctest_source]])], [petsc_works=yes], [petsc_works=no])
    AC_LANG_POP([Fortran])
    FPPFLAGS="$FPPFLAGS_TEMP"
    FCFLAGS="$FCFLAGS_TEMP"
    LIBS="$LIBS_TEMP"

    if test "$petsc_works" = "yes"; then
      AC_MSG_RESULT([$PETSC_DIR])
    else
      with_petsc=no
      AC_MSG_RESULT([no])
      AC_MSG_FAILURE([cannot build a basic PETSc program])
    fi

  else

    with_petsc=no
    AC_MSG_RESULT([no])
    AC_MSG_ERROR([cannot find petscvariables file])

  fi

else

  AC_MSG_RESULT([no])

fi

if test "$with_petsc" = "yes"; then

  dnl Check if PETScOptions* functions take a PetscOptions database parameter (version >= 3.7)
  CFLAGS_TEMP="$CFLAGS"
  CFLAGS="$CFLAGS_TEMP $PETSC_CFLAGS"
  petsctest_source="#include <petscsys.h>
  int main(int *argc, char **argv) {
    PetscBool has_option;
    PetscInitialize(argc, argv, PETSC_NULL, PETSC_NULL);
    PetscOptionsHasName(PETSC_NULL, PETSC_NULL, \"-some-option\", &has_option);
    PetscFinalize();
    return 0;
  }"
  AC_COMPILE_IFELSE([AC_LANG_SOURCE([[$petsctest_source]])], [petsc_options_takes_database=yes],
    [petsc_options_takes_database=no])
  CFLAGS="$CFLAGS_TEMP"

  if test "$petsc_options_takes_database" = "yes"; then
    PETSC_CFLAGS="$PETSC_CFLAGS -DPETSC_OPTIONS_TAKES_DATABASE"
    PETSC_FPPFLAGS="$PETSC_FPPFLAGS -DPETSC_OPTIONS_TAKES_DATABASE"
  fi

fi

AC_SUBST([PETSC_DIR])
AM_CONDITIONAL([HAVE_PETSC], [test x$with_petsc = xyes])
AS_IF([test "$with_petsc" = "yes"],
  [HAVE_PETSC=1],
  [HAVE_PETSC=0]
)
AC_SUBST([HAVE_PETSC])
if test "$with_petsc" = "yes"; then
  AC_DEFINE(HAVE_PETSC, 1, "Defined if building with PETSc")
fi

])
