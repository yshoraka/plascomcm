# Copyright (c) 2015, University of Illinois at Urbana-Champaign, XPACC
# License: MIT, http://opensource.org/licenses/MIT
#
AC_DEFUN([AX_PROG_MPI_FC],[
    if test -z "$FC" ; then
        AC_CHECK_TOOLS([FC], [mpif90 mpifort mpif95 mpxlf90_r mpixlf90_r mpxlf90 mpxlf95_r mpixlf95_r mpxlf95 mpixlf95 ftn mpf90 cmpif90c sxmpif90 mpxlf_r mpixlf_r mpxlf mpixlf mpifrt cmpifc xlf95 pgf95 pathf95 ifort g95 f95 fort ifc efc openf95 sunf95 crayftn gfortran lf95 ftn xlf90 f90 pgf90 pghpf pathf90 epcf90 sxf90 openf90 sunf90 xlf frt])
    fi
    # ToDo: Will this compile an MPI program?
])
