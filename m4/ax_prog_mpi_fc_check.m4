# Copyright (c) 2015, University of Illinois at Urbana-Champaign, XPACC
# License: MIT, http://opensource.org/licenses/MIT
#
# Must come AFTER all the autoconf processing of the Fortran compiler,
# since any options might impact the success of the test
AC_DEFUN([AX_PROG_MPI_FC_CHECK],[
AC_REQUIRE([AC_PROG_FC])
AC_MSG_CHECKING([whether $FC can build MPI programs])
AC_LANG([Fortran])
AC_LINK_IFELSE(
    [AC_LANG_PROGRAM([],[
       integer ierr
       call mpi_init(ierr)
       call mpi_finalize(ierr)
])],AC_MSG_RESULT([yes]),AC_MSG_ERROR([no]))
AC_MSG_CHECKING([for mpif.h])
AC_COMPILE_IFELSE([AC_LANG_PROGRAM([],[[
       include 'mpif.h'
]])],AC_MSG_RESULT([yes]),AC_MSG_ERROR([no]))
AC_MSG_CHECKING([for mpi module])
AC_COMPILE_IFELSE([AC_LANG_PROGRAM([],[
       use mpi
])],have_module_mpi=yes,have_module_mpi=no)
AC_MSG_RESULT([$have_module_mpi])
if test "$have_module_mpi" = "yes" ; then
    AC_DEFINE([HAVE_MPI_MODULE],[1],[Define if the MPI module is available])
fi
AC_MSG_CHECKING([for mpi_f08 module])
AC_COMPILE_IFELSE([AC_LANG_PROGRAM([],[
       use mpi_f08
])],have_module_mpi_f08=yes,have_module_mpi_f08=no)
AC_MSG_RESULT([$have_module_mpi_f08])
if test "$have_module_mpi_f08" = "yes" ; then
    AC_DEFINE([HAVE_MPI_F08_MODULE],[1],[Define if the MPI_F08 module is available])
fi
])
