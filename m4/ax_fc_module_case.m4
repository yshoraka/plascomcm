# Copyright (c) 2015, University of Illinois at Urbana-Champaign, XPACC
# License: MIT, http://opensource.org/licenses/MIT
#
AC_DEFUN([AX_FC_MODULE_CASE],[
dnl AC_REQUIRE([AX_FC_MODULE_EXTENSION])
AC_SUBST([FC_MODCASE])
if test "X$FC_MODCASE" = "X" ; then
    # The standard test failed to find the module extension.  Try with a few
    # commandline options
    AC_MSG_CHECKING([Fortran 90 module name case])
    AC_LANG([Fortran])
    # Run test in a separate directory to ensure that the ls check for
    # the module extension won't find another file that matches the check
    if test ! -d conftest.dir ; then mkdir conftest.dir ; fi
    cd conftest.dir
    ax_FCFLAGS_save=$FCFLAGS
    FCFLAGS="$FCFLAGS $FC_MODARG"
    AC_COMPILE_IFELSE([
      module conftest_Module
      contains
      subroutine conftest_routine
      write(*,'(a)') 'gotcha!'
      end subroutine
      end module
],[
    if test -f conftest_module.$FC_MODEXT ; then
      ax_cv_fc_module_case=lower
    elif test -f CONFTEST_MODULE.$FC_MODEXT ; then
      ax_cv_fc_module_case=upper
    elif test -f conftest_Module.$FC_MODEXT ; then
      ax_cv_fc_module_case=mixed
    else
      ax_cv_fc_module_case=unknown
    fi],[])
    FCFLAGS=$ax_FCFLAGS_save
    cd ..
    rm -rf conftest.dir
    if test "X$ax_cv_fc_module_case" = "X" ; then
        AC_MSG_RESULT([unknown])
    else
        FC_MODCASE=$ax_cv_fc_module_case
        AC_MSG_RESULT([$FC_MODCASE])
    fi
fi
])
