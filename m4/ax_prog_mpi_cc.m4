# Copyright (c) 2015, University of Illinois at Urbana-Champaign, XPACC
# License: MIT, http://opensource.org/licenses/MIT
# This macro looks for a compiler that can compile MPI programs.  If CC is
# defined, it first tries that.  If that compiler cannot compile MPI
# programs, it searches for another compiler
AC_DEFUN([AX_PROG_MPI_CC],[
# Let the user define a C compiler for MPI using MPICC.  If this is
# not set, try to find it using CC
# We have to do this first, because autoconf will otherwise incorrectly
# set CC for us.
if test -z "$MPICC" -a -z "$CC" ; then
    AC_CHECK_TOOLS([CC],[mpicc mpixlc_r mpixlc hcc mpxlc_r mpxlc sxmpicc mpifcc mpgcc mpcc cmpicc cc gcc])
    MPICC="$CC"
fi
])
# This is a separate test to keep Autoconf from hoisting a require on prog_cc
# before it might be reset
AC_DEFUN([AX_PROG_MPI_CC_WORKS],[
if test -n "$CC" ; then
    # check that CC can in fact compile an MPI program
    AC_CHECK_HEADER([mpi.h],[foundMPI=yes],[foundMPI=no])
    if test "$foundMPI" = "yes" ; then
       # check that CC can link MPI programs
       # This will update LIBS if necessary to add the library
       AC_SEARCH_LIBS([MPI_Init_thread],[mpich mpi],[foundMPI=yes],
					     [foundMPI=no])
    fi
    if test "$foundMPI" = "no" ; then
        AC_MSG_RESULT([Unable to find a C compiler for MPI])
    fi
else
    AC_MSG_ERROR([No C compiler found.  Run [AX_PROG_MPI_CC] first])
fi
])
