Input examples
===

This directory contains several examples as described below.

- Air_dv_spline.tbl
- Air_tv_spline.tbl
- MaterialData.tbl
- PETScOptions.txt
- TMbc.dat
- averaging.inp
- bc.dat
- plascomcm.inp
- plascomcm_hdf5.inp

See LICENSE.txt and COPYRIGHT.txt for more information.
