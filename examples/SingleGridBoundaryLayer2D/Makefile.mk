if BUILD_EXAMPLES

noinst_PROGRAMS += SingleGridBoundaryLayer2D/SingleGridBoundaryLayer2D

endif

SingleGridBoundaryLayer2D_SingleGridBoundaryLayer2D_SOURCES = \
  SingleGridBoundaryLayer2D/SingleGridBoundaryLayer2D.f90
SingleGridBoundaryLayer2D_SingleGridBoundaryLayer2D_DEPENDENCIES = \
  libexamples-common.a

example_extras += \
  SingleGridBoundaryLayer2D/README \
  SingleGridBoundaryLayer2D/plascomcm_plot3d.inp \
  SingleGridBoundaryLayer2D/plascomcm_hdf5.inp
