program SingleGridBoundaryLayer2D

  use ModExamplesCommon
  use ModBC
  use ModParseArguments
  use ModStdIO
  implicit none

  integer(ik) :: i, j
  character(len=CMD_ARG_LENGTH), dimension(:), allocatable :: RawArguments
  type(t_cmd_opt), dimension(2) :: Options
  integer(ik) :: N
  integer(ik) :: OutputFormat
  real(rk) :: U, V
  integer(ik), dimension(2) :: nPoints
  real(rk), dimension(2) :: Length
  real(rk), dimension(:,:,:), allocatable :: X
  real(rk), dimension(:,:,:), allocatable :: Q
  type(t_bc), dimension(3) :: BCs
  real(rk) :: Gamma
  real(rk) :: Temperature
  real(rk) :: Density
  real(rk), dimension(2) :: Velocity
  integer(ik), dimension(2) :: nSponge

  allocate(RawArguments(command_argument_count()))
  do i = 1, size(RawArguments)
    call get_command_argument(i, RawArguments(i))
  end do

  Options(1) = t_cmd_opt_("format", "f", OPT_VALUE_TYPE_STRING)
  Options(2) = t_cmd_opt_("size", "n", OPT_VALUE_TYPE_INTEGER)

  call ParseArguments(RawArguments, Options=Options)

  if (Options(1)%is_present) then
    if (Options(1)%value == "plot3d") then
      OutputFormat = OUTPUT_FORMAT_PLOT3D
    else if (Options(1)%value == "hdf5") then
      OutputFormat = OUTPUT_FORMAT_HDF5
    else
      write (ERROR_UNIT, '(3a)') "ERROR: Unrecognized grid format '", trim(Options(1)%value), "'."
      stop 1
    end if
  else
    OutputFormat = DefaultOutputFormat
  end if

  if (Options(2)%is_present) then
    read (Options(2)%value, *) N
  else
    N = 41
  end if

  nPoints = [N, N/2]

  Length = [1._rk, 0.5_rk]

  allocate(X(2,nPoints(1),nPoints(2)))

  do j = 1, nPoints(2)
    do i = 1, nPoints(1)
      U = real(i-1, kind=rk)/real(nPoints(1)-1, kind=rk)
      V = real(j-1, kind=rk)/real(nPoints(2)-1, kind=rk)
      X(:,i,j) = Length * ([U,V] - 0.5_rk)
    end do
  end do

  allocate(Q(4,nPoints(1),nPoints(2)))

  Gamma = 1.4_rk
  Temperature = 1._rk/(Gamma - 1._rk)
  Density = 1._rk
  Velocity = [0.2_rk, 0._rk]

  Q(1,:,:) = Density
  Q(2,:,:) = Density * Velocity(1)
  Q(3,:,:) = Density * Velocity(2)
  Q(4,:,:) = Density * (Temperature/Gamma + 0.5_rk * sum(Velocity**2))

  nSponge = nPoints/6

  BCs(1) = t_bc_(1, BC_TYPE_SAT_NOSLIP_ISOTHERMAL_WALL, 2, [1,-1], [1,1])
  BCs(2) = t_bc_(1, BC_TYPE_SAT_FAR_FIELD, -2, [1,-1], [-1,-1])
  BCs(3) = t_bc_(1, BC_TYPE_SPONGE, -2, [1,-1], [-nSponge(2),-1])

  select case (OutputFormat)
  case (OUTPUT_FORMAT_PLOT3D)
    call WritePLOT3DGrid("grid.xyz", nDims=2, nPoints=nPoints, X=X)
    call WritePLOT3DSolution("initial.q", nDims=2, nPoints=nPoints, Q=Q)
  case (OUTPUT_FORMAT_HDF5)
    call WriteHDF5("initial.h5", nDims=2, nPoints=nPoints, X=X, Q=Q, WriteTarget=.true.)
  end select

  call WriteBCs(BCs, "bc.dat")

end program SingleGridBoundaryLayer2D
