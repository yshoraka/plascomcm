program SingleGridBoundaryLayer3D

  use ModExamplesCommon
  use ModBC
  use ModParseArguments
  use ModStdIO
  implicit none

  integer(ik) :: i, j, k
  character(len=CMD_ARG_LENGTH), dimension(:), allocatable :: RawArguments
  type(t_cmd_opt), dimension(2) :: Options
  integer(ik) :: N
  integer(ik) :: OutputFormat
  character(len=PATH_LENGTH) :: OutputFile
  real(rk) :: U, V, W
  integer(ik), dimension(3) :: nPoints
  real(rk), dimension(3) :: Length
  real(rk), dimension(:,:,:,:), allocatable :: X
  real(rk), dimension(:,:,:,:), allocatable :: Q
  type(t_bc), dimension(3) :: BCs
  real(rk) :: Gamma
  real(rk) :: Temperature
  real(rk) :: Density
  real(rk), dimension(3) :: Velocity
  integer(ik), dimension(3) :: nSponge

  allocate(RawArguments(command_argument_count()))
  do i = 1, size(RawArguments)
    call get_command_argument(i, RawArguments(i))
  end do

  Options(1) = t_cmd_opt_("format", "f", OPT_VALUE_TYPE_STRING)
  Options(2) = t_cmd_opt_("size", "n", OPT_VALUE_TYPE_INTEGER)

  call ParseArguments(RawArguments, Options=Options)

  if (Options(1)%is_present) then
    if (Options(1)%value == "plot3d") then
      OutputFormat = OUTPUT_FORMAT_PLOT3D
    else if (Options(1)%value == "hdf5") then
      OutputFormat = OUTPUT_FORMAT_HDF5
    else
      write (ERROR_UNIT, '(3a)') "ERROR: Unrecognized grid format '", trim(Options(1)%value), "'."
      stop 1
    end if
  else
    OutputFormat = DefaultOutputFormat
  end if

  if (Options(2)%is_present) then
    read (Options(2)%value, *) N
  else
    N = 41
  end if

  nPoints = [N, N/2, N/2]

  Length = [1._rk, 0.5_rk, 0.5_rk]

  allocate(X(3,nPoints(1),nPoints(2),nPoints(3)))

  do k = 1, nPoints(3)
    do j = 1, nPoints(2)
      do i = 1, nPoints(1)
        U = real(i-1, kind=rk)/real(nPoints(1)-1, kind=rk)
        V = real(j-1, kind=rk)/real(nPoints(2)-1, kind=rk)
        W = real(k-1, kind=rk)/real(nPoints(3)-1, kind=rk)
        X(:,i,j,k) = Length * ([U,V,W] - 0.5_rk)
      end do
    end do
  end do

  allocate(Q(5,nPoints(1),nPoints(2),nPoints(3)))

  Gamma = 1.4_rk
  Temperature = 1._rk/(Gamma - 1._rk)
  Density = 1._rk
  Velocity = [0.2_rk, 0._rk, 0._rk]

  Q(1,:,:,:) = Density
  Q(2,:,:,:) = Density * Velocity(1)
  Q(3,:,:,:) = Density * Velocity(2)
  Q(4,:,:,:) = Density * Velocity(3)
  Q(5,:,:,:) = Density * (Temperature/Gamma + 0.5_rk * sum(Velocity**2))

  nSponge = nPoints/6

  BCs(1) = t_bc_(1, BC_TYPE_SAT_NOSLIP_ISOTHERMAL_WALL, 3, [1,-1], [1,-1], [1,1])
  BCs(2) = t_bc_(1, BC_TYPE_SAT_FAR_FIELD, -3, [1,-1], [1,-1], [-1,-1])
  BCs(3) = t_bc_(1, BC_TYPE_SPONGE, -3, [1,-1], [1,-1], [-nSponge(3),-1])

  select case (OutputFormat)
  case (OUTPUT_FORMAT_PLOT3D)
    call WritePLOT3DGrid("grid.xyz", nDims=3, nPoints=nPoints, X=X)
    call WritePLOT3DSolution("initial.q", nDims=3, nPoints=nPoints, Q=Q)
  case (OUTPUT_FORMAT_HDF5)
    call WriteHDF5("initial.h5", nDims=3, nPoints=nPoints, X=X, Q=Q, WriteTarget=.true.)
  end select

  call WriteBCs(BCs, "bc.dat")

end program SingleGridBoundaryLayer3D
