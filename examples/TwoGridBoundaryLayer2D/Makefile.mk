if BUILD_EXAMPLES
if BUILD_OVERSET

noinst_PROGRAMS += TwoGridBoundaryLayer2D/TwoGridBoundaryLayer2D

endif
endif

TwoGridBoundaryLayer2D_TwoGridBoundaryLayer2D_SOURCES = \
  TwoGridBoundaryLayer2D/TwoGridBoundaryLayer2D.f90
TwoGridBoundaryLayer2D_TwoGridBoundaryLayer2D_DEPENDENCIES = \
  libexamples-common.a $(abs_top_builddir)/utils/overset/liboverset.a

example_extras += \
  TwoGridBoundaryLayer2D/README \
  TwoGridBoundaryLayer2D/plascomcm_plot3d.inp \
  TwoGridBoundaryLayer2D/plascomcm_hdf5.inp
