module ModExamplesCommon

  use ModStdIO
  use ModPLOT3D_IO
#ifdef HAVE_HDF5
  use ModHDF5_IO
#endif

  implicit none

  private

  public :: rk, ik

  public :: MAX_ND

  public :: PATH_LENGTH

  public :: WritePLOT3DGrid
  public :: WritePLOT3DSolution
  public :: WritePLOT3DFunc
  public :: WriteHDF5

  public :: OUTPUT_FORMAT_PLOT3D
  public :: OUTPUT_FORMAT_HDF5

  public :: DefaultOutputFormat

  public :: Pi

  integer, parameter :: rk = selected_real_kind(15, 307)
  integer, parameter :: ik = selected_int_kind(9)

  integer(ik), parameter :: MAX_ND = 3

  integer(ik), parameter :: PATH_LENGTH = 256

  interface WritePLOT3DGrid
    module procedure WritePLOT3DGrid_Single_Rank2
    module procedure WritePLOT3DGrid_Single_Rank3
    module procedure WritePLOT3DGrid_Multiple_Rank2
    module procedure WritePLOT3DGrid_Multiple_Rank3
  end interface WritePLOT3DGrid

  interface WritePLOT3DSolution
    module procedure WritePLOT3DSolution_Single_Rank2
    module procedure WritePLOT3DSolution_Single_Rank3
    module procedure WritePLOT3DSolution_Multiple_Rank2
    module procedure WritePLOT3DSolution_Multiple_Rank3
  end interface WritePLOT3DSolution

  interface WritePLOT3DFunc
    module procedure WritePLOT3DFunc_Single_Rank2
    module procedure WritePLOT3DFunc_Single_Rank3
    module procedure WritePLOT3DFunc_Multiple_Rank2
    module procedure WritePLOT3DFunc_Multiple_Rank3
  end interface WritePLOT3DFunc

  interface WriteHDF5
    module procedure WriteHDF5_Single_Rank2
    module procedure WriteHDF5_Single_Rank3
    module procedure WriteHDF5_Multiple_Rank2
    module procedure WriteHDF5_Multiple_Rank3
  end interface WriteHDF5

  integer(ik), parameter :: OUTPUT_FORMAT_PLOT3D = 1
  integer(ik), parameter :: OUTPUT_FORMAT_HDF5 = 2

#ifdef HAVE_HDF5
    integer(ik), parameter :: DefaultOutputFormat = OUTPUT_FORMAT_HDF5
#else
    integer(ik), parameter :: DefaultOutputFormat = OUTPUT_FORMAT_PLOT3D
#endif

  real(rk), parameter :: Pi = 3.1415926535897932385_rk

contains

  subroutine WritePLOT3DGrid_Single_Rank2(Path, nDims, nPoints, X, IBlank)

    character(len=*), intent(in) :: Path
    integer(ik), intent(in) :: nDims
    integer(ik), dimension(nDims), intent(in) :: nPoints
    real(rk), dimension(:,:,:), intent(in) :: X
    integer(ik), dimension(:,:), intent(in), optional :: IBlank

    integer(ik) :: i, j
    character(len=PATH_LENGTH) :: Path_PLOT3D
    integer, dimension(:,:), pointer :: nPoints_PLOT3D
    real(rk), dimension(:,:,:,:,:), pointer :: X_PLOT3D
    integer, dimension(:,:,:,:), pointer :: IBlank_PLOT3D
    character(len=2) :: UseIBlank, CoordPrecision, GridFormat, VolumeFormat

    Path_PLOT3D = Path

    allocate(nPoints_PLOT3D(1,MAX_ND))

    nPoints_PLOT3D(1,:nDims) = nPoints
    nPoints_PLOT3D(1,nDims+1:) = 1

    allocate(X_PLOT3D(1,nPoints_PLOT3D(1,1),nPoints_PLOT3D(1,2),nPoints_PLOT3D(1,3),MAX_ND))

    do j = 1, nPoints_PLOT3D(1,2)
      do i = 1, nPoints_PLOT3D(1,1)
        X_PLOT3D(1,i,j,1,:nDims) = X(:,i,j)
        X_PLOT3D(1,i,j,1,nDims+1:) = 0._rk
      end do
    end do

    if (present(IBlank)) then
      UseIBlank = "y"
      allocate(IBlank_PLOT3D(1,nPoints_PLOT3D(1,1),nPoints_PLOT3D(1,2),nPoints_PLOT3D(1,3)))
      IBlank_PLOT3D(1,:,:,1) = IBlank
    else
      UseIBlank = "n"
      nullify(IBlank_PLOT3D)
    end if

    CoordPrecision = "d"
    GridFormat = "m"
    VolumeFormat = "w"

    call Write_Grid(MAX_ND, 1, nPoints_PLOT3D, X_PLOT3D, IBlank_PLOT3D, CoordPrecision, &
      GridFormat, VolumeFormat, UseIBlank, Path_PLOT3D)

    if (associated(IBLank_PLOT3D)) deallocate(IBlank_PLOT3D)
    deallocate(X_PLOT3D)
    deallocate(nPoints_PLOT3D)

  end subroutine WritePLOT3DGrid_Single_Rank2

  subroutine WritePLOT3DGrid_Single_Rank3(Path, nDims, nPoints, X, IBlank)

    character(len=*), intent(in) :: Path
    integer(ik), intent(in) :: nDims
    integer(ik), dimension(nDims), intent(in) :: nPoints
    real(rk), dimension(:,:,:,:), intent(in) :: X
    integer(ik), dimension(:,:,:), intent(in), optional :: IBlank

    integer(ik) :: i, j, k
    character(len=PATH_LENGTH) :: Path_PLOT3D
    integer, dimension(:,:), pointer :: nPoints_PLOT3D
    real(rk), dimension(:,:,:,:,:), pointer :: X_PLOT3D
    integer, dimension(:,:,:,:), pointer :: IBlank_PLOT3D
    character(len=2) :: UseIBlank, CoordPrecision, GridFormat, VolumeFormat

    Path_PLOT3D = Path

    allocate(nPoints_PLOT3D(1,MAX_ND))

    nPoints_PLOT3D(1,:nDims) = nPoints
    nPoints_PLOT3D(1,nDims+1:) = 1

    allocate(X_PLOT3D(1,nPoints_PLOT3D(1,1),nPoints_PLOT3D(1,2),nPoints_PLOT3D(1,3),MAX_ND))

    do k = 1, nPoints_PLOT3D(1,3)
      do j = 1, nPoints_PLOT3D(1,2)
        do i = 1, nPoints_PLOT3D(1,1)
          X_PLOT3D(1,i,j,k,:nDims) = X(:,i,j,k)
          X_PLOT3D(1,i,j,k,nDims+1:) = 0._rk
        end do
      end do
    end do

    if (present(IBlank)) then
      UseIBlank = "y"
      allocate(IBlank_PLOT3D(1,nPoints_PLOT3D(1,1),nPoints_PLOT3D(1,2),nPoints_PLOT3D(1,3)))
      IBlank_PLOT3D(1,:,:,:) = IBlank
    else
      UseIBlank = "n"
      nullify(IBlank_PLOT3D)
    end if

    CoordPrecision = "d"
    GridFormat = "m"
    VolumeFormat = "w"

    call Write_Grid(MAX_ND, 1, nPoints_PLOT3D, X_PLOT3D, IBlank_PLOT3D, CoordPrecision, &
      GridFormat, VolumeFormat, UseIBlank, Path_PLOT3D)

    if (associated(IBLank_PLOT3D)) deallocate(IBlank_PLOT3D)
    deallocate(X_PLOT3D)
    deallocate(nPoints_PLOT3D)

  end subroutine WritePLOT3DGrid_Single_Rank3

  subroutine WritePLOT3DGrid_Multiple_Rank2(Path, nDims, nGrids, nPoints, X, IBlank)

    character(len=*), intent(in) :: Path
    integer(ik), intent(in) :: nDims
    integer(ik), intent(in) :: nGrids
    integer(ik), dimension(nDims,nGrids), intent(in) :: nPoints
    real(rk), dimension(:,:,:,:), intent(in) :: X
    integer(ik), dimension(:,:,:), intent(in), optional :: IBlank

    integer(ik) :: i, j, m
    character(len=PATH_LENGTH) :: Path_PLOT3D
    integer, dimension(:,:), pointer :: nPoints_PLOT3D
    real(rk), dimension(:,:,:,:,:), pointer :: X_PLOT3D
    integer, dimension(:,:,:,:), pointer :: IBlank_PLOT3D
    character(len=2) :: UseIBlank, CoordPrecision, GridFormat, VolumeFormat

    Path_PLOT3D = Path

    allocate(nPoints_PLOT3D(nGrids,MAX_ND))

    do m = 1, nGrids
      nPoints_PLOT3D(m,:nDims) = nPoints(:,m)
      nPoints_PLOT3D(m,nDims+1:) = 1
    end do

    allocate(X_PLOT3D(nGrids,maxval(nPoints_PLOT3D(:,1)),maxval(nPoints_PLOT3D(:,2)), &
      maxval(nPoints_PLOT3D(:,3)),MAX_ND))

    do m = 1, nGrids
      do j = 1, nPoints_PLOT3D(m,2)
        do i = 1, nPoints_PLOT3D(m,1)
          X_PLOT3D(m,i,j,1,:nDims) = X(:,i,j,m)
          X_PLOT3D(m,i,j,1,nDims+1:) = 0._rk
        end do
      end do
    end do

    if (present(IBlank)) then
      UseIBlank = "y"
      allocate(IBlank_PLOT3D(nGrids,maxval(nPoints_PLOT3D(:,1)),maxval(nPoints_PLOT3D(:,2)), &
        maxval(nPoints_PLOT3D(:,3))))
      do m = 1, nGrids
        IBlank_PLOT3D(m,:,:,1) = IBlank(:,:,m)
      end do
    else
      UseIBlank = "n"
      nullify(IBlank_PLOT3D)
    end if

    CoordPrecision = "d"
    GridFormat = "m"
    VolumeFormat = "w"

    call Write_Grid(MAX_ND, nGrids, nPoints_PLOT3D, X_PLOT3D, IBlank_PLOT3D, CoordPrecision, &
      GridFormat, VolumeFormat, UseIBlank, Path_PLOT3D)

    if (associated(IBLank_PLOT3D)) deallocate(IBlank_PLOT3D)
    deallocate(X_PLOT3D)
    deallocate(nPoints_PLOT3D)

  end subroutine WritePLOT3DGrid_Multiple_Rank2

  subroutine WritePLOT3DGrid_Multiple_Rank3(Path, nDims, nGrids, nPoints, X, IBlank)

    character(len=*), intent(in) :: Path
    integer(ik), intent(in) :: nDims
    integer(ik), intent(in) :: nGrids
    integer(ik), dimension(nDims,nGrids), intent(in) :: nPoints
    real(rk), dimension(:,:,:,:,:), intent(in) :: X
    integer(ik), dimension(:,:,:,:), intent(in), optional :: IBlank

    integer(ik) :: i, j, k, m
    character(len=PATH_LENGTH) :: Path_PLOT3D
    integer, dimension(:,:), pointer :: nPoints_PLOT3D
    real(rk), dimension(:,:,:,:,:), pointer :: X_PLOT3D
    integer, dimension(:,:,:,:), pointer :: IBlank_PLOT3D
    character(len=2) :: UseIBlank, CoordPrecision, GridFormat, VolumeFormat

    Path_PLOT3D = Path

    allocate(nPoints_PLOT3D(nGrids,MAX_ND))

    do m = 1, nGrids
      nPoints_PLOT3D(m,:nDims) = nPoints(:,m)
      nPoints_PLOT3D(m,nDims+1:) = 1
    end do

    allocate(X_PLOT3D(nGrids,maxval(nPoints_PLOT3D(:,1)),maxval(nPoints_PLOT3D(:,2)), &
      maxval(nPoints_PLOT3D(:,3)),MAX_ND))

    do m = 1, nGrids
      do k = 1, nPoints_PLOT3D(m,3)
        do j = 1, nPoints_PLOT3D(m,2)
          do i = 1, nPoints_PLOT3D(m,1)
            X_PLOT3D(m,i,j,k,:nDims) = X(:,i,j,k,m)
            X_PLOT3D(m,i,j,k,nDims+1:) = 0._rk
          end do
        end do
      end do
    end do

    if (present(IBlank)) then
      UseIBlank = "y"
      allocate(IBlank_PLOT3D(nGrids,maxval(nPoints_PLOT3D(:,1)),maxval(nPoints_PLOT3D(:,2)), &
        maxval(nPoints_PLOT3D(:,3))))
      do m = 1, nGrids
        IBlank_PLOT3D(m,:,:,:) = IBlank(:,:,:,m)
      end do
    else
      UseIBlank = "n"
      nullify(IBlank_PLOT3D)
    end if

    CoordPrecision = "d"
    GridFormat = "m"
    VolumeFormat = "w"

    call Write_Grid(MAX_ND, nGrids, nPoints_PLOT3D, X_PLOT3D, IBlank_PLOT3D, CoordPrecision, &
      GridFormat, VolumeFormat, UseIBlank, Path_PLOT3D)

    if (associated(IBLank_PLOT3D)) deallocate(IBlank_PLOT3D)
    deallocate(X_PLOT3D)
    deallocate(nPoints_PLOT3D)

  end subroutine WritePLOT3DGrid_Multiple_Rank3

  subroutine WritePLOT3DSolution_Single_Rank2(Path, nDims, nPoints, Q, TimestepIndex, Time)

    character(len=*), intent(in) :: Path
    integer(ik), intent(in) :: nDims
    integer(ik), dimension(nDims), intent(in) :: nPoints
    real(rk), dimension(:,:,:), intent(in) :: Q
    integer(ik), intent(in), optional :: TimestepIndex
    real(rk), intent(in), optional :: Time

    integer(ik) :: i, j
    character(len=PATH_LENGTH) :: Path_PLOT3D
    integer, dimension(:,:), pointer :: nPoints_PLOT3D
    real(rk), dimension(:,:,:,:,:), pointer :: Q_PLOT3D
    real(rk), dimension(4) :: Tau
    character(len=2) :: CoordPrecision, GridFormat, VolumeFormat

    Path_PLOT3D = Path

    allocate(nPoints_PLOT3D(1,MAX_ND))

    nPoints_PLOT3D(1,:nDims) = nPoints
    nPoints_PLOT3D(1,nDims+1:) = 1

    allocate(Q_PLOT3D(1,nPoints_PLOT3D(1,1),nPoints_PLOT3D(1,2),nPoints_PLOT3D(1,3),MAX_ND+2))

    do j = 1, nPoints_PLOT3D(1,2)
      do i = 1, nPoints_PLOT3D(1,1)
        Q_PLOT3D(1,i,j,1,1) = Q(1,i,j)
        Q_PLOT3D(1,i,j,1,2) = Q(2,i,j)
        Q_PLOT3D(1,i,j,1,3) = Q(3,i,j)
        if (nDims == 3) then
          Q_PLOT3D(1,i,j,1,4) = Q(4,i,j)
          Q_PLOT3D(1,i,j,1,5) = Q(5,i,j)
        else
          Q_PLOT3D(1,i,j,1,4) = 0._rk
          Q_PLOT3D(1,i,j,1,5) = Q(4,i,j)
        end if
      end do
    end do

    Tau = 0._rk
    if (present(TimestepIndex)) then
      Tau(1) = real(TimestepIndex, kind=rk)
    end if
    if (present(Time)) then
      Tau(4) = Time
    end if

    CoordPrecision = "d"
    GridFormat = "m"
    VolumeFormat = "w"

    call Write_Soln(MAX_ND, 1, nPoints_PLOT3D, Q_PLOT3D, Tau, CoordPrecision, GridFormat, &
      VolumeFormat, Path_PLOT3D)

    deallocate(Q_PLOT3D)
    deallocate(nPoints_PLOT3D)

  end subroutine WritePLOT3DSolution_Single_Rank2

  subroutine WritePLOT3DSolution_Single_Rank3(Path, nDims, nPoints, Q, TimestepIndex, Time)

    character(len=*), intent(in) :: Path
    integer(ik), intent(in) :: nDims
    integer(ik), dimension(nDims), intent(in) :: nPoints
    real(rk), dimension(:,:,:,:), intent(in) :: Q
    integer(ik), intent(in), optional :: TimestepIndex
    real(rk), intent(in), optional :: Time

    integer(ik) :: i, j, k
    character(len=PATH_LENGTH) :: Path_PLOT3D
    integer, dimension(:,:), pointer :: nPoints_PLOT3D
    real(rk), dimension(:,:,:,:,:), pointer :: Q_PLOT3D
    real(rk), dimension(4) :: Tau
    character(len=2) :: CoordPrecision, GridFormat, VolumeFormat

    Path_PLOT3D = Path

    allocate(nPoints_PLOT3D(1,MAX_ND))

    nPoints_PLOT3D(1,:nDims) = nPoints
    nPoints_PLOT3D(1,nDims+1:) = 1

    allocate(Q_PLOT3D(1,nPoints_PLOT3D(1,1),nPoints_PLOT3D(1,2),nPoints_PLOT3D(1,3),MAX_ND+2))

    do k = 1, nPoints_PLOT3D(1,3)
      do j = 1, nPoints_PLOT3D(1,2)
        do i = 1, nPoints_PLOT3D(1,1)
          Q_PLOT3D(1,i,j,k,1) = Q(1,i,j,k)
          Q_PLOT3D(1,i,j,k,2) = Q(2,i,j,k)
          Q_PLOT3D(1,i,j,k,3) = Q(3,i,j,k)
          if (nDims == 3) then
            Q_PLOT3D(1,i,j,k,4) = Q(4,i,j,k)
            Q_PLOT3D(1,i,j,k,5) = Q(5,i,j,k)
          else
            Q_PLOT3D(1,i,j,k,4) = 0._rk
            Q_PLOT3D(1,i,j,k,5) = Q(4,i,j,k)
          end if
        end do
      end do
    end do

    Tau = 0._rk
    if (present(TimestepIndex)) then
      Tau(1) = real(TimestepIndex, kind=rk)
    end if
    if (present(Time)) then
      Tau(4) = Time
    end if

    CoordPrecision = "d"
    GridFormat = "m"
    VolumeFormat = "w"

    call Write_Soln(MAX_ND, 1, nPoints_PLOT3D, Q_PLOT3D, Tau, CoordPrecision, GridFormat, &
      VolumeFormat, Path_PLOT3D)

    deallocate(Q_PLOT3D)
    deallocate(nPoints_PLOT3D)

  end subroutine WritePLOT3DSolution_Single_Rank3

  subroutine WritePLOT3DSolution_Multiple_Rank2(Path, nDims, nGrids, nPoints, Q, TimestepIndex, &
    Time)

    character(len=*), intent(in) :: Path
    integer(ik), intent(in) :: nDims
    integer(ik), intent(in) :: nGrids
    integer(ik), dimension(nDims,nGrids), intent(in) :: nPoints
    real(rk), dimension(:,:,:,:), intent(in) :: Q
    integer(ik), intent(in), optional :: TimestepIndex
    real(rk), intent(in), optional :: Time

    integer(ik) :: i, j, m
    character(len=PATH_LENGTH) :: Path_PLOT3D
    integer, dimension(:,:), pointer :: nPoints_PLOT3D
    real(rk), dimension(:,:,:,:,:), pointer :: Q_PLOT3D
    real(rk), dimension(4) :: Tau
    character(len=2) :: CoordPrecision, GridFormat, VolumeFormat

    Path_PLOT3D = Path

    allocate(nPoints_PLOT3D(nGrids,MAX_ND))

    do m = 1, nGrids
      nPoints_PLOT3D(m,:nDims) = nPoints(:,m)
      nPoints_PLOT3D(m,nDims+1:) = 1
    end do

    allocate(Q_PLOT3D(nGrids,maxval(nPoints_PLOT3D(:,1)),maxval(nPoints_PLOT3D(:,2)), &
      maxval(nPoints_PLOT3D(:,3)),MAX_ND+2))

    do m = 1, nGrids
      do j = 1, nPoints_PLOT3D(m,2)
        do i = 1, nPoints_PLOT3D(m,1)
          Q_PLOT3D(m,i,j,1,1) = Q(1,i,j,m)
          Q_PLOT3D(m,i,j,1,2) = Q(2,i,j,m)
          Q_PLOT3D(m,i,j,1,3) = Q(3,i,j,m)
          if (nDims == 3) then
            Q_PLOT3D(m,i,j,1,4) = Q(4,i,j,m)
            Q_PLOT3D(m,i,j,1,5) = Q(5,i,j,m)
          else
            Q_PLOT3D(m,i,j,1,4) = 0._rk
            Q_PLOT3D(m,i,j,1,5) = Q(4,i,j,m)
          end if
        end do
      end do
    end do

    Tau = 0._rk
    if (present(TimestepIndex)) then
      Tau(1) = real(TimestepIndex, kind=rk)
    end if
    if (present(Time)) then
      Tau(4) = Time
    end if

    CoordPrecision = "d"
    GridFormat = "m"
    VolumeFormat = "w"

    call Write_Soln(MAX_ND, nGrids, nPoints_PLOT3D, Q_PLOT3D, Tau, CoordPrecision, GridFormat, &
      VolumeFormat, Path_PLOT3D)

    deallocate(Q_PLOT3D)
    deallocate(nPoints_PLOT3D)

  end subroutine WritePLOT3DSolution_Multiple_Rank2

  subroutine WritePLOT3DSolution_Multiple_Rank3(Path, nDims, nGrids, nPoints, Q, TimestepIndex, &
    Time)

    character(len=*), intent(in) :: Path
    integer(ik), intent(in) :: nDims
    integer(ik), intent(in) :: nGrids
    integer(ik), dimension(nDims,nGrids), intent(in) :: nPoints
    real(rk), dimension(:,:,:,:,:), intent(in) :: Q
    integer(ik), intent(in), optional :: TimestepIndex
    real(rk), intent(in), optional :: Time

    integer(ik) :: i, j, k, m
    character(len=PATH_LENGTH) :: Path_PLOT3D
    integer, dimension(:,:), pointer :: nPoints_PLOT3D
    real(rk), dimension(:,:,:,:,:), pointer :: Q_PLOT3D
    real(rk), dimension(4) :: Tau
    character(len=2) :: CoordPrecision, GridFormat, VolumeFormat

    Path_PLOT3D = Path

    allocate(nPoints_PLOT3D(nGrids,MAX_ND))

    do m = 1, nGrids
      nPoints_PLOT3D(m,:nDims) = nPoints(:,m)
      nPoints_PLOT3D(m,nDims+1:) = 1
    end do

    allocate(Q_PLOT3D(nGrids,maxval(nPoints_PLOT3D(:,1)),maxval(nPoints_PLOT3D(:,2)), &
      maxval(nPoints_PLOT3D(:,3)),MAX_ND+2))

    do m = 1, nGrids
      do k = 1, nPoints_PLOT3D(m,3)
        do j = 1, nPoints_PLOT3D(m,2)
          do i = 1, nPoints_PLOT3D(m,1)
            Q_PLOT3D(m,i,j,k,1) = Q(1,i,j,k,m)
            Q_PLOT3D(m,i,j,k,2) = Q(2,i,j,k,m)
            Q_PLOT3D(m,i,j,k,3) = Q(3,i,j,k,m)
            if (nDims == 3) then
              Q_PLOT3D(m,i,j,k,4) = Q(4,i,j,k,m)
              Q_PLOT3D(m,i,j,k,5) = Q(5,i,j,k,m)
            else
              Q_PLOT3D(m,i,j,k,4) = 0._rk
              Q_PLOT3D(m,i,j,k,5) = Q(4,i,j,k,m)
            end if
          end do
        end do
      end do
    end do

    Tau = 0._rk
    if (present(TimestepIndex)) then
      Tau(1) = real(TimestepIndex, kind=rk)
    end if
    if (present(Time)) then
      Tau(4) = Time
    end if

    CoordPrecision = "d"
    GridFormat = "m"
    VolumeFormat = "w"

    call Write_Soln(MAX_ND, nGrids, nPoints_PLOT3D, Q_PLOT3D, Tau, CoordPrecision, GridFormat, &
      VolumeFormat, Path_PLOT3D)

    deallocate(Q_PLOT3D)
    deallocate(nPoints_PLOT3D)

  end subroutine WritePLOT3DSolution_Multiple_Rank3

  subroutine WritePLOT3DFunc_Single_Rank2(Path, nDims, nPoints, F)

    character(len=*), intent(in) :: Path
    integer(ik), intent(in) :: nDims
    integer(ik), dimension(nDims), intent(in) :: nPoints
    real(rk), dimension(:,:,:), intent(in) :: F

    integer(ik) :: i, j, l
    character(len=PATH_LENGTH) :: Path_PLOT3D
    integer, dimension(:,:), pointer :: nPoints_PLOT3D
    real(rk), dimension(:,:,:,:,:), pointer :: F_PLOT3D

    Path_PLOT3D = Path

    allocate(nPoints_PLOT3D(1,MAX_ND))

    nPoints_PLOT3D(1,:nDims) = nPoints
    nPoints_PLOT3D(1,nDims+1:) = 1

    allocate(F_PLOT3D(1,nPoints_PLOT3D(1,1),nPoints_PLOT3D(1,2),nPoints_PLOT3D(1,3),size(F,1)))

    do j = 1, nPoints_PLOT3D(1,2)
      do i = 1, nPoints_PLOT3D(1,1)
        do l = 1, size(F,1)
          F_PLOT3D(1,i,j,1,l) = F(l,i,j)
        end do
      end do
    end do

    call Write_Func(1, nPoints_PLOT3D, F_PLOT3D, Path_PLOT3D)

    deallocate(F_PLOT3D)
    deallocate(nPoints_PLOT3D)

  end subroutine WritePLOT3DFunc_Single_Rank2

  subroutine WritePLOT3DFunc_Single_Rank3(Path, nDims, nPoints, F)

    character(len=*), intent(in) :: Path
    integer(ik), intent(in) :: nDims
    integer(ik), dimension(nDims), intent(in) :: nPoints
    real(rk), dimension(:,:,:,:), intent(in) :: F

    integer(ik) :: i, j, k, l
    character(len=PATH_LENGTH) :: Path_PLOT3D
    integer, dimension(:,:), pointer :: nPoints_PLOT3D
    real(rk), dimension(:,:,:,:,:), pointer :: F_PLOT3D

    Path_PLOT3D = Path

    allocate(nPoints_PLOT3D(1,MAX_ND))

    nPoints_PLOT3D(1,:nDims) = nPoints
    nPoints_PLOT3D(1,nDims+1:) = 1

    allocate(F_PLOT3D(1,nPoints_PLOT3D(1,1),nPoints_PLOT3D(1,2),nPoints_PLOT3D(1,3),size(F,1)))

    do k = 1, nPoints_PLOT3D(1,3)
      do j = 1, nPoints_PLOT3D(1,2)
        do i = 1, nPoints_PLOT3D(1,1)
          do l = 1, size(F,1)
            F_PLOT3D(1,i,j,k,l) = F(l,i,j,k)
          end do
        end do
      end do
    end do

    call Write_Func(1, nPoints_PLOT3D, F_PLOT3D, Path_PLOT3D)

    deallocate(F_PLOT3D)
    deallocate(nPoints_PLOT3D)

  end subroutine WritePLOT3DFunc_Single_Rank3

  subroutine WritePLOT3DFunc_Multiple_Rank2(Path, nDims, nGrids, nPoints, F)

    character(len=*), intent(in) :: Path
    integer(ik), intent(in) :: nDims
    integer(ik), intent(in) :: nGrids
    integer(ik), dimension(nDims,nGrids), intent(in) :: nPoints
    real(rk), dimension(:,:,:,:), intent(in) :: F

    integer(ik) :: i, j, l, m
    character(len=PATH_LENGTH) :: Path_PLOT3D
    integer, dimension(:,:), pointer :: nPoints_PLOT3D
    real(rk), dimension(:,:,:,:,:), pointer :: F_PLOT3D

    Path_PLOT3D = Path

    allocate(nPoints_PLOT3D(nGrids,MAX_ND))

    do m = 1, nGrids
      nPoints_PLOT3D(m,:nDims) = nPoints(:,m)
      nPoints_PLOT3D(m,nDims+1:) = 1
    end do

    allocate(F_PLOT3D(nGrids,maxval(nPoints_PLOT3D(:,1)),maxval(nPoints_PLOT3D(:,2)), &
      maxval(nPoints_PLOT3D(:,3)),size(F,1)))

    do m = 1, nGrids
      do j = 1, nPoints_PLOT3D(m,2)
        do i = 1, nPoints_PLOT3D(m,1)
          do l = 1, size(F,1)
            F_PLOT3D(m,i,j,1,l) = F(l,i,j,m)
          end do
        end do
      end do
    end do

    call Write_Func(nGrids, nPoints_PLOT3D, F_PLOT3D, Path_PLOT3D)

    deallocate(F_PLOT3D)
    deallocate(nPoints_PLOT3D)

  end subroutine WritePLOT3DFunc_Multiple_Rank2

  subroutine WritePLOT3DFunc_Multiple_Rank3(Path, nDims, nGrids, nPoints, F)

    character(len=*), intent(in) :: Path
    integer(ik), intent(in) :: nDims
    integer(ik), intent(in) :: nGrids
    integer(ik), dimension(nDims,nGrids), intent(in) :: nPoints
    real(rk), dimension(:,:,:,:,:), intent(in) :: F

    integer(ik) :: i, j, k, l, m
    character(len=PATH_LENGTH) :: Path_PLOT3D
    integer, dimension(:,:), pointer :: nPoints_PLOT3D
    real(rk), dimension(:,:,:,:,:), pointer :: F_PLOT3D

    Path_PLOT3D = Path

    allocate(nPoints_PLOT3D(nGrids,MAX_ND))

    do m = 1, nGrids
      nPoints_PLOT3D(m,:nDims) = nPoints(:,m)
      nPoints_PLOT3D(m,nDims+1:) = 1
    end do

    allocate(F_PLOT3D(nGrids,maxval(nPoints_PLOT3D(:,1)),maxval(nPoints_PLOT3D(:,2)), &
      maxval(nPoints_PLOT3D(:,3)),size(F,1)))

    do m = 1, nGrids
      do k = 1, nPoints_PLOT3D(m,3)
        do j = 1, nPoints_PLOT3D(m,2)
          do i = 1, nPoints_PLOT3D(m,1)
            do l = 1, size(F,1)
              F_PLOT3D(m,i,j,k,l) = F(l,i,j,k,m)
            end do
          end do
        end do
      end do
    end do

    call Write_Func(nGrids, nPoints_PLOT3D, F_PLOT3D, Path_PLOT3D)

    deallocate(F_PLOT3D)
    deallocate(nPoints_PLOT3D)

  end subroutine WritePLOT3DFunc_Multiple_Rank3

  subroutine WriteHDF5_Single_Rank2(Path, nDims, nPoints, X, IBlank, Q, QAux, WriteTarget, &
    WriteAuxTarget, TimestepIndex, Time)

    character(len=*), intent(in) :: Path
    integer(ik), intent(in) :: nDims
    integer(ik), dimension(nDims), intent(in) :: nPoints
    real(rk), dimension(:,:,:), intent(in) :: X
    integer(ik), dimension(:,:), intent(in), optional :: IBlank
    real(rk), dimension(:,:,:), intent(in), optional :: Q
    real(rk), dimension(:,:,:), intent(in), optional :: QAux
    logical, intent(in), optional :: WriteTarget
    logical, intent(in), optional :: WriteAuxTarget
    integer(ik), intent(in), optional :: TimestepIndex
    real(rk), intent(in), optional :: Time

#ifdef HAVE_HDF5

    integer(ik) :: i, j
    integer(ik) :: nVarsToWrite
    integer(ik) :: NextVarToWrite

    filename_HDF5 = Path

    nVarsToWrite = 1
    if (present(Q)) then
      nVarsToWrite = nVarsToWrite + 1
      if (present(WriteTarget)) then
        if (WriteTarget) nVarsToWrite = nVarsToWrite + 1
      end if
    end if
    if (present(QAux)) then
      nVarsToWrite = nVarsToWrite + 1
      if (present(WriteAuxTarget)) then
        if (WriteAuxTarget) nVarsToWrite = nVarsToWrite + 1
      end if
    end if

    allocate(vars_to_write_HDF5(nVarsToWrite))

    vars_to_write_HDF5(1) = "xyz"
    NextVarToWrite = 2
    if (present(Q)) then
      vars_to_write_HDF5(NextVarToWrite) = "cv"
      NextVarToWrite = NextVarToWrite + 1
      if (present(WriteTarget)) then
        if (WriteTarget) then
          vars_to_write_HDF5(NextVarToWrite) = "cvt"
          NextVarToWrite = NextVarToWrite + 1
        end if
      end if
    end if
    if (present(QAux)) then
      vars_to_write_HDF5(NextVarToWrite) = "aux"
      NextVarToWrite = NextVarToWrite + 1
      if (present(WriteAuxTarget)) then
        if (WriteAuxTarget) then
          vars_to_write_HDF5(NextVarToWrite) = "aut"
          NextVarToWrite = NextVarToWrite + 1
        end if
      end if
    end if

    NGRID_HDF5 = 1
    NDIM_HDF5 = nDims
    if (present(QAux)) then
      nAuxVars_HDF5 = size(QAux,3)
    else
      nAuxVars_HDF5 = 0
    end if

    allocate(ND_HDF5(1,MAX_ND))

    ND_HDF5(1,:nDims) = nPoints
    ND_HDF5(1,nDims+1:) = 1

    allocate(CONFIG_HDF5(1))

    if (present(TimestepIndex)) then
      CONFIG_HDF5(1)%t_iter = TimestepIndex
    else
      CONFIG_HDF5(1)%t_iter = 0
    end if
    CONFIG_HDF5(1)%NDIM = NDIM_HDF5
    CONFIG_HDF5(1)%ND = ND_HDF5(1,:)
    if (present(IBlank)) then
      CONFIG_HDF5(1)%Use_IB = 1
    else
      CONFIG_HDF5(1)%Use_IB = 0
    end if
    if (present(Time)) then
      CONFIG_HDF5(1)%time = Time
    else
      CONFIG_HDF5(1)%time = 0._rk
    end if
    CONFIG_HDF5(1)%Re = 0._rk
    CONFIG_HDF5(1)%Pr = 0._rk
    CONFIG_HDF5(1)%Sc = 0._rk

    allocate(X_HDF5(1,ND_HDF5(1,1),ND_HDF5(1,2),ND_HDF5(1,3),nDims))
    do j = 1, ND_HDF5(1,2)
      do i = 1, ND_HDF5(1,1)
        X_HDF5(1,i,j,1,:) = X(:,i,j)
      end do
    end do

    if (present(IBlank)) then
      allocate(IBLANK_HDF5(1,ND_HDF5(1,1),ND_HDF5(1,2),ND_HDF5(1,3)))
      do j = 1, ND_HDF5(1,2)
        do i = 1, ND_HDF5(1,1)
          IBLANK_HDF5(1,i,j,1) = IBlank(i,j)
        end do
      end do
    else
      nullify(IBLANK_HDF5)
    end if

    if (present(Q)) then
      allocate(Q_HDF5(1,ND_HDF5(1,1),ND_HDF5(1,2),ND_HDF5(1,3),nDims+2))
      do j = 1, ND_HDF5(1,2)
        do i = 1, ND_HDF5(1,1)
          Q_HDF5(1,i,j,1,:) = Q(:,i,j)
        end do
      end do
    else
      nullify(Q_HDF5)
    end if

    if (present(QAux)) then
      allocate(QAux_HDF5(1,ND_HDF5(1,1),ND_HDF5(1,2),ND_HDF5(1,3),nAuxVars_HDF5))
      do j = 1, ND_HDF5(1,2)
        do i = 1, ND_HDF5(1,1)
          QAux_HDF5(1,i,j,1,:) = QAux(:,i,j)
        end do
      end do
    else
      nullify(QAux_HDF5)
    end if

    call Write_HDF5

    if (associated(QAux_HDF5)) deallocate(QAux_HDF5)
    if (associated(Q_HDF5)) deallocate(Q_HDF5)
    if (associated(IBLANK_HDF5)) deallocate(IBLANK_HDF5)
    deallocate(X_HDF5)
    deallocate(CONFIG_HDF5)
    deallocate(ND_HDF5)
    deallocate(vars_to_write_HDF5)

#else

    write (ERROR_UNIT, '(a)') "ERROR: HDF5 support is not enabled."
    stop 1

#endif

  end subroutine WriteHDF5_Single_Rank2

  subroutine WriteHDF5_Single_Rank3(Path, nDims, nPoints, X, IBlank, Q, QAux, WriteTarget, &
    WriteAuxTarget, TimestepIndex, Time)

    character(len=*), intent(in) :: Path
    integer(ik), intent(in) :: nDims
    integer(ik), dimension(nDims), intent(in) :: nPoints
    real(rk), dimension(:,:,:,:), intent(in) :: X
    integer(ik), dimension(:,:,:), intent(in), optional :: IBlank
    real(rk), dimension(:,:,:,:), intent(in), optional :: Q
    real(rk), dimension(:,:,:,:), intent(in), optional :: QAux
    logical, intent(in), optional :: WriteTarget
    logical, intent(in), optional :: WriteAuxTarget
    integer(ik), intent(in), optional :: TimestepIndex
    real(rk), intent(in), optional :: Time

#ifdef HAVE_HDF5

    integer(ik) :: i, j, k
    integer(ik) :: nVarsToWrite
    integer(ik) :: NextVarToWrite

    filename_HDF5 = Path

    nVarsToWrite = 1
    if (present(Q)) then
      nVarsToWrite = nVarsToWrite + 1
      if (present(WriteTarget)) then
        if (WriteTarget) nVarsToWrite = nVarsToWrite + 1
      end if
    end if
    if (present(QAux)) then
      nVarsToWrite = nVarsToWrite + 1
      if (present(WriteAuxTarget)) then
        if (WriteAuxTarget) nVarsToWrite = nVarsToWrite + 1
      end if
    end if

    allocate(vars_to_write_HDF5(nVarsToWrite))

    vars_to_write_HDF5(1) = "xyz"
    NextVarToWrite = 2
    if (present(Q)) then
      vars_to_write_HDF5(NextVarToWrite) = "cv"
      NextVarToWrite = NextVarToWrite + 1
      if (present(WriteTarget)) then
        if (WriteTarget) then
          vars_to_write_HDF5(NextVarToWrite) = "cvt"
          NextVarToWrite = NextVarToWrite + 1
        end if
      end if
    end if
    if (present(QAux)) then
      vars_to_write_HDF5(NextVarToWrite) = "aux"
      NextVarToWrite = NextVarToWrite + 1
      if (present(WriteAuxTarget)) then
        if (WriteAuxTarget) then
          vars_to_write_HDF5(NextVarToWrite) = "aut"
          NextVarToWrite = NextVarToWrite + 1
        end if
      end if
    end if

    NGRID_HDF5 = 1
    NDIM_HDF5 = nDims
    if (present(QAux)) then
      nAuxVars_HDF5 = size(QAux,4)
    else
      nAuxVars_HDF5 = 0
    end if

    allocate(ND_HDF5(1,MAX_ND))

    ND_HDF5(1,:nDims) = nPoints
    ND_HDF5(1,nDims+1:) = 1

    allocate(CONFIG_HDF5(1))

    if (present(TimestepIndex)) then
      CONFIG_HDF5(1)%t_iter = TimestepIndex
    else
      CONFIG_HDF5(1)%t_iter = 0
    end if
    CONFIG_HDF5(1)%NDIM = NDIM_HDF5
    CONFIG_HDF5(1)%ND = ND_HDF5(1,:)
    if (present(IBlank)) then
      CONFIG_HDF5(1)%Use_IB = 1
    else
      CONFIG_HDF5(1)%Use_IB = 0
    end if
    if (present(Time)) then
      CONFIG_HDF5(1)%time = Time
    else
      CONFIG_HDF5(1)%time = 0._rk
    end if
    CONFIG_HDF5(1)%Re = 0._rk
    CONFIG_HDF5(1)%Pr = 0._rk
    CONFIG_HDF5(1)%Sc = 0._rk

    allocate(X_HDF5(1,ND_HDF5(1,1),ND_HDF5(1,2),ND_HDF5(1,3),nDims))
    do k = 1, ND_HDF5(1,3)
      do j = 1, ND_HDF5(1,2)
        do i = 1, ND_HDF5(1,1)
          X_HDF5(1,i,j,k,:) = X(:,i,j,k)
        end do
      end do
    end do

    if (present(IBlank)) then
      allocate(IBLANK_HDF5(1,ND_HDF5(1,1),ND_HDF5(1,2),ND_HDF5(1,3)))
      do k = 1, ND_HDF5(1,3)
        do j = 1, ND_HDF5(1,2)
          do i = 1, ND_HDF5(1,1)
            IBLANK_HDF5(1,i,j,k) = IBlank(i,j,k)
          end do
        end do
      end do
    else
      nullify(IBLANK_HDF5)
    end if

    if (present(Q)) then
      allocate(Q_HDF5(1,ND_HDF5(1,1),ND_HDF5(1,2),ND_HDF5(1,3),nDims+2))
      do k = 1, ND_HDF5(1,3)
        do j = 1, ND_HDF5(1,2)
          do i = 1, ND_HDF5(1,1)
            Q_HDF5(1,i,j,k,:) = Q(:,i,j,k)
          end do
        end do
      end do
    else
      nullify(Q_HDF5)
    end if

    if (present(QAux)) then
      allocate(QAux_HDF5(1,ND_HDF5(1,1),ND_HDF5(1,2),ND_HDF5(1,3),nAuxVars_HDF5))
      do k = 1, ND_HDF5(1,3)
        do j = 1, ND_HDF5(1,2)
          do i = 1, ND_HDF5(1,1)
            QAux_HDF5(1,i,j,k,:) = QAux(:,i,j,k)
          end do
        end do
      end do
    else
      nullify(QAux_HDF5)
    end if

    call Write_HDF5

    if (associated(QAux_HDF5)) deallocate(QAux_HDF5)
    if (associated(Q_HDF5)) deallocate(Q_HDF5)
    if (associated(IBLANK_HDF5)) deallocate(IBLANK_HDF5)
    deallocate(X_HDF5)
    deallocate(CONFIG_HDF5)
    deallocate(ND_HDF5)
    deallocate(vars_to_write_HDF5)

#else

    write (ERROR_UNIT, '(a)') "ERROR: HDF5 support is not enabled."
    stop 1

#endif

  end subroutine WriteHDF5_Single_Rank3

  subroutine WriteHDF5_Multiple_Rank2(Path, nDims, nGrids, nPoints, X, IBlank, Q, QAux, &
    WriteTarget, WriteAuxTarget, TimestepIndex, Time)

    character(len=*), intent(in) :: Path
    integer(ik), intent(in) :: nDims
    integer(ik), intent(in) :: nGrids
    integer(ik), dimension(nDims,nGrids), intent(in) :: nPoints
    real(rk), dimension(:,:,:,:), intent(in) :: X
    integer(ik), dimension(:,:,:), intent(in), optional :: IBlank
    real(rk), dimension(:,:,:,:), intent(in), optional :: Q
    real(rk), dimension(:,:,:,:), intent(in), optional :: QAux
    logical, intent(in), optional :: WriteTarget
    logical, intent(in), optional :: WriteAuxTarget
    integer(ik), intent(in), optional :: TimestepIndex
    real(rk), intent(in), optional :: Time

#ifdef HAVE_HDF5

    integer(ik) :: i, j, m
    integer(ik) :: nVarsToWrite
    integer(ik) :: NextVarToWrite

    filename_HDF5 = Path

    nVarsToWrite = 1
    if (present(Q)) then
      nVarsToWrite = nVarsToWrite + 1
      if (present(WriteTarget)) then
        if (WriteTarget) nVarsToWrite = nVarsToWrite + 1
      end if
    end if
    if (present(QAux)) then
      nVarsToWrite = nVarsToWrite + 1
      if (present(WriteAuxTarget)) then
        if (WriteAuxTarget) nVarsToWrite = nVarsToWrite + 1
      end if
    end if

    allocate(vars_to_write_HDF5(nVarsToWrite))

    vars_to_write_HDF5(1) = "xyz"
    NextVarToWrite = 2
    if (present(Q)) then
      vars_to_write_HDF5(NextVarToWrite) = "cv"
      NextVarToWrite = NextVarToWrite + 1
      if (present(WriteTarget)) then
        if (WriteTarget) then
          vars_to_write_HDF5(NextVarToWrite) = "cvt"
          NextVarToWrite = NextVarToWrite + 1
        end if
      end if
    end if
    if (present(QAux)) then
      vars_to_write_HDF5(NextVarToWrite) = "aux"
      NextVarToWrite = NextVarToWrite + 1
      if (present(WriteAuxTarget)) then
        if (WriteAuxTarget) then
          vars_to_write_HDF5(NextVarToWrite) = "aut"
          NextVarToWrite = NextVarToWrite + 1
        end if
      end if
    end if

    NGRID_HDF5 = nGrids
    NDIM_HDF5 = nDims
    if (present(QAux)) then
      nAuxVars_HDF5 = size(QAux,4)
    else
      nAuxVars_HDF5 = 0
    end if

    allocate(ND_HDF5(nGrids,MAX_ND))

    do m = 1, nGrids
      ND_HDF5(m,:nDims) = nPoints(:,m)
      ND_HDF5(m,nDims+1:) = 1
    end do

    allocate(CONFIG_HDF5(nGrids))

    do i = 1, nGrids
      if (present(TimestepIndex)) then
        CONFIG_HDF5(i)%t_iter = TimestepIndex
      else
        CONFIG_HDF5(i)%t_iter = 0
      end if
      CONFIG_HDF5(i)%NDIM = NDIM_HDF5
      CONFIG_HDF5(i)%ND = ND_HDF5(i,:)
      if (present(IBlank)) then
        CONFIG_HDF5(i)%Use_IB = 1
      else
        CONFIG_HDF5(i)%Use_IB = 0
      end if
      if (present(Time)) then
        CONFIG_HDF5(i)%time = Time
      else
        CONFIG_HDF5(i)%time = 0._rk
      end if
      CONFIG_HDF5(i)%Re = 0._rk
      CONFIG_HDF5(i)%Pr = 0._rk
      CONFIG_HDF5(i)%Sc = 0._rk
    end do

    allocate(X_HDF5(nGrids,maxval(ND_HDF5(:,1)),maxval(ND_HDF5(:,2)), &
      maxval(ND_HDF5(:,3)),nDims))
    do m = 1, nGrids
      do j = 1, ND_HDF5(m,2)
        do i = 1, ND_HDF5(m,1)
          X_HDF5(m,i,j,1,:) = X(:,i,j,m)
        end do
      end do
    end do

    if (present(IBlank)) then
      allocate(IBLANK_HDF5(nGrids,maxval(ND_HDF5(:,1)),maxval(ND_HDF5(:,2)), &
        maxval(ND_HDF5(:,3))))
      do m = 1, nGrids
        do j = 1, ND_HDF5(m,2)
          do i = 1, ND_HDF5(m,1)
            IBLANK_HDF5(m,i,j,1) = IBlank(i,j,m)
          end do
        end do
      end do
    else
      nullify(IBLANK_HDF5)
    end if

    if (present(Q)) then
      allocate(Q_HDF5(nGrids,maxval(ND_HDF5(:,1)),maxval(ND_HDF5(:,2)), &
        maxval(ND_HDF5(:,3)),nDims+2))
      do m = 1, nGrids
        do j = 1, ND_HDF5(m,2)
          do i = 1, ND_HDF5(m,1)
            Q_HDF5(m,i,j,1,:) = Q(:,i,j,m)
          end do
        end do
      end do
    else
      nullify(Q_HDF5)
    end if

    if (present(QAux)) then
      allocate(QAux_HDF5(nGrids,maxval(ND_HDF5(:,1)),maxval(ND_HDF5(:,2)), &
        maxval(ND_HDF5(:,3)),nAuxVars_HDF5))
      do m = 1, nGrids
        do j = 1, ND_HDF5(m,2)
          do i = 1, ND_HDF5(m,1)
            QAux_HDF5(m,i,j,1,:) = QAux(:,i,j,m)
          end do
        end do
      end do
    else
      nullify(QAux_HDF5)
    end if

    call Write_HDF5

    if (associated(QAux_HDF5)) deallocate(QAux_HDF5)
    if (associated(Q_HDF5)) deallocate(Q_HDF5)
    if (associated(IBLANK_HDF5)) deallocate(IBLANK_HDF5)
    deallocate(X_HDF5)
    deallocate(CONFIG_HDF5)
    deallocate(ND_HDF5)
    deallocate(vars_to_write_HDF5)

#else

    write (ERROR_UNIT, '(a)') "ERROR: HDF5 support is not enabled."
    stop 1

#endif

  end subroutine WriteHDF5_Multiple_Rank2

  subroutine WriteHDF5_Multiple_Rank3(Path, nDims, nGrids, nPoints, X, IBlank, Q, QAux, &
    WriteTarget, WriteAuxTarget, TimestepIndex, Time)

    character(len=*), intent(in) :: Path
    integer(ik), intent(in) :: nDims
    integer(ik), intent(in) :: nGrids
    integer(ik), dimension(nDims,nGrids), intent(in) :: nPoints
    real(rk), dimension(:,:,:,:,:), intent(in) :: X
    integer(ik), dimension(:,:,:,:), intent(in), optional :: IBlank
    real(rk), dimension(:,:,:,:,:), intent(in), optional :: Q
    real(rk), dimension(:,:,:,:,:), intent(in), optional :: QAux
    logical, intent(in), optional :: WriteTarget
    logical, intent(in), optional :: WriteAuxTarget
    integer(ik), intent(in), optional :: TimestepIndex
    real(rk), intent(in), optional :: Time

#ifdef HAVE_HDF5

    integer(ik) :: i, j, k, m
    integer(ik) :: nVarsToWrite
    integer(ik) :: NextVarToWrite

    filename_HDF5 = Path

    nVarsToWrite = 1
    if (present(Q)) then
      nVarsToWrite = nVarsToWrite + 1
      if (present(WriteTarget)) then
        if (WriteTarget) nVarsToWrite = nVarsToWrite + 1
      end if
    end if
    if (present(QAux)) then
      nVarsToWrite = nVarsToWrite + 1
      if (present(WriteAuxTarget)) then
        if (WriteAuxTarget) nVarsToWrite = nVarsToWrite + 1
      end if
    end if

    allocate(vars_to_write_HDF5(nVarsToWrite))

    vars_to_write_HDF5(1) = "xyz"
    NextVarToWrite = 2
    if (present(Q)) then
      vars_to_write_HDF5(NextVarToWrite) = "cv"
      NextVarToWrite = NextVarToWrite + 1
      if (present(WriteTarget)) then
        if (WriteTarget) then
          vars_to_write_HDF5(NextVarToWrite) = "cvt"
          NextVarToWrite = NextVarToWrite + 1
        end if
      end if
    end if
    if (present(QAux)) then
      vars_to_write_HDF5(NextVarToWrite) = "aux"
      NextVarToWrite = NextVarToWrite + 1
      if (present(WriteAuxTarget)) then
        if (WriteAuxTarget) then
          vars_to_write_HDF5(NextVarToWrite) = "aut"
          NextVarToWrite = NextVarToWrite + 1
        end if
      end if
    end if

    NGRID_HDF5 = nGrids
    NDIM_HDF5 = nDims
    if (present(QAux)) then
      nAuxVars_HDF5 = size(QAux,5)
    else
      nAuxVars_HDF5 = 0
    end if

    allocate(ND_HDF5(nGrids,MAX_ND))

    do m = 1, nGrids
      ND_HDF5(m,:nDims) = nPoints(:,m)
      ND_HDF5(m,nDims+1:) = 1
    end do

    allocate(CONFIG_HDF5(nGrids))

    do i = 1, nGrids
      if (present(TimestepIndex)) then
        CONFIG_HDF5(i)%t_iter = TimestepIndex
      else
        CONFIG_HDF5(i)%t_iter = 0
      end if
      CONFIG_HDF5(i)%NDIM = NDIM_HDF5
      CONFIG_HDF5(i)%ND = ND_HDF5(i,:)
      if (present(IBlank)) then
        CONFIG_HDF5(i)%Use_IB = 1
      else
        CONFIG_HDF5(i)%Use_IB = 0
      end if
      if (present(Time)) then
        CONFIG_HDF5(i)%time = Time
      else
        CONFIG_HDF5(i)%time = 0._rk
      end if
      CONFIG_HDF5(i)%Re = 0._rk
      CONFIG_HDF5(i)%Pr = 0._rk
      CONFIG_HDF5(i)%Sc = 0._rk
    end do

    allocate(X_HDF5(nGrids,maxval(ND_HDF5(:,1)),maxval(ND_HDF5(:,2)), &
      maxval(ND_HDF5(:,3)),nDims))
    do m = 1, nGrids
      do k = 1, ND_HDF5(m,3)
        do j = 1, ND_HDF5(m,2)
          do i = 1, ND_HDF5(m,1)
            X_HDF5(m,i,j,k,:) = X(:,i,j,k,m)
          end do
        end do
      end do
    end do

    if (present(IBlank)) then
      allocate(IBLANK_HDF5(nGrids,maxval(ND_HDF5(:,1)),maxval(ND_HDF5(:,2)), &
        maxval(ND_HDF5(:,3))))
      do m = 1, nGrids
        do k = 1, ND_HDF5(m,3)
          do j = 1, ND_HDF5(m,2)
            do i = 1, ND_HDF5(m,1)
              IBLANK_HDF5(m,i,j,k) = IBlank(i,j,k,m)
            end do
          end do
        end do
      end do
    else
      nullify(IBLANK_HDF5)
    end if

    if (present(Q)) then
      allocate(Q_HDF5(nGrids,maxval(ND_HDF5(:,1)),maxval(ND_HDF5(:,2)), &
        maxval(ND_HDF5(:,3)),nDims+2))
      do m = 1, nGrids
        do k = 1, ND_HDF5(m,3)
          do j = 1, ND_HDF5(m,2)
            do i = 1, ND_HDF5(m,1)
              Q_HDF5(m,i,j,k,:) = Q(:,i,j,k,m)
            end do
          end do
        end do
      end do
    else
      nullify(Q_HDF5)
    end if

    if (present(QAux)) then
      allocate(QAux_HDF5(nGrids,maxval(ND_HDF5(:,1)),maxval(ND_HDF5(:,2)), &
        maxval(ND_HDF5(:,3)),nAuxVars_HDF5))
      do m = 1, nGrids
        do k = 1, ND_HDF5(m,3)
          do j = 1, ND_HDF5(m,2)
            do i = 1, ND_HDF5(m,1)
              QAux_HDF5(m,i,j,k,:) = QAux(:,i,j,k,m)
            end do
          end do
        end do
      end do
    else
      nullify(QAux_HDF5)
    end if

    call Write_HDF5

    if (associated(QAux_HDF5)) deallocate(QAux_HDF5)
    if (associated(Q_HDF5)) deallocate(Q_HDF5)
    if (associated(IBLANK_HDF5)) deallocate(IBLANK_HDF5)
    deallocate(X_HDF5)
    deallocate(CONFIG_HDF5)
    deallocate(ND_HDF5)
    deallocate(vars_to_write_HDF5)

#else

    write (ERROR_UNIT, '(a)') "ERROR: HDF5 support is not enabled."
    stop 1

#endif

  end subroutine WriteHDF5_Multiple_Rank3

end module ModExamplesCommon
