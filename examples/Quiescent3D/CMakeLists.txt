#
# Build Quiescent3D example
#
add_executable(Quiescent3D Quiescent3D.f90)
set_target_properties(Quiescent3D PROPERTIES COMPILE_FLAGS "${F90-Flags}")
if(AutodetectMPI)
  set_target_properties(Quiescent3D PROPERTIES COMPILE_FLAGS "${MPI_FORTRAN_COMPILE_FLAGS}"
                                               LINK_FLAGS "${MPI_Fortran_LINK_FLAGS}")
endif()
if(BuildStatic)
  set_target_properties(Quiescent3D PROPERTIES LINK_FLAGS "-Wl,-Bdynamic")
endif()
target_link_libraries(Quiescent3D ${exampleExtraLibs})
install(TARGETS Quiescent3D RUNTIME DESTINATION bin/examples LIBRARY DESTINATION lib ARCHIVE DESTINATION lib)
install(FILES README plascomcm_hdf5.inp plascomcm_plot3d.inp DESTINATION examples/Quiescent3D)
