# Initialize variables
BUILT_SOURCES =
noinst_PROGRAMS =
EXTRA_DIST =
CLEAN_LOCAL_TARGETS =
example_extras =

# Include makefile for each example
include FlowPastCylinder/Makefile.mk
include Quiescent2D/Makefile.mk
include Quiescent3D/Makefile.mk
include SingleGridBoundaryLayer2D/Makefile.mk
include SingleGridBoundaryLayer3D/Makefile.mk
include TwoGridBoundaryLayer2D/Makefile.mk
include TwoGridBoundaryLayer3D/Makefile.mk

EXTRA_DIST += ModExamplesCommon.fpp
EXTRA_DIST += README
EXTRA_DIST += $(example_extras)

if BUILD_EXAMPLES

LDADD_UTILS = -L../utils -lutils
DEPS_UTILS = ../utils/libutils.a
LDADD_PLOT3D = -L../utils/PLOT3D -lplot3d
DEPS_PLOT3D = ../utils/PLOT3D/libplot3d.a
if HAVE_HDF5
LDADD_HDF5 = -L../utils/HDF5 -lhdf5io
DEPS_HDF5 = ../utils/HDF5/libhdf5io.a
endif
if BUILD_OVERSET
LDADD_OVERSET = -L../utils/overset -loverset
DEPS_OVERSET = ../utils/overset/liboverset.a
endif

LDADD = -L. -lexamples-common $(LDADD_UTILS) $(LDADD_PLOT3D) $(LDADD_HDF5) $(LDADD_OVERSET)

noinst_LIBRARIES = libexamples-common.a
nodist_libexamples_common_a_SOURCES = ModExamplesCommon.f90
libexamples_common_a_DEPENDENCIES = $(DEPS_UTILS) $(DEPS_PLOT3D) $(DEPS_HDF5)

AM_FCFLAGS = @FCFLAGS@
AM_FCFLAGS += $(FC_MODINC).
AM_FCFLAGS += $(FC_MODINC)$(abs_top_builddir)/utils/mod
AM_FCFLAGS += $(FC_MODINC)$(abs_top_builddir)/utils/PLOT3D
if HAVE_HDF5
AM_FCFLAGS += $(FC_MODINC)$(abs_top_builddir)/utils/HDF5
endif
if BUILD_OVERSET
AM_FCFLAGS += $(FC_MODINC)$(abs_top_builddir)/utils/overset/mod
endif

BUILT_SOURCES += $(abs_builddir)/.datastamp
$(abs_builddir)/.datastamp: $(abs_builddir)/Makefile
	if [ "$(builddir)" != "$(srcdir)" ]; then \
    for f in $(example_extras); do \
      test -d $${f%/*} || @MKDIR_P@ $${f%/*}; \
      test -L $$f || @LN_S@ "$(abs_srcdir)/$$f" $$f; \
    done; \
  fi
	touch .datastamp

CLEAN_LOCAL_TARGETS += examples-clean
examples-clean:
	rm -f ModExamplesCommon.f90
	rm -f .datastamp;
	if [ "$(builddir)" != "$(srcdir)" ]; then \
    for f in $(example_extras); do \
      rm -f $$f; \
    done; \
	fi

endif

FC_MODINC = @FC_MODINC@
FC_MODEXT = @FC_MODEXT@

clean-local: $(CLEAN_LOCAL_TARGETS)
	rm -f *.$(FC_MODEXT)
	find . -name *.dSYM -print0 | xargs -0 rm -rf

# Rule for processing .fpp files to .f90
# Note that Make has trouble combining implicit with explicit rules.
# Thus, a change to an .fpp file will not force a recompilation of all of the
# modules that make use of the module(s) created by that .fpp file unless
# we include that as an explicit dependency
.fpp.f90:
	$(FPP) $(FPPFLAGS) $< >$@
	-@rm -f $*.s
