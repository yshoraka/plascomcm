if BUILD_EXAMPLES
if BUILD_OVERSET

noinst_PROGRAMS += FlowPastCylinder/FlowPastCylinder

endif
endif

FlowPastCylinder_FlowPastCylinder_SOURCES = \
  FlowPastCylinder/FlowPastCylinder.f90
FlowPastCylinder_FlowPastCylinder_DEPENDENCIES = \
  libexamples-common.a $(abs_top_builddir)/utils/overset/liboverset.a

example_extras += \
  FlowPastCylinder/README \
  FlowPastCylinder/plascomcm_plot3d.inp \
  FlowPastCylinder/plascomcm_hdf5.inp
