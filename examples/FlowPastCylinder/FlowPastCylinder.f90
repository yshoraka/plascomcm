program FlowPastCylinder

  use ModExamplesCommon
  use ModBC
  use ModParseArguments
  use ModStdIO
  use ModOverset, overset_ik=>ik, overset_rk=>rk, overset_MAX_ND=>MAX_ND, &
    overset_OUTPUT_UNIT=>OUTPUT_UNIT, overset_INPUT_UNIT=>INPUT_UNIT, overset_ERROR_UNIT=>ERROR_UNIT
  use ModPegasus
  implicit none

  integer(ik) :: i, j, m
  character(len=CMD_ARG_LENGTH), dimension(:), allocatable :: RawArguments
  type(t_cmd_opt), dimension(4) :: Options
  integer(ik) :: OutputFormat
  character(len=PATH_LENGTH) :: OutputFile
  integer(ik) :: Nbg, Nfg
  integer(ik) :: InterpScheme
  real(rk) :: U, V
  integer(ik), dimension(2,2) :: nPoints
  real(rk) :: Length
  real(rk), dimension(2) :: Center
  real(rk) :: SphereRadius
  real(rk) :: RMin, RMax
  real(rk) :: RMinRel, RMaxRel
  real(rk) :: Shift
  real(rk) :: RShift
  real(rk) :: Radius, Theta
  real(rk) :: StretchAmount
  real(rk) :: StretchWeight
  real(rk), dimension(:,:,:,:), allocatable :: X
  integer(ik), dimension(2,2) :: Periodic
  integer(ik), dimension(2) :: PeriodicStorage
  real(rk), dimension(2,2) :: PeriodicLength
  integer(ik), dimension(:,:,:), allocatable :: IBlank
  integer(ik), dimension(2) :: GridType
  type(t_grid), dimension(2) :: Grids
  type(t_interp), dimension(2) :: InterpData
  type(t_pegasus) :: PegasusData
  real(rk), dimension(:,:,:,:), allocatable :: Q
  type(t_bc), dimension(9) :: BCs
  real(rk) :: Gamma
  real(rk) :: Temperature
  real(rk) :: Density
  real(rk), dimension(2) :: Velocity
  integer(ik), dimension(2,2) :: nSponge

  allocate(RawArguments(command_argument_count()))
  do i = 1, size(RawArguments)
    call get_command_argument(i, RawArguments(i))
  end do

  Options(1) = t_cmd_opt_("format", "f", OPT_VALUE_TYPE_STRING)
  Options(2) = t_cmd_opt_("size-bg", "N", OPT_VALUE_TYPE_INTEGER)
  Options(3) = t_cmd_opt_("size-fg", "n", OPT_VALUE_TYPE_INTEGER)
  Options(4) = t_cmd_opt_("interp", "i", OPT_VALUE_TYPE_STRING)

  call ParseArguments(RawArguments, Options=Options)

  if (Options(1)%is_present) then
    if (Options(1)%value == "plot3d") then
      OutputFormat = OUTPUT_FORMAT_PLOT3D
    else if (Options(1)%value == "hdf5") then
      OutputFormat = OUTPUT_FORMAT_HDF5
    else
      write (ERROR_UNIT, '(3a)') "ERROR: Unrecognized grid format '", trim(Options(1)%value), "'."
      stop 1
    end if
  else
    OutputFormat = DefaultOutputFormat
  end if

  if (Options(2)%is_present) then
    read (Options(2)%value, *) Nbg
  else
    Nbg = 61
  end if

  if (Options(3)%is_present) then
    read (Options(3)%value, *) Nfg
  else
    Nfg = 121
  end if

  if (Options(4)%is_present) then
    if (Options(4)%value == "linear") then
      InterpScheme = INTERP_LINEAR
    else if (Options(4)%value == "cubic") then
      InterpScheme = INTERP_CUBIC
    else
      write (ERROR_UNIT, '(3a)') "ERROR: Unrecognized interpolation scheme '", &
        trim(Options(4)%value), "'."
      stop 1
    end if
  else
    InterpScheme = INTERP_LINEAR
  end if

  nPoints(:,1) = [Nbg, Nbg]
  nPoints(:,2) = [Nfg, Nfg/3]

  Length = 8._rk

  Center = [-1.2_rk, 0._rk]
  RMin = 0.3_rk
  RMax = 1.8_rk
  StretchAmount = 0.7_rk

  ! Tunes radial distribution of points (Shift=0 => dR proportional to R; Shift>0 => closer to
  ! uniform dR; Shift<0 => more exaggerated clustering near RMin)
  Shift = 5._rk

  allocate(X(2,maxval(nPoints(1,:)),maxval(nPoints(2,:)),2))

  do j = 1, nPoints(2,1)
    do i = 1, nPoints(1,1)
      U = real(i-1, kind=rk)/real(nPoints(1,1)-1, kind=rk)
      V = real(j-1, kind=rk)/real(nPoints(2,1)-1, kind=rk)
      X(:,i,j,1) = Length * ([U,V] - 0.5_rk)
    end do
  end do

  RShift = (1._rk - 2._rk**(Shift)) * RMin
  RMinRel = RMin - RShift
  RMaxRel = RMax - RShift

  do j = 1, nPoints(2,2)
    do i = 1, nPoints(1,2)
      U = real(i-1, kind=rk)/real(nPoints(1,2)-1, kind=rk)
      V = real(j-1, kind=rk)/real(nPoints(2,2)-1, kind=rk)
      Radius = RMinRel * (RMaxRel/RMinRel)**V + RShift
      Theta = 2._rk * Pi * U
      StretchWeight = (Smoothstep(0.5_rk*Pi, 0._rk*Pi, Theta) + &
        Smoothstep(1.5_rk*Pi, 2._rk*Pi, Theta)) * (Radius - RMin)/(RMax-RMin)
      X(:,i,j,2) = Center + Radius * [cos(Theta) + StretchWeight*StretchAmount, -sin(Theta)]
    end do
  end do

  Periodic = FALSE
  PeriodicStorage = NO_OVERLAP_PERIODIC
  PeriodicLength = 0._rk

  Periodic(1,2) = TRUE
  PeriodicStorage(2) = OVERLAP_PERIODIC

  allocate(IBlank(maxval(nPoints(1,:)),maxval(nPoints(2,:)),2))

  IBlank = 1

  do j = 1, nPoints(2,1)
    do i = 1, nPoints(1,1)
      if (sum((X(:,i,j,1) - Center)**2) <= 1.05_rk * RMin**2) then
        IBlank(i,j,1) = 0
      end if
    end do
  end do

  GridType(1) = GRID_TYPE_CARTESIAN
  GridType(2) = GRID_TYPE_CURVILINEAR

  do m = 1, 2
    call MakeGrid(Grids(m), nDims=2, iStart=[1,1], iEnd=nPoints(:,m), &
      Coords=X(:,:nPoints(1,m),:nPoints(2,m),m), Periodic=Periodic(:,m), &
      PeriodicStorage=PeriodicStorage(m), PeriodicLength=PeriodicLength(:,m), &
      IBlank=IBlank(:nPoints(1,m),:nPoints(2,m),m), GridType=GridType(m), ID=m)
  end do

  call AssembleOverset(Grids, InterpData, FringeSize=2, InterpScheme=InterpScheme)
  call MakePegasusData(Grids, InterpData, PegasusData)

  do m = 1, 2
    IBlank(:nPoints(1,m),:nPoints(2,m),m) = reshape(Grids(m)%iblank, [nPoints(1,m),nPoints(2,m)])
  end do

  allocate(Q(4,maxval(nPoints(1,:)),maxval(nPoints(2,:)),2))

  Gamma = 1.4_rk
  Temperature = 1._rk/(Gamma - 1._rk)
  Density = 1._rk
  Velocity = [0.2_rk, 0._rk]

  Q(1,:,:,:) = Density
  Q(2,:,:,:) = Density * Velocity(1)
  Q(3,:,:,:) = Density * Velocity(2)
  Q(4,:,:,:) = Density * (Temperature/Gamma + 0.5_rk * sum(Velocity**2))

  nSponge = nPoints/10

  BCs(1) = t_bc_(1, BC_TYPE_SAT_FAR_FIELD, 1, [1,1], [1,-1])
  BCs(2) = t_bc_(1, BC_TYPE_SPONGE, 1, [1,nSponge(1,1)], [1,-1])
  BCs(3) = t_bc_(1, BC_TYPE_SAT_FAR_FIELD, -1, [-1,-1], [1,-1])
  BCs(4) = t_bc_(1, BC_TYPE_SPONGE, -1, [-nSponge(1,1),-1], [1,-1])
  BCs(5) = t_bc_(1, BC_TYPE_SAT_FAR_FIELD, 2, [1,-1], [1,1])
  BCs(6) = t_bc_(1, BC_TYPE_SPONGE, 2, [1,-1], [1,nSponge(2,1)])
  BCs(7) = t_bc_(1, BC_TYPE_SAT_FAR_FIELD, -2, [1,-1], [-1,-1])
  BCs(8) = t_bc_(1, BC_TYPE_SPONGE, -2, [1,-1], [-nSponge(2,1),-1])
  BCs(9) = t_bc_(2, BC_TYPE_SAT_NOSLIP_ISOTHERMAL_WALL, 2, [1,-1], [1,1])

  select case (OutputFormat)
  case (OUTPUT_FORMAT_PLOT3D)
    call WritePLOT3DGrid(Path="grid.xyz", nDims=2, nGrids=2, nPoints=nPoints, X=X, &
      IBlank=IBlank)
    call WritePLOT3DSolution("initial.q", nDims=2, nGrids=2, nPoints=nPoints, Q=Q)
  case (OUTPUT_FORMAT_HDF5)
    call WriteHDF5("initial.h5", nDims=2, nGrids=2, nPoints=nPoints, X=X, IBlank=IBlank, Q=Q, &
      WriteTarget=.true.)
  end select

  call WriteBCs(BCs, "bc.dat")
  call WritePegasusData(PegasusData, "XINTOUT.HO.2D", "XINTOUT.X.2D")

contains

  pure function Smoothstep(XMin, XMax, X) result (XSmooth)

    real(rk), intent(in):: XMin, XMax
    real(rk), intent(in) :: X
    real(rk) :: XSmooth

    real(rk) :: S

    if (XMax /= XMin) then
      S = min(max((X-XMin)/(XMax-XMin), 0._rk), 1._rk)
      XSmooth = S**3 * (6._rk * S**2 - 15._rk * S + 10._rk)
    else
      XSmooth = merge(1._rk, 0._rk, X > XMin)
    end if

  end function Smoothstep

end program FlowPastCylinder
