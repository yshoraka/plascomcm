program Quiescent2D

  use ModExamplesCommon
  use ModBC
  use ModParseArguments
  use ModStdIO
  implicit none

  integer(ik) :: i, j
  character(len=CMD_ARG_LENGTH), dimension(:), allocatable :: RawArguments
  type(t_cmd_opt), dimension(5) :: Options
  integer(ik) :: OutputFormat
  real(rk) :: U, V
  integer(ik), dimension(2) :: nPoints
  real(rk), dimension(2) :: Length
  real(rk), dimension(:,:,:), allocatable :: X
  real(rk), dimension(:,:,:), allocatable :: Q
  type(t_bc), dimension(8) :: BCs
  real(rk) :: Gamma
  real(rk) :: Temperature
  real(rk) :: Density
  integer(ik), dimension(2) :: nSponge

  allocate(RawArguments(command_argument_count()))
  do i = 1, size(RawArguments)
    call get_command_argument(i, RawArguments(i))
  end do

  Options(1) = t_cmd_opt_("format", "f", OPT_VALUE_TYPE_STRING)
  Options(2) = t_cmd_opt_("size-x", "x", OPT_VALUE_TYPE_INTEGER)
  Options(3) = t_cmd_opt_("size-y", "y", OPT_VALUE_TYPE_INTEGER)
  Options(4) = t_cmd_opt_("length-x", "a", OPT_VALUE_TYPE_REAL)
  Options(5) = t_cmd_opt_("length-y", "b", OPT_VALUE_TYPE_REAL)

  call ParseArguments(RawArguments, Options=Options)

  if (Options(1)%is_present) then
    if (Options(1)%value == "plot3d") then
      OutputFormat = OUTPUT_FORMAT_PLOT3D
    else if (Options(1)%value == "hdf5") then
      OutputFormat = OUTPUT_FORMAT_HDF5
    else
      write (ERROR_UNIT, '(3a)') "ERROR: Unrecognized grid format '", trim(Options(1)%value), "'."
      stop 1
    end if
  else
    OutputFormat = DefaultOutputFormat
  end if

  do i = 1, 2
    if (Options(1+i)%is_present) then
      read (Options(1+i)%value, *) nPoints(i)
    else
      nPoints(i) = 21
    end if
  end do

  do i = 1, 2
    if (Options(3+i)%is_present) then
      read (Options(3+i)%value, *) Length(i)
    else
      Length(i) = 1._rk
    end if
  end do

  allocate(X(2,nPoints(1),nPoints(2)))

  do j = 1, nPoints(2)
    do i = 1, nPoints(1)
      U = real(i-1, kind=rk)/real(nPoints(1)-1, kind=rk)
      V = real(j-1, kind=rk)/real(nPoints(2)-1, kind=rk)
      X(:,i,j) = Length * ([U,V] - 0.5_rk)
    end do
  end do

  allocate(Q(4,nPoints(1),nPoints(2)))

  Gamma = 1.4_rk
  Temperature = 1._rk/(Gamma - 1._rk)
  Density = 1._rk

  Q(1,:,:) = Density
  Q(2,:,:) = 0._rk
  Q(3,:,:) = 0._rk
  Q(4,:,:) = Density * Temperature/Gamma

  nSponge = nPoints/6

  BCs(1) = t_bc_(1, BC_TYPE_SAT_FAR_FIELD, 1, [1,1], [1,-1])
  BCs(2) = t_bc_(1, BC_TYPE_SPONGE, 1, [1,nSponge(1)], [1,-1])
  BCs(3) = t_bc_(1, BC_TYPE_SAT_FAR_FIELD, -1, [-1,-1], [1,-1])
  BCs(4) = t_bc_(1, BC_TYPE_SPONGE, -1, [-nSponge(1),-1], [1,-1])
  BCs(5) = t_bc_(1, BC_TYPE_SAT_FAR_FIELD, 2, [1,-1], [1,1])
  BCs(6) = t_bc_(1, BC_TYPE_SPONGE, 2, [1,-1], [1,nSponge(2)])
  BCs(7) = t_bc_(1, BC_TYPE_SAT_FAR_FIELD, -2, [1,-1], [-1,-1])
  BCs(8) = t_bc_(1, BC_TYPE_SPONGE, -2, [1,-1], [-nSponge(2),-1])

  select case (OutputFormat)
  case (OUTPUT_FORMAT_PLOT3D)
    call WritePLOT3DGrid("grid.xyz", nDims=2, nPoints=nPoints, X=X)
    call WritePLOT3DSolution("initial.q", nDims=2, nPoints=nPoints, Q=Q)
  case (OUTPUT_FORMAT_HDF5)
    call WriteHDF5("initial.h5", nDims=2, nPoints=nPoints, X=X, Q=Q, WriteTarget=.true.)
  end select

  call WriteBCs(BCs, "bc.dat")

end program Quiescent2D
