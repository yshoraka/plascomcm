if BUILD_EXAMPLES

noinst_PROGRAMS += Quiescent2D/Quiescent2D

endif

Quiescent2D_Quiescent2D_SOURCES = Quiescent2D/Quiescent2D.f90
Quiescent2D_Quiescent2D_DEPENDENCIES = libexamples-common.a

example_extras += \
  Quiescent2D/README \
  Quiescent2D/plascomcm_plot3d.inp \
  Quiescent2D/plascomcm_hdf5.inp
