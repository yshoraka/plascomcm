#
# Build PlasComCM examples
#
set(examples-FPP ModExamplesCommon.fpp)
# setup a place for the module files to reside
set(moduleDirectory ${CMAKE_CURRENT_BINARY_DIR}/modules)
file(MAKE_DIRECTORY ${moduleDirectory})
#include_directories(${moduleDirectory})

include(ExternalPreprocessor)
#SetupPreProcessor()
foreach(fileIn ${examples-FPP})
  PreProcess(${CMAKE_CURRENT_SOURCE_DIR}/${fileIn} 
             ${CMAKE_CURRENT_BINARY_DIR} fileOut)
  list(APPEND examples-F90 ${fileOut})
endforeach()

set_source_files_properties(${examples-F90} PROPERTIES COMPILE_FLAGS
  "${F90-Flags} ${MPI_FORTRAN_COMPILE_FLAGS}")
add_library(examples-common STATIC ${examples-F90})
set_target_properties(examples-common PROPERTIES Fortran_MODULE_DIRECTORY ${moduleDirectory})
#set(utilsModuleDir "${CMAKE_BINARY_DIR}/utils/modules")
#set(plot3dModuleDir "${CMAKE_BINARY_DIR}/utils/PLOT3D/modules")
set(utilsModuleDir "${CMAKE_BINARY_DIR}/utils/modules")
set(plot3dModuleDir "${CMAKE_BINARY_DIR}/utils/PLOT3D/modules")
target_include_directories(examples-common PUBLIC ${moduleDirectory} ${utilsModuleDir} ${plot3dModuleDir})
if(BuildStatic)
  set_target_properties(examples-common PROPERTIES LINK_FLAGS "-Wl,-Bdynamic")
endif()

install(TARGETS examples-common RUNTIME DESTINATION bin LIBRARY DESTINATION lib ARCHIVE DESTINATION lib)

set(exampleExtraLibs examples-common utils plot3d)
add_dependencies(examples-common utils plot3d)
if(useHDF)
  add_dependencies(examples-common hdf5io)
  set(exampleExtraLibs ${exampleExtraLibs} hdf5io)
  set(HDF5ModuleDir "${CMAKE_BINARY_DIR}/utils/HDF5/modules")
  target_include_directories(examples-common PUBLIC ${HDF5ModuleDir})
endif()
if(OversetTool)
  set(exampleExtraLibs ${exampleExtraLibs} overset)
endif()

add_subdirectory(FlowPastCylinder)
add_subdirectory(Quiescent2D)
add_subdirectory(Quiescent3D)
add_subdirectory(SingleGridBoundaryLayer2D)
add_subdirectory(SingleGridBoundaryLayer3D)
add_subdirectory(TwoGridBoundaryLayer2D)
add_subdirectory(TwoGridBoundaryLayer3D)
