! Copyright (c) 2015, University of Illinois at Urbana-Champaign, XPACC
! License: MIT, http://opensource.org/licenses/MIT
!-----------------------------------------------------------------------
! ModMetrics.f90
!  
! - basic code to compute grid metrics
!
! Revision history
! - 18 Dec 2006 : DJB : initial code
!
!-----------------------------------------------------------------------

MODULE ModMetrics

CONTAINS

  subroutine metrics(region, ng)

    USE ModGlobal
    USE ModDataStruct
    USE ModDeriv
    USE ModMPI

    ! ... Input variables
    type(t_region), pointer :: region
    integer :: ng

    ! ... local variables
    type(t_mixt_input), pointer :: input
    type(t_grid), pointer :: grid
    integer, pointer :: iblank(:)
    integer :: i, Nc, ND
    real(rfreal) :: timer
    integer :: ierr

    ! ... start timer
    timer = MPI_WTime()

    ! ... simplicity
    grid  => region%grid(ng)
    input => grid%input
    Nc = grid%nCells
    ND = grid%ND

    ! ... allocate metrics
    if (.not.associated(grid%JAC) .eqv. .true.) then
      allocate(grid%JAC(Nc))
#ifdef AXISYMMETRIC
      allocate(grid%JAC0(Nc))
#endif
    end if
    if (.not.associated(grid%INVJAC) .eqv. .true.) then
      allocate(grid%INVJAC(Nc))
    end if
    if (.not.associated(grid%XI_TAU) .eqv. .true.) then
      allocate(grid%XI_TAU(Nc,ND))
    end if
    if (.not.associated(grid%XYZ_TAU) .eqv. .true.) then
      allocate(grid%XYZ_TAU(Nc,ND))
      grid%XYZ_TAU(:,:) = 0.0_rfreal
    end if
    if (.not.associated(grid%XYZ_TAU2) .eqv. .true.) then
      allocate(grid%XYZ_TAU2(Nc,ND))
      grid%XYZ_TAU2(:,:) = 0.0_rfreal
    end if
    if (.not.associated(grid%XI_TAU_XI) .eqv. .true.) then
      allocate(grid%XI_TAU_XI(Nc,ND))
    end if
    if (.not.associated(grid%INVJAC_TAU) .eqv. .true.) then
      allocate(grid%INVJAC_TAU(Nc))
    end if
    if (.not.associated(grid%MT1) .eqv. .true.) then
#ifdef AXISYMMETRIC
      allocate(grid%MT0(Nc,ND**2))
#endif
      allocate(grid%MT1(Nc,ND**2))
    end if
    if (.not.associated(grid%MT2) .eqv. .true.) then
      allocate(grid%MT2(Nc,ND**2))
    end if
    if (region%input%useMetricIdentities == TRUE) then
      if (.not.associated(grid%ident) .eqv. .true.) then
        allocate(grid%ident(Nc,ND))
      end if
    end if
    if (region%input%fluidModel == Q1D) then
      if (.not.associated(grid%area) .eqv. .true.) then
        allocate(grid%area(Nc))
      end if
    end if

    ! ... until I come up with something better, let's work on a
    ! ... case-by-case basis
    if (grid%ND == 1) then
      call metrics_1D(region, ng)
    else if (grid%ND == 2) then
      call metrics_2D(region, ng)
    else if (grid%ND == 3) then
      call metrics_3D(region, ng)
    else
      if (region%myrank == 0) write (*,'(A)') 'Invalid state%ND entry in metrics()'
      call MPI_Finalize(ierr)
      stop
    end if

    ! ... save only the boundary portion of MT2
    Call Save_MT2_on_Boundary(region, ng)

    ! ... stop timer
    region%mpi_timings(ng)%metrics = region%mpi_timings(ng)%metrics + (MPI_WTime() - timer)

  end subroutine metrics

  subroutine metrics_1D(region, ng)

    USE ModGlobal
    USE ModDataStruct
    USE ModDeriv
    USE ModMPI

    type(t_region), pointer :: region
    type(t_grid), pointer :: grid
    integer :: ng

    ! ... local variables
    integer :: Nc, N(MAX_ND), i, j, k
    real(rfreal), pointer :: F(:), dF(:)

    ! ... simplicity
    grid => region%grid(ng)
    Nc = grid%nCells
    N  = 1
    do j = 1, grid%ND
      N(j) = grid%ie(j)-grid%is(j)+1
    end do;

    ! ... local memory allocation
    allocate(F(Nc), dF(Nc))

    ! ...   xihat_x = x_xi / JAC
    ! ...   xihat_t = -[x_tau xihat_x]

    ! ... xi_x = 1/x_xi
    do i = 1, Nc
      F(i) = grid%XYZ(i,1)
    end do
    Call APPLY_OPERATOR_BOX(region, ng, 1, 1, F, dF, .TRUE., 1)
    do i = 1, Nc
      grid%MT1(i,region%global%t2Map(1,1)) = 1.0_rfreal / dF(i)
      ! grid%MT1(i,region%global%t2Map(1,1)) = dble(grid%GlobalSize(1)-1.0_8)
    end do

    do i = 1, Nc

      ! ... compute jacobian J = dxi/dx
      grid%JAC(i) = 1.0_rfreal / dF(i)

      ! ... compute inverse jacobian
      grid%INVJAC(i) = 1.0_rfreal / grid%JAC(i)

      ! ... scale all metrics by 1/J
      do j = 1, grid%ND * grid%ND
        grid%MT1(i,j) = grid%MT1(i,j) * grid%INVJAC(i)
      end do

    end do

    ! ... compute derivatives of the metrics needed for BCs
    ! Gaitonde (JCP, 2002)

    do i = 1, Nc
      grid%MT2(i,region%global%t2Map(1,1)) = 0.0_rfreal ! I1 identity in Visbal &
      grid%XI_TAU(i,:)       = 0.0_rfreal
      grid%XI_TAU_XI(i,:)    = 0.0_rfreal
      grid%XYZ_TAU(i,:)      = 0.0_rfreal
      grid%XYZ_TAU2(i,:)     = 0.0_rfreal
      grid%INVJAC_TAU(i)     = 0.0_rfreal
    end do

    ! ... compute 2nd order metrics for d2f/dx_i2
    allocate(grid%cartMetric(Nc,grid%ND))
    do i = 1, grid%ND
      do j = 1, Nc
        F(j) = grid%JAC(j) * grid%MT1(j,1)
      end do
      Call APPLY_OPERATOR_BOX(region, ng, 1, i, F, dF, .TRUE., 1)
      do j = 1, Nc
        grid%cartMetric(j,i) = dF(j) * grid%MT1(j,1) * grid%JAC(j)
        ! grid%cartMetric(j,i) = 0.0_rfreal
      end do
    end do

    ! ... clean up!
    deallocate(F,dF)

  end subroutine metrics_1D

  subroutine metrics_2D(region, ng)

    USE ModGlobal
    USE ModDataStruct
    USE ModDeriv
    USE ModMPI

    type(t_region), pointer :: region
    type(t_grid), pointer :: grid
    integer :: ng

    ! ... local variables
    integer :: Nc, N(MAX_ND), i, j, k, ii
    real(rfreal), pointer :: F(:), dF(:)
    real(rfreal) :: ibfac_local, timer, time_old, time_new

    ! ... simplicity
    grid => region%grid(ng)
    Nc = grid%nCells
    N  = 1
    do j = 1, grid%ND
      N(j) = grid%ie(j)-grid%is(j)+1
    end do;

    ! ... local memory allocation
    allocate(F(Nc), dF(Nc))

    ! ...   xihat_x = y_eta / JAC
    ! ...   xihat_y = x_eta / JAC
    ! ...   xihat_t = -[x_tau xihat_x + y_tau xihat_y]

    ! ... xi_x = y_eta * J => xihat_x = xi_x/J = y_eta
    Do ii = 1, Nc
      F(ii) = grid%XYZ(ii,2)
    End Do
    Call APPLY_OPERATOR_BOX(region, ng, 1, 2, F, dF, .TRUE., 1)
    Do ii = 1, Nc
      grid%MT1(ii,region%global%t2Map(1,1)) = dF(ii)
      grid%INVJAC(ii) = dF(ii)
    End Do

    ! ... eta_y = x_xi * J => etahat_y = eta_y/J = x_xi
    Do ii = 1, Nc
      F(ii) = grid%XYZ(ii,1)
    End Do
    Call APPLY_OPERATOR_BOX(region, ng, 1, 1, F, dF, .TRUE., 1)
    Do ii = 1, Nc
      grid%MT1(ii,region%global%t2Map(2,2)) = dF(ii)
      grid%INVJAC(ii) = grid%INVJAC(ii) * dF(ii)
    End Do

    ! ... cross-terms
    if (grid%metric_type /= CARTESIAN) then

      ! ... eta_x = -y_xi * J => etahat_x = eta_x/J = -y_xi
      Do ii = 1, Nc
        F(ii) = grid%XYZ(ii,2)
      End Do
      Call APPLY_OPERATOR_BOX(region, ng, 1, 1, F, dF, .FALSE., 1)
      Do ii = 1, Nc
        grid%MT1(ii,region%global%t2Map(2,1)) = -dF(ii)
        grid%JAC(ii) = dF(ii) ! temporary storage for y_xi
      End Do

      ! ... xi_y = -x_eta * J => xihat_y = xi_y/J = -x_eta
      Do ii = 1, Nc
        F(ii) = grid%XYZ(ii,1)
      End Do
      Call APPLY_OPERATOR_BOX(region, ng, 1, 2, F, dF, .FALSE., 1)
      Do ii = 1, Nc
        grid%MT1(ii,region%global%t2Map(1,2)) = -dF(ii)
        grid%JAC(ii) = grid%JAC(ii) * dF(ii) ! temporary storage for y_xi * x_eta
      End Do

      ! ... finish the jacobian
      Do ii = 1, Nc
        grid%INVJAC(ii) = grid%INVJAC(ii) - grid%JAC(ii)
      End Do

    else

      Do ii = 1, Nc
        grid%MT1(ii,region%global%t2Map(1,2)) = 0.0_rfreal
        grid%MT1(ii,region%global%t2Map(2,1)) = 0.0_rfreal
      End Do

    end if

    do i = 1, grid%nCells
      ibfac_local = grid%ibfac(i)
      grid%INVJAC(i) = grid%INVJAC(i) * ibfac_local + (1.0_rfreal - ibfac_local)
      grid%JAC(i) = 1.0_rfreal / grid%INVJAC(i)
    end do

    ! ...   xihat_t = -[x_tau xihat_x + y_tau xihat_y]
    ! ...  etahat_t = -[x_tau etahat_x + y_tau etahat_y]
    ! ... (1/J)_tau = -[(xihat_t)_xi + (etahat_t)_eta]  
    if (grid%moving == FALSE) then
      Do ii = 1, Nc
        grid%XI_TAU(ii,:)     = 0.0_rfreal
        grid%XI_TAU_XI(ii,:)  = 0.0_rfreal
        grid%XYZ_TAU(ii,:)    = 0.0_rfreal
        grid%XYZ_TAU2(ii,:)   = 0.0_rfreal
        grid%INVJAC_TAU(ii)   = 0.0_rfreal
      End Do
    else
      Do ii = 1, Nc
        grid%XI_TAU(ii,1) = - (grid%XYZ_TAU(ii,1) * grid%MT1(ii,region%global%t2Map(1,1)) + grid%XYZ_TAU(ii,2) * grid%MT1(ii,region%global%t2Map(1,2)))
        grid%XI_TAU(ii,2) = - (grid%XYZ_TAU(ii,1) * grid%MT1(ii,region%global%t2Map(2,1)) + grid%XYZ_TAU(ii,2) * grid%MT1(ii,region%global%t2Map(2,2)))
      End Do
      Do ii = 1, Nc
        F(ii) = grid%XI_TAU(ii,1)
      End Do
      call APPLY_OPERATOR_BOX(region, ng, 1, 1, F, dF, .FALSE., 1)
      Do ii = 1, Nc
        grid%XI_TAU_XI(ii,1) = dF(ii)
        F(ii) = grid%XI_TAU(ii,2)
      End Do
      call APPLY_OPERATOR_BOX(region, ng, 1, 2, F, dF, .FALSE., 1)
      Do ii = 1, Nc
        grid%XI_TAU_XI(ii,2) = dF(ii)
        grid%INVJAC_TAU(ii) = - grid%XI_TAU_XI(ii,1) - grid%XI_TAU_XI(ii,2)
      End Do
    end if

    loop1: Do ii = 1, Nc
      if (grid%iblank(ii) /= 0) then
        If (grid%JAC(ii) <= 0.0_rfreal) Then
          Write (*,'(2(A,I3),A)') 'PlasComCM: WARNING: grid ', grid%iGridGlobal, ' on processor ', region%myrank, ' has a negative jacobian.'
          EXIT loop1
        End If
      end if
    End Do loop1

#ifdef AXISYMMETRIC
    ! ... communicate minimum value
    grid%drMin = minval(grid%INVJAC)
    Call MPI_Allreduce(MPI_IN_PLACE, grid%drMin, 1, MPI_DOUBLE_PRECISION, MPI_MIN, grid%comm, ierr)
    grid%drMin = sqrt(grid%drMin)*1d-6
    do i = 1, Nc
      ibfac = 1.0_rfreal
      if (grid%iblank(i) == 0) ibfac = 0.0_rfreal
      grid%JAC0(i) = grid%JAC(i) 
      grid%MT0(i,:) = grid%MT1(i,:)
      grid%INVJAC(i) = grid%INVJAC(i) * max(abs(grid%XYZ(i,1)),grid%drMin)
      grid%MT1(i,:) = grid%MT1(i,:) * max(abs(grid%XYZ(i,1)),grid%drMin)
      grid%INVJAC(i) = grid%INVJAC(i) * ibfac + (1.0_rfreal - ibfac)
      grid%JAC(i) = 1.0_rfreal / grid%INVJAC(i)
    end do
#endif
    
    ! ... compute derivatives of the metrics needed for BCs
    ! ... compute the xi-derivatives only
    i = 1
    do j = 1, grid%ND
      Do ii = 1, Nc
        F(ii) = grid%MT1(ii,region%global%t2Map(i,j))
      End Do
      call APPLY_OPERATOR_BOX(region, ng, 1, i, F, dF, .FALSE., 1)
      Do ii = 1, Nc
        grid%MT2(ii,region%global%t2Map(i,j)) = dF(ii)
      End Do
    end do

    ! ... get eta derivatives via I1 and I2 identities
    i = 2
    do j = 1, grid%ND
      Do ii = 1, Nc
        grid%MT2(ii,region%global%t2Map(i,j)) = -grid%MT2(ii,region%global%t2Map(1,j))
      End Do
    end do

    if (grid%metric_type == CARTESIAN) then

      ! ... compute 2nd order metrics for d2f/dx_i2
      allocate(grid%cartMetric(Nc,grid%ND))
      do i = 1, grid%ND
        Do ii = 1, Nc
          F(ii) = grid%JAC(ii) * grid%MT1(ii,region%global%t2Map(i,i))
        End Do
        Call APPLY_OPERATOR_BOX(region, ng, 1, i, F, dF, .TRUE., 1)
        Do ii = 1, Nc
          grid%cartMetric(ii,i) = dF(ii) * grid%JAC(ii) * grid%MT1(ii,region%global%t2Map(i,i))
        End Do
      end do

    end if

    ! ... compute metric identies
    If (region%input%useMetricIdentities == TRUE) Then

      Do i = 1, 2
        Do ii = 1, Nc
          grid%ident(ii,i) = 0.0_rfreal
        End Do
        do j = 1, 2
          Do ii = 1, Nc
            F(ii) = grid%MT1(ii,region%global%t2Map(j,i))
          End Do
          Call APPLY_OPERATOR_BOX(region, ng, 1, j, F, dF, .FALSE., 1)
          Do ii = 1, Nc
            grid%ident(ii,i) = grid%ident(ii,i) + dF(ii)
          End Do
        end do
      end do

    End If

    ! ... clean up!
    deallocate(F,dF)
    return

  end subroutine metrics_2D

  subroutine metrics_3D(region, ng)

    USE ModGlobal
    USE ModDataStruct
    USE ModDeriv
    USE ModMPI

    Implicit None

    type(t_region), pointer :: region
    type(t_grid), pointer :: grid
    type(t_mixt_input), pointer :: input
    integer :: ng

    ! ... local variables
    integer :: Nc, N(MAX_ND), i, j, k, l0, alloc_stat, ii, jj
    real(rfreal), pointer :: F(:), dF(:), storage(:,:)
    real(rfreal) :: ibfac_local, timer, time_old, time_new

    ! ... simplicity
    grid => region%grid(ng)
    Nc   = grid%nCells
    N(:) = 1
    do j = 1, grid%ND
      N(j) = grid%ie(j)-grid%is(j)+1
    end do;
    input => grid%input

    ! ... local memory allocation
    allocate(F(Nc), dF(Nc), storage(Nc,9), stat=alloc_stat)
    if (alloc_stat /= 0) then
      write (*,'(A,I4,A)') 'PlasComCM: Processor ', region%myrank, ' unable to allocate storage in metrics_3D.'
      call graceful_exit(region%myrank, 'PlasComCM: unable to allocate storage in metrics_3D.')
    end if


    ! ... timing of derivatives
!   time_old = 0.0_8
!   time_new = 0.0_8
!   Do ii = 1, grid%nCells
!     F(ii) = grid%XYZ(ii,3)
!   End Do
!
!   timer = MPI_WTIME()
!   Do j = 1, 10
!     Call APPLY_OPERATOR_OLD(region, ng, 1, 2, F, dF, .TRUE.)
!   End Do
!   time_old = (MPI_WTIME() - timer)
!
!   timer = MPI_WTIME()
!   Do j = 1, 10
!     Call APPLY_OPERATOR(region, ng, 1, 2, F, dF, .TRUE.)
!   End Do
!   time_new = MPI_WTIME() - timer
!   print *, 'old: ', time_old, 'new: ', time_new
!   Call Graceful_Exit(region%myrank, 'Testing derivative routines in metrics_3d...')

    ! ... compute the (x_i)_{xi_j} terms
    do i = 1, 3
      do j = 1, 3
        do ii = 1, grid%nCells
          F(ii) = grid%XYZ(ii,i)
        end do
        if (i == j) Call APPLY_OPERATOR_BOX(region, ng, 1, j, F, dF, .TRUE., 1)
        if (i /= j) Call APPLY_OPERATOR_BOX(region, ng, 1, j, F, dF, .FALSE., 1)
        do ii = 1, grid%nCells
          storage(ii,region%global%t2Map(i,j)) = dF(ii)
        end do
!        if (i == j) Call APPLY_OPERATOR_OLD(region, ng, 1, j, F, dF, .TRUE.)
!        if (i /= j) Call APPLY_OPERATOR_OLD(region, ng, 1, j, F, dF, .FALSE.)
!        Do ii = 1, grid%nCells
!          if ((ABS(storage(ii,region%global%t2Map(i,j))-dF(ii)) >= 1e-15)) then
!            write (*,'(3(I2,1X),2(E13.6,1X))') ng, i, j, storage(ii,region%global%t2Map(i,j)), dF(ii)
!          end if
!        End Do
      end do
    end do
    ! Call Graceful_Exit(region%myrank, 'Testing derivative routines in metrics_3d...')

    if (ANY(grid%periodic(:) == PLANE_PERIODIC)) then

      do ii = 1, grid%nCells
        grid%MT1(ii,region%global%t2Map(1,1)) =   storage(ii,region%global%t2Map(2,2)) * storage(ii,region%global%t2Map(3,3)) - storage(ii,region%global%t2Map(2,3)) * storage(ii,region%global%t2Map(3,2))
        grid%MT1(ii,region%global%t2Map(1,2)) = -(storage(ii,region%global%t2Map(1,2)) * storage(ii,region%global%t2Map(3,3)) - storage(ii,region%global%t2Map(1,3)) * storage(ii,region%global%t2Map(3,2)))
        grid%MT1(ii,region%global%t2Map(1,3)) =   storage(ii,region%global%t2Map(1,2)) * storage(ii,region%global%t2Map(2,3)) - storage(ii,region%global%t2Map(1,3)) * storage(ii,region%global%t2Map(2,2))

        grid%MT1(ii,region%global%t2Map(2,1)) = -(storage(ii,region%global%t2Map(2,1)) * storage(ii,region%global%t2Map(3,3)) - storage(ii,region%global%t2Map(2,3)) * storage(ii,region%global%t2Map(3,1)))
        grid%MT1(ii,region%global%t2Map(2,2)) =   storage(ii,region%global%t2Map(1,1)) * storage(ii,region%global%t2Map(3,3)) - storage(ii,region%global%t2Map(1,3)) * storage(ii,region%global%t2Map(3,1))
        grid%MT1(ii,region%global%t2Map(2,3)) = -(storage(ii,region%global%t2Map(1,1)) * storage(ii,region%global%t2Map(2,3)) - storage(ii,region%global%t2Map(1,3)) * storage(ii,region%global%t2Map(2,1)))

        grid%MT1(ii,region%global%t2Map(3,1)) =   storage(ii,region%global%t2Map(2,1)) * storage(ii,region%global%t2Map(3,2)) - storage(ii,region%global%t2Map(2,2)) * storage(ii,region%global%t2Map(3,1))
        grid%MT1(ii,region%global%t2Map(3,2)) = -(storage(ii,region%global%t2Map(1,1)) * storage(ii,region%global%t2Map(3,2)) - storage(ii,region%global%t2Map(1,2)) * storage(ii,region%global%t2Map(3,1)))
        grid%MT1(ii,region%global%t2Map(3,3)) =   storage(ii,region%global%t2Map(1,1)) * storage(ii,region%global%t2Map(2,2)) - storage(ii,region%global%t2Map(1,2)) * storage(ii,region%global%t2Map(2,1))
      end do

    else

      ! ... spatial metrics using Thomas & Lombard
      ! ...   xihat_x = (y_eta z)_zeta - (y_zeta z)_eta
      ! ...   xihat_y = (z_eta x)_zeta - (z_zeta x)_eta
      ! ...   xihat_z = (x_eta y)_zeta - (x_zeta y)_eta
      ! ... (y_eta z)_zeta
      do ii = 1, grid%nCells
        F(ii) = storage(ii,region%global%t2Map(2,2)) * grid%XYZ(ii,3)
      end do
      Call APPLY_OPERATOR_BOX(region, ng, 1, 3, F, dF, .FALSE., 1)
      do ii = 1, grid%nCells
        grid%MT1(ii,region%global%t2Map(1,1)) = dF(ii)
      end do
      ! ... (y_zeta z)_eta
      do ii = 1, grid%nCells
        F(ii) = storage(ii,region%global%t2Map(2,3)) * grid%XYZ(ii,3)
      end do
      Call APPLY_OPERATOR_BOX(region, ng, 1, 2, F, dF, .FALSE., 1)
      do ii = 1, grid%nCells
        grid%MT1(ii,region%global%t2Map(1,1)) = grid%MT1(ii,region%global%t2Map(1,1)) - dF(ii)
      end do

      ! ... (z_eta x)_zeta
      do ii = 1, grid%nCells
        F(ii) = storage(ii,region%global%t2Map(3,2)) * grid%XYZ(ii,1)
      end do
      Call APPLY_OPERATOR_BOX(region, ng, 1, 3, F, dF, .FALSE., 1)
      do ii = 1, grid%nCells
        grid%MT1(ii,region%global%t2Map(1,2)) = dF(ii)
      end do
      ! ... (z_zeta x)_eta
      do ii = 1, grid%nCells
        F(ii) = storage(ii,region%global%t2Map(3,3)) * grid%XYZ(ii,1)
      end do
      Call APPLY_OPERATOR_BOX(region, ng, 1, 2, F, dF, .FALSE., 1)
      do ii = 1, grid%nCells
        grid%MT1(ii,region%global%t2Map(1,2)) = grid%MT1(ii,region%global%t2Map(1,2)) - dF(ii)
      end do

      ! ... (x_eta y)_zeta
      do ii = 1, grid%nCells
        F(ii) = storage(ii,region%global%t2Map(1,2)) * grid%XYZ(ii,2)
      end do
      Call APPLY_OPERATOR_BOX(region, ng, 1, 3, F, dF, .FALSE., 1)
      do ii = 1, grid%nCells
        grid%MT1(ii,region%global%t2Map(1,3)) = dF(ii)
      end do
      ! ... (x_zeta y)_eta
      do ii = 1, grid%nCells
        F(ii) = storage(ii,region%global%t2Map(1,3)) * grid%XYZ(ii,2)
      end do
      Call APPLY_OPERATOR_BOX(region, ng, 1, 2, F, dF, .FALSE., 1)
      do ii = 1, grid%nCells
        grid%MT1(ii,region%global%t2Map(1,3)) = grid%MT1(ii,region%global%t2Map(1,3)) - dF(ii)
      end do

      ! ...  etahat_x = (y_zeta z)_xi - (y_xi z)_zeta
      ! ...  etahat_y = (z_zeta x)_xi - (z_xi x)_zeta
      ! ...  etahat_z = (x_zeta y)_xi - (x_xi y)_zeta
      ! ... (y_zeta z)_xi
      do ii = 1, grid%nCells
        F(ii) = storage(ii,region%global%t2Map(2,3)) * grid%XYZ(ii,3)
      end do
      Call APPLY_OPERATOR_BOX(region, ng, 1, 1, F, dF, .FALSE., 1)
      do ii = 1, grid%nCells
        grid%MT1(ii,region%global%t2Map(2,1)) = dF(ii)
      end do
      ! ... (y_xi z)_zeta
      do ii = 1, grid%nCells
        F(ii) = storage(ii,region%global%t2Map(2,1)) * grid%XYZ(ii,3)
      end do
      Call APPLY_OPERATOR_BOX(region, ng, 1, 3, F, dF, .FALSE., 1)
      do ii = 1, grid%nCells
        grid%MT1(ii,region%global%t2Map(2,1)) = grid%MT1(ii,region%global%t2Map(2,1)) - dF(ii)
      end do

      ! ... (z_zeta x)_xi
      do ii = 1, grid%nCells
        F(ii) = storage(ii,region%global%t2Map(3,3)) * grid%XYZ(ii,1)
      end do
      Call APPLY_OPERATOR_BOX(region, ng, 1, 1, F, dF, .FALSE., 1)
      do ii = 1, grid%nCells
        grid%MT1(ii,region%global%t2Map(2,2)) = dF(ii)
      end do
      ! ... (z_xi x)_zeta
      do ii = 1, grid%nCells
        F(ii) = storage(ii,region%global%t2Map(3,1)) * grid%XYZ(ii,1)
      end do
      Call APPLY_OPERATOR_BOX(region, ng, 1, 3, F, dF, .FALSE., 1)
      do ii = 1, grid%nCells
        grid%MT1(ii,region%global%t2Map(2,2)) = grid%MT1(ii,region%global%t2Map(2,2)) - dF(ii)
      end do

      ! ... (x_zeta y)_xi
      do ii = 1, grid%nCells
        F(ii) = storage(ii,region%global%t2Map(1,3)) * grid%XYZ(ii,2)
      end do
      Call APPLY_OPERATOR_BOX(region, ng, 1, 1, F, dF, .FALSE., 1)
      do ii = 1, grid%nCells
        grid%MT1(ii,region%global%t2Map(2,3)) = dF(ii)
      end do
      ! ... (x_xi y)_zeta
      do ii = 1, grid%nCells
        F(ii) = storage(ii,region%global%t2Map(1,1)) * grid%XYZ(ii,2)
      end do
      Call APPLY_OPERATOR_BOX(region, ng, 1, 3, F, dF, .FALSE., 1)
      do ii = 1, grid%nCells
        grid%MT1(ii,region%global%t2Map(2,3)) = grid%MT1(ii,region%global%t2Map(2,3)) - dF(ii)
      end do

      ! ... zetahat_x = (y_xi z)_eta - (y_eta z)_xi
      ! ... zetahat_y = (z_xi x)_eta - (z_eta x)_xi 
      ! ... zetahat_z = (x_xi y)_eta - (x_eta y)_xi 
      ! ... (y_xi z)_eta
      do ii = 1, grid%nCells
        F(ii) = storage(ii,region%global%t2Map(2,1)) * grid%XYZ(ii,3)
      end do
      Call APPLY_OPERATOR_BOX(region, ng, 1, 2, F, dF, .FALSE., 1)
      do ii = 1, grid%nCells
        grid%MT1(ii,region%global%t2Map(3,1)) = dF(ii)
      end do
      ! ... (y_eta z)_xi
      do ii = 1, grid%nCells
        F(ii) = storage(ii,region%global%t2Map(2,2)) * grid%XYZ(ii,3)
      end do
      Call APPLY_OPERATOR_BOX(region, ng, 1, 1, F, dF, .FALSE., 1)
      do ii = 1, grid%nCells
        grid%MT1(ii,region%global%t2Map(3,1)) = grid%MT1(ii,region%global%t2Map(3,1)) - dF(ii)
      end do

      ! ... (z_xi x)_eta
      do ii = 1, grid%nCells
        F(ii) = storage(ii,region%global%t2Map(3,1)) * grid%XYZ(ii,1)
      end do
      Call APPLY_OPERATOR_BOX(region, ng, 1, 2, F, dF, .FALSE., 1)
      do ii = 1, grid%nCells
        grid%MT1(ii,region%global%t2Map(3,2)) = dF(ii)
      end do
      ! ... (z_eta x)_xi
      do ii = 1, grid%nCells
        F(ii) = storage(ii,region%global%t2Map(3,2)) * grid%XYZ(ii,1)
      end do
      Call APPLY_OPERATOR_BOX(region, ng, 1, 1, F, dF, .FALSE., 1)
      do ii = 1, grid%nCells
        grid%MT1(ii,region%global%t2Map(3,2)) = grid%MT1(ii,region%global%t2Map(3,2)) - dF(ii)
      end do

      ! ... (x_xi y)_eta
      do ii = 1, grid%nCells
        F(ii) = storage(ii,region%global%t2Map(1,1)) * grid%XYZ(ii,2)
      end do
      Call APPLY_OPERATOR_BOX(region, ng, 1, 2, F, dF, .FALSE., 1)
      do ii = 1, grid%nCells
        grid%MT1(ii,region%global%t2Map(3,3)) = dF(ii)
      end do
      ! ... (x_eta y)_xi
      do ii = 1, grid%nCells
        F(ii) = storage(ii,region%global%t2Map(1,2)) * grid%XYZ(ii,2)
      end do
      Call APPLY_OPERATOR_BOX(region, ng, 1, 1, F, dF, .FALSE., 1)
      do ii = 1, grid%nCells
        grid%MT1(ii,region%global%t2Map(3,3)) = grid%MT1(ii,region%global%t2Map(3,3)) - dF(ii)
      end do

    end if

    ! ... (1/J) = det(x_xi) = x_xi [ y_eta z_zeta - y_zeta z_eta] -
    ! x_eta [ y_xi z_zeta - y_zeta z_xi ] + x_zeta [ y_xi z_eta -
    ! y_eta z_xi ]
    do ii = 1, grid%nCells
      grid%INVJAC(ii) = storage(ii,region%global%t2Map(1,1)) * (storage(ii,region%global%t2Map(2,2)) * storage(ii,region%global%t2Map(3,3)) - storage(ii,region%global%t2Map(2,3)) * storage(ii,region%global%t2Map(3,2))) &
                      - storage(ii,region%global%t2Map(1,2)) * (storage(ii,region%global%t2Map(2,1)) * storage(ii,region%global%t2Map(3,3)) - storage(ii,region%global%t2Map(2,3)) * storage(ii,region%global%t2Map(3,1))) &
                      + storage(ii,region%global%t2Map(1,3)) * (storage(ii,region%global%t2Map(2,1)) * storage(ii,region%global%t2Map(3,2)) - storage(ii,region%global%t2Map(2,2)) * storage(ii,region%global%t2Map(3,1)))
    end do

!!$    ! ... a hack to make 3D isotropic turbulence work
!!$    If (ALL(grid%periodic(1:3) == PLANE_PERIODIC)) Then
!!$      grid%MT1(:,:) = 0.0_rfreal
!!$      grid%MT1(:,region%global%t2Map(1,1)) = storage(:,region%global%t2Map(2,2)) * storage(:
    !!,region%global%t2Map(3,3))
!!$      grid%MT1(:,region%global%t2Map(2,2)) = storage(:,region%global%t2Map(1,1)) * storage(:
    !!,region%global%t2Map(3,3))
!!$      grid%MT1(:,region%global%t2Map(3,3)) = storage(:,region%global%t2Map(1,1)) * storage(:
    !!,region%global%t2Map(2,2))
!!$      grid%INVJAC(:) = storage(:,region%global%t2Map(1,1)) * storage(:,region%global%t2Map(2
    !!,2)) * storage(:,region%global%t2Map(3,3))
!!$    End If


    ! ... take into account iblank
    do i = 1, grid%nCells
      ibfac_local = grid%ibfac(i)
      grid%INVJAC(i) = grid%INVJAC(i) * ibfac_local + (1.0_rfreal - ibfac_local)
      grid%JAC(i) = 1.0_rfreal / grid%INVJAC(i)
    end do

    ! ...   xihat_t = -[x_tau xihat_x + y_tau xihat_y + z_tau xihat_z]
    ! ...  etahat_t = -[x_tau etahat_x + y_tau etahat_y + z_tau
    ! etahat_z]
    ! ... zetahat_t = -[x_tau zetahat_x + y_tau zetahat_y + z_tau
    ! zetahat_z]
    ! ... (1/J)_tau = -[(xihat_t)_xi + (etahat_t)_eta +
    ! (zetahat_t)_zeta]  
    if (grid%moving == FALSE) then
      Do ii = 1, grid%nCells
        grid%XI_TAU(ii,:)     = 0.0_rfreal
        grid%XI_TAU_XI(ii,:)  = 0.0_rfreal
        grid%XYZ_TAU(ii,:)    = 0.0_rfreal
        grid%XYZ_TAU2(ii,:)   = 0.0_rfreal
        grid%INVJAC_TAU(ii)   = 0.0_rfreal
      End Do
    else
      Do ii = 1, Nc
        grid%XI_TAU(ii,1) = - (grid%XYZ_TAU(ii,1) * grid%MT1(ii,region%global%t2Map(1,1)) + grid%XYZ_TAU(ii,2) * grid%MT1(ii,region%global%t2Map(1,2)) + grid%XYZ_TAU(ii,3) * grid%MT1(ii,region%global%t2Map(1,3)))
        grid%XI_TAU(ii,2) = - (grid%XYZ_TAU(ii,1) * grid%MT1(ii,region%global%t2Map(2,1)) + grid%XYZ_TAU(ii,2) * grid%MT1(ii,region%global%t2Map(2,2)) + grid%XYZ_TAU(ii,3) * grid%MT1(ii,region%global%t2Map(2,3)))
        grid%XI_TAU(ii,3) = - (grid%XYZ_TAU(ii,1) * grid%MT1(ii,region%global%t2Map(3,1)) + grid%XYZ_TAU(ii,2) * grid%MT1(ii,region%global%t2Map(3,2)) + grid%XYZ_TAU(ii,3) * grid%MT1(ii,region%global%t2Map(3,3)))
      End Do
      Do ii = 1, Nc
        F(ii) = grid%XI_TAU(ii,1)
      End Do
      call APPLY_OPERATOR_BOX(region, ng, input%iFirstDeriv, 1, F, dF, .FALSE., 1)
      Do ii = 1, Nc
        grid%XI_TAU_XI(ii,1) = dF(ii)
        F(ii) = grid%XI_TAU(ii,2)
      End Do
      call APPLY_OPERATOR_BOX(region, ng, input%iFirstDeriv, 2, F, dF, .FALSE., 1)
      Do ii = 1, Nc
        grid%XI_TAU_XI(ii,2) = dF(ii)
        F(ii) = grid%XI_TAU(ii,3)
      End Do
      call APPLY_OPERATOR_BOX(region, ng, input%iFirstDeriv, 3, F, dF, .FALSE., 1)
      Do ii = 1, Nc
        grid%XI_TAU_XI(ii,3) = dF(ii)
        grid%INVJAC_TAU(ii) = - grid%XI_TAU_XI(ii,1) - grid%XI_TAU_XI(ii,2) - grid%XI_TAU_XI(ii,3)
      End Do
    end if

    ! ... compute derivatives of the metrics needed for BCs
    ! ... compute the xi- and eta-derivatives only
    do i = 1, grid%ND-1
      do j = 1, grid%ND
        do ii = 1, grid%nCells
          F(ii) = grid%MT1(ii,region%global%t2Map(i,j))
        end do
        call APPLY_OPERATOR_BOX(region, ng, 1, i, F, dF, .FALSE., 1)
        do ii = 1, grid%nCells
          grid%MT2(ii,region%global%t2Map(i,j)) = dF(ii) 
        end do
      end do
    end do
    ! ... get zeta derivatives via I1, I2 and I3 identities
    i = 3
    do j = 1, grid%ND
      do ii = 1, grid%nCells
        grid%MT2(ii,region%global%t2Map(i,j)) = -grid%MT2(ii,region%global%t2Map(1,j))-grid%MT2(ii,region%global%t2Map(2,j))
      end do
    end do

    if (grid%metric_type == CARTESIAN) then

      ! ... compute 2nd order metrics for d2f/dx_i2
      allocate(grid%cartMetric(Nc,grid%ND))
      do i = 1, grid%ND
        do ii = 1, grid%nCells
          F(ii) = grid%JAC(ii) * grid%MT1(ii,region%global%t2Map(i,i))
        end do
        Call APPLY_OPERATOR_BOX(region, ng, 1, i, F, dF, .TRUE., 1)
        do ii = 1, grid%nCells
          grid%cartMetric(ii,i) = dF(ii) * grid%JAC(ii) * grid%MT1(ii,region%global%t2Map(i,i))
        end do
      end do

    end if

    loop1: Do ii = 1, Nc
      if (grid%iblank(ii) /= 0) then
        If (grid%JAC(ii) <= 0.0_rfreal) Then
          Write (*,'(2(A,I3),A)') 'PlasComCM: WARNING: grid ', grid%iGridGlobal, ' on processor ', region%myrank, ' has a negative jacobian.'
          EXIT loop1
        End If
      end if
    End Do loop1

    ! ... compute metric identies
    If (input%useMetricIdentities == TRUE) Then
      Do i = 1, 3
        do ii = 1, grid%nCells
          grid%ident(ii,i) = 0.0_rfreal
        end do
        do j = 1, 3
          do ii = 1, grid%nCells
            F(ii) = grid%MT1(ii,region%global%t2Map(j,i))
          end do
          Call APPLY_OPERATOR_BOX(region, ng, 1, j, F, dF, .FALSE., 1)
          do ii = 1, grid%nCells
            grid%ident(ii,i) = grid%ident(ii,i) + dF(ii)
          end do
        end do
      end do
    End If


    ! ... clean up and exit
    deallocate(F,dF,storage)
    nullify(F,dF,storage)

  end subroutine metrics_3D

  subroutine compute_metricInverse(region, grid, l0, metricInverse)

    USE ModGlobal
    USE ModDataStruct

    Type (t_region), Pointer :: region
    Type (t_grid), Pointer :: grid
    Integer :: l0
    Real(rfreal) :: metricInverse(MAX_ND,MAX_ND)
    Real(rfreal) :: det
    Real(rfreal) :: nx, ny, nz, tx, ty, tz, bx, by, bz

    ! ... simplify
    nx = grid%MT1(l0,region%global%t2Map(1,1))
    ny = grid%MT1(l0,region%global%t2Map(1,2))
    tx = grid%MT1(l0,region%global%t2Map(2,1))
    ty = grid%MT1(l0,region%global%t2Map(2,2))

    if (grid%ND == 3) then
      nz = grid%MT1(l0,region%global%t2Map(1,3))
      tz = grid%MT1(l0,region%global%t2Map(2,3))
      bx = grid%MT1(l0,region%global%t2Map(3,1))
      by = grid%MT1(l0,region%global%t2Map(3,2))
      bz = grid%MT1(l0,region%global%t2Map(3,3))
    else
      nz = 0.0_rfreal
      tz = 0.0_rfreal
      bx = 0.0_rfreal
      by = 0.0_rfreal
      bz = 1.0_rfreal
    end if

    ! ... compute the determinant
    If (grid%ND == 3) Then
      det = -bz * ny * tx + by * nz * tx + bz * nx * ty - bx * nz *&
           & ty - by * nx * tz + bx * ny * tz
    Else If (grid%ND == 2) Then
      det = nx * ty - ny * tx
    Else
      metricInverse(1,1) = 1.0_rfreal / metricInverse(1,1)
      return
    End If

    ! ... embed 2d-problem in 3 x 3 array
    metricInverse = 0.0_rfreal
    metricInverse(3,3) = 1.0_rfreal

    ! ... fill in the matrix
    If (grid%ND == 3) Then
      metricInverse(1,1) = bz * ty - by * tz
      metricInverse(1,2) = -bz * ny + by * nz
      metricInverse(1,3) = -nz * ty + ny * tz
      metricInverse(2,1) = -bz * tx + bx * tz
      metricInverse(2,2) = bz * nx - bx * nz
      metricInverse(2,3) = nz * tx - nx * tz
      metricInverse(3,1) = by * tx - bx * ty
      metricInverse(3,2) = -by * nx + bx * ny
      metricInverse(3,3) = -ny * tx + nx * ty
    Else
      metricInverse(1,1) = ty
      metricInverse(1,2) = -ny
      metricInverse(2,1) = -tx
      metricInverse(2,2) = nx
    End If

    metricInverse = metricInverse / det

  end subroutine compute_metricInverse

  subroutine inverseProjection(n, t1, t2, invProj)
    USE ModGlobal

    Real(rfreal) :: invProj(MAX_ND,MAX_ND)
    Real(rfreal) :: det, n(MAX_ND), t1(MAX_ND), t2(MAX_ND)
    Real(rfreal) :: nx, ny, nz, tx, ty, tz, bx, by, bz

    ! ... simplify
    nx = n(1)
    ny = n(2)
    nz = n(3)
    tx = t1(1)
    ty = t1(2)
    tz = t1(3)
    bx = t2(1)
    by = t2(2)
    bz = t2(3)

    ! ... compute the determinant
    det = -bz * ny * tx + by * nz * tx + bz * nx * ty - bx * nz * ty &
         &- by * nx * tz + bx * ny * tz

    ! ... fill in the matrix
    invProj(1,1) = bz * ty - by * tz
    invProj(1,2) = -bz * ny + by * nz
    invProj(1,3) = -nz * ty + ny * tz
    invProj(2,1) = -bz * tx + bx * tz
    invProj(2,2) = bz * nx - bx * nz
    invProj(2,3) = nz * tx - nx * tz
    invProj(3,1) = by * tx - bx * ty
    invProj(3,2) = -by * nx + bx * ny
    invProj(3,3) = -ny * tx + nx * ty

    ! ... divide by the determinant
    invProj = invProj / det

  end subroutine inverseProjection

  subroutine cross_product(v1, v2, y)

    USE ModGlobal

    Real(rfreal) :: v1(MAX_ND), v2(MAX_ND), y(MAX_ND)

    y(1) = v1(2)*v2(3) - v1(3)*v2(2)
    y(2) = v1(3)*v2(1) - v1(1)*v2(3)
    y(3) = v1(1)*v2(2) - v1(2)*v2(1)

  end subroutine cross_product

  subroutine invert_matrix(N,MAT)

    USE ModGlobal

    Integer :: N
    Real(rfreal) :: MAT(N,N)
    Real(rfreal) :: nx, ny, nz, tx, ty, tz, bx, by, bz, det

    ! ... simplify
    nx = MAT(1,1)
    ny = MAT(1,2)
    tx = MAT(2,1)
    ty = MAT(2,2)

    if (N == 3) then
      nz = MAT(1,3)
      tz = MAT(2,3)
      bx = MAT(3,1)
      by = MAT(3,2)
      bz = MAT(3,2)
    else
      nz = 0.0_rfreal
      tz = 0.0_rfreal
      bx = 0.0_rfreal
      by = 0.0_rfreal
      bz = 1.0_rfreal
    end if

    ! ... compute the determinant
    If (N == 3) Then
      det = -bz * ny * tx + by * nz * tx + bz * nx * ty - bx * nz *&
           & ty - by * nx * tz + bx * ny * tz
    Else If (N == 2) Then
      det = nx * ty - ny * tx
    Else
      MAT(1,1) = 1.0_rfreal / MAT(1,1)
      return
    End If

    ! ... fill in the matrix
    If (N == 3) Then
      MAT(1,1) = bz * ty - by * tz
      MAT(1,2) = -bz * ny + by * nz
      MAT(1,3) = -nz * ty + ny * tz
      MAT(2,1) = -bz * tx + bx * tz
      MAT(2,2) = bz * nx - bx * nz
      MAT(2,3) = nz * tx - nx * tz
      MAT(3,1) = by * tx - bx * ty
      MAT(3,2) = -by * nx + bx * ny
      MAT(3,3) = -ny * tx + nx * ty
    Else
      MAT(1,1) = ty
      MAT(1,2) = -ny
      MAT(2,1) = -tx
      MAT(2,2) = nx
    End If

    Return

  End Subroutine invert_matrix

  Subroutine compute_metric_invariants(region)

    USE ModGlobal
    USE ModDataStruct
    USE ModDeriv
    USE ModMPI

    TYPE (t_region), pointer :: region

    ! ... local variables
    TYPE (t_mixt_input), pointer :: input
    TYPE (t_grid), pointer :: grid
    INTEGER :: ng
    REAL(RFREAL), POINTER :: F(:), dF(:), I1(:), I2(:), I3(:)

    If (region%input%ND == 2) Then

      Do ng = 1, region%nGrids
  
        grid  => region%grid(ng)
        input => grid%input

        allocate(F(grid%nCells), dF(grid%nCells), I1(grid%nCells),&
             & I2(grid%nCells))

        I1(:) = 0.0_rfreal ; I2(:) = 0.0_rfreal

        ! ... (xihat_x)_xi + (etahat_x)_eta = 0
        F(:) = grid%MT1(:,region%global%t2Map(1,1))
        Call APPLY_OPERATOR(region, ng, 1, 1, F, dF, .TRUE.)
        I1(:) = dF(:)

        F(:) = grid%MT1(:,region%global%t2Map(2,1))
        Call APPLY_OPERATOR(region, ng, 1, 2, F, dF, .TRUE.)
        I1(:) = I1(:) + dF(:)

        ! ... (xihat_y)_xi + (etahat_y)_eta = 0
        F(:) = grid%MT1(:,region%global%t2Map(1,2))
        Call APPLY_OPERATOR(region, ng, 1, 1, F, dF, .TRUE.)
        I2(:) = dF(:)

        F(:) = grid%MT1(:,region%global%t2Map(2,2))
        Call APPLY_OPERATOR(region, ng, 1, 2, F, dF, .TRUE.)
        I2(:) = I2(:) + dF(:)

        grid%MT1(:,region%global%t2Map(1,1)) = I1(:)
        grid%MT1(:,region%global%t2Map(1,2)) = I2(:)
        grid%MT1(:,region%global%t2Map(2,1)) = 0.0_rfreal
        grid%MT1(:,region%global%t2Map(2,2)) = 0.0_rfreal

        deallocate(F, dF, I1, I2)

      End Do

    End If

  End subroutine compute_metric_invariants

  Subroutine Save_MT2_on_Boundary(region, ng)

    USE ModGlobal
    USE ModDataStruct
    USE ModMPI

    type (t_region), pointer :: region
    integer :: ng

    ! ... local variables
    type (t_grid), pointer :: grid
    type (t_patch), pointer :: patch
    integer :: patchID, Np(MAX_ND), N(MAX_ND), ip, jp, kp, lp, i, l0,&
         & j

    do patchID = 1, region%nPatches

      patch => region%patch(patchID)
      grid => region%grid(patch%gridID)

      if (patch%gridID == ng .and. patch%bcType /= SPONGE .and. patch%bcType /= SPONGE_WITH_LINEARIZED_DISTURBANCE) Then

        Np(:) = 1
        N(:)  = 1
        do j = 1, grid%ND
          Np(j) = patch%ie(j) - patch%is(j) + 1
          N(j)  =  grid%ie(j) -  grid%is(j) + 1
        end do

        do kp = patch%is(3), patch%ie(3)
          do jp = patch%is(2), patch%ie(2)
            do ip = patch%is(1), patch%ie(1)
              l0 = (kp- grid%is(3))* N(1)* N(2) + (jp- grid%is(2))*&
                   & N(1) + (ip- grid%is(1)+1)
              lp = (kp-patch%is(3))*Np(1)*Np(2) + (jp-patch%is(2))&
                   &*Np(1) + (ip-patch%is(1)+1)
              patch%MT2(lp,:) = grid%MT2(l0,:)
            end do
          end do
        end do

      end if

    end do

    ! ... free MT2
    deallocate(region%grid(ng)%MT2)
    nullify(region%grid(ng)%MT2)

  End Subroutine Save_MT2_on_Boundary

  Subroutine Move_Rotor_Grid(region)

    USE ModGlobal
    USE ModDataStruct
    USE ModDeriv
    USE ModMPI

    ! ... input variables
    type(t_region), pointer :: region

    ! ... local variables
    type(t_grid), pointer :: grid
    type(t_mixt), pointer :: state
    integer :: ng, offset
    real(rfreal) :: fac, time, Vmax, local_minVal, global_minVal
    integer :: ierr

    ! ... loop through all grids, only moving the rotor mesh
    do ng = 1, region%nGrids

      grid => region%grid(ng)
      state => region%state(ng)

      time = state%time(1)

      if (grid%iGridGlobal >= 3) then

        ! ... allocate and initialize, if necessary
        if (.not.associated(grid%XYZOld) .eqv. .true.) then
          allocate(grid%XYZOld(grid%nCells,grid%ND))
          grid%XYZOld(:,:) = grid%XYZ(:,:)
        end if

        ! ... y-position is y0 + U t
        Vmax = 0.56_rfreal
        grid%XYZ(:,1) = grid%XYZOld(:,1)
        grid%XYZ(:,2) = grid%XYZOld(:,2) + Vmax * time

        ! ... communicate minimum value
        local_minVal = minval(grid%XYZ(:,2))
        Call MPI_Allreduce(local_minVal, global_minVal, 1, MPI_DOUBLE_PRECISION, MPI_MIN, grid%comm, ierr)

        ! ... periodic wrap
        if (global_minVal > 5.025_rfreal) then
          grid%XYZ   (:,2) = grid%XYZ   (:,2) - 2.0_rfreal * 10.025_rfreal
          grid%XYZOld(:,2) = grid%XYZOld(:,2) - 2.0_rfreal * 10.025_rfreal
        end if

        ! ... update moving metrics
        if (associated(grid%XYZ_TAU) .eqv. .true.) then
          grid%XYZ_TAU (:,1) = 0.0_rfreal
          grid%XYZ_TAU2(:,1) = 0.0_rfreal
          grid%XYZ_TAU (:,2) = Vmax
          grid%XYZ_TAU2(:,2) = 0.0_rfreal
        end if

      end if

    end do

    ! ... everyone wait
    call mpi_barrier(mycomm, ierr)

  End Subroutine Move_Rotor_Grid


  Subroutine Move_Cylinder_Grid(region)

    USE ModGlobal
    USE ModDataStruct
    USE ModDeriv
    USE ModMPI

    ! ... input variables
    type(t_region), pointer :: region

    ! ... local variables
    type(t_grid), pointer :: grid
    type(t_mixt), pointer :: state
    integer :: ng, i
    real(rfreal) :: omega, amp, Vmax
    integer :: ierr

    ! ... loop through all grids, only moving the rotor mesh (iGridGlobal = 3)
    do ng = 1, region%nGrids

      grid => region%grid(ng)
      state => region%state(ng)

      if (grid%iGridGlobal == 2) then

        ! ... allocate and initialize, if necessary
        if (.not.associated(grid%XYZOld) .eqv. .true.) then
          allocate(grid%XYZOld(grid%nCells,grid%ND))
          grid%XYZOld(:,:) = grid%XYZ(:,:)
        end if

        ! ... y-position is y0 + U t
        omega =  1.00_rfreal
        amp   =  0.1_rfreal
        Vmax  = -0.00_rfreal
!        do i = 1, grid%nCells
          grid%XYZ(:,1) = grid%XYZOld(:,1) + amp * sin(omega * state%time(:))
          grid%XYZ(:,2) = grid%XYZOld(:,2)
!        end do

        ! ... update moving metrics
        if (associated(grid%XYZ_TAU) .eqv. .true.) then
!          do i = 1, grid%nCells
            grid%XYZ_TAU(:,1) = amp * omega * cos(omega * state%time(:))
            grid%XYZ_TAU(:,2) = 0.0_rfreal
            grid%XYZ_TAU2(:,1) = -amp * omega * omega * sin(omega * state%time(:))
            grid%XYZ_TAU2(:,2) = 0.0_rfreal
!          end do
        end if

      end if

    end do

    ! ... everyone wait
    call mpi_barrier(mycomm, ierr)

  End Subroutine Move_Cylinder_Grid

END MODULE ModMetrics
