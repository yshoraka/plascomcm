! Copyright (c) 2015, University of Illinois at Urbana-Champaign, XPACC
! License: MIT, http://opensource.org/licenses/MIT
!----------------------------------------------------------------------
!
! ModNavierStokesImplicit.F90
! 
! - basic code for the evaluation of the Jabocian for Navier-Stokes 
!   equations
!
! Revision history
! - 21 Oct 2009 : DJB : initial code
! 
! flags == FALSE    : compute inviscid fluxes only
! flags == TRUE     : compute dissipative fluxes only
! flags == COMBINED : inviscid + dissipative fluxes
!
! $Header: /cvsroot/genx/Codes/RocfloCM/Source/ModNavierStokesImplicit.f90,v 1.16 2011/02/02 04:12:10 bodony Exp $
!
!-----------------------------------------------------------------------
![UNUSED]
MODULE ModNavierStokesImplicit

CONTAINS

  subroutine NS_Implicit_Jacobian(region, flags, dtau, iters, buildPC, lowerdtau)

    USE ModGlobal
    USE ModDataStruct
    USE ModDeriv
    USE ModMPI
    Use ModIO

    ! ... global variables
    type(t_region), pointer :: region
    integer :: flags, iters, buildPC, lowerdtau

    ! ... local variables
    type(t_grid), pointer :: grid
    type(t_mixt), pointer :: state
    type(t_mixt_input), pointer :: input
    type(t_mpi_timings), pointer :: mpi_timings
    real(rfreal) :: timer, dummy
    integer :: ng, k, i, dir, ndim, npart, my_part, local_val, local_ind, l0, l1, order
    integer :: is(MAX_ND), ie(MAX_ND), varTypes(MAX_ND+2)
    integer :: icomm, igrid, min_stencil_start, max_stencil_end, stencil_size, lhs_var, rhs_var
    real(rfreal), pointer :: values_r(:), values_ru(:), values_rv(:), values_rw(:), values_rE(:), values(:)
    integer :: ii0, jj0, kk0, ii1, jj1, kk1, N1, N2, N3, j
    real(rfreal) :: P, T, GAM, RHO, dtau
    integer :: ierr

    ! ... simplicity
!    input => region%input 

    ! ! ... stencil size of implicit operator
    ! min_stencil_start = input%max_stencil_start(input%iFirstDerivImplicit)
    ! max_stencil_end   = input%max_stencil_end(input%iFirstDerivImplicit)
    ! if (input%RE > 0.0_rfreal .OR. input%shock /= FALSE .OR. input%LES /= FALSE) then
    !   min_stencil_start = min(min_stencil_start, input%max_stencil_start(input%iSecondDerivImplicit))
    !   max_stencil_end   = max(max_stencil_end  , input%max_stencil_end(input%iSecondDerivImplicit))
    ! end if
    ! stencil_size = input%ND * ( (max_stencil_end) - (min_stencil_start) ) + 1

    ! ... build the jacobian matrix one grid at a time
    Do ng = 1, region%nGrids

      state => region%state(ng)
      grid  => region%grid(ng)
      input => grid%input

      ! ... stencil size of implicit operator
      min_stencil_start = input%max_stencil_start(input%iFirstDerivImplicit)
      max_stencil_end   = input%max_stencil_end(input%iFirstDerivImplicit)
      if (input%RE > 0.0_rfreal .OR. input%shock /= FALSE .OR. input%LES /= FALSE) then
         min_stencil_start = min(min_stencil_start, input%max_stencil_start(input%iSecondDerivImplicit))
         max_stencil_end   = max(max_stencil_end  , input%max_stencil_end(input%iSecondDerivImplicit))
      end if
      stencil_size = input%ND * ( (max_stencil_end) - (min_stencil_start) ) + 1


      ! ... it is easier to create the values here and the pass them to hypre
      ! ... create the space
      allocate(values_r (grid%nCells*stencil_size))
      allocate(values_ru(grid%nCells*stencil_size))
      allocate(values_rE(grid%nCells*stencil_size))
      if (grid%ND == 1) then
        allocate(values_rv(1))
        allocate(values_rw(1))
      else if (grid%ND == 2) then
        allocate(values_rv(grid%nCells*stencil_size))
        allocate(values_rw(1))
      else if (grid%ND == 3) then
        allocate(values_rv(grid%nCells*stencil_size))
        allocate(values_rw(grid%nCells*stencil_size))
      end if

      ! ... be specific by dimension
      Select Case (grid%ND)

      Case (1)

        call graceful_exit(region%myrank, 'PlasComCM: ERROR: Incorrect dimensionality in NS_Implicit_Jacobian.  (No 1-D)')

        do lhs_var = 1, 3
          ! Call NSImplicit1D(input, grid, state, lhs_var, values_r, values_ru, values_rE, stencil_size)
          do rhs_var = 1, 3
            select case (rhs_var)
            case(1)
              values => values_r
            case(2)
              values => values_ru
            case(3)
              values => values_rE
            end select
#ifdef USE_HYPRE
            call fill_hypre_implicit_jacobian_matrix(grid%nGridsGlobal, grid%iGridGlobal, ng, region%nGrids, &
                 grid%myrank_inComm, grid%numproc_inComm, grid%comm, grid%ND, grid%is, grid%ie, &
                 min_stencil_start, max_stencil_end, values, lhs_var, rhs_var)
#endif
          end do
        end do
      
      Case (2)

        do lhs_var = 1, 4


          !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! ... doing analytical RHS...!!!!!!!!!!!!!!!!!!!!
!          Call NSImplicit2DAn(input, grid, state, region%global, lhs_var, values_r, values_ru, values_rv, values_rE, stencil_size, dtau)
          Call NSImplicit2D(region, ng, lhs_var, values_r, values_ru, values_rv, values_rE, stencil_size, dtau)
          Call NSImplicitAddBC2D(region, ng, lhs_var, values_r, values_ru, values_rv, values_rE, stencil_size, dtau)

          do rhs_var = 1, 4
            select case (rhs_var)
            case(1)
              values => values_r
            case(2)
              values => values_ru
            case(3)
              values => values_rv
            case(4)
              values => values_rE
            end select
! do i = 1,size(values)
! if(values(i) * 0.0 .ne. 0.0) then
!    print*,'Nan or Inf found at rhs_var, lhs_var, i,myrank',rhs_var, lhs_var, i,region%myrank
! endif
! enddo

! if (region%myrank == 1 .and. input%Sub1< 2) then
!             print*,'rhs_var,lhs_var,myrank',rhs_var,lhs_var,region%myrank
!             do i = 1,grid%nCells
!                print*,i,(values(stencil_size*(i-1)+j),j=1,stencil_size)
!             enddo
            
!          endif 

!!!!!!!!!!! Chris is outputting the LHS to 1000 + conv_iter*100 * lhs_var*10 + rhs_var, where each
!!!!!!!!!!! variabled (k index) refers to the index in the stencil (0,-1,+1,-1,+1)=(1,2,3,4,5)
!!!!!!!!!!! size(target(1,:) is only 4, therefore can't do all stencil entries at once
            if (.false.)then         
            do i = 1, grid%nCells
               state%timeOld(i)  = state%time(i)
               state%time(i) = dble(1000 + input%Sub1*100 + lhs_var*10+rhs_var)
            end do
            do k = 1, grid%ND+2
               do i = 1, grid%nCells
                  state%cvTargetOld(i,k) = state%cvTarget(i,k)
               end do
            end do
            do k = 1,4
               do i = 1, grid%nCells
                  select case(rhs_var)
                  case(1)
                     state%cvTarget(i,k) = values_r((i-1)*stencil_size+k)
                  case(2)
                     state%cvTarget(i,k) = values_ru((i-1)*stencil_size+k)
                  case(3)
                     state%cvTarget(i,k) = values_rv((i-1)*stencil_size+k)
                  case(4)
                     state%cvTarget(i,k) = values_rE((i-1)*stencil_size+k)
                  end select
               end do
            enddo
            call WriteData(region, 'cvt', 1000 + input%Sub1*100 + lhs_var*10+rhs_var)
            do i = 1, grid%nCells
               state%time(i) = state%timeOld(i)
            end do
         
            do k = 1, 4
               do i = 1, grid%nCells
                  state%cvTarget(i,k) = state%cvTargetOld(i,k)
               end do
            end do
         endif
  
         call mpi_barrier(mycomm,ierr)

#ifdef USE_HYPRE
         call fill_hypre_implicit_jacobian_matrix(grid%nGridsGlobal, grid%iGridGlobal, ng, region%nGrids, &
                                                  grid%myrank_inComm, grid%numproc_inComm, grid%comm, &
                                                  grid%ND, grid%is, grid%ie, min_stencil_start, max_stencil_end, &
                                                  values, lhs_var, rhs_var)
#endif

!if (input%Sub1 == 3) call graceful_exit(region%myrank, 'Show me the bug!')     
         end do
      end do
!call graceful_exit(region%myrank, 'just checking one ts')
      
      Case (3)

        do lhs_var = 1, 5

         Call NSImplicit3D(region, ng, lhs_var, values_r, values_ru, values_rv, values_rw, values_rE, stencil_size, dtau)
         Call NSImplicitAddBC3D(region, ng, lhs_var, values_r, values_ru, values_rv, values_rw, values_rE, stencil_size, dtau)

          do rhs_var = 1, 5

            select case (rhs_var)
            case(1)
              values => values_r
            case(2)
              values => values_ru
            case(3)
              values => values_rv
            case(4)
              values => values_rw
            case(5)
              values => values_rE
            end select
#ifdef USE_HYPRE
            call fill_hypre_implicit_jacobian_matrix(grid%nGridsGlobal, grid%iGridGlobal, ng, region%nGrids, &
                                                     grid%myrank_inComm, grid%numproc_inComm, grid%comm, &
                                                     grid%ND, grid%is, grid%ie, min_stencil_start, max_stencil_end, &
                                                     values, lhs_var, rhs_var)
#endif
          end do
       end do
      


      Case Default

        call graceful_exit(region%myrank, 'PlasComCM: ERROR: Incorrect dimensionality in NS_Implicit_Jacobian.')

      End Select


      ! ... final matrix assemble
#ifdef USE_HYPRE
      call assemble_hypre_implicit_jacobian_matrix(grid%nGridsGlobal, grid%iGridGlobal, ng, region%nGrids, &
                                                   grid%myrank_inComm, grid%numproc_inComm, grid%comm)
#endif


      ! ... build the rhs, b
#ifdef USE_HYPRE
      call fill_hypre_implicit_rhs_vector(grid%nGridsGlobal, grid%iGridGlobal, ng, region%nGrids, &
                                          grid%myrank_inComm, grid%numproc_inComm, grid%comm, &
                                          grid%ND, grid%is, grid%ie, min_stencil_start, max_stencil_end, &
                                          state%rhs, values_r)
#endif

      ! ... build the initial guess for the solution
      if (grid%ND == 2) call NSImplicitInitSoln2D(grid, state, values_r, values_ru, values_rv, values_rE)
      if (grid%ND == 3) call NSImplicitInitSoln3D(grid, state, values_r, values_ru, values_rv, values_rw, values_rE)

      ! ... build the soln vector, x
#ifdef USE_HYPRE
      call fill_hypre_implicit_soln_vector(grid%nGridsGlobal, grid%iGridGlobal, ng, region%nGrids, &
                                           grid%myrank_inComm, grid%numproc_inComm, grid%comm, &
                                           grid%ND, grid%is, grid%ie, min_stencil_start, max_stencil_end, &
                                           values_r, values_ru, values_rv, values_rw, values_rE)
#endif


      ! ... solve Ax = b for x
#ifdef USE_HYPRE
      call hypre_parcsr_solve_implicit_time(grid%nGridsGlobal, grid%iGridGlobal, ng, region%nGrids, &
                                            grid%myrank_inComm, grid%numproc_inComm, grid%comm, &
                                            grid%ND, grid%is, grid%ie, min_stencil_start, max_stencil_end,&
                                            iters, buildPC, lowerdtau, input%HypreParamsInt, input%HypreParamsReal)
#endif

!       call hypre_parcsr_solve_implicit_time(grid%nGridsGlobal, grid%iGridGlobal, ng, region%nGrids, &
!                                             grid%myrank_inComm, grid%numproc_inComm, grid%comm, &
!                                             grid%ND, grid%is, grid%ie, min_stencil_start, max_stencil_end,&
!                                             iters, buildPC, lowerdtau, input%GMRESConvThresh, input%EuclidLevel,&
!                                            input%KDim,input%numGMRESInnerIters)
      ! call hypre_solve_implicit_time(grid%nGridsGlobal, grid%iGridGlobal, ng, region%nGrids, &
      !                                grid%myrank_inComm, grid%numproc_inComm, grid%comm, &
      !                                grid%ND, grid%is, grid%ie, min_stencil_start, max_stencil_end, iters)

      ! if(lowerdtau /= 0) then
      !    print*,'lowering dtau'
      !    ! ! ... clean up
      !    ! deallocate(values_r)
      !    ! deallocate(values_ru)
      !    ! deallocate(values_rv)
      !    ! deallocate(values_rw)
      !    ! deallocate(values_rE)
      !    ! return
      ! end if


      ! ... fill state%cv with primative variables
      do i = 1, grid%nCells
        state%cv(i,1) = state%dv(i,1)
        state%cv(i,2) = state%cv(i,2) * state%dv(i,3)
      end do
      if (grid%ND >= 2) then
        do i = 1, grid%nCells
          state%cv(i,3) = state%cv(i,3) * state%dv(i,3)
        end do
      end if
      if (grid%ND == 3) then
        do i = 1, grid%nCells
          state%cv(i,4) = state%cv(i,4) * state%dv(i,3)
        end do
      end if
      do i = 1, grid%nCells
        state%cv(i,grid%ND+2) = state%dv(i,2) 
      end do

      ! ... extract the solution but don't update
#ifdef USE_HYPRE
      call hypre_update_implicit_soln_vector(grid%nGridsGlobal, grid%iGridGlobal, ng, region%nGrids, &
                                             grid%myrank_inComm, grid%numproc_inComm, grid%comm, &
                                             grid%ND, grid%is, grid%ie, min_stencil_start, max_stencil_end, &
                                             state%rhs, grid%JAC)
#endif

!!!!!!!!!chris is outputting the solution to target 3000 + conv_iter*100
if (.false.)then
      ! do ng = 1, region%nGrids
      !     state => region%state(ng)
      !     grid  => region%grid(ng)
          do i = 1, grid%nCells
            state%timeOld(i)  = state%time(i)
            state%time(i) = dble(3000 + input%Sub1*100)
          end do
          do k = 1, grid%ND+2
            do i = 1, grid%nCells
              state%cvTarget(i,k) = state%rhs(i,k)
            end do
         end do
!       end do
        call WriteData(region, 'cvt',3000 + 100*input%Sub1)
        !do ng = 1, region%nGrids
          state => region%state(ng)
          grid  => region%grid(ng)
          do i = 1, grid%nCells
            state%time(i) = state%timeOld(i)
          end do
          do k = 1, grid%ND+2 
            do i = 1, grid%nCells
              state%cvTarget(i,k) = state%cvTargetOld(i,k)
            end do
          end do
       !end do

        !!!!!!!!!!!!!!!!!!!! end of RHS output !!!!!!!!!!!!!!!!!!!!!!!
       end if
      ! ... state%rhs contains the increment
      ! ... filter it
      if (input%LHS_Filter == TRUE) then
        call filter(region,ng,TRUE,input%iLHS_Filter)
      endif

      ! ... now update
      do j = 1, size(state%cv,2)
        do i = 1, grid%nCells
          state%cv(i,j) = state%cv(i,j) + state%rhs(i,j)
        end do
      end do

      ! ... convert back to conservative variables
      do i = 1, grid%nCells
        P   = state%cv(i,1)
        T   = state%cv(i,grid%ND+2)
        GAM = state%gv(i,1)
        RHO = GAM * P / ((GAM-1.0_8) * T)

        state%cv(i,grid%ND+2) = P / (GAM - 1.0_8) + 0.5_8 * RHO * state%cv(i,2) * state%cv(i,2)
        state%cv(i,1)         = RHO
        state%cv(i,2)         = RHO * state%cv(i,2)
      end do
      if (grid%ND >= 2) then
        do i = 1, grid%nCells
          state%cv(i,grid%ND+2) = state%cv(i,grid%ND+2) + 0.5_8 * state%cv(i,1) * state%cv(i,3) * state%cv(i,3)
          state%cv(i,3) = state%cv(i,1) * state%cv(i,3)
        end do
      end if
      if (grid%ND == 3) then
        do i = 1, grid%nCells
          state%cv(i,grid%ND+2) = state%cv(i,grid%ND+2) + 0.5_8 * state%cv(i,1) * state%cv(i,4) * state%cv(i,4)
          state%cv(i,4) = state%cv(i,1) * state%cv(i,4)
        end do
      end if
      
      ! ... clean up
      deallocate(values_r)
      deallocate(values_ru)
      deallocate(values_rv)
      deallocate(values_rw)
      deallocate(values_rE)

   End Do

  end subroutine NS_Implicit_Jacobian

  Subroutine NSImplicit2D(region, ng, lhs_var, values_r, values_ru, values_rv, values_rE, stencil_size, dtau)

    USE ModGlobal
    USE ModDataStruct
    USE ModDeriv
    USE ModMPI
    USE ModNavierStokesImplicitSATArtDiss

    Implicit None

    ! ... global variables
    type(t_region), pointer :: region
    type(t_mixt_input), pointer :: input
    type(t_grid), pointer :: grid
    type(t_mixt), pointer :: state
    real(rfreal), pointer :: values_r(:), values_ru(:), values_rv(:), values_rE(:)
    real(rfreal) :: dtau
    integer :: lhs_var

    ! ... local variables
    integer :: ng, k, j, i, ndim, npart, my_part, local_ind, l0, l1, order, dir, opID, ierr
    integer :: is(MAX_ND), ie(MAX_ND), varTypes(MAX_ND+2)
    integer :: icomm, igrid, min_stencil_start, max_stencil_end, stencil_size, rhs_var, lhs_var_copy
    integer :: ii0, jj0, kk0, ii1, jj1, kk1, N1, N2, N3, index, N1g, N2g, N3g, ii, jj, kk, perOffset, Nc, Ncg
    real(rfreal) :: RHO_P, RHO_T, gam, gamm1i, UX, RHOE_P, RHOE_T, RHOE_UX, UY, RHO, RHOE_U, RHOE_V, KE, P, T, RHOE, local_val, jac
    real(rfreal) :: XI_X, XI_Y, PC(4), MI2, MMIN2, MU2, SNDSPD2, SNDSPD, MP2, EPS_P, RHO_PP, H0, H_T, H_P, LU, time_fac, MU, KAPPA
    real(rfreal) :: XI_X_l0, XI_Y_l0, MU_l0, JAC_l0, UX_l0, UY_l0, KAPPA_l0, RHO_l0, LAMBDA_l0
    real(rfreal) :: XI_X_l1, XI_Y_l1, MU_l1, JAC_l1, UX_l1, UY_l1, KAPPA_l1, LAMBDA_l1
    
    real(rfreal), pointer :: gamAux(:),pAux(:),TAux(:),rhoAux(:),rhouAux(:),rhovAux(:),rhoEAux(:),JACAux(:)
    real(rfreal), pointer :: XI_X_TILDEAux(:),XI_Y_TILDEAux(:), muAux(:), kappaAux(:), lambdaAux(:)


    ! ... simplicity
    grid  => region%grid(ng)
    state => region%state(ng)
    input => grid%input

    ! ... initialize arrays
    Do i = 1, grid%nCells*stencil_size
      values_r(i)  = 0.0_8
      values_ru(i) = 0.0_8
      values_rv(i) = 0.0_8
      values_rE(i) = 0.0_8
    End Do

    ! ... x-direction, first derivatives
    dir   = 1
    order = 1
    opID  = input%iFirstDerivImplicit

    ! ... local partition size
    N1 = grid%ie(1)-grid%is(1)+1
    N2 = grid%ie(2)-grid%is(2)+1
    N3 = grid%ie(3)-grid%is(3)+1

    ! ... partition size including ghost
    N1g = grid%ie(1)-grid%is(1)+1+SUM(grid%nGhostRHS(dir,:)) ! (MIN(grid%nGhostRHS(dir,1),1)+MIN(grid%nGhostRHS(dir,2),1))*input%overlap(opID)
    N2g = grid%ie(2)-grid%is(2)+1
    N3g = grid%ie(3)-grid%is(3)+1

    ! ... get ghost cell data
    Nc = grid%nCells
    Ncg = Nc + SUM(grid%nGhostRHS(dir,:))*grid%vdsGhost(dir)

    ! ... allocate a lot of arrays
    allocate(gamAux(Ncg),pAux(Ncg),TAux(Ncg),rhoAux(Ncg),rhouAux(Ncg),rhovAux(Ncg),rhoEAux(Ncg))
    allocate(JACAux(Ncg),XI_X_TILDEAux(Ncg),XI_Y_TILDEAux(Ncg))

    ! ... initialize auxiliary vectors
    gamAux(:) = 0.0_rfreal
    pAux(:) = 0.0_rfreal
    TAux(:) = 0.0_rfreal
    rhoAux(:) = 0.0_rfreal
    rhouAux(:) = 0.0_rfreal
    rhovAux(:) = 0.0_rfreal
    rhoEAux(:) = 0.0_rfreal
    JACAux(:) = 0.0_rfreal
    XI_X_TILDEAux(:) = 0.0_rfreal
    XI_Y_TILDEAux(:) = 0.0_rfreal



!    call mpi_barrier(mycomm,ierr)
!    if(region%myrank == 0) write(*,*) 'All are populating auxiliary matrices, order,dir = ',order,dir
    ! ... populate auxiliary vectors with local data
    Do i = 1, Nc
       ! ... gamma
       gamAux(grid%cell2ghost(i,dir)) = state%gv(i,1)
       ! ... pressure
       pAux(grid%cell2ghost(i,dir)) = state%dv(i,1)
       ! ... temperature
       TAux(grid%cell2ghost(i,dir)) = state%dv(i,2)
       ! ... density
       rhoAux(grid%cell2ghost(i,dir)) = state%cv(i,1)
       ! ... x-momentum
       rhouAux(grid%cell2ghost(i,dir)) = state%cv(i,2)
       ! ... y-momentum
       rhovAux(grid%cell2ghost(i,dir)) = state%cv(i,3)
       ! ... energy
       rhoEAux(grid%cell2ghost(i,dir)) = state%cv(i,4)
       ! ... Jacobian
       JACAux(grid%cell2ghost(i,dir)) = grid%JAC(i)
       ! ... XI_X_TILDE
       XI_X_TILDEAux(grid%cell2ghost(i,dir)) = grid%MT1(i, region%global%t2Map(dir,1))
       ! ... XI_Y_TILDE
       XI_Y_TILDEAux(grid%cell2ghost(i,dir)) = grid%MT1(i, region%global%t2Map(dir,2))
    End Do

    call mpi_barrier(mycomm,ierr)
!   if(region%myrank == 0) write(*,*) 'All are starting Ghost_Cell_Exchange, order,dir = ',order,dir

     ! do i = 1,Ncg
     !    if(region%myrank == 1) write(*,*) 'i,gamAux_2',i,gamAux(i),gamAuxAux(i)
     ! end do
    ! ... get ghost data
    call Ghost_Cell_Exchange(region,ng,dir, gamAux)
    call Ghost_Cell_Exchange(region,ng,dir,   pAux)
    call Ghost_Cell_Exchange(region,ng,dir,   TAux)
    call Ghost_Cell_Exchange(region,ng,dir, rhoAux)
    call Ghost_Cell_Exchange(region,ng,dir,rhouAux)
    call Ghost_Cell_Exchange(region,ng,dir,rhovAux)
    call Ghost_Cell_Exchange(region,ng,dir,rhoEAux)
    call Ghost_Cell_Exchange(region,ng,dir, JACAux)
    call Ghost_Cell_Exchange(region,ng,dir,XI_X_TILDEAux)
    call Ghost_Cell_Exchange(region,ng,dir,XI_Y_TILDEAux)

!    call mpi_barrier(mycomm,ierr)
!    if(region%myrank == 0) write(*,*) 'All are done with Ghost_Cell_Exchange, order,dir = ',order,dir
    ! ... done getting ghost data

     ! do i = 1,Ncg
     !    if(region%myrank == 1) write(*,*) 'i,gamAux_3',i,gamAux(i),gamAuxAux(i)
     ! end do

!    call graceful_exit(region%myrank, 'Check the values')

    SELECT CASE (lhs_var)
    CASE (1)

      ! ... A_p {delta_\xi} where
      !
      !       | u rho_p              rho                            0              u rho_T     |
      ! A_p = | rho_p u^2 + 1        2 rho u                        0              u^2 rho_T   |
      !       | rho_p u v            rho v                          rho u          u v rho_T   |
      !     = | [(rho E)_p + 1] u    [(rho E + p) + u (rho E)_u]    u (rho E)_v    u (rho E)_T |
      !
      !
      ! ... inviscid fluxes
      do k = 1, grid%vdsGhost(dir)
        do j = 1, grid%B(opID,dir)%n
          do i = grid%B(opID,dir)%row_ptr(k,j), grid%B(opID,dir)%row_ptr(k,j+1)-1

            ! ... l0 and l1 indices refer to BUFFERED data
            local_val = grid%B(opID,dir)%val(i,k)
            local_ind = grid%B(opID,dir)%col_ind(i,k)
            l0        = grid%vec_indGhost(k,local_ind,dir)
            l1        = grid%vec_indGhost(k,        j,dir)

            ! ... convert from linear to triplet indices
            ! ... rhs index (buffered data)
            ii0 = mod(l0-1,N1g)+1
            jj0 = mod((l0-ii0)/N1g,N2g)+1
            kk0 = (l0-ii0-(jj0-1)*N1g)/(N1g*N2g)+1

            ! ... lhs index (buffered data)
            ii1 = mod(l1-1,N1g)+1
            jj1 = mod((l1-ii1)/N1g,N2g)+1
            kk1 = (l1-ii1-(jj1-1)*N1g)/(N1g*N2g)+1

            ! ... convert to global indices
            ii0 = ii0 + grid%is(1) - 1 - grid%nGhostRHS(dir,1)
            jj0 = jj0 + grid%is(2) - 1
            kk0 = kk0 + grid%is(3) - 1
            ii1 = ii1 + grid%is(1) - 1 - grid%nGhostRHS(dir,1)
            jj1 = jj1 + grid%is(2) - 1
            kk1 = kk1 + grid%is(3) - 1

            if (ii1 < grid%is(1) .OR. ii1 > grid%ie(1)) CYCLE

            ! ... convert l1 back to non-buffered counting
            l1 = (kk1-grid%is(3))*N1*N2 + (jj1-grid%is(2))*N1 + (ii1-grid%is(1)+1)
            

!            ! ... intermediate values
!            gam     = state%gv(l1,1)
!            gamm1i  = 1.0_8 / (gam - 1.0_8)
!             P       = state%dv(l1,1)
!             T       = state%dv(l1,2)
!             RHO_P   = gam * gamm1i / T
!             RHO_T   = -P / T * RHO_P
!             UX      = state%cv(l1,2) * state%dv(l1,3)
!             UY      = state%cv(l1,3) * state%dv(l1,3)
!             RHO     = state%cv(l1,1)
!             RHOE_U  = UX * RHO
!             RHOE_V  = UY * RHO
!             KE      = 0.5_8 * (UX * UX + UY * UY)
!             RHOE_P  = gamm1i + KE * RHO_P
!             RHOE_T  = KE * RHO_T
!             JAC     = grid%JAC(l1)
!             XI_X    = grid%MT1(l1,region%global%t2Map(1,1)) * JAC
!             XI_Y    = grid%MT1(l1,region%global%t2Map(1,2)) * JAC


            ! ... intermediate values at l0
            gam     = gamAux(l0)
            gamm1i  = 1.0_8 / (gam - 1.0_8)
            P       = pAux(l0)
            T       = TAux(l0)
            RHO_P   = gam * gamm1i / T
            RHO_T   = -P / T * RHO_P
            RHO     = rhoAux(l0)
            UX      = rhouAux(l0) / RHO
            UY      = rhovAux(l0) / RHO
            RHOE_U  = UX * RHO
            RHOE_V  = UY * RHO
            KE      = 0.5_8 * (UX * UX + UY * UY)
            RHOE_P  = gamm1i + KE * RHO_P
            RHOE_T  = KE * RHO_T
            JAC     = JACAux(l0)
            XI_X    = XI_X_TILDEAux(l0) * JAC
            XI_Y    = XI_Y_TILDEAux(l0) * JAC


!if (region%myrank == 0 .and. ii1 .eq. 61 ) print*,'myrank,ii0,jj0,local_val,gam',region%myrank,ii0,jj0,XI_X,XI_Y_TILDEAux(l0),XI_Y,JAC

            ! ... index
            index = (l1-1)*stencil_size + input%HYPREStencilIndex(opID, ii0-ii1, jj0-jj1, kk0-kk1) + 1


            ! ... store in array
             values_r(index) =  values_r(index) + local_val * ( RHO_P * (UX * XI_X + UY * XI_Y) )
            values_ru(index) = values_ru(index) + local_val * ( RHO * XI_X )
            values_rv(index) = values_rv(index) + local_val * ( RHO * XI_Y )
            values_rE(index) = values_rE(index) + local_val * ( RHO_T * (UX * XI_X + UY * XI_Y) )

!if (region%myrank == 0 .and. ii1 .eq. 61 ) print*,'myrank,ii0,jj0,l1,values_r,index',region%myrank,ii0,jj0,l1,values_r(index),index
!            if (l1 == 1) write(*,*)' l1, l0,ii0,ii1,jj0,jj1,index,stencil_size,input%HYPREStencilIndex(opID, ii0-ii1, jj0-jj1, kk0-kk1),values_r(index)',l1, l0,ii0,ii1,jj0,jj1,index,stencil_size,input%HYPREStencilIndex(opID, ii0-ii1, jj0-jj1, kk0-kk1), values_r(index)

            ! dF(l1)    = dF(l1) + local_val * F(l0)
          end do
        end do
      end do
!call graceful_exit(region%myrank, 'stop')
    CASE (2)

      do k = 1, grid%vdsGhost(dir)
        do j = 1, grid%B(opID,dir)%n
          do i = grid%B(opID,dir)%row_ptr(k,j), grid%B(opID,dir)%row_ptr(k,j+1)-1

            ! ... l0 and l1 indices refer to BUFFERED data
            local_val = grid%B(opID,dir)%val(i,k)
            local_ind = grid%B(opID,dir)%col_ind(i,k)
            l0        = grid%vec_indGhost(k,local_ind,dir)
            l1        = grid%vec_indGhost(k,        j,dir)

            ! ... convert from linear to triplet indices
            ! ... rhs index (buffered data)
            ii0 = mod(l0-1,N1g)+1
            jj0 = mod((l0-ii0)/N1g,N2g)+1
            kk0 = (l0-ii0-(jj0-1)*N1g)/(N1g*N2g)+1

            ! ... lhs index (buffered data)
            ii1 = mod(l1-1,N1g)+1
            jj1 = mod((l1-ii1)/N1g,N2g)+1
            kk1 = (l1-ii1-(jj1-1)*N1g)/(N1g*N2g)+1

            ! ... convert to global indices
            ii0 = ii0 + grid%is(1) - 1 - grid%nGhostRHS(dir,1)
            jj0 = jj0 + grid%is(2) - 1
            kk0 = kk0 + grid%is(3) - 1
            ii1 = ii1 + grid%is(1) - 1 - grid%nGhostRHS(dir,1)
            jj1 = jj1 + grid%is(2) - 1
            kk1 = kk1 + grid%is(3) - 1

            if (ii1 < grid%is(1) .OR. ii1 > grid%ie(1)) CYCLE

            ! ... convert l1 back to non-buffered counting
            l1 = (kk1-grid%is(3))*N1*N2 + (jj1-grid%is(2))*N1 + (ii1-grid%is(1)+1)


            ! ... intermediate values
            gam     = gamAux(l0)
            gamm1i  = 1.0_8 / (gam - 1.0_8)
            P       = pAux(l0)
            T       = TAux(l0)
            RHO_P   = gam * gamm1i / T
            RHO_T   = -P / T * RHO_P
            RHO     = rhoAux(l0)
            UX      = rhouAux(l0) / RHO
            UY      = rhovAux(l0) / RHO
            RHOE_U  = UX * RHO
            RHOE_V  = UY * RHO
            KE      = 0.5_8 * (UX * UX + UY * UY)
            RHOE_P  = gamm1i + KE * RHO_P
            RHOE_T  = KE * RHO_T
            JAC     = JACAux(l0)
            XI_X    = XI_X_TILDEAux(l0) * JAC
            XI_Y    = XI_Y_TILDEAux(l0) * JAC

!             ! ... intermediate values
!             gam     = state%gv(l1,1)
!             gamm1i  = 1.0_8 / (gam - 1.0_8)
!             P       = state%dv(l1,1)
!             T       = state%dv(l1,2)
!             RHO_P   = gam * gamm1i / T
!             RHO_T   = -P / T * RHO_P
!             UX      = state%cv(l1,2) * state%dv(l1,3)
!             UY      = state%cv(l1,3) * state%dv(l1,3)
!             RHO     = state%cv(l1,1)
!             RHOE_U  = UX * RHO
!             RHOE_V  = UY * RHO
!             KE      = 0.5_8 * (UX * UX + UY * UY)
!             RHOE_P  = gamm1i + KE * RHO_P
!             RHOE_T  = KE * RHO_T
!             JAC     = grid%JAC(l1)
!             XI_X    = grid%MT1(l1,region%global%t2Map(1,1)) * JAC
!             XI_Y    = grid%MT1(l1,region%global%t2Map(1,2)) * JAC

            ! ... index
            index = (l1-1)*stencil_size + input%HYPREStencilIndex(opID, ii0-ii1, jj0-jj1, kk0-kk1) + 1
!            if (l1 == 1) write(*,*)' l1, l0,ii0,ii1,jj0,jj1,index,stencil_size,input%HYPREStencilIndex(opID, ii0-ii1, jj0-jj1, kk0-kk1)',l1, l0,ii0,ii1,jj0,jj1,index,stencil_size,input%HYPREStencilIndex(opID, ii0-ii1, jj0-jj1, kk0-kk1)
            ! ... store in array
            values_r (index) = values_r (index) + local_val * ( RHO_P * UX * ( UX * XI_X + UY * XI_Y ) + XI_X )
            values_ru(index) = values_ru(index) + local_val * ( RHO * ( 2.0_8 * UX * XI_X + UY * XI_Y ) )
            values_rv(index) = values_rv(index) + local_val * ( RHO * UX * XI_Y )
            values_rE(index) = values_rE(index) + local_val * ( RHO_T * UX * ( UX * XI_X + UY * XI_Y ) )

            ! dF(l1)    = dF(l1) + local_val * F(l0)
          end do
        end do
      end do

    CASE (3)

      do k = 1, grid%vdsGhost(dir)
        do j = 1, grid%B(opID,dir)%n
          do i = grid%B(opID,dir)%row_ptr(k,j), grid%B(opID,dir)%row_ptr(k,j+1)-1

            ! ... l0 and l1 indices refer to BUFFERED data
            local_val = grid%B(opID,dir)%val(i,k)
            local_ind = grid%B(opID,dir)%col_ind(i,k)
            l0        = grid%vec_indGhost(k,local_ind,dir)
            l1        = grid%vec_indGhost(k,        j,dir)

            ! ... convert from linear to triplet indices
            ! ... rhs index (buffered data)
            ii0 = mod(l0-1,N1g)+1
            jj0 = mod((l0-ii0)/N1g,N2g)+1
            kk0 = (l0-ii0-(jj0-1)*N1g)/(N1g*N2g)+1

            ! ... lhs index (buffered data)
            ii1 = mod(l1-1,N1g)+1
            jj1 = mod((l1-ii1)/N1g,N2g)+1
            kk1 = (l1-ii1-(jj1-1)*N1g)/(N1g*N2g)+1

            ! ... convert to global indices
            ii0 = ii0 + grid%is(1) - 1 - grid%nGhostRHS(dir,1)
            jj0 = jj0 + grid%is(2) - 1
            kk0 = kk0 + grid%is(3) - 1
            ii1 = ii1 + grid%is(1) - 1 - grid%nGhostRHS(dir,1)
            jj1 = jj1 + grid%is(2) - 1
            kk1 = kk1 + grid%is(3) - 1

            if (ii1 < grid%is(1) .OR. ii1 > grid%ie(1)) CYCLE

            ! ... convert l1 back to non-buffered counting
            l1 = (kk1-grid%is(3))*N1*N2 + (jj1-grid%is(2))*N1 + (ii1-grid%is(1)+1)

            ! ... intermediate values
            gam     = gamAux(l0)
            gamm1i  = 1.0_8 / (gam - 1.0_8)
            P       = pAux(l0)
            T       = TAux(l0)
            RHO_P   = gam * gamm1i / T
            RHO_T   = -P / T * RHO_P
            RHO     = rhoAux(l0)
            UX      = rhouAux(l0) / RHO
            UY      = rhovAux(l0) / RHO
            RHOE_U  = UX * RHO
            RHOE_V  = UY * RHO
            KE      = 0.5_8 * (UX * UX + UY * UY)
            RHOE_P  = gamm1i + KE * RHO_P
            RHOE_T  = KE * RHO_T
            JAC     = JACAux(l0)
            XI_X    = XI_X_TILDEAux(l0) * JAC
            XI_Y    = XI_Y_TILDEAux(l0) * JAC


!             ! ... intermediate values
!             gam     = state%gv(l1,1)
!             gamm1i  = 1.0_8 / (gam - 1.0_8)
!             P       = state%dv(l1,1)
!             T       = state%dv(l1,2)
!             RHO_P   = gam * gamm1i / T
!             RHO_T   = -P / T * RHO_P
!             UX      = state%cv(l1,2) * state%dv(l1,3)
!             UY      = state%cv(l1,3) * state%dv(l1,3)
!             RHO     = state%cv(l1,1)
!             RHOE_U  = UX * RHO
!             RHOE_V  = UY * RHO
!             KE      = 0.5_8 * (UX * UX + UY * UY)
!             RHOE_P  = gamm1i + KE * RHO_P
!             RHOE_T  = KE * RHO_T
!             JAC     = grid%JAC(l1)
!             XI_X    = grid%MT1(l1,region%global%t2Map(1,1)) * JAC
!             XI_Y    = grid%MT1(l1,region%global%t2Map(1,2)) * JAC

            ! ... index
            index = (l1-1)*stencil_size + input%HYPREStencilIndex(opID, ii0-ii1, jj0-jj1, kk0-kk1) + 1
!            if (ii1 > 177 .and. ii1<180) write(*,*)' l1, l0,ii0,ii1,jj0,jj1,index,stencil_size,input%HYPREStencilIndex(opID, ii0-ii1, jj0-jj1, kk0-kk1)',l1, l0,ii0,ii1,jj0,jj1,index,stencil_size,input%HYPREStencilIndex(opID, ii0-ii1, jj0-jj1, kk0-kk1),local_val
            ! ... store in array
            values_r (index) = values_r (index) + local_val * ( RHO_P * UY * ( UX * XI_X + UY * XI_Y ) + XI_Y )
            values_ru(index) = values_ru(index) + local_val * ( RHO * UY * XI_X )
            values_rv(index) = values_rv(index) + local_val * ( RHO * ( UX * XI_X + 2.0_8 * UY * XI_Y ) )
            values_rE(index) = values_rE(index) + local_val * ( RHO_T * UY * ( UX * XI_X + UY * XI_Y ) )
!if (region%myrank == 1) then
!           if(index == 1124 .or. index == 1284) print*,'xdir,index,values_r(index), RHO, RHO_P, UX,UY, XI_X,XI_Y',index,values_r(index), RHO, RHO_P, UX,UY, XI_X,XI_Y
!endif
            ! dF(l1)    = dF(l1) + local_val * F(l0)
          end do
        end do
      end do

    CASE (4)

      do k = 1, grid%vdsGhost(dir)
        do j = 1, grid%B(opID,dir)%n
          do i = grid%B(opID,dir)%row_ptr(k,j), grid%B(opID,dir)%row_ptr(k,j+1)-1

            ! ... l0 and l1 indices refer to BUFFERED data
            local_val = grid%B(opID,dir)%val(i,k)
            local_ind = grid%B(opID,dir)%col_ind(i,k)
            l0        = grid%vec_indGhost(k,local_ind,dir)
            l1        = grid%vec_indGhost(k,        j,dir)

            ! ... convert from linear to triplet indices
            ! ... rhs index (buffered data)
            ii0 = mod(l0-1,N1g)+1
            jj0 = mod((l0-ii0)/N1g,N2g)+1
            kk0 = (l0-ii0-(jj0-1)*N1g)/(N1g*N2g)+1

            ! ... lhs index (buffered data)
            ii1 = mod(l1-1,N1g)+1
            jj1 = mod((l1-ii1)/N1g,N2g)+1
            kk1 = (l1-ii1-(jj1-1)*N1g)/(N1g*N2g)+1

            ! ... convert to global indices
            ii0 = ii0 + grid%is(1) - 1 - grid%nGhostRHS(dir,1)
            jj0 = jj0 + grid%is(2) - 1
            kk0 = kk0 + grid%is(3) - 1
            ii1 = ii1 + grid%is(1) - 1 - grid%nGhostRHS(dir,1)
            jj1 = jj1 + grid%is(2) - 1
            kk1 = kk1 + grid%is(3) - 1

            if (ii1 < grid%is(1) .OR. ii1 > grid%ie(1)) CYCLE

            ! ... convert l1 back to non-buffered counting
            l1 = (kk1-grid%is(3))*N1*N2 + (jj1-grid%is(2))*N1 + (ii1-grid%is(1)+1)


            ! ... intermediate values
            gam     = gamAux(l0)
            gamm1i  = 1.0_8 / (gam - 1.0_8)
            P       = pAux(l0)
            T       = TAux(l0)
            RHO_P   = gam * gamm1i / T
            RHO_T   = -P / T * RHO_P
            RHO     = rhoAux(l0)
            UX      = rhouAux(l0) / RHO
            UY      = rhovAux(l0) / RHO
            RHOE_U  = UX * RHO
            RHOE_V  = UY * RHO
            KE      = 0.5_8 * (UX * UX + UY * UY)
            RHOE_P  = gamm1i + KE * RHO_P
            RHOE_T  = KE * RHO_T
            RHOE    = rhoEAux(l0)
            JAC     = JACAux(l0)
            XI_X    = XI_X_TILDEAux(l0) * JAC
            XI_Y    = XI_Y_TILDEAux(l0) * JAC

!             ! ... intermediate values
!             gam     = state%gv(l1,1)
!             gamm1i  = 1.0_8 / (gam - 1.0_8)
!             P       = state%dv(l1,1)
!             T       = state%dv(l1,2)
!             RHO_P   = gam * gamm1i / T
!             RHO_T   = -P / T * RHO_P
!             UX      = state%cv(l1,2) * state%dv(l1,3)
!             UY      = state%cv(l1,3) * state%dv(l1,3)
!             RHO     = state%cv(l1,1)
!             RHOE_U  = UX * RHO
!             RHOE_V  = UY * RHO
!             KE      = 0.5_8 * (UX * UX + UY * UY)
!             RHOE_P  = gamm1i + KE * RHO_P
!             RHOE_T  = KE * RHO_T
!             RHOE    = state%cv(l1,4)
!             JAC     = grid%JAC(l1)
!             XI_X    = grid%MT1(l1,region%global%t2Map(1,1)) * JAC
!             XI_Y    = grid%MT1(l1,region%global%t2Map(1,2)) * JAC

            ! ... index
            index = (l1-1)*stencil_size + input%HYPREStencilIndex(opID, ii0-ii1, jj0-jj1, kk0-kk1) + 1

            ! ... store in array
            values_r (index) = values_r (index) + local_val * ( ( RHOE_P + 1.0_8 ) * ( UX * XI_X + UY * XI_Y ) )
            values_ru(index) = values_ru(index) + local_val * ( ( RHOE + P ) * XI_X + RHOE_U * ( UX * XI_X + UY * XI_Y ) )
            values_rv(index) = values_rv(index) + local_val * ( ( RHOE + P ) * XI_Y + RHOE_V * ( UX * XI_X + UY * XI_Y ) )
            values_rE(index) = values_rE(index) + local_val * ( RHOE_T * ( UX * XI_X + UY * XI_Y ) )

!            if (l1 == 1) write(*,*)' l1, l0,ii0,ii1,jj0,jj1,index,stencil_size,input%HYPREStencilIndex(opID, ii0-ii1, jj0-jj1, kk0-kk1)',l1, l0,ii0,ii1,jj0,jj1,index,stencil_size,input%HYPREStencilIndex(opID, ii0-ii1, jj0-jj1, kk0-kk1)
            ! dF(l1)    = dF(l1) + local_val * F(l0)
          end do
        end do
      end do

    END SELECT


    ! ... deallocate auxiliary matrices for this direction
    deallocate(gamAux,pAux,TAux,rhoAux,rhouAux,rhovAux,rhoEAux,JACAux,XI_X_TILDEAux,XI_Y_TILDEAux)

    ! ... y-direction, first derivatives
    dir   = 2
    order = 1
    opID  = input%iFirstDerivImplicit

    ! ... local partition size
    N1 = grid%ie(1)-grid%is(1)+1
    N2 = grid%ie(2)-grid%is(2)+1
    N3 = grid%ie(3)-grid%is(3)+1

    ! ... partition size including ghost
    N1g = grid%ie(1)-grid%is(1)+1
    N2g = grid%ie(2)-grid%is(2)+1+SUM(grid%nGhostRHS(dir,:))
    N3g = grid%ie(3)-grid%is(3)+1

    ! ... get ghost cell data
    Nc = grid%nCells
    Ncg = Nc + SUM(grid%nGhostRHS(dir,:))*grid%vdsGhost(dir)

    ! ... allocate a lot of arrays
    allocate(gamAux(Ncg),pAux(Ncg),TAux(Ncg),rhoAux(Ncg),rhouAux(Ncg),rhovAux(Ncg),rhoEAux(Ncg))
    allocate(JACAux(Ncg),XI_X_TILDEAux(Ncg),XI_Y_TILDEAux(Ncg))

    ! ... initialize auxiliary vectors
    gamAux(:) = 0.0_rfreal
    pAux(:) = 0.0_rfreal
    TAux(:) = 0.0_rfreal
    rhoAux(:) = 0.0_rfreal
    rhouAux(:) = 0.0_rfreal
    rhovAux(:) = 0.0_rfreal
    rhoEAux(:) = 0.0_rfreal
    JACAux(:) = 0.0_rfreal
    XI_X_TILDEAux(:) = 0.0_rfreal
    XI_Y_TILDEAux(:) = 0.0_rfreal

!    call mpi_barrier(mycomm,ierr)
!    if(region%myrank == 0) write(*,*) 'All are populating auxiliary matrices, order,dir = ',order,dir
    ! ... populate auxiliary vectors with local data
    Do i = 1, Nc
       ! ... gamma
       gamAux(grid%cell2ghost(i,dir)) = state%gv(i,1)
       ! ... pressure
       pAux(grid%cell2ghost(i,dir)) = state%dv(i,1)
       ! ... temperature
       TAux(grid%cell2ghost(i,dir)) = state%dv(i,2)
       ! ... density
       rhoAux(grid%cell2ghost(i,dir)) = state%cv(i,1)
       ! ... x-momentum
       rhouAux(grid%cell2ghost(i,dir)) = state%cv(i,2)
       ! ... y-momentum
       rhovAux(grid%cell2ghost(i,dir)) = state%cv(i,3)
       ! ... energy
       rhoEAux(grid%cell2ghost(i,dir)) = state%cv(i,4)
       ! ... Jacobian
       JACAux(grid%cell2ghost(i,dir)) = grid%JAC(i)
       ! ... XI_X_TILDE
       XI_X_TILDEAux(grid%cell2ghost(i,dir)) = grid%MT1(i, region%global%t2Map(dir,1))
       ! ... XI_Y_TILDE
       XI_Y_TILDEAux(grid%cell2ghost(i,dir)) = grid%MT1(i, region%global%t2Map(dir,2))
    End Do

!    call mpi_barrier(mycomm,ierr)
!    if(region%myrank == 0) write(*,*) 'All are starting Ghost_Cell_Exchange, order,dir = ',order,dir
    ! ... get ghost data
    call Ghost_Cell_Exchange(region,ng,dir, gamAux)
    call Ghost_Cell_Exchange(region,ng,dir,   pAux)
    call Ghost_Cell_Exchange(region,ng,dir,   TAux)
    call Ghost_Cell_Exchange(region,ng,dir, rhoAux)
    call Ghost_Cell_Exchange(region,ng,dir,rhouAux)
    call Ghost_Cell_Exchange(region,ng,dir,rhovAux)
    call Ghost_Cell_Exchange(region,ng,dir,rhoEAux)
    call Ghost_Cell_Exchange(region,ng,dir, JACAux)
    call Ghost_Cell_Exchange(region,ng,dir,XI_X_TILDEAux)
    call Ghost_Cell_Exchange(region,ng,dir,XI_Y_TILDEAux)

!    call mpi_barrier(mycomm,ierr)
!    if(region%myrank == 0) write(*,*) 'All are done with Ghost_Cell_Exchange, order,dir = ',order,dir

    SELECT CASE (lhs_var)
    CASE (1)

      ! ... B_p {delta_\eta} where
      !
      !       | u rho_p              rho                            0              u rho_T     |
      ! B_p = | rho_p u^2 + 1        2 rho u                        0              u^2 rho_T   |
      !       | rho_p u v            rho v                          rho u          u v rho_T   |
      !     = | [(rho E)_p + 1] u    [(rho E + p) + u (rho E)_u]    u (rho E)_v    u (rho E)_T |
      !
      !
      ! ... inviscid fluxes
      do k = 1, grid%vdsGhost(dir)
        do j = 1, grid%B(opID,dir)%n
          do i = grid%B(opID,dir)%row_ptr(k,j), grid%B(opID,dir)%row_ptr(k,j+1)-1

            ! ... l0 and l1 indices refer to BUFFERED data
            local_val = grid%B(opID,dir)%val(i,k)
            local_ind = grid%B(opID,dir)%col_ind(i,k)
            l0        = grid%vec_indGhost(k,local_ind,dir)
            l1        = grid%vec_indGhost(k,        j,dir)

            ! ... convert from linear to triplet indices
            ! ... rhs index (buffered data)
            ii0 = mod(l0-1,N1g)+1
            jj0 = mod((l0-ii0)/N1g,N2g)+1
            kk0 = (l0-ii0-(jj0-1)*N1g)/(N1g*N2g)+1

            ! ... lhs index (buffered data)
            ii1 = mod(l1-1,N1g)+1
            jj1 = mod((l1-ii1)/N1g,N2g)+1
            kk1 = (l1-ii1-(jj1-1)*N1g)/(N1g*N2g)+1

            ! ... convert to global indices
            ii0 = ii0 + grid%is(1) - 1
            jj0 = jj0 + grid%is(2) - 1 - grid%nGhostRHS(dir,1)
            kk0 = kk0 + grid%is(3) - 1
            ii1 = ii1 + grid%is(1) - 1
            jj1 = jj1 + grid%is(2) - 1 - grid%nGhostRHS(dir,1)
            kk1 = kk1 + grid%is(3) - 1

            if (jj1 < grid%is(2) .OR. jj1 > grid%ie(2)) CYCLE

            ! ... convert l1 back to non-buffered counting
            l1 = (kk1-grid%is(3))*N1*N2 + (jj1-grid%is(2))*N1 + (ii1-grid%is(1)+1)

            ! ... intermediate values
            gam     = gamAux(l0)
            gamm1i  = 1.0_8 / (gam - 1.0_8)
            P       = pAux(l0)
            T       = TAux(l0)
            RHO_P   = gam * gamm1i / T
            RHO_T   = -P / T * RHO_P
            RHO     = rhoAux(l0)
            UX      = rhouAux(l0) / RHO
            UY      = rhovAux(l0) / RHO
            RHOE_U  = UX
            RHOE_V  = UY
            KE      = 0.5_8 * (UX * UX + UY * UY)
            RHOE_P  = gamm1i + KE * RHO_P
            RHOE_T  = KE * RHO_T
            JAC     = JACAux(l0)
            XI_X    = XI_X_TILDEAux(l0) * JAC
            XI_Y    = XI_Y_TILDEAux(l0) * JAC

!             ! ... interiate values
!             gam     = state%gv(l1,1)
!             gamm1i  = 1.0_8 / (gam - 1.0_8)
!             P       = state%dv(l1,1)
!             T       = state%dv(l1,2)
!             RHO_P   = gam * gamm1i / T
!             RHO_T   = -P / T * RHO_P
!             UX      = state%cv(l1,2) * state%dv(l1,3)
!             UY      = state%cv(l1,3) * state%dv(l1,3)
!             RHO     = state%cv(l1,1)
!             RHOE_U  = UX
!             RHOE_V  = UY
!             KE      = 0.5_8 * (UX * UX + UY * UY)
!             RHOE_P  = gamm1i + KE * RHO_P
!             RHOE_T  = KE * RHO_T
!             JAC     = grid%JAC(l1)
!             XI_X    = grid%MT1(l1,region%global%t2Map(2,1)) * JAC
!             XI_Y    = grid%MT1(l1,region%global%t2Map(2,2)) * JAC


            ! ... index
            index = (l1-1)*stencil_size + input%HYPREStencilIndex(opID, ii0-ii1, jj0-jj1, kk0-kk1) + 1

            ! ... store in array
            values_r (index) = values_r (index) + local_val * ( RHO_P * (UX * XI_X + UY * XI_Y) )
            values_ru(index) = values_ru(index) + local_val * ( RHO * XI_X )
            values_rv(index) = values_rv(index) + local_val * ( RHO * XI_Y )
            values_rE(index) = values_rE(index) + local_val * ( RHO_T * (UX * XI_X + UY * XI_Y) )

            ! dF(l1)    = dF(l1) + local_val * F(l0)
          end do
        end do
      end do

    CASE (2)

      do k = 1, grid%vdsGhost(dir)
        do j = 1, grid%B(opID,dir)%n
          do i = grid%B(opID,dir)%row_ptr(k,j), grid%B(opID,dir)%row_ptr(k,j+1)-1

            ! ... l0 and l1 indices refer to BUFFERED data
            local_val = grid%B(opID,dir)%val(i,k)
            local_ind = grid%B(opID,dir)%col_ind(i,k)
            l0        = grid%vec_indGhost(k,local_ind,dir)
            l1        = grid%vec_indGhost(k,        j,dir)

            ! ... convert from linear to triplet indices
            ! ... rhs index (buffered data)
            ii0 = mod(l0-1,N1g)+1
            jj0 = mod((l0-ii0)/N1g,N2g)+1
            kk0 = (l0-ii0-(jj0-1)*N1g)/(N1g*N2g)+1

            ! ... lhs index (buffered data)
            ii1 = mod(l1-1,N1g)+1
            jj1 = mod((l1-ii1)/N1g,N2g)+1
            kk1 = (l1-ii1-(jj1-1)*N1g)/(N1g*N2g)+1

            ! ... convert to global indices
            ii0 = ii0 + grid%is(1) - 1
            jj0 = jj0 + grid%is(2) - 1 - grid%nGhostRHS(dir,1)
            kk0 = kk0 + grid%is(3) - 1
            ii1 = ii1 + grid%is(1) - 1
            jj1 = jj1 + grid%is(2) - 1 - grid%nGhostRHS(dir,1)
            kk1 = kk1 + grid%is(3) - 1

            if (jj1 < grid%is(2) .OR. jj1 > grid%ie(2)) CYCLE

            ! ... convert l1 back to non-buffered counting
            l1 = (kk1-grid%is(3))*N1*N2 + (jj1-grid%is(2))*N1 + (ii1-grid%is(1)+1)
     
            ! ... intermediate values
            gam     = gamAux(l0)
            gamm1i  = 1.0_8 / (gam - 1.0_8)
            P       = pAux(l0)
            T       = TAux(l0)
            RHO_P   = gam * gamm1i / T
            RHO_T   = -P / T * RHO_P
            RHO     = rhoAux(l0)
            UX      = rhouAux(l0) / RHO
            UY      = rhovAux(l0) / RHO
            RHOE_U  = UX
            RHOE_V  = UY
            KE      = 0.5_8 * (UX * UX + UY * UY)
            RHOE_P  = gamm1i + KE * RHO_P
            RHOE_T  = KE * RHO_T
            JAC     = JACAux(l0)
            XI_X    = XI_X_TILDEAux(l0) * JAC
            XI_Y    = XI_Y_TILDEAux(l0) * JAC


!             ! ... intermediate values
!             gam     = state%gv(l1,1)
!             gamm1i  = 1.0_8 / (gam - 1.0_8)
!             P       = state%dv(l1,1)
!             T       = state%dv(l1,2)
!             RHO_P   = gam * gamm1i / T
!             RHO_T   = -P / T * RHO_P
!             UX      = state%cv(l1,2) * state%dv(l1,3)
!             UY      = state%cv(l1,3) * state%dv(l1,3)
!             RHO     = state%cv(l1,1)
!             RHOE_U  = UX
!             RHOE_V  = UY
!             KE      = 0.5_8 * (UX * UX + UY * UY)
!             RHOE_P  = gamm1i + KE * RHO_P
!             RHOE_T  = KE * RHO_T
!             JAC     = grid%JAC(l1)
!             XI_X    = grid%MT1(l1,region%global%t2Map(2,1)) * JAC
!             XI_Y    = grid%MT1(l1,region%global%t2Map(2,2)) * JAC

            ! ... index
            index = (l1-1)*stencil_size + input%HYPREStencilIndex(opID, ii0-ii1, jj0-jj1, kk0-kk1) + 1

            ! ... store in array
            values_r (index) = values_r (index) + local_val * ( RHO_P * UX * ( UX * XI_X + UY * XI_Y ) + XI_X )
            values_ru(index) = values_ru(index) + local_val * ( RHO * ( 2.0_8 * UX * XI_X + UY * XI_Y ) )
            values_rv(index) = values_rv(index) + local_val * ( RHO * UX * XI_Y )
            values_rE(index) = values_rE(index) + local_val * ( RHO_T * UX * ( UX * XI_X + UY * XI_Y ) )

            ! dF(l1)    = dF(l1) + local_val * F(l0)
          end do
        end do
      end do

    CASE (3)

      do k = 1, grid%vdsGhost(dir)
        do j = 1, grid%B(opID,dir)%n
          do i = grid%B(opID,dir)%row_ptr(k,j), grid%B(opID,dir)%row_ptr(k,j+1)-1

            ! ... l0 and l1 indices refer to BUFFERED data
            local_val = grid%B(opID,dir)%val(i,k)
            local_ind = grid%B(opID,dir)%col_ind(i,k)
            l0        = grid%vec_indGhost(k,local_ind,dir)
            l1        = grid%vec_indGhost(k,        j,dir)

            ! ... convert from linear to triplet indices
            ! ... rhs index (buffered data)
            ii0 = mod(l0-1,N1g)+1
            jj0 = mod((l0-ii0)/N1g,N2g)+1
            kk0 = (l0-ii0-(jj0-1)*N1g)/(N1g*N2g)+1

            ! ... lhs index (buffered data)
            ii1 = mod(l1-1,N1g)+1
            jj1 = mod((l1-ii1)/N1g,N2g)+1
            kk1 = (l1-ii1-(jj1-1)*N1g)/(N1g*N2g)+1

            ! ... convert to global indices
            ii0 = ii0 + grid%is(1) - 1
            jj0 = jj0 + grid%is(2) - 1 - grid%nGhostRHS(dir,1)
            kk0 = kk0 + grid%is(3) - 1
            ii1 = ii1 + grid%is(1) - 1
            jj1 = jj1 + grid%is(2) - 1 - grid%nGhostRHS(dir,1)
            kk1 = kk1 + grid%is(3) - 1

            if (jj1 < grid%is(2) .OR. jj1 > grid%ie(2)) CYCLE

            ! ... convert l1 back to non-buffered counting
            l1 = (kk1-grid%is(3))*N1*N2 + (jj1-grid%is(2))*N1 + (ii1-grid%is(1)+1)

            ! ... intermediate values
            gam     = gamAux(l0)
            gamm1i  = 1.0_8 / (gam - 1.0_8)
            P       = pAux(l0)
            T       = TAux(l0)
            RHO_P   = gam * gamm1i / T
            RHO_T   = -P / T * RHO_P
            RHO     = rhoAux(l0)
            UX      = rhouAux(l0) / RHO
            UY      = rhovAux(l0) / RHO
            RHOE_U  = UX
            RHOE_V  = UY
            KE      = 0.5_8 * (UX * UX + UY * UY)
            RHOE_P  = gamm1i + KE * RHO_P
            RHOE_T  = KE * RHO_T
            JAC     = JACAux(l0)
            XI_X    = XI_X_TILDEAux(l0) * JAC
            XI_Y    = XI_Y_TILDEAux(l0) * JAC

!             ! ... intermediate values
!             gam     = state%gv(l1,1)
!             gamm1i  = 1.0_8 / (gam - 1.0_8)
!             P       = state%dv(l1,1)
!             T       = state%dv(l1,2)
!             RHO_P   = gam * gamm1i / T
!             RHO_T   = -P / T * RHO_P
!             UX      = state%cv(l1,2) * state%dv(l1,3)
!             UY      = state%cv(l1,3) * state%dv(l1,3)
!             RHO     = state%cv(l1,1)
!             RHOE_U  = UX * RHO
!             RHOE_V  = UY * RHO
!             KE      = 0.5_8 * (UX * UX + UY * UY)
!             RHOE_P  = gamm1i + KE * RHO_P
!             RHOE_T  = KE * RHO_T
!             JAC     = grid%JAC(l1)
!             XI_X    = grid%MT1(l1,region%global%t2Map(2,1)) * JAC
!             XI_Y    = grid%MT1(l1,region%global%t2Map(2,2)) * JAC

            ! ... index
            index = (l1-1)*stencil_size + input%HYPREStencilIndex(opID, ii0-ii1, jj0-jj1, kk0-kk1) + 1

            ! ... store in array
            values_r (index) = values_r (index) + local_val * ( RHO_P * UY * ( UX * XI_X + UY * XI_Y ) + XI_Y )
            values_ru(index) = values_ru(index) + local_val * ( RHO * UY * XI_X )
            values_rv(index) = values_rv(index) + local_val * ( RHO * ( UX * XI_X + 2.0_8 * UY * XI_Y ) )
            values_rE(index) = values_rE(index) + local_val * ( RHO_T * UY * ( UX * XI_X + UY * XI_Y ) )
!if (region%myrank == 1) then
!            if(index == 1125 .or. index == 1284) print*,'ydir,ii1,jj1,ii0,jj0,index,values_r(index), RHO, RHO_P, UX,UY, XI_X,XI_Y,JAC,xyz',ii1,jj1,ii0,jj0,index,values_r(index), RHO, RHO_P, UX,UY, XI_X,XI_Y,JAC, grid%XYZ(l0,:)
!endif
            ! dF(l1)    = dF(l1) + local_val * F(l0)
          end do
        end do
      end do

    CASE (4)

      do k = 1, grid%vdsGhost(dir)
        do j = 1, grid%B(opID,dir)%n
          do i = grid%B(opID,dir)%row_ptr(k,j), grid%B(opID,dir)%row_ptr(k,j+1)-1

            ! ... l0 and l1 indices refer to BUFFERED data
            local_val = grid%B(opID,dir)%val(i,k)
            local_ind = grid%B(opID,dir)%col_ind(i,k)
            l0        = grid%vec_indGhost(k,local_ind,dir)
            l1        = grid%vec_indGhost(k,        j,dir)

            ! ... convert from linear to triplet indices
            ! ... rhs index (buffered data)
            ii0 = mod(l0-1,N1g)+1
            jj0 = mod((l0-ii0)/N1g,N2g)+1
            kk0 = (l0-ii0-(jj0-1)*N1g)/(N1g*N2g)+1

            ! ... lhs index (buffered data)
            ii1 = mod(l1-1,N1g)+1
            jj1 = mod((l1-ii1)/N1g,N2g)+1
            kk1 = (l1-ii1-(jj1-1)*N1g)/(N1g*N2g)+1

            ! ... convert to global indices
            ii0 = ii0 + grid%is(1) - 1
            jj0 = jj0 + grid%is(2) - 1 - grid%nGhostRHS(dir,1)
            kk0 = kk0 + grid%is(3) - 1
            ii1 = ii1 + grid%is(1) - 1
            jj1 = jj1 + grid%is(2) - 1 - grid%nGhostRHS(dir,1)
            kk1 = kk1 + grid%is(3) - 1

            if (jj1 < grid%is(2) .OR. jj1 > grid%ie(2)) CYCLE

            ! ... convert l1 back to non-buffered counting
            l1 = (kk1-grid%is(3))*N1*N2 + (jj1-grid%is(2))*N1 + (ii1-grid%is(1)+1)

            ! ... intermediate values
            gam     = gamAux(l0)
            gamm1i  = 1.0_8 / (gam - 1.0_8)
            P       = pAux(l0)
            T       = TAux(l0)
            RHO_P   = gam * gamm1i / T
            RHO_T   = -P / T * RHO_P
            RHO     = rhoAux(l0)
            UX      = rhouAux(l0) / RHO
            UY      = rhovAux(l0) / RHO
            RHOE_U  = UX * RHO
            RHOE_V  = UY * RHO
            KE      = 0.5_8 * (UX * UX + UY * UY)
            RHOE_P  = gamm1i + KE * RHO_P
            RHOE_T  = KE * RHO_T
            RHOE    = rhoEAux(l0)
            JAC     = JACAux(l0)
            XI_X    = XI_X_TILDEAux(l0) * JAC
            XI_Y    = XI_Y_TILDEAux(l0) * JAC

!             ! ... intermediate values
!             gam     = state%gv(l1,1)
!             gamm1i  = 1.0_8 / (gam - 1.0_8)
!             P       = state%dv(l1,1)
!             T       = state%dv(l1,2)
!             RHO_P   = gam * gamm1i / T
!             RHO_T   = -P / T * RHO_P
!             UX      = state%cv(l1,2) * state%dv(l1,3)
!             UY      = state%cv(l1,3) * state%dv(l1,3)
!             RHOE_U  = UX * RHO
!             RHOE_V  = UY * RHO
!             KE      = 0.5_8 * (UX * UX + UY * UY)
!             RHOE_P  = gamm1i + KE * RHO_P
!             RHOE_T  = KE * RHO_T
!             RHOE    = state%cv(l1,4)
!             JAC     = grid%JAC(l1)
!             XI_X    = grid%MT1(l1,region%global%t2Map(2,1)) * JAC
!             XI_Y    = grid%MT1(l1,region%global%t2Map(2,2)) * JAC

            ! ... index
            index = (l1-1)*stencil_size + input%HYPREStencilIndex(opID, ii0-ii1, jj0-jj1, kk0-kk1) + 1

            ! ... store in array
            values_r (index) = values_r (index) + local_val * ( ( RHOE_P + 1.0_8 ) * ( UX * XI_X + UY * XI_Y ) )
            values_ru(index) = values_ru(index) + local_val * ( ( RHOE + P ) * XI_X + RHOE_U * ( UX * XI_X + UY * XI_Y ) )
            values_rv(index) = values_rv(index) + local_val * ( ( RHOE + P ) * XI_Y + RHOE_V * ( UX * XI_X + UY * XI_Y ) )
            values_rE(index) = values_rE(index) + local_val * ( RHOE_T * ( UX * XI_X + UY * XI_Y ) )

            ! dF(l1)    = dF(l1) + local_val * F(l0)
          end do
        end do
      end do

    END SELECT

    ! ... deallocate auxiliary matrices for this direction
    deallocate(gamAux,pAux,TAux,rhoAux,rhouAux,rhovAux,rhoEAux,JACAux,XI_X_TILDEAux,XI_Y_TILDEAux)


    ! ... multiply by dtau
    do i = 1, grid%nCells*stencil_size
      values_r (i) = dtau * values_r (i)
      values_ru(i) = dtau * values_ru(i)
      values_rv(i) = dtau * values_rv(i)
      values_rE(i) = dtau * values_rE(i)
    end do

    ! ... no preconditioner
    PC(:)       = 0.0_8
    PC(lhs_var) = 1.0_8    

    ! ... precondintioner length
    LU = input%Lu

    ! ... fresstream Mach number
    MMIN2 = input%MachInf**2 * 3.0_8

    ! ... change time factor if we are on the first iteration
    time_fac = 1.5_dp
    if (region%global%main_ts_loop_index == 1) time_fac = 1.0_dp
    if (input%ImplicitSteadyState == TRUE) time_fac = 1.0_dp
    if (input%ImplicitTrueSteadyState == TRUE) time_fac = 0.0_dp

    ! ... only include preconditioner if steady-state
    ! ... the way to accomplish this should change in order to save on comp. time
    ! if (input%ImplicitSteadyState == TRUE) time_fac = 0.0_8
    ! ... the diagonal terms are the time dependent terms
    ! ... if steady-state, do not add the diagonal terms
    ! ... no case will be selected for lhs_var = 0
    ! if (input%ImplicitSteadyState == TRUE) then
    !    print*,'ImplicitSteadyState, lhs_var',input%ImplicitSteadyState, lhs_var
    !    lhs_var_copy = lhs_var
    !    lhs_var = 0
    !    print*,'lhs_var,lhs_var_copy',lhs_var,lhs_var_copy
    ! end if

    ! ... add the diagonal terms
    Select Case (lhs_var)

    Case (1)

      do i = 1, grid%nCells

        ! ... temporary values
        gam     = state%gv(i,1)
        gamm1i  = 1.0_8 / (gam - 1.0_8)
        P       = state%dv(i,1)
        T       = state%dv(i,2)
        RHO     = state%cv(i,1)
        RHO_P   = gam * gamm1i / T
        RHO_T   = -P / T * RHO_P
        UX      = state%cv(i,2) / RHO
        UY      = state%cv(i,3) / RHO

        ! ... preconditioner
        SNDSPD2 = GAM * P / RHO
        SNDSPD  = SQRT(SNDSPD2)
        MI2     = (UX*UX + UY*UY) / SNDSPD2
!        MU2     = (LU / (3.14_8 * state%dt(i) * SNDSPD * input%SndSpdRef)) * (LU / (3.14_8 * state%dt(i) * SNDSPD*input%SndSpdRef))
        MU2     = (LU / (3.14_8 * state%dt(i) * SNDSPD)) * (LU / (3.14_8 * state%dt(i) * SNDSPD))
        MP2     = MIN(MAX(MI2,MU2,MMIN2),1.0_8)
        EPS_P   = MP2 / (1.0_8 + (GAM - 1.0_8) * MP2)
        RHO_PP  = 1.0_8 / (EPS_P * SNDSPD2)
      PC(1)   = RHO_PP
      PC(2)   = 0.0_8
      PC(3)   = 0.0_8
      PC(4)   = RHO_T

        !        print*,(MAX(MI2,MU2,MMIN2)), MI2, MU2, MMIN2
        !        MP2 = MI2
        !       if(MP2 .lt. 1.0_8) then
        !          print*,'i,j,k,MP2',i,j,k,MP2
        !       end if
        ! ... index
        index = (i-1)*stencil_size + 1

         values_r(index) =  values_r(index) + PC(1) + time_fac * dtau / state%dt(i) * RHO_P
        values_ru(index) = values_ru(index) + PC(2)
        values_rv(index) = values_rv(index) + PC(3)
        values_rE(index) = values_rE(index) + PC(4) + time_fac * dtau / state%dt(i) * RHO_T

      end do

    Case (2)

      do i = 1, grid%nCells

        ! ... temporary values
        gam     = state%gv(i,1)
        gamm1i  = 1.0_8 / (gam - 1.0_8)
        RHO     = state%cv(i,1)
        P       = state%dv(i,1)
        T       = state%dv(i,2)
        RHO_P   = gam * gamm1i / T
        RHO_T   = -P / T * RHO_P
        UX      = state%cv(i,2) / RHO
        UY      = state%cv(i,3) / RHO

        ! ... preconditioner
        SNDSPD2 = GAM * P / RHO
        SNDSPD  = SQRT(SNDSPD2)
        MI2     = (UX*UX + UY*UY) / SNDSPD2
!        MU2     = (LU / (3.14_8 * state%dt(i) * SNDSPD * input%SndSpdRef)) * (LU / (3.14_8 * state%dt(i) * SNDSPD*input%SndSpdRef))
        MU2     = (LU / (3.14_8 * state%dt(i) * SNDSPD)) * (LU / (3.14_8 * state%dt(i) * SNDSPD))
        MP2     = MIN(MAX(MI2,MU2,MMIN2),1.0_8)
        EPS_P   = MP2 / (1.0_8 + (GAM - 1.0_8) * MP2)
        RHO_PP  = 1.0_8 / (EPS_P * SNDSPD2)
      PC(1)   = UX * RHO_PP
      PC(2)   = RHO
      PC(3)   = 0.0_8
      PC(4)   = UX * RHO_T

        ! ... index
        index = (i-1)*stencil_size + 1

         values_r(index) =  values_r(index) + PC(1) + time_fac * dtau / state%dt(i) * RHO_P * UX
        values_ru(index) = values_ru(index) + PC(2) + time_fac * dtau / state%dt(i) * RHO   
        values_rv(index) = values_rv(index) + PC(3)
        values_rE(index) = values_rE(index) + PC(4) + time_fac * dtau / state%dt(i) * RHO_T * UX

      end do

    Case (3)

      do i = 1, grid%nCells

        ! ... temporary values
        gam     = state%gv(i,1)
        gamm1i  = 1.0_8 / (gam - 1.0_8)
        RHO     = state%cv(i,1)
        P       = state%dv(i,1)
        T       = state%dv(i,2)
        RHO_P   = gam * gamm1i / T
        RHO_T   = -P / T * RHO_P
        UX      = state%cv(i,2) / RHO
        UY      = state%cv(i,3) / RHO

        ! ... preconditioner
        SNDSPD2 = GAM * P / RHO
        SNDSPD  = SQRT(SNDSPD2)
        MI2     = (UX*UX + UY*UY) / SNDSPD2
!        MU2     = (LU / (3.14_8 * state%dt(i) * SNDSPD * input%SndSpdRef)) * (LU / (3.14_8 * state%dt(i) * SNDSPD*input%SndSpdRef)) 
       MU2     = (LU / (3.14_8 * state%dt(i) * SNDSPD)) * (LU / (3.14_8 * state%dt(i) * SNDSPD))
        MP2     = MIN(MAX(MI2,MU2,MMIN2),1.0_8)
        EPS_P   = MP2 / (1.0_8 + (GAM - 1.0_8) * MP2)
        RHO_PP  = 1.0_8 / (EPS_P * SNDSPD2)
      PC(1)   = UY * RHO_PP
      PC(2)   = 0.0_8
      PC(3)   = RHO
      PC(4)   = UY * RHO_T

        ! ... index
        index = (i-1)*stencil_size + 1

         values_r(index) =  values_r(index) + PC(1) + time_fac * dtau / state%dt(i) * RHO_P * UY
        values_ru(index) = values_ru(index) + PC(2)
        values_rv(index) = values_rv(index) + PC(3) + time_fac * dtau / state%dt(i) * RHO
        values_rE(index) = values_rE(index) + PC(4) + time_fac * dtau / state%dt(i) * RHO_T * UY
if (region%myrank == 1) then
        if(index == 1124 .or. index == 1284) print*,'pc,index,values_r(index), RHO, RHO_P, UX,UY, XI_X,XI_Y',index,values_r(index), RHO, RHO_P, UX,UY, XI_X,XI_Y
endif
      end do

    Case (4)

      do i = 1, grid%nCells

        ! ... temporary values
        gam     = state%gv(i,1)
        gamm1i  = 1.0_8 / (gam - 1.0_8)
        RHO     = state%cv(i,1)
        P       = state%dv(i,1)
        T       = state%dv(i,2)
        RHO_P   = gam * gamm1i / T
        RHO_T   = -P / T * RHO_P
        UX      = state%cv(i,2) / RHO
        UY      = state%cv(i,3) / RHO
        KE      = 0.5 * (UX * UX + UY * UY)

        ! ... preconditioner
        SNDSPD2 = GAM * P / RHO
        SNDSPD  = SQRT(SNDSPD2)
        MI2     = (UX*UX + UY*UY) / SNDSPD2
!        MU2     = (LU / (3.14_8 * state%dt(i) * SNDSPD * input%SndSpdRef)) * (LU / (3.14_8 * state%dt(i) * SNDSPD*input%SndSpdRef))
        MU2     = (LU / (3.14_8 * state%dt(i) * SNDSPD)) * (LU / (3.14_8 * state%dt(i) * SNDSPD))
        MP2     = MIN(MAX(MI2,MU2,MMIN2),1.0_8)
        EPS_P   = MP2 / (1.0_8 + (GAM - 1.0_8) * MP2)
        RHO_PP  = 1.0_8 / (EPS_P * SNDSPD2)
        H_P     = 0.0_8
        H0      = T + 0.5_8 * (UX * UX + UY * UY)
        H_T     = 1.0_8
      PC(1)   = RHO * H_P + H0 * RHO_PP - 1.0_8
      PC(2)   = RHO * UX
      PC(3)   = RHO * UY
      PC(4)   = RHO * H_T + H0 * RHO_T

        ! ... index
        index = (i-1)*stencil_size + 1

         values_r(index) =  values_r(index) + PC(1) + time_fac * dtau / state%dt(i) * (gamm1i + KE * RHO_P) 
        values_ru(index) = values_ru(index) + PC(2) + time_fac * dtau / state%dt(i) * RHO * UX
        values_rv(index) = values_rv(index) + PC(3) + time_fac * dtau / state%dt(i) * RHO * UY
        values_rE(index) = values_rE(index) + PC(4) + time_fac * dtau / state%dt(i) * KE * RHO_T

      end do

    End Select

    If (input%Implicit_SAT_Art_Diss == TRUE) &
      Call NSImplicit2DSATArtDiss(region, ng, lhs_var, values_r, values_ru, values_rv, values_rE, stencil_size, dtau)

    if (state%RE <= 0.0_8) Return
   ! if(region%myrank==0) write(*,'(A)') 'Skipping all viscous terms ...'
   ! return
   ! if(region%myrank==0) write(*,'(A)') '... did not skip.'
    ! ... x-direction, second derivatives
    dir   = 1
    order = 2
    opID  = input%iSecondDerivImplicit

    ! ... local partition size
    N1 = grid%ie(1)-grid%is(1)+1
    N2 = grid%ie(2)-grid%is(2)+1
    N3 = grid%ie(3)-grid%is(3)+1

    ! ... partition size including ghost
    N1g = grid%ie(1)-grid%is(1)+1+SUM(grid%nGhostRHS(dir,:)) ! (MIN(grid%nGhostRHS(dir,1),1)+MIN(grid%nGhostRHS(dir,2),1))*input%overlap(opID)
    N2g = grid%ie(2)-grid%is(2)+1
    N3g = grid%ie(3)-grid%is(3)+1

    ! ... get ghost cell data
    Nc = grid%nCells
    Ncg = Nc + SUM(grid%nGhostRHS(dir,:))*grid%vdsGhost(dir)

    ! ... allocate a lot of arrays
    allocate(rhoAux(Ncg),rhouAux(Ncg),rhovAux(Ncg),muAux(Ncg),lambdaAux(Ncg))
    allocate(JACAux(Ncg),XI_X_TILDEAux(Ncg),XI_Y_TILDEAux(Ncg))

    ! ... initialize auxiliary vectors
    rhoAux(:) = 0.0_rfreal
    rhouAux(:) = 0.0_rfreal
    rhovAux(:) = 0.0_rfreal
    JACAux(:) = 0.0_rfreal
    XI_X_TILDEAux(:) = 0.0_rfreal
    XI_Y_TILDEAux(:) = 0.0_rfreal
    muAux(:) = 0.0_rfreal
    lambdaAux(:) = 0.0_rfreal
    

!    call mpi_barrier(mycomm,ierr)
!    if(region%myrank == 0) write(*,*) 'All are populating auxiliary matrices, order,dir = ',order,dir
    ! ... populate auxiliary vectors with local data
    Do i = 1, Nc
       ! ... density
       rhoAux(grid%cell2ghost(i,dir)) = state%cv(i,1)
       ! ... x-momentum
       rhouAux(grid%cell2ghost(i,dir)) = state%cv(i,2)
       ! ... y-momentum
       rhovAux(grid%cell2ghost(i,dir)) = state%cv(i,3)
       ! ... Jacobian
       JACAux(grid%cell2ghost(i,dir)) = grid%JAC(i)
       ! ... XI_X_TILDE
       XI_X_TILDEAux(grid%cell2ghost(i,dir)) = grid%MT1(i, region%global%t2Map(dir,1))
       ! ... XI_Y_TILDE
       XI_Y_TILDEAux(grid%cell2ghost(i,dir)) = grid%MT1(i, region%global%t2Map(dir,2))
       ! ... viscosity
       muAux(grid%cell2ghost(i,dir)) = state%tv(i,1)
       ! ... second coef. of viscosity
       lambdaAux(grid%cell2ghost(i,dir)) = state%tv(i,2)
    End Do

!    call mpi_barrier(mycomm,ierr)
!    if(region%myrank == 0) write(*,*) 'All are starting Ghost_Cell_Exchange, order,dir = ',order,dir
    ! ... get ghost data
    call Ghost_Cell_Exchange(region,ng,dir, rhoAux)
    call Ghost_Cell_Exchange(region,ng,dir,rhouAux)
    call Ghost_Cell_Exchange(region,ng,dir,rhovAux)
    call Ghost_Cell_Exchange(region,ng,dir, JACAux)
    call Ghost_Cell_Exchange(region,ng,dir,XI_X_TILDEAux)
    call Ghost_Cell_Exchange(region,ng,dir,XI_Y_TILDEAux)
    call Ghost_Cell_Exchange(region,ng,dir,muAux)
    call Ghost_Cell_Exchange(region,ng,dir,lambdaAux)

!    call mpi_barrier(mycomm,ierr)
!    if(region%myrank == 0) write(*,*) 'All are done with Ghost_Cell_Exchange, order,dir = ',order,dir

    
    !if(region%myrank==0) write(*,'(A)') 'Skipping x-direction viscous terms ...'

    SELECT CASE (lhs_var)
   ! SELECT CASE (10)
    CASE (2)

      ! ... viscous fluxes
      do k = 1, grid%vdsGhost(dir)
        do j = 1, grid%B(opID,dir)%n
          do i = grid%B(opID,dir)%row_ptr(k,j), grid%B(opID,dir)%row_ptr(k,j+1)-1

            ! ... l0 and l1 indices refer to BUFFERED data
            local_val = grid%B(opID,dir)%val(i,k)
            local_ind = grid%B(opID,dir)%col_ind(i,k)
            l0        = grid%vec_indGhost(k,local_ind,dir)
            l1        = grid%vec_indGhost(k,        j,dir)

            ! ... convert from linear to triplet indices
            ! ... rhs index (buffered data)
            ii0 = mod(l0-1,N1g)+1
            jj0 = mod((l0-ii0)/N1g,N2g)+1
            kk0 = (l0-ii0-(jj0-1)*N1g)/(N1g*N2g)+1

            ! ... lhs index (buffered data)
            ii1 = mod(l1-1,N1g)+1
            jj1 = mod((l1-ii1)/N1g,N2g)+1
            kk1 = (l1-ii1-(jj1-1)*N1g)/(N1g*N2g)+1

            ! ... convert to global indices
            ii0 = ii0 + grid%is(1) - 1 - grid%nGhostRHS(dir,1)
            jj0 = jj0 + grid%is(2) - 1
            kk0 = kk0 + grid%is(3) - 1
            ii1 = ii1 + grid%is(1) - 1 - grid%nGhostRHS(dir,1)
            jj1 = jj1 + grid%is(2) - 1
            kk1 = kk1 + grid%is(3) - 1


            if (ii1 < grid%is(1) .OR. ii1 > grid%ie(1)) CYCLE

            ! ... convert l1 back to non-buffered counting
            l1 = (kk1-grid%is(3))*N1*N2 + (jj1-grid%is(2))*N1 + (ii1-grid%is(1)+1)

!             if (region%myrank == 0)then
!                if (ii0-ii1 == 0) write(*,*) 'stencil_size,l0,l1,grid%cell2ghost(l1,dir)',stencil_size,l0,l1,grid%cell2ghost(l1,dir)
!                write(*,*) 'input%HYPREStencilIndex(order, ii0-ii1, jj0-jj1, kk0-kk1)',input%HYPREStencilIndex(order,:,:,:)
!             end if
!             call graceful_exit(region%myrank, 'hypre stencil')

            ! ... intermediate values for l1 index
            JAC_l1    = grid%JAC(l1)
            XI_X_l1   = grid%MT1(l1,region%global%t2Map(dir,1)) * JAC_l1
            XI_Y_l1   = grid%MT1(l1,region%global%t2Map(dir,2)) * JAC_l1
            MU_l1     = state%tv(l1,1) * state%REinv
            LAMBDA_l1 = state%tv(l1,2) * state%REinv

            ! ... intermediate values for l0 index
            JAC_l0    = JACAux(l0)
            XI_X_l0   = XI_X_TILDEAux(l0) * JAC_l0
            XI_Y_l0   = XI_Y_TILDEAux(l0) * JAC_l0
            MU_l0     = muAux(l0) * state%REinv
            LAMBDA_l0 = lambdaAux(l0) * state%REinv

            ! ... index for l0 first
            index = (l1-1)*stencil_size + input%HYPREStencilIndex(opID, ii0-ii1, jj0-jj1, kk0-kk1) + 1

!            if (ii1 == 435 .and. jj1 == 100) print*,'ii1,jj1,ii0,jj0,local_value,index,ii0-ii1,jj0-jj1',ii1,jj1,ii0,jj0,local_val,input%HYPREStencilIndex(opID, ii0-ii1, jj0-jj1, kk0-kk1),&
 !                index,ii0-ii1, jj0-jj1

            ! ... store in array
            values_ru(index) = values_ru(index) - 0.5_8 * local_val * &
                 ( ((2.0_8 * MU_l0 + LAMBDA_l0) * XI_X_l0 * XI_X_l0 + MU_l0 * XI_Y_l0 * XI_Y_l0) + & 
                   ((2.0_8 * MU_l1 + LAMBDA_l1) * XI_X_l1 * XI_X_l1 + MU_l1 * XI_Y_l1 * XI_Y_l1) )


            values_rv(index) = values_rv(index) - 0.5_8 * local_val * &
                 ( ((MU_l0 + LAMBDA_l0) * XI_Y_l0 * XI_X_l0) + &
                   ((MU_l1 + LAMBDA_l1) * XI_Y_l1 * XI_X_l1) )


!             ! ... store in array
!             values_ru(index) = values_ru(index) - 0.5_8 * local_val * &
!                  (MU_l0 * ( 4.0_8 / 3.0_8 * XI_X_l0 * XI_X_l0 + XI_Y_l0 * XI_Y_l0 ) + & 
!                  MU_l1 * ( 4.0_8 / 3.0_8 * XI_X_l1 * XI_X_l1 + XI_Y_l1 * XI_Y_l1 ))

!             values_rv(index) = values_rv(index) - 0.5_8 * local_val * &
!                  (MU_l0 * (-2.0_8 / 3.0_8 * XI_X_l0 * XI_Y_l0 + XI_Y_l0 * XI_X_l0 ) + &
!                  MU_l1 * (-2.0_8 / 3.0_8 * XI_X_l1 * XI_Y_l1 + XI_Y_l1 * XI_X_l1 ))


            ! ... index for l1 second
            index = (l1-1)*stencil_size + 1

            ! ... store in array
            values_ru(index) = values_ru(index) + 0.5_8 * local_val * &
                 ( ((2.0_8 * MU_l0 + LAMBDA_l0) * XI_X_l0 * XI_X_l0 + MU_l0 * XI_Y_l0 * XI_Y_l0) )

            values_rv(index) = values_rv(index) + 0.5_8 * local_val * &
                 ( ((MU_l0 + LAMBDA_l0) * XI_Y_l0 * XI_X_l0) )


!             ! ... store in array
!             values_ru(index) = values_ru(index) + 0.5_8 * local_val * MU_l0 * ( 4.0_8 / 3.0_8 * XI_X_l0 * XI_X_l0 + XI_Y_l0 * XI_Y_l0 )
!             values_rv(index) = values_rv(index) + 0.5_8 * local_val * MU_l0 * (-2.0_8 / 3.0_8 * XI_X_l0 * XI_Y_l0 + XI_Y_l0 * XI_X_l0 )


          end do
        end do
      end do

    CASE (3)

      ! ... viscous fluxes
      do k = 1, grid%vdsGhost(dir)
        do j = 1, grid%B(opID,dir)%n
          do i = grid%B(opID,dir)%row_ptr(k,j), grid%B(opID,dir)%row_ptr(k,j+1)-1

            ! ... l0 and l1 indices refer to BUFFERED data
            local_val = grid%B(opID,dir)%val(i,k)
            local_ind = grid%B(opID,dir)%col_ind(i,k)
            l0        = grid%vec_indGhost(k,local_ind,dir)
            l1        = grid%vec_indGhost(k,        j,dir)

            ! ... convert from linear to triplet indices
            ! ... rhs index (buffered data)
            ii0 = mod(l0-1,N1g)+1
            jj0 = mod((l0-ii0)/N1g,N2g)+1
            kk0 = (l0-ii0-(jj0-1)*N1g)/(N1g*N2g)+1

            ! ... lhs index (buffered data)
            ii1 = mod(l1-1,N1g)+1
            jj1 = mod((l1-ii1)/N1g,N2g)+1
            kk1 = (l1-ii1-(jj1-1)*N1g)/(N1g*N2g)+1

            ! ... convert to global indices
            ii0 = ii0 + grid%is(1) - 1 - grid%nGhostRHS(dir,1)
            jj0 = jj0 + grid%is(2) - 1
            kk0 = kk0 + grid%is(3) - 1
            ii1 = ii1 + grid%is(1) - 1 - grid%nGhostRHS(dir,1)
            jj1 = jj1 + grid%is(2) - 1
            kk1 = kk1 + grid%is(3) - 1

            if (ii1 < grid%is(1) .OR. ii1 > grid%ie(1)) CYCLE

            ! ... convert l1 back to non-buffered counting
            l1 = (kk1-grid%is(3))*N1*N2 + (jj1-grid%is(2))*N1 + (ii1-grid%is(1)+1)

            ! ... intermediate values for l1 index
            JAC_l1    = grid%JAC(l1)
            XI_X_l1   = grid%MT1(l1,region%global%t2Map(dir,1)) * JAC_l1
            XI_Y_l1   = grid%MT1(l1,region%global%t2Map(dir,2)) * JAC_l1
            MU_l1     = state%tv(l1,1) * state%REinv
            LAMBDA_l1 = state%tv(l1,2) * state%REinv

            ! ... intermediate values for l0 index
            JAC_l0    = JACAux(l0)
            XI_X_l0   = XI_X_TILDEAux(l0) * JAC_l0
            XI_Y_l0   = XI_Y_TILDEAux(l0) * JAC_l0
            MU_l0     = muAux(l0) * state%REinv
            LAMBDA_l0 = lambdaAux(l0) * state%REinv

            ! ... index at l0 first
            index = (l1-1)*stencil_size + input%HYPREStencilIndex(opID, ii0-ii1, jj0-jj1, kk0-kk1) + 1

            ! ... store in array
            values_ru(index) = values_ru(index) - 0.5_8 * local_val * &
                 ( ((MU_l0 + LAMBDA_l0) * XI_X_l0 * XI_Y_l0) + &
                   ((MU_l1 + LAMBDA_l1) * XI_Y_l1 * XI_X_l1) )

            values_rv(index) = values_rv(index) - 0.5_8 * local_val * &
                 ( ((2.0_8 * MU_l0 + LAMBDA_l0) * XI_Y_l0 * XI_Y_l0 + MU_l0 * XI_X_l0 * XI_X_l0) + &
                   ((2.0_8 * MU_l1 + LAMBDA_l1) * XI_Y_l1 * XI_Y_l1 + MU_l1 * XI_X_l1 * XI_X_l1) )
                

!             ! ... store in array
!             values_ru(index) = values_ru(index) - 0.5_8 * local_val * &
!                  (MU_l0 * (-2.0_8 / 3.0_8 * XI_X_l0 * XI_Y_l0 + XI_Y_l0 * XI_X_l0 ) + &
!                  MU_l1 * (-2.0_8 / 3.0_8 * XI_X_l1 * XI_Y_l1 + XI_Y_l1 * XI_X_l1 ))

!             values_rv(index) = values_rv(index) - 0.5_8 * local_val * &
!                  (MU_l0 * ( 4.0_8 / 3.0_8 * XI_Y_l0 * XI_Y_l0 + XI_X_l0 * XI_X_l0 ) + &
!                  MU_l1 * ( 4.0_8 / 3.0_8 * XI_Y_l1 * XI_Y_l1 + XI_X_l1 * XI_X_l1 ))

            ! ... now work on index at l1
            index = (l1-1)*stencil_size + 1

            ! ... store in array
            values_ru(index) = values_ru(index) + 0.5_8 * local_val * &
                 ( ((MU_l0 + LAMBDA_l0) * XI_Y_l0 * XI_X_l0) )

            values_rv(index) = values_rv(index) + 0.5_8 * local_val * &
                 ( ((2.0_8 * MU_l0 + LAMBDA_l0) * XI_Y_l0 * XI_Y_l0 + MU_l0 * XI_X_l0 * XI_X_l0) )


!             values_ru(index) = values_ru(index) + 0.5_8 * local_val * &
!                  (MU_l0 * (-2.0_8 / 3.0_8 * XI_X_l0 * XI_Y_l0 + XI_Y_l0 * XI_X_l0 ))
            
!             values_rv(index) = values_rv(index) + 0.5_8 * local_val * &
!                  (MU_l0 * ( 4.0_8 / 3.0_8 * XI_Y_l0 * XI_Y_l0 + XI_X_l0 * XI_X_l0 ))



          end do
        end do
      end do

    CASE (4)

      ! ... viscous fluxes
      do k = 1, grid%vdsGhost(dir)
        do j = 1, grid%B(opID,dir)%n
          do i = grid%B(opID,dir)%row_ptr(k,j), grid%B(opID,dir)%row_ptr(k,j+1)-1

            ! ... l0 and l1 indices refer to BUFFERED data
            local_val = grid%B(opID,dir)%val(i,k)
            local_ind = grid%B(opID,dir)%col_ind(i,k)
            l0        = grid%vec_indGhost(k,local_ind,dir)
            l1        = grid%vec_indGhost(k,        j,dir)

            ! ... convert from linear to triplet indices
            ! ... rhs index (buffered data)
            ii0 = mod(l0-1,N1g)+1
            jj0 = mod((l0-ii0)/N1g,N2g)+1
            kk0 = (l0-ii0-(jj0-1)*N1g)/(N1g*N2g)+1

            ! ... lhs index (buffered data)
            ii1 = mod(l1-1,N1g)+1
            jj1 = mod((l1-ii1)/N1g,N2g)+1
            kk1 = (l1-ii1-(jj1-1)*N1g)/(N1g*N2g)+1

            ! ... convert to global indices
            ii0 = ii0 + grid%is(1) - 1 - grid%nGhostRHS(dir,1)
            jj0 = jj0 + grid%is(2) - 1
            kk0 = kk0 + grid%is(3) - 1
            ii1 = ii1 + grid%is(1) - 1 - grid%nGhostRHS(dir,1)
            jj1 = jj1 + grid%is(2) - 1
            kk1 = kk1 + grid%is(3) - 1

            if (ii1 < grid%is(1) .OR. ii1 > grid%ie(1)) CYCLE

            ! ... convert l1 back to non-buffered counting
            l1 = (kk1-grid%is(3))*N1*N2 + (jj1-grid%is(2))*N1 + (ii1-grid%is(1)+1)

            ! ... intermediate values
            UX_l1    = state%cv(l1,2) * state%dv(l1,3)
            UY_l1    = state%cv(l1,3) * state%dv(l1,3)
            JAC_l1   = grid%JAC(l1)
            XI_X_l1  = grid%MT1(l1,region%global%t2Map(dir,1)) * JAC_l1
            XI_Y_l1  = grid%MT1(l1,region%global%t2Map(dir,2)) * JAC_l1
            MU_l1    = state%tv(l1,1) * state%REinv
            LAMBDA_l1= state%tv(l1,2) * state%REinv
            KAPPA_l1 = MU_l1 * state%PRinv
            
            RHO_l0   = rhoAux(l0)
            UX_l0    = rhouAux(l0) / RHO_l0
            UY_l0    = rhovAux(l0) / RHO_l0
            JAC_l0   = JACAux(l0)
            XI_X_l0  = XI_X_TILDEAux(l0) * JAC_l0
            XI_Y_l0  = XI_Y_TILDEAux(l0) * JAC_l0
            MU_l0    = muAux(l0) * state%REinv
            LAMBDA_l0= lambdaAux(l0) * state%REinv
            KAPPA_l0 = MU_l0 * state%PRinv

            ! ... index at l0 first
            index = (l1-1)*stencil_size + input%HYPREStencilIndex(opID, ii0-ii1, jj0-jj1, kk0-kk1) + 1

            ! ... store in array
            values_ru(index) = values_ru(index) - 0.5_8 * local_val * &
                 ( (UX_l0 * ((2.0_8 * MU_l0 + LAMBDA_l0) * XI_X_l0 * XI_X_l0 + MU_l0 * XI_Y_l0 * XI_Y_l0) + &
                    UY_l0 *(( MU_l0 + LAMBDA_l0) * XI_X_l0 * XI_Y_l0)) + &
                   (UX_l1 * ((2.0_8 * MU_l1 + LAMBDA_l1) * XI_X_l1 * XI_X_l1 + MU_l1 * XI_Y_l1 * XI_Y_l1) + &
                    UY_l1 *(( MU_l1 + LAMBDA_l1) * XI_X_l1 * XI_Y_l1)) )

            values_rv(index) = values_rv(index) - 0.5_8 * local_val * &
                 ( (UX_l0 * ((MU_l0 + LAMBDA_l0) * XI_X_l0 * XI_Y_l0) + &
                    UY_l0 * ((2.0_8 * MU_l0 + LAMBDA_l0) * XI_Y_l0 * XI_Y_l0 + MU_l0 * XI_X_l0 * XI_X_l0)) + &
                   (UX_l1 * ((MU_l1 + LAMBDA_l1) * XI_X_l1 * XI_Y_l1) + &
                    UY_l1 * ((2.0_8 * MU_l1 + LAMBDA_l1) * XI_Y_l1 * XI_Y_l1 + MU_l1 * XI_X_l1 * XI_X_l1)) )

            values_rE(index) = values_rE(index) - 0.5_8 * local_val * &
                 ( KAPPA_l0 * ( XI_X_l0 * XI_X_l0 + XI_Y_l0 * XI_Y_l0 ) + &
                   KAPPA_l1 * ( XI_X_l1 * XI_X_l1 + XI_Y_l1 * XI_Y_l1 ) )

!             ! ... store in array
!             values_ru(index) = values_ru(index) -  0.5_8 * local_val * &
!                  (MU_l0 * ( UX_l0 * ( 4.0_8/3.0_8 * XI_X_l0 * XI_X_l0 + XI_Y_l0 * XI_Y_l0 ) + &
!                   UY_l0 * ( -2.0_8/3.0_8 * XI_Y_l0 * XI_X_l0 + XI_X_l0 * XI_Y_l0 ) ) + &
!                   MU_l1 * ( UX_l1 * ( 4.0_8/3.0_8 * XI_X_l1 * XI_X_l1 + XI_Y_l1 * XI_Y_l1 ) + &
!                   UY_l1 * ( -2.0_8/3.0_8 * XI_Y_l1 * XI_X_l1 + XI_X_l1 * XI_Y_l1 ) ) )

!             values_rv(index) = values_rv(index) - 0.5_8 * local_val * &
!                  (MU_l0 * ( UX_l0 * (-2.0_8/3.0_8 * XI_X_l0 * XI_Y_l0 + XI_Y_l0 * XI_X_l0 ) + &
!                   UY_l0 * (  4.0_8/3.0_8 * XI_Y_l0 * XI_Y_l0 + XI_X_l0 * XI_X_l0 ) ) + &
!                   MU_l1 * ( UX_l1 * (-2.0_8/3.0_8 * XI_X_l1 * XI_Y_l1 + XI_Y_l1 * XI_X_l1 ) + &
!                   UY_l1 * (  4.0_8/3.0_8 * XI_Y_l1 * XI_Y_l1 + XI_X_l1 * XI_X_l1 ) ) )

!            values_rE(index) = values_rE(index) - 0.5_8 * local_val * &
!                 ( KAPPA_l0 * ( XI_X_l0 * XI_X_l0 + XI_Y_l0 * XI_Y_l0 ) + &
!                   KAPPA_l1 * ( XI_X_l1 * XI_X_l1 + XI_Y_l1 * XI_Y_l1 ) )

            ! ... now work on index at l1
            index = (l1-1)*stencil_size + 1

            ! ... store in array
            values_ru(index) = values_ru(index) +  0.5_8 * local_val * &
                 ( (UX_l0 * ((2.0_8 * MU_l0 + LAMBDA_l0) * XI_X_l0 * XI_X_l0 + MU_l0 * XI_Y_l0 * XI_Y_l0) + &
                    UY_l0 *(( MU_l0 + LAMBDA_l0) * XI_X_l0 * XI_Y_l0)) )

            values_rv(index) = values_rv(index) + 0.5_8 * local_val * &
                 ( (UX_l0 * ((MU_l0 + LAMBDA_l0) * XI_X_l0 * XI_Y_l0) + &
                    UY_l0 * ((2.0_8 * MU_l0 + LAMBDA_l0) * XI_Y_l0 * XI_Y_l0 + MU_l0 * XI_X_l0 * XI_X_l0)) )

            values_rE(index) = values_rE(index) + 0.5_8 * local_val * &
                 ( KAPPA_l0 * ( XI_X_l0 * XI_X_l0 + XI_Y_l0 * XI_Y_l0 ) )

!             values_ru(index) = values_ru(index) +  0.5_8 * local_val * &
!                  (MU_l0 * ( UX_l0 * ( 4.0_8/3.0_8 * XI_X_l0 * XI_X_l0 + XI_Y_l0 * XI_Y_l0 ) + &
!                   UY_l0 * ( -2.0_8/3.0_8 * XI_Y_l0 * XI_X_l0 + XI_X_l0 * XI_Y_l0 ) ) )

!             values_rv(index) = values_rv(index) + 0.5_8 * local_val * &
!                  (MU_l0 * ( UX_l0 * (-2.0_8/3.0_8 * XI_X_l0 * XI_Y_l0 + XI_Y_l0 * XI_X_l0 ) + &
!                   UY_l0 * (  4.0_8/3.0_8 * XI_Y_l0 * XI_Y_l0 + XI_X_l0 * XI_X_l0 ) ) )

!             values_rE(index) = values_rE(index) + 0.5_8 * local_val * &
!                  ( KAPPA_l0 * ( XI_X_l0 * XI_X_l0 + XI_Y_l0 * XI_Y_l0 ))


          end do
        end do
      end do

   End Select

   ! ... deallocate auxiliary arrays for this direction
   deallocate(rhoAux,rhouAux,rhovAux,muAux,lambdaAux,JACAux,XI_X_TILDEAux,XI_Y_TILDEAux)

!     if(region%myrank==0) write(*,'(A)') 'doing y direction viscous terms ...'
    ! return
    ! if(region%myrank==0) write(*,'(A)') '... did not skip.'
    ! ... y-direction, second derivatives
    dir   = 2
    order = 2
    opID  = input%iSecondDerivImplicit

    ! ... local partition size
    N1 = grid%ie(1)-grid%is(1)+1
    N2 = grid%ie(2)-grid%is(2)+1
    N3 = grid%ie(3)-grid%is(3)+1

    ! ... partition size including ghost
    N1g = grid%ie(1)-grid%is(1)+1
    N2g = grid%ie(2)-grid%is(2)+1+SUM(grid%nGhostRHS(dir,:)) ! (MIN(grid%nGhostRHS(dir,1),1)+MIN(grid%nGhostRHS(dir,2),1))*input%overlap(opID)
    N3g = grid%ie(3)-grid%is(3)+1

    ! ... get ghost cell data
    Nc = grid%nCells
    Ncg = Nc + SUM(grid%nGhostRHS(dir,:))*grid%vdsGhost(dir)

    ! ... allocate a lot of arrays
    allocate(rhoAux(Ncg),rhouAux(Ncg),rhovAux(Ncg),rhoEAux(Ncg),muAux(Ncg),lambdaAux(Ncg))
    allocate(JACAux(Ncg),XI_X_TILDEAux(Ncg),XI_Y_TILDEAux(Ncg))

    ! ... initialize auxiliary vectors
    rhoAux(:) = 0.0_rfreal
    rhouAux(:) = 0.0_rfreal
    rhovAux(:) = 0.0_rfreal
    JACAux(:) = 0.0_rfreal
    XI_X_TILDEAux(:) = 0.0_rfreal
    XI_Y_TILDEAux(:) = 0.0_rfreal
    muAux(:) = 0.0_rfreal
    lambdaAux(:) = 0.0_rfreal

!    call mpi_barrier(mycomm,ierr)
!    if(region%myrank == 0) write(*,*) 'All are populating auxiliary matrices, order,dir = ',order,dir
    ! ... populate auxiliary vectors with local data
    Do i = 1, Nc
       ! ... density
       rhoAux(grid%cell2ghost(i,dir)) = state%cv(i,1)
       ! ... x-momentum
       rhouAux(grid%cell2ghost(i,dir)) = state%cv(i,2)
       ! ... y-momentum
       rhovAux(grid%cell2ghost(i,dir)) = state%cv(i,3)
       ! ... Jacobian
       JACAux(grid%cell2ghost(i,dir)) = grid%JAC(i)
       ! ... XI_X_TILDE
       XI_X_TILDEAux(grid%cell2ghost(i,dir)) = grid%MT1(i, region%global%t2Map(dir,1))
       ! ... XI_Y_TILDE
       XI_Y_TILDEAux(grid%cell2ghost(i,dir)) = grid%MT1(i, region%global%t2Map(dir,2))
       ! ... viscosity
       muAux(grid%cell2ghost(i,dir)) = state%tv(i,1)
       ! ... 2nd coef. viscosity
       lambdaAux(grid%cell2ghost(i,dir)) = state%tv(i,2)
    End Do

!    call mpi_barrier(mycomm,ierr)
!    if(region%myrank == 0) write(*,*) 'All are starting Ghost_Cell_Exchange, order,dir = ',order,dir
    ! ... get ghost data
    call Ghost_Cell_Exchange(region,ng,dir, rhoAux)
    call Ghost_Cell_Exchange(region,ng,dir,rhouAux)
    call Ghost_Cell_Exchange(region,ng,dir,rhovAux)
    call Ghost_Cell_Exchange(region,ng,dir, JACAux)
    call Ghost_Cell_Exchange(region,ng,dir,XI_X_TILDEAux)
    call Ghost_Cell_Exchange(region,ng,dir,XI_Y_TILDEAux)
    call Ghost_Cell_Exchange(region,ng,dir,muAux)
    call Ghost_Cell_Exchange(region,ng,dir,lambdaAux)

!    call mpi_barrier(mycomm,ierr)
!    if(region%myrank == 0) write(*,*) 'All are done with Ghost_Cell_Exchange, order,dir = ',order,dir

    SELECT CASE (lhs_var)
    CASE (2)
!     if(region%myrank==0) write(*,'(A)') 'doing y direction viscous terms ...'
      ! ... viscous fluxes
      do k = 1, grid%vdsGhost(dir)
        do j = 1, grid%B(opID,dir)%n
          do i = grid%B(opID,dir)%row_ptr(k,j), grid%B(opID,dir)%row_ptr(k,j+1)-1

            ! ... l0 and l1 indices refer to BUFFERED data
            local_val = grid%B(opID,dir)%val(i,k)
            local_ind = grid%B(opID,dir)%col_ind(i,k)
            l0        = grid%vec_indGhost(k,local_ind,dir)
            l1        = grid%vec_indGhost(k,        j,dir)

            ! ... convert from linear to triplet indices
            ! ... rhs index (buffered data)
            ii0 = mod(l0-1,N1g)+1
            jj0 = mod((l0-ii0)/N1g,N2g)+1
            kk0 = (l0-ii0-(jj0-1)*N1g)/(N1g*N2g)+1

            ! ... lhs index (buffered data)
            ii1 = mod(l1-1,N1g)+1
            jj1 = mod((l1-ii1)/N1g,N2g)+1
            kk1 = (l1-ii1-(jj1-1)*N1g)/(N1g*N2g)+1

            ! ... convert to global indices
            ii0 = ii0 + grid%is(1) - 1
            jj0 = jj0 + grid%is(2) - 1 - grid%nGhostRHS(dir,1)
            kk0 = kk0 + grid%is(3) - 1
            ii1 = ii1 + grid%is(1) - 1
            jj1 = jj1 + grid%is(2) - 1 - grid%nGhostRHS(dir,1)
            kk1 = kk1 + grid%is(3) - 1

            if (jj1 < grid%is(2) .OR. jj1 > grid%ie(2)) CYCLE

            ! ... convert l1 back to non-buffered counting
            l1 = (kk1-grid%is(3))*N1*N2 + (jj1-grid%is(2))*N1 + (ii1-grid%is(1)+1)

            ! ... intermediate values for l1 index
            JAC_l1    = grid%JAC(l1)
            XI_X_l1   = grid%MT1(l1,region%global%t2Map(dir,1)) * JAC_l1
            XI_Y_l1   = grid%MT1(l1,region%global%t2Map(dir,2)) * JAC_l1
            MU_l1     = state%tv(l1,1) * state%REinv
            LAMBDA_l1 = state%tv(l1,2) * state%REinv

            ! ... intermediate values for l0 index
            JAC_l0    = JACAux(l0)
            XI_X_l0   = XI_X_TILDEAux(l0) * JAC_l0
            XI_Y_l0   = XI_Y_TILDEAux(l0) * JAC_l0
            MU_l0     = muAux(l0) * state%REinv
            LAMBDA_l0 = lambdaAux(l0) * state%REinv

            ! ... index for l0 first
            index = (l1-1)*stencil_size + input%HYPREStencilIndex(opID, ii0-ii1, jj0-jj1, kk0-kk1) + 1

!            if (ii1 == 435 .and. jj1 == 100) print*,'ii1,jj1,ii0,jj0,local_value,index,ii0-ii1,jj0-jj1',ii1,jj1,ii0,jj0,local_val,input%HYPREStencilIndex(opID, ii0-ii1, jj0-jj1, kk0-kk1),&
 !                index,ii0-ii1, jj0-jj1

            ! ... store in array
            values_ru(index) = values_ru(index) - 0.5_8 * local_val * &
                 ( ((2.0_8 * MU_l0 + LAMBDA_l0) * XI_X_l0 * XI_X_l0 + MU_l0 * XI_Y_l0 * XI_Y_l0) + & 
                   ((2.0_8 * MU_l1 + LAMBDA_l1) * XI_X_l1 * XI_X_l1 + MU_l1 * XI_Y_l1 * XI_Y_l1) )


            values_rv(index) = values_rv(index) - 0.5_8 * local_val * &
                 ( ((MU_l0 + LAMBDA_l0) * XI_Y_l0 * XI_X_l0) + &
                   ((MU_l1 + LAMBDA_l1) * XI_Y_l1 * XI_X_l1) )


!             ! ... store in array
!             values_ru(index) = values_ru(index) - 0.5_8 * local_val * &
!                  (MU_l0 * ( 4.0_8 / 3.0_8 * XI_X_l0 * XI_X_l0 + XI_Y_l0 * XI_Y_l0 ) + & 
!                  MU_l1 * ( 4.0_8 / 3.0_8 * XI_X_l1 * XI_X_l1 + XI_Y_l1 * XI_Y_l1 ))

!             values_rv(index) = values_rv(index) - 0.5_8 * local_val * &
!                  (MU_l0 * (-2.0_8 / 3.0_8 * XI_X_l0 * XI_Y_l0 + XI_Y_l0 * XI_X_l0 ) + &
!                  MU_l1 * (-2.0_8 / 3.0_8 * XI_X_l1 * XI_Y_l1 + XI_Y_l1 * XI_X_l1 ))


            ! ... index for l1 second
            index = (l1-1)*stencil_size + 1

            ! ... store in array
            values_ru(index) = values_ru(index) + 0.5_8 * local_val * &
                 ( ((2.0_8 * MU_l0 + LAMBDA_l0) * XI_X_l0 * XI_X_l0 + MU_l0 * XI_Y_l0 * XI_Y_l0) )

            values_rv(index) = values_rv(index) + 0.5_8 * local_val * &
                 ( ((MU_l0 + LAMBDA_l0) * XI_Y_l0 * XI_X_l0) )


!             ! ... store in array
!             values_ru(index) = values_ru(index) + 0.5_8 * local_val * MU_l0 * ( 4.0_8 / 3.0_8 * XI_X_l0 * XI_X_l0 + XI_Y_l0 * XI_Y_l0 )
!             values_rv(index) = values_rv(index) + 0.5_8 * local_val * MU_l0 * (-2.0_8 / 3.0_8 * XI_X_l0 * XI_Y_l0 + XI_Y_l0 * XI_X_l0 )


          end do
        end do
      end do

    CASE (3)

      ! ... viscous fluxes
      do k = 1, grid%vdsGhost(dir)
        do j = 1, grid%B(opID,dir)%n
          do i = grid%B(opID,dir)%row_ptr(k,j), grid%B(opID,dir)%row_ptr(k,j+1)-1

            ! ... l0 and l1 indices refer to BUFFERED data
            local_val = grid%B(opID,dir)%val(i,k)
            local_ind = grid%B(opID,dir)%col_ind(i,k)
            l0        = grid%vec_indGhost(k,local_ind,dir)
            l1        = grid%vec_indGhost(k,        j,dir)

            ! ... convert from linear to triplet indices
            ! ... rhs index (buffered data)
            ii0 = mod(l0-1,N1g)+1
            jj0 = mod((l0-ii0)/N1g,N2g)+1
            kk0 = (l0-ii0-(jj0-1)*N1g)/(N1g*N2g)+1

            ! ... lhs index (buffered data)
            ii1 = mod(l1-1,N1g)+1
            jj1 = mod((l1-ii1)/N1g,N2g)+1
            kk1 = (l1-ii1-(jj1-1)*N1g)/(N1g*N2g)+1

            ! ... convert to global indices
            ii0 = ii0 + grid%is(1) - 1
            jj0 = jj0 + grid%is(2) - 1 - grid%nGhostRHS(dir,1)
            kk0 = kk0 + grid%is(3) - 1
            ii1 = ii1 + grid%is(1) - 1
            jj1 = jj1 + grid%is(2) - 1 - grid%nGhostRHS(dir,1)
            kk1 = kk1 + grid%is(3) - 1

            if (jj1 < grid%is(2) .OR. jj1 > grid%ie(2)) CYCLE

            ! ... convert l1 back to non-buffered counting
            l1 = (kk1-grid%is(3))*N1*N2 + (jj1-grid%is(2))*N1 + (ii1-grid%is(1)+1)


            ! ... intermediate values for l1 index
            JAC_l1    = grid%JAC(l1)
            XI_X_l1   = grid%MT1(l1,region%global%t2Map(dir,1)) * JAC_l1
            XI_Y_l1   = grid%MT1(l1,region%global%t2Map(dir,2)) * JAC_l1
            MU_l1     = state%tv(l1,1) * state%REinv
            LAMBDA_l1 = state%tv(l1,2) * state%REinv

            ! ... intermediate values for l0 index
            JAC_l0    = JACAux(l0)
            XI_X_l0   = XI_X_TILDEAux(l0) * JAC_l0
            XI_Y_l0   = XI_Y_TILDEAux(l0) * JAC_l0
            MU_l0     = muAux(l0) * state%REinv
            LAMBDA_l0 = lambdaAux(l0) * state%REinv

            ! ... index at l0 first
            index = (l1-1)*stencil_size + input%HYPREStencilIndex(opID, ii0-ii1, jj0-jj1, kk0-kk1) + 1

            ! ... store in array
            values_ru(index) = values_ru(index) - 0.5_8 * local_val * &
                 ( ((MU_l0 + LAMBDA_l0) * XI_X_l0 * XI_Y_l0) + &
                   ((MU_l1 + LAMBDA_l1) * XI_Y_l1 * XI_X_l1) )

            values_rv(index) = values_rv(index) - 0.5_8 * local_val * &
                 ( ((2.0_8 * MU_l0 + LAMBDA_l0) * XI_Y_l0 * XI_Y_l0 + MU_l0 * XI_X_l0 * XI_X_l0) + &
                   ((2.0_8 * MU_l1 + LAMBDA_l1) * XI_Y_l1 * XI_Y_l1 + MU_l1 * XI_X_l1 * XI_X_l1) )
                

!             ! ... store in array
!             values_ru(index) = values_ru(index) - 0.5_8 * local_val * &
!                  (MU_l0 * (-2.0_8 / 3.0_8 * XI_X_l0 * XI_Y_l0 + XI_Y_l0 * XI_X_l0 ) + &
!                  MU_l1 * (-2.0_8 / 3.0_8 * XI_X_l1 * XI_Y_l1 + XI_Y_l1 * XI_X_l1 ))

!             values_rv(index) = values_rv(index) - 0.5_8 * local_val * &
!                  (MU_l0 * ( 4.0_8 / 3.0_8 * XI_Y_l0 * XI_Y_l0 + XI_X_l0 * XI_X_l0 ) + &
!                  MU_l1 * ( 4.0_8 / 3.0_8 * XI_Y_l1 * XI_Y_l1 + XI_X_l1 * XI_X_l1 ))

            ! ... now work on index at l1
            index = (l1-1)*stencil_size + 1

            ! ... store in array
            values_ru(index) = values_ru(index) + 0.5_8 * local_val * &
                 ( ((MU_l0 + LAMBDA_l0) * XI_Y_l0 * XI_X_l0) )

            values_rv(index) = values_rv(index) + 0.5_8 * local_val * &
                 ( ((2.0_8 * MU_l0 + LAMBDA_l0) * XI_Y_l0 * XI_Y_l0 + MU_l0 * XI_X_l0 * XI_X_l0) )


!             values_ru(index) = values_ru(index) + 0.5_8 * local_val * &
!                  (MU_l0 * (-2.0_8 / 3.0_8 * XI_X_l0 * XI_Y_l0 + XI_Y_l0 * XI_X_l0 ))
            
!             values_rv(index) = values_rv(index) + 0.5_8 * local_val * &
!                  (MU_l0 * ( 4.0_8 / 3.0_8 * XI_Y_l0 * XI_Y_l0 + XI_X_l0 * XI_X_l0 ))


          end do
        end do
      end do

    CASE (4)

      ! ... viscous fluxes
      do k = 1, grid%vdsGhost(dir)
        do j = 1, grid%B(opID,dir)%n
          do i = grid%B(opID,dir)%row_ptr(k,j), grid%B(opID,dir)%row_ptr(k,j+1)-1

            ! ... l0 and l1 indices refer to BUFFERED data
            local_val = grid%B(opID,dir)%val(i,k)
            local_ind = grid%B(opID,dir)%col_ind(i,k)
            l0        = grid%vec_indGhost(k,local_ind,dir)
            l1        = grid%vec_indGhost(k,        j,dir)

            ! ... convert from linear to triplet indices
            ! ... rhs index (buffered data)
            ii0 = mod(l0-1,N1g)+1
            jj0 = mod((l0-ii0)/N1g,N2g)+1
            kk0 = (l0-ii0-(jj0-1)*N1g)/(N1g*N2g)+1

            ! ... lhs index (buffered data)
            ii1 = mod(l1-1,N1g)+1
            jj1 = mod((l1-ii1)/N1g,N2g)+1
            kk1 = (l1-ii1-(jj1-1)*N1g)/(N1g*N2g)+1

            ! ... convert to global indices
            ii0 = ii0 + grid%is(1) - 1
            jj0 = jj0 + grid%is(2) - 1 - grid%nGhostRHS(dir,1)
            kk0 = kk0 + grid%is(3) - 1
            ii1 = ii1 + grid%is(1) - 1
            jj1 = jj1 + grid%is(2) - 1 - grid%nGhostRHS(dir,1)
            kk1 = kk1 + grid%is(3) - 1

            if (jj1 < grid%is(2) .OR. jj1 > grid%ie(2)) CYCLE

            ! ... convert l1 back to non-buffered counting
            l1 = (kk1-grid%is(3))*N1*N2 + (jj1-grid%is(2))*N1 + (ii1-grid%is(1)+1)

            ! ... intermediate values
            UX_l1    = state%cv(l1,2) * state%dv(l1,3)
            UY_l1    = state%cv(l1,3) * state%dv(l1,3)
            JAC_l1   = grid%JAC(l1)
            XI_X_l1  = grid%MT1(l1,region%global%t2Map(dir,1)) * JAC_l1
            XI_Y_l1  = grid%MT1(l1,region%global%t2Map(dir,2)) * JAC_l1
            MU_l1    = state%tv(l1,1) * state%REinv
            LAMBDA_l1= state%tv(l1,2) * state%REinv
            KAPPA_l1 = MU_l1 * state%PRinv
            
            RHO_l0   = rhoAux(l0)
            UX_l0    = rhouAux(l0) / RHO_l0
            UY_l0    = rhovAux(l0) / RHO_l0
            JAC_l0   = JACAux(l0)
            XI_X_l0  = XI_X_TILDEAux(l0) * JAC_l0
            XI_Y_l0  = XI_Y_TILDEAux(l0) * JAC_l0
            MU_l0    = muAux(l0) * state%REinv
            LAMBDA_l0= lambdaAux(l0) * state%REinv
            KAPPA_l0 = MU_l0 * state%PRinv

            ! ... index at l0 first
            index = (l1-1)*stencil_size + input%HYPREStencilIndex(opID, ii0-ii1, jj0-jj1, kk0-kk1) + 1

            ! ... store in array
            values_ru(index) = values_ru(index) -  0.5_8 * local_val * &
                 ( (UX_l0 * ((2.0_8 * MU_l0 + LAMBDA_l0) * XI_X_l0 * XI_X_l0 + MU_l0 * XI_Y_l0 * XI_Y_l0) + &
                    UY_l0 *(( MU_l0 + LAMBDA_l0) * XI_X_l0 * XI_Y_l0)) + &
                   (UX_l1 * ((2.0_8 * MU_l1 + LAMBDA_l1) * XI_X_l1 * XI_X_l1 + MU_l1 * XI_Y_l1 * XI_Y_l1) + &
                    UY_l1 *(( MU_l1 + LAMBDA_l1) * XI_X_l1 * XI_Y_l1)) )

            values_rv(index) = values_rv(index) - 0.5_8 * local_val * &
                 ( (UX_l0 * ((MU_l0 + LAMBDA_l0) * XI_X_l0 * XI_Y_l0) + &
                    UY_l0 * ((2.0_8 * MU_l0 + LAMBDA_l0) * XI_Y_l0 * XI_Y_l0 + MU_l0 * XI_X_l0 * XI_X_l0)) + &
                   (UX_l1 * ((MU_l1 + LAMBDA_l1) * XI_X_l1 * XI_Y_l1) + &
                    UY_l1 * ((2.0_8 * MU_l1 + LAMBDA_l1) * XI_Y_l1 * XI_Y_l1 + MU_l1 * XI_X_l1 * XI_X_l1)) )

            values_rE(index) = values_rE(index) - 0.5_8 * local_val * &
                 ( KAPPA_l0 * ( XI_X_l0 * XI_X_l0 + XI_Y_l0 * XI_Y_l0 ) + &
                   KAPPA_l1 * ( XI_X_l1 * XI_X_l1 + XI_Y_l1 * XI_Y_l1 ) )

!             ! ... store in array
!             values_ru(index) = values_ru(index) -  0.5_8 * local_val * &
!                  (MU_l0 * ( UX_l0 * ( 4.0_8/3.0_8 * XI_X_l0 * XI_X_l0 + XI_Y_l0 * XI_Y_l0 ) + &
!                   UY_l0 * ( -2.0_8/3.0_8 * XI_Y_l0 * XI_X_l0 + XI_X_l0 * XI_Y_l0 ) ) + &
!                   MU_l1 * ( UX_l1 * ( 4.0_8/3.0_8 * XI_X_l1 * XI_X_l1 + XI_Y_l1 * XI_Y_l1 ) + &
!                   UY_l1 * ( -2.0_8/3.0_8 * XI_Y_l1 * XI_X_l1 + XI_X_l1 * XI_Y_l1 ) ) )

!             values_rv(index) = values_rv(index) - 0.5_8 * local_val * &
!                  (MU_l0 * ( UX_l0 * (-2.0_8/3.0_8 * XI_X_l0 * XI_Y_l0 + XI_Y_l0 * XI_X_l0 ) + &
!                   UY_l0 * (  4.0_8/3.0_8 * XI_Y_l0 * XI_Y_l0 + XI_X_l0 * XI_X_l0 ) ) + &
!                   MU_l1 * ( UX_l1 * (-2.0_8/3.0_8 * XI_X_l1 * XI_Y_l1 + XI_Y_l1 * XI_X_l1 ) + &
!                   UY_l1 * (  4.0_8/3.0_8 * XI_Y_l1 * XI_Y_l1 + XI_X_l1 * XI_X_l1 ) ) )

!            values_rE(index) = values_rE(index) - 0.5_8 * local_val * &
!                 ( KAPPA_l0 * ( XI_X_l0 * XI_X_l0 + XI_Y_l0 * XI_Y_l0 ) + &
!                   KAPPA_l1 * ( XI_X_l1 * XI_X_l1 + XI_Y_l1 * XI_Y_l1 ) )

            ! ... now work on index at l1
            index = (l1-1)*stencil_size + 1

            ! ... store in array
            values_ru(index) = values_ru(index) + 0.5_8 * local_val * &
                 ( (UX_l0 * ((2.0_8 * MU_l0 + LAMBDA_l0) * XI_X_l0 * XI_X_l0 + MU_l0 * XI_Y_l0 * XI_Y_l0) + &
                    UY_l0 *(( MU_l0 + LAMBDA_l0) * XI_X_l0 * XI_Y_l0)) )

            values_rv(index) = values_rv(index) + 0.5_8 * local_val * &
                 ( (UX_l0 * ((MU_l0 + LAMBDA_l0) * XI_X_l0 * XI_Y_l0) + &
                    UY_l0 * ((2.0_8 * MU_l0 + LAMBDA_l0) * XI_Y_l0 * XI_Y_l0 + MU_l0 * XI_X_l0 * XI_X_l0)) )

            values_rE(index) = values_rE(index) + 0.5_8 * local_val * &
                 ( KAPPA_l0 * ( XI_X_l0 * XI_X_l0 + XI_Y_l0 * XI_Y_l0 ) )

!             values_ru(index) = values_ru(index) +  0.5_8 * local_val * &
!                  (MU_l0 * ( UX_l0 * ( 4.0_8/3.0_8 * XI_X_l0 * XI_X_l0 + XI_Y_l0 * XI_Y_l0 ) + &
!                   UY_l0 * ( -2.0_8/3.0_8 * XI_Y_l0 * XI_X_l0 + XI_X_l0 * XI_Y_l0 ) ) )

!             values_rv(index) = values_rv(index) + 0.5_8 * local_val * &
!                  (MU_l0 * ( UX_l0 * (-2.0_8/3.0_8 * XI_X_l0 * XI_Y_l0 + XI_Y_l0 * XI_X_l0 ) + &
!                   UY_l0 * (  4.0_8/3.0_8 * XI_Y_l0 * XI_Y_l0 + XI_X_l0 * XI_X_l0 ) ) )

!             values_rE(index) = values_rE(index) + 0.5_8 * local_val * &
!                  ( KAPPA_l0 * ( XI_X_l0 * XI_X_l0 + XI_Y_l0 * XI_Y_l0 ))

          end do
        end do
      end do

    End Select

    ! ... deallocate auxiliary arrays for this direction
    deallocate(rhoAux,rhouAux,rhovAux,muAux,lambdaAux,JACAux,XI_X_TILDEAux,XI_Y_TILDEAux)

    Return

  End Subroutine NSImplicit2D

  Subroutine NSImplicitAddBC2D(region, ng, lhs_var, values_r, values_ru, values_rv, values_rE, stencil_size, dtau)

    USE ModGlobal
    USE ModDataStruct
    USE ModDeriv
    USE ModMPI

    Implicit None

    ! ... global variables
    type(t_region), pointer :: region
    type(t_mixt_input), pointer :: input
    type(t_grid), pointer :: grid
    type(t_mixt), pointer :: state
    real(rfreal), pointer :: values_r(:), values_ru(:), values_rv(:), values_rE(:)
    real(rfreal) :: dtau
    integer :: lhs_var, ng, stencil_size

    ! ... local variables
    type(t_patch), pointer :: patch
    integer :: npatch, i, j, k, l0, lp, N(MAX_ND), Np(MAX_ND), index,jj
    real(rfreal) :: gam, gamm1i, RHO, P, T, RHO_P, RHO_T, UX, UY, KE, SAT_MAT(4), INVJAC, rel_pos(MAX_ND), del, eta, sponge_amp

    ! ... current grid
    grid  => region%grid(ng)
    state => region%state(ng)
    input => grid%input

    ! ... loop over all patches and pick the ones that have the same grid
    Do npatch = 1, region%nPatches

      patch => region%patch(npatch)

      ! ... check to see if grid ID matches
      if (patch%gridID /= ng) CYCLE

      ! ... check to see if this boundary is SAT
      if (associated(patch%implicit_sat_mat) .eqv. .false.) CYCLE

      ! ... now that we have a match, add the values of patch%implicit_sat_mat to values_*
      ! ... the SAT boundary conditions are local, so we'll only be adding on the diagonal
      N(:) = 1; Np(:) = 1;
      do j = 1, grid%ND
        N(j)  =  grid%ie(j) -  grid%is(j) + 1
        Np(j) = patch%ie(j) - patch%is(j) + 1
      end do;

      Do k = patch%is(3), patch%ie(3)
        Do j = patch%is(2), patch%ie(2)
          Do i = patch%is(1), patch%ie(1)

            ! ... indices
            l0 = (k- grid%is(3))* N(1)* N(2) + (j- grid%is(2))* N(1) + (i- grid%is(1)+1)
            lp = (k-patch%is(3))*Np(1)*Np(2) + (j-patch%is(2))*Np(1) + (i-patch%is(1)+1)

            ! ... current SAT values
            SAT_MAT(1) = patch%implicit_sat_mat(lp,lhs_var,1)! + patch%implicit_sat_mat_old(lp,lhs_var,1)
            SAT_MAT(2) = patch%implicit_sat_mat(lp,lhs_var,2)! + patch%implicit_sat_mat_old(lp,lhs_var,2)
            SAT_MAT(3) = patch%implicit_sat_mat(lp,lhs_var,3)! + patch%implicit_sat_mat_old(lp,lhs_var,3)
            SAT_MAT(4) = patch%implicit_sat_mat(lp,lhs_var,4)! + patch%implicit_sat_mat_old(lp,lhs_var,4)


            ! ... HYPRE index
            index = (l0-1)*stencil_size + 1
!            if(i>400 .and. j == 100) print*,'myrank,i,j,l0,lp,SAT_MAT,index',region%myrank,i,j,l0,lp,SAT_MAT,index


            ! ... add the values
             values_r(index) =  values_r(index) - dtau * SAT_MAT(1)
            values_ru(index) = values_ru(index) - dtau * SAT_MAT(2)
            values_rv(index) = values_rv(index) - dtau * SAT_MAT(3)
            values_rE(index) = values_rE(index) - dtau * SAT_MAT(4)

          End Do
        End Do
      End Do

    End Do

    ! ... now for the sponge zones
    ! ... loop over all patches and pick the ones that have the same grid
    Do npatch = 1, region%nPatches

      patch => region%patch(npatch)

      ! ... check to see if grid ID matches
      if (patch%gridID /= ng) CYCLE

      ! ... check to see if this boundary is SAT
      if (patch%bcType /= SPONGE) CYCLE

      N(:) = 1; Np(:) = 1;
      do j = 1, grid%ND
        N(j)  =  grid%ie(j) -  grid%is(j) + 1
        Np(j) = patch%ie(j) - patch%is(j) + 1
      end do;

      Do k = patch%is(3), patch%ie(3)
        Do j = patch%is(2), patch%ie(2)
          Do i = patch%is(1), patch%ie(1)

            ! ... indices
            l0 = (k- grid%is(3))* N(1)* N(2) + (j- grid%is(2))* N(1) + (i- grid%is(1)+1)
            lp = (k-patch%is(3))*Np(1)*Np(2) + (j-patch%is(2))*Np(1) + (i-patch%is(1)+1)

            ! ... HYPRE index
            index = (l0-1)*stencil_size + 1

            ! ... intermediate values
            gam     = state%gv(l0,1)
            gamm1i  = 1.0_8 / (gam - 1.0_8)
            P       = state%dv(l0,1)
            T       = state%dv(l0,2)
            RHO_P   = gam * gamm1i / T
            RHO_T   = -P / T * RHO_P
            UX      = state%cv(l0,2) * state%dv(l0,3)
            UY      = state%cv(l0,3) * state%dv(l0,3)
            RHO     = state%cv(l0,1)
            KE      = 0.5_8 * ( UX * UX + UY * UY )
            INVJAC  = grid%INVJAC(l0)

            ! ... distance from boundary to inner sponge start
            rel_pos(1:grid%ND) = patch%sponge_xs(lp,1:grid%ND) - patch%sponge_xe(lp,1:grid%ND)
            del = sqrt(dot_product(rel_pos(1:grid%ND),rel_pos(1:grid%ND)))

            ! ... distance from inner sponge start to point
            rel_pos(1:grid%ND) = patch%sponge_xs(lp,1:grid%ND) - grid%XYZ(l0,1:grid%ND)
            eta = sqrt(dot_product(rel_pos(1:grid%ND),rel_pos(1:grid%ND))) / del

            sponge_amp = 0.0_rfreal
            if (eta .le. 1.0_rfreal) sponge_amp = (input%sponge_amp) * eta**(input%sponge_pow)

            ! ... add the values
            Select Case (lhs_var)
            Case (1)
               values_r(index) =  values_r(index) - dtau * sponge_amp * RHO_P * INVJAC
              values_rE(index) = values_rE(index) - dtau * sponge_amp * RHO_T * INVJAC
            Case (2)
               values_r(index) =  values_r(index) - dtau * sponge_amp * RHO_P * UX * INVJAC
              values_ru(index) = values_ru(index) - dtau * sponge_amp * RHO * INVJAC
              values_rE(index) = values_rE(index) - dtau * sponge_amp * RHO_T * UX * INVJAC
            Case (3)
               values_r(index) =  values_r(index) - dtau * sponge_amp * RHO_P * UY * INVJAC
              values_rv(index) = values_rv(index) - dtau * sponge_amp * RHO * INVJAC
              values_rE(index) = values_rE(index) - dtau * sponge_amp * RHO_T * UY * INVJAC
            Case (4)
               values_r(index) =  values_r(index) - dtau * sponge_amp * (gamm1i + RHO_P * KE) * INVJAC
              values_ru(index) = values_ru(index) - dtau * sponge_amp * RHO * UX * INVJAC
              values_rv(index) = values_rv(index) - dtau * sponge_amp * RHO * UY * INVJAC
              values_rE(index) = values_rE(index) - dtau * sponge_amp * RHO_T * KE * INVJAC
            End Select

          End Do
        End Do
     End Do

   End Do
    
  End Subroutine NSImplicitAddBC2D

  Subroutine NSImplicitInitSoln2D(grid, state, values_r, values_ru, values_rv, values_rE)

    USE ModGlobal
    USE ModDataStruct
    USE ModDeriv
    USE ModMPI

    Implicit None

    ! ... global variables
    type(t_grid), pointer :: grid
    type(t_mixt), pointer :: state
    real(rfreal), pointer :: values_r(:), values_ru(:), values_rv(:), values_rE(:)
  
    ! ... local variables
    type(t_patch), pointer :: patch
    integer :: npatch, i, j, k, l0, lp, N(MAX_ND), Np(MAX_ND), index
    real(rfreal) :: GAM, GAMM1I, RHO, P, T, RHO_P, RHO_T, UX, UY, KE, SPVOL, diff_r, diff_ru, diff_rv, diff_rE

    ! ... convert from Delta Q to Delta Q_p
    Do i = 1, grid%nCells

      ! ... temporary values
      GAM     = state%gv(i,1)
      RHO     = state%cv(i,1)
      SPVOL   = 1.0_8 / RHO
      P       = state%dv(i,1)
      T       = state%dv(i,2)
      UX      = state%cv(i,2) * SPVOL
      UY      = state%cv(i,3) * SPVOL
      KE      = 0.5_8 * (UX * UX + UY * UY)

      diff_r  = (state%cv(i,1) - state%cvOld1(i,1))*grid%INVJAC(i)
      diff_ru = (state%cv(i,2) - state%cvOld1(i,2))*grid%INVJAC(i)
      diff_rv = (state%cv(i,3) - state%cvOld1(i,3))*grid%INVJAC(i)
      diff_rE = (state%cv(i,4) - state%cvOld1(i,4))*grid%INVJAC(i)



      ! diff_r  = 0.0_8
      ! diff_ru = 0.0_8
      ! diff_rv = 0.0_8
      ! diff_rE = 0.0_8

       values_r(i) = (GAM-1.0_8) * KE * diff_r + UX * (1.0_8-GAM) * diff_ru + UY * (1.0_8-GAM) * diff_rv + (GAM-1.0_8) * diff_rE
      values_ru(i) = (-UX * SPVOL) * diff_r + SPVOL * diff_ru
      values_rv(i) = (-UY * SPVOL) * diff_r + SPVOL * diff_rv
      values_rE(i) = (GAM * KE - T) * SPVOL * diff_r - (GAM * UX * SPVOL) * diff_ru - (GAM * UY * SPVOL) * diff_rv + GAM * SPVOL * diff_rE

    End Do

  End Subroutine NSImplicitInitSoln2D
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  !                                                                            !
  !                         3DImplicit Subroutines                             ! 
  !                                                                            !
  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

  Subroutine NSImplicit3D(region, ng, lhs_var, values_r, values_ru, values_rv, values_rw, values_rE, stencil_size, dtau)

    USE ModGlobal
    USE ModDataStruct
    USE ModDeriv
    USE ModMPI
    USE ModNavierStokesImplicitSATArtDiss

    Implicit None

    ! ... global variables
    type(t_region), pointer :: region
    type(t_mixt_input), pointer :: input
    type(t_grid), pointer :: grid
    type(t_mixt), pointer :: state
    real(rfreal), pointer :: values_r(:), values_ru(:), values_rv(:), values_rw(:), values_rE(:)
    real(rfreal) :: dtau
    integer :: lhs_var

    ! ... local variables
    integer :: ng, k, j, i, ndim, npart, my_part, local_ind, l0, l1, order, dir, opID, Nc, Ncg, ierr
    integer :: is(MAX_ND), ie(MAX_ND), varTypes(MAX_ND+2)
    integer :: icomm, igrid, min_stencil_start, max_stencil_end, stencil_size, rhs_var, lhs_var_copy
    integer :: ii0, jj0, kk0, ii1, jj1, kk1, N1, N2, N3, index, N1g, N2g, N3g, ii, jj, kk
    real(rfreal) :: RHO_P, RHO_T, gam, gamm1i, RHOE_P, RHOE_T, RHO, KE, P, T, RHOE, local_val, jac
    real(rfreal) :: UX, UY, UZ, RHOE_U, RHOE_V, RHOE_W, XI_X, XI_Y, XI_Z
    real(rfreal) :: PC(5), MI2, MMIN2, MU2, SNDSPD2, SNDSPD, MP2, EPS_P, RHO_PP, H0, H_T, H_P, LU, time_fac, MU, KAPPA, LAMBDA
    real(rfreal) :: XI_X_l0, XI_Y_l0, XI_Z_l0, MU_l0, JAC_l0, UX_l0, UY_l0, UZ_l0, KAPPA_l0, LAMBDA_l0, RHO_l0
    real(rfreal) :: XI_X_l1, XI_Y_l1, XI_Z_l1, MU_l1, JAC_l1, UX_l1, UY_l1, UZ_l1, KAPPA_l1, LAMBDA_l1, RHO_l1

    real(rfreal), pointer :: gamAux(:),pAux(:),TAux(:),rhoAux(:),rhouAux(:),rhovAux(:), rhowAux(:), rhoEAux(:),JACAux(:)
    real(rfreal), pointer :: XI_X_TILDEAux(:),XI_Y_TILDEAux(:), XI_Z_TILDEAux(:), muAux(:), kappaAux(:), lambdaAux(:)


    ! ... simplicity
    grid  => region%grid(ng)
    state => region%state(ng)
    input => grid%input
    
    ! ... initialize arrays
    Do i = 1, grid%nCells*stencil_size
      values_r(i)  = 0.0_8
      values_ru(i) = 0.0_8
      values_rv(i) = 0.0_8
      values_rw(i) = 0.0_8
      values_rE(i) = 0.0_8
    End Do

    ! ... x-direction, first derivatives
    dir   = 1
    order = 1
    opID  = input%iFirstDerivImplicit

    ! ... local partition size
    N1 = grid%ie(1)-grid%is(1)+1
    N2 = grid%ie(2)-grid%is(2)+1
    N3 = grid%ie(3)-grid%is(3)+1

    ! ... partition size including ghost
    N1g = grid%ie(1)-grid%is(1)+1+SUM(grid%nGhostRHS(dir,:)) ! (MIN(grid%nGhostRHS(dir,1),1)+MIN(grid%nGhostRHS(dir,2),1))*input%overlap(opID)
    N2g = grid%ie(2)-grid%is(2)+1
    N3g = grid%ie(3)-grid%is(3)+1

    ! ... get ghost cell data
    Nc = grid%nCells
    Ncg = Nc + SUM(grid%nGhostRHS(dir,:))*grid%vdsGhost(dir)

        ! ... allocate a lot of arrays
    allocate(gamAux(Ncg),pAux(Ncg),TAux(Ncg),rhoAux(Ncg),rhouAux(Ncg),rhovAux(Ncg),rhowAux(Ncg),rhoEAux(Ncg))
    allocate(JACAux(Ncg),XI_X_TILDEAux(Ncg),XI_Y_TILDEAux(Ncg),XI_Z_TILDEAux(Ncg))

    ! ... initialize auxiliary vectors
    gamAux(:) = 0.0_rfreal
    pAux(:) = 0.0_rfreal
    TAux(:) = 0.0_rfreal
    rhoAux(:) = 0.0_rfreal
    rhouAux(:) = 0.0_rfreal
    rhovAux(:) = 0.0_rfreal
    rhowAux(:) = 0.0_rfreal
    rhoEAux(:) = 0.0_rfreal
    JACAux(:) = 0.0_rfreal
    XI_X_TILDEAux(:) = 0.0_rfreal
    XI_Y_TILDEAux(:) = 0.0_rfreal
    XI_Z_TILDEAux(:) = 0.0_rfreal



!    call mpi_barrier(mycomm,ierr)
!    if(region%myrank == 0) write(*,*) 'All are populating auxiliary matrices, order,dir = ',order,dir
    ! ... populate auxiliary vectors with local data
    Do i = 1, Nc
       ! ... gamma
       gamAux(grid%cell2ghost(i,dir)) = state%gv(i,1)
       ! ... pressure
       pAux(grid%cell2ghost(i,dir)) = state%dv(i,1)
       ! ... temperature
       TAux(grid%cell2ghost(i,dir)) = state%dv(i,2)
       ! ... density
       rhoAux(grid%cell2ghost(i,dir)) = state%cv(i,1)
       ! ... x-momentum
       rhouAux(grid%cell2ghost(i,dir)) = state%cv(i,2)
       ! ... y-momentum
       rhovAux(grid%cell2ghost(i,dir)) = state%cv(i,3)
       ! ... z-momentum
       rhowAux(grid%cell2ghost(i,dir)) = state%cv(i,4)
       ! ... energy
       rhoEAux(grid%cell2ghost(i,dir)) = state%cv(i,5)
       ! ... Jacobian
       JACAux(grid%cell2ghost(i,dir)) = grid%JAC(i)
       ! ... XI_X_TILDE
       XI_X_TILDEAux(grid%cell2ghost(i,dir)) = grid%MT1(i, region%global%t2Map(dir,1))
       ! ... XI_Y_TILDE
       XI_Y_TILDEAux(grid%cell2ghost(i,dir)) = grid%MT1(i, region%global%t2Map(dir,2))
       ! ... XI_Z_TILDE
       XI_Z_TILDEAux(grid%cell2ghost(i,dir)) = grid%MT1(i, region%global%t2Map(dir,3))
    End Do

    call mpi_barrier(mycomm,ierr)
   if(region%myrank == 0) write(*,*) 'All are starting Ghost_Cell_Exchange, order,dir = ',order,dir

     ! do i = 1,Ncg
     !    if(region%myrank == 1) write(*,*) 'i,gamAux_2',i,gamAux(i),gamAuxAux(i)
     ! end do
    ! ... get ghost data
    call Ghost_Cell_Exchange(region,ng,dir, gamAux)
    call Ghost_Cell_Exchange(region,ng,dir,   pAux)
    call Ghost_Cell_Exchange(region,ng,dir,   TAux)
    call Ghost_Cell_Exchange(region,ng,dir, rhoAux)
    call Ghost_Cell_Exchange(region,ng,dir,rhouAux)
    call Ghost_Cell_Exchange(region,ng,dir,rhovAux)
    call Ghost_Cell_Exchange(region,ng,dir,rhowAux)
    call Ghost_Cell_Exchange(region,ng,dir,rhoEAux)
    call Ghost_Cell_Exchange(region,ng,dir, JACAux)
    call Ghost_Cell_Exchange(region,ng,dir,XI_X_TILDEAux)
    call Ghost_Cell_Exchange(region,ng,dir,XI_Y_TILDEAux)
    call Ghost_Cell_Exchange(region,ng,dir,XI_Z_TILDEAux)

!    call mpi_barrier(mycomm,ierr)
!    if(region%myrank == 0) write(*,*) 'All are done with Ghost_Cell_Exchange, order,dir = ',order,dir


    SELECT CASE (lhs_var)
    CASE (1)

      ! ... A_p {delta_\xi} where
      !
      !       | u rho_p              rho                            0              u rho_T     |
      ! A_p = | rho_p u^2 + 1        2 rho u                        0              u^2 rho_T   |
      !       | rho_p u v            rho v                          rho u          u v rho_T   |
      !     = | [(rho E)_p + 1] u    [(rho E + p) + u (rho E)_u]    u (rho E)_v    u (rho E)_T |
      !
      !
      ! ... inviscid fluxes
      do k = 1, grid%vdsGhost(dir)
        do j = 1, grid%B(opID,dir)%n
          do i = grid%B(opID,dir)%row_ptr(k,j), grid%B(opID,dir)%row_ptr(k,j+1)-1

            ! ... l0 and l1 indices refer to BUFFERED data
            local_val = grid%B(opID,dir)%val(i,k)
            local_ind = grid%B(opID,dir)%col_ind(i,k)
            l0        = grid%vec_indGhost(k,local_ind,dir)
            l1        = grid%vec_indGhost(k,        j,dir)
    
            ! ... convert from linear to triplet indices
            ! ... rhs index (buffered data)
            ii0 = mod(l0-1,N1g)+1
            jj0 = mod((l0-ii0)/N1g,N2g)+1
            kk0 = (l0-ii0-(jj0-1)*N1g)/(N1g*N2g)+1

            ! ... lhs index (buffered data)
            ii1 = mod(l1-1,N1g)+1
            jj1 = mod((l1-ii1)/N1g,N2g)+1
            kk1 = (l1-ii1-(jj1-1)*N1g)/(N1g*N2g)+1

            ! ... convert to global indices
            ii0 = ii0 + grid%is(1) - 1 - grid%nGhostRHS(dir,1)
            jj0 = jj0 + grid%is(2) - 1
            kk0 = kk0 + grid%is(3) - 1
            ii1 = ii1 + grid%is(1) - 1 - grid%nGhostRHS(dir,1)
            jj1 = jj1 + grid%is(2) - 1
            kk1 = kk1 + grid%is(3) - 1

            if (ii1 < grid%is(1) .OR. ii1 > grid%ie(1)) CYCLE

            ! ... convert l1 back to non-buffered counting
            l1 = (kk1-grid%is(3))*N1*N2 + (jj1-grid%is(2))*N1 + (ii1-grid%is(1)+1)

            ! ... intermediate values
            gam     = gamAux(l0)
            gamm1i  = 1.0_8 / (gam - 1.0_8)
            P       = pAux(l0)
            T       = TAux(l0)
            RHO_P   = gam * gamm1i / T
            RHO_T   = -P / T * RHO_P
            RHO     = rhoAux(l0)
            UX      = rhouAux(l0) / RHO
            UY      = rhovAux(l0) / RHO
            UZ      = rhowAux(l0) / RHO
            RHOE_U  = rhouAux(l0)
            RHOE_V  = rhovAux(l0)
            RHOE_W  = rhowAux(l0)
            RHOE    = rhoeAux(l0)
            KE      = 0.5_8 * (UX*UX + UY*UY + UZ*UZ)
            RHOE_P  = gamm1i + KE * RHO_P
            RHOE_T  = KE * RHO_T
            JAC     = JACAux(l0)
            XI_X    = XI_X_TILDEAux(l0) * JAC
            XI_Y    = XI_Y_TILDEAux(l0) * JAC
            XI_Z    = XI_Z_TILDEAux(l0) * JAC
            
            


!             ! ... intermediate values
!             gam     = state%gv(l1,1)
!             gamm1i  = 1.0_8 / (gam - 1.0_8)
!             P       = state%dv(l1,1)
!             T       = state%dv(l1,2)
!             RHO_P   = gam * gamm1i / T
!             RHO_T   = -P / T * RHO_P
!             UX      = state%cv(l1,2) * state%dv(l1,3)
!             UY      = state%cv(l1,3) * state%dv(l1,3)
!             UZ      = state%cv(l1,4) * state%dv(l1,3)
!             RHO     = state%cv(l1,1)
!             RHOE_U  = UX * RHO
!             RHOE_V  = UY * RHO
!             RHOE_W  = UZ * RHO
!             KE      = 0.5_8 * (UX * UX + UY * UY + UZ * UZ)
!             RHOE_P  = gamm1i + KE * RHO_P
!             RHOE_T  = KE * RHO_T
!             JAC     = grid%JAC(l1)
!             XI_X    = grid%MT1(l1,region%global%t2Map(1,1)) * JAC
!             XI_Y    = grid%MT1(l1,region%global%t2Map(1,2)) * JAC
!             XI_Z    = grid%MT1(l1,region%global%t2Map(1,3)) * JAC

            ! ... index
            index = (l1-1)*stencil_size + input%HYPREStencilIndex(opID, ii0-ii1, jj0-jj1, kk0-kk1) + 1
!!$            if (region%myrank == 1) print *, 'center  point ', ii1, jj1, kk1
!!$            if (region%myrank == 1) print *, '  stencil point ', ii0, jj0, kk0
!!$            if (region%myrank == 1) print *, '  offset        ', ii0-ii1, jj0-jj1, kk0-kk1
!!$            if (region%myrank == 1) print *, '  local_val     ', local_val

            ! ... store in array
             values_r(index) =  values_r(index) + local_val * ( RHO_P * (UX * XI_X + UY * XI_Y + UZ * XI_Z) )
            values_ru(index) = values_ru(index) + local_val * ( RHO * XI_X )
            values_rv(index) = values_rv(index) + local_val * ( RHO * XI_Y )
            values_rw(index) = values_rw(index) + local_val * ( RHO * XI_Z )
            values_rE(index) = values_rE(index) + local_val * ( RHO_T * (UX * XI_X + UY * XI_Y + UZ * XI_Z) )
         
            ! dF(l1)    = dF(l1) + local_val * F(l0)
          end do
        end do
      end do

    CASE (2)

      do k = 1, grid%vdsGhost(dir)
        do j = 1, grid%B(opID,dir)%n
          do i = grid%B(opID,dir)%row_ptr(k,j), grid%B(opID,dir)%row_ptr(k,j+1)-1

            ! ... l0 and l1 indices refer to BUFFERED data
            local_val = grid%B(opID,dir)%val(i,k)
            local_ind = grid%B(opID,dir)%col_ind(i,k)
            l0        = grid%vec_indGhost(k,local_ind,dir)
            l1        = grid%vec_indGhost(k,        j,dir)
    
            ! ... convert from linear to triplet indices
            ! ... rhs index (buffered data)
            ii0 = mod(l0-1,N1g)+1
            jj0 = mod((l0-ii0)/N1g,N2g)+1
            kk0 = (l0-ii0-(jj0-1)*N1g)/(N1g*N2g)+1

            ! ... lhs index (buffered data)
            ii1 = mod(l1-1,N1g)+1
            jj1 = mod((l1-ii1)/N1g,N2g)+1
            kk1 = (l1-ii1-(jj1-1)*N1g)/(N1g*N2g)+1

            ! ... convert to global indices
            ii0 = ii0 + grid%is(1) - 1 - grid%nGhostRHS(dir,1)
            jj0 = jj0 + grid%is(2) - 1
            kk0 = kk0 + grid%is(3) - 1
            ii1 = ii1 + grid%is(1) - 1 - grid%nGhostRHS(dir,1)
            jj1 = jj1 + grid%is(2) - 1
            kk1 = kk1 + grid%is(3) - 1

            if (ii1 < grid%is(1) .OR. ii1 > grid%ie(1)) CYCLE

            ! ... convert l1 back to non-buffered counting
            l1 = (kk1-grid%is(3))*N1*N2 + (jj1-grid%is(2))*N1 + (ii1-grid%is(1)+1)

            ! ... intermediate values
            gam     = gamAux(l0)
            gamm1i  = 1.0_8 / (gam - 1.0_8)
            P       = pAux(l0)
            T       = TAux(l0)
            RHO_P   = gam * gamm1i / T
            RHO_T   = -P / T * RHO_P
            RHO     = rhoAux(l0)
            UX      = rhouAux(l0) / RHO
            UY      = rhovAux(l0) / RHO
            UZ      = rhowAux(l0) / RHO
            RHOE_U  = rhouAux(l0)
            RHOE_V  = rhovAux(l0)
            RHOE_W  = rhowAux(l0)
            RHOE    = rhoeAux(l0)
            KE      = 0.5_8 * (UX*UX + UY*UY + UZ*UZ)
            RHOE_P  = gamm1i + KE * RHO_P
            RHOE_T  = KE * RHO_T
            JAC     = JACAux(l0)
            XI_X    = XI_X_TILDEAux(l0) * JAC
            XI_Y    = XI_Y_TILDEAux(l0) * JAC
            XI_Z    = XI_Z_TILDEAux(l0) * JAC


 
!             ! ... intermediate values
!             gam     = state%gv(l1,1)
!             gamm1i  = 1.0_8 / (gam - 1.0_8)
!             P       = state%dv(l1,1)
!             T       = state%dv(l1,2)
!             RHO_P   = gam * gamm1i / T
!             RHO_T   = -P / T * RHO_P
!             UX      = state%cv(l1,2) * state%dv(l1,3)
!             UY      = state%cv(l1,3) * state%dv(l1,3)
!             UZ      = state%cv(l1,4) * state%dv(l1,3)
!             RHO     = state%cv(l1,1)
!             RHOE_U  = UX * RHO
!             RHOE_V  = UY * RHO
!             RHOE_W  = UZ * RHO
!             KE      = 0.5_8 * (UX * UX + UY * UY + UZ * UZ)
!             RHOE_P  = gamm1i + KE * RHO_P
!             RHOE_T  = KE * RHO_T
!             JAC     = grid%JAC(l1)
!             XI_X    = grid%MT1(l1,region%global%t2Map(1,1)) * JAC
!             XI_Y    = grid%MT1(l1,region%global%t2Map(1,2)) * JAC
!             XI_Z    = grid%MT1(l1,region%global%t2Map(1,3)) * JAC


            ! ... index
            index = (l1-1)*stencil_size + input%HYPREStencilIndex(opID, ii0-ii1, jj0-jj1, kk0-kk1) + 1

            ! ... store in array
             values_r(index) =  values_r(index) + local_val * ( RHO_P * UX * ( UX * XI_X + UY * XI_Y + UZ * XI_Z ) + XI_X )
            values_ru(index) = values_ru(index) + local_val * ( RHO * ( 2.0_8 * UX * XI_X + UY * XI_Y + UZ * XI_Z ) )
            values_rv(index) = values_rv(index) + local_val * ( RHO * UX * XI_Y )
            values_rw(index) = values_rw(index) + local_val * ( RHO * UX * XI_Z )
            values_rE(index) = values_rE(index) + local_val * ( RHO_T * UX * ( UX * XI_X + UY * XI_Y + UZ * XI_Z ) )
         
            ! dF(l1)    = dF(l1) + local_val * F(l0)
          end do
        end do
      end do

    CASE (3)

      do k = 1, grid%vdsGhost(dir)
        do j = 1, grid%B(opID,dir)%n
          do i = grid%B(opID,dir)%row_ptr(k,j), grid%B(opID,dir)%row_ptr(k,j+1)-1

            ! ... l0 and l1 indices refer to BUFFERED data
            local_val = grid%B(opID,dir)%val(i,k)
            local_ind = grid%B(opID,dir)%col_ind(i,k)
            l0        = grid%vec_indGhost(k,local_ind,dir)
            l1        = grid%vec_indGhost(k,        j,dir)
    
            ! ... convert from linear to triplet indices
            ! ... rhs index (buffered data)
            ii0 = mod(l0-1,N1g)+1
            jj0 = mod((l0-ii0)/N1g,N2g)+1
            kk0 = (l0-ii0-(jj0-1)*N1g)/(N1g*N2g)+1

            ! ... lhs index (buffered data)
            ii1 = mod(l1-1,N1g)+1
            jj1 = mod((l1-ii1)/N1g,N2g)+1
            kk1 = (l1-ii1-(jj1-1)*N1g)/(N1g*N2g)+1

            ! ... convert to global indices
            ii0 = ii0 + grid%is(1) - 1 - grid%nGhostRHS(dir,1)
            jj0 = jj0 + grid%is(2) - 1
            kk0 = kk0 + grid%is(3) - 1
            ii1 = ii1 + grid%is(1) - 1 - grid%nGhostRHS(dir,1)
            jj1 = jj1 + grid%is(2) - 1
            kk1 = kk1 + grid%is(3) - 1

            if (ii1 < grid%is(1) .OR. ii1 > grid%ie(1)) CYCLE

            ! ... convert l1 back to non-buffered counting
            l1 = (kk1-grid%is(3))*N1*N2 + (jj1-grid%is(2))*N1 + (ii1-grid%is(1)+1)

            ! ... intermediate values
            gam     = gamAux(l0)
            gamm1i  = 1.0_8 / (gam - 1.0_8)
            P       = pAux(l0)
            T       = TAux(l0)
            RHO_P   = gam * gamm1i / T
            RHO_T   = -P / T * RHO_P
            RHO     = rhoAux(l0)
            UX      = rhouAux(l0) / RHO
            UY      = rhovAux(l0) / RHO
            UZ      = rhowAux(l0) / RHO
            RHOE_U  = rhouAux(l0)
            RHOE_V  = rhovAux(l0)
            RHOE_W  = rhowAux(l0)
            RHOE    = rhoeAux(l0)
            KE      = 0.5_8 * (UX*UX + UY*UY + UZ*UZ)
            RHOE_P  = gamm1i + KE * RHO_P
            RHOE_T  = KE * RHO_T
            JAC     = JACAux(l0)
            XI_X    = XI_X_TILDEAux(l0) * JAC
            XI_Y    = XI_Y_TILDEAux(l0) * JAC
            XI_Z    = XI_Z_TILDEAux(l0) * JAC

 
!             ! ... intermediate values
!             gam     = state%gv(l1,1)
!             gamm1i  = 1.0_8 / (gam - 1.0_8)
!             P       = state%dv(l1,1)
!             T       = state%dv(l1,2)
!             RHO_P   = gam * gamm1i / T
!             RHO_T   = -P / T * RHO_P
!             UX      = state%cv(l1,2) * state%dv(l1,3)
!             UY      = state%cv(l1,3) * state%dv(l1,3)
!             UZ      = state%cv(l1,4) * state%dv(l1,3)
!             RHO     = state%cv(l1,1)
!             RHOE_U  = UX * RHO
!             RHOE_V  = UY * RHO
!             RHOE_W  = UZ * RHO
!             KE      = 0.5_8 * (UX * UX + UY * UY + UZ * UZ)
!             RHOE_P  = gamm1i + KE * RHO_P
!             RHOE_T  = KE * RHO_T
!             JAC     = grid%JAC(l1)
!             XI_X    = grid%MT1(l1,region%global%t2Map(1,1)) * JAC
!             XI_Y    = grid%MT1(l1,region%global%t2Map(1,2)) * JAC
!             XI_Z    = grid%MT1(l1,region%global%t2Map(1,3)) * JAC

            ! ... index
            index = (l1-1)*stencil_size + input%HYPREStencilIndex(opID, ii0-ii1, jj0-jj1, kk0-kk1) + 1

            ! ... store in array
             values_r(index) =  values_r(index) + local_val * ( RHO_P * UY * ( UX * XI_X + UY * XI_Y + UZ * XI_Z ) + XI_Y )
            values_ru(index) = values_ru(index) + local_val * ( RHO * UY * XI_X )
            values_rv(index) = values_rv(index) + local_val * ( RHO * ( UX * XI_X + 2.0_8 * UY * XI_Y + UZ * XI_Z ) )
            values_rw(index) = values_rw(index) + local_val * ( RHO * UY * XI_Z )
            values_rE(index) = values_rE(index) + local_val * ( RHO_T * UY * ( UX * XI_X + UY * XI_Y + UZ * XI_Z ) )
         
            ! dF(l1)    = dF(l1) + local_val * F(l0)
          end do
        end do
      end do

    CASE (4)

      do k = 1, grid%vdsGhost(dir)
        do j = 1, grid%B(opID,dir)%n
          do i = grid%B(opID,dir)%row_ptr(k,j), grid%B(opID,dir)%row_ptr(k,j+1)-1

            ! ... l0 and l1 indices refer to BUFFERED data
            local_val = grid%B(opID,dir)%val(i,k)
            local_ind = grid%B(opID,dir)%col_ind(i,k)
            l0        = grid%vec_indGhost(k,local_ind,dir)
            l1        = grid%vec_indGhost(k,        j,dir)
    
            ! ... convert from linear to triplet indices
            ! ... rhs index (buffered data)
            ii0 = mod(l0-1,N1g)+1
            jj0 = mod((l0-ii0)/N1g,N2g)+1
            kk0 = (l0-ii0-(jj0-1)*N1g)/(N1g*N2g)+1

            ! ... lhs index (buffered data)
            ii1 = mod(l1-1,N1g)+1
            jj1 = mod((l1-ii1)/N1g,N2g)+1
            kk1 = (l1-ii1-(jj1-1)*N1g)/(N1g*N2g)+1

            ! ... convert to global indices
            ii0 = ii0 + grid%is(1) - 1 - grid%nGhostRHS(dir,1)
            jj0 = jj0 + grid%is(2) - 1
            kk0 = kk0 + grid%is(3) - 1
            ii1 = ii1 + grid%is(1) - 1 - grid%nGhostRHS(dir,1)
            jj1 = jj1 + grid%is(2) - 1
            kk1 = kk1 + grid%is(3) - 1

            if (ii1 < grid%is(1) .OR. ii1 > grid%ie(1)) CYCLE

            ! ... convert l1 back to non-buffered counting
            l1 = (kk1-grid%is(3))*N1*N2 + (jj1-grid%is(2))*N1 + (ii1-grid%is(1)+1)

            ! ... intermediate values
            gam     = gamAux(l0)
            gamm1i  = 1.0_8 / (gam - 1.0_8)
            P       = pAux(l0)
            T       = TAux(l0)
            RHO_P   = gam * gamm1i / T
            RHO_T   = -P / T * RHO_P
            RHO     = rhoAux(l0)
            UX      = rhouAux(l0) / RHO
            UY      = rhovAux(l0) / RHO
            UZ      = rhowAux(l0) / RHO
            RHOE_U  = rhouAux(l0)
            RHOE_V  = rhovAux(l0)
            RHOE_W  = rhowAux(l0)
            RHOE    = rhoeAux(l0)
            KE      = 0.5_8 * (UX*UX + UY*UY + UZ*UZ)
            RHOE_P  = gamm1i + KE * RHO_P
            RHOE_T  = KE * RHO_T
            JAC     = JACAux(l0)
            XI_X    = XI_X_TILDEAux(l0) * JAC
            XI_Y    = XI_Y_TILDEAux(l0) * JAC
            XI_Z    = XI_Z_TILDEAux(l0) * JAC

 
!             ! ... intermediate values
!             gam     = state%gv(l1,1)
!             gamm1i  = 1.0_8 / (gam - 1.0_8)
!             P       = state%dv(l1,1)
!             T       = state%dv(l1,2)
!             RHO_P   = gam * gamm1i / T
!             RHO_T   = -P / T * RHO_P
!             UX      = state%cv(l1,2) * state%dv(l1,3)
!             UY      = state%cv(l1,3) * state%dv(l1,3)
!             UZ      = state%cv(l1,4) * state%dv(l1,3)
!             RHO     = state%cv(l1,1)
!             RHOE_U  = UX * RHO
!             RHOE_V  = UY * RHO
!             RHOE_W  = UZ * RHO
!             KE      = 0.5_8 * (UX * UX + UY * UY + UZ * UZ)
!             RHOE_P  = gamm1i + KE * RHO_P
!             RHOE_T  = KE * RHO_T
!             JAC     = grid%JAC(l1)
!             XI_X    = grid%MT1(l1,region%global%t2Map(1,1)) * JAC
!             XI_Y    = grid%MT1(l1,region%global%t2Map(1,2)) * JAC
!             XI_Z    = grid%MT1(l1,region%global%t2Map(1,3)) * JAC

            ! ... index
            index = (l1-1)*stencil_size + input%HYPREStencilIndex(opID, ii0-ii1, jj0-jj1, kk0-kk1) + 1

            ! ... store in array
             values_r(index) =  values_r(index) + local_val * ( RHO_P * UZ * ( UX * XI_X + UY * XI_Y + UZ * XI_Z ) + XI_Z )
            values_ru(index) = values_ru(index) + local_val * ( RHO * UZ * XI_X )
            values_rv(index) = values_rv(index) + local_val * ( RHO * UZ * XI_Y )
            values_rw(index) = values_rw(index) + local_val * ( RHO * ( UX * XI_X + UY * XI_Y + 2.0_8 * UZ * XI_Z ) )
            values_rE(index) = values_rE(index) + local_val * ( RHO_T * UZ * ( UX * XI_X + UY * XI_Y + UZ * XI_Z ) )
         
            ! dF(l1)    = dF(l1) + local_val * F(l0)
          end do
        end do
      end do

    CASE (5)

      do k = 1, grid%vdsGhost(dir)
        do j = 1, grid%B(opID,dir)%n
          do i = grid%B(opID,dir)%row_ptr(k,j), grid%B(opID,dir)%row_ptr(k,j+1)-1

            ! ... l0 and l1 indices refer to BUFFERED data
            local_val = grid%B(opID,dir)%val(i,k)
            local_ind = grid%B(opID,dir)%col_ind(i,k)
            l0        = grid%vec_indGhost(k,local_ind,dir)
            l1        = grid%vec_indGhost(k,        j,dir)
    
            ! ... convert from linear to triplet indices
            ! ... rhs index (buffered data)
            ii0 = mod(l0-1,N1g)+1
            jj0 = mod((l0-ii0)/N1g,N2g)+1
            kk0 = (l0-ii0-(jj0-1)*N1g)/(N1g*N2g)+1

            ! ... lhs index (buffered data)
            ii1 = mod(l1-1,N1g)+1
            jj1 = mod((l1-ii1)/N1g,N2g)+1
            kk1 = (l1-ii1-(jj1-1)*N1g)/(N1g*N2g)+1

            ! ... convert to global indices
            ii0 = ii0 + grid%is(1) - 1 - grid%nGhostRHS(dir,1)
            jj0 = jj0 + grid%is(2) - 1
            kk0 = kk0 + grid%is(3) - 1
            ii1 = ii1 + grid%is(1) - 1 - grid%nGhostRHS(dir,1)
            jj1 = jj1 + grid%is(2) - 1
            kk1 = kk1 + grid%is(3) - 1

            if (ii1 < grid%is(1) .OR. ii1 > grid%ie(1)) CYCLE

            ! ... convert l1 back to non-buffered counting
            l1 = (kk1-grid%is(3))*N1*N2 + (jj1-grid%is(2))*N1 + (ii1-grid%is(1)+1)


            ! ... intermediate values
            gam     = gamAux(l0)
            gamm1i  = 1.0_8 / (gam - 1.0_8)
            P       = pAux(l0)
            T       = TAux(l0)
            RHO_P   = gam * gamm1i / T
            RHO_T   = -P / T * RHO_P
            RHO     = rhoAux(l0)
            UX      = rhouAux(l0) / RHO
            UY      = rhovAux(l0) / RHO
            UZ      = rhowAux(l0) / RHO
            RHOE_U  = rhouAux(l0)
            RHOE_V  = rhovAux(l0)
            RHOE_W  = rhowAux(l0)
            RHOE    = rhoeAux(l0)
            KE      = 0.5_8 * (UX*UX + UY*UY + UZ*UZ)
            RHOE_P  = gamm1i + KE * RHO_P
            RHOE_T  = KE * RHO_T
            JAC     = JACAux(l0)
            XI_X    = XI_X_TILDEAux(l0) * JAC
            XI_Y    = XI_Y_TILDEAux(l0) * JAC
            XI_Z    = XI_Z_TILDEAux(l0) * JAC

!             ! ... intermediate values
!             gam     = state%gv(l1,1)
!             gamm1i  = 1.0_8 / (gam - 1.0_8)
!             P       = state%dv(l1,1)
!             T       = state%dv(l1,2)
!             RHO_P   = gam * gamm1i / T
!             RHO_T   = -P / T * RHO_P
!             UX      = state%cv(l1,2) * state%dv(l1,3)
!             UY      = state%cv(l1,3) * state%dv(l1,3)
!             UZ      = state%cv(l1,4) * state%dv(l1,3)
!             RHO     = state%cv(l1,1)
!             RHOE_U  = UX * RHO
!             RHOE_V  = UY * RHO
!             RHOE_W  = UZ * RHO
!             KE      = 0.5_8 * (UX * UX + UY * UY + UZ * UZ)
!             RHOE_P  = gamm1i + KE * RHO_P
!             RHOE_T  = KE * RHO_T
!             RHOE    = state%cv(l1,5)
!             JAC     = grid%JAC(l1)
!             XI_X    = grid%MT1(l1,region%global%t2Map(1,1)) * JAC
!             XI_Y    = grid%MT1(l1,region%global%t2Map(1,2)) * JAC
!             XI_Z    = grid%MT1(l1,region%global%t2Map(1,3)) * JAC

            ! ... index
            index = (l1-1)*stencil_size + input%HYPREStencilIndex(opID, ii0-ii1, jj0-jj1, kk0-kk1) + 1
            ! print *, ii0-ii1, jj0-jj1, kk0-kk1

            ! ... store in array
             values_r(index) =  values_r(index) + local_val * ( ( RHOE_P + 1.0_8 ) * ( UX * XI_X + UY * XI_Y + UZ * XI_Z ) )
            values_ru(index) = values_ru(index) + local_val * ( ( RHOE + P ) * XI_X + RHOE_U * ( UX * XI_X + UY * XI_Y + UZ * XI_Z ) )
            values_rv(index) = values_rv(index) + local_val * ( ( RHOE + P ) * XI_Y + RHOE_V * ( UX * XI_X + UY * XI_Y + UZ * XI_Z ) )
            values_rw(index) = values_rw(index) + local_val * ( ( RHOE + P ) * XI_Z + RHOE_W * ( UX * XI_X + UY * XI_Y + UZ * XI_Z ) )
            values_rE(index) = values_rE(index) + local_val * ( RHOE_T * ( UX * XI_X + UY * XI_Y + UZ * XI_Z ) )
         
            ! dF(l1)    = dF(l1) + local_val * F(l0)
          end do
        end do
      end do

    END SELECT

    ! ... deallocate auxiliary matrices for this direction
    deallocate(gamAux,pAux,TAux,rhoAux,rhouAux,rhovAux,rhowAux,rhoEAux,JACAux,XI_X_TILDEAux,XI_Y_TILDEAux,XI_Z_TILDEAux)



    ! ... y-direction, first derivatives
    dir   = 2
    order = 1
    opID  = input%iFirstDerivImplicit

    ! ... local partition size
    N1 = grid%ie(1)-grid%is(1)+1
    N2 = grid%ie(2)-grid%is(2)+1
    N3 = grid%ie(3)-grid%is(3)+1

    ! ... partition size including ghost
    N1g = grid%ie(1)-grid%is(1)+1
    N2g = grid%ie(2)-grid%is(2)+1+SUM(grid%nGhostRHS(dir,:))
    N3g = grid%ie(3)-grid%is(3)+1

    ! ... get ghost cell data
    Nc = grid%nCells
    Ncg = Nc + SUM(grid%nGhostRHS(dir,:))*grid%vdsGhost(dir)

        ! ... allocate a lot of arrays
    allocate(gamAux(Ncg),pAux(Ncg),TAux(Ncg),rhoAux(Ncg),rhouAux(Ncg),rhovAux(Ncg),rhowAux(Ncg),rhoEAux(Ncg))
    allocate(JACAux(Ncg),XI_X_TILDEAux(Ncg),XI_Y_TILDEAux(Ncg),XI_Z_TILDEAux(Ncg))

    ! ... initialize auxiliary vectors
    gamAux(:) = 0.0_rfreal
    pAux(:) = 0.0_rfreal
    TAux(:) = 0.0_rfreal
    rhoAux(:) = 0.0_rfreal
    rhouAux(:) = 0.0_rfreal
    rhovAux(:) = 0.0_rfreal
    rhowAux(:) = 0.0_rfreal
    rhoEAux(:) = 0.0_rfreal
    JACAux(:) = 0.0_rfreal
    XI_X_TILDEAux(:) = 0.0_rfreal
    XI_Y_TILDEAux(:) = 0.0_rfreal
    XI_Z_TILDEAux(:) = 0.0_rfreal



!    call mpi_barrier(mycomm,ierr)
!    if(region%myrank == 0) write(*,*) 'All are populating auxiliary matrices, order,dir = ',order,dir
    ! ... populate auxiliary vectors with local data
    Do i = 1, Nc
       ! ... gamma
       gamAux(grid%cell2ghost(i,dir)) = state%gv(i,1)
       ! ... pressure
       pAux(grid%cell2ghost(i,dir)) = state%dv(i,1)
       ! ... temperature
       TAux(grid%cell2ghost(i,dir)) = state%dv(i,2)
       ! ... density
       rhoAux(grid%cell2ghost(i,dir)) = state%cv(i,1)
       ! ... x-momentum
       rhouAux(grid%cell2ghost(i,dir)) = state%cv(i,2)
       ! ... y-momentum
       rhovAux(grid%cell2ghost(i,dir)) = state%cv(i,3)
       ! ... z-momentum
       rhowAux(grid%cell2ghost(i,dir)) = state%cv(i,4)
       ! ... energy
       rhoEAux(grid%cell2ghost(i,dir)) = state%cv(i,5)
       ! ... Jacobian
       JACAux(grid%cell2ghost(i,dir)) = grid%JAC(i)
       ! ... XI_X_TILDE
       XI_X_TILDEAux(grid%cell2ghost(i,dir)) = grid%MT1(i, region%global%t2Map(dir,1))
       ! ... XI_Y_TILDE
       XI_Y_TILDEAux(grid%cell2ghost(i,dir)) = grid%MT1(i, region%global%t2Map(dir,2))
       ! ... XI_Z_TILDE
       XI_Z_TILDEAux(grid%cell2ghost(i,dir)) = grid%MT1(i, region%global%t2Map(dir,3))
    End Do

    call mpi_barrier(mycomm,ierr)
!   if(region%myrank == 0 write(*,*) 'All are starting Ghost_Cell_Exchange, order,dir = ',order,dir

     ! do i = 1,Ncg
     !    if(region%myrank == 1) write(*,*) 'i,gamAux_2',i,gamAux(i),gamAuxAux(i)
     ! end do
    ! ... get ghost data
    call Ghost_Cell_Exchange(region,ng,dir, gamAux)
    call Ghost_Cell_Exchange(region,ng,dir,   pAux)
    call Ghost_Cell_Exchange(region,ng,dir,   TAux)
    call Ghost_Cell_Exchange(region,ng,dir, rhoAux)
    call Ghost_Cell_Exchange(region,ng,dir,rhouAux)
    call Ghost_Cell_Exchange(region,ng,dir,rhovAux)
    call Ghost_Cell_Exchange(region,ng,dir,rhowAux)
    call Ghost_Cell_Exchange(region,ng,dir,rhoEAux)
    call Ghost_Cell_Exchange(region,ng,dir, JACAux)
    call Ghost_Cell_Exchange(region,ng,dir,XI_X_TILDEAux)
    call Ghost_Cell_Exchange(region,ng,dir,XI_Y_TILDEAux)
    call Ghost_Cell_Exchange(region,ng,dir,XI_Z_TILDEAux)

!    call mpi_barrier(mycomm,ierr)
! !   if(region%myrank == 0 write(*,*) 'All are done with Ghost_Cell_Exchange, order,dir = ',order,dir


    SELECT CASE (lhs_var)
    CASE (1)

      ! ... B_p {delta_\eta} where
      !
      !       | u rho_p              rho                            0              u rho_T     |
      ! B_p = | rho_p u^2 + 1        2 rho u                        0              u^2 rho_T   |
      !       | rho_p u v            rho v                          rho u          u v rho_T   |
      !     = | [(rho E)_p + 1] u    [(rho E + p) + u (rho E)_u]    u (rho E)_v    u (rho E)_T |
      !
      !
      ! ... inviscid fluxes
      do k = 1, grid%vdsGhost(dir)
        do j = 1, grid%B(opID,dir)%n
          do i = grid%B(opID,dir)%row_ptr(k,j), grid%B(opID,dir)%row_ptr(k,j+1)-1

            ! ... l0 and l1 indices refer to BUFFERED data
            local_val = grid%B(opID,dir)%val(i,k)
            local_ind = grid%B(opID,dir)%col_ind(i,k)
            l0        = grid%vec_indGhost(k,local_ind,dir)
            l1        = grid%vec_indGhost(k,        j,dir)
    
            ! ... convert from linear to triplet indices
            ! ... rhs index (buffered data)
            ii0 = mod(l0-1,N1g)+1
            jj0 = mod((l0-ii0)/N1g,N2g)+1
            kk0 = (l0-ii0-(jj0-1)*N1g)/(N1g*N2g)+1

            ! ... lhs index (buffered data)
            ii1 = mod(l1-1,N1g)+1
            jj1 = mod((l1-ii1)/N1g,N2g)+1
            kk1 = (l1-ii1-(jj1-1)*N1g)/(N1g*N2g)+1

            ! ... convert to global indices
            ii0 = ii0 + grid%is(1) - 1
            jj0 = jj0 + grid%is(2) - 1 - grid%nGhostRHS(dir,1)
            kk0 = kk0 + grid%is(3) - 1
            ii1 = ii1 + grid%is(1) - 1
            jj1 = jj1 + grid%is(2) - 1 - grid%nGhostRHS(dir,1)
            kk1 = kk1 + grid%is(3) - 1

            if (jj1 < grid%is(2) .OR. jj1 > grid%ie(2)) CYCLE

            ! ... convert l1 back to non-buffered counting
            l1 = (kk1-grid%is(3))*N1*N2 + (jj1-grid%is(2))*N1 + (ii1-grid%is(1)+1)


            ! ... intermediate values
            gam     = gamAux(l0)
            gamm1i  = 1.0_8 / (gam - 1.0_8)
            P       = pAux(l0)
            T       = TAux(l0)
            RHO_P   = gam * gamm1i / T
            RHO_T   = -P / T * RHO_P
            RHO     = rhoAux(l0)
            UX      = rhouAux(l0) / RHO
            UY      = rhovAux(l0) / RHO
            UZ      = rhowAux(l0) / RHO
            RHOE_U  = rhouAux(l0)
            RHOE_V  = rhovAux(l0)
            RHOE_W  = rhowAux(l0)
            RHOE    = rhoeAux(l0)
            KE      = 0.5_8 * (UX*UX + UY*UY + UZ*UZ)
            RHOE_P  = gamm1i + KE * RHO_P
            RHOE_T  = KE * RHO_T
            JAC     = JACAux(l0)
            XI_X    = XI_X_TILDEAux(l0) * JAC
            XI_Y    = XI_Y_TILDEAux(l0) * JAC
            XI_Z    = XI_Z_TILDEAux(l0) * JAC


!             ! ... interiate values
!             gam     = state%gv(l1,1)
!             gamm1i  = 1.0_8 / (gam - 1.0_8)
!             P       = state%dv(l1,1)
!             T       = state%dv(l1,2)
!             RHO_P   = gam * gamm1i / T
!             RHO_T   = -P / T * RHO_P
!             UX      = state%cv(l1,2) * state%dv(l1,3)
!             UY      = state%cv(l1,3) * state%dv(l1,3)
!             UZ      = state%cv(l1,4) * state%dv(l1,3)
!             RHO     = state%cv(l1,1)
!             RHOE_U  = UX * RHO
!             RHOE_V  = UY * RHO
!             RHOE_W  = UZ * RHO
!             KE      = 0.5_8 * (UX * UX + UY * UY + UZ * UZ)
!             RHOE_P  = gamm1i + KE * RHO_P
!             RHOE_T  = KE * RHO_T
!             JAC     = grid%JAC(l1)
!             XI_X    = grid%MT1(l1,region%global%t2Map(2,1)) * JAC
!             XI_Y    = grid%MT1(l1,region%global%t2Map(2,2)) * JAC
!             XI_Z    = grid%MT1(l1,region%global%t2Map(2,3)) * JAC

            ! ... index
            index = (l1-1)*stencil_size + input%HYPREStencilIndex(opID, ii0-ii1, jj0-jj1, kk0-kk1) + 1

            ! ... store in array
             values_r(index) =  values_r(index) + local_val * ( RHO_P * (UX * XI_X + UY * XI_Y + UZ * XI_Z) )
            values_ru(index) = values_ru(index) + local_val * ( RHO * XI_X )
            values_rv(index) = values_rv(index) + local_val * ( RHO * XI_Y )
            values_rw(index) = values_rw(index) + local_val * ( RHO * XI_Z )
            values_rE(index) = values_rE(index) + local_val * ( RHO_T * (UX * XI_X + UY * XI_Y + UZ * XI_Z) )

            ! dF(l1)    = dF(l1) + local_val * F(l0)
          end do
        end do
      end do

    CASE (2)

      do k = 1, grid%vdsGhost(dir)
        do j = 1, grid%B(opID,dir)%n
          do i = grid%B(opID,dir)%row_ptr(k,j), grid%B(opID,dir)%row_ptr(k,j+1)-1

            ! ... l0 and l1 indices refer to BUFFERED data
            local_val = grid%B(opID,dir)%val(i,k)
            local_ind = grid%B(opID,dir)%col_ind(i,k)
            l0        = grid%vec_indGhost(k,local_ind,dir)
            l1        = grid%vec_indGhost(k,        j,dir)
    
            ! ... convert from linear to triplet indices
            ! ... rhs index (buffered data)
            ii0 = mod(l0-1,N1g)+1
            jj0 = mod((l0-ii0)/N1g,N2g)+1
            kk0 = (l0-ii0-(jj0-1)*N1g)/(N1g*N2g)+1

            ! ... lhs index (buffered data)
            ii1 = mod(l1-1,N1g)+1
            jj1 = mod((l1-ii1)/N1g,N2g)+1
            kk1 = (l1-ii1-(jj1-1)*N1g)/(N1g*N2g)+1

            ! ... convert to global indices
            ii0 = ii0 + grid%is(1) - 1
            jj0 = jj0 + grid%is(2) - 1 - grid%nGhostRHS(dir,1)
            kk0 = kk0 + grid%is(3) - 1
            ii1 = ii1 + grid%is(1) - 1
            jj1 = jj1 + grid%is(2) - 1 - grid%nGhostRHS(dir,1)
            kk1 = kk1 + grid%is(3) - 1

            if (jj1 < grid%is(2) .OR. jj1 > grid%ie(2)) CYCLE

            ! ... convert l1 back to non-buffered counting
            l1 = (kk1-grid%is(3))*N1*N2 + (jj1-grid%is(2))*N1 + (ii1-grid%is(1)+1)

            ! ... intermediate values
            gam     = gamAux(l0)
            gamm1i  = 1.0_8 / (gam - 1.0_8)
            P       = pAux(l0)
            T       = TAux(l0)
            RHO_P   = gam * gamm1i / T
            RHO_T   = -P / T * RHO_P
            RHO     = rhoAux(l0)
            UX      = rhouAux(l0) / RHO
            UY      = rhovAux(l0) / RHO
            UZ      = rhowAux(l0) / RHO
            RHOE_U  = rhouAux(l0)
            RHOE_V  = rhovAux(l0)
            RHOE_W  = rhowAux(l0)
            RHOE    = rhoeAux(l0)
            KE      = 0.5_8 * (UX*UX + UY*UY + UZ*UZ)
            RHOE_P  = gamm1i + KE * RHO_P
            RHOE_T  = KE * RHO_T
            JAC     = JACAux(l0)
            XI_X    = XI_X_TILDEAux(l0) * JAC
            XI_Y    = XI_Y_TILDEAux(l0) * JAC
            XI_Z    = XI_Z_TILDEAux(l0) * JAC

!             ! ... interiate values
!             gam     = state%gv(l1,1)
!             gamm1i  = 1.0_8 / (gam - 1.0_8)
!             P       = state%dv(l1,1)
!             T       = state%dv(l1,2)
!             RHO_P   = gam * gamm1i / T
!             RHO_T   = -P / T * RHO_P
!             UX      = state%cv(l1,2) * state%dv(l1,3)
!             UY      = state%cv(l1,3) * state%dv(l1,3)
!             UZ      = state%cv(l1,4) * state%dv(l1,3)
!             RHO     = state%cv(l1,1)
!             RHOE_U  = UX * RHO
!             RHOE_V  = UY * RHO
!             RHOE_W  = UZ * RHO
!             KE      = 0.5_8 * (UX * UX + UY * UY + UZ * UZ)
!             RHOE_P  = gamm1i + KE * RHO_P
!             RHOE_T  = KE * RHO_T
!             JAC     = grid%JAC(l1)
!             XI_X    = grid%MT1(l1,region%global%t2Map(2,1)) * JAC
!             XI_Y    = grid%MT1(l1,region%global%t2Map(2,2)) * JAC
!             XI_Z    = grid%MT1(l1,region%global%t2Map(2,3)) * JAC

            ! ... index
            index = (l1-1)*stencil_size + input%HYPREStencilIndex(opID, ii0-ii1, jj0-jj1, kk0-kk1) + 1

            ! ... store in array
             values_r(index) =  values_r(index) + local_val * ( RHO_P * UX * ( UX * XI_X + UY * XI_Y + UZ * XI_Z ) + XI_X )
            values_ru(index) = values_ru(index) + local_val * ( RHO * ( 2.0_8 * UX * XI_X + UY * XI_Y + UZ * XI_Z ) )
            values_rv(index) = values_rv(index) + local_val * ( RHO * UX * XI_Y )
            values_rw(index) = values_rw(index) + local_val * ( RHO * UX * XI_Z )
            values_rE(index) = values_rE(index) + local_val * ( RHO_T * UX * ( UX * XI_X + UY * XI_Y + UZ * XI_Z ) )

            ! dF(l1)    = dF(l1) + local_val * F(l0)
          end do
        end do
      end do

    CASE (3)

      do k = 1, grid%vdsGhost(dir)
        do j = 1, grid%B(opID,dir)%n
          do i = grid%B(opID,dir)%row_ptr(k,j), grid%B(opID,dir)%row_ptr(k,j+1)-1

            ! ... l0 and l1 indices refer to BUFFERED data
            local_val = grid%B(opID,dir)%val(i,k)
            local_ind = grid%B(opID,dir)%col_ind(i,k)
            l0        = grid%vec_indGhost(k,local_ind,dir)
            l1        = grid%vec_indGhost(k,        j,dir)
    
            ! ... convert from linear to triplet indices
            ! ... rhs index (buffered data)
            ii0 = mod(l0-1,N1g)+1
            jj0 = mod((l0-ii0)/N1g,N2g)+1
            kk0 = (l0-ii0-(jj0-1)*N1g)/(N1g*N2g)+1

            ! ... lhs index (buffered data)
            ii1 = mod(l1-1,N1g)+1
            jj1 = mod((l1-ii1)/N1g,N2g)+1
            kk1 = (l1-ii1-(jj1-1)*N1g)/(N1g*N2g)+1

            ! ... convert to global indices
            ii0 = ii0 + grid%is(1) - 1
            jj0 = jj0 + grid%is(2) - 1 - grid%nGhostRHS(dir,1)
            kk0 = kk0 + grid%is(3) - 1
            ii1 = ii1 + grid%is(1) - 1
            jj1 = jj1 + grid%is(2) - 1 - grid%nGhostRHS(dir,1)
            kk1 = kk1 + grid%is(3) - 1

            if (jj1 < grid%is(2) .OR. jj1 > grid%ie(2)) CYCLE

            ! ... convert l1 back to non-buffered counting
            l1 = (kk1-grid%is(3))*N1*N2 + (jj1-grid%is(2))*N1 + (ii1-grid%is(1)+1)

            ! ... intermediate values
            gam     = gamAux(l0)
            gamm1i  = 1.0_8 / (gam - 1.0_8)
            P       = pAux(l0)
            T       = TAux(l0)
            RHO_P   = gam * gamm1i / T
            RHO_T   = -P / T * RHO_P
            RHO     = rhoAux(l0)
            UX      = rhouAux(l0) / RHO
            UY      = rhovAux(l0) / RHO
            UZ      = rhowAux(l0) / RHO
            RHOE_U  = rhouAux(l0)
            RHOE_V  = rhovAux(l0)
            RHOE_W  = rhowAux(l0)
            RHOE    = rhoeAux(l0)
            KE      = 0.5_8 * (UX*UX + UY*UY + UZ*UZ)
            RHOE_P  = gamm1i + KE * RHO_P
            RHOE_T  = KE * RHO_T
            JAC     = JACAux(l0)
            XI_X    = XI_X_TILDEAux(l0) * JAC
            XI_Y    = XI_Y_TILDEAux(l0) * JAC
            XI_Z    = XI_Z_TILDEAux(l0) * JAC

!             ! ... interiate values
!             gam     = state%gv(l1,1)
!             gamm1i  = 1.0_8 / (gam - 1.0_8)
!             P       = state%dv(l1,1)
!             T       = state%dv(l1,2)
!             RHO_P   = gam * gamm1i / T
!             RHO_T   = -P / T * RHO_P
!             UX      = state%cv(l1,2) * state%dv(l1,3)
!             UY      = state%cv(l1,3) * state%dv(l1,3)
!             UZ      = state%cv(l1,4) * state%dv(l1,3)
!             RHO     = state%cv(l1,1)
!             RHOE_U  = UX * RHO
!             RHOE_V  = UY * RHO
!             RHOE_W  = UZ * RHO
!             KE      = 0.5_8 * (UX * UX + UY * UY + UZ * UZ)
!             RHOE_P  = gamm1i + KE * RHO_P
!             RHOE_T  = KE * RHO_T
!             JAC     = grid%JAC(l1)
!             XI_X    = grid%MT1(l1,region%global%t2Map(2,1)) * JAC
!             XI_Y    = grid%MT1(l1,region%global%t2Map(2,2)) * JAC
!             XI_Z    = grid%MT1(l1,region%global%t2Map(2,3)) * JAC

            ! ... index
            index = (l1-1)*stencil_size + input%HYPREStencilIndex(opID, ii0-ii1, jj0-jj1, kk0-kk1) + 1

            ! ... store in array
             values_r(index) =  values_r(index) + local_val * ( RHO_P * UY * ( UX * XI_X + UY * XI_Y + UZ * XI_Z ) + XI_Y )
            values_ru(index) = values_ru(index) + local_val * ( RHO * UY * XI_X )
            values_rv(index) = values_rv(index) + local_val * ( RHO * ( UX * XI_X + 2.0_8 * UY * XI_Y + UZ * XI_Z ) )
            values_rw(index) = values_rw(index) + local_val * ( RHO * UY * XI_Z )
            values_rE(index) = values_rE(index) + local_val * ( RHO_T * UY * ( UX * XI_X + UY * XI_Y + UZ * XI_Z ) )

            ! dF(l1)    = dF(l1) + local_val * F(l0)
          end do
        end do
      end do

    CASE (4)

      do k = 1, grid%vdsGhost(dir)
        do j = 1, grid%B(opID,dir)%n
          do i = grid%B(opID,dir)%row_ptr(k,j), grid%B(opID,dir)%row_ptr(k,j+1)-1

            ! ... l0 and l1 indices refer to BUFFERED data
            local_val = grid%B(opID,dir)%val(i,k)
            local_ind = grid%B(opID,dir)%col_ind(i,k)
            l0        = grid%vec_indGhost(k,local_ind,dir)
            l1        = grid%vec_indGhost(k,        j,dir)
    
            ! ... convert from linear to triplet indices
            ! ... rhs index (buffered data)
            ii0 = mod(l0-1,N1g)+1
            jj0 = mod((l0-ii0)/N1g,N2g)+1
            kk0 = (l0-ii0-(jj0-1)*N1g)/(N1g*N2g)+1

            ! ... lhs index (buffered data)
            ii1 = mod(l1-1,N1g)+1
            jj1 = mod((l1-ii1)/N1g,N2g)+1
            kk1 = (l1-ii1-(jj1-1)*N1g)/(N1g*N2g)+1

            ! ... convert to global indices
            ii0 = ii0 + grid%is(1) - 1
            jj0 = jj0 + grid%is(2) - 1 - grid%nGhostRHS(dir,1)
            kk0 = kk0 + grid%is(3) - 1
            ii1 = ii1 + grid%is(1) - 1
            jj1 = jj1 + grid%is(2) - 1 - grid%nGhostRHS(dir,1)
            kk1 = kk1 + grid%is(3) - 1

            if (jj1 < grid%is(2) .OR. jj1 > grid%ie(2)) CYCLE

            ! ... convert l1 back to non-buffered counting
            l1 = (kk1-grid%is(3))*N1*N2 + (jj1-grid%is(2))*N1 + (ii1-grid%is(1)+1)

            ! ... intermediate values
            gam     = gamAux(l0)
            gamm1i  = 1.0_8 / (gam - 1.0_8)
            P       = pAux(l0)
            T       = TAux(l0)
            RHO_P   = gam * gamm1i / T
            RHO_T   = -P / T * RHO_P
            RHO     = rhoAux(l0)
            UX      = rhouAux(l0) / RHO
            UY      = rhovAux(l0) / RHO
            UZ      = rhowAux(l0) / RHO
            RHOE_U  = rhouAux(l0)
            RHOE_V  = rhovAux(l0)
            RHOE_W  = rhowAux(l0)
            RHOE    = rhoeAux(l0)
            KE      = 0.5_8 * (UX*UX + UY*UY + UZ*UZ)
            RHOE_P  = gamm1i + KE * RHO_P
            RHOE_T  = KE * RHO_T
            JAC     = JACAux(l0)
            XI_X    = XI_X_TILDEAux(l0) * JAC
            XI_Y    = XI_Y_TILDEAux(l0) * JAC
            XI_Z    = XI_Z_TILDEAux(l0) * JAC

!             ! ... interiate values
!             gam     = state%gv(l1,1)
!             gamm1i  = 1.0_8 / (gam - 1.0_8)
!             P       = state%dv(l1,1)
!             T       = state%dv(l1,2)
!             RHO_P   = gam * gamm1i / T
!             RHO_T   = -P / T * RHO_P
!             UX      = state%cv(l1,2) * state%dv(l1,3)
!             UY      = state%cv(l1,3) * state%dv(l1,3)
!             UZ      = state%cv(l1,4) * state%dv(l1,3)
!             RHO     = state%cv(l1,1)
!             RHOE_U  = UX * RHO
!             RHOE_V  = UY * RHO
!             RHOE_W  = UZ * RHO
!             KE      = 0.5_8 * (UX * UX + UY * UY + UZ * UZ)
!             RHOE_P  = gamm1i + KE * RHO_P
!             RHOE_T  = KE * RHO_T
!             JAC     = grid%JAC(l1)
!             XI_X    = grid%MT1(l1,region%global%t2Map(2,1)) * JAC
!             XI_Y    = grid%MT1(l1,region%global%t2Map(2,2)) * JAC
!             XI_Z    = grid%MT1(l1,region%global%t2Map(2,3)) * JAC

            ! ... index
            index = (l1-1)*stencil_size + input%HYPREStencilIndex(opID, ii0-ii1, jj0-jj1, kk0-kk1) + 1

            ! ... store in array
             values_r(index) =  values_r(index) + local_val * ( RHO_P * UZ * ( UX * XI_X + UY * XI_Y + UZ * XI_Z ) + XI_Z )
            values_ru(index) = values_ru(index) + local_val * ( RHO * UZ * XI_X )
            values_rv(index) = values_rv(index) + local_val * ( RHO * UZ * XI_Y )
            values_rw(index) = values_rw(index) + local_val * ( RHO * ( UX * XI_X + UY * XI_Y + 2.0_8 * UZ * XI_Z ) )
            values_rE(index) = values_rE(index) + local_val * ( RHO_T * UZ * ( UX * XI_X + UY * XI_Y + UZ * XI_Z ) )

            ! dF(l1)    = dF(l1) + local_val * F(l0)
          end do
        end do
      end do

    CASE (5)

      do k = 1, grid%vdsGhost(dir)
        do j = 1, grid%B(opID,dir)%n
          do i = grid%B(opID,dir)%row_ptr(k,j), grid%B(opID,dir)%row_ptr(k,j+1)-1

            ! ... l0 and l1 indices refer to BUFFERED data
            local_val = grid%B(opID,dir)%val(i,k)
            local_ind = grid%B(opID,dir)%col_ind(i,k)
            l0        = grid%vec_indGhost(k,local_ind,dir)
            l1        = grid%vec_indGhost(k,        j,dir)
    
            ! ... convert from linear to triplet indices
            ! ... rhs index (buffered data)
            ii0 = mod(l0-1,N1g)+1
            jj0 = mod((l0-ii0)/N1g,N2g)+1
            kk0 = (l0-ii0-(jj0-1)*N1g)/(N1g*N2g)+1

            ! ... lhs index (buffered data)
            ii1 = mod(l1-1,N1g)+1
            jj1 = mod((l1-ii1)/N1g,N2g)+1
            kk1 = (l1-ii1-(jj1-1)*N1g)/(N1g*N2g)+1

            ! ... convert to global indices
            ii0 = ii0 + grid%is(1) - 1
            jj0 = jj0 + grid%is(2) - 1 - grid%nGhostRHS(dir,1)
            kk0 = kk0 + grid%is(3) - 1
            ii1 = ii1 + grid%is(1) - 1
            jj1 = jj1 + grid%is(2) - 1 - grid%nGhostRHS(dir,1)
            kk1 = kk1 + grid%is(3) - 1

            if (jj1 < grid%is(2) .OR. jj1 > grid%ie(2)) CYCLE

            ! ... convert l1 back to non-buffered counting
            l1 = (kk1-grid%is(3))*N1*N2 + (jj1-grid%is(2))*N1 + (ii1-grid%is(1)+1)

            ! ... intermediate values
            gam     = gamAux(l0)
            gamm1i  = 1.0_8 / (gam - 1.0_8)
            P       = pAux(l0)
            T       = TAux(l0)
            RHO_P   = gam * gamm1i / T
            RHO_T   = -P / T * RHO_P
            RHO     = rhoAux(l0)
            UX      = rhouAux(l0) / RHO
            UY      = rhovAux(l0) / RHO
            UZ      = rhowAux(l0) / RHO
            RHOE_U  = rhouAux(l0)
            RHOE_V  = rhovAux(l0)
            RHOE_W  = rhowAux(l0)
            RHOE    = rhoeAux(l0)
            KE      = 0.5_8 * (UX*UX + UY*UY + UZ*UZ)
            RHOE_P  = gamm1i + KE * RHO_P
            RHOE_T  = KE * RHO_T
            JAC     = JACAux(l0)
            XI_X    = XI_X_TILDEAux(l0) * JAC
            XI_Y    = XI_Y_TILDEAux(l0) * JAC
            XI_Z    = XI_Z_TILDEAux(l0) * JAC

!             ! ... interiate values
!             gam     = state%gv(l1,1)
!             gamm1i  = 1.0_8 / (gam - 1.0_8)
!             P       = state%dv(l1,1)
!             T       = state%dv(l1,2)
!             RHO_P   = gam * gamm1i / T
!             RHO_T   = -P / T * RHO_P
!             UX      = state%cv(l1,2) * state%dv(l1,3)
!             UY      = state%cv(l1,3) * state%dv(l1,3)
!             UZ      = state%cv(l1,4) * state%dv(l1,3)
!             RHO     = state%cv(l1,1)
!             RHOE_U  = UX * RHO
!             RHOE_V  = UY * RHO
!             RHOE_W  = UZ * RHO
!             KE      = 0.5_8 * (UX * UX + UY * UY + UZ * UZ)
!             RHOE_P  = gamm1i + KE * RHO_P
!             RHOE_T  = KE * RHO_T
!             RHOE    = state%cv(l1,5)
!             JAC     = grid%JAC(l1)
!             XI_X    = grid%MT1(l1,region%global%t2Map(2,1)) * JAC
!             XI_Y    = grid%MT1(l1,region%global%t2Map(2,2)) * JAC
!             XI_Z    = grid%MT1(l1,region%global%t2Map(2,3)) * JAC

            ! ... index
            index = (l1-1)*stencil_size + input%HYPREStencilIndex(opID, ii0-ii1, jj0-jj1, kk0-kk1) + 1

            ! ... store in array
             values_r(index) =  values_r(index) + local_val * ( ( RHOE_P + 1.0_8 ) * ( UX * XI_X + UY * XI_Y + UZ * XI_Z ) )
            values_ru(index) = values_ru(index) + local_val * ( ( RHOE + P ) * XI_X + RHOE_U * ( UX * XI_X + UY * XI_Y + UZ * XI_Z ) )
            values_rv(index) = values_rv(index) + local_val * ( ( RHOE + P ) * XI_Y + RHOE_V * ( UX * XI_X + UY * XI_Y + UZ * XI_Z ) )
            values_rw(index) = values_rw(index) + local_val * ( ( RHOE + P ) * XI_Z + RHOE_W * ( UX * XI_X + UY * XI_Y + UZ * XI_Z ) )
            values_rE(index) = values_rE(index) + local_val * ( RHOE_T * ( UX * XI_X + UY * XI_Y + UZ * XI_Z ) )

            ! dF(l1)    = dF(l1) + local_val * F(l0)
          end do
        end do
      end do

    END SELECT

    ! ... deallocate auxiliary matrices for this direction
    deallocate(gamAux,pAux,TAux,rhoAux,rhouAux,rhovAux,rhowAux,rhoEAux,JACAux,XI_X_TILDEAux,XI_Y_TILDEAux,XI_Z_TILDEAux)


    ! ... z-direction, first derivatives
    dir   = 3
    order = 1
    opID  = input%iFirstDerivImplicit

    ! ... local partition size
    N1 = grid%ie(1)-grid%is(1)+1
    N2 = grid%ie(2)-grid%is(2)+1
    N3 = grid%ie(3)-grid%is(3)+1

    ! ... partition size including ghost
    N1g = grid%ie(1)-grid%is(1)+1
    N2g = grid%ie(2)-grid%is(2)+1
    N3g = grid%ie(3)-grid%is(3)+1+SUM(grid%nGhostRHS(dir,:))

    ! ... get ghost cell data
    Nc = grid%nCells
    Ncg = Nc + SUM(grid%nGhostRHS(dir,:))*grid%vdsGhost(dir)

        ! ... allocate a lot of arrays
    allocate(gamAux(Ncg),pAux(Ncg),TAux(Ncg),rhoAux(Ncg),rhouAux(Ncg),rhovAux(Ncg),rhowAux(Ncg),rhoEAux(Ncg))
    allocate(JACAux(Ncg),XI_X_TILDEAux(Ncg),XI_Y_TILDEAux(Ncg),XI_Z_TILDEAux(Ncg))

    ! ... initialize auxiliary vectors
    gamAux(:) = 0.0_rfreal
    pAux(:) = 0.0_rfreal
    TAux(:) = 0.0_rfreal
    rhoAux(:) = 0.0_rfreal
    rhouAux(:) = 0.0_rfreal
    rhovAux(:) = 0.0_rfreal
    rhowAux(:) = 0.0_rfreal
    rhoEAux(:) = 0.0_rfreal
    JACAux(:) = 0.0_rfreal
    XI_X_TILDEAux(:) = 0.0_rfreal
    XI_Y_TILDEAux(:) = 0.0_rfreal
    XI_Z_TILDEAux(:) = 0.0_rfreal



!    call mpi_barrier(mycomm,ierr)
! !   if(region%myrank == 0 write(*,*) 'All are populating auxiliary matrices, order,dir = ',order,dir
    ! ... populate auxiliary vectors with local data
    Do i = 1, Nc
       ! ... gamma
       gamAux(grid%cell2ghost(i,dir)) = state%gv(i,1)
       ! ... pressure
       pAux(grid%cell2ghost(i,dir)) = state%dv(i,1)
       ! ... temperature
       TAux(grid%cell2ghost(i,dir)) = state%dv(i,2)
       ! ... density
       rhoAux(grid%cell2ghost(i,dir)) = state%cv(i,1)
       ! ... x-momentum
       rhouAux(grid%cell2ghost(i,dir)) = state%cv(i,2)
       ! ... y-momentum
       rhovAux(grid%cell2ghost(i,dir)) = state%cv(i,3)
       ! ... z-momentum
       rhowAux(grid%cell2ghost(i,dir)) = state%cv(i,4)
       ! ... energy
       rhoEAux(grid%cell2ghost(i,dir)) = state%cv(i,5)
       ! ... Jacobian
       JACAux(grid%cell2ghost(i,dir)) = grid%JAC(i)
       ! ... XI_X_TILDE
       XI_X_TILDEAux(grid%cell2ghost(i,dir)) = grid%MT1(i, region%global%t2Map(dir,1))
       ! ... XI_Y_TILDE
       XI_Y_TILDEAux(grid%cell2ghost(i,dir)) = grid%MT1(i, region%global%t2Map(dir,2))
       ! ... XI_Z_TILDE
       XI_Z_TILDEAux(grid%cell2ghost(i,dir)) = grid%MT1(i, region%global%t2Map(dir,3))
    End Do

    call mpi_barrier(mycomm,ierr)
!!   if(region%myrank == 0 write(*,*) 'All are starting Ghost_Cell_Exchange, order,dir = ',order,dir

     ! do i = 1,Ncg
     !    if(region%myrank == 1) write(*,*) 'i,gamAux_2',i,gamAux(i),gamAuxAux(i)
     ! end do
    ! ... get ghost data
    call Ghost_Cell_Exchange(region,ng,dir, gamAux)
    call Ghost_Cell_Exchange(region,ng,dir,   pAux)
    call Ghost_Cell_Exchange(region,ng,dir,   TAux)
    call Ghost_Cell_Exchange(region,ng,dir, rhoAux)
    call Ghost_Cell_Exchange(region,ng,dir,rhouAux)
    call Ghost_Cell_Exchange(region,ng,dir,rhovAux)
    call Ghost_Cell_Exchange(region,ng,dir,rhowAux)
    call Ghost_Cell_Exchange(region,ng,dir,rhoEAux)
    call Ghost_Cell_Exchange(region,ng,dir, JACAux)
    call Ghost_Cell_Exchange(region,ng,dir,XI_X_TILDEAux)
    call Ghost_Cell_Exchange(region,ng,dir,XI_Y_TILDEAux)
    call Ghost_Cell_Exchange(region,ng,dir,XI_Z_TILDEAux)

!    call mpi_barrier(mycomm,ierr)
! !   if(region%myrank == 0 write(*,*) 'All are done with Ghost_Cell_Exchange, order,dir = ',order,dir


    SELECT CASE (lhs_var)
    CASE (1)

      ! ... C_p {delta_\zeta} where
      !
      !      
      ! C_p = |
      !       |
      !     = |
      !
      !
      ! ... inviscid fluxes
      do k = 1, grid%vdsGhost(dir)
        do j = 1, grid%B(opID,dir)%n
          do i = grid%B(opID,dir)%row_ptr(k,j), grid%B(opID,dir)%row_ptr(k,j+1)-1

            ! ... l0 and l1 indices refer to BUFFERED data
            local_val = grid%B(opID,dir)%val(i,k)
            local_ind = grid%B(opID,dir)%col_ind(i,k)
            l0        = grid%vec_indGhost(k,local_ind,dir)
            l1        = grid%vec_indGhost(k,        j,dir)
    
            ! ... convert from linear to triplet indices
            ! ... rhs index (buffered data)
            ii0 = mod(l0-1,N1g)+1
            jj0 = mod((l0-ii0)/N1g,N2g)+1
            kk0 = (l0-ii0-(jj0-1)*N1g)/(N1g*N2g)+1

            ! ... lhs index (buffered data)
            ii1 = mod(l1-1,N1g)+1
            jj1 = mod((l1-ii1)/N1g,N2g)+1
            kk1 = (l1-ii1-(jj1-1)*N1g)/(N1g*N2g)+1

            ! ... convert to global indices
            ii0 = ii0 + grid%is(1) - 1
            jj0 = jj0 + grid%is(2) - 1
            kk0 = kk0 + grid%is(3) - 1 - grid%nGhostRHS(dir,1)
            ii1 = ii1 + grid%is(1) - 1
            jj1 = jj1 + grid%is(2) - 1
            kk1 = kk1 + grid%is(3) - 1 - grid%nGhostRHS(dir,1)

            if (kk1 < grid%is(3) .OR. kk1 > grid%ie(3)) CYCLE

            ! ... convert l1 back to non-buffered counting
            l1 = (kk1-grid%is(3))*N1*N2 + (jj1-grid%is(2))*N1 + (ii1-grid%is(1)+1)

            ! ... intermediate values
            gam     = gamAux(l0)
            gamm1i  = 1.0_8 / (gam - 1.0_8)
            P       = pAux(l0)
            T       = TAux(l0)
            RHO_P   = gam * gamm1i / T
            RHO_T   = -P / T * RHO_P
            RHO     = rhoAux(l0)
            UX      = rhouAux(l0) / RHO
            UY      = rhovAux(l0) / RHO
            UZ      = rhowAux(l0) / RHO
            RHOE_U  = rhouAux(l0)
            RHOE_V  = rhovAux(l0)
            RHOE_W  = rhowAux(l0)
            RHOE    = rhoeAux(l0)
            KE      = 0.5_8 * (UX*UX + UY*UY + UZ*UZ)
            RHOE_P  = gamm1i + KE * RHO_P
            RHOE_T  = KE * RHO_T
            JAC     = JACAux(l0)
            XI_X    = XI_X_TILDEAux(l0) * JAC
            XI_Y    = XI_Y_TILDEAux(l0) * JAC
            XI_Z    = XI_Z_TILDEAux(l0) * JAC

!             ! ... interiate values
!             gam     = state%gv(l1,1)
!             gamm1i  = 1.0_8 / (gam - 1.0_8)
!             P       = state%dv(l1,1)
!             T       = state%dv(l1,2)
!             RHO_P   = gam * gamm1i / T
!             RHO_T   = -P / T * RHO_P
!             UX      = state%cv(l1,2) * state%dv(l1,3)
!             UY      = state%cv(l1,3) * state%dv(l1,3)
!             UZ      = state%cv(l1,4) * state%dv(l1,3)
!             RHO     = state%cv(l1,1)
!             RHOE_U  = UX * RHO
!             RHOE_V  = UY * RHO
!             RHOE_W  = UZ * RHO
!             KE      = 0.5_8 * (UX * UX + UY * UY + UZ * UZ)
!             RHOE_P  = gamm1i + KE * RHO_P
!             RHOE_T  = KE * RHO_T
!             JAC     = grid%JAC(l1)
!             XI_X    = grid%MT1(l1,region%global%t2Map(3,1)) * JAC
!             XI_Y    = grid%MT1(l1,region%global%t2Map(3,2)) * JAC
!             XI_Z   = grid%MT1(l1,region%global%t2Map(3,3)) * JAC

            ! ... index
            index = (l1-1)*stencil_size + input%HYPREStencilIndex(opID, ii0-ii1, jj0-jj1, kk0-kk1) + 1

            ! ... store in array
             values_r(index) =  values_r(index) + local_val * ( RHO_P * (UX * XI_X + UY * XI_Y + UZ * XI_Z) )
            values_ru(index) = values_ru(index) + local_val * ( RHO * XI_X )
            values_rv(index) = values_rv(index) + local_val * ( RHO * XI_Y )
            values_rw(index) = values_rw(index) + local_val * ( RHO * XI_Z )
            values_rE(index) = values_rE(index) + local_val * ( RHO_T * (UX * XI_X + UY * XI_Y + UZ * XI_Z) )

            ! dF(l1)    = dF(l1) + local_val * F(l0)
          end do
        end do
      end do

    CASE (2)

      do k = 1, grid%vdsGhost(dir)
        do j = 1, grid%B(opID,dir)%n
          do i = grid%B(opID,dir)%row_ptr(k,j), grid%B(opID,dir)%row_ptr(k,j+1)-1

            ! ... l0 and l1 indices refer to BUFFERED data
            local_val = grid%B(opID,dir)%val(i,k)
            local_ind = grid%B(opID,dir)%col_ind(i,k)
            l0        = grid%vec_indGhost(k,local_ind,dir)
            l1        = grid%vec_indGhost(k,        j,dir)
    
            ! ... convert from linear to triplet indices
            ! ... rhs index (buffered data)
            ii0 = mod(l0-1,N1g)+1
            jj0 = mod((l0-ii0)/N1g,N2g)+1
            kk0 = (l0-ii0-(jj0-1)*N1g)/(N1g*N2g)+1

            ! ... lhs index (buffered data)
            ii1 = mod(l1-1,N1g)+1
            jj1 = mod((l1-ii1)/N1g,N2g)+1
            kk1 = (l1-ii1-(jj1-1)*N1g)/(N1g*N2g)+1

            ! ... convert to global indices
            ii0 = ii0 + grid%is(1) - 1
            jj0 = jj0 + grid%is(2) - 1
            kk0 = kk0 + grid%is(3) - 1 - grid%nGhostRHS(dir,1)
            ii1 = ii1 + grid%is(1) - 1
            jj1 = jj1 + grid%is(2) - 1
            kk1 = kk1 + grid%is(3) - 1 - grid%nGhostRHS(dir,1)

            if (kk1 < grid%is(3) .OR. kk1 > grid%ie(3)) CYCLE

            ! ... convert l1 back to non-buffered counting
            l1 = (kk1-grid%is(3))*N1*N2 + (jj1-grid%is(2))*N1 + (ii1-grid%is(1)+1)

            ! ... intermediate values
            gam     = gamAux(l0)
            gamm1i  = 1.0_8 / (gam - 1.0_8)
            P       = pAux(l0)
            T       = TAux(l0)
            RHO_P   = gam * gamm1i / T
            RHO_T   = -P / T * RHO_P
            RHO     = rhoAux(l0)
            UX      = rhouAux(l0) / RHO
            UY      = rhovAux(l0) / RHO
            UZ      = rhowAux(l0) / RHO
            RHOE_U  = rhouAux(l0)
            RHOE_V  = rhovAux(l0)
            RHOE_W  = rhowAux(l0)
            RHOE    = rhoeAux(l0)
            KE      = 0.5_8 * (UX*UX + UY*UY + UZ*UZ)
            RHOE_P  = gamm1i + KE * RHO_P
            RHOE_T  = KE * RHO_T
            JAC     = JACAux(l0)
            XI_X    = XI_X_TILDEAux(l0) * JAC
            XI_Y    = XI_Y_TILDEAux(l0) * JAC
            XI_Z    = XI_Z_TILDEAux(l0) * JAC

            ! ! ... interiate values
            ! gam     = state%gv(l1,1)
            ! gamm1i  = 1.0_8 / (gam - 1.0_8)
            ! P       = state%dv(l1,1)
            ! T       = state%dv(l1,2)
            ! RHO_P   = gam * gamm1i / T
            ! RHO_T   = -P / T * RHO_P
            ! UX      = state%cv(l1,2) * state%dv(l1,3)
            ! UY      = state%cv(l1,3) * state%dv(l1,3)
            ! UZ      = state%cv(l1,4) * state%dv(l1,3)
            ! RHO     = state%cv(l1,1)
            ! RHOE_U  = UX * RHO
            ! RHOE_V  = UY * RHO
            ! RHOE_W  = UZ * RHO
            ! KE      = 0.5_8 * (UX * UX + UY * UY + UZ * UZ)
            ! RHOE_P  = gamm1i + KE * RHO_P
            ! RHOE_T  = KE * RHO_T
            ! JAC     = grid%JAC(l1)
            ! XI_X    = grid%MT1(l1,region%global%t2Map(3,1)) * JAC
            ! XI_Y    = grid%MT1(l1,region%global%t2Map(3,2)) * JAC
            ! XI_Z    = grid%MT1(l1,region%global%t2Map(3,3)) * JAC

            ! ... index
            index = (l1-1)*stencil_size + input%HYPREStencilIndex(opID, ii0-ii1, jj0-jj1, kk0-kk1) + 1

            ! ... store in array
             values_r(index) =  values_r(index) + local_val * ( RHO_P * UX * ( UX * XI_X + UY * XI_Y + UZ * XI_Z ) + XI_X )
            values_ru(index) = values_ru(index) + local_val * ( RHO * ( 2.0_8 * UX * XI_X + UY * XI_Y + UZ * XI_Z ) )
            values_rv(index) = values_rv(index) + local_val * ( RHO * UX * XI_Y )
            values_rw(index) = values_rw(index) + local_val * ( RHO * UX * XI_Z )
            values_rE(index) = values_rE(index) + local_val * ( RHO_T * UX * ( UX * XI_X + UY * XI_Y + UZ * XI_Z ) )

            ! dF(l1)    = dF(l1) + local_val * F(l0)
          end do
        end do
      end do

    CASE (3)

      do k = 1, grid%vdsGhost(dir)
        do j = 1, grid%B(opID,dir)%n
          do i = grid%B(opID,dir)%row_ptr(k,j), grid%B(opID,dir)%row_ptr(k,j+1)-1

            ! ... l0 and l1 indices refer to BUFFERED data
            local_val = grid%B(opID,dir)%val(i,k)
            local_ind = grid%B(opID,dir)%col_ind(i,k)
            l0        = grid%vec_indGhost(k,local_ind,dir)
            l1        = grid%vec_indGhost(k,        j,dir)
    
            ! ... convert from linear to triplet indices
            ! ... rhs index (buffered data)
            ii0 = mod(l0-1,N1g)+1
            jj0 = mod((l0-ii0)/N1g,N2g)+1
            kk0 = (l0-ii0-(jj0-1)*N1g)/(N1g*N2g)+1

            ! ... lhs index (buffered data)
            ii1 = mod(l1-1,N1g)+1
            jj1 = mod((l1-ii1)/N1g,N2g)+1
            kk1 = (l1-ii1-(jj1-1)*N1g)/(N1g*N2g)+1

            ! ... convert to global indices
            ii0 = ii0 + grid%is(1) - 1
            jj0 = jj0 + grid%is(2) - 1
            kk0 = kk0 + grid%is(3) - 1 - grid%nGhostRHS(dir,1)
            ii1 = ii1 + grid%is(1) - 1
            jj1 = jj1 + grid%is(2) - 1
            kk1 = kk1 + grid%is(3) - 1 - grid%nGhostRHS(dir,1)

            if (kk1 < grid%is(3) .OR. kk1 > grid%ie(3)) CYCLE

            ! ... convert l1 back to non-buffered counting
            l1 = (kk1-grid%is(3))*N1*N2 + (jj1-grid%is(2))*N1 + (ii1-grid%is(1)+1)

            ! ... intermediate values
            gam     = gamAux(l0)
            gamm1i  = 1.0_8 / (gam - 1.0_8)
            P       = pAux(l0)
            T       = TAux(l0)
            RHO_P   = gam * gamm1i / T
            RHO_T   = -P / T * RHO_P
            RHO     = rhoAux(l0)
            UX      = rhouAux(l0) / RHO
            UY      = rhovAux(l0) / RHO
            UZ      = rhowAux(l0) / RHO
            RHOE_U  = rhouAux(l0)
            RHOE_V  = rhovAux(l0)
            RHOE_W  = rhowAux(l0)
            RHOE    = rhoeAux(l0)
            KE      = 0.5_8 * (UX*UX + UY*UY + UZ*UZ)
            RHOE_P  = gamm1i + KE * RHO_P
            RHOE_T  = KE * RHO_T
            JAC     = JACAux(l0)
            XI_X    = XI_X_TILDEAux(l0) * JAC
            XI_Y    = XI_Y_TILDEAux(l0) * JAC
            XI_Z    = XI_Z_TILDEAux(l0) * JAC

            ! ! ... interiate values
            ! gam     = state%gv(l1,1)
            ! gamm1i  = 1.0_8 / (gam - 1.0_8)
            ! P       = state%dv(l1,1)
            ! T       = state%dv(l1,2)
            ! RHO_P   = gam * gamm1i / T
            ! RHO_T   = -P / T * RHO_P
            ! UX      = state%cv(l1,2) * state%dv(l1,3)
            ! UY      = state%cv(l1,3) * state%dv(l1,3)
            ! UZ      = state%cv(l1,4) * state%dv(l1,3)
            ! RHO     = state%cv(l1,1)
            ! RHOE_U  = UX * RHO
            ! RHOE_V  = UY * RHO
            ! RHOE_W  = UZ * RHO
            ! KE      = 0.5_8 * (UX * UX + UY * UY + UZ * UZ)
            ! RHOE_P  = gamm1i + KE * RHO_P
            ! RHOE_T  = KE * RHO_T
            ! JAC     = grid%JAC(l1)
            ! XI_X    = grid%MT1(l1,region%global%t2Map(3,1)) * JAC
            ! XI_Y    = grid%MT1(l1,region%global%t2Map(3,2)) * JAC
            ! XI_Z    = grid%MT1(l1,region%global%t2Map(3,3)) * JAC

            ! ... index
            index = (l1-1)*stencil_size + input%HYPREStencilIndex(opID, ii0-ii1, jj0-jj1, kk0-kk1) + 1

            ! ... store in array
             values_r(index) =  values_r(index) + local_val * ( RHO_P * UY * ( UX * XI_X + UY * XI_Y + UZ * XI_Z ) + XI_Y )
            values_ru(index) = values_ru(index) + local_val * ( RHO * UY * XI_X )
            values_rv(index) = values_rv(index) + local_val * ( RHO * ( UX * XI_X + 2.0_8 * UY * XI_Y + UZ * XI_Z ) )
            values_rw(index) = values_rw(index) + local_val * ( RHO * UY * XI_Z )
            values_rE(index) = values_rE(index) + local_val * ( RHO_T * UY * ( UX * XI_X + UY * XI_Y + UZ * XI_Z ) )

            ! dF(l1)    = dF(l1) + local_val * F(l0)
          end do
        end do
      end do

    CASE (4)

      do k = 1, grid%vdsGhost(dir)
        do j = 1, grid%B(opID,dir)%n
          do i = grid%B(opID,dir)%row_ptr(k,j), grid%B(opID,dir)%row_ptr(k,j+1)-1

            ! ... l0 and l1 indices refer to BUFFERED data
            local_val = grid%B(opID,dir)%val(i,k)
            local_ind = grid%B(opID,dir)%col_ind(i,k)
            l0        = grid%vec_indGhost(k,local_ind,dir)
            l1        = grid%vec_indGhost(k,        j,dir)
    
            ! ... convert from linear to triplet indices
            ! ... rhs index (buffered data)
            ii0 = mod(l0-1,N1g)+1
            jj0 = mod((l0-ii0)/N1g,N2g)+1
            kk0 = (l0-ii0-(jj0-1)*N1g)/(N1g*N2g)+1

            ! ... lhs index (buffered data)
            ii1 = mod(l1-1,N1g)+1
            jj1 = mod((l1-ii1)/N1g,N2g)+1
            kk1 = (l1-ii1-(jj1-1)*N1g)/(N1g*N2g)+1

            ! ... convert to global indices
            ii0 = ii0 + grid%is(1) - 1
            jj0 = jj0 + grid%is(2) - 1
            kk0 = kk0 + grid%is(3) - 1 - grid%nGhostRHS(dir,1)
            ii1 = ii1 + grid%is(1) - 1
            jj1 = jj1 + grid%is(2) - 1
            kk1 = kk1 + grid%is(3) - 1 - grid%nGhostRHS(dir,1)

            if (kk1 < grid%is(3) .OR. kk1 > grid%ie(3)) CYCLE

            ! ... convert l1 back to non-buffered counting
            l1 = (kk1-grid%is(3))*N1*N2 + (jj1-grid%is(2))*N1 + (ii1-grid%is(1)+1)

            ! ... intermediate values
            gam     = gamAux(l0)
            gamm1i  = 1.0_8 / (gam - 1.0_8)
            P       = pAux(l0)
            T       = TAux(l0)
            RHO_P   = gam * gamm1i / T
            RHO_T   = -P / T * RHO_P
            RHO     = rhoAux(l0)
            UX      = rhouAux(l0) / RHO
            UY      = rhovAux(l0) / RHO
            UZ      = rhowAux(l0) / RHO
            RHOE_U  = rhouAux(l0)
            RHOE_V  = rhovAux(l0)
            RHOE_W  = rhowAux(l0)
            RHOE    = rhoeAux(l0)
            KE      = 0.5_8 * (UX*UX + UY*UY + UZ*UZ)
            RHOE_P  = gamm1i + KE * RHO_P
            RHOE_T  = KE * RHO_T
            JAC     = JACAux(l0)
            XI_X    = XI_X_TILDEAux(l0) * JAC
            XI_Y    = XI_Y_TILDEAux(l0) * JAC
            XI_Z    = XI_Z_TILDEAux(l0) * JAC

            ! ! ... interiate values
            ! gam     = state%gv(l1,1)
            ! gamm1i  = 1.0_8 / (gam - 1.0_8)
            ! P       = state%dv(l1,1)
            ! T       = state%dv(l1,2)
            ! RHO_P   = gam * gamm1i / T
            ! RHO_T   = -P / T * RHO_P
            ! UX      = state%cv(l1,2) * state%dv(l1,3)
            ! UY      = state%cv(l1,3) * state%dv(l1,3)
            ! UZ      = state%cv(l1,4) * state%dv(l1,3)
            ! RHO     = state%cv(l1,1)
            ! RHOE_U  = UX * RHO
            ! RHOE_V  = UY * RHO
            ! RHOE_W  = UZ * RHO
            ! KE      = 0.5_8 * (UX * UX + UY * UY + UZ * UZ)
            ! RHOE_P  = gamm1i + KE * RHO_P
            ! RHOE_T  = KE * RHO_T
            ! JAC     = grid%JAC(l1)
            ! XI_X    = grid%MT1(l1,region%global%t2Map(3,1)) * JAC
            ! XI_Y    = grid%MT1(l1,region%global%t2Map(3,2)) * JAC
            ! XI_Z    = grid%MT1(l1,region%global%t2Map(3,3)) * JAC

            ! ... index
            index = (l1-1)*stencil_size + input%HYPREStencilIndex(opID, ii0-ii1, jj0-jj1, kk0-kk1) + 1

            ! ... store in array
             values_r(index) =  values_r(index) + local_val * ( RHO_P * UZ * ( UX * XI_X + UY * XI_Y + UZ * XI_Z ) + XI_Z )
            values_ru(index) = values_ru(index) + local_val * ( RHO * UZ * XI_X )
            values_rv(index) = values_rv(index) + local_val * ( RHO * UZ * XI_Y )
            values_rw(index) = values_rw(index) + local_val * ( RHO * ( UX * XI_X + UY * XI_Y + 2.0_8 * UZ * XI_Z ) )
            values_rE(index) = values_rE(index) + local_val * ( RHO_T * UZ * ( UX * XI_X + UY * XI_Y + UZ * XI_Z ) )

            ! dF(l1)    = dF(l1) + local_val * F(l0)
          end do
        end do
      end do

    CASE (5)

      do k = 1, grid%vdsGhost(dir)
        do j = 1, grid%B(opID,dir)%n
          do i = grid%B(opID,dir)%row_ptr(k,j), grid%B(opID,dir)%row_ptr(k,j+1)-1

            ! ... l0 and l1 indices refer to BUFFERED data
            local_val = grid%B(opID,dir)%val(i,k)
            local_ind = grid%B(opID,dir)%col_ind(i,k)
            l0        = grid%vec_indGhost(k,local_ind,dir)
            l1        = grid%vec_indGhost(k,        j,dir)
    
            ! ... convert from linear to triplet indices
            ! ... rhs index (buffered data)
            ii0 = mod(l0-1,N1g)+1
            jj0 = mod((l0-ii0)/N1g,N2g)+1
            kk0 = (l0-ii0-(jj0-1)*N1g)/(N1g*N2g)+1

            ! ... lhs index (buffered data)
            ii1 = mod(l1-1,N1g)+1
            jj1 = mod((l1-ii1)/N1g,N2g)+1
            kk1 = (l1-ii1-(jj1-1)*N1g)/(N1g*N2g)+1

            ! ... convert to global indices
            ii0 = ii0 + grid%is(1) - 1
            jj0 = jj0 + grid%is(2) - 1
            kk0 = kk0 + grid%is(3) - 1 - grid%nGhostRHS(dir,1)
            ii1 = ii1 + grid%is(1) - 1
            jj1 = jj1 + grid%is(2) - 1
            kk1 = kk1 + grid%is(3) - 1 - grid%nGhostRHS(dir,1)

            if (kk1 < grid%is(3) .OR. kk1 > grid%ie(3)) CYCLE

            ! ... convert l1 back to non-buffered counting
            l1 = (kk1-grid%is(3))*N1*N2 + (jj1-grid%is(2))*N1 + (ii1-grid%is(1)+1)

            ! ... intermediate values
            gam     = gamAux(l0)
            gamm1i  = 1.0_8 / (gam - 1.0_8)
            P       = pAux(l0)
            T       = TAux(l0)
            RHO_P   = gam * gamm1i / T
            RHO_T   = -P / T * RHO_P
            RHO     = rhoAux(l0)
            UX      = rhouAux(l0) / RHO
            UY      = rhovAux(l0) / RHO
            UZ      = rhowAux(l0) / RHO
            RHOE_U  = rhouAux(l0)
            RHOE_V  = rhovAux(l0)
            RHOE_W  = rhowAux(l0)
            RHOE    = rhoeAux(l0)
            KE      = 0.5_8 * (UX*UX + UY*UY + UZ*UZ)
            RHOE_P  = gamm1i + KE * RHO_P
            RHOE_T  = KE * RHO_T
            JAC     = JACAux(l0)
            XI_X    = XI_X_TILDEAux(l0) * JAC
            XI_Y    = XI_Y_TILDEAux(l0) * JAC
            XI_Z    = XI_Z_TILDEAux(l0) * JAC

!             ! ... interiate values
!             gam     = state%gv(l1,1)
!             gamm1i  = 1.0_8 / (gam - 1.0_8)
!             P       = state%dv(l1,1)
!             T       = state%dv(l1,2)
!             RHO_P   = gam * gamm1i / T
!             RHO_T   = -P / T * RHO_P
!             UX      = state%cv(l1,2) * state%dv(l1,3)
!             UY      = state%cv(l1,3) * state%dv(l1,3)
!             UZ      = state%cv(l1,4) * state%dv(l1,3)
!             RHO     = state%cv(l1,1)
!             RHOE_U  = UX * RHO
!             RHOE_V  = UY * RHO
!             RHOE_W  = UZ * RHO
!             KE      = 0.5_8 * (UX * UX + UY * UY + UZ * UZ)
!             RHOE_P  = gamm1i + KE * RHO_P
!             RHOE_T  = KE * RHO_T
!             RHOE    = state%cv(l1,5)
!             JAC     = grid%JAC(l1)
!             XI_X    = grid%MT1(l1,region%global%t2Map(3,1)) * JAC
!             XI_Y    = grid%MT1(l1,region%global%t2Map(3,2)) * JAC
!             XI_Z    = grid%MT1(l1,region%global%t2Map(3,3)) * JAC

            ! ... index
            index = (l1-1)*stencil_size + input%HYPREStencilIndex(opID, ii0-ii1, jj0-jj1, kk0-kk1) + 1

            ! ... store in array
             values_r(index) =  values_r(index) + local_val * ( ( RHOE_P + 1.0_8 ) * ( UX * XI_X + UY * XI_Y + UZ * XI_Z ) )
            values_ru(index) = values_ru(index) + local_val * ( ( RHOE + P ) * XI_X + RHOE_U * ( UX * XI_X + UY * XI_Y + UZ * XI_Z ) )
            values_rv(index) = values_rv(index) + local_val * ( ( RHOE + P ) * XI_Y + RHOE_V * ( UX * XI_X + UY * XI_Y + UZ * XI_Z ) )
            values_rw(index) = values_rw(index) + local_val * ( ( RHOE + P ) * XI_Z + RHOE_W * ( UX * XI_X + UY * XI_Y + UZ * XI_Z ) )
            values_rE(index) = values_rE(index) + local_val * ( RHOE_T * ( UX * XI_X + UY * XI_Y + UZ * XI_Z ) )

            ! dF(l1)    = dF(l1) + local_val * F(l0)
          end do
        end do
      end do

    END SELECT

    ! ... deallocate auxiliary vectors
    deallocate(gamAux,pAux,TAux,rhoAux,rhouAux,rhovAux,rhowAux,rhoEAux,JACAux,XI_X_TILDEAux,XI_Y_TILDEAux,XI_Z_TILDEAux)


    ! ... multiply by dtau
    do i = 1, grid%nCells*stencil_size
       values_r(i) = dtau *  values_r(i)
      values_ru(i) = dtau * values_ru(i)
      values_rv(i) = dtau * values_rv(i)
      values_rw(i) = dtau * values_rw(i)
      values_rE(i) = dtau * values_rE(i)
    end do     

    ! ... no preconditioner
    PC(:)       = 0.0_8
    PC(lhs_var) = 1.0_8    

    ! ... precondintioner length
    LU = input%Lu!101.0_8

    ! ... freestream Mach number
    MMIN2 = input%MachInf**2 * 3.0_8!0.5_8!3.0_8 * ( 0.5_8 ) * ( 0.5_8 )


    ! ... change time factor if we are on the first iteration
    time_fac = 1.5_dp
    if (region%global%main_ts_loop_index == 1) time_fac = 1.0_dp
    if (input%ImplicitSteadyState == TRUE) time_fac = 1.0_dp
    if (input%ImplicitTrueSteadyState == TRUE) time_fac = 0.0_dp

    ! ... add the diagonal terms
    Select Case (lhs_var)

    Case (1)

      do i = 1, grid%nCells

        ! ... temporary values
        gam     = state%gv(i,1)
        gamm1i  = 1.0_8 / (gam - 1.0_8)
        P       = state%dv(i,1)
        T       = state%dv(i,2)
        RHO     = state%cv(i,1)
        RHO_P   = gam * gamm1i / T
        RHO_T   = -P / T * RHO_P
        UX      = state%cv(i,2) / RHO
        UY      = state%cv(i,3) / RHO
        UZ      = state%cv(i,4) / RHO

        ! ... preconditioner
        SNDSPD2 = GAM * P / RHO
        SNDSPD  = SQRT(SNDSPD2)
        MI2     = (UX*UX + UY*UY + UZ*UZ) / SNDSPD2
        MU2     = (LU / (3.14_8 * state%dt(i) * SNDSPD)) * (LU / (3.14_8 * state%dt(i) * SNDSPD))
        MP2     = MIN(MAX(MI2,MU2,MMIN2),1.0_8)
        EPS_P   = MP2 / (1.0_8 + (GAM - 1.0_8) * MP2)
        RHO_PP  = 1.0_8 / (EPS_P * SNDSPD2)
        PC(1)   = RHO_PP
        PC(2)   = 0.0_8
        PC(3)   = 0.0_8
        PC(4)   = 0.0_8
        PC(5)   = RHO_T
  
        ! ... index
        index = (i-1)*stencil_size + 1

         values_r(index) =  values_r(index) + PC(1) + time_fac * dtau / state%dt(i) * RHO_P
        values_ru(index) = values_ru(index) + PC(2)
        values_rv(index) = values_rv(index) + PC(3)
        values_rw(index) = values_rw(index) + PC(4)
        values_rE(index) = values_rE(index) + PC(5) + time_fac * dtau / state%dt(i) * RHO_T

      end do

    Case (2)

      do i = 1, grid%nCells

        ! ... temporary values
        gam     = state%gv(i,1)
        gamm1i  = 1.0_8 / (gam - 1.0_8)
        RHO     = state%cv(i,1)
        P       = state%dv(i,1)
        T       = state%dv(i,2)
        RHO_P   = gam * gamm1i / T
        RHO_T   = -P / T * RHO_P
        UX      = state%cv(i,2) / RHO
        UY      = state%cv(i,3) / RHO
        UZ      = state%cv(i,4) / RHO

        ! ... preconditioner
        SNDSPD2 = GAM * P / RHO
        SNDSPD  = SQRT(SNDSPD2)
        MI2     = (UX*UX + UY*UY + UZ*UZ) / SNDSPD2
        MU2     = (LU / (3.14_8 * state%dt(i) * SNDSPD)) * (LU / (3.14_8 * state%dt(i) * SNDSPD))
        MP2     = MIN(MAX(MI2,MU2,MMIN2),1.0_8)
        EPS_P   = MP2 / (1.0_8 + (GAM - 1.0_8) * MP2)
        RHO_PP  = 1.0_8 / (EPS_P * SNDSPD2)
        PC(1)   = UX * RHO_PP
        PC(2)   = RHO
        PC(3)   = 0.0_8
        PC(4)   = 0.0_8
        PC(5)   = UX * RHO_T
  
        ! ... index
        index = (i-1)*stencil_size + 1

         values_r(index) =  values_r(index) + PC(1) + time_fac * dtau / state%dt(i) * RHO_P * UX
        values_ru(index) = values_ru(index) + PC(2) + time_fac * dtau / state%dt(i) * RHO   
        values_rv(index) = values_rv(index) + PC(3)
        values_rw(index) = values_rw(index) + PC(4)
        values_rE(index) = values_rE(index) + PC(5) + time_fac * dtau / state%dt(i) * RHO_T * UX

      end do

    Case (3)

      do i = 1, grid%nCells

        ! ... temporary values
        gam     = state%gv(i,1)
        gamm1i  = 1.0_8 / (gam - 1.0_8)
        RHO     = state%cv(i,1)
        P       = state%dv(i,1)
        T       = state%dv(i,2)
        RHO_P   = gam * gamm1i / T
        RHO_T   = -P / T * RHO_P
        UX      = state%cv(i,2) / RHO
        UY      = state%cv(i,3) / RHO
        UZ      = state%cv(i,4) / RHO

        ! ... preconditioner
        SNDSPD2 = GAM * P / RHO
        SNDSPD  = SQRT(SNDSPD2)
        MI2     = (UX*UX + UY*UY + UZ*UZ) / SNDSPD2
        MU2     = (LU / (3.14_8 * state%dt(i) * SNDSPD)) * (LU / (3.14_8 * state%dt(i) * SNDSPD))
        MP2     = MIN(MAX(MI2,MU2,MMIN2),1.0_8)
        EPS_P   = MP2 / (1.0_8 + (GAM - 1.0_8) * MP2)
        RHO_PP  = 1.0_8 / (EPS_P * SNDSPD2)
        PC(1)   = UY * RHO_PP
        PC(2)   = 0.0_8
        PC(3)   = RHO
        PC(4)   = 0.0_8
        PC(5)   = UY * RHO_T

        ! ... index
        index = (i-1)*stencil_size + 1

         values_r(index) =  values_r(index) + PC(1) + time_fac * dtau / state%dt(i) * RHO_P * UY
        values_ru(index) = values_ru(index) + PC(2)
        values_rv(index) = values_rv(index) + PC(3) + time_fac * dtau / state%dt(i) * RHO
        values_rw(index) = values_rw(index) + PC(4)
        values_rE(index) = values_rE(index) + PC(5) + time_fac * dtau / state%dt(i) * RHO_T * UY

      end do
    Case (4)

      do i = 1, grid%nCells

        ! ... temporary values
        gam     = state%gv(i,1)
        gamm1i  = 1.0_8 / (gam - 1.0_8)
        RHO     = state%cv(i,1)
        P       = state%dv(i,1)
        T       = state%dv(i,2)
        RHO_P   = gam * gamm1i / T
        RHO_T   = -P / T * RHO_P
        UX      = state%cv(i,2) / RHO
        UY      = state%cv(i,3) / RHO
        UZ      = state%cv(i,4) / RHO

        ! ... preconditioner
        SNDSPD2 = GAM * P / RHO
        SNDSPD  = SQRT(SNDSPD2)
        MI2     = (UX*UX + UY*UY + UZ*UZ) / SNDSPD2
        MU2     = (LU / (3.14_8 * state%dt(i) * SNDSPD)) * (LU / (3.14_8 * state%dt(i) * SNDSPD))
        MP2     = MIN(MAX(MI2,MU2,MMIN2),1.0_8)
        EPS_P   = MP2 / (1.0_8 + (GAM - 1.0_8) * MP2)
        RHO_PP  = 1.0_8 / (EPS_P * SNDSPD2)
        PC(1)   = UZ * RHO_PP
        PC(2)   = 0.0_8
        PC(3)   = 0.0_8
        PC(4)   = RHO
        PC(5)   = UZ * RHO_T

        ! ... index
        index = (i-1)*stencil_size + 1

         values_r(index) =  values_r(index) + PC(1) + time_fac * dtau / state%dt(i) * RHO_P * UZ
        values_ru(index) = values_ru(index) + PC(2)
        values_rv(index) = values_rv(index) + PC(3)
        values_rw(index) = values_rw(index) + PC(4) + time_fac * dtau / state%dt(i) * RHO
        values_rE(index) = values_rE(index) + PC(5) + time_fac * dtau / state%dt(i) * RHO_T * UZ

      end do

    Case (5)

      do i = 1, grid%nCells

        ! ... temporary values
        gam     = state%gv(i,1)
        gamm1i  = 1.0_8 / (gam - 1.0_8)
        RHO     = state%cv(i,1)
        P       = state%dv(i,1)
        T       = state%dv(i,2)
        RHO_P   = gam * gamm1i / T
        RHO_T   = -P / T * RHO_P
        UX      = state%cv(i,2) / RHO
        UY      = state%cv(i,3) / RHO
        UZ      = state%cv(i,4) / RHO
        KE      = 0.5 * (UX * UX + UY * UY + UZ * UZ)

        ! ... preconditioner
        SNDSPD2 = GAM * P / RHO
        SNDSPD  = SQRT(SNDSPD2)
        MI2     = (UX*UX + UY*UY + UZ*UZ) / SNDSPD2
        MU2     = (LU / (3.14_8 * state%dt(i) * SNDSPD)) * (LU / (3.14_8 * state%dt(i) * SNDSPD))
        MP2     = MIN(MAX(MI2,MU2,MMIN2),1.0_8)
        EPS_P   = MP2 / (1.0_8 + (GAM - 1.0_8) * MP2)
        RHO_PP  = 1.0_8 / (EPS_P * SNDSPD2)
        H_P     = 0.0_8
        H0      = T + 0.5_8 * (UX * UX + UY * UY + UZ * UZ)
        H_T     = 1.0_8
        PC(1)   = RHO * H_P + H0 * RHO_PP - 1.0_8
        PC(2)   = RHO * UX
        PC(3)   = RHO * UY
        PC(4)   = RHO * UZ
        PC(5)   = RHO * H_T + H0 * RHO_T
  
        ! ... index
        index = (i-1)*stencil_size + 1

         values_r(index) =  values_r(index) + PC(1) + time_fac * dtau / state%dt(i) * (gamm1i + KE * RHO_P) 
        values_ru(index) = values_ru(index) + PC(2) + time_fac * dtau / state%dt(i) * RHO * UX
        values_rv(index) = values_rv(index) + PC(3) + time_fac * dtau / state%dt(i) * RHO * UY
        values_rw(index) = values_rw(index) + PC(4) + time_fac * dtau / state%dt(i) * RHO * UZ
        values_rE(index) = values_rE(index) + PC(5) + time_fac * dtau / state%dt(i) * KE * RHO_T

      end do

    End Select

    if (state%RE <= 0.0_8) Return

    ! ... x-direction, second derivatives
    dir   = 1
    order = 2
    opID  = input%iSecondDerivImplicit

    ! ... local partition size
    N1 = grid%ie(1)-grid%is(1)+1
    N2 = grid%ie(2)-grid%is(2)+1
    N3 = grid%ie(3)-grid%is(3)+1

    ! ... partition size including ghost
    N1g = grid%ie(1)-grid%is(1)+1+SUM(grid%nGhostRHS(dir,:)) ! (MIN(grid%nGhostRHS(dir,1),1)+MIN(grid%nGhostRHS(dir,2),1))*input%overlap(opID)
    N2g = grid%ie(2)-grid%is(2)+1
    N3g = grid%ie(3)-grid%is(3)+1

    ! ... get ghost cell data
    Nc = grid%nCells
    Ncg = Nc + SUM(grid%nGhostRHS(dir,:))*grid%vdsGhost(dir)

        ! ... allocate a lot of arrays
    allocate(rhoAux(Ncg),rhouAux(Ncg),rhovAux(Ncg),rhowAux(Ncg),rhoEAux(Ncg),muAux(Ncg),lambdaAux(Ncg))
    allocate(JACAux(Ncg),XI_X_TILDEAux(Ncg),XI_Y_TILDEAux(Ncg),XI_Z_TILDEAux(Ncg))

    ! ... initialize auxiliary vectors
    rhouAux(:) = 0.0_rfreal
    rhovAux(:) = 0.0_rfreal
    rhowAux(:) = 0.0_rfreal
    rhoEAux(:) = 0.0_rfreal
    JACAux(:)  = 0.0_rfreal
    muAux(:)   = 0.0_rfreal
    lambdaAux(:)     = 0.0_rfreal
    XI_X_TILDEAux(:) = 0.0_rfreal
    XI_Y_TILDEAux(:) = 0.0_rfreal
    XI_Z_TILDEAux(:) = 0.0_rfreal


!    call mpi_barrier(mycomm,ierr)
! !   if(region%myrank == 0 write(*,*) 'All are populating auxiliary matrices, order,dir = ',order,dir
    ! ... populate auxiliary vectors with local data
    Do i = 1, Nc
       ! ... density
       rhoAux(grid%cell2ghost(i,dir))  = state%cv(i,1)
       ! ... x-momentum
       rhouAux(grid%cell2ghost(i,dir)) = state%cv(i,2)
       ! ... y-momentum
       rhovAux(grid%cell2ghost(i,dir)) = state%cv(i,3)
       ! ... z-momentum
       rhowAux(grid%cell2ghost(i,dir)) = state%cv(i,4)
       ! ... energy
       rhoEAux(grid%cell2ghost(i,dir)) = state%cv(i,5)
       ! ... Jacobian
       JACAux(grid%cell2ghost(i,dir) ) = grid%JAC(i)
       ! ... viscosity
       muAux(grid%cell2ghost(i,dir))   = state%tv(i,1)
       ! ... 2nd coef. viscosity
       lambdaAux(grid%cell2ghost(i,dir))     = state%tv(i,2)
       ! ... XI_X_TILDE
       XI_X_TILDEAux(grid%cell2ghost(i,dir)) = grid%MT1(i, region%global%t2Map(dir,1))
       ! ... XI_Y_TILDE
       XI_Y_TILDEAux(grid%cell2ghost(i,dir)) = grid%MT1(i, region%global%t2Map(dir,2))
       ! ... XI_Z_TILDE
       XI_Z_TILDEAux(grid%cell2ghost(i,dir)) = grid%MT1(i, region%global%t2Map(dir,3))
    End Do

    call mpi_barrier(mycomm,ierr)
!!   if(region%myrank == 0 write(*,*) 'All are starting Ghost_Cell_Exchange, order,dir = ',order,dir

     ! do i = 1,Ncg
     !    if(region%myrank == 1) write(*,*) 'i,gamAux_2',i,gamAux(i),gamAuxAux(i)
     ! end do
    ! ... get ghost data
    call Ghost_Cell_Exchange(region,ng,dir, rhoAux)
    call Ghost_Cell_Exchange(region,ng,dir,rhouAux)
    call Ghost_Cell_Exchange(region,ng,dir,rhovAux)
    call Ghost_Cell_Exchange(region,ng,dir,rhowAux)
    call Ghost_Cell_Exchange(region,ng,dir,rhoEAux)
    call Ghost_Cell_Exchange(region,ng,dir, JACAux)
    call Ghost_Cell_Exchange(region,ng,dir,  muAux)
    call Ghost_Cell_Exchange(region,ng,dir,    lambdaAux)
    call Ghost_Cell_Exchange(region,ng,dir,XI_X_TILDEAux)
    call Ghost_Cell_Exchange(region,ng,dir,XI_Y_TILDEAux)
    call Ghost_Cell_Exchange(region,ng,dir,XI_Z_TILDEAux)

!    call mpi_barrier(mycomm,ierr)
! !   if(region%myrank == 0 write(*,*) 'All are done with Ghost_Cell_Exchange, order,dir = ',order,dir



    SELECT CASE (lhs_var)
    CASE (2)

      ! ... viscous fluxes
      do k = 1, grid%vdsGhost(dir)
        do j = 1, grid%B(opID,dir)%n
          do i = grid%B(opID,dir)%row_ptr(k,j), grid%B(opID,dir)%row_ptr(k,j+1)-1

            ! ... l0 and l1 indices refer to BUFFERED data
            local_val = grid%B(opID,dir)%val(i,k)
            local_ind = grid%B(opID,dir)%col_ind(i,k)
            l0        = grid%vec_indGhost(k,local_ind,dir)
            l1        = grid%vec_indGhost(k,        j,dir)
    
            ! ... convert from linear to triplet indices
            ! ... rhs index (buffered data)
            ii0 = mod(l0-1,N1g)+1
            jj0 = mod((l0-ii0)/N1g,N2g)+1
            kk0 = (l0-ii0-(jj0-1)*N1g)/(N1g*N2g)+1

            ! ... lhs index (buffered data)
            ii1 = mod(l1-1,N1g)+1
            jj1 = mod((l1-ii1)/N1g,N2g)+1
            kk1 = (l1-ii1-(jj1-1)*N1g)/(N1g*N2g)+1

            ! ... convert to global indices
            ii0 = ii0 + grid%is(1) - 1 - grid%nGhostRHS(dir,1)
            jj0 = jj0 + grid%is(2) - 1
            kk0 = kk0 + grid%is(3) - 1
            ii1 = ii1 + grid%is(1) - 1 - grid%nGhostRHS(dir,1)
            jj1 = jj1 + grid%is(2) - 1
            kk1 = kk1 + grid%is(3) - 1

            if (ii1 < grid%is(1) .OR. ii1 > grid%ie(1)) CYCLE

            ! ... convert l1 back to non-buffered counting
            l1 = (kk1-grid%is(3))*N1*N2 + (jj1-grid%is(2))*N1 + (ii1-grid%is(1)+1)

            ! ... intermediate values for l1 index
            JAC_l1     = grid%JAC(l1)
            XI_X_l1    = grid%MT1(l1,region%global%t2Map(dir,1)) * JAC_l1
            XI_Y_l1    = grid%MT1(l1,region%global%t2Map(dir,2)) * JAC_l1
            XI_Z_l1    = grid%MT1(l1,region%global%t2Map(dir,3)) * JAC_l1
            
            MU_l1      = state%tv(l1,1) * state%REinv
            LAMBDA_l1    = state%tv(l1,2) * state%REinv

            ! ... intermediate values for l0 index
            JAC_l0    = JACAux(l0)
            XI_X_l0   = XI_X_TILDEAux(l0) / JAC_l0
            XI_Y_l0   = XI_Y_TILDEAux(l0) / JAC_l0
            XI_Z_l0   = XI_Z_TILDEAux(l0) / JAC_l0

            MU_l0     = muAux(l0) * state%REinv
            LAMBDA_l0 = lambdaAux(l0) * state%REinv

            ! ... index at l0 first
            index = (l1-1)*stencil_size + input%HYPREStencilIndex(opID, ii0-ii1, jj0-jj1, kk0-kk1) + 1

            ! ... store in array at index l0
            values_ru(index) = values_ru(index) - 0.5_8 * local_val * &
                 (( XI_X_l0 * XI_X_l0 * ( 2.0_8 * MU_l0 + LAMBDA_l0 ) + MU_l0 * ( XI_Y_l0 * XI_Y_l0 + XI_Z_l0 * XI_Z_l0 )) + &
                  ( XI_X_l1 * XI_X_l1 * ( 2.0_8 * MU_l1 + LAMBDA_l1 ) + MU_l1 * ( XI_Y_l1 * XI_Y_l1 + XI_Z_l1 * XI_Z_l1 )))
                 
            values_rv(index) = values_rv(index) - 0.5_8 * local_val * &
                 ( XI_X_l0 * XI_Y_l0 * (MU_l0 + LAMBDA_l0) + XI_X_l1 * XI_Y_l1 * (MU_l1 + LAMBDA_l1) )
  
            values_rw(index) = values_rw(index) - 0.5_8 * local_val * &
                 ( XI_X_l0 * XI_Z_l0 * (MU_l0 + LAMBDA_l0) + XI_X_l1 * XI_Z_l1 * (MU_l1 + LAMBDA_l1) )

            
            ! ... now work on index at l1
            index = (l1-1)*stencil_size + 1

            ! ... store in array at index l1
            values_ru(index) = values_ru(index) + 0.5_8 * local_val * &
                 (( XI_X_l0 * XI_X_l0 * ( 2.0_8 * MU_l0 + LAMBDA_l0 ) + MU_l0 * ( XI_Y_l0 * XI_Y_l0 + XI_Z_l0 * XI_Z_l0 )))

            values_rv(index) = values_rv(index) + 0.5_8 * local_val * &
                 ( XI_X_l0 * XI_Y_l0 * (MU_l0 + LAMBDA_l0) )
  
            values_rw(index) = values_rw(index) + 0.5_8 * local_val * &
                 ( XI_X_l0 * XI_Z_l0 * (MU_l0 + LAMBDA_l0) ) 
            


          end do
        end do
      end do

    CASE (3)

      ! ... viscous fluxes
      do k = 1, grid%vdsGhost(dir)
        do j = 1, grid%B(opID,dir)%n
          do i = grid%B(opID,dir)%row_ptr(k,j), grid%B(opID,dir)%row_ptr(k,j+1)-1

            ! ... l0 and l1 indices refer to BUFFERED data
            local_val = grid%B(opID,dir)%val(i,k)
            local_ind = grid%B(opID,dir)%col_ind(i,k)
            l0        = grid%vec_indGhost(k,local_ind,dir)
            l1        = grid%vec_indGhost(k,        j,dir)
    
            ! ... convert from linear to triplet indices
            ! ... rhs index (buffered data)
            ii0 = mod(l0-1,N1g)+1
            jj0 = mod((l0-ii0)/N1g,N2g)+1
            kk0 = (l0-ii0-(jj0-1)*N1g)/(N1g*N2g)+1

            ! ... lhs index (buffered data)
            ii1 = mod(l1-1,N1g)+1
            jj1 = mod((l1-ii1)/N1g,N2g)+1
            kk1 = (l1-ii1-(jj1-1)*N1g)/(N1g*N2g)+1

            ! ... convert to global indices
            ii0 = ii0 + grid%is(1) - 1 - grid%nGhostRHS(dir,1)
            jj0 = jj0 + grid%is(2) - 1
            kk0 = kk0 + grid%is(3) - 1
            ii1 = ii1 + grid%is(1) - 1 - grid%nGhostRHS(dir,1)
            jj1 = jj1 + grid%is(2) - 1
            kk1 = kk1 + grid%is(3) - 1

            if (ii1 < grid%is(1) .OR. ii1 > grid%ie(1)) CYCLE

            ! ... convert l1 back to non-buffered counting
            l1 = (kk1-grid%is(3))*N1*N2 + (jj1-grid%is(2))*N1 + (ii1-grid%is(1)+1)

            ! ... intermediate values for l1 index
            JAC_l1     = grid%JAC(l1)
            XI_X_l1    = grid%MT1(l1,region%global%t2Map(dir,1)) * JAC_l1
            XI_Y_l1    = grid%MT1(l1,region%global%t2Map(dir,2)) * JAC_l1
            XI_Z_l1    = grid%MT1(l1,region%global%t2Map(dir,3)) * JAC_l1
            
            MU_l1      = state%tv(l1,1) * state%REinv
            LAMBDA_l1    = state%tv(l1,2) * state%REinv

            ! ... intermediate values for l0 index
            JAC_l0    = JACAux(l0)
            XI_X_l0   = XI_X_TILDEAux(l0) / JAC_l0
            XI_Y_l0   = XI_Y_TILDEAux(l0) / JAC_l0
            XI_Z_l0   = XI_Z_TILDEAux(l0) / JAC_l0

            MU_l0     = muAux(l0) * state%REinv
            LAMBDA_l0 = lambdaAux(l0) * state%REinv


            ! ... index at l0 first
            index = (l1-1)*stencil_size + input%HYPREStencilIndex(opID, ii0-ii1, jj0-jj1, kk0-kk1) + 1

            ! ... store in array at l0 index
            values_ru(index) = values_ru(index) - 0.5_8 * local_val * &
                 ( XI_X_l0 * XI_Y_l0 * ( MU_l0 + LAMBDA_l0) + XI_X_l1 * XI_Y_l1 * ( MU_l1 + LAMBDA_l1) )

            values_rv(index) = values_rv(index) - 0.5_8 * local_val * &
                 ( (XI_Y_l0 * XI_Y_l0 * ( 2.0_8 * MU_l0 + LAMBDA_l0 ) + MU_l0 * ( XI_X_l0 * XI_X_l0 + XI_Z_l0 * XI_Z_l0 )) + &
                   (XI_Y_l1 * XI_Y_l1 * ( 2.0_8 * MU_l1 + LAMBDA_l1 ) + MU_l1 * ( XI_X_l1 * XI_X_l1 + XI_Z_l1 * XI_Z_l1 )) )
                 
            values_rw(index) = values_rw(index) - 0.5_8 * local_val * &
                 ( XI_Y_l0 * XI_Z_l0 * ( MU_l0 + LAMBDA_l0) + XI_Y_l1 * XI_Z_l1 * ( MU_l1 + LAMBDA_l1) )


            ! ... now work on index at l1
            index = (l1-1)*stencil_size + 1

            ! ... store in array at l1 index
            values_ru(index) = values_ru(index) + 0.5_8 * local_val * &
                 ( XI_X_l0 * XI_Y_l0 * ( MU_l0 + LAMBDA_l0) )

            values_rv(index) = values_rv(index) + 0.5_8 * local_val * &
                 ( (XI_Y_l0 * XI_Y_l0 * ( 2.0_8 * MU_l0 + LAMBDA_l0 ) + MU_l0 * ( XI_X_l0 * XI_X_l0 + XI_Z_l0 * XI_Z_l0 )) )
                 
            values_rw(index) = values_rw(index) + 0.5_8 * local_val * &
                 ( XI_Y_l0 * XI_Z_l0 * ( MU_l0 + LAMBDA_l0) )
            

          end do
        end do
      end do

    CASE (4)

      ! ... viscous fluxes
      do k = 1, grid%vdsGhost(dir)
        do j = 1, grid%B(opID,dir)%n
          do i = grid%B(opID,dir)%row_ptr(k,j), grid%B(opID,dir)%row_ptr(k,j+1)-1

            ! ... l0 and l1 indices refer to BUFFERED data
            local_val = grid%B(opID,dir)%val(i,k)
            local_ind = grid%B(opID,dir)%col_ind(i,k)
            l0        = grid%vec_indGhost(k,local_ind,dir)
            l1        = grid%vec_indGhost(k,        j,dir)
    
            ! ... convert from linear to triplet indices
            ! ... rhs index (buffered data)
            ii0 = mod(l0-1,N1g)+1
            jj0 = mod((l0-ii0)/N1g,N2g)+1
            kk0 = (l0-ii0-(jj0-1)*N1g)/(N1g*N2g)+1

            ! ... lhs index (buffered data)
            ii1 = mod(l1-1,N1g)+1
            jj1 = mod((l1-ii1)/N1g,N2g)+1
            kk1 = (l1-ii1-(jj1-1)*N1g)/(N1g*N2g)+1

            ! ... convert to global indices
            ii0 = ii0 + grid%is(1) - 1 - grid%nGhostRHS(dir,1)
            jj0 = jj0 + grid%is(2) - 1
            kk0 = kk0 + grid%is(3) - 1
            ii1 = ii1 + grid%is(1) - 1 - grid%nGhostRHS(dir,1)
            jj1 = jj1 + grid%is(2) - 1
            kk1 = kk1 + grid%is(3) - 1

            if (ii1 < grid%is(1) .OR. ii1 > grid%ie(1)) CYCLE

            ! ... convert l1 back to non-buffered counting
            l1 = (kk1-grid%is(3))*N1*N2 + (jj1-grid%is(2))*N1 + (ii1-grid%is(1)+1)

            ! ... intermediate values for l1 index
            JAC_l1     = grid%JAC(l1)
            XI_X_l1    = grid%MT1(l1,region%global%t2Map(dir,1)) * JAC_l1
            XI_Y_l1    = grid%MT1(l1,region%global%t2Map(dir,2)) * JAC_l1
            XI_Z_l1    = grid%MT1(l1,region%global%t2Map(dir,3)) * JAC_l1
            
            MU_l1      = state%tv(l1,1) * state%REinv
            LAMBDA_l1    = state%tv(l1,2) * state%REinv

            ! ... intermediate values for l0 index
            JAC_l0    = JACAux(l0)
            XI_X_l0   = XI_X_TILDEAux(l0) / JAC_l0
            XI_Y_l0   = XI_Y_TILDEAux(l0) / JAC_l0
            XI_Z_l0   = XI_Z_TILDEAux(l0) / JAC_l0

            MU_l0     = muAux(l0) * state%REinv
            LAMBDA_l0 = lambdaAux(l0) * state%REinv


            ! ... work on index at l0 first
            index = (l1-1)*stencil_size + input%HYPREStencilIndex(opID, ii0-ii1, jj0-jj1, kk0-kk1) + 1

            ! ... store in array at index l0
            values_ru(index) = values_ru(index) - 0.5_8 * local_val * &
                 ( XI_X_l0 * XI_Z_l0 * ( MU_l0 + LAMBDA_l0) + XI_X_l1 * XI_Z_l1 * ( MU_l1 + LAMBDA_l1) )

            values_rv(index) = values_rv(index) - 0.5_8 * local_val * &
                 ( XI_Y_l0 * XI_Z_l0 * ( MU_l0 + LAMBDA_l0) + XI_Y_l1 * XI_Z_l1 * ( MU_l1 + LAMBDA_l1) )
            
            values_rw(index) = values_rw(index) - 0.5_8 * local_val * &
                 ( (XI_Z_l0 * XI_Z_l0 * ( 2.0_8 * MU_l0 + LAMBDA_l0 ) + MU_l0 * ( XI_X_l0 * XI_X_l0 + XI_Y_l0 * XI_Y_l0 )) + &
                   (XI_Z_l1 * XI_Z_l1 * ( 2.0_8 * MU_l1 + LAMBDA_l1 ) + MU_l1 * ( XI_X_l1 * XI_X_l1 + XI_Y_l1 * XI_Y_l1 )) )


            ! ... now work on index at l1
            index = (l1-1)*stencil_size + 1

            ! ... store in array at index l1
            values_ru(index) = values_ru(index) + 0.5_8 * local_val * &
                 ( XI_X_l0 * XI_Z_l0 * ( MU_l0 + LAMBDA_l0) )

            values_rv(index) = values_rv(index) + 0.5_8 * local_val * &
                 ( XI_Y_l0 * XI_Z_l0 * ( MU_l0 + LAMBDA_l0) )
            
            values_rw(index) = values_rw(index) + 0.5_8 * local_val * &
                 ( (XI_Z_l0 * XI_Z_l0 * ( 2.0_8 * MU_l0 + LAMBDA_l0 ) + MU_l0 * ( XI_X_l0 * XI_X_l0 + XI_Y_l0 * XI_Y_l0 )) )
                 

          end do
        end do
      end do

    CASE (5)

      ! ... viscous fluxes
      do k = 1, grid%vdsGhost(dir)
        do j = 1, grid%B(opID,dir)%n
          do i = grid%B(opID,dir)%row_ptr(k,j), grid%B(opID,dir)%row_ptr(k,j+1)-1

            ! ... l0 and l1 indices refer to BUFFERED data
            local_val = grid%B(opID,dir)%val(i,k)
            local_ind = grid%B(opID,dir)%col_ind(i,k)
            l0        = grid%vec_indGhost(k,local_ind,dir)
            l1        = grid%vec_indGhost(k,        j,dir)
    
            ! ... convert from linear to triplet indices
            ! ... rhs index (buffered data)
            ii0 = mod(l0-1,N1g)+1
            jj0 = mod((l0-ii0)/N1g,N2g)+1
            kk0 = (l0-ii0-(jj0-1)*N1g)/(N1g*N2g)+1

            ! ... lhs index (buffered data)
            ii1 = mod(l1-1,N1g)+1
            jj1 = mod((l1-ii1)/N1g,N2g)+1
            kk1 = (l1-ii1-(jj1-1)*N1g)/(N1g*N2g)+1

            ! ... convert to global indices
            ii0 = ii0 + grid%is(1) - 1 - grid%nGhostRHS(dir,1)
            jj0 = jj0 + grid%is(2) - 1
            kk0 = kk0 + grid%is(3) - 1
            ii1 = ii1 + grid%is(1) - 1 - grid%nGhostRHS(dir,1)
            jj1 = jj1 + grid%is(2) - 1
            kk1 = kk1 + grid%is(3) - 1

            if (ii1 < grid%is(1) .OR. ii1 > grid%ie(1)) CYCLE

            ! ... convert l1 back to non-buffered counting
            l1 = (kk1-grid%is(3))*N1*N2 + (jj1-grid%is(2))*N1 + (ii1-grid%is(1)+1)

            ! ... intermediate values for l1 index
            RHO_l1     = state%cv(l1,1)
            UX_l1      = state%cv(l1,2) / RHO_l1
            UY_l1      = state%cv(l1,3) / RHO_l1
            UZ_l1      = state%cv(l1,4) / RHO_l1
            JAC_l1     = grid%JAC(l1)
            XI_X_l1    = grid%MT1(l1,region%global%t2Map(dir,1)) * JAC_l1
            XI_Y_l1    = grid%MT1(l1,region%global%t2Map(dir,2)) * JAC_l1
            XI_Z_l1    = grid%MT1(l1,region%global%t2Map(dir,3)) * JAC_l1
            
            MU_l1      = state%tv(l1,1) * state%REinv
            LAMBDA_l1  = state%tv(l1,2) * state%REinv
            KAPPA_l1   = MU_l1 * state%PRinv

            ! ... intermediate values for l0 index
            RHO_l0    = rhoAux(l0)
            UX_l0     = rhouAux(l0) / RHO_l0
            UY_l0     = rhovAux(l0) / RHO_l0
            UZ_l0     = rhowAux(l0) / RHO_l0
            JAC_l0    = JACAux(l0)
            XI_X_l0   = XI_X_TILDEAux(l0) / JAC_l0
            XI_Y_l0   = XI_Y_TILDEAux(l0) / JAC_l0
            XI_Z_l0   = XI_Z_TILDEAux(l0) / JAC_l0

            MU_l0     = muAux(l0) * state%REinv
            LAMBDA_l0 = lambdaAux(l0) * state%REinv
            KAPPA_l0  = MU_l0 * state%PRinv


            ! ... index at l0 first
            index = (l1-1)*stencil_size + input%HYPREStencilIndex(opID, ii0-ii1, jj0-jj1, kk0-kk1) + 1

            ! ... store in array at index l0
            values_ru(index) = values_ru(index) - 0.5_8 * local_val * &
                 ( (UX_l0 * ( XI_X_l0 * XI_X_l0 * (2.0_8 * MU_l0 + LAMBDA_l0) + MU_l0 * ( XI_Y_l0 * XI_Y_l0 + XI_Z_l0 * XI_Z_l0) ) + &
                    UY_l0 * XI_X_l0 * XI_Y_l0 * ( MU_l0 + LAMBDA_l0 ) + &
                    UZ_l0 * XI_X_l0 * XI_Z_l0 * ( MU_l0 + LAMBDA_l0 )) + &
                   (UX_l1 * ( XI_X_l1 * XI_X_l1 * (2.0_8 * MU_l1 + LAMBDA_l1) + MU_l1 * ( XI_Y_l1 * XI_Y_l1 + XI_Z_l1 * XI_Z_l1) ) + &
                    UY_l1 * XI_X_l1 * XI_Y_l1 * ( MU_l1 + LAMBDA_l1 ) + &
                    UZ_l0 * XI_X_l1 * XI_Z_l1 * ( MU_l1 + LAMBDA_l1 )) )

            values_rv(index) = values_rv(index) - 0.5_8 * local_val * &
                 ( (UY_l0 * ( XI_Y_l0 * XI_Y_l0 * (2.0_8 * MU_l0 + LAMBDA_l0) + MU_l0 * ( XI_X_l0 * XI_X_l0 + XI_Z_l0 * XI_Z_l0)) + &
                    UX_l0 * XI_X_l0 * XI_Y_l0 * ( MU_l0 + LAMBDA_l0 ) + &
                    UZ_l0 * XI_Y_l0 * XI_Z_l0 * ( MU_l0 + LAMBDA_l0 )) + &
                   (UY_l1 * ( XI_Y_l1 * XI_Y_l1 * (2.0_8 * MU_l1 + LAMBDA_l1) + MU_l1 * ( XI_X_l1 * XI_X_l1 + XI_Z_l1 * XI_Z_l1)) + &
                    UX_l1 * XI_X_l1 * XI_Y_l1 * ( MU_l1 + LAMBDA_l1 ) + &
                    UZ_l1 * XI_Y_l1 * XI_Z_l1 * ( MU_l1 + LAMBDA_l1 )) )

            values_rw(index) = values_rw(index) - 0.5_8 * local_val * &
                 ( (UZ_l0 * ( XI_Z_l0 * XI_Z_l0 * (2.0_8 * MU_l0 + LAMBDA_l0) + MU_l0 * ( XI_X_l0 * XI_X_l0 + XI_Y_l0 * XI_Y_l0)) + &
                    UX_l0 * XI_X_l0 * XI_Z_l0 * ( MU_l0 + LAMBDA_l0 ) + &
                    UY_l0 * XI_Y_l0 * XI_Z_l0 * ( MU_l0 + LAMBDA_l0 )) + &
                   (UZ_l1 * ( XI_Z_l1 * XI_Z_l1 * (2.0_8 * MU_l1 + LAMBDA_l1) + MU_l1 * ( XI_X_l1 * XI_X_l1 + XI_Y_l1 * XI_Y_l1)) + &
                    UX_l1 * XI_X_l1 * XI_Z_l1 * ( MU_l1 + LAMBDA_l1 ) + &
                    UY_l1 * XI_Y_l1 * XI_Z_l1 * ( MU_l1 + LAMBDA_l1 )) )

            values_rE(index) = values_rE(index) - 0.5_8 * local_val * &
                 ( KAPPA_l0 * ( XI_X_l0 * XI_X_l0 + XI_Y_l0 * XI_Y_l0 + XI_Z_l0 * XI_Z_l0 ) + &
                   KAPPA_l1 * ( XI_X_l1 * XI_X_l1 + XI_Y_l1 * XI_Y_l1 + XI_Z_l1 * XI_Z_l1 ) )


            ! ... now work on index at l1
            index = (l1-1)*stencil_size + 1

            ! ... store in array at index l1
            values_ru(index) = values_ru(index) + 0.5_8 * local_val * &
                 ( (UX_l0 * ( XI_X_l0 * XI_X_l0 * (2.0_8 * MU_l0 + LAMBDA_l0) + MU_l0 * ( XI_Y_l0 * XI_Y_l0 + XI_Z_l0 * XI_Z_l0) ) + &
                    UY_l0 * XI_X_l0 * XI_Y_l0 * ( MU_l0 + LAMBDA_l0 ) + &
                    UZ_l0 * XI_X_l0 * XI_Z_l0 * ( MU_l0 + LAMBDA_l0 )) )

            values_rv(index) = values_rv(index) + 0.5_8 * local_val * &
                 ( (UY_l0 * ( XI_Y_l0 * XI_Y_l0 * (2.0_8 * MU_l0 + LAMBDA_l0) + MU_l0 * ( XI_X_l0 * XI_X_l0 + XI_Z_l0 * XI_Z_l0)) + &
                    UX_l0 * XI_X_l0 * XI_Y_l0 * ( MU_l0 + LAMBDA_l0 ) + &
                    UZ_l0 * XI_Y_l0 * XI_Z_l0 * ( MU_l0 + LAMBDA_l0 )) )

            values_rw(index) = values_rw(index) + 0.5_8 * local_val * &
                 ( (UZ_l0 * ( XI_Z_l0 * XI_Z_l0 * (2.0_8 * MU_l0 + LAMBDA_l0) + MU_l0 * ( XI_X_l0 * XI_X_l0 + XI_Y_l0 * XI_Y_l0)) + &
                    UX_l0 * XI_X_l0 * XI_Z_l0 * ( MU_l0 + LAMBDA_l0 ) + &
                    UY_l0 * XI_Y_l0 * XI_Z_l0 * ( MU_l0 + LAMBDA_l0 )) )

            values_rE(index) = values_rE(index) + 0.5_8 * local_val * &
                 ( KAPPA_l0 * ( XI_X_l0 * XI_X_l0 + XI_Y_l0 * XI_Y_l0 + XI_Z_l0 * XI_Z_l0 ) )


          end do
        end do
      end do

    End Select

    ! ... deallocate auxiliary vectors
    deallocate(rhoAux,rhouAux,rhovAux,rhowAux,rhoEAux,muAux,lambdaAux,JACAux,XI_X_TILDEAux,XI_Y_TILDEAux,XI_Z_TILDEAux)


    ! ... y-direction, second derivatives
    dir   = 2
    order = 2
    opID  = input%iSecondDerivImplicit

    ! ... local partition size
    N1 = grid%ie(1)-grid%is(1)+1
    N2 = grid%ie(2)-grid%is(2)+1
    N3 = grid%ie(3)-grid%is(3)+1

    ! ... partition size including ghost
    N1g = grid%ie(1)-grid%is(1)+1
    N2g = grid%ie(2)-grid%is(2)+1+SUM(grid%nGhostRHS(dir,:)) ! (MIN(grid%nGhostRHS(dir,1),1)+MIN(grid%nGhostRHS(dir,2),1))*input%overlap(opID)
    N3g = grid%ie(3)-grid%is(3)+1


    ! ... get ghost cell data
    Nc = grid%nCells
    Ncg = Nc + SUM(grid%nGhostRHS(dir,:))*grid%vdsGhost(dir)

    ! ... allocate a lot of arrays
    allocate(rhoAux(Ncg),rhouAux(Ncg),rhovAux(Ncg),rhowAux(Ncg),rhoEAux(Ncg),muAux(Ncg),lambdaAux(Ncg))
    allocate(JACAux(Ncg),XI_X_TILDEAux(Ncg),XI_Y_TILDEAux(Ncg),XI_Z_TILDEAux(Ncg))

    ! ... initialize auxiliary vectors
    rhouAux(:) = 0.0_rfreal
    rhovAux(:) = 0.0_rfreal
    rhowAux(:) = 0.0_rfreal
    rhoEAux(:) = 0.0_rfreal
    JACAux(:)  = 0.0_rfreal
    muAux(:)   = 0.0_rfreal
    lambdaAux(:)     = 0.0_rfreal
    XI_X_TILDEAux(:) = 0.0_rfreal
    XI_Y_TILDEAux(:) = 0.0_rfreal
    XI_Z_TILDEAux(:) = 0.0_rfreal


!    call mpi_barrier(mycomm,ierr)
! !   if(region%myrank == 0 write(*,*) 'All are populating auxiliary matrices, order,dir = ',order,dir
    ! ... populate auxiliary vectors with local data
    Do i = 1, Nc
       ! ... density
       rhoAux(grid%cell2ghost(i,dir))  = state%cv(i,1)
       ! ... x-momentum
       rhouAux(grid%cell2ghost(i,dir)) = state%cv(i,2)
       ! ... y-momentum
       rhovAux(grid%cell2ghost(i,dir)) = state%cv(i,3)
       ! ... z-momentum
       rhowAux(grid%cell2ghost(i,dir)) = state%cv(i,4)
       ! ... energy
       rhoEAux(grid%cell2ghost(i,dir)) = state%cv(i,5)
       ! ... Jacobian
       JACAux(grid%cell2ghost(i,dir) ) = grid%JAC(i)
       ! ... viscosity
       muAux(grid%cell2ghost(i,dir))   = state%tv(i,1)
       ! ... 2nd coef. viscosity
       lambdaAux(grid%cell2ghost(i,dir))     = state%tv(i,2)
       ! ... XI_X_TILDE
       XI_X_TILDEAux(grid%cell2ghost(i,dir)) = grid%MT1(i, region%global%t2Map(dir,1))
       ! ... XI_Y_TILDE
       XI_Y_TILDEAux(grid%cell2ghost(i,dir)) = grid%MT1(i, region%global%t2Map(dir,2))
       ! ... XI_Z_TILDE
       XI_Z_TILDEAux(grid%cell2ghost(i,dir)) = grid%MT1(i, region%global%t2Map(dir,3))
    End Do

    call mpi_barrier(mycomm,ierr)
!!   if(region%myrank == 0 write(*,*) 'All are starting Ghost_Cell_Exchange, order,dir = ',order,dir

     ! do i = 1,Ncg
     !    if(region%myrank == 1) write(*,*) 'i,gamAux_2',i,gamAux(i),gamAuxAux(i)
     ! end do
    ! ... get ghost data
    call Ghost_Cell_Exchange(region,ng,dir, rhoAux)
    call Ghost_Cell_Exchange(region,ng,dir,rhouAux)
    call Ghost_Cell_Exchange(region,ng,dir,rhovAux)
    call Ghost_Cell_Exchange(region,ng,dir,rhowAux)
    call Ghost_Cell_Exchange(region,ng,dir,rhoEAux)
    call Ghost_Cell_Exchange(region,ng,dir, JACAux)
    call Ghost_Cell_Exchange(region,ng,dir,  muAux)
    call Ghost_Cell_Exchange(region,ng,dir,    lambdaAux)
    call Ghost_Cell_Exchange(region,ng,dir,XI_X_TILDEAux)
    call Ghost_Cell_Exchange(region,ng,dir,XI_Y_TILDEAux)
    call Ghost_Cell_Exchange(region,ng,dir,XI_Z_TILDEAux)

!    call mpi_barrier(mycomm,ierr)
! !   if(region%myrank == 0 write(*,*) 'All are done with Ghost_Cell_Exchange, order,dir = ',order,dir


    SELECT CASE (lhs_var)
    CASE (2)

      ! ... viscous fluxes
      do k = 1, grid%vdsGhost(dir)
        do j = 1, grid%B(opID,dir)%n
          do i = grid%B(opID,dir)%row_ptr(k,j), grid%B(opID,dir)%row_ptr(k,j+1)-1

            ! ... l0 and l1 indices refer to BUFFERED data
            local_val = grid%B(opID,dir)%val(i,k)
            local_ind = grid%B(opID,dir)%col_ind(i,k)
            l0        = grid%vec_indGhost(k,local_ind,dir)
            l1        = grid%vec_indGhost(k,        j,dir)
    
            ! ... convert from linear to triplet indices
            ! ... rhs index (buffered data)
            ii0 = mod(l0-1,N1g)+1
            jj0 = mod((l0-ii0)/N1g,N2g)+1
            kk0 = (l0-ii0-(jj0-1)*N1g)/(N1g*N2g)+1

            ! ... lhs index (buffered data)
            ii1 = mod(l1-1,N1g)+1
            jj1 = mod((l1-ii1)/N1g,N2g)+1
            kk1 = (l1-ii1-(jj1-1)*N1g)/(N1g*N2g)+1

            ! ... convert to global indices
            ii0 = ii0 + grid%is(1) - 1
            jj0 = jj0 + grid%is(2) - 1 - grid%nGhostRHS(dir,1)
            kk0 = kk0 + grid%is(3) - 1
            ii1 = ii1 + grid%is(1) - 1
            jj1 = jj1 + grid%is(2) - 1 - grid%nGhostRHS(dir,1)
            kk1 = kk1 + grid%is(3) - 1

            if (jj1 < grid%is(2) .OR. jj1 > grid%ie(2)) CYCLE

            ! ... convert l1 back to non-buffered counting
            l1 = (kk1-grid%is(3))*N1*N2 + (jj1-grid%is(2))*N1 + (ii1-grid%is(1)+1)


            ! ... intermediate values for l1 index
            JAC_l1     = grid%JAC(l1)
            XI_X_l1    = grid%MT1(l1,region%global%t2Map(dir,1)) * JAC_l1
            XI_Y_l1    = grid%MT1(l1,region%global%t2Map(dir,2)) * JAC_l1
            XI_Z_l1    = grid%MT1(l1,region%global%t2Map(dir,3)) * JAC_l1
            
            MU_l1      = state%tv(l1,1) * state%REinv
            LAMBDA_l1    = state%tv(l1,2) * state%REinv

            ! ... intermediate values for l0 index
            JAC_l0    = JACAux(l0)
            XI_X_l0   = XI_X_TILDEAux(l0) / JAC_l0
            XI_Y_l0   = XI_Y_TILDEAux(l0) / JAC_l0
            XI_Z_l0   = XI_Z_TILDEAux(l0) / JAC_l0

            MU_l0     = muAux(l0) * state%REinv
            LAMBDA_l0 = lambdaAux(l0) * state%REinv

            ! ... index at l0 first
            index = (l1-1)*stencil_size + input%HYPREStencilIndex(opID, ii0-ii1, jj0-jj1, kk0-kk1) + 1

            ! ... store in array at index l0
            values_ru(index) = values_ru(index) - 0.5_8 * local_val * &
                 (( XI_X_l0 * XI_X_l0 * ( 2.0_8 * MU_l0 + LAMBDA_l0 ) + MU_l0 * ( XI_Y_l0 * XI_Y_l0 + XI_Z_l0 * XI_Z_l0 )) + &
                  ( XI_X_l1 * XI_X_l1 * ( 2.0_8 * MU_l1 + LAMBDA_l1 ) + MU_l1 * ( XI_Y_l1 * XI_Y_l1 + XI_Z_l1 * XI_Z_l1 )))
                 
            values_rv(index) = values_rv(index) - 0.5_8 * local_val * &
                 ( XI_X_l0 * XI_Y_l0 * (MU_l0 + LAMBDA_l0) + XI_X_l1 * XI_Y_l1 * (MU_l1 + LAMBDA_l1) )
  
            values_rw(index) = values_rw(index) - 0.5_8 * local_val * &
                 ( XI_X_l0 * XI_Z_l0 * (MU_l0 + LAMBDA_l0) + XI_X_l1 * XI_Z_l1 * (MU_l1 + LAMBDA_l1) )

            
            ! ... now work on index at l1
            index = (l1-1)*stencil_size + 1

            ! ... store in array at index l1
            values_ru(index) = values_ru(index) + 0.5_8 * local_val * &
                 (( XI_X_l0 * XI_X_l0 * ( 2.0_8 * MU_l0 + LAMBDA_l0 ) + MU_l0 * ( XI_Y_l0 * XI_Y_l0 + XI_Z_l0 * XI_Z_l0 )))

            values_rv(index) = values_rv(index) + 0.5_8 * local_val * &
                 ( XI_X_l0 * XI_Y_l0 * (MU_l0 + LAMBDA_l0) )
  
            values_rw(index) = values_rw(index) + 0.5_8 * local_val * &
                 ( XI_X_l0 * XI_Z_l0 * (MU_l0 + LAMBDA_l0) ) 
            

!             ! ... intermediate values
!             ! ... intermediate values
!             XI_X    = grid%MT1(l1,region%global%t2Map(2,1)) * JAC
!             XI_Y    = grid%MT1(l1,region%global%t2Map(2,2)) * JAC
!             XI_Z    = grid%MT1(l1,region%global%t2Map(2,3)) * JAC
            
!             MU      = state%tv(l1,1) * state%REinv
!             LAMBDA    = state%tv(l1,2) * state%REinv

!             ! ... index
!             index = (l1-1)*stencil_size + input%HYPREStencilIndex(opID, ii0-ii1, jj0-jj1, kk0-kk1) + 1

!             ! ... store in array
!             values_ru(index) = values_ru(index) + local_val * ( XI_X * XI_X * ( 2.0_8 * MU + LAMBDA ) &
!                                + MU * ( XI_Y * XI_Y + XI_Z * XI_Z ) )
!             values_rv(index) = values_rv(index) + local_val * ( XI_X * XI_Y * ( MU + LAMBDA) ) 
!             values_rw(index) = values_rw(index) + local_val * ( XI_X * XI_Z * ( MU + LAMBDA) ) 

          end do
        end do
      end do

    CASE (3)

      ! ... viscous fluxes
      do k = 1, grid%vdsGhost(dir)
        do j = 1, grid%B(opID,dir)%n
          do i = grid%B(opID,dir)%row_ptr(k,j), grid%B(opID,dir)%row_ptr(k,j+1)-1

            ! ... l0 and l1 indices refer to BUFFERED data
            local_val = grid%B(opID,dir)%val(i,k)
            local_ind = grid%B(opID,dir)%col_ind(i,k)
            l0        = grid%vec_indGhost(k,local_ind,dir)
            l1        = grid%vec_indGhost(k,        j,dir)
    
            ! ... convert from linear to triplet indices
            ! ... rhs index (buffered data)
            ii0 = mod(l0-1,N1g)+1
            jj0 = mod((l0-ii0)/N1g,N2g)+1
            kk0 = (l0-ii0-(jj0-1)*N1g)/(N1g*N2g)+1

            ! ... lhs index (buffered data)
            ii1 = mod(l1-1,N1g)+1
            jj1 = mod((l1-ii1)/N1g,N2g)+1
            kk1 = (l1-ii1-(jj1-1)*N1g)/(N1g*N2g)+1

            ! ... convert to global indices
            ii0 = ii0 + grid%is(1) - 1
            jj0 = jj0 + grid%is(2) - 1 - grid%nGhostRHS(dir,1)
            kk0 = kk0 + grid%is(3) - 1
            ii1 = ii1 + grid%is(1) - 1
            jj1 = jj1 + grid%is(2) - 1 - grid%nGhostRHS(dir,1)
            kk1 = kk1 + grid%is(3) - 1

            if (jj1 < grid%is(2) .OR. jj1 > grid%ie(2)) CYCLE

            ! ... convert l1 back to non-buffered counting
            l1 = (kk1-grid%is(3))*N1*N2 + (jj1-grid%is(2))*N1 + (ii1-grid%is(1)+1)


            ! ... intermediate values for l1 index
            JAC_l1     = grid%JAC(l1)
            XI_X_l1    = grid%MT1(l1,region%global%t2Map(dir,1)) * JAC_l1
            XI_Y_l1    = grid%MT1(l1,region%global%t2Map(dir,2)) * JAC_l1
            XI_Z_l1    = grid%MT1(l1,region%global%t2Map(dir,3)) * JAC_l1
            
            MU_l1      = state%tv(l1,1) * state%REinv
            LAMBDA_l1    = state%tv(l1,2) * state%REinv

            ! ... intermediate values for l0 index
            JAC_l0    = JACAux(l0)
            XI_X_l0   = XI_X_TILDEAux(l0) / JAC_l0
            XI_Y_l0   = XI_Y_TILDEAux(l0) / JAC_l0
            XI_Z_l0   = XI_Z_TILDEAux(l0) / JAC_l0

            MU_l0     = muAux(l0) * state%REinv
            LAMBDA_l0 = lambdaAux(l0) * state%REinv


            ! ... index at l0 first
            index = (l1-1)*stencil_size + input%HYPREStencilIndex(opID, ii0-ii1, jj0-jj1, kk0-kk1) + 1

            ! ... store in array at l0 index
            values_ru(index) = values_ru(index) - 0.5_8 * local_val * &
                 ( XI_X_l0 * XI_Y_l0 * ( MU_l0 + LAMBDA_l0) + XI_X_l1 * XI_Y_l1 * ( MU_l1 + LAMBDA_l1) )

            values_rv(index) = values_rv(index) - 0.5_8 * local_val * &
                 ( (XI_Y_l0 * XI_Y_l0 * ( 2.0_8 * MU_l0 + LAMBDA_l0 ) + MU_l0 * ( XI_X_l0 * XI_X_l0 + XI_Z_l0 * XI_Z_l0 )) + &
                   (XI_Y_l1 * XI_Y_l1 * ( 2.0_8 * MU_l1 + LAMBDA_l1 ) + MU_l1 * ( XI_X_l1 * XI_X_l1 + XI_Z_l1 * XI_Z_l1 )) )
                 
            values_rw(index) = values_rw(index) - 0.5_8 * local_val * &
                 ( XI_Y_l0 * XI_Z_l0 * ( MU_l0 + LAMBDA_l0) + XI_Y_l1 * XI_Z_l1 * ( MU_l1 + LAMBDA_l1) )


            ! ... now work on index at l1
            index = (l1-1)*stencil_size + 1

            ! ... store in array at l1 index
            values_ru(index) = values_ru(index) + 0.5_8 * local_val * &
                 ( XI_X_l0 * XI_Y_l0 * ( MU_l0 + LAMBDA_l0) )

            values_rv(index) = values_rv(index) + 0.5_8 * local_val * &
                 ( (XI_Y_l0 * XI_Y_l0 * ( 2.0_8 * MU_l0 + LAMBDA_l0 ) + MU_l0 * ( XI_X_l0 * XI_X_l0 + XI_Z_l0 * XI_Z_l0 )) )
                 
            values_rw(index) = values_rw(index) + 0.5_8 * local_val * &
                 ( XI_Y_l0 * XI_Z_l0 * ( MU_l0 + LAMBDA_l0) )

!             ! ... intermediate values
!             XI_X    = grid%MT1(l1,region%global%t2Map(2,1)) * JAC
!             XI_Y    = grid%MT1(l1,region%global%t2Map(2,2)) * JAC
!             XI_Z    = grid%MT1(l1,region%global%t2Map(2,3)) * JAC
            
!             MU      = state%tv(l1,1) * state%REinv
!             LAMBDA    = state%tv(l1,2) * state%REinv

!             ! ... index
!             index = (l1-1)*stencil_size + input%HYPREStencilIndex(opID, ii0-ii1, jj0-jj1, kk0-kk1) + 1

!             ! ... store in array
!             values_ru(index) = values_ru(index) + local_val * ( XI_X * XI_Y * ( MU + LAMBDA) ) 
!             values_rv(index) = values_rv(index) + local_val * ( XI_Y * XI_Y * ( 2.0_8 * MU + LAMBDA ) &
!                                + MU * ( XI_X * XI_X + XI_Z * XI_Z ) )
!             values_rw(index) = values_rw(index) + local_val * ( XI_Y * XI_Z * ( MU + LAMBDA) ) 

          end do
        end do
      end do

    CASE (4)

      ! ... viscous fluxes
      do k = 1, grid%vdsGhost(dir)
        do j = 1, grid%B(opID,dir)%n
          do i = grid%B(opID,dir)%row_ptr(k,j), grid%B(opID,dir)%row_ptr(k,j+1)-1

            ! ... l0 and l1 indices refer to BUFFERED data
            local_val = grid%B(opID,dir)%val(i,k)
            local_ind = grid%B(opID,dir)%col_ind(i,k)
            l0        = grid%vec_indGhost(k,local_ind,dir)
            l1        = grid%vec_indGhost(k,        j,dir)
    
            ! ... convert from linear to triplet indices
            ! ... rhs index (buffered data)
            ii0 = mod(l0-1,N1g)+1
            jj0 = mod((l0-ii0)/N1g,N2g)+1
            kk0 = (l0-ii0-(jj0-1)*N1g)/(N1g*N2g)+1

            ! ... lhs index (buffered data)
            ii1 = mod(l1-1,N1g)+1
            jj1 = mod((l1-ii1)/N1g,N2g)+1
            kk1 = (l1-ii1-(jj1-1)*N1g)/(N1g*N2g)+1

            ! ... convert to global indices
            ii0 = ii0 + grid%is(1) - 1
            jj0 = jj0 + grid%is(2) - 1 - grid%nGhostRHS(dir,1)
            kk0 = kk0 + grid%is(3) - 1
            ii1 = ii1 + grid%is(1) - 1
            jj1 = jj1 + grid%is(2) - 1 - grid%nGhostRHS(dir,1)
            kk1 = kk1 + grid%is(3) - 1

            if (jj1 < grid%is(2) .OR. jj1 > grid%ie(2)) CYCLE

            ! ... convert l1 back to non-buffered counting
            l1 = (kk1-grid%is(3))*N1*N2 + (jj1-grid%is(2))*N1 + (ii1-grid%is(1)+1)

            ! ... intermediate values for l1 index
            JAC_l1     = grid%JAC(l1)
            XI_X_l1    = grid%MT1(l1,region%global%t2Map(dir,1)) * JAC_l1
            XI_Y_l1    = grid%MT1(l1,region%global%t2Map(dir,2)) * JAC_l1
            XI_Z_l1    = grid%MT1(l1,region%global%t2Map(dir,3)) * JAC_l1
            
            MU_l1      = state%tv(l1,1) * state%REinv
            LAMBDA_l1    = state%tv(l1,2) * state%REinv

            ! ... intermediate values for l0 index
            JAC_l0    = JACAux(l0)
            XI_X_l0   = XI_X_TILDEAux(l0) / JAC_l0
            XI_Y_l0   = XI_Y_TILDEAux(l0) / JAC_l0
            XI_Z_l0   = XI_Z_TILDEAux(l0) / JAC_l0

            MU_l0     = muAux(l0) * state%REinv
            LAMBDA_l0 = lambdaAux(l0) * state%REinv


            ! ... work on index at l0 first
            index = (l1-1)*stencil_size + input%HYPREStencilIndex(opID, ii0-ii1, jj0-jj1, kk0-kk1) + 1

            ! ... store in array at index l0
            values_ru(index) = values_ru(index) - 0.5_8 * local_val * &
                 ( XI_X_l0 * XI_Z_l0 * ( MU_l0 + LAMBDA_l0) + XI_X_l1 * XI_Z_l1 * ( MU_l1 + LAMBDA_l1) )

            values_rv(index) = values_rv(index) - 0.5_8 * local_val * &
                 ( XI_Y_l0 * XI_Z_l0 * ( MU_l0 + LAMBDA_l0) + XI_Y_l1 * XI_Z_l1 * ( MU_l1 + LAMBDA_l1) )
            
            values_rw(index) = values_rw(index) - 0.5_8 * local_val * &
                 ( (XI_Z_l0 * XI_Z_l0 * ( 2.0_8 * MU_l0 + LAMBDA_l0 ) + MU_l0 * ( XI_X_l0 * XI_X_l0 + XI_Y_l0 * XI_Y_l0 )) + &
                   (XI_Z_l1 * XI_Z_l1 * ( 2.0_8 * MU_l1 + LAMBDA_l1 ) + MU_l1 * ( XI_X_l1 * XI_X_l1 + XI_Y_l1 * XI_Y_l1 )) )


            ! ... now work on index at l1
            index = (l1-1)*stencil_size + 1

            ! ... store in array at index l1
            values_ru(index) = values_ru(index) + 0.5_8 * local_val * &
                 ( XI_X_l0 * XI_Z_l0 * ( MU_l0 + LAMBDA_l0) )

            values_rv(index) = values_rv(index) + 0.5_8 * local_val * &
                 ( XI_Y_l0 * XI_Z_l0 * ( MU_l0 + LAMBDA_l0) )
            
            values_rw(index) = values_rw(index) + 0.5_8 * local_val * &
                 ( (XI_Z_l0 * XI_Z_l0 * ( 2.0_8 * MU_l0 + LAMBDA_l0 ) + MU_l0 * ( XI_X_l0 * XI_X_l0 + XI_Y_l0 * XI_Y_l0 )) )

!             ! ... intermediate values
!             XI_X    = grid%MT1(l1,region%global%t2Map(2,1)) * JAC
!             XI_Y    = grid%MT1(l1,region%global%t2Map(2,2)) * JAC
!             XI_Z    = grid%MT1(l1,region%global%t2Map(2,3)) * JAC
            
!             MU      = state%tv(l1,1) * state%REinv
!             LAMBDA    = state%tv(l1,2) * state%REinv


!             ! ... index
!             index = (l1-1)*stencil_size + input%HYPREStencilIndex(opID, ii0-ii1, jj0-jj1, kk0-kk1) + 1

!             ! ... store in array
!             values_ru(index) = values_ru(index) + local_val * ( XI_X * XI_Z * ( MU + LAMBDA) ) 
!             values_rv(index) = values_rv(index) + local_val * ( XI_Y * XI_Z * ( MU + LAMBDA) ) 
!             values_rw(index) = values_rw(index) + local_val * ( XI_Z * XI_Z * ( 2.0_8 * MU + LAMBDA ) &
!                                + MU * ( XI_X * XI_X + XI_Y * XI_Y ) )

          end do
        end do
      end do

    CASE (5)

      ! ... viscous fluxes
      do k = 1, grid%vdsGhost(dir)
        do j = 1, grid%B(opID,dir)%n
          do i = grid%B(opID,dir)%row_ptr(k,j), grid%B(opID,dir)%row_ptr(k,j+1)-1

            ! ... l0 and l1 indices refer to BUFFERED data
            local_val = grid%B(opID,dir)%val(i,k)
            local_ind = grid%B(opID,dir)%col_ind(i,k)
            l0        = grid%vec_indGhost(k,local_ind,dir)
            l1        = grid%vec_indGhost(k,        j,dir)
    
            ! ... convert from linear to triplet indices
            ! ... rhs index (buffered data)
            ii0 = mod(l0-1,N1g)+1
            jj0 = mod((l0-ii0)/N1g,N2g)+1
            kk0 = (l0-ii0-(jj0-1)*N1g)/(N1g*N2g)+1

            ! ... lhs index (buffered data)
            ii1 = mod(l1-1,N1g)+1
            jj1 = mod((l1-ii1)/N1g,N2g)+1
            kk1 = (l1-ii1-(jj1-1)*N1g)/(N1g*N2g)+1

            ! ... convert to global indices
            ii0 = ii0 + grid%is(1) - 1
            jj0 = jj0 + grid%is(2) - 1 - grid%nGhostRHS(dir,1)
            kk0 = kk0 + grid%is(3) - 1
            ii1 = ii1 + grid%is(1) - 1
            jj1 = jj1 + grid%is(2) - 1 - grid%nGhostRHS(dir,1)
            kk1 = kk1 + grid%is(3) - 1

            if (jj1 < grid%is(2) .OR. jj1 > grid%ie(2)) CYCLE

            ! ... convert l1 back to non-buffered counting
            l1 = (kk1-grid%is(3))*N1*N2 + (jj1-grid%is(2))*N1 + (ii1-grid%is(1)+1)


            ! ... intermediate values for l1 index
            RHO_l1     = state%cv(l1,1)
            UX_l1      = state%cv(l1,2) / RHO_l1
            UY_l1      = state%cv(l1,3) / RHO_l1
            UZ_l1      = state%cv(l1,4) / RHO_l1
            JAC_l1     = grid%JAC(l1)
            XI_X_l1    = grid%MT1(l1,region%global%t2Map(dir,1)) * JAC_l1
            XI_Y_l1    = grid%MT1(l1,region%global%t2Map(dir,2)) * JAC_l1
            XI_Z_l1    = grid%MT1(l1,region%global%t2Map(dir,3)) * JAC_l1
            
            MU_l1      = state%tv(l1,1) * state%REinv
            LAMBDA_l1  = state%tv(l1,2) * state%REinv
            KAPPA_l1   = MU_l1 * state%PRinv

            ! ... intermediate values for l0 index
            RHO_l0    = rhoAux(l0)
            UX_l0     = rhouAux(l0) / RHO_l0
            UY_l0     = rhovAux(l0) / RHO_l0
            UZ_l0     = rhowAux(l0) / RHO_l0
            JAC_l0    = JACAux(l0)
            XI_X_l0   = XI_X_TILDEAux(l0) / JAC_l0
            XI_Y_l0   = XI_Y_TILDEAux(l0) / JAC_l0
            XI_Z_l0   = XI_Z_TILDEAux(l0) / JAC_l0

            MU_l0     = muAux(l0) * state%REinv
            LAMBDA_l0 = lambdaAux(l0) * state%REinv
            KAPPA_l0  = MU_l0 * state%PRinv


            ! ... index at l0 first
            index = (l1-1)*stencil_size + input%HYPREStencilIndex(opID, ii0-ii1, jj0-jj1, kk0-kk1) + 1

            ! ... store in array at index l0
            values_ru(index) = values_ru(index) - 0.5_8 * local_val * &
                 ( (UX_l0 * ( XI_X_l0 * XI_X_l0 * (2.0_8 * MU_l0 + LAMBDA_l0) + MU_l0 * ( XI_Y_l0 * XI_Y_l0 + XI_Z_l0 * XI_Z_l0) ) + &
                    UY_l0 * XI_X_l0 * XI_Y_l0 * ( MU_l0 + LAMBDA_l0 ) + &
                    UZ_l0 * XI_X_l0 * XI_Z_l0 * ( MU_l0 + LAMBDA_l0 )) + &
                   (UX_l1 * ( XI_X_l1 * XI_X_l1 * (2.0_8 * MU_l1 + LAMBDA_l1) + MU_l1 * ( XI_Y_l1 * XI_Y_l1 + XI_Z_l1 * XI_Z_l1) ) + &
                    UY_l1 * XI_X_l1 * XI_Y_l1 * ( MU_l1 + LAMBDA_l1 ) + &
                    UZ_l0 * XI_X_l1 * XI_Z_l1 * ( MU_l1 + LAMBDA_l1 )) )

            values_rv(index) = values_rv(index) - 0.5_8 * local_val * &
                 ( (UY_l0 * ( XI_Y_l0 * XI_Y_l0 * (2.0_8 * MU_l0 + LAMBDA_l0) + MU_l0 * ( XI_X_l0 * XI_X_l0 + XI_Z_l0 * XI_Z_l0)) + &
                    UX_l0 * XI_X_l0 * XI_Y_l0 * ( MU_l0 + LAMBDA_l0 ) + &
                    UZ_l0 * XI_Y_l0 * XI_Z_l0 * ( MU_l0 + LAMBDA_l0 )) + &
                   (UY_l1 * ( XI_Y_l1 * XI_Y_l1 * (2.0_8 * MU_l1 + LAMBDA_l1) + MU_l1 * ( XI_X_l1 * XI_X_l1 + XI_Z_l1 * XI_Z_l1)) + &
                    UX_l1 * XI_X_l1 * XI_Y_l1 * ( MU_l1 + LAMBDA_l1 ) + &
                    UZ_l1 * XI_Y_l1 * XI_Z_l1 * ( MU_l1 + LAMBDA_l1 )) )

            values_rw(index) = values_rw(index) - 0.5_8 * local_val * &
                 ( (UZ_l0 * ( XI_Z_l0 * XI_Z_l0 * (2.0_8 * MU_l0 + LAMBDA_l0) + MU_l0 * ( XI_X_l0 * XI_X_l0 + XI_Y_l0 * XI_Y_l0)) + &
                    UX_l0 * XI_X_l0 * XI_Z_l0 * ( MU_l0 + LAMBDA_l0 ) + &
                    UY_l0 * XI_Y_l0 * XI_Z_l0 * ( MU_l0 + LAMBDA_l0 )) + &
                   (UZ_l1 * ( XI_Z_l1 * XI_Z_l1 * (2.0_8 * MU_l1 + LAMBDA_l1) + MU_l1 * ( XI_X_l1 * XI_X_l1 + XI_Y_l1 * XI_Y_l1)) + &
                    UX_l1 * XI_X_l1 * XI_Z_l1 * ( MU_l1 + LAMBDA_l1 ) + &
                    UY_l1 * XI_Y_l1 * XI_Z_l1 * ( MU_l1 + LAMBDA_l1 )) )

            values_rE(index) = values_rE(index) - 0.5_8 * local_val * &
                 ( KAPPA_l0 * ( XI_X_l0 * XI_X_l0 + XI_Y_l0 * XI_Y_l0 + XI_Z_l0 * XI_Z_l0 ) + &
                   KAPPA_l1 * ( XI_X_l1 * XI_X_l1 + XI_Y_l1 * XI_Y_l1 + XI_Z_l1 * XI_Z_l1 ) )


            ! ... now work on index at l1
            index = (l1-1)*stencil_size + 1

            ! ... store in array at index l1
            values_ru(index) = values_ru(index) + 0.5_8 * local_val * &
                 ( (UX_l0 * ( XI_X_l0 * XI_X_l0 * (2.0_8 * MU_l0 + LAMBDA_l0) + MU_l0 * ( XI_Y_l0 * XI_Y_l0 + XI_Z_l0 * XI_Z_l0) ) + &
                    UY_l0 * XI_X_l0 * XI_Y_l0 * ( MU_l0 + LAMBDA_l0 ) + &
                    UZ_l0 * XI_X_l0 * XI_Z_l0 * ( MU_l0 + LAMBDA_l0 )) )

            values_rv(index) = values_rv(index) + 0.5_8 * local_val * &
                 ( (UY_l0 * ( XI_Y_l0 * XI_Y_l0 * (2.0_8 * MU_l0 + LAMBDA_l0) + MU_l0 * ( XI_X_l0 * XI_X_l0 + XI_Z_l0 * XI_Z_l0)) + &
                    UX_l0 * XI_X_l0 * XI_Y_l0 * ( MU_l0 + LAMBDA_l0 ) + &
                    UZ_l0 * XI_Y_l0 * XI_Z_l0 * ( MU_l0 + LAMBDA_l0 )) )

            values_rw(index) = values_rw(index) + 0.5_8 * local_val * &
                 ( (UZ_l0 * ( XI_Z_l0 * XI_Z_l0 * (2.0_8 * MU_l0 + LAMBDA_l0) + MU_l0 * ( XI_X_l0 * XI_X_l0 + XI_Y_l0 * XI_Y_l0)) + &
                    UX_l0 * XI_X_l0 * XI_Z_l0 * ( MU_l0 + LAMBDA_l0 ) + &
                    UY_l0 * XI_Y_l0 * XI_Z_l0 * ( MU_l0 + LAMBDA_l0 )) )

            values_rE(index) = values_rE(index) + 0.5_8 * local_val * &
                 ( KAPPA_l0 * ( XI_X_l0 * XI_X_l0 + XI_Y_l0 * XI_Y_l0 + XI_Z_l0 * XI_Z_l0 ) )

!             ! ... intermediate values
!             UX      = state%cv(l1,2) * state%dv(l1,3)
!             UY      = state%cv(l1,3) * state%dv(l1,3)
!             UZ      = state%cv(l1,4) * state%dv(l1,3)
!             JAC     = grid%JAC(l1)
!             XI_X    = grid%MT1(l1,region%global%t2Map(2,1)) * JAC
!             XI_Y    = grid%MT1(l1,region%global%t2Map(2,2)) * JAC
!             XI_Z    = grid%MT1(l1,region%global%t2Map(2,3)) * JAC

!             MU      = state%tv(l1,1) * state%REinv
!             KAPPA   = MU * state%PRinv
!             LAMBDA    = state%tv(l1,2) * state%REinv
           

!             ! ... index
!             index = (l1-1)*stencil_size + input%HYPREStencilIndex(opID, ii0-ii1, jj0-jj1, kk0-kk1) + 1

!             ! ... store in array
!             values_ru(index) = values_ru(index) + local_val * ( UX * ( XI_X * XI_X * (2.0_8 * MU + LAMBDA) &
!                                + MU * ( XI_Y * XI_Y + XI_Z * XI_Z) ) + UY * XI_X * XI_Y * ( MU + LAMBDA ) &
!                                + UZ * XI_X * XI_Z * ( MU + LAMBDA ) )
!             values_rv(index) = values_rv(index) + local_val * ( UY * ( XI_Y * XI_Y * (2.0_8 * MU + LAMBDA) &
!                                + MU * ( XI_X * XI_X + XI_Z * XI_Z) ) + UX * XI_X * XI_Y * ( MU + LAMBDA ) &
!                                + UZ * XI_Y * XI_Z * ( MU + LAMBDA ) )
!             values_rw(index) = values_rw(index) + local_val * ( UZ * ( XI_Z * XI_Z * (2.0_8 * MU + LAMBDA) &
!                                + MU * ( XI_X * XI_X + XI_Y * XI_Y) ) + UX * XI_X * XI_Z * ( MU + LAMBDA ) &
!                                + UY * XI_Y * XI_Z * ( MU + LAMBDA ) )
!             values_rE(index) = values_rE(index) + local_val * KAPPA * ( XI_X * XI_X + XI_Y * XI_Y + XI_Z * XI_Z )

          end do
        end do
      end do

    End Select

    ! ... deallocate auxiliary vectors
    deallocate(rhoAux,rhouAux,rhovAux,rhowAux,rhoEAux,muAux,lambdaAux,JACAux,XI_X_TILDEAux,XI_Y_TILDEAux,XI_Z_TILDEAux)


    ! ... z-direction, second derivatives
    dir   = 3
    order = 2
    opID  = input%iSecondDerivImplicit

    ! ... local partition size
    N1 = grid%ie(1)-grid%is(1)+1
    N2 = grid%ie(2)-grid%is(2)+1
    N3 = grid%ie(3)-grid%is(3)+1

    ! ... partition size including ghost
    N1g = grid%ie(1)-grid%is(1)+1
    N2g = grid%ie(2)-grid%is(2)+1
    N3g = grid%ie(3)-grid%is(3)+1+SUM(grid%nGhostRHS(dir,:)) ! (MIN(grid%nGhostRHS(dir,1),1)+MIN(grid%nGhostRHS(dir,2),1))*input%overlap(opID)

    ! ... get ghost cell data
    Nc = grid%nCells
    Ncg = Nc + SUM(grid%nGhostRHS(dir,:))*grid%vdsGhost(dir)

    ! ... allocate a lot of arrays
    allocate(rhoAux(Ncg),rhouAux(Ncg),rhovAux(Ncg),rhowAux(Ncg),rhoEAux(Ncg),muAux(Ncg),lambdaAux(Ncg))
    allocate(JACAux(Ncg),XI_X_TILDEAux(Ncg),XI_Y_TILDEAux(Ncg),XI_Z_TILDEAux(Ncg))

    ! ... initialize auxiliary vectors
    rhouAux(:) = 0.0_rfreal
    rhovAux(:) = 0.0_rfreal
    rhowAux(:) = 0.0_rfreal
    rhoEAux(:) = 0.0_rfreal
    JACAux(:)  = 0.0_rfreal
    muAux(:)   = 0.0_rfreal
    lambdaAux(:)     = 0.0_rfreal
    XI_X_TILDEAux(:) = 0.0_rfreal
    XI_Y_TILDEAux(:) = 0.0_rfreal
    XI_Z_TILDEAux(:) = 0.0_rfreal


!    call mpi_barrier(mycomm,ierr)
! !   if(region%myrank == 0 write(*,*) 'All are populating auxiliary matrices, order,dir = ',order,dir
    ! ... populate auxiliary vectors with local data
    Do i = 1, Nc
       ! ... density
       rhoAux(grid%cell2ghost(i,dir))  = state%cv(i,1)
       ! ... x-momentum
       rhouAux(grid%cell2ghost(i,dir)) = state%cv(i,2)
       ! ... y-momentum
       rhovAux(grid%cell2ghost(i,dir)) = state%cv(i,3)
       ! ... z-momentum
       rhowAux(grid%cell2ghost(i,dir)) = state%cv(i,4)
       ! ... energy
       rhoEAux(grid%cell2ghost(i,dir)) = state%cv(i,5)
       ! ... Jacobian
       JACAux(grid%cell2ghost(i,dir) ) = grid%JAC(i)
       ! ... viscosity
       muAux(grid%cell2ghost(i,dir))   = state%tv(i,1)
       ! ... 2nd coef. viscosity
       lambdaAux(grid%cell2ghost(i,dir))     = state%tv(i,2)
       ! ... XI_X_TILDE
       XI_X_TILDEAux(grid%cell2ghost(i,dir)) = grid%MT1(i, region%global%t2Map(dir,1))
       ! ... XI_Y_TILDE
       XI_Y_TILDEAux(grid%cell2ghost(i,dir)) = grid%MT1(i, region%global%t2Map(dir,2))
       ! ... XI_Z_TILDE
       XI_Z_TILDEAux(grid%cell2ghost(i,dir)) = grid%MT1(i, region%global%t2Map(dir,3))
    End Do

    call mpi_barrier(mycomm,ierr)
!   if(region%myrank == 0 write(*,*) 'All are starting Ghost_Cell_Exchange, order,dir = ',order,dir

     ! do i = 1,Ncg
     !    if(region%myrank == 1) write(*,*) 'i,gamAux_2',i,gamAux(i),gamAuxAux(i)
     ! end do
    ! ... get ghost data
    call Ghost_Cell_Exchange(region,ng,dir, rhoAux)
    call Ghost_Cell_Exchange(region,ng,dir,rhouAux)
    call Ghost_Cell_Exchange(region,ng,dir,rhovAux)
    call Ghost_Cell_Exchange(region,ng,dir,rhowAux)
    call Ghost_Cell_Exchange(region,ng,dir,rhoEAux)
    call Ghost_Cell_Exchange(region,ng,dir, JACAux)
    call Ghost_Cell_Exchange(region,ng,dir,  muAux)
    call Ghost_Cell_Exchange(region,ng,dir,    lambdaAux)
    call Ghost_Cell_Exchange(region,ng,dir,XI_X_TILDEAux)
    call Ghost_Cell_Exchange(region,ng,dir,XI_Y_TILDEAux)
    call Ghost_Cell_Exchange(region,ng,dir,XI_Z_TILDEAux)

!    call mpi_barrier(mycomm,ierr)
! !   if(region%myrank == 0 write(*,*) 'All are done with Ghost_Cell_Exchange, order,dir = ',order,dir


    SELECT CASE (lhs_var)
    CASE (2)

      ! ... viscous fluxes
      do k = 1, grid%vdsGhost(dir)
        do j = 1, grid%B(opID,dir)%n
          do i = grid%B(opID,dir)%row_ptr(k,j), grid%B(opID,dir)%row_ptr(k,j+1)-1

            ! ... l0 and l1 indices refer to BUFFERED data
            local_val = grid%B(opID,dir)%val(i,k)
            local_ind = grid%B(opID,dir)%col_ind(i,k)
            l0        = grid%vec_indGhost(k,local_ind,dir)
            l1        = grid%vec_indGhost(k,        j,dir)
    
            ! ... convert from linear to triplet indices
            ! ... rhs index (buffered data)
            ii0 = mod(l0-1,N1g)+1
            jj0 = mod((l0-ii0)/N1g,N2g)+1
            kk0 = (l0-ii0-(jj0-1)*N1g)/(N1g*N2g)+1

            ! ... lhs index (buffered data)
            ii1 = mod(l1-1,N1g)+1
            jj1 = mod((l1-ii1)/N1g,N2g)+1
            kk1 = (l1-ii1-(jj1-1)*N1g)/(N1g*N2g)+1

            ! ... convert to global indices
            ii0 = ii0 + grid%is(1) - 1
            jj0 = jj0 + grid%is(2) - 1
            kk0 = kk0 + grid%is(3) - 1 - grid%nGhostRHS(dir,1)
            ii1 = ii1 + grid%is(1) - 1
            jj1 = jj1 + grid%is(2) - 1
            kk1 = kk1 + grid%is(3) - 1 - grid%nGhostRHS(dir,1)

            if (kk1 < grid%is(3) .OR. kk1 > grid%ie(3)) CYCLE

            ! ... convert l1 back to non-buffered counting
            l1 = (kk1-grid%is(3))*N1*N2 + (jj1-grid%is(2))*N1 + (ii1-grid%is(1)+1)


            ! ... intermediate values for l1 index
            JAC_l1     = grid%JAC(l1)
            XI_X_l1    = grid%MT1(l1,region%global%t2Map(dir,1)) * JAC_l1
            XI_Y_l1    = grid%MT1(l1,region%global%t2Map(dir,2)) * JAC_l1
            XI_Z_l1    = grid%MT1(l1,region%global%t2Map(dir,3)) * JAC_l1
            
            MU_l1      = state%tv(l1,1) * state%REinv
            LAMBDA_l1    = state%tv(l1,2) * state%REinv

            ! ... intermediate values for l0 index
            JAC_l0    = JACAux(l0)
            XI_X_l0   = XI_X_TILDEAux(l0) / JAC_l0
            XI_Y_l0   = XI_Y_TILDEAux(l0) / JAC_l0
            XI_Z_l0   = XI_Z_TILDEAux(l0) / JAC_l0

            MU_l0     = muAux(l0) * state%REinv
            LAMBDA_l0 = lambdaAux(l0) * state%REinv

            ! ... index at l0 first
            index = (l1-1)*stencil_size + input%HYPREStencilIndex(opID, ii0-ii1, jj0-jj1, kk0-kk1) + 1

            ! ... store in array at index l0
            values_ru(index) = values_ru(index) - 0.5_8 * local_val * &
                 (( XI_X_l0 * XI_X_l0 * ( 2.0_8 * MU_l0 + LAMBDA_l0 ) + MU_l0 * ( XI_Y_l0 * XI_Y_l0 + XI_Z_l0 * XI_Z_l0 )) + &
                  ( XI_X_l1 * XI_X_l1 * ( 2.0_8 * MU_l1 + LAMBDA_l1 ) + MU_l1 * ( XI_Y_l1 * XI_Y_l1 + XI_Z_l1 * XI_Z_l1 )))
                 
            values_rv(index) = values_rv(index) - 0.5_8 * local_val * &
                 ( XI_X_l0 * XI_Y_l0 * (MU_l0 + LAMBDA_l0) + XI_X_l1 * XI_Y_l1 * (MU_l1 + LAMBDA_l1) )
  
            values_rw(index) = values_rw(index) - 0.5_8 * local_val * &
                 ( XI_X_l0 * XI_Z_l0 * (MU_l0 + LAMBDA_l0) + XI_X_l1 * XI_Z_l1 * (MU_l1 + LAMBDA_l1) )

            
            ! ... now work on index at l1
            index = (l1-1)*stencil_size + 1

            ! ... store in array at index l1
            values_ru(index) = values_ru(index) + 0.5_8 * local_val * &
                 (( XI_X_l0 * XI_X_l0 * ( 2.0_8 * MU_l0 + LAMBDA_l0 ) + MU_l0 * ( XI_Y_l0 * XI_Y_l0 + XI_Z_l0 * XI_Z_l0 )))

            values_rv(index) = values_rv(index) + 0.5_8 * local_val * &
                 ( XI_X_l0 * XI_Y_l0 * (MU_l0 + LAMBDA_l0) )
  
            values_rw(index) = values_rw(index) + 0.5_8 * local_val * &
                 ( XI_X_l0 * XI_Z_l0 * (MU_l0 + LAMBDA_l0) ) 
            

!             ! ... intermediate values
!             ! ... intermediate values
!             XI_X    = grid%MT1(l1,region%global%t2Map(3,1)) * JAC
!             XI_Y    = grid%MT1(l1,region%global%t2Map(3,2)) * JAC
!             XI_Z    = grid%MT1(l1,region%global%t2Map(3,3)) * JAC
            
!             MU      = state%tv(l1,1) * state%REinv
!             LAMBDA    = state%tv(l1,2) * state%REinv

!             ! ... index
!             index = (l1-1)*stencil_size + input%HYPREStencilIndex(opID, ii0-ii1, jj0-jj1, kk0-kk1) + 1

!             ! ... store in array
!             values_ru(index) = values_ru(index) + local_val * ( XI_X * XI_X * ( 2.0_8 * MU + LAMBDA ) &
!                                + MU * ( XI_Y * XI_Y + XI_Z * XI_Z ) )
!             values_rv(index) = values_rv(index) + local_val * ( XI_X * XI_Y * ( MU + LAMBDA) ) 
!             values_rw(index) = values_rw(index) + local_val * ( XI_X * XI_Z * ( MU + LAMBDA) ) 

          end do
        end do
      end do

    CASE (3)

      ! ... viscous fluxes
      do k = 1, grid%vdsGhost(dir)
        do j = 1, grid%B(opID,dir)%n
          do i = grid%B(opID,dir)%row_ptr(k,j), grid%B(opID,dir)%row_ptr(k,j+1)-1

            ! ... l0 and l1 indices refer to BUFFERED data
            local_val = grid%B(opID,dir)%val(i,k)
            local_ind = grid%B(opID,dir)%col_ind(i,k)
            l0        = grid%vec_indGhost(k,local_ind,dir)
            l1        = grid%vec_indGhost(k,        j,dir)
    
            ! ... convert from linear to triplet indices
            ! ... rhs index (buffered data)
            ii0 = mod(l0-1,N1g)+1
            jj0 = mod((l0-ii0)/N1g,N2g)+1
            kk0 = (l0-ii0-(jj0-1)*N1g)/(N1g*N2g)+1

            ! ... lhs index (buffered data)
            ii1 = mod(l1-1,N1g)+1
            jj1 = mod((l1-ii1)/N1g,N2g)+1
            kk1 = (l1-ii1-(jj1-1)*N1g)/(N1g*N2g)+1

            ! ... convert to global indices
            ii0 = ii0 + grid%is(1) - 1
            jj0 = jj0 + grid%is(2) - 1
            kk0 = kk0 + grid%is(3) - 1 - grid%nGhostRHS(dir,1)
            ii1 = ii1 + grid%is(1) - 1
            jj1 = jj1 + grid%is(2) - 1
            kk1 = kk1 + grid%is(3) - 1 - grid%nGhostRHS(dir,1)

            if (kk1 < grid%is(3) .OR. kk1 > grid%ie(3)) CYCLE

            ! ... convert l1 back to non-buffered counting
            l1 = (kk1-grid%is(3))*N1*N2 + (jj1-grid%is(2))*N1 + (ii1-grid%is(1)+1)


            ! ... intermediate values for l1 index
            JAC_l1     = grid%JAC(l1)
            XI_X_l1    = grid%MT1(l1,region%global%t2Map(dir,1)) * JAC_l1
            XI_Y_l1    = grid%MT1(l1,region%global%t2Map(dir,2)) * JAC_l1
            XI_Z_l1    = grid%MT1(l1,region%global%t2Map(dir,3)) * JAC_l1
            
            MU_l1      = state%tv(l1,1) * state%REinv
            LAMBDA_l1    = state%tv(l1,2) * state%REinv

            ! ... intermediate values for l0 index
            JAC_l0    = JACAux(l0)
            XI_X_l0   = XI_X_TILDEAux(l0) / JAC_l0
            XI_Y_l0   = XI_Y_TILDEAux(l0) / JAC_l0
            XI_Z_l0   = XI_Z_TILDEAux(l0) / JAC_l0

            MU_l0     = muAux(l0) * state%REinv
            LAMBDA_l0 = lambdaAux(l0) * state%REinv


            ! ... index at l0 first
            index = (l1-1)*stencil_size + input%HYPREStencilIndex(opID, ii0-ii1, jj0-jj1, kk0-kk1) + 1

            ! ... store in array at l0 index
            values_ru(index) = values_ru(index) - 0.5_8 * local_val * &
                 ( XI_X_l0 * XI_Y_l0 * ( MU_l0 + LAMBDA_l0) + XI_X_l1 * XI_Y_l1 * ( MU_l1 + LAMBDA_l1) )

            values_rv(index) = values_rv(index) - 0.5_8 * local_val * &
                 ( (XI_Y_l0 * XI_Y_l0 * ( 2.0_8 * MU_l0 + LAMBDA_l0 ) + MU_l0 * ( XI_X_l0 * XI_X_l0 + XI_Z_l0 * XI_Z_l0 )) + &
                   (XI_Y_l1 * XI_Y_l1 * ( 2.0_8 * MU_l1 + LAMBDA_l1 ) + MU_l1 * ( XI_X_l1 * XI_X_l1 + XI_Z_l1 * XI_Z_l1 )) )
                 
            values_rw(index) = values_rw(index) - 0.5_8 * local_val * &
                 ( XI_Y_l0 * XI_Z_l0 * ( MU_l0 + LAMBDA_l0) + XI_Y_l1 * XI_Z_l1 * ( MU_l1 + LAMBDA_l1) )


            ! ... now work on index at l1
            index = (l1-1)*stencil_size + 1

            ! ... store in array at l1 index
            values_ru(index) = values_ru(index) + 0.5_8 * local_val * &
                 ( XI_X_l0 * XI_Y_l0 * ( MU_l0 + LAMBDA_l0) )

            values_rv(index) = values_rv(index) + 0.5_8 * local_val * &
                 ( (XI_Y_l0 * XI_Y_l0 * ( 2.0_8 * MU_l0 + LAMBDA_l0 ) + MU_l0 * ( XI_X_l0 * XI_X_l0 + XI_Z_l0 * XI_Z_l0 )) )
                 
            values_rw(index) = values_rw(index) + 0.5_8 * local_val * &
                 ( XI_Y_l0 * XI_Z_l0 * ( MU_l0 + LAMBDA_l0) )

!             ! ... intermediate values
!             XI_X    = grid%MT1(l1,region%global%t2Map(3,1)) * JAC
!             XI_Y    = grid%MT1(l1,region%global%t2Map(3,2)) * JAC
!             XI_Z    = grid%MT1(l1,region%global%t2Map(3,3)) * JAC
            
!             MU      = state%tv(l1,1) * state%REinv
!             LAMBDA    = state%tv(l1,2) * state%REinv

!             ! ... index
!             index = (l1-1)*stencil_size + input%HYPREStencilIndex(opID, ii0-ii1, jj0-jj1, kk0-kk1) + 1

!             ! ... store in array
!             values_ru(index) = values_ru(index) + local_val * ( XI_X * XI_Y * ( MU + LAMBDA) ) 
!             values_rv(index) = values_rv(index) + local_val * ( XI_Y * XI_Y * ( 2.0_8 * MU + LAMBDA ) &
!                                + MU * ( XI_X * XI_X + XI_Z * XI_Z ) )
!             values_rw(index) = values_rw(index) + local_val * ( XI_Y * XI_Z * ( MU + LAMBDA) ) 

          end do
        end do
      end do

    CASE (4)

      ! ... viscous fluxes
      do k = 1, grid%vdsGhost(dir)
        do j = 1, grid%B(opID,dir)%n
          do i = grid%B(opID,dir)%row_ptr(k,j), grid%B(opID,dir)%row_ptr(k,j+1)-1

            ! ... l0 and l1 indices refer to BUFFERED data
            local_val = grid%B(opID,dir)%val(i,k)
            local_ind = grid%B(opID,dir)%col_ind(i,k)
            l0        = grid%vec_indGhost(k,local_ind,dir)
            l1        = grid%vec_indGhost(k,        j,dir)
    
            ! ... convert from linear to triplet indices
            ! ... rhs index (buffered data)
            ii0 = mod(l0-1,N1g)+1
            jj0 = mod((l0-ii0)/N1g,N2g)+1
            kk0 = (l0-ii0-(jj0-1)*N1g)/(N1g*N2g)+1

            ! ... lhs index (buffered data)
            ii1 = mod(l1-1,N1g)+1
            jj1 = mod((l1-ii1)/N1g,N2g)+1
            kk1 = (l1-ii1-(jj1-1)*N1g)/(N1g*N2g)+1

            ! ... convert to global indices
            ii0 = ii0 + grid%is(1) - 1
            jj0 = jj0 + grid%is(2) - 1
            kk0 = kk0 + grid%is(3) - 1 - grid%nGhostRHS(dir,1)
            ii1 = ii1 + grid%is(1) - 1
            jj1 = jj1 + grid%is(2) - 1
            kk1 = kk1 + grid%is(3) - 1 - grid%nGhostRHS(dir,1)

            if (kk1 < grid%is(3) .OR. kk1 > grid%ie(3)) CYCLE

            ! ... convert l1 back to non-buffered counting
            l1 = (kk1-grid%is(3))*N1*N2 + (jj1-grid%is(2))*N1 + (ii1-grid%is(1)+1)


            ! ... intermediate values for l1 index
            JAC_l1     = grid%JAC(l1)
            XI_X_l1    = grid%MT1(l1,region%global%t2Map(dir,1)) * JAC_l1
            XI_Y_l1    = grid%MT1(l1,region%global%t2Map(dir,2)) * JAC_l1
            XI_Z_l1    = grid%MT1(l1,region%global%t2Map(dir,3)) * JAC_l1
            
            MU_l1      = state%tv(l1,1) * state%REinv
            LAMBDA_l1    = state%tv(l1,2) * state%REinv

            ! ... intermediate values for l0 index
            JAC_l0    = JACAux(l0)
            XI_X_l0   = XI_X_TILDEAux(l0) / JAC_l0
            XI_Y_l0   = XI_Y_TILDEAux(l0) / JAC_l0
            XI_Z_l0   = XI_Z_TILDEAux(l0) / JAC_l0

            MU_l0     = muAux(l0) * state%REinv
            LAMBDA_l0 = lambdaAux(l0) * state%REinv


            ! ... work on index at l0 first
            index = (l1-1)*stencil_size + input%HYPREStencilIndex(opID, ii0-ii1, jj0-jj1, kk0-kk1) + 1

            ! ... store in array at index l0
            values_ru(index) = values_ru(index) - 0.5_8 * local_val * &
                 ( XI_X_l0 * XI_Z_l0 * ( MU_l0 + LAMBDA_l0) + XI_X_l1 * XI_Z_l1 * ( MU_l1 + LAMBDA_l1) )

            values_rv(index) = values_rv(index) - 0.5_8 * local_val * &
                 ( XI_Y_l0 * XI_Z_l0 * ( MU_l0 + LAMBDA_l0) + XI_Y_l1 * XI_Z_l1 * ( MU_l1 + LAMBDA_l1) )
            
            values_rw(index) = values_rw(index) - 0.5_8 * local_val * &
                 ( (XI_Z_l0 * XI_Z_l0 * ( 2.0_8 * MU_l0 + LAMBDA_l0 ) + MU_l0 * ( XI_X_l0 * XI_X_l0 + XI_Y_l0 * XI_Y_l0 )) + &
                   (XI_Z_l1 * XI_Z_l1 * ( 2.0_8 * MU_l1 + LAMBDA_l1 ) + MU_l1 * ( XI_X_l1 * XI_X_l1 + XI_Y_l1 * XI_Y_l1 )) )


            ! ... now work on index at l1
            index = (l1-1)*stencil_size + 1

            ! ... store in array at index l1
            values_ru(index) = values_ru(index) + 0.5_8 * local_val * &
                 ( XI_X_l0 * XI_Z_l0 * ( MU_l0 + LAMBDA_l0) )

            values_rv(index) = values_rv(index) + 0.5_8 * local_val * &
                 ( XI_Y_l0 * XI_Z_l0 * ( MU_l0 + LAMBDA_l0) )
            
            values_rw(index) = values_rw(index) + 0.5_8 * local_val * &
                 ( (XI_Z_l0 * XI_Z_l0 * ( 2.0_8 * MU_l0 + LAMBDA_l0 ) + MU_l0 * ( XI_X_l0 * XI_X_l0 + XI_Y_l0 * XI_Y_l0 )) )

!             ! ... intermediate values
!             XI_X    = grid%MT1(l1,region%global%t2Map(3,1)) * JAC
!             XI_Y    = grid%MT1(l1,region%global%t2Map(3,2)) * JAC
!             XI_Z    = grid%MT1(l1,region%global%t2Map(3,3)) * JAC
            
!             MU      = state%tv(l1,1) * state%REinv
!             LAMBDA    = state%tv(l1,2) * state%REinv


!             ! ... index
!             index = (l1-1)*stencil_size + input%HYPREStencilIndex(opID, ii0-ii1, jj0-jj1, kk0-kk1) + 1

!             ! ... store in array
!             values_ru(index) = values_ru(index) + local_val * ( XI_X * XI_Z * ( MU + LAMBDA) ) 
!             values_rv(index) = values_rv(index) + local_val * ( XI_Y * XI_Z * ( MU + LAMBDA) ) 
!             values_rw(index) = values_rw(index) + local_val * ( XI_Z * XI_Z * ( 2.0_8 * MU + LAMBDA ) &
!                                + MU * ( XI_X * XI_X + XI_Y * XI_Y ) )

          end do
        end do
      end do

    CASE (5)

      ! ... viscous fluxes
      do k = 1, grid%vdsGhost(dir)
        do j = 1, grid%B(opID,dir)%n
          do i = grid%B(opID,dir)%row_ptr(k,j), grid%B(opID,dir)%row_ptr(k,j+1)-1

            ! ... l0 and l1 indices refer to BUFFERED data
            local_val = grid%B(opID,dir)%val(i,k)
            local_ind = grid%B(opID,dir)%col_ind(i,k)
            l0        = grid%vec_indGhost(k,local_ind,dir)
            l1        = grid%vec_indGhost(k,        j,dir)
    
            ! ... convert from linear to triplet indices
            ! ... rhs index (buffered data)
            ii0 = mod(l0-1,N1g)+1
            jj0 = mod((l0-ii0)/N1g,N2g)+1
            kk0 = (l0-ii0-(jj0-1)*N1g)/(N1g*N2g)+1

            ! ... lhs index (buffered data)
            ii1 = mod(l1-1,N1g)+1
            jj1 = mod((l1-ii1)/N1g,N2g)+1
            kk1 = (l1-ii1-(jj1-1)*N1g)/(N1g*N2g)+1

            ! ... convert to global indices
            ii0 = ii0 + grid%is(1) - 1
            jj0 = jj0 + grid%is(2) - 1
            kk0 = kk0 + grid%is(3) - 1 - grid%nGhostRHS(dir,1)
            ii1 = ii1 + grid%is(1) - 1
            jj1 = jj1 + grid%is(2) - 1
            kk1 = kk1 + grid%is(3) - 1 - grid%nGhostRHS(dir,1)

            if (kk1 < grid%is(3) .OR. kk1 > grid%ie(3)) CYCLE

            ! ... convert l1 back to non-buffered counting
            l1 = (kk1-grid%is(3))*N1*N2 + (jj1-grid%is(2))*N1 + (ii1-grid%is(1)+1)


            ! ... intermediate values for l1 index
            RHO_l1     = state%cv(l1,1)
            UX_l1      = state%cv(l1,2) / RHO_l1
            UY_l1      = state%cv(l1,3) / RHO_l1
            UZ_l1      = state%cv(l1,4) / RHO_l1
            JAC_l1     = grid%JAC(l1)
            XI_X_l1    = grid%MT1(l1,region%global%t2Map(dir,1)) * JAC_l1
            XI_Y_l1    = grid%MT1(l1,region%global%t2Map(dir,2)) * JAC_l1
            XI_Z_l1    = grid%MT1(l1,region%global%t2Map(dir,3)) * JAC_l1
            
            MU_l1      = state%tv(l1,1) * state%REinv
            LAMBDA_l1  = state%tv(l1,2) * state%REinv
            KAPPA_l1   = MU_l1 * state%PRinv

            ! ... intermediate values for l0 index
            RHO_l0    = rhoAux(l0)
            UX_l0     = rhouAux(l0) / RHO_l0
            UY_l0     = rhovAux(l0) / RHO_l0
            UZ_l0     = rhowAux(l0) / RHO_l0
            JAC_l0    = JACAux(l0)
            XI_X_l0   = XI_X_TILDEAux(l0) / JAC_l0
            XI_Y_l0   = XI_Y_TILDEAux(l0) / JAC_l0
            XI_Z_l0   = XI_Z_TILDEAux(l0) / JAC_l0

            MU_l0     = muAux(l0) * state%REinv
            LAMBDA_l0 = lambdaAux(l0) * state%REinv
            KAPPA_l0  = MU_l0 * state%PRinv


            ! ... index at l0 first
            index = (l1-1)*stencil_size + input%HYPREStencilIndex(opID, ii0-ii1, jj0-jj1, kk0-kk1) + 1

            ! ... store in array at index l0
            values_ru(index) = values_ru(index) - 0.5_8 * local_val * &
                 ( (UX_l0 * ( XI_X_l0 * XI_X_l0 * (2.0_8 * MU_l0 + LAMBDA_l0) + MU_l0 * ( XI_Y_l0 * XI_Y_l0 + XI_Z_l0 * XI_Z_l0) ) + &
                    UY_l0 * XI_X_l0 * XI_Y_l0 * ( MU_l0 + LAMBDA_l0 ) + &
                    UZ_l0 * XI_X_l0 * XI_Z_l0 * ( MU_l0 + LAMBDA_l0 )) + &
                   (UX_l1 * ( XI_X_l1 * XI_X_l1 * (2.0_8 * MU_l1 + LAMBDA_l1) + MU_l1 * ( XI_Y_l1 * XI_Y_l1 + XI_Z_l1 * XI_Z_l1) ) + &
                    UY_l1 * XI_X_l1 * XI_Y_l1 * ( MU_l1 + LAMBDA_l1 ) + &
                    UZ_l0 * XI_X_l1 * XI_Z_l1 * ( MU_l1 + LAMBDA_l1 )) )

            values_rv(index) = values_rv(index) - 0.5_8 * local_val * &
                 ( (UY_l0 * ( XI_Y_l0 * XI_Y_l0 * (2.0_8 * MU_l0 + LAMBDA_l0) + MU_l0 * ( XI_X_l0 * XI_X_l0 + XI_Z_l0 * XI_Z_l0)) + &
                    UX_l0 * XI_X_l0 * XI_Y_l0 * ( MU_l0 + LAMBDA_l0 ) + &
                    UZ_l0 * XI_Y_l0 * XI_Z_l0 * ( MU_l0 + LAMBDA_l0 )) + &
                   (UY_l1 * ( XI_Y_l1 * XI_Y_l1 * (2.0_8 * MU_l1 + LAMBDA_l1) + MU_l1 * ( XI_X_l1 * XI_X_l1 + XI_Z_l1 * XI_Z_l1)) + &
                    UX_l1 * XI_X_l1 * XI_Y_l1 * ( MU_l1 + LAMBDA_l1 ) + &
                    UZ_l1 * XI_Y_l1 * XI_Z_l1 * ( MU_l1 + LAMBDA_l1 )) )

            values_rw(index) = values_rw(index) - 0.5_8 * local_val * &
                 ( (UZ_l0 * ( XI_Z_l0 * XI_Z_l0 * (2.0_8 * MU_l0 + LAMBDA_l0) + MU_l0 * ( XI_X_l0 * XI_X_l0 + XI_Y_l0 * XI_Y_l0)) + &
                    UX_l0 * XI_X_l0 * XI_Z_l0 * ( MU_l0 + LAMBDA_l0 ) + &
                    UY_l0 * XI_Y_l0 * XI_Z_l0 * ( MU_l0 + LAMBDA_l0 )) + &
                   (UZ_l1 * ( XI_Z_l1 * XI_Z_l1 * (2.0_8 * MU_l1 + LAMBDA_l1) + MU_l1 * ( XI_X_l1 * XI_X_l1 + XI_Y_l1 * XI_Y_l1)) + &
                    UX_l1 * XI_X_l1 * XI_Z_l1 * ( MU_l1 + LAMBDA_l1 ) + &
                    UY_l1 * XI_Y_l1 * XI_Z_l1 * ( MU_l1 + LAMBDA_l1 )) )

            values_rE(index) = values_rE(index) - 0.5_8 * local_val * &
                 ( KAPPA_l0 * ( XI_X_l0 * XI_X_l0 + XI_Y_l0 * XI_Y_l0 + XI_Z_l0 * XI_Z_l0 ) + &
                   KAPPA_l1 * ( XI_X_l1 * XI_X_l1 + XI_Y_l1 * XI_Y_l1 + XI_Z_l1 * XI_Z_l1 ) )


            ! ... now work on index at l1
            index = (l1-1)*stencil_size + 1

            ! ... store in array at index l1
            values_ru(index) = values_ru(index) + 0.5_8 * local_val * &
                 ( (UX_l0 * ( XI_X_l0 * XI_X_l0 * (2.0_8 * MU_l0 + LAMBDA_l0) + MU_l0 * ( XI_Y_l0 * XI_Y_l0 + XI_Z_l0 * XI_Z_l0) ) + &
                    UY_l0 * XI_X_l0 * XI_Y_l0 * ( MU_l0 + LAMBDA_l0 ) + &
                    UZ_l0 * XI_X_l0 * XI_Z_l0 * ( MU_l0 + LAMBDA_l0 )) )

            values_rv(index) = values_rv(index) + 0.5_8 * local_val * &
                 ( (UY_l0 * ( XI_Y_l0 * XI_Y_l0 * (2.0_8 * MU_l0 + LAMBDA_l0) + MU_l0 * ( XI_X_l0 * XI_X_l0 + XI_Z_l0 * XI_Z_l0)) + &
                    UX_l0 * XI_X_l0 * XI_Y_l0 * ( MU_l0 + LAMBDA_l0 ) + &
                    UZ_l0 * XI_Y_l0 * XI_Z_l0 * ( MU_l0 + LAMBDA_l0 )) )

            values_rw(index) = values_rw(index) + 0.5_8 * local_val * &
                 ( (UZ_l0 * ( XI_Z_l0 * XI_Z_l0 * (2.0_8 * MU_l0 + LAMBDA_l0) + MU_l0 * ( XI_X_l0 * XI_X_l0 + XI_Y_l0 * XI_Y_l0)) + &
                    UX_l0 * XI_X_l0 * XI_Z_l0 * ( MU_l0 + LAMBDA_l0 ) + &
                    UY_l0 * XI_Y_l0 * XI_Z_l0 * ( MU_l0 + LAMBDA_l0 )) )

            values_rE(index) = values_rE(index) + 0.5_8 * local_val * &
                 ( KAPPA_l0 * ( XI_X_l0 * XI_X_l0 + XI_Y_l0 * XI_Y_l0 + XI_Z_l0 * XI_Z_l0 ) )

!             ! ... intermediate values
!             UX      = state%cv(l1,2) * state%dv(l1,3)
!             UY      = state%cv(l1,3) * state%dv(l1,3)
!             UZ      = state%cv(l1,4) * state%dv(l1,3)
!             JAC     = grid%JAC(l1)
!             XI_X    = grid%MT1(l1,region%global%t2Map(3,1)) * JAC
!             XI_Y    = grid%MT1(l1,region%global%t2Map(3,2)) * JAC
!             XI_Z    = grid%MT1(l1,region%global%t2Map(3,3)) * JAC

!             MU      = state%tv(l1,1) * state%REinv
!             KAPPA   = MU * state%PRinv
!             LAMBDA    = state%tv(l1,2) * state%REinv

!             ! ... index
!             index = (l1-1)*stencil_size + input%HYPREStencilIndex(opID, ii0-ii1, jj0-jj1, kk0-kk1) + 1

!             ! ... store in array
!             values_ru(index) = values_ru(index) + local_val * ( UX * ( XI_X * XI_X * (2.0_8 * MU + LAMBDA) &
!                                + MU * ( XI_Y * XI_Y + XI_Z * XI_Z) ) + UY * XI_X * XI_Y * ( MU + LAMBDA ) &
!                                + UZ * XI_X * XI_Z * ( MU + LAMBDA ) )
!             values_rv(index) = values_rv(index) + local_val * ( UY * ( XI_Y * XI_Y * (2.0_8 * MU + LAMBDA) &
!                                + MU * ( XI_X * XI_X + XI_Z * XI_Z) ) + UX * XI_X * XI_Y * ( MU + LAMBDA ) &
!                                + UZ * XI_Y * XI_Z * ( MU + LAMBDA ) )
!             values_rw(index) = values_rw(index) + local_val * ( UZ * ( XI_Z * XI_Z * (2.0_8 * MU + LAMBDA) &
!                                + MU * ( XI_X * XI_X + XI_Y * XI_Y) ) + UX * XI_X * XI_Z * ( MU + LAMBDA ) &
!                                + UY * XI_Y * XI_Z * ( MU + LAMBDA ) )
!             values_rE(index) = values_rE(index) + local_val * KAPPA * ( XI_X * XI_X + XI_Y * XI_Y + XI_Z * XI_Z )

          end do
        end do
      end do

   END SELECT

   ! ... deallocate auxiliary vectors
   deallocate(rhoAux,rhouAux,rhovAux,rhowAux,rhoEAux,muAux,lambdaAux,JACAux,XI_X_TILDEAux,XI_Y_TILDEAux,XI_Z_TILDEAux)

!! !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!! !!! Chris is debugging
!! !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

!!       ! ... viscous fluxes
!!       do k = 1, grid%vdsGhost(dir)
!!         do j = 1, grid%B(opID,dir)%n
!!           do i = grid%B(opID,dir)%row_ptr(k,j), grid%B(opID,dir)%row_ptr(k,j+1)-1

!!             ! ... l0 and l1 indices refer to BUFFERED data
!!             local_val = grid%B(opID,dir)%val(i,k)
!!             local_ind = grid%B(opID,dir)%col_ind(i,k)
!!             l0        = grid%vec_indGhost(k,local_ind,dir)
!!             l1        = grid%vec_indGhost(k,        j,dir)
    
!!             ! ... convert from linear to triplet indices
!!             ! ... rhs index (buffered data)
!!             ii0 = mod(l0-1,N1g)+1
!!             jj0 = mod((l0-ii0)/N1g,N2g)+1
!!             kk0 = (l0-ii0-(jj0-1)*N1g)/(N1g*N2g)+1

!!             ! ... lhs index (buffered data)
!!             ii1 = mod(l1-1,N1g)+1
!!             jj1 = mod((l1-ii1)/N1g,N2g)+1
!!             kk1 = (l1-ii1-(jj1-1)*N1g)/(N1g*N2g)+1

!!             ! ... convert to global indices
!!             ii0 = ii0 + grid%is(1) - 1
!!             jj0 = jj0 + grid%is(2) - 1
!!             kk0 = kk0 + grid%is(3) - 1 - grid%nGhostRHS(dir,1)
!!             ii1 = ii1 + grid%is(1) - 1
!!             jj1 = jj1 + grid%is(2) - 1
!!             kk1 = kk1 + grid%is(3) - 1 - grid%nGhostRHS(dir,1)

!!             if (kk1 < grid%is(3) .OR. kk1 > grid%ie(3)) CYCLE

!!             ! ... convert l1 back to non-buffered counting
!!             l1 = (kk1-grid%is(3))*N1*N2 + (jj1-grid%is(2))*N1 + (ii1-grid%is(1)+1)

!!             ! ... intermediate values
!!             UX      = state%cv(l1,2) * state%dv(l1,3)
!!             UY      = state%cv(l1,3) * state%dv(l1,3)
!!             UY      = state%cv(l1,4) * state%dv(l1,3)
!!             JAC     = grid%JAC(l1)
!!             XI_X    = grid%MT1(l1,region%global%t2Map(3,1)) * JAC
!!             XI_Y    = grid%MT1(l1,region%global%t2Map(3,2)) * JAC
!!             XI_Z    = grid%MT1(l1,region%global%t2Map(3,3)) * JAC

!!             MU      = state%tv(l1,1) * state%REinv
!!             KAPPA   = MU * state%PRinv
!!             LAMBDA    = state%tv(l1,2) * state%REinv

!!             ! ... index
!!             index = (l1-1)*stencil_size + input%HYPREStencilIndex(order, ii0-ii1, jj0-jj1, kk0-kk1) + 1

!!             ! ... store in array
!!             values_r(index) = 0.0

!!             values_ru(index) = 0.0
!!             values_rv(index) = 0.0
                               
                               
!!             values_rw(index) = 0.0
                               
                               
!!             values_rE(index) = 0.0

!!             select case(lhs_var)
!!                case(1)
!!                   values_r(index) = 4.0
!!                case(2)
!!                   values_ru(index) = 4.0
!!                case(3)
!!                   values_rv(index) = 4.0
!!                case(4)
!!                   values_rw(index) = 4.0
!!                case(5)
!!                   values_rE(index) = 4.0
!!                end select
!!           end do
!!         end do
!!       end do

!! !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!! !!! Chris is debugging
!! !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

    Return

  End Subroutine NSImplicit3D


  
  Subroutine NSImplicitAddBC3D(region, ng, lhs_var, values_r, values_ru, values_rv, values_rw, values_rE, stencil_size, dtau)

    USE ModGlobal
    USE ModDataStruct
    USE ModDeriv
    USE ModMPI

    Implicit None

    ! ... global variables
    type(t_region), pointer :: region
    type(t_mixt_input), pointer :: input
    type(t_grid), pointer :: grid
    type(t_mixt), pointer :: state
    real(rfreal), pointer :: values_r(:), values_ru(:), values_rv(:), values_rw(:), values_rE(:)
    real(rfreal) :: dtau
    integer :: lhs_var, ng, stencil_size

    ! ... local variables
    type(t_patch), pointer :: patch
    integer :: npatch, i, j, k, l0, lp, N(MAX_ND), Np(MAX_ND), index
    real(rfreal) :: gam, gamm1i, RHO, P, T, RHO_P, RHO_T, UX, UY, UZ, KE, SAT_MAT(5), INVJAC, JAC, rel_pos(MAX_ND), del, eta, sponge_amp

    ! ... current grid
    grid  => region%grid(ng)
    state => region%state(ng)
    input => grid%input

    ! ... loop over all patches and pick the ones that have the same grid
    Do npatch = 1, region%nPatches

      patch => region%patch(npatch)

      ! ... check to see if grid ID matches
      if (patch%gridID /= ng) CYCLE

      ! ... check to see if this boundary is SAT
      if (associated(patch%implicit_sat_mat) .eqv. .false.) CYCLE

      ! ... now that we have a match, add the values of patch%implicit_sat_mat to values_*
      ! ... the SAT boundary conditions are local, so we'll only be adding on the diagonal
      N(:) = 1; Np(:) = 1;
      do j = 1, grid%ND
        N(j)  =  grid%ie(j) -  grid%is(j) + 1
        Np(j) = patch%ie(j) - patch%is(j) + 1
      end do;

      Do k = patch%is(3), patch%ie(3)
        Do j = patch%is(2), patch%ie(2)
          Do i = patch%is(1), patch%ie(1)

            ! ... indices
            l0 = (k- grid%is(3))* N(1)* N(2) + (j- grid%is(2))* N(1) + (i- grid%is(1)+1)
            lp = (k-patch%is(3))*Np(1)*Np(2) + (j-patch%is(2))*Np(1) + (i-patch%is(1)+1)


            ! ... current SAT values
            SAT_MAT(1) = patch%implicit_sat_mat(lp,lhs_var,1)! + patch%implicit_sat_mat_old(lp,lhs_var,1)
            SAT_MAT(2) = patch%implicit_sat_mat(lp,lhs_var,2)! + patch%implicit_sat_mat_old(lp,lhs_var,2)
            SAT_MAT(3) = patch%implicit_sat_mat(lp,lhs_var,3)! + patch%implicit_sat_mat_old(lp,lhs_var,3)
            SAT_MAT(4) = patch%implicit_sat_mat(lp,lhs_var,4)! + patch%implicit_sat_mat_old(lp,lhs_var,4)
            SAT_MAT(5) = patch%implicit_sat_mat(lp,lhs_var,5)! + patch%implicit_sat_mat_old(lp,lhs_var,4)


            ! ... HYPRE index
            index = (l0-1)*stencil_size + 1
!            if(i>400 .and. j == 100) print*,'myrank,i,j,l0,lp,SAT_MAT,index',region%myrank,i,j,l0,lp,SAT_MAT,index


            ! ... add the values
             values_r(index) =  values_r(index) - dtau * SAT_MAT(1)
            values_ru(index) = values_ru(index) - dtau * SAT_MAT(2)
            values_rv(index) = values_rv(index) - dtau * SAT_MAT(3)
            values_rw(index) = values_rw(index) - dtau * SAT_MAT(4)
            values_rE(index) = values_rE(index) - dtau * SAT_MAT(5)

!             ! ... temporary values
!             gam     = state%gv(l0,1)
!             gamm1i  = 1.0_8 / (gam - 1.0_8)
!             RHO     = state%cv(l0,1)
!             P       = state%dv(l0,1)
!             T       = state%dv(l0,2)
!             RHO_P   = gam * gamm1i / T
!             RHO_T   = -P / T * RHO_P
!             UX      = state%cv(l0,2) / RHO
!             UY      = state%cv(l0,3) / RHO
!             UZ      = state%cv(l0,4) / RHO
!             KE      = 0.5_8 * (UX * UX + UY * UY + UZ * UZ)

!             ! ... current SAT values
!             SAT_MAT(1) = patch%implicit_sat_mat(lp,lhs_var,1)
!             SAT_MAT(2) = patch%implicit_sat_mat(lp,lhs_var,2)
!             SAT_MAT(3) = patch%implicit_sat_mat(lp,lhs_var,3)
!             SAT_MAT(4) = patch%implicit_sat_mat(lp,lhs_var,4)
!             SAT_MAT(5) = patch%implicit_sat_mat(lp,lhs_var,5)

!             ! ... HYPRE index
!             index = (l0-1)*stencil_size + 1

!             ! ... add the values
!              values_r(index) =  values_r(index) - dtau * (SAT_MAT(1) * RHO_P + SAT_MAT(5) * RHO_T)
!             values_ru(index) = values_ru(index) - dtau * (UX * RHO_P * SAT_MAT(1) + RHO * SAT_MAT(2) + UX * RHO_T * SAT_MAT(5))
!             values_rv(index) = values_rv(index) - dtau * (UY * RHO_P * SAT_MAT(1) + RHO * SAT_MAT(3) + UY * RHO_T * SAT_MAT(5))
!             values_rw(index) = values_rw(index) - dtau * (UZ * RHO_P * SAT_MAT(1) + RHO * SAT_MAT(4) + UZ * RHO_T * SAT_MAT(5))
!             values_rE(index) = values_rE(index) - dtau * ((GAMM1I + KE * RHO_P) * SAT_MAT(1) + RHO * UX * SAT_MAT(2) &
!                  + RHO * UY * SAT_MAT(3) + RHO * UZ * SAT_MAT(4) + KE * RHO_T * SAT_MAT(5))

          End Do
        End Do
      End Do

    End Do

    ! ... now for the sponge zones
    ! ... loop over all patches and pick the ones that have the same grid
    Do npatch = 1, region%nPatches

      patch => region%patch(npatch)

      ! ... check to see if grid ID matches
      if (patch%gridID /= ng) CYCLE

      ! ... check to see if this boundary is SAT
      if (patch%bcType /= SPONGE) CYCLE

      N(:) = 1; Np(:) = 1;
      do j = 1, grid%ND
        N(j)  =  grid%ie(j) -  grid%is(j) + 1
        Np(j) = patch%ie(j) - patch%is(j) + 1
      end do;

      Do k = patch%is(3), patch%ie(3)
        Do j = patch%is(2), patch%ie(2)
          Do i = patch%is(1), patch%ie(1)

            ! ... indices
            l0 = (k- grid%is(3))* N(1)* N(2) + (j- grid%is(2))* N(1) + (i- grid%is(1)+1)
            lp = (k-patch%is(3))*Np(1)*Np(2) + (j-patch%is(2))*Np(1) + (i-patch%is(1)+1)

            ! ... HYPRE index
            index = (l0-1)*stencil_size + 1

            ! ... intermediate values
            gam     = state%gv(l0,1)
            gamm1i  = 1.0_8 / (gam - 1.0_8)
            P       = state%dv(l0,1)
            T       = state%dv(l0,2)
            RHO_P   = gam * gamm1i / T
            RHO_T   = -P / T * RHO_P
            UX      = state%cv(l0,2) * state%dv(l0,3)
            UY      = state%cv(l0,3) * state%dv(l0,3) 
            UZ      = state%cv(l0,4) * state%dv(l0,3)
            RHO     = state%cv(l0,1)
            KE      = 0.5_8 * ( UX * UX + UY * UY )
            INVJAC  = grid%INVJAC(l0)
            JAC     = grid%JAC(l0)

            ! ... distance from boundary to inner sponge start
            rel_pos(1:grid%ND) = patch%sponge_xs(lp,1:grid%ND) - patch%sponge_xe(lp,1:grid%ND)
            del = sqrt(dot_product(rel_pos(1:grid%ND),rel_pos(1:grid%ND)))

            ! ... distance from inner sponge start to point
            rel_pos(1:grid%ND) = patch%sponge_xs(lp,1:grid%ND) - grid%XYZ(l0,1:grid%ND)
            eta = sqrt(dot_product(rel_pos(1:grid%ND),rel_pos(1:grid%ND))) / del

            sponge_amp = 0.0_rfreal
            if (eta .le. 1.0_rfreal) sponge_amp = (input%sponge_amp) * eta**(input%sponge_pow)

            ! ... add the values
            Select Case (lhs_var)
            Case (1)
               values_r(index) =  values_r(index) + dtau * sponge_amp * RHO_P * JAC
              values_rE(index) = values_rE(index) + dtau * sponge_amp * RHO_T * JAC
            Case (2)
               values_r(index) =  values_r(index) + dtau * sponge_amp * RHO_P * UX * JAC
              values_ru(index) = values_ru(index) + dtau * sponge_amp * RHO * JAC
              values_rE(index) = values_rE(index) + dtau * sponge_amp * RHO_T * UX * JAC
            Case (3)
               values_r(index) =  values_r(index) + dtau * sponge_amp * RHO_P * UY * JAC
              values_rv(index) = values_rv(index) + dtau * sponge_amp * RHO * JAC
              values_rE(index) = values_rE(index) + dtau * sponge_amp * RHO_T * UY * JAC
            Case (4)
               values_r(index) =  values_r(index) + dtau * sponge_amp * RHO_P * UZ * JAC
              values_rw(index) = values_rw(index) + dtau * sponge_amp * RHO * JAC
              values_rE(index) = values_rE(index) + dtau * sponge_amp * RHO_T * UZ * JAC
            Case (5)
               values_r(index) =  values_r(index) + dtau * sponge_amp * (gamm1i + RHO_P * KE) * JAC
              values_ru(index) = values_ru(index) + dtau * sponge_amp * RHO * UX * JAC
              values_rv(index) = values_rv(index) + dtau * sponge_amp * RHO * UY * JAC
              values_rw(index) = values_rw(index) + dtau * sponge_amp * RHO * UZ * JAC 
              values_rE(index) = values_rE(index) + dtau * sponge_amp * RHO_T * KE * JAC
           End Select

          End Do
        End Do
      End Do

   End Do
  
  End Subroutine NSImplicitAddBC3D

  Subroutine NSImplicitInitSoln3D(grid, state, values_r, values_ru, values_rv, values_rw, values_rE)

    USE ModGlobal
    USE ModDataStruct
    USE ModDeriv
    USE ModMPI

    Implicit None

    ! ... global variables
    type(t_grid), pointer :: grid
    type(t_mixt), pointer :: state
    real(rfreal), pointer :: values_r(:), values_ru(:), values_rv(:), values_rw(:), values_rE(:)
  
    ! ... local variables
    type(t_patch), pointer :: patch
    integer :: npatch, i, j, k, l0, lp, N(MAX_ND), Np(MAX_ND), index
    real(rfreal) :: GAM, GAMM1I, RHO, P, T, RHO_P, RHO_T, UX, UY, UZ, KE, SPVOL, diff_r, diff_ru, diff_rv, diff_rw, diff_rE

    ! ... convert from Delta Q to Delta Q_p/J
    Do i = 1, grid%nCells

      ! ... temporary values
      GAM     = state%gv(i,1)
      RHO     = state%cv(i,1)
      SPVOL   = 1.0_8 / RHO
      P       = state%dv(i,1)
      T       = state%dv(i,2)
      UX      = state%cv(i,2) * SPVOL
      UY      = state%cv(i,3) * SPVOL
      UZ      = state%cv(i,4) * SPVOL
      KE      = 0.5_8 * (UX * UX + UY * UY + UZ * UZ)
      diff_r  = (state%cv(i,1) - state%cvOld(i,1)) * grid%INVJAC(i)
      diff_ru = (state%cv(i,2) - state%cvOld(i,2)) * grid%INVJAC(i)
      diff_rv = (state%cv(i,3) - state%cvOld(i,3)) * grid%INVJAC(i)
      diff_rw = (state%cv(i,4) - state%cvOld(i,4)) * grid%INVJAC(i)
      diff_rE = (state%cv(i,5) - state%cvOld(i,5)) * grid%INVJAC(i)

       values_r(i) = (GAM-1.0_8) * KE * diff_r + UX * (1.0_8-GAM) * diff_ru + UY * (1.0_8-GAM) * diff_rv + UZ * (1.0_8-GAM) * diff_rw &
                      + (GAM-1.0_8) * diff_rE
      values_ru(i) = (-UX * SPVOL) * diff_r + SPVOL * diff_ru
      values_rv(i) = (-UY * SPVOL) * diff_r + SPVOL * diff_rv
      values_rw(i) = (-UZ * SPVOL) * diff_r + SPVOL * diff_rw
      values_rE(i) = (GAM * KE - T) * SPVOL * diff_r - (GAM * UX * SPVOL) * diff_ru - (GAM * UY * SPVOL) * diff_rv &
                      - (GAM * UZ * SPVOL) * diff_rw + GAM * SPVOL * diff_rE

    End Do

  End Subroutine NSImplicitInitSoln3D


!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!  Analytical RHS: LHS 

  Subroutine NSImplicit2DAn(myrank, input, grid, state, global, lhs_var, values_r, values_ru, values_rv, values_rE, stencil_size, dtau)

    USE ModGlobal
    USE ModDataStruct
    USE ModDeriv
    USE ModMPI

    Implicit None

    ! ... global variables
    integer :: myrank
    type(t_mixt_input), pointer :: input
    type(t_grid), pointer :: grid
    type(t_mixt), pointer :: state
    type(t_modglobal), pointer :: global
    real(rfreal), pointer :: values_r(:), values_ru(:), values_rv(:), values_rE(:)
    real(rfreal) :: dtau
    integer :: lhs_var

    ! ... local variables
    integer :: ng, k, j, i, ndim, npart, my_part, local_ind, l0, l1, order, dir, opID
    integer :: is(MAX_ND), ie(MAX_ND), varTypes(MAX_ND+2)
    integer :: icomm, igrid, min_stencil_start, max_stencil_end, stencil_size, rhs_var, lhs_var_copy
    integer :: ii0, jj0, kk0, ii1, jj1, kk1, N1, N2, N3, index, N1g, N2g, N3g, ii, jj, kk
    real(rfreal) :: RHO_P, RHO_T, gam, gamm1i, UX, RHOE_P, RHOE_T, RHOE_UX, UY, RHO, RHOE_U, RHOE_V, KE, P, T, RHOE, local_val, jac
    real(rfreal) :: XI_X, XI_Y, PC(4), MI2, MMIN2, MU2, SNDSPD2, SNDSPD, MP2, EPS_P, RHO_PP, H0, H_T, H_P, LU, time_fac, MU, KAPPA

    ! ... initialize arrays
    Do i = 1, grid%nCells*stencil_size
      values_r(i)  = 0.0_8
      values_ru(i) = 0.0_8
      values_rv(i) = 0.0_8
      values_rE(i) = 0.0_8
    End Do

   if (myrank == 0) write(*,'(A)') 'Analytical right hand side ...'

!!     ! ... x-direction, first derivatives
!!     dir   = 1
!!     order = 1
!!     opID  = input%iFirstDerivImplicit

!!     ! ... local partition size
!!     N1 = grid%ie(1)-grid%is(1)+1
!!     N2 = grid%ie(2)-grid%is(2)+1
!!     N3 = grid%ie(3)-grid%is(3)+1

!!     ! ... partition size including ghost
!!     N1g = grid%ie(1)-grid%is(1)+1+SUM(grid%nGhostRHS(dir,:)) ! (MIN(grid%nGhostRHS(dir,1),1)+MIN(grid%nGhostRHS(dir,2),1))*input%overlap(opID)
!!     N2g = grid%ie(2)-grid%is(2)+1
!!     N3g = grid%ie(3)-grid%is(3)+1

!!     SELECT CASE (lhs_var)
!!     CASE (1)

!!       ! ... A_p {delta_\xi} where
!!       !
!!       !       | u rho_p              rho                            0              u rho_T     |
!!       ! A_p = | rho_p u^2 + 1        2 rho u                        0              u^2 rho_T   |
!!       !       | rho_p u v            rho v                          rho u          u v rho_T   |
!!       !     = | [(rho E)_p + 1] u    [(rho E + p) + u (rho E)_u]    u (rho E)_v    u (rho E)_T |
!!       !
!!       !
!!       ! ... inviscid fluxes
!!       do k = 1, grid%vdsGhost(dir)
!!         do j = 1, grid%B(opID,dir)%n
!!           do i = grid%B(opID,dir)%row_ptr(k,j), grid%B(opID,dir)%row_ptr(k,j+1)-1

!!             ! ... l0 and l1 indices refer to BUFFERED data
!!             local_val = grid%B(opID,dir)%val(i,k)
!!             local_ind = grid%B(opID,dir)%col_ind(i,k)
!!             l0        = grid%vec_indGhost(k,local_ind,dir)
!!             l1        = grid%vec_indGhost(k,        j,dir)
    
!!             ! ... convert from linear to triplet indices
!!             ! ... rhs index (buffered data)
!!             ii0 = mod(l0-1,N1g)+1
!!             jj0 = mod((l0-ii0)/N1g,N2g)+1
!!             kk0 = (l0-ii0-(jj0-1)*N1g)/(N1g*N2g)+1

!!             ! ... lhs index (buffered data)
!!             ii1 = mod(l1-1,N1g)+1
!!             jj1 = mod((l1-ii1)/N1g,N2g)+1
!!             kk1 = (l1-ii1-(jj1-1)*N1g)/(N1g*N2g)+1

!!             ! ... convert to global indices
!!             ii0 = ii0 + grid%is(1) - 1 - grid%nGhostRHS(dir,1)
!!             jj0 = jj0 + grid%is(2) - 1
!!             kk0 = kk0 + grid%is(3) - 1
!!             ii1 = ii1 + grid%is(1) - 1 - grid%nGhostRHS(dir,1)
!!             jj1 = jj1 + grid%is(2) - 1
!!             kk1 = kk1 + grid%is(3) - 1

!!             if (ii1 < grid%is(1) .OR. ii1 > grid%ie(1)) CYCLE

!!             ! ... convert l1 back to non-buffered counting
!!             l1 = (kk1-grid%is(3))*N1*N2 + (jj1-grid%is(2))*N1 + (ii1-grid%is(1)+1)

!!             ! ... intermediate values
!!             gam     = state%gv(l1,1)
!!             gamm1i  = 1.0_8 / (gam - 1.0_8)
!!             P       = state%dv(l1,1)
!!             T       = state%dv(l1,2)
!!             RHO_P   = gam * gamm1i / T
!!             RHO_T   = -P / T * RHO_P
!!             UX      = state%cv(l1,2) * state%dv(l1,3)
!!             UY      = state%cv(l1,3) * state%dv(l1,3)
!!             RHO     = state%cv(l1,1)
!!             RHOE_U  = UX * RHO
!!             RHOE_V  = UY * RHO
!!             KE      = 0.5_8 * (UX * UX + UY * UY)
!!             RHOE_P  = gamm1i + KE * RHO_P
!!             RHOE_T  = KE * RHO_T
!!             JAC     = grid%JAC(l1)
!!             XI_X    = grid%MT1(l1,region%global%t2Map(1,1)) * JAC
!!             XI_Y    = grid%MT1(l1,region%global%t2Map(1,2)) * JAC

!!             ! ... index
!!             index = (l1-1)*stencil_size + input%HYPREStencilIndex(order, ii0-ii1, jj0-jj1, kk0-kk1) + 1
!! !!$            if (region%myrank == 1) print *, 'center  point ', ii1, jj1, kk1
!! !!$            if (region%myrank == 1) print *, '  stencil point ', ii0, jj0, kk0
!! !!$            if (region%myrank == 1) print *, '  offset        ', ii0-ii1, jj0-jj1, kk0-kk1
!! !!$            if (region%myrank == 1) print *, '  local_val     ', local_val

!!             ! ... store in array
!!             values_r(index) = values_r(index) + local_val * ( RHO_P * (UX * XI_X + UY * XI_Y) )
!!             values_ru(index) = values_ru(index) + local_val * ( RHO * XI_X )
!!             values_rv(index) = values_rv(index) + local_val * ( RHO * XI_Y )
!!             values_rE(index) = values_rE(index) + local_val * ( RHO_T * (UX * XI_X + UY * XI_Y) )
         
!!             ! dF(l1)    = dF(l1) + local_val * F(l0)
!!           end do
!!         end do
!!       end do

!!     CASE (2)

!!       do k = 1, grid%vdsGhost(dir)
!!         do j = 1, grid%B(opID,dir)%n
!!           do i = grid%B(opID,dir)%row_ptr(k,j), grid%B(opID,dir)%row_ptr(k,j+1)-1

!!             ! ... l0 and l1 indices refer to BUFFERED data
!!             local_val = grid%B(opID,dir)%val(i,k)
!!             local_ind = grid%B(opID,dir)%col_ind(i,k)
!!             l0        = grid%vec_indGhost(k,local_ind,dir)
!!             l1        = grid%vec_indGhost(k,        j,dir)
    
!!             ! ... convert from linear to triplet indices
!!             ! ... rhs index (buffered data)
!!             ii0 = mod(l0-1,N1g)+1
!!             jj0 = mod((l0-ii0)/N1g,N2g)+1
!!             kk0 = (l0-ii0-(jj0-1)*N1g)/(N1g*N2g)+1

!!             ! ... lhs index (buffered data)
!!             ii1 = mod(l1-1,N1g)+1
!!             jj1 = mod((l1-ii1)/N1g,N2g)+1
!!             kk1 = (l1-ii1-(jj1-1)*N1g)/(N1g*N2g)+1

!!             ! ... convert to global indices
!!             ii0 = ii0 + grid%is(1) - 1 - grid%nGhostRHS(dir,1)
!!             jj0 = jj0 + grid%is(2) - 1
!!             kk0 = kk0 + grid%is(3) - 1
!!             ii1 = ii1 + grid%is(1) - 1 - grid%nGhostRHS(dir,1)
!!             jj1 = jj1 + grid%is(2) - 1
!!             kk1 = kk1 + grid%is(3) - 1

!!             if (ii1 < grid%is(1) .OR. ii1 > grid%ie(1)) CYCLE

!!             ! ... convert l1 back to non-buffered counting
!!             l1 = (kk1-grid%is(3))*N1*N2 + (jj1-grid%is(2))*N1 + (ii1-grid%is(1)+1)

!!             ! ... intermediate values
!!             gam     = state%gv(l1,1)
!!             gamm1i  = 1.0_8 / (gam - 1.0_8)
!!             P       = state%dv(l1,1)
!!             T       = state%dv(l1,2)
!!             RHO_P   = gam * gamm1i / T
!!             RHO_T   = -P / T * RHO_P
!!             UX      = state%cv(l1,2) * state%dv(l1,3)
!!             UY      = state%cv(l1,3) * state%dv(l1,3)
!!             RHO     = state%cv(l1,1)
!!             RHOE_U  = UX * RHO
!!             RHOE_V  = UY * RHO
!!             KE      = 0.5_8 * (UX * UX + UY * UY)
!!             RHOE_P  = gamm1i + KE * RHO_P
!!             RHOE_T  = KE * RHO_T
!!             JAC     = grid%JAC(l1)
!!             XI_X    = grid%MT1(l1,region%global%t2Map(1,1)) * JAC
!!             XI_Y    = grid%MT1(l1,region%global%t2Map(1,2)) * JAC

!!             ! ... index
!!             index = (l1-1)*stencil_size + input%HYPREStencilIndex(order, ii0-ii1, jj0-jj1, kk0-kk1) + 1

!!             ! ... store in array
!!             values_r (index) = values_r (index) + local_val * ( RHO_P * UX * ( UX * XI_X + UY * XI_Y ) + XI_X )
!!             values_ru(index) = values_ru(index) + local_val * ( RHO * ( 2.0_8 * UX * XI_X + UY * XI_Y ) )
!!             values_rv(index) = values_rv(index) + local_val * ( RHO * UX * XI_Y )
!!             values_rE(index) = values_rE(index) + local_val * ( RHO_T * UX * ( UX * XI_X + UY * XI_Y ) )
         
!!             ! dF(l1)    = dF(l1) + local_val * F(l0)
!!           end do
!!         end do
!!       end do

!!     CASE (3)

!!       do k = 1, grid%vdsGhost(dir)
!!         do j = 1, grid%B(opID,dir)%n
!!           do i = grid%B(opID,dir)%row_ptr(k,j), grid%B(opID,dir)%row_ptr(k,j+1)-1

!!             ! ... l0 and l1 indices refer to BUFFERED data
!!             local_val = grid%B(opID,dir)%val(i,k)
!!             local_ind = grid%B(opID,dir)%col_ind(i,k)
!!             l0        = grid%vec_indGhost(k,local_ind,dir)
!!             l1        = grid%vec_indGhost(k,        j,dir)
    
!!             ! ... convert from linear to triplet indices
!!             ! ... rhs index (buffered data)
!!             ii0 = mod(l0-1,N1g)+1
!!             jj0 = mod((l0-ii0)/N1g,N2g)+1
!!             kk0 = (l0-ii0-(jj0-1)*N1g)/(N1g*N2g)+1

!!             ! ... lhs index (buffered data)
!!             ii1 = mod(l1-1,N1g)+1
!!             jj1 = mod((l1-ii1)/N1g,N2g)+1
!!             kk1 = (l1-ii1-(jj1-1)*N1g)/(N1g*N2g)+1

!!             ! ... convert to global indices
!!             ii0 = ii0 + grid%is(1) - 1 - grid%nGhostRHS(dir,1)
!!             jj0 = jj0 + grid%is(2) - 1
!!             kk0 = kk0 + grid%is(3) - 1
!!             ii1 = ii1 + grid%is(1) - 1 - grid%nGhostRHS(dir,1)
!!             jj1 = jj1 + grid%is(2) - 1
!!             kk1 = kk1 + grid%is(3) - 1

!!             if (ii1 < grid%is(1) .OR. ii1 > grid%ie(1)) CYCLE

!!             ! ... convert l1 back to non-buffered counting
!!             l1 = (kk1-grid%is(3))*N1*N2 + (jj1-grid%is(2))*N1 + (ii1-grid%is(1)+1)

!!             ! ... intermediate values
!!             gam     = state%gv(l1,1)
!!             gamm1i  = 1.0_8 / (gam - 1.0_8)
!!             P       = state%dv(l1,1)
!!             T       = state%dv(l1,2)
!!             RHO_P   = gam * gamm1i / T
!!             RHO_T   = -P / T * RHO_P
!!             UX      = state%cv(l1,2) * state%dv(l1,3)
!!             UY      = state%cv(l1,3) * state%dv(l1,3)
!!             RHO     = state%cv(l1,1)
!!             RHOE_U  = UX * RHO
!!             RHOE_V  = UY * RHO
!!             KE      = 0.5_8 * (UX * UX + UY * UY)
!!             RHOE_P  = gamm1i + KE * RHO_P
!!             RHOE_T  = KE * RHO_T
!!             JAC     = grid%JAC(l1)
!!             XI_X    = grid%MT1(l1,region%global%t2Map(1,1)) * JAC
!!             XI_Y    = grid%MT1(l1,region%global%t2Map(1,2)) * JAC

!!             ! ... index
!!             index = (l1-1)*stencil_size + input%HYPREStencilIndex(order, ii0-ii1, jj0-jj1, kk0-kk1) + 1

!!             ! ... store in array
!!             values_r (index) = values_r (index) + local_val * ( RHO_P * UY * ( UX * XI_X + UY * XI_Y ) + XI_Y )
!!             values_ru(index) = values_ru(index) + local_val * ( RHO * UY * XI_X )
!!             values_rv(index) = values_rv(index) + local_val * ( RHO * ( UX * XI_X + 2.0_8 * UY * XI_Y ) )
!!             values_rE(index) = values_rE(index) + local_val * ( RHO_T * UY * ( UX * XI_X + UY * XI_Y ) )
         
!!             ! dF(l1)    = dF(l1) + local_val * F(l0)
!!           end do
!!         end do
!!       end do

!!     CASE (4)

!!       do k = 1, grid%vdsGhost(dir)
!!         do j = 1, grid%B(opID,dir)%n
!!           do i = grid%B(opID,dir)%row_ptr(k,j), grid%B(opID,dir)%row_ptr(k,j+1)-1

!!             ! ... l0 and l1 indices refer to BUFFERED data
!!             local_val = grid%B(opID,dir)%val(i,k)
!!             local_ind = grid%B(opID,dir)%col_ind(i,k)
!!             l0        = grid%vec_indGhost(k,local_ind,dir)
!!             l1        = grid%vec_indGhost(k,        j,dir)
    
!!             ! ... convert from linear to triplet indices
!!             ! ... rhs index (buffered data)
!!             ii0 = mod(l0-1,N1g)+1
!!             jj0 = mod((l0-ii0)/N1g,N2g)+1
!!             kk0 = (l0-ii0-(jj0-1)*N1g)/(N1g*N2g)+1

!!             ! ... lhs index (buffered data)
!!             ii1 = mod(l1-1,N1g)+1
!!             jj1 = mod((l1-ii1)/N1g,N2g)+1
!!             kk1 = (l1-ii1-(jj1-1)*N1g)/(N1g*N2g)+1

!!             ! ... convert to global indices
!!             ii0 = ii0 + grid%is(1) - 1 - grid%nGhostRHS(dir,1)
!!             jj0 = jj0 + grid%is(2) - 1
!!             kk0 = kk0 + grid%is(3) - 1
!!             ii1 = ii1 + grid%is(1) - 1 - grid%nGhostRHS(dir,1)
!!             jj1 = jj1 + grid%is(2) - 1
!!             kk1 = kk1 + grid%is(3) - 1

!!             if (ii1 < grid%is(1) .OR. ii1 > grid%ie(1)) CYCLE

!!             ! ... convert l1 back to non-buffered counting
!!             l1 = (kk1-grid%is(3))*N1*N2 + (jj1-grid%is(2))*N1 + (ii1-grid%is(1)+1)

!!             ! ... intermediate values
!!             gam     = state%gv(l1,1)
!!             gamm1i  = 1.0_8 / (gam - 1.0_8)
!!             P       = state%dv(l1,1)
!!             T       = state%dv(l1,2)
!!             RHO_P   = gam * gamm1i / T
!!             RHO_T   = -P / T * RHO_P
!!             UX      = state%cv(l1,2) * state%dv(l1,3)
!!             UY      = state%cv(l1,3) * state%dv(l1,3)
!!             RHO     = state%cv(l1,1)
!!             RHOE_U  = UX * RHO
!!             RHOE_V  = UY * RHO
!!             KE      = 0.5_8 * (UX * UX + UY * UY)
!!             RHOE_P  = gamm1i + KE * RHO_P
!!             RHOE_T  = KE * RHO_T
!!             RHOE    = state%cv(l1,4)
!!             JAC     = grid%JAC(l1)
!!             XI_X    = grid%MT1(l1,region%global%t2Map(1,1)) * JAC
!!             XI_Y    = grid%MT1(l1,region%global%t2Map(1,2)) * JAC

!!             ! ... index
!!             index = (l1-1)*stencil_size + input%HYPREStencilIndex(order, ii0-ii1, jj0-jj1, kk0-kk1) + 1
!!             ! print *, ii0-ii1, jj0-jj1, kk0-kk1

!!             ! ... store in array
!!             values_r (index) = values_r (index) + local_val * ( ( RHOE_P + 1.0_8 ) * ( UX * XI_X + UY * XI_Y ) )
!!             values_ru(index) = values_ru(index) + local_val * ( ( RHOE + P ) * XI_X + RHOE_U * ( UX * XI_X + UY * XI_Y ) )
!!             values_rv(index) = values_rv(index) + local_val * ( ( RHOE + P ) * XI_Y + RHOE_V * ( UX * XI_X + UY * XI_Y ) )
!!             values_rE(index) = values_rE(index) + local_val * ( RHOE_T * ( UX * XI_X + UY * XI_Y ) )
         
!!             ! dF(l1)    = dF(l1) + local_val * F(l0)
!!           end do
!!         end do
!!       end do

!!     END SELECT

!!     ! ... y-direction, first derivatives
!!     dir   = 2
!!     order = 1
!!     opID  = input%iFirstDerivImplicit

!!     ! ... local partition size
!!     N1 = grid%ie(1)-grid%is(1)+1
!!     N2 = grid%ie(2)-grid%is(2)+1
!!     N3 = grid%ie(3)-grid%is(3)+1

!!     ! ... partition size including ghost
!!     N1g = grid%ie(1)-grid%is(1)+1
!!     N2g = grid%ie(2)-grid%is(2)+1+SUM(grid%nGhostRHS(dir,:))
!!     N3g = grid%ie(3)-grid%is(3)+1

!!     SELECT CASE (lhs_var)
!!     CASE (1)

!!       ! ... B_p {delta_\eta} where
!!       !
!!       !       | u rho_p              rho                            0              u rho_T     |
!!       ! B_p = | rho_p u^2 + 1        2 rho u                        0              u^2 rho_T   |
!!       !       | rho_p u v            rho v                          rho u          u v rho_T   |
!!       !     = | [(rho E)_p + 1] u    [(rho E + p) + u (rho E)_u]    u (rho E)_v    u (rho E)_T |
!!       !
!!       !
!!       ! ... inviscid fluxes
!!       do k = 1, grid%vdsGhost(dir)
!!         do j = 1, grid%B(opID,dir)%n
!!           do i = grid%B(opID,dir)%row_ptr(k,j), grid%B(opID,dir)%row_ptr(k,j+1)-1

!!             ! ... l0 and l1 indices refer to BUFFERED data
!!             local_val = grid%B(opID,dir)%val(i,k)
!!             local_ind = grid%B(opID,dir)%col_ind(i,k)
!!             l0        = grid%vec_indGhost(k,local_ind,dir)
!!             l1        = grid%vec_indGhost(k,        j,dir)
    
!!             ! ... convert from linear to triplet indices
!!             ! ... rhs index (buffered data)
!!             ii0 = mod(l0-1,N1g)+1
!!             jj0 = mod((l0-ii0)/N1g,N2g)+1
!!             kk0 = (l0-ii0-(jj0-1)*N1g)/(N1g*N2g)+1

!!             ! ... lhs index (buffered data)
!!             ii1 = mod(l1-1,N1g)+1
!!             jj1 = mod((l1-ii1)/N1g,N2g)+1
!!             kk1 = (l1-ii1-(jj1-1)*N1g)/(N1g*N2g)+1

!!             ! ... convert to global indices
!!             ii0 = ii0 + grid%is(1) - 1
!!             jj0 = jj0 + grid%is(2) - 1 - grid%nGhostRHS(dir,1)
!!             kk0 = kk0 + grid%is(3) - 1
!!             ii1 = ii1 + grid%is(1) - 1
!!             jj1 = jj1 + grid%is(2) - 1 - grid%nGhostRHS(dir,1)
!!             kk1 = kk1 + grid%is(3) - 1

!!             if (jj1 < grid%is(2) .OR. jj1 > grid%ie(2)) CYCLE

!!             ! ... convert l1 back to non-buffered counting
!!             l1 = (kk1-grid%is(3))*N1*N2 + (jj1-grid%is(2))*N1 + (ii1-grid%is(1)+1)

!!             ! ... interiate values
!!             gam     = state%gv(l1,1)
!!             gamm1i  = 1.0_8 / (gam - 1.0_8)
!!             P       = state%dv(l1,1)
!!             T       = state%dv(l1,2)
!!             RHO_P   = gam * gamm1i / T
!!             RHO_T   = -P / T * RHO_P
!!             UX      = state%cv(l1,2) * state%dv(l1,3)
!!             UY      = state%cv(l1,3) * state%dv(l1,3)
!!             RHO     = state%cv(l1,1)
!!             RHOE_U  = UX
!!             RHOE_V  = UY
!!             KE      = 0.5_8 * (UX * UX + UY * UY)
!!             RHOE_P  = gamm1i + KE * RHO_P
!!             RHOE_T  = KE * RHO_T
!!             JAC     = grid%JAC(l1)
!!             XI_X    = grid%MT1(l1,region%global%t2Map(2,1)) * JAC
!!             XI_Y    = grid%MT1(l1,region%global%t2Map(2,2)) * JAC

!!             ! ... index
!!             index = (l1-1)*stencil_size + input%HYPREStencilIndex(order, ii0-ii1, jj0-jj1, kk0-kk1) + 1

!!             ! ... store in array
!!             values_r (index) = values_r (index) + local_val * ( RHO_P * (UX * XI_X + UY * XI_Y) )
!!             values_ru(index) = values_ru(index) + local_val * ( RHO * XI_X )
!!             values_rv(index) = values_rv(index) + local_val * ( RHO * XI_Y )
!!             values_rE(index) = values_rE(index) + local_val * ( RHO_T * (UX * XI_X + UY * XI_Y) )

!!             ! dF(l1)    = dF(l1) + local_val * F(l0)
!!           end do
!!         end do
!!       end do

!!     CASE (2)

!!       do k = 1, grid%vdsGhost(dir)
!!         do j = 1, grid%B(opID,dir)%n
!!           do i = grid%B(opID,dir)%row_ptr(k,j), grid%B(opID,dir)%row_ptr(k,j+1)-1

!!             ! ... l0 and l1 indices refer to BUFFERED data
!!             local_val = grid%B(opID,dir)%val(i,k)
!!             local_ind = grid%B(opID,dir)%col_ind(i,k)
!!             l0        = grid%vec_indGhost(k,local_ind,dir)
!!             l1        = grid%vec_indGhost(k,        j,dir)
    
!!             ! ... convert from linear to triplet indices
!!             ! ... rhs index (buffered data)
!!             ii0 = mod(l0-1,N1g)+1
!!             jj0 = mod((l0-ii0)/N1g,N2g)+1
!!             kk0 = (l0-ii0-(jj0-1)*N1g)/(N1g*N2g)+1

!!             ! ... lhs index (buffered data)
!!             ii1 = mod(l1-1,N1g)+1
!!             jj1 = mod((l1-ii1)/N1g,N2g)+1
!!             kk1 = (l1-ii1-(jj1-1)*N1g)/(N1g*N2g)+1

!!             ! ... convert to global indices
!!             ii0 = ii0 + grid%is(1) - 1
!!             jj0 = jj0 + grid%is(2) - 1 - grid%nGhostRHS(dir,1)
!!             kk0 = kk0 + grid%is(3) - 1
!!             ii1 = ii1 + grid%is(1) - 1
!!             jj1 = jj1 + grid%is(2) - 1 - grid%nGhostRHS(dir,1)
!!             kk1 = kk1 + grid%is(3) - 1

!!             if (jj1 < grid%is(2) .OR. jj1 > grid%ie(2)) CYCLE

!!             ! ... convert l1 back to non-buffered counting
!!             l1 = (kk1-grid%is(3))*N1*N2 + (jj1-grid%is(2))*N1 + (ii1-grid%is(1)+1)

!!             ! ... intermediate values
!!             gam     = state%gv(l1,1)
!!             gamm1i  = 1.0_8 / (gam - 1.0_8)
!!             P       = state%dv(l1,1)
!!             T       = state%dv(l1,2)
!!             RHO_P   = gam * gamm1i / T
!!             RHO_T   = -P / T * RHO_P
!!             UX      = state%cv(l1,2) * state%dv(l1,3)
!!             UY      = state%cv(l1,3) * state%dv(l1,3)
!!             RHO     = state%cv(l1,1)
!!             RHOE_U  = UX
!!             RHOE_V  = UY
!!             KE      = 0.5_8 * (UX * UX + UY * UY)
!!             RHOE_P  = gamm1i + KE * RHO_P
!!             RHOE_T  = KE * RHO_T
!!             JAC     = grid%JAC(l1)
!!             XI_X    = grid%MT1(l1,region%global%t2Map(2,1)) * JAC
!!             XI_Y    = grid%MT1(l1,region%global%t2Map(2,2)) * JAC

!!             ! ... index
!!             index = (l1-1)*stencil_size + input%HYPREStencilIndex(order, ii0-ii1, jj0-jj1, kk0-kk1) + 1

!!             ! ... store in array
!!             values_r (index) = values_r (index) + local_val * ( RHO_P * UX * ( UX * XI_X + UY * XI_Y ) + XI_X )
!!             values_ru(index) = values_ru(index) + local_val * ( RHO * ( 2.0_8 * UX * XI_X + UY * XI_Y ) )
!!             values_rv(index) = values_rv(index) + local_val * ( RHO * UX * XI_Y )
!!             values_rE(index) = values_rE(index) + local_val * ( RHO_T * UX * ( UX * XI_X + UY * XI_Y ) )

!!             ! dF(l1)    = dF(l1) + local_val * F(l0)
!!           end do
!!         end do
!!       end do

!!     CASE (3)

!!       do k = 1, grid%vdsGhost(dir)
!!         do j = 1, grid%B(opID,dir)%n
!!           do i = grid%B(opID,dir)%row_ptr(k,j), grid%B(opID,dir)%row_ptr(k,j+1)-1

!!             ! ... l0 and l1 indices refer to BUFFERED data
!!             local_val = grid%B(opID,dir)%val(i,k)
!!             local_ind = grid%B(opID,dir)%col_ind(i,k)
!!             l0        = grid%vec_indGhost(k,local_ind,dir)
!!             l1        = grid%vec_indGhost(k,        j,dir)
    
!!             ! ... convert from linear to triplet indices
!!             ! ... rhs index (buffered data)
!!             ii0 = mod(l0-1,N1g)+1
!!             jj0 = mod((l0-ii0)/N1g,N2g)+1
!!             kk0 = (l0-ii0-(jj0-1)*N1g)/(N1g*N2g)+1

!!             ! ... lhs index (buffered data)
!!             ii1 = mod(l1-1,N1g)+1
!!             jj1 = mod((l1-ii1)/N1g,N2g)+1
!!             kk1 = (l1-ii1-(jj1-1)*N1g)/(N1g*N2g)+1

!!             ! ... convert to global indices
!!             ii0 = ii0 + grid%is(1) - 1
!!             jj0 = jj0 + grid%is(2) - 1 - grid%nGhostRHS(dir,1)
!!             kk0 = kk0 + grid%is(3) - 1
!!             ii1 = ii1 + grid%is(1) - 1
!!             jj1 = jj1 + grid%is(2) - 1 - grid%nGhostRHS(dir,1)
!!             kk1 = kk1 + grid%is(3) - 1

!!             if (jj1 < grid%is(2) .OR. jj1 > grid%ie(2)) CYCLE

!!             ! ... convert l1 back to non-buffered counting
!!             l1 = (kk1-grid%is(3))*N1*N2 + (jj1-grid%is(2))*N1 + (ii1-grid%is(1)+1)

!!             ! ... intermediate values
!!             gam     = state%gv(l1,1)
!!             gamm1i  = 1.0_8 / (gam - 1.0_8)
!!             P       = state%dv(l1,1)
!!             T       = state%dv(l1,2)
!!             RHO_P   = gam * gamm1i / T
!!             RHO_T   = -P / T * RHO_P
!!             UX      = state%cv(l1,2) * state%dv(l1,3)
!!             UY      = state%cv(l1,3) * state%dv(l1,3)
!!             RHO     = state%cv(l1,1)
!!             RHOE_U  = UX * RHO
!!             RHOE_V  = UY * RHO
!!             KE      = 0.5_8 * (UX * UX + UY * UY)
!!             RHOE_P  = gamm1i + KE * RHO_P
!!             RHOE_T  = KE * RHO_T
!!             JAC     = grid%JAC(l1)
!!             XI_X    = grid%MT1(l1,region%global%t2Map(2,1)) * JAC
!!             XI_Y    = grid%MT1(l1,region%global%t2Map(2,2)) * JAC

!!             ! ... index
!!             index = (l1-1)*stencil_size + input%HYPREStencilIndex(order, ii0-ii1, jj0-jj1, kk0-kk1) + 1

!!             ! ... store in array
!!             values_r (index) = values_r (index) + local_val * ( RHO_P * UY * ( UX * XI_X + UY * XI_Y ) + XI_Y )
!!             values_ru(index) = values_ru(index) + local_val * ( RHO * UY * XI_X )
!!             values_rv(index) = values_rv(index) + local_val * ( RHO * ( UX * XI_X + 2.0_8 * UY * XI_Y ) )
!!             values_rE(index) = values_rE(index) + local_val * ( RHO_T * UY * ( UX * XI_X + UY * XI_Y ) )

!!             ! dF(l1)    = dF(l1) + local_val * F(l0)
!!           end do
!!         end do
!!       end do

!!     CASE (4)

!!       do k = 1, grid%vdsGhost(dir)
!!         do j = 1, grid%B(opID,dir)%n
!!           do i = grid%B(opID,dir)%row_ptr(k,j), grid%B(opID,dir)%row_ptr(k,j+1)-1

!!             ! ... l0 and l1 indices refer to BUFFERED data
!!             local_val = grid%B(opID,dir)%val(i,k)
!!             local_ind = grid%B(opID,dir)%col_ind(i,k)
!!             l0        = grid%vec_indGhost(k,local_ind,dir)
!!             l1        = grid%vec_indGhost(k,        j,dir)
    
!!             ! ... convert from linear to triplet indices
!!             ! ... rhs index (buffered data)
!!             ii0 = mod(l0-1,N1g)+1
!!             jj0 = mod((l0-ii0)/N1g,N2g)+1
!!             kk0 = (l0-ii0-(jj0-1)*N1g)/(N1g*N2g)+1

!!             ! ... lhs index (buffered data)
!!             ii1 = mod(l1-1,N1g)+1
!!             jj1 = mod((l1-ii1)/N1g,N2g)+1
!!             kk1 = (l1-ii1-(jj1-1)*N1g)/(N1g*N2g)+1

!!             ! ... convert to global indices
!!             ii0 = ii0 + grid%is(1) - 1
!!             jj0 = jj0 + grid%is(2) - 1 - grid%nGhostRHS(dir,1)
!!             kk0 = kk0 + grid%is(3) - 1
!!             ii1 = ii1 + grid%is(1) - 1
!!             jj1 = jj1 + grid%is(2) - 1 - grid%nGhostRHS(dir,1)
!!             kk1 = kk1 + grid%is(3) - 1

!!             if (jj1 < grid%is(2) .OR. jj1 > grid%ie(2)) CYCLE

!!             ! ... convert l1 back to non-buffered counting
!!             l1 = (kk1-grid%is(3))*N1*N2 + (jj1-grid%is(2))*N1 + (ii1-grid%is(1)+1)

!!             ! ... intermediate values
!!             gam     = state%gv(l1,1)
!!             gamm1i  = 1.0_8 / (gam - 1.0_8)
!!             P       = state%dv(l1,1)
!!             T       = state%dv(l1,2)
!!             RHO_P   = gam * gamm1i / T
!!             RHO_T   = -P / T * RHO_P
!!             UX      = state%cv(l1,2) * state%dv(l1,3)
!!             UY      = state%cv(l1,3) * state%dv(l1,3)
!!             RHOE_U  = UX * RHO
!!             RHOE_V  = UY * RHO
!!             KE      = 0.5_8 * (UX * UX + UY * UY)
!!             RHOE_P  = gamm1i + KE * RHO_P
!!             RHOE_T  = KE * RHO_T
!!             RHOE    = state%cv(l1,4)
!!             JAC     = grid%JAC(l1)
!!             XI_X    = grid%MT1(l1,region%global%t2Map(2,1)) * JAC
!!             XI_Y    = grid%MT1(l1,region%global%t2Map(2,2)) * JAC

!!             ! ... index
!!             index = (l1-1)*stencil_size + input%HYPREStencilIndex(order, ii0-ii1, jj0-jj1, kk0-kk1) + 1

!!             ! ... store in array
!!             values_r (index) = values_r (index) + local_val * ( ( RHOE_P + 1.0_8 ) * ( UX * XI_X + UY * XI_Y ) )
!!             values_ru(index) = values_ru(index) + local_val * ( ( RHOE + P ) * XI_X + RHOE_U * ( UX * XI_X + UY * XI_Y ) )
!!             values_rv(index) = values_rv(index) + local_val * ( ( RHOE + P ) * XI_Y + RHOE_V * ( UX * XI_X + UY * XI_Y ) )
!!             values_rE(index) = values_rE(index) + local_val * ( RHOE_T * ( UX * XI_X + UY * XI_Y ) )

!!             ! dF(l1)    = dF(l1) + local_val * F(l0)
!!           end do
!!         end do
!!       end do

!!     END SELECT

!!     ! ... multiply by dtau
!!     do i = 1, grid%nCells*stencil_size
!!       values_r (i) = dtau * values_r (i)
!!       values_ru(i) = dtau * values_ru(i)
!!       values_rv(i) = dtau * values_rv(i)
!!       values_rE(i) = dtau * values_rE(i)
!!     end do     

    ! ... no preconditioner
    PC(:)       = 0.0_8
    PC(lhs_var) = 1.0_8    

    ! ... precondintioner length
    LU = input%Lu

    ! ... fresstream Mach number
    MMIN2 = input%MachInf**2 * 3.0_8

    ! ... change time factor if we are on the first iteration
    time_fac = 1.5_dp
    if (global%main_ts_loop_index == 1) time_fac = 1.0_dp
    if (input%ImplicitSteadyState == TRUE) time_fac = 1.0_dp
    if (input%ImplicitTrueSteadyState == TRUE) time_fac = 0.0_dp

    ! ... only include preconditioner if steady-state
    ! ... the way to accomplish this should change in order to save on comp. time
    ! if (input%ImplicitSteadyState == TRUE) time_fac = 0.0_8
    ! ... the diagonal terms are the time dependent terms
    ! ... if steady-state, do not add the diagonal terms
    ! ... no case will be selected for lhs_var = 0
    ! if (input%ImplicitSteadyState == TRUE) then
    !    print*,'ImplicitSteadyState, lhs_var',input%ImplicitSteadyState, lhs_var
    !    lhs_var_copy = lhs_var
    !    lhs_var = 0
    !    print*,'lhs_var,lhs_var_copy',lhs_var,lhs_var_copy
    ! end if


    ! ... add the diagonal terms
    Select Case (lhs_var)
    
    Case (1)

      do i = 1, grid%nCells

        ! ... temporary values
        gam     = state%gv(i,1)
        gamm1i  = 1.0_8 / (gam - 1.0_8)
        P       = state%dv(i,1)
        T       = state%dv(i,2)
        RHO     = state%cv(i,1)
        RHO_P   = gam * gamm1i / T
        RHO_T   = -P / T * RHO_P
        UX      = state%cv(i,2) / RHO
        UY      = state%cv(i,3) / RHO

        ! ... preconditioner
        SNDSPD2 = GAM * P / RHO
        SNDSPD  = SQRT(SNDSPD2)
        MI2     = (UX*UX + UY*UY) / SNDSPD2
        MU2     = (LU / (3.14_8 * state%dt(i) * SNDSPD)) * (LU / (3.14_8 * state%dt(i) * SNDSPD))
        MP2     = MIN(MAX(MI2,MU2,MMIN2),1.0_8)
        EPS_P   = MP2 / (1.0_8 + (GAM - 1.0_8) * MP2)
        RHO_PP  = 1.0_8 / (EPS_P * SNDSPD2)
      PC(1)   = RHO_PP
      PC(2)   = 0.0_8
      PC(3)   = 0.0_8
      PC(4)   = RHO_T

!        print*,(MAX(MI2,MU2,MMIN2)), MI2, MU2, MMIN2
!        MP2 = MI2
!       if(MP2 .lt. 1.0_8) then
!          print*,'i,j,k,MP2',i,j,k,MP2
!       end if
        ! ... index
        index = (i-1)*stencil_size + 1

         values_r(index) =  values_r(index) + PC(1) + dtau * (time_fac / state%dt(i) + 1) * RHO_P
        values_ru(index) = values_ru(index) + PC(2)
        values_rv(index) = values_rv(index) + PC(3)
        values_rE(index) = values_rE(index) + PC(4) + dtau * (time_fac / state%dt(i) + 1) * RHO_T

      end do

    Case (2)

      do i = 1, grid%nCells

        ! ... temporary values
        gam     = state%gv(i,1)
        gamm1i  = 1.0_8 / (gam - 1.0_8)
        RHO     = state%cv(i,1)
        P       = state%dv(i,1)
        T       = state%dv(i,2)
        RHO_P   = gam * gamm1i / T
        RHO_T   = -P / T * RHO_P
        UX      = state%cv(i,2) / RHO
        UY      = state%cv(i,3) / RHO

        ! ... preconditioner
        SNDSPD2 = GAM * P / RHO
        SNDSPD  = SQRT(SNDSPD2)
        MI2     = (UX*UX + UY*UY) / SNDSPD2
        MU2     = (LU / (3.14_8 * state%dt(i) * SNDSPD)) * (LU / (3.14_8 * state%dt(i) * SNDSPD))
        MP2     = MIN(MAX(MI2,MU2,MMIN2),1.0_8)
        EPS_P   = MP2 / (1.0_8 + (GAM - 1.0_8) * MP2)
        RHO_PP  = 1.0_8 / (EPS_P * SNDSPD2)
      PC(1)   = UX * RHO_PP
      PC(2)   = RHO
      PC(3)   = 0.0_8
      PC(4)   = UX * RHO_T
  
        ! ... index
        index = (i-1)*stencil_size + 1

         values_r(index) =  values_r(index) + PC(1) + dtau * (time_fac / state%dt(i) + 1) * RHO_P * UX
        values_ru(index) = values_ru(index) + PC(2) + dtau * (time_fac / state%dt(i) + 1) * RHO   
        values_rv(index) = values_rv(index) + PC(3)
        values_rE(index) = values_rE(index) + PC(4) + dtau * (time_fac / state%dt(i) + 1) * RHO_T * UX

      end do

    Case (3)

      do i = 1, grid%nCells

        ! ... temporary values
        gam     = state%gv(i,1)
        gamm1i  = 1.0_8 / (gam - 1.0_8)
        RHO     = state%cv(i,1)
        P       = state%dv(i,1)
        T       = state%dv(i,2)
        RHO_P   = gam * gamm1i / T
        RHO_T   = -P / T * RHO_P
        UX      = state%cv(i,2) / RHO
        UY      = state%cv(i,3) / RHO

        ! ... preconditioner
        SNDSPD2 = GAM * P / RHO
        SNDSPD  = SQRT(SNDSPD2)
        MI2     = (UX*UX + UY*UY) / SNDSPD2
        MU2     = (LU / (3.14_8 * state%dt(i) * SNDSPD)) * (LU / (3.14_8 * state%dt(i) * SNDSPD))
        MP2     = MIN(MAX(MI2,MU2,MMIN2),1.0_8)
        EPS_P   = MP2 / (1.0_8 + (GAM - 1.0_8) * MP2)
        RHO_PP  = 1.0_8 / (EPS_P * SNDSPD2)
      PC(1)   = UY * RHO_PP
      PC(2)   = 0.0_8
      PC(3)   = RHO
      PC(4)   = UY * RHO_T

        ! ... index
        index = (i-1)*stencil_size + 1

         values_r(index) =  values_r(index) + PC(1) + dtau * (time_fac / state%dt(i) + 1) * RHO_P * UY
        values_ru(index) = values_ru(index) + PC(2)
        values_rv(index) = values_rv(index) + PC(3) + dtau * (time_fac / state%dt(i) + 1) * RHO
        values_rE(index) = values_rE(index) + PC(4) + dtau * (time_fac / state%dt(i) + 1) * RHO_T * UY

      end do

    Case (4)

      do i = 1, grid%nCells

        ! ... temporary values
        gam     = state%gv(i,1)
        gamm1i  = 1.0_8 / (gam - 1.0_8)
        RHO     = state%cv(i,1)
        P       = state%dv(i,1)
        T       = state%dv(i,2)
        RHO_P   = gam * gamm1i / T
        RHO_T   = -P / T * RHO_P
        UX      = state%cv(i,2) / RHO
        UY      = state%cv(i,3) / RHO
        KE      = 0.5 * (UX * UX + UY * UY)

        ! ... preconditioner
        SNDSPD2 = GAM * P / RHO
        SNDSPD  = SQRT(SNDSPD2)
        MI2     = (UX*UX + UY*UY) / SNDSPD2
        MU2     = (LU / (3.14_8 * state%dt(i) * SNDSPD)) * (LU / (3.14_8 * state%dt(i) * SNDSPD))
        MP2     = MIN(MAX(MI2,MU2,MMIN2),1.0_8)
        EPS_P   = MP2 / (1.0_8 + (GAM - 1.0_8) * MP2)
        RHO_PP  = 1.0_8 / (EPS_P * SNDSPD2)
        H_P     = 0.0_8
        H0      = T + 0.5_8 * (UX * UX + UY * UY)
        H_T     = 1.0_8
      PC(1)   = RHO * H_P + H0 * RHO_PP - 1.0_8
      PC(2)   = RHO * UX
      PC(3)   = RHO * UY
      PC(4)   = RHO * H_T + H0 * RHO_T
  
        ! ... index
        index = (i-1)*stencil_size + 1

         values_r(index) =  values_r(index) + PC(1) + dtau * (time_fac / state%dt(i) + 1) * (gamm1i + KE * RHO_P) 
        values_ru(index) = values_ru(index) + PC(2) + dtau * (time_fac / state%dt(i) + 1) * RHO * UX
        values_rv(index) = values_rv(index) + PC(3) + dtau * (time_fac / state%dt(i) + 1) * RHO * UY
        values_rE(index) = values_rE(index) + PC(4) + dtau * (time_fac / state%dt(i) + 1) * KE * RHO_T

      end do

    End Select

!!     ! if (input%ImplicitSteadyState == TRUE) then
!!     !    print*,'ImplicitSteadyState, lhs_var',input%ImplicitSteadyState, lhs_var
!!     !    lhs_var = lhs_var_copy
!!     !    print*,'lhs_var,lhs_var_copy',lhs_var,lhs_var_copy
!!     ! end if

!!     if (state%RE <= 0.0_8) Return

!!     ! ... x-direction, second derivatives
!!     dir   = 1
!!     order = 2
!!     opID  = input%iSecondDerivImplicit

!!     ! ... local partition size
!!     N1 = grid%ie(1)-grid%is(1)+1
!!     N2 = grid%ie(2)-grid%is(2)+1
!!     N3 = grid%ie(3)-grid%is(3)+1

!!     ! ... partition size including ghost
!!     N1g = grid%ie(1)-grid%is(1)+1+SUM(grid%nGhostRHS(dir,:)) ! (MIN(grid%nGhostRHS(dir,1),1)+MIN(grid%nGhostRHS(dir,2),1))*input%overlap(opID)
!!     N2g = grid%ie(2)-grid%is(2)+1
!!     N3g = grid%ie(3)-grid%is(3)+1

!!     SELECT CASE (lhs_var)
!!     CASE (2)

!!       ! ... viscous fluxes
!!       do k = 1, grid%vdsGhost(dir)
!!         do j = 1, grid%B(opID,dir)%n
!!           do i = grid%B(opID,dir)%row_ptr(k,j), grid%B(opID,dir)%row_ptr(k,j+1)-1

!!             ! ... l0 and l1 indices refer to BUFFERED data
!!             local_val = grid%B(opID,dir)%val(i,k)
!!             local_ind = grid%B(opID,dir)%col_ind(i,k)
!!             l0        = grid%vec_indGhost(k,local_ind,dir)
!!             l1        = grid%vec_indGhost(k,        j,dir)
    
!!             ! ... convert from linear to triplet indices
!!             ! ... rhs index (buffered data)
!!             ii0 = mod(l0-1,N1g)+1
!!             jj0 = mod((l0-ii0)/N1g,N2g)+1
!!             kk0 = (l0-ii0-(jj0-1)*N1g)/(N1g*N2g)+1

!!             ! ... lhs index (buffered data)
!!             ii1 = mod(l1-1,N1g)+1
!!             jj1 = mod((l1-ii1)/N1g,N2g)+1
!!             kk1 = (l1-ii1-(jj1-1)*N1g)/(N1g*N2g)+1

!!             ! ... convert to global indices
!!             ii0 = ii0 + grid%is(1) - 1 - grid%nGhostRHS(dir,1)
!!             jj0 = jj0 + grid%is(2) - 1
!!             kk0 = kk0 + grid%is(3) - 1
!!             ii1 = ii1 + grid%is(1) - 1 - grid%nGhostRHS(dir,1)
!!             jj1 = jj1 + grid%is(2) - 1
!!             kk1 = kk1 + grid%is(3) - 1

!!             if (ii1 < grid%is(1) .OR. ii1 > grid%ie(1)) CYCLE

!!             ! ... convert l1 back to non-buffered counting
!!             l1 = (kk1-grid%is(3))*N1*N2 + (jj1-grid%is(2))*N1 + (ii1-grid%is(1)+1)

!!             ! ... intermediate values
!!             XI_X    = grid%MT1(l1,region%global%t2Map(1,1)) * JAC
!!             XI_Y    = grid%MT1(l1,region%global%t2Map(1,2)) * JAC
!!             MU      = state%tv(l1,1) * state%REinv

!!             ! ... index
!!             index = (l1-1)*stencil_size + input%HYPREStencilIndex(order, ii0-ii1, jj0-jj1, kk0-kk1) + 1

!!             ! ... store in array
!!             values_ru(index) = values_ru(index) + local_val * MU * ( 4.0_8 / 3.0_8 * XI_X * XI_X + XI_Y * XI_Y )
!!             values_rv(index) = values_rv(index) + local_val * MU * (-2.0_8 / 3.0_8 * XI_X * XI_Y + XI_Y * XI_X )

!!           end do
!!         end do
!!       end do

!!     CASE (3)

!!       ! ... viscous fluxes
!!       do k = 1, grid%vdsGhost(dir)
!!         do j = 1, grid%B(opID,dir)%n
!!           do i = grid%B(opID,dir)%row_ptr(k,j), grid%B(opID,dir)%row_ptr(k,j+1)-1

!!             ! ... l0 and l1 indices refer to BUFFERED data
!!             local_val = grid%B(opID,dir)%val(i,k)
!!             local_ind = grid%B(opID,dir)%col_ind(i,k)
!!             l0        = grid%vec_indGhost(k,local_ind,dir)
!!             l1        = grid%vec_indGhost(k,        j,dir)
    
!!             ! ... convert from linear to triplet indices
!!             ! ... rhs index (buffered data)
!!             ii0 = mod(l0-1,N1g)+1
!!             jj0 = mod((l0-ii0)/N1g,N2g)+1
!!             kk0 = (l0-ii0-(jj0-1)*N1g)/(N1g*N2g)+1

!!             ! ... lhs index (buffered data)
!!             ii1 = mod(l1-1,N1g)+1
!!             jj1 = mod((l1-ii1)/N1g,N2g)+1
!!             kk1 = (l1-ii1-(jj1-1)*N1g)/(N1g*N2g)+1

!!             ! ... convert to global indices
!!             ii0 = ii0 + grid%is(1) - 1 - grid%nGhostRHS(dir,1)
!!             jj0 = jj0 + grid%is(2) - 1
!!             kk0 = kk0 + grid%is(3) - 1
!!             ii1 = ii1 + grid%is(1) - 1 - grid%nGhostRHS(dir,1)
!!             jj1 = jj1 + grid%is(2) - 1
!!             kk1 = kk1 + grid%is(3) - 1

!!             if (ii1 < grid%is(1) .OR. ii1 > grid%ie(1)) CYCLE

!!             ! ... convert l1 back to non-buffered counting
!!             l1 = (kk1-grid%is(3))*N1*N2 + (jj1-grid%is(2))*N1 + (ii1-grid%is(1)+1)

!!             ! ... intermediate values
!!             MU      = state%tv(l1,1) * state%REinv
!!             XI_X    = grid%MT1(l1,region%global%t2Map(1,1)) * JAC
!!             XI_Y    = grid%MT1(l1,region%global%t2Map(1,2)) * JAC

!!             ! ... index
!!             index = (l1-1)*stencil_size + input%HYPREStencilIndex(order, ii0-ii1, jj0-jj1, kk0-kk1) + 1

!!             ! ... store in array
!!             values_ru(index) = values_ru(index) + local_val * MU * (-2.0_8 / 3.0_8 * XI_X * XI_Y + XI_Y * XI_X )
!!             values_rv(index) = values_rv(index) + local_val * MU * ( 4.0_8 / 3.0_8 * XI_Y * XI_Y + XI_X * XI_X )
!! !            values_rv(index) = values_rv(index) + local_val * MU * ( 4.0_8 / 3.0_8 * XI_X * XI_X + XI_Y * XI_Y )

!!           end do
!!         end do
!!       end do

!!     CASE (4)

!!       ! ... viscous fluxes
!!       do k = 1, grid%vdsGhost(dir)
!!         do j = 1, grid%B(opID,dir)%n
!!           do i = grid%B(opID,dir)%row_ptr(k,j), grid%B(opID,dir)%row_ptr(k,j+1)-1

!!             ! ... l0 and l1 indices refer to BUFFERED data
!!             local_val = grid%B(opID,dir)%val(i,k)
!!             local_ind = grid%B(opID,dir)%col_ind(i,k)
!!             l0        = grid%vec_indGhost(k,local_ind,dir)
!!             l1        = grid%vec_indGhost(k,        j,dir)
    
!!             ! ... convert from linear to triplet indices
!!             ! ... rhs index (buffered data)
!!             ii0 = mod(l0-1,N1g)+1
!!             jj0 = mod((l0-ii0)/N1g,N2g)+1
!!             kk0 = (l0-ii0-(jj0-1)*N1g)/(N1g*N2g)+1

!!             ! ... lhs index (buffered data)
!!             ii1 = mod(l1-1,N1g)+1
!!             jj1 = mod((l1-ii1)/N1g,N2g)+1
!!             kk1 = (l1-ii1-(jj1-1)*N1g)/(N1g*N2g)+1

!!             ! ... convert to global indices
!!             ii0 = ii0 + grid%is(1) - 1 - grid%nGhostRHS(dir,1)
!!             jj0 = jj0 + grid%is(2) - 1
!!             kk0 = kk0 + grid%is(3) - 1
!!             ii1 = ii1 + grid%is(1) - 1 - grid%nGhostRHS(dir,1)
!!             jj1 = jj1 + grid%is(2) - 1
!!             kk1 = kk1 + grid%is(3) - 1

!!             if (ii1 < grid%is(1) .OR. ii1 > grid%ie(1)) CYCLE

!!             ! ... convert l1 back to non-buffered counting
!!             l1 = (kk1-grid%is(3))*N1*N2 + (jj1-grid%is(2))*N1 + (ii1-grid%is(1)+1)

!!             ! ... intermediate values
!!             UX      = state%cv(l1,2) * state%dv(l1,3)
!!             UY      = state%cv(l1,3) * state%dv(l1,3)
!!             JAC     = grid%JAC(l1)
!!             XI_X    = grid%MT1(l1,region%global%t2Map(1,1)) * JAC
!!             XI_Y    = grid%MT1(l1,region%global%t2Map(1,2)) * JAC
!!             MU      = state%tv(l1,1) * state%REinv
!!             KAPPA   = MU * state%PRinv

!!             ! ... index
!!             index = (l1-1)*stencil_size + input%HYPREStencilIndex(order, ii0-ii1, jj0-jj1, kk0-kk1) + 1

!!             ! ... store in array
!!             values_ru(index) = values_ru(index) + local_val * MU * ( UX * ( 4.0_8/3.0_8 * XI_X * XI_X + XI_Y * XI_Y ) + UY * ( -2.0_8/3.0_8 * XI_Y * XI_X + XI_X * XI_Y ) )
!!             values_rv(index) = values_rv(index) + local_val * MU * ( UX * (-2.0_8/3.0_8 * XI_X * XI_Y + XI_Y * XI_X ) + UY * (  4.0_8/3.0_8 * XI_Y * XI_Y + XI_X * XI_X ) )
!! !           values_rv(index) = values_rv(index) + local_val * MU * ( UX * (-2.0_8/3.0_8 * XI_X * XI_Y + XI_Y * XI_X ) + UY * (  4.0_8/3.0_8 * XI_X * XI_X + XI_Y * XI_Y ) )
!!             values_rE(index) = values_rE(index) + local_val * KAPPA * ( XI_X * XI_X + XI_Y * XI_Y )

!!           end do
!!         end do
!!       end do

!!     End Select

!!     ! ... y-direction, second derivatives
!!     dir   = 2
!!     order = 2
!!     opID  = input%iSecondDerivImplicit

!!     ! ... local partition size
!!     N1 = grid%ie(1)-grid%is(1)+1
!!     N2 = grid%ie(2)-grid%is(2)+1
!!     N3 = grid%ie(3)-grid%is(3)+1

!!     ! ... partition size including ghost
!!     N1g = grid%ie(1)-grid%is(1)+1
!!     N2g = grid%ie(2)-grid%is(2)+1+SUM(grid%nGhostRHS(dir,:)) ! (MIN(grid%nGhostRHS(dir,1),1)+MIN(grid%nGhostRHS(dir,2),1))*input%overlap(opID)
!!     N3g = grid%ie(3)-grid%is(3)+1

!!     SELECT CASE (lhs_var)
!!     CASE (2)

!!       ! ... viscous fluxes
!!       do k = 1, grid%vdsGhost(dir)
!!         do j = 1, grid%B(opID,dir)%n
!!           do i = grid%B(opID,dir)%row_ptr(k,j), grid%B(opID,dir)%row_ptr(k,j+1)-1

!!             ! ... l0 and l1 indices refer to BUFFERED data
!!             local_val = grid%B(opID,dir)%val(i,k)
!!             local_ind = grid%B(opID,dir)%col_ind(i,k)
!!             l0        = grid%vec_indGhost(k,local_ind,dir)
!!             l1        = grid%vec_indGhost(k,        j,dir)
    
!!             ! ... convert from linear to triplet indices
!!             ! ... rhs index (buffered data)
!!             ii0 = mod(l0-1,N1g)+1
!!             jj0 = mod((l0-ii0)/N1g,N2g)+1
!!             kk0 = (l0-ii0-(jj0-1)*N1g)/(N1g*N2g)+1

!!             ! ... lhs index (buffered data)
!!             ii1 = mod(l1-1,N1g)+1
!!             jj1 = mod((l1-ii1)/N1g,N2g)+1
!!             kk1 = (l1-ii1-(jj1-1)*N1g)/(N1g*N2g)+1

!!             ! ... convert to global indices
!!             ii0 = ii0 + grid%is(1) - 1
!!             jj0 = jj0 + grid%is(2) - 1 - grid%nGhostRHS(dir,1)
!!             kk0 = kk0 + grid%is(3) - 1
!!             ii1 = ii1 + grid%is(1) - 1
!!             jj1 = jj1 + grid%is(2) - 1 - grid%nGhostRHS(dir,1)
!!             kk1 = kk1 + grid%is(3) - 1

!!             if (jj1 < grid%is(2) .OR. jj1 > grid%ie(2)) CYCLE

!!             ! ... convert l1 back to non-buffered counting
!!             l1 = (kk1-grid%is(3))*N1*N2 + (jj1-grid%is(2))*N1 + (ii1-grid%is(1)+1)

!!             ! ... intermediate values
!!             JAC     = grid%JAC(l1)
!!             XI_X    = grid%MT1(l1,region%global%t2Map(2,1)) * JAC
!!             XI_Y    = grid%MT1(l1,region%global%t2Map(2,2)) * JAC
!!             MU      = state%tv(l1,1) * state%REinv

!!             ! ... index
!!             index = (l1-1)*stencil_size + input%HYPREStencilIndex(order, ii0-ii1, jj0-jj1, kk0-kk1) + 1

!!             ! ... store in array
!!             ! ... store in array
!!             values_ru(index) = values_ru(index) + local_val * MU * ( 4.0_8 / 3.0_8 * XI_X * XI_X + XI_Y * XI_Y )
!!             values_rv(index) = values_rv(index) + local_val * MU * (-2.0_8 / 3.0_8 * XI_X * XI_Y + XI_Y * XI_X )

!!           end do
!!         end do
!!       end do

!!     CASE (3)

!!       ! ... viscous fluxes
!!       do k = 1, grid%vdsGhost(dir)
!!         do j = 1, grid%B(opID,dir)%n
!!           do i = grid%B(opID,dir)%row_ptr(k,j), grid%B(opID,dir)%row_ptr(k,j+1)-1

!!             ! ... l0 and l1 indices refer to BUFFERED data
!!             local_val = grid%B(opID,dir)%val(i,k)
!!             local_ind = grid%B(opID,dir)%col_ind(i,k)
!!             l0        = grid%vec_indGhost(k,local_ind,dir)
!!             l1        = grid%vec_indGhost(k,        j,dir)
    
!!             ! ... convert from linear to triplet indices
!!             ! ... rhs index (buffered data)
!!             ii0 = mod(l0-1,N1g)+1
!!             jj0 = mod((l0-ii0)/N1g,N2g)+1
!!             kk0 = (l0-ii0-(jj0-1)*N1g)/(N1g*N2g)+1

!!             ! ... lhs index (buffered data)
!!             ii1 = mod(l1-1,N1g)+1
!!             jj1 = mod((l1-ii1)/N1g,N2g)+1
!!             kk1 = (l1-ii1-(jj1-1)*N1g)/(N1g*N2g)+1

!!             ! ... convert to global indices
!!             ii0 = ii0 + grid%is(1) - 1
!!             jj0 = jj0 + grid%is(2) - 1 - grid%nGhostRHS(dir,1)
!!             kk0 = kk0 + grid%is(3) - 1
!!             ii1 = ii1 + grid%is(1) - 1
!!             jj1 = jj1 + grid%is(2) - 1 - grid%nGhostRHS(dir,1)
!!             kk1 = kk1 + grid%is(3) - 1

!!             if (jj1 < grid%is(2) .OR. jj1 > grid%ie(2)) CYCLE

!!             ! ... convert l1 back to non-buffered counting
!!             l1 = (kk1-grid%is(3))*N1*N2 + (jj1-grid%is(2))*N1 + (ii1-grid%is(1)+1)

!!             ! ... intermediate values
!!             JAC     = grid%JAC(l1)
!!             XI_X    = grid%MT1(l1,region%global%t2Map(2,1)) * JAC
!!             XI_Y    = grid%MT1(l1,region%global%t2Map(2,2)) * JAC
!!             MU      = state%tv(l1,1) * state%REinv

!!             ! ... index
!!             index = (l1-1)*stencil_size + input%HYPREStencilIndex(order, ii0-ii1, jj0-jj1, kk0-kk1) + 1

!!             ! ... store in array
!!             values_ru(index) = values_ru(index) + local_val * MU * (-2.0_8 / 3.0_8 * XI_X * XI_Y + XI_Y * XI_X )
!!             values_rv(index) = values_rv(index) + local_val * MU * ( 4.0_8 / 3.0_8 * XI_Y * XI_Y + XI_X * XI_X )
!! !           values_rv(index) = values_rv(index) + local_val * MU * ( 4.0_8 / 3.0_8 * XI_X * XI_X + XI_Y * XI_Y )

!!           end do
!!         end do
!!       end do

!!    CASE (4)

!!       ! ... viscous fluxes
!!       do k = 1, grid%vdsGhost(dir)
!!         do j = 1, grid%B(opID,dir)%n
!!           do i = grid%B(opID,dir)%row_ptr(k,j), grid%B(opID,dir)%row_ptr(k,j+1)-1

!!             ! ... l0 and l1 indices refer to BUFFERED data
!!             local_val = grid%B(opID,dir)%val(i,k)
!!             local_ind = grid%B(opID,dir)%col_ind(i,k)
!!             l0        = grid%vec_indGhost(k,local_ind,dir)
!!             l1        = grid%vec_indGhost(k,        j,dir)
    
!!             ! ... convert from linear to triplet indices
!!             ! ... rhs index (buffered data)
!!             ii0 = mod(l0-1,N1g)+1
!!             jj0 = mod((l0-ii0)/N1g,N2g)+1
!!             kk0 = (l0-ii0-(jj0-1)*N1g)/(N1g*N2g)+1

!!             ! ... lhs index (buffered data)
!!             ii1 = mod(l1-1,N1g)+1
!!             jj1 = mod((l1-ii1)/N1g,N2g)+1
!!             kk1 = (l1-ii1-(jj1-1)*N1g)/(N1g*N2g)+1

!!             ! ... convert to global indices
!!             ii0 = ii0 + grid%is(1) - 1
!!             jj0 = jj0 + grid%is(2) - 1 - grid%nGhostRHS(dir,1)
!!             kk0 = kk0 + grid%is(3) - 1
!!             ii1 = ii1 + grid%is(1) - 1
!!             jj1 = jj1 + grid%is(2) - 1 - grid%nGhostRHS(dir,1)
!!             kk1 = kk1 + grid%is(3) - 1

!!             if (jj1 < grid%is(2) .OR. jj1 > grid%ie(2)) CYCLE

!!             ! ... convert l1 back to non-buffered counting
!!             l1 = (kk1-grid%is(3))*N1*N2 + (jj1-grid%is(2))*N1 + (ii1-grid%is(1)+1)

!!             ! ... intermediate values
!!             UX      = state%cv(l1,2) * state%dv(l1,3)
!!             UY      = state%cv(l1,3) * state%dv(l1,3)
!!             KE      = 0.5_8 * (UX * UX + UY * UY)
!!             JAC     = grid%JAC(l1)
!!             XI_X    = grid%MT1(l1,region%global%t2Map(2,1)) * JAC
!!             XI_Y    = grid%MT1(l1,region%global%t2Map(2,2)) * JAC
!!             MU      = state%tv(l1,1) * state%REinv
!!             KAPPA   = MU * state%PRinv

!!             ! ... index
!!             index = (l1-1)*stencil_size + input%HYPREStencilIndex(order, ii0-ii1, jj0-jj1, kk0-kk1) + 1

!!             ! ... store in array
!!             values_ru(index) = values_ru(index) + local_val * MU * ( UX * ( 4.0_8/3.0_8 * XI_X * XI_X + XI_Y * XI_Y ) + UY * ( -2.0_8/3.0_8 * XI_Y * XI_X + XI_X * XI_Y ) )
!!             values_rv(index) = values_rv(index) + local_val * MU * ( UX * (-2.0_8/3.0_8 * XI_X * XI_Y + XI_Y * XI_X ) + UY * (  4.0_8/3.0_8 * XI_Y * XI_Y + XI_X * XI_X ) )
!! !           values_rv(index) = values_rv(index) + local_val * MU * ( UX * (-2.0_8/3.0_8 * XI_X * XI_Y + XI_Y * XI_X ) + UY * (  4.0_8/3.0_8 * XI_X * XI_X + XI_Y * XI_Y ) )
!!             values_rE(index) = values_rE(index) + local_val * KAPPA * ( XI_X * XI_X + XI_Y * XI_Y )

!!           end do
!!         end do
!!       end do

!!    End Select

    Return

  End Subroutine NSImplicit2DAn


end MODULE ModNavierStokesImplicit

![/UNUSED]
