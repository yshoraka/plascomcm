#ifndef EF_FD_H
#define EF_FD_H

#include <petscsys.h>

#include "ef_stencil.h"

/**
 * file: ef_fd.h
 *
 * Class that is used to hold finite difference
 * coefficients on a grid with unit spacing
 */

typedef const char* ef_fd_type;

#define EF_FD_STANDARD_O2 "standard second order"

/**
 * Contains finite difference stencil
 * coefficients.
 */
typedef struct {
	ef_stencil **first; /**< first derivative finite difference stencil */
	ef_stencil **second;/**< second derivative finite difference stencil */
	ef_fd_type type;
} ef_fd;


PetscErrorCode ef_fd_create(ef_fd **fd, ef_fd_type type);


PetscErrorCode ef_fd_destroy(ef_fd *fd);


#endif
