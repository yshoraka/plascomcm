! Copyright (c) 2015, University of Illinois at Urbana-Champaign, XPACC
! License: MIT, http://opensource.org/licenses/MIT
!-----------------------------------------------------------------------
!
! ModElasticity.f90
!
! - Computation of stress and tangent stiffness for various constitutive models
!  
!
! Revision history
! - 22 Mar 2012 : CMO - initial code
!
!
!-----------------------------------------------------------------------
Module ModElasticity

CONTAINS

  Subroutine GetStress(region, ng, YngMod, PsnRat, F, Stress)

    USE ModGlobal
    USE ModDataStruct
    USE ModTMInt
    USE ModPETSc

    Implicit None

    Type(t_region), pointer :: region
    Real(rfreal) :: F(MAX_ND,MAX_ND), Stress(MAX_ND, MAX_ND), YngMod, PsnRat
    Integer :: ng

    ! ... local variables
    Type(t_mixt_input), pointer :: input
    Type(t_smixt), pointer :: TMstate
    Type(t_sgrid), pointer :: TMgrid
    Real(rfreal) :: mu, lambda, E(MAX_ND,MAX_ND), TrE, Eye(MAX_ND,MAX_ND)
    Integer :: i, j


     ! ... simplicity
     TMgrid  => region%TMgrid(ng)
!     TMstate => region%TMstate(ng)
     input   => TMgrid%input
     !print*,'stress, yng, pssn',YngMod,PsnRat
    if(input%ConstModel == SVK) then
       ! ... Saint-Venant Kirchhoff model
       Call SVKStress(region, ng, YngMod, PsnRat, F, Stress)
    elseif(input%ConstModel == NH) then
       ! ... modified neo-Hookean model (Doghri, p. 377)
       Call NHStress(region, ng, YngMod, PsnRat, F, Stress)
    end if
    

!     ! ... 1st Lame constant
!     lambda = input%YngMod*input%PsnRto/((1.0_rfreal+input%PsnRto)*(1.0_rfreal-2.0_rfreal*input%PsnRto))
!     ! ... 2nd Lame constant
!     mu     = input%YngMod/(2.0_rfreal*(1.0_rfreal+input%PsnRto))


!     ! ... Cauchy-Green strain tensor
!     Eye(:,:) = 0.0_rfreal
!     E = MATMUL(TRANSPOSE(F),F)
!     TrE = 0.0_rfreal
!     do j = 1, MAX_ND
!        Eye(j,j) = 1.0_rfreal
!     end do
!     do j = 1, MAX_ND
!        do i = 1, MAX_ND
!           E(i,j) = (E(i,j)-Eye(i,j))*0.5_rfreal
!        end do
!        ! ... Trace of E
!        TrE = TrE + E(j,j)
!     end do
    
    
!     ! if(input%ConstModel == SVK) then
!     ! ... Saint-Venant Kirchhoff model
!     Stress(:,:) = 0.0_rfreal
!     do j = 1, MAX_ND
!        Stress(j,j) = lambda*TrE
!        do i = 1, MAX_ND
!           Stress(i,j) = Stress(i,j) + 2.0_rfreal*mu*E(i,j)
!        end do
!     end do

!     ! ... multiply by F
!     Stress = MATMUL(F,Stress)

!    print*,'Stress, E, TrE',Stress, E, TrE


  end Subroutine GetStress

  Subroutine GetElasticityTens(region, ng, YngMod, PsnRat, F, A)

    USE ModGlobal
    USE ModDataStruct
    USE ModTMInt
    USE ModPETSc

    Implicit None

    Type(t_region), pointer :: region
    Real(rfreal) :: F(MAX_ND,MAX_ND), A(MAX_ND, MAX_ND, MAX_ND, MAX_ND), YngMod, PsnRat
    Integer :: ng

    ! ... local variables
    Type(t_mixt_input), pointer :: input
    Type(t_smixt), pointer :: TMstate
    Type(t_sgrid), pointer :: TMgrid
    Real(rfreal) :: mu, lambda, PreMult, TrE, Temp
    Real(rfreal) :: Eye(MAX_ND,MAX_ND),E(MAX_ND,MAX_ND), PK2(MAX_ND,MAX_ND)
    Integer :: i, j, k, p, m, n, l, ND
    
!     ! ... simplicity
    TMgrid  => region%TMgrid(ng)
!     TMstate => region%TMstate(ng)
     input   => TMgrid%input
!     ND      =  input%ND

!     ! ... 1st Lame constant
!     lambda = input%YngMod*input%PsnRto/((1.0_rfreal+input%PsnRto)*(1.0_rfreal-2.0_rfreal*input%PsnRto))
!     ! ... 2nd Lame constant
!     mu     = input%YngMod/(2.0_rfreal*(1.0_rfreal+input%PsnRto))

!     A(:,:,:,:) = 0.0_rfreal

!     ! ... Cauchy-Green strain tensor
!     E = MATMUL(TRANSPOSE(F),F)
!     TrE = 0.0_rfreal
!     ! ... identity matrix
!     Eye(:,:) = 0.0_rfreal
!     do j = 1, MAX_ND
!        Eye(j,j) = 1.0_rfreal
!     end do
!     do j = 1, MAX_ND
!        do i = 1, MAX_ND
!           E(i,j) = (E(i,j)-Eye(i,j))*0.5_rfreal
!           !if(E(i,j) < 1D-15) E(i,j) = 0.0_rfreal
!        end do
!        ! ... Trace of E
!        TrE = TrE + E(j,j)
!     end do


!     ! if(input%ConstModel == SVK) then
!     ! ... Saint-Venant Kirchhoff model
!     PK2(:,:) = 0.0_rfreal
!     do j = 1, MAX_ND
!        PK2(j,j) = lambda*TrE
!        do i = 1, MAX_ND
!           PK2(i,j) = PK2(i,j) + 2.0_rfreal*mu*E(i,j)
!        end do
!     end do
    
!    !print*,'PK2',PK2

!     do i = 1, MAX_ND
!        do j = 1, MAX_ND
          
!           !                PreMult = F(i,p)*F(j,m)
!           !                print*,PreMult
!           do k = 1, MAX_ND
!              do l = 1, MAX_ND
!                 temp = 0.0_rfreal
!                 do m = 1, MAX_ND
!                    do p = 1, MAX_ND
                      
! !                      A(k,i,l,j) = A(k,i,l,j) + F(i,p)*F(j,m)*(lambda*Eye(k,p)*Eye(m,l) + mu*(Eye(k,m)*Eye(p,l)+Eye(k,l)*Eye(p,m)))
!                       temp = temp + F(i,p)*F(j,m)*(lambda*Eye(k,p)*Eye(m,l) + mu*(Eye(k,m)*Eye(p,l)+Eye(k,l)*Eye(p,m)))
!                    end do
!                 end do
! !                 A(k,i,l,j) = A(k,i,l,j) + Eye(i,j)*PK2(k,l)
!                 A(k,i,l,j) = temp + Eye(i,j)*PK2(k,l)
!              end do
!           end do
!        end do
!     end do
     !print*,'elas, yng, pssn',YngMod,PsnRat
    if(input%ConstModel == SVK) then
       ! ... Saint-Venant Kirchhoff model
       call SVKElasticityTens(region, ng, YngMod, PsnRat, F, A)
    elseif(input%ConstModel == NH) then
       ! ... modified neo-Hookean model (Doghri, p. 377)
       call NHElasticityTens(region,ng, YngMod, PsnRat, F, A)
    end if

  end Subroutine GetElasticityTens

  Subroutine SVKStress(region, ng, YngMod, PsnRat, F, Stress)

    USE ModGlobal
    USE ModDataStruct
    USE ModTMInt
    USE ModPETSc

    Implicit None

    Type(t_region), pointer :: region
    Real(rfreal) :: F(MAX_ND,MAX_ND), Stress(MAX_ND, MAX_ND), YngMod, PsnRat
    Integer :: ng

    ! ... local variables
    Type(t_mixt_input), pointer :: input
    Type(t_smixt), pointer :: TMstate
    Type(t_sgrid), pointer :: TMgrid
    Real(rfreal) :: mu, lambda, E(MAX_ND,MAX_ND), TrE, Eye(MAX_ND,MAX_ND)
    Integer :: i, j

    ! ... simplicity
    TMgrid  => region%TMgrid(ng)
    TMstate => region%TMstate(ng)
    input   => TMgrid%input

    ! ... 1st Lame constant
    lambda = YngMod*PsnRat/((1.0_rfreal+PsnRat)*(1.0_rfreal-2.0_rfreal*PsnRat))
    !lambda = input%YngMod*input%PsnRto/((1.0_rfreal+input%PsnRto)*(1.0_rfreal-2.0_rfreal*input%PsnRto))
    ! ... 2nd Lame constant
    !mu     = input%YngMod/(2.0_rfreal*(1.0_rfreal+input%PsnRto))
    mu     = YngMod/(2.0_rfreal*(1.0_rfreal+PsnRat))


    ! ... Cauchy-Green strain tensor
    Eye(:,:) = 0.0_rfreal
    E = MATMUL(TRANSPOSE(F),F)
    TrE = 0.0_rfreal
    do j = 1, MAX_ND
       Eye(j,j) = 1.0_rfreal
    end do
    do j = 1, MAX_ND
       do i = 1, MAX_ND
          E(i,j) = (E(i,j)-Eye(i,j))*0.5_rfreal
       end do
       ! ... Trace of E
       TrE = TrE + E(j,j)
    end do
    
    
    ! if(input%ConstModel == SVK) then
    ! ... Saint-Venant Kirchhoff model
    Stress(:,:) = 0.0_rfreal
    do j = 1, MAX_ND
       Stress(j,j) = lambda*TrE
       do i = 1, MAX_ND
          Stress(i,j) = Stress(i,j) + 2.0_rfreal*mu*E(i,j)
       end do
    end do

    ! ... multiply by F
    Stress = MATMUL(F,Stress)

!    print*,'Stress, E, TrE',Stress, E, TrE


  end Subroutine SVKStress


  Subroutine NHStress(region, ng, YngMod, PsnRat, F, Stress)

    USE ModGlobal
    USE ModDataStruct
    USE ModTMInt
    USE ModPETSc

    Implicit None

    Type(t_region), pointer :: region
    Real(rfreal) :: F(MAX_ND,MAX_ND), Stress(MAX_ND, MAX_ND), YngMod, PsnRat
    Integer :: ng

    ! ... local variables
    Type(t_mixt_input), pointer :: input
    Type(t_smixt), pointer :: TMstate
    Type(t_sgrid), pointer :: TMgrid
    Real(rfreal), pointer :: Cinv(:,:)
    Real(rfreal) :: G, K, TrC, Eye(MAX_ND,MAX_ND), Jac
    Integer :: i, j

    ! ... simplicity
    TMgrid  => region%TMgrid(ng)
    TMstate => region%TMstate(ng)
    input   => TMgrid%input

    ! ... G, shear modulus
    !G = 0.5_rfreal*input%YngMod/(1.0_rfreal+input%PsnRto)
    G = 0.5_rfreal*YngMod/(1.0_rfreal+PsnRat)
    
    ! ... K, bulk modulus
    !K = 1.0_rfreal/3.0_rfreal*input%YngMod/(1.0_rfreal-2.0_rfreal*input%PsnRto)
    K = 1.0_rfreal/3.0_rfreal*YngMod/(1.0_rfreal-2.0_rfreal*PsnRat)

    Allocate(Cinv(MAX_ND,MAX_ND))

    ! ... right Cauchy-Green strain tensor
    Eye(:,:) = 0.0_rfreal
    Cinv = MATMUL(TRANSPOSE(F),F)
    TrC = 0.0_rfreal
    do j = 1, MAX_ND
       ! ... identity tensor
       Eye(j,j) = 1.0_rfreal
       ! ... Trace of C
       TrC = TrC + Cinv(j,j)
    end do

    ! ... get C^-1 and Jac = det(F) = sqrt(det(C))
    call InvertMatrix(Cinv,Jac)
    Jac = sqrt(Jac)

    ! ... premultiply
    G = G/(Jac*Jac)**(1.0_rfreal/3.0_rfreal)
    K = K*(Jac*Jac-Jac)
    TrC = TrC/3.0_rfreal

    ! ... 2nd Piola-Kirchhoff stress tensor
    Stress(:,:) = 0.0_rfreal
    do j = 1, MAX_ND
       do i = 1, MAX_ND
          Stress(i,j) = Stress(i,j) + G*(Eye(i,j)-TrC*Cinv(i,j)) + K*Cinv(i,j)
       end do
    end do

    ! ... multiply by F to get 1st Piola-Kirchhoff stress tensor
    Stress = MATMUL(F,Stress)

!    print*,'Stress, E, TrE',Stress, E, TrE

    deallocate(Cinv)

  end Subroutine NHStress

  Subroutine NHElasticityTens(region, ng, YngMod, PsnRat, F, A)

    USE ModGlobal
    USE ModDataStruct
    USE ModTMInt
    USE ModPETSc

    Implicit None

    Type(t_region), pointer :: region
    Real(rfreal) :: F(MAX_ND,MAX_ND), A(MAX_ND, MAX_ND, MAX_ND, MAX_ND), YngMod, PsnRat
    Integer :: ng

    ! ... local variables
    Type(t_mixt_input), pointer :: input
    Type(t_smixt), pointer :: TMstate
    Type(t_sgrid), pointer :: TMgrid
    Real(rfreal), pointer :: Cinv(:,:)
    Real(rfreal) :: mu, lambda, PreMult, TrE, Temp
    Real(rfreal) :: G, K, TrC, Eye(MAX_ND,MAX_ND), Jac, Stress(MAX_ND,MAX_ND), Lam1, Lam2, Lam8
    Integer :: i, j, p, m, n, l, ND
    
    ! ... simplicity
    TMgrid  => region%TMgrid(ng)
    TMstate => region%TMstate(ng)
    input   => TMgrid%input
    ND      =  input%ND

    A(:,:,:,:) = 0.0_rfreal

    ! ... G, shear modulus
    !G = 0.5_rfreal*input%YngMod/(1.0_rfreal+input%PsnRto)
    G = 0.5_rfreal*YngMod/(1.0_rfreal+PsnRat)
    
    ! ... K, bulk modulus
    !K = 1.0_rfreal/3.0_rfreal*input%YngMod/(1.0_rfreal-2.0_rfreal*input%PsnRto)
    K = 1.0_rfreal/3.0_rfreal*YngMod/(1.0_rfreal-2.0_rfreal*PsnRat)

    Allocate(Cinv(MAX_ND,MAX_ND))

    ! ... right Cauchy-Green strain tensor
    Eye(:,:) = 0.0_rfreal
    Cinv = MATMUL(TRANSPOSE(F),F)
    TrC = 0.0_rfreal
    do j = 1, MAX_ND
       ! ... identity tensor
       Eye(j,j) = 1.0_rfreal
       ! ... Trace of C
       TrC = TrC + Cinv(j,j)
    end do

    ! ... get C^-1 and Jac = det(F) = sqrt(det(C))
    call InvertMatrix(Cinv,Jac)
    Jac = sqrt(Jac)

!     print*,'Cinv,Jac,TrC',Jac, TrC
!     do i = 1, MAX_ND
!        print*,(Cinv(i,j),j=1,MAX_ND)
!     end do
    

    ! ... coeficients
    Lam1 = K*(2.0_rfreal*Jac*Jac-Jac) + 2.0_rfreal/9.0_rfreal*G*TrC*1.0_rfreal/(Jac*Jac)**(1.0_rfreal/3.0_rfreal)
    Lam2 = -2.0_rfreal/3.0_rfreal*G/(Jac*Jac)**(1.0_rfreal/3.0_rfreal)
    Lam8 = 2.0_rfreal/3.0_rfreal*G*1.0_rfreal/(Jac*Jac)**(1.0_rfreal/3.0_rfreal)*TrC - 2.0_rfreal*K*(Jac*Jac-Jac)

    ! ... premultiply
    G = G/(Jac*Jac)**(1.0_rfreal/3.0_rfreal)
    K = K*(Jac*Jac-Jac)
    TrC = TrC/3.0_rfreal

    ! ... 2nd Piola-Kirchhoff stress tensor
    Stress(:,:) = 0.0_rfreal
    do j = 1, MAX_ND
       do i = 1, MAX_ND
          Stress(i,j) = Stress(i,j) + G*(Eye(i,j)-TrC*Cinv(i,j)) + K*Cinv(i,j)
       end do
    end do

    ! ... premultiply
    !Lam1 = 4.0_rfreal*Lam1
    !Lam2 = 4.0_rfreal*Lam2
    Lam8 = 0.5_rfreal*Lam8

!    print*,'Lamvec',Lam1,Lam2,Lam8

    do i = 1, MAX_ND
       do j = 1, MAX_ND
          do l = 1, MAX_ND
             do m = 1, MAX_ND
                do n = 1, MAX_ND
                   do p = 1, MAX_ND
!                      A(i,j,l,m) = A(i,j,l,m) + F(i,p)*F(l,n)*(Lam1*Cinv(j,p)*Cinv(n,m) + Lam2*(Eye(j,p)*Cinv(n,m) + Cinv(j,p)*Eye(n,m)) + &
 !                          Lam8*(Cinv(j,n)*Cinv(p,m) + Cinv(j,m)*Cinv(p,n)))
                      A(i,j,l,m) = A(i,j,l,m) + F(j,p)*F(m,n)*(Lam1*Cinv(i,p)*Cinv(l,n) + Lam2*(Eye(i,p)*Cinv(l,n) + Cinv(i,p)*Eye(l,n)) + &
                           Lam8*(Cinv(i,l)*Cinv(p,n) + Cinv(i,n)*Cinv(p,l)))
                   end do
                end do
                A(i,j,l,m) = A(i,j,l,m) + Eye(j,m)*Stress(i,l)
             end do
          end do
       end do
    end do
!     print*,'A'
!                 do i = 1,MAX_ND
!                    do j = 1,MAX_ND
!                       write(*,'(I,x,I,x)') j,i
!                       do l = 1, MAX_ND
!                          write(*,'(3(D13.4,x))') (A(l,m,j,i),m = 1,MAX_ND)
!                       end do
!                    end do
!                end do


    deallocate(Cinv)
    
  end Subroutine NHElasticityTens

  Subroutine SVKElasticityTens(region, ng, YngMod, PsnRat, F, A)

    USE ModGlobal
    USE ModDataStruct
    USE ModTMInt
    USE ModPETSc

    Implicit None

    Type(t_region), pointer :: region
    Real(rfreal) :: F(MAX_ND,MAX_ND), A(MAX_ND, MAX_ND, MAX_ND, MAX_ND), YngMod, PsnRat
    Integer :: ng

    ! ... local variables
    Type(t_mixt_input), pointer :: input
    Type(t_smixt), pointer :: TMstate
    Type(t_sgrid), pointer :: TMgrid
    Real(rfreal) :: mu, lambda, PreMult, TrE, Temp
    Real(rfreal) :: Eye(MAX_ND,MAX_ND),E(MAX_ND,MAX_ND), PK2(MAX_ND,MAX_ND)
    Integer :: i, j, k, p, m, n, l, ND
    
    ! ... simplicity
    TMgrid  => region%TMgrid(ng)
    TMstate => region%TMstate(ng)
    input   => TMgrid%input
    ND      =  input%ND

    ! ... 1st Lame constant
    !lambda = input%YngMod*input%PsnRto/((1.0_rfreal+input%PsnRto)*(1.0_rfreal-2.0_rfreal*input%PsnRto))
    lambda = YngMod*PsnRat/((1.0_rfreal+PsnRat)*(1.0_rfreal-2.0_rfreal*PsnRat))
    ! ... 2nd Lame constant
    !mu     = input%YngMod/(2.0_rfreal*(1.0_rfreal+input%PsnRto))
    mu     = YngMod/(2.0_rfreal*(1.0_rfreal+PsnRat))

    A(:,:,:,:) = 0.0_rfreal

    ! ... Cauchy-Green strain tensor
    E = MATMUL(TRANSPOSE(F),F)
    TrE = 0.0_rfreal
    ! ... identity matrix
    Eye(:,:) = 0.0_rfreal
    do j = 1, MAX_ND
       Eye(j,j) = 1.0_rfreal
    end do
    do j = 1, MAX_ND
       do i = 1, MAX_ND
          E(i,j) = (E(i,j)-Eye(i,j))*0.5_rfreal
          !if(E(i,j) < 1D-15) E(i,j) = 0.0_rfreal
       end do
       ! ... Trace of E
       TrE = TrE + E(j,j)
    end do


    ! if(input%ConstModel == SVK) then
    ! ... Saint-Venant Kirchhoff model
    PK2(:,:) = 0.0_rfreal
    do j = 1, MAX_ND
       PK2(j,j) = lambda*TrE
       do i = 1, MAX_ND
          PK2(i,j) = PK2(i,j) + 2.0_rfreal*mu*E(i,j)
       end do
    end do
    
   !print*,'PK2',PK2

    do i = 1, MAX_ND
       do j = 1, MAX_ND
          
          !                PreMult = F(i,p)*F(j,m)
          !                print*,PreMult
          do k = 1, MAX_ND
             do l = 1, MAX_ND
                temp = 0.0_rfreal
                do m = 1, MAX_ND
                   do p = 1, MAX_ND
                      
!                      A(k,i,l,j) = A(k,i,l,j) + F(i,p)*F(j,m)*(lambda*Eye(k,p)*Eye(m,l) + mu*(Eye(k,m)*Eye(p,l)+Eye(k,l)*Eye(p,m)))
                      temp = temp + F(i,p)*F(j,m)*(lambda*Eye(k,p)*Eye(m,l) + mu*(Eye(k,m)*Eye(p,l)+Eye(k,l)*Eye(p,m)))
                   end do
                end do
!                 A(k,i,l,j) = A(k,i,l,j) + Eye(i,j)*PK2(k,l)
                A(k,i,l,j) = temp + Eye(i,j)*PK2(k,l)
             end do
          end do
       end do
    end do
    
  end Subroutine SVKElasticityTens

  Subroutine InvertMatrix(A, DetOut)
    
    USE ModGlobal

    implicit none

    Real(rfreal), pointer :: A(:,:)
    Real(rfreal), allocatable :: B(:,:)
    Real(rfreal), optional :: DetOut
    Real(rfreal) :: Det, InvDet
    Integer :: ND, i, j
    
    ND = size(A,1)
    allocate(B(ND,ND))
    
    if(ND == 3) then
       
       Det = A(1,1)*(A(2,2)*A(3,3)-A(3,2)*A(2,3)) &
            -A(1,2)*(A(2,1)*A(3,3)-A(3,1)*A(2,3)) &
            +A(1,3)*(A(2,1)*A(3,2)-A(3,1)*A(2,2))
       
       InvDet = 1.0_rfreal/Det
       
       ! ... now the inverse
       B(1,1) = (A(2,2)*A(3,3)-A(3,2)*A(2,3))*InvDet
       B(1,2) = (A(1,3)*A(3,2)-A(3,3)*A(1,2))*InvDet
       B(1,3) = (A(1,2)*A(2,3)-A(2,2)*A(1,3))*InvDet
       B(2,1) = (A(2,3)*A(3,1)-A(3,3)*A(2,1))*InvDet
       B(2,2) = (A(1,1)*A(3,3)-A(3,1)*A(1,3))*InvDet
       B(2,3) = (A(1,3)*A(2,1)-A(2,3)*A(1,1))*InvDet
       B(3,1) = (A(2,1)*A(3,2)-A(3,1)*A(2,2))*InvDet
       B(3,2) = (A(1,2)*A(3,1)-A(3,2)*A(1,1))*InvDet
       B(3,3) = (A(1,1)*A(2,2)-A(2,1)*A(1,2))*InvDet
       
       ! ... overwrite A
       A(:,:) = B(:,:)
       
       
    elseif(ND == 2) then
       
       Det = A(1,1)*A(2,2)-A(2,1)*A(1,2)
       
       InvDet = 1.0_rfreal/Det
       
       ! ... now the inverse
       B(1,1) =   A(2,2)*InvDet
       B(1,2) = - A(1,2)*InvDet
       B(2,1) = - A(2,1)*InvDet
       B(2,2) =   A(1,1)*InvDet

       ! ... overwrite A
       A(:,:) = B(:,:)
       
    end if

    if(PRESENT(DetOut)) DetOut = Det
   
  End Subroutine InvertMatrix

end Module ModElasticity

    

    
