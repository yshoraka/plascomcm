! Copyright (c) 2015, University of Illinois at Urbana-Champaign, XPACC
! License: MIT, http://opensource.org/licenses/MIT
!-----------------------------------------------------------------------
!
! ModTMEOMBC.f90
!
! - code to apply Neumann and coupled boundary conditions
!
! Revision history
! - 13 Mar 2012 : CMO - initial code
!
!
!-----------------------------------------------------------------------
Module ModTMEOMBC

CONTAINS

  Subroutine GetExternalHeatLoad(region, ng)

    USE ModGlobal
    USE ModDataStruct
    USE ModTMEOM
    USE ModMetrics
    USE ModPETSc

    Implicit None

    Type(t_region), pointer :: region
    Integer :: ng

    ! ... local variables
    Type(t_mixt_input), pointer :: input
    Type(t_smixt), pointer :: TMstate
    Type(t_sgrid), pointer :: TMgrid
    Type(t_spatch), pointer :: TMpatch
    Integer :: Npe(MAX_ND), Ne(MAX_ND),Npee(MAX_ND), Nee(MAX_ND),Npes(MAX_ND), Nes(MAX_ND)
    Integer, pointer :: LM(:,:), LM2D(:,:), TanDir(:)
    Integer :: ei, ej, ek, le, l0, dir, lp, i, j, k, p, dir2, lg, lpe
    Integer :: NdsPerElem, ND, nGauss, NdsPerFace, npatch, normDir, sgn
    Real(rfreal), allocatable :: X(:,:), Xc(:,:), qc(:), Q(:), SNrm(:), TanVec(:,:)
    Real(rfreal), pointer :: dVolShpFcn(:,:,:),BndShpFcn(:,:), Weight(:)
    Real(rfreal), pointer :: F(:,:), Cinv(:,:), Jac(:,:), BndJac(:,:), r(:)
    Real(rfreal) :: dJac, SurfJac, R2CFac, JF, dsgn, IntegralTest

    ! ... simplicity
    TMgrid  => region%TMgrid(ng)
    TMstate => region%TMstate(ng)
    input   => TMgrid%input
    LM      => TMgrid%LM
    ND      =  TMgrid%ND

    ! ... how many nodes and Gauss points
    if(input%ElemType == QUADRATIC) then
       if(ND == 2) then
          NdsPerElem = 8
          NdsPerFace = 3
          nGauss = 3
       elseif(ND > 2) then
          NdsPerElem = 20
          NdsPerFace = 8
          nGauss = 9
       end if
    elseif(input%ElemType == LINEAR) then
       if(ND == 2) then
          NdsPerElem = 4
          NdsPerFace = 2
          nGauss = 2
       elseif(ND > 2) then
          NdsPerElem = 8
          NdsPerFace = 4
          nGauss = 4
       end if
    end if
    
    ! ... allocate element local arrays
    ! ... current configuration, element thermal load array
    allocate(Xc(NdsPerElem,ND), r(NdsPerFace))
    ! ... reference configuration
    allocate(X(NdsPerElem,ND))

    ! ... Jacobian matrix
    allocate(Jac(ND,ND))

    ! ... deformation gradient, inverse of right Cauchy strain tensor
    allocate(F(ND,ND),Cinv(ND,ND))
    
    ! ... surface normal
    allocate(SNrm(ND))

    ! ... tangent vectors
    allocate(TanVec(ND,ND-1))

    ! ... heat flux in the current and regerence configurations
    allocate(qc(NdsPerFace), Q(NdsPerFace))

    do npatch = 1, region%nTMPatches

       TMpatch => region%TMpatch(npatch)
       TMgrid  => region%TMgrid(TMpatch%gridID)
       TMstate => region%TMstate(TMpatch%gridID)
       input   => TMgrid%input
       TanDir  => TMpatch%TanDir
       normDir = abs(TMpatch%normDir)
       sgn = normDir / TMpatch%normDir
       dsgn = dble(sgn)
       LM2D => TMpatch%LM2D

       if (TMpatch%gridID /= ng) CYCLE ! ... apply BC only in this grid
       if (TMpatch%BCType /= THERMAL_HEAT_FLUX .and. TMpatch%BCType /= STRUCTURAL_INTERACTING) CYCLE ! ... only heat flux boundary conditions

       BndShpFcn  => TMpatch%BndShpFcn
       dVolShpFcn => TMpatch%dVolShpFcn
       Weight     => TMpatch%Weight

       ! ... how many elements in this partition
       Ne(:) = 1
       if(input%ElemType == QUADRATIC) then
          do dir = 1, ND
             Ne(dir) = (TMgrid%ie(dir)-TMgrid%is(dir))/2
          end do
       elseif(input%ElemType == LINEAR) then
          do dir = 1, ND
             Ne(dir) = (TMgrid%ie(dir)-TMgrid%is(dir))
          end do
       end if

       ! ... how many elements are on this patch
       Npe(:) = 1
       if(input%ElemType == QUADRATIC) then
          do dir = 1, ND
             Npe(dir) = (TMpatch%ie(dir)-TMpatch%is(dir))/2
          end do
       elseif(input%ElemType == LINEAR) then
          do dir = 1, ND
             Npe(dir) = (TMpatch%ie(dir)-TMpatch%is(dir))
          end do
       end if

       ! ... starting and ending elements in this process' partition of the grid
       Nes(:) = 1; Nee(:) = 1
       if(input%ElemType == QUADRATIC) then
          do dir = 1, ND
             Nes(dir) = (TMgrid%is(dir)-1)/2+1
             Nee(dir) = (TMgrid%ie(dir)-1)/2
          end do
       elseif(input%ElemType == LINEAR) then
          do dir = 1, ND
             Nes(dir) = TMgrid%is(dir)
             Nee(dir) = TMgrid%ie(dir)-1
          end do
       end if
       
       ! ... starting and ending elements in the patch
       Npes(:) = 1; Npee(:) = 1
       do dir = 1, ND
          if(Npe(dir) == 0) then
             ! ... keep counting correct if patch is on face with normDir == dir
             if(sgn<0) then
                Npes(dir) = Nee(dir)
                Npee(dir) = Nee(dir)
             else
                Npes(dir) = Nes(dir)
                Npee(dir) = Nes(dir)
             end if
             ! ... to keep product(Npe) >= 1
             Npe(dir) = 1
          else
             if(input%ElemType == QUADRATIC) then
                Npes(dir) = (TMpatch%is(dir)-1)/2+1
                Npee(dir) = (TMpatch%ie(dir)-1)/2
             elseif(input%ElemType == LINEAR) then
                Npes(dir) = TMpatch%is(dir)
                Npee(dir) = TMpatch%ie(dir)-1                
             end if
          end if
       end do

       IntegralTest = 0.0_rfreal

       do ek = Npes(3), Npee(3)
          do ej = Npes(2), Npee(2)
             do ei = Npes(1), Npee(1)
                
                le  = (ek-Nes(3) )*Ne(2) *Ne(1)  + (ej-Nes(2) )*Ne(1)  + (ei-Nes(1))  + 1
                lpe = (ek-Npes(3))*Npe(2)*Npe(1) + (ej-Npes(2))*Npe(1) + (ei-Npes(1)) + 1


                ! ... fill the local array
                if(input%TMSolve == 3) then
                   do dir = 1, ND
                      do lp = 1, NdsPerElem
                         X(lp,dir) = TMgrid%X(LM(lp,le),dir)
                         Xc(lp,dir) = TMstate%q(LM(lp,le),dir)
                      end do
                   end do
                else
                   do dir = 1, ND
                      do lp = 1, NdsPerElem
                         ! ... current configuration = reference configuration
                         Xc(lp,dir) = TMgrid%X(LM(lp,le),dir)
                         X(lp,dir) = TMgrid%X(LM(lp,le),dir)
                      end do
                   end do
                end if

                ! ... get the normal heat flux at each node
                if (TMpatch%BCType == THERMAL_HEAT_FLUX) then
                   do lp = 1, NdsPerFace
                      ! ... currently, only uniform heat flux over patch
                      qc(lp) = TMpatch%BCVal(1)
                   end do
                elseif (TMpatch%BCType == STRUCTURAL_INTERACTING) then
                   do lp = 1, NdsPerFace
                      ! ... get heat flux from fluid solutin
                      qc(lp) = TMpatch%FluidHtFlux(TMpatch%LMP3D2D(lp,lpe))

                   end do
                end if

                r(:) = 0.0_rfreal
                ! ... loop through the Gauss points
                do lg = 1, nGauss

                   ! ... compute the Jacobian
                   Jac(:,:) = 0.0_rfreal
                   do j = 1, ND
                      do i = 1, ND
                         do lp = 1, NdsPerElem
                            Jac(i,j) = Jac(i,j) + dVolShpFcn(lp,j,lg)*X(lp,i)
                         end do
                      end do
                   end do

                   ! ... get surface normal and Jacobian before we invert
                   do dir = 1, ND
                      do j = 1, ND-1
                         TanVec(dir,j) = Jac(dir,TanDir(j))
                      end do
                   end do

                   if(ND == 3) then
                      
                      call cross_product(TanVec(:,1),TanVec(:,2),SNrm)

                   elseif(ND == 2) then

                     SNrm(1) = -TanVec(2,1)
                     SNrm(2) =  TanVec(1,1)

                   end if

                   ! ... now compute the Jacobian
                   SurfJac = DOT_PRODUCT(SNrm,SNrm)
                   SurfJac = sqrt(SurfJac)

                   ! ... normalize the surface normal vector
                   SNrm = SNrm/SurfJac

                   ! ... assure surface norm is outward facing
                   do dir = 1, ND
                      SNrm(dir) = -dsgn*abs(SNrm(dir))
                   end do
                  
                   ! ... compute the determinant and inverse
                   call InvertMatrix(Jac,dJac)

                   ! ... check for singular matrix
                   if(dJac .lt. tiny) write(*,'(A)') 'PlasComCM: Negative Jacobian in solid grid!'

                   if(input%TMSolve == 3) then
                      ! ... compute the deformation gradient tensor
                      F(:,:) = 0.0_rfreal
                      do j = 1, ND
                         do i = 1, ND
                            do dir = 1, ND
                               do lp = 1, NdsPerElem
                                  F(i,j) = F(i,j) + Xc(lp,i)*dVolShpFcn(lp,dir,lg)*Jac(dir,j)
                               end do
                            end do
                         end do
                      end do

                      Cinv = MATMUL(TRANSPOSE(F),F)

                      Call InvertMatrix(Cinv)
                      
                      ! ... determinant of the deformation gradient tensor
                      Call MatrixDeterminant(F,JF)

                   elseif(input%TMSolve == 1) then
                      Cinv(:,:) = 0.0_rfreal
                      do j = 1, ND
                         Cinv(j,j) = 1.0_rfreal
                      end do
                      JF = 1.0_rfreal
                   end if

                   ! ... calculate heat load in refference configuration
                   R2CFac = 0.0_rfreal
                   do i = 1, ND
                      do j = 1, ND
                         R2CFac = R2CFac + SNrm(i)*Cinv(i,j)*SNrm(j)
                      end do 
                   end do
                   
                   Q(:) = qc(:)*sqrt(R2CFac)

                   ! ... add to integral at this Gauss point
                   do j = 1, NdsPerFace
                      do i = 1, NdsPerFace
                         r(i) = r(i) + Q(j)*BndShpFcn(j,lg)*BndShpFcn(i,lg)*JF*SurfJac*Weight(lg)
                      end do
                      !IntegralTest = IntegralTest + Q(j)*BndShpFcn(j,lg)*JF*SurfJac*Weight(lg)
                   end do
                end do ! ... lg
                ! ... add to PETSc matrix
                Call InsertRt(region, npatch, NdsPerFace, lpe, r)
             end do ! ... ei
          end do ! ... ej
       end do ! ... ek
       !print*,IntegralTest

       

    end do

    deallocate(F,Cinv,Jac,r)
       
  end Subroutine GetExternalHeatLoad

  Subroutine GetTemperatureBCs(region, ng)

    USE ModGlobal
    USE ModDataStruct
    USE ModTMEOM
    USE ModMetrics
    USE ModPETSc

    Implicit None

    Type(t_region), pointer :: region
    Integer :: ng

    ! ... local variables
    Type(t_mixt_input), pointer :: input
    Type(t_smixt), pointer :: TMstate
    Type(t_sgrid), pointer :: TMgrid
    Type(t_spatch), pointer :: TMpatch
    Integer :: Npe(MAX_ND), Ne(MAX_ND)
    Integer, pointer :: LM(:,:), LM2D(:,:), L2G(:)
    Integer :: ei, ej, ek, le, l0, dir, lp, i, j, k, p, dir2, lg, n
    Integer :: NdsPerElem, ND, NdsPerFace, npatch, nPtsGlb, nStart, nPtsOwned, BCcounter

    ! ... simplicity
    TMgrid  => region%TMgrid(ng)
    TMstate => region%TMstate(ng)
    input   => TMgrid%input
    LM      => TMgrid%LM
    ND      =  TMgrid%ND
    nPtsOwned = TMgrid%nPtsOwned
    
    call RemoveTempBCs(input%TMSolve, region, ng)

  end Subroutine GetTemperatureBCs

  Subroutine GetExternalLoadFromTensor(region, ng)

    USE ModGlobal
    USE ModDataStruct
    USE ModTMEOM
    USE ModMetrics
    USE ModPETSc
    USE ModMPI

    Implicit None

    Type(t_region), pointer :: region
    Integer :: ng

    ! ... local variables
    Type(t_mixt_input), pointer :: input
    Type(t_smixt), pointer :: TMstate
    Type(t_sgrid), pointer :: TMgrid
    Type(t_spatch), pointer :: TMpatch
    Integer :: Npe(MAX_ND), Ne(MAX_ND), Npee(MAX_ND), Npes(MAX_ND), Nes(MAX_ND), Nee(MAX_ND)
    Integer, pointer :: LM(:,:), LM2D(:,:), TanDir(:)
    Integer :: ei, ej, ek, le, l0, dir, lp, i, j, k, p, dir2, lg, lpe, count
    Integer :: NdsPerElem, ND, nGauss, NdsPerFace, npatch, normDir, sgn
    Real(rfreal), allocatable :: X(:,:), Xc(:,:), TauC(:,:), Tau(:,:), SNrm(:), TanVec(:,:), Sigma(:,:)
    Real(rfreal), pointer :: dVolShpFcn(:,:,:),BndShpFcn(:,:), Weight(:)
    Real(rfreal), pointer :: F(:,:), Finv(:,:), Jac(:,:), BndJac(:,:), rs_ext(:)
    Real(rfreal) :: dJac, SurfJac, R2CFac, JF, PreMult, dsgn

    ! ... simplicity
    TMgrid  => region%TMgrid(ng)
    TMstate => region%TMstate(ng)
    input   => TMgrid%input
    LM      => TMgrid%LM
    ND      =  TMgrid%ND

    ! ... how many nodes and Gauss points
    if(input%ElemType == QUADRATIC) then
       if(ND == 2) then
          NdsPerElem = 8
          NdsPerFace = 3
          nGauss = 3
       elseif(ND > 2) then
          NdsPerElem = 20
          NdsPerFace = 8
          nGauss = 9
       end if
    elseif(input%ElemType == LINEAR) then
       if(ND == 2) then
          NdsPerElem = 4
          NdsPerFace = 2
          nGauss = 2
       elseif(ND > 2) then
          NdsPerElem = 8
          NdsPerFace = 4
          nGauss = 4
       end if
    end if
    
    ! ... allocate element local arrays
    ! ... current configuration, element structural
    allocate(Xc(NdsPerElem,ND), rs_ext(NdsPerFace*ND))
    ! ... reference configuration
    allocate(X(NdsPerElem,ND))

    ! ... Jacobian matrix
    allocate(Jac(ND,ND))

    ! ... deformation gradient and its inverse
    allocate(F(ND,ND),Finv(ND,ND))
    
    ! ... surface normal
    allocate(SNrm(ND))

    ! ... tangent vectors
    allocate(TanVec(ND,ND-1))

    ! ... surface traction in the current and reference configurations
    allocate(TauC(NdsPerFace,ND), Tau(NdsPerFace,ND))

    ! ... stress tensor
    allocate(Sigma(NdsPerFace,ND*ND))

    do npatch = 1, region%nTMPatches

       TMpatch => region%TMpatch(npatch)
       TMgrid  => region%TMgrid(TMpatch%gridID)
       TMstate => region%TMstate(TMpatch%gridID)
       input   => TMgrid%input
       TanDir  => TMpatch%TanDir
       normDir = abs(TMpatch%normDir)
       sgn = normDir / TMpatch%normDir
       dsgn = dble(sgn)
       LM2D    => TMpatch%LM2D
       
       if (TMpatch%gridID /= ng) CYCLE ! ... apply BC only in this grid
       if (TMpatch%BCType /= STRUCTURAL_PRESSURE .AND. TMpatch%BCType /= STRUCTURAL_INTERACTING) CYCLE ! ... only structural load bounadry conditions

       BndShpFcn  => TMpatch%BndShpFcn
       dVolShpFcn => TMpatch%dVolShpFcn
       Weight     => TMpatch%Weight
       
       
       ! ... how many elements in this partition
       Ne(:) = 1
       if(input%ElemType == QUADRATIC) then
          do dir = 1, ND
             Ne(dir) = (TMgrid%ie(dir)-TMgrid%is(dir))/2
          end do
       elseif(input%ElemType == LINEAR) then
          do dir = 1, ND
             Ne(dir) = (TMgrid%ie(dir)-TMgrid%is(dir))
          end do
       end if

       ! ... how many elements are on this patch
       Npe(:) = 1
       if(input%ElemType == QUADRATIC) then
          do dir = 1, ND
             Npe(dir) = (TMpatch%ie(dir)-TMpatch%is(dir))/2
          end do
       elseif(input%ElemType == LINEAR) then
          do dir = 1, ND
             Npe(dir) = (TMpatch%ie(dir)-TMpatch%is(dir))
          end do
       end if

       ! ... starting and ending elements in this process' partition of the grid
       Nes(:) = 1; Nee(:) = 1
       if(input%ElemType == QUADRATIC) then
          do dir = 1, ND
             Nes(dir) = (TMgrid%is(dir)-1)/2+1
             Nee(dir) = (TMgrid%ie(dir)-1)/2
          end do
       elseif(input%ElemType == LINEAR) then
          do dir = 1, ND
             Nes(dir) = TMgrid%is(dir)
             Nee(dir) = TMgrid%ie(dir)-1
          end do
       end if
       
       ! ... starting and ending elements in the patch
       Npes(:) = 1; Npee(:) = 1
       do dir = 1, ND
          if(Npe(dir) == 0) then
             ! ... keep counting correct if patch is on face with normDir == dir
             if(sgn<0) then
                Npes(dir) = Nee(dir)
                Npee(dir) = Nee(dir)
             else
                Npes(dir) = Nes(dir)
                Npee(dir) = Nes(dir)
             end if
             ! ... to keep product(Npe) >= 1
             Npe(dir) = 1
          else
             if(input%ElemType == QUADRATIC) then
                Npes(dir) = (TMpatch%is(dir)-1)/2+1
                Npee(dir) = (TMpatch%ie(dir)-1)/2
             elseif(input%ElemType == LINEAR) then
                Npes(dir) = TMpatch%is(dir)
                Npee(dir) = TMpatch%ie(dir)-1                
             end if
          end if
       end do

       PreMult = 0.0_rfreal
       count = 0
       do ek = Npes(3), Npee(3)
          do ej = Npes(2), Npee(2)
             do ei = Npes(1), Npee(1)
                count = count + 1
                le  = (ek-Nes(3) )*Ne(2) *Ne(1)  + (ej-Nes(2) )*Ne(1)  + (ei-Nes(1))  + 1
                lpe = (ek-Npes(3))*Npe(2)*Npe(1) + (ej-Npes(2))*Npe(1) + (ei-Npes(1)) + 1

                ! ... fill the local array
                do dir = 1, ND
                   do lp = 1, NdsPerElem
                      X(lp,dir) = TMgrid%X(LM(lp,le),dir)
                      Xc(lp,dir) = TMstate%q(LM(lp,le),dir)
                   end do
                end do
            
                Sigma(:,:) = 0.0_rfreal
                ! ... get the load from the stress tensor at each node point

                if (TMpatch%BCType == STRUCTURAL_INTERACTING) then
                   ! ... get data from stress tensor from the fluid domain
                   do lp = 1, NdsPerFace
                      do i = 1, ND
                         do j = 1, ND
                            Sigma(lp,t2map(i,j)) = TMpatch%FluidStressTns(TMpatch%LMP3D2D(lp,lpe),t2mapsym(i,j))
                         end do
                      end do
                   end do
                elseif (TMpatch%BCType == STRUCTURAL_PRESSURE) then
                   do lp = 1, NdsPerFace
                      do i = 1, ND
                         ! ... pressure is opposite the outward normal
                         Sigma(lp,t2map(i,i)) = -TMpatch%BCVal(1)
                         
                      end do
                   end do
                end if
                
                rs_ext(:) = 0.0_rfreal
                ! ... loop through the Gauss points
                do lg = 1, nGauss

                   ! ... compute the Jacobian
                   Jac(:,:) = 0.0_rfreal
                   do j = 1, ND
                      do i = 1, ND
                         do lp = 1, NdsPerElem
                            Jac(i,j) = Jac(i,j) + dVolShpFcn(lp,j,lg)*X(lp,i)
                         end do
                      end do
                   end do

                   ! ... get surface normal and Jacobian before we invert
                   do dir = 1, ND
                      do j = 1, ND-1
                         TanVec(dir,j) = Jac(dir,TanDir(j))
                      end do
                   end do

                   if(ND == 3) then
                      
                      call cross_product(TanVec(:,1),TanVec(:,2),SNrm)

                   elseif(ND == 2) then

                      SNrm(1) = -TanVec(2,1)
                      SNrm(2) =  TanVec(1,1)
                      
                   end if

                   ! ... now compute the Jacobian
                   SurfJac = DOT_PRODUCT(SNrm,SNrm)
                   SurfJac = sqrt(SurfJac)

                   ! ... normalize the surface normal vector
                   SNrm = SNrm/SurfJac
             
                   ! ... assure surface norm is outward facing
                   do dir = 1, ND
                      SNrm(dir) = -dsgn*abs(SNrm(dir))
                   end do
      
                   ! ... compute the determinant and inverse
                   call InvertMatrix(Jac,dJac)

                   ! ... check for singular matrix
                   if(dJac .lt. tiny) write(*,'(A)') 'PlasComCM: Negative Jacobian in solid grid!'

                   ! ... compute the deformation gradient tensor
                   F(:,:) = 0.0_rfreal
                   do j = 1, ND
                      do i = 1, ND
                         do dir = 1, ND
                            do lp = 1, NdsPerElem
                               F(i,j) = F(i,j) + Xc(lp,i)*dVolShpFcn(lp,dir,lg)*Jac(dir,j)
                            end do
                         end do
                      end do
                   end do
                   
                   Finv = F;

                   ! ... determinant of the deformation gradient tensor
                   Call InvertMatrix(Finv,JF)

                   ! ... now get the traction
                   Tau(:,:) = 0.0_rfreal
                   do j = 1, ND
                      do i = 1, ND 
                         do lp = 1, NdsPerFace
                            do dir = 1, ND
                               Tau(lp,j) = Tau(lp,j) + Sigma(lp,t2map(i,j))*JF*Finv(dir,i)*SNrm(dir)
                            end do
                         end do
                      end do
                   end do
                   

                   do j = 1, NdsPerFace

                      do dir = 1, ND

                         do i = 1, NdsPerFace
                            l0 = (i-1)*ND + dir
                            rs_ext(l0) = rs_ext(l0) + BndShpFcn(i,lg)*BndShpFcn(j,lg)*SurfJac*Weight(lg)*Tau(j,dir)
                         end do

    
                      end do
                   end do
                end do ! ... lg                
                ! ... add to PETSc matrix
                Call InsertRs(region, npatch, NdsPerFace, lpe, rs_ext)
             end do ! ... ei
          end do ! ... ej
       end do ! ... ek

    
    end do


    deallocate(F,Finv,Jac,rs_ext)
       
  end Subroutine GetExternalLoadFromTensor


  Subroutine GetTensorLoadTangent(region, ng)

    USE ModGlobal
    USE ModDataStruct
    USE ModTMEOM
    USE ModMetrics
    USE ModPETSc
    USE ModMPI

    Implicit None

    Type(t_region), pointer :: region
    Integer :: ng

    ! ... local variables
    Type(t_mixt_input), pointer :: input
    Type(t_smixt), pointer :: TMstate
    Type(t_sgrid), pointer :: TMgrid
    Type(t_spatch), pointer :: TMpatch
    Integer :: Npe(MAX_ND), Ne(MAX_ND), Npee(MAX_ND), Npes(MAX_ND), Nes(MAX_ND), Nee(MAX_ND)
    Integer, pointer :: LM(:,:), LM2D(:,:), TanDir(:)
    Integer :: ei, ej, ek, le, l0, dir, lp, i, j, k, p, dir2, lg, l, q, l1, l2, lpe, P2
    Integer :: NdsPerElem, ND, nGauss, NdsPerFace, npatch, normDir, sgn
    Real(rfreal), allocatable :: X(:,:), Xc(:,:), TauC(:,:), Tau(:,:), TanVec(:,:), Sigma(:,:)
    Real(rfreal), pointer :: dVolShpFcn(:,:,:),BndShpFcn(:,:), Weight(:), SNrm(:), dShpdX(:,:)
    Real(rfreal), pointer :: F(:,:), Finv(:,:), Jac(:,:), BndJac(:,:), rs_tan(:,:),Temp(:,:)
    Real(rfreal) :: dJac, SurfJac, R2CFac, JF, G(MAX_ND,MAX_ND,MAX_ND,MAX_ND), FaceNum

    ! ... simplicity
    TMgrid  => region%TMgrid(ng)
    TMstate => region%TMstate(ng)
    input   => TMgrid%input
    LM      => TMgrid%LM
    ND      =  TMgrid%ND

    ! ... how many nodes and Gauss points
    if(input%ElemType == QUADRATIC) then
       if(ND == 2) then
          NdsPerElem = 8
          NdsPerFace = 3
          nGauss = 3
       elseif(ND > 2) then
          NdsPerElem = 20
          NdsPerFace = 8
          nGauss = 9
       end if
    elseif(input%ElemType == LINEAR) then
       if(ND == 2) then
          NdsPerElem = 4
          NdsPerFace = 2
          nGauss = 2
       elseif(ND > 2) then
          NdsPerElem = 8
          NdsPerFace = 4
          nGauss = 4
       end if
    end if

    ! ... 
    allocate(dShpdX(NdsPerElem,ND))

    
    ! ... allocate element local arrays
    ! ... current configuration, element structural
    allocate(Xc(NdsPerElem,ND), rs_tan(NdsPerFace*ND,NdsPerFace*ND))
    ! ... reference configuration
    allocate(X(NdsPerElem,ND))

    ! ... Jacobian matrix
    allocate(Jac(ND,ND))

    ! ... deformation gradient and its inverse
    allocate(F(MAX_ND,MAX_ND),Finv(MAX_ND,MAX_ND))
    
    ! ... surface normal
    allocate(SNrm(ND))

    ! ... tangent vectors
    allocate(TanVec(ND,ND-1))

    ! ... surface traction in the current and reference configurations
    allocate(TauC(NdsPerFace,ND), Tau(NdsPerFace,ND))
    
    allocate(Temp(NdsPerFace,ND))

    ! ... stress tensor
    allocate(Sigma(NdsPerFace,ND*ND))

    do npatch = 1, region%nTMPatches

       TMpatch => region%TMpatch(npatch)
       TMgrid  => region%TMgrid(TMpatch%gridID)
       TMstate => region%TMstate(TMpatch%gridID)
       input   => TMgrid%input
       TanDir  => TMpatch%TanDir
       normDir = abs(TMpatch%normDir)
       sgn = normDir / TMpatch%normDir

       ! ... select which face of the 3D element this patch is on
       if(sgn > 0) then
          FaceNum = 1
       elseif(sgn < 0) then
          FaceNum = 2
       end if
       
       if (TMpatch%gridID /= ng) CYCLE ! ... apply BC only in this grid
       if (TMpatch%BCType /= STRUCTURAL_PRESSURE .AND. TMpatch%BCType /= STRUCTURAL_INTERACTING) CYCLE ! ... only traction boundary conditions

       BndShpFcn  => TMpatch%BndShpFcn
       dVolShpFcn => TMpatch%dVolShpFcn
       Weight     => TMpatch%Weight


       
       ! ... how many elements in this partition
       Ne(:) = 1
       if(input%ElemType == QUADRATIC) then
          do dir = 1, ND
             Ne(dir) = (TMgrid%ie(dir)-TMgrid%is(dir))/2
          end do
       elseif(input%ElemType == LINEAR) then
          do dir = 1, ND
             Ne(dir) = (TMgrid%ie(dir)-TMgrid%is(dir))
          end do
       end if

       ! ... how many elements are on this patch
       Npe(:) = 1
       if(input%ElemType == QUADRATIC) then
          do dir = 1, ND
             Npe(dir) = (TMpatch%ie(dir)-TMpatch%is(dir))/2
          end do
       elseif(input%ElemType == LINEAR) then
          do dir = 1, ND
             Npe(dir) = (TMpatch%ie(dir)-TMpatch%is(dir))
          end do
       end if


       ! ... starting and ending elements in this process' partition of the grid
       Nes(:) = 1; Nee(:) = 1
       if(input%ElemType == QUADRATIC) then
          do dir = 1, ND
             Nes(dir) = (TMgrid%is(dir)-1)/2+1
             Nee(dir) = (TMgrid%ie(dir)-1)/2
          end do
       elseif(input%ElemType == LINEAR) then
          do dir = 1, ND
             Nes(dir) = TMgrid%is(dir)
             Nee(dir) = TMgrid%ie(dir)-1
          end do
       end if
       
       ! ... starting and ending elements in the patch
       Npes(:) = 1; Npee(:) = 1
       do dir = 1, ND
          if(Npe(dir) == 0) then
             ! ... keep counting correct if patch is on face with normDir == dir
             if(sgn<0) then
                Npes(dir) = Nee(dir)
                Npee(dir) = Nee(dir)
             else
                Npes(dir) = Nes(dir)
                Npee(dir) = Nes(dir)
             end if
             ! ... to keep product(Npe) >= 1
             Npe(dir) = 1
          else
             if(input%ElemType == QUADRATIC) then
                Npes(dir) = (TMpatch%is(dir)-1)/2+1
                Npee(dir) = (TMpatch%ie(dir)-1)/2
             elseif(input%ElemType == LINEAR) then
                Npes(dir) = TMpatch%is(dir)
                Npee(dir) = TMpatch%ie(dir)-1                
             end if
          end if
       end do

       do ek = Npes(3), Npee(3)
          do ej = Npes(2), Npee(2)
             do ei = Npes(1), Npee(1)
                
                le  = (ek-Nes(3) )*Ne(2) *Ne(1)  + (ej-Nes(2) )*Ne(1)  + (ei-Nes(1))  + 1
                lpe = (ek-Npes(3))*Npe(2)*Npe(1) + (ej-Npes(2))*Npe(1) + (ei-Npes(1)) + 1
       

                ! ... fill the local array
                if(input%TMSolve == 3) then
                   do dir = 1, ND
                      do lp = 1, NdsPerElem
                         X(lp,dir) = TMgrid%X(LM(lp,le),dir)
                         Xc(lp,dir) = TMstate%q(LM(lp,le),dir)
                      end do
                   end do
                else
                   do dir = 1, ND
                      do lp = 1, NdsPerElem
                         ! ... current configuration = reference configuration
                         Xc(lp,dir) = TMgrid%X(LM(lp,le),dir)
                         X(lp,dir) = TMgrid%X(LM(lp,le),dir)
                      end do
                   end do
                end if

                Sigma(:,:) = 0.0_rfreal
                ! ... get the load from the stress tensor at each node point
                if (TMpatch%BCType == STRUCTURAL_INTERACTING) then
                   ! ... get data from stress tensor from the fluid domain
                   do lp = 1, NdsPerFace
                      do i = 1, ND
                         do j = 1, ND
                            Sigma(lp,t2map(i,j)) = TMpatch%FluidStressTns(TMpatch%LMP3D2D(lp,lpe),t2mapsym(i,j))
                         end do
                      end do
                   end do
                elseif (TMpatch%BCType == STRUCTURAL_PRESSURE) then
                   do lp = 1, NdsPerFace
                      do i = 1, ND
                         Sigma(lp,t2map(i,i)) = -TMpatch%BCVal(1)
                      end do
                   end do
                end if

                
                rs_tan(:,:) = 0.0_rfreal
                ! ... loop through the Gauss points
                do lg = 1, nGauss

                   ! ... compute the Jacobian
                   Jac(:,:) = 0.0_rfreal
                   do j = 1, ND
                      do i = 1, ND
                         do lp = 1, NdsPerElem
                            Jac(i,j) = Jac(i,j) + dVolShpFcn(lp,j,lg)*X(lp,i)
                         end do
                      end do
                   end do

                   ! ... get surface normal and Jacobian before we invert
                   do dir = 1, ND
                      do j = 1, ND-1
                         TanVec(dir,j) = Jac(dir,TanDir(j))
                      end do
                   end do

                   if(ND == 3) then
                      
                      call cross_product(TanVec(:,1),TanVec(:,2),SNrm)

                   elseif(ND == 2) then

                      SNrm(1) = -TanVec(2,1)
                      SNrm(2) = TanVec(1,1)

                   end if

                   ! ... now compute the Jacobian
                   SurfJac = DOT_PRODUCT(SNrm,SNrm)
                   SurfJac = sqrt(SurfJac)

                   ! ... normalize the surface normal vector
                   SNrm = SNrm/SurfJac
                   
                   ! ... compute the determinant and inverse
                   call InvertMatrix(Jac,dJac)

                   ! ... check for singular matrix
                   if(dJac .lt. tiny) write(*,'(A)') 'PlasComCM: Negative Jacobian in solid grid!'

                   dShpdX(:,:) = 0.0_rfreal
                   ! ... coordinate transformation to Cartesian
                   do dir = 1, ND
                      do j = 1, ND 
                         do lp = 1, NdsPerElem
                            dShpdX(lp,j) = dShpdX(lp,j) + dVolShpFcn(lp,dir,lg)*Jac(dir,j)
                         end do
                      end do
                   end do


                   ! ... compute the deformation gradient tensor
                   F(:,:) = 0.0_rfreal
                   do j = 1, ND
                      do i = 1, ND

                            do lp = 1, NdsPerElem
                               F(i,j) = F(i,j) + Xc(lp,i)*dShpdX(lp,j)
                            end do

                      end do
                   end do
                   if(ND == 2) F(MAX_ND,MAX_ND) = 1.0_rfreal
                   
                   Finv = F;

                   ! ... determinant of the deformation gradient tensor
                   Call InvertMatrix(Finv,JF)

                   G = 0.0_rfreal
                   ! ... now get the tangent operator
                   Call StressTensTangent(G,Finv,SNrm,JF)

                   Tau(:,:) = 0.0_rfreal
                   do j = 1, ND
                      do i = 1, ND 
                         do lp = 1, NdsPerFace
                            do dir = 1, ND
                               Tau(lp,j) = Tau(lp,j) + Sigma(lp,t2map(i,j))*JF*Finv(dir,i)*SNrm(dir)
                            end do
                         end do
                      end do
                   end do

                   Temp(:,:) = 0.0_rfreal
                   do l0 = 1, NdsPerFace
                      do dir = 1, ND
                            Temp(l0,dir) = Temp(l0,dir)+dShpdX(TMgrid%FaceNodes(l0,normDir,FaceNum),dir)
                      end do
                   end do


                   do dir = 1, ND
                      do dir2 = 1, ND
                         do P2 = 1, ND
                            do l = 1, ND
                               do q = 1, ND
                                  do j = 1, NdsPerFace
                                     do i = 1, NdsPerFace
                                        do k = 1, NdsPerFace
                                           l1 = (i-1)*ND + dir
                                           l2 = (j-1)*ND + dir2
                                           
                                           rs_tan(l1,l2) = rs_tan(l1,l2) - BndShpFcn(i,lg)*BndShpFcn(k,lg)*&
                                                Sigma(k,t2map(l,dir))*SNrm(q)*G(q,l,dir2,P2)*Temp(j,P2)*SurfJac
                                           
                                        end do
                                     end do
                                  end do
                               end do
                            end do
                         end do
                      end do
                   end do
                end do ! ... lg
                ! ... add to PETSc matrix
                ! ... call PETSc here
                Call InsertRs_tan(region, npatch, NdsPerFace, lpe, rs_tan)
             end do ! ... ei
          end do ! ... ej
       end do ! ... ek
       
    end do

    deallocate(F, Finv, Jac, rs_tan, SNrm, Temp,dShpdX)
       
  end Subroutine GetTensorLoadTangent


  Subroutine StressTensTangent(G,Finv,SNrm,JF)

    USE ModDataStruct
    USE ModGlobal

    implicit none

    Real(rfreal), pointer :: SNrm(:)
    Real(rfreal) :: G(MAX_ND,MAX_ND,MAX_ND,MAX_ND), Finv(MAX_ND,MAX_ND), JF

    ! ... local variables
    Integer :: I1, i2, k2, K1
    
    do I1 = 1, MAX_ND
       do i2 = 1, MAX_ND
          do k2 = 1, MAX_ND
             do K1 = 1, MAX_ND
                G(I1,i2,k2,K1) = JF*(Finv(I1,i2)*Finv(K1,k2)-Finv(I1,k2)*Finv(K1,i2))
             end do
          end do
       end do
    end do
    
  end Subroutine StressTensTangent

  Subroutine GetDisplacementBCs(region, ng)

    USE ModGlobal
    USE ModDataStruct
    USE ModTMEOM
    USE ModMetrics
    USE ModPETSc

    Implicit None

    Type(t_region), pointer :: region
    Integer :: ng

    ! ... local variables
    Type(t_mixt_input), pointer :: input
    Type(t_smixt), pointer :: TMstate
    Type(t_sgrid), pointer :: TMgrid
    Type(t_spatch), pointer :: TMpatch
    Integer :: Npe(MAX_ND), Ne(MAX_ND)
    Integer, pointer :: LM(:,:), LM2D(:,:), L2G(:)
    Integer :: ei, ej, ek, le, l0, dir, lp, i, j, k, p, dir2, lg, n
    Integer :: NdsPerElem, ND, NdsPerFace, npatch, nPtsGlb, nStart, nPtsOwned, BCcounter
    Real(rfreal), pointer :: BCValTemp(:),BCVal(:)
    Integer, pointer :: BCIndTemp(:),BCInd(:)


    ! ... simplicity
    TMgrid  => region%TMgrid(ng)
    TMstate => region%TMstate(ng)
    input   => TMgrid%input
    LM      => TMgrid%LM
    ND      =  TMgrid%ND
    nPtsOwned = TMgrid%nPtsOwned

    call RemoveDispBCs(input%TMSolve, region, ng)

  end Subroutine GetDisplacementBCs
  
  Subroutine SetDirichletDofs(region)

    USE ModGlobal
    USE ModDataStruct
    USE ModTMEOM
    USE ModMetrics
    USE ModPETSc
    USE ModMPI

    Implicit None

    Type(t_region), pointer :: region

    ! ... local variables
    Type(t_mixt_input), pointer :: input
    Type(t_smixt), pointer :: TMstate
    Type(t_sgrid), pointer :: TMgrid
    Type(t_spatch), pointer :: TMpatch
    Integer :: Npe(MAX_ND), Ne(MAX_ND)
    Integer, pointer :: LM(:,:), LM2D(:,:), L2G(:)
    Integer :: ei, ej, ek, le, l0, dir, lp, i, j, k, p, dir2, lg, n, ng
    Integer :: NdsPerElem, ND, NdsPerFace, npatch, nPtsGlb, nStart, nPtsOwned, BCcounter, TempFlag, DispFlag
    Integer, pointer :: BCIndTempThrm(:), BCIndTempDisp(:)
    Real(rfreal), pointer :: BCValTempThrm(:),BCValTempDisp(:)


    ! ... initialize
    do ng = 1, region%nTMGrids

       region%TMstate(ng)%TempBCNum = 0
       region%TMstate(ng)%DispBCNum = 0
       ND = region%input%ND
       TempFlag = 0
       DispFlag = 0

       BCcounter = 0


       nPtsOwned =  region%TMgrid(ng)%nPtsOwned
       allocate(BCIndTempThrm(nPtsOwned),BCValTempThrm(nPtsOwned))
       BCIndTempThrm = -1
       allocate(BCIndTempDisp(ND*nPtsOwned),BCValTempDisp(ND*nPtsOwned))
       BCIndTempDisp = -1
       
       do npatch = 1, region%nTMPatches


          TMpatch   => region%TMpatch(npatch)
          TMgrid    => region%TMgrid(TMpatch%gridID)
          TMstate   => region%TMstate(TMpatch%gridID)
          input     => TMgrid%input
          LM2D      => TMpatch%LM2D
          L2G       => TMgrid%L2G
          nPtsGlb   =  TMgrid%nPtsGlb
          nStart    =  TMgrid%nStart
          nPtsOwned =  TMgrid%nPtsOwned
          ND        =  input%ND
          
          if (TMpatch%gridID /= ng) CYCLE ! ... apply BC only in this grid

          if (TMpatch%BCType /= THERMAL_TEMPERATURE .and. TMpatch%BCType /= STRUCTURAL_DISPLACEMENT) cycle ! ... only Dirichlet  boundary conditions

          if (TMpatch%BCType == THERMAL_TEMPERATURE .and. input%TMSolve == 2) cycle ! ... only relevant boundary conditions

          if (TMpatch%BCType == STRUCTURAL_DISPLACEMENT .and. input%TMSolve == 1) cycle ! ... only relevant boundary conditions

          ! ... how many nodes and Gauss points
          if(input%ElemType == QUADRATIC) then
             if(ND == 2) then
                NdsPerElem = 8
                NdsPerFace = 3
             elseif(ND > 2) then
                NdsPerElem = 20
                NdsPerFace = 8
             end if
          elseif(input%ElemType == LINEAR) then
             if(ND == 2) then
                NdsPerElem = 4
                NdsPerFace = 2
             elseif(ND > 2) then
                NdsPerElem = 8
                NdsPerFace = 4
             end if
          end if

          ! ... how many elements in this partition
          Ne(:) = 1
          if(input%ElemType == QUADRATIC) then
             do dir = 1, ND
                Ne(dir) = (TMgrid%ie(dir)-TMgrid%is(dir))/2
             end do
          elseif(input%ElemType == LINEAR) then
             do dir = 1, ND
                Ne(dir) = (TMgrid%ie(dir)-TMgrid%is(dir))
             end do
          end if
          
          ! ... how many elements are on this patch
          Npe(:) = 1
          if(input%ElemType == QUADRATIC) then
             do dir = 1, ND
                Npe(dir) = (TMpatch%ie(dir)-TMpatch%is(dir))/2
                Npe(dir) = max(Npe(dir),1)
             end do
          elseif(input%ElemType == LINEAR) then
             do dir = 1, ND
                Npe(dir) = (TMpatch%ie(dir)-TMpatch%is(dir))
                Npe(dir) = max(Npe(dir),1)
             end do
          end if


          ! ... loop through the elements
          do ek = 1, Npe(3)
             do ej = 1, Npe(2)
                do ei = 1, Npe(1)
                   ! ... element index
                   le = (ek-1)*Npe(1)*Npe(2) + (ej-1)*Npe(1) + ei

                   ! ... get the index number and value at each face node
                   do lp = 1, NdsPerFace
                      n = L2G(LM2D(lp,le))
                      if(n < nStart .or. n >= nStart + nPtsOwned) cycle
                      BCcounter = BCcounter + 1
                      if(TMpatch%BCType == THERMAL_TEMPERATURE) then
                         TempFlag = 1
                         BCIndTempThrm(n-nStart + 1) = L2G(LM2D(lp,le))
                         BCValTempThrm(n-nStart + 1) = TMpatch%BCVal(1)
                      elseif(TMpatch%BCType == STRUCTURAL_DISPLACEMENT) then
                         DispFlag = 1
                         do dir = 1, ND
                            BCIndTempDisp(((n-nStart + 1)-1)*ND+dir) = (L2G(LM2D(lp,le))-1)*ND+dir
                            BCValTempDisp(((n-nStart + 1)-1)*ND+dir) = TMpatch%BCVal(dir)
                         end do
                      end if
                   end do
                end do ! ... ei
             end do ! ... ej
          end do ! ... ek       

       end do ! ... npatch

       ! ... is it a temperature BC or a displacement BC
       if(TempFlag == 1) then

          BCcounter = 0
          do lp = 1, nPtsOwned
             if(BCIndTempThrm(lp) == -1) cycle
             BCcounter = BCCounter + 1
          end do

          Allocate(TMstate%BCTempVal(BCcounter))
          Allocate(TMstate%BCTempInd(BCcounter))
          TMstate%TempBCNum = BCCounter
          BCcounter = 0
          do lp = 1, nPtsOwned
             if(BCIndTempThrm(lp) == -1) cycle
             BCcounter = BCcounter + 1
             TMstate%BCTempInd(BCcounter) = BCIndTempThrm(lp)
             TMstate%BCTempVal(BCcounter) = BCValTempThrm(lp)
          end do
       end if
       if(DispFlag == 1) then
          ! ... ND values associated with this node
          ! ... may need to change later to have indices for each dof
          BCcounter = 0
          do lp = 1, nPtsOwned*ND
             if(BCIndTempDisp(lp) == -1) cycle
             BCcounter = BCCounter + 1
          end do
          Allocate(TMstate%BCDispVal(BCcounter))
          Allocate(TMstate%BCDispInd(BCcounter))
          TMstate%DispBCNum = BCCounter
          BCcounter = 0
          do lp = 1, nPtsOwned*ND
             if(BCIndTempDisp(lp) == -1) cycle
             BCcounter = BCcounter + 1
             TMstate%BCDispInd(BCcounter) = BCIndTempDisp(lp)
             TMstate%BCDispVal(BCcounter) = BCValTempDisp(lp)
          end do
       end if

       deallocate(BCIndTempThrm,BCValTempThrm)
       deallocate(BCIndTempDisp,BCValTempDisp)

       if(region%input%PinnedBC == TRUE) then
          if(region%TMRank==0) write(*,'(A)') 'PlasComCM: WARNING: Using pinned boundary conditions'
          call AdjustBCs(region, ng)
       end if

       do lp = 1, region%input%numTMproc
          if(region%TMRank == lp-1) then
             write(*,'(A,I3,A,I5,A,I5,A,I1)')&
                  'Rank ',region%TMRank,' has ',region%TMstate(ng)%TempBCNum,' temperature nodes and ',region%TMstate(ng)%DispBCNum,' displacement nodes on grid ',ng
          end if
          call MPI_BARRIER(region%TMgrid(ng)%Comm,ierr)
       end do

    end do
    

  end Subroutine SetDirichletDofs

  Subroutine AdjustBCs(region, ng)
    
    USE ModGlobal
    USE ModDataStruct 
    USE ModMPI

    Implicit None

    Type(t_region), pointer :: region
    Integer :: ng

    ! ... local variables
    Type(t_smixt), pointer :: TMstate
    Type(t_sgrid), pointer :: TMgrid
    Type(t_mixt_input), pointer :: input
    Integer, pointer :: L2G(:), L2P3D(:), BCIndTempDisp(:)
    Integer :: nPtsLoc, ProcLoc(4), N(MAX_ND)
    Integer :: dir, l0, l1, ND, count, i, j, k, ll

    ! ... simplicity
    TMgrid  => region%TMgrid(ng)
    TMstate => region%TMstate(ng)
    L2G     => TMgrid%L2G
    L2P3D   => TMgrid%L2P3D
    ND      = TMgrid%ND
    nPtsLoc = TMgrid%nPtsLoc
    
    ! ... clear all previously defined displacement BCs
    TMstate%DispBCNum = 0
    deallocate(TMstate%BCDispVal,TMstate%BCDispInd)

    ! ... pinned points on j = 1 plane
    if(TMgrid%CartCoords(2) > 0) return

    ProcLoc(:) = 0
    
    ! ... find out which stream and spanwise boundaries this process is on
    if(TMgrid%CartCoords(1) == 0) ProcLoc(1) = 1
    if(TMgrid%CartCoords(1) == TMgrid%cartDims(1)-1) ProcLoc(2) = 1
    if(ND == 3) then
       if(TMgrid%CartCoords(3) == 0) ProcLoc(3) = 1
       if(TMgrid%CartCoords(3) == TMgrid%cartDims(3)-1) ProcLoc(4) = 1
    end if
    
    ! ... exit if not on any edge
    if(sum(ProcLoc) == 0) return

    ! ... allocate space for temporary vector
    N(:) = 1
    do dir = 1, ND
       N(dir) = TMgrid%ie(dir) - TMgrid%is(dir) + 1
    end do

    allocate(BCIndTempDisp(2*ND*(N(1)+N(3))))

    ! ... count and record edge nodes to be pinned
    count = 0
    
    do l0 = 1, nPtsLoc

       ll = TMgrid%L2P3D(l0)
       ! ... convert from linear to triplet indices
       i = mod(ll-1,N(1))+1
       j = mod((ll-i)/N(1),N(2))+1
       k = (ll-i-(j-1)*N(1))/(N(1)*N(2))+1

       if(j > 1) cycle
       if(i == 1 .and. ProcLoc(1) == 1) then
          do dir = 1, ND
             count = count + 1
             ! ... global index
             l1 = (L2G(l0)-1)*ND + dir
             BCIndTempDisp(count) = l1
          end do
       elseif(i == N(1) .and. ProcLoc(2) == 1) then
          do dir = 1, ND
             count = count + 1
             ! ... global index
             l1 = (L2G(l0)-1)*ND + dir
             BCIndTempDisp(count) = l1
          end do
       elseif(ND == 3) then
          if(k == 1 .and. ProcLoc(3) == 1) then
             do dir = 1, ND
                count = count + 1
                ! ... global index
                l1 = (L2G(l0)-1)*ND + dir
                BCIndTempDisp(count) = l1
             end do
          elseif(k == N(3) .and. ProcLoc(4) == 1) then
             do dir = 1, ND
                count = count + 1
                ! ... global index
                l1 = (L2G(l0)-1)*ND + dir

                BCIndTempDisp(count) = l1
             end do
          end if
       end if

    end do

    ! ... allocate space to store new BC data
    TMstate%DispBCNum = count
    allocate(TMstate%BCDispVal(count),TMstate%BCDispInd(count))
    
    ! ... copy the indices from the temporary array
    do l0 = 1, TMstate%DispBCNum
       TMstate%BCDispInd(l0) = BCIndTempDisp(l0)
       TMstate%BCDispVal(l0) = 0.0_rfreal
    end do
    
  end Subroutine AdjustBCs

end Module ModTMEOMBC
