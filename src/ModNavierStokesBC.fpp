! Copyright (c) 2015, University of Illinois at Urbana-Champaign, XPACC
! License: MIT, http://opensource.org/licenses/MIT
!----------------------------------------------------------------------
!
! ModNavierStokes.f90
! 
! - basic code for the evaluation of the Navier-Stokes equations
!
! Revision history
! - 19 Dec 2006 : DJB : initial code
! - 20 Dec 2006 : DJB : continued evolution
! - 20 Mar 2007 : DJB : initial working Poinsot-Lele BC on gen. meshes
! - 20 Apr 2007 : DJB : initial working Poinsot-Lele BC on moving meshes
! - 10 Jul 2007 : DJB : initial CVS submit
! - 11 Jul 2007 : DJB : initial work on ``multi-dimensional'' Poinsot-Lele
! - 01 Feb 2008 : DJB : merged in JKim's LES & 3D bc routines
! - 29 Jul 2008 : DJB : Initial SAT boundary conditions
! 
! flags == FALSE    : compute inviscid fluxes only
! flags == TRUE     : compute dissipative fluxes only
! flags == COMBINED : inviscid + dissipative fluxes
!
! $Header: /cvsroot/genx/Codes/RocfloCM/Source/ModNavierStokesBC.f90,v 1.23 2011/10/20 16:55:21 bodony Exp $
!
!-----------------------------------------------------------------------
MODULE ModNavierStokesBC

CONTAINS

  subroutine NS_BC_Fix_Value(region)

    USE ModGlobal
    USE ModDataStruct
    USE ModEOS
    USE ModDeriv
    USE ModMPI
    USE ModMatrixVectorOps
    USE ModPSAAP2

    Implicit None

    TYPE (t_region), POINTER :: region
    INTEGER :: ng
    INTEGER :: flags

    ! ... local variables
    type(t_patch), pointer :: patch
    type(t_grid), pointer :: grid
    type(t_mixt), pointer :: state
    type(t_mixt_input), pointer :: input
    type(t_combustion), pointer :: combustion
    real(rfreal), pointer :: UVWhat(:,:), gvec(:), var(:)
    integer :: ND, Nc, N(MAX_ND), npatch, i, j, l0, k, jj, l1, l2, isign, lp, kk
    real(rfreal) :: UU(MAX_ND), norm(MAX_ND), mag_norm, tang(MAX_ND-1,MAX_ND)
    real(rfreal) :: mag_tang, constant, del, eta, rel_pos(MAX_ND), dsign
    integer :: normDir, tangDir(MAX_ND,MAX_ND-1), Np(MAX_ND)
    real(rfreal) :: d(5), wave_amp(5), wave_spd(5), spd_snd
    real(rfreal) :: spvol, un, ut(MAX_ND-1)
    real(rfreal) :: norm_vec(MAX_ND), tang_vec(2,MAX_ND), fvec(MAX_ND,2)
    real(rfreal) :: metricInverse(MAX_ND,MAX_ND), invProj(MAX_ND, MAX_ND)
    real(rfreal) :: sponge_amp, bndry_q(MAX_ND+2), bndry_p(MAX_ND+2), bndry_rhs(MAX_ND+2)
    integer :: sgn, handedness(MAX_ND), alpha
    integer, pointer :: inorm(:)
    real(rfreal), pointer :: dudn(:), drdn(:), dpdn(:), flux(:), drudn(:)
    real(rfreal) :: bndry_norm(MAX_ND), p_inf, u_inf, du_inf, un_old, metric_fac, T_stag, T_inf, dT_inf
    real(rfreal) :: T_wall, p_wall, V_wall, invtempref
    real(rfreal) :: UVEL, VVEL, WVEL, CUVEL(5), CVVEL(5), CWVEL(5), PRESSURE, RHO
    real(rfreal) :: RREF, UREF, VREF, WREF, PREF, pert(5), charVar(5), GAMREF
    real(rfreal) :: UX, UY, UZ, XI_X, XI_Y, XI_Z, SPD_SND_INV, KE, ENTHALPY, MACH2, GAMMA, V_DOT_XI, e_internal
    real(rfreal) :: XI_X_TILDE, XI_Y_TILDE, XI_Z_TILDE, dsgn, denom, ui, Mi, Ti, T_inflow, u_inflow
    real(rfreal) :: w_of_t, t0, tempref, sndspdref2, e_internal_offset
    real(rfreal) :: radNozz, Tc, u0, r0, coefVel, P2, RHO2, dRHO, dPRESSURE, lhs_fac, rhs_fac, ChemEng
    type(t_spline), pointer :: spline

!!$    if (region%input%fv_grid(1) == FINITE_VOL) then
!!$       ! ... if FV, then call FV boundary condition subroutine
!!$       call FV_BC(region)       
!!$       return
!!$    end if

    tangDir(1,:) = (/2, 3/)
    tangDir(2,:) = (/1, 3/)
    tangDir(3,:) = (/1, 2/)
    handedness = (/1, -1, 1/)

    ! ... simplicity
    ND = region%input%ND

    allocate(gvec(size(region%input%bndry_explicit_deriv)))
    allocate(inorm(size(gvec)),var(size(gvec)))

    do npatch = 1, region%nPatches

      patch => region%patch(npatch)
      grid  => region%grid(patch%gridID)
      state => region%state(patch%gridID)
      input => grid%input
      combustion => state%combustion
      normDir = abs(patch%normDir)
      sgn = normDir / patch%normDir
      dsgn = dble(sgn)

      sndspdref2 = input%sndspdref * input%sndspdref
      invtempref = input%Cpref / sndspdref2
      tempref    = 1.0_8 / invtempref
      gamref     = input%GamRef

      ! ... CYCLE if we are sponge
      if ( patch%bcType == SPONGE ) CYCLE

      Nc = grid%nCells
      N  = 1; Np = 1;
      do j = 1, grid%ND
        N(j)  =  grid%ie(j) -  grid%is(j) + 1
        Np(j) = patch%ie(j) - patch%is(j) + 1
      end do;

      if ( patch%bcType /= SPONGE_WITH_LINEARIZED_DISTURBANCE ) then

        if (patch%bcType < 0) CYCLE ! ... BC for adjoint N-S neglected

        Do k = patch%is(3), patch%ie(3)
          Do j = patch%is(2), patch%ie(2)
            Do i = patch%is(1), patch%ie(1)

              l0 = (k- grid%is(3))* N(1)* N(2) + (j- grid%is(2))* N(1) + (i- grid%is(1)+1)
              lp = (k-patch%is(3))*Np(1)*Np(2) + (j-patch%is(2))*Np(1) + (i-patch%is(1)+1)

              if ( grid%iblank(l0) == 0 ) CYCLE

              ! ... normal indices
              do jj = 1, size(gvec)
                if (normDir == 1) then
                  l1 = l0 + sgn*(jj-1)
                else if (normDir == 2) then
                  l1 = l0 + sgn*(jj-1)*N(1)
                else
                  l1 = l0 + sgn*(jj-1)*N(1)*N(2)
                end if
                inorm(jj) = l1
              end do


              ! ... primitive variables
              GAMMA = state%gv(l0,1)
              GAMREF = input%GamRef
              RHO   = state%cv(l0,1)
              SPVOL = 1.0_rfreal / RHO
              UX    = SPVOL * state%cv(l0,2); UY = 0.0_rfreal; UZ = 0.0_rfreal;
              if (ND >= 2) UY = SPVOL * state%cv(l0,3)
              if (ND == 3) UZ = SPVOL * state%cv(l0,4)
              PRESSURE = state%dv(l0,1)

              SPD_SND = sqrt(GAMMA * PRESSURE * SPVOL)
              SPD_SND_INV = 1.0_rfreal / SPD_SND
              KE = 0.5_rfreal * (UX**2 + UY**2 + UZ**2)
              ENTHALPY = (state%cv(l0,ND+2) + PRESSURE) / RHO
              MACH2 = (2.0_rfreal * KE) * SPD_SND_INV**2

              ! ... construct the normalized metrics
              XI_X = grid%MT1(l0,region%global%t2Map(normDir,1)); XI_Y = 0.0_rfreal; XI_Z = 0.0_rfreal
              if (ND >= 2) XI_Y = grid%MT1(l0,region%global%t2Map(normDir,2))
              if (ND == 3) XI_Z = grid%MT1(l0,region%global%t2Map(normDir,3))

              ! ... multiply by the Jacobian
              XI_X = XI_X * grid%JAC(l0)
              XI_Y = XI_Y * grid%JAC(l0)
              XI_Z = XI_Z * grid%JAC(l0)
              denom = 1.0_rfreal / sqrt(XI_X**2+XI_Y**2+XI_Z**2)
              XI_X_TILDE = XI_X * denom
              XI_Y_TILDE = XI_Y * denom
              XI_Z_TILDE = XI_Z * denom

              ! ... the unit normal is (XI_X_TILDE, XI_Y_TILDE, XI_Z_TILDE)
              ! ... and points into the domain on + boundaries
              ! ... and points out of the domain on - boundaries
              V_DOT_XI = UX * XI_X_TILDE + UY * XI_Y_TILDE + UZ * XI_Z_TILDE

              if ( (patch%bcType == NSCBC_INFLOW_VELOCITY_TEMPERATURE_FROM_SPONGE) .OR. &
                   (patch%bcType == NSCBC_INFLOW_VELOCITY_TEMPERATURE) .OR. &
                   (patch%bcType == SAT_SLIP_ADIABATIC_WALL_COUPLED) .OR. &
                   (patch%bcType == NSCBC_INFLOW_VELOCITY_TEMPERATURE_FORCING_SLOT)) then ! ... JKim 06/2008

                if (MACH2 >= 1.0_rfreal) then

                  state%cv(l0,1:ND+2)  = state%cvTarget(l0,1:ND+2)

                else

                  ! ... assume density equation is good

                  ! ... update momentum
                  UX = patch%u_inflow(lp); UY = 0.0_rfreal; UZ = 0.0_rfreal
                  If (ND >= 2) UY = patch%v_inflow(lp)
                  If (ND == 3) UZ = patch%w_inflow(lp)
                  state%cv(l0,2) = RHO * UX
                  if (ND >= 2) state%cv(l0,3) = RHO * UY
                  if (ND == 3) state%cv(l0,4) = RHO * UZ

                  ! ... update the total energy
                  if (input%gas_dv_model == DV_MODEL_IDEALGAS) then
                    PRESSURE = (GAMREF-1.0_rfreal)/GAMREF * RHO * patch%T_inflow(lp)
                    state%cv(l0,ND+2) = PRESSURE / (GAMREF-1.0_rfreal) + 0.5_rfreal * RHO * (UX**2 + UY**2 + UZ**2)
                  else if (input%gas_dv_model == DV_MODEL_IDEALGAS_MIXTURE) then                            
                    state%cv(l0,ND+2) = (sum(state%auxVars(l0,:)*combustion%Mwcoeff) + combustion%MwcoeffRho*state%cv(l0,1))/gamref*patch%T_inflow(lp) + 0.5_rfreal * RHO * (UX**2 + UY**2 + UZ**2)
                  elseif (input%gas_dv_model == DV_MODEL_THERMALLY_PERFECT) then
                    spline => state%dvSpline(1)
                    e_internal = evaluateSpline(spline%nxb, spline%dxb, spline%xb, spline%coeffs, 3, patch%T_inflow(lp)*tempref)/sndspdref2
                    state%cv(l0,ND+2) = RHO * e_internal + 0.5_rfreal * RHO * (UX**2 + UY**2 + UZ**2)
                  else
                    Call graceful_exit(region%myrank, 'PlasComCM: ERROR: Unknown gas model in NS_BC_Fix_Value')
                  end if
                end if

              else if ( patch%bcType == NSCBC_SUBSONIC_INFLOW ) Then

                UX = state%cvTarget(l0,2) / state%cvTarget(l0,1); Uy = 0.0_rfreal; Uz = 0.0_rfreal
                if (ND >= 2) UY = state%cvTarget(l0,3) / state%cvTarget(l0,1)
                if (ND == 3) UZ = state%cvTarget(l0,4) / state%cvTarget(l0,1)

                ! ... fix the velocity
                state%cv(l0,2) = RHO * UX
                if (ND >= 2) state%cv(l0,3) = RHO * UY
                if (ND == 3) state%cv(l0,4) = RHO * UZ

                ! ... update the total energy
                state%cv(l0,ND+2) = state%cv(l0,ND+2) + 0.5_rfreal * RHO * (UX**2 - (state%cv(l0,2)/state%cv(l0,1))**2)
                if (ND >= 2) state%cv(l0,ND+2) = state%cv(l0,ND+2) + 0.5_rfreal * RHO * (UY**2 - (state%cv(l0,3)/state%cv(l0,1))**2)
                if (ND == 3) state%cv(l0,ND+2) = state%cv(l0,ND+2) + 0.5_rfreal * RHO * (UZ**2 - (state%cv(l0,4)/state%cv(l0,1))**2)

              else if ( patch%bcType == NSCBC_WALL_ADIABATIC_SLIP ) then

                ! ... compute the normal and tangential velocity vectors
                if (ND == 2) then ! JKim 11/2007
                  un    = XI_X_TILDE * UX + XI_Y_TILDE * UY
                  ut(1) =-XI_Y_TILDE * UX + XI_X_TILDE * UY
                else if (ND == 3) then ! JKim 11/2007
                  norm_vec(:) = (/XI_X_TILDE, XI_Y_TILDE, XI_Z_TILDE/) ! boundary-normal vector in the
                  ! direction of increasing xi or
                  ! eta or zeta.
                  tang_vec(1,:) = (/grid%MT1(l0,region%global%t2Map(tangDir(normDir,1),1)), & ! pick up another direction
                       grid%MT1(l0,region%global%t2Map(tangDir(normDir,1),2)), & ! in computational domain
                       grid%MT1(l0,region%global%t2Map(tangDir(normDir,1),3))/)  ! to get boundary-tangential vector
                  tang_vec(1,:) = tang_vec(1,:) * handedness(normDir) ! adjust handed-ness for right-handed-ness
                  tang_vec(1,:) = tang_vec(1,:) * grid%JAC(l0) ! multiply by the Jacobian
                  denom = 1.0_rfreal / sqrt(dot_product(tang_vec(1,:), tang_vec(1,:)))
                  tang_vec(1,:) = tang_vec(1,:) * denom ! normal vector for another direction, say,
                  ! (eta_x_tilde, eta_y_tilde, eta_z_tilde)
                  call cross_product(norm_vec, tang_vec(1,:), tang_vec(1,:))
                  tang_vec(1,:) = tang_vec(1,:) / sqrt(dot_product(tang_vec(1,:), tang_vec(1,:))) ! now it's done ^^

                  call cross_product(norm_vec, tang_vec(1,:), tang_vec(2,:))
                  tang_vec(2,:) = tang_vec(2,:) / sqrt(dot_product(tang_vec(2,:), tang_vec(2,:))) ! now it's done ^^

                  un    = dot_product((/UX, UY, UZ/), norm_vec(:)  )
                  ut(1) = dot_product((/UX, UY, UZ/), tang_vec(1,:))
                  ut(2) = dot_product((/UX, UY, UZ/), tang_vec(2,:))
                else
                  call graceful_exit(region%myrank, 'No adiabatic slip wall for ND /= (2,3).  Stopping...')
                end if ! ND

                ! ... update the wall-normal velocity
                un = XI_X_TILDE * grid%XYZ_TAU(l0,1)
                if (ND >= 2) un = un + XI_Y_TILDE * grid%XYZ_TAU(l0,2)
                if (ND == 3) un = un + XI_Z_TILDE * grid%XYZ_TAU(l0,3)

                ! ... return to Cartesian velocities
                if (ND == 2) then ! JKim 11/2007
                  UX = XI_X_TILDE * un - XI_Y_TILDE * ut(1)
                  UY = XI_Y_TILDE * un + XI_X_TILDE * ut(1)
                else if (ND == 3) then ! JKim 11/2007
                  UX = dot_product((/un, ut(1), ut(2)/), &
                       (/norm_vec(1), tang_vec(1,1), tang_vec(2,1)/))
                  UY = dot_product((/un, ut(1), ut(2)/), &
                       (/norm_vec(2), tang_vec(1,2), tang_vec(2,2)/))
                  UZ = dot_product((/un, ut(1), ut(2)/), &
                       (/norm_vec(3), tang_vec(1,3), tang_vec(2,3)/))
                end if ! ND

!                ! ... update Cartesian velocities
!                state%cv(l0,2) = state%cv(l0,1) * UX
!                if (ND >= 2) state%cv(l0,3) = state%cv(l0,1) * UY
!                if (ND == 3) state%cv(l0,4) = state%cv(l0,1) * UZ

                ! ... update the total energy
                state%cv(l0,ND+2) = state%cv(l0,ND+2) + 0.5_rfreal * RHO * (UX**2 - (state%cv(l0,2)/state%cv(l0,1))**2)
                if (ND >= 2) state%cv(l0,ND+2) = state%cv(l0,ND+2) + 0.5_rfreal * RHO * (UY**2 - (state%cv(l0,3)/state%cv(l0,1))**2)
                if (ND == 3) state%cv(l0,ND+2) = state%cv(l0,ND+2) + 0.5_rfreal * RHO * (UZ**2 - (state%cv(l0,4)/state%cv(l0,1))**2)

                ! ... update Cartesian velocities
                state%cv(l0,2) = state%cv(l0,1) * UX
                if (ND >= 2) state%cv(l0,3) = state%cv(l0,1) * UY
                if (ND == 3) state%cv(l0,4) = state%cv(l0,1) * UZ

              else if ( patch%bcType == NSCBC_WALL_ISOTHERMAL_NOSLIP .OR. & 
                   patch%bcType == NSCBC_WALL_ISOTHERMAL_NOSLIP_COUPLED ) then

                ! ... momentum
                state%cv(l0,2:ND+1) = grid%XYZ_TAU(l0,1:ND) * state%cv(l0,1)

                ! ... update the total energy
                T_wall = input%bcic_WallTemp * invtempref

                ! ... thermally perfect or not
                if (input%gas_dv_model == DV_MODEL_IDEALGAS) then

                  p_wall = (gamref - 1.0_rfreal) / gamref * state%cv(l0,1) * T_wall
                  state%cv(l0,ND+2) = p_wall / (gamref - 1.0_rfreal) + 0.5_rfreal * &
                       spvol * SUM(state%cv(l0,2:ND+1)**2)

                elseif (input%gas_dv_model == DV_MODEL_IDEALGAS_MIXTURE) then

                  state%cv(l0,ND+2) = (sum(state%auxVars(l0,:)*combustion%Mwcoeff) + combustion%MwcoeffRho*state%cv(l0,1))/gamref*T_wall + 0.5_rfreal * &
                       spvol * SUM(state%cv(l0,2:ND+1)**2)

                elseif (input%gas_dv_model == DV_MODEL_THERMALLY_PERFECT) then

                  p_wall = (gamref - 1.0_rfreal) / gamref * state%cv(l0,1) * T_wall
                  ! ... TPGasLookup(input,varout,valin,valout,gamma)
! call TPGasLookup(input,1,T_wall,e_internal,gamma)
                  spline => state%dvSpline(1)
                  e_internal = evaluateSpline(spline%nxb, spline%dxb, spline%xb, spline%coeffs, 3, T_wall*tempref)/sndspdref2
                  state%cv(l0,ND+2) = state%cv(l0,1) * e_internal + 0.5_rfreal * spvol * SUM(state%cv(l0,2:ND+1)**2)
                else
                  call graceful_exit(region%myrank, 'PlasComCM: ERROR: Unknown gas model in NS_BC_FIX_VALUE.')
                end if

                ! ... zero out the rhs for momentum
!!$                state%rhs(l0,2:ND+1) = 0.0_rfreal

                ! ... set the rhs for the total energy
!!$                state%rhs(l0,ND+2) = T_wall * state%rhs(l0,1) / state%gv(l0,1)

              else if (patch%bcType == NSCBC_WALL_NOSLIP_THERMALLY_COUPLED) then

                ! ... momentum
                state%cv(l0,2:ND+1) = grid%XYZ_TAU(l0,1:ND) * state%cv(l0,1)

                ! ... update the total energy
                ! ... The variable CellTemp is already non-dimensionalized in ModRocstar
                ! ... subroutine SetInterfaceTemp
                T_wall = patch%CellTemp(lp)  
                p_wall = (gamref - 1.0_rfreal) / gamref * RHO * T_wall
                if(input%gas_dv_model == DV_MODEL_IDEALGAS) then
                  state%cv(l0,ND+2) = p_wall / (GAMMA - 1.0_rfreal) + 0.5_rfreal * RHO * (UX**2 + UY**2 + UZ**2)
                elseif (input%gas_dv_model == DV_MODEL_IDEALGAS_MIXTURE) then                          
                  state%cv(l0,ND+2) = (sum(state%auxVars(l0,:)*combustion%Mwcoeff) + combustion%MwcoeffRho*state%cv(l0,1))/GAMMA*T_wall + 0.5_rfreal * RHO * (UX**2 + UY**2 + UZ**2)
                elseif(input%gas_dv_model == DV_MODEL_THERMALLY_PERFECT) then
                  spline => state%dvSpline(1)
                  e_internal = evaluateSpline(spline%nxb, spline%dxb, spline%xb, spline%coeffs, 3, T_wall*tempref)/sndspdref2
                  state%cv(l0,ND+2) = e_internal * RHO + 0.5_rfreal * RHO * (UX**2 + UY**2 + UZ**2)
                end if

              else if (patch%bcType == STRUCTURAL_INTERACTING) then

                ! ... momentum
                state%cv(l0,2:ND+1) = grid%XYZ_TAU(l0,1:ND) * state%cv(l0,1)

                ! ... update the total energy
                ! ... The variable CellTemp is already non-dimensionalized in ModRocstar
                ! ... subroutine SetInterfaceTemp
                T_wall = patch%CellTemp(lp) + patch%dCellTemp(lp)

                p_wall = (gamref - 1.0_rfreal) / gamref * RHO * T_wall
                
                if(input%gas_dv_model == DV_MODEL_IDEALGAS) then
                  state%cv(l0,ND+2) = p_wall / (GAMMA - 1.0_rfreal) + 0.5_rfreal * RHO * (UX**2 + UY**2 + UZ**2)
                elseif (input%gas_dv_model == DV_MODEL_IDEALGAS_MIXTURE) then
                  state%cv(l0,ND+2) = (sum(state%auxVars(l0,:)*combustion%Mwcoeff) + combustion%MwcoeffRho*state%cv(l0,1))/gamref*T_wall + 0.5_rfreal * &
                       RHO * (UX**2 + UY**2 + UZ**2)
                elseif(input%gas_dv_model == DV_MODEL_THERMALLY_PERFECT) then
                  spline => state%dvSpline(1)
                  e_internal = evaluateSpline(spline%nxb, spline%dxb, spline%xb, spline%coeffs, 3, T_wall*tempref)/sndspdref2
                  state%cv(l0,ND+2) = e_internal * RHO + 0.5_rfreal * RHO * (UX**2 + UY**2 + UZ**2)
               end if


              else if ( patch%bcType == NSCBC_SUPERSONIC_INFLOW ) then

                ! ... specify everything
                state%cv(l0,:)  = state%cvTarget(l0,:)

              else if ( patch%bcType == FV_DIRICHLET ) then

                ! ... specify everything
                state%cv(l0,:)  = state%cvTarget(l0,:)

              else if ( patch%bcType == SAT_FAR_FIELD) then
                if (V_DOT_XI*dsgn >= 0.0_8) then
                  do jj = 1, input%nAuxVars
                    state%auxVars(l0,jj) = state%auxVarsTarget(l0,jj)
                  end do
                end if

              else if ( patch%bcType == SAT_AXISYMMETRIC) then
               state%cv(l0,normDir+1)  = 0d0
               state%rhs(l0,normDir+1)  = 0d0

              end if

            end do
          end do
        end do

      end if

    end do

    deallocate(gvec,inorm,var)

  end subroutine NS_BC_Fix_Value

  subroutine surface_grid(region)
    !Subroutine for interpolating pressure on the nodes to cell center
    !USEd for communiaction with rocstar
    !Written by Mahesh
    !The plate dimension have to be called 
    !Maybe a few of the variables are redundant
    USE ModGlobal
    USE ModDataStruct
    USE ModDeriv
    USE ModMPI

    Implicit None

    TYPE (t_region), POINTER :: region
    INTEGER :: ng
    INTEGER :: flags

    ! ... local variables
    type(t_patch), pointer :: patch
    type(t_grid), pointer :: grid
    type(t_mixt), pointer :: state
    type(t_mixt_input), pointer :: input
    integer :: ND, Nc, N(MAX_ND), npatch, i, j, l0, k, jj, l1, l2, isign, lp
    integer :: Np(MAX_ND)
    integer :: refindex
    real(rfreal) :: spvol
    real(rfreal) :: RHO 
    real(rfreal) :: PRESSURE11,PRESSURE12,PRESSURE21,PRESSURE22
    real(rfreal) :: x11,x12,x21,x22
    real(rfreal) :: y11,y12,y21,y22
    real(rfreal) :: z11,z12,z21,z22
    real(rfreal) :: x_cellcenter,y_cellcenter
    real(rfreal) :: factor1,factor2,factor3,factor4
    real(rfreal) :: PRESSURE_CELL_CENTER
    real(rfreal) :: UX, UY, UZ, GAMMA
    real(rfreal) :: x_platec,y_platec,plate_ht,plate_len
    integer :: interrvalue,unit, point_count

    ! ... simplicity
    ND = region%input%ND
    unit=21
!    OPEN (UNIT=unit, FILE='PatchCoords.dat' , STATUS='NEW', ACTION='WRITE',IOSTAT=interrvalue)
    do npatch = 1, region%nPatches
       
       patch => region%patch(npatch)
       grid  => region%grid(patch%gridID)
       state => region%state(patch%gridID)
       input => grid%input

       if ( patch%bcType == NSCBC_WALL_ADIABATIC_SLIP_COUPLED    .OR. &
            patch%bcType == NSCBC_WALL_ISOTHERMAL_NOSLIP_COUPLED .OR. &
            patch%bctype == SAT_WALL_NOSLIP_THERMALLY_COUPLED   .OR. &
            patch%bcType == SAT_SLIP_ADIABATIC_WALL_COUPLED      .OR. &
            patch%bcType == NSCBC_WALL_NOSLIP_THERMALLY_COUPLED) then
          Nc = grid%nCells
          N  = 1; Np = 1;
          do j = 1, grid%ND
             N(j)  = grid%ie(j) - grid%is(j)+1
             Np(j) = patch%ie(j) - patch%is(j) + 1
          end do
          
          point_count = 1
          patch%patch_xyz(:) = 0.0_8
          Do k = patch%is(3), patch%ie(3)
             Do j = patch%is(2), patch%ie(2)
                Do i = patch%is(1), patch%ie(1)
                   
                   l0 = (k-grid%is(3))*N(1)*N(2) + (j-grid%is(2))*N(1) + i-grid%is(1)+1
                   lp = (k-patch%is(3))*Np(1)*Np(2) &
                        + (j-patch%is(2))*Np(1) + (i-patch%is(1)+1)
                   
                   patch%patch_xyz(point_count)   = grid%XYZ(l0,1)
                   patch%patch_xyz(point_count+1) = grid%XYZ(l0,2)
                   patch%patch_xyz(point_count+2) = grid%XYZ(l0,3)
                   point_count = point_count+3

                end do
             end do
          end do
          
       endif
    end do
  end subroutine surface_grid

  !
  ! Extended, ``multi-dimensional'' boundary conditions in the
  ! style of Thompson and Poinsot & Lele.  The method of Kim & Lee
  ! (AIAA J., vol. 42(1), 2004) is extended to handle deforming
  ! grids.
  !
  ! Also includes SAT boundary conditions of Kreiss, Nordstrom, Carpenter, ...
  !
  subroutine NS_BC(region, ng, flags)

    USE ModGlobal
    USE ModDataStruct
    USE ModEOS
    USE ModPSAAP2
    USE ModDeriv
    USE ModMPI
    USE ModModel1

    Implicit None

    TYPE (t_region), POINTER :: region
    INTEGER :: ng
    INTEGER :: flags

    ! ... local variables
    type(t_patch), pointer :: patch
    type(t_grid), pointer :: grid
    type(t_mixt), pointer :: state
    type(t_mixt_input), pointer :: input
    type(t_combustion), pointer :: combustion
    real(rfreal), pointer :: gvec(:), SC(:), Pinv(:,:), Pfor(:,:), Tmat(:,:), Tinv(:,:), cvPredictor(:), rhsPredictor(:)
    real(rfreal), pointer :: cons_inviscid_flux(:,:), L_vector(:), new_normal_flux(:), ViscFluxJac(:,:)
    integer :: ND, Nc, N(MAX_ND), npatch, i, j, l0, k, ii, jj, l1, l2, isign, lp, kk
    real(rfreal) :: UU(MAX_ND), norm(MAX_ND), mag_norm, tang(MAX_ND-1,MAX_ND)
    real(rfreal) :: mag_tang, constant, del, eta, rel_pos(MAX_ND), dsign, denom
    integer :: normDir, tangDir(MAX_ND,MAX_ND-1), Np(MAX_ND), ijkInd(MAX_ND)
    real(rfreal) :: d(5), wave_amp(5), wave_spd(5), spd_snd
    real(rfreal) :: un, ut(MAX_ND-1)
    real(rfreal) :: norm_vec(MAX_ND), tang_vec(2,MAX_ND), fvec(MAX_ND,2)
    real(rfreal) :: metricInverse(MAX_ND,MAX_ND), invProj(MAX_ND, MAX_ND)
    real(rfreal) :: sponge_amp, bndry_q(MAX_ND+2), bndry_p(MAX_ND+2), bndry_rhs(MAX_ND+2)
    integer :: sgn, handedness(MAX_ND)
    integer, pointer :: inorm(:)
    real(rfreal) :: dundn, drdn, dpdn, drundn, dut1dn, dut2dn, sigma, SpecRad, KrnkrDlta
    real(rfreal) :: bndry_norm(MAX_ND), p_inf, u_inf, du_inf, un_old, metric_fac, T_stag, T_inf, dT_inf, theta, P_stag
    real(rfreal) :: charVar(MAX_ND+2), primVar(MAX_ND+2), dsgn
    real(rfreal) :: V_wall, V_wall_vec(MAX_ND), V_wall_target, V_wall_target_vec(MAX_ND), A_wall, drhodn, dp_inf, T_wall
    real(rfreal) :: Uhat_alpha(MAX_ND), alpha_x(MAX_ND,MAX_ND), alpha_t(MAX_ND)
    real(rfreal) :: x_alpha(MAX_ND,MAX_ND), primVar_RHS(MAX_ND+2), M2(2,2), M3(3,3)
    real(rfreal) :: SPVOL, RHO, UX, UY, UZ, PRESSURE, ENERGY, XI_X, XI_Y, XI_Z, SPD_SND_INV, KE, ENTHALPY, MACH2, GAMMA, V_DOT_XI, MU, LAMBDAvisc
    real(rfreal) :: XI_X_TILDE, XI_Y_TILDE, XI_Z_TILDE, epsilon, omega, dUdt_inflow, dTdt_inflow, Mi, Ui, Ti, u_inflow, T_inflow
    real(rfreal) :: w_of_t, dwdt, t0, wi, w_inflow, v_inflow, dVdt_inflow, vi, dWdt_inflow
    real(rfreal), pointer :: uB(:), gI1(:), gI2(:), Aprime(:,:), Lambda(:,:), matX(:,:), matXTrans(:,:), penalty(:), gI1w(:)
    real(rfreal) :: radNozz, Tc, u0, r0, coefVel, sigmaI1, sigmaI2, sigma_inter, e_internal
    real(rfreal) :: ubAux, sigmaAux,Sfactor  !LM 09/14
    real(rfreal) :: junk, ibfac_local ! JKim 11/2007
    real(rfreal) :: RHO_TARGET, UX_TARGET, UY_TARGET, UZ_TARGET, PRESSURE_TARGET, U_wall, UVWhat(MAX_ND), UVW(MAX_ND)
    real(rfreal) :: alpha, beta, phi2, ucon, PRESSURE2, bndry_h, gamm1i, T, RHO_P, RHO_T, sndspdref2, tempref, invtempref
    real(rfreal), pointer :: pnorm_vec(:), Lambda_in(:,:), Lambda_out(:,:), BI1(:,:)
    real(rfreal) :: p_in, p_out, sig_v, sigR(MAX_ND+2), sigL(MAX_ND+2), tmp(MAX_ND+2), tmp2(MAX_ND+2), sigma_scale
    real(rfreal) :: ChemEng, GAMREF, e_internal_offset, XI_T, Tinflow, MwcoeffRho
    type(t_spline), pointer :: spline

    ! ... declarations for pointer dereference, WZhang 05/2014
    integer, pointer :: fv_grid(:), grid_is(:), grid_ie(:), patch_is(:), patch_ie(:), iblank(:), t2Map(:,:)
    real(rfreal), pointer :: bndry_explicit_deriv(:), gv(:,:), cv(:,:), dv(:,:), tv(:,:), MT1(:,:), XI_TAU(:,:), JAC(:)
    real(rfreal), pointer :: rhs(:,:), patch_deriv(:,:,:), XI_TAU_XI(:,:), patch_MT2(:,:), XYZ_TAU2(:,:)
    real(rfreal), pointer :: patch_dvdt_inflow(:), patch_dudt_inflow(:), patch_dTdt_inflow(:), INVJAC(:), cvTarget(:,:)
    real(rfreal), pointer :: SBP_invPdiag_block1(:), rhs_auxVars(:,:), patch_implicit_sat_mat(:,:,:), patch_ViscousFlux(:,:,:), patch_ViscousFluxAux(:,:,:), patch_ViscousRHSAux(:,:), Mwcoeff(:)
    real(rfreal), pointer :: patch_implicit_sat_mat_old(:,:,:), patch_cv_out(:,:), patch_ViscousFlux_out(:,:,:), XYZ_TAU(:,:)
    real(rfreal), pointer :: patch_CellTemp(:), spline_xb(:), spline_coeffs(:,:,:), patch_sat_fac(:), auxVars(:,:)
    real(rfreal), pointer :: patch_sponge_xs(:,:), patch_sponge_xe(:,:), XYZ(:,:), auxVarsTarget(:,:), ibfac(:)
    integer :: nPatches, patch_gridID, patch_bcType, patch_normDir, gas_dv_model, nAuxVars, timeScheme, spline_nxb, input_sponge_pow
    real(rfreal) :: REinv, SAT_sigmaI1_FF, region_gamref, PRinv, SAT_sigmaI2_FF, SAT_sigmaI1, bcic_WallTemp, spline_dxb, input_tempref
    real(rfreal) :: SAT_sigmaI2, Pr, input_sponge_amp, BCIC_WALL_VEL_VEC(MAX_ND)

    ! ...detailed kinetics
    real(rfreal) :: Qaux(m_nspecies+m_steady+1), Waux(m_nspecies+m_steady+1)
#ifdef HAVE_CANTERA
    real(rfreal) :: WauxM(m_nspecies+m_steady+1),WauxR(m_nspecies+m_steady+1), Qv(MAX_ND+2), Qvs(MAX_ND+2),  Wv(MAX_ND+2), Wvs(MAX_ND+2)
#endif

    ! ... pointer dereference, WZhang 05/2014
    fv_grid => region%input%fv_grid
    bndry_explicit_deriv => region%input%bndry_explicit_deriv
    t2Map => region%global%t2Map
    nPatches = region%nPatches

    if (fv_grid(1) == FINITE_VOL) then
       ! ... if FV, then only 1 BC subroutine
       ! ... to be called when NS_BC_Fix_Value is called
       return
    end if

    tangDir(1,:) = (/2, 3/)
    tangDir(2,:) = (/1, 3/)
    tangDir(3,:) = (/1, 2/)
    handedness   = (/1, -1, 1/)

    ! ... simplicity
    ND = region%input%ND

    allocate(gvec(size(bndry_explicit_deriv)))
    allocate(inorm(size(gvec)))
    allocate(SC(ND+2),Pinv(ND+2,ND+2),Pfor(ND+2,ND+2),Tmat(ND+2,ND+2),Tinv(ND+2,ND+2))
    allocate(cons_inviscid_flux(ND,ND+2),L_vector(ND+2),new_normal_flux(ND+2), pnorm_vec(ND))
    allocate(uB(ND+2), gI1(ND+2), gI2(ND+2), Aprime(ND+2,ND+2), Lambda(ND+2,ND+2), matX(ND+2,ND+2), matXTrans(ND+2,ND+2), penalty(ND+2))
    allocate(Lambda_in(ND+2,ND+2), Lambda_out(ND+2,ND+2),ViscFluxJac(ND+2,ND+2),BI1(ND+2,ND+2))

    Qaux = 0d0; Waux = 0d0

    do npatch = 1, nPatches

      patch => region%patch(npatch)
      grid  => region%grid(patch%gridID)
      state => region%state(patch%gridID)
      input => grid%input
      combustion => state%combustion
      normDir = abs(patch%normDir)
      sgn = normDir / patch%normDir
      dsgn = dble(sgn)
      sndspdref2 = input%sndspdref * input%sndspdref
      invtempref = input%Cpref / sndspdref2
      tempref    = 1.0_8 / invtempref 
      gamref = input%GamRef

      ! ... pointer dereference, WZhang 05/2014
      ND                          = grid%ND
      grid_is                    => grid%is
      grid_ie                    => grid%ie
      patch_is                   => patch%is
      patch_ie                   => patch%ie
      patch_gridID                = patch%gridID
      patch_bcType                = patch%bcType
      iblank                     => grid%iblank
      gv                         => state%gv
      cv                         => state%cv
      dv                         => state%dv
      tv                         => state%tv
      REinv                       = state%REinv
      MT1                        => grid%MT1
      XI_TAU                     => grid%XI_TAU
      JAC                        => grid%JAC
      ibfac                      => grid%ibfac
      rhs                        => state%rhs
      patch_deriv                => patch%deriv
      XI_TAU_XI                  => grid%XI_TAU_XI
      patch_MT2                  => patch%MT2
      XYZ_TAU2                   => grid%XYZ_TAU2
      patch_normDir               = patch%normDir
      patch_dvdt_inflow          => patch%dvdt_inflow
      patch_dudt_inflow          => patch%dudt_inflow
      patch_dTdt_inflow          => patch%dTdt_inflow
      INVJAC                     => grid%INVJAC
      cvTarget                   => state%cvTarget
      SAT_sigmaI1_FF              = input%SAT_sigmaI1_FF
      gas_dv_model                = input%gas_dv_model
      region_gamref               = region%input%GamRef
      SBP_invPdiag_block1        => grid%SBP_invPdiag_block1
      nAuxVars                    = input%nAuxVars
      rhs_auxVars                => state%rhs_auxVars
      timeScheme                  = input%timeScheme
      patch_implicit_sat_mat     => patch%implicit_sat_mat
      PRinv                       = state%PRinv
      SAT_sigmaI2_FF              = input%SAT_sigmaI2_FF
      patch_ViscousFlux          => patch%ViscousFlux
      patch_ViscousFluxAux       => patch%ViscousFluxAux
      patch_ViscousRHSAux       => patch%ViscousRHSAux
      patch_implicit_sat_mat_old => patch%implicit_sat_mat_old
      patch_cv_out               => patch%cv_out
      patch_ViscousFlux_out      => patch%ViscousFlux_out
      SAT_sigmaI1                 = input%SAT_sigmaI1
      XYZ_TAU                    => grid%XYZ_TAU
      patch_CellTemp             => patch%CellTemp
      bcic_WallTemp               = input%bcic_WallTemp
      BCIC_WALL_VEL_VEC           = (/ input%BCIC_WALL_VEL_X, &
                                       input%BCIC_WALL_VEL_Y, &
                                       input%BCIC_WALL_VEL_Z /)
      bcic_WallTemp               = input%bcic_WallTemp
      tempref                     = input%TempRef
      SAT_sigmaI2                 = input%SAT_sigmaI2
      Pr                          = state%Pr
      patch_sat_fac              => patch%sat_fac
      auxVars                    => state%auxVars
      patch_sponge_xs            => patch%sponge_xs
      patch_sponge_xe            => patch%sponge_xe
      XYZ                        => grid%XYZ
      input_sponge_amp            = input%sponge_amp
      input_sponge_pow            = input%sponge_pow
      auxVarsTarget              => state%auxVarsTarget
      MwcoeffRho                 = combustion%MwcoeffRho
      Mwcoeff                    => combustion%Mwcoeff

      Nc = grid%nCells
      N  = 1; Np = 1;
      do j = 1, ND
        N(j)  =  grid_ie(j) -  grid_is(j) + 1
        Np(j) = patch_ie(j) - patch_is(j) + 1
      end do

      if (patch_gridID /= ng) CYCLE ! ... apply BC only in this grid

      if ( patch_bcType /= SPONGE .and. patch_bcType /= SPONGE_WITH_LINEARIZED_DISTURBANCE ) then

        if (patch_bcType < 0) CYCLE ! ... BC for adjoint N-S neglected

        Do k = patch_is(3), patch_ie(3)
          Do j = patch_is(2), patch_ie(2)
            Do i = patch_is(1), patch_ie(1)

              l0 = (k- grid_is(3))* N(1)* N(2) + (j- grid_is(2))* N(1) +  i- grid_is(1)+1
              lp = (k-patch_is(3))*Np(1)*Np(2) + (j-patch_is(2))*Np(1) + (i-patch_is(1)+1)

              if ( iblank(l0) == 0 ) CYCLE

              ! ... iblank factor (either 1 or zero)
              ibfac_local = ibfac(l0)

              ! ... special treatment for MODEL1
              if (input%chemistry_model == MODEL1) then
#ifdef HAVE_CANTERA
                 Qv(1:ND+2) = cv(l0,1:ND+2)
                 Qaux(1:m_nspecies) = auxVars(l0,1:m_nspecies)
                 if(m_steady > 0) Qaux(combustion%model1%isteadyB:combustion%model1%isteadyE) = state%auxVarsSteady(l0,1:m_steady)
                 Qaux(combustion%model1%iN2) = Qv(1)
                 Qvs(1:ND+2) = removeBondEnergy(Qv(1:ND+2),Qaux, combustion%model1, input)
                 Wv(1:ND+2) = cvTarget(l0,1:ND+2)
                 Waux(1:m_nspecies) = auxVarsTarget(l0,1:m_nspecies)
                 if(m_steady > 0) Waux(combustion%model1%isteadyB:combustion%model1%isteadyE) = 0d0 !cold flow
                 Waux(combustion%model1%iN2) = Wv(1)
                 Wvs(1:ND+2) = removeBondEnergy(Wv(1:ND+2),Waux, combustion%model1, input)
                 WauxM = Waux*Qv(1)/Wv(1)
                 WauxR = Qaux*Wv(1)/Qv(1)
                 ENERGY = Qvs(ND+2)
#endif
              else
                 ENERGY = cv(l0,ND+2)
              end if

              ! ... primitive variables
              GAMMA = gv(l0,1)
              RHO   = cv(l0,1)
              SPVOL = 1.0_rfreal / RHO
              UX  = SPVOL * cv(l0,2); UY = 0.0_rfreal; UZ = 0.0_rfreal;
              if (ND >= 2) UY = SPVOL * cv(l0,3)
              if (ND == 3) UZ = SPVOL * cv(l0,4)
              PRESSURE = dv(l0,1)
              SPD_SND = sqrt(GAMMA * PRESSURE * SPVOL)
              SPD_SND_INV = 1.0_rfreal / SPD_SND
              KE = 0.5_rfreal * (UX**2 + UY**2 + UZ**2)
              ENTHALPY = (ENERGY + PRESSURE) / RHO
              MACH2 = (2.0_rfreal * KE) * SPD_SND_INV**2
              MU = tv(l0,1) * REinv
              LAMBDAvisc = tv(l0,2) * REinv

              ! ... construct the normalized metrics
              XI_X = MT1(l0,t2Map(normDir,1)); XI_Y = 0.0_rfreal; XI_Z = 0.0_rfreal
              if (ND >= 2) XI_Y = MT1(l0,t2Map(normDir,2))
              if (ND == 3) XI_Z = MT1(l0,t2Map(normDir,3))
              XI_T = XI_TAU(l0,normDir)

              ! ... multiply by the Jacobian
              XI_X = XI_X * JAC(l0)
              XI_Y = XI_Y * JAC(l0)
              XI_Z = XI_Z * JAC(l0)
              XI_T = XI_T * JAC(l0)

              denom = 1.0_rfreal / sqrt(XI_X**2+XI_Y**2+XI_Z**2)
              XI_X_TILDE = XI_X * denom
              XI_Y_TILDE = XI_Y * denom
              XI_Z_TILDE = XI_Z * denom
              bndry_h = denom

              ! ... wall-normal velocity
              V_DOT_XI = UX * XI_X_TILDE + UY * XI_Y_TILDE + UZ * XI_Z_TILDE

              ! ... construct the forward and backward transformation matrices
              ! ... these matrices are used for both NSCBC and SAT
              do ii = 1, ND+2
                do jj = 1, ND+2
                  Pinv(ii,jj) = 0.0_rfreal; 
                  Pfor(ii,jj) = 0.0_rfreal; 
                end do
              end do

              if (ND == 2) then

                ! ... the forward matrix P
                Pfor(1,1) = 1.0_rfreal
                Pfor(1,2) = 0.0_rfreal
                Pfor(1,3) = 0.5_rfreal * RHO * SPD_SND_INV
                Pfor(1,4) = Pfor(1,3)
                Pfor(2,1) = UX
                Pfor(2,2) = RHO * XI_Y_TILDE
                Pfor(2,3) = Pfor(1,3) * (UX + SPD_SND * XI_X_TILDE)
                Pfor(2,4) = Pfor(1,3) * (UX - SPD_SND * XI_X_TILDE)
                Pfor(3,1) = UY
                Pfor(3,2) = -RHO * XI_X_TILDE
                Pfor(3,3) = Pfor(1,3) * (UY + SPD_SND * XI_Y_TILDE)
                Pfor(3,4) = Pfor(1,3) * (UY - SPD_SND * XI_Y_TILDE)
                Pfor(4,1) = KE
                Pfor(4,2) = RHO * (UX * XI_Y_TILDE - UY * XI_X_TILDE)
                Pfor(4,3) = Pfor(1,3) * (ENTHALPY + SPD_SND * V_DOT_XI)
                Pfor(4,4) = Pfor(1,3) * (ENTHALPY - SPD_SND * V_DOT_XI)

                ! ... the inverse matrix Pinv
                Pinv(1,1) = 1.0 - 0.5_rfreal * (GAMMA-1.0_rfreal) * MACH2
                Pinv(1,2) =  (GAMMA-1.0_rfreal) * UX * SPD_SND_INV**2
                Pinv(1,3) =  (GAMMA-1.0_rfreal) * UY * SPD_SND_INV**2
                Pinv(1,4) = -(GAMMA-1.0_rfreal) * SPD_SND_INV**2
                Pinv(2,1) =  SPVOL * (UY * XI_X_TILDE - UX * XI_Y_TILDE)
                Pinv(2,2) =  SPVOL * XI_Y_TILDE
                Pinv(2,3) = -SPVOL * XI_X_TILDE
                Pinv(3,1) = SPD_SND * SPVOL * (0.5_rfreal * (GAMMA-1.0_rfreal) * MACH2 - V_DOT_XI * SPD_SND_INV)
                Pinv(3,2) = SPVOL * (XI_X_TILDE - (GAMMA-1.0_rfreal) * UX * SPD_SND_INV)
                Pinv(3,3) = SPVOL * (XI_Y_TILDE - (GAMMA-1.0_rfreal) * UY * SPD_SND_INV)
                Pinv(3,4) = (GAMMA-1.0_rfreal) * SPVOL * SPD_SND_INV
                Pinv(4,1) = SPD_SND * SPVOL * (0.5_rfreal * (GAMMA-1.0_rfreal) * MACH2 + V_DOT_XI * SPD_SND_INV)
                Pinv(4,2) =-SPVOL * (XI_X_TILDE + (GAMMA-1.0_rfreal) * UX * SPD_SND_INV)
                Pinv(4,3) =-SPVOL * (XI_Y_TILDE + (GAMMA-1.0_rfreal) * UY * SPD_SND_INV)
                Pinv(4,4) = (GAMMA-1.0_rfreal) * SPVOL * SPD_SND_INV

              else if (ND == 3) then

                ! ... the forward matrix P
                Pfor(1,1) = XI_X_TILDE
                Pfor(1,2) = XI_Y_TILDE
                Pfor(1,3) = XI_Z_TILDE
                Pfor(1,4) = 0.5_rfreal * RHO * SPD_SND_INV
                Pfor(1,5) = Pfor(1,4)

                Pfor(2,1) = UX * XI_X_TILDE
                Pfor(2,2) = UX * XI_Y_TILDE - RHO * XI_Z_TILDE
                Pfor(2,3) = UX * XI_Z_TILDE + RHO * XI_Y_TILDE
                Pfor(2,4) = Pfor(1,4) * (UX + SPD_SND * XI_X_TILDE)
                Pfor(2,5) = Pfor(1,4) * (UX - SPD_SND * XI_X_TILDE)

                Pfor(3,1) = UY * XI_X_TILDE + RHO * XI_Z_TILDE
                Pfor(3,2) = UY * XI_Y_TILDE
                Pfor(3,3) = UY * XI_Z_TILDE - RHO * XI_X_TILDE
                Pfor(3,4) = Pfor(1,4) * (UY + SPD_SND * XI_Y_TILDE)
                Pfor(3,5) = Pfor(1,4) * (UY - SPD_SND * XI_Y_TILDE)

                Pfor(4,1) = UZ * XI_X_TILDE - RHO * XI_Y_TILDE
                Pfor(4,2) = UZ * XI_Y_TILDE + RHO * XI_X_TILDE
                Pfor(4,3) = UZ * XI_Z_TILDE
                Pfor(4,4) = Pfor(1,4) * (UZ + SPD_SND * XI_Z_TILDE)
                Pfor(4,5) = Pfor(1,4) * (UZ - SPD_SND * XI_Z_TILDE)

                Pfor(5,1) = KE * XI_X_TILDE + RHO * (UY * XI_Z_TILDE - UZ * XI_Y_TILDE)
                Pfor(5,2) = KE * XI_Y_TILDE + RHO * (UZ * XI_X_TILDE - UX * XI_Z_TILDE)
                Pfor(5,3) = KE * XI_Z_TILDE + RHO * (UX * XI_Y_TILDE - UY * XI_X_TILDE)
                Pfor(5,4) = Pfor(1,4) * (ENTHALPY + SPD_SND * V_DOT_XI)
                Pfor(5,5) = Pfor(1,4) * (ENTHALPY - SPD_SND * V_DOT_XI)

                ! ... the inverse matrix Pinv
                junk      = (GAMMA-1.0_rfreal) * SPD_SND_INV**2
                Pinv(1,1) =  (1.0_rfreal - 0.5_rfreal * (GAMMA-1.0_rfreal) * MACH2) * XI_X_TILDE &
                     - SPVOL * (UY * XI_Z_TILDE - UZ * XI_Y_TILDE)
                Pinv(1,2) =  junk * UX * XI_X_TILDE
                Pinv(1,3) =  junk * UY * XI_X_TILDE + SPVOL * XI_Z_TILDE
                Pinv(1,4) =  junk * UZ * XI_X_TILDE - SPVOL * XI_Y_TILDE
                Pinv(1,5) = -junk * XI_X_TILDE

                Pinv(2,1) = (1.0_rfreal - 0.5_rfreal * (GAMMA-1.0_rfreal) * MACH2) * XI_Y_TILDE &
                     - SPVOL * (UZ * XI_X_TILDE - UX * XI_Z_TILDE)
                Pinv(2,2) =  junk * UX * XI_Y_TILDE - SPVOL * XI_Z_TILDE
                Pinv(2,3) =  junk * UY * XI_Y_TILDE
                Pinv(2,4) =  junk * UZ * XI_Y_TILDE + SPVOL * XI_X_TILDE
                Pinv(2,5) = -junk * XI_Y_TILDE

                Pinv(3,1) = (1.0_rfreal - 0.5_rfreal * (GAMMA-1.0_rfreal) * MACH2) * XI_Z_TILDE &
                     - SPVOL * (UX * XI_Y_TILDE - UY * XI_X_TILDE)
                Pinv(3,2) =  junk * UX * XI_Z_TILDE + SPVOL * XI_Y_TILDE
                Pinv(3,3) =  junk * UY * XI_Z_TILDE - SPVOL * XI_X_TILDE
                Pinv(3,4) =  junk * UZ * XI_Z_TILDE
                Pinv(3,5) = -junk * XI_Z_TILDE

                junk      = (GAMMA-1.0_rfreal) * SPVOL * SPD_SND_INV
                Pinv(4,1) =  SPD_SND * SPVOL * (0.5_rfreal * (GAMMA-1.0_rfreal) * MACH2 - V_DOT_XI * SPD_SND_INV)
                Pinv(4,2) =  SPVOL * XI_X_TILDE - junk * UX
                Pinv(4,3) =  SPVOL * XI_Y_TILDE - junk * UY
                Pinv(4,4) =  SPVOL * XI_Z_TILDE - junk * UZ
                Pinv(4,5) =  junk

                Pinv(5,1) =  SPD_SND * SPVOL * (0.5_rfreal * (GAMMA-1.0_rfreal) * MACH2 + V_DOT_XI * SPD_SND_INV)
                Pinv(5,2) = -SPVOL * XI_X_TILDE - junk * UX
                Pinv(5,3) = -SPVOL * XI_Y_TILDE - junk * UY
                Pinv(5,4) = -SPVOL * XI_Z_TILDE - junk * UZ
                Pinv(5,5) =  junk

              end if

              if ( (patch_bcType == NSCBC_INFLOW_VELOCITY_TEMPERATURE) .OR. &
                   (patch_bcType == NSCBC_INFLOW_VELOCITY_TEMPERATURE_FROM_SPONGE) .OR. &
                   (patch_bcType == NSCBC_INFLOW_VELOCITY_TEMPERATURE_FORCING_SLOT) .OR. & ! ... JKim 06/2008
                   (patch_bcType == NSCBC_OUTFLOW_PERFECT_NONREFLECTION) .OR. &
                   (patch_bcType == NSCBC_OUTFLOW_PERFECT_REFLECTION) .OR. &
                   (patch_bcType == NSCBC_WALL_ADIABATIC_SLIP) .OR. &
                   (patch_bcType == NSCBC_WALL_ISOTHERMAL_NOSLIP) .OR. &
                   (patch_bcType == NSCBC_WALL_ISOTHERMAL_NOSLIP_COUPLED) .OR. &
                   (patch_bcType == NSCBC_WALL_NOSLIP_THERMALLY_COUPLED) .OR. &
                   (patch_bcType == NSCBC_SUBSONIC_INFLOW) .OR. &
                   (patch_bcType == NSCBC_SUPERSONIC_INFLOW) .or. &
                   (patch_bcType == STRUCTURAL_INTERACTING )) then

                ! ... compute the conservative flux variables
                do ii = 1, ND
                  do jj = 1, ND+2
                    cons_inviscid_flux(ii,jj) = 0.0_rfreal
                  end do
                end do

                ! ... one-dimensional fluxes
                cons_inviscid_flux(1,1)      = cv(l0,2)
                cons_inviscid_flux(1,2)      = RHO * UX**2 + PRESSURE
                cons_inviscid_flux(1,ND+2)   = (ENERGY + PRESSURE) * UX

                ! ... two-dimensional fluxes
                if (ND >= 2) then
                  cons_inviscid_flux(1,3)    = RHO * UY * UX
                  cons_inviscid_flux(2,1)    = cv(l0,3)
                  cons_inviscid_flux(2,2)    = RHO * UX * UY
                  cons_inviscid_flux(2,3)    = RHO * UY**2 + PRESSURE
                  cons_inviscid_flux(2,ND+2) = (ENERGY + PRESSURE) * UY
                end if

                ! ... three-dimensional fluxes
                if (ND == 3) then
                  cons_inviscid_flux(1,4)    = RHO * UZ * UX
                  cons_inviscid_flux(2,4)    = RHO * UZ * UY
                  cons_inviscid_flux(3,1)    = cv(l0,4)
                  cons_inviscid_flux(3,2)    = RHO * UX * UZ
                  cons_inviscid_flux(3,3)    = RHO * UY * UZ
                  cons_inviscid_flux(3,4)    = RHO * UZ * UZ + PRESSURE
                  cons_inviscid_flux(3,ND+2) = (ENERGY + PRESSURE) * UZ
                end if

                ! ... store the current rhs in SC(:)
                do ii = 1, ND+2
                  SC(ii) = rhs(l0,ii)
                end do

                ! ... subtract off the normal flux
                do ii = 1, ND+2
                  SC(ii) = SC(ii) + patch_deriv(ii,lp,normDir) * JAC(l0)
                end do

                ! ... subtract off the flux/metric derivative terms
                do ii = 1, ND+2
                  SC(ii) = SC(ii) - JAC(l0) * cv(l0,ii) * XI_TAU_XI(l0,normDir)
                  do jj = 1, ND
                    SC(ii) = SC(ii) - JAC(l0) * cons_inviscid_flux(jj,ii) * patch_MT2(lp,t2Map(normDir,jj))
                  end do
                  SC(ii) = 0.0_rfreal
                end do

                if (ND == 2) then

                  ! ... compute the characteristic L_vector
                  L_vector(:) = JAC(l0) * ( patch_deriv(:,lp,normDir) &
                       - ( cons_inviscid_flux(1,:) * patch_MT2(lp,t2Map(normDir,1)) &
                       + cons_inviscid_flux(2,:) * patch_MT2(lp,t2Map(normDir,2)) & 
                       + cv(l0,:) * XI_TAU_XI(l0,normDir) ) )

                  ! ... scale the L_vector by Pinv
                  L_vector = MATMUL(Pinv,L_vector)

                  ! ... transform SC with Pinv
                  SC = MATMUL(Pinv,SC)

                  ! ... wavespeeds
                  wave_spd(1:2) = UX * XI_X + UY * XI_Y
                  wave_spd(3)   = wave_spd(1) + SPD_SND / denom
                  wave_spd(4)   = wave_spd(1) - SPD_SND / denom

                else if (ND == 3) then ! JKim 11/2007

                  ! ... compute the characteristic L_vector
                  L_vector(:) = JAC(l0) * ( patch_deriv(:,lp,normDir) &
                       - ( cons_inviscid_flux(1,:) * patch_MT2(lp,t2Map(normDir,1)) &
                       + cons_inviscid_flux(2,:) * patch_MT2(lp,t2Map(normDir,2)) & 
                       + cons_inviscid_flux(3,:) * patch_MT2(lp,t2Map(normDir,3)) & ! JKim 11/2007
                       + cv(l0,:) * XI_TAU_XI(l0,normDir) ) )

                  ! ... scale the L_vector by Pinv
                  L_vector = MATMUL(Pinv,L_vector)

                  ! ... transform SC with Pinv
                  SC = MATMUL(Pinv,SC)

                else

                  call graceful_exit(region%myrank, 'No PINV & PFOR matrices for ND /= (2,3).  Stopping...')

                end if ! ND

              end if


              ! ... apply bc...
              ! ... solid wall first because no normal velocity thru wall
              if ( patch_bcType == NSCBC_WALL_ADIABATIC_SLIP ) then

                A_wall = XYZ_TAU2(l0,1) * XI_X_TILDE
                if (ND >= 2) A_wall = A_wall + XYZ_TAU2(l0,2) * XI_Y_TILDE
                if (ND == 3) A_wall = A_wall + XYZ_TAU2(l0,3) * XI_Z_TILDE

                if (patch_normDir > 0) then
                  L_vector(ND+1) = L_vector(ND+2) + SC(ND+1) - SC(ND+2) - 2.0_rfreal * A_wall
                else
                  L_vector(ND+2) = L_vector(ND+1) - SC(ND+1) + SC(ND+2) + 2.0_rfreal * A_wall
                end if

              else if ( (patch_bcType == NSCBC_INFLOW_VELOCITY_TEMPERATURE_FROM_SPONGE) .OR. &
                   (patch_bcType == NSCBC_INFLOW_VELOCITY_TEMPERATURE) .OR. &
                   (patch_bcType == NSCBC_INFLOW_VELOCITY_TEMPERATURE_FORCING_SLOT)) then ! ... JKim 06/2008

                if (patch_normDir > 0) then
                  L_vector(ND+1) = L_vector(ND+2) + SC(ND+1) - SC(ND+2) &
                       - 2.0_rfreal * (patch_dudt_inflow(lp) * XI_X_TILDE + patch_dvdt_inflow(lp) * XI_Y_TILDE)
                else
                  L_vector(ND+2) = L_vector(ND+1) - SC(ND+1) + SC(ND+2) &
                       + 2.0_rfreal * (patch_dudt_inflow(lp) * XI_X_TILDE + patch_dvdt_inflow(lp) * XI_Y_TILDE)
                end if

                L_vector(1) = + 0.5_rfreal * (GAMMA-1.0_rfreal) * RHO * SPD_SND_INV * (L_vector(ND+1)+L_vector(ND+2)) &
                     - 0.5_rfreal * (GAMMA-1.0_rfreal) * RHO * SPD_SND_INV * (SC(ND+1)+SC(ND+2)) + SC(1) &
                     + SPD_SND**2 / (GAMMA-1.0_rfreal) * patch_dTdt_inflow(lp)

                if (ND >= 2) L_vector(2) = -SC(2)
                if (ND == 3) L_vector(3) = -SC(3)

              else if ( patch_bcType == NSCBC_OUTFLOW_PERFECT_NONREFLECTION ) then

                SC(:) = 0.0_rfreal
                if (patch_normDir > 0) then
                  L_vector(ND+1) = 0.0_rfreal
                else
                  L_vector(ND+2) = 0.0_rfreal
                end if

              else if ( patch_bcType == NSCBC_WALL_ISOTHERMAL_NOSLIP .OR. &
                   patch_bcType == NSCBC_WALL_ISOTHERMAL_NOSLIP_COUPLED .OR. &
                   patch_bcType == NSCBC_WALL_NOSLIP_THERMALLY_COUPLED .or. &
                   patch_bcType == STRUCTURAL_INTERACTING ) then

                L_vector(2)  = 0.0_rfreal
                L_vector(ND) = 0.0_rfreal
                SC(2)  = 0.0_rfreal
                SC(ND) = 0.0_rfreal
                if (patch_normDir > 0) then
                  L_vector(ND+1) = L_vector(ND+2) + SC(ND+1) - SC(ND+2)
                else
                  L_vector(ND+2) = L_vector(ND+1) - SC(ND+1) + SC(ND+2)
                end if

              else if ( patch_bcType == NSCBC_OUTFLOW_PERFECT_REFLECTION) Then

                if (patch_normDir > 0) then
                  L_vector(ND+1) = -L_vector(ND+2) - SC(ND+1) - SC(ND+2)
                else
                  L_vector(ND+2) = -L_vector(ND+1) - SC(ND+1) - SC(ND+2)
                end if

              else if ( patch_bcType == NSCBC_SUPERSONIC_INFLOW ) Then

                ! ... do nothing, everything is specified in NS_BC_Fix_Value

              end if ! patch%bcType

              if ( (patch_bcType == NSCBC_INFLOW_VELOCITY_TEMPERATURE) .OR. &
                   (patch_bcType == NSCBC_INFLOW_VELOCITY_TEMPERATURE_FROM_SPONGE) .OR. &
                   (patch_bcType == NSCBC_INFLOW_VELOCITY_TEMPERATURE_FORCING_SLOT) .OR. & ! ... JKim 06/2008
                   (patch_bcType == NSCBC_OUTFLOW_PERFECT_NONREFLECTION) .OR. &
                   (patch_bcType == NSCBC_OUTFLOW_PERFECT_REFLECTION) .OR. &
                   (patch_bcType == NSCBC_WALL_ADIABATIC_SLIP) .OR. &
                   (patch_bcType == NSCBC_WALL_ISOTHERMAL_NOSLIP) .OR. &
                   (patch_bcType == NSCBC_WALL_ISOTHERMAL_NOSLIP_COUPLED) .OR. &
                   (patch_bcType == NSCBC_WALL_NOSLIP_THERMALLY_COUPLED) .OR. &
                   (patch_bcType == NSCBC_SUBSONIC_INFLOW) .OR. &
                   (patch_bcType == NSCBC_SUPERSONIC_INFLOW) .or. &
                   (patch_bcType == STRUCTURAL_INTERACTING )) then

                ! ... compute the new normal flux
                new_normal_flux(:) = MATMUL(Pfor,L_vector)
                new_normal_flux(:) = INVJAC(l0) * new_normal_flux(:)
                do jj = 1, ND
                   new_normal_flux(:) = new_normal_flux(:) + cons_inviscid_flux(jj,:) * patch_MT2(lp,t2Map(normDir,jj))
                end do
                new_normal_flux(:) = new_normal_flux + cv(l0,:) * XI_TAU_XI(l0,normDir)

                ! ... applies bc to only those nodes with iblank == 1
                rhs(l0,:) = rhs(l0,:) + ibfac_local *(JAC(l0) * patch_deriv(:,lp,normDir) - JAC(l0) * new_normal_flux(:))

             end if

             if ( patch_bcType == SAT_FAR_FIELD .or. patch_bcType == SAT_PRESSURE .or. patch_bcType == SAT_INJECTION) then
                
                if (input%chemistry_model == MODEL1) then
#ifdef HAVE_CANTERA

                   ! ... wall-normal vector
                   norm_vec(1:MAX_ND) = (/ XI_X_TILDE, XI_Y_TILDE, XI_Z_TILDE /)

                   ! ... inviscid penalty parameter
                   sigmaI1 = -SAT_sigmaI1_FF * dsgn

                   ! ... pull off the boundary data 
                   uB(:) = Qvs(1:ND+2)

                   ! ... target data
                   gI1(:) = Wvs(1:ND+2)

                   ! ... follow Magnus's algorithm (norm_vec has unit length)
                   pnorm_vec(:) = norm_vec(1:ND) * sqrt(XI_X**2 + XI_Y**2 + XI_Z**2)

                   ! ... use target data to make implicit work better?
                   if (gas_dv_model == DV_MODEL_IDEALGAS .or. gas_dv_model == DV_MODEL_IDEALGAS_MIXTURE) then
                      if(patch_bcType == SAT_INJECTION) then 
                         Call SAT_Form_Roe_Matrices_INJ(uB, gI1, input%gamRef, pnorm_vec, Tmat, Tinv, Lambda)
                      else
                         Call SAT_Form_Roe_Matrices(uB, gI1, gamma, pnorm_vec, Tmat, Tinv, Lambda)
                      end if
                   elseif (gas_dv_model == DV_MODEL_THERMALLY_PERFECT) then
                      Call SAT_Form_Roe_Matrices_TP(uB, gI1, ND, gamma, region_gamref, pnorm_vec, Tmat, Tinv, Lambda)
                   end if

                   ! ... save ucon, speed_sound
                   ucon    = Lambda(1,1)
                   spd_snd = Lambda(ND+1,ND+1) - ucon

                   ! ... only pick those Lambda that are incoming
                   if ( sgn == 1 ) then
                      do jj = 1, ND+2
                         Lambda(jj,jj) = max(Lambda(jj,jj),0.0_rfreal)
                      end do
                   else 
                      do jj = 1, ND+2
                         Lambda(jj,jj) = min(Lambda(jj,jj),0.0_rfreal)
                      end do
                   end if

                   ! ... modify Lambda if subsonic outflow
                   ! ... left boundary
                   if (patch_bcType == SAT_FAR_FIELD) then
                      if (sgn == 1 .and. ucon < 0.0_rfreal .and. ucon + spd_snd > 0.0_rfreal) then
                         Lambda(ND+2,ND+1) = Lambda(ND+2,ND+1) - ucon - spd_snd
                         ! right boundary
                      else if (sgn == -1 .and. ucon > 0.0_rfreal .and. ucon - spd_snd < 0.0_rfreal) then
                         Lambda(ND+1,ND+2) = Lambda(ND+1,ND+2) - ucon + spd_snd
                      end if
                   endif

                   ! ... multiply Lambda' into X
                   matX = MATMUL(Lambda,Tinv)

                   ! ... compute the characteristic matrix
                   Aprime = MATMUL(Tmat,matX)

                   ! ... subtract off the target
                   ub(1:ND+2) = ub(1:ND+2) - gI1(1:ND+2)

                   ! ... compute the characteristic penalty vector
                   gI2(1:ND+2) = MATMUL(Aprime,uB)

                   if(patch_bcType == SAT_PRESSURE) then
                      if(ucon*sgn <= 0d0) then
                         jj = ND+2;
                         ub(jj) = ub(jj) - 0.5d0*sum(cv(l0,2:ND+1)**2)/cv(l0,1) + 0.5d0*sum(cvTarget(l0,2:ND+1)**2)/cvTarget(l0,1)
                         ub(1:ND+1) = 0d0
                         gI2(1:ND+2) = Lambda(jj,jj)/Tmat(jj,jj)*UB(jj)*Tmat(1:ND+2,jj) - Lambda(jj,jj)/Tmat(jj,jj)*UB(jj)*Tmat(1:ND+2,jj-1)
                      endif
                   endif

                   ! ... compute penalty (Bndry_h is included in Lambda already)
                   do jj = 1, ND+2
                      penalty(jj) = sigmaI1 * SBP_invPdiag_block1(1) * gI2(jj)
                   end do

                   ! ... add the rhs
                   do jj = 1, ND+2
                      rhs(l0,jj) = rhs(l0,jj) + ibfac_local * penalty(jj)
                   end do

                   ! ... SAT FARFIELD (species treatment)
                   if (dsgn * ucon > 0) then
                      Sfactor =  ucon * ibfac_local * sigmaI1 * SBP_invPdiag_block1(1)
                      do jj = 1, nAuxVars
                         rhs_auxVars(l0,jj) = rhs_auxVars(l0,jj) + Sfactor*(auxVars(l0,jj) - auxVarsTarget(l0,jj)/cvTarget(l0,1)*cv(l0,1))
                      end do
                   end if

                   ! ... store Aprime
                   if (timeScheme == BDF_GMRES_IMPLICIT) then

                      ! ... intermediate values
                      gamm1i  = 1.0_rfreal / (gamma - 1.0_rfreal)
                      T       = dv(lp,2)
                      RHO_P   = gamma * gamm1i / T
                      RHO_T   = -PRESSURE / T * RHO_P

                      ! ... construct BI1 = Gamma_e
                      If (ND == 2) then
                         BI1(1,1) = RHO_P
                         BI1(1,2) = 0.0_rfreal
                         BI1(1,3) = 0.0_rfreal
                         BI1(1,4) = RHO_T
                         BI1(2,1) = UX * RHO_P
                         BI1(2,2) = RHO
                         BI1(2,3) = 0.0_rfreal
                         BI1(2,4) = UX * RHO_T
                         BI1(3,1) = UY * RHO_P
                         BI1(3,2) = 0.0_rfreal
                         BI1(3,3) = RHO
                         BI1(3,4) = UY * RHO_T
                         BI1(4,1) = gamm1i + KE * RHO_P
                         BI1(4,2) = RHO * UX
                         BI1(4,3) = RHO * UY
                         BI1(4,4) = KE * RHO_T
                      Else If (ND == 3) then
                         BI1(1,1) = RHO_P
                         BI1(1,2) = 0.0_rfreal
                         BI1(1,3) = 0.0_rfreal
                         BI1(1,4) = 0.0_rfreal
                         BI1(1,5) = RHO_T
                         BI1(2,1) = UX * RHO_P
                         BI1(2,2) = RHO
                         BI1(2,3) = 0.0_rfreal
                         BI1(2,4) = 0.0_rfreal
                         BI1(2,5) = UX * RHO_T
                         BI1(3,1) = UY * RHO_P
                         BI1(3,2) = 0.0_rfreal
                         BI1(3,3) = RHO
                         BI1(3,4) = 0.0_rfreal
                         BI1(3,5) = UY * RHO_T
                         BI1(4,1) = UZ * RHO_P
                         BI1(4,2) = 0.0_rfreal
                         BI1(4,3) = 0.0_rfreal
                         BI1(4,4) = RHO
                         BI1(4,5) = UZ * RHO_T
                         BI1(5,1) = gamm1i + KE * RHO_P
                         BI1(5,2) = RHO * UX
                         BI1(5,3) = RHO * UY
                         BI1(5,4) = RHO * UZ
                         BI1(5,5) = KE * RHO_T
                      End if

                      Aprime = MATMUL(Aprime,BI1)

                      do jj = 1, ND+2
                         do kk = 1, ND+2  
                            patch_implicit_sat_mat(lp,jj,kk) = patch_implicit_sat_mat(lp,jj,kk) + ibfac_local * sigmaI1 * SBP_invPdiag_block1(1) * Aprime(jj,kk)
                         end do
                      end do
                   end if

                   if ( REinv > 0.0_rfreal ) then

                      if (timeScheme == BDF_GMRES_IMPLICIT) then

                         ! ... fill vector for viscous flux Jacobian diagonal entries
                         ! ... MU and LAMBDAvisc already multiplied by REinv
                         ViscFluxJac(:,:) = 0.0_rfreal
                         ViscFluxJac(2,2) = XI_X * XI_X * (2*MU + LAMBDAvisc)

                         ! ... off diagonal
                         ! ViscFluxJac(5,2) = UX * ViscFluxJac(2,2)

                         if (ND >= 2) then

                            ! ... add to existing terms
                            ViscFluxJac(2,2) = ViscFluxJac(2,2) + MU * (XI_Y * XI_Y)

                            ! ... new terms
                            ViscFluxJac(3,3) = XI_Y * XI_Y * (2*MU + LAMBDAvisc) + MU * (XI_X * XI_X)
                            ! ... off diagonal
                            ViscFluxJac(2,3) = XI_X * XI_Y * (MU + LAMBDAvisc)
                            ViscFluxJac(3,2) = XI_Y * XI_X * (MU + LAMBDAvisc)

                         end if

                         if (ND == 3)  then

                            ! ... add to existing terms
                            ViscFluxJac(2,2) = ViscFluxJac(2,2) + MU * (XI_Z * XI_Z)
                            ViscFluxJac(3,3) = ViscFluxJac(3,3) + MU * (XI_Z * XI_Z)
                            ViscFluxJac(ND+2,ND+2) = ViscFluxJac(ND+2,ND+2) + XI_Z * XI_Z

                            ! ... new terms
                            ViscFluxJac(4,4) = XI_Z * XI_Z * (2*MU + LAMBDAvisc) + MU * (XI_X * XI_X + XI_Y * XI_Y)
                            ! ... off diagonal
                            ViscFluxJac(2,4) = XI_X * XI_Z * (MU + LAMBDAvisc)
                            ViscFluxJac(3,4) = XI_Y * XI_Z * (MU + LAMBDAvisc)
                            ViscFluxJac(4,2) = XI_Z * XI_X * (MU + LAMBDAvisc)
                            ViscFluxJac(4,3) = XI_Z * XI_Y * (MU + LAMBDAvisc)

                         end if

                         ! ... lhs_var = ND+2 terms
                         ! ... if ND >= 1
                         ViscFluxJac(ND+2,ND+2) = XI_X * XI_X
                         ViscFluxJac(ND+2,2) = UX * ViscFluxJac(2,2)

                         if (ND >= 2) then
                            ! ... add to existing terms
                            ViscFluxJac(ND+2,ND+2) = ViscFluxJac(ND+2,ND+2) + XI_Y * XI_Y
                            ! ... existing off diagonal
                            ViscFluxJac(ND+2,2) = ViscFluxJac(ND+2,2) + UY * ViscFluxJac(2,3)

                            ! ... off diagonal
                            ViscFluxJac(ND+2,3) = UX * ViscFluxJac(2,3) + UY * ViscFluxJac(3,3)
                         end if

                         if (ND == 3) then
                            ! ... add to existing terms
                            ViscFluxJac(ND+2,ND+2) = ViscFluxJac(ND+2,ND+2) + XI_Z * XI_Z
                            ! ... existing off diagonal
                            ViscFluxJac(ND+2,2) = ViscFluxJac(ND+2,2) + UZ * ViscFluxJac(2,4)
                            ViscFluxJac(ND+2,3) = ViscFluxJac(ND+2,3) + UZ * ViscFluxJac(3,4)

                            ! ... off diagonal
                            ViscFluxJac(ND+2,4) = UX * ViscFluxJac(2,4) + UY * ViscFluxJac(3,4) +&
                                 UZ * ViscFluxJac(4,4)
                         end if

                         ! ... multiply r_(5,5) by mu/Re/Pr
                         ViscFluxJac(ND+2,ND+2) = ViscFluxJac(ND+2,ND+2) * MU * REinv * PRinv
                      end if

                      if(patch_bcType == SAT_PRESSURE .or. patch_bcType == SAT_FAR_FIELD) then
                         ! ... factor
                         sigmaI2 = dsgn * SAT_sigmaI2_FF * SBP_invPdiag_block1(1) / bndry_h !* JAC(l0)

                         if(patch_bcType == SAT_PRESSURE) sigmaI2 = dsgn * 1d0 * SBP_invPdiag_block1(1) * JAC(l0)

                         ! ... target state
                         do jj = 1, ND+2
                            gI2(jj) = 0.0_rfreal
                         end do

                         ! ... boundary data (already includes 1/Re factor)
                         uB(:) = patch%ViscousFlux(:,lp,1) * XI_X_TILDE
                         if (ND >= 2) &
                              uB(:) = uB(:) + patch%ViscousFlux(:,lp,2) * XI_Y_TILDE
                         if (ND == 3) &
                              uB(:) = uB(:) + patch%ViscousFlux(:,lp,3) * XI_Z_TILDE

                         if(patch_bcType == SAT_PRESSURE) uB(:) = patch_ViscousFlux(:,lp,normDir)

                         ! ... penalty term
                         penalty(:) = sigmaI2 * (uB(:) - gI2(:))

                         ! ... add to RHS
                         do jj = 1, ND+2
                            rhs(l0,jj) = rhs(l0,jj) + ibfac_local * penalty(jj)
                         end do

                         sigmaAux = ibfac_local * sigmaI2
                         do jj = 1, nAuxVars
                            ubAux = patch_ViscousFluxAux(jj,lp,normDir) 
                            rhs_auxVars(l0,jj) = rhs_auxVars(l0,jj) + sigmaAux* ubAux
                         end do

                      elseif(patch_bcType == SAT_INJECTION) then
                         ! ... boundary data
                         uB(:) = Qvs(1:ND+2)
                         ! ... target state

                         if( ucon >= 0.0_rfreal .and. ucon - spd_snd <= 0.0_rfreal) then
                            RHO = uB(1)
                            gI2(1)      = RHO
                            gI2(2:ND+1) = cvTarget(l0,2:ND+1)/cvTarget(l0,1)*RHO
                            ! ... update the total energy
                            Tinflow = gamref*cvTarget(l0,ND+2)- 0.5_rfreal * sum(cvTarget(l0,2:ND+1))/ cvTarget(l0,1)
                            if (gas_dv_model == DV_MODEL_IDEALGAS) then
                               Tinflow = Tinflow/cvTarget(l0,1)
                               PRESSURE = (GAMREF-1.0_rfreal)/GAMREF * RHO * Tinflow
                               gI2(ND+2) = PRESSURE / (GAMREF-1.0_rfreal) + 0.5_rfreal * sum(gI2(2:ND+1)**2)/RHO
                            else if (gas_dv_model == DV_MODEL_IDEALGAS_MIXTURE) then
                               Tinflow = input%TempRef / combustion%model1%dimTempFactor !lucacancel only injection at constant T=Tref
                               !...Tinflow is dimensionless
                               gI2(ND+2) = TRhoAux2Energy(Tinflow, RHO, Qaux, combustion%model1, RemoveBEDefaultF_in=.true.)  + 0.5_rfreal * sum(gI2(2:ND+1)**2)/RHO
                            else
                               Call Graceful_Exit(region%myRank,'PlasComCM: ERROR: Unknown gas model in NS_BC_Fix_Value')
                            end if
                         else
                            gI2(1:ND+2) = Wvs(1:ND+2)
                            RHO = gI2(1)
                         endif


                         ! ... penalty parameter
                         sigmaI2 = -(SAT_sigmaI2_FF / 4.0_rfreal) * ( SBP_invPdiag_block1(1) / bndry_h )**2 * &
                              maxval(tv(l0,1:input%heatDiffusivityIndex-1)) / RHO * MAX(input%gamRef / Pr, 5.0_rfreal / 3.0_rfreal)
                         sigmaI2 = sigmaI2 * REinv

                         ! ... compute penalty
                         penalty(:) = sigmaI2 * (uB(1:ND+2) - gI2(1:ND+2))
                         do jj = 1, ND+2
                            rhs(l0,jj) = rhs(l0,jj) + ibfac_local * penalty(jj)
                         end do
                         do jj = 1, input%nAuxVars
                            rhs_auxVars(l0,jj) = rhs_auxVars(l0,jj) + ibfac_local *sigmaI2 * (auxVars(l0,jj) - RHO*auxVarsTarget(l0,jj)/cvTarget(l0,1))
                         end do

                      endif

                      if (timeScheme == BDF_GMRES_IMPLICIT) then
                         do ii = 1,ND+2
                            do jj = 1, ND+2
                               patch_implicit_sat_mat_old(lp,ii,jj) = patch_implicit_sat_mat_old(lp,ii,jj) +&
                                    ibfac_local * sigmaI2 * ViscFluxJac(ii,jj)
                            end do
                         end do
                      end if

                   end if

#endif
                else

                   ! ... wall-normal vector
                   norm_vec(1:MAX_ND) = (/ XI_X_TILDE, XI_Y_TILDE, XI_Z_TILDE /)

                   ! ... inviscid penalty parameter
                   sigmaI1 = -SAT_sigmaI1_FF * dsgn

                   ! ... pull off the boundary data 
                   uB(:) = cv(l0,:)

                   ! ... target data
                   gI1(:) = cvTarget(l0,:)

                   ! ...Pressure is not imposed on an injection boundary. The SAT_BCs should take care of this
                   ! ...but they do a poor job.

                   ! ... follow Magnus's algorithm (norm_vec has unit length)
                   pnorm_vec(:) = norm_vec(1:ND) * sqrt(XI_X**2 + XI_Y**2 + XI_Z**2)

                   ! ... use target data to make implicit work better?
                   if (gas_dv_model == DV_MODEL_IDEALGAS .or. gas_dv_model == DV_MODEL_IDEALGAS_MIXTURE) then
                      if(patch_bcType == SAT_INJECTION) then 
                         Call SAT_Form_Roe_Matrices_INJ(uB, gI1, gamma, pnorm_vec, Tmat, Tinv, Lambda)
                      else
                         Call SAT_Form_Roe_Matrices(uB, gI1, gamma, pnorm_vec, Tmat, Tinv, Lambda)
                      end if
                   elseif (gas_dv_model == DV_MODEL_THERMALLY_PERFECT) then
                      Call SAT_Form_Roe_Matrices_TP(uB, gI1, ND, gamma, region_gamref, pnorm_vec, Tmat, Tinv, Lambda)
                   end if

                   ! ... save ucon, speed_sound
                   ucon    = Lambda(1,1)
                   spd_snd = Lambda(ND+1,ND+1) - ucon

                   ! ... only pick those Lambda that are incoming
                   if ( sgn == 1 ) then
                      do jj = 1, ND+2
                         Lambda(jj,jj) = max(Lambda(jj,jj),0.0_rfreal)
                      end do
                   else 
                      do jj = 1, ND+2
                         Lambda(jj,jj) = min(Lambda(jj,jj),0.0_rfreal)
                      end do
                   end if

                   ! ... modify Lambda if subsonic outflow
                   if (patch_bcType == SAT_PRESSURE) then
                      if(sgn == 1) then
                         if(ucon < 0.0_rfreal .and. ucon + spd_snd > 0.0_rfreal) then
                            Lambda(ND+2,ND+1) = Lambda(ND+2,ND+1) - ucon - spd_snd
                            Lambda(ND+2,ND+2) = Lambda(ND+2,ND+2) - ucon - spd_snd
                         endif
                         Lambda(ND+1,ND+2) = Lambda(ND+1,ND+2) + ucon + spd_snd
                      endif
                      if(sgn == -1) then
                         if(ucon >= 0.0_rfreal .and. ucon - spd_snd <= 0.0_rfreal) then
                            Lambda(ND+1,ND+2) = Lambda(ND+1,ND+2) - ucon + spd_snd
                            Lambda(ND+1,ND+1) = Lambda(ND+1,ND+1) - ucon + spd_snd
                         endif
                         Lambda(ND+2,ND+1) = Lambda(ND+2,ND+1) + ucon - spd_snd
                      endif
                   endif

                   ! ... modify Lambda if subsonic outflow
                   ! ... left boundary
                   if (patch_bcType == SAT_FAR_FIELD) then
                      if (sgn == 1 .and. &
                           ucon < 0.0_rfreal .and. ucon + spd_snd > 0.0_rfreal) then
                         Lambda(ND+2,ND+1) = Lambda(ND+2,ND+1) - ucon - spd_snd
                         ! right boundary
                      else if (sgn == -1 .and. &
                           ucon > 0.0_rfreal .and. ucon - spd_snd < 0.0_rfreal) then
                         Lambda(ND+1,ND+2) = Lambda(ND+1,ND+2) - ucon + spd_snd
                      end if
                   end if

                   ! ... multiply Lambda' into X
                   matX = MATMUL(Lambda,Tinv)

                   ! ... compute the characteristic matrix
                   Aprime = MATMUL(Tmat,matX)

                   ! ... subtract off the target
                   ub(1:ND+2) = ub(1:ND+2) - gI1(1:ND+2)

                   ! ... compute the characteristic penalty vector
                   gI2(1:ND+2) = MATMUL(Aprime,uB)

                   if(patch_bcType == SAT_PRESSURE) then
                      if(ucon*sgn <= 0d0) then
                         jj = ND+2;
                         ub(jj) = ub(jj) - 0.5d0*sum(cv(l0,2:ND+1)**2)/cv(l0,1) + 0.5d0*sum(cvTarget(l0,2:ND+1)**2)/cvTarget(l0,1)
                         ub(1:ND+1) = 0d0
                         gI2(1:ND+2) = Lambda(jj,jj)/Tmat(jj,jj)*UB(jj)*Tmat(1:ND+2,jj)!! - Lambda(jj,jj)/Tmat(jj,jj)*UB(jj)*Tmat(1:ND+2,jj-1)
                      endif
                   endif

                   ! ... compute penalty (Bndry_h is included in Lambda already)
                   do jj = 1, ND+2
                      penalty(jj) = sigmaI1 * SBP_invPdiag_block1(1) * gI2(jj)
                   end do

                   ! ... add the rhs
                   do jj = 1, ND+2
                      rhs(l0,jj) = rhs(l0,jj) + ibfac_local * penalty(jj)
                   end do

                   !SAT FARFIELD (species treatment)
                   if (dsgn * ucon > 0) then
                      Sfactor =  ucon * ibfac_local * sigmaI1 * SBP_invPdiag_block1(1)
                      do jj = 1, nAuxVars
                         rhs_auxVars(l0,jj) = rhs_auxVars(l0,jj) + Sfactor*(auxVars(l0,jj) - auxVarsTarget(l0,jj)/cvTarget(l0,1)*cv(l0,1))
                      end do
                   end if

                   ! ... store Aprime
                   if (timeScheme == BDF_GMRES_IMPLICIT) then

                      ! ... intermediate values
                      gamm1i  = 1.0_rfreal / (gamma - 1.0_rfreal)
                      T       = dv(lp,2)
                      RHO_P   = gamma * gamm1i / T
                      RHO_T   = -PRESSURE / T * RHO_P

                      ! ... construct BI1 = Gamma_e
                      If (ND == 2) then
                         BI1(1,1) = RHO_P
                         BI1(1,2) = 0.0_rfreal
                         BI1(1,3) = 0.0_rfreal
                         BI1(1,4) = RHO_T
                         BI1(2,1) = UX * RHO_P
                         BI1(2,2) = RHO
                         BI1(2,3) = 0.0_rfreal
                         BI1(2,4) = UX * RHO_T
                         BI1(3,1) = UY * RHO_P
                         BI1(3,2) = 0.0_rfreal
                         BI1(3,3) = RHO
                         BI1(3,4) = UY * RHO_T
                         BI1(4,1) = gamm1i + KE * RHO_P
                         BI1(4,2) = RHO * UX
                         BI1(4,3) = RHO * UY
                         BI1(4,4) = KE * RHO_T
                      Else If (ND == 3) then
                         BI1(1,1) = RHO_P
                         BI1(1,2) = 0.0_rfreal
                         BI1(1,3) = 0.0_rfreal
                         BI1(1,4) = 0.0_rfreal
                         BI1(1,5) = RHO_T
                         BI1(2,1) = UX * RHO_P
                         BI1(2,2) = RHO
                         BI1(2,3) = 0.0_rfreal
                         BI1(2,4) = 0.0_rfreal
                         BI1(2,5) = UX * RHO_T
                         BI1(3,1) = UY * RHO_P
                         BI1(3,2) = 0.0_rfreal
                         BI1(3,3) = RHO
                         BI1(3,4) = 0.0_rfreal
                         BI1(3,5) = UY * RHO_T
                         BI1(4,1) = UZ * RHO_P
                         BI1(4,2) = 0.0_rfreal
                         BI1(4,3) = 0.0_rfreal
                         BI1(4,4) = RHO
                         BI1(4,5) = UZ * RHO_T
                         BI1(5,1) = gamm1i + KE * RHO_P
                         BI1(5,2) = RHO * UX
                         BI1(5,3) = RHO * UY
                         BI1(5,4) = RHO * UZ
                         BI1(5,5) = KE * RHO_T
                      End if

                      Aprime = MATMUL(Aprime,BI1)

                      do jj = 1, ND+2
                         do kk = 1, ND+2  
                            patch_implicit_sat_mat(lp,jj,kk) = patch_implicit_sat_mat(lp,jj,kk) + ibfac_local * sigmaI1 * SBP_invPdiag_block1(1) * Aprime(jj,kk)
                         end do
                      end do
                   end if

                   if ( REinv > 0.0_rfreal ) then

                      if (timeScheme == BDF_GMRES_IMPLICIT) then

                         ! ... fill vector for viscous flux Jacobian diagonal entries
                         ! ... MU and LAMBDAvisc already multiplied by REinv
                         ViscFluxJac(:,:) = 0.0_rfreal
                         ViscFluxJac(2,2) = XI_X * XI_X * (2*MU + LAMBDAvisc)


                         ! ... off diagonal
                         ! ViscFluxJac(5,2) = UX * ViscFluxJac(2,2)

                         if (ND >= 2) then

                            ! ... add to existing terms
                            ViscFluxJac(2,2) = ViscFluxJac(2,2) + MU * (XI_Y * XI_Y)

                            ! ... new terms
                            ViscFluxJac(3,3) = XI_Y * XI_Y * (2*MU + LAMBDAvisc) + MU * (XI_X * XI_X)
                            ! ... off diagonal
                            ViscFluxJac(2,3) = XI_X * XI_Y * (MU + LAMBDAvisc)
                            ViscFluxJac(3,2) = XI_Y * XI_X * (MU + LAMBDAvisc)

                         end if

                         if (ND == 3)  then

                            ! ... add to existing terms
                            ViscFluxJac(2,2) = ViscFluxJac(2,2) + MU * (XI_Z * XI_Z)
                            ViscFluxJac(3,3) = ViscFluxJac(3,3) + MU * (XI_Z * XI_Z)
                            ViscFluxJac(ND+2,ND+2) = ViscFluxJac(ND+2,ND+2) + XI_Z * XI_Z

                            ! ... new terms
                            ViscFluxJac(4,4) = XI_Z * XI_Z * (2*MU + LAMBDAvisc) + MU * (XI_X * XI_X + XI_Y * XI_Y)
                            ! ... off diagonal
                            ViscFluxJac(2,4) = XI_X * XI_Z * (MU + LAMBDAvisc)
                            ViscFluxJac(3,4) = XI_Y * XI_Z * (MU + LAMBDAvisc)
                            ViscFluxJac(4,2) = XI_Z * XI_X * (MU + LAMBDAvisc)
                            ViscFluxJac(4,3) = XI_Z * XI_Y * (MU + LAMBDAvisc)

                         end if

                         ! ... lhs_var = ND+2 terms
                         ! ... if ND >= 1
                         ViscFluxJac(ND+2,ND+2) = XI_X * XI_X
                         ViscFluxJac(ND+2,2) = UX * ViscFluxJac(2,2)

                         if (ND >= 2) then
                            ! ... add to existing terms
                            ViscFluxJac(ND+2,ND+2) = ViscFluxJac(ND+2,ND+2) + XI_Y * XI_Y
                            ! ... existing off diagonal
                            ViscFluxJac(ND+2,2) = ViscFluxJac(ND+2,2) + UY * ViscFluxJac(2,3)

                            ! ... off diagonal
                            ViscFluxJac(ND+2,3) = UX * ViscFluxJac(2,3) + UY * ViscFluxJac(3,3)
                         end if

                         if (ND == 3) then
                            ! ... add to existing terms
                            ViscFluxJac(ND+2,ND+2) = ViscFluxJac(ND+2,ND+2) + XI_Z * XI_Z
                            ! ... existing off diagonal
                            ViscFluxJac(ND+2,2) = ViscFluxJac(ND+2,2) + UZ * ViscFluxJac(2,4)
                            ViscFluxJac(ND+2,3) = ViscFluxJac(ND+2,3) + UZ * ViscFluxJac(3,4)

                            ! ... off diagonal
                            ViscFluxJac(ND+2,4) = UX * ViscFluxJac(2,4) + UY * ViscFluxJac(3,4) +&
                                 UZ * ViscFluxJac(4,4)
                         end if

                         ! ... multiply r_(5,5) by mu/Re/Pr
                         ViscFluxJac(ND+2,ND+2) = ViscFluxJac(ND+2,ND+2) * MU * REinv * PRinv
                      end if

                      if (patch_bcType == SAT_PRESSURE .or. &
                           patch_bcType == SAT_FAR_FIELD) then

                         ! ... factor
                         sigmaI2 = dsgn * SAT_sigmaI2_FF * SBP_invPdiag_block1(1) &
                              / bndry_h ! * JAC(l0)

                         ! ... target state
                         do jj = 1, ND+2
                            gI2(jj) = 0.0_rfreal
                         end do

                         ! ... boundary data (already includes 1/Re factor)
                         uB(:) = patch%ViscousFlux(:,lp,1) * XI_X_TILDE
                         if (ND >= 2) &
                              uB(:) = uB(:) + patch%ViscousFlux(:,lp,2) * XI_Y_TILDE
                         if (ND == 3) &
                              uB(:) = uB(:) + patch%ViscousFlux(:,lp,3) * XI_Z_TILDE

                         ! ... penalty term
                         penalty(:) = sigmaI2 * (uB(:) - gI2(:))

                         ! ... add to RHS
                         do jj = 1, ND+2
                            rhs(l0,jj) = rhs(l0,jj) + ibfac_local * penalty(jj)
                         end do

                         sigmaAux = ibfac_local * sigmaI2
                         do jj = 1, nAuxVars
                            ubAux = patch_ViscousFluxAux(jj,lp,normDir) 
                            rhs_auxVars(l0,jj) = rhs_auxVars(l0,jj) + sigmaAux* ubAux
                         end do

                      elseif(patch_bcType == SAT_INJECTION) then
                         ! ... boundary data
                         uB(:) = cv(l0,:)
                         ! ... target state

                         if( ucon >= 0.0_rfreal .and. ucon - spd_snd <= 0.0_rfreal) then
                            RHO = uB(1)
                            gI2(1)      = RHO
                            ! ... update the total energy
                            Tinflow = gamref*cvTarget(l0,ND+2)- 0.5_rfreal * sum(cvTarget(l0,2:ND+1))/ cvTarget(l0,1)
                            if (gas_dv_model == DV_MODEL_IDEALGAS) then
                               Tinflow = Tinflow/cvTarget(l0,1)
                               PRESSURE = (GAMREF-1.0_rfreal)/GAMREF * RHO * Tinflow
                               gI2(ND+2) = PRESSURE / (GAMREF-1.0_rfreal) + 0.5_rfreal * sum(gI2(2:ND+1)**2)/RHO
                            else if (gas_dv_model == DV_MODEL_IDEALGAS_MIXTURE) then
                               Tinflow = Tinflow/(cvTarget(l0,1)*MwcoeffRho+sum(auxVarsTarget(l0,:)*Mwcoeff))
                               gI2(ND+2) = RHO*(sum(auxVarsTarget(l0,:)*Mwcoeff)/cvTarget(l0,1) + MwcoeffRho)/gamref*Tinflow  + 0.5_rfreal * sum(gI2(2:ND+1)**2)/RHO
                            else
                               Call Graceful_Exit(region%myRank,'PlasComCM: ERROR: Unknown gas model in NS_BC_Fix_Value')
                            end if
                            gI2(2:ND+1) = cvTarget(l0,2:ND+1)/cvTarget(l0,1)*RHO
                         else
                            gI2(1:ND+2) = cvTarget(l0,1:ND+2)
                            RHO = gI2(1)
                         endif


                         ! ... penalty parameter
                         sigmaI2 = -(SAT_sigmaI2 / 4.0_rfreal) * (SBP_invPdiag_block1(1)/bndry_h)**2 * &
                              maxval(tv(l0,1:)) / RHO * MAX(GAMMA / Pr, 5.0_rfreal / 3.0_rfreal)
                         sigmaI2 = sigmaI2 * REinv

                         ! ... compute penalty
                         penalty(:) = sigmaI2 * (uB(1:ND+2) - gI2(1:ND+2))
                         do jj = 1, ND+2
                            rhs(l0,jj) = rhs(l0,jj) + ibfac_local * penalty(jj)
                         end do
                         do jj = 1, input%nAuxVars
                            rhs_auxVars(l0,jj) = rhs_auxVars(l0,jj) + ibfac_local *sigmaI2 * (auxVars(l0,jj) - RHO*auxVarsTarget(l0,jj)/cvTarget(l0,1))
                         end do

                      endif

                      if (timeScheme == BDF_GMRES_IMPLICIT) then
                         do ii = 1,ND+2
                            do jj = 1, ND+2
                               patch_implicit_sat_mat_old(lp,ii,jj) = patch_implicit_sat_mat_old(lp,ii,jj) +&
                                    ibfac_local * sigmaI2 * ViscFluxJac(ii,jj)
                            end do
                         end do
                      end if

                   end if

                end if !... if (input%chemistry_model == MODEL1)

             end if

              if ( patch_bcType == SAT_BLOCK_INTERFACE .or. patch_bcType == SAT_BLOCK_INTERFACE_PERIODIC ) then

                 if (input%chemistry_model == MODEL1) then
#ifdef HAVE_CANTERA

                    ! ... wall-normal vector
                    norm_vec(1:MAX_ND) = (/ XI_X_TILDE, XI_Y_TILDE, XI_Z_TILDE /)

                    ! ... inviscid penalty parameter
                    sigmaI1 = 1.0_rfreal ! -input%SAT_sigmaI1_BI * dsgn

                    ! ... pull off the boundary data 
                    uB(:) = Qvs(1:ND+2)

                    ! ... target data
                    gI1(:) = removeBondEnergy(patch_cv_out(lp,:),Waux, combustion%model1, input)

                    ! ... follow Magnus's algorithm (norm_vec has unit length)
                    pnorm_vec(:) = norm_vec(1:ND) * sqrt(XI_X**2 + XI_Y**2 + XI_Z**2)
                    if(gas_dv_model == DV_MODEL_IDEALGAS .or. gas_dv_model == DV_MODEL_IDEALGAS_MIXTURE) then
                       Call SAT_Form_Roe_Matrices(gI1, gI1, input%gamRef, pnorm_vec, Tmat, Tinv, Lambda)
                    elseif(gas_dv_model == DV_MODEL_THERMALLY_PERFECT) then
                       Call SAT_Form_Roe_Matrices_TP(gI1, gI1, ND, gamma, gamref, pnorm_vec, Tmat, Tinv, Lambda)
                    end if

                    ! ... add moving mesh term to Lambda
                    Do jj = 1, ND+2
                       Lambda(jj,jj) = Lambda(jj,jj) + XI_T
                    End Do

                    ! ... minimal dissipation if sigma_scale = 1
                    ! ... complete upwinding if sigma_scale = 2
                    sigma_scale = 2.0
                    do jj = 1, ND+2
                       sigR(jj) = 0.5_rfreal * Lambda(jj,jj) * sigma_scale
                       sigL(jj) = sigR(jj) - Lambda(jj,jj)
                    end do

                    ! ... only pick those Lambda that are incoming
                    Lambda_in(:,:)  = 0.0_rfreal
                    Lambda_out(:,:) = 0.0_rfreal
                    if ( sgn == 1 ) then
                       do jj = 1, ND+2
                          if (Lambda(jj,jj) > 0.0_rfreal) then
                             Lambda_in(jj,jj)  = -sigR(jj)
                          else
                             Lambda_out(jj,jj) = sigL(jj) 
                          end if
                       end do
                    else 
                       do jj = 1, ND+2
                          if (Lambda(jj,jj) < 0.0_rfreal) then
                             Lambda_in(jj,jj)  = sigR(jj)
                          else
                             Lambda_out(jj,jj) = -sigL(jj) 
                          end if
                       end do
                    end if

                    ! ... scalings across interface
                    ! ... assumes same FD scheme on both sides
                    p_in  = 1.0_rfreal / SBP_invPdiag_block1(1);
                    p_out = 1.0_rfreal / SBP_invPdiag_block1(1);

                    if ( sgn == 1 ) then
                       sig_v = 1.0_rfreal - p_in / (p_in + p_out);
                    else
                       sig_v = -p_out / (p_in + p_out);
                    end if

                    ! ... compute extra contribution to inviscid term
                    if (REinv > 0.0_rfreal) then

                       sigma_inter = - 0.25_rfreal / (p_in + p_out) * REinv

                       ! ... for block interface, add sigma_interface to Lambda
                       do jj = 1, ND+2
                          Lambda_in(jj,jj)  = Lambda_in(jj,jj)  + sigma_inter * (XI_X**2 + XI_Y**2 + XI_Z**2)
                          Lambda_out(jj,jj) = Lambda_out(jj,jj) + sigma_inter * (XI_X**2 + XI_Y**2 + XI_Z**2)
                       end do

                    end if

                    ! ... incoming characteristics

                    ! ... multiply Lambda' into X
                    matX = MATMUL(Lambda_in,Tinv)

                    ! ... compute the characteristic matrix
                    Aprime = MATMUL(Tmat,matX)

                    ! ... subtract off the target
                    tmp(1:ND+2) = ub(1:ND+2) - gI1(1:ND+2)

                    ! ... compute the characteristic penalty vector
                    tmp2(1:ND+2) = MATMUL(Aprime,tmp(1:ND+2))

                    ! ... compute penalty (Bndry_h is included in Lambda already)
                    penalty(1:ND+2) = sigmaI1 * SBP_invPdiag_block1(1) * tmp2(1:ND+2)

                    ! ... outgoing characteristcs

                    ! ... multiply Lambda' into X
                    matX = MATMUL(Lambda_out,Tinv)

                    ! ... compute the characteristic matrix
                    Aprime = MATMUL(Tmat,matX)

                    ! ... subtract off the target
                    tmp(1:ND+2) = ub(1:ND+2) - gI1(1:ND+2)

                    ! ... compute the characteristic penalty vector
                    tmp2(1:ND+2) = MATMUL(Aprime,tmp(1:ND+2))

                    ! ... compute penalty (Bndry_h is included in Lambda already)
                    penalty(1:ND+2) = penalty(1:ND+2) + sigmaI1 * SBP_invPdiag_block1(1) * tmp2(1:ND+2)

                    ! ... add the rhs
                    rhs(l0,:) = rhs(l0,:) + penalty(:)

                    if ( REinv < 0.0_rfreal ) then

                       ! ... factor
                       sigmaI2 = dsgn * SBP_invPdiag_block1(1) * JAC(l0) !/ bndry_h

                       ! ... target state
                       gI2(:) = patch_ViscousFlux_out(:,lp,1) * XI_X_TILDE
                       if (ND >= 2) gI2(:) = gI2(:) + patch_ViscousFlux_out(:,lp,2) * XI_Y_TILDE
                       if (ND == 3) gI2(:) = gI2(:) + patch_ViscousFlux_out(:,lp,3) * XI_Z_TILDE

                       ! ... boundary data (already includes 1/Re factor)
                       uB(:) = patch_ViscousFlux(:,lp,1) * XI_X_TILDE
                       if (ND >= 2) uB(:) = uB(:) + patch_ViscousFlux(:,lp,2) * XI_Y_TILDE
                       if (ND == 3) uB(:) = uB(:) + patch_ViscousFlux(:,lp,3) * XI_Z_TILDE

                       ! penalty term
                       penalty(:) = penalty(:) + sigmaI2 * (uB(:) - gI2(:))

                       ! ... add to RHS
                       rhs(l0,:) = rhs(l0,:) + ibfac_local * penalty(:)

                    end if

#endif
                 else

                    ! ... wall-normal vector
                    norm_vec(1:MAX_ND) = (/ XI_X_TILDE, XI_Y_TILDE, XI_Z_TILDE /)

                    ! ... inviscid penalty parameter
                    sigmaI1 = 1.0_rfreal ! -input%SAT_sigmaI1_BI * dsgn

                    ! ... pull off the boundary data 
                    uB(:) = cv(l0,:)

                    ! ... target data
                    gI1(:) = patch_cv_out(lp,:)

                    ! ... follow Magnus's algorithm (norm_vec has unit length)
                    pnorm_vec(:) = norm_vec(1:ND) * sqrt(XI_X**2 + XI_Y**2 + XI_Z**2)
                    if(gas_dv_model == DV_MODEL_IDEALGAS .or. gas_dv_model == DV_MODEL_IDEALGAS_MIXTURE) then
                       Call SAT_Form_Roe_Matrices(gI1, gI1, gamma, pnorm_vec, Tmat, Tinv, Lambda)
                    elseif(gas_dv_model == DV_MODEL_THERMALLY_PERFECT) then
                       Call SAT_Form_Roe_Matrices_TP(gI1, gI1, ND, gamma, gamref, pnorm_vec, Tmat, Tinv, Lambda)
                    end if

                    ! ... add moving mesh term to Lambda
                    Do jj = 1, ND+2
                       Lambda(jj,jj) = Lambda(jj,jj) + XI_T
                    End Do

                    ! ... minimal dissipation if sigma_scale = 1
                    ! ... complete upwinding if sigma_scale = 2
                    sigma_scale = 2.0
                    do jj = 1, ND+2
                       sigR(jj) = 0.5_rfreal * Lambda(jj,jj) * sigma_scale
                       sigL(jj) = sigR(jj) - Lambda(jj,jj)
                    end do

                    ! ... only pick those Lambda that are incoming
                    Lambda_in(:,:)  = 0.0_rfreal
                    Lambda_out(:,:) = 0.0_rfreal
                    if ( sgn == 1 ) then
                       do jj = 1, ND+2
                          if (Lambda(jj,jj) > 0.0_rfreal) then
                             Lambda_in(jj,jj)  = -sigR(jj)
                          else
                             Lambda_out(jj,jj) = sigL(jj) 
                          end if
                       end do
                    else 
                       do jj = 1, ND+2
                          if (Lambda(jj,jj) < 0.0_rfreal) then
                             Lambda_in(jj,jj)  = sigR(jj)
                          else
                             Lambda_out(jj,jj) = -sigL(jj) 
                          end if
                       end do
                    end if

                    ! ... scalings across interface
                    ! ... assumes same FD scheme on both sides
                    p_in  = 1.0_rfreal / SBP_invPdiag_block1(1);
                    p_out = 1.0_rfreal / SBP_invPdiag_block1(1);

                    if ( sgn == 1 ) then
                       sig_v = 1.0_rfreal - p_in / (p_in + p_out);
                    else
                       sig_v = -p_out / (p_in + p_out);
                    end if

                    ! ... compute extra contribution to inviscid term
                    if (REinv > 0.0_rfreal) then

                       sigma_inter = - 0.25_rfreal / (p_in + p_out) * REinv

                       ! ... for block interface, add sigma_interface to Lambda
                       do jj = 1, ND+2
                          Lambda_in(jj,jj)  = Lambda_in(jj,jj)  + sigma_inter * (XI_X**2 + XI_Y**2 + XI_Z**2)
                          Lambda_out(jj,jj) = Lambda_out(jj,jj) + sigma_inter * (XI_X**2 + XI_Y**2 + XI_Z**2)
                       end do

                    end if


                    ! ... incoming characteristics

                    ! ... multiply Lambda' into X
                    matX = MATMUL(Lambda_in,Tinv)

                    ! ... compute the characteristic matrix
                    Aprime = MATMUL(Tmat,matX)

                    ! ... subtract off the target
                    tmp(1:ND+2) = ub(1:ND+2) - gI1(1:ND+2)

                    ! ... compute the characteristic penalty vector
                    tmp2(1:ND+2) = MATMUL(Aprime,tmp(1:ND+2))

                    ! ... compute penalty (Bndry_h is included in Lambda already)
                    penalty(1:ND+2) = sigmaI1 * SBP_invPdiag_block1(1) * tmp2(1:ND+2)


                    ! ... outgoing characteristcs

                    ! ... multiply Lambda' into X
                    matX = MATMUL(Lambda_out,Tinv)

                    ! ... compute the characteristic matrix
                    Aprime = MATMUL(Tmat,matX)

                    ! ... subtract off the target
                    tmp(1:ND+2) = ub(1:ND+2) - gI1(1:ND+2)

                    ! ... compute the characteristic penalty vector
                    tmp2(1:ND+2) = MATMUL(Aprime,tmp(1:ND+2))

                    ! ... compute penalty (Bndry_h is included in Lambda already)
                    penalty(1:ND+2) = penalty(1:ND+2) + sigmaI1 * SBP_invPdiag_block1(1) * tmp2(1:ND+2)

                    ! ... add the rhs
                    rhs(l0,:) = rhs(l0,:) + penalty(:)

                    if ( REinv < 0.0_rfreal ) then

                       ! ... factor
                       sigmaI2 = dsgn * SBP_invPdiag_block1(1) !/ bndry_h ! * JAC(l0)

                       ! ... target state
                       gI2(:) = patch_ViscousFlux_out(:,lp,1) * XI_X_TILDE
                       if (ND >= 2) gI2(:) = gI2(:) + patch_ViscousFlux_out(:,lp,2) * XI_Y_TILDE
                       if (ND == 3) gI2(:) = gI2(:) + patch_ViscousFlux_out(:,lp,3) * XI_Z_TILDE

                       ! ... boundary data (already includes 1/Re factor)
                       uB(:) = patch_ViscousFlux(:,lp,1) * XI_X_TILDE
                       if (ND >= 2) uB(:) = uB(:) + patch_ViscousFlux(:,lp,2) * XI_Y_TILDE
                       if (ND == 3) uB(:) = uB(:) + patch_ViscousFlux(:,lp,3) * XI_Z_TILDE


                       ! penalty term
                       penalty(:) = penalty(:) + sigmaI2 * (uB(:) - gI2(:))

                       ! ... add to RHS
                       rhs(l0,:) = rhs(l0,:) + ibfac_local * penalty(:)

                    end if

                 end if !... if (input%chemistry_model == MODEL1)

              end if

              ! ... 
              ! ... SAT boundary conditions as given in 
              ! ... Svard & Nordstrom, JCP, Vol. 227, 2008
              if ( patch_bcType == SAT_SLIP_ADIABATIC_WALL .OR. &
                   patch_bcType == SAT_NOSLIP_ISOTHERMAL_WALL .OR. &
                   patch_bcType == SAT_WALL_NOSLIP_THERMALLY_COUPLED .OR. &
                   patch_bcType == SAT_NOSLIP_ADIABATIC_WALL .OR. &
                   patch%bcType == SAT_AXISYMMETRIC ) THEN

                ! ... wall-normal vector
                norm_vec(1:MAX_ND) = (/ XI_X_TILDE, XI_Y_TILDE, XI_Z_TILDE /)

                ! ... compute the current wall-normal velocity
                V_wall_vec(1:ND) = V_DOT_XI * norm_vec(1:ND)

                ! ... target normal wall velocity
                If (input%caseID == 'USE_BCIC_WALL_VELOCITIES') Then
                  V_wall_target_vec(1:ND) = dot_product(BCIC_WALL_VEL_VEC(1:ND),norm_vec(1:ND)) * norm_vec(1:ND)
                Else
                  V_wall_target_vec(1:ND) = dot_product(XYZ_TAU(l0,1:ND),norm_vec(1:ND)) * norm_vec(1:ND)
                End If

                ! ... penalty parameter
                sigmaI1 = -SAT_sigmaI1 * dsgn

                if (input%chemistry_model == MODEL1) then
#ifdef HAVE_CANTERA

                   ! ... pull off the boundary data 
                   uB(:) = Qvs(1:ND+2)

                   ! ... target data
                   RHO_TARGET = Qvs(1)
                   UX_TARGET  = UX - (V_wall_vec(1)-V_wall_target_vec(1))
                   UY_TARGET  = UY - (V_wall_vec(2)-V_wall_target_vec(2))
                   UZ_TARGET  = UZ - (V_wall_vec(3)-V_wall_target_vec(3))
                   PRESSURE_TARGET = PRESSURE

                   ! ... compute the target data for the inviscid conditions
                   gI1(1) = RHO_TARGET
                   gI1(2) = RHO_TARGET * UX_TARGET
                   gI1(ND+2) = Qvs(ND+2) + 0.5_rfreal * RHO_TARGET * ( UX_TARGET**2 - UX**2 )
                   If (ND >= 2) Then
                      gI1(3) = RHO_TARGET * UY_TARGET
                      gI1(ND+2) = gI1(ND+2) + 0.5_rfreal * RHO_TARGET * ( UY_TARGET**2 - UY**2 )
                   End If
                   If (ND == 3) Then
                      gI1(4) = RHO_TARGET * UZ_TARGET
                      gI1(ND+2) = gI1(ND+2) + 0.5_rfreal * RHO_TARGET * ( UZ_TARGET**2 - UZ**2 )
                   End If

#endif
                else

                   ! ... pull off the boundary data 
                   uB(:) = cv(l0,:)

                   ! ... target data
                   RHO_TARGET = cv(l0,1)
                   UX_TARGET  = UX - (V_wall_vec(1)-V_wall_target_vec(1))
                   if (ND >= 2) then
                      UY_TARGET  = UY - (V_wall_vec(2)-V_wall_target_vec(2))
                   end if
                   if (ND == 3) then
                      UZ_TARGET  = UZ - (V_wall_vec(3)-V_wall_target_vec(3))
                   end if
                   PRESSURE_TARGET = PRESSURE

                   ! ... compute the target data for the inviscid conditions
                   gI1(1) = RHO_TARGET
                   gI1(2) = RHO_TARGET * UX_TARGET
                   gI1(ND+2) = cv(l0,ND+2) + 0.5_rfreal * RHO_TARGET * ( UX_TARGET**2 - UX**2 )
                   If (ND >= 2) Then
                      gI1(3) = RHO_TARGET * UY_TARGET
                      gI1(ND+2) = gI1(ND+2) + 0.5_rfreal * RHO_TARGET * ( UY_TARGET**2 - UY**2 )
                   End If
                   If (ND == 3) Then
                      gI1(4) = RHO_TARGET * UZ_TARGET
                      gI1(ND+2) = gI1(ND+2) + 0.5_rfreal * RHO_TARGET * ( UZ_TARGET**2 - UZ**2 )
                   End If

                end if ! ... if (input%chemistry_model == MODEL1)

                ! ... follow Magnus's algorithm (norm_vec has unit length)
                pnorm_vec(:) = norm_vec(1:ND) * sqrt(XI_X**2 + XI_Y**2 + XI_Z**2)

                ! ... use target state to define matrix for better implicit performance?
                if(gas_dv_model == DV_MODEL_IDEALGAS .or. gas_dv_model == DV_MODEL_IDEALGAS_MIXTURE) then
                   if (input%chemistry_model == MODEL1) then
#ifdef HAVE_CANTERA
                      Call SAT_Form_Roe_Matrices(ub, gI1, input%gamRef, pnorm_vec, Tmat, Tinv, Lambda)
#endif
                   else
                      Call SAT_Form_Roe_Matrices(ub, gI1, gamma, pnorm_vec, Tmat, Tinv, Lambda)
                   end if
                elseif(gas_dv_model == DV_MODEL_THERMALLY_PERFECT) then
                  Call SAT_Form_Roe_Matrices_TP(gI1, gI1, ND, gamma, gamref, pnorm_vec, Tmat, Tinv, Lambda)
                end if

                SpecRad = 0.0_rfreal
                ucon    = V_DOT_XI/bndry_h
                ! ... only pick those Lambda that are incoming
                ! ... also find spectral radius
                if ( sgn == 1 ) then
                  do jj = 1, ND+2
                    Lambda(jj,jj) = max(Lambda(jj,jj),0.0_rfreal)
                    SpecRad = max(abs(Lambda(jj,jj)),abs(SpecRad))
                  end do
                else 
                  do jj = 1, ND+2
                    Lambda(jj,jj) = min(Lambda(jj,jj),0.0_rfreal)
                    SpecRad = max(abs(Lambda(jj,jj)),abs(SpecRad))
                  end do
                end if

                ! ... multiply Lambda' into X
                matX = MATMUL(Lambda,Tinv)

                ! ... compute the characteristic matrix
                Aprime = MATMUL(Tmat,matX)

                ! ... subtract off the target
                ub(1:ND+2) = ub(1:ND+2) - gI1(1:ND+2)

                ! ... compute the characteristic penalty vector
                gI2(1:ND+2) = MATMUL(Aprime,uB)

                ! ... compute penalty (Bndry_h is included in Lambda already)
                penalty(:) = sigmaI1 * SBP_invPdiag_block1(1) * gI2(:)

                ! ... add the rhs
                do jj = 1, ND+2
                  rhs(l0,jj) = rhs(l0,jj) + ibfac_local * penalty(jj)
                end do

!SAT,LM:: for the species and noslip conditions I use div(ru Y) = Y*div(ru) when u is zero, for slip conditions it uses the ucon advection, which should be very small, taken from SAT_FARFIELD
                if (input%chemistry_model == MODEL1) then
#ifdef HAVE_CANTERA
                   do jj = 1, input%nAuxVars
                      if ( patch_bcType == SAT_SLIP_ADIABATIC_WALL .OR. patch%bcType == SAT_AXISYMMETRIC .OR. auxVars(l0,jj) < 0d0) then
                         if (dsgn * ucon >= 0_8) then
                            Sfactor = -ucon * dsgn * ibfac_local * SBP_invPdiag_block1(1)!
                            rhs_auxVars(l0,jj) = rhs_auxVars(l0,jj) + Sfactor*auxVars(l0,jj) 
                         endif
                      else
                         rhs_auxVars(l0,jj) = rhs_auxVars(l0,jj) + ibfac_local * penalty(1)*auxVars(l0,jj)/cv(l0,1)
                      end if
                   end do
#endif
                else
                   if ( patch_bcType == SAT_SLIP_ADIABATIC_WALL .OR. patch%bcType == SAT_AXISYMMETRIC) then
                      if (dsgn * ucon >= 0_8) then
                         Sfactor = -ucon * dsgn * ibfac_local * SBP_invPdiag_block1(1)!
                         do jj = 1, input%nAuxVars
                            rhs_auxVars(l0,jj) = rhs_auxVars(l0,jj) + Sfactor*auxVars(l0,jj)
                         end do
                      endif
                   else
                      do jj = 1, input%nAuxVars
                         rhs_auxVars(l0,jj) = rhs(l0,1)*auxVars(l0,jj)/cv(l0,1) + patch_ViscousRHSAux(jj,lp)* JAC(l0)*merge(1d0,0d0,REinv > 0d0)
                      end do
                   end if
                end if

                ! ... find spectral radius of Aprime
!                SpecRad = Abs(SpecRad)

                ! ... store Aprime if implicit
                if ( timeScheme == BDF_GMRES_IMPLICIT) then

                  ! ... intermediate values
                  gamm1i  = 1.0_rfreal / (gamma - 1.0_rfreal)
                  T       = dv(lp,2)
                  RHO_P   = gamma * gamm1i / T
                  RHO_T   = -PRESSURE / T * RHO_P

                  ! ... construct BI1
                  If (ND == 2) then
                    BI1(1,1:ND+2) = 0.0_rfreal
                    BI1(2,1) = RHO_P * (V_DOT_XI) * XI_X_TILDE
                    BI1(2,2) = RHO_TARGET * XI_X_TILDE * XI_X_TILDE
                    BI1(2,3) = RHO_TARGET * XI_Y_TILDE * XI_X_TILDE
                    BI1(2,4) = RHO_T * (V_DOT_XI) * XI_X_TILDE
                    BI1(3,1) = RHO_P * (V_DOT_XI) * XI_Y_TILDE
                    BI1(3,2) = RHO_TARGET * XI_X_TILDE * XI_Y_TILDE
                    BI1(3,3) = RHO_TARGET * XI_Y_TILDE * XI_Y_TILDE
                    BI1(3,4) = RHO_T * (V_DOT_XI) * XI_Y_TILDE
                    BI1(4,1) = 0.5_rfreal * RHO_P * (V_DOT_XI) * (V_DOT_XI)
                    BI1(4,2) = V_DOT_XI * XI_X_TILDE
                    BI1(4,3) = V_DOT_XI * XI_Y_TILDE
                    BI1(4,4) = 0.5_rfreal * RHO_T * (V_DOT_XI) * (V_DOT_XI)
                 Else If (ND == 3) then
                    BI1(1,1) = 0.0_rfreal
                    BI1(1,2) = 0.0_rfreal
                    BI1(1,3) = 0.0_rfreal
                    BI1(1,4) = 0.0_rfreal
                    BI1(1,5) = 0.0_rfreal
                    BI1(2,1) = RHO_P * (V_DOT_XI) * XI_X_TILDE
                    BI1(2,2) = RHO_TARGET * XI_X_TILDE * XI_X_TILDE
                    BI1(2,3) = RHO_TARGET * XI_Y_TILDE * XI_X_TILDE
                    BI1(2,4) = RHO_TARGET * XI_Z_TILDE * XI_X_TILDE
                    BI1(2,5) = RHO_T * (V_DOT_XI) * XI_X_TILDE
                    BI1(3,1) = RHO_P * (V_DOT_XI) * XI_Y_TILDE
                    BI1(3,2) = RHO_TARGET * XI_X_TILDE * XI_Y_TILDE
                    BI1(3,3) = RHO_TARGET * XI_Y_TILDE * XI_Y_TILDE
                    BI1(3,4) = RHO_TARGET * XI_Z_TILDE * XI_Y_TILDE
                    BI1(3,5) = RHO_T * (V_DOT_XI) * XI_Y_TILDE
                    BI1(4,1) = RHO_P * (V_DOT_XI) * XI_Z_TILDE
                    BI1(4,2) = RHO_TARGET * XI_X_TILDE * XI_Z_TILDE
                    BI1(4,3) = RHO_TARGET * XI_Y_TILDE * XI_Z_TILDE
                    BI1(4,4) = RHO_TARGET * XI_Z_TILDE * XI_Z_TILDE
                    BI1(4,5) = RHO_T * (V_DOT_XI) * XI_Z_TILDE
                    BI1(5,1) = 0.5_rfreal * RHO_P * (V_DOT_XI) * (V_DOT_XI)
                    BI1(5,2) = V_DOT_XI * XI_X_TILDE
                    BI1(5,3) = V_DOT_XI * XI_Y_TILDE
                    BI1(5,4) = V_DOT_XI * XI_Z_TILDE
                    BI1(5,5) = 0.5_rfreal * RHO_T * (V_DOT_XI) * (V_DOT_XI)
                  End if

                  ! ... multiply A and BI1
                  Aprime = MATMUL(Aprime,BI1)

                  do jj = 1, ND+2
                    do kk = 1, ND+2
                      patch_implicit_sat_mat(lp,jj,kk) = patch_implicit_sat_mat(lp,jj,kk) + ibfac_local * sigmaI1 * SBP_invPdiag_block1(1) * Aprime(jj,kk)
                    end do
                  end do
                end if

                if ( patch_bcType == SAT_NOSLIP_ISOTHERMAL_WALL .OR. patch_bcType == SAT_WALL_NOSLIP_THERMALLY_COUPLED) THEN

                  ! ... boundary data
                   if (input%chemistry_model == MODEL1) then
#ifdef HAVE_CANTERA
                      uB(1:ND+2) = Qvs(1:ND+2)
#endif
                   else
                      uB(:) = cv(l0,:)
                   end if

                  ! ... wall temperature
                  ! ---- Changes due to C.O. version ------
                  IF (patch_bcType == SAT_WALL_NOSLIP_THERMALLY_COUPLED ) then
                    ! ... non-dimensionalization done in ModRocstar.f90 :: SetInterfaceTemp
                    T_wall = patch_CellTemp(lp)
                  ELSEIF(patch_bcType == SAT_NOSLIP_ISOTHERMAL_WALL) then
                    T_wall = bcic_WallTemp / ((gamref - 1.0_8)*tempref)
                  END IF

                  ! ... target state
                  gI2(1)      = RHO_TARGET
               
                  ! ... wall velocity
                  If (input%caseID == 'USE_BCIC_WALL_VELOCITIES') Then
                    gI2(2:ND+1) = BCIC_WALL_VEL_VEC(1:ND) * RHO_TARGET
                  Else
                    gI2(2:ND+1) = XYZ_TAU(l0,1:ND) * RHO_TARGET
                  End If

                  ! ... thermally perfect or not                 
                  if (gas_dv_model == DV_MODEL_IDEALGAS) then
                    gI2(ND+2)   = RHO_TARGET * T_wall / GAMMA + 0.5_rfreal * SUM(gI2(2:ND+1)**2) / RHO_TARGET
                  else if (gas_dv_model == DV_MODEL_IDEALGAS_MIXTURE) then
                     if (input%chemistry_model == MODEL1) then
#ifdef HAVE_CANTERA
                        ! ...T_wall is dimensionless
                        gI2(ND+2)= TRhoAux2Energy(T_wall, RHO_TARGET, Qaux, combustion%model1, RemoveBEDefaultF_in=.true.) + 0.5_rfreal * SUM(XYZ_TAU(l0,1:ND)**2) * RHO_TARGET
#endif
                     else
                        gI2(ND+2)   = RHO_TARGET*(sum(auxVars(l0,:)*Mwcoeff)/cv(l0,1) + MwcoeffRho)*T_wall/GAMMA + 0.5_rfreal * SUM(gI2(2:ND+1)**2) / RHO_TARGET
                     end if
                  elseif(gas_dv_model == DV_MODEL_THERMALLY_PERFECT) then
! call TPGasLookup(input,1,T_wall,e_internal,gamma)
                    spline     => state%dvSpline(1)

! pointer dereference, WZhang 05/2014
                    spline_nxb     = spline%nxb
                    spline_dxb     = spline%dxb
                    spline_xb     => spline%xb
                    spline_coeffs => spline%coeffs
                    input_tempref  = input%tempref 

                    e_internal = evaluateSpline(spline_nxb, spline_dxb, spline_xb, spline_coeffs, 3, T_wall*input_tempref) / sndspdref2
                    gI2(ND+2)  = RHO_TARGET * e_internal + 0.5_rfreal * SUM(gI2(2:ND+1)**2) / RHO_TARGET
                  end if

                  ! ... penalty parameter (LM:: take the maxval(tv); note, tv(l0,2) < tv(l0,1), so it doesnt count)
                  if (input%chemistry_model == MODEL1) then
#ifdef HAVE_CANTERA
                     sigmaI2 = -(SAT_sigmaI2 / 4.0_rfreal) * ( SBP_invPdiag_block1(1) / bndry_h )**2  * &
                          maxval(tv(l0,1:input%heatDiffusivityIndex-1)) / Wvs(1) * MAX(input%gamRef / Pr, 5.0_rfreal / 3.0_rfreal)
                     sigmaI2 = sigmaI2 * REinv
#endif
                  else
                     sigmaI2 = -(SAT_sigmaI2 / 4.0_rfreal) * (SBP_invPdiag_block1(1)/bndry_h)**2 * &
                          maxval(tv(l0,1:)) / RHO_TARGET * MAX(GAMMA / Pr, 5.0_rfreal / 3.0_rfreal)
                     sigmaI2 = sigmaI2 * REinv
                  end if

                  ! ... compute penalty
                  penalty(:) = sigmaI2 * (uB(1:ND+2) - gI2(1:ND+2))

                  ! ... add to the rhs
                  if ( timeScheme == IMEX_RK4 ) then

                    patch_sat_fac(lp) = ibfac_local * sigmaI2 
                    do jj = 1, ND+2
                      rhs(l0,jj) = rhs(l0,jj) - ibfac_local * sigmaI2 * gI2(jj)
                    end do

                  else if ( timeScheme == BDF_GMRES_IMPLICIT ) then

                    ! ... intermediate values
                    gamm1i  = 1.0_rfreal / (gamma - 1.0_rfreal)
                    T       = dv(lp,2)
                    RHO_P   = gamma * gamm1i / T
                    RHO_T   = -PRESSURE / T * RHO_P
                    KE      = 0.5_rfreal * (UX * UX + UY * UY)

                    ! ... construct BI1
                    If (ND == 2) then
                      BI1(1,1:ND+2) = 0.0_rfreal
                      BI1(2,1) = UX * RHO_P
                      BI1(2,2) = RHO_TARGET
                      BI1(2,3) = 0.0_rfreal
                      BI1(2,4) = UX * RHO_T
                      BI1(3,1) = UY * RHO_P
                      BI1(3,2) = 0.0_rfreal
                      BI1(3,3) = RHO_TARGET
                      BI1(3,4) = UY * RHO_T
                      BI1(4,1) = gamm1i + RHO_P * (KE - T_wall * gamm1i/GAMMA)
                      BI1(4,2) = RHO_TARGET * UX
                      BI1(4,3) = RHO_TARGET * UY
                      BI1(4,4) = RHO_T * (KE - T_wall * gamm1i/GAMMA)
                   Else If (ND == 3) then
                      BI1(1,1) = 0.0_rfreal
                      BI1(1,2) = 0.0_rfreal
                      BI1(1,3) = 0.0_rfreal
                      BI1(1,4) = 0.0_rfreal
                      BI1(1,5) = 0.0_rfreal
                      BI1(2,1) = UX * RHO_P
                      BI1(2,2) = RHO_TARGET
                      BI1(2,3) = 0.0_rfreal
                      BI1(2,4) = 0.0_rfreal
                      BI1(2,5) = UX * RHO_T
                      BI1(3,1) = UY * RHO_P
                      BI1(3,2) = 0.0_rfreal
                      BI1(3,3) = RHO_TARGET
                      BI1(3,4) = 0.0_rfreal
                      BI1(3,5) = UY * RHO_T
                      BI1(4,1) = UZ * RHO_P
                      BI1(4,2) = 0.0_rfreal
                      BI1(4,3) = 0.0_rfreal
                      BI1(4,4) = RHO_TARGET
                      BI1(4,5) = UZ * RHO_T
                      BI1(5,1) = gamm1i + RHO_P * (KE - T_wall * gamm1i/GAMMA)
                      BI1(5,2) = RHO_TARGET * UX
                      BI1(5,3) = RHO_TARGET * UY
                      BI1(5,4) = RHO_TARGET * UZ
                      BI1(5,5) = RHO_T * (KE - T_wall * gamm1i/GAMMA)
                   End if

                    do jj = 1, ND+2
                      rhs(l0,jj) = rhs(l0,jj) + ibfac_local * penalty(jj)
                      ! patch%implicit_sat_mat(lp,jj,jj) = patch%implicit_sat_mat(lp,jj,jj) + ibfac_local * sigmaI2
                      do kk = 1, ND+2
                        patch_implicit_sat_mat(lp,jj,kk) = patch_implicit_sat_mat(lp,jj,kk) + ibfac_local * sigmaI2 * BI1(jj,kk)
                      end do
                    end do

                  else

                    do jj = 1, ND+2
                      rhs(l0,jj) = rhs(l0,jj) + ibfac_local * penalty(jj)
                    end do
!zero grad for species
                    sigmaAux = ibfac_local * dsgn *1_8* SBP_invPdiag_block1(1) * JAC(l0)
                    do jj = 1, nAuxVars
                      ubAux = patch_ViscousFluxAux(jj,lp,normDir) 
                      rhs_auxVars(l0,jj) = rhs_auxVars(l0,jj) + sigmaAux* ubAux
                    end do


                  end if !IMEX_RK4

                end if

                
                if ( patch_bcType == SAT_NOSLIP_ADIABATIC_WALL) THEN

                  ! ... boundary data
                   if (input%chemistry_model == MODEL1) then
#ifdef HAVE_CANTERA
                      uB(:) = Qvs(1:ND+2)
#endif
                   else
                      uB(:) = cv(l0,:)
                   end if

                  ! ... target state
                  gI2(1)      = RHO_TARGET
                  gI2(2:ND+1) = XYZ_TAU(l0,1:ND) * RHO_TARGET

                  ! ... penalty parameter
                  if (input%chemistry_model == MODEL1) then
#ifdef HAVE_CANTERA
                     ! ... penalty parameter
                     sigmaI2 = -(SAT_sigmaI2 / 4.0_rfreal) * ( SBP_invPdiag_block1(1) / bndry_h )**2  * &
                          maxval(tv(l0,1:input%heatDiffusivityIndex-1))/ RHO_TARGET *  MAX(input%gamref / Pr, 5.0_rfreal / 3.0_rfreal)
                     sigmaI2 = sigmaI2 * REinv
#endif
                  else
                     sigmaI2 = -(SAT_sigmaI2 / 4.0_rfreal) * (SBP_invPdiag_block1(1)/bndry_h)**2 * &
                          tv(l0,1) / RHO_TARGET *  MAX(GAMMA / Pr, 5.0_rfreal / 3.0_rfreal)
                     sigmaI2 = sigmaI2 * REinv
                  end if

                  ! ... compute penalty
                  penalty(:) = sigmaI2 * (uB(1:ND+2) - gI2(1:ND+2))

                  do jj = 1, ND+1
                    rhs(l0,jj) = rhs(l0,jj) + ibfac_local * penalty(jj)
                  end do

                  sigmaAux = ibfac_local * dsgn *1_8* SBP_invPdiag_block1(1) * JAC(l0)
                  jj = ND+2
                  rhs(l0, jj) = rhs(l0,jj) + sigmaAux*patch_ViscousFlux(jj,lp,normDir)

                  do jj = 1, nAuxVars
                    ubAux = patch_ViscousFluxAux(jj,lp,normDir) 
                    rhs_auxVars(l0,jj) = rhs_auxVars(l0,jj) + sigmaAux* ubAux
                  end do

                end if

!LM> zero the gradient if SAT_SLIP_ADIABATIC_WALL
                if(patch_bcType == SAT_SLIP_ADIABATIC_WALL .OR.  patch%bcType == SAT_AXISYMMETRIC ) then

                  if ( REinv > 0.0_rfreal ) then
                    sigmaI2 = ibfac_local * dsgn * 1_8 * SBP_invPdiag_block1(1)  * JAC(l0)
                    gI2 = 0_8
                    uB(:) = patch_ViscousFlux(:,lp,normDir)
                    penalty(:) = sigmaI2 * (uB(:) - gI2(:))
                    penalty(normDir+1) = 0_8   !normal momentum
                    do jj = 1, ND+2
                      rhs(l0,jj) = rhs(l0,jj) + penalty(jj)
                    end do
                    do jj = 1, nAuxVars
                      ubAux = patch_ViscousFluxAux(jj,lp,normDir) 
                      rhs_auxVars(l0,jj) = rhs_auxVars(l0,jj) + sigmaI2* ubAux
                    end do
                  endif !REinv > 0.0_rfreal
                end if  !SAT_SLIP_ADIABATIC_WALL
                
              end if

            end do
          end do
        end do

      else if (patch_bcType == SPONGE .or. patch_bcType == SPONGE_WITH_LINEARIZED_DISTURBANCE) then

        Do k = patch_is(3), patch_ie(3)
          Do j = patch_is(2), patch_ie(2)
            Do i = patch_is(1), patch_ie(1)

              l0 = (k- grid_is(3))* N(1)* N(2) + (j- grid_is(2))* N(1) +  i- grid_is(1)+1
              lp = (k-patch_is(3))*Np(1)*Np(2) + (j-patch_is(2))*Np(1) + (i-patch_is(1)+1)

              ! ... distance from boundary to inner sponge start
              rel_pos(1:ND) = patch_sponge_xs(lp,1:ND) - patch_sponge_xe(lp,1:ND)
              del = sqrt(dot_product(rel_pos(1:ND),rel_pos(1:ND)))

              ! ... distance from inner sponge start to point
              if(input%useHDF5 /= 1) then
                 ! sponge distance based on physical space
                 rel_pos(1:ND) = patch_sponge_xs(lp,1:ND) - XYZ(l0,1:ND)
              else
                 ! sponge distance based on integer spacing
                 ijkInd(:) = (/i, j, k/)
                 rel_pos(1:ND) = patch_sponge_xs(lp,1:ND) - DBLE(ijkInd(1:ND))
              end if
              eta = sqrt(dot_product(rel_pos(1:ND),rel_pos(1:ND))) / del

              if (patch_bcType == SPONGE_WITH_LINEARIZED_DISTURBANCE .and. eta .gt. 1.0_rfreal) eta = 1.0_rfreal

              sponge_amp = 0.0_rfreal
              if (eta .le. 1.0_rfreal) sponge_amp = (input_sponge_amp) * eta**(input_sponge_pow)

              if (iblank(l0) /= 0) then
                rhs(l0,:) = rhs(l0,:) - sponge_amp * (cv(l0,:) - cvTarget(l0,:))
                do jj = 1, nAuxVars
                  rhs_auxVars(l0,jj) = rhs_auxVars(l0,jj) - sponge_amp * (auxVars(l0,jj) - auxVarsTarget(l0,jj))
                end do
              else
                rhs(l0,:) = 0.0_rfreal
                do jj = 1, nAuxVars
                  rhs_auxVars(l0,jj) = 0.0_rfreal
                end do
              end if

!!$              !if ( input%timeScheme == BDF_GMRES_IMPLICIT ) then
!!$
!!$                ! ... add to LHS
!!$                do jj = 1, grid%ND+2
!!$                  patch%implicit_sat_mat(lp,jj,jj) = patch%implicit_sat_mat(lp,jj,jj) + sponge_amp
!!$                end do
!!$
!!$              end if

            end do
          end do
        end do

      end if

    end do

    deallocate(gvec,inorm,SC,Pinv,Pfor,Tmat,Tinv)
    deallocate(cons_inviscid_flux,L_vector,new_normal_flux, pnorm_vec)
    deallocate(uB, gI1, gI2, Aprime, Lambda, matX, matXTrans, penalty)
    deallocate(Lambda_in, Lambda_out,ViscFluxJac,BI1)

    return

  end subroutine NS_BC

  subroutine NS_Update_Target(region, ng)

    USE ModGlobal
    USE ModDataStruct
    USE ModDeriv
    USE ModMPI
    USE ModIO

    Implicit None

    TYPE (t_region), POINTER :: region
    INTEGER :: ng
    INTEGER :: flags

    ! ... local variables
    type(t_grid), pointer :: grid
    type(t_mixt), pointer :: state
    type(t_mixt_input), pointer :: input
    type(t_patch), pointer :: patch
    real(rfreal), pointer :: cvTarget(:,:), cvTargetOld(:,:), XYZ(:,:)
    integer :: i, j, k, l0, lp, npatch, ND, N(MAX_ND), Np(MAX_ND), p, q, jj, ii, nCells
    integer :: iModeTemp, iModeAzi, num_rndNum, count_rndNum, nSubStep, iSeed(12), NM
    real(rfreal) :: RHO, UX, UY, UZ, GAMMA, PRESSURE, TEMPERATURE
    real(rfreal) :: BASE_RHO, BASE_UX, BASE_UY, BASE_UZ, BASE_PRESSURE, BASE_TEMPERATURE, SPD_SND
    real(rfreal) :: epsilon, omega, sincos_arg, to_rad, angle, Heaviside, heaviside_arg
    real(rfreal) :: rnd_num, dphase, exp_arg
    complex(rfreal), parameter :: eye = (0.0_rfreal, 1.0_rfreal)
    complex(rfreal) :: qPrime(MAX_ND+2), Greens_Function, cdexp_arg, hankel_arg, hankel_res(0:1)
    real(rfreal) :: phi, theta, time, nhat(MAX_ND), x0(MAX_ND), nhat_dot_x
    real(rfreal) :: x_1, x_2, x_m, xi, f, t0, v_amp, M_inf ! ... specific to T-S wave simulation
    Real(rfreal) :: Mach, pinf, Tinf, Tjet, pjet, rjet, cjet, ujet, radius, rinf, delta1, amp
    Real(rfreal) :: tramp, tstart, tanh_fac, eta, M1, Xcomp, Ycomp, Zcomp, xx, yy, waveno, pprime, rhoprime, beta, sqrt_fac
    complex(rfreal) :: CHF1(0:2), CHD1(0:2), CHF2(0:2), CHD2(0:2), ddx_Greens_Function, ddy_Greens_Function
    Real(rfreal) :: ZBESH_ZR, ZBESH_ZI, ZBESH_FNU, ZBESH_CYR(2), ZBESH_CYI(2)
    Integer :: ZBESH_KODE, ZBESH_M, ZBESH_N, ZBESH_IERR, ZBESH_NZ

    ! ... simplicity
    grid  => region%grid(ng)
    state => region%state(ng)
    input => grid%input
    ND     = input%ND
    to_rad = TWOPI / 360.0_rfreal
    cvTarget => state%cvTarget
    cvTargetOld => state%cvTargetOld
    XYZ => grid%XYZ
    nCells = grid%nCells

    ! ... by default, set the current target to the 'saved' version
    Do k = 1, size(state%cv,2)
      Do l0 = 1, nCells
        cvTarget(l0,k) = cvTargetOld(l0,k)
      End Do
    End Do

    ! ... 2-D acoustic point source at constant frequency in a mean flow in x-direction
    If (input%caseID == 'WSMR') Then
      x0(1)   = input%initflow_xloc
      x0(2)   = input%initflow_yloc
      Mach    = input%initflow_mach
      omega   = input%initflow_frequency
      gamma   = input%gamref
      beta    = 1.0_8 - Mach**2
      time    = state%time(1)
      amp     = input%initflow_amplitude
      ZBESH_ZI = 0.0_8
      ZBESH_FNU = 0.0_8
      ZBESH_KODE = 1
      ZBESH_M = 1
      ZBESH_N = 2
      Do ii = 1, nCells
        BASE_RHO = cvTargetOld(ii,1)
        BASE_UX  = cvTargetOld(ii,2) / BASE_RHO 
        BASE_UY  = cvTargetOld(ii,3) / BASE_RHO
        BASE_PRESSURE = (GAMMA-1.0_rfreal) * (cvTargetOld(ii,ND+2) - 0.5_rfreal * (BASE_UX**2 + BASE_UY**2) * BASE_RHO)
        SPD_SND = Sqrt(GAMMA * BASE_PRESSURE / BASE_RHO)
        waveno  = omega / SPD_SND
        xx      = XYZ(ii,1)
        yy      = XYZ(ii,2)
        cdexp_arg = -eye * waveno * Mach * (xx - x0(1)) / beta
        sqrt_fac  = dsqrt((xx-x0(1))**2 + beta*(yy-x0(2))**2)
        ZBESH_ZR = waveno / beta * sqrt_fac
        Call ZBESH(ZBESH_ZR, ZBESH_ZI, ZBESH_FNU, ZBESH_KODE, ZBESH_M, ZBESH_N, ZBESH_CYR, ZBESH_CYI, ZBESH_NZ, ZBESH_IERR)
        hankel_res(0) = dcmplx(ZBESH_CYR(1), ZBESH_CYI(1))
        hankel_res(1) = dcmplx(ZBESH_CYR(2), ZBESH_CYI(2))
!hankel_arg = dcmplx(waveno / beta * sqrt_fac,0.0_8)
!Call CH12N(1,hankel_arg,NM,CHF1,CHD1,CHF2,CHD2)
!hankel_res(0) = CHF1(0)
!hankel_res(1) = CHF1(1)
        Greens_Function = amp * eye * 0.25_8 * cdexp(cdexp_arg) * hankel_res(0)

        ! ... computed from Mathematica
        ddx_Greens_Function = amp * cdexp(cdexp_arg) * waveno * ( -Mach * hankel_res(0) + eye * (xx - x0(1)) * hankel_res(1) / sqrt_fac) / (4.0_8 * (Mach**2 - 1.0_8))

        ! ... computed from Mathematica
        ddy_Greens_Function = - amp * eye * cdexp(cdexp_arg) * waveno * (yy-x0(2)) * hankel_res(1) / (4.0_8 * sqrt_fac)

        ! ... compute derived perturbations
        PRESSURE = dble(Greens_Function * cdexp(-eye * omega * time))
        RHO      = PRESSURE / SPD_SND**2
        UX       = dble(1.0_8 / (eye * BASE_RHO * omega) * ddx_Greens_Function * cdexp(-eye * omega * time))
        UY       = dble(1.0_8 / (eye * BASE_RHO * omega) * ddy_Greens_Function * cdexp(-eye * omega * time))
        cvTarget(ii,1) = BASE_RHO + RHO
        cvTarget(ii,2) = cvTarget(ii,1) * (BASE_UX + UX)
        cvTarget(ii,3) = cvTarget(ii,1) * (BASE_UY + UY)
        cvTarget(ii,4) = (BASE_PRESSURE + PRESSURE) / (gamma - 1.0_8) + 0.5_8 * (cvTarget(ii,2)**2 + cvTarget(ii,3)**2) / cvTarget(ii,1)
      End Do
    End If

    ! ... slowly increasing freestream Mach number
    If (input%caseID == 'UNIFORM_TIME_RAMP') Then

      M1 = 0.5_rfreal * input%initflow_mach * (1.0_rfreal + tanh((state%time(1) - input%initflow_tstart)/input%initflow_tramp))
      gamma = input%GamRef

      ! ... calculate the directional modifiers
      Xcomp = COS(input%initflow_phi) * SIN(input%initflow_theta)
      Ycomp = SIN(input%initflow_phi) * SIN(input%initflow_theta)
      Zcomp = COS(input%initflow_theta)

      do ii = 1, size(state%cv,1)

        ! ... initialize flow velocity with specified direction
        state%cvTarget(ii,1) = 1.0_rfreal
        state%cvTarget(ii,2) = M1 * Xcomp * state%cvTarget(ii,1)
        IF(grid%ND >= 2) THEN
          state%cvTarget(ii,3) =  M1 * Ycomp * state%cvTarget(ii,1)
          IF(grid%ND > 2) THEN
            state%cvTarget(ii,4) = M1 * Zcomp * state%cvTarget(ii,1)
          ENDIF
        ENDIF

        ! ... Initialize E 
        state%cvTarget(ii,grid%ND+2) = (1.0_rfreal/gamma)/(gamma-1.0_rfreal)
        ! ... add kinetic energy to E
        do j = 1, grid%ND
          state%cvTarget(ii,grid%ND+2) = state%cvTarget(ii,grid%ND+2) + 0.5_8 * state%cvTarget(ii,j+1) * state%cvTarget(ii,j+1) / state%cvTarget(ii,1)
        end do

      end do


    End If

    ! ... NASA Jet
    if (Trim(input%initflow_name) == "NASA_JET") then

      ! ... compute the target state first 
      gamma  = input%gamref
      tstart = input%initflow_tstart
      tramp  = input%initflow_tramp
      Mach   = 0.5_8 * input%initflow_Mach * (1.0_8 + tanh((state%time(1) - tstart)/tramp))
      delta1 = input%initflow_delta
      rinf   = 1.0_8
      pinf   = 1.0_8 / gamma
      Tinf   = 1.0_8 / (gamma - 1.0_8)
      Tjet   = Tinf / (1.0_8 + 0.5_8 * (gamma-1.0_8) * Mach**2)
      pjet   = pinf
      rjet   = gamma * pjet / (gamma - 1.0_8) / Tjet
      cjet   = dsqrt(gamma * pjet / rjet)
      ujet   = Mach * cjet   

      do ii = 1, size(state%cv,1)

        ! ... compute radius 
        radius = dsqrt(grid%XYZ(ii,1)**2+grid%XYZ(ii,2)**2)
        eta = (0.5_8 - radius) / delta1

        ! ... check if in nozzle
        if (radius <= 0.5_8 .and. grid%XYZ(ii,3) <= 0.0_8) then
          if (eta <= 1.0_8) then
            state%cvTarget(ii,1) = rjet
            state%cvTarget(ii,2) = rjet * 0.0_8
            state%cvTarget(ii,3) = rjet * 0.0_8
            state%cvTarget(ii,4) = rjet * ujet * (1.5_8 * eta - 0.5_8 * eta**3)
            state%cvTarget(ii,5) = pjet / (gamma - 1.0_8)
          else
            state%cvTarget(ii,1) = rjet
            state%cvTarget(ii,2) = rjet * 0.0_8
            state%cvTarget(ii,3) = rjet * 0.0_8
            state%cvTarget(ii,4) = rjet * ujet
            state%cvTarget(ii,5) = pjet / (gamma - 1.0_8)
          end if
        else
          state%cvTarget(ii,1) = rinf
          state%cvTarget(ii,2) = rinf * 0.0_8
          state%cvTarget(ii,3) = rinf * 0.0_8
          state%cvTarget(ii,4) = rinf * 0.01_8
          state%cvTarget(ii,5) = pinf / (gamma - 1.0_8)
        end if

        ! ... compute total energy
        do j = 1, grid%ND
          state%cvTarget(ii,5) = state%cvTarget(ii,5) + 0.5_rfreal * state%cvTarget(ii,1+j)**2 / state%cvTarget(ii,1)
        end do

      end do

      return

    end if

    ! ... PLANE WAVE EXCITATION --- update target
    If ( input%bcic_planewave == TRUE ) then

      ! ... parameters
      epsilon = 10.0_rfreal**(input%bcic_amplitude/20.0_rfreal - 9.701_rfreal)
      omega   = input%bcic_frequency * TWOPI * input%LengRef / input%SndSpdRef
      theta   = input%bcic_angle_theta * to_rad
      phi     = input%bcic_angle_phi   * to_rad

      ! ... wavenumber orientation
      nhat(1)  = sin(phi) * cos(theta)
      nhat(2)  = sin(phi) * sin(theta)
      nhat(3)  = cos(phi)

      ! ... point defining location of t-(nhat . (x-x0)/c0) = 0 at time = 0
      x0(1) = 0.0_rfreal
      x0(2) = 0.0_rfreal
      x0(3) = 1.0_rfreal

      ! ... update the interior target data
      Do l0 = 1, size(state%cv,1)

        ! ... ratio of specific heats
        GAMMA = state%gv(l0,1)

        ! ... time
        time  = state%time(l0)

        ! ... base values
        BASE_RHO = state%cvTargetOld(l0,1)
        BASE_UX  = state%cvTargetOld(l0,2) / BASE_RHO; BASE_UY = 0.0_rfreal; BASE_UZ = 0.0_rfreal;
        if (ND >= 2) BASE_UY  = state%cvTargetOld(l0,3) / BASE_RHO
        if (ND >= 3) BASE_UZ  = state%cvTargetOld(l0,4) / BASE_RHO
        BASE_PRESSURE = (GAMMA-1.0_rfreal) * (state%cvTargetOld(l0,ND+2) - 0.5_rfreal * (BASE_UX**2+BASE_UY**2+BASE_UZ**2) * BASE_RHO)
        BASE_TEMPERATURE = GAMMA * BASE_PRESSURE / (GAMMA-1.0_rfreal) / BASE_RHO
        SPD_SND = Sqrt(GAMMA * BASE_PRESSURE / BASE_RHO)

        ! ... wave direction dotted into position
        nhat_dot_x = 0.0_rfreal
        do j = 1, grid%ND
          nhat_dot_x = nhat_dot_x + nhat(j) * grid%XYZ(l0,j)
        end do

        ! ... limit plane wave by Heaviside
        heaviside_arg = time
        do j = 1, grid%ND
          heaviside_arg = heaviside_arg - nhat(j) * (grid%XYZ(l0,j) - x0(j)) / SPD_SND
        end do
        Heaviside = 0.0_rfreal
        if (heaviside_arg >= 0.0_rfreal) Heaviside = 1.0_rfreal

        ! ... plane wave
        sincos_arg = omega * (time - nhat_dot_x / SPD_SND)
        RHO        = epsilon * cos(sincos_arg) * Heaviside
        PRESSURE   = RHO * SPD_SND**2
        UX         = nhat(1) * PRESSURE / (BASE_RHO * SPD_SND)
        UY         = nhat(2) * PRESSURE / (BASE_RHO * SPD_SND)
        UZ         = nhat(3) * PRESSURE / (BASE_RHO * SPD_SND)

        ! ... add in the wave
        state%cvTarget(l0,1) = BASE_RHO + RHO
        state%cvTarget(l0,2) = state%cvTarget(l0,1) * ( UX + BASE_UX )
        if (ND >= 2) state%cvTarget(l0,3) = state%cvTarget(l0,1) * ( UY + BASE_UY )
        if (ND == 3) state%cvTarget(l0,4) = state%cvTarget(l0,1) * ( UZ + BASE_UZ )
        state%cvTarget(l0,ND+2) = (BASE_PRESSURE + PRESSURE) / (GAMMA-1.0_rfreal) + 0.5_rfreal * ((BASE_UX+UX)**2+(BASE_UY+UY)**2+(BASE_UZ+UZ)**2) * state%cvTarget(l0,1)

      End Do

    End If

    ! ... update the boundary patch data
    Do npatch = 1, region%npatches

      patch => region%patch(npatch)
      grid => region%grid(patch%gridID)
      state => region%state(patch%gridID)

      N  = 1; Np = 1;
      do j = 1, grid%ND
        N(j)  = grid%ie(j)-grid%is(j)+1
        Np(j) = patch%ie(j) - patch%is(j) + 1
      end do;

      If ( (patch%bcType == NSCBC_INFLOW_VELOCITY_TEMPERATURE_FROM_SPONGE) .OR. &
           (patch%bcType == NSCBC_INFLOW_VELOCITY_TEMPERATURE) .OR. &
           (patch%bcType == NSCBC_INFLOW_VELOCITY_TEMPERATURE_FORCING_SLOT)) then ! ... JKim 06/2008

        Do k = patch%is(3), patch%ie(3)
          Do j = patch%is(2), patch%ie(2)
            Do i = patch%is(1), patch%ie(1)

              l0 = (k- grid%is(3))* N(1)* N(2) + (j- grid%is(2))* N(1) + (i- grid%is(1)+1)
              lp = (k-patch%is(3))*Np(1)*Np(2) + (j-patch%is(2))*Np(1) + (i-patch%is(1)+1)

              if ( grid%iblank(l0) == 0 ) CYCLE

              ! ... ratio of specific heats
              GAMMA = state%gv(l0,1)

              ! ... time
              time  = state%time(l0)

              ! ... base values
              BASE_RHO = state%cvTargetOld(l0,1)
              BASE_UX  = state%cvTargetOld(l0,2) / BASE_RHO; BASE_UY = 0.0_rfreal; BASE_UZ = 0.0_rfreal;
              if (ND >= 2) BASE_UY  = state%cvTargetOld(l0,3) / BASE_RHO
              if (ND >= 3) BASE_UZ  = state%cvTargetOld(l0,4) / BASE_RHO
              BASE_PRESSURE = (GAMMA-1.0_rfreal) * (state%cvTargetOld(l0,ND+2) - 0.5_rfreal * (BASE_UX**2+BASE_UY**2+BASE_UZ**2)*BASE_RHO)
              BASE_TEMPERATURE = GAMMA * BASE_PRESSURE / (GAMMA-1.0_rfreal) / BASE_RHO
              SPD_SND = Sqrt(GAMMA * BASE_PRESSURE / BASE_RHO)

              ! ... wave direction dotted into position
              nhat_dot_x = 0.0_rfreal
              do jj = 1, grid%ND
                nhat_dot_x = nhat_dot_x + nhat(jj) * grid%XYZ(l0,jj)
              end do

              ! ... limit plane wave by Heaviside
              heaviside_arg = time
              do jj = 1, grid%ND
                heaviside_arg = heaviside_arg - nhat(jj) * (grid%XYZ(l0,jj) - x0(jj)) / SPD_SND
              end do
              Heaviside = 0.0_rfreal
              if (heaviside_arg >= 0.0_rfreal) Heaviside = 1.0_rfreal


              patch%dudt_inflow(lp) = 0.0_rfreal
              If (ND >= 2) patch%dvdt_inflow(lp) = 0.0_rfreal
              If (ND == 3) patch%dwdt_inflow(lp) = 0.0_rfreal
              patch%dTdt_inflow(lp) = 0.0_rfreal

              If ( (input%bcic_planewave == TRUE) ) then

                ! ... plane wave
                ! ... plane wave
                sincos_arg = omega * (time - nhat_dot_x / SPD_SND)
                RHO        = epsilon * cos(sincos_arg) * Heaviside
                PRESSURE   = RHO * SPD_SND**2
                UX         = nhat(1) * PRESSURE / (RHO * SPD_SND)
                UY         = nhat(2) * PRESSURE / (RHO * SPD_SND)
                UZ         = nhat(3) * PRESSURE / (RHO * SPD_SND)

                ! ... time derivative
                patch%dudt_inflow(lp) = -omega * UX * Heaviside
                if (ND >= 2) patch%dvdt_inflow(lp) = -omega * UY * Heaviside
                if (ND == 3) patch%dwdt_inflow(lp) = -omega * UZ * Heaviside
                patch%dTdt_inflow(lp) = -omega * BASE_TEMPERATURE * ( PRESSURE / BASE_PRESSURE - RHO / BASE_RHO ) * Heaviside

              End If

              if ( patch%bcType == NSCBC_INFLOW_VELOCITY_TEMPERATURE_FROM_SPONGE ) then
                patch%u_inflow(lp) = state%cvTarget(l0,2) / state%cvTarget(l0,1)
                if (ND >= 2) patch%v_inflow(lp) = state%cvTarget(l0,3) / state%cvTarget(l0,1)
                if (ND == 3) patch%w_inflow(lp) = state%cvTarget(l0,4) / state%cvTarget(l0,1)
                PRESSURE = (GAMMA-1.0_rfreal) * (state%cvTarget(l0,ND+2) - 0.5_rfreal * state%cvTarget(l0,1) * patch%u_inflow(lp)**2)
                if (ND >= 2) PRESSURE = PRESSURE - (GAMMA-1.0_rfreal) * 0.5_rfreal * state%cvTarget(l0,1)* patch%v_inflow(lp)**2
                if (ND == 3) PRESSURE = PRESSURE - (GAMMA-1.0_rfreal) * 0.5_rfreal * state%cvTarget(l0,1)* patch%w_inflow(lp)**2
                patch%T_inflow(lp) = GAMMA * PRESSURE / (GAMMA - 1.0_rfreal) / state%cvTarget(l0,1)
              else
                patch%u_inflow(lp) = 0.1_rfreal
                if (ND >= 2) patch%v_inflow(lp) = 0.0_rfreal
                if (ND == 3) patch%w_inflow(lp) = 0.0_rfreal
                patch%T_inflow(lp) = 1.0_rfreal / (GAMMA - 1.0_rfreal)
              end if

            End Do
          End Do
        End Do

      Else If (patch%bcType == SPONGE_WITH_LINEARIZED_DISTURBANCE) Then

        ! ... phase jittering based on random walk (Lui, Ph.D thesis)
        ! ... assuming the phase jittering started at the first time step, 
        ! ... generate random numbers up to the current time step
        ! ... we don't bother to store the last phase or something since 
        ! ... a large number of random numbers (O(10^6)) can be generated 
        ! ... within tens of seconds
        
!if (first_run) then
        if (.not. region%nsbcInit) then
          ! ... calculate total random number count up to now
          num_rndNum = (region%global%main_ts_loop_index-1) &
               * (patch%LST_num_mode_azimuthal-1) &
               * (patch%LST_num_mode_temporal)
          select case( input%timeScheme )
          case( EXPLICIT_RK4 )
            nSubStep = 4
          case( EXPLICIT_RK5 )
            nSubStep = 5
          case default
            call graceful_exit(region%myrank, "... ERROR: need number of substeps for LST phase jittering.")
          end select ! input%timeScheme
          num_rndNum = num_rndNum * nSubStep
          
          ! ... seed a random number sequence; make sure it's exactly same seed everytime
          iSeed(1) = 1
          call random_seed(put=iSeed)
          
          ! ... initial phase and phase-varying direction
          patch%LST_phase(:,:) = 0.0_rfreal
          patch%LST_phase_dir(:,:) = 1.0_rfreal
          
          ! ... if main_ts_loop_index = 1, skip the next; unless, generate lots of random numbers
          if (num_rndNum > 0) then
            count_rndNum = 0 ! ... count random number generation and compare with num_rndNum
            do p = 1,region%global%main_ts_loop_index-1
              do q = 1,nSubStep

!Do iModeAzi = 1,patch%LST_num_mode_azimuthal ! ... 0th azimuthal mode not included
                Do iModeAzi = 2,patch%LST_num_mode_azimuthal
                  Do iModeTemp = 1,patch%LST_num_mode_temporal
                    
                    ! ... phase change is mild (i.e. 10%)
                    dphase = 0.1_rfreal * patch%LST_freq_angular(iModeTemp) * state%dt(1)
                    
                    call random_number(rnd_num); count_rndNum = count_rndNum + 1
                    if (rnd_num < 0.65_rfreal) then ! ... move in the same direction (65%)
                      patch%LST_phase(iModeTemp,iModeAzi) = patch%LST_phase(iModeTemp,iModeAzi) &
                           + patch%LST_phase_dir(iModeTemp,iModeAzi) * dphase
                    else if (rnd_num > 0.85_rfreal) then ! ... move in the opposite direction (15%)
                      patch%LST_phase_dir(iModeTemp,iModeAzi) = - patch%LST_phase_dir(iModeTemp,iModeAzi)
                      patch%LST_phase(iModeTemp,iModeAzi) = patch%LST_phase(iModeTemp,iModeAzi) &
                           + patch%LST_phase_dir(iModeTemp,iModeAzi) * dphase
                    end if ! rnd_num
                    
                  End Do ! iModeTemp
                End Do ! iModeAzi
                
              end do ! q
            end do ! p
            if (count_rndNum /= num_rndNum) & ! ... defensive programming
                 call graceful_exit(region%myrank, "... ERROR: random number generation cannot be restarted in LST.")
          end if ! num_rndNum
          
!first_run = .false.
          region%nsbcInit = .true.
        end if ! first_run
        
!Do iModeAzi = 1,patch%LST_num_mode_azimuthal ! ... 0th azimuthal mode not included
        Do iModeAzi = 2,patch%LST_num_mode_azimuthal
          Do iModeTemp = 1,patch%LST_num_mode_temporal
            
            ! ... phase change is mild (i.e. 10%)
            dphase = 0.1_rfreal * patch%LST_freq_angular(iModeTemp) * state%dt(1)
            
            call random_number(rnd_num)
            if (rnd_num < 0.65_rfreal) then ! ... move in the same direction (65%)
              patch%LST_phase(iModeTemp,iModeAzi) = patch%LST_phase(iModeTemp,iModeAzi) &
                   + patch%LST_phase_dir(iModeTemp,iModeAzi) * dphase
            else if (rnd_num > 0.85_rfreal) then ! ... move in the opposite direction (15%)
              patch%LST_phase_dir(iModeTemp,iModeAzi) = - patch%LST_phase_dir(iModeTemp,iModeAzi)
              patch%LST_phase(iModeTemp,iModeAzi) = patch%LST_phase(iModeTemp,iModeAzi) &
                   + patch%LST_phase_dir(iModeTemp,iModeAzi) * dphase
            end if ! rnd_num
            
            exp_arg = - patch%LST_freq_angular(iModeTemp) * state%time(1) & ! ... omega * t
                 + patch%LST_phase(iModeTemp,iModeAzi)
            
            Do k = patch%is(3), patch%ie(3)
              Do j = patch%is(2), patch%ie(2)
                Do i = patch%is(1), patch%ie(1)
                  
                  l0 = (k- grid%is(3))* N(1)* N(2) + (j- grid%is(2))* N(1) + (i- grid%is(1)+1)
                  lp = (k-patch%is(3))*Np(1)*Np(2) + (j-patch%is(2))*Np(1) + (i-patch%is(1)+1)
                  
                  qPrime(:) = (0.0_rfreal, 0.0_rfreal)
                  qPrime(1) = patch%LST_eigenf(lp,iModeTemp,iModeAzi,1)
                  Do p = 2,ND+1
                    qPrime(p) = patch%LST_eigenf(lp,iModeTemp,iModeAzi,p)
                  End Do ! p
                  qPrime(ND+2) = patch%LST_eigenf(lp,iModeTemp,iModeAzi,ND+2)
                  
                  qPrime(:) = patch%LST_amp * REAL(qPrime(:) * EXP(eye * exp_arg))
                  
                  ! ... add in the wave
                  state%cvTarget(l0,1) = state%cvTarget(l0,1) + REAL(qPrime(1))
                  state%cvTarget(l0,2) = state%cvTarget(l0,2) + REAL(qPrime(2))
                  if (ND >= 2) state%cvTarget(l0,3) = state%cvTarget(l0,3) + REAL(qPrime(3))
                  if (ND == 3) state%cvTarget(l0,4) = state%cvTarget(l0,4) + REAL(qPrime(4))
                  state%cvTarget(l0,ND+2) = state%cvTarget(l0,ND+2) + REAL(qPrime(ND+2))
                  
                End Do ! i
              End Do ! j
            End Do ! k
            
          End Do ! iModeTemp
        End Do ! iModeAzi
        
      End If ! patch%bcType
    End Do ! npatch

    !           ! ... add time-dependence to 'ramp' up values
    !           t0     = 5.0_rfreal
    !           region%global%delta  = 1.0_rfreal
    !           w_of_t = 0.5_rfreal * (1.0_rfreal + tanh((state%time(l0) - t0)/region%global%delta))
    !           dwdt   = 0.5_rfreal / (region%global%delta * (cosh((state%time(l0)-t0)/region%global%delta))**2)
    !           w_of_t = 1.0_rfreal
    !           dwdt   = 0.0_rfreal

  end subroutine NS_Update_Target

  subroutine SAT_Form_Roe_Matrices(u_in, u_out, gamma, norm, tmat, tinv, lambda)

    USE ModGlobal

    Implicit None

    Real(rfreal), Pointer :: u_in(:), u_out(:), tmat(:,:), tinv(:,:), norm(:), lambda(:,:)
    Real(rfreal) :: gamma, rho_in, ux_in, uy_in, uz_in, ke_in, p_in, en_in, h_in
    Real(rfreal) :: gm1, rho_out, ux_out, uy_out, uz_out, ke_out, p_out, en_out, h_out
    Real(rfreal) :: cc, bb, ux_Roe, uy_Roe, uz_Roe, h_Roe, ke_Roe, spd_snd_Roe
    Real(rfreal) :: XI_X_TILDE, XI_Y_TILDE, XI_Z_TILDE, ucon, rho, alpha, theta, beta, phi2
    Real(rfreal) :: XI_X, XI_Y, XI_Z, denom
    Integer :: N, ND, jj

    ! ... size of this problem
    ND = size(u_in) - 2

    ! ... constants
    gm1 = gamma - 1.0_rfreal

    ! ... form primative variables from "in"
    rho_in = u_in(1)
    ux_in  = u_in(2) / rho_in; uy_in = 0.0_rfreal; uz_in = 0.0_rfreal
    if (ND >= 2) uy_in  = u_in(3) / rho_in
    if (ND == 3) uz_in  = u_in(4) / rho_in
    ke_in  = 0.5_rfreal * (ux_in**2 + uy_in**2 + uz_in**2)
    en_in  = u_in(ND+2)/rho_in - ke_in
    p_in   = gm1 * rho_in * en_in
    h_in   = (gamma / gm1) * p_in / rho_in + ke_in 

    ! ... form primative variables from "out"
    rho_out = u_out(1)
    ux_out  = u_out(2) / rho_out; uy_out = 0.0_rfreal; uz_out = 0.0_rfreal
    if (ND >= 2) uy_out  = u_out(3) / rho_out
    if (ND == 3) uz_out  = u_out(4) / rho_out
    ke_out  = 0.5_rfreal * (ux_out**2 + uy_out**2 + uz_out**2)
    en_out  = u_out(ND+2)/rho_out - ke_out
    p_out   = gm1 * rho_out * en_out
    h_out   = (gamma / gm1) * p_out / rho_out + ke_out 

    ! ... form Roe variables
    cc = sqrt(rho_out / rho_in)
    bb = 1.0_rfreal / (1.0_rfreal + cc)
    ux_Roe = (Ux_in + Ux_out * cc) * bb
    uy_Roe = (Uy_in + Uy_out * cc) * bb
    uz_Roe = (Uz_in + Uz_out * cc) * bb
    h_Roe  = ( h_in +  h_out * cc) * bb
    ke_Roe = 0.5_rfreal * (ux_Roe**2 + uy_Roe**2 + uz_Roe**2)
    if (h_Roe - ke_Roe <= 0.0_8) then
!      write (*,'(6(E20.8,1X))') gamma, h_Roe, ke_Roe, ux_roe, uy_roe, uz_roe
      write (*,'(5(a,E20.8,1X))') "gamma = ", gamma, ", ke_in = ", ke_in, ", en_in = ", en_in, &
        ", p_in = ", p_in, ", h_in = ", h_in
    end if
    spd_snd_Roe = sqrt(gm1*(h_Roe - ke_Roe))

    ! ... variable renaming
    XI_X = norm(1); XI_Y = 0.0_rfreal; XI_Z = 0.0_rfreal
    if (ND >= 2) XI_Y = norm(2)
    if (ND == 3) XI_Z = norm(3)
    denom = 1.0_rfreal / sqrt(XI_X**2 + XI_Y**2 + XI_Z**2)
    XI_X_TILDE = XI_X * denom
    XI_Y_TILDE = XI_Y * denom
    XI_Z_TILDE = XI_Z * denom
    RHO = rho_in

!!$    ! ... debugging
!!$    RHO = rho_in
!!$    UX_ROE = UX_IN
!!$    UY_ROE = UY_IN
!!$    UZ_ROE = UZ_IN
!!$    SPD_SND_ROE = sqrt(gm1*(H_IN - KE_IN))

    ! ... form transformation matrices
    ucon = ux_Roe * XI_X + uy_Roe * XI_Y + uz_Roe * XI_Z

    if (ND == 2) then

      ! ... directly from Pulliam & Chaussee
      alpha = 0.5_rfreal * RHO / SPD_SND_ROE
      beta = 1.0_rfreal / (RHO * SPD_SND_ROE)
      theta = XI_X_TILDE * UX_ROE + XI_Y_TILDE * UY_ROE
      phi2 = 0.5_rfreal * (GAMMA - 1.0_rfreal) * (UX_ROE**2 + UY_ROE**2)
      Tmat(1,1) = 1.0_rfreal
      Tmat(1,2) = 0.0_rfreal
      Tmat(1,3) = alpha
      Tmat(1,4) = alpha
      Tmat(2,1) = UX_ROE
      Tmat(2,2) = XI_Y_TILDE * RHO
      Tmat(2,3) = alpha * (UX_ROE + XI_X_TILDE * SPD_SND_ROE)
      Tmat(2,4) = alpha * (UX_ROE - XI_X_TILDE * SPD_SND_ROE)
      Tmat(3,1) = UY_ROE
      Tmat(3,2) = -XI_X_TILDE * RHO
      Tmat(3,3) = alpha * (UY_ROE + XI_Y_TILDE * SPD_SND_ROE)
      Tmat(3,4) = alpha * (UY_ROE - XI_Y_TILDE * SPD_SND_ROE)
      Tmat(4,1) = PHI2 / (GAMMA - 1.0_rfreal)
      Tmat(4,2) = RHO * (XI_Y_TILDE * UX_ROE - XI_X_TILDE * UY_ROE)
      Tmat(4,3) = alpha * ((PHI2 + SPD_SND_ROE**2)/(GAMMA - 1.0_rfreal) + SPD_SND_ROE * THETA)
      Tmat(4,4) = alpha * ((PHI2 + SPD_SND_ROE**2)/(GAMMA - 1.0_rfreal) - SPD_SND_ROE * THETA)

      Tinv(1,1) = 1.0_rfreal - PHI2 / SPD_SND_ROE**2
      Tinv(1,2) = (GAMMA - 1.0_rfreal) * UX_ROE / SPD_SND_ROE**2
      Tinv(1,3) = (GAMMA - 1.0_rfreal) * UY_ROE / SPD_SND_ROE**2
      Tinv(1,4) = -(GAMMA - 1.0_Rfreal) / SPD_SND_ROE**2
      Tinv(2,1) = -(XI_Y_TILDE * UX_ROE - XI_X_TILDE * UY_ROE) / RHO
      Tinv(2,2) =  XI_Y_TILDE / RHO
      Tinv(2,3) = -XI_X_TILDE / RHO
      Tinv(2,4) = 0.0_rfreal
      Tinv(3,1) = beta * (PHI2 - SPD_SND_ROE * theta)
      Tinv(3,2) = beta * (XI_X_TILDE * SPD_SND_ROE - (GAMMA - 1.0_rfreal) * UX_ROE)
      Tinv(3,3) = beta * (XI_Y_TILDE * SPD_SND_ROE - (GAMMA - 1.0_rfreal) * UY_ROE)
      Tinv(3,4) = beta * (GAMMA - 1.0_rfreal)
      Tinv(4,1) = beta * (PHI2 + SPD_SND_ROE * theta)
      Tinv(4,2) = -beta * (XI_X_TILDE * SPD_SND_ROE + (GAMMA - 1.0_rfreal) * UX_ROE)
      Tinv(4,3) = -beta * (XI_Y_TILDE * SPD_SND_ROE + (GAMMA - 1.0_rfreal) * UY_ROE)
      Tinv(4,4) = beta * (GAMMA - 1.0_rfreal)

      ! ... compute the diagonal matrix lambda
      Lambda(:,:) = 0.0_rfreal
      do jj = 1, ND
        Lambda(jj,jj) = ucon
      end do
      Lambda(ND+1,ND+1) = ucon + SPD_SND_ROE * sqrt(XI_X**2 + XI_Y**2)
      Lambda(ND+2,ND+2) = ucon - SPD_SND_ROE * sqrt(XI_X**2 + XI_Y**2)

    else if (ND == 3) then

      ! ... directly from Pulliam & Chaussee
      alpha = 0.5_rfreal * RHO / SPD_SND_ROE
      beta = 1.0_rfreal / (RHO * SPD_SND_ROE)
      theta = XI_X_TILDE * UX_ROE + XI_Y_TILDE * UY_ROE + XI_Z_TILDE * UZ_ROE
      phi2 = 0.5_rfreal * (GAMMA - 1.0_rfreal) * (UX_ROE**2 + UY_ROE**2 + UZ_ROE**2)

      Tmat(1,1) = XI_X_TILDE
      Tmat(1,2) = XI_Y_TILDE
      Tmat(1,3) = XI_Z_TILDE
      Tmat(1,4) = alpha
      Tmat(1,5) = alpha
      Tmat(2,1) = XI_X_TILDE * UX_ROE
      Tmat(2,2) = XI_Y_TILDE * UX_ROE - XI_Z_TILDE * RHO
      Tmat(2,3) = XI_Z_TILDE * UX_ROE + XI_Y_TILDE * RHO
      Tmat(2,4) = alpha * (UX_ROE + XI_X_TILDE * SPD_SND_ROE)
      Tmat(2,5) = alpha * (UX_ROE - XI_X_TILDE * SPD_SND_ROE)
      Tmat(3,1) = XI_X_TILDE * UY_ROE + XI_Z_TILDE * RHO
      Tmat(3,2) = XI_Y_TILDE * UY_ROE
      Tmat(3,3) = XI_Z_TILDE * UY_ROE - XI_X_TILDE * RHO
      Tmat(3,4) = alpha * (UY_ROE + XI_Y_TILDE * SPD_SND_ROE)
      Tmat(3,5) = alpha * (UY_ROE - XI_Y_TILDE * SPD_SND_ROE)
      Tmat(4,1) = XI_X_TILDE * UZ_ROE - XI_Y_TILDE * RHO
      Tmat(4,2) = XI_Y_TILDE * UZ_ROE + XI_X_TILDE * RHO
      Tmat(4,3) = XI_Z_TILDE * UZ_ROE
      Tmat(4,4) = alpha * (UZ_ROE + XI_Z_TILDE * SPD_SND_ROE)
      Tmat(4,5) = alpha * (UZ_ROE - XI_Z_TILDE * SPD_SND_ROE)
      Tmat(5,1) = XI_X_TILDE * PHI2 / (GAMMA - 1.0_rfreal) + RHO * (XI_Z_TILDE * UY_ROE - XI_Y_TILDE * UZ_ROE)
      Tmat(5,2) = XI_Y_TILDE * PHI2 / (GAMMA - 1.0_rfreal) + RHO * (XI_X_TILDE * UZ_ROE - XI_Z_TILDE * UX_ROE)
      Tmat(5,3) = XI_Z_TILDE * PHI2 / (GAMMA - 1.0_rfreal) + RHO * (XI_Y_TILDE * UX_ROE - XI_X_TILDE * UY_ROE)
      Tmat(5,4) = alpha * ((PHI2 + SPD_SND_ROE**2)/(GAMMA - 1.0_rfreal) + SPD_SND_ROE * THETA)
      Tmat(5,5) = alpha * ((PHI2 + SPD_SND_ROE**2)/(GAMMA - 1.0_rfreal) - SPD_SND_ROE * THETA)

      Tinv(1,1) =  XI_X_TILDE * (1.0_rfreal - PHI2 / SPD_SND_ROE**2) - (XI_Z_TILDE * UY_ROE - XI_Y_TILDE * UZ_ROE) / RHO
      Tinv(1,2) =  XI_X_TILDE * (GAMMA - 1.0_rfreal) * UX_ROE / SPD_SND_ROE**2
      Tinv(1,3) =  XI_Z_TILDE / RHO + XI_X_TILDE * (GAMMA - 1.0_rfreal) * UY_ROE / SPD_SND_ROE**2
      Tinv(1,4) = -XI_Y_TILDE / RHO + XI_X_TILDE * (GAMMA - 1.0_rfreal) * UZ_ROE / SPD_SND_ROE**2
      Tinv(1,5) = -XI_X_TILDE * (GAMMA - 1.0_rfreal) / SPD_SND_ROE**2
      Tinv(2,1) =  XI_Y_TILDE * (1.0_rfreal - PHI2 / SPD_SND_ROE**2) - (XI_X_TILDE * UZ_ROE - XI_Z_TILDE * UX_ROE) / RHO
      Tinv(2,2) = -XI_Z_TILDE / RHO + XI_Y_TILDE * (GAMMA - 1.0_rfreal) * UX_ROE / SPD_SND_ROE**2
      Tinv(2,3) =  XI_Y_TILDE * (GAMMA - 1.0_rfreal) * UY_ROE / SPD_SND_ROE**2
      Tinv(2,4) =  XI_X_TILDE / RHO + XI_Y_TILDE * (GAMMA - 1.0_rfreal) * UZ_ROE / SPD_SND_ROE**2
      Tinv(2,5) = -XI_Y_TILDE * (GAMMA - 1.0_rfreal) / SPD_SND_ROE**2
      Tinv(3,1) =  XI_Z_TILDE * (1.0_rfreal - PHI2 / SPD_SND_ROE**2) - (XI_Y_TILDE * UX_ROE - XI_X_TILDE * UY_ROE) / RHO
      Tinv(3,2) =  XI_Y_TILDE / RHO + XI_Z_TILDE * (GAMMA - 1.0_rfreal) * UX_ROE / SPD_SND_ROE**2
      Tinv(3,3) = -XI_X_TILDE / RHO + XI_Z_TILDE * (GAMMA - 1.0_rfreal) * UY_ROE / SPD_SND_ROE**2
      Tinv(3,4) =  XI_Z_TILDE * (GAMMA - 1.0_rfreal) * UZ_ROE / SPD_SND_ROE**2
      Tinv(3,5) = -XI_Z_TILDE * (GAMMA - 1.0_rfreal) / SPD_SND_ROE**2
      Tinv(4,1) =  beta * (PHI2 - SPD_SND_ROE * theta)
      Tinv(4,2) =  beta * (XI_X_TILDE * SPD_SND_ROE - (GAMMA - 1.0_rfreal) * UX_ROE)
      Tinv(4,3) =  beta * (XI_Y_TILDE * SPD_SND_ROE - (GAMMA - 1.0_rfreal) * UY_ROE)
      Tinv(4,4) =  beta * (XI_Z_TILDE * SPD_SND_ROE - (GAMMA - 1.0_rfreal) * UZ_ROE)
      Tinv(4,5) =  beta * (GAMMA - 1.0_rfreal)
      Tinv(5,1) =  beta * (PHI2 + SPD_SND_ROE * theta)
      Tinv(5,2) = -beta * (XI_X_TILDE * SPD_SND_ROE + (GAMMA - 1.0_rfreal) * UX_ROE)
      Tinv(5,3) = -beta * (XI_Y_TILDE * SPD_SND_ROE + (GAMMA - 1.0_rfreal) * UY_ROE)
      Tinv(5,4) = -beta * (XI_Z_TILDE * SPD_SND_ROE + (GAMMA - 1.0_rfreal) * UZ_ROE)
      Tinv(5,5) =  beta * (GAMMA - 1.0_rfreal)

      ! ... compute the diagonal matrix lambda
      Lambda(:,:) = 0.0_rfreal
      do jj = 1, ND
        Lambda(jj,jj) = ucon
      end do
      Lambda(ND+1,ND+1) = ucon + SPD_SND_ROE * sqrt(XI_X**2 + XI_Y**2 + XI_Z**2)
      Lambda(ND+2,ND+2) = ucon - SPD_SND_ROE * sqrt(XI_X**2 + XI_Y**2 + XI_Z**2)

    end if

  end subroutine SAT_Form_Roe_Matrices
  subroutine SAT_Form_Roe_Matrices_INJ(u_in, u_out, gamma, norm, tmat, tinv, lambda)

    USE ModGlobal

    Implicit None

    Real(rfreal), Pointer :: u_in(:), u_out(:), tmat(:,:), tinv(:,:), norm(:), lambda(:,:)
    Real(rfreal) :: gamma, rho_in, ux_in, uy_in, uz_in, ke_in, p_in, en_in, h_in
    Real(rfreal) :: gm1, rho_out, ux_out, uy_out, uz_out, ke_out, p_out, en_out, h_out
    Real(rfreal) :: cc, bb, ux_Roe, uy_Roe, uz_Roe, h_Roe, ke_Roe, spd_snd_Roe
    Real(rfreal) :: XI_X_TILDE, XI_Y_TILDE, XI_Z_TILDE, ucon, rho, alpha, theta, beta, phi2
    Real(rfreal) :: XI_X, XI_Y, XI_Z, denom
    Integer :: N, ND, jj

    ! ... size of this problem
    ND = size(u_in) - 2

    ! ... constants
    gm1 = gamma - 1.0_rfreal

    ! ... form primative variables from "in"
    rho_in = u_in(1)
    ux_in  = u_in(2) / rho_in; uy_in = 0.0_rfreal; uz_in = 0.0_rfreal
    if (ND >= 2) uy_in  = u_in(3) / rho_in
    if (ND == 3) uz_in  = u_in(4) / rho_in
    ke_in  = 0.5_rfreal * (ux_in**2 + uy_in**2 + uz_in**2)
    en_in  = u_in(ND+2)/rho_in - ke_in
    p_in   = gm1 * rho_in * en_in
    h_in   = (gamma / gm1) * p_in / rho_in + ke_in 

    ! ... form primative variables from "out"
    rho_out = u_out(1)
    ux_out  = u_out(2) / rho_out; uy_out = 0.0_rfreal; uz_out = 0.0_rfreal
    if (ND >= 2) uy_out  = u_out(3) / rho_out
    if (ND == 3) uz_out  = u_out(4) / rho_out
    ke_out  = 0.5_rfreal * (ux_out**2 + uy_out**2 + uz_out**2)
    en_out  = u_out(ND+2)/rho_out - ke_out
    p_out   = gm1 * rho_out * en_out
    h_out   = (gamma / gm1) * p_out / rho_out + ke_out 

    ! ... form Roe variables
    cc = sqrt(rho_out / rho_in)
    bb = 1.0_rfreal / (1.0_rfreal + cc)
    ux_Roe = (Ux_in + Ux_out * cc) * bb
    uy_Roe = (Uy_in + Uy_out * cc) * bb
    uz_Roe = (Uz_in + Uz_out * cc) * bb
    h_Roe  = ( h_in +  h_out * cc) * bb
    ke_Roe = 0.5_rfreal * (ux_Roe**2 + uy_Roe**2 + uz_Roe**2)
    if (h_Roe - ke_Roe <= 0.0_8) then
!      write (*,'(6(E20.8,1X))') gamma, h_Roe, ke_Roe, ux_roe, uy_roe, uz_roe
      write (*,'(6(E20.8,1X))') gamma, ke_in, en_in, p_in, h_in
    end if
    spd_snd_Roe = sqrt(gm1*(h_Roe - ke_Roe))

    ! ... variable renaming
    XI_X = norm(1); XI_Y = 0.0_rfreal; XI_Z = 0.0_rfreal
    if (ND >= 2) XI_Y = norm(2)
    if (ND == 3) XI_Z = norm(3)
    denom = 1.0_rfreal / sqrt(XI_X**2 + XI_Y**2 + XI_Z**2)
    XI_X_TILDE = XI_X * denom
    XI_Y_TILDE = XI_Y * denom
    XI_Z_TILDE = XI_Z * denom
    RHO = rho_in

!!$    ! ... debugging
!!$    RHO = rho_in
!!$    UX_ROE = UX_IN
!!$    UY_ROE = UY_IN
!!$    UZ_ROE = UZ_IN
!!$    SPD_SND_ROE = sqrt(gm1*(H_IN - KE_IN))

    ! ... form transformation matrices
    ucon = ux_Roe * XI_X + uy_Roe * XI_Y + uz_Roe * XI_Z

    if (ND == 2) then

      ! ... directly from Pulliam & Chaussee
      alpha = 0.5_rfreal * RHO / SPD_SND_ROE
      beta = 1.0_rfreal / (RHO * SPD_SND_ROE)
      theta = XI_X_TILDE * UX_ROE + XI_Y_TILDE * UY_ROE
      phi2 = 0.5_rfreal * (GAMMA - 1.0_rfreal) * (UX_ROE**2 + UY_ROE**2)
      Tmat(1,1) = 1.0_rfreal
      Tmat(1,2) = 0.0_rfreal
      Tmat(1,3) = alpha
      Tmat(1,4) = alpha
      Tmat(2,1) = UX_ROE
      Tmat(2,2) = XI_Y_TILDE * RHO
      Tmat(2,3) = alpha * (UX_ROE + XI_X_TILDE * SPD_SND_ROE)
      Tmat(2,4) = alpha * (UX_ROE - XI_X_TILDE * SPD_SND_ROE)
      Tmat(3,1) = UY_ROE
      Tmat(3,2) = -XI_X_TILDE * RHO
      Tmat(3,3) = alpha * (UY_ROE + XI_Y_TILDE * SPD_SND_ROE)
      Tmat(3,4) = alpha * (UY_ROE - XI_Y_TILDE * SPD_SND_ROE)
      Tmat(4,1) = PHI2 / (GAMMA - 1.0_rfreal)
      Tmat(4,2) = RHO * (XI_Y_TILDE * UX_ROE - XI_X_TILDE * UY_ROE)
      Tmat(4,3) = alpha * ((PHI2 + SPD_SND_ROE**2)/(GAMMA - 1.0_rfreal) + SPD_SND_ROE * THETA)
      Tmat(4,4) = alpha * ((PHI2 + SPD_SND_ROE**2)/(GAMMA - 1.0_rfreal) - SPD_SND_ROE * THETA)

      Tinv = 0d0
      Tinv(1,1)=1d0 + (UX_ROE*XI_X_TILDE + UY_ROE*XI_Y_TILDE)/(SPD_SND_ROE*XI_X_TILDE**2 + SPD_SND_ROE*XI_Y_TILDE**2)
      Tinv(1,2)=-(XI_X_TILDE/(SPD_SND_ROE*XI_X_TILDE**2 + SPD_SND_ROE*XI_Y_TILDE**2))
      Tinv(1,3)=-(XI_Y_TILDE/(SPD_SND_ROE*XI_X_TILDE**2 + SPD_SND_ROE*XI_Y_TILDE**2))
      Tinv(2,1)=(UY_ROE*XI_X_TILDE - UX_ROE*XI_Y_TILDE)/(RHO*XI_X_TILDE**2 + RHO*XI_Y_TILDE**2)
      Tinv(2,2)=XI_Y_TILDE/(RHO*XI_X_TILDE**2 + RHO*XI_Y_TILDE**2)
      Tinv(2,3)=-(XI_X_TILDE/(RHO*XI_X_TILDE**2 + RHO*XI_Y_TILDE**2))
      Tinv(3,1)=(-2d0*(UX_ROE*XI_X_TILDE + UY_ROE*XI_Y_TILDE))/(RHO*(XI_X_TILDE**2 + XI_Y_TILDE**2))
      Tinv(3,2)=(2d0*XI_X_TILDE)/(RHO*XI_X_TILDE**2 + RHO*XI_Y_TILDE**2)
      Tinv(3,3)=(2d0*XI_Y_TILDE)/(RHO*XI_X_TILDE**2 + RHO*XI_Y_TILDE**2)

      ! ... compute the diagonal matrix lambda
      Lambda(:,:) = 0.0_rfreal
      do jj = 1, ND
        Lambda(jj,jj) = ucon
      end do
      Lambda(ND+1,ND+1) = ucon + SPD_SND_ROE * sqrt(XI_X**2 + XI_Y**2)
      Lambda(ND+2,ND+2) = ucon - SPD_SND_ROE * sqrt(XI_X**2 + XI_Y**2)

    else if (ND == 3) then

      ! ... directly from Pulliam & Chaussee
      alpha = 0.5_rfreal * RHO / SPD_SND_ROE
      beta = 1.0_rfreal / (RHO * SPD_SND_ROE)
      theta = XI_X_TILDE * UX_ROE + XI_Y_TILDE * UY_ROE + XI_Z_TILDE * UZ_ROE
      phi2 = 0.5_rfreal * (GAMMA - 1.0_rfreal) * (UX_ROE**2 + UY_ROE**2 + UZ_ROE**2)

      Tmat(1,1) = XI_X_TILDE
      Tmat(1,2) = XI_Y_TILDE
      Tmat(1,3) = XI_Z_TILDE
      Tmat(1,4) = alpha
      Tmat(1,5) = alpha
      Tmat(2,1) = XI_X_TILDE * UX_ROE
      Tmat(2,2) = XI_Y_TILDE * UX_ROE - XI_Z_TILDE * RHO
      Tmat(2,3) = XI_Z_TILDE * UX_ROE + XI_Y_TILDE * RHO
      Tmat(2,4) = alpha * (UX_ROE + XI_X_TILDE * SPD_SND_ROE)
      Tmat(2,5) = alpha * (UX_ROE - XI_X_TILDE * SPD_SND_ROE)
      Tmat(3,1) = XI_X_TILDE * UY_ROE + XI_Z_TILDE * RHO
      Tmat(3,2) = XI_Y_TILDE * UY_ROE
      Tmat(3,3) = XI_Z_TILDE * UY_ROE - XI_X_TILDE * RHO
      Tmat(3,4) = alpha * (UY_ROE + XI_Y_TILDE * SPD_SND_ROE)
      Tmat(3,5) = alpha * (UY_ROE - XI_Y_TILDE * SPD_SND_ROE)
      Tmat(4,1) = XI_X_TILDE * UZ_ROE - XI_Y_TILDE * RHO
      Tmat(4,2) = XI_Y_TILDE * UZ_ROE + XI_X_TILDE * RHO
      Tmat(4,3) = XI_Z_TILDE * UZ_ROE
      Tmat(4,4) = alpha * (UZ_ROE + XI_Z_TILDE * SPD_SND_ROE)
      Tmat(4,5) = alpha * (UZ_ROE - XI_Z_TILDE * SPD_SND_ROE)
      Tmat(5,1) = XI_X_TILDE * PHI2 / (GAMMA - 1.0_rfreal) + RHO * (XI_Z_TILDE * UY_ROE - XI_Y_TILDE * UZ_ROE)
      Tmat(5,2) = XI_Y_TILDE * PHI2 / (GAMMA - 1.0_rfreal) + RHO * (XI_X_TILDE * UZ_ROE - XI_Z_TILDE * UX_ROE)
      Tmat(5,3) = XI_Z_TILDE * PHI2 / (GAMMA - 1.0_rfreal) + RHO * (XI_Y_TILDE * UX_ROE - XI_X_TILDE * UY_ROE)
      Tmat(5,4) = alpha * ((PHI2 + SPD_SND_ROE**2)/(GAMMA - 1.0_rfreal) + SPD_SND_ROE * THETA)
      Tmat(5,5) = alpha * ((PHI2 + SPD_SND_ROE**2)/(GAMMA - 1.0_rfreal) - SPD_SND_ROE * THETA)

      Tinv(1,1) =  XI_X_TILDE * (1.0_rfreal - PHI2 / SPD_SND_ROE**2) - (XI_Z_TILDE * UY_ROE - XI_Y_TILDE * UZ_ROE) / RHO
      Tinv(1,2) =  XI_X_TILDE * (GAMMA - 1.0_rfreal) * UX_ROE / SPD_SND_ROE**2
      Tinv(1,3) =  XI_Z_TILDE / RHO + XI_X_TILDE * (GAMMA - 1.0_rfreal) * UY_ROE / SPD_SND_ROE**2
      Tinv(1,4) = -XI_Y_TILDE / RHO + XI_X_TILDE * (GAMMA - 1.0_rfreal) * UZ_ROE / SPD_SND_ROE**2
      Tinv(1,5) = -XI_X_TILDE * (GAMMA - 1.0_rfreal) / SPD_SND_ROE**2
      Tinv(2,1) =  XI_Y_TILDE * (1.0_rfreal - PHI2 / SPD_SND_ROE**2) - (XI_X_TILDE * UZ_ROE - XI_Z_TILDE * UX_ROE) / RHO
      Tinv(2,2) = -XI_Z_TILDE / RHO + XI_Y_TILDE * (GAMMA - 1.0_rfreal) * UX_ROE / SPD_SND_ROE**2
      Tinv(2,3) =  XI_Y_TILDE * (GAMMA - 1.0_rfreal) * UY_ROE / SPD_SND_ROE**2
      Tinv(2,4) =  XI_X_TILDE / RHO + XI_Y_TILDE * (GAMMA - 1.0_rfreal) * UZ_ROE / SPD_SND_ROE**2
      Tinv(2,5) = -XI_Y_TILDE * (GAMMA - 1.0_rfreal) / SPD_SND_ROE**2
      Tinv(3,1) =  XI_Z_TILDE * (1.0_rfreal - PHI2 / SPD_SND_ROE**2) - (XI_Y_TILDE * UX_ROE - XI_X_TILDE * UY_ROE) / RHO
      Tinv(3,2) =  XI_Y_TILDE / RHO + XI_Z_TILDE * (GAMMA - 1.0_rfreal) * UX_ROE / SPD_SND_ROE**2
      Tinv(3,3) = -XI_X_TILDE / RHO + XI_Z_TILDE * (GAMMA - 1.0_rfreal) * UY_ROE / SPD_SND_ROE**2
      Tinv(3,4) =  XI_Z_TILDE * (GAMMA - 1.0_rfreal) * UZ_ROE / SPD_SND_ROE**2
      Tinv(3,5) = -XI_Z_TILDE * (GAMMA - 1.0_rfreal) / SPD_SND_ROE**2
      Tinv(4,1) =  beta * (PHI2 - SPD_SND_ROE * theta)
      Tinv(4,2) =  beta * (XI_X_TILDE * SPD_SND_ROE - (GAMMA - 1.0_rfreal) * UX_ROE)
      Tinv(4,3) =  beta * (XI_Y_TILDE * SPD_SND_ROE - (GAMMA - 1.0_rfreal) * UY_ROE)
      Tinv(4,4) =  beta * (XI_Z_TILDE * SPD_SND_ROE - (GAMMA - 1.0_rfreal) * UZ_ROE)
      Tinv(4,5) =  beta * (GAMMA - 1.0_rfreal)
      Tinv(5,1) =  beta * (PHI2 + SPD_SND_ROE * theta)
      Tinv(5,2) = -beta * (XI_X_TILDE * SPD_SND_ROE + (GAMMA - 1.0_rfreal) * UX_ROE)
      Tinv(5,3) = -beta * (XI_Y_TILDE * SPD_SND_ROE + (GAMMA - 1.0_rfreal) * UY_ROE)
      Tinv(5,4) = -beta * (XI_Z_TILDE * SPD_SND_ROE + (GAMMA - 1.0_rfreal) * UZ_ROE)
      Tinv(5,5) =  beta * (GAMMA - 1.0_rfreal)

      ! ... compute the diagonal matrix lambda
      Lambda(:,:) = 0.0_rfreal
      do jj = 1, ND
        Lambda(jj,jj) = ucon
      end do
      Lambda(ND+1,ND+1) = ucon + SPD_SND_ROE * sqrt(XI_X**2 + XI_Y**2 + XI_Z**2)
      Lambda(ND+2,ND+2) = ucon - SPD_SND_ROE * sqrt(XI_X**2 + XI_Y**2 + XI_Z**2)

    end if

  end subroutine SAT_Form_Roe_Matrices_INJ
  
  subroutine SAT_Form_Roe_Matrices_TP(u_in, u_out, ND, gamma, gamref, norm, tmat, tinv, lambda)

    USE ModGlobal
    USE ModMPI

    Implicit None

    Real(rfreal), Pointer :: u_in(:), u_out(:), tmat(:,:), tinv(:,:), norm(:), lambda(:,:)
    Real(rfreal) :: gamma, rho_in, ux_in, uy_in, uz_in, ke_in, p_in, en_in, h_in
    Real(rfreal) :: gm1, rho_out, ux_out, uy_out, uz_out, ke_out, p_out, en_out, h_out
    Real(rfreal) :: cc, bb, ux_Roe, uy_Roe, uz_Roe, h_Roe, ke_Roe, spd_snd_Roe, T_Roe, en_Roe
    Real(rfreal) :: XI_X_TILDE, XI_Y_TILDE, XI_Z_TILDE, ucon, rho, alpha, theta, beta, phi2
    Real(rfreal) :: XI_X, XI_Y, XI_Z, XI_T, denom, GAMREF, Up, Um

    Integer :: N, ND, jj, i, j, counter,ii
    Logical, dimension(ND+2) :: BWORK
    Integer, dimension(ND+2) :: order

    ! ... Lapack arguments
    CHARACTER(len=1) :: JOBVL, JOBVR
    INTEGER :: INFO, LDA, LDVL, LDVR, LWORK,M
    Real(rfreal), dimension(ND+2,ND+2) :: A, VL, VR,B,LAM, Pmat
    Real(rfreal), dimension(ND+2) :: WI, WR
    Real(rfreal), dimension(300) :: WORK
    ! ... for right vector inversion
    integer, dimension(ND+2) :: IPIV

    gamma = gamref
    
    ! ... temporary
    XI_T = 0.0_rfreal

    JOBVL = 'N'
    JOBVR = 'V'
    LDA = ND + 2
    LDVL = ND + 2
    LDVR = ND + 2
    M = ND+2

    LWORK = 300

    LAM(:,:) = 0.0_8
    A(:,:) = 0.0_8
    VL(:,:) = 0.0_8
    VR(:,:) = 0.0_8
    WR(:) = 0.0_8
    WI(:) = 0.0_8
    INFO = 0
    
    
    ! ... constants
    gm1 = gamma - 1.0_rfreal

    ! ... form primative variables from "in"
    rho_in = u_in(1)
    ux_in  = u_in(2) / rho_in; uy_in = 0.0_rfreal; uz_in = 0.0_rfreal
    if (ND >= 2) uy_in  = u_in(3) / rho_in
    if (ND == 3) uz_in  = u_in(4) / rho_in
    ke_in  = 0.5_rfreal * (ux_in**2 + uy_in**2 + uz_in**2)
    en_in  = u_in(ND+2)/rho_in - ke_in
    p_in   = gm1 * rho_in * en_in
    h_in   = (gamma / gm1) * p_in / rho_in + ke_in 

    ! ... form primative variables from "out"
    rho_out = u_out(1)
    ux_out  = u_out(2) / rho_out; uy_out = 0.0_rfreal; uz_out = 0.0_rfreal
    if (ND >= 2) uy_out  = u_out(3) / rho_out
    if (ND == 3) uz_out  = u_out(4) / rho_out
    ke_out  = 0.5_rfreal * (ux_out**2 + uy_out**2 + uz_out**2)
    en_out  = u_out(ND+2)/rho_out - ke_out
    p_out   = gm1 * rho_out * en_out
    h_out   = (gamma / gm1) * p_out / rho_out + ke_out 

    ! ... form Roe variables
    cc = sqrt(rho_out / rho_in)
    bb = 1.0_rfreal / (1.0_rfreal + cc)
    ux_Roe = (Ux_in + Ux_out * cc) * bb
    uy_Roe = (Uy_in + Uy_out * cc) * bb
    uz_Roe = (Uz_in + Uz_out * cc) * bb
    h_Roe  = ( h_in +  h_out * cc) * bb
    ke_Roe = 0.5_rfreal * (ux_Roe**2 + uy_Roe**2 + uz_Roe**2)
    spd_snd_Roe = sqrt(gamma/GAMREF * gm1*(h_Roe - ke_Roe))

    ! ... temporarily
    T_Roe = gamma*(en_in + en_out * cc) * bb
    en_Roe = T_Roe/gamma
    
    ! ... variable renaming
    XI_X = norm(1); XI_Y = 0.0_rfreal; XI_Z = 0.0_rfreal
    if (ND >= 2) XI_Y = norm(2)
    if (ND == 3) XI_Z = norm(3)
    denom = 1.0_rfreal / sqrt(XI_X**2 + XI_Y**2 + XI_Z**2)
    XI_X_TILDE = XI_X * denom
    XI_Y_TILDE = XI_Y * denom
    XI_Z_TILDE = XI_Z * denom
    RHO = rho_in

    ! ... directly from Pulliam & Chaussee
    theta = XI_X * UX_ROE + XI_Y * UY_ROE + XI_Z * UZ_ROE
    phi2 = 0.5_rfreal * (GAMMA - 1.0_rfreal) * (UX_ROE**2 + UY_ROE**2 + UZ_ROE**2)
    
    if (ND == 2) then

      A(1,1) = XI_T
      A(1,2) = XI_X
      A(1,3) = XI_Y
      A(1,4) = 0.0_rfreal

      A(2,1) = XI_X * ( T_Roe / GAMREF * (GAMREF - 1.0_rfreal) - (gm1 * en_Roe - phi2)) - theta * ux_Roe
      A(2,2) = XI_T + theta - XI_X * (GAMMA - 2.0_rfreal) * ux_Roe
      A(2,3) = XI_Y * ux_Roe - gm1 * XI_X * uy_Roe
      A(2,4) = gm1 * XI_X

      A(3,1) = XI_Y * ( T_Roe / GAMREF * (GAMREF - 1.0_rfreal) - (gm1 * en_Roe - phi2)) - theta * uy_Roe
      A(3,2) = XI_X * uy_Roe - gm1 * XI_Y * ux_Roe
      A(3,3) = XI_T + theta - XI_Y * (GAMMA - 2.0_rfreal) * uy_Roe
      A(3,4) = gm1 * XI_Y

      A(4,1) = theta * (2.0_rfreal * phi2 - gamma * (en_Roe + ke_Roe)) 
      A(4,2) = XI_X * (T_Roe / GAMREF * (GAMREF - 1.0_rfreal) + en_Roe + phi2 / gm1 ) - gm1 * theta * ux_Roe
      A(4,3) = XI_Y * (T_Roe / GAMREF * (GAMREF - 1.0_rfreal) + en_Roe + phi2 / gm1 ) - gm1 * theta * uy_Roe
      A(4,4) = XI_T + gamma * theta

    else if (ND == 3) then

      A(1,1) = XI_T
      A(1,2) = XI_X
      A(1,3) = XI_Y
      A(1,4) = XI_Z
      A(1,5) = 0.0_rfreal

      A(2,1) = XI_X * ( T_Roe / GAMREF * (GAMREF - 1.0_rfreal) - (gm1 * en_Roe - phi2)) - theta * ux_Roe
      A(2,2) = XI_T + theta - XI_X * (GAMMA - 2.0_rfreal) * ux_Roe
      A(2,3) = XI_Y * ux_Roe - gm1 * XI_X * uy_Roe
      A(2,4) = XI_Z * ux_Roe - gm1 * XI_X * uz_Roe
      A(2,5) = gm1 * XI_X

      A(3,1) = XI_Y * ( T_Roe / GAMREF * (GAMREF - 1.0_rfreal) - (gm1 * en_Roe - phi2)) - theta * uy_Roe
      A(3,2) = XI_X * uy_Roe - gm1 * XI_Y * ux_Roe
      A(3,3) = XI_T + theta - XI_Y * (GAMMA - 2.0_rfreal) * uy_Roe
      A(3,4) = XI_Z * uy_Roe - gm1 * XI_Y * uz_Roe
      A(3,5) = gm1 * XI_Y

      A(4,1) = XI_Z * ( T_Roe / GAMREF * (GAMREF - 1.0_rfreal) - (gm1 * en_Roe - phi2)) - theta * uz_Roe
      A(4,2) = XI_X * uz_Roe - gm1 * XI_Z * ux_Roe
      A(4,3) = XI_Y * uz_Roe - gm1 * XI_Z * uy_Roe
      A(4,4) = XI_T + theta - XI_Z * (GAMMA - 2.0_rfreal) * uz_Roe
      A(4,5) = gm1 * XI_Z

      A(5,1) = theta * (2.0_rfreal * phi2 - gamma * (en_Roe + ke_Roe)) 
      A(5,2) = XI_X * (T_Roe / GAMREF * (GAMREF - 1.0_rfreal) + en_Roe + phi2 / gm1 ) - gm1 * theta * ux_Roe
      A(5,3) = XI_Y * (T_Roe / GAMREF * (GAMREF - 1.0_rfreal) + en_Roe + phi2 / gm1 ) - gm1 * theta * uy_Roe
      A(5,4) = XI_Z * (T_Roe / GAMREF * (GAMREF - 1.0_rfreal) + en_Roe + phi2 / gm1 ) - gm1 * theta * uz_Roe
      A(5,5) = XI_T + gamma * theta

    end if

    ! ... find the Eigenvalues and Eigenvectors
    ! ... Lapack driver DGEEV
#ifdef HAVE_BLAS    
    CALL DGEEV( JOBVL, JOBVR, M, A, LDA, WR, WI, VL, LDVL, VR, &
         LDVR, WORK, LWORK, INFO )
#endif

    lambda(:,:) = 0.0_rfreal
    Pmat(:,:) = 0.0_rfreal

    ! ... form transformation matrices
    ucon = ux_Roe * XI_X + uy_Roe * XI_Y + uz_Roe * XI_Z
    Up = ucon + 0.1_rfreal * SPD_SND_ROE * sqrt(XI_X**2 + XI_Y**2 + XI_Z**2)
    Um = ucon - 0.1_rfreal * SPD_SND_ROE * sqrt(XI_X**2 + XI_Y**2 + XI_Z**2)
    counter = 0

    ! ... Sort eigenvalues: {U,U,U,U+c,U-c}
    do jj = 1, ND+2

      if(WR(jj) .lt. Up .and. WR(jj) .gt. Um) then

        counter = counter + 1
        lambda(counter,counter) = WR(jj)
        order(jj) = counter
        Pmat(jj,counter) = 1.0_rfreal
      elseif(WR(jj) .gt. ucon) then
        lambda(ND+1,ND+1) = WR(jj)
        order(jj) = ND+1
        Pmat(jj,ND+1) = 1.0_rfreal
      elseif(WR(jj) .lt. ucon) then
        lambda(ND+2,ND+2) = WR(jj)
        order(jj) = ND+2
        Pmat(jj,ND+2) = 1.0_rfreal
      end if
    end do

    Tmat = matmul(VR,Pmat)
    VR = Tmat

    ! ... invert VR
    ! ... using a pseudoinvert using singular value decomposition
    ! ... VR = U*D*V^T

    ! ... Args: A,A, size VR, size VR, VR, size VR, array of singular values WI, U, size U, V^T, size V, work, lwork, INFO
    JOBVL = 'A'
    WI(:) = 0.0_rfreal
    VL(:,:) = 0.0_rfreal
    A(:,:) = 0.0_rfreal

    ! ... VR = VL*diag(WI)*A^T
#ifdef HAVE_BLAS
    call DGESVD(JOBVL,JOBVL,LDVR,LDVR,VR,LDVR,WI,VL,LDVR,A,LDVR,WORK,LWORK,INFO)
#endif

    Tinv(:,:) = 0.0_rfreal
    ! ... pseudo inverse is inv(VR) = inv(VL*diag(WI)*A^T) = A*inv(diag(WI))*VL^T where small (~0) values of WI -> 1/(WI) = 0
    do i = 1,ND+2
      if(WI(i) .gt. tiny) then
        Tinv(i,i) = 1.0_rfreal/WI(i)
      else
        Tinv(i,i) = 0.0_rfreal
      end if
    end do


    Tinv = MATMUL(TRANSPOSE(A),Tinv)
    Tinv = MATMUL(Tinv,TRANSPOSE(VL))

  end subroutine SAT_Form_Roe_Matrices_TP

  Subroutine FV_BC(region)
    USE ModGlobal
    USE ModDataStruct
    USE ModDeriv
    USE ModMPI
    USE ModMatrixVectorOps

    Implicit None

    TYPE (t_region), POINTER :: region
    INTEGER :: ng
    INTEGER :: flags

    ! ... local variables
    type(t_patch), pointer :: patch
    type(t_grid), pointer :: grid
    type(t_mixt), pointer :: state
    type(t_mixt_input), pointer :: input
    real(rfreal), pointer :: Q_bndry(:,:),MT1Prod(:),Pres_XI(:,:),flux(:),dflux(:)
    integer :: ND, Nc, N(MAX_ND),G(MAX_ND),npatch, i, j, l0, k, ii, jj, l1, l2, isign, lp, kk
    integer :: normDir, tangDir(MAX_ND,MAX_ND-1), Np(MAX_ND)
    real(rfreal) :: norm_vec(MAX_ND), tang_vec(2,MAX_ND), dsgn
    integer :: sgn, handedness(MAX_ND),sweepdir,ijk_pInd(MAX_ND),numghost,a,peri_index
    integer, pointer :: inorm(:)
    real(rfreal) :: XI_X_TILDE, XI_Y_TILDE, XI_Z_TILDE,XI_X,XI_Y,XI_Z
    real(rfreal) :: UX,UY,UZ,PRESSURE,TEMPERATURE,GAMMA,RHO,SPVOL,un, ut(MAX_ND-1),denom
    real(rfreal) :: BASE_RHO,BASE_UX,BASE_UY,BASE_UZ,BASE_PRESSURE, ibfac_local
    integer :: velsweep, tangNum

    ! ... simplicity
    ND = region%input%ND

    tangDir(1,:) = (/2, 3/)
    tangDir(2,:) = (/1, 3/)
    tangDir(3,:) = (/1, 2/)
    handedness   = (/1, -1, 1/)

    allocate(inorm(2))

    do npatch = 1, region%nPatches

      patch => region%patch(npatch)
      grid  => region%grid(patch%gridID)
      state => region%state(patch%gridID)
      input => grid%input
      normDir = abs(patch%normDir)
      sgn = normDir / patch%normDir
      dsgn = dble(sgn)

      if (input%fv_grid(patch%gridID) == FINITE_VOL) then !operate only on finite volume patches

        Nc = grid%nCells
        N  = 1; Np = 1;G = 1;
        do j = 1, grid%ND
          N(j)  = grid%ie(j)-grid%is(j)+1
          if (j==normDir) then
            G(j) = N(j) + grid%fv%nGhost(j,1) + grid%fv%nGhost(j,2)
          else
            G(j) = N(j)
          end if
          Np(j) = patch%ie(j) - patch%is(j) + 1
        end do;

        Do k = patch%is(3),patch%ie(3)
          Do j = patch%is(2),patch%ie(2)
            Do i = patch%is(1),patch%ie(1)

              l0 = (k- grid%is(3))* N(1)* N(2) + (j- grid%is(2))* N(1) + (i- grid%is(1)+1)

              ! ... normal indices
              do jj = 1, 2!size(gvec)
                if (normDir == 1) then
                  l1 = l0 + sgn*(jj-1)
                else if (normDir == 2) then
                  l1 = l0 + sgn*(jj-1)*N(1)
                else
                  l1 = l0 + sgn*(jj-1)*N(1)*N(2)
                end if
                inorm(jj) = l1
              end do

              ibfac_local = grid%ibfac(l0)

              if (patch%bcType == FV_WALL_SLIP_EXTRAPOLATION_0TH_ORDER .OR. patch%bcType == FV_WALL_ISOTHERMAL_NOSLIP) then


                ! ... 0th order extrapolation, take everything (no gamma) from interior point.
                GAMMA = state%gv(l0,1)
                TEMPERATURE = state%dv(inorm(2),2)
                RHO = state%cv(inorm(2),1)
                SPVOL = 1.0_rfreal / RHO
                UX    = SPVOL * state%cv(inorm(2),2); UY = 0.0_rfreal; UZ = 0.0_rfreal;
                if (ND >= 2) UY = SPVOL * state%cv(inorm(2),3)
                if (ND == 3) UZ = SPVOL * state%cv(inorm(2),4)
                PRESSURE = state%dv(inorm(2),1)
!                      PRESSURE = (GAMMA-1.0_rfreal)*(state%cv(l0,ND+2) - 0.5_rfreal * RHO * (UX**2.0_rfreal + UY**2.0_rfreal + UZ**2.0_rfreal))

                ! ... construct the normalized metrics
                XI_X = grid%MT1(l0,region%global%t2Map(normDir,1)); XI_Y = 0.0_rfreal; XI_Z = 0.0_rfreal
                if (ND >= 2) XI_Y = grid%MT1(l0,region%global%t2Map(normDir,2))
                if (ND == 3) XI_Z = grid%MT1(l0,region%global%t2Map(normDir,3))

                ! ... multiply by the Jacobian
                XI_X = XI_X * grid%JAC(l0)
                XI_Y = XI_Y * grid%JAC(l0)
                XI_Z = XI_Z * grid%JAC(l0)
                denom = 1.0_rfreal / (ibfac_local * sqrt(XI_X**2+XI_Y**2+XI_Z**2) + (1.0_rfreal - ibfac_local))
                XI_X_TILDE = XI_X * denom
                XI_Y_TILDE = XI_Y * denom
                XI_Z_TILDE = XI_Z * denom

                if ( patch%bcType == NSCBC_WALL_ADIABATIC_SLIP ) then

                  ! ... compute the normal and tangential velocity vectors
                  if (ND == 2) then ! JKim 11/2007
                    un    = XI_X_TILDE * UX + XI_Y_TILDE * UY
                    ut(1) =-XI_Y_TILDE * UX + XI_X_TILDE * UY
                  else if (ND == 3) then ! JKim 11/2007
                    norm_vec(:) = (/XI_X_TILDE, XI_Y_TILDE, XI_Z_TILDE/) ! boundary-normal vector in the
! direction of increasing xi or
! eta or zeta.
                    tang_vec(1,:) = (/grid%MT1(l0,region%global%t2Map(tangDir(normDir,1),1)), & ! pick up another direction
                         grid%MT1(l0,region%global%t2Map(tangDir(normDir,1),2)), & ! in computational domain
                         grid%MT1(l0,region%global%t2Map(tangDir(normDir,1),3))/)  ! to get boundary-tangential vector
                    tang_vec(1,:) = tang_vec(1,:) * handedness(normDir) ! adjust handed-ness for right-handed-ness
                    tang_vec(1,:) = tang_vec(1,:) * grid%JAC(l0) ! multiply by the Jacobian
                    denom = 1.0_rfreal / sqrt(dot_product(tang_vec(1,:), tang_vec(1,:)))
                    tang_vec(1,:) = tang_vec(1,:) * denom ! normal vector for another direction, say,
                                ! (eta_x_tilde, eta_y_tilde, eta_z_tilde)
                    call cross_product(norm_vec, tang_vec(1,:), tang_vec(1,:))
                    tang_vec(1,:) = tang_vec(1,:) / sqrt(dot_product(tang_vec(1,:), tang_vec(1,:))) ! now it's done ^^

                    call cross_product(norm_vec, tang_vec(1,:), tang_vec(2,:))
                    tang_vec(2,:) = tang_vec(2,:) / sqrt(dot_product(tang_vec(2,:), tang_vec(2,:))) ! now it's done ^^

                    un    = dot_product((/UX, UY, UZ/), norm_vec(:)  )
                    ut(1) = dot_product((/UX, UY, UZ/), tang_vec(1,:))
                    ut(2) = dot_product((/UX, UY, UZ/), tang_vec(2,:))
                  else
                    call graceful_exit(region%myrank, 'No adiabatic slip wall for ND /= (2,3).  Stopping...')
                  end if ! ND

                  ! ... update the wall-normal velocity
                  un = XI_X_TILDE * grid%XYZ_TAU(l0,1)
                  if (ND >= 2) un = un + XI_Y_TILDE * grid%XYZ_TAU(l0,2)
                  if (ND == 3) un = un + XI_Z_TILDE * grid%XYZ_TAU(l0,3)

                  ! ... return to Cartesian velocities
                  if (ND == 2) then ! JKim 11/2007
                    UX = XI_X_TILDE * un - XI_Y_TILDE * ut(1)
                    UY = XI_Y_TILDE * un + XI_X_TILDE * ut(1)
                  else if (ND == 3) then ! JKim 11/2007
                    UX = dot_product((/un, ut(1), ut(2)/), &
                         (/norm_vec(1), tang_vec(1,1), tang_vec(2,1)/))
                    UY = dot_product((/un, ut(1), ut(2)/), &
                         (/norm_vec(2), tang_vec(1,2), tang_vec(2,2)/))
                    UZ = dot_product((/un, ut(1), ut(2)/), &
                         (/norm_vec(3), tang_vec(1,3), tang_vec(2,3)/))
                  end if ! ND

                  ! ... update the total energy
                  state%cv(l0,ND+2) = state%cv(l0,ND+2) + 0.5_rfreal * RHO * (UX**2)! - (state%cv(l0,2)/state%cv(l0,1))**2)
                  if (ND >= 2) state%cv(l0,ND+2) = state%cv(l0,ND+2) + 0.5_rfreal * RHO * (UY**2)! - (state%cv(l0,3)/state%cv(l0,1))**2)
                  if (ND == 3) state%cv(l0,ND+2) = state%cv(l0,ND+2) + 0.5_rfreal * RHO * (UZ**2)! - (state%cv(l0,4)/state%cv(l0,1))**2)

                  state%cv(l0,1) = RHO
                  ! ... update Cartesian velocities
                  state%cv(l0,2) = state%cv(l0,1) * UX
                  if (ND >= 2) state%cv(l0,3) = state%cv(l0,1) * UY
                  if (ND == 3) state%cv(l0,4) = state%cv(l0,1) * UZ


                elseif (patch%bcType == FV_WALL_ISOTHERMAL_NOSLIP) then
                  UX = grid%XYZ_TAU(l0,1); UY = 0.0_rfreal; UZ = 0.0_rfreal
                  if(ND >= 2) UY = grid%XYZ_TAU(l0,2)
                  if(ND == 3) UZ = grid%XYZ_TAU(l0,3)
                  TEMPERATURE = input%bcic_WallTemp / (input%GamRef - 1.0_rfreal)/input%TempRef 
                  RHO = GAMMA*PRESSURE/(GAMMA-1.0_rfreal) / TEMPERATURE
                end if

! RETURN CONSERVATIVE VARIABLES
!                      RHO = GAMMA*PRESSURE/(GAMMA-1.0_rfreal)/TEMPERATURE
                state%cv(l0,1) = RHO
                state%cv(l0,2) = RHO * UX
                if (ND >= 2) state%cv(l0,3) = RHO * UY
                if (ND == 3) state%cv(l0,4) = RHO * UZ
                state%cv(l0,ND+2) = PRESSURE / (GAMMA-1.0_rfreal) + 0.5_rfreal * RHO * (UX**2.0_rfreal + UY**2.0_rfreal + UZ**2.0_rfreal)

              elseif ( patch%bcType == FV_TRANSMISSIVE_EXTRAPOLATION_0TH_ORDER) then

                state%cv(l0,:) = state%cv(inorm(2),:)

              else if (patch%bcType == FV_SUPERSONIC_INFLOW) then ! specify everything... these values get copied with transmissive

                state%cv(l0,:)  = state%cvTarget(l0,:)

              end if
            end do
          end do
        end do
      end if !only update patches on fv grid
    end do

  end subroutine FV_BC

  !
  ! The below subroutine re-implements the NS_BC() subroutine
  ! in a (hopefully) more efficient and faster manner
  !
  subroutine NS_BC_Optimized(region, ng, flags)

    USE ModGlobal
    USE ModDataStruct
    USE ModMPI

    Implicit None

    ! ... Incoming Variables
    TYPE (t_region), POINTER :: region
    INTEGER :: ng
    INTEGER :: flags

    ! ... Local variables
    INTEGER :: nPatches, npatch, patch_bcType
    TYPE (t_patch), Pointer :: patch

    ! ... if FV, then only 1 BC subroutine
    ! ... to be called when NS_BC_Fix_Value is called
    If (region%input%fv_grid(ng) == FINITE_VOL) Return

    ! ... pointer dereference, WZhang 05/2014
    nPatches = region%nPatches

    ! ... loop over all of the boundary patches
    Do npatch = 1, nPatches

      ! ... dereference the pointer
      patch => region%patch(npatch)
      patch_bcType = patch%bcType

      ! ... select boundary condition
      Select Case (patch_bcType)

      Case (SAT_FAR_FIELD)

        Call NS_BC_SAT_FAR_FIELD(region, ng, flags, patch)

      Case Default 

        Write (*,'(A,1X,I2,1X,A)') 'PlasComCM: ERROR: Boundary condition ', patch_bcType, &
                                   'not yet implemented in NS_BC_Optimized'
        Call Graceful_Exit(region%myrank, 'ERROR: Terminating')

      End Select

    End Do

    Return

  End Subroutine NS_BC_Optimized


  Subroutine NS_BC_SAT_FAR_FIELD(region, ng, flags, patch)

    USE ModGlobal
    USE ModDataStruct
    USE ModMPI

    Implicit None

    ! ... Incoming Variables
    TYPE (t_region), POINTER :: region
    INTEGER :: ng, flags
    TYPE (t_patch), Pointer :: patch

    ! ... Local variables
    INTEGER :: ND, Nc, i, j, k, nAuxVars, l0, lp, jj
    INTEGER :: patch_gridID, normdir, sgn, gas_dv_model
    INTEGER, Dimension(MAX_ND) :: N, Np
    INTEGER, Pointer :: iblank(:), t2Map(:,:)
    INTEGER, Pointer :: grid_is(:), grid_ie(:), patch_is(:), patch_ie(:)
    TYPE (t_grid), Pointer :: grid
    TYPE (t_mixt), Pointer :: state
    TYPE (t_mixt_input), Pointer :: input
    REAL(KIND=RFREAL) :: dsgn, sndspdref2, invtempref, tempref, gamref
    REAL(KIND=RFREAL) :: XI_X, XI_Y, XI_Z, XI_T, bndry_h
    REAL(KIND=RFREAL) :: XI_X_TILDE, XI_Y_TILDE, XI_Z_TILDE
    REAL(KIND=RFREAL), Pointer :: XI_TAU(:,:), cv(:,:), gv(:,:)
    REAL(KIND=RFREAL), Pointer :: pnorm_vec(:), rhs(:,:)
    REAL(KIND=RFREAL), Pointer :: patch_ViscousFlux(:,:,:)
    REAL(KIND=RFREAL), Pointer :: patch_ViscousFluxAux(:,:,:)
    REAL(KIND=RFREAL), Pointer :: patch_ViscousRHSAux(:,:,:), rhs_auxVars(:,:)
    REAL(KIND=RFREAL), Pointer :: auxVars(:,:), Lambda(:,:), ibfac(:)
    REAL(KIND=RFREAL), Pointer :: gI1(:), ub(:), Tinv(:,:), Tmat(:,:)
    REAL(KIND=RFREAL), Pointer :: Aprime(:,:), gI2(:), matX(:,:), penalty(:) 
    REAL(KIND=RFREAL), Pointer :: auxVarsTarget(:,:), cvTarget(:,:)
    REAL(KIND=RFREAL), Pointer :: SBP_invPdiag_block1(:), JAC(:), MT1(:,:)
    REAL(KIND=RFREAL) :: norm_vec(MAX_ND), gamma, ucon, ubAux
    REAL(KIND=RFREAL) :: sigmaI1, sigmaI2, sigmaAux, Sfactor, ibfac_local
    REAL(KIND=RFREAL) :: spd_snd, REInv, SAT_sigmaI2_FF, SAT_sigmaI1_FF

    ! ... Main data containers
    patch_gridID = patch%gridID
    grid  => region%grid(patch_gridID)
    state => region%state(patch_gridID)
    input => grid%input

    ! ... Pointer dereferences
    grid_is                    => grid%is
    grid_ie                    => grid%ie
    patch_is                   => patch%is
    patch_ie                   => patch%ie
    cv                         => state%cv
    gv                         => state%gv
    cvTarget                   => state%cvTarget
    rhs                        => state%rhs
    iblank                     => grid%iblank
    JAC                        => grid%JAC
    MT1                        => grid%MT1
    ibfac                      => grid%ibfac
    t2Map                      => region%global%t2Map
    XI_TAU                     => grid%XI_TAU
    rhs_auxVars                => state%rhs_auxVars
    patch_ViscousFlux          => patch%ViscousFlux
    patch_ViscousFluxAux       => patch%ViscousFluxAux
    SBP_invPdiag_block1        => grid%SBP_invPdiag_block1
    auxVars                    => state%auxVars
    auxVarsTarget              => state%auxVarsTarget

    ! ... Problem size
    ND = input%ND
    Nc = grid%nCells
    N(1:MAX_ND) = 1; Np(1:MAX_ND) = 1;
    Do j = 1, ND
      N(j)  =  grid_ie(j) -  grid_is(j) + 1
      Np(j) = patch_ie(j) - patch_is(j) + 1
    End Do
    nAuxVars = input%nAuxVars

    ! ... Useful quantities about this boundary
    normDir = abs(patch%normDir)
    sgn = normDir / patch%normDir
    dsgn = dble(sgn)

    ! ... Reference quantities
    sndspdref2 = input%sndspdref * input%sndspdref
    invtempref = input%Cpref / sndspdref2
    tempref    = 1.0_8 / invtempref 
    gamref     = input%GamRef

    ! ... BC Constants
    SAT_sigmaI1_FF              = input%SAT_sigmaI1_FF
    SAT_sigmaI2_FF              = input%SAT_sigmaI2_FF
    REinv                       = state%REinv
    gas_dv_model                = input%gas_dv_model

    ! ... memory allocation
    Allocate(gI1(ND+2), uB(ND+2), Tmat(ND+2,ND+2), Tinv(ND+2,ND+2), penalty(ND+2))
    Allocate(gI2(ND+2), Aprime(ND+2,ND+2), Lambda(ND+2,ND+2), matX(ND+2,ND+2), pnorm_vec(ND))

    ! ... loop over all of the points
    Do k = patch_is(3), patch_ie(3)
      Do j = patch_is(2), patch_ie(2)
        Do i = patch_is(1), patch_ie(1)

          l0 = (k- grid_is(3))* N(1)* N(2) + (j- grid_is(2))* N(1) +  i- grid_is(1)+1
          lp = (k-patch_is(3))*Np(1)*Np(2) + (j-patch_is(2))*Np(1) + (i-patch_is(1)+1)

          ! ... iblank factor (either 1 or zero)
          ibfac_local = ibfac(l0)

          ! ... construct the normalized metrics
          XI_X = MT1(l0,t2Map(normDir,1)); XI_Y = 0.0_rfreal; XI_Z = 0.0_rfreal
          if (ND >= 2) XI_Y = MT1(l0,t2Map(normDir,2))
          if (ND == 3) XI_Z = MT1(l0,t2Map(normDir,3))
          XI_T = XI_TAU(l0,normDir)

          ! ... multiply by the Jacobian
          XI_X = XI_X * JAC(l0)
          XI_Y = XI_Y * JAC(l0)
          XI_Z = XI_Z * JAC(l0)
          XI_T = XI_T * JAC(l0)

          ! ... normalized metrics
          bndry_h = 1.0_rfreal / sqrt( (XI_X * XI_X) + (XI_Y * XI_Y) + (XI_Z * XI_Z) )
          XI_X_TILDE = XI_X * bndry_h
          XI_Y_TILDE = XI_Y * bndry_h
          XI_Z_TILDE = XI_Z * bndry_h

          ! ... inviscid penalty parameter
          sigmaI1 = -SAT_sigmaI1_FF * dsgn

          ! ... pull off the boundary data 
          uB(:) = cv(l0,:)

          ! ... target data
          gI1(:) = cvTarget(l0,:)

          ! ... follow Magnus's algorithm (norm_vec has unit length)
          norm_vec(1:MAX_ND) = (/ XI_X_TILDE, XI_Y_TILDE, XI_Z_TILDE /)
          pnorm_vec(:) = norm_vec(1:ND) * sqrt(XI_X**2 + XI_Y**2 + XI_Z**2)

          ! ... use target data to make implicit work better?
          GAMMA = gv(l0,1)
          If (gas_dv_model == DV_MODEL_IDEALGAS .or. &
              gas_dv_model == DV_MODEL_IDEALGAS_MIXTURE) then

            Call SAT_Form_Roe_Matrices(uB, gI1, gamma, pnorm_vec, Tmat, Tinv, Lambda)

          Else If (gas_dv_model == DV_MODEL_THERMALLY_PERFECT) then

            Call SAT_Form_Roe_Matrices_TP(uB, gI1, ND, gamma, gamref, pnorm_vec, Tmat, Tinv, Lambda)
          End If

          ! ... save ucon, speed_sound
          ucon    = Lambda(1,1)
          spd_snd = Lambda(ND+1,ND+1) - ucon

          ! ... only pick those Lambda that are incoming
          If ( sgn == 1 ) Then
            Do jj = 1, ND+2
              Lambda(jj,jj) = max(Lambda(jj,jj),0.0_rfreal)
            End Do
          Else 
            Do jj = 1, ND+2
              Lambda(jj,jj) = min(Lambda(jj,jj),0.0_rfreal)
            End Do
          End If

          ! ... modify Lambda if subsonic outflow
          ! ... left boundary
          if (sgn == 1 .and. &
              ucon < 0.0_rfreal .and. ucon + spd_snd > 0.0_rfreal) then
            Lambda(ND+2,ND+1) = Lambda(ND+2,ND+1) - ucon - spd_snd
          ! right boundary
          else if (sgn == -1 .and. &
              ucon > 0.0_rfreal .and. ucon - spd_snd < 0.0_rfreal) then
            Lambda(ND+1,ND+2) = Lambda(ND+1,ND+2) - ucon + spd_snd
          end if

          ! ... multiply Lambda' into X
          matX = MATMUL(Lambda,Tinv)

          ! ... compute the characteristic matrix
          Aprime = MATMUL(Tmat,matX)

          ! ... subtract off the target
          uB(1:ND+2) = uB(1:ND+2) - gI1(1:ND+2)

          ! ... compute the characteristic penalty vector
          gI2(1:ND+2) = MATMUL(Aprime,uB)

          ! ... compute penalty (Bndry_h is included in Lambda already)
          Do jj = 1, ND+2
            rhs(l0,jj) = rhs(l0,jj) &
                       + sigmaI1 * SBP_invPdiag_block1(1) * gI2(jj)
          End Do

          ! ... SAT FARFIELD (species treatment)
          If (dsgn * ucon > 0) Then
            Sfactor =  ucon * ibfac_local * sigmaI1 * SBP_invPdiag_block1(1)
            Do jj = 1, nAuxVars
              rhs_auxVars(l0,jj) = rhs_auxVars(l0,jj) + Sfactor * &
                (auxVars(l0,jj) - auxVarsTarget(l0,jj)/cvTarget(l0,1)*cv(l0,1))
            End do
          End If

          ! ... Work out the viscous terms
          If ( REinv > 0.0_rfreal ) then

            ! ... factor
            sigmaI2 = dsgn * SAT_sigmaI2_FF * SBP_invPdiag_block1(1) / bndry_h

            ! ... target state (could get this from derivatives of the 
            ! ... base state)
            Do jj = 1, ND+2
              gI2(jj) = 0.0_rfreal
            End do

            ! ... boundary data (already includes 1/Re factor)
            uB(:) = patch%ViscousFlux(:,lp,1) * XI_X_TILDE
            if (ND >= 2) &
               uB(:) = uB(:) + patch%ViscousFlux(:,lp,2) * XI_Y_TILDE
            if (ND == 3) &
               uB(:) = uB(:) + patch%ViscousFlux(:,lp,3) * XI_Z_TILDE

            ! ... add to RHS
            Do jj = 1, ND+2
              rhs(l0,jj) = rhs(l0,jj) &
                         + ibfac_local * sigmaI2 * (uB(jj) - gI2(jj))
            End Do

            ! ... now update the scalars
            sigmaAux = ibfac_local * sigmaI2
            Do jj = 1, nAuxVars
              ubAux = patch_ViscousFluxAux(jj,lp,normDir) 
              rhs_auxVars(l0,jj) = rhs_auxVars(l0,jj) + sigmaAux * ubAux
            End Do

          End If

        End Do
      End Do
    End Do

    Deallocate(gI1, uB, Tmat, Tinv, penalty)
    Deallocate(gI2, Aprime, Lambda, matX, pnorm_vec)

    Return

  End Subroutine NS_BC_SAT_FAR_FIELD

END MODULE ModNavierStokesBC
