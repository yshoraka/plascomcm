! Copyright (c) 2015, University of Illinois at Urbana-Champaign, XPACC
! License: MIT, http://opensource.org/licenses/MIT
!----------------------------------------------------------------------
!
! ModSBI - Sets up and performs communication across conformal
!          inter-block and C-like intra-block interfaces 
! 
! - API for native SAT Block-Interface
!   -- Setup
!      - SAT_BLOCK_INTERFACE_SETUP        (ModPlasComCM.fpp)
!      - INIT_SBI_PATCH_DATA                      (internal)
!   -- Exchange
!      - Native_Block_Interface_Exchange (ModRungeKutta.fpp)
!      - INIT_SBI_EXCHANGE                        (internal)
!      - FINALIZE_SBI_EXCHANGE                    (internal)
!   -- Cleanup (currently not called)
!      - DESTROY_SBI_COMM_BUFFERS
!      - DESTROY_SBI_PATCH_DATA
!
!-----------------------------------------------------------------------
Module ModSBI

CONTAINS

  subroutine DESTROY_SBI_PATCH_DATA(region)
    
    USE ModGlobal
    USE ModDataStruct
    
    Implicit None
    
    TYPE (t_region), POINTER :: region
    
    ! ... local variables
    type(t_patch), pointer :: patch
    
    integer :: npatch
    
    do npatch = 1, region%nPatches

       patch => region%patch(npatch)
       
       if ( patch%bcType == SAT_BLOCK_INTERFACE .or. patch%bcType == FV_BLOCK_INTERFACE ) then
          IF(patch%ncomp .GT. 0) THEN
             DEALLOCATE(patch%sat_send_buffer)
             DEALLOCATE(patch%sat_recv_buffer)
             patch%ncomp = 0
          ENDIF
       endif
    end do
    
  end subroutine DESTROY_SBI_PATCH_DATA

  subroutine init_sat_descriptor(sat_descriptor)

    USE ModDataStruct
    implicit none

    type(t_sat_descriptor), pointer, intent(out) :: sat_descriptor

    allocate(sat_descriptor)

    sat_descriptor%comm = 0
    sat_descriptor%rank = 0
    sat_descriptor%color = 0
    sat_descriptor%np = 0
    sat_descriptor%nlocal_sats = 0
    sat_descriptor%nremote_sats = 0
    sat_descriptor%nrequests = 0
    nullify(sat_descriptor%requests)
    nullify(sat_descriptor%statuses)
    nullify(sat_descriptor%local_sats)
    nullify(sat_descriptor%remote_sat_info)
    nullify(sat_descriptor%comm_buffers)

  end subroutine init_sat_descriptor

  SUBROUTINE INIT_SBI_PATCH_DATA(region)
    
    USE ModGlobal
    USE ModDataStruct
    USE ModMPI

    IMPLICIT NONE
    
    TYPE (t_region), POINTER :: region
    
! ... local variables
    TYPE(t_grid), POINTER  :: grid
    TYPE(t_patch), POINTER :: patch
    TYPE(t_mixt), POINTER :: state
    TYPE(t_sat_descriptor), POINTER :: sat_descriptor

    INTEGER :: npatch, N(MAX_ND),Np(MAX_ND), point_count
    INTEGER :: datasize, i, j, k, l0, lp, icomp, ioffset(3)
  
    sat_descriptor => region%sat_descriptor

    IF(sat_descriptor%color .gt. 0) THEN
       DO npatch = 1, sat_descriptor%nlocal_sats
          patch => region%patch(sat_descriptor%local_sats(npatch))
          
          grid   => region%grid(patch%gridID)
          state => region%state(patch%gridID)
          
          datasize = SIZE(patch%cv_in,2)
          
          IF(datasize .le. 0) CALL graceful_exit(region%myrank, 'ERROR: Invalid datasize for SBI patch data.')
          
          IF(patch%ncomp .NE. datasize) THEN
             IF(patch%ncomp .GT. 0) THEN
                DEALLOCATE(patch%sat_send_buffer)
                DEALLOCATE(patch%sat_recv_buffer)
             ENDIF
             ALLOCATE(patch%sat_send_buffer(datasize*SIZE(patch%patch_xyz)/3))
             ALLOCATE(patch%sat_recv_buffer(datasize*SIZE(patch%patch_xyz)/3))
             patch%ncomp = datasize
          ENDIF
          DO j = 1, grid%ND
             N(j)  = grid%ie(j) - grid%is(j)+1
             Np(j) = patch%ie(j) - patch%is(j) + 1
          END DO
          
          point_count = 0
          patch%sat_send_buffer(:) = 0.0_8

          ! ... check for offset
          ioffset = 0
          if (patch%bcType == FV_BLOCK_INTERFACE) ioffset(abs(patch%normDir)) = patch%normDir / abs(patch%normDir)
          
          DO k = patch%is(3), patch%ie(3)
             DO j = patch%is(2), patch%ie(2)
                DO i = patch%is(1), patch%ie(1)
                   
                   lp = (k-patch%is(3))*Np(1)*Np(2) + (j-patch%is(2))*Np(1) + (i-patch%is(1)+1)
                   point_count = point_count+1
                   DO icomp = 1,datasize
                      patch%sat_send_buffer(datasize*(point_count-1) + icomp) = patch%cv_in(lp,icomp)
                   ENDDO
                   
                END DO
             END DO
          END DO
       END DO
    ENDIF
  END SUBROUTINE INIT_SBI_PATCH_DATA

  subroutine DESTROY_SBI_COMM_BUFFERS(region)

    USE ModDataStruct
    USE ModMPI
    
    Implicit None
    
    TYPE (t_region), POINTER :: region
    TYPE (t_sat_descriptor), POINTER :: sat_descriptor
    INTEGER :: isat

    sat_descriptor => region%sat_descriptor
    
    IF((sat_descriptor%color .gt. 0) .and. (sat_descriptor%nremote_sats .gt. 0) ) THEN
       if(sat_descriptor%nrequests .gt. 0) deallocate(sat_descriptor%requests)
       sat_descriptor%nrequests = 0
       do isat = 1, sat_descriptor%nremote_sats
          if(sat_descriptor%comm_buffers(isat)%nrecv .gt. 0) then
             deallocate(sat_descriptor%comm_buffers(isat)%rbuffer)
             sat_descriptor%comm_buffers(isat)%nrecv = 0
          endif
       end do
!       deallocate(sat_descriptor%comm_buffers)
    ENDIF
  end subroutine DESTROY_SBI_COMM_BUFFERS

  ! ... Here is the procedure for completing the communications  
  ! ... for the SAT_BLOCK_INTERFACE patch exchange 
  SUBROUTINE FINALIZE_SBI_EXCHANGE(region)

    USE ModGlobal
    USE ModDataStruct
    USE ModMPI
    USE ModDataUtils
    
    Implicit None
    
    TYPE (t_region), POINTER :: region
    
! ... local variables
    TYPE(t_grid), POINTER  :: grid
    TYPE(t_patch), POINTER :: patch
    TYPE(t_sat_descriptor), POINTER :: sat_descriptor
    
    INTEGER :: npatch, N(MAX_ND),Np(MAX_ND), point_count,nindex,ipts,npts,ierr
    INTEGER :: datasize, i, j, k, l0, lp, icomp, icoli,iproc,isat,isatx,cindex
    REAL(RFREAL), POINTER :: remote_data(:)
    REAL(RFREAL), POINTER :: local_data(:)
    INTEGER, POINTER :: indices(:)

    sat_descriptor => region%sat_descriptor
    
!   IF(region%myrank .eq. 0) WRITE(*,*) 'PlasComCM: Finalizing SBI Exchange.'
    ! Wait on any pending communications
   IF(sat_descriptor%color .gt. 0) THEN
      IF(sat_descriptor%nrequests .GT. 0) THEN
         CALL MPI_WAITALL(sat_descriptor%nrequests,sat_descriptor%requests,sat_descriptor%statuses,ierr)
         IF(ierr .GT. 0) THEN
            WRITE(*,*) 'PlasComCM: ERROR: SBI COMMUNICATION FAILED TO FINALIZE.'
            CALL graceful_exit(region%myrank, 'SBI COMMUNICATION FINALIZATION FAILED.')
         ENDIF
         sat_descriptor%nrequests = 0
         !       deallocate(sat_descriptor%requests)
      ENDIF
      CALL MPI_BARRIER(sat_descriptor%comm,ierr)
      datasize = region%patch(sat_descriptor%local_sats(1))%ncomp
      IF(datasize .le. 0) CALL graceful_exit(region%myrank, 'ERROR: Invalid datasize for SBI exchange finalization')
      DO isat = 1,sat_descriptor%nlocal_sats
         patch => region%patch(sat_descriptor%local_sats(isat))
         npts = SIZE(patch%patch_xyz)/3
         local_data => patch%sat_recv_buffer
         IF(SIZE(local_data) .ne. datasize*npts) THEN
            CALL graceful_exit(region%myrank, 'ERROR: Recv Buffer size mismatch in FINALIZE_SBI_EXCHANGE')
         ENDIF
         local_data(:) = 0.0_8
         ! ... Copy the received data out of the receive buffers and into the 
         ! ... appropriate local SBI patch buffers
         IF(patch%nremote_collisions .GT. 0) THEN
            DO icoli = 1,patch%nremote_collisions
               nindex = 4*(icoli-1)+1
               isatx =  patch%remote_collisions(nindex)
               remote_data => sat_descriptor%comm_buffers(isatx)%rbuffer
               indices => patch%remote_buffer_indices(:,icoli)
               IF(npts .ne. SIZE(indices)) THEN
                  CALL graceful_exit(region%myrank, 'ERROR: Index Buffer size mismatch in FINALIZE_SBI_EXCHANGE')
               ENDIF
!               write(*,*) sat_descriptor%rank,sat_descriptor%comm_buffers(isatx)%rrank,icoli,'r'
               DO ipts = 1, npts
                  IF(indices(ipts) .GT. 0) THEN
                     nindex = datasize*(ipts - 1)
                     cindex = datasize*(indices(ipts) - 1)
                     DO icomp = 1,datasize
                        local_data(nindex+icomp) = remote_data(cindex+icomp)
 !                       write(*,*)nindex+icomp, cindex+icomp,local_data(nindex+icomp)
                     ENDDO
                  ENDIF
               END DO
            ENDDO
         ENDIF
      END DO
      CALL MPI_BARRIER(sat_descriptor%comm,ierr)
      DO isat = 1,sat_descriptor%nlocal_sats
         patch => region%patch(sat_descriptor%local_sats(isat))
         npts = SIZE(patch%patch_xyz)/3
         local_data => patch%sat_recv_buffer
         IF((npts*datasize) .ne. SIZE(patch%sat_recv_buffer)) &
              CALL graceful_exit(region%myrank, 'ERROR: Buffer size mismatch in Finalize_SBI_Exchange.')

         ! ... Copy the received data out of the local patch buffers and into the 
         ! ... appropriate local SBI patch buffers
         IF(patch%nlocal_collisions .GT. 0) THEN
            DO icoli = 1,patch%nlocal_collisions
               nindex = 4*(icoli-1)+1
               isatx =  patch%local_collisions(nindex)
               remote_data => region%patch(sat_descriptor%local_sats(isatx))%sat_send_buffer
               indices => patch%local_buffer_indices(:,icoli)
               DO ipts = 1, npts
                  IF(indices(ipts) .GT. 0) THEN
                     nindex = datasize*(ipts - 1)
                     cindex = datasize*(indices(ipts) - 1)
                     DO icomp = 1,datasize
                        local_data(nindex+icomp) = remote_data(cindex+icomp)
                     ENDDO
                  ENDIF
               END DO
            ENDDO
         ENDIF
      END DO
      CALL MPI_BARRIER(sat_descriptor%comm,ierr)
      DO isat = 1,sat_descriptor%nlocal_sats
         patch => region%patch(sat_descriptor%local_sats(isat))
         npts = SIZE(patch%patch_xyz)/3
         local_data => patch%sat_recv_buffer
         ! ... Finally, copy the data back from the received buffers to the qinterp_local array
         grid   => region%grid(patch%gridID)

         DO j = 1, grid%ND
            N(j)  = grid%ie(j) - grid%is(j)+1
            Np(j) = patch%ie(j) - patch%is(j) + 1
         END DO
         
         point_count = 0
         DO k = patch%is(3), patch%ie(3)
            DO j = patch%is(2), patch%ie(2)
               DO i = patch%is(1), patch%ie(1)
                  lp = (k-patch%is(3))*Np(1)*Np(2) + (j-patch%is(2))*Np(1) + (i-patch%is(1)+1)
                  point_count = point_count+1            
                  DO icomp = 1,datasize
                     patch%cv_out(lp,icomp) = local_data(datasize*(point_count - 1)+icomp)
                  ENDDO
               END DO
            END DO
         END DO
      END DO
      CALL MPI_BARRIER(sat_descriptor%comm,ierr)
   END IF
   CALL MPI_BARRIER(mycomm,ierr)
   !   IF(region%myrank .eq. 0) WRITE(*,*) 'PlasComCM: SBI Exchange Finalized'
   
 END SUBROUTINE FINALIZE_SBI_EXCHANGE

  ! ... Here is the procedure for initiating the communications 
  ! ... for the SAT_BLOCK_INTERFACE patch exchange 
  SUBROUTINE INIT_SBI_EXCHANGE(region)

    USE ModGlobal
    USE ModDataStruct
    USE ModMPI
    USE ModDataUtils
    
    Implicit None

    TYPE (t_region), POINTER :: region
    TYPE (t_sat_descriptor), POINTER :: sat_descriptor

    ! ... local variables
    type(t_patch), pointer :: patch
    type(t_grid), pointer  :: grid
    type(t_mixt), pointer  :: state
    type(t_mixt_input), pointer :: input

    INTEGER :: isat, nnodes, nindex, rrank, tag, request_count, iproc, nVals, ierr

    sat_descriptor => region%sat_descriptor

    sat_descriptor%nrequests = 0
!   IF(region%myrank .eq. 0) WRITE(*,*) 'PlasComCM: Initiating SBI Exchange.'
    IF(sat_descriptor%color .GT. 0) THEN
       nVals = region%patch(sat_descriptor%local_sats(1))%ncomp
       request_count = 0
       sat_descriptor%requests(:) = 0
       ! ... Post the receives
       CALL MPI_BARRIER(sat_descriptor%comm,ierr)
       IF(ierr .ne. 0) THEN
          CALL graceful_exit(region%myrank, 'MPI ERROR in INIT_SBI_EXCHANGE')
       ENDIF
!       IF(sat_descriptor%rank .EQ. 0) WRITE(*,*) 'PlasComCM: Initiating SBI Recvs'
       IF(sat_descriptor%nremote_sats > 0) THEN
          DO isat = 1,sat_descriptor%nremote_sats
             nindex = 3*(isat - 1) + 1
             nnodes = sat_descriptor%remote_sat_info(nindex+2)
             tag    = sat_descriptor%remote_sat_info(nindex+1)
             rrank  = sat_descriptor%remote_sat_info(nindex)
             IF(sat_descriptor%comm_buffers(isat)%nrecv .NE. (nnodes*nVals)) THEN
                IF(sat_descriptor%comm_buffers(isat)%nrecv > 0) THEN
                   DEALLOCATE(sat_descriptor%comm_buffers(isat)%rbuffer)
                ENDIF
                sat_descriptor%comm_buffers(isat)%nrecv = nnodes*nVals
                ALLOCATE(sat_descriptor%comm_buffers(isat)%rbuffer(nnodes*nVals))
             ENDIF
             request_count = request_count + 1
             CALL MPI_IRECV(sat_descriptor%comm_buffers(isat)%rbuffer,nnodes*nVals,MPI_DOUBLE_PRECISION, &
                  rrank,tag,sat_descriptor%comm,sat_descriptor%requests(request_count),ierr)
             IF(ierr .ne. 0) THEN
                CALL graceful_exit(region%myrank, 'MPI ERROR in INIT_SBI_EXCHANGE')
             ENDIF
          END DO
       END IF
       ! ... Post the sends 
       CALL MPI_BARRIER(sat_descriptor%comm,ierr)
       IF(ierr .ne. 0) THEN
          CALL graceful_exit(region%myrank, 'MPI ERROR in INIT_SBI_EXCHANGE')
       ENDIF
       !       IF(sat_descriptor%rank .EQ. 0) WRITE(*,*) 'PlasComCM: Initiating SBI Sends'
       DO isat = 1,sat_descriptor%nlocal_sats
          patch => region%patch(sat_descriptor%local_sats(isat))
          IF(patch%nremote_send .GT. 0) THEN
             DO iproc = 1,patch%nremote_send
                rrank = patch%remote_ranks(iproc)-1
                tag = isat
                request_count = request_count + 1
                IF(nVals*SIZE(patch%patch_xyz)/3 .ne. SIZE(patch%sat_send_buffer)) THEN
                   CALL graceful_exit(region%myrank, 'SBI SEND BUFFER SIZE MISMATCH')
                ENDIF
!                WRITE(*,*)sat_descriptor%rank,isat,rrank,nVals*SIZE(patch%patch_xyz)/3
                !Write (*,'(A,I6,A,I6)') '    In INIT_SBI_EXCHANGE, MPI_ISEND from Rank ', region%myrank, ' to ', rrank
                CALL MPI_ISEND(patch%sat_send_buffer,nVals*SIZE(patch%patch_xyz)/3,MPI_DOUBLE_PRECISION, &
                     rrank,tag,sat_descriptor%comm,sat_descriptor%requests(request_count),ierr)
                IF(ierr .ne. 0) THEN
                   CALL graceful_exit(region%myrank, 'MPI ERROR in INIT_SBI_EXCHANGE')
                ENDIF
             END DO
          ENDIF
       END DO
       sat_descriptor%nrequests = request_count
    END IF
! A subsequent call to WAITALL will ensure that the communications complete
    CALL MPI_BARRIER(mycomm,ierr)
!    IF(region%myrank .eq. 0) WRITE(*,*) 'PlasComCM: SBI Exchange Initiated'
  END SUBROUTINE INIT_SBI_EXCHANGE


  ! ... Collides SAT_BLOCK_INTERFACE patches across all processors
  ! ... and populates the data structures necessary for full native 
  ! ... support of partitioned SAT_BLOCK_INTERFACE patches in 2D
  ! ... and in 3D. (mtc)
  ! 
  ! ... Note: The calls to CollideCoords are unnecessary and should
  ! ... be replaced by calls to CoordsCollide.  e.g.:
  ! ... IF(CoordsCollide(...) .eqv. .true.) THEN
  ! ... This call became unnecessary in the upgrade to full 3D support
  !  
  SUBROUTINE SAT_BLOCK_INTERFACE_SETUP(region)
    
    USE ModGlobal
    USE ModDataStruct
    USE ModMPI
    USE ModDataUtils
    
    Implicit None
    
    TYPE (t_region), POINTER :: region
    
    ! ... local variables
    type(t_patch),      pointer :: patch
    type(t_grid),       pointer  :: grid
    type(t_mixt),       pointer  :: state
    type(t_mixt_input), pointer :: input
    type(t_sat_descriptor), pointer :: sat_descriptor
    
    ! ... local dynamic arrays
    type(t_comm_buf), pointer :: comm_buffers(:)
    integer, pointer :: nnodes_sat_patch(:),sat_patch_info(:),nsat_p(:)
    integer, pointer :: offsets(:), sat_patch_info_p(:), requests(:)
    integer, pointer :: nsend_p(:),sat_patches(:)
    integer, pointer :: recv_from_sat(:),nrecv_from_sat_processor(:)
    integer, pointer :: send_to_sat_processor(:,:),local_collisions(:)
    integer, pointer :: remote_collisions(:,:), nremote_collisions(:)
    integer, pointer :: collides_remotely(:),remote_sats(:,:), remote_local(:)
    integer, pointer :: remote_sat_ids(:), patchtmp(:), remote_ranks(:),statuses(:,:)

    integer :: nsat_tot, sat_color, sat_processor, nsat_tot_p,nsat_local, ierr
    integer :: ND, Nc, N(MAX_ND), Np(MAX_ND), npatch, i, j, k, l0, lp, nnodes
    integer :: point_count, patch_count, sat_count, local_patch_count, sat_index
    integer :: sat_rank, nrequest, npts, rgrid_id, local_grid_id
    integer :: nindex, nloc,nlocp,istart, sstart, nlocal_collisions, ncomm, target_grid_id
    integer :: target_nnodes, nremote_sats, cindex, remote_sat_id,nremote_local, insat, ncoli
    integer :: nremote_ranks,sub_comm,sub_rank,sub_np,isat,iproc,nrcol,remote_rank,remote_patch_id

    real(rfreal) :: timer

    timer = MPI_Wtime()

    IF(region%myrank .eq. 0) WRITE(*,*) 'PlasComCM: Setting up for SAT_BLOCK_INTERFACE boundaries.'

    ! ... simplicity
    sat_descriptor => region%sat_descriptor
    ND = region%input%ND
    nsat_local = 0
    sat_color = 0
   
    allocate(sat_patches(region%nPatches))
    allocate(nnodes_sat_patch(region%nPatches))
    sat_patches(:) = 0
    nnodes_sat_patch(:) = 0

    ! ... Count SAT_BLOCK_INTERFACE boundaries and set up coordinates
    do npatch = 1, region%nPatches
       
       patch => region%patch(npatch)
       grid  => region%grid(patch%gridID)
!       state => region%state(patch%gridID)
!       input => grid%input
       
       if ( patch%bcType == SAT_BLOCK_INTERFACE .or. patch%bcType == FV_BLOCK_INTERFACE ) then
          nsat_local = nsat_local + 1
          sat_patches(nsat_local) = npatch
          do j = 1, grid%ND
             N(j)  = grid%ie(j) - grid%is(j)+1
             Np(j) = patch%ie(j) - patch%is(j) + 1
          end do
          
          point_count = 0
          patch%patch_xyz(:) = 0.0_8
          Do k = patch%is(3), patch%ie(3)
             Do j = patch%is(2), patch%ie(2)
                Do i = patch%is(1), patch%ie(1)
                   
                   l0 = (k-grid%is(3))*N(1)*N(2) + (j-grid%is(2))*N(1) + i-grid%is(1)+1
                   lp = (k-patch%is(3))*Np(1)*Np(2) &
                        + (j-patch%is(2))*Np(1) + (i-patch%is(1)+1)
                   point_count = point_count + 1  
                   nindex = 3*(point_count - 1) + 1
                   patch%patch_xyz(nindex)   = grid%XYZ(l0,1)
                   if(grid%ND >= 2) patch%patch_xyz(nindex+1) = grid%XYZ(l0,2)
                   if(grid%ND == 3) patch%patch_xyz(nindex+2) = grid%XYZ(l0,3)
                end do
             end do
          end do
          nnodes_sat_patch(nsat_local) = point_count
       endif
    end do

    sat_descriptor%color = 0
    if(nsat_local .gt. 0) sat_descriptor%color = 1
    sat_color = sat_descriptor%color
    sat_descriptor%nlocal_sats = 0
    
    CALL MPI_COMM_SPLIT(mycomm,sat_color,region%myrank,sat_descriptor%comm,ierr)
    sub_comm = sat_descriptor%comm
    
    IF(nsat_local .GT. 0) THEN
       nsat_tot_p = 0
       CALL MPI_COMM_SIZE(sub_comm,sat_descriptor%np,ierr)
       CALL MPI_COMM_RANK(sub_comm,sat_descriptor%rank,ierr)
       sub_np = sat_descriptor%np
       sub_rank = sat_descriptor%rank
       sat_descriptor%nlocal_sats = nsat_local
       allocate(sat_descriptor%local_sats(nsat_local))
       sat_descriptor%local_sats(1:nsat_local) = sat_patches(1:nsat_local)
       
       ! Debugging report
       if((debug_on .eqv. .true.)) then
          IF(sub_rank .EQ. 0) WRITE(*,'(A,I4)') 'PlasComCM: Number of processors with SBI:',sub_np
          DO iproc = 1,sub_np
             IF((sub_rank+1) .EQ. iproc) THEN
                WRITE(*,'(A,I4,A,I4)') 'PlasComCM: Processor ',region%myrank+1,' is SBI processor ',iproc
             ENDIF
             CALL MPI_BARRIER(sub_comm,ierr)
          ENDDO
       endif

       ! ... sub_np   = total number of processors with a SAT_BLOCK_INTERFACE
       ! ... sub_rank = local rank on the SAT_BLK_X communicator
       !      WRITE(*,*) 'PlasComCM:',region%myrank+1,':SBI',sub_rank+1,'/',sub_np
       allocate(nsat_p(sub_np))
       allocate(nsend_p(sub_np))
       allocate(offsets(sub_np))
       allocate(sat_patch_info(2*nsat_local))
       nsat_p(:)  = 0
       nsend_p(:) = 0
       offsets(:) = 0
       sat_patch_info(:) = 0

       ! ... Allgather will find out the total number of SAT_BLK_X on each processor
       ! ... and, by extension, the total number of SAT_BLK_X across all processors
       ! ... Both of these pieces of information are needed in the following sections
       CALL MPI_ALLGATHER(nsat_local,1,MPI_INTEGER,nsat_p,1,MPI_INTEGER,sub_comm,ierr)
       if(ierr .ne. 0) CALL graceful_exit(region%myrank, 'MPI call failed in SBI processing.')
       
       nsat_tot_p = SUM(nsat_p)
       allocate(comm_buffers(nsat_tot_p))
       DO npatch = 1,nsat_tot_p
          comm_buffers(npatch)%nsend = 0
          comm_buffers(npatch)%nrecv = 0
       ENDDO
       ! ... Get the count totals and pack the information to send to other processors
       ! ... in the SAT_BLK_X communicator.   At this point, we send the gridID and the
       ! ... number of points on the SAT_BLOCK_INTERFACE patch.  This information will 
       ! ... be used to weed out SAT_BLOCK_INTERFACEs that belong to a common Grid, and
       ! ... to size the required buffers for XYZ communications.
       do npatch = 1, nsat_local
          patch => region%patch(sat_patches(npatch))
          grid  => region%grid(patch%gridID)
          sat_patch_info((npatch-1)*2 + 1) = grid%iGridGlobal
          sat_patch_info((npatch-1)*2 + 2) = nnodes_sat_patch(npatch)
       enddo
       offsets(1) = 0
       nsend_p(1) = 2*nsat_p(1)
       if(sub_np .gt. 1) then
          do sat_processor = 2, sub_np
             offsets(sat_processor) = offsets(sat_processor - 1) + (2*nsat_p(sat_processor - 1))
             nsend_p(sat_processor) = 2*nsat_p(sat_processor)
          enddo
       endif
       
       allocate(sat_patch_info_p(2*nsat_tot_p))
       allocate(recv_from_sat(nsat_tot_p))
       allocate(nrecv_from_sat_processor(sub_np))
       allocate(requests(2*(nsat_local*nsat_tot_p + nsat_tot_p)))
       allocate(send_to_sat_processor(sub_np,nsat_local))
       nrecv_from_sat_processor(:) = 0
       nrequest = 0
       recv_from_sat(:) = 0
       sat_patch_info_p(:) = 0
       requests(:) = 0
       send_to_sat_processor(:,:) = 0
       ! ... This Allgatherv takes the sat_patch_info arrays from each remote processor and packs
       ! ... it into sat_patch_info_p.
       CALL MPI_ALLGATHERV(sat_patch_info,2*nsat_local,MPI_INTEGER,sat_patch_info_p,nsend_p, &
            offsets,MPI_INTEGER,sub_comm,ierr)
       if(ierr .ne. 0) CALL graceful_exit(region%myrank, 'MPI call failed in SBI processing.')
       deallocate(nsend_p)
       deallocate(offsets)
       

       ! Debugging report
       if((debug_on .eqv. .true.))then
          if(sub_rank .EQ. 0)  &
               WRITE(*,*) 'PlasComCM: Total number of SBI: ',nsat_tot_p
          sat_count = 1
          do sat_processor = 1, sub_np
             write(*,'(A,I4,A)')'PlasComCM: Sat processor',sat_processor,':'
             do sat_index = 1,nsat_p(sat_processor)
                nindex = 2*(sat_count - 1) + 1
                write(*,'(A,I4,A,I4,A,I4)')'PlasComCM: Patch: ',sat_index,' GridId: ',sat_patch_info_p(nindex), &
                     ' Nnodes: ',sat_patch_info_p(nindex+1)
                sat_count = sat_count + 1
             enddo
          end do
       endif

       ! ... Now need to loop through each of the locally owned SAT_BLOCK_INTERFACE patches and 
       ! ... find out the candidate matching SAT_BLOCK_INTERFACES.  These candidates can be remote
       ! ... or local.  
       allocate(local_collisions(4*nsat_local*nsat_local)) 
       allocate(remote_collisions(3*(nsat_tot_p - nsat_local),nsat_local))
       allocate(nremote_collisions(nsat_local))
       ! ... local_patch_collisions(npts,sstart,+-istart)
       do npatch = 1, nsat_local
          patch => region%patch(sat_patches(npatch))
          ! ... Loop through all SAT_BLOCK_INTERFACEs - local and remote and identify which
          ! ... ones we'll need to exchange coordinates with for the actual comparison 
          sat_count = 1
          nlocal_collisions = 0
          send_to_sat_processor(:,npatch) = 0
          local_collisions(:) = 0
          nremote_collisions(npatch)  = 0
          remote_collisions(:,npatch) = 0
          do sat_processor = 1, sub_np
             sat_rank = sat_processor - 1
             do sat_index = 1, nsat_p(sat_processor)
                rgrid_id = sat_patch_info_p((2*(sat_count-1)+1))
                local_grid_id = region%grid(patch%GridID)%iGridGlobal
                ! ... If the grid_id of the current local patch we are looking at does
                ! ... not match the grid_id for the one in the inner loop, then it is
                ! ... a viable candidate
                if(local_grid_id .ne. rgrid_id) then
                   if(sat_rank .ne. sub_rank) then
                      ! ... If the viable candidate is on a remote processor, then initiate
                      ! ... the necessary communications
                      if(send_to_sat_processor(sat_processor,npatch) == 0) then
                        ! ... Each local SBI only needs to be sent *once* to each remote processor
                         send_to_sat_processor(sat_processor,npatch) = 1
                         nrequest = nrequest + 1
                         !Write (*,'(A,I6,A,I6)') '    In SAT_BLOCK_INTERFACE_SETUP, MPI_ISEND from Rank ', region%myrank, ' to ', sat_rank
                         CALL MPI_ISEND(patch%patch_xyz,nnodes_sat_patch(npatch)*3,MPI_DOUBLE_PRECISION, &
                              sat_rank,npatch,sub_comm,requests(nrequest),ierr)
                         !                        write(*,*)sub_rank,'sending',npatch,SIZE(patch%patch_xyz)/3,'to',sat_rank
                         if(ierr .ne. 0) CALL graceful_exit(region%myrank, 'MPI call failed in SBI processing.')
                      endif
                      if(recv_from_sat(sat_count) == 0) then
                         ! ... Each SBI only needs to be received once
                         recv_from_sat(sat_count) = 1
                         nrecv_from_sat_processor(sat_processor) = nrecv_from_sat_processor(sat_processor)+1
                         nrequest = nrequest + 1
                         nnodes = sat_patch_info_p(2*(sat_count-1)+2)
                         allocate(comm_buffers(sat_count)%rbuffer(nnodes*3))
                         comm_buffers(sat_count)%nrecv = nnodes*3
                         comm_buffers(sat_count)%rrank = sat_rank
                         comm_buffers(sat_count)%rid   = sat_index
                         CALL MPI_IRECV(comm_buffers(sat_count)%rbuffer,nnodes*3,MPI_DOUBLE_PRECISION, &
                              sat_rank,nrecv_from_sat_processor(sat_processor),sub_comm,requests(nrequest),ierr)
                         if(ierr .ne. 0) CALL graceful_exit(region%myrank, 'MPI call failed in SBI processing.')
                         !                        write(*,*)sub_rank,'recving',nnodes,'from',&
                         !                             sat_rank,nrecv_from_sat_processor(sat_processor)
                      endif
                      nremote_collisions(npatch) = nremote_collisions(npatch) + 1
                      nindex = 3*(nremote_collisions(npatch) - 1) + 1
                      remote_collisions(nindex,npatch)   = sat_rank
                      remote_collisions(nindex+1,npatch) = sat_index
                      remote_collisions(nindex+2,npatch) = sat_count
                   else 
                      !                     write(*,*)sub_rank,'local',npatch,'w/local',sat_index
                      npts = 0
                      sstart = 0
                      istart = 0
                      if (sat_index > npatch) then
                         ! ... The viable candidate is on the local processor, go ahead and
                         ! ... do the comparison.
                         ! Note that in this case, the sat_index is a number in [1,nsat_local]
                         ! with the number *npatch* removed. If the sat_index is less than 
                         ! npatch, then the comparison has been done when the other patch was
                         ! processed.
                         CALL CollideCoords(patch%patch_xyz,region%patch(sat_patches(sat_index))%patch_xyz, &
                              nnodes_sat_patch(npatch),nnodes_sat_patch(sat_index),npts,sstart,istart)
                      else 
                         ! ... The collision has been done already, reuse it
                         nlocp = region%patch(sat_patches(sat_index))%nlocal_collisions
                         if(nlocp > 0) then
                            do nloc = 1, nlocp
                               nindex = 4*(nloc-1)+1
                               if(region%patch(sat_patches(sat_index))%local_collisions(nindex) == npatch) then
                                  npts   = region%patch(sat_patches(sat_index))%local_collisions(nindex+1)
                                  sstart = region%patch(sat_patches(sat_index))%local_collisions(nindex+2)
                                  istart = region%patch(sat_patches(sat_index))%local_collisions(nindex+3)
                                  if((sstart*istart) < 0) sstart = -1*sstart
                                  exit
                               endif
                            enddo
                         endif
                      endif
                      if(npts .gt. 0) then
                         ! ... Local patches have collided
                         nlocal_collisions = nlocal_collisions + 1
                         nindex = 4*(nlocal_collisions - 1)
                         local_collisions(nindex + 1) = sat_index
                         local_collisions(nindex + 2) = npts
                         local_collisions(nindex + 3) = sstart
                         local_collisions(nindex + 4) = istart
                         !                        if(istart > 0) then
                         !                           write(*,*)sub_rank,npatch,'lcollided',sat_index,npts
                         !                        else
                         !                           write(*,*)sub_rank,npatch,'lcollided',-1*sat_index,npts
                         !                        endif
                      endif
                   endif ! on same processor?
                endif ! have same iGlobalGrid?
                sat_count = sat_count + 1
             end do ! sat_index (1:nsat_p(sat_processor))
          end do ! sat_processor (1:sub_np)
          if(nlocal_collisions > 0) then
             patch%nlocal_collisions = nlocal_collisions 
             allocate(patch%local_collisions(4*nlocal_collisions))
            ! added for full 3D support (allows full index resolution)
             !             write(*,*) 'allocating',nnodes_sat_patch(npatch)
             allocate(patch%local_buffer_indices(nnodes_sat_patch(npatch),nlocal_collisions))
             patch%local_collisions(1:4*nlocal_collisions) = local_collisions(1:4*nlocal_collisions)
             patch%local_buffer_indices(:,:) = 0
             allocate(patchtmp(nnodes_sat_patch(npatch)))
             do ncoli = 1,nlocal_collisions
                nindex = 4*(ncoli-1)+1
                sat_index = local_collisions(nindex)
                patchtmp(:) = 0
                CALL CollideCoords2(patch%patch_xyz,patchtmp,&
                     region%patch(sat_patches(sat_index))%patch_xyz, &
                     nnodes_sat_patch(npatch),nnodes_sat_patch(sat_index),&
                     patch%local_collisions(nindex+2))
                patch%local_buffer_indices(:,ncoli) = patchtmp(:)
             enddo
             deallocate(patchtmp)
          endif
       end do ! npatch (1:nsat_local) 
       
       
       allocate(statuses(MPI_STATUS_SIZE,nrequest))
       CALL MPI_WAITALL(nrequest,requests,statuses,ierr)
       deallocate(statuses)
       if(ierr .ne. 0) CALL graceful_exit(region%myrank, 'MPI call failed in SBI processing.')
       
       CALL MPI_BARRIER(sub_comm,ierr)
       if(ierr .ne. 0) CALL graceful_exit(region%myrank, 'MPI call failed in SBI processing.')
       
       ! Debugging report
       if((debug_on .eqv. .true.)) then
          do sat_processor = 1, sub_np
             if((sub_rank+1) .eq.  sat_processor) then
                nlocal_collisions = 0
                do npatch = 1, nsat_local
                   patch => region%patch(sat_patches(npatch))
                   nlocal_collisions = nlocal_collisions + patch%nlocal_collisions
                enddo
                write(*,'(A,I4,A,I4)')'PlasComCM: Sat processor ',sat_processor,' local collisions : ',nlocal_collisions
                if(nlocal_collisions > 0) then
                   do npatch = 1, nsat_local
                      patch => region%patch(sat_patches(npatch))
                      nlocal_collisions = patch%nlocal_collisions
                      if(nlocal_collisions .gt. 0) then
                         do ncoli = 1,nlocal_collisions
                            nindex = 4*(ncoli-1) + 1
                            sat_index = patch%local_collisions(nindex)
                            nnodes = patch%local_collisions(nindex + 2)
                            write(*,'(A,I4,A,I4,A,I4)')'  PlasComCM: SBI Patch: ',npatch,' collides ',nnodes,' nodes locally with SBI ',sat_index
                         enddo
                      endif
                   end do
                end if
             end if
             CALL MPI_BARRIER(sub_comm,ierr)
          enddo
       end if
       

       deallocate(local_collisions) 
       ! ... At this time, all of the local interactions have been worked out and
       ! ... all of the remote coordinates to be processed have been communicated.
       ! ... Do the remote patch collisions to find out what processors we will
       ! ... actually need to talk to.
       allocate(collides_remotely(nsat_tot_p))
       allocate(remote_local(4*nsat_tot_p))
       allocate(remote_sat_ids(nsat_tot_p))
       allocate(remote_ranks(sub_np))
       nremote_sats = 0
       nremote_local = 0
       remote_sat_ids(:) = 0
       collides_remotely(:) = 0
       nremote_ranks = 0
       remote_ranks(:) = 0
       do npatch = 1, nsat_local
          patch => region%patch(sat_patches(npatch))
          remote_local(:) = 0
          remote_ranks(:) = 0
          nremote_ranks = 0
          nremote_local = 0
          do ncomm = 1, nremote_collisions(npatch)
             nindex = 3*(ncomm-1)+1
             sat_rank  = remote_collisions(nindex,npatch)
             sat_index = remote_collisions(nindex+1,npatch)
             sat_count = remote_collisions(nindex+2,npatch)
             nindex = 2*(sat_count-1)+1
             target_grid_id = sat_patch_info_p(nindex)
             target_nnodes  = sat_patch_info_p(nindex+1)
             !            write(*,*) sub_rank,'colliding',npatch,'with',sat_rank,sat_index
             npts = 0
             sstart = 0
             istart = 0
             CALL CollideCoords(patch%patch_xyz,comm_buffers(sat_count)%rbuffer, &
                  SIZE(patch%patch_xyz)/3,target_nnodes, npts,sstart,istart)
             if(npts > 0) then
                ! ... The local patch has collided with a remote patch, record
                ! ... the specifics to the local patch, and prepare the communication
                ! ... buffer for receiving the remote patch 
                !               if(istart > 0) then
                !                  write(*,*) sub_rank,'rcollided',npts,sat_rank,sat_index
                !               else
                !                  write(*,*) sub_rank,'rcollided',npts,sat_rank,-1*sat_index
                !               endif
                
                ! ... Populate the remote patch-specific info:
                ! ... - number of collision regions
                ! ... - for each collision region:
                ! ... -  istart,npts
                if(collides_remotely(sat_count) == 0) then
                   nremote_sats = nremote_sats + 1
                   remote_sat_ids(nremote_sats) = sat_count
                   collides_remotely(sat_count) = 1
                   remote_sat_id = nremote_sats
                   remote_ranks(comm_buffers(sat_count)%rrank+1) = 1
                else
                   remote_ranks(comm_buffers(sat_count)%rrank+1) = 1
                   collides_remotely(sat_count) = collides_remotely(sat_count) + 1
                   cindex = 2*collides_remotely(sat_count) - 1
                   remote_sat_id = 0
                   do insat = 1,nremote_sats
                      if(remote_sat_ids(insat) == sat_count) then
                         remote_sat_id = insat
                         exit
                      endif
                   enddo
                   if(remote_sat_id == 0) then
                      write(*,*) 'PlasComCM: ERROR: Could not find remote sat id.'
                      CALL graceful_exit(region%myrank, 'SAT_BLOCK_INTERFACE or FV_BLOCK_INTERFACE Setup error.')
                   endif
                endif
                
                ! ... Populate some temporary information 
                nremote_local = nremote_local+1
                cindex = 4*(nremote_local - 1)+1
                remote_local(cindex)   = remote_sat_id
                remote_local(cindex+1) = comm_buffers(remote_sat_ids(remote_sat_id))%rid
                remote_local(cindex+2) = comm_buffers(remote_sat_ids(remote_sat_id))%rrank
                remote_local(cindex+3) = npts
             endif ! (npts > 0)
          enddo ! nremote_collisions(npatch)
          
          ! ... Finally, populate the patch's representation of the collisions, if any
          if(nremote_local > 0) then
             nremote_ranks = SUM(remote_ranks)
             allocate(patch%remote_ranks(nremote_ranks))
             patch%nremote_send = nremote_ranks
             nremote_ranks = 0
             do ncoli = 1,sub_np
                if(remote_ranks(ncoli) > 0) then
                   nremote_ranks = nremote_ranks + 1
                   patch%remote_ranks(nremote_ranks) = ncoli
                endif
             end do
             patch%nremote_collisions = nremote_local
             allocate(patch%remote_collisions(4*nremote_local))
             ! ... added to enable full 3D support (allows full index resolution)
             allocate(patch%remote_buffer_indices(nnodes_sat_patch(npatch),nremote_local))
             patch%remote_buffer_indices(:,:) = 0
             patch%remote_collisions(1:4*nremote_local) = remote_local(1:4*nremote_local)
             allocate(patchtmp(nnodes_sat_patch(npatch)))
             do ncoli = 1,nremote_local
                cindex = 4*(ncoli-1)+1
                sat_count = remote_sat_ids(remote_local(cindex))
                nindex = 2*(sat_count-1)+1
                target_nnodes  = sat_patch_info_p(nindex+1)
                patchtmp(:) = 0
                CALL CollideCoords2(patch%patch_xyz,patchtmp,&
                     comm_buffers(sat_count)%rbuffer, &
                     nnodes_sat_patch(npatch),target_nnodes,&
                     patch%remote_collisions(cindex+3))
                patch%remote_buffer_indices(:,ncoli) = patchtmp(:)
             enddo
             deallocate(patchtmp)
          endif
          
       enddo ! npatch
       
       ! ... All that remains is to form the persistent information describing the
       ! ... data this processor will be receiving from remote
       ! ... Some care needs to be taken here to ensure that the right buffers are 
       ! ... matched.  The buffer matching strategy uses the "rid", which is the
       ! ... remote processor's identifier for the incoming patch.  This processor 
       ! ... inferred the remote patch id from the order in which it was received
       ! ... when coordinates were exchanged.  I think if we use the "rid" for the 
       ! ... the tag in the matched receive that it will ensure proper buffer matching.
       sat_descriptor%nremote_sats = nremote_sats
       ALLOCATE(sat_descriptor%remote_sat_info(3*nremote_sats))
       ! nindex is reused here as a temporary integer
       nindex = 2*sat_descriptor%nlocal_sats*sat_descriptor%nremote_sats + sat_descriptor%nremote_sats
       ALLOCATE(sat_descriptor%requests(nindex))
       ALLOCATE(sat_descriptor%statuses(MPI_STATUS_SIZE,nindex))
       ALLOCATE(sat_descriptor%comm_buffers(sat_descriptor%nremote_sats))
       DO isat = 1,nremote_sats
          sat_count = remote_sat_ids(isat)
          nindex = 3*(isat - 1) + 1
          sat_descriptor%remote_sat_info(nindex)   = comm_buffers(sat_count)%rrank
          sat_descriptor%remote_sat_info(nindex+1) = comm_buffers(sat_count)%rid
          sat_descriptor%remote_sat_info(nindex+2) = comm_buffers(sat_count)%nrecv/3
          sat_descriptor%comm_buffers(isat)%rrank = comm_buffers(sat_count)%rrank
          sat_descriptor%comm_buffers(isat)%rid   = comm_buffers(sat_count)%rid
          sat_descriptor%comm_buffers(isat)%nsend = 0
          sat_descriptor%comm_buffers(isat)%nrecv = 0
       END DO
       sat_descriptor%nrequests = 0
       
       CALL MPI_BARRIER(sat_descriptor%comm,ierr)
       if(ierr .ne. 0) CALL graceful_exit(region%myrank, 'MPI call failed in SBI processing.')
!       IF(sub_rank .EQ. 0) WRITE(*,*) 'PlasComCM: SBI processing done, deallocating.'
       
       ! Debugging report
       if((debug_on .eqv. .true.)) then
          do sat_processor = 1, sub_np
             if((sub_rank+1) .eq.  sat_processor) then
                nrcol = 0
                do npatch = 1, nsat_local
                   patch => region%patch(sat_patches(npatch))
                   nrcol = nrcol + patch%nremote_collisions
                enddo
                write(*,'(A,I4,A,I4)')'PlasComCM: Sat processor ',sat_processor,' remote collisions : ',nrcol
                if(nrcol > 0) then
                   do npatch = 1, nsat_local
                      patch => region%patch(sat_patches(npatch))
                      nrcol = patch%nremote_collisions
                      if(nrcol .gt. 0) then
                         do ncoli = 1,nrcol
                            nindex = 4*(ncoli-1) + 1
                            sat_index = patch%remote_collisions(nindex)
                            nnodes = patch%remote_collisions(nindex + 3)
                            remote_patch_id = patch%remote_collisions(nindex+1)
                            remote_rank = patch%remote_collisions(nindex+2)+1
                            write(*,'(A,I4,A,I4,A,I4,A,I4,I4)')'  PlasComCM: SBI Processor',sat_processor,' Patch: ',npatch,' collides ',nnodes,&
                                 ' nodes remotely with SBI ',remote_rank,remote_patch_id
                         enddo
                      endif
                   end do
                end if
             end if
             CALL MPI_BARRIER(sub_comm,ierr)
          enddo
       endif

       ! ... do away with temporary data structures
       DO isat = 1,nsat_tot_p
          IF(comm_buffers(isat)%nrecv > 0) DEALLOCATE(comm_buffers(isat)%rbuffer)
       END DO
       
       DEALLOCATE(comm_buffers)
       DEALLOCATE(remote_sat_ids)
       DEALLOCATE(remote_ranks)  
       DEALLOCATE(nsat_p)
       DEALLOCATE(sat_patch_info)
       DEALLOCATE(sat_patch_info_p)
       DEALLOCATE(recv_from_sat)
       DEALLOCATE(nrecv_from_sat_processor)
       DEALLOCATE(requests)
       DEALLOCATE(send_to_sat_processor)
       DEALLOCATE(remote_collisions)
       DEALLOCATE(nremote_collisions)
       DEALLOCATE(collides_remotely)
       DEALLOCATE(remote_local)
       
       
    END IF ! if(nsat_local .gt. 0)
    
    
    CALL MPI_BARRIER(mycomm,ierr)
    if(ierr .ne. 0) CALL graceful_exit(region%myrank, 'MPI call failed in SBI processing.')

    region%mpi_timings(:)%interp_setup = region%mpi_timings(:)%interp_setup + (MPI_Wtime() - timer)
 !   IF(region%myrank .EQ. 0) WRITE(*,*) 'PlasComCM: SBI setup done on all processors.'

    DEALLOCATE(sat_patches)
    DEALLOCATE(nnodes_sat_patch)

    ! Debugging report
    if((debug_on .eqv. .true.) .and. region%myrank .eq. 0) then
       WRITE(*,'(A)') 'PlasComCM: SBI Setup done.'      
    endif

    
  END SUBROUTINE SAT_BLOCK_INTERFACE_SETUP

  SUBROUTINE Native_Block_Interface_Exchange(region)

    USE ModGlobal
    USE ModDataStruct
    USE ModEOS
    USE ModDeriv
    USE ModMPI
    USE ModNASARotor

    implicit none

    ! ... function call variables
    type(t_region), pointer :: region

    ! ... local variables
    type(t_patch), pointer :: patch
    type(t_mixt), pointer :: state
    type(t_grid), pointer :: grid
    type(t_mixt_input), pointer :: input
    integer :: ng, npatch, i, j, k, l0, lp, N(MAX_ND), Np(MAX_ND), ierr
    real(rfreal) :: timer

    ! ... start the timer
    timer = MPI_WTime()
    CALL mpi_barrier(mycomm, ierr)

    input => region%input

    ! ... assign state data to patches
    IF (region%input%AdjNS .EQV. .TRUE.) THEN
      DO npatch = 1, region%nPatches
         patch => region%patch(npatch)
         grid => region%grid(patch%gridID)
         state => region%state(patch%gridID)
         N  = 1; Np = 1;
         DO j = 1, grid%ND
            N(j)  =  grid%ie(j) -  grid%is(j )+ 1
            Np(j) = patch%ie(j) - patch%is(j) + 1
         END DO;
         IF ( patch%bcType == SAT_BLOCK_INTERFACE .or. patch%bcType == FV_BLOCK_INTERFACE ) THEN
            DO k = patch%is(3), patch%ie(3)
               DO j = patch%is(2), patch%ie(2)
                  DO i = patch%is(1), patch%ie(1)
                     l0 = (k- grid%is(3))* N(1)* N(2) + (j- grid%is(2))* N(1) + (i- grid%is(1)+1)
                     lp = (k-patch%is(3))*Np(1)*Np(2) + (j-patch%is(2))*Np(1) + (i-patch%is(1)+1)
                     patch%cv_in(lp,:) = state%av(l0,:)
                  END DO
               END DO
            END DO
         END IF
      END DO
    ELSE
      DO npatch = 1, region%nPatches
         patch => region%patch(npatch)
         grid => region%grid(patch%gridID)
         state => region%state(patch%gridID)
         N  = 1; Np = 1;
         DO j = 1, grid%ND
            N(j)  =  grid%ie(j) -  grid%is(j )+ 1
            Np(j) = patch%ie(j) - patch%is(j) + 1
         END DO;
         IF ( patch%bcType == SAT_BLOCK_INTERFACE .or. patch%bcType == FV_BLOCK_INTERFACE ) THEN
            DO k = patch%is(3), patch%ie(3)
               DO j = patch%is(2), patch%ie(2)
                  DO i = patch%is(1), patch%ie(1)
                     l0 = (k- grid%is(3))* N(1)* N(2) + (j- grid%is(2))* N(1) + (i- grid%is(1)+1)
                     lp = (k-patch%is(3))*Np(1)*Np(2) + (j-patch%is(2))*Np(1) + (i-patch%is(1)+1)
                     patch%cv_in(lp,:) = state%cv(l0,:)
                  END DO
               END DO
            END DO
         END IF
      END DO
    END IF ! input%AdjNS

    If (input%caseID == 'NASA_ROTOR_STATOR') Then
      Call Interpolate_In_Theta_NASA(region)
      Call Exchange_Periodic_Data_NASA(region)
    End If

    CALL INIT_SBI_PATCH_DATA(region)
    CALL INIT_SBI_EXCHANGE(region)
    CALL FINALIZE_SBI_EXCHANGE(region)

    CALL mpi_barrier(mycomm, ierr)

    ! ... stop timer
    region%mpi_timings(:)%interp_nbix = region%mpi_timings(:)%interp_nbix + (MPI_WTime() - timer)

    Return

  END SUBROUTINE Native_Block_Interface_Exchange
  
END Module ModSBI
