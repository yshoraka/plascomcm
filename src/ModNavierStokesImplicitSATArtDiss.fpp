! Copyright (c) 2015, University of Illinois at Urbana-Champaign, XPACC
! License: MIT, http://opensource.org/licenses/MIT
!----------------------------------------------------------------------
!
! ModNavierStokesImplicitSATArtDiss.f90
! 
! - implicit LHS for spectral radius-based artificial dissipation
!
! Revision history
! - 10 Apr 2010 : DJB : initial code
! 
! flags == FALSE    : compute inviscid fluxes only
! flags == TRUE     : compute dissipative fluxes only
! flags == COMBINED : inviscid + dissipative fluxes
!
! $Header: /cvsroot/genx/Codes/RocfloCM/Source/ModNavierStokesImplicitSATArtDiss.f90,v 1.2 2010/08/06 15:58:10 mtcampbe Exp $
!
!-----------------------------------------------------------------------
MODULE ModNavierStokesImplicitSATArtDiss

CONTAINS

  Subroutine NSImplicit2DSATArtDiss(region, ng, lhs_var, values_r, values_ru, values_rv, values_rE, stencil_size, dtau)

    USE ModGlobal
    USE ModDataStruct
    USE ModEOS
    USE ModDeriv
    USE ModIO
    USE ModMPI
    USE ModNavierStokesRHS

    Implicit None

    ! ... global variables
    type(t_region), pointer :: region
    type(t_mixt_input), pointer :: input
    type(t_grid), pointer :: grid
    type(t_mixt), pointer :: state
    real(rfreal), pointer :: values_r(:), values_ru(:), values_rv(:), values_rE(:)
    real(rfreal) :: dtau
    integer :: lhs_var

    ! ... local variables
    integer :: ng, k, j, i, ndim, npart, my_part, local_ind, l0, l1, order, dir, opID, NC, ND
    integer :: is(MAX_ND), ie(MAX_ND), varTypes(MAX_ND+2)
    integer :: icomm, igrid, min_stencil_start, max_stencil_end, stencil_size, rhs_var, lhs_var_copy
    integer :: ii0, jj0, kk0, ii1, jj1, kk1, N1, N2, N3, index, N1g, N2g, N3g, ii, jj, kk, perOffset
    real(rfreal) :: RHO_P, RHO_T, gam, gamm1i, UX, RHOE_P, RHOE_T, RHOE_UX, UY, RHO, RHOE_U, RHOE_V, KE, P, T, RHOE, local_val, jac
    real(rfreal) :: XI_X, XI_Y, PC(4), MI2, MMIN2, MU2, SNDSPD2, SNDSPD, MP2, EPS_P, RHO_PP, H0, H_T, H_P, LU, time_fac, MU, KAPPA
    real(rfreal), pointer :: SpectralRadius(:,:)

    ! ... simplicity
    input => region%input
    grid  => region%grid(ng)
    state => region%state(ng)
    Nc = grid%nCells
    ND = grid%ND

    ! ... compute the spectral radius
    allocate(SpectralRadius(Nc,ND))
    Call NS_Spectral_Radius(region, ng, SpectralRadius, 2)
    Do j = 1, ND
      Do i = 1, Nc
        SpectralRadius(i,j) = SpectralRadius(i,j) * input%Amount_Implicit_SAT_Art_Diss * grid%INVJAC(i) * dtau
      End Do
    End Do

    ! ... x-direction, first derivatives
    dir   = 1
    order = 2
    opID  = input%iSecondDerivImplicitArtDiss

    ! ... local partition size
    N1 = grid%ie(1)-grid%is(1)+1
    N2 = grid%ie(2)-grid%is(2)+1
    N3 = grid%ie(3)-grid%is(3)+1

    ! ... partition size including ghost
    N1g = grid%ie(1)-grid%is(1)+1+SUM(grid%nGhostRHS(dir,:))
    N2g = grid%ie(2)-grid%is(2)+1
    N3g = grid%ie(3)-grid%is(3)+1

    SELECT CASE (lhs_var)
    CASE (1)

      ! ... inviscid fluxes
      do k = 1, grid%vdsGhost(dir)
        do j = 1, grid%B(opID,dir)%n
          do i = grid%B(opID,dir)%row_ptr(k,j), grid%B(opID,dir)%row_ptr(k,j+1)-1

            ! ... l0 and l1 indices refer to BUFFERED data
            local_val = grid%B(opID,dir)%val(i,k)
            local_ind = grid%B(opID,dir)%col_ind(i,k)
            l0        = grid%vec_indGhost(k,local_ind,dir)
            l1        = grid%vec_indGhost(k,        j,dir)

            ! ... convert from linear to triplet indices
            ! ... rhs index (buffered data)
            ii0 = mod(l0-1,N1g)+1
            jj0 = mod((l0-ii0)/N1g,N2g)+1
            kk0 = (l0-ii0-(jj0-1)*N1g)/(N1g*N2g)+1

            ! ... lhs index (buffered data)
            ii1 = mod(l1-1,N1g)+1
            jj1 = mod((l1-ii1)/N1g,N2g)+1
            kk1 = (l1-ii1-(jj1-1)*N1g)/(N1g*N2g)+1

            ! ... convert to global indices
            ii0 = ii0 + grid%is(1) - 1 - grid%nGhostRHS(dir,1)
            jj0 = jj0 + grid%is(2) - 1
            kk0 = kk0 + grid%is(3) - 1
            ii1 = ii1 + grid%is(1) - 1 - grid%nGhostRHS(dir,1)
            jj1 = jj1 + grid%is(2) - 1
            kk1 = kk1 + grid%is(3) - 1

            if (ii1 < grid%is(1) .OR. ii1 > grid%ie(1)) CYCLE

            ! ... convert l1 back to non-buffered counting
            l1 = (kk1-grid%is(3))*N1*N2 + (jj1-grid%is(2))*N1 + (ii1-grid%is(1)+1)

            ! ... index
            index = (l1-1)*stencil_size + input%HYPREStencilIndex(opID, ii0-ii1, jj0-jj1, kk0-kk1) + 1

            ! ... store in array
            values_r(index) = values_r(index) + local_val * SpectralRadius(l1,dir)

          end do
        end do
      end do

    CASE (2)

      do k = 1, grid%vdsGhost(dir)
        do j = 1, grid%B(opID,dir)%n
          do i = grid%B(opID,dir)%row_ptr(k,j), grid%B(opID,dir)%row_ptr(k,j+1)-1

            ! ... l0 and l1 indices refer to BUFFERED data
            local_val = grid%B(opID,dir)%val(i,k)
            local_ind = grid%B(opID,dir)%col_ind(i,k)
            l0        = grid%vec_indGhost(k,local_ind,dir)
            l1        = grid%vec_indGhost(k,        j,dir)

            ! ... convert from linear to triplet indices
            ! ... rhs index (buffered data)
            ii0 = mod(l0-1,N1g)+1
            jj0 = mod((l0-ii0)/N1g,N2g)+1
            kk0 = (l0-ii0-(jj0-1)*N1g)/(N1g*N2g)+1

            ! ... lhs index (buffered data)
            ii1 = mod(l1-1,N1g)+1
            jj1 = mod((l1-ii1)/N1g,N2g)+1
            kk1 = (l1-ii1-(jj1-1)*N1g)/(N1g*N2g)+1

            ! ... convert to global indices
            ii0 = ii0 + grid%is(1) - 1 - grid%nGhostRHS(dir,1)
            jj0 = jj0 + grid%is(2) - 1
            kk0 = kk0 + grid%is(3) - 1
            ii1 = ii1 + grid%is(1) - 1 - grid%nGhostRHS(dir,1)
            jj1 = jj1 + grid%is(2) - 1
            kk1 = kk1 + grid%is(3) - 1

            if (ii1 < grid%is(1) .OR. ii1 > grid%ie(1)) CYCLE

            ! ... convert l1 back to non-buffered counting
            l1 = (kk1-grid%is(3))*N1*N2 + (jj1-grid%is(2))*N1 + (ii1-grid%is(1)+1)

            ! ... index
            index = (l1-1)*stencil_size + input%HYPREStencilIndex(opID, ii0-ii1, jj0-jj1, kk0-kk1) + 1

            ! ... store in array
            values_ru(index) = values_ru(index) + local_val * SpectralRadius(l1,dir)

          end do
        end do
      end do

    CASE (3)

      do k = 1, grid%vdsGhost(dir)
        do j = 1, grid%B(opID,dir)%n
          do i = grid%B(opID,dir)%row_ptr(k,j), grid%B(opID,dir)%row_ptr(k,j+1)-1

            ! ... l0 and l1 indices refer to BUFFERED data
            local_val = grid%B(opID,dir)%val(i,k)
            local_ind = grid%B(opID,dir)%col_ind(i,k)
            l0        = grid%vec_indGhost(k,local_ind,dir)
            l1        = grid%vec_indGhost(k,        j,dir)

            ! ... convert from linear to triplet indices
            ! ... rhs index (buffered data)
            ii0 = mod(l0-1,N1g)+1
            jj0 = mod((l0-ii0)/N1g,N2g)+1
            kk0 = (l0-ii0-(jj0-1)*N1g)/(N1g*N2g)+1

            ! ... lhs index (buffered data)
            ii1 = mod(l1-1,N1g)+1
            jj1 = mod((l1-ii1)/N1g,N2g)+1
            kk1 = (l1-ii1-(jj1-1)*N1g)/(N1g*N2g)+1

            ! ... convert to global indices
            ii0 = ii0 + grid%is(1) - 1 - grid%nGhostRHS(dir,1)
            jj0 = jj0 + grid%is(2) - 1
            kk0 = kk0 + grid%is(3) - 1
            ii1 = ii1 + grid%is(1) - 1 - grid%nGhostRHS(dir,1)
            jj1 = jj1 + grid%is(2) - 1
            kk1 = kk1 + grid%is(3) - 1

            if (ii1 < grid%is(1) .OR. ii1 > grid%ie(1)) CYCLE

            ! ... convert l1 back to non-buffered counting
            l1 = (kk1-grid%is(3))*N1*N2 + (jj1-grid%is(2))*N1 + (ii1-grid%is(1)+1)

            ! ... index
            index = (l1-1)*stencil_size + input%HYPREStencilIndex(opID, ii0-ii1, jj0-jj1, kk0-kk1) + 1

            ! ... store in array
            values_rv(index) = values_rv(index) + local_val * SpectralRadius(l1,dir)

          end do
        end do
      end do

    CASE (4)

      do k = 1, grid%vdsGhost(dir)
        do j = 1, grid%B(opID,dir)%n
          do i = grid%B(opID,dir)%row_ptr(k,j), grid%B(opID,dir)%row_ptr(k,j+1)-1

            ! ... l0 and l1 indices refer to BUFFERED data
            local_val = grid%B(opID,dir)%val(i,k)
            local_ind = grid%B(opID,dir)%col_ind(i,k)
            l0        = grid%vec_indGhost(k,local_ind,dir)
            l1        = grid%vec_indGhost(k,        j,dir)

            ! ... convert from linear to triplet indices
            ! ... rhs index (buffered data)
            ii0 = mod(l0-1,N1g)+1
            jj0 = mod((l0-ii0)/N1g,N2g)+1
            kk0 = (l0-ii0-(jj0-1)*N1g)/(N1g*N2g)+1

            ! ... lhs index (buffered data)
            ii1 = mod(l1-1,N1g)+1
            jj1 = mod((l1-ii1)/N1g,N2g)+1
            kk1 = (l1-ii1-(jj1-1)*N1g)/(N1g*N2g)+1

            ! ... convert to global indices
            ii0 = ii0 + grid%is(1) - 1 - grid%nGhostRHS(dir,1)
            jj0 = jj0 + grid%is(2) - 1
            kk0 = kk0 + grid%is(3) - 1
            ii1 = ii1 + grid%is(1) - 1 - grid%nGhostRHS(dir,1)
            jj1 = jj1 + grid%is(2) - 1
            kk1 = kk1 + grid%is(3) - 1

            if (ii1 < grid%is(1) .OR. ii1 > grid%ie(1)) CYCLE

            ! ... convert l1 back to non-buffered counting
            l1 = (kk1-grid%is(3))*N1*N2 + (jj1-grid%is(2))*N1 + (ii1-grid%is(1)+1)

            ! ... index
            index = (l1-1)*stencil_size + input%HYPREStencilIndex(opID, ii0-ii1, jj0-jj1, kk0-kk1) + 1

            ! ... store in array
            values_rE(index) = values_rE(index) + local_val * SpectralRadius(l1,dir)

          end do
        end do
      end do

    END SELECT

    ! ... y-direction, first derivatives
    dir   = 2
    order = 2
    opID  = input%iSecondDerivImplicitArtDiss

    ! ... local partition size
    N1 = grid%ie(1)-grid%is(1)+1
    N2 = grid%ie(2)-grid%is(2)+1
    N3 = grid%ie(3)-grid%is(3)+1

    ! ... partition size including ghost
    N1g = grid%ie(1)-grid%is(1)+1
    N2g = grid%ie(2)-grid%is(2)+1+SUM(grid%nGhostRHS(dir,:))
    N3g = grid%ie(3)-grid%is(3)+1

    SELECT CASE (lhs_var)
    CASE (1)

      ! ... inviscid fluxes
      do k = 1, grid%vdsGhost(dir)
        do j = 1, grid%B(opID,dir)%n
          do i = grid%B(opID,dir)%row_ptr(k,j), grid%B(opID,dir)%row_ptr(k,j+1)-1

            ! ... l0 and l1 indices refer to BUFFERED data
            local_val = grid%B(opID,dir)%val(i,k)
            local_ind = grid%B(opID,dir)%col_ind(i,k)
            l0        = grid%vec_indGhost(k,local_ind,dir)
            l1        = grid%vec_indGhost(k,        j,dir)

            ! ... convert from linear to triplet indices
            ! ... rhs index (buffered data)
            ii0 = mod(l0-1,N1g)+1
            jj0 = mod((l0-ii0)/N1g,N2g)+1
            kk0 = (l0-ii0-(jj0-1)*N1g)/(N1g*N2g)+1

            ! ... lhs index (buffered data)
            ii1 = mod(l1-1,N1g)+1
            jj1 = mod((l1-ii1)/N1g,N2g)+1
            kk1 = (l1-ii1-(jj1-1)*N1g)/(N1g*N2g)+1

            ! ... convert to global indices
            ii0 = ii0 + grid%is(1) - 1
            jj0 = jj0 + grid%is(2) - 1 - grid%nGhostRHS(dir,1)
            kk0 = kk0 + grid%is(3) - 1
            ii1 = ii1 + grid%is(1) - 1
            jj1 = jj1 + grid%is(2) - 1 - grid%nGhostRHS(dir,1)
            kk1 = kk1 + grid%is(3) - 1

            if (jj1 < grid%is(2) .OR. jj1 > grid%ie(2)) CYCLE

            ! ... convert l1 back to non-buffered counting
            l1 = (kk1-grid%is(3))*N1*N2 + (jj1-grid%is(2))*N1 + (ii1-grid%is(1)+1)

            ! ... index
            index = (l1-1)*stencil_size + input%HYPREStencilIndex(opID, ii0-ii1, jj0-jj1, kk0-kk1) + 1

            ! ... store in array
            values_r(index) = values_r(index) + local_val * SpectralRadius(l1,dir)

          end do
        end do
      end do

    CASE (2)

      do k = 1, grid%vdsGhost(dir)
        do j = 1, grid%B(opID,dir)%n
          do i = grid%B(opID,dir)%row_ptr(k,j), grid%B(opID,dir)%row_ptr(k,j+1)-1

            ! ... l0 and l1 indices refer to BUFFERED data
            local_val = grid%B(opID,dir)%val(i,k)
            local_ind = grid%B(opID,dir)%col_ind(i,k)
            l0        = grid%vec_indGhost(k,local_ind,dir)
            l1        = grid%vec_indGhost(k,        j,dir)

            ! ... convert from linear to triplet indices
            ! ... rhs index (buffered data)
            ii0 = mod(l0-1,N1g)+1
            jj0 = mod((l0-ii0)/N1g,N2g)+1
            kk0 = (l0-ii0-(jj0-1)*N1g)/(N1g*N2g)+1

            ! ... lhs index (buffered data)
            ii1 = mod(l1-1,N1g)+1
            jj1 = mod((l1-ii1)/N1g,N2g)+1
            kk1 = (l1-ii1-(jj1-1)*N1g)/(N1g*N2g)+1

            ! ... convert to global indices
            ii0 = ii0 + grid%is(1) - 1
            jj0 = jj0 + grid%is(2) - 1 - grid%nGhostRHS(dir,1)
            kk0 = kk0 + grid%is(3) - 1
            ii1 = ii1 + grid%is(1) - 1
            jj1 = jj1 + grid%is(2) - 1 - grid%nGhostRHS(dir,1)
            kk1 = kk1 + grid%is(3) - 1

            if (jj1 < grid%is(2) .OR. jj1 > grid%ie(2)) CYCLE

            ! ... convert l1 back to non-buffered counting
            l1 = (kk1-grid%is(3))*N1*N2 + (jj1-grid%is(2))*N1 + (ii1-grid%is(1)+1)

            ! ... index
            index = (l1-1)*stencil_size + input%HYPREStencilIndex(opID, ii0-ii1, jj0-jj1, kk0-kk1) + 1

            ! ... store in array
            values_ru(index) = values_ru(index) + local_val * SpectralRadius(l1,dir)

          end do
        end do
      end do

    CASE (3)

      do k = 1, grid%vdsGhost(dir)
        do j = 1, grid%B(opID,dir)%n
          do i = grid%B(opID,dir)%row_ptr(k,j), grid%B(opID,dir)%row_ptr(k,j+1)-1

            ! ... l0 and l1 indices refer to BUFFERED data
            local_val = grid%B(opID,dir)%val(i,k)
            local_ind = grid%B(opID,dir)%col_ind(i,k)
            l0        = grid%vec_indGhost(k,local_ind,dir)
            l1        = grid%vec_indGhost(k,        j,dir)

            ! ... convert from linear to triplet indices
            ! ... rhs index (buffered data)
            ii0 = mod(l0-1,N1g)+1
            jj0 = mod((l0-ii0)/N1g,N2g)+1
            kk0 = (l0-ii0-(jj0-1)*N1g)/(N1g*N2g)+1

            ! ... lhs index (buffered data)
            ii1 = mod(l1-1,N1g)+1
            jj1 = mod((l1-ii1)/N1g,N2g)+1
            kk1 = (l1-ii1-(jj1-1)*N1g)/(N1g*N2g)+1

            ! ... convert to global indices
            ii0 = ii0 + grid%is(1) - 1
            jj0 = jj0 + grid%is(2) - 1 - grid%nGhostRHS(dir,1)
            kk0 = kk0 + grid%is(3) - 1
            ii1 = ii1 + grid%is(1) - 1
            jj1 = jj1 + grid%is(2) - 1 - grid%nGhostRHS(dir,1)
            kk1 = kk1 + grid%is(3) - 1

            if (jj1 < grid%is(2) .OR. jj1 > grid%ie(2)) CYCLE

            ! ... convert l1 back to non-buffered counting
            l1 = (kk1-grid%is(3))*N1*N2 + (jj1-grid%is(2))*N1 + (ii1-grid%is(1)+1)

            ! ... index
            index = (l1-1)*stencil_size + input%HYPREStencilIndex(opID, ii0-ii1, jj0-jj1, kk0-kk1) + 1

            ! ... store in array
            values_rv(index) = values_rv(index) + local_val * SpectralRadius(l1,dir)

          end do
        end do
      end do

    CASE (4)

      do k = 1, grid%vdsGhost(dir)
        do j = 1, grid%B(opID,dir)%n
          do i = grid%B(opID,dir)%row_ptr(k,j), grid%B(opID,dir)%row_ptr(k,j+1)-1

            ! ... l0 and l1 indices refer to BUFFERED data
            local_val = grid%B(opID,dir)%val(i,k)
            local_ind = grid%B(opID,dir)%col_ind(i,k)
            l0        = grid%vec_indGhost(k,local_ind,dir)
            l1        = grid%vec_indGhost(k,        j,dir)

            ! ... convert from linear to triplet indices
            ! ... rhs index (buffered data)
            ii0 = mod(l0-1,N1g)+1
            jj0 = mod((l0-ii0)/N1g,N2g)+1
            kk0 = (l0-ii0-(jj0-1)*N1g)/(N1g*N2g)+1

            ! ... lhs index (buffered data)
            ii1 = mod(l1-1,N1g)+1
            jj1 = mod((l1-ii1)/N1g,N2g)+1
            kk1 = (l1-ii1-(jj1-1)*N1g)/(N1g*N2g)+1

            ! ... convert to global indices
            ii0 = ii0 + grid%is(1) - 1
            jj0 = jj0 + grid%is(2) - 1 - grid%nGhostRHS(dir,1)
            kk0 = kk0 + grid%is(3) - 1
            ii1 = ii1 + grid%is(1) - 1
            jj1 = jj1 + grid%is(2) - 1 - grid%nGhostRHS(dir,1)
            kk1 = kk1 + grid%is(3) - 1

            if (jj1 < grid%is(2) .OR. jj1 > grid%ie(2)) CYCLE

            ! ... convert l1 back to non-buffered counting
            l1 = (kk1-grid%is(3))*N1*N2 + (jj1-grid%is(2))*N1 + (ii1-grid%is(1)+1)

            ! ... index
            index = (l1-1)*stencil_size + input%HYPREStencilIndex(opID, ii0-ii1, jj0-jj1, kk0-kk1) + 1

            ! ... store in array
            values_rE(index) = values_rE(index) + local_val * SpectralRadius(l1,dir)

          end do
        end do
      end do

    END SELECT

    deallocate(SpectralRadius)

  End Subroutine NSImplicit2DSATArtDiss

End MODULE ModNavierStokesImplicitSATArtDiss

  
