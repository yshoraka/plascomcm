! Copyright (c) 2015, University of Illinois at Urbana-Champaign, XPACC
! License: MIT, http://opensource.org/licenses/MIT
!-----------------------------------------------------------------------
!
! ModPlasComCM
!
! - basic code for the control of the application
!
! Revision history
! - 20 Dec 2006 : DJB : initial code
! - 13 Jan 2007 : DJB : evolution (added region)
! - 01 May 2007 : DJB : evolution (added restart)
!
!-----------------------------------------------------------------------
Module ModPlasComCM

  USE ModGlobal
  USE ModDataStruct
  USE ModInput
  USE ModMPI
  USE ModTimemarch
  USE ModPostProcess
  USE ModIO
  USE ModRegion
  USE ModInterp
  USE ModOptimization
  USE ModNASARotor
  USE ModDerivBuildOps
  USE ModSBI
#ifdef BUILD_TMSOLVER
  USE ModInitTM
  USE ModFTMCoupling
#endif
  USE ModStat
  USE ModGeometry

  implicit none

Contains

  subroutine PlasComCM_Init_MPI(region, comm)

    type(t_region), pointer, intent(in) :: region
    integer, intent(in) :: comm

    integer :: ierr

    mycomm = comm
    Call MPI_COMM_SIZE(mycomm, numproc, ierr)
    Call MPI_COMM_RANK(mycomm, region%myrank, ierr)


    CALL MPI_BARRIER(mycomm, ierr)
!    IF((debug_on .eqv. .true.)) THEN
!       Write (*,'(A,I7,A,I7,A)') 'PlasComCM: Processor ', region%myrank, ' of ', numproc, ' reporting for duty.'
!    ELSE IF (region%myrank == 0) THEN
    IF(region%myrank == 0 .and. (debug_on .eqv. .true.)) THEN
       write(*,'(A,I7,A)') 'PlasComCM: All ', numproc, ' processors ready.'       
    ENDIF

  end subroutine PlasComCM_Init_MPI

  subroutine PlasComCM_Init_Timings(region)

    type(t_region), pointer, intent(inout) :: region

    region%total_time = MPI_WTIME()

  end subroutine PlasComCM_Init_Timings

  subroutine PlasComCM_Read_Cmd_Line(region, input_fname, restart_fname, restart_aux_fname)

    type(t_region), pointer, intent(inout) :: region
    character(len=PATH_LENGTH), intent(out) :: input_fname
    character(len=PATH_LENGTH), intent(out) :: restart_fname
    character(len=PATH_LENGTH), intent(out) :: restart_aux_fname

    integer :: i, j, k, l, m
    character(len=CMD_ARG_LENGTH), dimension(:), allocatable :: raw_arguments
    character(len=CMD_ARG_LENGTH) :: raw_argument, next_raw_argument
    logical :: skip_next
    character(len=CMD_ARG_LENGTH) :: option_name
    character(len=CMD_ARG_LENGTH) :: option_value
    integer :: option_value_type
    character(len=PATH_LENGTH) :: progname
    integer :: backslash

    integer, parameter :: OPT_VALUE_TYPE_NONE = 1
    integer, parameter :: OPT_VALUE_TYPE_STRING = 2

    input_fname = ''
    restart_fname = ''
    restart_aux_fname = ''

#ifdef USE_AMPI
    allocate(raw_arguments(mpi_command_argument_count()))
#else
    allocate(raw_arguments(command_argument_count()))
#endif

    do i = 1, size(raw_arguments)
#ifdef USE_AMPI
      call mpi_get_command_argument(i, raw_arguments(i), CMD_ARG_LENGTH, ierr)
#else
      call get_command_argument(i, raw_arguments(i))
#endif
    end do

    i = 1
    m = 1
    do while (i <= size(raw_arguments))

      raw_argument = raw_arguments(i)
      if (i < size(raw_arguments)) then
        next_raw_argument = raw_arguments(i+1)
      else
        next_raw_argument = ''
      end if

      skip_next = .false.

      if (raw_argument(1:2) == '--') then

        j = index(raw_argument, '=')
        j = merge(j, len_trim(raw_argument)+1, j /= 0)
        option_name = raw_argument(3:j-1)
        option_value = raw_argument(j+1:)
        select case(option_name)
        case ('input')
          option_value_type = OPT_VALUE_TYPE_STRING
        case default
          call graceful_exit(region%myrank, '... ERROR: Unrecognized option "' // &
            trim(option_name) // '".')
        end select
        if (option_value == '' .and. option_value_type /= OPT_VALUE_TYPE_NONE) then
          if (next_raw_argument /= '') then
            option_value = next_raw_argument
            skip_next = .true.
          else
            call graceful_exit(region%myrank, '... ERROR: Missing value for option "' // &
              trim(option_name) // '".')
          end if
        end if
        select case (option_name)
        case ('input')
          input_fname = option_value
        end select

      else if (raw_argument(1:1) == "-") then

        do j = 2, len_trim(raw_argument)
          option_name = raw_argument(j:j)
          select case (option_name)
          case ('i')
            option_value_type = OPT_VALUE_TYPE_STRING
          case default
            call graceful_exit(region%myrank, '... ERROR: Unrecognized option "' // &
              trim(option_name) // '".')
          end select
          if (option_value_type == OPT_VALUE_TYPE_NONE) then
            option_value = ''
          else 
            option_value = raw_argument(j+1:)
            if (option_value == '') then
              if (next_raw_argument /= '') then
                option_value = next_raw_argument
                skip_next = .true.
              else
                call graceful_exit(region%myrank, '... ERROR: Missing value for option "' // &
                  trim(option_name) // '".')
              end if
            end if
            select case (option_name)
            case ('i')
              input_fname = option_value
            end select
            exit
          end if
        end do

      else

        if (m == 1) then
          restart_fname = raw_argument 
        else if (m == 2) then
          restart_aux_fname = raw_argument
        else
          call graceful_exit(region%myrank, '... ERROR: Too many arguments.')
        end if
        m = m + 1

      end if

      i = merge(i+2, i+1, skip_next)

    end do

    ! the default input file is the executable name + '.inp'
    if (input_fname == '') then
#ifdef USE_AMPI
      call mpi_get_command_argument(0, progname, CMD_ARG_LENGTH, ierr)
#else
      call get_command_argument(0, progname)
#endif
      backslash = INDEX(trim(progname),'/',back=.true.)
      if (backslash == 0) then
        input_fname = trim(progname)//'.inp'
      else
        input_fname = trim(progname(backslash+1:))//'.inp'
      end if
    end if

  end subroutine PlasComCM_Read_Cmd_Line

  subroutine PlasComCM_Init_Input(region, input_fname, restart_fname, restart_aux_fname)

    IMPLICIT NONE
    
    type(t_region), pointer, intent(inout) :: region
    character(len=PATH_LENGTH), intent(in) :: input_fname
    character(len=PATH_LENGTH), intent(in) :: restart_fname
    character(len=PATH_LENGTH), intent(in) :: restart_aux_fname

    logical :: file_exists
    type(t_grid), pointer :: grid
    type(t_mixt_input), pointer :: input
    integer :: ng, ierr

    allocate(region%global)
    if (restart_fname /= '') then
      region%global%USE_RESTART_FILE = TRUE
      region%global%restart_fname(1) = restart_fname
      if (restart_aux_fname /= '') then
        region%global%restart_fname(2) = restart_aux_fname
      else
        region%global%restart_fname(2) = 'NONE'
      end if
    else
      region%global%USE_RESTART_FILE = FALSE
      region%global%restart_fname = ''
    end if

    ! Check if input and restart files exist
    inquire(file=input_fname, exist=file_exists)
    if (.not. file_exists) then
      call graceful_exit(region%myrank, '... ERROR: Input file "' // trim(input_fname) // &
        '" does not exist.')
    end if
    if (restart_fname /= '') then
      inquire(file=restart_fname, exist=file_exists)
      if (.not. file_exists) then
        call graceful_exit(region%myrank, '... ERROR: Restart file "' // trim(restart_fname) // &
          '" does not exist.')
      end if
      if (restart_aux_fname /= '') then
        inquire(file=restart_aux_fname, exist=file_exists)
        if (.not. file_exists) then
          call graceful_exit(region%myrank, '... ERROR: Restart file "' // &
            trim(restart_aux_fname) // '" does not exist.')
        end if
      end if
    end if

  ! ... read input
    allocate(region%input); region%input%nOverlap = 0;
    IF((debug_on .eqv. .true.) .and. (region%myrank == 0)) write(*,'(A)') 'PlasComCM: Reading input.'
    Call read_input(region, region%input, input_fname, .true.)
    call mpi_barrier(mycomm, ierr)
    IF((debug_on .eqv. .true.) .and. (region%myrank == 0)) write(*,'(A)') 'PlasComCM: Reading input done.'

  ! ... initialize global data
    IF((debug_on .eqv. .true.) .and. (region%myrank == 0)) write(*,'(A)') 'PlasComCM: Initializing global data.'
    Call initialize_global_data(region, region%input)
    call mpi_barrier(mycomm, ierr)
    IF((debug_on .eqv. .true.) .and. (region%myrank == 0)) write(*,'(A)') 'PlasComCM: Initializing global data done.'

  ! ... initialize grid generation
    if (region%input%generateGrid == TRUE) then
      IF((debug_on .eqv. .true.) .and. (region%myrank == 0)) write(*,'(A)') 'PlasComCM: Initializing grid generation.'
      Call GeometryInit(region)
      call mpi_barrier(mycomm, ierr)
      IF((debug_on .eqv. .true.) .and. (region%myrank == 0)) write(*,'(A)') 'PlasComCM: Initializing grid generation done.'
    end if

  ! ... decompose the domain
    IF((debug_on .eqv. .true.) .and. (region%myrank == 0)) write(*,'(A)') 'PlasComCM: Decomposing domain.'
    Call DecomposeDomain(region)
    Call MPI_Barrier(mycomm, ierr)
    IF((debug_on .eqv. .true.) .and. (region%myrank == 0)) write(*,'(A)') 'PlasComCM: Decomposing domain done.'

  ! ... initial fill of grid%input
    IF((debug_on .eqv. .true.) .and. (region%myrank == 0)) write(*,'(A)') 'PlasComCM: Reading grid-wise input.'
    allocate(region%grid(region%nGrids))
    allocate(region%state(region%nGrids))
    allocate(region%electric(region%nGrids))
    Do ng = 1, region%nGrids
      grid  => region%grid(ng)
      allocate(grid%input)
      input => grid%input
      Call read_input(region, input, input_fname, .false.)
    End Do
    call mpi_barrier(mycomm, ierr)
    IF((debug_on .eqv. .true.) .and. (region%myrank == 0)) write(*,'(A)') 'PlasComCM: Reading grid-wise input done.'

  ! ... set up the region
    IF((debug_on .eqv. .true.) .and. (region%myrank == 0)) write(*,'(A)') 'PlasComCM: Initializing region.'
    Call initialize_region(region)
    call mpi_barrier(mycomm, ierr)
    IF((debug_on .eqv. .true.) .and. (region%myrank == 0)) write(*,'(A)') 'PlasComCM: Initializing region done.'


  end subroutine PlasComCM_Init_Input

  subroutine PlasComCM_Init_Grids(region)

    USE ModMPI

    type(t_region), pointer, intent(inout) :: region

    type(t_grid), pointer :: grid
    type(t_mixt_input), pointer :: input
    integer :: ng, i, dir, ierr
    integer :: my_max_overlap, all_max_overlap
    integer, pointer :: comm_max_overlap(:)

  !! ... send initial grid (also sets vectorization tables)
  !  IF((debug_on .eqv. .true.) .and. (myrank == 0)) write(*,'(A)') 'PlasComCM: Send initial grid, set vectorization tables.'
  !  Do i = 1, region%nGridsGlobal
  !
  !! ... only read if grid%iGridGlobal == i
  !    Call CompleteVectorization(region, i)
  !
  !! ... have everyone wait to save on memory
  !    Call MPI_BARRIER(mycomm, IERR)
  !
  !  End Do

  ! ... send initial grid
    Do i = 1, region%nGrids

  ! ... only read if grid%iGridGlobal == i
!      Call Read_And_Distribute_Single_Grid(region, i)
       CALL ReadSingleGrid(region,i)
!       CALL SetSpongeInfo(region,i)
  ! ... have everyone wait to save on memory
       CALL MPI_BARRIER(region%grid(i)%comm, IERR)
       
    End Do

    CALL InitialHaloExchange(region)

    CALL MPI_BARRIER(mycomm,IERR)
    
    IF((debug_on .eqv. .true.) .and. (region%myrank == 0)) write(*,'(A)') 'PlasComCM: Send initial grid, set vectorization tables done.'
   
    my_max_overlap = 0
  ! ... collect local edge information for each processor
    Do ng = 1, region%nGrids
      grid  => region%grid(ng)
      input => grid%input

      grid%max_overlap_b(:) = 0

      Do dir = 1, grid%ND
        Call Collect_Local_Edge_Information_Box(region, ng, input, dir, grid, grid%periodic(dir))
      End Do
     
      if (my_max_overlap < maxval(grid%max_overlap_b(:))) then
        my_max_overlap = maxval(grid%max_overlap_b(:))
      endif

      if (my_max_overlap < input%nOverlap) then
        my_max_overlap = input%nOverlap
      endif

    End do

    call MPI_Barrier(mycomm, ierr)
    if (region%myrank == 0) then
      Write (*,'(A)') 'PlasComCM: Done collecting local edge information.'
    end if

    allocate(comm_max_overlap(numproc))
    Call MPI_AllGATHER(my_max_overlap, 1, MPI_INTEGER, comm_max_overlap, 1, MPI_INTEGER, mycomm, ierr)
    all_max_overlap = maxval(comm_max_overlap(:))
    deallocate(comm_max_overlap)

  ! ... if the ghost points are insufficient, stop and ask the user to increase
    IF(all_max_overlap > input%nOverlap ) THEN
      write(*,'(A,i2)') 'The default number of ghost points ', input%nOverlap, ' is insufficient, please increase to ', all_max_overlap 
      call graceful_exit(region%myrank, 'PlasComCM: Insufficient ghost points.')
    END IF
    
  !! ... update the vectorization table if in the last step we find that more ghost points are needed
  !  IF((debug_on .eqv. .true.) .and. (myrank == 0)) write(*,'(A)') 'PlasComCM: Updating the vectorization table...'
  !  Do i = 1, region%nGridsGlobal
  !
  !! ... only read if grid%iGridGlobal == i
  !    Call UpdateVectorization(region, i, all_max_overlap)
  !
  !! ... have everyone wait to save on memory
  !    Call MPI_BARRIER(mycomm, IERR)
  !
  !  End Do
  !  IF((debug_on .eqv. .true.) .and. (myrank == 0)) write(*,'(A)') 'PlasComCM: Updating the vectorization table, done.'
    if ( region%myrank == 0 ) then
      write (*,'(A)') ''
      write (*,'(A)') 'PlasComCM: ==> Beginning CHIMERA section <=='
    end if

    input => region%input

    ! ... set up the interpolation data once for static grids
    if (input%TMOnly /= TRUE) then
      if (input%gridType == CHIMERA .and. input%volumeIntersection == TRUE) then
        call Setup_Interpolation(region)
      end if
    end if

    ! ... New handler for SAT_BLOCK_INTERFACE boundaries
    call init_sat_descriptor(region%sat_descriptor)
    if(input%useNativeSBI == TRUE .and. input%boundaryIntersection == TRUE) then
       CALL SAT_BLOCK_INTERFACE_SETUP(region)
    endif

    if(input%moveGrid == TRUE) then
       IF((debug_on .eqv. .true.) .and. (region%myrank == 0)) write(*,'(A)') 'PlasComCM: Setting up transfinite interpolation ... '
       Do i = 1, region%nGridsGlobal
          
  ! ... only read if grid%iGridGlobal == i
          Call SetUpTransfiniteInterp(region, i)
          
  ! ... have everyone wait to save on memory
          Call MPI_BARRIER(mycomm, IERR)

       End Do
       IF((debug_on .eqv. .true.) .and. (region%myrank == 0)) write(*,'(A)') 'PlasComCM: Setting up transfinite interpolation done. '
    end if

    if ( region%myrank == 0 ) then
      write (*,'(A)') 'PlasComCM: ==> Done with CHIMERA section <=='
      write (*,'(A)') ''
    end if

  end subroutine PlasComCM_Init_Grids

  subroutine PlasComCM_Init_State(region)

    type(t_region), pointer, intent(inout) :: region

    integer :: ierr

  ! ... read the state data
    IF((debug_on .eqv. .true.) .and. (region%myrank == 0)) write(*,'(A)') 'PlasComCM: Initializing state.'
    call InitializeState(region, region%global%restart_fname)
    CALL MPI_BARRIER(mycomm,ierr)
    IF((debug_on .eqv. .true.) .and. (region%myrank == 0)) write(*,'(A)') 'PlasComCM: Initializing state done.'

  end subroutine PlasComCM_Init_State

  subroutine PlasComCM_Init_Adjoint(region)

    type(t_region), pointer, intent(inout) :: region

    type(t_mixt_input), pointer :: input
    integer :: ng, ierr

    input => region%input

  ! ... define control time horizon for adjoint-based optimization
    if (input%AdjOptim) then
       IF((debug_on .eqv. .true.) .and. (region%myrank == 0)) write(*,'(A)') 'PlasComCM: Adj define control horizon.'
       call DefineControlHorizon(region)
       call mpi_barrier(mycomm,ierr)
       IF((debug_on .eqv. .true.) .and. (region%myrank == 0)) write(*,'(A)') 'PlasComCM: Adj define control horizon done.'
    endif
  ! ... for a stand-alone adjoint N-S equation solver, interpolate or specify initial state%cv
    if (input%AdjNS) then
       IF((debug_on .eqv. .true.) .and. (region%myrank == 0)) write(*,'(A)') 'PlasComCM: Adj initialize interpolate CV.'
      call Initialize_AdjInterpolate_cv(region, input%AdjNS_ReadNSSoln)
      call mpi_barrier(mycomm,ierr)
      IF((debug_on .eqv. .true.) .and. (region%myrank == 0)) write(*,'(A)') 'PlasComCM: Adj initialize interpolate CV done.'

      do ng = 1, region%nGrids
        call AdjInterpolate_cv(region, ng)
      end do ! ng
    end if ! input%AdjNS
    call mpi_barrier(mycomm,ierr)
    IF((debug_on .eqv. .true.) .and. (region%myrank == 0)) write(*,'(A)') 'PlasComCM: Adj interpolate CV done.'

  end subroutine PlasComCM_Init_Adjoint

  subroutine PlasComCM_Init_Probe(region)

    type(t_region), pointer, intent(inout) :: region

    integer :: ierr

    IF((debug_on .eqv. .true.) .and. (region%myrank == 0)) write(*,'(A)') 'PlasComCM: Initializing probe data.'
    call initialize_probe_data(region) ! ... JKim 12/2008
    call mpi_barrier(mycomm,ierr)
    IF((debug_on .eqv. .true.) .and. (region%myrank == 0)) write(*,'(A)') 'PlasComCM: Initializing probe data done.'

  end subroutine PlasComCM_Init_Probe

  subroutine PlasComCM_Init_LST(region)

    type(t_region), pointer, intent(inout) :: region

    integer :: ierr

    IF((debug_on .eqv. .true.) .and. (region%myrank == 0)) write(*,'(A)') 'PlasComCM: Read and interpolate LST.'
    call Read_And_Interpolate_LST_Eigenmodes(region) ! ... JKim 12/2009
    call mpi_barrier(mycomm,ierr)
    IF((debug_on .eqv. .true.) .and. (region%myrank == 0)) write(*,'(A)') 'PlasComCM: Read and interpolate LST done.'

  end subroutine PlasComCM_Init_LST

  subroutine PlasComCM_Init_TM(region)

    type(t_region), pointer, intent(inout) :: region

    integer :: ng, nadjust, ierr

  ! ... load solid data here
#ifdef BUILD_TMSOLVER
    If (region%input%TMSolve /= FALSE) then

       region%TM_total_time = MPI_WTIME()
       Call LoadSolidData(region)
       region%TM_total_time = MPI_WTIME() - region%TM_total_time
       call MPI_BARRIER(mycomm, ierr)

        nadjust = 0
  ! ... adjust restart on fluid procs
        Call MPI_AllReduce(region%input%nrestart,nadjust,1,MPI_INTEGER,MPI_MAX,mycomm,ierr)

        if(region%input%nrestart /= nadjust) then
           write(*,'(A,I5,A,I7,A,I5)') 'PlasComCM: Adjusting restart frequency of ',region%input%nrestart,' on fluid-only rank ',region%myrank,' to ',nadjust
           region%input%nrestart = nadjust
           do ng = 1, region%nGrids
             region%grid(ng)%input%nrestart = nadjust
           end do
        end if

        nadjust = 0
  ! ... adjust nstepmax on fluid procs
        Call MPI_AllReduce(region%input%nstepmax,nadjust,1,MPI_INTEGER,MPI_MAX,mycomm,ierr)
        if(region%input%nstepmax /= nadjust) then
          write(*,'(A,I0.1,A,I0.1,A,I0.1)') 'PlasComCM: Adjusting nstepmax of ',region%input%nstepmax,' on fluid-only rank ',region%myrank,' to ',nadjust
          region%input%nstepmax = nadjust
          do ng = 1, region%nGrids
            region%grid(ng)%input%nstepmax = nadjust
          end do
        end if


  ! ... initialize communication between fluid and solid grids
        If(region%input%TMOnly /= TRUE) then
          Call SetUpFTMCoupling(region)
        end if
     end If
#endif
 
    call mpi_barrier(mycomm,ierr)

    call mpi_barrier(mycomm,ierr)
    if(region%myrank == 0) write(*,'(A)') 'PlasComCM: Rocsmash done'

  end subroutine PlasComCM_Init_TM

  subroutine PlasComCM_Init_NASA_Rotor_Grid(region)

    type(t_region), pointer, intent(inout) :: region

    ! ... define theta, radius
    Call NASA_Rotor_Grid_Initialize(region)

  end subroutine PlasComCM_Init_NASA_Rotor_Grid

  subroutine PlasComCM_Init_LES(region)

    type(t_region), pointer, intent(inout) :: region

    type(t_mixt_input), pointer :: input
    integer :: ng, io_err
    integer :: diravg(MAX_ND+1)
    Character(LEN=80) :: strbuf
    Logical :: mf_exists

    input => region%input

    if (input%LES >= 200 .AND. input%LES < 300) then
      mf_exists = .FALSE.
      inquire(file=trim(input%LESAvgDir), exist=mf_exists)
      if (mf_exists) then
        open(66, file=trim(input%LESAvgDir)); io_err = 0
        Do while (io_err == 0)
          Read (66,'(A)',iostat=io_err) strbuf
          If ((io_err == 0) .AND. (strbuf(1:1) /= '#') .AND. (len_trim(strbuf) /= 0)) Then
              Read (strbuf,*) diravg(:)        
              Do ng =1, region%nGrids
                 if (diravg(1) == region%grid(ng)%iGridGlobal) then
                   region%grid(ng)%AvgDIR(:) = diravg(2:region%grid(ng)%ND+1)
                 end if
              end do
          endif
        end do
        close(66)
        Do ng =1, region%nGrids
           if (region%grid(ng)%AvgDIR(1)+region%grid(ng)%AvgDIR(2)+region%grid(ng)%AvgDIR(3)==0) then
               CALL graceful_exit(region%myrank, "... For LES, averaging should be done at least one direction")
           endif
        enddo
      else
        CALL graceful_exit(region%myrank, "... LES averaging direction is missing")
      end if
    endif

  end subroutine PlasComCM_Init_LES

  subroutine PlasComCM_Init_Stat(region)

    type(t_region), pointer, intent(inout) :: region

    ! Initialize on-the-fly stats (2D only for now)
    if (region%input%use_stats==1) Call Stat2D_Init(region)

  end subroutine PlasComCM_Init_Stat

  subroutine PlasComCM_Output_Decomp_Map(region)

    type(t_region), pointer, intent(inout) :: region

    type(t_grid), pointer :: grid
    integer :: ng, i, j, p, ierr
   
  ! ... output running decomposition
    Do i = 1, region%nGridsGlobal
      Do ng = 1, region%nGrids
        grid => region%grid(ng)
        If (grid%iGridGlobal == i) Then
          Do p = 0, grid%numproc_inComm
            If (grid%myrank_inComm == p) Then
              If (p == 0 .and. i == 1) then
                open (unit=10, file='PlasComCM.map', status='unknown')
              else
                open (unit=10, file='PlasComCM.map', status='unknown', position='append')
              End If
              write (10,'(8(I5,1X))') region%myrank, grid%iGridGlobal, (grid%is_unique(j), grid%ie_unique(j), j = 1, MAX_ND)
              close (10)
            End If
            Call MPI_Barrier(grid%comm, ierr)
          End Do
        End If
      End Do
      Call MPI_Barrier(mycomm, ierr)
    End Do

    IF((debug_on .eqv. .true.) .and. (region%myrank == 0)) write(*,'(A)') 'PlasComCM: Map reading done.'
    call mpi_barrier(mycomm,ierr)

  end subroutine PlasComCM_Output_Decomp_Map

  subroutine PlasComCM_Run(region)

    type(t_region), pointer, intent(inout) :: region

    integer :: ierr

    if (region%input%usePP/=1) then
       ! ------------------------------------
       ! ... time march solution
       if (region%input%AdjOptim) then
          IF((debug_on .eqv. .true.) .and. (region%myrank == 0)) write(*,'(A)') 'PlasComCM: Running OptimizeCG.'
          Call optimizeCG(region, region%global%restart_fname(1)) ! ... adjoint-based conjugate gradient optimization
       else
          IF((debug_on .eqv. .true.) .and. (region%myrank == 0)) write(*,'(A)') 'PlasComCM: Time marching.'
          Call timemarch(region) ! ... regular forward or adjoint N-S solver
       end if ! region%input%AdjOptim
       call mpi_barrier(mycomm, ierr)
       if(region%myrank == 0) write(*,'(A)') 'PlasComCM: Simulation done.'

    else
       ! ------------------------------------
       ! ... postprocess PlasComCM solution
       Call PostProcess(region)

    end if ! ... postprocess

  end subroutine PlasComCM_Run

  subroutine PlasComCM_Finalize_Timings(region)

    type(t_region), pointer, intent(inout) :: region

    integer :: ierr

  ! ... Write timings
    region%total_time = MPI_WTIME() - region%total_time
    Call Write_MPI_Timings(region)
    call mpi_barrier(mycomm, ierr)

  end subroutine PlasComCM_Finalize_Timings

  subroutine PlasComCM_Finalize_TM(region)

    type(t_region), pointer, intent(inout) :: region

#ifdef BUILD_TMSOLVER
    if(region%input%TMSolve /= FALSE) call CleanUpThermomechanical(region)
#endif

  end subroutine PlasComCM_Finalize_TM

End Module ModPlasComCM
