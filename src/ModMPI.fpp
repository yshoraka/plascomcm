! Copyright (c) 2015, University of Illinois at Urbana-Champaign, XPACC
! License: MIT, http://opensource.org/licenses/MIT
!-----------------------------------------------------------------------
!
! ModMPI.f90
! 
! - container for MPI
!
! Revision history
! - 20 Dec 2006 : DJB : initial code
! - 21 Dec 2006 : DJB : evolution
!
! $Header: /cvsroot/genx/Codes/RocfloCM/Source/ModMPI.f90,v 1.13 2011/09/26 21:12:14 bodony Exp $
!
!-----------------------------------------------------------------------
MODULE ModMPI

! #ifdef __USE_MPIF_DOT_H
  Include 'mpif.h'
! #else
!   USE MPI
! #endif

  ! Global variables are written-once, then final
  integer :: numproc, mycomm ! Global variables are written-once, then final

! #ifndef MPI2
!  integer, parameter :: MPI_MODE_RDONLY = 2
! #endif

CONTAINS

  subroutine Ghost_Cell_Exchange_Box(region, gridID, F)

    USE ModGlobal
    USE ModDataStruct

    implicit none

    type (t_region), pointer :: region
    integer :: gridID, ierr
    real(rfreal), pointer :: F(:)

    ! ... local variables
    type (t_grid), pointer :: grid
    type(t_mixt_input), pointer :: input
    integer :: N1, N2, N3, G1, G2, G3, N1g, N2g, N3g, Nghost, ND
    integer :: lbuf, k, j, i, req(52), cartComm
    integer :: dist_k, dist_j, dist_i
    integer :: offset_i, offset_ki, offset_kji, slice_k, row_j
    integer :: bufsz_W, bufsz_S, bufsz_SW, bufsz_D, bufsz_D_W, bufsz_D_S, bufsz_D_SW
    real(rfreal), allocatable :: sbuf_W(:), sbuf_E(:)
    real(rfreal), allocatable :: rbuf_W(:), rbuf_E(:)
    real(rfreal), allocatable :: sbuf_S(:), sbuf_N(:), sbuf_SW(:), sbuf_SE(:), sbuf_NW(:), sbuf_NE(:)
    real(rfreal), allocatable :: rbuf_S(:), rbuf_N(:), rbuf_SW(:), rbuf_SE(:), rbuf_NW(:), rbuf_NE(:)
    real(rfreal), allocatable :: sbuf_D(:), sbuf_U(:)
    real(rfreal), allocatable :: rbuf_D(:), rbuf_U(:)
    real(rfreal), allocatable :: sbuf_D_W(:), sbuf_D_E(:), sbuf_D_S(:), sbuf_D_N(:), sbuf_U_W(:), sbuf_U_E(:), sbuf_U_S(:), sbuf_U_N(:)
    real(rfreal), allocatable :: sbuf_D_SW(:), sbuf_D_SE(:), sbuf_D_NW(:), sbuf_D_NE(:)
    real(rfreal), allocatable :: sbuf_U_SW(:), sbuf_U_SE(:), sbuf_U_NW(:), sbuf_U_NE(:)
    real(rfreal), allocatable :: rbuf_D_W(:), rbuf_D_E(:), rbuf_D_S(:), rbuf_D_N(:), rbuf_U_W(:), rbuf_U_E(:), rbuf_U_S(:), rbuf_U_N(:)
    real(rfreal), allocatable :: rbuf_D_SW(:), rbuf_D_SE(:), rbuf_D_NW(:), rbuf_D_NE(:)
    real(rfreal), allocatable :: rbuf_U_SW(:), rbuf_U_SE(:), rbuf_U_NW(:), rbuf_U_NE(:)
    integer, pointer :: box_nbs(:), nGhostRHS(:,:)
    integer :: peroffset(3,2), dir

    ! ... optimization for vectorization, WZhang 03/2015
    integer :: counter_k, counter_kj, counter_slice

    ! ... pointer dereference
    grid   => region%grid(gridID)
    input => grid%input
    box_nbs => grid%box_nbs
    nGhostRHS => grid%nGhostRHS
    cartComm = grid%cartComm

    ND         = grid%ND
    N1         = grid%ie_unique(1) - grid%is_unique(1) + 1
    N2         = grid%ie_unique(2) - grid%is_unique(2) + 1
    N3         = grid%ie_unique(3) - grid%is_unique(3) + 1
    G1         = nGhostRHS(1,1) + nGhostRHS(1,2)
    G2         = nGhostRHS(2,1) + nGhostRHS(2,2)
    G3         = nGhostRHS(3,1) + nGhostRHS(3,2)
    N1g        = N1 + G1
    N2g        = N2 + G2
    N3g        = N3 + G3
    Nghost = input%nOverLap

    slice_k = N2g*N1g
    row_j = N1g

    bufsz_W    = N3 * N2 * Nghost 
    bufsz_S    = N3 * N1 * Nghost 
    bufsz_SW   = N3 * Nghost * Nghost
    bufsz_D    = N2 * N1 * Nghost
    bufsz_D_W   = N2 * Nghost * Nghost
    bufsz_D_S   = N1 * Nghost * Nghost
    bufsz_D_SW  = Nghost * Nghost * Nghost

    ! ... establish periodic storage index offset for sending data to our right
    peroffset(:,:) = 0
    do dir = 1, ND
      if (grid%periodic(dir) >= TRUE .and. grid%periodicStorage == OVERLAP_PERIODIC) then
        if (grid%ie_unique(dir) == grid%GlobalSize(dir)) then
          peroffset(dir,1) = 1
        end if
        if (grid%is_unique(dir) == 1) then
          peroffset(dir,2) = 1
        end if
      end if
    end do

    ! ... exhange the data
    if (box_nbs(PROC_W)    /= MPI_PROC_NULL .or. & 
        box_nbs(PROC_E)    /= MPI_PROC_NULL .or. &
        box_nbs(PROC_S)    /= MPI_PROC_NULL .or. &
        box_nbs(PROC_N)    /= MPI_PROC_NULL .or. &
        box_nbs(PROC_SW)   /= MPI_PROC_NULL .or. &
        box_nbs(PROC_SE)   /= MPI_PROC_NULL .or. &
        box_nbs(PROC_NW)   /= MPI_PROC_NULL .or. &
        box_nbs(PROC_NE)   /= MPI_PROC_NULL .or. &
        box_nbs(PROC_D)    /= MPI_PROC_NULL .or. &
        box_nbs(PROC_U)    /= MPI_PROC_NULL .or. &
        box_nbs(PROC_D_W)  /= MPI_PROC_NULL .or. &
        box_nbs(PROC_D_E)  /= MPI_PROC_NULL .or. &
        box_nbs(PROC_D_S)  /= MPI_PROC_NULL .or. &
        box_nbs(PROC_D_N)  /= MPI_PROC_NULL .or. &
        box_nbs(PROC_U_W)  /= MPI_PROC_NULL .or. &
        box_nbs(PROC_U_E)  /= MPI_PROC_NULL .or. &
        box_nbs(PROC_U_S)  /= MPI_PROC_NULL .or. &
        box_nbs(PROC_U_N)  /= MPI_PROC_NULL .or. &
        box_nbs(PROC_D_SW) /= MPI_PROC_NULL .or. &
        box_nbs(PROC_D_SE) /= MPI_PROC_NULL .or. &
        box_nbs(PROC_D_NW) /= MPI_PROC_NULL .or. &
        box_nbs(PROC_D_NE) /= MPI_PROC_NULL .or. &
        box_nbs(PROC_U_SW) /= MPI_PROC_NULL .or. &
        box_nbs(PROC_U_SE) /= MPI_PROC_NULL .or. &
        box_nbs(PROC_U_NW) /= MPI_PROC_NULL .or. &
        box_nbs(PROC_U_NE) /= MPI_PROC_NULL ) then

      ! ... allocate the send buffer
      allocate(sbuf_W(bufsz_W))
      allocate(sbuf_E(bufsz_W))

      allocate(sbuf_S(bufsz_S))
      allocate(sbuf_N(bufsz_S))
      allocate(sbuf_SW(bufsz_SW))
      allocate(sbuf_SE(bufsz_SW))
      allocate(sbuf_NW(bufsz_SW))
      allocate(sbuf_NE(bufsz_SW))

      allocate(sbuf_D(bufsz_D))
      allocate(sbuf_U(bufsz_D))
      allocate(sbuf_D_W(bufsz_D_W))
      allocate(sbuf_D_E(bufsz_D_W))
      allocate(sbuf_U_W(bufsz_D_W))
      allocate(sbuf_U_E(bufsz_D_W))
      allocate(sbuf_D_S(bufsz_D_S))
      allocate(sbuf_D_N(bufsz_D_S))
      allocate(sbuf_U_S(bufsz_D_S))
      allocate(sbuf_U_N(bufsz_D_S))
      allocate(sbuf_D_SW(bufsz_D_SW))
      allocate(sbuf_D_SE(bufsz_D_SW))
      allocate(sbuf_D_NW(bufsz_D_SW))
      allocate(sbuf_D_NE(bufsz_D_SW))
      allocate(sbuf_U_SW(bufsz_D_SW))
      allocate(sbuf_U_SE(bufsz_D_SW))
      allocate(sbuf_U_NW(bufsz_D_SW))
      allocate(sbuf_U_NE(bufsz_D_SW))

      ! ... allocate the receive buffer
      allocate(rbuf_W(bufsz_W))
      allocate(rbuf_E(bufsz_W))

      allocate(rbuf_S(bufsz_S))
      allocate(rbuf_N(bufsz_S))
      allocate(rbuf_SW(bufsz_SW))
      allocate(rbuf_SE(bufsz_SW))
      allocate(rbuf_NW(bufsz_SW))
      allocate(rbuf_NE(bufsz_SW))

      allocate(rbuf_D(bufsz_D))
      allocate(rbuf_U(bufsz_D))
      allocate(rbuf_D_W(bufsz_D_W))
      allocate(rbuf_D_E(bufsz_D_W))
      allocate(rbuf_U_W(bufsz_D_W))
      allocate(rbuf_U_E(bufsz_D_W))
      allocate(rbuf_D_S(bufsz_D_S))
      allocate(rbuf_D_N(bufsz_D_S))
      allocate(rbuf_U_S(bufsz_D_S))
      allocate(rbuf_U_N(bufsz_D_S))
      allocate(rbuf_D_SW(bufsz_D_SW))
      allocate(rbuf_D_SE(bufsz_D_SW))
      allocate(rbuf_D_NW(bufsz_D_SW))
      allocate(rbuf_D_NE(bufsz_D_SW))
      allocate(rbuf_U_SW(bufsz_D_SW))
      allocate(rbuf_U_SE(bufsz_D_SW))
      allocate(rbuf_U_NW(bufsz_D_SW))
      allocate(rbuf_U_NE(bufsz_D_SW))

      ! ... Post receive buffers
      If (input%caseID /= 'SCALING_NO_COMM') Then

        Call MPI_IRECV(rbuf_W, bufsz_W, MPI_DOUBLE_PRECISION, &
             box_nbs(PROC_W), PROC_W, cartComm, req(1), ierr)
        Call MPI_IRECV(rbuf_E, bufsz_W, MPI_DOUBLE_PRECISION, &
             box_nbs(PROC_E), PROC_E, cartComm, req(2), ierr)

        Call MPI_IRECV(rbuf_S, bufsz_S, MPI_DOUBLE_PRECISION, &
             box_nbs(PROC_S), PROC_S, cartComm, req(3), ierr)
        Call MPI_IRECV(rbuf_N, bufsz_S, MPI_DOUBLE_PRECISION, &
             box_nbs(PROC_N), PROC_N, cartComm, req(4), ierr)
        Call MPI_IRECV(rbuf_SW, bufsz_SW, MPI_DOUBLE_PRECISION, &
             box_nbs(PROC_SW), PROC_SW, cartComm, req(5), ierr)
        Call MPI_IRECV(rbuf_SE, bufsz_SW, MPI_DOUBLE_PRECISION, &
             box_nbs(PROC_SE), PROC_SE, cartComm, req(6), ierr)
        Call MPI_IRECV(rbuf_NW, bufsz_SW, MPI_DOUBLE_PRECISION, &
             box_nbs(PROC_NW), PROC_NW, cartComm, req(7), ierr)
        Call MPI_IRECV(rbuf_NE, bufsz_SW, MPI_DOUBLE_PRECISION, &
             box_nbs(PROC_NE), PROC_NE, cartComm, req(8), ierr)

        Call MPI_IRECV(rbuf_D, bufsz_D, MPI_DOUBLE_PRECISION, &
             box_nbs(PROC_D), PROC_D, cartComm, req(9), ierr)
        Call MPI_IRECV(rbuf_U, bufsz_D, MPI_DOUBLE_PRECISION, &
             box_nbs(PROC_U), PROC_U, cartComm, req(10), ierr)
        Call MPI_IRECV(rbuf_D_W, bufsz_D_W, MPI_DOUBLE_PRECISION, &
             box_nbs(PROC_D_W), PROC_D_W, cartComm, req(11), ierr)
        Call MPI_IRECV(rbuf_D_E, bufsz_D_W, MPI_DOUBLE_PRECISION, &
             box_nbs(PROC_D_E), PROC_D_E, cartComm, req(12), ierr)
        Call MPI_IRECV(rbuf_U_W, bufsz_D_W, MPI_DOUBLE_PRECISION, &
             box_nbs(PROC_U_W), PROC_U_W, cartComm, req(13), ierr)
        Call MPI_IRECV(rbuf_U_E, bufsz_D_W, MPI_DOUBLE_PRECISION, &
             box_nbs(PROC_U_E), PROC_U_E, cartComm, req(14), ierr)
        Call MPI_IRECV(rbuf_D_S, bufsz_D_S, MPI_DOUBLE_PRECISION, &
             box_nbs(PROC_D_S), PROC_D_S, cartComm, req(15), ierr)
        Call MPI_IRECV(rbuf_D_N, bufsz_D_S, MPI_DOUBLE_PRECISION, &
             box_nbs(PROC_D_N), PROC_D_N, cartComm, req(16), ierr)
        Call MPI_IRECV(rbuf_U_S, bufsz_D_S, MPI_DOUBLE_PRECISION, &
             box_nbs(PROC_U_S), PROC_U_S, cartComm, req(17), ierr)
        Call MPI_IRECV(rbuf_U_N, bufsz_D_S, MPI_DOUBLE_PRECISION, &
             box_nbs(PROC_U_N), PROC_U_N, cartComm, req(18), ierr)
        Call MPI_IRECV(rbuf_D_SW, bufsz_D_SW, MPI_DOUBLE_PRECISION, &
             box_nbs(PROC_D_SW), PROC_D_SW, cartComm, req(19), ierr)
        Call MPI_IRECV(rbuf_D_SE, bufsz_D_SW, MPI_DOUBLE_PRECISION, &
             box_nbs(PROC_D_SE), PROC_D_SE, cartComm, req(20), ierr)
        Call MPI_IRECV(rbuf_D_NW, bufsz_D_SW, MPI_DOUBLE_PRECISION, &
             box_nbs(PROC_D_NW), PROC_D_NW, cartComm, req(21), ierr)
        Call MPI_IRECV(rbuf_D_NE, bufsz_D_SW, MPI_DOUBLE_PRECISION, &
             box_nbs(PROC_D_NE), PROC_D_NE, cartComm, req(22), ierr)
        Call MPI_IRECV(rbuf_U_SW, bufsz_D_SW, MPI_DOUBLE_PRECISION, &
             box_nbs(PROC_U_SW), PROC_U_SW, cartComm, req(23), ierr)
        Call MPI_IRECV(rbuf_U_SE, bufsz_D_SW, MPI_DOUBLE_PRECISION, &
             box_nbs(PROC_U_SE), PROC_U_SE, cartComm, req(24), ierr)
        Call MPI_IRECV(rbuf_U_NW, bufsz_D_SW, MPI_DOUBLE_PRECISION, &
             box_nbs(PROC_U_NW), PROC_U_NW, cartComm, req(25), ierr)
        Call MPI_IRECV(rbuf_U_NE, bufsz_D_SW, MPI_DOUBLE_PRECISION, &
             box_nbs(PROC_U_NE), PROC_U_NE, cartComm, req(26), ierr)
      Else

        rbuf_W(1:bufsz_W) = 0.0_rfreal
        rbuf_E(1:bufsz_W) = 0.0_rfreal

        rbuf_S(1:bufsz_S) = 0.0_rfreal
        rbuf_N(1:bufsz_S) = 0.0_rfreal
        rbuf_SW(1:bufsz_SW) = 0.0_rfreal
        rbuf_SE(1:bufsz_SW) = 0.0_rfreal
        rbuf_NW(1:bufsz_SW) = 0.0_rfreal
        rbuf_NE(1:bufsz_SW) = 0.0_rfreal

        rbuf_D(1:bufsz_D) = 0.0_rfreal
        rbuf_U(1:bufsz_D) = 0.0_rfreal
        rbuf_D_W(1:bufsz_D_W) = 0.0_rfreal
        rbuf_D_E(1:bufsz_D_W) = 0.0_rfreal
        rbuf_U_W(1:bufsz_D_W) = 0.0_rfreal
        rbuf_U_E(1:bufsz_D_W) = 0.0_rfreal
        rbuf_D_S(1:bufsz_D_S) = 0.0_rfreal
        rbuf_D_N(1:bufsz_D_S) = 0.0_rfreal
        rbuf_U_S(1:bufsz_D_S) = 0.0_rfreal
        rbuf_U_N(1:bufsz_D_S) = 0.0_rfreal
        rbuf_D_SW(1:bufsz_D_SW) = 0.0_rfreal
        rbuf_D_SE(1:bufsz_D_SW) = 0.0_rfreal
        rbuf_D_NW(1:bufsz_D_SW) = 0.0_rfreal
        rbuf_D_NE(1:bufsz_D_SW) = 0.0_rfreal
        rbuf_U_SW(1:bufsz_D_SW) = 0.0_rfreal
        rbuf_U_SE(1:bufsz_D_SW) = 0.0_rfreal
        rbuf_U_NW(1:bufsz_D_SW) = 0.0_rfreal
        rbuf_U_NE(1:bufsz_D_SW) = 0.0_rfreal

      End If

      ! ... fill the send buffer W
      if (box_nbs(PROC_W) /= MPI_PROC_NULL) then
        dist_k = -1 + nGhostRHS(3,1)
        dist_j = -1 + nGhostRHS(2,1)
        dist_i = nGhostRHS(1,1) + peroffset(1,2)
        offset_i = dist_i
        counter_slice = N2*Nghost
        do k = 1, N3
          offset_ki = offset_i + (k+dist_k)*slice_k
          counter_k = (k-1)*counter_slice
          do j = 1, N2
            offset_kji = offset_ki + (j+dist_j)*row_j
            counter_kj = counter_k + (j-1)*Nghost
#ifdef ENABLE_SIMD
!DIR$ SIMD
#endif
            do i = 1, Nghost
              sbuf_W(i+counter_kj) = F(i+offset_kji)
            end do
          end do
        end do
      end if
      ! ... fill the send buffer E
      if (box_nbs(PROC_E) /= MPI_PROC_NULL) then
        dist_k = -1 + nGhostRHS(3,1)
        dist_j = -1 + nGhostRHS(2,1)
        dist_i = nGhostRHS(1,1) + N1 - Nghost - peroffset(1,1)
        offset_i = dist_i
        counter_slice = N2*Nghost
        do k = 1, N3
          offset_ki = offset_i + (k+dist_k)*slice_k
          counter_k = (k-1)*counter_slice
          do j = 1, N2
            offset_kji = offset_ki + (j+dist_j)*row_j
            counter_kj = counter_k + (j-1)*Nghost
#ifdef ENABLE_SIMD
!DIR$ SIMD
#endif
            do i = 1, Nghost
              sbuf_E(i+counter_kj) = F(i+offset_kji)
            end do
          end do
        end do
      end if 

      ! ... fill the send buffer S
      if (box_nbs(PROC_S) /= MPI_PROC_NULL) then
        dist_k = -1 + nGhostRHS(3,1)
        dist_j = -1 + nGhostRHS(2,1) + peroffset(2,2)
        dist_i = nGhostRHS(1,1)
        offset_i = dist_i
        counter_slice = Nghost*N1
        do k = 1, N3
          offset_ki = offset_i + (k+dist_k)*slice_k
          counter_k = (k-1)*counter_slice
          do j = 1, Nghost
            offset_kji = offset_ki + (j+dist_j)*row_j
            counter_kj = counter_k + (j-1)*N1
#ifdef ENABLE_SIMD
!DIR$ SIMD
#endif
            do i = 1, N1
              sbuf_S(i+counter_kj) = F(i+offset_kji)
            end do
          end do
        end do
      end if
      ! ... fill the send buffer N
      if (box_nbs(PROC_N) /= MPI_PROC_NULL) then
        dist_k = -1 + nGhostRHS(3,1)
        dist_j = -1 + nGhostRHS(2,1) + N2 - Nghost - peroffset(2,1)
        dist_i = nGhostRHS(1,1)
        offset_i = dist_i
        counter_slice = Nghost*N1
        do k = 1, N3
          offset_ki = offset_i + (k+dist_k)*slice_k
          counter_k = (k-1)*counter_slice
          do j = 1, Nghost
            offset_kji = offset_ki + (j+dist_j)*row_j
            counter_kj = counter_k + (j-1)*N1
#ifdef ENABLE_SIMD
!DIR$ SIMD
#endif
            do i = 1, N1
              sbuf_N(i+counter_kj) = F(i+offset_kji)
            end do
          end do
        end do
      end if
      ! ... fill the send buffer SW
      if (box_nbs(PROC_SW) /= MPI_PROC_NULL) then
        dist_k = -1 + nGhostRHS(3,1)
        dist_j = -1 + nGhostRHS(2,1) + peroffset(2,2)
        dist_i = nGhostRHS(1,1) + peroffset(1,2)
        offset_i = dist_i
        counter_slice = Nghost*Nghost
        do k = 1, N3
          offset_ki = offset_i + (k+dist_k)*slice_k
          counter_k = (k-1)*counter_slice
          do j = 1, Nghost
            offset_kji = offset_ki + (j+dist_j)*row_j
            counter_kj = counter_k + (j-1)*Nghost
#ifdef ENABLE_SIMD
!DIR$ SIMD
#endif
            do i = 1, Nghost
              sbuf_SW(i+counter_kj) = F(i+offset_kji)
            end do
          end do
        end do
      end if
      ! ... fill the send buffer SE
      if (box_nbs(PROC_SE) /= MPI_PROC_NULL) then
        dist_k = -1 + nGhostRHS(3,1)
        dist_j = -1 + nGhostRHS(2,1) + peroffset(2,2)
        dist_i = nGhostRHS(1,1) + N1 - Nghost - peroffset(1,1)
        offset_i = dist_i
        counter_slice = Nghost*Nghost
        do k = 1, N3
          offset_ki = offset_i + (k+dist_k)*slice_k
          counter_k = (k-1)*counter_slice
          do j = 1, Nghost
            offset_kji = offset_ki + (j+dist_j)*row_j
            counter_kj = counter_k + (j-1)*Nghost
#ifdef ENABLE_SIMD
!DIR$ SIMD
#endif
            do i = 1, Nghost
              sbuf_SE(i+counter_kj) = F(i+offset_kji)
            end do
          end do
        end do
      end if
      ! ... fill the send buffer NW
      if (box_nbs(PROC_NW) /= MPI_PROC_NULL) then
        dist_k = -1 + nGhostRHS(3,1)
        dist_j = -1 + nGhostRHS(2,1) + N2 - Nghost - peroffset(2,1)
        dist_i = nGhostRHS(1,1) + peroffset(1,2)
        offset_i = dist_i
        counter_slice = Nghost*Nghost
        do k = 1, N3
          offset_ki = offset_i + (k+dist_k)*slice_k
          counter_k = (k-1)*counter_slice
          do j = 1, Nghost
            offset_kji = offset_ki + (j+dist_j)*row_j
            counter_kj = counter_k + (j-1)*Nghost
#ifdef ENABLE_SIMD
!DIR$ SIMD
#endif
            do i = 1, Nghost
              sbuf_NW(i+counter_kj) = F(i+offset_kji)
            end do
          end do
        end do
      end if
      ! ... fill the send buffer NE
      if (box_nbs(PROC_NE) /= MPI_PROC_NULL) then
        dist_k = -1 + nGhostRHS(3,1)
        dist_j = -1 + nGhostRHS(2,1) + N2 - Nghost - peroffset(2,1)
        dist_i = nGhostRHS(1,1) + N1 - Nghost - peroffset(1,1)
        offset_i = dist_i
        counter_slice = Nghost*Nghost
        do k = 1, N3
          offset_ki = offset_i + (k+dist_k)*slice_k
          counter_k = (k-1)*counter_slice
          do j = 1, Nghost
            offset_kji = offset_ki + (j+dist_j)*row_j
            counter_kj = counter_k + (j-1)*Nghost
#ifdef ENABLE_SIMD
!DIR$ SIMD
#endif
            do i = 1, Nghost
              sbuf_NE(i+counter_kj) = F(i+offset_kji)
            end do
          end do
        end do
      end if

      ! ... fill the send buffer D
      if (box_nbs(PROC_D) /= MPI_PROC_NULL) then
        dist_k = -1 + nGhostRHS(3,1) + peroffset(3,2) 
        dist_j = -1 + nGhostRHS(2,1)
        dist_i = nGhostRHS(1,1)
        offset_i = dist_i
        counter_slice = N2*N1
        do k = 1, Nghost
          offset_ki = offset_i + (k+dist_k)*slice_k
          counter_k = (k-1)*counter_slice
          do j = 1, N2
            offset_kji = offset_ki + (j+dist_j)*row_j
            counter_kj = counter_k + (j-1)*N1
#ifdef ENABLE_SIMD
!DIR$ SIMD
#endif
            do i = 1, N1
              sbuf_D(i+counter_kj) = F(i+offset_kji)
            end do
          end do
        end do
      end if
      ! ... fill the send buffer U
      if (box_nbs(PROC_U) /= MPI_PROC_NULL) then
        dist_k = -1 + nGhostRHS(3,1) + N3 - Nghost - peroffset(3,1)
        dist_j = -1 + nGhostRHS(2,1)
        dist_i = nGhostRHS(1,1)
        offset_i = dist_i
        counter_slice = N2*N1
        do k = 1, Nghost
          offset_ki = offset_i + (k+dist_k)*slice_k
          counter_k = (k-1)*counter_slice
          do j = 1, N2
            offset_kji = offset_ki + (j+dist_j)*row_j
            counter_kj = counter_k + (j-1)*N1
#ifdef ENABLE_SIMD
!DIR$ SIMD
#endif
            do i = 1, N1
              sbuf_U(i+counter_kj) = F(i+offset_kji)
            end do
          end do
        end do
      end if
      ! ... fill the send buffer D_W
      if (box_nbs(PROC_D_W) /= MPI_PROC_NULL) then
        dist_k = -1 + nGhostRHS(3,1) + peroffset(3,2)
        dist_j = -1 + nGhostRHS(2,1)
        dist_i = nGhostRHS(1,1) + peroffset(1,2)
        offset_i = dist_i
        counter_slice = N2*Nghost
        do k = 1, Nghost
          offset_ki = offset_i + (k+dist_k)*slice_k
          counter_k = (k-1)*counter_slice
          do j = 1, N2
            offset_kji = offset_ki + (j+dist_j)*row_j
            counter_kj = counter_k + (j-1)*Nghost
#ifdef ENABLE_SIMD
!DIR$ SIMD
#endif
            do i = 1, Nghost
              sbuf_D_W(i+counter_kj) = F(i+offset_kji)
            end do
          end do
        end do
      end if
      ! ... fill the send buffer D_E
      if (box_nbs(PROC_D_E) /= MPI_PROC_NULL) then
        dist_k = -1 + nGhostRHS(3,1) + peroffset(3,2)
        dist_j = -1 + nGhostRHS(2,1)
        dist_i = nGhostRHS(1,1) + N1 - Nghost - peroffset(1,1)
        offset_i = dist_i
        counter_slice = N2*Nghost
        do k = 1, Nghost
          offset_ki = offset_i + (k+dist_k)*slice_k
          counter_k = (k-1)*counter_slice
          do j = 1, N2
            offset_kji = offset_ki + (j+dist_j)*row_j
            counter_kj = counter_k + (j-1)*Nghost
#ifdef ENABLE_SIMD
!DIR$ SIMD
#endif
            do i = 1, Nghost
              sbuf_D_E(i+counter_kj) = F(i+offset_kji)
            end do
          end do
        end do
      end if
      ! ... fill the send buffer U_W
      if (box_nbs(PROC_U_W) /= MPI_PROC_NULL) then
        dist_k = -1 + nGhostRHS(3,1) + N3 - Nghost - peroffset(3,1)
        dist_j = -1 + nGhostRHS(2,1)
        dist_i = nGhostRHS(1,1) + peroffset(1,2)
        offset_i = dist_i
        counter_slice = N2*Nghost
        do k = 1, Nghost
          offset_ki = offset_i + (k+dist_k)*slice_k
          counter_k = (k-1)*counter_slice
          do j = 1, N2
            offset_kji = offset_ki + (j+dist_j)*row_j
            counter_kj = counter_k + (j-1)*Nghost
#ifdef ENABLE_SIMD
!DIR$ SIMD
#endif
            do i = 1, Nghost
              sbuf_U_W(i+counter_kj) = F(i+offset_kji)
            end do
          end do
        end do
      end if
      ! ... fill the send buffer U_E
      if (box_nbs(PROC_U_E) /= MPI_PROC_NULL) then
        dist_k = -1 + nGhostRHS(3,1) + N3 - Nghost - peroffset(3,1)
        dist_j = -1 + nGhostRHS(2,1)
        dist_i = nGhostRHS(1,1) + N1 - Nghost - peroffset(1,1)
        offset_i = dist_i
        counter_slice = N2*Nghost
        do k = 1, Nghost
          offset_ki = offset_i + (k+dist_k)*slice_k
          counter_k = (k-1)*counter_slice
          do j = 1, N2
            offset_kji = offset_ki + (j+dist_j)*row_j
            counter_kj = counter_k + (j-1)*Nghost
#ifdef ENABLE_SIMD
!DIR$ SIMD
#endif
            do i = 1, Nghost
              sbuf_U_E(i+counter_kj) = F(i+offset_kji)
            end do
          end do
        end do
      end if
      ! ... fill the send buffer D_S
      if (box_nbs(PROC_D_S) /= MPI_PROC_NULL) then
        dist_k = -1 + nGhostRHS(3,1) + peroffset(3,2)
        dist_j = -1 + nGhostRHS(2,1) + peroffset(2,2)
        dist_i = nGhostRHS(1,1)
        offset_i = dist_i
        counter_slice = Nghost*N1
        do k = 1, Nghost
          offset_ki = offset_i + (k+dist_k)*slice_k
          counter_k = (k-1)*counter_slice
          do j = 1, Nghost
            offset_kji = offset_ki + (j+dist_j)*row_j
            counter_kj = counter_k + (j-1)*N1
#ifdef ENABLE_SIMD
!DIR$ SIMD
#endif
            do i = 1, N1
              sbuf_D_S(i+counter_kj) = F(i+offset_kji)
            end do
          end do
        end do
      end if
      ! ... fill the send buffer D_N
      if (box_nbs(PROC_D_N) /= MPI_PROC_NULL) then
        dist_k = -1 + nGhostRHS(3,1) + peroffset(3,2)
        dist_j = -1 + nGhostRHS(2,1) + N2 - Nghost - peroffset(2,1)
        dist_i = nGhostRHS(1,1)
        offset_i = dist_i
        counter_slice = Nghost*N1
        do k = 1, Nghost
          offset_ki = offset_i + (k+dist_k)*slice_k
          counter_k = (k-1)*counter_slice
          do j = 1, Nghost
            offset_kji = offset_ki + (j+dist_j)*row_j
            counter_kj = counter_k + (j-1)*N1
#ifdef ENABLE_SIMD
!DIR$ SIMD
#endif
            do i = 1, N1
              sbuf_D_N(i+counter_kj) = F(i+offset_kji)
            end do
          end do
        end do
      end if
      ! ... fill the send buffer U_S
      if (box_nbs(PROC_U_S) /= MPI_PROC_NULL) then
        dist_k = -1 + nGhostRHS(3,1) + N3 - Nghost - peroffset(3,1)
        dist_j = -1 + nGhostRHS(2,1) + peroffset(2,2)
        dist_i = nGhostRHS(1,1)
        offset_i = dist_i
        counter_slice = Nghost*N1
        do k = 1, Nghost
          offset_ki = offset_i + (k+dist_k)*slice_k
          counter_k = (k-1)*counter_slice
          do j = 1, Nghost
            offset_kji = offset_ki + (j+dist_j)*row_j
            counter_kj = counter_k + (j-1)*N1
#ifdef ENABLE_SIMD
!DIR$ SIMD
#endif
            do i = 1, N1
              sbuf_U_S(i+counter_kj) = F(i+offset_kji)
            end do
          end do
        end do
      end if
      ! ... fill the send buffer U_N
      if (box_nbs(PROC_U_N) /= MPI_PROC_NULL) then
        dist_k = -1 + nGhostRHS(3,1) + N3 - Nghost - peroffset(3,1)
        dist_j = -1 + nGhostRHS(2,1) + N2 - Nghost - peroffset(2,1)
        dist_i = nGhostRHS(1,1)
        offset_i = dist_i
        counter_slice = Nghost*N1
        do k = 1, Nghost
          offset_ki = offset_i + (k+dist_k)*slice_k
          counter_k = (k-1)*counter_slice
          do j = 1, Nghost
            offset_kji = offset_ki + (j+dist_j)*row_j
            counter_kj = counter_k + (j-1)*N1
#ifdef ENABLE_SIMD
!DIR$ SIMD
#endif
            do i = 1, N1
              sbuf_U_N(i+counter_kj) = F(i+offset_kji)
            end do
          end do
        end do
      end if
      ! ... fill the send buffer D_SW
      if (box_nbs(PROC_D_SW) /= MPI_PROC_NULL) then
        dist_k = -1 + nGhostRHS(3,1) + peroffset(3,2)
        dist_j = -1 + nGhostRHS(2,1) + peroffset(2,2)
        dist_i = nGhostRHS(1,1) + peroffset(1,2)
        offset_i = dist_i
        counter_slice = Nghost*Nghost
        do k = 1, Nghost
          offset_ki = offset_i + (k+dist_k)*slice_k
          counter_k = (k-1)*counter_slice
          do j = 1, Nghost
            offset_kji = offset_ki + (j+dist_j)*row_j
            counter_kj = counter_k + (j-1)*Nghost
#ifdef ENABLE_SIMD
!DIR$ SIMD
#endif
            do i = 1, Nghost
              sbuf_D_SW(i+counter_kj) = F(i+offset_kji)
            end do
          end do
        end do
      end if
      ! ... fill the send buffer D_SE
      if (box_nbs(PROC_D_SE) /= MPI_PROC_NULL) then
        dist_k = -1 + nGhostRHS(3,1) + peroffset(3,2)
        dist_j = -1 + nGhostRHS(2,1) + peroffset(2,2)
        dist_i = nGhostRHS(1,1) + N1 - Nghost - peroffset(1,1)
        offset_i = dist_i
        counter_slice = Nghost*Nghost
        do k = 1, Nghost
          offset_ki = offset_i + (k+dist_k)*slice_k
          counter_k = (k-1)*counter_slice
          do j = 1, Nghost
            offset_kji = offset_ki + (j+dist_j)*row_j
            counter_kj = counter_k + (j-1)*Nghost
#ifdef ENABLE_SIMD
!DIR$ SIMD
#endif
            do i = 1, Nghost
              sbuf_D_SE(i+counter_kj) = F(i+offset_kji)
            end do
          end do
        end do
      end if
      ! ... fill the send buffer D_NW
      if (box_nbs(PROC_D_NW) /= MPI_PROC_NULL) then
        dist_k = -1 + nGhostRHS(3,1) + peroffset(3,2)
        dist_j = -1 + nGhostRHS(2,1) + N2 - Nghost - peroffset(2,1)
        dist_i = nGhostRHS(1,1) + peroffset(1,2)
        offset_i = dist_i
        counter_slice = Nghost*Nghost
        do k = 1, Nghost
          offset_ki = offset_i + (k+dist_k)*slice_k
          counter_k = (k-1)*counter_slice
          do j = 1, Nghost
            offset_kji = offset_ki + (j+dist_j)*row_j
            counter_kj = counter_k + (j-1)*Nghost
#ifdef ENABLE_SIMD
!DIR$ SIMD
#endif
            do i = 1, Nghost
              sbuf_D_NW(i+counter_kj) = F(i+offset_kji)
            end do
          end do
        end do
      end if
      ! ... fill the send buffer D_NE
      if (box_nbs(PROC_D_NE) /= MPI_PROC_NULL) then
        dist_k = -1 + nGhostRHS(3,1) + peroffset(3,2)
        dist_j = -1 + nGhostRHS(2,1) + N2 - Nghost - peroffset(2,1)
        dist_i = nGhostRHS(1,1) + N1 - Nghost - peroffset(1,1)
        offset_i = dist_i
        counter_slice = Nghost*Nghost
        do k = 1, Nghost
          offset_ki = offset_i + (k+dist_k)*slice_k
          counter_k = (k-1)*counter_slice
          do j = 1, Nghost
            offset_kji = offset_ki + (j+dist_j)*row_j
            counter_kj = counter_k + (j-1)*Nghost
#ifdef ENABLE_SIMD
!DIR$ SIMD
#endif
            do i = 1, Nghost
              sbuf_D_NE(i+counter_kj) = F(i+offset_kji)
            end do
          end do
        end do
      end if
      ! ... fill the send buffer U_SW
      if (box_nbs(PROC_U_SW) /= MPI_PROC_NULL) then
        dist_k = -1 + nGhostRHS(3,1) + N3 - Nghost - peroffset(3,1)
        dist_j = -1 + nGhostRHS(2,1) + peroffset(2,2)
        dist_i = nGhostRHS(1,1) + peroffset(1,2)
        offset_i = dist_i
        counter_slice = Nghost*Nghost
        do k = 1, Nghost
          offset_ki = offset_i + (k+dist_k)*slice_k
          counter_k = (k-1)*counter_slice
          do j = 1, Nghost
            offset_kji = offset_ki + (j+dist_j)*row_j
            counter_kj = counter_k + (j-1)*Nghost
#ifdef ENABLE_SIMD
!DIR$ SIMD
#endif
            do i = 1, Nghost
              sbuf_U_SW(i+counter_kj) = F(i+offset_kji)
            end do
          end do
        end do
      end if
      ! ... fill the send buffer U_SE
      if (box_nbs(PROC_U_SE) /= MPI_PROC_NULL) then
        dist_k = -1 + nGhostRHS(3,1) + N3 - Nghost - peroffset(3,1)
        dist_j = -1 + nGhostRHS(2,1) + peroffset(2,2)
        dist_i = nGhostRHS(1,1) + N1 - Nghost - peroffset(1,1)
        offset_i = dist_i
        counter_slice = Nghost*Nghost
        do k = 1, Nghost
          offset_ki = offset_i + (k+dist_k)*slice_k
          counter_k = (k-1)*counter_slice
          do j = 1, Nghost
            offset_kji = offset_ki + (j+dist_j)*row_j
            counter_kj = counter_k + (j-1)*Nghost
#ifdef ENABLE_SIMD
!DIR$ SIMD
#endif
            do i = 1, Nghost
              sbuf_U_SE(i+counter_kj) = F(i+offset_kji)
            end do
          end do
        end do
      end if
      ! ... fill the send buffer U_NW
      if (box_nbs(PROC_U_NW) /= MPI_PROC_NULL) then
        dist_k = -1 + nGhostRHS(3,1) + N3 - Nghost - peroffset(3,1)
        dist_j = -1 + nGhostRHS(2,1) + N2 - Nghost - peroffset(2,1)
        dist_i = nGhostRHS(1,1) + peroffset(1,2)
        offset_i = dist_i
        counter_slice = Nghost*Nghost
        do k = 1, Nghost
          offset_ki = offset_i + (k+dist_k)*slice_k
          counter_k = (k-1)*counter_slice
          do j = 1, Nghost
            offset_kji = offset_ki + (j+dist_j)*row_j
            counter_kj = counter_k + (j-1)*Nghost
#ifdef ENABLE_SIMD
!DIR$ SIMD
#endif
            do i = 1, Nghost
              sbuf_U_NW(i+counter_kj) = F(i+offset_kji)
            end do
          end do
        end do
      end if
      ! ... fill the send buffer U_NE
      if (box_nbs(PROC_U_NE) /= MPI_PROC_NULL) then
        dist_k = -1 + nGhostRHS(3,1) + N3 - Nghost - peroffset(3,1)
        dist_j = -1 + nGhostRHS(2,1) + N2 - Nghost - peroffset(2,1)
        dist_i = nGhostRHS(1,1) + N1 - Nghost - peroffset(1,1)
        offset_i = dist_i
        counter_slice = Nghost*Nghost
        do k = 1, Nghost
          offset_ki = offset_i + (k+dist_k)*slice_k
          counter_k = (k-1)*counter_slice
          do j = 1, Nghost
            offset_kji = offset_ki + (j+dist_j)*row_j
            counter_kj = counter_k + (j-1)*Nghost
#ifdef ENABLE_SIMD
!DIR$ SIMD
#endif
            do i = 1, Nghost
              sbuf_U_NE(i+counter_kj) = F(i+offset_kji)
            end do
          end do
        end do
      end if

      ! ... Send data to neighbors
      If (input%caseID /= 'SCALING_NO_COMM') Then

        Call MPI_ISEND(sbuf_W, bufsz_W, MPI_DOUBLE_PRECISION, &
             box_nbs(PROC_W), PROC_E, cartComm, req(27), ierr)
        Call MPI_ISEND(sbuf_E, bufsz_W, MPI_DOUBLE_PRECISION, &
             box_nbs(PROC_E), PROC_W, cartComm, req(28), ierr)

        Call MPI_ISEND(sbuf_S, bufsz_S, MPI_DOUBLE_PRECISION, &
             box_nbs(PROC_S), PROC_N, cartComm, req(29), ierr)
        Call MPI_ISEND(sbuf_N, bufsz_S, MPI_DOUBLE_PRECISION, &
             box_nbs(PROC_N), PROC_S, cartComm, req(30), ierr)
        Call MPI_ISEND(sbuf_SW, bufsz_SW, MPI_DOUBLE_PRECISION, &
             box_nbs(PROC_SW), PROC_NE, cartComm, req(31), ierr)
        Call MPI_ISEND(sbuf_SE, bufsz_SW, MPI_DOUBLE_PRECISION, &
             box_nbs(PROC_SE), PROC_NW, cartComm, req(32), ierr)
        Call MPI_ISEND(sbuf_NW, bufsz_SW, MPI_DOUBLE_PRECISION, &
             box_nbs(PROC_NW), PROC_SE, cartComm, req(33), ierr)
        Call MPI_ISEND(sbuf_NE, bufsz_SW, MPI_DOUBLE_PRECISION, &
             box_nbs(PROC_NE), PROC_SW, cartComm, req(34), ierr)

        Call MPI_ISEND(sbuf_D, bufsz_D, MPI_DOUBLE_PRECISION, &
             box_nbs(PROC_D), PROC_U, cartComm, req(35), ierr)
        Call MPI_ISEND(sbuf_U, bufsz_D, MPI_DOUBLE_PRECISION, &
             box_nbs(PROC_U), PROC_D, cartComm, req(36), ierr)
        Call MPI_ISEND(sbuf_D_W, bufsz_D_W, MPI_DOUBLE_PRECISION, &
             box_nbs(PROC_D_W), PROC_U_E, cartComm, req(37), ierr)
        Call MPI_ISEND(sbuf_D_E, bufsz_D_W, MPI_DOUBLE_PRECISION, &
             box_nbs(PROC_D_E), PROC_U_W, cartComm, req(38), ierr)
        Call MPI_ISEND(sbuf_U_W, bufsz_D_W, MPI_DOUBLE_PRECISION, &
             box_nbs(PROC_U_W), PROC_D_E, cartComm, req(39), ierr)
        Call MPI_ISEND(sbuf_U_E, bufsz_D_W, MPI_DOUBLE_PRECISION, &
             box_nbs(PROC_U_E), PROC_D_W, cartComm, req(40), ierr)
        Call MPI_ISEND(sbuf_D_S, bufsz_D_S, MPI_DOUBLE_PRECISION, &
             box_nbs(PROC_D_S), PROC_U_N, cartComm, req(41), ierr)
        Call MPI_ISEND(sbuf_D_N, bufsz_D_S, MPI_DOUBLE_PRECISION, &
             box_nbs(PROC_D_N), PROC_U_S, cartComm, req(42), ierr)
        Call MPI_ISEND(sbuf_U_S, bufsz_D_S, MPI_DOUBLE_PRECISION, &
             box_nbs(PROC_U_S), PROC_D_N, cartComm, req(43), ierr)
        Call MPI_ISEND(sbuf_U_N, bufsz_D_S, MPI_DOUBLE_PRECISION, &
             box_nbs(PROC_U_N), PROC_D_S, cartComm, req(44), ierr)
        Call MPI_ISEND(sbuf_D_SW, bufsz_D_SW, MPI_DOUBLE_PRECISION, &
             box_nbs(PROC_D_SW), PROC_U_NE, cartComm, req(45), ierr)
        Call MPI_ISEND(sbuf_D_SE, bufsz_D_SW, MPI_DOUBLE_PRECISION, &
             box_nbs(PROC_D_SE), PROC_U_NW, cartComm, req(46), ierr)
        Call MPI_ISEND(sbuf_D_NW, bufsz_D_SW, MPI_DOUBLE_PRECISION, &
             box_nbs(PROC_D_NW), PROC_U_SE, cartComm, req(47), ierr)
        Call MPI_ISEND(sbuf_D_NE, bufsz_D_SW, MPI_DOUBLE_PRECISION, &
             box_nbs(PROC_D_NE), PROC_U_SW, cartComm, req(48), ierr)
        Call MPI_ISEND(sbuf_U_SW, bufsz_D_SW, MPI_DOUBLE_PRECISION, &
             box_nbs(PROC_U_SW), PROC_D_NE, cartComm, req(49), ierr)
        Call MPI_ISEND(sbuf_U_SE, bufsz_D_SW, MPI_DOUBLE_PRECISION, &
             box_nbs(PROC_U_SE), PROC_D_NW, cartComm, req(50), ierr)
        Call MPI_ISEND(sbuf_U_NW, bufsz_D_SW, MPI_DOUBLE_PRECISION, &
             box_nbs(PROC_U_NW), PROC_D_SE, cartComm, req(51), ierr)
        Call MPI_ISEND(sbuf_U_NE, bufsz_D_SW, MPI_DOUBLE_PRECISION, &
             box_nbs(PROC_U_NE), PROC_D_SW, cartComm, req(52), ierr)

        ! ... wait for completing the communication
        Call MPI_Waitall(52, req, MPI_STATUSES_IGNORE, ierr)
    
      End If

      ! ... store the data in the recv buffer W
      if (box_nbs(PROC_W) /= MPI_PROC_NULL) then
        dist_k = -1 + nGhostRHS(3,1)
        dist_j = -1 + nGhostRHS(2,1)
        dist_i = 0
        offset_i = dist_i
        counter_slice = N2*Nghost
        do k = 1, N3
          offset_ki = offset_i + (k+dist_k)*slice_k
          counter_k = (k-1)*counter_slice
          do j = 1, N2
            offset_kji = offset_ki + (j+dist_j)*row_j
            counter_kj = counter_k + (j-1)*Nghost
#ifdef ENABLE_SIMD
!DIR$ SIMD
#endif
            do i = 1, Nghost
              F(i+offset_kji) = rbuf_W(i+counter_kj)
            end do
          end do
        end do
      end if
      ! ... store the data in the recv buffer E
      if (box_nbs(PROC_E) /= MPI_PROC_NULL) then
        dist_k = -1 + nGhostRHS(3,1)
        dist_j = -1 + nGhostRHS(2,1)
        dist_i = nGhostRHS(1,1) + N1
        offset_i = dist_i
        counter_slice = N2*Nghost
        do k = 1, N3
          offset_ki = offset_i + (k+dist_k)*slice_k
          counter_k = (k-1)*counter_slice
          do j = 1, N2
            offset_kji = offset_ki + (j+dist_j)*row_j
            counter_kj = counter_k + (j-1)*Nghost
#ifdef ENABLE_SIMD
!DIR$ SIMD
#endif
            do i = 1, Nghost
              F(i+offset_kji) = rbuf_E(i+counter_kj)
            end do
          end do
        end do
      end if 

      ! ... store the data in the recv buffer S
      if (box_nbs(PROC_S) /= MPI_PROC_NULL) then
        dist_k = -1 + nGhostRHS(3,1)
        dist_j = -1
        dist_i = nGhostRHS(1,1)
        offset_i = dist_i
        counter_slice = Nghost*N1
        do k = 1, N3
          offset_ki = offset_i + (k+dist_k)*slice_k
          counter_k = (k-1)*counter_slice
          do j = 1, Nghost
            offset_kji = offset_ki + (j+dist_j)*row_j
            counter_kj = counter_k + (j-1)*N1
#ifdef ENABLE_SIMD
!DIR$ SIMD
#endif
            do i = 1, N1
              F(i+offset_kji) = rbuf_S(i+counter_kj)
            end do
          end do
        end do
      end if
      ! ... store the data in the recv buffer N
      if (box_nbs(PROC_N) /= MPI_PROC_NULL) then
        dist_k = -1 + nGhostRHS(3,1)
        dist_j = -1 + nGhostRHS(2,1) + N2
        dist_i = nGhostRHS(1,1)
        offset_i = dist_i
        counter_slice = Nghost*N1
        do k = 1, N3
          offset_ki = offset_i + (k+dist_k)*slice_k
          counter_k = (k-1)*counter_slice
          do j = 1, Nghost
            offset_kji = offset_ki + (j+dist_j)*row_j
            counter_kj = counter_k + (j-1)*N1
#ifdef ENABLE_SIMD
!DIR$ SIMD
#endif
            do i = 1, N1
              F(i+offset_kji) = rbuf_N(i+counter_kj)
            end do
          end do
        end do
      end if
      ! ... store the data in the recv buffer SW
      if (box_nbs(PROC_SW) /= MPI_PROC_NULL) then
        dist_k = -1 + nGhostRHS(3,1)
        dist_j = -1
        dist_i = 0
        offset_i = dist_i
        counter_slice = Nghost*Nghost
        do k = 1, N3
          offset_ki = offset_i + (k+dist_k)*slice_k
          counter_k = (k-1)*counter_slice
          do j = 1, Nghost
            offset_kji = offset_ki + (j+dist_j)*row_j
            counter_kj = counter_k + (j-1)*Nghost
#ifdef ENABLE_SIMD
!DIR$ SIMD
#endif
            do i = 1, Nghost
              F(i+offset_kji) = rbuf_SW(i+counter_kj)
            end do
          end do
        end do
      end if
      ! ... store the data in the recv buffer SE
      if (box_nbs(PROC_SE) /= MPI_PROC_NULL) then
        dist_k = -1 + nGhostRHS(3,1)
        dist_j = -1
        dist_i = nGhostRHS(1,1) + N1
        offset_i = dist_i
        counter_slice = Nghost*Nghost
        do k = 1, N3
          offset_ki = offset_i + (k+dist_k)*slice_k
          counter_k = (k-1)*counter_slice
          do j = 1, Nghost
            offset_kji = offset_ki + (j+dist_j)*row_j
            counter_kj = counter_k + (j-1)*Nghost
#ifdef ENABLE_SIMD
!DIR$ SIMD
#endif
            do i = 1, Nghost
              F(i+offset_kji) = rbuf_SE(i+counter_kj)
            end do
          end do
        end do
      end if
      ! ... store the data in the recv buffer NW
      if (box_nbs(PROC_NW) /= MPI_PROC_NULL) then
        dist_k = -1 + nGhostRHS(3,1)
        dist_j = -1 + nGhostRHS(2,1) + N2
        dist_i = 0
        offset_i = dist_i
        counter_slice = Nghost*Nghost
        do k = 1, N3
          offset_ki = offset_i + (k+dist_k)*slice_k
          counter_k = (k-1)*counter_slice
          do j = 1, Nghost
            offset_kji = offset_ki + (j+dist_j)*row_j
            counter_kj = counter_k + (j-1)*Nghost
#ifdef ENABLE_SIMD
!DIR$ SIMD
#endif
            do i = 1, Nghost
              F(i+offset_kji) = rbuf_NW(i+counter_kj)
            end do
          end do
        end do
      end if
      ! ... store the data in the recv buffer NE
      if (box_nbs(PROC_NE) /= MPI_PROC_NULL) then
        dist_k = -1 + nGhostRHS(3,1)
        dist_j = -1 + nGhostRHS(2,1) + N2
        dist_i = nGhostRHS(1,1) + N1
        offset_i = dist_i
        counter_slice = Nghost*Nghost
        do k = 1, N3
          offset_ki = offset_i + (k+dist_k)*slice_k
          counter_k = (k-1)*counter_slice
          do j = 1, Nghost
            offset_kji = offset_ki + (j+dist_j)*row_j
            counter_kj = counter_k + (j-1)*Nghost
#ifdef ENABLE_SIMD
!DIR$ SIMD
#endif
            do i = 1, Nghost
              F(i+offset_kji) = rbuf_NE(i+counter_kj)
            end do
          end do
        end do
      end if

      ! ... store the data in the recv buffer D
      if (box_nbs(PROC_D) /= MPI_PROC_NULL) then
        dist_k = -1
        dist_j = -1 + nGhostRHS(2,1)
        dist_i = nGhostRHS(1,1)
        offset_i = dist_i
        counter_slice = N2*N1
        do k = 1, Nghost
          offset_ki = offset_i + (k+dist_k)*slice_k
          counter_k = (k-1)*counter_slice
          do j = 1, N2
            offset_kji = offset_ki + (j+dist_j)*row_j
            counter_kj = counter_k + (j-1)*N1
#ifdef ENABLE_SIMD
!DIR$ SIMD
#endif
            do i = 1, N1
              F(i+offset_kji) = rbuf_D(i+counter_kj)
            end do
          end do
        end do
      end if
      ! ... store the data in the recv buffer U
      if (box_nbs(PROC_U) /= MPI_PROC_NULL) then
        dist_k = -1 + nGhostRHS(3,1) + N3
        dist_j = -1 + nGhostRHS(2,1)
        dist_i = nGhostRHS(1,1)
        offset_i = dist_i
        counter_slice = N2*N1
        do k = 1, Nghost
          offset_ki = offset_i + (k+dist_k)*slice_k
          counter_k = (k-1)*counter_slice
          do j = 1, N2
            offset_kji = offset_ki + (j+dist_j)*row_j
            counter_kj = counter_k + (j-1)*N1
#ifdef ENABLE_SIMD
!DIR$ SIMD
#endif
            do i = 1, N1
              F(i+offset_kji) = rbuf_U(i+counter_kj)
            end do
          end do
        end do
      end if
      ! ... store the data in the recv buffer D_W
      if (box_nbs(PROC_D_W) /= MPI_PROC_NULL) then
        dist_k = -1
        dist_j = -1 + nGhostRHS(2,1)
        dist_i = 0
        offset_i = dist_i
        counter_slice = N2*Nghost
        do k = 1, Nghost
          offset_ki = offset_i + (k+dist_k)*slice_k
          counter_k = (k-1)*counter_slice
          do j = 1, N2
            offset_kji = offset_ki + (j+dist_j)*row_j
            counter_kj = counter_k + (j-1)*Nghost
#ifdef ENABLE_SIMD
!DIR$ SIMD
#endif
            do i = 1, Nghost
              F(i+offset_kji) = rbuf_D_W(i+counter_kj)
            end do
          end do
        end do
      end if
      ! ... store the data in the recv buffer D_E
      if (box_nbs(PROC_D_E) /= MPI_PROC_NULL) then
        dist_k = -1
        dist_j = -1 + nGhostRHS(2,1)
        dist_i = nGhostRHS(1,1) + N1
        offset_i = dist_i
        counter_slice = N2*Nghost
        do k = 1, Nghost
          offset_ki = offset_i + (k+dist_k)*slice_k
          counter_k = (k-1)*counter_slice
          do j = 1, N2
            offset_kji = offset_ki + (j+dist_j)*row_j
            counter_kj = counter_k + (j-1)*Nghost
#ifdef ENABLE_SIMD
!DIR$ SIMD
#endif
            do i = 1, Nghost
              F(i+offset_kji) = rbuf_D_E(i+counter_kj)
            end do
          end do
        end do
      end if
      ! ... store the data in the recv buffer U_W
      if (box_nbs(PROC_U_W) /= MPI_PROC_NULL) then
        dist_k = -1 + nGhostRHS(3,1) + N3
        dist_j = -1 + nGhostRHS(2,1)
        dist_i = 0
        offset_i = dist_i
        counter_slice = N2*Nghost
        do k = 1, Nghost
          offset_ki = offset_i + (k+dist_k)*slice_k
          counter_k = (k-1)*counter_slice
          do j = 1, N2
            offset_kji = offset_ki + (j+dist_j)*row_j
            counter_kj = counter_k + (j-1)*Nghost
#ifdef ENABLE_SIMD
!DIR$ SIMD
#endif
            do i = 1, Nghost
              F(i+offset_kji) = rbuf_U_W(i+counter_kj)
            end do
          end do
        end do
      end if
      ! ... store the data in the recv buffer U_E
      if (box_nbs(PROC_U_E) /= MPI_PROC_NULL) then
        dist_k = -1 + nGhostRHS(3,1) + N3
        dist_j = -1 + nGhostRHS(2,1)
        dist_i = nGhostRHS(1,1) + N1
        offset_i = dist_i
        counter_slice = N2*Nghost
        do k = 1, Nghost
          offset_ki = offset_i + (k+dist_k)*slice_k
          counter_k = (k-1)*counter_slice
          do j = 1, N2
            offset_kji = offset_ki + (j+dist_j)*row_j
            counter_kj = counter_k + (j-1)*Nghost
#ifdef ENABLE_SIMD
!DIR$ SIMD
#endif
            do i = 1, Nghost
              F(i+offset_kji) = rbuf_U_E(i+counter_kj)
            end do
          end do
        end do
      end if
      ! ... store the data in the recv buffer D_S
      if (box_nbs(PROC_D_S) /= MPI_PROC_NULL) then
        dist_k = -1
        dist_j = -1
        dist_i = nGhostRHS(1,1)
        offset_i = dist_i
        counter_slice = Nghost*N1
        do k = 1, Nghost
          offset_ki = offset_i + (k+dist_k)*slice_k
          counter_k = (k-1)*counter_slice
          do j = 1, Nghost
            offset_kji = offset_ki + (j+dist_j)*row_j
            counter_kj = counter_k + (j-1)*N1
#ifdef ENABLE_SIMD
!DIR$ SIMD
#endif
            do i = 1, N1
              F(i+offset_kji) = rbuf_D_S(i+counter_kj)
            end do
          end do
        end do
      end if
      ! ... store the data in the recv buffer D_N
      if (box_nbs(PROC_D_N) /= MPI_PROC_NULL) then
        dist_k = -1
        dist_j = -1 + nGhostRHS(2,1) + N2
        dist_i = nGhostRHS(1,1)
        offset_i = dist_i
        counter_slice = Nghost*N1
        do k = 1, Nghost
          offset_ki = offset_i + (k+dist_k)*slice_k
          counter_k = (k-1)*counter_slice
          do j = 1, Nghost
            offset_kji = offset_ki + (j+dist_j)*row_j
            counter_kj = counter_k + (j-1)*N1
#ifdef ENABLE_SIMD
!DIR$ SIMD
#endif
            do i = 1, N1
              F(i+offset_kji) = rbuf_D_N(i+counter_kj)
            end do
          end do
        end do
      end if
      ! ... store the data in the recv buffer U_S
      if (box_nbs(PROC_U_S) /= MPI_PROC_NULL) then
        dist_k = -1 + nGhostRHS(3,1) + N3
        dist_j = -1
        dist_i = nGhostRHS(1,1)
        offset_i = dist_i
        counter_slice = Nghost*N1
        do k = 1, Nghost
          offset_ki = offset_i + (k+dist_k)*slice_k
          counter_k = (k-1)*counter_slice
          do j = 1, Nghost
            offset_kji = offset_ki + (j+dist_j)*row_j
            counter_kj = counter_k + (j-1)*N1 
#ifdef ENABLE_SIMD
!DIR$ SIMD
#endif
            do i = 1, N1
              F(i+offset_kji) = rbuf_U_S(i+counter_kj)
            end do
          end do
        end do
      end if
      ! ... store the data in the recv buffer U_N
      if (box_nbs(PROC_U_N) /= MPI_PROC_NULL) then
        dist_k = -1 + nGhostRHS(3,1) + N3
        dist_j = -1 + nGhostRHS(2,1) + N2
        dist_i = nGhostRHS(1,1)
        offset_i = dist_i
        counter_slice = Nghost*N1
        do k = 1, Nghost
          offset_ki = offset_i + (k+dist_k)*slice_k
          counter_k = (k-1)*counter_slice
          do j = 1, Nghost
            offset_kji = offset_ki + (j+dist_j)*row_j
            counter_kj = counter_k + (j-1)*N1
#ifdef ENABLE_SIMD
!DIR$ SIMD
#endif
            do i = 1, N1
              F(i+offset_kji) = rbuf_U_N(i+counter_kj)
            end do
          end do
        end do
      end if
      ! ... store the data in the recv buffer D_SW
      if (box_nbs(PROC_D_SW) /= MPI_PROC_NULL) then
        dist_k = -1
        dist_j = -1
        dist_i = 0
        offset_i = dist_i
        counter_slice = Nghost*Nghost
        do k = 1, Nghost
          offset_ki = offset_i + (k+dist_k)*slice_k
          counter_k = (k-1)*counter_slice
          do j = 1, Nghost
            offset_kji = offset_ki + (j+dist_j)*row_j
            counter_kj = counter_k + (j-1)*Nghost
#ifdef ENABLE_SIMD
!DIR$ SIMD
#endif
            do i = 1, Nghost
              F(i+offset_kji) = rbuf_D_SW(i+counter_kj)
            end do
          end do
        end do
      end if
      ! ... store the data in the recv buffer D_SE
      if (box_nbs(PROC_D_SE) /= MPI_PROC_NULL) then
        dist_k = -1
        dist_j = -1
        dist_i = nGhostRHS(1,1) + N1
        offset_i = dist_i
        counter_slice = Nghost*Nghost
        do k = 1, Nghost
          offset_ki = offset_i + (k+dist_k)*slice_k
          counter_k = (k-1)*counter_slice
          do j = 1, Nghost
            offset_kji = offset_ki + (j+dist_j)*row_j
            counter_kj = counter_k + (j-1)*Nghost
#ifdef ENABLE_SIMD
!DIR$ SIMD
#endif
            do i = 1, Nghost
              F(i+offset_kji) = rbuf_D_SE(i+counter_kj)
            end do
          end do
        end do
      end if
      ! ... store the data in the recv buffer D_NW
      if (box_nbs(PROC_D_NW) /= MPI_PROC_NULL) then
        dist_k = -1
        dist_j = -1 + nGhostRHS(2,1) + N2
        dist_i = 0
        offset_i = dist_i
        counter_slice = Nghost*Nghost
        do k = 1, Nghost
          offset_ki = offset_i + (k+dist_k)*slice_k
          counter_k = (k-1)*counter_slice
          do j = 1, Nghost
            offset_kji = offset_ki + (j+dist_j)*row_j
            counter_kj = counter_k + (j-1)*Nghost
#ifdef ENABLE_SIMD
!DIR$ SIMD
#endif
            do i = 1, Nghost
              F(i+offset_kji) = rbuf_D_NW(i+counter_kj)
            end do
          end do
        end do
      end if
      ! ... store the data in the recv buffer D_NE
      if (box_nbs(PROC_D_NE) /= MPI_PROC_NULL) then
        dist_k = -1
        dist_j = -1 + nGhostRHS(2,1) + N2
        dist_i = nGhostRHS(1,1) + N1
        offset_i = dist_i
        counter_slice = Nghost*Nghost
        do k = 1, Nghost
          offset_ki = offset_i + (k+dist_k)*slice_k
          counter_k = (k-1)*counter_slice
          do j = 1, Nghost
            offset_kji = offset_ki + (j+dist_j)*row_j
            counter_kj = counter_k + (j-1)*Nghost
#ifdef ENABLE_SIMD
!DIR$ SIMD
#endif
            do i = 1, Nghost
              F(i+offset_kji) = rbuf_D_NE(i+counter_kj)
            end do
          end do
        end do
      end if
      ! ... store the data in the recv buffer U_SW
      if (box_nbs(PROC_U_SW) /= MPI_PROC_NULL) then
        dist_k = -1 + nGhostRHS(3,1) + N3
        dist_j = -1
        dist_i = 0
        offset_i = dist_i
        counter_slice = Nghost*Nghost
        do k = 1, Nghost
          offset_ki = offset_i + (k+dist_k)*slice_k
          counter_k = (k-1)*counter_slice
          do j = 1, Nghost
            offset_kji = offset_ki + (j+dist_j)*row_j
            counter_kj = counter_k + (j-1)*Nghost
#ifdef ENABLE_SIMD
!DIR$ SIMD
#endif
            do i = 1, Nghost
              F(i+offset_kji) = rbuf_U_SW(i+counter_kj)
            end do
          end do
        end do
      end if
      ! ... store the data in the recv buffer U_SE
      if (box_nbs(PROC_U_SE) /= MPI_PROC_NULL) then
        dist_k = -1 + nGhostRHS(3,1) + N3
        dist_j = -1
        dist_i = nGhostRHS(1,1) + N1
        offset_i = dist_i
        counter_slice = Nghost*Nghost
        do k = 1, Nghost
          offset_ki = offset_i + (k+dist_k)*slice_k
          counter_k = (k-1)*counter_slice
          do j = 1, Nghost
            offset_kji = offset_ki + (j+dist_j)*row_j
            counter_kj = counter_k + (j-1)*Nghost
#ifdef ENABLE_SIMD
!DIR$ SIMD
#endif
            do i = 1, Nghost
              F(i+offset_kji) = rbuf_U_SE(i+counter_kj)
            end do
          end do
        end do
      end if
      ! ... store the data in the recv buffer U_NW
      if (box_nbs(PROC_U_NW) /= MPI_PROC_NULL) then
        dist_k = -1 + nGhostRHS(3,1) + N3
        dist_j = -1 + nGhostRHS(2,1) + N2
        dist_i = 0
        offset_i = dist_i
        counter_slice = Nghost*Nghost
        do k = 1, Nghost
          offset_ki = offset_i + (k+dist_k)*slice_k
          counter_k = (k-1)*counter_slice
          do j = 1, Nghost
            offset_kji = offset_ki + (j+dist_j)*row_j
            counter_kj = counter_k + (j-1)*Nghost
#ifdef ENABLE_SIMD
!DIR$ SIMD
#endif
            do i = 1, Nghost
              F(i+offset_kji) = rbuf_U_NW(i+counter_kj)
            end do
          end do
        end do
      end if
      ! ... store the data in the recv buffer U_NE
      if (box_nbs(PROC_U_NE) /= MPI_PROC_NULL) then
        dist_k = -1 + nGhostRHS(3,1) + N3
        dist_j = -1 + nGhostRHS(2,1) + N2
        dist_i = nGhostRHS(1,1) + N1
        offset_i = dist_i
        counter_slice = Nghost*Nghost
        do k = 1, Nghost
          offset_ki = offset_i + (k+dist_k)*slice_k
          counter_k = (k-1)*counter_slice
          do j = 1, Nghost
            offset_kji = offset_ki + (j+dist_j)*row_j
            counter_kj = counter_k + (j-1)*Nghost
#ifdef ENABLE_SIMD
!DIR$ SIMD
#endif
            do i = 1, Nghost
              F(i+offset_kji) = rbuf_U_NE(i+counter_kj)
            end do
          end do
        end do
      end if

      ! ... deallocate the send buffer
      deallocate(sbuf_W)
      deallocate(sbuf_E)

      deallocate(sbuf_S)
      deallocate(sbuf_N)
      deallocate(sbuf_SW)
      deallocate(sbuf_SE)
      deallocate(sbuf_NW)
      deallocate(sbuf_NE)

      deallocate(sbuf_D)
      deallocate(sbuf_U)
      deallocate(sbuf_D_W)
      deallocate(sbuf_D_E)
      deallocate(sbuf_U_W)
      deallocate(sbuf_U_E)
      deallocate(sbuf_D_S)
      deallocate(sbuf_D_N)
      deallocate(sbuf_U_S)
      deallocate(sbuf_U_N)
      deallocate(sbuf_D_SW)
      deallocate(sbuf_D_SE)
      deallocate(sbuf_D_NW)
      deallocate(sbuf_D_NE)
      deallocate(sbuf_U_SW)
      deallocate(sbuf_U_SE)
      deallocate(sbuf_U_NW)
      deallocate(sbuf_U_NE)

      ! ... deallocate the receive buffer
      deallocate(rbuf_W)
      deallocate(rbuf_E)

      deallocate(rbuf_S)
      deallocate(rbuf_N)
      deallocate(rbuf_SW)
      deallocate(rbuf_SE)
      deallocate(rbuf_NW)
      deallocate(rbuf_NE)

      deallocate(rbuf_D)
      deallocate(rbuf_U)
      deallocate(rbuf_D_W)
      deallocate(rbuf_D_E)
      deallocate(rbuf_U_W)
      deallocate(rbuf_U_E)
      deallocate(rbuf_D_S)
      deallocate(rbuf_D_N)
      deallocate(rbuf_U_S)
      deallocate(rbuf_U_N)
      deallocate(rbuf_D_SW)
      deallocate(rbuf_D_SE)
      deallocate(rbuf_D_NW)
      deallocate(rbuf_D_NE)
      deallocate(rbuf_U_SW)
      deallocate(rbuf_U_SE)
      deallocate(rbuf_U_NW)
      deallocate(rbuf_U_NE)

    end if

  end subroutine Ghost_Cell_Exchange_Box

  subroutine Ghost_Cell_Exchange(region, gridID, dir, F)

    USE ModGlobal
    USE ModDataStruct

    implicit none

    type (t_region), pointer :: region
    real(rfreal), pointer :: F(:)
    integer :: gridID, dir, leftTag, rightTag, idx, ireq, ierr

    ! ... local variables
    integer :: N, Nc, Ng, Ncg, lp, rp, req(4), sz, gOffset(2)
    integer :: i, j,  l0, l1, perOffset(2)
    type (t_grid), pointer :: grid

    ! ... simplicity
    grid       => region%grid(gridID)
    Nc         = grid%nCells
    N          = grid%ie(dir)-grid%is(dir)+1
    Ncg        = Nc + SUM(grid%nGhostRHS(dir,:))*grid%vdsGhost(dir)
    Ng         = N + SUM(grid%nGhostRHS(dir,:))
    gOffset(:) = grid%nGhostRHS(dir,:)*grid%vdsGhost(dir)
    ! ... grid ng is requesting Ghost Cell information

    ! ... NOTE: This is the "old fashioned way" of using matching
    ! ... send and recvs.  In MPI-2 one could (should?) use remote
    ! ... memory access MPI_Put and MPI_Get.

    ! Call MPI_Barrier(grid%cartComm, IERR)
    
    if (.not. region%derivInit) then
      allocate(region%deriv)
      region%deriv%bufSz = 0
      region%derivInit = .true.
    end if
    
    leftTag  = dir*10
    rightTag = leftTag+1

    ! ... left and right processors
    if ( grid%cartDims(dir) > 1 ) then
      lp = grid%left_process(dir)
      rp = grid%right_process(dir)
    else
      lp = region%myrank
      rp = region%myrank
    end if

    sz = max(gOffset(1), gOffset(2))

    if (sz > region%deriv%bufSz) then
       ! ... deallocate old send and receive bufs
       if (region%deriv%bufSz > 0) deallocate(region%deriv%sbuf, region%deriv%rbuf)
       region%deriv%bufSz = sz
       ! ... allocate send and recv buffers
       allocate(region%deriv%sbuf(region%deriv%bufSz,2))
       allocate(region%deriv%rbuf(region%deriv%bufSz,2))
    end if
       
    ! ... establish periodic storage index offset for sending data to our right
    perOffset(:) = 0
    if (grid%periodic(dir) >= TRUE .and. grid%periodicStorage == OVERLAP_PERIODIC) then
      if (grid%ie(dir) == grid%GlobalSize(dir)) perOffset(1) = 1
      if (grid%is(dir) == 1                   ) perOffset(2) = 1
    end if

    ! ... Post receive buffers
    if ( grid%cartDims(dir) > 1 ) then
      Call MPI_IRECV(region%deriv%rbuf(1,1), gOffset(1), MPI_DOUBLE_PRECISION, &
           lp, leftTag, grid%cartComm, req(1), ierr)
      Call MPI_IRECV(region%deriv%rbuf(1,2), gOffset(2), MPI_DOUBLE_PRECISION, &
           rp, rightTag, grid%cartComm, req(2), ierr)
    end if

    ! ... fill the send buffer 1
    do i = N-grid%nGhostRHS(dir,2)+1, N
      do j = 1, grid%vds(dir)
        l0 = grid%cell2ghost(grid%vec_ind(j,i-perOffset(1),dir),dir)
        l1 = (j-1)*grid%nGhostRHS(dir,2)+i-(N-grid%nGhostRHS(dir,2))
        region%deriv%sbuf(l1,1) = F(l0)
      end do
    end do

    ! ... fill the send buffer 2
    do i = 1, grid%nGhostRHS(dir,1)
      do j = 1, grid%vds(dir)
        l0 = grid%cell2ghost(grid%vec_ind(j,i+perOffset(2),dir),dir)
        l1 = (j-1)*grid%nGhostRHS(dir,1)+i
        region%deriv%sbuf(l1,2) = F(l0)
      end do
    end do


    ! ... Send data to neighbors
    if ( grid%cartDims(dir) > 1 ) then
      !Write (*,'(A,I6,A,I6,A,I8,A)') '    In Ghost_Cell_Exchange 1, MPI_ISEND from Rank ', region%myrank, ' to ', rp, ' of ', gOffset(2), ' doubles.'
      Call MPI_ISEND(region%deriv%sbuf(1,1), gOffset(2), MPI_DOUBLE_PRECISION, &
         rp, leftTag, grid%cartComm, req(3), ierr)
      !Write (*,'(A,I6,A,I6,A,I8,A)') '    In Ghost_Cell_Exchange 2, MPI_ISEND from Rank ', region%myrank, ' to ', lp, ' of ', gOffset(1), ' doubles.'
      Call MPI_ISEND(region%deriv%sbuf(1,2), gOffset(1), MPI_DOUBLE_PRECISION, &
           lp, rightTag, grid%cartComm, req(4), ierr)
      Call MPI_Waitall(4, req, MPI_STATUSES_IGNORE, ierr)
    else
      do j = 1, 2
        do i = 1, gOffset(j)
          region%deriv%rbuf(i,j) = region%deriv%sbuf(i,j)
        end do
      end do
    end if

    ! ... store the data in the recv buffer 1
    do i = 1, grid%nGhostRHS(dir,1)
      do j = 1, grid%vdsGhost(dir)
        l0 = grid%vec_indGhost(j,i,dir)
        l1 = (j-1)*grid%nGhostRHS(dir,1)+i
        F(l0) = region%deriv%rbuf(l1,1)
      end do
    end do

    ! ... store the data in the recv buffer 2
    do i = Ng-grid%nGhostRHS(dir,2)+1, Ng
      do j = 1, grid%vdsGhost(dir)
        l0 = grid%vec_indGhost(j,i,dir)
        l1 = (j-1)*grid%nGhostRHS(dir,2)+i-(Ng-grid%nGhostRHS(dir,2))
        F(l0) = region%deriv%rbuf(l1,2)
      end do
    end do
  end subroutine Ghost_Cell_Exchange

  Subroutine Ghost_Cell_Exchange_FV(region, F, gridID)

    USE ModGlobal
    USE ModDataStruct

    implicit none

    type (t_region), pointer :: region
    integer :: gridID

    ! ... local variables
    integer :: Nc, gOffset(2), lp, rp, req(4), dir, var, nVar
    integer :: i, j, k, l0, l1, perOffset
    integer :: N(MAX_ND), Ng(MAX_ND)
    type (t_grid), pointer :: grid
    real(rfreal), pointer :: sbuf(:), rbuf(:)
    real(rfreal), pointer :: F(:)
    integer :: leftTag, rightTag, ierr

    leftTag  = gridID * 10
    rightTag = leftTag+1

    ! ... simplicity
    grid       => region%grid(gridID)
    Nc         = grid%nCells

    !    allocate(N(grid%ND), Ng(grid%ND))

    N  = 1
    Ng = 1
    do dir = 1, grid%ND
       N(dir) = grid%ie(dir)-grid%is(dir)+1
       Ng(dir) = N(dir) + SUM(grid%fv%nGhost(dir,:))
    end do

    dir = 1
    gOffset(:) = grid%fv%nGhost(dir,:)*(N(2)*N(3))

    !Call MPI_Barrier(grid%cartComm, IERR)

    ! ... left and right processors
    if ( grid%cartDims(dir) > 1 ) then
       lp = grid%left_process(dir)
       rp = grid%right_process(dir)
    else
       lp = region%myrank
       rp = region%myrank
    end if


    ! ... allocate send and recv buffers
    allocate(sbuf(gOffset(2)))
    allocate(rbuf(gOffset(1)))

    ! ... establish periodic storage index offset for sending data to our right
    perOffset = 0
    if (grid%periodic(dir) >= TRUE .and. grid%periodicStorage == OVERLAP_PERIODIC) then
       if (grid%ie(dir) == grid%GlobalSize(dir)) perOffset = 1
    end if


    ! ... fill the send buffer
    do i = N(1)+grid%fv%nGhost(1,1)-grid%fv%nGhost(1,2) + 1, N(1) + grid%fv%nGhost(1,1)
       do k = grid%fv%nGhost(3,1)+1, N(3) + grid%fv%nGhost(3,1)
          do j = grid%fv%nGhost(2,1)+1, N(2) + grid%fv%nGhost(2,1)
             l0 = (k-1)*Ng(2)*Ng(1) + (j-1)*Ng(1) + i-perOffset;
             l1 = (k-1-grid%fv%nGhost(3,1))*N(2)*grid%fv%nGhost(1,2) + &
                  (j-1-grid%fv%nGhost(2,1))*grid%fv%nGhost(1,2) + &
                  (i-N(1)-grid%fv%nGhost(1,1)+grid%fv%nGhost(1,2))
             sbuf(l1) = F(l0)

          end do
       end do
    end do

    ! ... first exchange from the left
    if ( grid%cartDims(dir) > 1 ) then
       Call MPI_IRECV(rbuf,gOffset(1), MPI_DOUBLE_PRECISION, &
            lp, 0, grid%cartComm, req(1), ierr)
       !Write (*,'(A,I6,A,I6)') '    In Ghost_Cell_Exchange_FV, MPI_ISEND from Rank ', region%myrank, ' to ', rp
       Call MPI_ISEND(sbuf,goffset(2),MPI_DOUBLE_PRECISION, &
            rp, 0, grid%cartComm, req(2), ierr)
       Call MPI_WaitAll(2, req, MPI_STATUSES_IGNORE, ierr)
    else
       if (gOffset(1) /= 0) then
          do i = 1, gOffset(1)
             rbuf(i) = sbuf(i)
          end do
       end if
    end if

    ! ... store the data from the receive buffer
    do i = 1, grid%fv%nGhost(1,1)
       do k = grid%fv%nGhost(3,1)+1, N(3) + grid%fv%nGhost(3,1) 
          do j = grid%fv%nGhost(2,1)+1, N(2) + grid%fv%nGhost(2,1) 
             l0 = (k-1)*Ng(2)*Ng(1) + (j-1)*Ng(1) + i
             l1 = (k-1-grid%fv%nGhost(3,1))*N(2)*grid%fv%nGhost(1,1) + &
                  (j-1-grid%fv%nGhost(2,1))*grid%fv%nGhost(1,1) + i
             F(l0) = rbuf(l1)

          end do
       end do
    end do

    ! ... deallocate
    deallocate(sbuf,rbuf)

    ! ... allocate send and recv buffers
    allocate(sbuf(gOffset(1)))
    allocate(rbuf(gOffset(2)))

    ! ... establish periodic storage index offset for sending data to our left
    perOffset = 0
    if (grid%periodic(dir) >= TRUE .and. grid%periodicStorage == OVERLAP_PERIODIC) then
       if (grid%is(dir) == 1) perOffset = 1
    end if

    ! ... fill the send buffer
    do i = grid%fv%nGhost(1,1)+1, grid%fv%nGhost(1,1)*2
       do k = grid%fv%nGhost(3,1)+1, N(3) + grid%fv%nGhost(3,1)
          do j = grid%fv%nGhost(2,1)+1, N(2) + grid%fv%nGhost(2,1)
             l0 = (k-1)*Ng(2)*Ng(1) + (j-1)*Ng(1) + i + perOffset;
             l1 = (k-1-grid%fv%nGhost(3,1))*N(2)*grid%fv%nGhost(1,1) + &
                  (j-1-grid%fv%nGhost(2,1))*grid%fv%nGhost(1,1) + (i-grid%fv%nGhost(1,1))
             sbuf(l1) = F(l0)
          end do
       end do
    end do

    ! ... then exchange from the right
    if ( grid%cartDims(dir) > 1 ) then
       Call MPI_IRECV(rbuf, gOffset(2), MPI_DOUBLE_PRECISION, &
            rp, 1, grid%cartComm, req(1), ierr)
       Call MPI_ISEND(sbuf, gOffset(1), MPI_DOUBLE_PRECISION, &
            lp, 1, grid%cartComm, req(2), ierr)
       Call MPI_WaitAll(2, req, MPI_STATUSES_IGNORE, ierr)
    else
       if (gOffset(2) /= 0) then
          do i = 1, gOffset(2)
             rbuf(i) = sbuf(i)
          end do
       end if
    end if

    ! ... store the data from the receive buffer
    do i = Ng(1)-grid%fv%nGhost(1,2)+1, Ng(1)
       do k = grid%fv%nGhost(3,1)+1, N(3) + grid%fv%nGhost(3,1) 
          do j = grid%fv%nGhost(2,1)+1, N(2) + grid%fv%nGhost(2,1) 
             l0 = (k-1)*Ng(2)*Ng(1) + (j-1)*Ng(1) + i
             l1 = (k-1-grid%fv%nGhost(3,1))*N(2)*grid%fv%nGhost(1,2) + &
                  (j-1-grid%fv%nGhost(2,1))*grid%fv%nGhost(1,2) + (i-Ng(1)+grid%fv%nGhost(1,2))
             F(l0) = rbuf(l1)
          end do
       end do
    end do

    ! ... deallocate and reallocate
    deallocate(sbuf,rbuf)

    !    end do ! ... var

    ! ... done with first direction, now second direction
    dir = 2
    ! ... now we are transferring ghost data from direction 1 also
    gOffset(:) = grid%fv%nGhost(dir,:)*(Ng(1)*N(3))

    Call MPI_Barrier(grid%cartComm, IERR)

    ! ... left and right processors
    if ( grid%cartDims(dir) > 1 ) then
       lp = grid%left_process(dir)
       rp = grid%right_process(dir)
    else
       lp = region%myrank
       rp = region%myrank
    end if


    !    ! ... do this for every variable
    !    do var = 1, grid%ND

    ! ... allocate send and recv buffers
    allocate(sbuf(gOffset(2)))
    allocate(rbuf(gOffset(1)))

    ! ... establish periodic storage index offset for sending data to our right
    perOffset = 0
    if (grid%periodic(dir) >= TRUE .and. grid%periodicStorage == OVERLAP_PERIODIC) then
       if (grid%ie(dir) == grid%GlobalSize(dir)) perOffset = 1
    end if


    ! ... fill the send buffer
    do j = N(2)+grid%fv%nGhost(2,1)-grid%fv%nGhost(2,2) + 1, N(2) + grid%fv%nGhost(2,1)
       do k = grid%fv%nGhost(3,1)+1, N(3) + grid%fv%nGhost(3,1)
          do i = 1, Ng(1)
             l0 = (k-1)*Ng(2)*Ng(1) + (j-1)*Ng(1) + i - perOffset;
             l1 = (k-1-grid%fv%nGhost(3,1))*Ng(1)*grid%fv%nGhost(2,2) + &
                  (j-N(2)-grid%fv%nGhost(2,1)+grid%fv%nGhost(2,2)-1)*Ng(1) + i
             sbuf(l1) = F(l0)
          end do
       end do
    end do

    ! ... first exchange from the left
    if ( grid%cartDims(dir) > 1 ) then
       Call MPI_IRECV(rbuf,gOffset(1), MPI_DOUBLE_PRECISION, &
            lp, 0, grid%cartComm, req(1), ierr)
       Call MPI_ISEND(sbuf,goffset(2),MPI_DOUBLE_PRECISION, &
            rp, 0, grid%cartComm, req(2), ierr)
       Call MPI_WaitAll(2, req, MPI_STATUSES_IGNORE, ierr)
    else
       if (gOffset(1) /= 0) then
          do i = 1, gOffset(1)
             rbuf(i) = sbuf(i)
          end do
       end if
    end if

    ! ... store the data from the receive buffer
    do j = 1, grid%fv%nGhost(2,1)
       do k = grid%fv%nGhost(3,1)+1, N(3) + grid%fv%nGhost(3,1) 
          do i = 1,Ng(1)
             l0 = (k-1)*Ng(2)*Ng(1) + (j-1)*Ng(1) + i
             l1 = (k-1-grid%fv%nGhost(3,1))*Ng(1)*grid%fv%nGhost(2,1) + &
                  (j-1)*Ng(1) + i
             F(l0) = rbuf(l1)
          end do
       end do
    end do

    ! ... deallocate
    deallocate(sbuf,rbuf)

    ! ... allocate send and recv buffers
    allocate(sbuf(gOffset(1)))
    allocate(rbuf(gOffset(2)))

    ! ... establish periodic storage index offset for sending data to our left
    perOffset = 0
    if (grid%periodic(dir) >= TRUE .and. grid%periodicStorage == OVERLAP_PERIODIC) then
       if (grid%is(dir) == 1) perOffset = 1
    end if

    ! ... fill the send buffer
    do j = grid%fv%nGhost(2,1)+1, grid%fv%nGhost(2,1)*2
       do k = grid%fv%nGhost(3,1)+1, N(3) + grid%fv%nGhost(3,1)
          do i = 1,Ng(1)
             l0 = (k-1)*Ng(2)*Ng(1) + (j-1)*Ng(1) + i + perOffset;
             l1 = (k-1-grid%fv%nGhost(3,1))*Ng(1)*grid%fv%nGhost(2,1) + &
                  (j-1-grid%fv%nGhost(2,1))*Ng(1) + i
             sbuf(l1) = F(l0)
          end do
       end do
    end do

    ! ... then exchange from the right
    if ( grid%cartDims(dir) > 1 ) then
       Call MPI_IRECV(rbuf, gOffset(2), MPI_DOUBLE_PRECISION, &
            rp, 1, grid%cartComm, req(1), ierr)
       Call MPI_ISEND(sbuf, gOffset(1), MPI_DOUBLE_PRECISION, &
            lp, 1, grid%cartComm, req(2), ierr)
       Call MPI_WaitAll(2, req, MPI_STATUSES_IGNORE, ierr)
    else
       if (gOffset(2) /= 0) then
          do i = 1, gOffset(2)
             rbuf(i) = sbuf(i)
          end do
       end if
    end if

    ! ... store the data from the receive buffer
    do j = Ng(2)-grid%fv%nGhost(2,2)+1, Ng(2)
       do k = grid%fv%nGhost(3,1)+1, N(3) + grid%fv%nGhost(3,1) 
          do i = 1, Ng(1)
             l0 = (k-1)*Ng(2)*Ng(1) + (j-1)*Ng(1) + i
             l1 = (k-1-grid%fv%nGhost(3,1))*Ng(1)*grid%fv%nGhost(2,2) + &
                  (j-Ng(2)+grid%fv%nGhost(2,2)-1)*Ng(1) + i
             F(l0) = rbuf(l1)
          end do
       end do
    end do

    ! ... deallocate and reallocate
    deallocate(sbuf,rbuf)

    !    end do ! ... var

    if(grid%ND==3) then

       ! ... done with second direction, now third
       dir = 3
       ! ... now we are transferring ghost data from direction 2 also
       gOffset(:) = grid%fv%nGhost(dir,:)*(Ng(1)*Ng(2))

       Call MPI_Barrier(grid%cartComm, IERR)

       ! ... left and right processors
       if ( grid%cartDims(dir) > 1 ) then
          lp = grid%left_process(dir)
          rp = grid%right_process(dir)
       else
          lp = region%myrank
          rp = region%myrank
       end if


       ! ... do this for every variable
       !       do var = 1, grid%ND

       ! ... allocate send and recv buffers
       allocate(sbuf(gOffset(2)))
       allocate(rbuf(gOffset(1)))

       ! ... establish periodic storage index offset for sending data to our right
       perOffset = 0
       if (grid%periodic(dir) >= TRUE .and. grid%periodicStorage == OVERLAP_PERIODIC) then
          if (grid%ie(dir) == grid%GlobalSize(dir)) perOffset = 1
       end if


       ! ... fill the send buffer
       do k = N(3) + grid%fv%nGhost(3,1)-grid%fv%nGhost(3,2)+1, N(3) + grid%fv%nGhost(3,1)
          do j = 1,Ng(2)
             do i = 1, Ng(1)
                l0 = (k-1)*Ng(2)*Ng(1) + (j-1)*Ng(1) + i - perOffset;
                l1 = (k-N(3)-grid%fv%nGhost(3,1)+grid%fv%nGhost(3,2)-1)*Ng(1)*Ng(2) + &
                     (j-1)*Ng(1) + i
                sbuf(l1) = F(l0)
             end do
          end do
       end do

       ! ... first exchange from the left
       if ( grid%cartDims(dir) > 1 ) then
          Call MPI_IRECV(rbuf,gOffset(1), MPI_DOUBLE_PRECISION, &
               lp, 0, grid%cartComm, req(1), ierr)
          Call MPI_ISEND(sbuf,goffset(2),MPI_DOUBLE_PRECISION, &
               rp, 0, grid%cartComm, req(2), ierr)
          Call MPI_WaitAll(2, req, MPI_STATUSES_IGNORE, ierr)
       else
          if (gOffset(1) /= 0) then
             do i = 1, gOffset(1)
                rbuf(i) = sbuf(i)
             end do
          end if
       end if

       ! ... store the data from the receive buffer
       do k = 1, grid%fv%nGhost(3,1) 
          do j = 1, Ng(2)
             do i = 1,Ng(1)
                l0 = (k-1)*Ng(2)*Ng(1) + (j-1)*Ng(1) + i
                l1 = (k-1)*Ng(1)*Ng(2) + &
                     (j-1)*Ng(1) + i
                F(l0) = rbuf(l1)
             end do
          end do
       end do

       ! ... deallocate
       deallocate(sbuf,rbuf)

       ! ... allocate send and recv buffers
       allocate(sbuf(gOffset(1)))
       allocate(rbuf(gOffset(2)))

       ! ... establish periodic storage index offset for sending data to our left
       perOffset = 0
       if (grid%periodic(dir) >= TRUE .and. grid%periodicStorage == OVERLAP_PERIODIC) then
          if (grid%is(dir) == 1) perOffset = 1
       end if

       ! ... fill the send buffer
       do k = grid%fv%nGhost(3,1)+1, grid%fv%nGhost(3,1)*2
          do j = 1, Ng(2)
             do i = 1, Ng(1)
                l0 = (k-1)*Ng(2)*Ng(1) + (j-1)*Ng(1) + i + perOffset;
                l1 = (k-1-grid%fv%nGhost(3,1))*Ng(1)*Ng(2) + &
                     (j-1)*Ng(1) + i
                sbuf(l1) = F(l0)
             end do
          end do
       end do

       ! ... then exchange from the right
       if ( grid%cartDims(dir) > 1 ) then
          Call MPI_IRECV(rbuf, gOffset(2), MPI_DOUBLE_PRECISION, &
               rp, 1, grid%cartComm, req(1), ierr)
          Call MPI_ISEND(sbuf, gOffset(1), MPI_DOUBLE_PRECISION, &
               lp, 1, grid%cartComm, req(2), ierr)
          Call MPI_WaitAll(2, req, MPI_STATUSES_IGNORE, ierr)
       else
          if (gOffset(2) /= 0) then
             do i = 1, gOffset(2)
                rbuf(i) = sbuf(i)
             end do
          end if
       end if

       ! ... store the data from the receive buffer
       do k = Ng(3) - grid%fv%nGhost(3,2) + 1, Ng(3)
          do j = 1, Ng(2)
             do i = 1, Ng(1)
                l0 = (k-1)*Ng(2)*Ng(1) + (j-1)*Ng(1) + i
                l1 = (k-Ng(3)+grid%fv%nGhost(3,2)-1)*Ng(1)*Ng(2) + &
                     (j-1)*Ng(1) + i
                F(l0) = rbuf(l1)
             end do
          end do
       end do

       ! ... deallocate and reallocate
       deallocate(sbuf,rbuf)

       !       end do ! ... var

    end if

  end Subroutine Ghost_Cell_Exchange_FV

  Subroutine Ghost_Cell_Exchange_patch(region, F, npatch)

    USE ModGlobal
    USE ModDataStruct

    implicit none

    type (t_region), pointer :: region
    real(rfreal), pointer :: F(:)
    integer :: npatch

    ! ... local variables
    integer :: Nc, gOffset(2), lp, rp, req(4), dir, var, nVar, gridID
    integer :: i, j, k, l0, l1, perOffset
    integer :: normDir, req_cnt
    integer :: Np(MAX_ND), Npg(MAX_ND)
    type (t_grid), pointer :: grid
    type(t_patch), pointer :: patch
    real(rfreal), pointer :: sbuf(:), rbuf(:)
    integer :: leftTag, rightTag, ierr
    Integer :: reqsnd(1), reqrcv(1), statsnd(MPI_STATUS_SIZE,1), statrcv(MPI_STATUS_SIZE,1)

    ! ... simplicity
    patch      => region%patch(npatch)
    gridID     =  patch%gridID
    grid       => region%grid(gridID)
    normDir    =  abs(patch%normDir)
    Nc         =  grid%nCells

    !    allocate(N(grid%ND), Ng(grid%ND))

    Np  = 1
    Npg = 1
    ! ... each patch will "grow" to the right
    do dir = 1, grid%ND
       Np(dir) = patch%ie(dir)-patch%is(dir)+1
       Npg(dir) = Np(dir) + patch%SurfIntCommAmt(dir,2)
    end do

    if(normDir /= 1) then
       ! ... communicate information in direction 1
       dir = 1
       gOffset(:) = patch%SurfIntCommAmt(dir,:)*(Np(2)*Np(3))

       ! ... pass info to the left?
       lp = region%myrank
       if(patch%SurfIntCommAmt(dir,1) == 1) lp = grid%left_process(dir)

       ! ... receive info from the right?
       rp = region%myrank
       if(patch%SurfIntCommAmt(dir,2) == 1) rp = grid%right_process(dir)
       
       ! ... allocate communicate buffers
       allocate(sbuf(goffset(1)), rbuf(goffset(2)))

       ! ... fill send buffer
       do i = 1, patch%SurfIntCommAmt(dir,1)
          do j = 1, Np(2)
             do k = 1, Np(3)
                l0 = (k-1)*Npg(2)*Npg(1) + (j-1)*Npg(1) + i
                ! ... l1 = (k-1)*Np(2)*patch%SurfIntCommAmt(dir,1) + (j-1)*patch%SurfIntCommAmt(dir,1) + 1
                ! ... below is simple and equivalent
                l1 = (k-1) + (j-1) + 1
                sbuf(l1) = F(l0)
             end do
          end do
       end do
   
       ! ... get info from right
       if(patch%SurfIntCommAmt(dir,2) == 1) then
          Call MPI_IRECV(rbuf, gOffset(2), MPI_DOUBLE_PRECISION, &
            rp, 1, grid%cartComm, reqrcv, ierr)
       end if

       ! ... send info to the left
       if(patch%SurfIntCommAmt(dir,1) == 1) then
          Call MPI_ISEND(sbuf, gOffset(1), MPI_DOUBLE_PRECISION, &
            lp, 1, grid%cartComm, reqsnd, ierr)
       end if
       
       if(patch%SurfIntCommAmt(dir,2) == 1) Call MPI_Wait(reqrcv, statrcv, ierr)
       if(patch%SurfIntCommAmt(dir,1) == 1) Call MPI_Wait(reqsnd, statsnd, ierr)

       ! ... store info from the receive buffer
       do i = Npg(1) - patch%SurfIntCommAmt(dir,2) + 1, Npg(1)
          do j = 1, Np(2)
             do k = 1, Np(3)
                l0 = (k-1)*Npg(2)*Npg(1) + (j-1)*Npg(1) + i
                ! ... l1 = (k-1)*Np(2)*patch%SurfIntCommAmt(dir,2) + (j-1)*patch%SurfIntCommAmt(dir,2) + 1
                ! ... below is simple and equivalent
                l1 = (k-1) + (j-1) + 1
                F(l0) = rbuf(l1)
             end do
          end do
       end do
       
       deallocate(rbuf,sbuf)
       
    end if

    ! ... now, direction 2
    if(normDir /= 2) then
       dir = 2
       gOffset(:) = patch%SurfIntCommAmt(dir,:)*(Npg(1)*Np(3))

       ! ... pass info to the left?
       lp = region%myrank
       if(patch%SurfIntCommAmt(dir,1) == 1) lp = grid%left_process(dir)

       ! ... receive info from the right?
       rp = region%myrank
       if(patch%SurfIntCommAmt(dir,2) == 1) rp = grid%right_process(dir)
       
       ! ... allocate communicate buffers
       allocate(sbuf(goffset(1)), rbuf(goffset(2)))

       ! ... fill send buffer
       do j = 1, patch%SurfIntCommAmt(dir,1)
          do i = 1, Npg(1)
             do k = 1, Np(3)
                l0 = (k-1)*Npg(2)*Npg(1) + (j-1)*Npg(1) + i
                ! ... l1 = (k-1)*Npg(1)*patch%SurfIntCommAmt(dir,1) + (j-1)*Npg(1) + i
                ! ... below is simple and equivalent
                l1 = (k-1) + i
                sbuf(l1) = F(l0)
             end do
          end do
       end do
    
       ! ... get info from right
       if(patch%SurfIntCommAmt(dir,2) == 1) then
          Call MPI_IRECV(rbuf, gOffset(2), MPI_DOUBLE_PRECISION, &
            rp, 1, grid%cartComm, reqrcv, ierr)
       end if

       ! ... send info to the left
       if(patch%SurfIntCommAmt(dir,1) == 1) then
          Call MPI_ISEND(sbuf, gOffset(1), MPI_DOUBLE_PRECISION, &
            lp, 1, grid%cartComm, reqsnd, ierr)
       end if
       
       
       if(patch%SurfIntCommAmt(dir,2) == 1) Call MPI_Wait(reqrcv, statrcv, ierr)
       if(patch%SurfIntCommAmt(dir,1) == 1) Call MPI_Wait(reqsnd, statsnd, ierr)

       ! ... store info from the receive buffer
       do j = Npg(2) - patch%SurfIntCommAmt(dir,2) + 1, Npg(2)
          do i = 1, Npg(1)
             do k = 1, Np(3)
                l0 = (k-1)*Npg(2)*Npg(1) + (j-1)*Npg(1) + i
                ! ... l1 = (k-1)*Npg(1)*patch%SurfIntCommAmt(dir,2) + (j-1)*Npg(1) + i
                ! ... below is simple and equivalent
                l1 = (k-1) + i
                F(l0) = rbuf(l1)
             end do
          end do
       end do
       
       deallocate(rbuf,sbuf)

    end if

    ! ... now, direction 3
    if(normDir /= 3 .and. grid%ND == 3) then
       dir = 3
       gOffset(:) = patch%SurfIntCommAmt(dir,:)*(Npg(1)*Npg(2))

       ! ... pass info to the left?
       !lp = region%myrank
       if(patch%SurfIntCommAmt(dir,1) == 1) lp = grid%left_process(dir)

       ! ... receive info from the right?
       rp = region%myrank
       if(patch%SurfIntCommAmt(dir,2) == 1) rp = grid%right_process(dir)
       
       ! ... allocate communicate buffers
       allocate(sbuf(goffset(1)), rbuf(goffset(2)))

       ! ... fill send buffer
       do k = 1, patch%SurfIntCommAmt(dir,1)
          do i = 1, Npg(1)
             do j = 1, Npg(2)
                l0 = (k-1)*Npg(2)*Npg(1) + (j-1)*Npg(1) + i
                ! ... l1 = (k-1)*Npg(1)*Npg(2) + (j-1)*Npg(1) + i
                ! ... below is simple and equivalent
                l1 = (j-1) + i
                sbuf(l1) = F(l0)
             end do
          end do
       end do


       ! ... get info from right
       if(patch%SurfIntCommAmt(dir,2) == 1) then
          Call MPI_IRECV(rbuf, gOffset(2), MPI_DOUBLE_PRECISION, &
            rp, 1, grid%cartComm, reqrcv, ierr)
       end if

       ! ... send info to the left
       if(patch%SurfIntCommAmt(dir,1) == 1) then
          Call MPI_ISEND(sbuf, gOffset(1), MPI_DOUBLE_PRECISION, &
            lp, 1, grid%cartComm, reqsnd, ierr)
       end if
       
       if(patch%SurfIntCommAmt(dir,2) == 1) Call MPI_Wait(reqrcv, statrcv, ierr)
       if(patch%SurfIntCommAmt(dir,1) == 1) Call MPI_Wait(reqsnd, statsnd, ierr)

       ! ... store info from the receive buffer
       do k = Npg(3) - patch%SurfIntCommAmt(dir,2) + 1, Npg(3)
          do i = 1, Npg(1)
             do j = 1, Npg(2)
                l0 = (k-1)*Npg(2)*Npg(1) + (j-1)*Npg(1) + i
                ! ... l1 = (k-Npg(3) + patch%SurfIntCommAmt(dir,2) - 1)*Npg(1)*Npg(2) + (j-1)*Npg(1) + i
                ! ... below is simple and equivalent
                l1 = (j-1) + i
                F(l0) = rbuf(l1)
             end do
          end do
       end do
       
       deallocate(rbuf,sbuf)

    end if
    
  end Subroutine Ghost_Cell_Exchange_patch

  SUBROUTINE InitialHaloExchange(region)

    USE ModGlobal
    USE ModDataStruct

    IMPLICIT NONE

    Type (t_region), pointer :: region

    ! ... local variables
    INTEGER :: ng,i
    Type (t_mixt_input), pointer :: input
    Type (t_grid), pointer :: grid
    real(rfreal), pointer :: ptr_to_data(:)
    real(rfreal), pointer :: iblank_dble(:)

    Do ng = 1, region%nGrids
       
       grid  => region%grid(ng)
       input => grid%input
       
       ! ... xyz
       do i = 1, size(grid%XYZ,2)
          ptr_to_data => grid%XYZ(:,i)
          Call Ghost_Cell_Exchange_Box(region, ng, ptr_to_data)
       end do
       
       ! ... iblank
       allocate(iblank_dble(size(grid%iblank,1)))
       iblank_dble(:) = dble(grid%iblank(:))
       ptr_to_data => iblank_dble
       Call Ghost_Cell_Exchange_Box(region, ng, ptr_to_data)
       grid%iblank(:) = int(iblank_dble(:))
       deallocate(iblank_dble)
       
       ! ... Define ibfac
       Do i = 1, grid%nCells
          grid%ibfac(i) = dble(min(1,abs(grid%iblank(i))))
       End Do
       
    End do

  END SUBROUTINE InitialHaloExchange

  subroutine assign_local_indices_slab(ND, grid)

    USE ModGlobal
    USE ModDataStruct

    type(t_grid), pointer :: grid
    integer, pointer :: ND(:,:)

    ! ... local vars
    integer :: i, is, ie
    
    ! ... if parDir == FALSE, we are single processor (Chimera is handled in ModRegion)
    do i = 1, grid%ND
      grid%is(i) = 1
      grid%ie(i) = ND(grid%iGridGlobal,i)
    end do
    if (grid%parDir == FALSE) return

    ! ... else, decompose in 1-d slices
    call MPE_Decomp1D(ND(grid%iGridGlobal,grid%parDir), grid%numproc_inComm, grid%myrank_inComm, &
         grid%is(grid%parDir), grid%ie(grid%parDir))

  end subroutine assign_local_indices_slab

  subroutine assign_local_indices_cube(myrank, ND, grid, input, ng)

    USE ModGlobal
    USE ModDataStruct

    integer :: myrank
    type(t_grid), pointer :: grid
    integer, pointer :: ND(:,:)
    type(t_mixt_input), pointer :: input
    integer :: ng

    ! ... local vars
    integer :: i, is, ie, ndims, dir
    logical :: isperiodic(MAX_ND), reorder, dims(MAX_ND)
    integer :: indGrid, numDecomp(MAX_ND), io_err, ierr
    logical :: mf_exists


    ! ... initialize
    do i = 1, grid%ND
      grid%is(i) = 1
      grid%ie(i) = ND(grid%iGridGlobal,i)
    end do

    ! ... use MPI_DIMS_CREATE to determine the decomposition
    grid%cartDims(1:grid%ND) = 0

    ! ... if slab (1-D) we parallize in direction parDir
    if (grid%parallelTopology == SLAB .and. grid%numproc_inComm > 1) then
      grid%cartDims(grid%parDir) = grid%numproc_inComm

    ! ... if cube (N-D) check for decomposition file
    else if (grid%parallelTopology == CUBE .and. grid%numproc_inComm > 1) then
      ! ... read domain-decomposition map
      ! ... if error occurs while reading, we do nothing, haha! -_-a
      mf_exists = .FALSE.
      if (input%useDecompMap == TRUE) inquire(file=trim(input%decompMap), exist=mf_exists)
      if (mf_exists) then
        open(66, file=trim(input%decompMap)); io_err = 0
        Do while (io_err == 0) 
          read(66,*,IOSTAT=io_err) indGrid, numDecomp(1:MAX_ND)
          if (indGrid == grid%iGridGlobal) grid%cartDims(:) = numDecomp(:)
        end do ! i
        close(66)
      end if ! mf_exists
    end if ! grid%parallelTopology
    
    if (input%useLocalDecomp == TRUE) then
      call graceful_exit(myrank, 'ERROR: LOCAL_DIMS_CREATE not yet ready.  Please unset LOCAL_DECOMP.')
      call LOCAL_DIMS_CREATE(myrank, grid%numproc_inComm, grid%ND, ND, grid%iGridGlobal, grid%cartDims)
      if (grid%myrank_inComm == 0) print *, '>>>> ', grid%numproc_inComm, grid%cartDims, ' <<<<'
      call mpi_barrier(mycomm, ierr)
    end if

    ! ... decompose
    call MPI_DIMS_CREATE(grid%numproc_inComm, grid%ND, grid%cartDims, ierr)

    ! ... initialize data for MPI_CART_CREATE
    isperiodic(:) = .FALSE.
    do dir = 1, grid%ND
      if (grid%periodic(dir) /= FALSE) isperiodic(dir) = .TRUE.
    end do

!   if (input%fv_grid(ng) == FINITE_DIF) then
!     do dir = 1, grid%ND
!       if (grid%periodic(dir) /= FALSE) isperiodic(dir) = .TRUE.
!     end do
!   else if (input%fv_grid(ng) == FINITE_VOL) then
!     do dir = 1, grid%ND
!       if (grid%fv%periodic(dir) /= FALSE) isperiodic(dir) = .TRUE.
!     end do
!   end if

    reorder = .TRUE.

    ! ... use MPI_CART_CREATE to get the cell decomposition
    Call MPI_CART_CREATE(grid%comm, grid%ND, grid%cartDims(1:grid%ND), isperiodic(1:grid%ND), reorder, grid%cartComm, ierr)

    ! ... retain basic information about Deriv communicators
    Call MPI_Comm_size(grid%cartComm, grid%numproc_inCartComm, ierr)
    Call MPI_Comm_rank(grid%cartComm, grid%myrank_inCartComm, ierr)
    Call MPI_Cart_Coords(grid%cartComm, grid%myrank_inCartComm, grid%ND, grid%cartCoords, ierr)

    ! ... loop over the directions to get neighbors
    do dir = 1, grid%ND
      Call MPI_Cart_Shift(grid%cartComm, dir-1, 1, grid%left_process(dir), grid%right_process(dir), ierr)
    end do

    ! ... now loop over the directions to get our size
    do dir = 1, grid%ND
      if (grid%cartDims(dir) > 1) call MPE_Decomp1D(ND(grid%iGridGlobal,dir), grid%cartDims(dir), grid%cartCoords(dir), grid%is(dir), grid%ie(dir))
    end do

    ! ... determine pencil communicators
    ! ... pencils in 1-dir
    dir = 1
    dims = (/ .true., .false., .false. /)
    Call MPI_CART_Sub(grid%cartComm, dims, grid%pencilComm(dir), ierr)
    Call MPI_Comm_Size(grid%pencilComm(dir), grid%numproc_inPencilComm(dir), ierr)
    Call MPI_Comm_Rank(grid%pencilComm(dir), grid%myrank_inPencilComm(dir), ierr)
    ! print *, myrank, grid%is(1), grid%is(2)
    ! write (*,'(5(I4,1X))') myrank, grid%myrank_inPencilComm(dir), grid%numproc_inPencilComm(dir), grid%is(1), grid%is(2)
    ! call Graceful_Exit(myrank, 'DEBUG.')

    ! ... pencils in 2-dir
    if (grid%ND >= 2) then
      dir = 2
      dims = (/ .false., .true., .false. /)
      Call MPI_CART_Sub(grid%cartComm, dims, grid%pencilComm(dir), ierr)
      Call MPI_Comm_Size(grid%pencilComm(dir), grid%numproc_inPencilComm(dir), ierr)
      Call MPI_Comm_Rank(grid%pencilComm(dir), grid%myrank_inPencilComm(dir), ierr)
    end if

    ! ... pencils in 3-dir
    if (grid%ND == 3) then
      dir = 3
      dims = (/ .false., .false., .true. /)
      Call MPI_CART_Sub(grid%cartComm, dims, grid%pencilComm(dir), ierr)
      Call MPI_Comm_Size(grid%pencilComm(dir), grid%numproc_inPencilComm(dir), ierr)
      Call MPI_Comm_Rank(grid%pencilComm(dir), grid%myrank_inPencilComm(dir), ierr)
    end if

  end subroutine assign_local_indices_cube

  subroutine assign_local_indices_cube_box(myrank, ND, grid, input, ng)

    USE ModGlobal
    USE ModDataStruct

    integer :: myrank
    type(t_grid), pointer :: grid
    integer, pointer :: ND(:,:)
    type(t_mixt_input), pointer :: input
    integer :: ng

    ! ... local vars
    integer :: i, is, ie, ndims, dir
    integer :: ierr
    logical :: isperiodic(MAX_ND), reorder, dims(MAX_ND)
    integer :: indGrid, numDecomp(MAX_ND), io_err
    logical :: mf_exists
    integer :: lefttag, righttag, req(4)


    ! ... initialize
    do i = 1, grid%ND
      grid%is(i) = 1
      grid%ie(i) = ND(grid%iGridGlobal,i)
    end do

    ! ... use MPI_DIMS_CREATE to determine the decomposition
    grid%cartDims(1:grid%ND) = 0

    ! ... if slab (1-D) we parallize in direction parDir
    if (grid%parallelTopology == SLAB .and. grid%numproc_inComm > 1) then
      grid%cartDims(grid%parDir) = grid%numproc_inComm

    ! ... if cube (N-D) check for decomposition file
    else if (grid%parallelTopology == CUBE .and. grid%numproc_inComm > 1) then
      ! ... read domain-decomposition map
      ! ... if error occurs while reading, we do nothing, haha! -_-a
      mf_exists = .FALSE.
      if (input%useDecompMap == TRUE) inquire(file=trim(input%decompMap), exist=mf_exists)
      if (mf_exists) then
        open(66, file=trim(input%decompMap)); io_err = 0
        Do while (io_err == 0) 
          read(66,*,IOSTAT=io_err) indGrid, numDecomp(1:MAX_ND)
          if (indGrid == grid%iGridGlobal) grid%cartDims(:) = numDecomp(:)
        end do ! i
        close(66)
      end if ! mf_exists
    end if ! grid%parallelTopology
    
    if (input%useLocalDecomp == TRUE) then
      call graceful_exit(myrank, 'ERROR: LOCAL_DIMS_CREATE not yet ready.  Please unset LOCAL_DECOMP.')
      call LOCAL_DIMS_CREATE(myrank, grid%numproc_inComm, grid%ND, ND, grid%iGridGlobal, grid%cartDims)
      if (grid%myrank_inComm == 0) print *, '>>>> ', grid%numproc_inComm, grid%cartDims, ' <<<<'
      call mpi_barrier(mycomm, ierr)
    end if

    ! ... decompose
    call MPI_DIMS_CREATE(grid%numproc_inComm, grid%ND, grid%cartDims, ierr)

    ! ... initialize data for MPI_CART_CREATE
    isperiodic(:) = .FALSE.
    do dir = 1, grid%ND
      if (grid%periodic(dir) /= FALSE) isperiodic(dir) = .TRUE.
    end do

!   if (input%fv_grid(ng) == FINITE_DIF) then
!     do dir = 1, grid%ND
!       if (grid%periodic(dir) /= FALSE) isperiodic(dir) = .TRUE.
!     end do
!   else if (input%fv_grid(ng) == FINITE_VOL) then
!     do dir = 1, grid%ND
!       if (grid%fv%periodic(dir) /= FALSE) isperiodic(dir) = .TRUE.
!     end do
!   end if

    reorder = .TRUE.

    ! ... use MPI_CART_CREATE to get the cell decomposition
    Call MPI_CART_CREATE(grid%comm, grid%ND, grid%cartDims(1:grid%ND), isperiodic(1:grid%ND), reorder, grid%cartComm, ierr)

    ! ... retain basic information about Deriv communicators
    Call MPI_Comm_size(grid%cartComm, grid%numproc_inCartComm, ierr)
    Call MPI_Comm_rank(grid%cartComm, grid%myrank_inCartComm, ierr)
    Call MPI_Cart_Coords(grid%cartComm, grid%myrank_inCartComm, grid%ND, grid%cartCoords, ierr)

    ! ... loop over the directions to get neighbors
    do dir = 1, grid%ND
      Call MPI_Cart_Shift(grid%cartComm, dir-1, 1, grid%left_process(dir), grid%right_process(dir), ierr)
    end do

    ! ... find the 26 neighbors for box shape ghost cell exchange, WZhang 09/2014
    lefttag  = grid%ND*10
    righttag = lefttag + 1
    ! ... initialize all of them to MPI_PROC_NULL
    grid%box_nbs(:) = MPI_PROC_NULL

    ! ... 1D
    if (grid%ND == 1) then
      if (grid%cartDims(1) > 1 .or. isperiodic(1)) then
        Call MPI_Cart_Shift(grid%cartComm, 0, 1, grid%box_nbs(PROC_W),  grid%box_nbs(PROC_E), ierr)
      end if
    end if

    ! ... 2D
    if (grid%ND == 2) then

      if (grid%CartDims(1) > 1 .or. isperiodic(1)) then
        Call MPI_Cart_Shift(grid%cartComm, 0, 1, grid%box_nbs(PROC_W), grid%box_nbs(PROC_E), ierr)
      end if
      if (grid%CartDims(2) > 1 .or. isperiodic(2)) then
        Call MPI_Cart_Shift(grid%cartComm, 1, 1, grid%box_nbs(PROC_S), grid%box_nbs(PROC_N), ierr)
      end if

      if ((grid%CartDims(1) > 1 .or. isperiodic(1)) .and. (grid%CartDims(2) > 1 .or. isperiodic(2))) then
        Call MPI_Irecv(grid%box_nbs(PROC_SW), 1, MPI_INTEGER, grid%box_nbs(PROC_S),  lefttag, grid%cartComm, req(1), ierr)
        Call MPI_Irecv(grid%box_nbs(PROC_NW), 1, MPI_INTEGER, grid%box_nbs(PROC_N), righttag, grid%cartComm, req(2), ierr)
        Call MPI_Isend( grid%box_nbs(PROC_W), 1, MPI_INTEGER, grid%box_nbs(PROC_N),  lefttag, grid%cartComm, req(3), ierr)
        Call MPI_Isend( grid%box_nbs(PROC_W), 1, MPI_INTEGER, grid%box_nbs(PROC_S), righttag, grid%cartComm, req(4), ierr)
        Call MPI_Waitall(4, req, MPI_STATUSES_IGNORE, ierr)

        Call MPI_Irecv(grid%box_nbs(PROC_SE), 1, MPI_INTEGER, grid%box_nbs(PROC_S),  lefttag, grid%cartComm, req(1), ierr)
        Call MPI_Irecv(grid%box_nbs(PROC_NE), 1, MPI_INTEGER, grid%box_nbs(PROC_N), righttag, grid%cartComm, req(2), ierr)
        Call MPI_Isend( grid%box_nbs(PROC_E), 1, MPI_INTEGER, grid%box_nbs(PROC_N),  lefttag, grid%cartComm, req(3), ierr)
        Call MPI_Isend( grid%box_nbs(PROC_E), 1, MPI_INTEGER, grid%box_nbs(PROC_S), righttag, grid%cartComm, req(4), ierr)
        Call MPI_Waitall(4, req, MPI_STATUSES_IGNORE, ierr)
      end if

    end if

    ! ... 3D
    if (grid%ND == 3) then

      if (grid%CartDims(1) > 1 .or. isperiodic(1)) then
        Call MPI_Cart_Shift(grid%cartComm, 0, 1, grid%box_nbs(PROC_W), grid%box_nbs(PROC_E), ierr)
      end if
      if (grid%CartDims(2) > 1 .or. isperiodic(2)) then
        Call MPI_Cart_Shift(grid%cartComm, 1, 1, grid%box_nbs(PROC_S), grid%box_nbs(PROC_N), ierr)
      end if
      if (grid%CartDims(3) > 1 .or. isperiodic(3)) then
        Call MPI_Cart_Shift(grid%cartComm, 2, 1, grid%box_nbs(PROC_D), grid%box_nbs(PROC_U), ierr)
      end if

      if ((grid%CartDims(1) > 1 .or. isperiodic(1)) .and. (grid%CartDims(2) > 1 .or. isperiodic(2))) then
        Call MPI_Irecv(grid%box_nbs(PROC_SW), 1, MPI_INTEGER, grid%box_nbs(PROC_S),  lefttag, grid%cartComm, req(1), ierr)
        Call MPI_Irecv(grid%box_nbs(PROC_NW), 1, MPI_INTEGER, grid%box_nbs(PROC_N), righttag, grid%cartComm, req(2), ierr)
        Call MPI_Isend( grid%box_nbs(PROC_W), 1, MPI_INTEGER, grid%box_nbs(PROC_N),  lefttag, grid%cartComm, req(3), ierr)
        Call MPI_Isend( grid%box_nbs(PROC_W), 1, MPI_INTEGER, grid%box_nbs(PROC_S), righttag, grid%cartComm, req(4), ierr)
        Call MPI_Waitall(4, req, MPI_STATUSES_IGNORE, ierr)

        Call MPI_Irecv(grid%box_nbs(PROC_SE), 1, MPI_INTEGER, grid%box_nbs(PROC_S),  lefttag, grid%cartComm, req(1), ierr)
        Call MPI_Irecv(grid%box_nbs(PROC_NE), 1, MPI_INTEGER, grid%box_nbs(PROC_N), righttag, grid%cartComm, req(2), ierr)
        Call MPI_Isend( grid%box_nbs(PROC_E), 1, MPI_INTEGER, grid%box_nbs(PROC_N),  lefttag, grid%cartComm, req(3), ierr)
        Call MPI_Isend( grid%box_nbs(PROC_E), 1, MPI_INTEGER, grid%box_nbs(PROC_S), righttag, grid%cartComm, req(4), ierr)
        Call MPI_Waitall(4, req, MPI_STATUSES_IGNORE, ierr)
      end if

      if ((grid%CartDims(1) > 1 .or. isperiodic(1)) .and. (grid%CartDims(3) > 1 .or. isperiodic(3))) then
        Call MPI_Irecv(grid%box_nbs(PROC_D_W), 1, MPI_INTEGER, grid%box_nbs(PROC_D),  lefttag, grid%cartComm, req(1), ierr)
        Call MPI_Irecv(grid%box_nbs(PROC_U_W), 1, MPI_INTEGER, grid%box_nbs(PROC_U), righttag, grid%cartComm, req(2), ierr)
        Call MPI_Isend(  grid%box_nbs(PROC_W), 1, MPI_INTEGER, grid%box_nbs(PROC_U),  lefttag, grid%cartComm, req(3), ierr)
        Call MPI_Isend(  grid%box_nbs(PROC_W), 1, MPI_INTEGER, grid%box_nbs(PROC_D), righttag, grid%cartComm, req(4), ierr)
        Call MPI_Waitall(4, req, MPI_STATUSES_IGNORE, ierr)

        Call MPI_Irecv(grid%box_nbs(PROC_D_E), 1, MPI_INTEGER, grid%box_nbs(PROC_D),  lefttag, grid%cartComm, req(1), ierr)
        Call MPI_Irecv(grid%box_nbs(PROC_U_E), 1, MPI_INTEGER, grid%box_nbs(PROC_U), righttag, grid%cartComm, req(2), ierr)
        Call MPI_Isend(  grid%box_nbs(PROC_E), 1, MPI_INTEGER, grid%box_nbs(PROC_U),  lefttag, grid%cartComm, req(3), ierr)
        Call MPI_Isend(  grid%box_nbs(PROC_E), 1, MPI_INTEGER, grid%box_nbs(PROC_D), righttag, grid%cartComm, req(4), ierr)
        Call MPI_Waitall(4, req, MPI_STATUSES_IGNORE, ierr)
      end if

      if ((grid%CartDims(2) > 1 .or. isperiodic(2)) .and. (grid%CartDims(3) > 1 .or. isperiodic(3))) then
        Call MPI_Irecv(grid%box_nbs(PROC_D_S), 1, MPI_INTEGER, grid%box_nbs(PROC_D),  lefttag, grid%cartComm, req(1), ierr)
        Call MPI_Irecv(grid%box_nbs(PROC_U_S), 1, MPI_INTEGER, grid%box_nbs(PROC_U), righttag, grid%cartComm, req(2), ierr)
        Call MPI_Isend(  grid%box_nbs(PROC_S), 1, MPI_INTEGER, grid%box_nbs(PROC_U),  lefttag, grid%cartComm, req(3), ierr)
        Call MPI_Isend(  grid%box_nbs(PROC_S), 1, MPI_INTEGER, grid%box_nbs(PROC_D), righttag, grid%cartComm, req(4), ierr)
        Call MPI_Waitall(4, req, MPI_STATUSES_IGNORE, ierr)

        Call MPI_Irecv(grid%box_nbs(PROC_D_N), 1, MPI_INTEGER, grid%box_nbs(PROC_D),  lefttag, grid%cartComm, req(1), ierr)
        Call MPI_Irecv(grid%box_nbs(PROC_U_N), 1, MPI_INTEGER, grid%box_nbs(PROC_U), righttag, grid%cartComm, req(2), ierr)
        Call MPI_Isend(  grid%box_nbs(PROC_N), 1, MPI_INTEGER, grid%box_nbs(PROC_U),  lefttag, grid%cartComm, req(3), ierr)
        Call MPI_Isend(  grid%box_nbs(PROC_N), 1, MPI_INTEGER, grid%box_nbs(PROC_D), righttag, grid%cartComm, req(4), ierr)
        Call MPI_Waitall(4, req, MPI_STATUSES_IGNORE, ierr)
      end if

      if ((grid%CartDims(1) > 1 .or. isperiodic(1)) .and. (grid%CartDims(2) > 1 .or. isperiodic(2))&
           .and. (grid%CartDims(3) > 1 .or. isperiodic(3))) then
        Call MPI_Irecv(grid%box_nbs(PROC_D_SW), 1, MPI_INTEGER, grid%box_nbs(PROC_D),  lefttag, grid%cartComm, req(1), ierr)
        Call MPI_Irecv(grid%box_nbs(PROC_U_SW), 1, MPI_INTEGER, grid%box_nbs(PROC_U), righttag, grid%cartComm, req(2), ierr)
        Call MPI_Isend(  grid%box_nbs(PROC_SW), 1, MPI_INTEGER, grid%box_nbs(PROC_U),  lefttag, grid%cartComm, req(3), ierr)
        Call MPI_Isend(  grid%box_nbs(PROC_SW), 1, MPI_INTEGER, grid%box_nbs(PROC_D), righttag, grid%cartComm, req(4), ierr)
        Call MPI_Waitall(4, req, MPI_STATUSES_IGNORE, ierr)

        Call MPI_Irecv(grid%box_nbs(PROC_D_NW), 1, MPI_INTEGER, grid%box_nbs(PROC_D),  lefttag, grid%cartComm, req(1), ierr)
        Call MPI_Irecv(grid%box_nbs(PROC_U_NW), 1, MPI_INTEGER, grid%box_nbs(PROC_U), righttag, grid%cartComm, req(2), ierr)
        Call MPI_Isend(  grid%box_nbs(PROC_NW), 1, MPI_INTEGER, grid%box_nbs(PROC_U),  lefttag, grid%cartComm, req(3), ierr)
        Call MPI_Isend(  grid%box_nbs(PROC_NW), 1, MPI_INTEGER, grid%box_nbs(PROC_D), righttag, grid%cartComm, req(4), ierr)
        Call MPI_Waitall(4, req, MPI_STATUSES_IGNORE, ierr)

        Call MPI_Irecv(grid%box_nbs(PROC_D_SE), 1, MPI_INTEGER, grid%box_nbs(PROC_D),  lefttag, grid%cartComm, req(1), ierr)
        Call MPI_Irecv(grid%box_nbs(PROC_U_SE), 1, MPI_INTEGER, grid%box_nbs(PROC_U), righttag, grid%cartComm, req(2), ierr)
        Call MPI_Isend(  grid%box_nbs(PROC_SE), 1, MPI_INTEGER, grid%box_nbs(PROC_U),  lefttag, grid%cartComm, req(3), ierr)
        Call MPI_Isend(  grid%box_nbs(PROC_SE), 1, MPI_INTEGER, grid%box_nbs(PROC_D), righttag, grid%cartComm, req(4), ierr)
        Call MPI_Waitall(4, req, MPI_STATUSES_IGNORE, ierr)

        Call MPI_Irecv(grid%box_nbs(PROC_D_NE), 1, MPI_INTEGER, grid%box_nbs(PROC_D),  lefttag, grid%cartComm, req(1), ierr)
        Call MPI_Irecv(grid%box_nbs(PROC_U_NE), 1, MPI_INTEGER, grid%box_nbs(PROC_U), righttag, grid%cartComm, req(2), ierr)
        Call MPI_Isend(  grid%box_nbs(PROC_NE), 1, MPI_INTEGER, grid%box_nbs(PROC_U),  lefttag, grid%cartComm, req(3), ierr)
        Call MPI_Isend(  grid%box_nbs(PROC_NE), 1, MPI_INTEGER, grid%box_nbs(PROC_D), righttag, grid%cartComm, req(4), ierr)
        Call MPI_Waitall(4, req, MPI_STATUSES_IGNORE, ierr)
      end if

    end if

    ! ... now loop over the directions to get our size
    do dir = 1, grid%ND
      if (grid%cartDims(dir) > 1) call MPE_Decomp1D(ND(grid%iGridGlobal,dir), grid%cartDims(dir), grid%cartCoords(dir), grid%is(dir), grid%ie(dir))
    end do

!    if (myrank == 0) then
!      write(*,*) 'isperiodic(:)=',isperiodic(:)
!    end if
!    write(*,'(A,I3,26I3)') 'b_myrank=', myrank, grid%box_nbs(:)
!    write(*,'(A,I3,13I3)') 'b_myrank=', myrank, grid%box_nbs(13:26)
!    write(*,'(A,I3,6I4)') 'b_myrank=', myrank, grid%is(1), grid%ie(1), grid%is(2), grid%ie(2), grid%is(3), grid%ie(3)
!    call Graceful_Exit('DEBUG.')

    ! ... determine pencil communicators
    ! ... pencils in 1-dir
    dir = 1
    dims = (/ .true., .false., .false. /)
    Call MPI_CART_Sub(grid%cartComm, dims, grid%pencilComm(dir), ierr)
    Call MPI_Comm_Size(grid%pencilComm(dir), grid%numproc_inPencilComm(dir), ierr)
    Call MPI_Comm_Rank(grid%pencilComm(dir), grid%myrank_inPencilComm(dir), ierr)
    ! print *, myrank, grid%is(1), grid%is(2)
    ! write (*,'(5(I4,1X))') myrank, grid%myrank_inPencilComm(dir), grid%numproc_inPencilComm(dir), grid%is(1), grid%is(2)
    ! call Graceful_Exit('DEBUG.')

    ! ... pencils in 2-dir
    if (grid%ND >= 2) then
      dir = 2
      dims = (/ .false., .true., .false. /)
      Call MPI_CART_Sub(grid%cartComm, dims, grid%pencilComm(dir), ierr)
      Call MPI_Comm_Size(grid%pencilComm(dir), grid%numproc_inPencilComm(dir), ierr)
      Call MPI_Comm_Rank(grid%pencilComm(dir), grid%myrank_inPencilComm(dir), ierr)
    end if

    ! ... pencils in 3-dir
    if (grid%ND == 3) then
      dir = 3
      dims = (/ .false., .false., .true. /)
      Call MPI_CART_Sub(grid%cartComm, dims, grid%pencilComm(dir), ierr)
      Call MPI_Comm_Size(grid%pencilComm(dir), grid%numproc_inPencilComm(dir), ierr)
      Call MPI_Comm_Rank(grid%pencilComm(dir), grid%myrank_inPencilComm(dir), ierr)
    end if

  end subroutine assign_local_indices_cube_box
!
!  This file contains a routine for producing a decomposition of a 1-d array
!  when given a number of processors.  It may be used in "direct" product
!  decomposition.  The values returned assume a "global" domain in [1:n]
!
  subroutine MPE_DECOMP1D( n, numprocs, myid, s, e )
    integer :: n, numprocs, myid, s, e
    integer :: nlocal
    integer :: deficit
    !
    nlocal  = n / numprocs
    s	    = myid * nlocal + 1
    deficit = mod(n,numprocs)
    s	    = s + min(myid,deficit)
    if (myid .lt. deficit) then
      nlocal = nlocal + 1
    endif
    e = s + nlocal - 1
    if (e .gt. n .or. myid .eq. numprocs-1) e = n
    return
  end subroutine MPE_DECOMP1D

  subroutine graceful_exit(myrank, error_string)

    USE ModGlobal

    integer :: myrank
    character(len=*) :: error_string
    integer :: ierr

    if (myrank == 0) write (*,'(A)') 'PlasComCM:'//trim(error_string)
    if (abort_on_exit) then
      call mpi_abort(mycomm, 1, ierr)
    else
      call mpi_barrier(mycomm, ierr)
      call mpi_finalize(ierr)
      stop 1
    end if
 
  end subroutine graceful_exit

  subroutine build_mpi_file_prefix(mpi_prefix,prefix)

    implicit none

    character(len=*), intent(out) :: mpi_prefix
    character(len=*), intent(in) :: prefix

    ! ------------------------------------

    write(mpi_prefix,'(a)') trim(prefix)

  end subroutine build_mpi_file_prefix

  subroutine build_mpi_indexed_filename(mpi_filename,filename,index)

    implicit none

    character(len=*), intent(out) :: mpi_filename
    character(len=*), intent(in) :: filename
    integer, intent(in) :: index

    ! --------------------------------------

    write(mpi_filename,'(a,i5.5)') trim(filename),index

  end subroutine build_mpi_indexed_filename

  subroutine build_mpi_indexed_filename_4(mpi_filename,filename,index)

    ! for files indexed with *.XXXX (e.g. 2.1 restart/grid files)

    implicit none

    character(len=*), intent(out) :: mpi_filename
    character(len=*), intent(in) :: filename
    integer, intent(in) :: index

    ! --------------------------------------

    write(mpi_filename,'(a,i4.4)') trim(filename),index

  end subroutine build_mpi_indexed_filename_4

  subroutine build_mpi_filename(mpi_filename,filename)

    implicit none

    character(len=*), intent(out) :: mpi_filename
    character(len=*), intent(in) :: filename

    ! --------------------------------------

    mpi_filename = trim(filename)

  end subroutine build_mpi_filename

  Subroutine LOCAL_DIMS_CREATE(myrank, nproc,ND,N1,iGrid,cartDims1)
    
    USE ModGlobal
    USE ModDataStruct
    
    IMPLICIT NONE
    
    ! ... arguments
    INTEGER :: myrank
    INTEGER, pointer :: N1(:,:)
    INTEGER :: nproc, ND, iGrid
    INTEGER :: cartDims1(ND)


    ! ... local
    REAL(rfreal), pointer :: N(:)
    INTEGER, pointer :: TempFac(:), Fac(:), cartDims(:)
    REAL(rfreal), dimension(1:12) :: primes
    REAL(rfreal) :: Np, Rval, UsrVal
    INTEGER :: FcLen, ind, Ival, facsave, Nc, NDAux, UsrInd
    INTEGER :: i, j, k, p, counter

    if (myrank == 0) write(*,'(A)') 'PlasComCM: Using PlasComCM domain decomposition.'

    ! ... maximum number of factors
    FcLen = INT(log(dble(nproc))/log(2.0_rfreal))+1

    ! ... are any directions user specified?
    UsrVal = 1.0_rfreal
    UsrInd = -1
    do i = 1, ND
       if(cartDims1(i) > 0) then
          UsrVal = dble(cartDims1(i))
          UsrInd = i
       end if
    end do
    
    allocate(TempFac(FcLen))
    TempFac(:) = 0

    ! ... vector of first 12 prime numbers
    primes = (/2.0_rfreal,3.0_rfreal,5.0_rfreal,7.0_rfreal,11.0_rfreal, &
         13.0_rfreal,17.0_rfreal,19.0_rfreal,23.0_rfreal,29.0_rfreal,31.0_rfreal,37.0_rfreal/)

    allocate(N(ND))
    NDAux = 0
    ! ... Copy number of grid points
    do i = 1,ND
       N(i) = dble(N1(iGrid,i))       
    end do

    ! ... factor the number of processors
    p = 1
    ! ... divide by number in user specified decompostion
    Np = dble(numproc)/UsrVal
    counter = 0
    do i = 1, FcLen
       if(MOD(NP,primes(p))==0) then
          TempFac(i) = INT(primes(p))
          Np = Np / primes(p)
          counter = counter + 1
       else
          ! ... next prime number
          p = p + 1
          if(p > 12) continue
       end if
    end do
    ! ... include remaining factor, if it exists
    if(Np > 1.0_rfreal) then
       counter = counter + 1
       TempFac(FcLen) = INT(Np)
    end if


    ! ... now order the factors
    allocate(Fac(counter))
    do i = 1, counter
       Ival = TempFac(1)
       ind = 1
       do j = 2, FcLen
          if(TempFac(j) > Ival) then
             Ival = TempFac(j)
             ind = j
          end if
       end do
       Fac(i) = TempFac(ind)
       TempFac(ind) = 0
    end do

    ! ... don't need this anymore
    deallocate(TempFac)
    FcLen = counter
    counter = 0

    ! ... now determine number of procs in each dimension
    allocate(cartDims(ND))
    cartDims(:) = 1
    do i = 1, FcLen
       Rval = 0.0_rfreal
       ind = 0
       ! ... find direction with largest number of grid points per proc
       do j = 1, ND
          if (N(j) .gt. Rval .and. j /= UsrInd) then
             Rval = N(j)
             ind = j
          end if
       end do
       ! ... divide that by the largest remaining factor
       cartDims(ind) = cartDims(ind) * Fac(i)
       N(ind) = N(ind) / dble(Fac(i))
    end do
    
    if(UsrInd /= -1) then
       cartDims(UsrInd) = cartDims(UsrInd)*INT(UsrVal)
       N(UsrInd) = N(UsrInd)/UsrVal
    end if

!    if(myrank ==0) write(*,*) 'PlasComCM: Processor decomposition: ',cartDims(:)

    do i = 1, ND
       cartDims1(i) = cartDims(i)
    end do

    deallocate(cartDims,Fac)
    deallocate(N)

  END Subroutine LOCAL_DIMS_CREATE

  Subroutine Agglomerate_Data_To_Core_Rank(region, var_to_write)

    USE ModGlobal
    USE ModDataStruct

    Implicit None

    ! ... function arguments
    type(t_region), pointer :: region
    character(len=3) :: var_to_write

    ! ... local variables
    real(rfreal), pointer :: ptr_to_data(:,:), buf2(:,:)
    type(t_grid), pointer :: grid
    type(t_mixt), pointer :: state, mean
    Type(t_sub_actuator), Pointer :: act
    Type(t_sub_ctrl_target), Pointer :: targ
    integer :: ng, i, j, k, p, l0, ii, jj, kk, na, nt, ierr
    integer, pointer :: ND(:,:), buf(:,:), remote_is(:,:), remote_ie(:,:)
    integer :: status(MPI_STATUS_SIZE)

    Do ng = 1, region%nGrids

      grid => region%grid(ng)
      state => region%state(ng)
      mean => region%mean(ng)

      ! ... nullify
      nullify(ptr_to_data)

      ! ... point pointer
      If ( var_to_write(1:3) == 'cv ' ) Then
        ptr_to_data => state%cv
      Else If ( var_to_write(1:3) == 'rst' ) Then
        if (region%input%AdjNS) then ! ... Adjoint N-S
          ptr_to_data => state%av
        else
          ptr_to_data => state%cv
        end if ! input%AdjNS
      Else If ( var_to_write(1:3) == 'cvt' ) Then
        ptr_to_data => state%cvTarget
      Else If ( var_to_write(1:3) == 'rhs' ) Then
        ptr_to_data => state%rhs
      Else If ( var_to_write(1:3) == 'xyz' ) Then
        ptr_to_data => grid%XYZ
      Else If ( var_to_write(1:3) == 'met' ) Then
        ptr_to_data => grid%MT1
      Else If ( var_to_write(1:3) == 'jac' ) Then
        Nullify(ptr_to_data); Allocate(ptr_to_data(grid%nCells,1)); ptr_to_data = 0.0_rfreal
        Do l0 = 1, grid%nCells
          ptr_to_data(l0,1) = grid%JAC(l0)
        End Do
      Else If ( var_to_write(1:3) == 'dv ' ) Then
        ptr_to_data => state%dv
      Else If ( var_to_write(1:3) == 'av ' ) Then ! ... Adjoint N-S
        ptr_to_data => state%av
      Else If ( var_to_write(1:3) == 'avt' ) Then ! ... Adjoint N-S
        ptr_to_data => state%avTarget
      Else If ( var_to_write(1:3) == 'aux' ) Then
        ptr_to_data => state%auxVars
#ifdef BUILD_ELECTRIC
      Else If ( var_to_write(1:3) == 'phi' ) Then
        if (region%electric(ng)%grid_has_efield) then
          region%electric(ng)%phi_data_ptr(:,1) = region%electric(ng)%phi
        end if
        ptr_to_data => region%electric(ng)%phi_data_ptr
#endif
      Else If ( var_to_write(1:3) == 'ctr' ) Then ! ... Control optimization
        Nullify(ptr_to_data); Allocate(ptr_to_data(grid%nCells,1)); ptr_to_data = 0.0_rfreal
        If (region%nSubActuators > 0) Then
          Do na = 1,region%nSubActuators
            act => region%subActuator(na)

            If (act%gridID == ng) Then ! ... this grid, ng has actuator, na
                                       ! ... a grid can have more than one actuator
              Do i = 1,act%numPts
                l0 = act%index(i)

                ptr_to_data(l0,1) = act%phi(i) ! ... instantaneous control variable
!!$             ptr_to_data(l0,2) = act%distFunc(i) ! ... function determining shape of actuator
              End Do ! i
            End If ! act%gridID
          End Do ! na
        End If ! region%nSubActuators
        If (region%nSubCtrlTarget > 0) then
          Do nt = 1,region%nSubCtrlTarget
            targ => region%subCtrlTarget(nt)

            Do i = 1,targ%numPts
              l0 = targ%index(i)

!!$           ptr_to_data(l0,3) = targ%distFunc(i) ! ... function determining shape of target
            End Do ! i
          End Do ! nt
        End If ! region%nSubCtrlTarget
      Else If ( var_to_write(1:3) == 'drv' ) Then
        ptr_to_data => state%cv
      Else If ( var_to_write(1:3) == 'mid' ) Then
        ptr_to_data => grid%ident
      Else If ( var_to_write(1:3) == 'men' ) Then
        ptr_to_data => mean%cv
      Else If ( var_to_write(1:3) == 'pp ' ) Then
        ptr_to_data => state%pp
      Else
        Write(*,'(A,A)') 'PlasComCM: ERROR: Unknown value of var_to_write: ', var_to_write
        Stop 'Stopping in Agglomerate_Data_To_Core_Rank'
      End If

      ! ... coreRank must allocate buffer space
      if ( region%myrank == grid%coreRank_inComm ) Then
        allocate(state%DBUF_IO(grid%GlobalSize(1),grid%GlobalSize(2),grid%GlobalSize(3),size(ptr_to_data,2)))
        if ( var_to_write(1:3) == 'xyz' ) allocate(state%IBUF_IO(grid%GlobalSize(1),grid%GlobalSize(2),grid%GlobalSize(3)))
      end if

      ! ... have all core processors fill the buf with their own data
      if ( region%myrank == grid%coreRank_inComm ) Then

        ! ... field data
        do k = grid%is(3), grid%ie(3)
          do j = grid%is(2), grid%ie(2)
            do i = grid%is(1), grid%ie(1)
              l0 = (k-grid%is(3))*(grid%ie(2)-grid%is(2)+1)&
                   *(grid%ie(1)-grid%is(1)+1) + &
                   (j-grid%is(2))*(grid%ie(1)-grid%is(1)+1) + &
                   (i-grid%is(1)+1)
              state%DBUF_IO(i,j,k,:) = ptr_to_data(l0,:)
            end do
          end do
        end do

        ! ... iblank data
        if ( var_to_write(1:3) == 'xyz' ) then

          do k = grid%is(3), grid%ie(3)
            do j = grid%is(2), grid%ie(2)
              do i = grid%is(1), grid%ie(1)
                l0 = (k-grid%is(3))*(grid%ie(2)-grid%is(2)+1)&
                     *(grid%ie(1)-grid%is(1)+1) + &
                     (j-grid%is(2))*(grid%ie(1)-grid%is(1)+1) + &
                     (i-grid%is(1)+1)
                state%IBUF_IO(i,j,k) = grid%IBLANK(l0)
              end do
            end do
          end do

        end if

      end if


      ! ... have all non-core processors send their data to the core
      ! ... first have the core processor figure everybody's size
      If (region%myrank == grid%coreRank_inComm) Then

        allocate(remote_is(MAX_ND,0:grid%numproc_inComm-1)); remote_is(:,:) = 1
        allocate(remote_ie(MAX_ND,0:grid%numproc_inComm-1)); remote_ie(:,:) = 1
        allocate(buf(grid%ND,2))

        remote_is(1:grid%ND,0) = grid%is(1:grid%ND)
        remote_ie(1:grid%ND,0) = grid%ie(1:grid%ND)

        do p = 1, grid%numproc_inComm-1
          Call MPI_RECV(buf, 2*grid%ND, MPI_INTEGER, p, p, &
               grid%comm, status, ierr)
          remote_is(1:grid%ND,p) = buf(1:grid%ND,1)
          remote_ie(1:grid%ND,p) = buf(1:grid%ND,2)
        end do

        deallocate(buf)

      Else

        allocate(buf(grid%ND,2));
        do p = 1, grid%ND
          buf(p,1) = grid%is(p)
          buf(p,2) = grid%ie(p)
        end do
        call MPI_Send(buf, 2*grid%ND, MPI_INTEGER, 0, grid%myrank_inComm, &
             grid%comm, ierr)
        deallocate(buf)

      End If


      ! ... now send and receive the data
      If (region%myrank == grid%coreRank_inComm) Then

        allocate(ND(MAX_ND,grid%numproc_inComm)); ND(:,:) = 1;

        ! ... get the data from the other processors
        do p = 1, grid%numproc_inComm-1

          ND(1:grid%ND,p) = remote_ie(1:grid%ND,p) - remote_is(1:grid%ND,p) + 1

          allocate(buf2(ND(1,p)*ND(2,p)*ND(3,p),size(ptr_to_data,2)))

          Call MPI_Recv(buf2, PRODUCT(ND(:,p))*(size(ptr_to_data,2)), MPI_DOUBLE_PRECISION, &
               p, p, grid%comm, status, ierr)

          do k = remote_is(3,p), remote_ie(3,p)
            do j = remote_is(2,p), remote_ie(2,p)
              do i = remote_is(1,p), remote_ie(1,p)

                l0 = (k-remote_is(3,p))*ND(1,p)*ND(2,p) + (j-remote_is(2,p))*ND(1,p) + (i-remote_is(1,p)+1)
                state%DBUF_IO(i,j,k,:) = buf2(l0,:)

              end do
            end do
          end do

          deallocate(buf2)

        end do

      Else

        Call MPI_Send(ptr_to_data, size(ptr_to_data,1)*size(ptr_to_data,2), &
             MPI_DOUBLE_PRECISION, 0, grid%myrank_inComm, grid%comm, ierr)

      End If


      ! ... repeat the process for iblank data
      if ( var_to_write(1:3) == 'xyz' ) then

        If (region%myrank == grid%coreRank_inComm) Then

          ! ... get the data from the other processors
          do p = 1, grid%numproc_inComm-1

            allocate(buf(ND(1,p)*ND(2,p)*ND(3,p),1))

            Call MPI_Recv(buf, PRODUCT(ND(:,p)), MPI_INTEGER, &
                 p, p, grid%comm, status, ierr)

            do k = remote_is(3,p), remote_ie(3,p)
              do j = remote_is(2,p), remote_ie(2,p)
                do i = remote_is(1,p), remote_ie(1,p)

                  l0 = (k-remote_is(3,p))*ND(1,p)*ND(2,p) + (j-remote_is(2,p))*ND(1,p) + (i-remote_is(1,p)+1)
                  state%IBUF_IO(i,j,k) = buf(l0,1)

                end do
              end do
            end do

            deallocate(buf)

          end do

        Else

          Call MPI_Send(grid%iblank, size(grid%iblank,1), &
               MPI_INTEGER, 0, grid%myrank_inComm, grid%comm, ierr)

        End If

      end if

      ! ... in case ptr_to_data is actually allocated
      If ( var_to_write(1:3) == 'jac' .OR. var_to_write(1:3) == 'ctr' ) Then ! ... Control optimization
        deallocate(ptr_to_data)
      End If ! var_to_write(1:3)

      If (region%myrank == grid%coreRank_inComm) deallocate(ND, remote_is, remote_ie)

    End Do

  End Subroutine Agglomerate_Data_To_Core_Rank

  Subroutine Agglomerate_IBLANK_To_Core_Rank(region, ierr)

    USE ModGlobal
    USE ModDataStruct

    Implicit None

    ! ... function arguments
    type(t_region), pointer :: region
    integer :: ierr

    ! ... local variables
    real(rfreal), pointer :: ptr_to_data(:,:), buf2(:,:)
    type(t_grid), pointer :: grid
    type(t_mixt), pointer :: state
    integer :: ng, i, j, k, p, l0, ii, jj, kk
    integer, pointer :: ND(:,:), buf(:,:), remote_is(:,:), remote_ie(:,:)
    integer :: status(MPI_STATUS_SIZE)
    real(rfreal) :: timer

    Do ng = 1, region%nGrids

      ! ... start timer
      timer = MPI_WTime()

      grid => region%grid(ng)
      state => region%state(ng)

      ! ... coreRank must allocate buffer space
      if ( region%myrank == grid%coreRank_inComm ) Then
        allocate(grid%global_iblank(grid%GlobalSize(1),grid%GlobalSize(2),grid%GlobalSize(3)))
      end if

      ! ... have all core processors fill the buf with their own data
      if ( region%myrank == grid%coreRank_inComm ) Then

        do k = grid%is(3), grid%ie(3)
          do j = grid%is(2), grid%ie(2)
            do i = grid%is(1), grid%ie(1)
              l0 = (k-grid%is(3))*(grid%ie(2)-grid%is(2)+1)&
                   *(grid%ie(1)-grid%is(1)+1) + &
                   (j-grid%is(2))*(grid%ie(1)-grid%is(1)+1) + &
                   (i-grid%is(1)+1)
              grid%global_iblank(i,j,k) = grid%IBLANK(l0)
            end do
          end do
        end do

      end if


      ! ... have all non-core processors send their data to the core
      ! ... first have the core processor figure everybody's size
      If (region%myrank == grid%coreRank_inComm) Then

        allocate(remote_is(MAX_ND,0:grid%numproc_inComm-1)); remote_is(:,:) = 1
        allocate(remote_ie(MAX_ND,0:grid%numproc_inComm-1)); remote_ie(:,:) = 1
        allocate(buf(grid%ND,2))

        remote_is(1:grid%ND,0) = grid%is(1:grid%ND)
        remote_ie(1:grid%ND,0) = grid%ie(1:grid%ND)

        do p = 1, grid%numproc_inComm-1
          Call MPI_RECV(buf, 2*grid%ND, MPI_INTEGER, p, p, &
               grid%comm, status, ierr)
          remote_is(1:grid%ND,p) = buf(1:grid%ND,1)
          remote_ie(1:grid%ND,p) = buf(1:grid%ND,2)
        end do

        deallocate(buf)

      Else

        allocate(buf(grid%ND,2));
        do p = 1, grid%ND
          buf(p,1) = grid%is(p)
          buf(p,2) = grid%ie(p)
        end do
        call MPI_Send(buf, 2*grid%ND, MPI_INTEGER, 0, grid%myrank_inComm, &
             grid%comm, ierr)
        deallocate(buf)

      End If


      ! ... now send and receive the data
      If (region%myrank == grid%coreRank_inComm) Then

        allocate(ND(MAX_ND,grid%numproc_inComm)); ND(:,:) = 1;

        ! ... get the data from the other processors
        do p = 1, grid%numproc_inComm-1

          ND(1:grid%ND,p) = remote_ie(1:grid%ND,p) - remote_is(1:grid%ND,p) + 1

          allocate(buf(ND(1,p)*ND(2,p)*ND(3,p),1))

          Call MPI_Recv(buf, PRODUCT(ND(:,p)), MPI_INTEGER, &
               p, p, grid%comm, status, ierr)

          do k = remote_is(3,p), remote_ie(3,p)
            do j = remote_is(2,p), remote_ie(2,p)
              do i = remote_is(1,p), remote_ie(1,p)

                l0 = (k-remote_is(3,p))*ND(1,p)*ND(2,p) + (j-remote_is(2,p))*ND(1,p) + (i-remote_is(1,p)+1)
                grid%global_iblank(i,j,k) = buf(l0,1)

              end do
            end do
          end do

          deallocate(buf)

        end do

        deallocate(ND, remote_is, remote_ie)

      Else

        Call MPI_Send(grid%iblank, size(grid%iblank,1), &
             MPI_INTEGER, 0, grid%myrank_inComm, grid%comm, ierr)

      End If

      ! ... stop timer  
      region%mpi_timings(ng)%operator_setup(:,:) = region%mpi_timings(ng)%operator_setup(:,:) + (MPI_WTime()-timer)

    End Do

  End Subroutine Agglomerate_IBLANK_To_Core_Rank

  Subroutine Agglomerate_Data_To_Core_Rank_box(region, var_to_write)

    USE ModGlobal
    USE ModDataStruct

    Implicit None

    ! ... function arguments
    type(t_region), pointer :: region
    character(len=3) :: var_to_write

    ! ... local variables
    real(rfreal), pointer :: ptr_to_data(:,:), buf2(:,:)
    type(t_grid), pointer :: grid
    type(t_mixt_input), pointer :: input
    type(t_mixt), pointer :: state, mean
    Type(t_sub_actuator), Pointer :: act
    Type(t_sub_ctrl_target), Pointer :: targ
    integer :: ng, i, j, k, p, l0, ii, jj, kk, na, nt, nghost, ierr
    integer, pointer :: ND(:,:), buf(:,:), remote_is(:,:), remote_ie(:,:), remote_nghost(:,:,:)
    integer :: status(MPI_STATUS_SIZE)

    Do ng = 1, region%nGrids

      grid => region%grid(ng)
      state => region%state(ng)
      mean => region%mean(ng)
      input => grid%input

      nghost = input%nOverLap

      ! ... nullify
      nullify(ptr_to_data)

      ! ... point pointer
      If ( var_to_write(1:3) == 'cv ' ) Then
        ptr_to_data => state%cv
      ELSE IF ( var_to_write(1:3) == 'rst' ) Then
        if (region%input%AdjNS) then ! ... Adjoint N-S
          ptr_to_data => state%av
        else
          ptr_to_data => state%cv
        end if ! input%AdjNS
      ELSE IF ( var_to_write(1:3) == 'cvt' ) Then
        ptr_to_data => state%cvTarget
      ELSE IF ( var_to_write(1:3) == 'rhs' ) Then
        ptr_to_data => state%rhs
      ELSE IF ( var_to_write(1:3) == 'xyz' ) Then
        ptr_to_data => grid%XYZ
      ELSE IF ( var_to_write(1:3) == 'met' ) Then
        ptr_to_data => grid%MT1
      ELSE IF ( var_to_write(1:3) == 'jac' ) Then
        Nullify(ptr_to_data); Allocate(ptr_to_data(grid%nCells,1)); ptr_to_data = 0.0_rfreal
        Do l0 = 1, grid%nCells
          ptr_to_data(l0,1) = grid%JAC(l0)
        End Do
      ELSE IF ( var_to_write(1:3) == 'dv ' ) Then
        ptr_to_data => state%dv
      ELSE IF ( var_to_write(1:3) == 'av ' ) Then ! ... Adjoint N-S
        ptr_to_data => state%av
      ELSE IF ( var_to_write(1:3) == 'avt' ) Then ! ... Adjoint N-S
        ptr_to_data => state%avTarget
      ELSE IF ( var_to_write(1:3) == 'aux' ) Then
        ptr_to_data => state%auxVars
#ifdef BUILD_ELECTRIC
      ELSE IF ( var_to_write(1:3) == 'phi' ) Then
        if (region%electric(ng)%grid_has_efield) then
          region%electric(ng)%phi_data_ptr(:,1) = region%electric(ng)%phi
        end if
        ptr_to_data => region%electric(ng)%phi_data_ptr
#endif
      ELSE IF ( var_to_write(1:3) == 'ctr' ) Then ! ... Control optimization
        Nullify(ptr_to_data); Allocate(ptr_to_data(grid%nCells,1)); ptr_to_data = 0.0_rfreal
        If (region%nSubActuators > 0) Then
          Do na = 1,region%nSubActuators
            act => region%subActuator(na)

            If (act%gridID == ng) Then ! ... this grid, ng has actuator, na
                                       ! ... a grid can have more than one actuator
              Do i = 1,act%numPts
                l0 = act%index(i)

                ptr_to_data(l0,1) = act%phi(i) ! ... instantaneous control variable
!!$             ptr_to_data(l0,2) = act%distFunc(i) ! ... function determining shape of actuator
              End Do ! i
            End If ! act%gridID
          End Do ! na
        End If ! region%nSubActuators
        If (region%nSubCtrlTarget > 0) then
          Do nt = 1,region%nSubCtrlTarget
            targ => region%subCtrlTarget(nt)

            Do i = 1,targ%numPts
              l0 = targ%index(i)

!!$           ptr_to_data(l0,3) = targ%distFunc(i) ! ... function determining shape of target
            End Do ! i
          End Do ! nt
        End If ! region%nSubCtrlTarget
      ELSE IF ( var_to_write(1:3) == 'drv' ) Then
        ptr_to_data => state%cv
      ELSE IF ( var_to_write(1:3) == 'mid' ) Then
        ptr_to_data => grid%ident
      ELSE IF ( var_to_write(1:3) == 'men' ) Then
        ptr_to_data => mean%cv
      ELSE IF ( var_to_write(1:3) == 'pp ' ) Then
        ptr_to_data => state%pp
      Else
        Write(*,'(A,A)') 'PlasComCM: ERROR: Unknown value of var_to_write: ', var_to_write
        Stop 'Stopping in Agglomerate_Data_To_Core_Rank'
      End If

      ! ... coreRank must allocate buffer space
      if ( region%myrank == grid%coreRank_inComm ) Then
        allocate(state%DBUF_IO(grid%GlobalSize(1),grid%GlobalSize(2),grid%GlobalSize(3),size(ptr_to_data,2)))
        if ( var_to_write(1:3) == 'xyz' ) allocate(state%IBUF_IO(grid%GlobalSize(1),grid%GlobalSize(2),grid%GlobalSize(3)))
      end if

      ! ... have all core processors fill the buf with their own data
      if ( region%myrank == grid%coreRank_inComm ) Then
        ! ... field data
        Select Case (grid%ND)
          Case(1)
            ! ... 1D
            do k = grid%is_unique(3), grid%ie_unique(3)
              do j = grid%is_unique(2), grid%ie_unique(2)
                do i = grid%is_unique(1), grid%ie_unique(1)
    !              l0 = (k-grid%is(3))*(grid%ie(2)-grid%is(2)+1)&
    !                   *(grid%ie(1)-grid%is(1)+1) + &
    !                   (j-grid%is(2))*(grid%ie(1)-grid%is(1)+1) + &
    !                   (i-grid%is(1)+1)
                  l0 = (i-grid%is_unique(1)+1+grid%nGhostRHS(1,1))
                  state%DBUF_IO(i,j,k,:) = ptr_to_data(l0,:)
                end do
              end do
            end do
          Case(2)
            ! ... 2D
            do k = grid%is_unique(3), grid%ie_unique(3)
              do j = grid%is_unique(2), grid%ie_unique(2)
                do i = grid%is_unique(1), grid%ie_unique(1)
                  l0 = (j-grid%is_unique(2)+grid%nGhostRHS(2,1))*(grid%ie_unique(1)-grid%is_unique(1)+1+sum(grid%nGhostRHS(1,:))) + &
                       (i-grid%is_unique(1)+1+grid%nGhostRHS(1,1))
                  state%DBUF_IO(i,j,k,:) = ptr_to_data(l0,:)
                end do
              end do
            end do
          Case(3)
            ! ... 3D
            do k = grid%is_unique(3), grid%ie_unique(3)
              do j = grid%is_unique(2), grid%ie_unique(2)
                do i = grid%is_unique(1), grid%ie_unique(1)
                  l0 = (k-grid%is_unique(3)+grid%nGhostRHS(3,1))*(grid%ie_unique(2)-grid%is_unique(2)+1+sum(grid%nGhostRHS(2,:)))*&
                       (grid%ie_unique(1)-grid%is_unique(1)+1+sum(grid%nGhostRHS(1,:))) + &
                       (j-grid%is_unique(2)+grid%nGhostRHS(2,1))*(grid%ie_unique(1)-grid%is_unique(1)+1+sum(grid%nGhostRHS(1,:))) + &
                       (i-grid%is_unique(1)+1+grid%nGhostRHS(1,1))
                  state%DBUF_IO(i,j,k,:) = ptr_to_data(l0,:)
                end do
              end do
            end do
        End Select
        ! ... iblank data
        if ( var_to_write(1:3) == 'xyz' ) then
          Select Case (grid%ND)
            Case (1)
              do k = grid%is_unique(3), grid%ie_unique(3)
                do j = grid%is_unique(2), grid%ie_unique(2)
                  do i = grid%is_unique(1), grid%ie_unique(1)
                    l0 = (i-grid%is_unique(1)+1+grid%nGhostRHS(1,1))
                    state%IBUF_IO(i,j,k) = grid%IBLANK(l0)
                  end do
                end do
              end do
            Case (2)
              do k = grid%is_unique(3), grid%ie_unique(3)
                do j = grid%is_unique(2), grid%ie_unique(2)
                  do i = grid%is_unique(1), grid%ie_unique(1)
                    l0 = (j-grid%is_unique(2)+grid%nGhostRHS(2,1))*(grid%ie_unique(1)-grid%is_unique(1)+1+sum(grid%nGhostRHS(1,:))) + &
                         (i-grid%is_unique(1)+1+grid%nGhostRHS(1,1))
                    state%IBUF_IO(i,j,k) = grid%IBLANK(l0)
                  end do
                end do
              end do
            Case (3)
              do k = grid%is_unique(3), grid%ie_unique(3)
                do j = grid%is_unique(2), grid%ie_unique(2)
                  do i = grid%is_unique(1), grid%ie_unique(1)
                    l0 = (k-grid%is_unique(3)+grid%nGhostRHS(3,1))*(grid%ie_unique(2)-grid%is_unique(2)+1+sum(grid%nGhostRHS(2,:)))*&
                         (grid%ie_unique(1)-grid%is_unique(1)+1+sum(grid%nGhostRHS(1,:))) + &
                         (j-grid%is_unique(2)+grid%nGhostRHS(2,1))*(grid%ie_unique(1)-grid%is_unique(1)+1+sum(grid%nGhostRHS(1,:))) + &
                         (i-grid%is_unique(1)+1+grid%nGhostRHS(1,1))
                    state%IBUF_IO(i,j,k) = grid%IBLANK(l0)
                  end do
                end do
              end do
          End Select

        end if

      end if


      ! ... have all non-core processors send their data to the core
      ! ... first have the core processor figure everybody's size
      If (region%myrank == grid%coreRank_inComm) Then

        allocate(remote_is(MAX_ND,0:grid%numproc_inComm-1)); remote_is(:,:) = 1
        allocate(remote_ie(MAX_ND,0:grid%numproc_inComm-1)); remote_ie(:,:) = 1
        allocate(buf(grid%ND,2))

        remote_is(1:grid%ND,0) = grid%is_unique(1:grid%ND)
        remote_ie(1:grid%ND,0) = grid%ie_unique(1:grid%ND)

        do p = 1, grid%numproc_inComm-1
          Call MPI_RECV(buf, 2*grid%ND, MPI_INTEGER, p, p, &
               grid%comm, status, ierr)
          remote_is(1:grid%ND,p) = buf(1:grid%ND,1)
          remote_ie(1:grid%ND,p) = buf(1:grid%ND,2)
        end do

        deallocate(buf)

      Else

        allocate(buf(grid%ND,2));
        do p = 1, grid%ND
          buf(p,1) = grid%is_unique(p)
          buf(p,2) = grid%ie_unique(p)
        end do
        call MPI_Send(buf, 2*grid%ND, MPI_INTEGER, 0, grid%myrank_inComm, &
             grid%comm, ierr)
        deallocate(buf)

      End If

      ! ... then for the box shape we also need the ghost cell information
      If (region%myrank == grid%coreRank_inComm) Then

        allocate(remote_nghost(MAX_ND,0:grid%numproc_inComm-1,2)); remote_nghost(:,:,:) = 0
        allocate(buf(grid%ND,2))

        remote_nghost(1:grid%ND,0,1) = grid%nGhostRHS(1:grid%ND,1)
        remote_nghost(1:grid%ND,0,2) = grid%nGhostRHS(1:grid%ND,2)

        do p = 1, grid%numproc_inComm-1
          Call MPI_RECV(buf, 2*grid%ND, MPI_INTEGER, p, p, &
               grid%comm, status, ierr)
          remote_nghost(1:grid%ND,p,1) = buf(1:grid%ND,1)
          remote_nghost(1:grid%ND,p,2) = buf(1:grid%ND,2)
        end do

        deallocate(buf)

      Else

        allocate(buf(grid%ND,2));
        do p = 1, grid%ND
          buf(p,1) = grid%nGhostRHS(p,1)
          buf(p,2) = grid%nGhostRHS(p,2)
        end do
        call MPI_Send(buf, 2*grid%ND, MPI_INTEGER, 0, grid%myrank_inComm, &
             grid%comm, ierr)
        deallocate(buf)

      End If

      ! ... now send and receive the data
      If (region%myrank == grid%coreRank_inComm) Then

        allocate(ND(MAX_ND,grid%numproc_inComm)); ND(:,:) = 1;

        ! ... get the data from the other processors
        do p = 1, grid%numproc_inComm-1

!          ND(1:grid%ND,p) = remote_ie(1:grid%ND,p) - remote_is(1:grid%ND,p) + 1
          do i = 1, grid%ND
            ND(i,p) = remote_ie(i,p) - remote_is(i,p) + 1 + sum(remote_nghost(i,p,:))
          end do

          allocate(buf2(ND(1,p)*ND(2,p)*ND(3,p),size(ptr_to_data,2)))

          Call MPI_Recv(buf2, PRODUCT(ND(:,p))*(size(ptr_to_data,2)), MPI_DOUBLE_PRECISION, &
               p, p, grid%comm, status, ierr)

          Select Case (grid%ND)
            Case (1)
              do k = remote_is(3,p), remote_ie(3,p)
                do j = remote_is(2,p), remote_ie(2,p)
                  do i = remote_is(1,p), remote_ie(1,p)
    !                l0 = (k-remote_is(3,p))*ND(1,p)*ND(2,p) + (j-remote_is(2,p))*ND(1,p) + (i-remote_is(1,p)+1)
                    l0 = (i-remote_is(1,p)+1+remote_nghost(1,p,1))
                    state%DBUF_IO(i,j,k,:) = buf2(l0,:)
                  end do
                end do
              end do
            Case (2)
              do k = remote_is(3,p), remote_ie(3,p)
                do j = remote_is(2,p), remote_ie(2,p)
                  do i = remote_is(1,p), remote_ie(1,p)
    !                l0 = (k-remote_is(3,p))*ND(1,p)*ND(2,p) + (j-remote_is(2,p))*ND(1,p) + (i-remote_is(1,p)+1)
                    l0 = (j-remote_is(2,p)+remote_nghost(2,p,1))*ND(1,p) + (i-remote_is(1,p)+1+remote_nghost(1,p,1))
                    state%DBUF_IO(i,j,k,:) = buf2(l0,:)
                  end do
                end do
              end do
            Case (3)
              do k = remote_is(3,p), remote_ie(3,p)
                do j = remote_is(2,p), remote_ie(2,p)
                  do i = remote_is(1,p), remote_ie(1,p)
    !                l0 = (k-remote_is(3,p))*ND(1,p)*ND(2,p) + (j-remote_is(2,p))*ND(1,p) + (i-remote_is(1,p)+1)
                    l0 = (k-remote_is(3,p)+remote_nghost(3,p,1))*ND(1,p)*ND(2,p) + (j-remote_is(2,p)+remote_nghost(2,p,1))*ND(1,p) &
                       + (i-remote_is(1,p)+1+remote_nghost(1,p,1))
                    state%DBUF_IO(i,j,k,:) = buf2(l0,:)
                  end do
                end do
              end do
          End Select

          deallocate(buf2)

        end do

      Else

        Call MPI_Send(ptr_to_data, size(ptr_to_data,1)*size(ptr_to_data,2), &
             MPI_DOUBLE_PRECISION, 0, grid%myrank_inComm, grid%comm, ierr)

      End If


      ! ... repeat the process for iblank data
      if ( var_to_write(1:3) == 'xyz' ) then

        If (region%myrank == grid%coreRank_inComm) Then

          ! ... get the data from the other processors
          do p = 1, grid%numproc_inComm-1

            allocate(buf(ND(1,p)*ND(2,p)*ND(3,p),1))

            Call MPI_Recv(buf, PRODUCT(ND(:,p)), MPI_INTEGER, &
                 p, p, grid%comm, status, ierr)

            Select Case (grid%ND)
              Case (1)
                do k = remote_is(3,p), remote_ie(3,p)
                  do j = remote_is(2,p), remote_ie(2,p)
                    do i = remote_is(1,p), remote_ie(1,p)
!                      l0 = (k-remote_is(3,p))*ND(1,p)*ND(2,p) + (j-remote_is(2,p))*ND(1,p) + (i-remote_is(1,p)+1)
                      l0 = (i-remote_is(1,p)+1+remote_nghost(1,p,1))
                      state%IBUF_IO(i,j,k) = buf(l0,1)
                    end do
                  end do
                end do
              Case (2)
                do k = remote_is(3,p), remote_ie(3,p)
                  do j = remote_is(2,p), remote_ie(2,p)
                    do i = remote_is(1,p), remote_ie(1,p)
!                      l0 = (k-remote_is(3,p))*ND(1,p)*ND(2,p) + (j-remote_is(2,p))*ND(1,p) + (i-remote_is(1,p)+1)
                      l0 = (j-remote_is(2,p)+remote_nghost(2,p,1))*ND(1,p) + (i-remote_is(1,p)+1+remote_nghost(1,p,1))
                      state%IBUF_IO(i,j,k) = buf(l0,1)
                    end do
                  end do
                end do
              Case (3)
                do k = remote_is(3,p), remote_ie(3,p)
                  do j = remote_is(2,p), remote_ie(2,p)
                    do i = remote_is(1,p), remote_ie(1,p)
!                      l0 = (k-remote_is(3,p))*ND(1,p)*ND(2,p) + (j-remote_is(2,p))*ND(1,p) + (i-remote_is(1,p)+1)
                      l0 = (k-remote_is(3,p)+remote_nghost(3,p,1))*ND(1,p)*ND(2,p) + (j-remote_is(2,p)+remote_nghost(2,p,1))*ND(1,p) &
                         + (i-remote_is(1,p)+1+remote_nghost(1,p,1))
                      state%IBUF_IO(i,j,k) = buf(l0,1)
                    end do
                  end do
                end do
            End Select

            deallocate(buf)

          end do

        Else

          Call MPI_Send(grid%iblank, size(grid%iblank,1), &
               MPI_INTEGER, 0, grid%myrank_inComm, grid%comm, ierr)

        End If

      end if

      ! ... in case ptr_to_data is actually allocated
      If ( var_to_write(1:3) == 'jac' .OR. var_to_write(1:3) == 'ctr' ) Then ! ... Control optimization
        deallocate(ptr_to_data)
      End If ! var_to_write(1:3)

      If (region%myrank == grid%coreRank_inComm) deallocate(ND, remote_is, remote_ie)

    End Do

  End Subroutine Agglomerate_Data_To_Core_Rank_box

END MODULE ModMPI
