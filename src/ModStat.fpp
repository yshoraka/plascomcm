! Copyright (c) 2015, University of Illinois at Urbana-Champaign, XPACC
! License: MIT, http://opensource.org/licenses/MIT
!-----------------------------------------------------------------------
!
! ModStat.fpp
! 
! - basic code for computing stats on the fly
!
! Revision history
! - 13 Feb 2015 : J. Capecelatro & J. Byun
!
!-----------------------------------------------------------------------
MODULE ModStat

  USE ModGlobal
  USE ModDataStruct

CONTAINS

  Subroutine Stat2D_Init(region)
    USE ModGlobal
    USE ModDataStruct
    USE ModMPI
    USE ModHDF5_IO

    type(t_region), pointer :: region

    ! ... local variables
    type(t_mixt_input), pointer :: input
    type(t_grid), pointer :: grid
    type(t_mixt), pointer :: state
    Integer :: ng, N(MAX_ND), iter, j
    Character(LEN=28) :: fname_h5
    logical :: file_is_there

    ! ... loop over all of the grids
    do ng = 1, region%nGrids

       grid  => region%grid(ng)
       state => region%state(ng)
       input => grid%input

       ! Return if not using stats on this sgrid
       If (input%stat_grid.ne.ng) Cycle

       ! Number of variables
       stat2D%nvar = input%stat_nvar

       ! Grid to compute statistics on
       stat2D%statgrid = input%stat_grid

       ! Define 2D average
       Allocate(stat2D%symdir(1))

       ! Symmetric direction to average out
       stat2D%symdir(1) = input%stat_symdir1

       ! Get local dimensions
       N(:) = 1;
       do j = 1, grid%ND
          N(j) = grid%ie(j) - grid%is(j) + 1
       end do

       ! Get stat dimensions and normalization factor (2D)
       Allocate(stat2D%dim(2))
       Select case(stat2D%symdir(1))
       Case(1)
          stat2D%dim(1)=N(2)
          stat2D%dim(2)=N(3)
          stat2D%spacenorm=1.0D0/(N(1)*grid%cartDims(1))
       Case(2)
          stat2D%dim(1)=N(1)
          stat2D%dim(2)=N(3)
          stat2D%spacenorm=1.0D0/(N(2)*grid%cartDims(2))
       Case(3)
          stat2D%dim(1)=N(1)
          stat2D%dim(2)=N(2)
          stat2D%spacenorm=1.0D0/(N(3)*grid%cartDims(3))
       Case Default
          Call graceful_exit(region%myRank, 'PlasComCM: ModStat2D: symmetric direction not equal 1, 2, or 3!')
       End Select

       ! Initialize sample time
       stat2D%sampler = 1

       ! Variable names (hard-code for now)
       Allocate(stat2D%varname(stat2D%nvar))
       stat2D%varname(1) = 'rhoU'

       ! Allocate 2D stat array
       Allocate(stat2D%sumvar2D(stat2D%dim(1),stat2D%dim(2),stat2D%nvar))

       ! Set file name
       iter = region%input%nstepi
       write(fname_h5(1:25),'(A17,I8.8)') 'RocFlo-CM.Stat2D.',iter
       write(fname_h5(26:28),'(A3)') '.h5'

       ! Open file if it exists, otherwise set to zero
       inquire(file=fname_h5,exist=file_is_there)
       if (file_is_there) then
#ifdef USE_HDF5
          call read_HDF_stats2D(region,fname_h5)
#endif
       else
          ! Initialize averaging time
          stat2D%Delta_t = 0.0D0

          ! Initialize statistic array
          stat2D%sumvar2D = 0.0D0
       end if

    end do
    

  End Subroutine Stat2D_Init
  

  Subroutine Stat2D_Sample(region)

    USE ModGlobal
    USE ModDataStruct
    USE ModMetrics
    USE ModDeriv
    USE ModMPI
    USE ModHDF5_IO

    Implicit None

    TYPE (t_region), POINTER :: region

    ! Local variables
    type(t_grid), pointer :: grid
    type(t_mixt), pointer :: state
    type(t_mixt_input), pointer :: input
    Integer :: ng, N(MAX_ND)
    Integer :: N1, N2, statgrid, symdir, l0, l, m, I, J, K, NDIM, ierr
    Real(KIND=8) :: dt, ibfac, RHO ,rhoU ,rhoV ,rhoW , P
    Real(KIND=8), Allocatable, dimension(:,:) :: sumvar1,buf2d

    ! ... loop over all of the grids
    do ng = 1, region%nGrids

       ! ... simplicity
       grid  => region%grid(ng)
       state => region%state(ng)
       input => grid%input
       statgrid = stat2D%statgrid
       
       ! Cycle if not using stats on this sgrid
       If (statgrid.ne.ng) Cycle

       ! Dimensions of averaging grid
       N1 = stat2D%dim(1)
       N2 = stat2D%dim(2)
       symdir = stat2D%symdir(1)

       ! Get local dimensions
       N(:) = 1;
       do j = 1, grid%ND
          N(j) = grid%ie(j) - grid%is(j) + 1
       end do

       ! Initialize temporary arrays
       Allocate(sumvar1(N1,N2)); sumvar1=0.0D0
       Allocate(buf2d(N1,N2)); buf2d=0.0D0

       ! Current timestep
       dt = region%state(1)%dt(1)

       ! Compute running some of various statistics
       ! Assumes symmetric direction has uniform grid spacing
       Do k = 1, N(3)
          Do j = 1, N(2)
             Do i = 1, N(1)
             
                l0 = (k-1)*N(1)*N(2) + (j-1)*N(1) + i

                ! Get the variables
                RHO = state%cv(l0,1)
                rhoU = state%cv(l0,2)
                rhoV = state%cv(l0,3)
                rhoW = state%cv(l0,4)

                ! Avoid iBlank regions
                ibfac = dble(MIN(abs(grid%iblank(i)),1))*dt*stat2D%spacenorm

                ! Average variables (hard-coded for now)
                Select case(symdir)
                Case(1)
                   ! Average-out 1-direction
                   sumvar1(j,k) = sumvar1(j,k) + rhoU*ibfac
                Case(2)
                   ! Average-out 2-direction
                   sumvar1(i,k) = sumvar1(i,k) + rhoU*ibfac
                Case(3)
                   ! Average-out 3-direction
                   sumvar1(i,j) = sumvar1(i,j) + rhoU*ibfac
                End Select

             End Do
          End Do
       End Do

       ! Sum data across all procs in symmetric direction
       Call MPI_Allreduce(sumvar1, buf2d, N1*N2, MPI_REAL8, MPI_SUM, grid%pencilComm(symdir), ierr)

       ! ... normalize data in space and add to stat array
       stat2D%sumvar2D(:,:,1) = stat2D%sumvar2D(:,:,1) + buf2d

       ! ... update old time
       stat2D%Delta_t = stat2D%Delta_t + dt

       ! ... dump statistics (only implemented with HDF5 right now)
       If (input%stat_noutput > 0) Then
          If (stat2D%sampler == input%stat_noutput .or. region%state(1)%sampler == 0) then
             If (input%useHDF5 == 1) Then
#ifdef USE_HDF5
                Call write_HDF_stats2D(region)
#endif
             Else
                If (grid%myrank_inComm == 0) Write (*,'(A)') 'PlasComCM: Warning: on-the-fly stats only implemented with HDF5...'
             End If
             ! ... reset stat sampler
             stat2D%sampler = 1
          Else
             stat2D%sampler = stat2D%sampler + 1
          End If
       End If

       ! ... clean up
       Deallocate(sumvar1,buf2D)

    End Do

  End Subroutine Stat2D_Sample


END MODULE ModStat
