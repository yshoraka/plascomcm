! Copyright (c) 2015, University of Illinois at Urbana-Champaign, XPACC
! License: MIT, http://opensource.org/licenses/MIT
!-----------------------------------------------------------------------
!
! ModModel1.f90
! 
! - XPACC Model 1
! 
!-----------------------------------------------------------------------

MODULE PolynomialRoots
! ---------------------------------------------------------------------------
! PURPOSE - Solve for the roots of a polynomial equation with real
!   coefficients, up to quartic order. Retrun a code indicating the nature
!   of the roots found.

! AUTHORS - Alfred H. Morris, Naval Surface Weapons Center, Dahlgren,VA
!           William L. Davis, Naval Surface Weapons Center, Dahlgren,VA
!           Alan Miller,  CSIRO Mathematical & Information Sciences
!                         CLAYTON, VICTORIA, AUSTRALIA 3169
!                         http://www.mel.dms.csiro.au/~alan
!           Ralph L. Carmichael, Public Domain Aeronautical Software
!                         http://www.pdas.com
!     REVISION HISTORY
!   DATE  VERS PERSON  STATEMENT OF CHANGES
!    ??    1.0 AHM&WLD Original coding                     
! 27Feb97  1.1   AM    Converted to be compatible with ELF90
! 12Jul98  1.2   RLC   Module format; numerous style changes
!  4Jan99  1.3   RLC   Made the tests for zero constant term exactly zero


  IMPLICIT NONE

  INTEGER,PARAMETER,PRIVATE:: SP=KIND(1.0), DP=KIND(1.0D0)
  REAL(DP),PARAMETER,PRIVATE:: ZERO=0.0D0, FOURTH=0.25D0, HALF=0.5D0
  REAL(DP),PARAMETER,PRIVATE:: ONE=1.0D0, TWO=2.0D0, THREE=3.0D0, FOUR=4.0D0
  COMPLEX(DP),PARAMETER,PRIVATE:: CZERO=(0.D0,0.D0)

  REAL(DP),PARAMETER,PRIVATE:: EPS=EPSILON(ONE)

  CHARACTER(LEN=*),PARAMETER,PUBLIC:: POLYROOTS_VERSION= "1.3 (4 Jan 1999)"
  INTEGER,PRIVATE:: outputCode
!    =0 degenerate equation
!    =1 one real root
!    =21 two identical real roots
!    =22 two distinct real roots
!    =23 two complex roots
!    =31 multiple real roots
!    =32 one real and two complex roots
!    =33 three distinct real roots
!    =41
!    =42 two real and two complex roots
!    =43
!    =44 four complex roots

  PRIVATE:: CubeRoot
  PUBLIC:: LinearRoot
  PRIVATE:: OneLargeTwoSmall
  PUBLIC:: QuadraticRoots
  PUBLIC:: CubicRoots
  PUBLIC:: QuarticRoots
  PUBLIC:: SolvePolynomial
!----------------------------------------------------------------------------

  INTERFACE Swap
    MODULE PROCEDURE SwapDouble, SwapSingle
  END INTERFACE

CONTAINS

!+
FUNCTION CubeRoot(x) RESULT(f)
! ---------------------------------------------------------------------------
! PURPOSE - Compute the Cube Root of a REAL(DP) number. If the argument is
!   negative, then the cube root is also negative.

  REAL(DP),INTENT(IN) :: x
  REAL(DP):: f
!----------------------------------------------------------------------------
  IF (x < ZERO) THEN
    f=-EXP(LOG(-x)/THREE)
  ELSE IF (x > ZERO) THEN
    f=EXP(LOG(x)/THREE)
  ELSE
    f=ZERO
  END IF
  RETURN
END Function CubeRoot   ! ---------------------------------------------------

!+
SUBROUTINE LinearRoot(a, z)
! ---------------------------------------------------------------------------
! PURPOSE - COMPUTES THE ROOTS OF THE REAL POLYNOMIAL
!              A(1) + A(2)*Z 
!     AND STORES THE RESULTS IN Z. It is assumed that a(2) is non-zero.
  REAL(DP),INTENT(IN),DIMENSION(:):: a
  REAL(DP),INTENT(OUT):: z
!----------------------------------------------------------------------------
  IF (a(2)==0.0) THEN
    z=ZERO
  ELSE
    z=-a(1)/a(2)
  END IF
  RETURN
END Subroutine LinearRoot   ! -----------------------------------------------

!+
SUBROUTINE OneLargeTwoSmall(a1,a2,a4,w, z)
! ---------------------------------------------------------------------------
! PURPOSE - Compute the roots of a cubic when one root, w, is known to be
!   much larger in magnitude than the other two

  REAL(DP),INTENT(IN):: a1,a2,a4
  REAL(DP),INTENT(IN):: w
  COMPLEX(DP),INTENT(OUT),DIMENSION(:):: z


  REAL(DP),DIMENSION(3):: aq
!----------------------------------------------------------------------------
  aq(1)=a1
  aq(2)=a2+a1/w
  aq(3)=-a4*w
  CALL QuadraticRoots(aq, z)
  z(3)=CMPLX(w,ZERO,DP)
  
  IF (AIMAG(z(1)) == ZERO) RETURN
  z(3)=z(2)
  z(2)=z(1)
  z(1)=CMPLX(w,ZERO,DP)
  RETURN
END Subroutine OneLargeTwoSmall   ! -----------------------------------------

!+
SUBROUTINE QuadraticRoots(a, z)
! ---------------------------------------------------------------------------
! PURPOSE - COMPUTES THE ROOTS OF THE REAL POLYNOMIAL
!              A(1) + A(2)*Z + A(3)*Z**2
!     AND STORES THE RESULTS IN Z.  IT IS ASSUMED THAT A(3) IS NONZERO.

  REAL(DP),INTENT(IN),DIMENSION(:):: a
  COMPLEX(DP),INTENT(OUT),DIMENSION(:):: z


  REAL(DP):: d, r, w, x, y
!----------------------------------------------------------------------------
  IF(a(1)==0.0) THEN     ! EPS is a global module constant (private)
    z(1) = CZERO               ! one root is obviously zero
    z(2) = CMPLX(-a(2)/a(3), ZERO,DP)    ! remainder is a linear eq.
    outputCode=21   ! two identical real roots
    RETURN
  END IF

  d = a(2)*a(2) - FOUR*a(1)*a(3)             ! the discriminant
  IF (ABS(d) <= TWO*eps*a(2)*a(2)) THEN
    z(1) = CMPLX(-HALF*a(2)/a(3), ZERO, DP) ! discriminant is tiny
    z(2) = z(1)
    outputCode=22  ! two distinct real roots
    RETURN
  END IF

  r = SQRT(ABS(d))
  IF (d < ZERO) THEN
    x = -HALF*a(2)/a(3)        ! negative discriminant => roots are complex   
    y = ABS(HALF*r/a(3))
    z(1) = CMPLX(x, y, DP)
    z(2) = CMPLX(x,-y, DP)   ! its conjugate
    outputCode=23                        !  COMPLEX ROOTS
    RETURN
  END IF

  IF (a(2) /= ZERO) THEN              ! see Numerical Recipes, sec. 5.5
    w = -(a(2) + SIGN(r,a(2)))
    z(1) = CMPLX(TWO*a(1)/w,  ZERO, DP)
    z(2) = CMPLX(HALF*w/a(3), ZERO, DP)
    outputCode=22           ! two real roots
    RETURN
  END IF

  x = ABS(HALF*r/a(3))   ! a(2)=0 if you get here
  z(1) = CMPLX( x, ZERO, DP)
  z(2) = CMPLX(-x, ZERO, DP)
  outputCode=22
  RETURN
END Subroutine QuadraticRoots   ! -------------------------------------------

!+
SUBROUTINE CubicRoots(a, z)
!----------------------------------------------------------------------------
! PURPOSE - Compute the roots of the real polynomial
!              A(1) + A(2)*Z + A(3)*Z**2 + A(4)*Z**3
  REAL(DP),INTENT(IN),DIMENSION(:):: a
  COMPLEX(DP),INTENT(OUT),DIMENSION(:):: z

  REAL(DP),PARAMETER:: RT3=1.7320508075689D0    ! (Sqrt(3)
  REAL (DP) :: aq(3), arg, c, cf, d, p, p1, q, q1
  REAL(DP):: r, ra, rb, rq, rt
  REAL(DP):: r1, s, sf, sq, sum, t, tol, t1, w
  REAL(DP):: w1, w2, x, x1, x2, x3, y, y1, y2, y3

! NOTE -   It is assumed that a(4) is non-zero. No test is made here.
!----------------------------------------------------------------------------
  IF (a(1)==0.0) THEN
    z(1) = CZERO  ! one root is obviously zero
    CALL QuadraticRoots(a(2:4), z(2:3))   ! remaining 2 roots here
    RETURN
  END IF

  p = a(3)/(THREE*a(4))
  q = a(2)/a(4)
  r = a(1)/a(4)
  tol = FOUR*EPS

  c = ZERO
  t = a(2) - p*a(3)
  IF (ABS(t) > tol*ABS(a(2))) c = t/a(4)

  t = TWO*p*p - q
  IF (ABS(t) <= tol*ABS(q)) t = ZERO
  d = r + p*t
  IF (ABS(d) <= tol*ABS(r)) GO TO 110

!           SET  SQ = (A(4)/S)**2 * (C**3/27 + D**2/4)

  s = MAX(ABS(a(1)), ABS(a(2)), ABS(a(3)))
  p1 = a(3)/(THREE*s)
  q1 = a(2)/s
  r1 = a(1)/s

  t1 = q - 2.25D0*p*p
  IF (ABS(t1) <= tol*ABS(q)) t1 = ZERO
  w = FOURTH*r1*r1
  w1 = HALF*p1*r1*t
  w2 = q1*q1*t1/27.0D0

  IF (w1 >= ZERO) THEN
    w = w + w1
    sq = w + w2
  ELSE IF (w2 < ZERO) THEN
    sq = w + (w1 + w2)
  ELSE
    w = w + w2
    sq = w + w1
  END IF

  IF (ABS(sq) <= tol*w) sq = ZERO
  rq = ABS(s/a(4))*SQRT(ABS(sq))
  IF (sq >= ZERO) GO TO 40

!                   ALL ROOTS ARE REAL

  arg = ATAN2(rq, -HALF*d)
  cf = COS(arg/THREE)
  sf = SIN(arg/THREE)
  rt = SQRT(-c/THREE)
  y1 = TWO*rt*cf
  y2 = -rt*(cf + rt3*sf)
  y3 = -(d/y1)/y2

  x1 = y1 - p
  x2 = y2 - p
  x3 = y3 - p

  IF (ABS(x1) > ABS(x2)) CALL Swap(x1,x2)
  IF (ABS(x2) > ABS(x3)) CALL Swap(x2,x3)
  IF (ABS(x1) > ABS(x2)) CALL Swap(x1,x2)

  w = x3

  IF (ABS(x2) < 0.1D0*ABS(x3)) GO TO 70
  IF (ABS(x1) < 0.1D0*ABS(x2)) x1 = - (r/x3)/x2
  z(1) = CMPLX(x1, ZERO,DP)
  z(2) = CMPLX(x2, ZERO,DP)
  z(3) = CMPLX(x3, ZERO,DP)
  RETURN

!                  REAL AND COMPLEX ROOTS

40 ra =CubeRoot(-HALF*d - SIGN(rq,d))
  rb = -c/(THREE*ra)
  t = ra + rb
  w = -p
  x = -p
  IF (ABS(t) <= tol*ABS(ra)) GO TO 41
  w = t - p
  x = -HALF*t - p
  IF (ABS(x) <= tol*ABS(p)) x = ZERO
  41 t = ABS(ra - rb)
  y = HALF*rt3*t
  
  IF (t <= tol*ABS(ra)) GO TO 60
  IF (ABS(x) < ABS(y)) GO TO 50
  s = ABS(x)
  t = y/x
  GO TO 51
50 s = ABS(y)
  t = x/y
51 IF (s < 0.1D0*ABS(w)) GO TO 70
  w1 = w/s
  sum = ONE + t*t
  IF (w1*w1 < 0.01D0*sum) w = - ((r/sum)/s)/s
  z(1) = CMPLX(w, ZERO,DP)
  z(2) = CMPLX(x, y,DP)
  z(3) = CMPLX(x,-y,DP)
  RETURN

!               AT LEAST TWO ROOTS ARE EQUAL

60 IF (ABS(x) < ABS(w)) GO TO 61
  IF (ABS(w) < 0.1D0*ABS(x)) w = - (r/x)/x
  z(1) = CMPLX(w, ZERO,DP)
  z(2) = CMPLX(x, ZERO,DP)
  z(3) = z(2)
  RETURN
  61 IF (ABS(x) < 0.1D0*ABS(w)) GO TO 70
  z(1) = CMPLX(x, ZERO,DP)
  z(2) = z(1)
  z(3) = CMPLX(w, ZERO,DP)
  RETURN

!     HERE W IS MUCH LARGER IN MAGNITUDE THAN THE OTHER ROOTS.
!     AS A RESULT, THE OTHER ROOTS MAY BE EXCEEDINGLY INACCURATE
!     BECAUSE OF ROUNDOFF ERROR.  TO DEAL WITH THIS, A QUADRATIC
!     IS FORMED WHOSE ROOTS ARE THE SAME AS THE SMALLER ROOTS OF
!     THE CUBIC.  THIS QUADRATIC IS THEN SOLVED.

!     THIS CODE WAS WRITTEN BY WILLIAM L. DAVIS (NSWC).

70 aq(1) = a(1)
  aq(2) = a(2) + a(1)/w
  aq(3) = -a(4)*w
  CALL QuadraticRoots(aq, z)
  z(3) = CMPLX(w, ZERO,DP)
  
  IF (AIMAG(z(1)) == ZERO) RETURN
  z(3) = z(2)
  z(2) = z(1)
  z(1) = CMPLX(w, ZERO,DP)
  RETURN
!-----------------------------------------------------------------------


!                   CASE WHEN D = 0

110 z(1) = CMPLX(-p, ZERO,DP)
  w = SQRT(ABS(c))
  IF (c < ZERO) GO TO 120
  z(2) = CMPLX(-p, w,DP)
  z(3) = CMPLX(-p,-w,DP)
  RETURN

120 IF (p /= ZERO) GO TO 130
  z(2) = CMPLX(w, ZERO,DP)
  z(3) = CMPLX(-w, ZERO,DP)
  RETURN

130 x = -(p + SIGN(w,p))
  z(3) = CMPLX(x, ZERO,DP)
  t = THREE*a(1)/(a(3)*x)
  IF (ABS(p) > ABS(t)) GO TO 131
  z(2) = CMPLX(t, ZERO,DP)
  RETURN
131 z(2) = z(1)
  z(1) = CMPLX(t, ZERO,DP)
  RETURN
END Subroutine CubicRoots   ! -----------------------------------------------


!+
SUBROUTINE QuarticRoots(a,z)
!----------------------------------------------------------------------------
! PURPOSE - Compute the roots of the real polynomial
!               A(1) + A(2)*Z + ... + A(5)*Z**4

  REAL(DP), INTENT(IN)     :: a(:)
  COMPLEX(DP), INTENT(OUT) :: z(:)

  COMPLEX(DP) :: w
  REAL(DP):: b,b2, c, d, e, h, p, q, r, t
  REAL(DP),DIMENSION(4):: temp
  REAL(DP):: u, v, v1, v2, x, x1, x2, x3, y


! NOTE - It is assumed that a(5) is non-zero. No test is made here

!----------------------------------------------------------------------------

  IF (a(1)==0.0) THEN
    z(1) = CZERO    !  one root is obviously zero
    CALL CubicRoots(a(2:), z(2:))
    RETURN
  END IF


  b = a(4)/(FOUR*a(5))
  c = a(3)/a(5)
  d = a(2)/a(5)
  e = a(1)/a(5)
  b2 = b*b

  p = HALF*(c - 6.0D0*b2)
  q = d - TWO*b*(c - FOUR*b2)
  r = b2*(c - THREE*b2) - b*d + e

! SOLVE THE RESOLVENT CUBIC EQUATION. THE CUBIC HAS AT LEAST ONE
! NONNEGATIVE REAL ROOT.  IF W1, W2, W3 ARE THE ROOTS OF THE CUBIC
! THEN THE ROOTS OF THE ORIGINIAL EQUATION ARE
!     Z = -B + CSQRT(W1) + CSQRT(W2) + CSQRT(W3)
! WHERE THE SIGNS OF THE SQUARE ROOTS ARE CHOSEN SO
! THAT CSQRT(W1) * CSQRT(W2) * CSQRT(W3) = -Q/8.

  temp(1) = -q*q/64.0D0
  temp(2) = 0.25D0*(p*p - r)
  temp(3) =  p
  temp(4) = ONE
  CALL CubicRoots(temp,z)
  IF (AIMAG(z(2)) /= ZERO) GO TO 60

!         THE RESOLVENT CUBIC HAS ONLY REAL ROOTS
!         REORDER THE ROOTS IN INCREASING ORDER

  x1 = DBLE(z(1))
  x2 = DBLE(z(2))
  x3 = DBLE(z(3))
  IF (x1 > x2) CALL Swap(x1,x2)
  IF (x2 > x3) CALL Swap(x2,x3)
  IF (x1 > x2) CALL Swap(x1,x2)

  u = ZERO
  IF (x3 > ZERO) u = SQRT(x3)
  IF (x2 <= ZERO) GO TO 41
  IF (x1 >= ZERO) GO TO 30
  IF (ABS(x1) > x2) GO TO 40
  x1 = ZERO

30 x1 = SQRT(x1)
  x2 = SQRT(x2)
  IF (q > ZERO) x1 = -x1
  temp(1) = (( x1 + x2) + u) - b
  temp(2) = ((-x1 - x2) + u) - b
  temp(3) = (( x1 - x2) - u) - b
  temp(4) = ((-x1 + x2) - u) - b
  CALL SelectSort(temp)
  IF (ABS(temp(1)) >= 0.1D0*ABS(temp(4))) GO TO 31
  t = temp(2)*temp(3)*temp(4)
  IF (t /= ZERO) temp(1) = e/t
31 z(1) = CMPLX(temp(1), ZERO,DP)
  z(2) = CMPLX(temp(2), ZERO,DP)
  z(3) = CMPLX(temp(3), ZERO,DP)
  z(4) = CMPLX(temp(4), ZERO,DP)
  RETURN

40 v1 = SQRT(ABS(x1))
v2 = ZERO
GO TO 50
41 v1 = SQRT(ABS(x1))
v2 = SQRT(ABS(x2))
IF (q < ZERO) u = -u

50 x = -u - b
y = v1 - v2
z(1) = CMPLX(x, y,DP)
z(2) = CMPLX(x,-y,DP)
x =  u - b
y = v1 + v2
z(3) = CMPLX(x, y,DP)
z(4) = CMPLX(x,-y,DP)
RETURN

!                THE RESOLVENT CUBIC HAS COMPLEX ROOTS

60 t = DBLE(z(1))
x = ZERO
IF (t < ZERO) THEN
  GO TO 61
ELSE IF (t == ZERO) THEN
  GO TO 70
ELSE
  GO TO 62
END IF
61 h = ABS(DBLE(z(2))) + ABS(AIMAG(z(2)))
IF (ABS(t) <= h) GO TO 70
GO TO 80
62 x = SQRT(t)
IF (q > ZERO) x = -x

70 w = SQRT(z(2))
  u = TWO*DBLE(w)
  v = TWO*ABS(AIMAG(w))
  t =  x - b
  x1 = t + u
  x2 = t - u
  IF (ABS(x1) <= ABS(x2)) GO TO 71
  t = x1
  x1 = x2
  x2 = t
71 u = -x - b
  h = u*u + v*v
  IF (x1*x1 < 0.01D0*MIN(x2*x2,h)) x1 = e/(x2*h)
  z(1) = CMPLX(x1, ZERO,DP)
  z(2) = CMPLX(x2, ZERO,DP)
  z(3) = CMPLX(u, v,DP)
  z(4) = CMPLX(u,-v,DP)
  RETURN

80 v = SQRT(ABS(t))
  z(1) = CMPLX(-b, v,DP)
  z(2) = CMPLX(-b,-v,DP)
  z(3) = z(1)
  z(4) = z(2)
  RETURN

END Subroutine QuarticRoots

!+
SUBROUTINE SelectSort(a)
! ---------------------------------------------------------------------------
! PURPOSE - Reorder the elements of in increasing order.
  REAL(DP),INTENT(IN OUT),DIMENSION(:):: a

  INTEGER:: j
  INTEGER,DIMENSION(1):: k
! NOTE - This is a n**2 method. It should only be used for small arrays. <25
!----------------------------------------------------------------------------
  DO j=1,SIZE(a)-1
    k=MINLOC(a(j:))
    IF (j /= k(1)) CALL Swap(a(k(1)),a(j))
  END DO
  RETURN
END Subroutine SelectSort   ! -----------------------------------------------

!+
SUBROUTINE SolvePolynomial(quarticCoeff, cubicCoeff, quadraticCoeff, &
  linearCoeff, constantCoeff, code, root1,root2,root3,root4)
! ---------------------------------------------------------------------------
  REAL(DP),INTENT(IN):: quarticCoeff
  REAL(DP),INTENT(IN):: cubicCoeff, quadraticCoeff
  REAL(DP),INTENT(IN):: linearCoeff, constantCoeff
  INTEGER,INTENT(OUT):: code
  COMPLEX(DP),INTENT(OUT):: root1,root2,root3,root4
  REAL(DP),DIMENSION(5):: a
  COMPLEX(DP),DIMENSION(5):: z
!----------------------------------------------------------------------------
  a(1)=constantCoeff
  a(2)=linearCoeff
  a(3)=quadraticCoeff
  a(4)=cubicCoeff
  a(5)=quarticCoeff

  IF (quarticCoeff /= ZERO) THEN
    CALL QuarticRoots(a,z)  
  ELSE IF (cubicCoeff /= ZERO) THEN
    CALL CubicRoots(a,z)
  ELSE IF (quadraticCoeff /= ZERO) THEN
    CALL QuadraticRoots(a,z)
  ELSE IF (linearCoeff /= ZERO) THEN
    z(1)=CMPLX(-constantCoeff/linearCoeff, 0, DP)
    outputCode=1
  ELSE
    outputCode=0    !  { no roots }
  END IF

  code=outputCode
  IF (outputCode > 0) root1=z(1)
  IF (outputCode > 1) root2=z(2)
  IF (outputCode > 23) root3=z(3)
  IF (outputCode > 99) root4=z(4)
  RETURN
END Subroutine SolvePolynomial   ! ------------------------------------------

!+
SUBROUTINE SwapDouble(a,b)
! ---------------------------------------------------------------------------
! PURPOSE - Interchange the contents of a and b
  REAL(DP),INTENT(IN OUT):: a,b
  REAL(DP):: t
!----------------------------------------------------------------------------
  t=b
  b=a
  a=t
  RETURN
END Subroutine SwapDouble   ! -----------------------------------------------

!+
SUBROUTINE SwapSingle(a,b)
! ---------------------------------------------------------------------------
! PURPOSE - Interchange the contents of a and b
  REAL(SP),INTENT(IN OUT):: a,b
  REAL(SP):: t
!----------------------------------------------------------------------------
  t=b
  b=a
  a=t
  RETURN
END Subroutine SwapSingle   ! -----------------------------------------------


END Module PolynomialRoots   ! ==============================================

MODULE ModModel1

  USE ModGlobal

  Implicit none

  integer,parameter :: m_12steps = 9
  integer,parameter :: m_steady = 3
  integer,parameter :: m_nspecies = m_12steps-m_steady-1
  integer,parameter :: m_nreactions = 4

!species
  integer,parameter :: m_iF = 1
  integer,parameter :: m_iO = 2
  integer,parameter :: m_iR = 3
  integer,parameter :: m_H2O =  4
  integer,parameter :: m_H2 = m_iF
  integer,parameter :: m_O2 = m_iO
  integer,parameter :: m_H =  m_iR

!reactions
  integer,parameter :: m_jI = min(1,m_nreactions) !initiation
  integer,parameter :: m_jR = min(2,m_nreactions) !Recombination
  integer,parameter :: m_jD = min(3,m_nreactions) !decomposition
  integer,parameter :: m_jT = min(4,m_nreactions) !termination


  logical, parameter :: m_isConstTV = .false., doSpeciesFix = .true., m_unreactive = .false.

  real(rfreal), parameter :: m_rUniversal = 8314.4621d0 !J/K/kmole
  real(rfreal), parameter :: m_cal_cms2W_m = 418.4d0
  real(rfreal), parameter :: R_calMolK = 1.9872041d0;
  real(rfreal), parameter :: m_MAXTEMP = 7d3, m_HUGETEMP = 3d4, m_HACKTEMP=1.0d3, m_MAX_DENSITY_STEADY_SPECIES = 3d-4, m_COLDTEMP=3.1d2 
  real(rfreal), parameter :: m_smallNumber = 1d-18, m_fixFactor = 8d0/9d0
  real(rfreal), parameter :: m_minTemperature = 1.d2
  integer :: m_iRKstage=1
  real(rfreal), parameter :: m_wO=15.9994_rfreal, m_wH=1.00794_rfreal, m_wN=14.00674_rfreal
  
CONTAINS
  
  Subroutine Model1Init(this, input, first_param_ptr, myrank, gridComm, regionInput)
    
    USE ModGlobal
    USE ModDataStruct
    USE ModParam
    USE ModMPI
#ifdef HAVE_CANTERA
    use Cantera
    USE ModXMLCantera
#endif

    Implicit None

    Type(t_model1), pointer :: this
    Type(t_mixt_input), pointer :: input
    Type(t_mixt_input), pointer, intent(in), optional :: regionInput
    Type(t_param), pointer :: first_param_ptr
    Integer, intent(in) :: myrank, gridComm

    ! ... Model 1 requires Cantera
#ifndef HAVE_CANTERA
    Call Graceful_Exit(myrank, 'Model1Init: Model1 requires PlasComCM to be built with Cantera!')
#else

    Type(phase_t), pointer :: Cgas
    Type(t_canteraInterface), pointer :: canteraInterface
    type(t_jetCrossFlow), pointer :: jetCrossFlow

    real(rfreal), pointer :: mfrac(:), diff(:), molW(:)
    integer, pointer :: X2C(:) !mapping
    real(rfreal) :: gamma, TempRef, DensRef, SndSpdRef, PresScale, junk, YF, factor
    real(rfreal) :: cpMix,lambdaref,alpharef,M0,Mf
    integer :: i,j,k,l,m,ksp, numSpecies, numReactions, ierr
    integer :: gridRank
    real(rfreal),allocatable :: Amat(:,:),Bmat(:,:), Imat(:,:)
!temporary test:: cancel below
    real(rfreal):: rho,e,Q(MAX_ND+2),Qaux(m_nspecies+m_steady+1),Csum(6)
    real(rfreal) :: Rgas, cvMix,MwRef, OldRE
    real(rfreal) :: mwFuel, Lref, PresRef, Rref, rhoH2, muH2, lambdaH2,alphaH2,SH2, tempState

    Call MPI_COMM_RANK(gridComm, gridRank, ierr)

    nullify(this%canteraInterface)
    nullify(this%jetCrossflow)
    nullify(this%order)
    nullify(this%MwCoeff)
    nullify(this%massDens2Conc)
    nullify(this%dimDividers)
    nullify(this%E, this%theta, this%b, this%Da, this%Qg, this%kr, this%conc, this%omega, &
      this%tBconc, this%AK, this%AJ, this%Rcoeff, this%AmatL)
    nullify(this%k0)
    nullify(this%polynomials)
    nullify(this%products)
    nullify(this%massMatrix, this%AmatK, this%AmatJ, this%Amat)
    nullify(this%thirdBodyEff)
    nullify(this%is3BR, this%isFO)
    nullify(this%speciesArray)

    !Cantera Interface
    allocate(this%canteraInterface)
    canteraInterface=> this%canteraInterface
    
    !Jet in a Crossflow parameters
    allocate(this%jetCrossFlow)
    jetCrossFlow=> this%jetCrossFlow
    
    X2C => canteraInterface%X2C

    
    TempRef = input%TempRef
    PresRef = input%PresRef
    gamma = input%GamRef

    ! ... get the XML file name
    call set_param(myrank,input%combustion_model_fname,'COMBUSTION_MODEL_FNAME',first_param_ptr,default='')

    ! ... get the combustion model mix
    call set_param(myrank,input%combustion_model_mix,'COMBUSTION_MODEL_MIX',first_param_ptr,default='')

    input%nTv = 3+2*m_nspecies
    input%massDiffusivityIndex = 4
    input%heatDiffusivityIndex = 4+m_nspecies
    input%nScalars = m_nspecies
    input%nAuxVars = m_nspecies
    input%nAuxVarsSteady = m_steady

    if(m_nspecies > 0) then
      input%gas_dv_model = DV_MODEL_IDEALGAS_MIXTURE!
       input%gas_tv_model = TV_MODEL_EXTERNAL
    else
       input%gas_dv_model = DV_MODEL_IDEALGAS
       input%gas_tv_model = TV_MODEL_POWERLAW
       input%TimeRef = input%LengRef / input%sndSpdref
       RETURN
    end if

    Cgas  => this%canteraInterface%Cgas
    if (myrank == 0) print*, "Reading Chemistry File ", trim(input%combustion_model_fname)
    Cgas = importPhase(trim(input%combustion_model_fname),trim(input%combustion_model_mix))

    call mpi_barrier(gridComm, ierr)

!cantera indexes
    canteraInterface%CkR = speciesIndex(Cgas, 'H')
    canteraInterface%CkF = speciesIndex(Cgas, 'H2')
    canteraInterface%CkO = speciesIndex(Cgas, 'O2')
    canteraInterface%CkN = speciesIndex(Cgas, 'N2')
    canteraInterface%CkW = speciesIndex(Cgas, 'H2O')
    ksp = nSpecies(Cgas)

    if (myrank == 0) print'("Num Species ",i5," indexes R, F, O, N, W",5i5)',ksp,canteraInterface%CkR,canteraInterface%CkF,canteraInterface%CkO,canteraInterface%CkN,canteraInterface%CkW

    allocate(canteraInterface%mfrac(ksp), canteraInterface%diff(ksp), canteraInterface%X2C(m_nspecies+m_steady+1), canteraInterface%molW(ksp));
    canteraInterface%mfrac=0d0;canteraInterface%diff=0d0
    !preset X2C for minimum species set
    canteraInterface%X2C(m_iF) = canteraInterface%CkF
    canteraInterface%X2C(m_iO) = canteraInterface%CkO
    if(m_iR <= m_nspecies) canteraInterface%X2C(m_iR) = canteraInterface%CkR
    if(m_H2O <= m_nspecies) canteraInterface%X2C(m_H2O) = canteraInterface%CkW
    
!store properties for the jet stream
    call setState_TPX(Cgas, TempRef, PresRef, 'H2:0.99999, O2:0.00001')
    Rgas = m_rUniversal/meanMolecularWeight(Cgas)
    jetCrossFlow%jetSndSpdRef = sqrt( gamma* Rgas * TempRef)
    jetCrossFlow%jetmuref=viscosity(Cgas)
    jetCrossFlow%jetRref=density(Cgas)

    rhoH2 = jetCrossFlow%jetRref
    muH2=viscosity(Cgas)
    lambdaH2=thermalConductivity(Cgas)
    call getMixDiffCoeffsMass(Cgas, canteraInterface%diff)
    alphaH2 = rhoH2*canteraInterface%diff(canteraInterface%CkF)
    SH2 = muH2/alphaH2
    if (myrank == 0) print*,'Schmidt H_2',SH2, 'YO', 1d0/(1d0+7d0/8d0*this%nitrogenOxygenMoleRatio)

! properties for the cross stream
    call setState_TPX(Cgas, TempRef, PresRef, 'H2:0.00000001, O2:1, N2:3.76')
    Rgas = m_rUniversal/meanMolecularWeight(Cgas)
    cpMix = Rgas*gamma/(gamma-1d0)
    cvMix = Rgas/(gamma-1d0)
    input%EintRef = cvMix*TempRef
    input%Cpref = cpMix
    input%Cvref = cvMix
    input%gasConstantRef = Rgas
    jetCrossFlow%airSndSpdRef = sqrt( gamma* Rgas * TempRef)
    jetCrossFlow%airmuref=viscosity(Cgas)
    jetCrossFlow%airRref=density(Cgas)


    this%YO = 1d0/(1d0+m_wN/m_wO*this%nitrogenOxygenMoleRatio)
    this%oxygenFuelMassRatio = m_wO/2d0/m_wH
    mwFuel = meanMolecularWeight(Cgas)
    call getMolecularWeights(Cgas, this%canteraInterface%molW)

    !density at reference (pressure and temperature are read from input, the density, speed of sound etc.. are overridden)
    Rref = density(Cgas)
    input%DensRef = Rref
    input%SndSpdRef = sqrt( gamma* Rgas * TempRef)


!dimensional values to get dimensionless qtities in computeTV
    canteraInterface%muref=viscosity(Cgas)
    canteraInterface%lambdaref=thermalConductivity(Cgas)
    call getMixDiffCoeffsMass(Cgas, canteraInterface%diff)
    canteraInterface%alpharef = Rref*canteraInterface%diff(canteraInterface%CkO)

!reassign the Reynolds and other numbers
    this%isInviscid = input%RE <= 0.0d0
    OldRE = input%RE
    input%RE = input%DensRef*input%SndSpdRef*input%LengRef/canteraInterface%muref
    input%PR = input%Cpref*canteraInterface%muref/canteraInterface%lambdaref
    input%SC = canteraInterface%muref/canteraInterface%alpharef   
    input%TimeRef = input%LengRef / input%sndSpdref
    
    if (myrank == 0) print'("OldRE, Reynolds, PR, Schmidt",1p123e12.4)', OldRE, input%RE, input%PR, input%SC
    if (myrank == 0) print'("Rey per unit Velocity",1p123e12.4)', input%RE/input%SndSpdRef
    if (myrank == 0) print'("Temperature Scale",1p123e12.4)',  input%TempRef *(input%GamRef-1)
    if (myrank == 0)  print'("DensRef,SndSpdRef,LengRef,muref ",1p123e12.4)',  input%DensRef,input%SndSpdRef,input%LengRef,canteraInterface%muref
    if (myrank == 0)  print'("rhoH2 ",1p123e12.4)',  rhoH2, rhoH2**2*this%Qg1/canteraInterface%molW(m_H2), canteraInterface%molW(m_H2)


    input%Froude = input%SndSpdRef**2/9.81d0/input%LengRef
    input%gravity_angle_phi = TWOPI/4d0
    input%gravity_angle_theta = -TWOPI/4d0
    if (myrank == 0) print'("Froude, Froude_code",1p123e12.4)',jetCrossFlow%injectionVelocity**2/9.81d0/input%LengRef,input%Froude


    if (myrank == 0) write (*,'(A,1p999e12.4)') 'Combustion: ==> Resetting RE, PR, SC,U <==', input%RE, input%RE/input%SndSpdRef*jetCrossFlow%injectionVelocity, input%PR, input%SC,input%SndSpdRef,input%DensRef,input%LengRef,canteraInterface%muref


!this block increases the velocity and the viscoisty, gravity to match Reynolds Froude and so on
    if(input%machFactor > 1d0) then  !scale the reynolds and Froude numbers
       input%RE = input%RE/input%machFactor
       input%Froude = input%Froude/(input%machFactor**2)
       input%TimeRef = input%TimeRef*input%machFactor
       if (myrank == 0) write (*,'(A,1p999e12.4)') 'Combustion: ==> Modified Re, input%machFactor <==', input%RE,input%machFactor,  input%machFactor*jetCrossFlow%injectionVelocity/input%SndSpdRef,  input%machFactor*jetCrossFlow%crossVelocity/jetCrossFlow%airSndSpdRef
    else
       input%machFactor=1.0d0
    end if
    input%invFroude = 1.0_WP / input%Froude
    this%dimVelFactor = input%SndSpdRef/input%machFactor

    if(this%isInviscid) then
      input%RE = 0.0d0
      input%Froude = 0d0
      input%invFroude = 0d0
    endif
    if(myRank == 0) print'(a,1p123e12.4)',"Inverse Froude g L /c^2 ",input%invFroude

!!  record indexes for exporting iformation to other modules
    this%nspecies = m_nspecies
    this%iH2 = m_H2
    this%iO2 = m_O2
    this%iH =  m_H
    this%iH2O =m_H2O
    if(.false.) then  !use the flow scale
      this%DBDTimeRef = input%TimeRef
    else  !use the acoustic scale
      this%DBDTimeRef = input%LengRef / input%sndSpdref
    endif
    this%momentumScaling =  input%TimeRef * input%machFactor / (input%DensRef*input%sndSpdref)
    this%radicalSourceScaling = input%DensRef*input%TimeRef /canteraInterface%molW(m_H2)
    this%energySourceScaling = input%DensRef**2*input%TimeRef /(input%DensRef*input%SndSpdRef**2)


    this%isChemistryInitialized = .false.

    molW => canteraInterface%molW
    X2C => canteraInterface%X2C
    Cgas  => canteraInterface%Cgas

    ! ... reference values
    TempRef   = input%TempRef
    DensRef   = input%densref
    SndSpdRef = input%sndspdref
    gamma    = input%gamref
    this%gamma = gamma
!lucaNotice:: the input%presref below is not the pressure scale but the pressure at the reference state
    PresScale   = input%presref*gamma


!source dimension factor
    allocate(this%dimDividers(m_nspecies))
    this%dimDividers = DensRef
    this%dimDividers = this%dimDividers/input%TimeRef

!factors to get dimensional temperature, density and momentum
    this%dimTempFactor = (gamma-1.0d0)*TempRef
    this%dimDensFactor = input%densref
    this%dimPresFactor = input%presref*gamma
    this%dimMomFactor = DensRef*SndSpdRef

!model1 set-up
    if (gridRank == 0) then
      call read_xml_file_cantera_M1(trim(input%combustion_model_fname), this )
    endif

    call MPI_BCAST(this%numSpecies, 1, MPI_INTEGER, 0, gridComm, ierr)
    call MPI_BCAST(this%numReactions, 1, MPI_INTEGER, 0, gridComm, ierr)
    call BroadcastPtr(this%Da, gridComm)
    call BroadcastPtr(this%b, gridComm)
    call BroadcastPtr(this%E, gridComm)
    call BroadcastPtr(this%theta, gridComm)
    call BroadcastPtrL(this%is3BR, gridComm)
    call BroadcastPtrL(this%isFO, gridComm)
    call BroadcastPtr2(this%order, gridComm)
    call BroadcastPtr2(this%thirdBodyEff, gridComm)
    call BroadcastPtr2(this%products, gridComm)
    call BroadcastPtrC(this%speciesArray, gridComm)
    call BroadcastPtrKF0(this%k0, gridComm)
    call BroadcastPtrPolyCoeff(this%polynomials, gridComm)

    call getMolecularWeights(Cgas, this%canteraInterface%molW)
    
!factors to transform the dimensionless mass densities in dimensional concentrations in kmol/m^3
    allocate(this%massDens2Conc(this%numspecies+1))
    this%massDens2Conc(1:this%numspecies+1) = DensRef/molW(1:this%numspecies+1)
    
!temporary vectors used in the rate routines
    allocate( this%kr(this%numReactions), this%tBconc(this%numReactions),this%partners(this%numReactions,2))

!the N2 index
    this%iN2 = speciesIndex(Cgas, 'N2');
    this%iH2 = speciesIndex(Cgas, 'H2');
    this%iO2 = speciesIndex(Cgas, 'O2');
    this%iH =  speciesIndex(Cgas, 'H');
    this%iH2O =speciesIndex(Cgas, 'H2O');
    if(myrank ==0) print*,'iN2, iH2, iO2, iH, iH2O :',this%iN2, this%iH2, this%iO2, this%iH, this%iH2O
    
    
!evaluate the XPACC->Cantera Mapping
    do i = 1,this%numspecies+1
      X2C(i) = speciesIndex(Cgas, trim(this%speciesArray(i)))
    enddo
    
!temporary matrices to build the Boivin Model
    allocate( Amat(ubound(this%order,1),ubound(this%order,2)), Bmat(m_nspecies,ubound(this%order,2)), Imat(m_nspecies,m_nspecies) )
    allocate( this%AmatK(ubound(this%order,2),4),  this%AmatJ(ubound(this%order,2),4),this%AmatL(ubound(this%order,2)) )
    Amat = this%products - this%order
    Bmat = 0d0; do i = 1,m_nspecies;Bmat(i,i) = 1d0;enddo
    j=speciesIndex(Cgas, 'O');k=speciesIndex(Cgas, 'OH');l=speciesIndex(Cgas, 'H2O2');
    this%jO = j;this%jOH = k;this%jH2O2 = l;
    this%isteadyB=min(this%jO,this%jOH,this%jH2O2);
    this%isteadyE=max(this%jO,this%jOH,this%jH2O2);
    if(m_steady > 0) then
      i = speciesIndex(Cgas, 'H2');
      Bmat(i,j) = -2d0; Bmat(i,k) = -1d0;Bmat(i,l) = -2d0;
      i = speciesIndex(Cgas, 'H');
      Bmat(i,j) = 2d0; Bmat(i,k) = 1d0;Bmat(i,l) = 2d0;
      i = speciesIndex(Cgas, 'H2O');
      Bmat(i,j) = 1d0; Bmat(i,k) = 1d0;Bmat(i,l) = 2d0;
    endif
    Bmat = MATMUL(Bmat(:,:ubound(this%order,1)),Amat)
    
    allocate( this%AK(ubound(this%order,2)), this%AJ(ubound(this%order,2)), this%Amat(ubound(this%order,1),ubound(this%order,2)) )!cancel these
    this%Amat = Amat!cancel this
    
    Amat(k,:) = Amat(k,:) + Amat(k,ubound(Amat,2))* Amat(l,:) + Amat(k,1)* Amat(j,:)
    
     this%AK = Amat(k,:)!just for checking
     this%AJ = Amat(j,:)!just for checking
     this%AmatL(:) = Amat(L,:)
!positive parts
    do i = 1,2
      this%AmatK(:,i) = (Amat(k,:) + abs(Amat(k,:)))/2d0
      this%AmatJ(:,i) = (Amat(j,:) + abs(Amat(j,:)))/2d0
    enddo
!negative parts
    do i = 3,4
      this%AmatK(:,i) = (Amat(k,:) - abs(Amat(k,:)))/2d0
      this%AmatJ(:,i) = (Amat(j,:) - abs(Amat(j,:)))/2d0
    enddo


    do i = 1,ubound(this%order,2)
      m = count( this%order(:,i) > 0.5)
      if(m == 2) then
        this%partners(i,1) = sum( minloc(this%order(:,i),this%order(:,i)>0d0) )
        this%partners(i,2) = sum( this%partners(i,1)+minloc(this%order(this%partners(i,1)+1:,i),this%order(this%partners(i,1)+1:,i)>0d0))
        if(this%order(this%partners(i,1),i) .ne. 1d0 .or. this%order(this%partners(i,2),i) .ne. 1d0) then
          print*,'Error',this%order(this%partners(i,:),i)
        endif
      elseif(m==1) then
        this%partners(i,1) = sum( minloc(this%order(:,i),this%order(:,i)>0d0))
        if(this%order(this%partners(i,1),i) == 2d0) then
          this%partners(i,2) =this%partners(i,1)
        else
          this%partners(i,2) = 0
        end if
      endif
      if(myrank == 0) print'(2i4,123i8)',i,m,this%partners(i,1:2), minloc(this%order(:,i),this%order(:,i)>0d0), int(this%order(this%partners(i,1),i))
      if( any(this%partners(i,:) == j) ) then
        this%AmatK(i,1) = 0d0
        this%AmatK(i,3) = 0d0
      else
        this%AmatK(i,2) = 0d0
        this%AmatK(i,4) = 0d0
      end if
      if( any(this%partners(i,:) == k) ) then
        this%AmatJ(i,1) = 0d0
        this%AmatJ(i,3) = 0d0
      else
        this%AmatJ(i,2) = 0d0
        this%AmatJ(i,4) = 0d0
      endif
    enddo

!make sure these are identical
    if(myrank == 0) print*,' n1==n2==n3',m_nspecies+m_steady+1,'==',this%numSpecies + 1,'==',nSpecies(Cgas)

    !need only to take care of the non-dimensionalization
    Imat = 0d0;
    do i = 1,m_nspecies
      Imat(i,i) = this%canteraInterface%molW(i)
    enddo
    Bmat = MATMUL(Imat,Bmat)

    
    do i = 1,this%numspecies+1
      allocate(this%polynomials(i)%cMod(7,2))
      this%polynomials(i)%cMod(:,:) = this%polynomials(i)%coeffs(:,:)
      this%polynomials(i)%cMod(1,:) = this%polynomials(i)%cMod(1,:) -1d0 !remove R T from H
      do m=1,2
        this%polynomials(i)%cMod(1:5,m) = this%polynomials(i)%cMod(1:5,m)/[1d0,2d0,3d0,4d0,5d0]
      enddo
      this%polynomials(i)%cMod(:,:) = this%polynomials(i)%cMod(:,:)*m_rUniversal/this%canteraInterface%molW(i)
    enddo
    
!remove the nitrogen
    do i = 1,this%numspecies+1
      if (i == this%iN2) cycle
      this%polynomials(i)%cMod(:,:) = this%polynomials(i)%cMod(:,:) - this%polynomials(this%iN2)%cMod(:,:)
    enddo
    
    !scale the termocaloric terms
    do i = 1,this%numspecies+1
      this%polynomials(i)%cMod = this%polynomials(i)%cMod/PresScale*DensRef
    enddo

    
!mass matrix based on atom conservation
    allocate(this%massMatrix(m_nspecies, ubound(Bmat,2)))

    this%massMatrix(1:m_nspecies,:) = Bmat(1:m_nspecies,:)


!do the same for the enthalpy
    do i = 1,this%numspecies+1
      allocate(this%polynomials(i)%cH(7,2))
      this%polynomials(i)%cH(:,:) = this%polynomials(i)%coeffs(:,:)
      do m=1,2
        this%polynomials(i)%cH(1:5,m) = this%polynomials(i)%cH(1:5,m)/[1d0,2d0,3d0,4d0,5d0]
      enddo
      this%polynomials(i)%cH(:,:) = this%polynomials(i)%cH(:,:)*m_rUniversal/this%canteraInterface%molW(i)
    enddo
!remove the nitrogen from the enthalpy terms
    do i = 1,m_nspecies
      if (i == this%iN2) cycle
      this%polynomials(i)%cH(:,:) = this%polynomials(i)%cH(:,:) - this%polynomials(this%iN2)%cH(:,:)
    enddo
!scale the termocaloric terms
    do i = 1,this%numspecies+1
      this%polynomials(i)%cH = this%polynomials(i)%cH*DensRef/PresScale
    enddo

    !the gas constant
    allocate(this%Rcoeff(this%numspecies+1))
    do i = 1,this%numspecies+1
      !... remove N2
      if(i /= this%iN2) then
        this%Rcoeff(i) = m_rUniversal*(1d0/this%canteraInterface%molW(i) - 1d0/this%canteraInterface%molW(this%iN2))
      else
        this%Rcoeff(i) = m_rUniversal/this%canteraInterface%molW(this%iN2)
      endif
    enddo
    !transform to K^{-1}. It will be multiplied by the dimensional temperature
    this%Rcoeff = this%Rcoeff*DensRef/PresScale
    
    !the Cp
    do i = 1,this%numspecies+1
      allocate(this%polynomials(i)%cp(7,2))
      this%polynomials(i)%cp(:,:) = this%polynomials(i)%coeffs(:,:)
      this%polynomials(i)%cp(:,:) = this%polynomials(i)%cp(:,:)*m_rUniversal/this%canteraInterface%molW(i)
    enddo
!remove the nitrogen from the cp terms
    do i = 1,this%numspecies+1
      if(i/=this%iN2)&
           this%polynomials(i)%cp(:,:) = this%polynomials(i)%cp(:,:) - this%polynomials(this%iN2)%cp(:,:)
    enddo
!scale the cp terms
    do i = 1,this%numspecies+1
      this%polynomials(i)%cp = this%polynomials(i)%cp*DensRef/PresScale
    enddo

    
    do i = 1,this%numspecies+1
      Qaux = 0d0
      Qaux(i) = 1d0
      Q(1) = 1d0
      call TRhoAux2Pgamma(input%TempRef/this%dimTempFactor,Q(1), Qaux, this, junk)
      this%polynomials(i)%Delta = 0d0
      this%polynomials(i)%Delta = TRhoAux2Energy(input%TempRef/this%dimTempFactor,Q(1),Qaux,this) - junk/(input%gamref-1.0_rfreal)
    enddo

    this%isUnreactive = .false.

    this%isChemistryInitialized = .true.

    ! ... equation of state
    if (input%gas_dv_model /= DV_MODEL_IDEALGAS_MIXTURE .or. input%gas_dv_model /= TV_MODEL_EXTERNAL) &
         Call Graceful_Exit(myrank, 'Model1Init: Model1 requires GAS_EQUATION_OF_STATE = IDEAL_GAS_MIXTURE or TV_MODEL_EXTERNAL!')

    ! ... overwrite region input file if present
    if (present(regionInput)) then
       regionInput%nAuxVars = input%nAuxVars
       regionInput%nTv = input%nTv
       regionInput%massDiffusivityIndex = input%massDiffusivityIndex
       regionInput%gas_dv_model = input%gas_dv_model
       regionInput%gas_tv_model = input%gas_tv_model
       regionInput%TimeRef = input%TimeRef
       regionInput%EintRef = input%EintRef
       regionInput%Cpref = input%Cpref
       regionInput%Cvref = input%Cvref
       regionInput%gasConstantRef = input%gasConstantRef
       regionInput%DensRef = input%DensRef
       regionInput%SndSpdRef = input%SndSpdRef
       regionInput%RE = input%RE
       regionInput%PR = input%PR
       regionInput%SC = input%SC
       regionInput%Froude = input%Froude
       regionInput%gravity_angle_phi = input%gravity_angle_phi
       regionInput%gravity_angle_theta = input%gravity_angle_theta
       regionInput%machFactor = input%machFactor
       regionInput%invFroude = input%invFroude
    end if

#endif

  End Subroutine Model1Init

  Subroutine Model1Source(this, xyz, cv, dv, aux, auxs, rhsSP, iblank, input, myrank, iRKstage)
    USE ModGlobal
    USE ModMPI
    USE ModDataStruct

    IMPLICIT none

    Type(t_model1), pointer :: this
    real(rfreal), pointer, dimension(:,:) :: cv, dv, aux
    real(rfreal), dimension(:,:), intent(INOUT) :: rhsSP, auxs
    INTEGER, dimension(:), intent(IN) :: iblank
    type(t_mixt_input), pointer :: input
    real(rfreal), pointer, dimension(:,:),optional,intent(IN) :: XYZ
    Integer, Intent(in) :: myrank, iRKstage

#ifdef HAVE_CANTERA

    real(rfreal),pointer, dimension(:) :: dimDividers, massDens2Conc, molW
    real(rfreal), pointer,dimension(:)  :: theta, b, Da, kr, tBconc
    real(rfreal), pointer, dimension(:,:) :: order !d log(k_i)/ d log(Y_j)
    real(rfreal), pointer, dimension(:,:) :: thirdBodyEff !third body efficiencies
    real(rfreal),pointer, dimension(:,:) :: massMatrix
    real(rfreal),pointer, dimension(:,:) :: AmatK,AmatJ
    logical, pointer, dimension(:) :: is3BR, isFO
    integer, pointer, dimension(:,:) :: partners
    type(t_kF0), pointer, dimension(:) :: k0

    real(rfreal) :: temp, mdotU, conc(0:m_nspecies+m_steady+1), prodConc,dimTempFactor, Qaux(m_nspecies+m_steady+1)

    real(rfreal) ::  omega(m_nspecies),maxNS, allMaxNS, sumSpec, Ys, PresRef

    integer :: Nx, ienergy,irho, isteadyB,isteadyE
    integer :: i, k, ir, is, nreactions, iLL(1)
    logical :: has3BR,useActivityStandPressure=.false.
    real(rfreal) :: xfactor, Tfactor, maxT, portRadius, x0,z0,y0,r0
!Troe Falloff
    real(rfreal) :: Pr, alphaT, rt3, rt1, t2, Fcent, work, lpr, cc ,nn, f1,lgf, Ajo(4), Ako(4), A0, A1, A2, AmLL
    real(rfreal),allocatable :: AmatL(:)


    PresRef   = input%presref
    useActivityStandPressure = input%useActivityStandardPressure

    dimDividers => this%dimDividers
    massDens2Conc => this%massDens2Conc
    massMatrix  => this%massMatrix
    Da  => this%Da
    theta  => this%theta
    b  => this%b
    order  => this%order
    thirdBodyEff  => this%thirdBodyEff
    is3BR  => this%is3BR
    isFO  => this%isFO
    molW => this%canteraInterface%molW
    nreactions = this%numReactions
    k0  => this%k0
    kr  => this%kr
    tBconc  => this%tBconc
    partners => this%partners
    AmatK => this%AmatK
    AmatJ => this%AmatJ
    allocate(AmatL(nreactions))
    AmatL = this%AmatL
    AmLL = -minval(AmatL)
    iLL = minloc(AmatL)
    AmatL(iLL(1)) = 0d0

    has3BR = any(is3BR)

    if(.not. this%isChemistryInitialized) then
       call graceful_exit(myRank, "in addCombustionSources w/o ChemistryInitialized")
    endif

    Nx = ubound(dv,1)
    ienergy = ubound(cv,2)
    irho =    lbound(cv,2)
    dimTempFactor = this%dimTempFactor

    if(m_steady > 0) auxS = 0d0
    if(m_nspecies == 0 .or. m_nreactions == 0 .or. m_unreactive) return

    conc=1d0;tBconc=0d0
    isteadyB=min(this%jO,this%jOH,this%jH2O2);isteadyE=max(this%jO,this%jOH,this%jH2O2);
    Do i = 1, Nx

!unroll
      if(iblank(i) == 0) cycle

      temp = dv(i,2) * dimTempFactor
      if(temp > m_HUGETEMP) cycle
      if(temp < m_COLDTEMP) cycle
      if(useActivityStandPressure) then
         Qaux(1:m_nspecies) =  aux(i,1:)
         Qaux(m_nspecies+1:) = 0d0
        Call TRhoAux2Pgamma(input%gamref, cv(i,1), Qaux, this, temp)
        temp = dimTempFactor/temp
        sumSpec = 0d0
        prodConc = 1d0
        do is = 1,m_nspecies
          Ys =  aux(i,is)/cv(i,irho)
          conc(is) = Ys/molW(is)  !Y_s/M_s
          if(Ys>0d0) then 
            sumSpec = sumSpec + conc(is)
            prodConc = prodConc - Ys
          endif
        enddo
        sumSpec = sumSpec + prodConc/molW(this%iN2)
        sumSpec =  PresRef/m_rUniversal/temp/sumSpec
        do is = 1,m_nspecies
          conc(is) = conc(is)*sumSpec            !P/Rhat/T*X_i=[]_i (dimensional)
        enddo
      endif

      if(.not.useActivityStandPressure) then
        do is = 1,m_nspecies
          conc(is) = aux(i,is)*massDens2Conc(is)  !dimensional pressure dependent
        enddo
      endif
      prodConc = PresRef/m_rUniversal/temp - sum(conc(1:m_nspecies)) !kmole/m^3

      conc(this%iN2) = prodConc

      if(m_steady > 0) then
        if(this%jO > 0)conc(this%jO) = 1d0  !O
        if(this%jOH> 0)conc(this%jOH) = 1d0  !OH
        if(this%jH2O2> 0)conc(this%jH2O2) = 0d0  !H2O2
      endif
        
!remove negative concentrations
      do is = 1,m_nspecies
        conc(is) = max(conc(is), m_smallNumber)
      enddo
      
      !...3rdbody reactions
      if(has3BR) then
        do ir =1,nreactions
          if(.not. is3BR(ir)) cycle
          tBconc(ir) = 0e0
          do is =1,m_nspecies
            tBconc(ir)=tBconc(ir)+thirdBodyEff(is,ir)*conc(is) 
          enddo
          is = this%iN2
          tBconc(ir)=tBconc(ir)+thirdBodyEff(is,ir)*prodConc
        enddo
      endif

!Fall-off correction
      do ir =1,nreactions
        if(.not. isFO(ir)) cycle
        Pr = k0(ir)%da*exp(-k0(ir)%theta/temp)*temp**(k0(ir)%b)*tBconc(ir)
        Pr = Pr/(da(ir)*exp(-theta(ir)/temp)*temp**(b(ir)))
        alphaT = k0(ir)%fallOff(1)
        rt3 = 1d0/k0(ir)%fallOff(2)
        rt1 = 1d0/k0(ir)%fallOff(3)
        t2 = k0(ir)%fallOff(4)
        Fcent = (1d0 - alphaT) * exp(- temp * rt3) + alphaT * exp(- temp * rt1) + exp(- t2 / temp);
        work = log10(max(Fcent, m_smallNumber));
        lpr = log10(max(Pr, m_smallNumber));
        cc = -0.4d0 - 0.67d0 * (work);
        nn = 0.75d0 - 1.27d0 * (work);
        f1 = (lpr + cc)/ (nn - 0.14d0 * (lpr + cc));
        lgf = (work) / (1d0 + f1 * f1);
        tBconc(ir)= 1d1**lgf * Pr/(1d0+Pr);
      enddo


      do ir =1,nreactions
        kr(ir) = da(ir)*exp(-theta(ir)/temp)*temp**(b(ir))
        if(is3BR(ir).eqv. .true.) kr(ir) = kr(ir)*tBconc(ir)
        kr(ir) = kr(ir)*conc(partners(ir,1))
        if(partners(ir,2) > 0) kr(ir) = kr(ir)*conc(partners(ir,2))
      enddo

      if(m_steady > 0) then
        do k=1,4
          Ajo(k) = sum(AmatJ(:,k)*kr)
          Ako(k) = sum(AmatK(:,k)*kr)
        enddo
        A0 = -Ajo(3)*Ako(1)
        A1 = -Ajo(3)*Ako(3) + Ajo(1)*Ako(4) - Ajo(4)*Ako(1)
        A2 = -Ajo(4)*Ako(3) + Ajo(2)*Ako(4)
!Boivin convention
        A1 = -A1
        A2 = -A2

!solve
        if(A1**2 + 4*A0*A2 < 0d0) call testPrint("Delta < 0d0")
        if(abs(A2) < 1d-18) call testPrint("A2==0")
        WORK =  (-A1 + Sqrt(A1**2 + 4*A0*A2))/(2d0*A2)  !OH

        if(this%jO > 0)conc(this%jO) = (Ajo(1) + Ajo(2)*WORK)/(-Ajo(3) -Ajo(4)*WORK)  !O
        if(this%jOH> 0)conc(this%jOH) = WORK !OH
        if(this%jH2O2> 0)conc(this%jH2O2) = 1d0  !H2O2
        
        
        if(useActivityStandPressure) then
           !disregarding H2O2
           Qaux(this%jOH) = max(min(conc(this%jOH)/massDens2Conc(this%jOH), m_MAX_DENSITY_STEADY_SPECIES),0d0)
           Qaux(this%jO) =  max(min(conc(this%jO)/massDens2Conc(this%jO) ,  m_MAX_DENSITY_STEADY_SPECIES),0d0)
           Call TRhoAux2Pgamma(input%gamref, cv(i,1), Qaux, this, temp)
           temp = dimTempFactor/temp
        endif

        do ir =1,nreactions
          kr(ir) = da(ir)*exp(-theta(ir)/temp)*temp**(b(ir))
          if(is3BR(ir).eqv. .true.) kr(ir) = kr(ir)*tBconc(ir)
          kr(ir) = kr(ir)*conc(partners(ir,1))
          if(partners(ir,2) > 0) kr(ir) = kr(ir)*conc(partners(ir,2))
        enddo
        if(this%jH2O2> 0)conc(this%jH2O2) = sum(AmatL*kr)/(AmLL*kr(iLL(1)))  !H2O2
      
        do is = isteadyB,isteadyE
          ir = is-isteadyB+1
          auxS(i,ir) = conc(is)/massDens2Conc(is)
          !steady state approximation does not coserve mass, limit the max mass !density
          auxS(i,ir) = max(min(auxS(i,ir), m_MAX_DENSITY_STEADY_SPECIES),0d0)
        enddo

      endif
        
!mutliply by the mass matrix => mass prduction rates, MKS dimensions
      omega = MATMUL(massMatrix, kr)

!make omega  dimensionless
      omega = omega / dimDividers !energy,radical,fuel,oxidizer

      do is =1,m_nspecies
        rhsSP(i,is) = rhsSP(i,is) + omega(is)
      enddo

    enddo !i=1,Nx

  contains
    Subroutine testPrint(msg)
      character*(*)::msg
      integer :: ir

      print*,trim(msg)

      print*,"Stage",iRKstage
      print*,"Cell#",i
      print*,"Iblank",iblank(i)
      print'(a,1p123e12.4)','Temp',temp
      print'(a,1p123e12.4)','A0,A1,A2',A0,A1,A2
      print'(a,1p123e12.4)','conc',conc
      print'(a,1p123e12.4)','aux',aux(i,:)
      print'(a,1p123e12.4)','auxS',auxS(i,:)
      print'(a,1p123e12.4)','cv',cv(i,:)
      print'(a,1p123e12.4)','tBconc',tBconc
      print'(a,1p123e12.4)','concProd',(conc(partners(ir,1))*conc(partners(ir,2)),ir = 1,nreactions)

    end Subroutine testPrint

#endif

  End Subroutine Model1Source

  Subroutine Model1Dv(this, grid, state, myrank)
    USE ModGlobal
    USE ModDataStruct
    USE ModMPI
    
    Implicit None
    Type(t_model1), pointer :: this
    TYPE(t_grid), pointer :: grid
    TYPE(t_mixt), pointer :: state
    Integer, intent(in) :: myrank

#ifdef HAVE_CANTERA

    ! ... local variables
    TYPE(t_mixt_input), pointer :: input
    integer :: j, i, ND, Nc, iN2, isteadyB,isteadyE
    real(rfreal) :: ibfac, gamma, gamref, floatT,floatG
    real(rfreal),allocatable :: Qaux(:), Qv(:), Qvs(:)

    ! ...Mixture of Calorically perfect gas:: assume cp/R is constant among species

    ! ... simplicity
    ND = grid%ND
    Nc = grid%nCells
    input => grid%input
    gamref = input%GamRef
    iN2 = this%iN2

    allocate(Qv(ND+2), Qaux(m_nspecies+m_steady+1), Qvs(ND+2))
    Qaux = 0d0
    isteadyB=min(this%jO,this%jOH,this%jH2O2);isteadyE=max(this%jO,this%jOH,this%jH2O2);
    do i = 1, Nc
       ibfac = grid%ibfac(i)

       ! ... adjust original values wrt IBLANK
       state%cv(i,   1) = ibfac * state%cv(i,1) + (1.0_8 - ibfac)
       state%cv(i,ND+2) = ibfac * state%cv(i,ND+2) + (1.0_8 - ibfac) / (gamref - 1.0_8)
       do j = 1, ND
          state%cv(i,j+1) = ibfac * state%cv(i,j+1)
       end do

       ! specific volume
       state%dv(i,3) = 1.0_8 / state%cv(i,1)

       Qv = state%cv(i,1:ND+2)
       Qaux(1:m_nspecies) = state%auxVars(i,1:m_nspecies)
       Qaux(iN2) = Qv(1)
       if(m_steady > 0) then
          Qaux(isteadyB:isteadyE) = state%auxVarsSteady(i,1:m_steady)
       endif

       ! ...temperature
       if(grid%iblank(i) /= 0 ) then
          state%dv(i,2) = poly4Temperature(Qv, Qaux, this, input, state%dv(i,2))
          ! ...pressure
          Call TRhoAux2Pgamma(state%dv(i,2), Qv(1), Qaux, this, state%dv(i,1), state%gv(i,1))
       else
          if(m_steady > 0) then
             state%auxVarsSteady(i,1:m_steady) = 0d0
             Qaux(isteadyB:isteadyE) = 0d0
          endif
          Qvs = removeBondEnergy(Qv, Qaux, this, input)
          state%dv(i,1) = (gamref-1.0_rfreal) * (Qvs(ND+2) - 0.5d0 * sum(Qvs(2:ND+1)**2)/Qvs(1))
          Call TRhoAux2Pgamma(1d0/state%dv(i,1), Qv(1), Qaux, this, floatT, state%gv(i,1))
          state%dv(i,2) = 1d0/floatT
          state%gv(i,1) = input%gamref
       endif

    end do

    do j = 1, input%nAuxVars
       do i = 1, Nc
          ibfac = grid%ibfac(i)
          state%auxVars(i,j) = state%auxVars(i,j) * ibfac
       end do
    end do

#endif

  End Subroutine Model1Dv

  Subroutine Model1Tv(this, grid, state)
    USE ModGlobal
    USE ModDataStruct
    USE ModMPI
#ifdef HAVE_CANTERA
    use cantera ! lower case 'use' to trick makedeps
#endif
    
    Implicit None
    Type(t_model1), pointer :: this
    TYPE(t_grid), pointer :: grid
    TYPE(t_mixt), pointer :: state

#ifdef HAVE_CANTERA
    type(t_NASApolynomials), pointer :: poly(:)
    type(phase_t), pointer :: Cgas
    real(rfreal), dimension(:,:),pointer :: tv,dv,cv,aux, auxS
    real(rfreal), pointer :: mfrac(:), diff(:), MolWeight(:), molW(:)
    integer, pointer :: X2C(:) !mapping
    real(rfreal) :: temp,dens,dmu,dlam,factor
    integer :: i,k,kxpacc,kcantr
    integer :: CkR,CkF,CkO,CkN, numSpecies
    real(rfreal) :: dimTempFactor, dimDensFactor, muref, lambdaref, alpharef
    integer :: L,M, isteadyB,isteadyE
    real(rfreal) :: Tsq,Tcu,Tqu,T5,Tvec(6)

    if(state%combustion%iRKstage > 1) return

    tv => state%tv
    dv => state%dv
    cv => state%cv
    molWeight => state%MolWeight
    molW => this%canteraInterface%molW
    aux => state%auxVars
    auxS => state%auxVarsSteady
    mfrac => this%canteraInterface%mfrac
    diff => this%canteraInterface%diff
    X2C => this%canteraInterface%X2C
    poly => this%polynomials
    CkF = this%canteraInterface%CkF
    CkO = this%canteraInterface%CkO
    CkR = this%canteraInterface%CkR
    CkN = this%canteraInterface%CkN
    Cgas  => this%canteraInterface%Cgas
    alpharef = this%canteraInterface%alpharef
    lambdaref = this%canteraInterface%lambdaref
    muref = this%canteraInterface%muref
    dimTempFactor = this%dimTempFactor
    dimDensFactor = this%dimDensFactor
    numSpecies = this%numSpecies


    if(m_isConstTV .or. this%isInviscid) then;tv=1.0d0;where(grid%iblank==0);tv(:,1)=0d0;tv(:,2)=0d0;endwhere;return;endif

    isteadyB=min(this%jO,this%jOH,this%jH2O2);isteadyE=max(this%jO,this%jOH,this%jH2O2);
    
    mfrac=1d-9
    do i = 1, grid%nCells
      if(grid%iblank(i) == 0) then
        tv(i,:) = 0.0d0
        cycle
      end if

      temp = min(dimTempFactor*dv(i,2),m_MAXTEMP)
                                !!temp = dimTempFactor*dv(i,2)
      dens = dimDensFactor*cv(i,1)
      mfrac = 0d0
      do k = 1,this%numspecies+1
        if(k == this%iN2) cycle
        kcantr = X2C(k)
        if(k>=isteadyB .and. k<= isteadyE .and. m_steady > 0) then
          mfrac(kcantr) = auxS(i,k-isteadyB+1)/cv(i,1)
        else
          mfrac(kcantr) = aux(i,k)/cv(i,1)
        endif
      enddo
      ! ...N2
      mfrac(CkN) = max(1.0d0-sum(mfrac),1d-9)
      

      call setState_TRY(Cgas,temp,dens,mfrac)

      dmu = viscosity(Cgas)
      dlam = thermalConductivity(Cgas)
      MolWeight(i) = meanMolecularWeight(Cgas)
      call getMixDiffCoeffs(Cgas, diff)

      tv(i,1) =dmu/muref
      ! ... lambda (bulk viscosity = 0.60 mu for air)
      ! ... lambda = (mu'/mu -2/3) mu
      tv(i,2) = (0.60_rfreal - 2.0_rfreal/3.0_rfreal) * state%tv(i,1)
      tv(i,3) = dlam/lambdaref

      factor = dens/alpharef/MolWeight(i)
      do k = 1,m_nspecies
        kcantr = X2C(k)
        tv(i,3+k) = factor*diff(kcantr)*molW(kcantr)
      enddo
!
      L = merge(1,2, temp<= 1d3)
      M = 4+m_nspecies
      Tsq = temp*temp
      Tcu = Tsq*temp
      Tqu = Tcu*temp
      T5 =  Tqu*temp
      Tvec = [temp,Tsq,Tcu,Tqu,T5,1d0]
      do k = M,M+m_nspecies-1
        tv(i,k) = sum(poly(k - M +1)%CH(1:6,L)*Tvec)  !(h_k-h_N2)/Pscale*Rscale
      enddo

    enddo
#endif
  End Subroutine Model1Tv

 Subroutine Model1FixSpecies(this, aux,cv, dv, input,auxS)

    USE ModGlobal
    USE ModDataStruct
    USE ModMPI

    Implicit None
!real(rfreal), dimension(:,:),intent(IN) :: cv
    real(rfreal), dimension(:,:),intent(INOUT) :: aux, cv, dv
    real(rfreal), dimension(:,:),intent(IN), optional :: auxS
    Type(t_mixt_input), pointer :: input
    Type (t_model1), pointer :: this

#ifdef HAVE_CANTERA

! ... local variables
    real(rfreal),allocatable :: Qaux(:), Qv(:)
    integer :: i, k, Nx,Nvars,imomM, ND, isteadyB, isteadyE
    real(rfreal) :: SSUM, temp, MaxTemp,dimTempFactor, press, dens
    logical :: changed

!do nothing
    if(m_nspecies == 0 .or. .not. doSpeciesFix) return

    dimTempFactor = this%dimTempFactor
    isteadyB=min(this%jO,this%jOH,this%jH2O2);
    isteadyE=max(this%jO,this%jOH,this%jH2O2);

    
    ND = input%ND
    allocate(Qv(ND+2), Qaux(m_nspecies+m_steady+1))

    Nx = ubound(aux,1)
    Nvars = ubound(aux,2)
    imomM = ubound(cv,2) - 1
    do i = 1, Nx
       if(cv(i,1) <= 0.0d0)cv(i,1)=m_smallNumber
       temp = dv(i,2) * dimTempFactor
       
       changed = .false.
       do k = 1, Nvars
          if (aux(i,k) < -m_smallNumber) then
             aux(i,k) = aux(i,k)*m_fixFactor
             changed = .true.
          endif
       enddo

       Qaux(1:m_nspecies) = aux(i,1:m_nspecies)
       if(present(auxS) .and. m_steady>0 ) then
         Qaux(isteadyB:isteadyE) = auxS(i,1:m_steady)
       endif
       if(temp < 273.20d0) then
          dv(i,2) = dv(i,2) - (dv(i,2) - 273.2d0/dimTempFactor)*(1d0-m_fixFactor)
          changed = .true.
       endif
       if(changed) then
         dens = TPAux2Rho(dv(i,2), dv(i,1), Qaux, this)
         cv(i,2:ND+1) = cv(i,2:ND+1)/cv(i,1)*dens
         cv(i,1) =dens
         cv(i,ND+2) = TRhoAux2Energy(dv(i,2), cv(i,1), Qaux, this) +  0.5d0 * sum(cv(i,2:ND+1)**2)/cv(i,1)
       endif
    end do

#endif

  End Subroutine Model1FixSpecies

#ifdef HAVE_CANTERA

 Subroutine BroadcastPtr(ptr, gridComm)

    USE ModGlobal
    USE ModDataStruct
    USE ModMPI

    Implicit None
    real(rfreal), pointer :: ptr(:)
    integer :: length, myrank, gridComm, ierr

    Call MPI_COMM_RANK(gridComm, myrank, ierr)
    length=0
    if(myrank == 0) length = ubound(ptr,1)
    Call MPI_BCAST(length, 1, MPI_INTEGER, 0, gridComm, ierr)
    if(myrank /= 0) allocate(ptr(length))
    if (length > 0) then
      Call MPI_BCAST(ptr, length, MPI_DOUBLE_PRECISION, 0, gridComm, ierr)
    endif
  end Subroutine BroadcastPtr

  Subroutine BroadcastPtr2(ptr, gridComm)

    USE ModGlobal
    USE ModDataStruct
    USE ModMPI

    Implicit None
    real(rfreal), pointer :: ptr(:,:)
    integer :: length(2), myrank, gridComm, ierr

    Call MPI_COMM_RANK(gridComm, myrank, ierr)
    length=0
    if(myrank == 0) length = shape(ptr)
    Call MPI_BCAST(length, 2, MPI_INTEGER, 0, gridComm, ierr)
    if(myrank /= 0) allocate(ptr(length(1),length(2)))
    if (length(1)*length(2) > 0) then
      Call MPI_BCAST(ptr, length(1)*length(2), MPI_DOUBLE_PRECISION, 0, gridComm, ierr)
    endif 
  end Subroutine BroadcastPtr2

  Subroutine BroadcastPtrL(ptr, gridComm)

    USE ModGlobal
    USE ModDataStruct
    USE ModMPI

    Implicit None
    logical, pointer :: ptr(:)
    integer :: length, myrank, gridComm, ierr

    Call MPI_COMM_RANK(gridComm, myrank, ierr)
    length=0
    if(myrank == 0) length = ubound(ptr,1)
    Call MPI_BCAST(length, 1, MPI_INTEGER, 0, gridComm, ierr)
    if(myrank /= 0) allocate(ptr(length))
    if (length > 0) then
      Call MPI_BCAST(ptr, length, MPI_LOGICAL, 0, gridComm, ierr)
    endif
  end Subroutine BroadcastPtrL

  Subroutine BroadcastPtrC(ptr, gridComm)

    USE ModGlobal
    USE ModDataStruct
    USE ModMPI

    Implicit None
    character(len=10),dimension(:),pointer :: ptr
    integer :: length, i, myrank, gridComm, ierr

    Call MPI_COMM_RANK(gridComm, myrank, ierr)
    length=0
    if(myrank == 0) length = ubound(ptr,1)
    Call MPI_BCAST(length, 1, MPI_INTEGER, 0, gridComm, ierr)
    if(myrank /= 0) allocate(ptr(length))
    do i=1, length
      Call MPI_BCAST(ptr(i), 10, MPI_CHARACTER, 0, gridComm, ierr)
    enddo
  end Subroutine BroadcastPtrC

  Subroutine BroadcastPtrKF0(ptr, gridComm)

    USE ModGlobal
    USE ModDataStruct
    USE ModMPI

    Implicit None
    type(t_kF0),  pointer, dimension(:)  :: ptr
    integer :: length, j, myrank, gridComm, ierr
    logical haveFallOff

    Call MPI_COMM_RANK(gridComm, myrank, ierr)
    length=0
    if(myrank == 0) length = ubound(ptr,1)
    Call MPI_BCAST(length, 1, MPI_INTEGER, 0, gridComm, ierr)
    if(myrank /= 0) allocate(ptr(length))
    do j=1, length
      Call MPI_BCAST(ptr(j)%E, 1, MPI_DOUBLE_PRECISION, 0, gridComm, ierr)
      Call MPI_BCAST(ptr(j)%b, 1, MPI_DOUBLE_PRECISION, 0, gridComm, ierr)
      Call MPI_BCAST(ptr(j)%Da, 1, MPI_DOUBLE_PRECISION, 0, gridComm, ierr)
      Call MPI_BCAST(ptr(j)%theta, 1, MPI_DOUBLE_PRECISION, 0, gridComm, ierr)

      haveFallOff = .false.
      if(myrank == 0) then
        if(size(ptr(j)%fallOff) /= 0) haveFallOff = .true.
      endif
      Call MPI_BCAST(haveFallOff, 1, MPI_LOGICAL, 0, gridComm, ierr)
      if(haveFallOff) then
        Call BroadcastPtr(ptr(j)%FallOff, gridComm)
      else
        if (myrank /= 0) allocate(ptr(j)%Falloff(0))
      endif
    enddo
  end Subroutine BroadcastPtrKF0

  Subroutine BroadcastPtrPolyCoeff(ptr, gridComm)

    USE ModGlobal
    USE ModDataStruct
    USE ModMPI

    Implicit None
    type(t_NASApolynomials),  pointer, dimension(:)  :: ptr
    integer :: i, length, myrank, gridComm, ierr

    Call MPI_COMM_RANK(gridComm, myrank, ierr)
    length=0
    if(myrank == 0) length = ubound(ptr,1)
    Call MPI_BCAST(length, 1, MPI_INTEGER, 0, gridComm, ierr)
    if(myrank /= 0) allocate(ptr(length))
    do i=1, length
      call BroadcastPtr2(ptr(i)%coeffs, gridComm)
    enddo
  end Subroutine BroadcastPtrPolyCoeff

function removeBondEnergy(Qin, Qaux, this, input, sgn) result(Qout)
    USE ModGlobal
    USE ModDataStruct
    USE ModXMLCantera

    Implicit None

    real(rfreal) :: Qin(:),Qaux(:), C6, Ein
    real(rfreal),INTENT(IN), OPTIONAL :: sgn
    real(rfreal) :: Qout(size(Qin)), Eout
    type(t_model1), pointer, INTENT(IN) :: this
    type(t_mixt_input), pointer, intent(in) :: input
    integer :: i,M

    Ein = Qin(input%ND +2)
    C6 = 0d0
    M = ubound(Qaux,1)
    do i = 1, this%numspecies+1
      if(i <= M .and. i /= this%iN2) C6 = C6 + this%polynomials(i)%Delta*Qaux(i)
      if(i == this%iN2) C6 = C6 + this%polynomials(i)%Delta*Qin(1)
    enddo
    if(present(sgn)) C6 = C6*sgn
    Eout = Ein - C6
    Qout(1:input%ND +1) = Qin(1:input%ND +1)
    Qout(input%ND +2) = Eout
    !!if(Eout < 0d0) print'("Eout<0",1p123e12.4)',Qin, Qaux,Eout

  end function removeBondEnergy

 function poly4Temperature(Q,Qaux,this,input,Tguess,Flag_in,printVal_in) result(Tout)
    USE ModGlobal
    USE ModDataStruct
    use PolynomialRoots
    real(rfreal) :: Q(:),Qaux(:), Tguess, coeSum(7,2), Tout
    real(rfreal) :: coe(6), Tout4, rhoe, s3, third,twothirds,twototwothirds,  s6, error, Tout2, Tout3
    complex*16 :: sols(4)
    logical,intent(in),optional :: Flag_in, printVal_in
    logical :: Flag, success, printVal,useNewton
    type(t_model1), pointer, INTENT(IN) :: this
    type(t_mixt_input), pointer, intent(in) :: input
    integer :: n,i, k, M
    type(t_NASApolynomials), pointer :: poly(:)

    !...Tguess & Tout are nondimensional!
    
    if(present(Flag_in)) then
      Flag = Flag_in
    else
      Flag = .false.
    end if
    if(present(PrintVal_in)) then
      PrintVal = PrintVal_in
    else
      PrintVal = .false.
    end if
!  
    poly => this%polynomials
!   
    M = ubound(Qaux,1)
    coeSum = 0d0
    do i = 1, this%numspecies+1
      if(i <= M .and. i /= this%iN2) coeSum = coeSum  + poly(i)%cMod*Qaux(i)
      if(i == this%iN2) coeSum = coeSum  + poly(i)%cMod*Q(1)
    enddo

    k=input%ND + 1
    rhoe = Q(k+1) - 0.5_rfreal * sum(Q(2:k)**2)/Q(1)
    
    if(Flag) then
      s6 = 0d0
      do i = 1, this%numspecies+1
        if(i <= M .and. i /= this%iN2) s6 = s6  + poly(i)%delta*Qaux(i)
        if(i == this%iN2) s6 = s6  + poly(i)%delta*Q(1)
      enddo
      Q(k + 1) = rhoe - s6
    endif


    coeSum(6,:) = coeSum(6,:) - rhoe

    Tout = Tguess*this%dimTempFactor
    success = .false.
    useNewton = .false.
    do i = 1,30
      if(Tout <= 1d3) coe = coeSum (1:6,1)
      if(Tout > 1d3)  coe = coeSum (1:6,2)

!linearization
      Tout2 = Tout*Tout
      Tout3 = Tout2*Tout
      Tout4 = Tout2*Tout2
      if(i < 5 .and. .not. useNewton) then
!coe(6) = coe(6) - 4d0*coe(5)*Tout4*Tout
!coe(1) = coe(1) + 5d0*coe(5)*Tout4
         coe(6) = coe(6) + coe(5)*Tout4*Tout
         coe(1) = coe(1) - 5d0*coe(5)*Tout4
         coe(2) = coe(2) + 1d1*coe(5)*Tout3
         coe(3) = coe(3) - 1d1*coe(5)*Tout2
         coe(4) = coe(4) + 5d0*coe(5)*Tout
         
         
         call QuarticRoots([coe(6),coe(1),coe(2),coe(3),coe(4)],sols)
         
      
         k = sum(minloc(abs(aimag(sols)),real(sols)>=m_minTemperature .and. real(sols)<2d4))
         if(k == 0) then
            if(printVal) then
               print'("Cycling",2i3,1p123e12.4)',i,k,Tout,real(sols), aimag(sols)
               print'(2i4,1p123e12.4)',i,k,Tguess*this%dimTempFactor, rhoe, Qaux, TRhoAux2Energy(Tguess,Q(1),Qaux,this)
            endif
            Tout = merge(1001d0,999d0,Tout <= 1d3)
            cycle
         endif
         if(abs(aimag(sols(k))) > 1d-3) then
            useNewton = .true.
            cycle
         endif
         error = abs(Tout -sols(k))
         Tout = real(sols(k))
      else
         error = sum(coe(1:6)*[Tout,Tout2,Tout3,Tout4,Tout3*Tout2,1d0])/sum(coe(1:5)*[1d0,2d0*Tout,3d0*Tout2,4d0*Tout3,5d0*Tout4])
         Tout = Tout - error
      end if
      if(printVal) then
        print'(2i4,1p123e16.8)',i,k,sols(k),Tout,Tguess*this%dimTempFactor, rhoe, Qaux, TRhoAux2Energy(Tguess,Q(1),Qaux,this),error,  sum(coe(1:6)*[Tout,Tout2,Tout3,Tout3*Tout,Tout3*Tout2,1d0])
        if(Tout <= 1d3) coe = coeSum (1:6,1)
        if(Tout > 1d3)  coe = coeSum (1:6,2)
        Tout2 = Tout*Tout
        Tout3 = Tout2*Tout
        error = sum(coe(1:6)*[Tout,Tout2,Tout3,Tout3*Tout,Tout3*Tout2,1d0])
        if(i > 25) print*,">>",error,coe(1:6)
      endif
      if(abs(error/Tout) < 1d-6 .and. i > 2) then
        if(Tout <= 1d3) coe = coeSum (1:6,1)
        if(Tout > 1d3)  coe = coeSum (1:6,2)
        Tout2 = Tout*Tout
        Tout3 = Tout2*Tout
        error = sum(coe(1:6)*[Tout,Tout2,Tout3,Tout3*Tout,Tout3*Tout2,1d0])
        if(abs(error) < 1d-6) then
          success = .true.
          EXIT
        endif
      endif
    enddo
    if(.not. success .or. Tout < m_minTemperature) then
      Tout = max(Tguess*this%dimTempFactor, m_minTemperature)
    endif
    Tout = Tout / this%dimTempFactor
    
    return
  end function poly4Temperature

    
  function TRhoAux2Energy(TIn,rhoIN, Qaux, this,RemoveBEDefaultF_in,DimensionalTempDefaultF_in) result(Eout)
    USE ModGlobal
    USE ModDataStruct
    USE ModXMLCantera

    Implicit None
    real(rfreal) :: Tin, Tout, Tout2, Tout3, rhoIN, Eout, Qaux(:), Coesum(6), s6
    type(t_model1), pointer, INTENT(IN) :: this
    integer :: i, L, M
    logical,intent(in),optional :: RemoveBEDefaultF_in, DimensionalTempDefaultF_in
    logical :: RemoveBEDefaultF,  DimensionalTempDefaultF
    type(t_NASApolynomials), pointer :: poly(:)

    if(present(RemoveBEDefaultF_in)) then
      RemoveBEDefaultF = RemoveBEDefaultF_in
    else
      RemoveBEDefaultF = .false.
    end if
    if(present(DimensionalTempDefaultF_in)) then
      DimensionalTempDefaultF = DimensionalTempDefaultF_in
    else
      DimensionalTempDefaultF = .false.
    end if
    
    poly => this%polynomials
    
    Tout = Tin
    if(.not. DimensionalTempDefaultF) Tout = Tout*this%dimTempFactor

    M = ubound(Qaux,1)
    L = merge(1,2, Tout<= 1d3)
    coeSum = 0d0
    do i = 1, this%numspecies+1
      if(i <= M .and. i /= this%iN2) coeSum = coeSum  + poly(i)%cMod(1:6,L)*Qaux(i)
      if(i == this%iN2)              coeSum = coeSum  + poly(i)%cMod(1:6,L)*rhoIN
    enddo

    Tout2 = Tout*Tout
    Tout3 = Tout2*Tout
!
    Eout = sum(coeSum(1:6)*[Tout,Tout2,Tout3,Tout3*Tout,Tout3*Tout2,1d0])
    if(RemoveBEDefaultF) then
      s6 = 0d0
      do i = 1, this%numspecies+1
        if(i <= M .and. i /= this%iN2) s6 = s6 + poly(i)%delta*Qaux(i)
        if(i == this%iN2) s6 = s6 + poly(i)%delta*rhoIN
      enddo
      Eout = Eout-s6
    endif

  end function TRhoAux2Energy

  
  subroutine TRhoAux2Pgamma(TIn,rhoIN, Qaux, this, pOut, gammaOut)
    USE ModGlobal
    USE ModDataStruct
    USE ModXMLCantera

    Implicit None
    real(rfreal),intent(OUT), Optional :: gammaOut
    real(rfreal) :: Tin, Tout, Tout2, Tout3, rhoIN, pOut, Qaux(:), Coesum(6), Rsum
    type(t_model1), pointer, INTENT(IN) :: this
    integer :: i, L, M
    type(t_NASApolynomials), pointer :: poly(:)
    real(rfreal), pointer :: Rcoeff(:)


    Tout = Tin*this%dimTempFactor
!  
    poly => this%polynomials
    Rcoeff => this%Rcoeff
!

    !coeSum \gets rho cp_i
    !RSum \gets rho R
    M = ubound(Qaux,1)
    L = merge(1,2, Tout<= 1d3)
    coeSum = 0d0;Rsum=0d0
    do i = 1,this%numspecies+1
      if(i <= M .and. i/=  this%iN2) then
        coeSum = coeSum + poly(i)%cp(1:6,L)*Qaux(i)
        Rsum   = Rsum   + Rcoeff(i)*Qaux(i)
      endif
      if(i ==  this%iN2) then
        coeSum = coeSum + poly(i)%cp(1:6,L)*rhoIN
        Rsum   = Rsum   + Rcoeff(i)*rhoIN
      endif
    enddo
    

    Tout2 = Tout*Tout
    Tout3 = Tout2*Tout

    pOut = Rsum*Tout ! pout \gets pressure
    if(present(gammaOut)) then
      gammaOut = sum(coeSum(1:5)*[Tout,Tout2,Tout3,Tout3*Tout,Tout3*Tout2])/pOut  !rho*cp*T / rho*R*T
      gammaOut = gammaOut/(gammaOut-1d0)  ! gammaout \gets cp/R/(cp/R-1)
    endif

  end subroutine TRhoAux2Pgamma

  
  function TPAux2Rho(TIn,Pin, Qaux, this, rhoIn) result(rhoOut)
    USE ModGlobal
    USE ModDataStruct
    USE ModXMLCantera

    Implicit None
    real(rfreal) :: rhoOut
    real(rfreal), intent(IN), Optional :: rhoIn
    real(rfreal) :: Tin, Pin, Qaux(:), Rsum, RN2, Tout
    type(t_model1), pointer, INTENT(IN) :: this
    integer :: i, L, M
    type(t_NASApolynomials), pointer :: poly(:)
    real(rfreal), pointer :: Rcoeff(:)

!  
    poly => this%polynomials
    Rcoeff => this%Rcoeff
!
    Tout = Tin*this%dimTempFactor

    !RSum \gets rho R
    M = ubound(Qaux,1)
    Rsum=0d0;RN2=1d0
    do i = 1,this%numspecies+1
      if(i <= M .and. i/=  this%iN2) then
        Rsum   = Rsum   + Rcoeff(i)*Qaux(i)
      endif
    enddo
    RN2   = Rcoeff(this%iN2)

    
    if(present(rhoIn)) then
      rhoOut = Pin/Tout/(Rsum/rhoIn + RN2)
    else
      rhoOut = (Pin/Tout - Rsum)/RN2
    endif

  end function TPAux2Rho

#endif
  
End Module ModModel1
