! Copyright (c) 2015, University of Illinois at Urbana-Champaign, XPACC
! License: MIT, http://opensource.org/licenses/MIT
!-----------------------------------------------------------------------
!
! ModInput.f90
! 
! - read the input file
!
! Revision history
! - 20 Dec 2006 : DJB : initial code
! - 17 May 2007 : DJB : evolution (added gridType & metricType)
! - 10 Jul 2007 : DJB : initial CVS commit
!
! $Header: /cvsroot/genx/Codes/RocfloCM/Source/ModParam.f90,v 1.17 2011/05/30 18:46:43 bodony Exp $
!
!-----------------------------------------------------------------------
module ModParam

  use ModGlobal
  use ModMPI
  use ModDataStruct

  integer, parameter :: NUMERICAL_PARAM_TYPE = 1
  integer, parameter :: LOGICAL_PARAM_TYPE = 2
  integer, parameter :: STRING_PARAM_TYPE = 3
  integer, parameter :: NO_PARAM_TYPE = 4
  integer, parameter :: MAX_PARAM_LEN = 64
  integer, parameter, private :: MAX_LINE_LEN = 256
  integer, parameter, private :: MAX_RECURSION_LEVEL = 8
  integer, parameter, private :: MAX_TOKEN_LEN = 64
  integer, parameter, private :: MAX_TOKEN_COUNT = 32

contains

  subroutine init_param(myrank, first_param_ptr, input_filename)

    Use ModDataStruct

    implicit none

    integer :: myrank
    type(t_param), pointer, intent(inout) :: first_param_ptr
    character(len=*), intent(in) :: input_filename
    ! -----------------------------

    !if (myrank == 0) &
    !	write(*,*) 'in init_param...'

    call read_param_file(myrank, input_filename, first_param_ptr)

  end subroutine init_param

  subroutine destroy_param(first_param_ptr)

    Use ModDataStruct

    implicit none

    type(t_param), pointer, intent(inout) :: first_param_ptr
    
    if (associated(first_param_ptr)) then
      call destroy_param_recursive(first_param_ptr)
      deallocate(first_param_ptr)
    end if

  contains

    recursive subroutine destroy_param_recursive(param)

      Use ModDataStruct

      implicit none

      type(t_param), intent(inout) :: param

      ! -------------------------------------

      if (param%nextInit) then
        call destroy_param_recursive(param%next)
        deallocate(param%next)
      end if
      deallocate(param%argv)

    end subroutine destroy_param_recursive

  end subroutine destroy_param

  function get_new_param(first_param_ptr) result(param)

    Use ModDataStruct

    implicit none

    type(t_param), pointer, intent(inout) :: first_param_ptr
    type(t_param), pointer :: param

    ! ---------------------------------

    param => first_param_ptr
    do while (param%nextInit)
      param => param%next
    end do
    allocate(param%next)
    param%nextInit = .true.
    param => param%next

    param%line = char(0)
    param%argc = 0
    param%request_count = 0
    NULLIFY(param%argv)
    param%nextInit = .false.
    NULLIFY(param%next)

  end function get_new_param

  recursive subroutine read_param_file(myrank, filename, first_param_ptr)

    implicit none

    integer :: myrank
    character (len=*), intent(in) :: filename
    type(t_param), pointer, intent(inout) :: first_param_ptr

    ! ----------------------------------

    integer, parameter :: CHAR_BUFFER_LEN = 1024
    integer, parameter :: IACHAR_TAB = 9
    integer, parameter :: IACHAR_LF = 10
    integer, parameter :: IACHAR_CR = 13
    integer, parameter :: IACHAR_SPACE = IACHAR(' ')
    integer, parameter :: IACHAR_COMMA = IACHAR(',')
    integer, parameter :: IACHAR_EQUALS = IACHAR('=')
    integer, parameter :: IACHAR_SEMICOLON = IACHAR(';')
    integer, parameter :: IACHAR_HASH = IACHAR('#')
    integer, parameter :: IACHAR_QUOTE = IACHAR('"')

    integer :: fh,ierr,pos,max_pos,previous_max_pos,status(MPI_STATUS_SIZE),last_ierr 
    character(len=MAX_TOKEN_LEN) :: token
    character :: char_buffer(CHAR_BUFFER_LEN)
    character(len=MAX_LINE_LEN) :: line
    integer :: argc,argv(MAX_TOKEN_COUNT+1)
    character(len=PATH_LENGTH) :: mpi_filename
    character(len=PATH_LENGTH) :: c_filename
    logical :: comment_mode, at_end_of_file

    ! --------------------------------

!    if (myrank == 0) &
!         write(*,*) 'in read_param_file ',trim(filename),'...'

    ! increase and check recursion_level...
    first_param_ptr%recursion_level = first_param_ptr%recursion_level + 1
    if (first_param_ptr%recursion_level > MAX_RECURSION_LEVEL) then
      call graceful_exit(myrank, 'Error: increase MAX_RECURSION_LEVEL, or check includes.')
    end if

    ! build the mpi_filename...
    if (filename(1:1)=='"') then
      ierr = len_trim(filename)
      ! check...
      if (filename(ierr:ierr) /= '"') then
        write(*,*) 'PlasComCM: Error: expected double quote at end: ',trim(filename)
        call graceful_exit(myrank, 'Error: expected double quote at end: '//trim(filename))
      end if
      call build_mpi_filename(mpi_filename,filename(2:ierr-1))
    else
      call build_mpi_filename(mpi_filename,filename)
    end if

    ierr = len_trim(mpi_filename)
    c_filename = mpi_filename
    c_filename(ierr+1:ierr+1) = char(0)

!#ifdef MPI2
!    call MPI_FILE_OPEN(mycomm, mpi_filename, &
!         MPI_MODE_RDONLY, MPI_INFO_NULL, fh, ierr)
!#else
    if (myrank == 0) then
       write (*,'(A)') 'PlasComCM: WARNING :: USING HACK for MPI2 open, read, close in ModParam.f90.  Check your results! <<<<<<<<'
    endif
    fh = 59;
!!$    open (unit=fh, file=mpi_filename(1:len_trim(mpi_filename)), status='old', access='direct', form='formatted', recl=1, iostat=ierr)
    open (unit=fh, file=mpi_filename(1:len_trim(mpi_filename)), status='old', form='formatted', iostat=ierr)
!#endif
    
    if (ierr == 0 .and. myrank == 0) write (*,'(A)') 'PlasComCM: Successfully opened "'//mpi_filename(1:len_trim(mpi_filename))//'"'
    if (ierr /= 0) then
       if (myrank == 0) &
           call graceful_exit(myrank, 'PlasComCM: Could not find/open inputfile. skipping.')
      return
    endif

    pos = 0
    max_pos = 0 ! pos = max_pos will force a read right away
    previous_max_pos = 0
    argc = 0
    argv(1) = 1 ! always
    comment_mode = .FALSE.
    at_end_of_file = .FALSE.
    last_ierr = 0

    token_loop: do
      select case(get_next_token(c_filename, previous_max_pos, at_end_of_file, last_ierr))
      case (-1)
        exit token_loop

      case (0)
        ! normal token...
        if (argc == 0) then
          line = trim(token)
        else
          line = trim(line)//' '//trim(token)
        end if
        argc = argc + 1
        argv(argc+1) = len_trim(line) + 2

      case (1)

        ! concluding token...
        if (argc == 0) then
          line = trim(token)
        else
          line = trim(line)//' '//trim(token)
        end if
        argc = argc + 1
        argv(argc+1) = len_trim(line) + 2

        ! check for include...
        if (( index(trim(line),'include ') == 1 ).OR. &
             ( index(trim(line),'INCLUDE ') == 1 )) then
          if (argc /= 2) then
            write(*,*) 'Error: expect only one filename with include.'
            call graceful_exit(myrank, 'Error: expect only one filename with include.')
          end if
          call read_param_file(myrank, line(argv(2):argv(3)-2), first_param_ptr)
        else
          call add_param_line(line, argc, argv, first_param_ptr)
        end if

        ! reset...
        argc = 0

      case (2)

        ! just concluding...
        if (argc > 0) then

          ! check for include...
          if (( index(trim(line),'include ') == 1 ).OR. &
               ( index(trim(line),'INCLUDE ') == 1 )) then
            if (argc /= 2) then
              write(*,*) 'Error: expect only one filename with include.'
              call graceful_exit(myrank, 'Error: expect only one filename with include.')
            end if
            call read_param_file(myrank, line(argv(2):argv(3)-2), first_param_ptr)
          else
            call add_param_line(line, argc, argv, first_param_ptr)
          end if

          ! reset...
          argc = 0

        end if

      end select

    end do token_loop

    ! note that this does not recover memory on SGI for some reason...	
!#ifdef MPI2
!    call MPI_FILE_CLOSE(fh,ierr)
!#else
    close(fh)
!#endif

    ! return recursion_level...
    first_param_ptr%recursion_level = first_param_ptr%recursion_level - 1

  contains

    function get_next_token(c_filename, previous_max_pos, at_end_of_file, last_ierr) result(flag)

      implicit none

      character(len=PATH_LENGTH) :: c_filename
      integer :: flag, previous_max_pos, last_ierr
      logical :: at_end_of_file

      ! -------------------------------------

      integer :: token_pos,ic,i,iat_end_of_file
      logical :: quote_mode

      ! -------------------------------------

      flag = 0
      token_pos = 0
      quote_mode = .FALSE.

      ! fill the whole token with 0, so we can just return when we are done...
      token = char(0)

      do while(.TRUE.)

        do while (pos < max_pos)

          ! check the character at pos+1...
          ic = IACHAR(char_buffer(pos+1))
          !write(*,*) 'next character: ',char_buffer(pos+1),ic

          if (quote_mode) then

            ! quote_mode overrides the interpretation of all characters
            ! except another quote...
            pos = pos + 1
            token_pos = token_pos + 1
            if (token_pos >= MAX_TOKEN_LEN) then
              write(*,*) 'Error: increase MAX_TOKEN_LEN.'
              call graceful_exit(myrank, 'Error: increase MAX_TOKEN_LEN.')
            end if
            token(token_pos:token_pos) = char_buffer(pos)
            if (ic == IACHAR_QUOTE) &
                 return

          else if (comment_mode) then

            ! comment_mode ends with the next return...
            pos = pos + 1
            if (ic == IACHAR_LF .or. ic == IACHAR_CR) &
                 comment_mode = .FALSE.

          else

            select case (ic)
            case (IACHAR_QUOTE)
              ! a quote should start a token...
              if (token_pos /= 0) then
                write(*,*) 'Error: quote not at token_pos == 0.'
                call graceful_exit(myrank, 'Error: quote not at token_pos == 0.')
              end if
              pos = pos + 1
              token_pos = 1
              token(token_pos:token_pos) = '"' ! include the quote in the returned token
              quote_mode = .TRUE.
            case (IACHAR_SPACE,IACHAR_COMMA,IACHAR_TAB,IACHAR_EQUALS)
              ! white space...
              if (token_pos > 0) then
                ! token is complete, no need to read this space on the next call...
                pos = pos + 1
                return
              else
                ! just skip...
                pos = pos + 1
              end if
            case (IACHAR_LF,IACHAR_CR,IACHAR_SEMICOLON)
              ! completes line...
              if (token_pos > 0) then
                ! token is complete, no need to read this space on the next call...
                pos = pos + 1
                flag = 1
                return
              else
                pos = pos + 1
                flag = 2
                return
              end if
            case (IACHAR_HASH)
              !write(*,*) 'got a hash.'
              comment_mode = .TRUE.
              if (token_pos > 0) then
                ! token is complete, so return it...
                pos = pos + 1
                flag = 1
                return
              else
                pos = pos + 1
                flag = 2
                return
              end if
            case default
              ! a normal character...
              pos = pos + 1
              token_pos = token_pos + 1
              if (token_pos >= MAX_TOKEN_LEN) then
                write(*,*) 'Error: increase MAX_TOKEN_LEN.'
                call graceful_exit(myrank, 'Error: increase MAX_TOKEN_LEN.')
              end if
              token(token_pos:token_pos) = char_buffer(pos)
            end select
          end if

        end do

!#ifdef MPI2
!        call MPI_FILE_READ(fh, char_buffer, CHAR_BUFFER_LEN, MPI_CHARACTER, status, ierr)
!        call MPI_GET_COUNT(status, MPI_CHARACTER, max_pos, ierr)
!#else
!!$        max_pos = 0
!!$        if (.not.at_end_of_file) then
!!$          do i = 1, CHAR_BUFFER_LEN
!!$            read (unit=fh,fmt='(A)',rec=previous_max_pos+i,iostat=ierr,err=130) char_buffer(i)
!!$            if (ierr == 0) max_pos = max_pos + 1
!!$          end do
!!$          previous_max_pos = previous_max_pos + max_pos
!!$        end if
!!$  130   if (max_pos /= CHAR_BUFFER_LEN) at_end_of_file = .true.
!        Call readline(fh, max_pos, CHAR_BUFFER_LEN, char_buffer, at_end_of_file, last_ierr)
        Call readbuffer(c_filename, previous_max_pos, max_pos, CHAR_BUFFER_LEN, char_buffer, &
          iat_end_of_file)
        previous_max_pos = previous_max_pos + max_pos
        if (max_pos /= CHAR_BUFFER_LEN) at_end_of_file = .true.
!#endif
        pos = 0 ! reset pos
        if (max_pos == 0) then
          if (token_pos > 0) then
            ! assume this completes the token, so return, 
            ! and next time we will return FALSE...
            flag = 1
            return
          else
            flag = -1
            return
          end if
        end if

      end do

    end function get_next_token

  end subroutine read_param_file

  subroutine add_param_line(line, argc, argv, first_param_ptr)

    Use ModDataStruct

    implicit none

    character(len=*), intent(in) :: line
    integer, intent(in) :: argc,argv(:)
    type(t_param), pointer, intent(inout) :: first_param_ptr
    type(t_param), pointer :: param

    ! -------------------------------------
    ! -------------------------------------

    if (first_param_ptr%lineInit) then
      param => get_new_param(first_param_ptr)
    else
      first_param_ptr%lineInit = .true.
      param => first_param_ptr
    end if

    param%line = line
    param%argc = argc
    allocate(param%argv(argc+1))
    param%argv(1:argc+1) = argv(1:argc+1)

  end subroutine add_param_line

  function get_param(name, first_param_ptr) result(param)

    Use ModDataStruct

    implicit none

    character(len=*), intent(in) :: name
    type(t_param), pointer, intent(inout) :: first_param_ptr
    type(t_param), pointer :: param

    ! --------------------------------

    param => first_param_ptr
    do while (associated(param)) 
      if ( (index(param%line,name) == 1) .AND. &
           (len_trim(name) == param%argv(2)-2) ) then
        param%request_count = param%request_count + 1
        return
      end if
      if (param%nextInit) then
        param => param%next
      else
        return
      end if
    end do

  end function get_param

  subroutine set_next_param(param,name)

    Use ModDataStruct

    implicit none

    type(t_param), pointer :: param
    character(len=*), intent(in) :: name

    ! --------------------------------

    if (.NOT. associated(param)) &
         return

    param => param%next
    do while (associated(param)) 
      if ( (index(param%line,name) == 1) .AND. &
           (len_trim(name) == param%argv(2)-2) ) then
        param%request_count = param%request_count + 1
        return
      end if
      param => param%next
    end do

  end subroutine set_next_param

  function get_param_type(param,iarg) result(param_type)

    Use ModDataStruct

    implicit none

    type(t_param), intent(in) :: param
    integer, intent(in) :: iarg
    integer :: param_type

    ! --------------------------------------

    integer :: ierr,test_integer,argv_f,argv_l
    real(WP) :: test_real_wp
    logical :: test_logical

    ! --------------------------------------

    ! make sure there are enough args...
    if (iarg >= param%argc) then
      param_type = NO_PARAM_TYPE
      return
    end if

    argv_f = param%argv(iarg+1)
    argv_l = param%argv(iarg+2)-2
    !do i = argv_f,argv_l
    !	write(*,*) 'param%line(i:i), IACHAR(param%line(i:i)) = ', &
    !		param%line(i:i), IACHAR(param%line(i:i))
    !end do

    ! logical is strictly '.TRUE.' or '.FALSE.'

    ! try real...
    read(param%line(argv_f:),*,iostat=ierr) test_real_wp
    if ((ierr == 0).AND.(IACHAR(param%line(argv_f:argv_f))<65)) then
      param_type = NUMERICAL_PARAM_TYPE
      return
    end if

    ! must be a string...
    param_type = STRING_PARAM_TYPE

  end function get_param_type

  subroutine dump_param(first_param_ptr)

    Use ModDataStruct

    implicit none

    type(t_param), pointer :: first_param_ptr
    type(t_param), pointer :: param

    write(*,*) '******************* params ***********************'
    param => first_param_ptr
    do while (associated(param))
      !write(*,*) trim(param%line),param%argc,param%argv(1:param%argc+1)
      write(*,*) trim(param%line)
      param => param%next
    end do
    write(*,*) '**************** end of params *******************'

  end subroutine dump_param

  subroutine dump_param_usage(myrank, first_param_ptr)

    USE ModMPI
    Use ModDataStruct

    implicit none

    integer :: myrank
    type(t_param), pointer, intent(inout) :: first_param_ptr
    
    ! -------------------------------

    integer :: pcount, ierr
    type(t_param), pointer :: param
    integer, allocatable :: my_used(:),used(:)

    ! -------------------------------

    pcount = 0
    param => first_param_ptr
    do while (associated(param))
      pcount = pcount + 1
      param => param%next
    end do

    if (pcount > 0) then

      allocate(my_used(pcount))
      my_used(1:pcount) = 0

      pcount = 0
      param => first_param_ptr
      do while (associated(param))
        pcount = pcount + 1
        my_used(pcount) = param%request_count
        param => param%next
      end do

      if (myrank == 0) &
           allocate(used(pcount))

      call MPI_REDUCE(my_used,used,pcount,MPI_INTEGER,MPI_MAX,0,mycomm,ierr)

    end if

    if (myrank == 0) then

      write(*,*) '***************** param usage ********************'

      pcount = 0
      param => first_param_ptr
      do while (associated(param))
        pcount = pcount + 1
        write(*,*) trim(param%line(param%argv(1):param%argv(2)-2)),', max request_count = ', &
             used(pcount)
        param => param%next
      end do

      write(*,*) '************** end of param usage ****************'

      if (pcount > 0) &
           deallocate(used)

    end if

    if (pcount > 0) &
         deallocate(my_used)

  end subroutine dump_param_usage

  logical function check_param(name, first_param_ptr)

    use ModString
    Use ModDataStruct

    implicit none

    character (len=*),intent(in) :: name
    type(t_param), pointer, intent(inout) :: first_param_ptr

    ! -------------------------------

    type(t_param), pointer :: param

    ! -------------------------------

    param => first_param_ptr
    do while (associated(param))
      if ( string_compare( param%line(1:param%argv(2)-2),name) ) then
        check_param = .TRUE.
        param%request_count = param%request_count + 1
        return
      end if
      param => param%next
    end do

    check_param = .FALSE.
    return

  end function check_param

  logical function get_logical_param(myrank, name, first_param_ptr, default)

    use ModString
    Use ModDataStruct

    implicit none

    integer, intent(in) :: myrank
    character (len=*),intent(in) :: name
    type(t_param), pointer, intent(inout) :: first_param_ptr
    logical, intent(in), optional :: default

    ! ----------------------------------------

    integer :: ierr
    type(t_param), pointer :: param

    ! ----------------------------------------

    param => first_param_ptr
    do while (associated(param))
      if ( string_compare( param%line(1:param%argv(2)-2),name) ) then

        if (param%argc == 1) then
          ! the param is present, but there is no
          ! associated value, so assume true...
          get_logical_param = .TRUE.
        else
          read(param%line(param%argv(2):),*,iostat=ierr) get_logical_param
          if (ierr /= 0) then
            !write(*,*) 'Format for param "',trim(name),'" does not match requested logical.'
            call graceful_exit(myrank, 'Format for param "'//trim(name)//'" does not match requested logical.')
          end if
        end if

        param%request_count = param%request_count + 1
        return

      end if
      param => param%next
    end do

    ! if not found, and no default, FALSE is assumed...
    if (present(default) .eqv. .true.) then
      get_logical_param = default
    else
      get_logical_param = .FALSE.
    end if

  end function get_logical_param

  integer function get_integer_param(myrank, name, first_param_ptr, default)

    use ModString
    Use ModDataStruct

    implicit none

    integer, intent(in) :: myrank
    character (len=*),intent(in) :: name
    type(t_param), pointer, intent(inout) :: first_param_ptr
    integer, intent(in), optional :: default

    ! ----------------------------------------

    integer :: ierr
    type(t_param), pointer :: param

    ! ----------------------------------------

    param => first_param_ptr
    do while (associated(param))

      if ( string_compare( param%line(1:param%argv(2)-2),name) ) then

        if (param%argc == 1) then
          write(*,*) 'Error: param "',trim(name),'" has no associated value.'
          call graceful_exit(myrank, 'Error: param "'//trim(name)//'" has no associated value.')
        end if

        ! ... check for TRUE, FALSE, PLANE_PERIODIC
        if (string_compare(param%line(param%argv(2):),'TRUE')) then
          get_integer_param = TRUE
          return
        else if (string_compare(param%line(param%argv(2):),'FALSE')) then
          get_integer_param = FALSE
          return
        else if (string_compare(param%line(param%argv(2):),'PLANE_PERIODIC')) then
          get_integer_param = PLANE_PERIODIC
          return       
        else if (string_compare(param%line(param%argv(2):),'CONSTANT_CFL')) then
          get_integer_param = CONSTANT_CFL
          return
        else if (string_compare(param%line(param%argv(2):),'CONSTANT_DT')) then
          get_integer_param = CONSTANT_DT
          return
        else if (string_compare(param%line(param%argv(2):),'SAMPLE_TIME')) then
          get_integer_param = SAMPLE_TIME
          return
        else if (string_compare(param%line(param%argv(2):),'ITERATION')) then
          get_integer_param = ITERATION
          return
        else if (string_compare(param%line(param%argv(2):),'EXPLICIT_RK4')) then
          get_integer_param = EXPLICIT_RK4
          return
        else if (string_compare(param%line(param%argv(2):),'EXPLICIT_RK5')) then
          get_integer_param = EXPLICIT_RK5
          return
        else if (string_compare(param%line(param%argv(2):),'IMEX_RK4')) then
          get_integer_param = IMEX_RK4
          return
        else if (string_compare(param%line(param%argv(2):),'EXPLICIT_ARK2_RK4')) then
          get_integer_param = EXPLICIT_ARK2_RK4
          return
        else if (string_compare(param%line(param%argv(2):),'APPROXIMATE_LINEAR_OPERATOR')) then
          get_integer_param = APPROXIMATE_LINEAR_OPERATOR
          return
        else if (string_compare(param%line(param%argv(2):),'APPROXIMATE_LINEAR_OPERATOR_POWER_METHOD')) then
          get_integer_param = APPROXIMATE_LINEAR_OPERATOR_POWER_METHOD
          return
        else if (string_compare(param%line(param%argv(2):),'IMPLICIT')) then
          get_integer_param = IMPLICIT
          return
        else if (string_compare(param%line(param%argv(2):),'BDF_RK5')) then
          get_integer_param = BDF_RK5
          return
        else if (string_compare(param%line(param%argv(2):),'EXPLICIT')) then
          get_integer_param = EXPLICIT
          return
        else if (string_compare(param%line(param%argv(2):),'DEFAULT')) then
          get_integer_param = DEFAULT_VALUE
          return
        else if (string_compare(param%line(param%argv(2):),'MONOLITHIC')) then
          get_integer_param = MONOLITHIC
          return        
        else if (string_compare(param%line(param%argv(2):),'CHIMERA')) then
          get_integer_param = CHIMERA
          return
        else if (string_compare(param%line(param%argv(2):),'RIGID')) then
          get_integer_param = RIGID
          return
        else if (string_compare(param%line(param%argv(2):),'DEFORMING')) then
          get_integer_param = DEFORMING
          return
        else if (string_compare(param%line(param%argv(2):),'NATIVE_SBI')) then
          get_integer_param = NATIVE_SBI
          return
        else if (string_compare(param%line(param%argv(2):),'NONORTHOGONAL_WEAKFORM')) then
          get_integer_param = NONORTHOGONAL_WEAKFORM
          return
        else if (string_compare(param%line(param%argv(2):),'NONORTHOGONAL_WEAKFORM_OPT')) then
          get_integer_param = NONORTHOGONAL_WEAKFORM_OPT
          return
        else if (string_compare(param%line(param%argv(2):),'NONORTHOGONAL_STRONGFORM')) then
          get_integer_param = NONORTHOGONAL_STRONGFORM
          return
	else if (string_compare(param%line(param%argv(2):),'STRONGFORM_NARROW')) then
          get_integer_param = STRONGFORM_NARROW
          return
	else if (string_compare(param%line(param%argv(2):),'STANDARD_EULER')) then
          get_integer_param = STANDARD_EULER
          return
	else if (string_compare(param%line(param%argv(2):),'SKEW_SYMMETRIC')) then
          get_integer_param = SKEW_SYMMETRIC
          return
        else if (string_compare(param%line(param%argv(2):),'CARTESIAN')) then
          get_integer_param = CARTESIAN
          return
        else if (string_compare(param%line(param%argv(2):),'SLAB')) then
          get_integer_param = SLAB
          return
        else if (string_compare(param%line(param%argv(2):),'CUBE')) then
          get_integer_param = CUBE
          return
        else if (string_compare(param%line(param%argv(2):),'BELLERO')) then
          get_integer_param = BELLERO
          return
        else if (string_compare(param%line(param%argv(2):),'OGEN')) then
          get_integer_param = OGEN
          return
        else if (string_compare(param%line(param%argv(2):),'ROCSMASH')) then
          get_integer_param = ROCSMASH
          return
        else if (string_compare(param%line(param%argv(2):),'CNS')) then
          get_integer_param = CNS
          return
        else if (string_compare(param%line(param%argv(2):),'LINCNS')) then
          get_integer_param = LINCNS
          return
        else if (string_compare(param%line(param%argv(2):),'Q1D')) then
          get_integer_param = Q1D
          return
        else if (string_compare(param%line(param%argv(2):),'CYL1D')) then
          get_integer_param = CYL1D
          return
        else if (string_compare(param%line(param%argv(2):),'PROCESSOR_IO')) then
          get_integer_param = PROCESSOR_IO
          return
        else if (string_compare(param%line(param%argv(2):),'BDF_GMRES_IMPLICIT')) then
          get_integer_param = BDF_GMRES_IMPLICIT
          return
        else if (string_compare(param%line(param%argv(2):),'BDF_GMRES_SEMIIMPLICIT')) then
          get_integer_param = BDF_GMRES_SEMIIMPLICIT
          return
        else if (string_compare(param%line(param%argv(2):),'PIECE_CNST')) then
          get_integer_param = PIECE_CNST
          return
        else if (string_compare(param%line(param%argv(2):),'MINMOD')) then
          get_integer_param = MINMOD
          return
        else if (string_compare(param%line(param%argv(2):),'PPM')) then
          get_integer_param = PPM
          return
        else if (string_compare(param%line(param%argv(2):),'PQM')) then
          get_integer_param = PQM
          return
        else if (string_compare(param%line(param%argv(2):),'THIRD_ORDER_MUSCL')) then
          get_integer_param = THIRD_ORDER_MUSCL
          return
        else if (string_compare(param%line(param%argv(2):),'LLF')) then
          get_integer_param = LLF
          return
        else if (string_compare(param%line(param%argv(2):),'HLLC')) then
          get_integer_param = HLLC
          return
        else if (string_compare(param%line(param%argv(2):),'WENO')) then
          get_integer_param = WENO
          return
        else if (string_compare(param%line(param%argv(2):),'ENO')) then
          get_integer_param = ENO
          return
        else if (string_compare(param%line(param%argv(2):),'OVERLAP_PERIODIC')) then
          get_integer_param = OVERLAP_PERIODIC
          return
        else if (string_compare(param%line(param%argv(2):),'NO_OVERLAP_PERIODIC')) then
          get_integer_param = NO_OVERLAP_PERIODIC
          return
        else if (string_compare(param%line(param%argv(2):),'PSE')) then
          get_integer_param = PSE
          return
        else if (string_compare(param%line(param%argv(2):),'FUNCTIONAL_SOUND')) then
          get_integer_param = FUNCTIONAL_SOUND
          return
        else if (string_compare(param%line(param%argv(2):),'CONTROL_MASS')) then
          get_integer_param = CONTROL_MASS
          return
        else if (string_compare(param%line(param%argv(2):),'CONTROL_BODY_FORCE_X')) then
          get_integer_param = CONTROL_BODY_FORCE_X
          return
        else if (string_compare(param%line(param%argv(2):),'CONTROL_BODY_FORCE_Y')) then
          get_integer_param = CONTROL_BODY_FORCE_Y
          return
        else if (string_compare(param%line(param%argv(2):),'CONTROL_BODY_FORCE_Z')) then
          get_integer_param = CONTROL_BODY_FORCE_Z
          return
        else if (string_compare(param%line(param%argv(2):),'CONTROL_BODY_FORCE_XY')) then
          get_integer_param = CONTROL_BODY_FORCE_XY
          return
        else if (string_compare(param%line(param%argv(2):),'CONTROL_BODY_FORCE_YZ')) then
          get_integer_param = CONTROL_BODY_FORCE_YZ
          return
        else if (string_compare(param%line(param%argv(2):),'CONTROL_BODY_FORCE_ZX')) then
          get_integer_param = CONTROL_BODY_FORCE_ZX
          return
        else if (string_compare(param%line(param%argv(2):),'CONTROL_BODY_FORCE_XYZ')) then
          get_integer_param = CONTROL_BODY_FORCE_XYZ
          return
        else if (string_compare(param%line(param%argv(2):),'CONTROL_BODY_FORCE_R')) then
          get_integer_param = CONTROL_BODY_FORCE_R
          return
        else if (string_compare(param%line(param%argv(2):),'CONTROL_BODY_FORCE_THETA')) then
          get_integer_param = CONTROL_BODY_FORCE_THETA
          return
        else if (string_compare(param%line(param%argv(2):),'CONTROL_INTERNAL_ENERGY')) then
          get_integer_param = CONTROL_INTERNAL_ENERGY
          return
        else if (string_compare(param%line(param%argv(2):),'CONTROL_UNCONSTRAINED')) then
          get_integer_param = CONTROL_UNCONSTRAINED
          return
        else if (string_compare(param%line(param%argv(2):),'CONTROL_CONSTRAINED_SPACE')) then
          get_integer_param = CONTROL_CONSTRAINED_SPACE
          return
        else if (string_compare(param%line(param%argv(2):),'CONTROL_CONSTRAINED_TIME')) then
          get_integer_param = CONTROL_CONSTRAINED_TIME
          return        
        else if (string_compare(param%line(param%argv(2):),'GMRES')) then
          get_integer_param = GMRES
          return
        else if (string_compare(param%line(param%argv(2):),'DIRECT_PENTA')) then
          get_integer_param = DIRECT_PENTA
          return
       else if (string_compare(param%line(param%argv(2):),'QUADRATIC')) then
          get_integer_param = QUADRATIC
          return
        else if (string_compare(param%line(param%argv(2):),'LINEAR')) then
          get_integer_param = LINEAR
          return
        else if (string_compare(param%line(param%argv(2):),'STEADY_SOLN')) then
          get_integer_param = STEADY_SOLN
          return
        else if (string_compare(param%line(param%argv(2):),'DYNAMIC_SOLN')) then
          get_integer_param = DYNAMIC_SOLN
          return
        else if (string_compare(param%line(param%argv(2):),'SVK')) then
          get_integer_param = SVK
          return
        else if (string_compare(param%line(param%argv(2):),'NH')) then
          get_integer_param = NH
          return
        else if (string_compare(param%line(param%argv(2):),'NONE')) then
          get_integer_param = FALSE
          return
        else if (string_compare(param%line(param%argv(2):),'ONESTEP')) then
          get_integer_param = ONESTEP
          return
        else if (string_compare(param%line(param%argv(2):),'MODEL0')) then
          get_integer_param = MODEL0
          return
        else if (string_compare(param%line(param%argv(2):),'MODEL1')) then
          get_integer_param = MODEL1
          return
        end if

        read(param%line(param%argv(2):),*,iostat=ierr) get_integer_param
        if (ierr /= 0) then
          write(*,*) 'Format for param "',trim(name),'" does not match requested integer.'
          call graceful_exit(myrank, 'Format for param "'//trim(name)//'" does not match requested integer.')
        end if

        param%request_count = param%request_count + 1
        return

      end if
      param => param%next
    end do

    if (present(default) .eqv. .true.) then
      get_integer_param = default
    else
      write(*,*) 'Error: could not find param "',name,'"'
      call graceful_exit(myrank, 'Error: could not find param "'//name//'"')
    end if

  end function get_integer_param

  real(WP) function get_real_param(myrank, name, first_param_ptr, default)

    use ModString
    Use ModDataStruct

    implicit none

    integer, intent(in) :: myrank
    character (len=*),intent(in) :: name
    type(t_param), pointer, intent(inout) :: first_param_ptr
    real(WP), intent(in), optional :: default

    ! ----------------------------------------

    integer :: ierr
    type(t_param), pointer :: param

    ! ----------------------------------------

    param => first_param_ptr
    do while (associated(param))
      if ( string_compare( param%line(1:param%argv(2)-2),name) ) then

        if (param%argc == 1) then
          write(*,*) 'Error: param "',trim(name),'" has no associated value.'
          call graceful_exit(myrank, 'Error: param "'//trim(name)//'" has no associated value.')
        end if

        read(param%line(param%argv(2):),*,iostat=ierr) get_real_param
        if (ierr /= 0) then
          write(*,*) 'Format for param "',trim(name),'" does not match requested real.'
          call graceful_exit(myrank, 'Format for param "'//trim(name)//'" does not match requested real.')
        end if

        param%request_count = param%request_count + 1
        return

      end if
      param => param%next
    end do

    if (present(default) .eqv. .true.) then
      get_real_param = default
    else
      write(*,*) 'Error: could not find param "',name,'"'
      call graceful_exit(myrank, 'Error: could not find param "'//name//'"')
    end if

  end function get_real_param

  subroutine set_param(myrank, value, name, first_param_ptr, index, default)

    use ModString
    Use ModDataStruct

    implicit none

    integer, intent(in) :: myrank
    character(len=*), intent(out) :: value
    character(len=*), intent(in) :: name
    type(t_param), pointer, intent(inout) :: first_param_ptr
    integer, intent(in), optional :: index
    character(len=*), intent(in), optional :: default 

    ! -----------------------------------------

    type(t_param), pointer :: param
    integer :: index_copy
    character(len=80) :: str

    ! ----------------------------------------
    
    if (present(index) .eqv. .true.) then
      index_copy = index
    else
      ! choose 2 by default - i.e. the first token
      ! after the keyword...
      index_copy = 2
    end if

    param => first_param_ptr
    do while (associated(param))
      if ( string_compare( param%line(1:param%argv(2)-2),name) ) then
        if (param%argc < index_copy) then
          write(str,*) 'Error: param "',trim(name),'" has no value associated with index: ',index_copy
          call graceful_exit(myrank, trim(str))
        end if

        value = param%line(param%argv(index_copy):param%argv(index_copy+1)-2)

        param%request_count = param%request_count + 1			
        return

      end if
      param => param%next
    end do

    if (present(default) .eqv. .true.) then
      value = default
    else
      write(*,*) 'Error: could not find param "',name,'"'
      call graceful_exit(myrank, 'Error: could not find param "'//name//'"')
    end if

  end subroutine set_param

  subroutine set_param_token(myrank, value, param, index)

    use ModString
    Use ModDataStruct

    implicit none

    integer :: myrank
    character(len=*), intent(out) :: value
    type(t_param), intent(in) :: param
    integer, intent(in), optional :: index

    ! -----------------------------------------

    integer :: index_copy
    character(len=80) :: str

    ! ----------------------------------------

    if (present(index) .eqv. .true.) then
      index_copy = index
    else
      ! choose 2 by default - i.e. the first token
      ! after the keyword...
      index_copy = 2
    end if

    if (param%argc < index_copy) then
      write(str,*) 'Error: param "',trim(param%line), &
           '" has no value associated with index: ',index_copy
      call graceful_exit(myrank, trim(str))
    else
      value = param%line(param%argv(index_copy):param%argv(index_copy+1)-2)
      return
    end if

  end subroutine set_param_token

  Subroutine readLine(fh, lineLength, buffer_len, buffer, at_end_of_file, last_ierr)

    Implicit None

    ! ... global variables  
    Integer :: fh, lineLength, buffer_len, last_ierr
    Character :: buffer(buffer_len)
    Logical :: at_end_of_file

    ! ... local variables
    Integer :: ierr, pos
    Character :: single_char
    Character(LEN=1024) :: message

    lineLength = 0;
    at_end_of_file = .FALSE.

    ! ... add in to keep Cray compiler from complaining message is uninitialized
    write (message,'(A)') 'WARNING: default message not changed on iomsg error.'

    do while ( .true. )
      read (fh, '(A)', ADVANCE='NO', iostat=ierr, iomsg=message, end=100) single_char
      print *, ierr, single_char, last_ierr, lineLength
      lineLength = lineLength + 1
      if (ierr == 0) then
        buffer(lineLength) = single_char
        last_ierr = ierr
      end if
      if (ierr /= 0) print *, message
      if (ierr < 0) then
        buffer(lineLength) = NEW_LINE('A')
        last_ierr = ierr
      end if
      if (IS_IOSTAT_END(ierr)) then
        last_ierr = ierr
        at_end_of_file = .TRUE.
        return
      end if
      if (lineLength == buffer_len) return
    end do
100 at_end_of_file = .TRUE.

    return

  End Subroutine readLine

end module ModParam
