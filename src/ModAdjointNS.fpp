! Copyright (c) 2015, University of Illinois at Urbana-Champaign, XPACC
! License: MIT, http://opensource.org/licenses/MIT
!----------------------------------------------------------------------
!
! ModAdjointNS.f90
! 
! - module solving adjoint N-S equations
!
! Revision history
! - 05 Jun 2009 : JKim : initial code
! 
! flags == FALSE    : compute inviscid fluxes only
! flags == TRUE     : compute dissipative fluxes only
! flags == COMBINED : inviscid + dissipative fluxes
!
!-----------------------------------------------------------------------
MODULE ModAdjointNS

CONTAINS

  SUBROUTINE AdjNS_RHS(region, ng, flags)

    USE ModGlobal
    USE ModDataStruct
    USE ModDeriv
    USE ModMPI

    Implicit None

    ! ... subroutine arguments
    type(t_region), pointer :: region
    integer :: flags, ng

    ! ... local variables and arrays
    type(t_grid), pointer :: grid
    type(t_mixt), pointer :: state
    type(t_mixt_input), pointer :: input
    type(t_mpi_timings), pointer :: mpi_timings
    real(rfreal) :: timer
    integer :: ND, Nc, NVARS, N(MAX_ND)
    integer :: i, j, k, l, m, ii
    real(rfreal) :: n_of_PowerLaw
    real(rfreal), pointer :: UVW(:,:), UVWhat(:,:)
    real(rfreal), pointer :: drStardx(:,:), duStardx(:,:), dpStardx(:,:)
    real(rfreal), pointer :: tau_ij(:,:), tauStar_ij(:,:)

    ! ... simplicity
    grid  => region%grid(ng)
    state => region%state(ng)
    input => grid%input
    mpi_timings => region%mpi_timings(ng)
    ND = grid%ND
    Nc = grid%nCells
    NVARS = state%NVARS
    N = 1
    do j = 1, grid%ND
      N(j) = grid%ie(j)-grid%is(j)+1
    end do ! j

    n_of_PowerLaw = 0.666_rfreal

    ! ... allocate space for arrays
    nullify(UVW); allocate(UVW(Nc,ND)); UVW = 0.0_rfreal
    nullify(UVWhat); allocate(UVWhat(Nc,ND)); UVWhat = 0.0_rfreal
    nullify(drStardx); allocate(drStardx(Nc,ND)); drStardx = 0.0_rfreal
    nullify(duStardx); allocate(duStardx(Nc,ND*ND)); duStardx = 0.0_rfreal
    nullify(dpStardx); allocate(dpStardx(Nc,ND)); dpStardx = 0.0_rfreal
    if (state%RE /= 0.0_rfreal) then
      nullify(tau_ij); allocate(tau_ij(Nc,ND*(ND+1)/2)); tau_ij = 0.0_rfreal
      nullify(tauStar_ij); allocate(tauStar_ij(Nc,ND*(ND+1)/2)); tauStar_ij = 0.0_rfreal
    end if ! flags

    ! ... Step 0: precompute necessary terms
    do i = 1, ND ! ... primitive velocity of the forward problem
      do ii = 1, Nc
        UVW(ii,i) = state%cv(ii,region%global%vMap(i+1)) * state%dv(ii,3)
      end do ! ii
    end do ! i
    do l = 1, ND ! ... contravariant velocity w/o unsteady term
      do ii = 1, Nc
        UVWhat(ii,l) = 0.0_rfreal
      end do ! ii
      do j = 1, ND
        do ii = 1, Nc
          UVWhat(ii,l) = UVWhat(ii,l) + UVW(ii,j) * grid%MT1(ii,region%global%t2Map(l,j))
        end do ! ii
      end do ! j
    end do ! l
    if (state%RE /= 0.0_rfreal) then
      ! ... compute viscous stress tensor of the forward problem in the Cartesian space
      do i = 1, ND ! ... velocity gradient tensor of the forward problem
        do j = 1, ND
          do ii = 1, Nc
            state%flux(ii) = UVW(ii,i)
            state%dflux(ii) = 0.0_rfreal
          end do ! ii
          Call FIRST_DERIV(region, ng, j, state%flux, state%dflux, 1.0_rfreal, .FALSE.)
          do ii = 1, Nc
            duStardx(ii,region%global%t2Map(i,j)) = state%dflux(ii)
          end do ! ii
        end do ! j
      end do ! i
      do i = 1, ND ! ... shear stress tensor
        do j = i, ND ! ... tensor symmetry considered
          do ii = 1, Nc
            tau_ij(ii,region%global%t2MapSym(i,j)) = state%tv(ii,1) * (duStardx(ii,region%global%t2Map(i,j)) + duStardx(ii,region%global%t2Map(j,i)))
          end do ! ii
        end do ! j
      end do ! i
      do ii = 1, Nc
        state%flux(ii) = 0.0_rfreal ! ... velocity divergence of the forward problem
      end do ! ii
      do k = 1, ND
        do ii = 1, Nc
          state%flux(ii) = state%flux(ii) + duStardx(ii,region%global%t2Map(k,k))
        end do ! ii
      end do ! k
      do i = 1, ND ! ... viscous stress tensor of the forward problem
        do ii = 1, Nc
          tau_ij(ii,region%global%t2MapSym(i,i)) = tau_ij(ii,region%global%t2MapSym(i,i)) + state%tv(ii,2) * state%flux(ii) ! ... bulk viscosity
        end do ! ii
      end do ! i
      do j = 1, ND*ND
        do i = 1, Nc
          duStardx(i,j) = 0.0_rfreal ! ... defensive programming
        end do ! ii
      end do ! j
    end if ! state%RE

    ! ... gradient of adjoint variables in a computational coordinate
    do j = 1, ND ! ... gradient of adjoint density
      do ii = 1, Nc
        state%flux(ii) = state%av(ii,1)
        state%dflux(ii) = 0.0_rfreal
      end do ! ii
      Call APPLY_OPERATOR(region, ng, input%iFirstDeriv, j, state%flux, state%dflux, .FALSE.)
      do ii = 1, Nc
        drStardx(ii,j) = state%dflux(ii)
      end do ! ii

      Call Save_Patch_Deriv(region, ng, 1, j, state%dflux, .true.)
    end do ! j
    do i = 1, ND ! ... gradient tensor of adjoint velocity
      do j = 1, ND
        do ii = 1, Nc
          state%flux(ii) = state%av(ii,i+1)
          state%dflux(ii) = 0.0_rfreal
        end do ! ii
        Call APPLY_OPERATOR(region, ng, input%iFirstDeriv, j, state%flux, state%dflux, .FALSE.)
        do ii = 1, Nc
          duStardx(ii,region%global%t2Map(i,j)) = state%dflux(ii)
        end do ! ii

        Call Save_Patch_Deriv(region, ng, i+1, j, state%dflux, .true.)
      end do ! j
    end do ! i
    do j = 1, ND ! ... gradient of adjoint pressure
      do ii = 1, Nc
        state%flux(ii) = state%av(ii,NVARS)
        state%dflux(ii) = 0.0_rfreal
      end do ! ii
      Call APPLY_OPERATOR(region, ng, input%iFirstDeriv, j, state%flux, state%dflux, .FALSE.)
      do ii = 1, Nc
        dpStardx(ii,j) = state%dflux(ii)
      end do ! ii

      Call Save_Patch_Deriv(region, ng, NVARS, j, state%dflux, .true.)
    end do ! j
    
    ! Populate GradAv for shape optimization
!    DO j = 1,ND
!       state%GradAv(:,1,j) = drStardx(:,j)
!       state%GradAv(:,NVARS,j) = dpStardx(:,j)
!       DO i = 1,ND
!          state%GradAv(:,i+1,j) = duStardx(:,region%global%t2Map(j,i))
!       ENDDO
!    ENDDO
    




    ! ... Step 1: inviscid fluxes of adjoint density equation
    do ii = 1, Nc
      state%flux(ii) = 0.0_rfreal
    end do ! ii
    do i = 1, ND
      do ii = 1, Nc
        state%flux(ii) = state%flux(ii) + UVW(ii,i)**2
      end do ! ii
    end do ! i
    do ii = 1, Nc
      state%flux(ii) = 0.5_rfreal * state%flux(ii) ! ... specific kinetic energy
    end do ! ii
    do l = 1, ND

      do ii = 1, Nc
        state%dflux(ii) = 0.0_rfreal
      end do ! ii
      do i = 1, ND
        do ii = 1, Nc
          state%dflux(ii) = state%dflux(ii) + UVW(ii,i) * duStardx(ii,region%global%t2Map(i,l))
        end do ! ii
      end do ! i

      ! ... derivative in the xi_l direction
      do ii = 1, Nc
        state%dflux(ii) = UVWhat(ii,l) * &
              (drStardx(ii,l) + state%dflux(ii) + state%flux(ii)*dpStardx(ii,l))

        state%rhs(ii,1) = state%rhs(ii,1) - grid%JAC(ii) * state%dflux(ii)
      end do ! ii

      Call Save_Patch_Deriv(region, ng, 1, l, state%dflux)

    end do ! l





    ! ... Step 2: inviscid fluxes of adjoint velocity equations
    do i = 1, ND
      do l = 1, ND

        do ii = 1, Nc
          state%dflux(ii) = 0.0_rfreal
        end do ! ii
        do j = 1, ND
          do ii = 1, Nc
            state%dflux(ii) = state%dflux(ii) + UVW(ii,j) * duStardx(ii,region%global%t2Map(j,l))
          end do ! ii
        end do ! j

        ! ... derivative in the xi_l direction
        do ii = 1, Nc
          state%dflux(ii) = grid%MT1(ii,region%global%t2Map(l,i)) * ( state%cv(ii,1)*drStardx(ii,l) + &
                                                        state%cv(ii,1)*state%dflux(ii) + &
                                                       (state%cv(ii,NVARS) + state%dv(ii,1))*dpStardx(ii,l)) + &
                            state%cv(ii,1)*UVWhat(ii,l)*( duStardx(ii,region%global%t2Map(i,l)) + UVW(ii,i)*dpStardx(ii,l) )

          state%rhs(ii,i+1) = state%rhs(ii,i+1) - grid%JAC(ii) * state%dflux(ii)
        end do ! ii

        Call Save_Patch_Deriv(region, ng, i+1, l, state%dflux)

      end do ! l
    end do ! i





    ! ... Step 3: inviscid fluxes of adjoint pressure equation
    do l = 1, ND

      do ii = 1, Nc
        state%dflux(ii) = 0.0_rfreal
      end do ! ii
      do j = 1, ND
        do ii = 1, Nc
          state%dflux(ii) = state%dflux(ii) + grid%MT1(ii,region%global%t2Map(l,j))*duStardx(ii,region%global%t2Map(j,l))
        end do ! ii
      end do ! j

      ! ... derivative in the xi_l direction
      do ii = 1, Nc
        state%dflux(ii) = state%dflux(ii) + &
                          state%gv(ii,1)/(state%gv(ii,1) - 1.0_rfreal) * UVWhat(ii,l) * dpStardx(ii,l)

        state%rhs(ii,NVARS) = state%rhs(ii,NVARS) - grid%JAC(ii) * state%dflux(ii)
      end do ! ii

      Call Save_Patch_Deriv(region, ng, NVARS, l, state%dflux)

    end do ! l





    ! ... Step 4: subtract time-varying metric portions
    ! ... (1) adjoint pressure equation first
    do ii = 1, Nc
      state%flux(ii) = 0.0_rfreal
    end do ! ii
    do l = 1, ND
      do ii = 1, Nc
        state%flux(ii) = state%flux(ii) + grid%XI_TAU(ii,l)*dpStardx(ii,l)
      end do ! ii
    end do ! l
    do ii = 1, Nc
      state%rhs(ii,NVARS) = state%rhs(ii,NVARS) - grid%JAC(ii)*state%flux(ii)/(state%gv(ii,1) - 1.0_rfreal)
    end do ! ii
    ! ... (2) adjoint velocity equations
    do i = 1, ND
      do ii = 1, Nc
        state%flux(ii) = 0.0_rfreal
      end do ! ii
      do l = 1, ND
        do ii = 1, Nc
          state%flux(ii) = state%flux(ii) + grid%XI_TAU(ii,l) * (duStardx(ii,region%global%t2Map(i,l)) + UVW(ii,i)*dpStardx(ii,l))
        end do ! ii
      end do ! l
      do ii = 1, Nc
        state%rhs(ii,i+1) = state%rhs(ii,i+1) - grid%JAC(ii)*state%flux(ii)*state%cv(ii,1)
      end do ! ii
    end do ! i
    ! ... (3) adjoint density equation
    do l = 1, ND
      do ii = 1, Nc
        state%flux(ii) = drStardx(ii,l)
      end do ! ii
      do i = 1, ND
        do ii = 1, Nc
          state%flux(ii) = state%flux(ii) + UVW(ii,i)*duStardx(ii,region%global%t2Map(i,l)) + 0.5_rfreal*(UVW(ii,i)**2)*dpStardx(ii,l)
        end do ! ii
      end do ! i
      do ii = 1, Nc
        state%rhs(ii,1) = state%rhs(ii,1) - grid%JAC(ii)*grid%XI_TAU(ii,l)*state%flux(ii)
      end do ! ii
    end do ! l





    ! ... Step 5: 
    ! ... Now that all inviscid fluxes are computed and their boundary values 
    ! ... are stored, viscous fluxes are computed.  Before doing that, gradient 
    ! ... of adjoint variables are recomputed in the Cartesian coordinate.
    ! ... UVWhat is used to store intermediate results since viscous terms are 
    ! ... computed as if in the Cartesian coordinate.
    if (state%RE /= 0.0_rfreal) then
      do j = 1, ND
        do i = 1, Nc
          UVWhat(i,j) = 0.0_rfreal ! ... adjoint density gradient
        end do ! i
      end do ! j
      do j = 1, ND
        do l = 1, ND
          do ii = 1, Nc
            UVWhat(ii,j) = UVWhat(ii,j) + grid%MT1(ii,region%global%t2Map(l,j)) * drStardx(ii,l)
          end do ! ii
        end do ! l
      end do ! j
      do j = 1, ND
        do ii = 1, Nc
          drStardx(ii,j) = grid%JAC(ii) * UVWhat(ii,j)
        end do ! ii
      end do ! j

      do i = 1, ND
        do j = 1, ND
          do ii = 1, Nc
            UVWhat(ii,j) = 0.0_rfreal
          end do ! ii
        end do ! j
        do j = 1, ND
          do l = 1, ND
            do ii = 1, Nc
              UVWhat(ii,j) = UVWhat(ii,j) + grid%MT1(ii,region%global%t2Map(l,j)) * duStardx(ii,region%global%t2Map(i,l))
            end do ! ii
          end do ! l
        end do ! j
        do j = 1, ND
          do ii = 1, Nc
            duStardx(ii,region%global%t2Map(i,j)) = grid%JAC(ii) * UVWhat(ii,j)
          end do ! ii
        end do ! j
      end do ! i

      do j = 1, ND
        do i = 1, Nc
          UVWhat(i,j) = 0.0_rfreal ! ... adjoint pressure gradient
        end do ! i
      end do ! j
      do j = 1, ND
        do l = 1, ND
          do ii = 1, Nc
            UVWhat(ii,j) = UVWhat(ii,j) + grid%MT1(ii,region%global%t2Map(l,j)) * dpStardx(ii,l)
          end do ! ii
        end do ! l
      end do ! j
      do j = 1, ND
        do ii = 1, Nc
          dpStardx(ii,j) = grid%JAC(ii) * UVWhat(ii,j)
        end do ! ii
      end do ! j
    end if ! state%RE





    ! ... Step 6: "viscous stress tensor" of the adjoint problem
    if (state%RE /= 0.0_rfreal) then
      do i = 1, ND
        do j = i, ND ! ... tensor symmetry considered
          do ii = 1, Nc
            tauStar_ij(ii,region%global%t2MapSym(i,j)) = state%tv(ii,1) * (duStardx(ii,region%global%t2Map(i,j)) + duStardx(ii,region%global%t2Map(j,i)))
          end do ! ii
        end do ! j
      end do ! i
      do ii = 1, Nc
        state%flux(ii) = 0.0_rfreal
      end do ! ii
      do k = 1, ND
        do ii = 1, Nc
          state%flux(ii) = state%flux(ii) + duStardx(ii,region%global%t2Map(k,k))
        end do ! ii
      end do ! k
      do i = 1, ND
        do ii = 1, Nc
          tauStar_ij(ii,region%global%t2MapSym(i,i)) = tauStar_ij(ii,region%global%t2MapSym(i,i)) + state%tv(ii,2) * state%flux(ii)
        end do ! ii
      end do ! i
    end if ! state%RE





    ! ... Step 7: add viscous effect of adjoint velocity gradient portions to each equation
    if (state%RE /= 0.0_rfreal) then
      do i = 1, ND ! ... due to viscosity perturbation
        do j = 1, ND
          do ii = 1, Nc
            state%rhs(ii,1) = state%rhs(ii,1) - n_of_PowerLaw * tau_ij(ii,region%global%t2MapSym(i,j)) * state%REinv * state%dv(ii,3) * duStardx(ii,region%global%t2Map(i,j))
            state%rhs(ii,NVARS) = state%rhs(ii,NVARS) + n_of_PowerLaw * tau_ij(ii,region%global%t2MapSym(i,j)) * state%REinv / state%dv(ii,1) * duStardx(ii,region%global%t2Map(i,j))
          end do ! ii
        end do ! j
      end do ! i
      do i = 1, ND
        do j = 1, ND ! ... tensor symmetry CAN'T be considered here
          do ii = 1, Nc
            state%flux(ii) = tauStar_ij(ii,region%global%t2MapSym(i,j))
            state%dflux(ii) = 0.0_rfreal
          end do ! ii
          Call FIRST_DERIV(region, ng, j, state%flux, state%dflux, 1.0_rfreal, .FALSE.)

          do ii = 1, Nc
            state%rhs(ii,i+1) = state%rhs(ii,i+1) - state%REinv * state%dflux(ii)
          end do ! ii
        end do ! j
      end do ! i
    end if ! state%RE





    ! ... Step 8: add viscous effect of adjoint pressure gradient portions to each equation
    if (state%RE /= 0.0_rfreal) then
      do j = 1, ND
        ! ... heat flux vector of the forward problem
        do ii = 1, Nc
          state%flux(ii) = state%dv(ii,2) ! ... temperature
          state%dflux(ii) = 0.0_rfreal
        end do ! ii
        Call FIRST_DERIV(region, ng, j, state%flux, state%dflux, 1.0_rfreal, .FALSE.)
        do ii = 1, Nc
          state%dflux(ii) = - state%tv(ii,3) * state%dflux(ii) ! ... j-th component of heat flux vector
        end do ! ii

        do ii = 1, Nc
          state%flux(ii) = 0.0_rfreal
        end do ! ii
        do k = 1,ND
          do ii = 1, Nc
            state%flux(ii) = state%flux(ii) + tau_ij(ii,region%global%t2MapSym(j,k)) * UVW(ii,k)
          end do ! ii
        end do ! k
        do ii = 1, Nc
          state%flux(ii) = n_of_PowerLaw * (state%flux(ii) - state%dflux(ii) * state%PRinv) * state%REinv ! ... due to viscosity perturbation
        end do ! ii

        do ii = 1, Nc
          state%rhs(ii,1) = state%rhs(ii,1) - state%flux(ii) * state%dv(ii,3) * dpStardx(ii,j)
        end do ! ii
        do i = 1, ND
          do ii = 1, Nc
            state%rhs(ii,i+1) = state%rhs(ii,i+1) + tau_ij(ii,region%global%t2MapSym(i,j)) * state%REinv * dpStardx(ii,j)
          end do ! ii
        end do ! i
        do ii = 1, Nc
          state%rhs(ii,NVARS) = state%rhs(ii,NVARS) + state%flux(ii) / state%dv(ii,1) * dpStardx(ii,j)
        end do ! ii
      end do ! j

      do j = 1, ND ! ... diffusion of adjoint pressure
        do ii = 1, Nc
          state%flux(ii) = state%tv(ii,1) * dpStardx(ii,j)
          state%dflux(ii) = 0.0_rfreal
        end do ! ii
        Call FIRST_DERIV(region, ng, j, state%flux, state%dflux, 1.0_rfreal, .FALSE.)
        do ii = 1, Nc
          state%dflux(ii) = state%dflux(ii) * state%dv(ii,2) * state%REinv * state%PRinv
        end do ! ii

        do ii = 1, Nc
          state%rhs(ii,1) = state%rhs(ii,1) + state%dflux(ii) * state%dv(ii,3)
          state%rhs(ii,NVARS) = state%rhs(ii,NVARS) - state%dflux(ii) / state%dv(ii,1)
        end do ! ii
      end do ! j

      do i = 1, ND
        do j = i, ND ! ... tensor symmetry considered
          do ii = 1, Nc
            tauStar_ij(ii,region%global%t2MapSym(i,j)) = state%tv(ii,1) * (UVW(ii,j) * dpStardx(ii,i) + UVW(ii,i) * dpStardx(ii,j))
          end do ! ii
        end do ! j
      end do ! i
      do ii = 1, Nc
        state%flux(ii) = 0.0_rfreal
      end do ! ii
      do k = 1, ND
        do ii = 1, Nc
          state%flux(ii) = state%flux(ii) + UVW(ii,k) * dpStardx(ii,k)
        end do ! ii
      end do ! k
      do i = 1, ND
        do ii = 1, Nc
          tauStar_ij(ii,region%global%t2MapSym(i,i)) = tauStar_ij(ii,region%global%t2MapSym(i,i)) + state%tv(ii,2) * state%flux(ii)
        end do ! ii
      end do ! i
      do i = 1, ND
        do j = 1, ND ! ... tensor symmetry CAN'T be considered here
          do ii = 1, Nc
            state%flux(ii) = tauStar_ij(ii,region%global%t2MapSym(i,j))
            state%dflux(ii) = 0.0_rfreal
          end do ! ii
          Call FIRST_DERIV(region, ng, j, state%flux, state%dflux, 1.0_rfreal, .FALSE.)

          do ii = 1, Nc
            state%rhs(ii,i+1) = state%rhs(ii,i+1) - state%REinv * state%dflux(ii)
          end do ! ii
        end do ! j
      end do ! i
    end if ! state%RE





    ! ... Step 9: subtract time-derivatives of other adjoint variables
    ! ...         this is equivalent to inverting coefficient matrix of unsteady 
    ! ...         adjoint variables
    ! ... (1) adjoint pressure equation first
    do ii = 1, Nc
      state%rhs(ii,NVARS) = (state%gv(ii,1) - 1.0_rfreal) * state%rhs(ii,NVARS)
    end do ! ii
    ! ... (2) adjoint velocity equations
    do i = 1, ND
      do ii = 1, Nc
        state%rhs(ii,i+1) = state%dv(ii,3)*state%rhs(ii,i+1) - UVW(ii,i)*state%rhs(ii,NVARS)
      end do ! ii
    end do ! i
    ! ... (3) adjoint density equation
    do i = 1, ND
      do ii = 1, Nc
        state%rhs(ii,1) = state%rhs(ii,1) - UVW(ii,i)*state%rhs(ii,i+1) - 0.5_rfreal*(UVW(ii,i)**2)*state%rhs(ii,NVARS)
      end do ! ii
    end do ! i





    ! ... deallocate
    deallocate(UVW, UVWhat, drStardx, duStardx, dpStardx)
    if (state%RE /= 0.0_rfreal) then
      deallocate(tau_ij, tauStar_ij)
    end if ! state%RE

  END SUBROUTINE AdjNS_RHS

  subroutine AdjNS_Allocate_Memory(region, ng, flags, rkStep)

    USE ModGlobal
    USE ModDataStruct
    USE ModDeriv
    USE ModMPI

    Implicit None

    ! ... local variables
    type(t_grid), pointer :: grid
    type(t_mixt), pointer :: state
    type(t_mixt_input), pointer :: input
    type(t_region), pointer :: region
    integer :: flags, rkStep
    integer :: ND, Nc, N(MAX_ND), ng, j, alloc_stat(2)

    ! ... local variables
    integer :: l,m

    ! ... simplicity
    grid  => region%grid(ng)
    state => region%state(ng)
    input => grid%input
    ND = grid%ND
    Nc = grid%nCells
    N  = 1
    do j = 1, grid%ND
      N(j) = grid%ie(j)-grid%is(j)+1
    end do ! j

    ! ... allocate space for arrays
    allocate(state%flux(Nc), stat=alloc_stat(1));
    allocate(state%dflux(Nc), stat=alloc_stat(2));
    if (sum(alloc_stat(:)) /= 0)  call graceful_exit(region%myrank, 'Unable to allocate flux, dflux in AdjNS_Allocate_Memory.')

  end subroutine AdjNS_Allocate_Memory

  subroutine AdjNS_Deallocate_Memory(region, ng, flags, rkStep)

    USE ModGlobal
    USE ModDataStruct
    USE ModDeriv
    USE ModMPI

    Implicit None

    ! ... local variables
    type(t_grid), pointer :: grid
    type(t_mixt), pointer :: state
    type(t_mixt_input), pointer :: input
    type(t_region), pointer :: region
    integer :: flags, ng, rkStep

    ! ... simplicity
    grid  => region%grid(ng)
    state => region%state(ng)
    input => grid%input

    ! ... deallocate
    deallocate(state%flux, state%dflux)

  end subroutine AdjNS_Deallocate_Memory

  subroutine AdjNS_Update_Target(region, ng)

    USE ModGlobal
    USE ModDataStruct
    USE ModDeriv
    USE ModMPI

    Implicit None

    ! ... subroutine arguments
    type(t_region), pointer :: region
    integer :: ng

    ! ... local variables and arrays

  end subroutine AdjNS_Update_Target

  subroutine AdjNS_BC_Fix_Value(region)

    USE ModGlobal
    USE ModDataStruct
    USE ModDeriv
    USE ModMPI

    Implicit None

    ! ... subroutine arguments
    type(t_region), pointer :: region

    ! ... local variables and arrays

  end subroutine AdjNS_BC_Fix_Value

  !
  ! Characteristic-based BC of Thompson and Poinsot & Lele applied to adjoint 
  ! Navier-Stokes equations in a generalized coordinate.  Multi-dimensional 
  ! formulation of Kim & Lee is not considered (I don't think "multi"-dimensional 
  ! characteristic analysis makes sense, JKim).
  !
  subroutine AdjNS_BC(region, ng, flags)

    USE ModGlobal
    USE ModDataStruct
    USE ModDeriv
    USE ModMPI

    Implicit None

    ! ... subroutine arguments
    type(t_region), pointer :: region
    integer :: flags, ng

    ! ... local variables and arrays
    type(t_patch), pointer :: patch
    type(t_grid), pointer :: grid
    type(t_mixt), pointer :: state
    type(t_mixt_input), pointer :: input
    integer :: i, j, k, l, m, l0, lp, jj
    integer :: ND, npatch, normDir, sgn, Nc
    integer :: N(MAX_ND), Np(MAX_ND), BcType
    real(rfreal) :: dsgn, denom
    real(rfreal) :: GAMMA, RHO, SPVOL, UX, UY, UZ, PRESSURE, SPD_SND, SPD_SND_INV, KE, RHOE, ENTHALPY, MACH2
    real(rfreal) :: XI_X, XI_Y, XI_Z, XI_T, XI_X_TILDE, XI_Y_TILDE, XI_Z_TILDE
    real(rfreal) :: Uhat0, Uhat, junk
    real(rfreal) :: del, eta, sponge_amp, rel_pos(MAX_ND)
    real(rfreal), pointer :: Pinv(:,:), Pfor(:,:), wave_spd(:), L_vector(:)
    real(rfreal), pointer :: new_normal_flux(:)
    real(rfreal) :: ibfac

    ! ... simplicity
    ND = region%input%ND

    ! ... allocate space for arrays
    allocate(Pinv(ND+2,ND+2),Pfor(ND+2,ND+2))
    allocate(wave_spd(ND+2), L_vector(ND+2), new_normal_flux(ND+2))

    do npatch = 1, region%nPatches

      patch => region%patch(npatch)
      grid  => region%grid(patch%gridID)
      state => region%state(patch%gridID)
      input => grid%input
      normDir = abs(patch%normDir)
      sgn = normDir / patch%normDir
      dsgn = dble(sgn)

      Nc = grid%nCells
      N  = 1; Np = 1;
      do j = 1, grid%ND
        N(j)  =  grid%ie(j) -  grid%is(j) + 1
        Np(j) = patch%ie(j) - patch%is(j) + 1
      end do;

      if (patch%gridID /= ng) CYCLE ! ... apply BC only in this grid

      if ( patch%bcType /= SPONGE .and. patch%bcType /= SPONGE_WITH_LINEARIZED_DISTURBANCE ) then

        if (patch%bcType > 0) CYCLE ! ... BC for forward N-S neglected

        Do k = patch%is(3), patch%ie(3)
          Do j = patch%is(2), patch%ie(2)
            Do i = patch%is(1), patch%ie(1)

              l0 = (k- grid%is(3))* N(1)* N(2) + (j- grid%is(2))* N(1) +  i- grid%is(1)+1
              lp = (k-patch%is(3))*Np(1)*Np(2) + (j-patch%is(2))*Np(1) + (i-patch%is(1)+1)

              if ( grid%iblank(l0) == 0 ) CYCLE

              ! ... iblank factor (either 1 or zero)
              ibfac = grid%ibfac(i)

              ! ... primitive variables
              GAMMA = state%gv(l0,1)
              RHO = state%cv(l0,1)
              SPVOL = 1.0_rfreal / RHO
              UX  = SPVOL * state%cv(l0,2); UY = 0.0_rfreal; UZ = 0.0_rfreal;
              if (ND >= 2) UY = SPVOL * state%cv(l0,3)
              if (ND == 3) UZ = SPVOL * state%cv(l0,4)
              PRESSURE = state%dv(l0,1)
              SPD_SND = sqrt(GAMMA * PRESSURE * SPVOL)
              SPD_SND_INV = 1.0_rfreal / SPD_SND
              KE = 0.5_rfreal * (UX**2 + UY**2 + UZ**2)
              RHOE = state%cv(l0,ND+2)
              ENTHALPY = KE + SPD_SND**2 / (GAMMA - 1.0_rfreal)
              MACH2 = (2.0_rfreal * KE) * SPD_SND_INV**2

              ! ... construct the normalized metrics
              XI_X = grid%MT1(l0,region%global%t2Map(normDir,1)); XI_Y = 0.0_rfreal; XI_Z = 0.0_rfreal
              if (ND >= 2) XI_Y = grid%MT1(l0,region%global%t2Map(normDir,2))
              if (ND == 3) XI_Z = grid%MT1(l0,region%global%t2Map(normDir,3))
              XI_T = grid%XI_TAU(l0,normDir)

!!$           if (XI_X == 0.0_rfreal) XI_X = TINY ! ... to avoid dividing by zero 
!!$                                               ! ... in Pfor and Pinv

              ! ... multiply by the Jacobian
              !XI_X = XI_X * grid%JAC(l0)
              !XI_Y = XI_Y * grid%JAC(l0)
              !XI_Z = XI_Z * grid%JAC(l0)
              denom = 1.0_rfreal / sqrt(XI_X**2+XI_Y**2+XI_Z**2)
              XI_X_TILDE = XI_X * denom
              XI_Y_TILDE = XI_Y * denom
              XI_Z_TILDE = XI_Z * denom

              ! ... steady and unsteady contravariant velocity
              Uhat0 = UX*XI_X + UY*XI_Y + UZ*XI_Z
              Uhat  = XI_T + Uhat0

              ! ... construct the forward and backward transformation matrices
              ! ... these matrices are used for both NSCBC and SAT
              Pinv(:,:) = 0.0_rfreal; Pfor(:,:) = 0.0_rfreal; 

              if (ND == 2) then

                ! ... the forward matrix P
                Pfor(1,1) = UX*Uhat0 - (RHOE + PRESSURE)*SPVOL*XI_X
                Pfor(2,1) =-Uhat0
                Pfor(3,1) = 0.0_rfreal
                Pfor(4,1) = XI_X

                Pfor(1,2) = XI_Y*UX - XI_X*UY
                Pfor(2,2) =-XI_Y
                Pfor(3,2) = XI_X
                Pfor(4,2) = 0.0_rfreal

                junk = SPD_SND/(GAMMA-1.0_rfreal)*denom
                Pfor(1,3) = junk*Uhat0 + KE
                Pfor(2,3) =-UX - junk*XI_X 
                Pfor(3,3) =-UY - junk*XI_Y 
                Pfor(4,3) = 1.0_rfreal     

                Pfor(1,4) =-junk*Uhat0 + KE
                Pfor(2,4) =-UX + junk*XI_X
                Pfor(3,4) =-UY + junk*XI_Y
                Pfor(4,4) = 1.0_rfreal

                ! ... the inverse matrix Pinv
                junk = (GAMMA - 1.0_rfreal)/SPD_SND**2
                Pinv(1,1) =-junk
                Pinv(2,1) = junk*UY
                Pinv(3,1) = 0.5_rfreal*junk
                Pinv(4,1) = 0.5_rfreal*junk

                Pinv(1,2) =-junk*UX
                Pinv(2,2) =-XI_X_TILDE*XI_Y_TILDE + junk*UX*UY
                Pinv(3,2) = junk * 0.5_rfreal * (UX - SPD_SND*XI_X_TILDE)
                Pinv(4,2) = junk * 0.5_rfreal * (UX + SPD_SND*XI_X_TILDE)

                Pinv(1,3) =-junk*UY
                Pinv(2,3) = 1.0_rfreal - XI_Y_TILDE**2 + junk*UY**2
                Pinv(3,3) = junk * 0.5_rfreal * (UY - SPD_SND*XI_Y_TILDE)
                Pinv(4,3) = junk * 0.5_rfreal * (UY + SPD_SND*XI_Y_TILDE)

                Pinv(1,4) =-junk*KE
                Pinv(2,4) = XI_X_TILDE*(XI_X_TILDE*UY - XI_Y_TILDE*UX) + junk*KE*UY
                Pinv(3,4) = junk * 0.5_rfreal * (-SPD_SND*Uhat0*denom + (RHOE + PRESSURE)*SPVOL)
                Pinv(4,4) = junk * 0.5_rfreal * ( SPD_SND*Uhat0*denom + (RHOE + PRESSURE)*SPVOL)

              else if (ND == 3) then

                ! ... the forward matrix P
                Pfor(1,1) = UX*Uhat0 - (RHOE + PRESSURE)*SPVOL*XI_X
                Pfor(2,1) =-Uhat0
                Pfor(3,1) = 0.0_rfreal
                Pfor(4,1) = 0.0_rfreal
                Pfor(5,1) = XI_X

                Pfor(1,2) = XI_Z*UX - XI_X*UZ
                Pfor(2,2) =-XI_Z
                Pfor(3,2) = 0.0_rfreal
                Pfor(4,2) = XI_X
                Pfor(5,2) = 0.0_rfreal

                Pfor(1,3) = XI_Y*UX - XI_X*UY
                Pfor(2,3) =-XI_Y
                Pfor(3,3) = XI_X
                Pfor(4,3) = 0.0_rfreal
                Pfor(5,3) = 0.0_rfreal

                junk = SPD_SND/(GAMMA-1.0_rfreal)*denom
                Pfor(1,4) = junk*Uhat0 + KE
                Pfor(2,4) =-UX - junk*XI_X 
                Pfor(3,4) =-UY - junk*XI_Y 
                Pfor(4,4) =-UZ - junk*XI_Z 
                Pfor(5,4) = 1.0_rfreal     

                Pfor(1,5) =-junk*Uhat0 + KE
                Pfor(2,5) =-UX + junk*XI_X
                Pfor(3,5) =-UY + junk*XI_Y
                Pfor(4,5) =-UZ + junk*XI_Z
                Pfor(5,5) = 1.0_rfreal

                ! ... the inverse matrix Pinv
                junk = (GAMMA - 1.0_rfreal)/SPD_SND**2
                Pinv(1,1) =-junk
                Pinv(2,1) = junk*UZ
                Pinv(3,1) = junk*UY
                Pinv(4,1) = 0.5_rfreal*junk
                Pinv(5,1) = 0.5_rfreal*junk

                Pinv(1,2) =-junk*UX
                Pinv(2,2) =-XI_X_TILDE*XI_Z_TILDE + junk*UX*UZ
                Pinv(3,2) =-XI_X_TILDE*XI_Y_TILDE + junk*UX*UY
                Pinv(4,2) = junk * 0.5_rfreal * (UX - SPD_SND*XI_X_TILDE)
                Pinv(5,2) = junk * 0.5_rfreal * (UX + SPD_SND*XI_X_TILDE)

                Pinv(1,3) =-junk*UY
                Pinv(2,3) =-XI_Y_TILDE*XI_Z_TILDE + junk*UY*UZ
                Pinv(3,3) = 1.0_rfreal - XI_Y_TILDE**2 + junk*UY**2
                Pinv(4,3) = junk * 0.5_rfreal * (UY - SPD_SND*XI_Y_TILDE)
                Pinv(5,3) = junk * 0.5_rfreal * (UY + SPD_SND*XI_Y_TILDE)

                Pinv(1,4) =-junk*UZ
                Pinv(2,4) = 1.0_rfreal - XI_Z_TILDE**2 + junk*UZ**2
                Pinv(3,4) =-XI_Y_TILDE*XI_Z_TILDE + junk*UY*UZ
                Pinv(4,4) = junk * 0.5_rfreal * (UZ - SPD_SND*XI_Z_TILDE)
                Pinv(5,4) = junk * 0.5_rfreal * (UZ + SPD_SND*XI_Z_TILDE)

                Pinv(1,5) =-junk*KE
                Pinv(2,5) = XI_X_TILDE*(XI_X_TILDE*UZ - XI_Z_TILDE*UX) + XI_Y_TILDE*(XI_Y_TILDE*UZ - XI_Z_TILDE*UY) + junk*KE*UZ
                Pinv(3,5) = XI_X_TILDE*(XI_X_TILDE*UY - XI_Y_TILDE*UX) + XI_Z_TILDE*(XI_Z_TILDE*UY - XI_Y_TILDE*UZ) + junk*KE*UY
                Pinv(4,5) = junk * 0.5_rfreal * (-SPD_SND*Uhat0*denom + (RHOE + PRESSURE)*SPVOL)
                Pinv(5,5) = junk * 0.5_rfreal * ( SPD_SND*Uhat0*denom + (RHOE + PRESSURE)*SPVOL)

              end if ! ND

              ! ... wave speeds
              ! ... note that the order of left/right-moving acoustic modes is 
              ! ... opposite to the forward N-S solver in NS_BC (sorry for 
              ! ... confusion, JKim)
              wave_spd(1:ND) = Uhat
              wave_spd(ND+1) = Uhat - SPD_SND / denom
              wave_spd(ND+2) = Uhat + SPD_SND / denom
              wave_spd(:) = grid%JAC(l0) * wave_spd(:)

              ! ... get boundary derivatives of adjoint variables
              L_vector(:) = patch%DerivAdj(:,lp,normDir)

              ! ... scale the L_vector by Pinv and form characteristic variables
              L_vector = MATMUL(Pinv,L_vector)

              ! ... compute the characteristic L_vector following its definition
              L_vector(:) = wave_spd(:) * L_vector(:)

              ! ... manipulate L_vector depending on types of BC
              ! ... note that adjoint N-S solver is integrated backwards in time
              BcType = ABS(patch%bcType)
              if ( BcType == NSCBC_OUTFLOW_PERFECT_NONREFLECTION ) then
                if (patch%normDir > 0) then
                  L_vector(ND+1) = 0.0_rfreal
                else
                  L_vector(ND+2) = 0.0_rfreal
                end if

              ! ... give me more boundary conditions!

              else if (BcType == NSCBC_OUTFLOW_PERFECT_REFLECTION) then
                 if(patch%normDir > 0) then
                    L_vector(ND+1) =  L_vector(ND+2)
                 else
                    L_vector(ND+2) =  L_vector(ND+1)
                 ENDIF
              else
                 call graceful_exit(region%myrank, "... ERROR: unknown BC, see AdjNS_BC")
              end if ! patch%bcType

              ! ... compute the new normal flux
              new_normal_flux(:) = MATMUL(Pfor,L_vector)

              ! ... inverse of coefficient matrix of unsteady adjoint variables
              Pinv(:,:) = 0.0_rfreal
              Pinv(1,1) = 1.0_rfreal
              Pinv(1,2) =-UX * SPVOL
              if (ND >= 2) Pinv(1,3) =-UY * SPVOL
              if (ND == 3) Pinv(1,4) =-UZ * SPVOL
              Pinv(1,ND+2) = (GAMMA - 1.0_rfreal) * KE

              Pinv(2,2) = SPVOL
              Pinv(2,ND+2) =-(GAMMA - 1.0_rfreal) * UX
              if (ND >= 2) then
                Pinv(3,3) = SPVOL
                Pinv(3,ND+2) =-(GAMMA - 1.0_rfreal) * UY
              end if ! ND
              if (ND == 3) then
                Pinv(4,4) = SPVOL
                Pinv(4,ND+2) =-(GAMMA - 1.0_rfreal) * UZ
              end if ! ND

              Pinv(ND+2,ND+2) = GAMMA - 1.0_rfreal

              new_normal_flux(:) = grid%JAC(l0) * MATMUL(Pinv,patch%deriv(:,lp,normDir)) - new_normal_flux(:)

              ! ... reset the rhs
              if (grid%iblank(l0) /= 0) then
                state%rhs(l0,:) = state%rhs(l0,:) + new_normal_flux(:)
              else
                state%rhs(l0,:) = 0.0_rfreal
              end if 

            end do ! i
          end do ! j
        end do ! k

      else if (patch%bcType == SPONGE .or. patch%bcType == SPONGE_WITH_LINEARIZED_DISTURBANCE) then

        Do k = patch%is(3), patch%ie(3)
          Do j = patch%is(2), patch%ie(2)
            Do i = patch%is(1), patch%ie(1)

              l0 = (k- grid%is(3))* N(1)* N(2) + (j- grid%is(2))* N(1) +  i- grid%is(1)+1
              lp = (k-patch%is(3))*Np(1)*Np(2) + (j-patch%is(2))*Np(1) + (i-patch%is(1)+1)

              ! ... distance from boundary to inner sponge start
              rel_pos(1:grid%ND) = patch%sponge_xs(lp,1:grid%ND) - patch%sponge_xe(lp,1:grid%ND)
              del = sqrt(dot_product(rel_pos(1:grid%ND),rel_pos(1:grid%ND)))

              ! ... distance from inner sponge start to point
              rel_pos(1:grid%ND) = patch%sponge_xs(lp,1:grid%ND) - grid%XYZ(l0,1:grid%ND)
              eta = sqrt(dot_product(rel_pos(1:grid%ND),rel_pos(1:grid%ND))) / del

              if (patch%bcType == SPONGE_WITH_LINEARIZED_DISTURBANCE .and. eta .gt. 1.0_rfreal) eta = 1.0_rfreal

              sponge_amp = 0.0_rfreal
              if (eta .le. 1.0_rfreal) sponge_amp = (input%sponge_amp) * eta**(input%sponge_pow)

              if (grid%iblank(l0) /= 0) then
                state%rhs(l0,:) = state%rhs(l0,:) + sponge_amp * (state%av(l0,:) - state%avTarget(l0,:))
              else
                state%rhs(l0,:) = 0.0_rfreal
              end if

              if ( input%timeScheme == BDF_GMRES_IMPLICIT ) then

                ! ... add to LHS
                do jj = 1, grid%ND+2
                  patch%implicit_sat_mat(lp,jj,jj) = patch%implicit_sat_mat(lp,jj,jj) + sponge_amp
                end do

              end if

            end do
          end do
        end do

      end if ! patch%bcType

    end do ! npatch

    deallocate(Pinv, Pfor, wave_spd, L_vector, new_normal_flux)

  end subroutine AdjNS_BC

END MODULE ModAdjointNS
