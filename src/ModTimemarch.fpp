! Copyright (c) 2015, University of Illinois at Urbana-Champaign, XPACC
! License: MIT, http://opensource.org/licenses/MIT
!-----------------------------------------------------------------------
!
! ModTimemarch.f90
!
! - basic code for the timemarching a system of ODEs/PDEs
!
! Revision history
! - 20 Dec 2006 : DJB : initial code
! - 13 Jan 2007 : DJB : conversion to region
! - 08 Jan 2008 : DJB : addition of RK5
!
! $Header: /cvsroot/genx/Codes/RocfloCM/Source/ModTimemarch.f90,v 1.72 2011/09/26 21:12:16 bodony Exp $
!
!-----------------------------------------------------------------------
MODULE ModTimemarch

CONTAINS

  Subroutine Timemarch(region)

    USE ModGlobal
    USE ModDataStruct
    USE ModIO
    USE ModMPI
    USE ModDerivBuildOps
    USE ModDeriv
    USE ModMetrics
    USE ModInterp
    USE ModDataUtils
    USE ModRungeKutta
    USE ModEOS
    USE ModNavierStokesRHS
    USE ModFVSetup
    USE ModNASARotor
    USE ModStat
    USE ModCombustion
#ifdef BUILD_TMSOLVER
    USE ModTMSingleStep
    USE ModTMIO
    USE ModFTMCoupling
#endif
#ifdef BUILD_ELECTRIC
    USE ModElectric
    USE ModElectricState
    USE ModElectricSchwarz
#endif

    Implicit None

    ! ... global variables
    type(t_region), pointer :: region

    ! ... local variables
    type(t_rk), pointer :: rk
    type(t_grid), pointer :: grid
    type(t_fvgrid), pointer :: fv
    type(t_mixt), pointer :: state
    type(t_mixt_input), pointer :: input
    integer :: ng, nsub, i, j, conv_iter, GMRES_iter, GMRES_iter_ave, timeSchemeOld, p, err_sum, k
    integer :: ThermalStep, StructuralStep, ierr
    real(rfreal) :: timer, solution_diff_local_cv, solution_diff_cv, maxtemp, allTemps
    real(rfreal) :: timer_mainloop
    integer, allocatable :: err(:)
    integer, pointer :: main_ts_loop_index
    character(len=3), dimension(:), pointer :: vars_to_write
    logical :: lopened
    real(rfreal) :: YH,YO2,YH2,rhoA,EnA
    integer :: RESTART = TRUE
    logical :: solve_efield

    ! ... simplicity
    input => region%input
    main_ts_loop_index => region%global%main_ts_loop_index
    region%global%filtercount(:) = 1
    allocate(err(region%nGrids)); err(:) = 0
    gmres_iter = 0

    ! ... set initial iteration
    main_ts_loop_index = input%nstepi

#ifdef USE_AMPI
    ! ... set AMPI options
    allocate(region%ampi)
    region%ampi%migrate       = .false.
    region%ampi%checkpoint    = .false.
    region%ampi%migrationStep = 5
#endif

    ! ... nullify ARK2 pointers
    do ng = 1, region%nGrids
      grid => region%grid(ng)
      state => region%state(ng)
      nullify(state%auxVarsOld,state%cvOld,state%rhs_auxVars,state%rhs_auxVars_implicit,state%rhs_explicit,state%rhs,state%timeOld,state%cfl,state%dt,state%rhs_auxVars_explicit,region%electric(ng)%momentumTerm,region%electric(ng)%eMag);
    end do

    ! ... skip all of this in TM only case
    if (input%TMOnly /= TRUE) then

      ! ... if grid motion is specified analytically, do so here
      if (input%moveGrid == TRUE) then

        ! ... move only the rotor grid
        If (input%caseID == 'NASA_ROTOR_STATOR') Then
          call Setup_Rotor_Stator_Interface_Comms(region)
          call Move_Rotor_Grid_NASA(region)
        End If

      end if

      IF(input%AdjNS .eqv. .false.) THEN
         do ng = 1, region%nGrids
            grid => region%grid(ng)
            state => region%state(ng)
            call computeDv(region%myrank, grid, state)
         end do
      ENDIF

!      timer = MPI_Wtime()
!      CALL WriteOutput(region)
!      region%mpi_timings(:)%io_write_soln = region%mpi_timings(:)%io_write_soln + (MPI_WTIME() - timer)

      ! ... alternative OPERATOR_SETUP which does not require all processors to have global_iblank at the same time
      If (input%use_lowmem_operator_setup == TRUE) Then
        If (region%myrank == 0) Write (*,'(A)') 'PlasComCM: Building operators using low-memory option.  This may take a while...'
        Do i = 1, region%nGridsGlobal
          Do ng = 1, region%nGrids
            grid => region%grid(ng)
            If (grid%iGridGlobal == i) Then
              Do p = 0, grid%numproc_inComm-1
                Call Request_Global_Mask(region, ng, p)
                If (grid%myrank_inComm == p) Call Operator_Setup(region, grid%input, grid, err(ng), ng)
                Call MPI_Barrier(grid%comm, ierr)
              End Do
            End if
            Call MPI_Barrier(mycomm, ierr)
          End Do
        End Do
        Do ng = 1, region%nGrids
          grid => region%grid(ng)
          Call HYPRE_Pade_Operator_Setup(region, ng)
        End Do
     Else
        ! ... compute the derivative matrices in the computational
        ! ... coordinates.  These are independent of the mesh coordinates, but depend on IBLANK
        If (region%myrank == 0) Write (*,'(A)') 'PlasComCM: Building operators...'
        ! ... no need to call create_global_mask for the box shape, WZhang 12/2014
        !Call create_global_mask(region)
        do ng = 1, region%nGrids
          grid => region%grid(ng)
          Call Operator_Setup(region, grid%input, grid, err(ng), ng)
          if (region%input%operator_implicit_solver == GMRES) Call HYPRE_Pade_Operator_Setup(region, ng)
        end do
     End If

      Call MPI_Allreduce(sum(err(:)), err_sum, 1, MPI_INTEGER, MPI_SUM, mycomm, ierr)
      if (err_sum /= 0) Call Output_Rank_As_IBLANK(region)

      ! ... if the grid is not moving, compute metrics OUTSIDE loop
      do ng = 1, region%nGrids
        call metrics(region, ng)
      end do


      ! ... compute rhs forcing terms from target (base flow) in temporal simulation
      if(region%input%TemporalRHSForcing == TRUE) then
        do ng = 1, region%nGrids
          call NS_RHS_TemporalForcing(region, ng, region%input%StrmDir, region%input%NrmDir)
        end do
      end if

      ! ... output diagnostic file if asked
      if (input%outputBC_asIBLANK == TRUE) call output_bc_as_iblank(region)

      ! Set up Efield solver
#ifdef BUILD_ELECTRIC
      if (input%includeElectric) then
        if (input%gridType == CHIMERA .AND. input%volumeIntersection == TRUE) Then
          call ElectricSchwarzSetup(region)
        else
          do ng = 1, region%nGrids
             call ElectricSetup(region, ng)
          end do
        end if
        do ng = 1, region%nGrids
          call ElectricRHS(region, ng)
        end do
        if (input%gridType == CHIMERA .AND. input%volumeIntersection == TRUE) Then
          call ElectricSchwarzSolve(region)
        else
          do ng = 1, region%nGrids
            call ElectricSolve(region, ng)
          end do
        end if
      end if
#endif

      ! Only write if not a restart
      IF(main_ts_loop_index == 0) THEN
         timer = MPI_Wtime()
         CALL WriteOutput(region)
         region%mpi_timings(:)%io_write_soln = region%mpi_timings(:)%io_write_soln + (MPI_WTIME() - timer)
      ENDIF

   end if ! ... thermomechanical only

    call MPI_BARRIER(mycomm,ierr)
      
    ! ... output
    if (region%myrank == 0) write (*,'(A)') 'PlasComCM: ==== BEGIN ITERATIONS ===='
    if (region%myrank == 0 .and. input%TMOnly == TRUE) write (*,'(A)') 'PlasComCM: ==== THERMOMECHANICAL ONLY ===='

    ! ... initialize dynamic thermal and structural solvers
#ifdef BUILD_TMSOLVER
    if (input%TMSolve /= FALSE) then

      if (input%TMOnly /= TRUE) then
        do ng = 1, region%nGrids
          grid  => region%grid(ng)
          state => region%state(ng)         

          ! ... initial fill of dependent variables
          Call computeDv(region%myrank, grid, state)

          ! ... initial fill of transport variables
          Call computeTv(region%cantera, grid, state)

        end do
      end if

      ! ... transfer fluid loads at timestep n
      if (input%TMOnly /= TRUE .and. (input%StructuralTimeScheme == DYNAMIC_SOLN .or. input%ThermalTimeScheme == DYNAMIC_SOLN))&
           Call FTMExchange(region,TRUE,TRUE,FLUID2SOLID)

      ! ... transfer interacting surface state at timestep n
      if (input%TMOnly /= TRUE .and. (input%StructuralTimeScheme == DYNAMIC_SOLN .or. input%ThermalTimeScheme == DYNAMIC_SOLN))&
           Call FTMExchange(region,TRUE,TRUE,SOLID2FLUID)


      if (region%TMColor /= FALSE) then
        do ng = 1, region%nTMGrids

          ! ... form thermal capacitance matrix and initialize temperature rate
          if((input%TMSolve == 1 .or. input%TMSolve == 3) .and. input%ThermalTimeScheme == DYNAMIC_SOLN) &
               call InitializeDynamicThermal(region,ng)

          ! ... form the mass matrix and initialize nodal acceleration and velocity
          if(input%TMSolve > 1 .and. input%StructuralTimeScheme == DYNAMIC_SOLN) &
               call InitializeDynamicStructural(region,ng)

        end do
      end if
    end if
#endif

    call MPI_BARRIER(mycomm,ierr)
    ! ... timer to profile the main loop, WZhang 10/2014
    if (region%myrank == 0) then
      timer_mainloop = MPI_Wtime()
    end if

    region%mainloop_time = MPI_WTIME()
    region%nsteps_this_run = 0
    ! ... main time marching loop
    mainLoop: Do main_ts_loop_index = input%nstepi+1, input%nstepi+input%nstepmax

      if(main_ts_loop_index==1) then 
        region%state(1)%t_output_next = region%input%t_output
      endif

      ! ... if we need the fluid solution
      if (input%TMOnly /= TRUE) then

        ! Solve Efield
#ifdef BUILD_ELECTRIC
        if (input%includeElectric) then
          if (input%ef_solve_interval > 0) then
            solve_efield = mod(main_ts_loop_index,input%ef_solve_interval) == 0
          else
            solve_efield = .false.
          end if
          if (solve_efield) then
            do ng = 1, region%nGrids
              call ElectricRHS(region, ng)
            end do
            if (input%gridType == CHIMERA .AND. input%volumeIntersection == TRUE) Then
              call ElectricSchwarzSolve(region)
            else
              do ng = 1, region%nGrids
                call ElectricSolve(region, ng)
              end do
            end if
          end if
        end if
#endif

        ! ... if using the BDF implicit scheme
        if ((input%timeScheme == BDF_GMRES_IMPLICIT) .AND. input%fluidModel == CNS) then

          ! ... save initial condition
          if (main_ts_loop_index == input%nstepi+1) then
            Do ng = 1, region%nGrids
              grid  => region%grid(ng)
              state => region%state(ng)
              allocate(state%cvOld1(grid%nCells,size(state%cv,2)))
              allocate(state%cvOld2(grid%nCells,size(state%cv,2)))
              allocate(grid%INVJACOld1(grid%nCells))
              allocate(grid%INVJACOld2(grid%nCells))
              do i = 1, grid%nCells
                grid%INVJACOld1(i) = grid%INVJAC(i)
              end do
              do j = 1, size(state%cv,2)
                do i = 1, grid%nCells
                  state%cvOld1(i,j) = state%cv(i,j)
                end do
              end do
            End Do
          end if

          ! ... call BDF
          call BDF_GMRES_IMPLICIT_SUB(region, conv_iter, GMRES_iter)

          ! ... check convergence to steady state
          solution_diff_local_cv = 0.0_rfreal
          do ng = 1, region%nGrids
            state => region%state(ng)
            grid  => region%grid(ng)
            do k = 1, size(state%rhs,2)
              do i = 1, grid%nCells
                solution_diff_local_cv = MAX(solution_diff_local_cv, abs(state%cv(i,k) - state%cvOld1(i,k)))
              end do
            end do
          end do
          Call MPI_Allreduce(solution_diff_local_cv, solution_diff_cv, 1, MPI_DOUBLE_PRECISION, MPI_MAX, mycomm, ierr)

        else

          timer = MPI_WTIME()

          ! ... time accurate nonlinear RK4
          if (input%timeScheme == EXPLICIT_RK4) then
            if (input%fluidModel == CNS) then
#ifdef ENABLE_DEPRECIATED_RK4
              if (input%AdjNS .eqv. .FALSE.) call RK4(region)
#else
              if (input%AdjNS .eqv. .FALSE.) call ARK2(region)
#endif
              if (input%AdjNS .eqv. .TRUE. ) call RK4_AdjNS(region)
            end if
            if (input%fluidModel == Q1D) call RK4_Q1D(region)
            if (input%fluidModel == CYL1D) call RK4_CYL1D(region)
            if (input%fluidModel == LINCNS) call LINRK4(region)
          end if

          ! ... march to steady state with 5-stage, 1st order accurage RK5 scheme of Jameson
          if (input%timeScheme == EXPLICIT_RK5 .and. input%fluidModel == CNS) Then
#ifdef ENABLE_DEPRECIATED_RK5
            call RK5_SteadyState(region)
#else
            call ARK2(region)
#endif
          end if

          ! ... time accurate RK4 with implicit BC
          if (input%timeScheme == IMEX_RK4 .and. input%fluidModel == CNS) &
               call ARK2(region)

          ! ... compute the approximate linearized RHS operator
          if (input%timeScheme == APPROXIMATE_LINEAR_OPERATOR .and. input%fluidModel == CNS) &
               call APPROXIMATE_LINEAR_CNS_OPERATOR(region)

          ! ... compute the approximate linearized RHS operator
          if (input%timeScheme == APPROXIMATE_LINEAR_OPERATOR_POWER_METHOD .and. input%fluidModel == CNS) &
               call APPROXIMATE_LINEAR_CNS_OPERATOR_POWER_METHOD(region)

         ! ... time accurate 3-4-1 BDF with RK5 inner loop
         ! ... note :: this must be started with some other method
         if (input%timeScheme == BDF_RK5 .and. input%fluidModel == CNS) then
           if (main_ts_loop_index == input%nstepi+1) then
             ! ... save initial condition
             Do ng = 1, region%nGrids
               grid  => region%grid(ng)
               state => region%state(ng)
               allocate(state%cvOld1(grid%nCells,size(state%cv,2)))
               allocate(state%cvOld2(grid%nCells,size(state%cv,2)))
               do j = 1, size(state%cv,2)
                 do i = 1, grid%nCells
                   state%cvOld1(i,j) = state%cv(i,j)
                 end do
               end do
             End Do
             ! ... march forward one timestep
             call ARK2(region)
             region%global%rk_alloc = .true.
             conv_iter = 0
           else
             call BDF_RK5_SemiImplicit(region, conv_iter)
           end if
         end if
         timer = MPI_WTime() - timer
         region%rk_time = region%rk_time + timer
       end if

        ! ... Added check routine to stop in case we have invalid CV values
        IF(input%CheckingLevel .GT. 0) THEN
          CALL CheckCV(region,input%FailOnDataError)
        ENDIF

        if(region%nIntegralSurfaces > 0) Call CalcPower(region)

        ! ... iteration output
        maxtemp = 0._rfreal
        do ng = 1, region%nGrids
          maxtemp = max(maxtemp, maxval(region%state(ng)%dv(:,2),region%grid(ng)%iblank==1))
        end do
        Call MPI_Allreduce(maxtemp, allTemps, 1, MPI_DOUBLE_PRECISION, MPI_MAX, mycomm, ierr)
        if (region%myrank == 0) then
          if (input%timeScheme /= BDF_GMRES_IMPLICIT) then
            write(*,'(A,I8,4(A,D13.6))') 'PlasComCM: iteration = ', &
                 main_ts_loop_index, ', dt = ', region%state(1)%dt(1), &
                 ', time = ', region%state(1)%time(1), ', cfl = ', &
                 region%state(1)%cfl(1), ', maxT = ', &
                 allTemps*input%TempRef * (input%GamRef-1)
          else
            write(*,'(A,I8,4(A,D13.6),2(A,I5))') 'PlasComCM: iteration = ', &
                 main_ts_loop_index, ', dt = ', region%state(1)%dt(1), &
                 ', time = ', region%state(1)%time(1), ', cfl = ', &
                 region%state(1)%cfl(1),', Q^(n+1)-Q(n) = ',solution_diff_cv, ', conv_iter = ', &
                 conv_iter, ', max GMRES_iter = ', GMRES_iter
          end if
          If (trim(input%caseID) == '0DCOMBUSTION') Then
            inquire (file="plascom.dat",opened=lopened)
            if(.not. lopened) then
              open(1234,file="plascom.dat")
            endif
            rhoA = sum(region%state(1)%cv(:,1),region%grid(1)%ibfac==1d0)/dble(count(region%grid(1)%ibfac==1d0))
            EnA = sum(region%state(1)%cv(:,4),region%grid(1)%ibfac==1d0)/dble(count(region%grid(1)%ibfac==1d0))
            YH2 = sum(region%state(1)%auxVars(:,1),region%grid(1)%ibfac==1d0)/dble(count(region%grid(1)%ibfac==1d0))/rhoA
            YO2 = sum(region%state(1)%auxVars(:,2),region%grid(1)%ibfac==1d0)/dble(count(region%grid(1)%ibfac==1d0))/rhoA
            YH = sum(region%state(1)%auxVars(:,3),region%grid(1)%ibfac==1d0)/dble(count(region%grid(1)%ibfac==1d0))/rhoA
            write(1234,'(1p123e15.8)')region%state(1)%time(1)*input%TimeRef,allTemps*input%TempRef * (input%GamRef-1), YH,YO2,YH2, rhoA*input%densRef, EnA/rhoA*input%sndSpdref**2
          Endif
        end if

        if (input%output_mode == SAMPLE_TIME) then
          region%state(1)%sampler = region%state(1)%tcnt 
        else
          region%state(1)%sampler = mod(main_ts_loop_index,input%noutput)
        end if

        ! ... compute statistics on the fly
        if (input%use_stats==1) then
           Call Stat2D_Sample(region)
        end if

        ! output routines
        if (input%noutput > 0) then
          if (region%state(1)%sampler == 0) then
             IF((input%AdjNS .eqv. .false.) .or. (input%fluidModel == CYL1D)) THEN
                do ng = 1, region%nGrids
                   state => region%state(ng)
                   grid  => region%grid(ng)
                   call computeDv(region%myrank, grid, state)
                end do
             ENDIF
             timer = MPI_WTime()
             CALL WriteOutput(region)
             region%mpi_timings(:)%io_write_soln = region%mpi_timings(:)%io_write_soln + (MPI_WTIME() - timer)
          end if
       end if
       
       ! restart routines
       if (input%nrestart > 0) then
          if ((mod(main_ts_loop_index,input%nrestart) == 0) .or. &
               main_ts_loop_index == (input%nstepi+input%nstepmax)) then
             CALL WriteOutput(region,RESTART)
             region%mpi_timings(:)%io_write_restart = region%mpi_timings(:)%io_write_restart + (MPI_WTIME() - timer)
          end if
       end if

       call store_probe_data(region) ! ... JKim 12/2008

       if (input%noutput > 0) then
          if (mod(main_ts_loop_index,input%noutput) == 0) then
             call write_probe_data(region) ! ... JKim 12/2008
          end if ! mod(main_ts_loop_index,input%noutput)
       end if
       
       if (main_ts_loop_index == (input%nstepi + input%nstepmax)) &
            call write_probe_data(region)
       
       !call mpi_barrier(mycomm,ierr)
       ! ... if we have reached steady-state, then stop the simulation
       if (input%Converged == TRUE)then
          if (region%myrank == 0) write (*,'(A)') 'PlasComCM: ==== CONVERGED TO STEADY STATE ===='
          timer = MPI_WTIME()
          CALL WriteOutput(region,RESTART)
          region%mpi_timings(:)%io_write_restart = region%mpi_timings(:)%io_write_restart + (MPI_WTIME() - timer)
          exit
       end if
    end if ! ... TM Only
    ! ... now enter the thermomechanical solver
#ifdef BUILD_TMSOLVER
    if(input%TMSolve /= FALSE) then
       StructuralStep = FALSE
       ThermalStep = FALSE
       if(mod(main_ts_loop_index-1,input%ThermalSolnFreq)==0 .and. input%TMSolve /= 2) ThermalStep = TRUE
       if(mod(eain_ts_loop_index-1,input%StructSolnFreq)==0 .and. input%TMSolve /= 1) StructuralStep = TRUE
       
       ! ... pass fluid loads to solid domain
       if(input%TMOnly /= TRUE .and. (ThermalStep == TRUE .or. StructuralStep == TRUE)) &
            Call FTMExchange(region,ThermalStep,StructuralStep,FLUID2SOLID)
       
       if(region%TMColor == TRUE) then
          
          !if(input%TMOnly /= TRUE) call transfer fluid loads here !!!
          timer = MPI_WTIME()
          if(ThermalStep == TRUE .or. StructuralStep == TRUE) call TMStep(region,ThermalStep,StructuralStep)
          ! ... write the solution file
          
          if (region%state(1)%sampler == 0) then
             
             Call TMwrite_plot3d_file(region, 'TMq')
             if(input%SaveSolidData == TRUE) Call WriteSolidData(region) 
             
          end if
          
          ! ... pass fluid loads to solid domain
          if(input%TMOnly /= TRUE .and. (ThermalStep == TRUE .or. StructuralStep == TRUE)) &
               Call FTMExchange(region,ThermalStep,StructuralStep,FLUID2SOLID)
          
          if(region%TMColor == TRUE) then
               
             !if(input%TMOnly /= TRUE) call transfer fluid loads here !!!
             timer = MPI_WTIME()
             if(ThermalStep == TRUE .or. StructuralStep == TRUE) call TMStep(region,ThermalStep,StructuralStep)
             ! ... write the solution file
             
             if (region%state(1)%sampler == 0) then
                
                Call TMwrite_plot3d_file(region, 'TMq')
                if(input%SaveSolidData == TRUE) Call WriteSolidData(region) 
                
             end if
               
             if ((mod(main_ts_loop_index,input%nrestart) == 0) .or. &
                  main_ts_loop_index == (input%nstepi+input%nstepmax)) then
                ! ... write a solution file
                Call TMwrite_plot3d_file(region, 'TMq')
                ! ... write a aux data (velocity + acceleration) file
                if(input%StructuralTimeScheme == DYNAMIC_SOLN .or. &
                     input%ThermalTimeScheme == DYNAMIC_SOLN) Call TMwrite_plot3d_file(region, 'dTM')
                
             end if
             !if(region%TMRank == 0) Call DataProbe(region)
          end if
          region%TM_total_time = region%TM_total_time + (MPI_WTIME() - timer)
            
          ! ... pass solid information to fluid domain
            
          if(input%TMOnly /= TRUE .and. (ThermalStep == TRUE .or. StructuralStep == TRUE)) &
               Call FTMExchange(region,ThermalStep,StructuralStep, SOLID2FLUID)
          
          call MPI_BARRIER(mycomm,ierr)
            
       end if
    end if
#endif
     
#ifdef USE_AMPI
         Call MPI_BARRIER(mycomm,ierr)
         
         ! Fault tolerance (checkpoint/restart)
         if (region%ampi%checkpoint .AND. MODULO(main_ts_loop_index, region%ampi%migrationStep) == 0) then
            Call MPI_MemCheckpoint()
         end if
         
         ! Load balancing
         if (region%ampi%migrate .AND. MODULO(main_ts_loop_index, region%ampi%migrationStep) == 0) then
            Call MPI_Migrate()
         end if
#endif
         
         region%nsteps_this_run = region%nsteps_this_run + 1
      End Do mainLoop

   if (region%myrank == 0) write (*,'(A)') 'PlasComCM: ==== END ITERATIONS ===='
   region%mainloop_time = MPI_Wtime() - region%mainloop_time
   
   call MPI_BARRIER(mycomm,ierr)
   ! ... timer to profile the main loop, WZhang 10/2014
   if (region%myrank == 0) then
      timer_mainloop = MPI_Wtime() - timer_mainloop
   end if
   if (region%myrank == 0) write (*,'(A,E12.4,A)') 'PlasComCM: Wallclock time of the main time marching loop is ', timer_mainloop, ' seconds.'
   
 end Subroutine Timemarch
 
!-----------------------------------------------------------------------

  subroutine setup_single_steppingOLD(region)

    USE ModGlobal
    USE ModDataStruct
    USE ModIO
    USE ModMPI
    USE ModDerivBuildOps
    USE ModDeriv
    USE ModMetrics
    USE ModInterp

    Implicit None

    type(t_region), pointer :: region

    ! ... local variables
    type(t_rk), pointer :: rk
    type(t_grid), pointer :: grid
    type(t_mixt), pointer :: state
    type(t_mixt_input), pointer :: input
    integer :: ng, nsub, p, i
    integer :: errflag
    integer, allocatable :: err(:)
    real(rfreal) :: timer


    ! ... simplicity
    input => region%input
    region%global%filtercount = 1
    region%global%rk_alloc = .true.
    allocate(err(region%nGrids)); err(:) = 0

    ! ... initialize the time march
    if (input%timeScheme == IMPLICIT) then

    else

       !      allocate(rk)
       !      call initializeExplicitTimemarch(input, rk)

    end if

    ! ... compute the derivative matrices in the computational
    ! ... coordinates.  These are independent of the mesh.
    Call create_global_mask(region)
    do ng = 1, region%nGrids
       grid => region%grid(ng)
       Call Operator_Setup(region, grid%input, grid, err(ng), ng)
       Call HYPRE_Pade_Operator_Setup(region, ng)
    end do

    ! ... if the grid is not moving, compute metrics OUTSIDE loop
    do ng = 1, region%nGrids
       call metrics(region, ng)
    end do

    ! ROCSTAR Integration: This block (initial state writing) can be called outside
    ! of this routine on the first step after initialization - otherwise, we shouldn't
    ! do this every single step.
    ! ..q. write initial state
    region%global%main_ts_loop_index = input%nstepi
    CALL WriteData(region,'cv ')
    CALL WriteData(region,'xyz')
    CALL WriteData(region,'met')
    CALL WriteData(region,'cvt')
    !    call write_state(region)
    !    call write_grid(region)
    !    call write_metrics(region)
    !    call write_target(region)
    !call compute_metric_invariants(region); call write_metrics(region); call graceful_exit(region%myrank, 'Stopping after write_metric_invariants')

    ! ... set up the interpolation data once for static grids
    if ((input%moveGrid == FALSE) .and. (input%gridType == CHIMERA)) &
         call Setup_Interpolation(region)

    ! ... output

  end subroutine setup_single_steppingOLD



  subroutine setup_single_stepping(region)

    USE ModGlobal
    USE ModDataStruct
    USE ModIO
    USE ModMPI
    USE ModDerivBuildOps
    USE ModDeriv
    USE ModMetrics
    USE ModInterp

    Implicit None

    type(t_region), pointer :: region

    ! ... local variables
    type(t_rk), pointer :: rk
    type(t_grid), pointer :: grid
    type(t_mixt), pointer :: state
    type(t_mixt_input), pointer :: input
    integer :: ng, nsub, p, i
    integer :: errflag, ierr
    integer, allocatable :: err(:)

    real(rfreal) :: timer

    ! ... simplicity
    input => region%input
    region%global%filtercount = 1
    region%global%rk_alloc = .true.
    allocate(err(region%nGrids)); err(:) = 0

    ! ... initialize the time march
    if (input%timeScheme == IMPLICIT) then

    else

       !      allocate(rk)
       !      call initializeExplicitTimemarch(input, rk)

    end if

    ! ... set up the interpolation data once for static grids
    if ((input%moveGrid == FALSE) .and. (input%gridType == CHIMERA)) &
         call Setup_Interpolation(region)

    ! ... start at input step (specified by the restart file)
    region%global%main_ts_loop_index = input%nstepi

    ! ROCSTAR Integration: This block (initial state writing) can be called outside
    ! of this routine on the first step after initialization - otherwise, we shouldn't
    ! do this every single step.
    ! ..q. write initial state
    ! CALL write_plot3d_file(region,'cv ')
    ! CALL write_plot3d_file(region,'xyz')
    ! CALL write_plot3d_file(region,'met')
    ! CALL write_plot3d_file(region,'cvt')
    !call compute_metric_invariants(region); call write_metrics(region); call graceful_exit(region%myrank, 'Stopping after write_metric_invariants')

    ! ... alternative OPERATOR_SETUP which does not require all processors to have global_iblank at the same time
    If (input%use_lowmem_operator_setup == TRUE) Then
       If (region%myrank == 0) Write (*,'(A)') 'PlasComCM: Building operators using low-memory option.  This may take a while...'
       Do i = 1, region%nGridsGlobal
          Do ng = 1, region%nGrids
             grid => region%grid(ng)
             If (grid%iGridGlobal == i) Then
                Do p = 0, grid%numproc_inComm-1
                   Call Request_Global_Mask(region, ng, p)
                   If (grid%myrank_inComm == p) Call Operator_Setup(region, grid%input, grid, err(ng), ng)
                   Call MPI_Barrier(grid%comm, ierr)
                End Do
             End if
             Call MPI_Barrier(mycomm, ierr)
          End Do
       End Do
       Do ng = 1, region%nGrids
          grid => region%grid(ng)
          Call HYPRE_Pade_Operator_Setup(region, ng)
       End Do
    Else
       ! ... compute the derivative matrices in the computational
       ! ... coordinates.  These are independent of the mesh coordinates, but depend on IBLANK
       Call create_global_mask(region)
       do ng = 1, region%nGrids
          grid => region%grid(ng)
          Call Operator_Setup(region, grid%input, grid, err(ng), ng)
          Call HYPRE_Pade_Operator_Setup(region, ng)
       end do
    End If
    !    Call MPI_Allreduce(sum(err(:)), err_sum, 1, MPI_INTEGER, MPI_SUM, mycomm, ierr)
    !    if (err_sum /= 0) Call Output_Rank_As_IBLANK(region)
    !    call mpi_barrier(mycomm, ierr)

    !     ! ... compute the derivative matrices in the computational
    !     ! ... coordinates.  These are independent of the mesh.
    !     Call create_global_mask(region)
    !     do ng = 1, region%nGrids
    !       grid => region%grid(ng)
    !       Call Operator_Setup(region, grid%input, grid, err(ng))
    !       Call HYPRE_Pade_Operator_Setup(region, ng)
    !     end do

    ! ... if the grid is not moving, compute metrics OUTSIDE loop
    do ng = 1, region%nGrids
       call metrics(region, ng)
    end do
    timer = MPI_Wtime()
    if (input%useMetricIdentities == TRUE) call WriteData(region, 'mid')
    region%mpi_timings(:)%io_write_grid = region%mpi_timings(:)%io_write_grid + (MPI_WTIME() - timer)

    ! ... output diagnostic file if asked
    if (input%outputBC_asIBLANK == TRUE) call output_bc_as_iblank(region)

    ! ... now we are ready to either single step or march in quasi steady steps
    ! ... Ready for timestep(region) or Quasi_SS_March(region)
  end subroutine setup_single_stepping

  subroutine timestep(region)

    USE ModGlobal
    USE ModDataStruct
    USE ModIO
#ifdef HAVE_HDF5
    USE ModHDF5_IO
#endif
    USE ModMPI
    USE ModDerivBuildOps
    USE ModDeriv
    USE ModMetrics
    USE ModInterp
    USE ModRungeKutta

    Implicit None

    type(t_region), pointer :: region

    ! ... local variables
    type(t_rk), pointer :: rk
    type(t_grid), pointer :: grid
    type(t_mixt), pointer :: state
    type(t_mixt_input), pointer :: input
    integer :: ng, nsub, loop_index
    integer :: errflag
    real(rfreal) :: timer
    character(len=3), dimension(:), pointer :: vars_to_write

    ! ... simplicity
    input => region%input
    loop_index = region%global%main_ts_loop_index
    ! ... output
    if (region%myrank == 0) write (*,'(A)') 'PlasComCM: ==== BEGIN SINGLE STEP ===='

    ! ... main time marching loop
    if(debug_on .eqv. .true. .and. region%myrank .eq. 0) then
       write(*,*) 'PlasComCM: Step = ',loop_index
    endif
    ! time = time + dt

    if (input%timeScheme == IMPLICIT) then

       do nsub = 1, input%maxsubits

          ! call implicit solver

       end do ! nsub

    else

       !call LinearMultistepExplicit(region, rk)
       !call DEBUG_NS_RHS(region)
       if (input%timeScheme == EXPLICIT_RK4) then
          if(debug_on .eqv. .true. .and. region%myrank .eq. 0) then
             write(*,*) 'PlasComCM: All processors calling RK4'
          endif
          call ARK2(region)
          if(debug_on .eqv. .true. .and. region%myrank .eq. 0) then
             write(*,*) 'PlasComCM: All processors done with RK4',&
                  loop_index,region%global%main_ts_loop_index
          endif
       endif
       if (input%timeScheme == EXPLICIT_RK5) then
          !         if(debug_on .eqv. .true. .and. region%myrank .eq. 0) then
          !           write(*,*) 'PlasComCM: All processors calling RK5_SteadyState'
          !       endif
          call ARK2(region)
          !          if(debug_on .eqv. .true. .and. region%myrank .eq. 0) then
          !             write(*,*) 'PlasComCM: All processors done with RK5_SteadyState'
          !          endif
       endif

    end if

    ! ... iteration output
    !call mpi_barrier(mycomm,errflag)
    if (region%myrank == 0) &
         write(*,'(A,I7,3(A,E20.8))') 'PlasComCM: iteration = ', &
         loop_index, ', dt = ', region%state(1)%dt(1), &
         ', time = ', region%state(1)%time(1), ', cfl = ', &
         region%state(1)%cfl(1)
    !call mpi_barrier(mycomm,errflag)
    !      if(debug_on .eqv. .true. .and. region%myrank .eq. 0) then
    !         write(*,*) 'RFLOCM: Finalizing time march.'
    !      endif
    ! output routines
    if (mod(loop_index,input%noutput) == 0) then
       call mpi_barrier(mycomm,errflag)
       if(region%myrank .eq. 0) then
          write(*,*) 'PlasComCM: Dumping state at step',loop_index
       endif
       CALL WriteData(region,'cv ')
       if (region%movingGrid == TRUE) then
          CALL WriteData(region,'xyz')
          !call write_metrics(region)
       end if
       call mpi_barrier(mycomm,errflag)
       if(region%myrank .eq. 0) then
          write(*,*) 'PlasComCM: Dump complete.'
       endif
    end if

    ! restart routines
    if ((mod(loop_index,input%nrestart) == 0) .or. &
         loop_index == (input%nstepi+input%nstepmax)) then
       call mpi_barrier(mycomm,errflag)
       timer = MPI_WTime()
       CALL WriteOutput(region,TRUE)
       region%mpi_timings(:)%io_write_restart = region%mpi_timings(:)%io_write_restart + (MPI_WTIME() - timer)
       call mpi_barrier(mycomm,errflag)
    end if
    !call mpi_barrier(mycomm,errflag)
    !      if(debug_on .eqv. .true. .and. region%myrank .eq. 0) then
    !         write(*,*) 'PlasComCM: March complete'
    !      endif
    !    End Do

    !call mpi_barrier(mycomm,errflag)
    if (region%myrank == 0) write (*,'(A)') 'PlasComCM: ==== END SINGLE TIME STEP ===='
    if (region%myrank == 0) write (*,*) 'PlasComCM: Finished Step',loop_index
    region%global%main_ts_loop_index = loop_index

  end subroutine timestep


  ! ... Quasi_SS_March
  ! ... to be called by Rocstar: RocfloCM will take control
  ! ... and march to steady state
  ! ... Steady state: RHS < very small number
  subroutine Quasi_SS_March(region)

    USE ModGlobal
    USE ModDataStruct
    USE ModIO
    USE ModMPI
    USE ModDerivBuildOps
    USE ModDeriv
    USE ModMetrics
    USE ModInterp
    USE ModRungeKutta
    USE ModEOS
    USE ModNavierStokesRHS

    Implicit None

    type(t_region), pointer :: region

    ! ... local variables
    type(t_rk), pointer :: rk
    type(t_grid), pointer :: grid
    type(t_mixt), pointer :: state
    type(t_mixt_input), pointer :: input
    integer :: ng, nsub, i, j, conv_iter, GMRES_iter, GMRES_iter_ave, timeSchemeOld, p, err_sum, k, subiteration, ts_save, ierr
    real(rfreal) :: timer, solution_diff_local_cv, solution_diff_cv,SystemTime, AveHtFlux
    real(rfreal) :: MaxdHtFlux, AveHtFluxOld, DeltaHtFlux, RelativeResidual,RMSdHtFlux,MaxSurfRHS
    integer, allocatable :: err(:)
    character(len=3), dimension(:), pointer :: vars_to_write

    ! ... simplicity
    input => region%input
    region%global%filtercount(:) = 1
    allocate(err(region%nGrids)); err(:) = 0

    ! ... output
    if (region%myrank == 0) write (*,'(A,E20.8)') 'PlasComCM: ==== BEGINNING MARCH TO STEADY STATE : Delta(RMS Heat Flux) < ',input%ConvCrit
    !     do ng = 1, region%nGrids
    !        state => region%state(ng)
    !        allocate(state%timei(size(state%time)))
    !        do i=1,size(state%cv,1)
    !           state%timei(i) = state%time(i)
    !        end do
    !        if (region%myrank == 0 .and. i == 1) write (*,*) 'PlasComCM: Initial time = ',state%timei(i)
    !     end do

    ! ... main time marching loop
    !    Do region%global%main_ts_loop_index = input%nstepi+1, input%nstepi+input%nstepmax
    subiteration = 0
    DeltaHtFlux = 0.0_rfreal
    AveHtFlux = 0.0_rfreal
    region%state(1)%AveHtFluxOld = 0.0_rfreal
    gmres_iter = 0
    SystemTime = region%input%dt*dble(region%global%main_ts_loop_index)/region%input%SndSpdRef*region%input%LengRef

    Do While(region%input%Converged /= TRUE)
       subiteration = subiteration + 1

       ! ... if we are using implicit
       if ((input%timeScheme == BDF_GMRES_IMPLICIT) .AND. input%fluidModel == CNS) then

          if (region%global%main_ts_loop_index == input%nstepi+1) then

             ! ... save initial condition
             Do ng = 1, region%nGrids
                grid  => region%grid(ng)
                state => region%state(ng)
                allocate(state%cvOld1(grid%nCells,size(state%cv,2)))
                allocate(state%cvOld2(grid%nCells,size(state%cv,2)))
                allocate(grid%INVJACOld1(grid%nCells))
                allocate(grid%INVJACOld2(grid%nCells))
                do i = 1, grid%nCells
                   grid%INVJACOld1(i) = grid%INVJAC(i)
                end do
                do j = 1, size(state%cv,2)
                   do i = 1, grid%nCells
                      state%cvOld1(i,j) = state%cv(i,j)
                   end do
                end do
             End Do

          end if

          call BDF_GMRES_IMPLICIT_SUB(region, conv_iter, GMRES_iter)

          ! ... check convergence to steady state
          solution_diff_local_cv = 0.0_8
          do ng = 1, region%nGrids
             state => region%state(ng)
             grid  => region%grid(ng)
             do k = 1, size(state%rhs,2)
                do i = 1, grid%nCells
                   solution_diff_local_cv = MAX(solution_diff_local_cv, abs(state%cv(i,k) - state%cvOld1(i,k)))
                end do
             end do
          end do

          Call MPI_Allreduce(solution_diff_local_cv, solution_diff_cv, 1, MPI_DOUBLE_PRECISION, MPI_MAX, mycomm, ierr)

       else

          ! ... explicit routines

          timer = MPI_WTIME()

          ! ... time accurate nonlinear RK4
          if (input%timeScheme == EXPLICIT_RK4) then
             if (input%fluidModel == CNS) call ARK2(region)
             if (input%fluidModel == Q1D) call RK4_Q1D(region)
             if (input%fluidModel == CYL1D) call RK4_CYL1D(region)
             if (input%fluidModel == LINCNS) call LINRK4(region)
          end if

          ! ... march to steady state with 5-stage, 1st order accurage RK5 scheme of Jameson
          if (input%timeScheme == EXPLICIT_RK5 .and. input%fluidModel == CNS) call ARK2(region,j,MaxSurfRHS)

          ! ... explicit RK4 via ARK2 framework
          if (input%timeScheme == EXPLICIT_RK4 .and. input%fluidModel == CNS) call ARK2(region)

          ! ... time accurate RK4 with implicit part
          if (input%timeScheme == IMEX_RK4 .and. input%fluidModel == CNS) call ARK2(region)

          ! ... time accurate 3-4-1 BDF with RK5 inner loop
          ! ... note :: this must be started with some other method
          if (input%timeScheme == BDF_RK5 .and. input%fluidModel == CNS) then
             if (region%global%main_ts_loop_index == input%nstepi+1) then
                ! ... save initial condition
                Do ng = 1, region%nGrids
                   grid  => region%grid(ng)
                   state => region%state(ng)
                   allocate(state%cvOld1(grid%nCells,size(state%cv,2)))
                   allocate(state%cvOld2(grid%nCells,size(state%cv,2)))
                   do j = 1, size(state%cv,2)
                      do i = 1, grid%nCells
                         state%cvOld1(i,j) = state%cv(i,j)
                      end do
                   end do
                End Do
                ! ... march forward one timestep
                call ARK2(region)
                region%global%rk_alloc = .true.
                conv_iter = 0
             else
                call BDF_RK5_SemiImplicit(region, conv_iter)
             end if
          end if

       end if

       do i = 1, size(region%state(1)%dt)
          region%state(1)%time(i) = SystemTime
       end do

       ! ... calculate average heatflux on this subiteration
       call GetAverageHTFlux(region,AveHtFlux,RMSdHtFlux,MaxdHtFlux)

       ! ... calculate relative residual
       if(subiteration < 2) then
          !         region%state(1)%InitialRHS = 0.264451E4
          region%state(1)%InitialRHS = region%state(1)%CurrentRHSMax
          if (region%myrank == 0) write(*,'(2(A,E20.8))') 'PlasComCM: initial residual = ', region%state(1)%InitialRHS,' convergence criteria: Delta(RMS HtFlux) < ',input%ConvCrit
          region%Input%Converged = FALSE
          RelativeResidual = region%state(1)%CurrentRHSMax/(region%state(1)%InitialRHS + tiny)
          region%state(1)%AveHtFluxOld = AveHtFlux
       elseif(subiteration >= 2)then
          RelativeResidual = region%state(1)%CurrentRHSMax/(region%state(1)%InitialRHS + tiny)
          DeltaHtFlux = abs(AveHtFlux - region%state(1)%AveHtFluxOld)
          region%state(1)%AveHtFluxOld = AveHtFlux
          if(RMSdHtFlux .le. input%ConvCrit) region%input%Converged = TRUE

       endif

       ! ... iteration output
       if (region%myrank == 0) then
          if (input%timeScheme /= BDF_GMRES_IMPLICIT) then
             write(*,'(A,E20.8,A,I8,5(A,E20.11),(A,E20.8),(A,E12.4))') 'PlasComCM: time (s) = ', &
                  region%state(1)%time(1),', subiteration = ',subiteration, ', rel. res. = ', &
                  RelativeResidual,', ave. ht. flux = ',AveHtFlux,', dHtFlux = ',RMSdHtFlux,', MaxdHtFlux = ',MaxdHtFlux, &
                  ', MaxSurfRHS = ',MaxSurfRHS,', dt = ', region%state(1)%dt(1), ', cfl = ', &
                  region%state(1)%cfl(1)
          else
             write(*,'(A,I8,4(A,E20.8),2(A,I5))') 'PlasComCM: iteration = ', &
                  region%global%main_ts_loop_index, ', dt = ', region%state(1)%dt(1), &
                  ', time = ', region%state(1)%time(1), ', cfl = ', &
                  region%state(1)%cfl(1),', Q^(n+1)-Q(n) = ',solution_diff_cv, ', conv_iter = ', conv_iter, ', max GMRES_iter = ', GMRES_iter
          end if
       end if

       ! output routines
       if (mod(subiteration,input%noutput) == 0) then
          timer = MPI_Wtime()
          ts_save = region%global%main_ts_loop_index
          region%global%main_ts_loop_index = subiteration
          call WriteData(region, 'cv ')
          region%global%main_ts_loop_index = ts_save
          call WriteData(region, 'cvt')

          !write( *,* ) 'PlasComCM: Writing VTK files!';        
          !call rocsmash_write_vtk_file( region, region%global%main_ts_loop_index )
          if (input%nAuxVars > 0) call WriteData(region, 'aux')
          if (input%fluidModel == Cyl1D) then
             do ng = 1, region%nGrids
                state => region%state(ng)
                grid  => region%grid(ng)
                call computeDv(region%myrank, grid, state)
             end do
             if (input%write_dv == TRUE) call WriteData(region, 'dv ')
          end if
          region%mpi_timings(:)%io_write_soln = region%mpi_timings(:)%io_write_soln + (MPI_WTIME() - timer)
          if (input%moveGrid == TRUE) then
             timer = MPI_WTIME()
             call WriteData(region, 'xyz')
             region%mpi_timings(:)%io_write_soln = region%mpi_timings(:)%io_write_soln + (MPI_WTIME() - timer)
          end if
       end if

       ! restart routines
       if ((mod(subiteration,input%nrestart) == 0) .or. &
            region%global%main_ts_loop_index == (input%nstepi+input%nstepmax)) then
          timer = MPI_Wtime()
          CALL WriteOutput(region,TRUE)
          region%mpi_timings(:)%io_write_restart = region%mpi_timings(:)%io_write_restart + (MPI_WTIME() - timer)
       end if


       call mpi_barrier(mycomm,ierr)
    End Do

    ! ... we have reached steady-state, write a restart file
    ! ... we will start from this state at next timestep
    timer = MPI_Wtime()
    CALL WriteOutput(region,TRUE)
    region%mpi_timings(:)%io_write_restart = region%mpi_timings(:)%io_write_restart + (MPI_WTIME() - timer)

    if (region%myrank == 0) write (*,'(A)') 'PlasComCM: ==== CONVERGED TO STEADY STATE ===='
    ! ... set flag back to false for next timestep
    region%input%Converged = FALSE
  end subroutine Quasi_SS_March

  subroutine perform_native_output(region)

    USE ModGlobal
    USE ModDataStruct
    USE ModIO
    USE ModMPI
    USE ModDerivBuildOps
    USE ModDeriv
    USE ModMetrics
    USE ModInterp

    Implicit None

    type(t_region), pointer :: region

    ! ... local variables
    type(t_mixt), pointer :: state
    type(t_mixt_input), pointer :: input
    integer :: ng, nsub, loop_index
    integer :: errflag
    character(len=3), dimension(:), pointer :: vars_to_write

    ! ... simplicity
    input => region%input
    loop_index = region%global%main_ts_loop_index

    ! ... output
    if (region%myrank == 0) write (*,'(A)') 'PlasComCM: ==== Dumping Native Data ===='

    if(region%myrank .eq. 0) then
       write(*,*) 'PlasComCM: Dumping state at step',loop_index
    endif
    CALL WriteData(region,'cv ')

    if (region%movingGrid == TRUE) then
       CALL WriteData(region,'xyz')
       !call write_metrics(region)
    end if

    call mpi_barrier(mycomm,errflag)

    if(region%myrank .eq. 0) then
       write(*,*) 'PlasComCM: Writing restart at step',region%global%main_ts_loop_index
    endif
    CALL WriteOutput(region,TRUE)
    call mpi_barrier(mycomm,errflag)

    if(region%myrank .eq. 0) then
       write(*,*) 'PlasComCM: Native dump complete.'
    endif

  end subroutine perform_native_output



  subroutine Initialize_NS_RHS(region, flag, rkStep_in)

    USE ModGlobal
    USE ModDataStruct
    USE ModIO
    USE ModDerivBuildOps
    USE ModDeriv
    USE ModMetrics
    USE ModEOS
    USE ModNavierStokesBC
    USE ModNavierStokesRHS
    USE ModMPI
    USE ModInterp

    Implicit None

    ! ... global variables
    type(t_region), pointer :: region
    integer :: flag
    integer, optional :: rkStep_in

    ! ... local variables
    type(t_mixt), pointer :: state
    type(t_grid), pointer :: grid
    type(t_mpi_timings), pointer :: mpi_timings
    integer :: ng, i, j, ii, rkStep

    ! ... check presence of rkStep
    if (present(rkStep_in) .eqv. .false.) then
       rkStep = 1
    else
       rkStep = rkStep_in
    end if

    ! ... loop over all grids
    do ng = 1, region%nGrids

       ! ... simplicity
       state       => region%state(ng)
       grid        => region%grid(ng)
       mpi_timings => region%mpi_timings(ng)

       ! ... compute the dependent variables
       call computeDv(region%myrank, grid, state)

       ! ... allocate memory, if needed
       call NS_Allocate_Memory(region, ng, flag, rkStep)

       ! ... compute velocity gradient tensor, if needed
       call NS_VelGradTensor(region, ng, flag)

       ! ... compute the LES-modified transport variables, if needed
       ! ... this modifies the pressure (state%dv(1)) and temperature (state%dv(2))
       call NS_LES_Model(region, ng, flag, rkStep)

       ! ... compute the transport variables
       call computeTv(grid, state)

       ! ... compute temperature gradient, if needed
       call NS_Temperature_Gradient(region, ng, flag, rkStep)

       ! ... compute the shock-capturing-modified transport variables, if needed
       call NS_Shock_Capture(region, ng, flag, rkStep)

       ! ... add tvCor to tv
       if ( grid%input%tvCor_in_dt == TRUE) then
          do j = 1, size(state%tv,2)
             do i = 1, size(state%tv,1)
                state%tv(i,j) = state%tv(i,j) + state%tvCor(i,j)
             end do
          end do
       end if

    end do

  end subroutine Initialize_NS_RHS

  subroutine Compute_NS_RHS(region, flag, initRHS)

    USE ModGlobal
    USE ModDataStruct
    USE ModIO
    USE ModDerivBuildOps
    USE ModDeriv
    USE ModMetrics
    USE ModEOS
    USE ModNavierStokesBC
    USE ModNavierStokesRHS
    USE ModNavierStokesImplicit
    USE ModMPI
    USE ModInterp

    Implicit None

    ! ... global variables
    type(t_region), pointer :: region
    integer :: flag
    integer, optional :: initRHS

    ! ... local variables
    type(t_mixt), pointer :: state
    type(t_grid), pointer :: grid
    type(t_mpi_timings), pointer :: mpi_timings
    type(t_mixt_input), pointer :: input
    type(t_patch), pointer :: patch
    integer :: ng, i, j, k, ii, jj, kk, npatch
    real(rfreal) :: ibfac_local, timer

    ! ... simplicity
    !    input => region%input

    ! ... check for initRHS
    if (present(initRHS) .eqv. .false.) initRHS = TRUE

    ! ... initialize rhs vector
    if (initRHS == TRUE) then
       do ng = 1, region%nGrids
          state => region%state(ng)
          do k = 1, size(state%cv,2)
             do i = 1, size(state%cv,1)
                state%rhs(i,k) = 0.0_rfreal
             end do
          end do
       end do
    end if

    ! ... compute the flow equations
    do ng = 1, region%nGrids

       state       => region%state(ng)
       grid        => region%grid(ng)
       mpi_timings => region%mpi_timings(ng)
       input       => grid%input

       ! ... update the target
       timer = MPI_WTIME()
       call NS_Update_Target(region, ng)
       mpi_timings%rhs_bc = mpi_timings%rhs_bc + (MPI_WTIME() - timer)

       ! ... compute the RHS for each grid
       call NS_RHS(region, ng, flag)

    end do

    ! ... zero out the implicit patch terms, if present
    do npatch = 1, region%nPatches

       patch => region%patch(npatch)
       grid  => region%grid(patch%gridID)

       if (associated(patch%implicit_sat_mat) .eqv. .false.) CYCLE

       do kk = 1, grid%ND+2
          do jj = 1, grid%ND+2
             do ii = 1, patch%prodN
                patch%implicit_sat_mat(ii,jj,kk) = 0.0_rfreal
                patch%implicit_sat_mat_old(ii,jj,kk) = 0.0_rfreal
             end do
          end do
       end do

    end do

    ! ... update the rhs with the boundary conditions
    do ng = 1, region%nGrids

       state       => region%state(ng)
       grid        => region%grid(ng)
       mpi_timings => region%mpi_timings(ng)
       input       => grid%input

       ! ... call the boundary conditions
       timer = MPI_WTIME()
       call NS_BC(region, ng, flag)
       mpi_timings%rhs_bc = mpi_timings%rhs_bc + (MPI_WTIME() - timer)

       timer = MPI_WTIME()
       call NS_BC_Fix_Value(region)
       region%mpi_timings(:)%rhs_bc = region%mpi_timings(:)%rhs_bc + (MPI_WTIME() - timer)

       ! ... use iblank to zero out rhs for hole and interpolated points
       do k = 1, size(state%rhs,2)
          do i = 1, size(state%rhs,1)
             state%rhs(i,k) = state%rhs(i,k) * grid%ibfac(i)
          end do
       end do

    end do

  end subroutine Compute_NS_RHS

  !---------------------------------------------------------------
  !
  ! BDF_GMRES :: USE the 3-4-1 BDF with GMRES subiterations
  !
  ! Note: GMRES is implemented via PETSc
  !
  !---------------------------------------------------------------
  subroutine BDF_GMRES_IMPLICIT_SUB(region, conv_iter, GMRES_iter)

    USE ModGlobal
    USE ModDataStruct
    USE ModIO
    USE ModDerivBuildOps
    USE ModDeriv
    USE ModMetrics
    USE ModEOS
    USE ModNavierStokesBC
    USE ModNavierStokesRHS
    USE ModNavierStokesImplicit
    USE ModMPI
    USE ModInterp

    Implicit None

    ! ... global variables
    type(t_region), pointer :: region
    integer :: conv_iter, GMRES_iter

#ifdef USE_HYPRE
    ! ... local variables
    integer :: rkLevel, rkStep, i, j, ii, k, ng
    type(t_grid), pointer :: grid
    type(t_mixt), pointer :: state
    type(t_mixt_input), pointer :: input
    type(t_mpi_timings), pointer :: mpi_timings
    real(rfreal) :: ibfac
    real(rfreal) :: timer, timer_total
    integer, allocatable :: err(:)
    integer :: stop_criteria, restart_iter, inner_iter, min_stencil_start, max_stencil_end, GMRES_iter_local, buildPC, lowerdtau
    real(rfreal) :: dtau_local, dtau, solution_diff_local, solution_diff, solution_diff_init, solution_diff_old, dtcount, dtinc, dtinc2, cfl_local, cfl
    real(rfreal), pointer :: beta(:), SolnSS(:)
    real(rfreal), pointer :: DTauOld

    ! ... timers
    real(rfreal) :: OutT1, OutT2, InT1, InT2, AvgGMRES_iter,RHSResidual,RHSResidual_local,RHSConvergeTest,RHSConvergeTest_local
    integer :: NumChangeDTau, per(MAX_ND),Converged


    ! ... simplicity
    !    input => region%input

    allocate(err(region%nGrids)); err(:) = 0
    timer_total = MPI_WTIME()

    ! ! ... Keep track of what subiteration we are on
    ! input%Sub1 = 0

    if (region%global%rk_alloc .eqv. .true.) then

       region%global%rk_alloc = .false.
       do ng = 1, region%nGrids
          grid => region%grid(ng)
          input => grid%input

          if (region%myrank == 0) then 
             if (input%ImplicitSteadyState == TRUE) then
                write (*,'(A)') 'PlasComCM: ==> Using BDF_GMRES_IMPLICIT (Steady State) time integration <=='
             else
                write (*,'(A)') 'PlasComCM: ==> Using BDF_GMRES_IMPLICIT time integration <=='
             endif
             call GMRESParamOut(input)
          end if

          call mpi_barrier(mycomm, ierr)

          ! ... initialize HYPRE
          min_stencil_start = input%max_stencil_start(input%iFirstDerivImplicit)
          max_stencil_end   = input%max_stencil_end(input%iFirstDerivImplicit)
          if (input%RE > 0.0_rfreal .OR. input%shock /= FALSE .OR. input%LES /= FALSE) then
             min_stencil_start = min(min_stencil_start, input%max_stencil_start(input%iSecondDerivImplicit))
             max_stencil_end   = max(max_stencil_end  , input%max_stencil_end(input%iSecondDerivImplicit))
          end if

          ! ... create the basic objects {A,x,b} to solve Ax = b
          ! ... the objects are filled with non-zero values later since A & b change
          ! ... each {A,x,b} triplet belongs to the grid-specific communicator grid%comm
          Call allocate_hypre_objects(region%nGrids, region%myrank, input%ND)

          ! ... set periodicity
          per(1:MAX_ND) = 0;
          do j = 1, grid%ND
             if (grid%periodic(j) > 0) per(j) = grid%GlobalSize(j)
          end do

          Call init_hypre_objects(grid%nGridsGlobal, grid%iGridGlobal, ng, region%nGrids, &
               grid%myrank_inComm, grid%numproc_inComm, grid%comm, grid%ND, grid%is, grid%ie, &
               min_stencil_start, max_stencil_end, per)
       end do

    endif


    ! ... allocate memory if needed
    do ng = 1, region%nGrids
       state => region%state(ng)
       grid  => region%grid(ng)

       if (.not.associated(state%rhs)   .eqv. .true.) allocate(state%rhs(size(state%cv,1),size(state%cv,2)))
       if (.not.associated(state%cfl)   .eqv. .true.) allocate(state%cfl(size(state%time)))
       if (.not.associated(state%cvOld) .eqv. .true.) allocate(state%cvOld(size(state%cv,1),size(state%cv,2)))
       if (.not.associated(state%dt)    .eqv. .true.) allocate(state%dt(size(state%cv,1)))
       if (.not.associated(state%timeOld) .eqv. .true.) allocate(state%timeOld(size(state%time)))
    end do

    ! ... first save push data down
    Do ng = 1, region%nGrids
       state => region%state(ng)
       grid  => region%grid(ng)
       Do i = 1, grid%nCells
          grid%INVJACOld2(i) = grid%INVJACOld1(i)
          grid%INVJACOld1(i) = grid%INVJAC(i)
       End Do
       Do j = 1, size(state%cv,2)
          Do i = 1, grid%nCells
             state%cvOld2(i,j) = state%cvOld1(i,j)
             state%cvOld1(i,j) = state%cv(i,j)
          End Do
       End Do
    End Do

    ! .. guess the new data
    Do ng = 1, region%nGrids
       state => region%state(ng)
       grid  => region%grid(ng)
       Do j = 1, size(state%cv,2)
          Do i = 1, grid%nCells
             state%cv(i,j) = 2.0_rfreal * state%cvOld1(i,j) - state%cvOld2(i,j)
          End Do
       End Do
    End Do

    ! ... allocate memory, call EOS, call models
    Call Initialize_NS_RHS(region, COMBINED)

    ! ... compute the timestep
    Call NS_Timestep(region)

    ! ... update the time
    dtau_local = 0.0_rfreal
    Do ng = 1, region%nGrids
       state => region%state(ng)
       Do i = 1, size(state%time)
          state%time(i) = state%time(i) + state%dt(i)
       End do
       dtau_local = MAX(dtau_local,MAXVAL(state%dt))
       DTauOld => state%DTAUold
    End do
    Call MPI_Allreduce(dtau_local, dtau, 1, MPI_DOUBLE_PRECISION, MPI_MAX, mycomm, ierr)

    ! ... compute the maximum CFL if running CONSTANT_DT
    do ng = 1, region%nGrids
       state => region%state(ng)
       input => region%grid(ng)%input
       cfl = input%cfl
       if (input%cfl_mode == CONSTANT_DT) then
          cfl_local = -1.0_rfreal
          cfl_local = MAX(MAXVAL(state%cfl),cfl_local)
       end if
    end do
    Call MPI_Allreduce(cfl_local, cfl, 1, MPI_DOUBLE_PRECISION, MPI_MAX, mycomm, ierr)


    do ng = 1,region%nGrids
       ! ... updates needed for moving grids (only when time changes)
       if (input%moveGrid == TRUE) then

          ! ... move only the rotor grid
          ! call Move_Rotor_Grid(region)

          ! ... move only the cylinder grid
          call Move_Cylinder_Grid(region)

          ! ... Chimera-specific updates
          if (input%gridType == CHIMERA) then

             ! ... set up the new grid interp
             call Setup_Interpolation(region)

             ! ... update operators since they depend on IBLANK
             Call create_global_mask(region)
             !              do ng = 1, region%nGrids
             grid => region%grid(ng)
             Call Operator_Setup(region, input, grid, err(ng), ng)
             Call HYPRE_Pade_Operator_Setup(region, ng)
             !             end do
             if (sum(err(:)) /= 0) Call Output_Rank_As_IBLANK(region)

          end if


          ! ... update metrics
          !      do ng = 1, region%nGrids
          call metrics(region, ng)
          !      end do
       end if
    end do

    if (.false.)then
       do ng = 1,region%nGrids
          input => region%grid(ng)%input
          ! ... Files for output of diagnostic info
          if (region%global%main_ts_loop_index == input%nstepi+1 .and. region%myrank == 0) then
             write(*,'(A)') 'Outputing diagnostic information ...'
             print*, input%DTauDtRatio, input%DTauDecreaseFac, input%EuclidLevel, input%KDim, input%GMRESConvThresh
             open(unit = 201,file = 'MainIterations.txt',form = 'formatted',status = 'unknown')
             open(unit = 202,file = 'SubIterations.txt',form = 'formatted',status = 'unknown')

             write(201,'(a)') 'DTauDtRatio, DTauDecreaseFac, EuclidLevel, KDim, GMRES_Tolerance'
             write(201,123) input%DTauDtRatio, input%DTauDecreaseFac, input%EuclidLevel, input%KDim, input%GMRESConvThresh
             write(201,'(a)') 'Avg GMRES Iterations, Number Subiterations, Time for 1 ts, NumberDtauInc., dtau/dt, timestep'


             write(202,'(a)') 'DTauDtRatio, DTauDecreaseFac, EuclidLevel, KDim, GMRES_Tolerance'
             write(202,123) input%DTauDtRatio, input%DTauDecreaseFac, input%EuclidLevel, input%KDim, input%GMRESConvThresh 
             write(202,'(a)') 'SubIterNumber, NumGMRES, SubIterTime, DTau/dt, CFL, deltaQp, Residual'

123          format(e10.3,1x,f7.3,1x,i1,1x,i3,1x,e12.4)
          elseif (region%myrank == 0) then
             open(unit = 201, file = 'MainIterations.txt',form = 'formatted',status = 'old',position = 'append')
             open(unit = 202, file = 'SubIterations.txt',form = 'formatted',status = 'old',position = 'append')
          end if
       end do
       call mpi_barrier(mycomm, ierr)
    end if
    ! ... initialize 
    AvgGMRES_iter = 0.0
    NumChangeDTau = 0    
    dtcount = 0
    dtinc = 0
    dtinc2 = 0
    Converged = FALSE
    GMRES_iter         = 0
    stop_criteria      = FALSE
    conv_iter          = 0
    restart_iter       = 0
    solution_diff_old  = 2.0_rfreal
    solution_diff_init = 1.0_rfreal
    solution_diff      = 1.0_rfreal
    !    dtau = 10.0_rfreal / cfl * dtau * input%DTauDtRatio

    do ng = 1,region%nGrids
       state => region%state(ng)
       input => region%grid(ng)%input
       dtau = state%dt(1) * input%DTauDtRatio
       if (region%global%main_ts_loop_index > input%nstepi+1) dtau = DTauOld
    end do


    ! ... set long time solution for analytical RHS: u_t = u_ss - u

    ! Allocate(SolnSS(size(state%cv,2)))
    ! SolnSS(1) = 1.0_rfreal
    ! SolnSS(2) = 0.2_rfreal * SolnSS(1)
    ! SolnSS(size(state%cv,2)) = SolnSS(2)**2
    ! if (size(state%cv,2) >= 2) then
    !    SolnSS(3) = SolnSS(2)
    !    SolnSS(size(state%cv,2)) = SolnSS(size(state%cv,2)) + SolnSS(3)**2
    ! endif
    ! if (size(state%cv,2) == 3) then
    !    SolnSS(4) = SolnSS(2)
    !    SolnSS(size(state%cv,2)) = SolnSS(size(state%cv,2)) + SolnSS(4)**2
    ! endif

    ! SolnSS(size(state%cv,2)) = 1/input%GamRef*1/(input%GamRef - 1.0_rfreal) &
    !      + 0.5_rfreal * SolnSS(size(state%cv,2))/SolnSS(1)

    ! print*,SolnSS




    !    Call CPU_Time(OutT1)

    ! ... loop over k until convergence
    Do While (stop_criteria /= TRUE) 


       ! ... Keep track of what subiteration we are on

       do ng = 1, region%nGrids
          region%grid(ng)%input%Sub1 = conv_iter
       end do

       !      Call CPU_Time(InT1)

       ! ... save this solution
       do ng = 1, region%nGrids
          grid  => region%grid(ng)
          state => region%state(ng)
          do j = 1, size(state%cv,2)
             do i = 1, size(state%cv,1)
                state%cvOld(i,j) = state%cv(i,j)
             end do
          end do
       end do

       ! ... allocate memory, call EOS, call models
       if (conv_iter /= 0) Call Initialize_NS_RHS(region, COMBINED)

       ! ... compute the timestep
       ! if (conv_iter == 0) Call NS_Timestep(region)

       ! ... compute the explicit RHS only
       ! ... state%rhs = J ( d{Ehat}/d{xi} + d{Fhat}/d{eta} + d{Ghat}/d{zeta} )
       Call Compute_NS_RHS(region, COMBINED, TRUE)

       !    ! ... if we are just starting transient simulation then we need initial guess for state%cv
       !    if ((region%global%main_ts_loop_index == input%nstepi+1)) then
       !    do ng = 1, region%nGrids
       !      grid  => region%grid(ng)
       !      state => region%state(ng)
       !      do j = 1, size(state%cv,2)
       !        do i = 1, size(state%cv,1)
       !          state%cv(i,j) = state%cvOld1(i,j) + state%dt(i) * state%rhs(i,j)
       !        end do
       !      end do
       !    end do
       ! endif

       ! ... if this is steady-state
       RHSResidual_local = 0.0_rfreal
       RHSConvergeTest = 0.0_rfreal
       RHSConvergeTest_local = 0.0_rfreal


       do ng = 1, region%nGrids
          grid  => region%grid(ng)
          state => region%state(ng)
          input => grid%input

          if (input%ImplicitTrueSteadyState == TRUE) then
             ! ... divide out the Jacobian, multiply by dtau, don't include the unsteady term
             do k = 1, size(state%rhs,2)
                do i = 1, size(state%rhs,1)
                   !state%rhs(i,k) = SolnSS(k) - state%cv(i,k)!*grid%JAC(i)
                   RHSConvergeTest_local = MAX(RHSConvergeTest_local , abs(state%RHS(i,k)))
                   state%rhs(i,k) =  dtau * grid%INVJAC(i) * state%rhs(i,k)
                   RHSResidual_local = MAX(RHSResidual_local , abs(state%RHS(i,k)))
                   ! if (RHSResidual_local .gt. 1d-10) print*,'RHSResidual_local',RHSResidual_local
                end do
             end do



             ! ... if we are just starting transient simulation then we need implicit Euler
          else if ((region%global%main_ts_loop_index == input%nstepi+1) .or. (input%ImplicitSteadyState == TRUE)) then

             ! ... divide out the Jacobian, multiply by dtau, include the unsteady term
             !        do ng = 1, region%nGrids

             grid  => region%grid(ng)
             state => region%state(ng)

             do k = 1, size(state%rhs,2)
                do i = 1, size(state%rhs,1)
                   !              state%rhs(i,k) = SolnSS(k) - state%cv(i,k)!*grid%JAC(i)
                   RHSConvergeTest_local = MAX(RHSConvergeTest_local , abs(state%RHS(i,k)))
                   !               if(conv_iter == 0 .and. RHSConvergeTest .le. 1.0d-10) input%Converged = TRUE


                   state%rhs(i,k) = dtau * grid%INVJAC(i) * state%rhs(i,k) - ( (  state%cv(i,k) * grid%INVJAC(i) &
                        - state%cvOld1(i,k) * grid%INVJACOld1(i) ) / (state%dt(i)) ) * dtau
                   RHSResidual_local = MAX(RHSResidual_local , abs(state%RHS(i,k)))
                   !              print*,state%cv(i,k)*grid%INVJAC(i)-state%cvOld1(i,k)*grid%INVJACOld1(i)
                end do
             end do

             ! end do
             !call graceful_exit(region%myrank, 'okay, here we are ...')
          else

             ! ... divide out the Jacobian, multiply by dtau, include the unsteady term
             ! do ng = 1, region%nGrids

             grid  => region%grid(ng)
             state => region%state(ng)

             do k = 1, size(state%rhs,2)
                do i = 1, size(state%rhs,1)
                   RHSConvergeTest_local = MAX(RHSConvergeTest_local , abs(state%RHS(i,k)))
                   !              state%rhs(i,k) = SolnSS(k) - state%cv(i,k)!*grid%JAC(i)
                   state%rhs(i,k) = dtau * grid%INVJAC(i) * state%rhs(i,k) - ( (  3.0_dp * state%cv(i,k)     * grid%INVJAC(i) &
                        - 4.0_dp * state%cvOld1(i,k) * grid%INVJACOld1(i) &
                        + 1.0_dp * state%cvOld2(i,k) * grid%INVJACOld2(i) ) / (2.0_dp * state%dt(i)) ) * dtau
                   RHSResidual_local = MAX(RHSResidual_local , abs(state%RHS(i,k)))
                end do
             end do

             !end do

          end if
       end do

       if(conv_iter == 0) then
          Call MPI_Allreduce(RHSConvergeTest_local, RHSConvergeTest, 1, MPI_DOUBLE_PRECISION, MPI_MAX, mycomm, ierr)
          if(region%myrank == 0) print*,'RHSConvergeTest',RHSConvergeTest
          do ng = 1,region%nGrids
             input => region%grid(ng)%input
             if (RHSConvergeTest .le. 1.0d-10) then
                input%Converged = TRUE
                return
             else
                input%Converged = FALSE
             end if
          end do
       end if

!!!!!!!!!chris is outputting the tvcor to target 2000 + conv_iter*100
       if (.false.) then
          do ng = 1, region%nGrids
             state => region%state(ng)
             grid  => region%grid(ng)
             do i = 1, grid%nCells
                state%timeOld(i)  = state%time(i)
                state%time(i) = dble(2000 + conv_iter*100)
             end do
             !          do k = 1, size(state%rhs,2)
             do k = 1, size(state%tvcor,2)
                do i = 1, grid%nCells
                   state%cvTarget(i,k) = state%tvCor(i,k)
                   !              state%cvTarget(i,k) = state%rhs(i,k)
                end do
             end do
          end do
          CALL WriteData(region,'cvt',2000 + 100*conv_iter)
!          call write_plot3d_file(region, 'cvt',2000 + 100*conv_iter)
          do ng = 1, region%nGrids
             state => region%state(ng)
             grid  => region%grid(ng)
             do i = 1, grid%nCells
                state%time(i) = state%timeOld(i)
             end do
             do k = 1, grid%ND+2 
                do i = 1, grid%nCells
                   state%cvTarget(i,k) = state%cvTargetOld(i,k)
                end do
             end do
          end do

!!!!!!!!!!!!!!!!!!!! end of RHS output !!!!!!!!!!!!!!!!!!!!!!!
       end if




       ! ... update the PC ?
       if (conv_iter == 0) buildPC = TRUE
       Call NS_Implicit_Jacobian(region, COMBINED, dtau, GMRES_iter_local, buildPC, lowerdtau)
       GMRES_iter = MAX(GMRES_iter, GMRES_iter_local)
       buildPC = FALSE

       ! ... save previous value of solution_diff
       solution_diff_old = solution_diff

       ! ... check convergence
       solution_diff_local = 0.0_rfreal
       do ng = 1, region%nGrids
          state => region%state(ng)
          grid  => region%grid(ng)
          do k = 1, size(state%rhs,2)
             do i = 1, grid%nCells
                solution_diff_local = MAX(solution_diff_local, abs(state%cv(i,k) - state%cvOld(i,k)))
             end do
          end do
       end do

       RHSResidual_local = RHSResidual_local/dtau

       Call MPI_Allreduce(solution_diff_local, solution_diff, 1, MPI_DOUBLE_PRECISION, MPI_MAX, mycomm, ierr)
       Call MPI_Allreduce(RHSResidual_local, RHSResidual, 1, MPI_DOUBLE_PRECISION, MPI_MAX, mycomm, ierr)

       ! ... save initial value of solution_diff
       if (conv_iter == 0) solution_diff_init = solution_diff

       ! ... adjust dtau based on history
       if(conv_iter > 0) then
          !        if (solution_diff/solution_diff_init > 100.0_rfreal) dtcount = 20
          !        if (GMRES_iter_local > 100) dtcount = 20
          !!         if ((solution_diff > solution_diff_old) .OR. (solution_diff > 1d-3))then
          if(.false.) then
             if ((solution_diff > solution_diff_old))then
                dtcount = dtcount + 1

                !          if (dtcount > input%DivThresh .and. GMRES_iter_local > 1) then
                dtcount = 0
                dtau = dtau * input%DTauDecreaseFac
                ! ... go back to previous solution
                do ng = 1, region%nGrids
                   grid  => region%grid(ng)
                   state => region%state(ng)
                   do j = 1, size(state%cv,2)
                      do i = 1, grid%nCells
                         state%cv(i,j) = state%cvOld(i,j)
                      end do
                   end do
                end do
                buildPC = TRUE
                if (region%myrank == 0) write (*,'(A,E20.12,A,E20.12)') '  PlasComCM: Lowering dtau to: ', dtau,' rel conv:',solution_diff/solution_diff_init
                NumChangeDTau = NumChangeDTau + 1
                CYCLE

             elseif (GMRES_iter_local > 1 .and. dtinc > input%DTauIncThresh .and. NumChangeDTau < 1)then
                dtau = dtau * input%DTauIncFac
                if (region%myrank == 0) print*,'input%DTauIncThresh',input%DTauIncThresh
                if (region%myrank == 0) write(*,'(A,E20.12,A,E20.12)') '  PlasComCM: dtau increased from ', dtau/input%DTauIncFac,'to', dtau
                buildPC = TRUE
                dtcount = 0
                dtinc = 0
                dtinc2 = dtinc2 + 1
             end if
          end if
       end if
       ! ... counter between dTau increases
       dtinc = dtinc + 1

       ! ... update convergence counter
       conv_iter = conv_iter + 1

       ! ... check for convergence
       if (solution_diff_init <= 1d-16) then
          stop_criteria = TRUE
       else if (solution_diff <= 1d-10) then
          stop_criteria = TRUE
          ! ... for now, just use the input for grid 1
       else if (RHSResidual <= 1d-10 .OR. conv_iter == region%grid(1)%input%maxsubits) then
          stop_criteria = TRUE
       end if


       ! if (stop_criteria == TRUE)then
       !    if (region%myrank == 0) write(*,*) 'Not stopping on convergence'
       !    stop_criteria = FALSE
       ! end if

       if(.true.)then
          if (stop_criteria == TRUE .and. conv_iter <= 1 .and. GMRES_iter < 30)then
             if (region%myrank == 0) write(*,*) 'Increasing timestep'
             do ng = 1,region%nGrids
                input => region%grid(ng)%input
                input%dt = input%dt * 1.1_rfreal
             end do
          end if
       end if
       ! ... diagonstic output
       if (region%myrank == 0) then
          if (solution_diff_init <= 1d-16) then
             write (*,'(A,I4,A,E12.4)') '  PlasComCM: BDF_GMRES_IMPLICIT: subiteration: ', conv_iter, ', relative convergence: ', solution_diff_init
          else
             write (*,'(A,I4,A,E12.4,A,E12.4,A,E12.4)') '  PlasComCM: BDF_GMRES_IMPLICIT: subiteration: ', conv_iter, ', relative convergence: ', solution_diff/solution_diff_init, &
                  ' RHS: ', RHSResidual,' solution_diff: ',solution_diff
          end if
       end if

       ! ... output
       if (region%grid(1)%input%writeSubItData == TRUE) then
          do ng = 1, region%nGrids
             state => region%state(ng)
             grid  => region%grid(ng)
             do i = 1, grid%nCells
                state%timeOld(i)  = state%time(i)
                state%time(i) = dble(conv_iter)
             end do
             do k = 1, grid%ND+2
                do i = 1, grid%nCells
                   state%cvTarget(i,k) = state%cv(i,k) - state%cvOld(i,k)
                end do
             end do
          end do
          call WriteData(region, 'cvt', conv_iter)
          do ng = 1, region%nGrids
             state => region%state(ng)
             grid  => region%grid(ng)
             do i = 1, grid%nCells
                state%time(i) = state%timeOld(i)
             end do
             do k = 1, grid%ND+2
                do i = 1, grid%nCells
                   state%cvTarget(i,k) = state%cvTargetOld(i,k)
                end do
             end do
          end do
       end if

       if (.false.)then
          ! ... Write subiteration diagnostic information
          !      Call CPU_Time(InT2)
          if (solution_diff_init > TINY) then
             if (region%myrank == 0) write(202,*) conv_iter, GMRES_iter_local, InT2-InT1,dtau/state%dt(1),input%CFL, solution_diff/solution_diff_init, solution_diff
          end if
       end if
       ! ... keeping track the average number of GMRES iterations for this timestep
       AvgGMRES_iter = AvgGMRES_iter + GMRES_iter_local
       call mpi_barrier(mycomm, ierr)

    End Do

    ! ... filter the conservative variables
    if (input%filter .eqv. .true.) then
       do ng = 1, region%nGrids
          call filter(region,ng,FALSE)
       end do
    end if

    if (.false.)then
       ! ... write time step diagnostic information
       ! ... keeping track the average number of GMRES iterations for this timestep
       AvgGMRES_iter = AvgGMRES_iter/real(conv_iter)
       !       Call CPU_Time(OutT2)
       if (region%myrank == 0) then
          write(201,124) AvgGMRES_iter, conv_iter, OutT2-OutT1,NumChangeDTau,input%CFL,state%dt(1)
124       format(8x,e10.3,7x,8x,i4,8x,7x,f8.3,7x,i3,8x,f5.2,2x,e10.3)
          close(201)
          close(202)
       end if
    end if

    DTauOld = dtau


    ! ... clean up and exit
    !    deallocate(SolnSS)

    call mpi_barrier(mycomm, ierr)

#endif

  end subroutine BDF_GMRES_IMPLICIT_SUB

  Subroutine GMRESParamOut(input)

    USE ModGlobal
    USE ModDataStruct
    USE ModIO
    USE ModDerivBuildOps
    USE ModDeriv
    USE ModMetrics
    USE ModEOS
    USE ModNavierStokesBC
    USE ModNavierStokesRHS
    USE ModNavierStokesImplicit
    USE ModMPI
    USE ModInterp

    Implicit None

    Type(t_mixt_input), pointer :: input
    Integer :: PCNum
    character(len=40) :: PCName

    PCNum = input%HypreParamsInt(1)

    if (PCNum == 0) then
       write(PCName(1:40),'(A)') ' Solver: GMRES Preconditioner: None     '
    elseif ( PCNum == 1) then
       write(PCName(1:40),'(A)') ' Solver: GMRES Preconditioner: Euclid  '
    elseif (PCNum == 2) then
       write(PCName(1:40),'(A)') ' Solver: GMRES Preconditioner: ParaSails'
    elseif (PCNum == 3) then
       write(PCName(1:40),'(A)') ' Solver: GMRES Preconditioner: BoomerAMG'
    elseif (PCNum == 4) then
       write(PCName(1:40),'(A)') ' Solver: BoomerAMG  '
    endif
    ! ... output Hypre parameters, if desired

    if (.true. .eqv. .true.) then
       write (*,'(A)') 'PlasComCM: ============> Solver Parameters <================='
       write (*,'(A)') '        ================================================='
       write (*,144) PCName
       write (*,145) ' Solver Maximum Iterations = ', input%HypreParamsInt(2)
       write (*,145) ' Krylov Space Dimension = ', input%HypreParamsInt(3)
       write (*,146) ' Outer Loop Preconditioner Length, Lu = ',input%Lu
       write (*,'(A)') '        ================================================='
    endif

144 format(5X,A40)
145 format(5X,A43,I7.7)
146 format(5X,A42,F7.2)
  end Subroutine GMRESParamOut


END MODULE ModTimemarch
