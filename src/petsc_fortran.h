#ifdef HAVE_PETSC

#ifdef PETSC_NAMESPACED_HEADERS
#include <petsc/finclude/petscsys.h>
#include <petsc/finclude/petscvec.h>
#include <petsc/finclude/petscmat.h>
#include <petsc/finclude/petscksp.h>
#include <petsc/finclude/petscpc.h>
#include <petsc/finclude/petscvec.h90>
#include <petsc/finclude/petscis.h>
#include <petsc/finclude/petscviewer.h>
#else
#include <finclude/petscsys.h>
#include <finclude/petscvec.h>
#include <finclude/petscmat.h>
#include <finclude/petscksp.h>
#include <finclude/petscpc.h>
#include <finclude/petscvec.h90>
#include <finclude/petscis.h>
#include <finclude/petscviewer.h>
#endif

#endif
