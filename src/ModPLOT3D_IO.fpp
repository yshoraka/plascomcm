! Copyright (c) 2015, University of Illinois at Urbana-Champaign, XPACC
! License: MIT, http://opensource.org/licenses/MIT
Module ModPLOT3D_IO

  Implicit None

Contains

  SUBROUTINE ReadRestart_P3D(region, fname)

    USE ModGlobal
    USE ModDataStruct
    USE ModMPI

    IMPLICIT None

    TYPE (t_region), POINTER :: region
    CHARACTER(LEN=*) :: fname(2)

    ! ... local variables
    TYPE (t_mixt), POINTER :: state
    TYPE (t_grid), POINTER :: grid
    REAL(rfreal), DIMENSION(:,:,:,:,:), POINTER :: Y ! ... read the whole grid for MONOLITHIC, JKim 04/2008
    REAL(rfreal), DIMENSION(:,:,:,:), POINTER :: X ! ... reduction in rank for CHIMERA, JKim 04/2008
    REAL(rfreal) :: tau(4), timer(2)
    CHARACTER(LEN=2) :: prec, gf, vf
    INTEGER, DIMENSION(:,:), POINTER :: ND
    INTEGER :: NDIM, ngrid, ng, var(5), m, k, j, i, l0, N(MAX_ND), p, nvar, ierr
    INTEGER :: gridID ! ... specify which grid we want to read, JKim 04/2008
    INTEGER :: output_flag
    REAL(rfreal), DIMENSION(:,:), POINTER :: ptr_to_data, ptr_to_auxVars ! ... Adjoint N-S

    output_flag = 0
!    if(region%myrank == 0) output_flag = 1

    timer(1) = MPI_WTime()

    CALL mpi_barrier(mycomm,ierr)
!    IF((debug_on .EQV. .TRUE.) .AND. (region%myrank == 0)) WRITE(*,*) 'PlasComCM: ReadGridSize'
    IF(region%myrank == 0) THEN
       CALL ReadGridSize_P3D(NDIM, ngrid, ND, fname(1), output_flag)
    ENDIF
    CALL MPI_Bcast(ngrid,1,MPI_INTEGER,0,mycomm,ierr)
    CALL MPI_Bcast(NDIM,1,MPI_INTEGER,0,mycomm,ierr)
    IF(region%myrank .NE. 0) THEN
       ALLOCATE(ND(ngrid,3))
    ENDIF
    CALL MPI_Bcast(ND,3*ngrid,MPI_INTEGER,0,mycomm,ierr)
!    CALL mpi_barrier(mycomm,ierr)
!    IF((debug_on .EQV. .TRUE.) .AND. (region%myrank == 0)) WRITE(*,*) 'PlasComCM: read_restart done with grid size'
    IF(region%myrank == 0) THEN
       CALL ReadSolutionHeader_P3D(NDIM, ngrid, ND, fname(1), tau)
    ENDIF
    CALL MPI_BCast(tau,4,MPI_DOUBLE_PRECISION,0,mycomm,ierr)
!    CALL mpi_barrier(mycomm,ierr)
!    IF((debug_on .EQV. .TRUE.) .AND. (region%myrank == 0)) WRITE(*,*) 'PlasComCM: read_restart done with soln header'
    IF (region%input%nAuxVars > 0) THEN
      IF(TRIM(fname(2)) == "NONE") THEN
         i = INDEX(fname(1),'.',BACK=.TRUE.)
         fname(2) = TRIM(fname(1)(1:i) // 'aux.q')
         IF((debug_on .EQV. .TRUE.) .AND. (region%myrank == 0)) WRITE(*,*) 'PlasComCM: ReadRestart_P3D changed filename to',fname(2) 
      ENDIF
      IF(region%myrank == 0) THEN
         CALL ReadFuncHeader_P3D(NDIM, ngrid, fname(2), nvar)
      ENDIF
      CALL MPI_Bcast(nvar,1,MPI_INTEGER,0,mycomm,ierr)
      IF((debug_on .EQV. .TRUE.) .AND. (region%myrank == 0)) WRITE(*,'(A,I2,A)') 'PlasComCM: Found ', nvar, ' auxiliary variables in file "'//TRIM(fname(2))//'".'
      !!Luca: please leave this uncommented. If a  restart has fewer variable than the simulation they are assumed zero.  
      !!if (nvar /= region%input%nAuxVars) then
      !!  call graceful_exit(region%myrank, 'PlasComCM: ERROR: Number of auxVars is incorrect in restart file.')
      !!end if
    END IF

    ! ... pointer
    Do m = 1, 5
      var(m) = m
    End Do

    If (NDIM == 3 .and. region%input%ND == 2 .and. ND(1,3) == 1) Then
      if (region%myrank == 0) print *, 'PlasComCM: Adjusting for 2D in ReadRestart_P3D.'
      var(4) = 5
    End If

    if (region%myrank == 0) then
      Write (*,'(A)') 'PlasComCM: Reading "'//trim(fname(1))//'".'
      if (region%input%nAuxVars > 0) Write (*,'(A)') 'PlasComCM: Also reading "'//trim(fname(2))//'".'
    end if
    
    Do ng = 1, region%nGrids
       
       ! ... this grid's data
       grid  => region%grid(ng)
       state => region%state(ng)
       
       ! ... nullify
       nullify(ptr_to_data)
       ptr_to_data => state%cv
       if (region%input%AdjNS) ptr_to_data => state%av ! ... Adjoint N-S
       
       region%mpi_timings(ng)%io_read_soln = MPI_Wtime()

       call ReadSolution_P3D(NDIM, ND, region, grid, fname(1), ptr_to_data, grid%iGridGlobal)

       if (region%input%nAuxVars > 0) then
          nullify(ptr_to_auxVars)
          ptr_to_auxVars => state%auxVars
          call ReadFunc_P3D(NDIM, ND, region, grid, fname(2), ptr_to_auxVars, grid%iGridGlobal, nvar)
       end if       
       region%mpi_timings(ng)%io_read_soln = MPI_Wtime() - region%mpi_timings(ng)%io_read_soln
       
       
       ! ... update the time
       do k = 1, grid%nCells
          state%time(k) = tau(4)
       end do
       
       ! ... update the iteration number
       region%input%nstepi = nint(tau(1))
       ! ... read in the next sampling time
       region%state(1)%t_output_next = tau(2)
       ! ... temporary fix
       grid%input%nstepi = region%input%nstepi
       
    end do

    deallocate(ND)

    call MPI_BARRIER(MYCOMM,ierr)
    if ((debug_on .eqv. .true.) .and. (region%myrank == 0)) Write (*,'(A)') 'PlasComCM: Done reading "'//trim(fname(1))//'".'

    return

  end subroutine ReadRestart_P3D

  SUBROUTINE ReadTarget_P3D(region, fname)

    USE ModGlobal
    USE ModDataStruct
    USE ModMPI

    IMPLICIT NONE

    TYPE (t_region), POINTER :: region
    CHARACTER(LEN=80) :: fname, fnameAux

    ! ... local variables
    TYPE (t_mixt), POINTER :: state
    TYPE (t_grid), POINTER :: grid
    REAL(rfreal), DIMENSION(:,:,:,:,:), POINTER :: Y ! ... read the whole grid for MONOLITHIC, JKim 04/2008
    REAL(rfreal), DIMENSION(:,:,:,:), POINTER :: X ! ... reduction in rank, JKim 04/2008
    REAL(rfreal) :: tau(4)
    CHARACTER(LEN=2) :: prec, gf, vf
    INTEGER, DIMENSION(:,:), POINTER :: ND
    INTEGER :: NDIM, ngrid, ng, var(5), m, i, j, k, l0, N(MAX_ND), p, ierr, Ijunk, nvar,output_flag
    REAL(rfreal), DIMENSION(:,:), POINTER :: ptr_to_data, ptr_to_auxVars ! ... Adjoint N-S

    output_flag = 0
!    if(region%myrank == 0) output_flag = 1

    IF(region%myrank == 0) THEN
       CALL ReadGridSize_P3D(NDIM, ngrid, ND, fname, output_flag)
    ENDIF
    CALL MPI_Bcast(NDIM,1,MPI_INTEGER,0,mycomm,ierr)
    CALL MPI_Bcast(ngrid,1,MPI_INTEGER,0,mycomm,ierr)
    IF(region%myrank .NE. 0) THEN
       ALLOCATE(ND(ngrid,3))
    ENDIF
    CALL MPI_Bcast(ND,ngrid*3,MPI_INTEGER,0,mycomm,ierr)

    IF (region%myrank == 0) WRITE (*,'(A)') 'PlasComCM: Reading "'//TRIM(fname)//'".'

    IF (region%input%nAuxVars > 0) THEN
      Ijunk = INDEX(fname,'.',BACK=.TRUE.)
      fnameAux = TRIM(fname(1:Ijunk) // 'aux.q')
      IF(region%myrank == 0) THEN
         CALL ReadFuncHeader_P3D(NDIM, ngrid, fnameAux, nvar)
      ENDIF
      CALL MPI_Bcast(nvar,1,MPI_INTEGER,0,mycomm,ierr)
      IF (region%myrank == 0) WRITE (*,'(A)') 'PlasComCM (read_target): Also reading "'//TRIM(fnameAux)//'".'
    ENDIF

    DO ng = 1, region%nGrids
       
       grid  => region%grid(ng)
       state => region%state(ng)
       
       NULLIFY(ptr_to_data)
       ptr_to_data => state%cvTarget
       IF (region%input%AdjNS) ptr_to_data => state%avTarget ! ... Adjoint N-S
       

       region%mpi_timings(ng)%io_read_target = MPI_WTime()
       CALL ReadSolution_P3D(NDIM, ND, region, grid, fname, ptr_to_data, grid%iGridGlobal)
       
       IF (region%input%nAuxVars > 0) THEN
          IF (region%myrank == 0) WRITE (*,*) 'PlasComCM (read_target): ubound(state%auxVarsTarget)', UBOUND(state%auxVarsTarget)
          CALL ReadFunc_P3D(NDIM, ND, region, grid, fnameAux, state%auxVarsTarget, grid%iGridGlobal, nvar)
       END IF
       region%mpi_timings(ng)%io_read_target = MPI_WTime() - region%mpi_timings(ng)%io_read_target
       
    END DO
    
    DEALLOCATE(ND) ! ... JKim 04/2008

    CALL MPI_BARRIER(MYCOMM,ierr)
    IF ((debug_on .EQV. .TRUE.) .AND. (region%myrank == 0)) WRITE (*,'(A)') 'PlasComCM: *Done reading "'//TRIM(fname)//'".'

  END SUBROUTINE ReadTarget_P3D

  subroutine ReadMean_P3D(region, fname)

    USE ModGlobal
    USE ModDataStruct
    USE ModMPI

    Implicit None

    Type (t_region), Pointer :: region
    Character(LEN=80) :: fname

    ! ... local variables
    Type (t_mixt), Pointer :: mean
    Type (t_grid), Pointer :: grid
    real(rfreal) :: tau(4)
    character(len=2) :: prec, gf, vf
    Integer, Dimension(:,:), Pointer :: ND
    Integer :: NDIM, ngrid, ng, var(5), m, i, j, k, l0, N(MAX_ND), p, ierr, output_flag

    output_flag = 0
!    if(region%myrank == 0) output_flag = 1

    IF((debug_on.eqv..true.).and.(region%myrank == 0)) Write (*,*) 'PlasComCM: ReadMean_P3D::Reading grid size'
    IF(region%myrank == 0) THEN
       CALL ReadGridSize_P3D(NDIM, ngrid, ND, fname, output_flag)
    ENDIF
    CALL MPI_Bcast(NDIM,1,MPI_INTEGER,0,mycomm,ierr)
    CALL MPI_Bcast(ngrid,1,MPI_INTEGER,0,mycomm,ierr)
    IF(region%myrank .NE. 0) THEN
       ALLOCATE(ND(ngrid,3))
    ENDIF
    CALL MPI_Bcast(ND,ngrid*3,MPI_INTEGER,0,mycomm,ierr)

    IF((region%myrank == 0))  Write (*,'(A)') 'PlasComCM: Reading "'//trim(fname)//'".'
    
          Do ng = 1, region%nGrids
             
             grid  => region%grid(ng)
             mean => region%mean(ng)
             IF(debug_on .eqv. .true.) write(*,*) 'PlasComCM:',region%myrank,'reading single soln lowmem'
             region%mpi_timings(ng)%io_read_mean = MPI_WTime()
             call ReadSolution_P3D(NDIM, ND, region, grid, fname, mean%cv, grid%iGridGlobal)
             region%mpi_timings(ng)%io_read_mean = MPI_WTime() - region%mpi_timings(ng)%io_read_mean
          end do

    call MPI_BARRIER(MYCOMM,ierr)
    IF((region%myrank == 0)) Write (*,'(A)') 'PlasComCM: **Done reading mean from "'//trim(fname)//'".'
  end subroutine ReadMean_P3D

  Subroutine ReadGridSize_P3D(NDIM, ngrid, ND, file, OUTPUT_FLAG, ib)

    Implicit None

    Character(LEN=2), Intent(out), Optional :: ib
    Real(KIND=8), Dimension(:,:,:,:,:), Pointer :: X
    Integer, Dimension(:,:,:,:), Pointer :: IBLANK
    Character(LEN=2) :: prec, gf, vf, iblocal
    Character(LEN=*) :: file
    Integer :: ngrid, I, J, K, L, Nx, Ny, Nz, NDIM, M, ftype
    Integer, Pointer :: ND(:,:)
    Integer :: OMap(3), NMap(3), ND_copy(3)
    Real(KIND=4), Dimension(:,:,:,:), Pointer :: fX
    Logical :: gf_exists
    Integer :: OUTPUT_FLAG

    Inquire(file=Trim(file),exist=gf_exists)
    If (.NOT.(gf_exists.eqv..true.)) Then
      Write (*,'(A,A,A)') 'File ', file(1:LEN_TRIM(file)), ' is not readable'
      Stop
    End If

    prec = ""; gf = ""; vf = ""; iblocal = ""
    Call p3d_detect(LEN(Trim(file)),Trim(file),prec,NDIM,gf,vf,iblocal,ftype)
    If (OUTPUT_FLAG .gt. 0) Call output_format(file,prec,NDIM,gf,vf,iblocal,ftype)
    If (PRESENT(ib) .eqv. .true.) ib = iblocal

    Open (unit=10, file=trim(file), form='unformatted', status='old')

    !  Read number of grids
    If (gf(1:1) .eq. 's') Then
      ngrid = 1
    Else
      Read (10) ngrid
      If (ngrid < 0 .OR. ngrid > 100) Then
          Write (*, '(A)') "WARNING :: PlasComCM uses big-endian file I/O ..."
          Write (*, '(A)') " Set environment variable F_UFMTENDIAN='big' for Intel compilers."
#ifdef USE_AMPI
          Write (*, '(A)') " Set environment variable GFORTRAN_CONVERT_UNIT='big_endian' for AMPI with GNU compilers."
#endif
      End If
    End If
    Allocate(ND(ngrid,3))

    If (OUTPUT_FLAG > 0) Then
      Write (*,'(A)') ' '
      Write (*,'(A,I2)') 'PlasComCM: Number of grids: ', ngrid
    End If

    !  Read grid sizes
    ND(:,3) = 1
    Read (10) ((ND(I,J),J=1,NDIM),I=1,ngrid)
    If (OUTPUT_FLAG > 0) Then
      Do I = 1, ngrid
        Write (*,'(A,I2,A,3(I5,1X))') 'PlasComCM: Grid ', I, ' is ', (ND(I,J),J=1,NDIM)
      End Do
    End IF

    close (10)

  End Subroutine ReadGridSize_P3D

  Subroutine ReadSolutionHeader_P3D(NDIM, ngrid, ND, fname, tau)

    Implicit None

    ! ... function call variables
    Integer :: NDIM, ngrid
    Integer, Dimension(:,:), Pointer :: ND
    Character(LEN=*) :: fname
    Real(KIND=8) :: tau(4)

    ! ... local variables
    Integer :: ibuf, I, J

    ! ... open the file
    Open (Unit=10, file=trim(fname), form='unformatted', status='old')

    ! ... read the number of grids, exit if error
    Read (10) ibuf

    ! ... read the size of each grid, throw value away
    Read (10) ((ibuf,J=1,NDIM),I=1,ngrid)

    ! ... read the header
    Read (10) tau

    ! ... close and exit
    Close (10)

  End Subroutine ReadSolutionHeader_P3D



  Subroutine ReadFuncHeader_P3D(NDIM, ngrid, fname, nvar)

    Implicit None

    Character(LEN=*) :: fname
    Integer :: ngrid, I, J, NDIM, nvar, ibuf
    Integer :: ND2(ngrid, NDIM+1)
    Integer, Parameter :: funit = 10

    Open (unit=funit, file=trim(fname), form='unformatted', status='old')

    !  read number of grids
    Read (funit) ibuf

    !  Read grid sizes & nvars
    Read (funit) ((ND2(I,J),J=1,NDIM+1),I=1,ngrid)

    !  Save nvar
    nvar = ND2(ngrid,NDIM+1) 

    Close (funit)

  End Subroutine ReadFuncHeader_P3D

  subroutine ReadLocalForwardSoln_P3D(region, ng, file_index, ptr_to_data)

    USE ModGlobal
    USE ModDataStruct
    USE ModMPI

    ! ... subroutine arguments
    Type (t_region), Pointer :: region
    Integer :: ng, file_index
    Real(rfreal), Dimension(:,:), Pointer :: ptr_to_data ! ... Adjoint N-S

    ! ... local variables and arrays
    Type(t_mixt_input), Pointer :: input
    Type(t_grid), Pointer :: grid
    Type(t_mixt), Pointer :: state
    Integer :: NDIM, ngrid, var(5), m, i, j, k, l0, N(MAX_ND), p
    Integer, Dimension(:,:), Pointer :: ND
    Character(len=2) :: prec, gf, vf
    Character(LEN=80) :: fname
    Real(rfreal) :: tau(4)
    Real(rfreal), Dimension(:,:,:,:), Pointer :: X ! ... reduction in rank, JKim 04/2008
    integer :: ierr

    ! ... simplicity
    grid  => region%grid(ng)
    state => region%state(ng)
    input => grid%input

    Write(fname(1:20),'(A10,I8.8,A2)') 'RocFlo-CM.', state%iter_cvFiles(file_index), '.q'

    IF(region%myrank == 0) THEN
       CALL ReadGridSize_P3D(NDIM, ngrid, ND, fname,0)
    ENDIF
    CALL MPI_Bcast(ngrid,1,MPI_INTEGER,0,mycomm,ierr)
    CALL MPI_Bcast(NDIM,1,MPI_INTEGER,0,mycomm,ierr)
    IF(region%myrank .NE. 0) THEN
       ALLOCATE(ND(ngrid,3))
    ENDIF
    CALL MPI_Bcast(ND,3*ngrid,MPI_INTEGER,0,mycomm,ierr)
!    call ReadGridSize_P3D(NDIM, ngrid, ND, fname, 0)
!    CALL ReadSolutionHeader_P3D(NDIM, ngrid, ND, fname, tau)
    IF(region%myrank == 0) THEN
       CALL ReadSolutionHeader_P3D(NDIM, ngrid, ND, fname, tau)
    ENDIF
    CALL MPI_BCast(tau,4,MPI_DOUBLE_PRECISION,0,mycomm,ierr)

    ! ... pointer
    Do m = 1, 5
      var(m) = m
    End Do

    If (NDIM == 3 .and. input%ND == 2 .and. ND(1,3) == 1) Then
      if (region%myrank == 0) print *, 'PlasComCM: changing var(4) = 5 in read_local_forward_soln().'
      var(4) = 5
    End If


    call ReadSolution_P3D(NDIM, ND, region, grid, fname, ptr_to_data, grid%iGridGlobal)

    deallocate(ND)

  end subroutine ReadLocalForwardSoln_P3D

  Subroutine output_format(file,prec,NDIM,gf,vf,ib,ftype)

    Character(LEN=80) :: file
    Character(LEN=2) :: prec, gf, vf, ib
    Integer :: NDIM, ftype

    Write (*,'(A)') ""
    Write (*,'(A)') 'File "'//file(1:LEN_TRIM(file))//'" is '

    If (ftype .eq. 0) Then
      Write (*,'(5X,A)') 'a grid file'
    Else
      Write (*,'(5X,A)') 'a solution file'
    End If

    Write (*,'(5X,I1,A)') NDIM, 'D'

    If (prec(1:1) .eq. 'd') Then
      Write (*,'(5X,A)') 'double precision'
    Else
      Write (*,'(5X,A)') 'single precision'
    End If

    If (gf(1:1) .eq. 's') Then
      Write (*,'(5X,A)') 'single grid'
    Else
      Write (*,'(5X,A)') 'multi-block'
    End If

    If (vf(1:1) .eq. 'w') Then
      Write (*,'(5X,A)') 'whole'
    Else
      Write (*,'(5X,A)') 'planes'
    End If

    If (ib(1:1) .eq. 'y') Then
      Write (*,'(5X,A)') 'with IBLANK'
    Else
      Write (*,'(5X,A)') 'without IBLANK'
    End If

    Return

  End Subroutine output_format

  Subroutine Write_Soln_Single_Grid(NDIM, ND, X, tau, fnamei, fnameleni, funit)

    Implicit None

    Real(KIND=8), Dimension(:,:,:,:), Pointer :: X
    Real(KIND=8) :: tau(4)
    Character(LEN=80) :: fnamei
    Integer :: I, J, K, L, M, NDIM, funit, fnameleni
    Integer, Dimension(:), Pointer :: ND

    ! ... open
    Open (unit=funit, file=fnamei(1:fnameleni), form='unformatted', status='unknown', position='append')

    ! ... Write soln values
    Write (funit) ((tau(i)),i=1,4)
    if (NDIM == 3) then
      Write (funit) ((((X(I,J,K,M),I=1,ND(1)),J=1,ND(2)),K=1,ND(3)),M=1,size(X,4))
    else if (NDIM == 2) Then
      Write (funit) ((((X(I,J,K,M),I=1,ND(1)),J=1,ND(2)),K=1,ND(3)),M=1,3), &
                    ((((0.0_8,I=1,ND(1)),J=1,ND(2)),K=1,ND(3)),M=1,1), &
                    ((((X(I,J,K,M),I=1,ND(1)),J=1,ND(2)),K=1,ND(3)),M=4,4)
    else if (NDIM == 1) Then
      Write (funit) ((((X(I,J,K,M),I=1,ND(1)),J=1,ND(2)),K=1,ND(3)),M=1,2), &
                    ((((0.0_8,I=1,ND(1)),J=1,ND(2)),K=1,ND(3)),M=1,2), &
                    ((((X(I,J,K,M),I=1,ND(1)),J=1,ND(2)),K=1,ND(3)),M=3,3)
    end if

    ! ... close
    Close (funit)

    Return

  End Subroutine Write_Soln_Single_Grid

  Subroutine Write_Func_Single_Grid(NDIM, ND, X, fnamei, fnameleni, funit)

    Implicit None

    Real(KIND=8), Dimension(:,:,:,:), Pointer :: X
    Character(LEN=80) :: fnamei
    Integer :: I, J, K, L, M, NDIM, funit, fnameleni
    Integer, Dimension(:), Pointer :: ND

    ! ... open
    Open (unit=funit, file=fnamei(1:fnameleni), form='unformatted', status='unknown', position='append')

    ! ... Write soln values
    Write (funit) ((((X(I,J,K,M),I=1,ND(1)),J=1,ND(2)),K=1,ND(3)),M=1,size(X,4))

    ! ... close
    Close (funit)

    Return

  End Subroutine Write_Func_Single_Grid

  Subroutine Write_Grid_Single_Grid(NDIM, ND, X, IB, fnamei, fnameleni, funit)

    Implicit None

    Real(KIND=8), Dimension(:,:,:,:), Pointer :: X
    Integer, Pointer :: IB(:,:,:)
    Real(KIND=8) :: tau(4)
    Character(LEN=80) :: fnamei
    Integer :: I, J, K, L, M, NDIM, funit, fnameleni
    Integer, Dimension(:), Pointer :: ND

    ! ... open
    Open (unit=funit, file=fnamei(1:fnameleni), form='unformatted', status='unknown', position='append')

    ! ... Write grid values
    if (NDIM == 3) then
      Write (funit) ((((X(I,J,K,M),I=1,ND(1)),J=1,ND(2)),K=1,ND(3)),M=1,size(X,4)), &
                    (((IB(I,J,K),I=1,ND(1)),J=1,ND(2)),K=1,ND(3))
    else if (NDIM == 2) then
      Write (funit) ((((X(I,J,K,M),I=1,ND(1)),J=1,ND(2)),K=1,ND(3)),M=1,2), &
                    ((((0.0_8,I=1,ND(1)),J=1,ND(2)),K=1,ND(3)),M=1,1), &
                    (((IB(I,J,K),I=1,ND(1)),J=1,ND(2)),K=1,ND(3))
    else if (NDIM == 1) then
      Write (funit) ((((X(I,J,K,M),I=1,ND(1)),J=1,ND(2)),K=1,ND(3)),M=1,1), &
                    ((((0.0_8,I=1,ND(1)),J=1,ND(2)),K=1,ND(3)),M=1,2), &
                    (((IB(I,J,K),I=1,ND(1)),J=1,ND(2)),K=1,ND(3))
    end if

    ! ... close
    Close (funit)

    Return

  End Subroutine Write_Grid_Single_Grid


  Subroutine Write_Grid_Soln_Header(NDIM, ngrid, ND, file, fnamelen, funit)

    Implicit None

    Character(LEN=80) :: file
    Integer :: ngrid, I, J, NDIM, funit, fnamelen
    Integer, Dimension(:,:), Pointer :: ND

    Open (unit=funit, file=file(1:fnamelen), form='unformatted', status='unknown')

    !  Write number of grids
    Write (funit) ngrid

    !  Write grid sizes
    Write (funit) ((ND(I,J),J=1,NDIM),I=1,ngrid)

    Close (funit)

  End Subroutine Write_Grid_Soln_Header

  Subroutine Write_Grid_Header_And_Pad(NDIM, ngrid, ND, file, fnamelen, funit)

    Implicit None

    Character(LEN=80) :: file
    Integer :: ngrid, I, J, NDIM, funit, fnamelen
    Integer, Dimension(:,:), Pointer :: ND

    Open (unit=funit, file=file(1:fnamelen), form='unformatted', status='unknown')

    !  Write number of grids
    Write (funit) ngrid

    !  Write grid sizes
    Write (funit) ((ND(I,J),J=1,NDIM),I=1,ngrid)

    ! ... write a bunch of zeros
    Do I = 1, ngrid
      Write (funit) (0.0_8, J=1,PRODUCT(ND(I,:))*3), (0, J=1,PRODUCT(ND(I,:)))
    End Do

    Close (funit)

  End Subroutine Write_Grid_Header_And_Pad


  Subroutine WriteGridFileHeader(NDIM, ngrid, ND, file, fnamelen, funit)

    Implicit None

    Character(LEN=80) :: file
    Integer :: ngrid, I, J, NDIM, funit, fnamelen
    Integer, Dimension(:,:), Pointer :: ND

    Open (unit=funit, file=file(1:fnamelen), form='unformatted', status='unknown')

    !  Write number of grids
    Write (funit) ngrid

    !  Write grid sizes
    Write (funit) ((ND(I,J),J=1,NDIM),I=1,ngrid)

    Close (funit)

  End Subroutine WriteGridFileHeader

  Subroutine Write_Soln_Header_And_Pad(NDIM, ngrid, ND, file, fnamelen, funit)

    Implicit None

    Character(LEN=80) :: file
    Integer :: ngrid, I, J, NDIM, funit, fnamelen
    Integer, Dimension(:,:), Pointer :: ND

    Open (unit=funit, file=file(1:fnamelen), form='unformatted', status='unknown')

    !  Write number of grids
    Write (funit) ngrid

    !  Write grid sizes
    Write (funit) ((ND(I,J),J=1,NDIM),I=1,ngrid)

    ! ... write a bunch of zeros
    Do I = 1, ngrid
      Write (funit) (0.0_8, J=1,4)
      Write (funit) (0.0_8, J=1,PRODUCT(ND(I,:))*5)
    End Do

    Close (funit)

  End Subroutine Write_Soln_Header_And_Pad


  Subroutine WriteSolutionFileHeader(NDIM, ngrid, ND, file, fnamelen, funit)

    Implicit None

    Character(LEN=80) :: file
    Integer :: ngrid, I, J, NDIM, funit, fnamelen
    Integer, Dimension(:,:), Pointer :: ND

    Open (unit=funit, file=file(1:fnamelen), form='unformatted', status='unknown')

    !  Write number of grids
    Write (funit) ngrid

    !  Write grid sizes
    Write (funit) ((ND(I,J),J=1,NDIM),I=1,ngrid)

    Close (funit)

  End Subroutine WriteSolutionFileHeader

  INTEGER FUNCTION SolutionFileSize(numDim,numGrids,gridSizes)

    IMPLICIT NONE

    INTEGER                          :: numDim,numGrids
    INTEGER, DIMENSION(:,:), POINTER :: gridSizes

    INTEGER :: numBlocks, fileSize, intSize, doubleSize, iGrid, numPoints

    intSize = 4
    doubleSize = 8

    numBlocks = 2*numGrids + 2
    fileSize = numBlocks*intSize*2
    
    DO iGrid = 1,numGrids
       numPoints = PRODUCT(gridSizes(iGrid,:))
       fileSize = fileSize + (numPoints*5 + 4)*doubleSize
    END DO

    SolutionFileSize = fileSize

  END FUNCTION SolutionFileSize

  Subroutine Write_Func_Header(NDIM, ngrid, ND, nvar, file, fnamelen, funit)

    Implicit None

    Character(LEN=80) :: file
    Integer :: ngrid, I, J, NDIM, funit, nvar, fnamelen
    Integer, Dimension(:,:), Pointer :: ND
    Integer :: ND2(ngrid, NDIM+1)

    ND2(:,1:NDIM) = ND(:,:)
    ND2(:,NDIM+1) = nvar

    Open (unit=funit, file=file(1:fnamelen), form='unformatted', status='unknown')

    !  Write number of grids
    Write (funit) ngrid

    !  Write grid sizes
    Write (funit) ((ND2(I,J),J=1,NDIM+1),I=1,ngrid)

    Close (funit)

  End Subroutine Write_Func_Header

  Subroutine Write_Func_Header_And_Pad(NDIM, ngrid, ND, nvar, file, fnamelen, funit)

    Implicit None

    Character(LEN=80) :: file
    Integer :: ngrid, I, J, NDIM, funit, nvar, fnamelen
    Integer, Dimension(:,:), Pointer :: ND
    Integer :: ND2(ngrid, NDIM+1)

    ND2(:,1:NDIM) = ND(:,:)
    ND2(:,NDIM+1) = nvar

    Open (unit=funit, file=file(1:fnamelen), form='unformatted', status='unknown')

    !  Write number of grids
    Write (funit) ngrid

    !  Write grid sizes
    Write (funit) ((ND2(I,J),J=1,NDIM+1),I=1,ngrid)

    ! ... now write a bunch of zeros
    Do I = 1, ngrid
      Write (funit) (0.0_8,J=1,PRODUCT(ND2(I,:)))
    End Do

    Close (funit)

  End Subroutine Write_Func_Header_And_Pad



  SUBROUTINE WriteFunctionFileHeader(NDIM, ngrid, ND, nvar, file, fnamelen, funit)

    Implicit None

    Character(LEN=80) :: file
    Integer :: ngrid, I, J, NDIM, funit, nvar, fnamelen
    Integer, Dimension(:,:), Pointer :: ND
    Integer :: ND2(ngrid, NDIM+1)

    ND2(:,1:NDIM) = ND(:,:)
    ND2(:,NDIM+1) = nvar

    Open (unit=funit, file=file(1:fnamelen), form='unformatted', status='unknown')

    !  Write number of grids
    Write (funit) ngrid

    !  Write grid sizes
    Write (funit) ((ND2(I,J),J=1,NDIM+1),I=1,ngrid)

    Close (funit)

  END SUBROUTINE WriteFunctionFileHeader

  subroutine ReadGrid_P3D(NDIM, region, grid_fname, grid, gridID, ib)

    USE ModGlobal
    USE ModDataStruct
    USE ModMPI

    Implicit None

    type(t_region), pointer :: region
    type(t_grid), pointer :: grid
    Character(LEN=80) :: grid_fname
    integer :: gridID ! ... specify which grid we want to read, JKim 04/2008
    integer :: NDIM   ! ... dimension in grid file
    Character(LEN=2) :: ib

    ! ... local variables
    Real(rfreal), Dimension(:,:,:,:), Pointer :: X
    Integer, Dimension(:,:,:), Pointer :: IBLANK
    Integer :: ngrid, I, J, K, L0, Nx, Ny, Nz, M, ftype, dir, N(MAX_ND)
    Integer :: fh, filetype
    Integer :: offset
    Integer :: ibuf(1), is(MAX_ND), ie(MAX_ND)

    ! ... seek to the current array from the beginning of the file
    call GridFileOffset_P3D(gridID, NDIM, region%nGridsGlobal, grid%AllGridsGlobalSize, ib, offset)

    ! ... extent of this grid (in C-counting)
    is = 0; ie = 0; N = 1;
    do j = 1, grid%ND
       is(j) = grid%is_unique(j)-1
       ie(j) = grid%ie_unique(j)-1
       N(j) = ie(j) - is(j) + 1
    end do

    ! ... allocate buffer
    allocate(X(N(1),N(2),N(3),NDIM))

    ! ... read the file in binary mode
    if (ib(1:1) .eq. 'y') then
       Allocate(IBLANK(N(1),N(2),N(3)))
       call read_plot3d_single_grid_ib_mpiio(grid%comm,NDIM, grid%GlobalSize, offset, is, ie, &
            gridID, X, IBLANK, grid_fname, len_trim(grid_fname))
    else
       call read_plot3d_single_grid_mpiio(grid%comm,NDIM, grid%GlobalSize, offset, is, ie, &
            gridID, X, grid_fname, len_trim(grid_fname))
       grid%IBLANK(:) = 1
    end if

    ! ... unpack buffer
    do k = grid%is_unique(3), grid%ie_unique(3)
       do j = grid%is_unique(2), grid%ie_unique(2)
          do i = grid%is_unique(1), grid%ie_unique(1)
             do m = 1, grid%ND
                l0 = (k-grid%is_unique(3)+grid%nGhostRHS(3,1))*(grid%ie(1)-grid%is(1)+1) &
                     *(grid%ie(2)-grid%is(2)+1) &
                     + (j-grid%is_unique(2)+grid%nGhostRHS(2,1))*(grid%ie(1)-grid%is(1)+1) + grid%nGhostRHS(1,1)+i-grid%is_unique(1)+1
                grid%XYZ(l0,m) = X(i-grid%is_unique(1)+1,j-grid%is_unique(2)+1,k-grid%is_unique(3)+1,m)
             end do
          end do
       end do
    end do

    if (ib(1:1) .eq. 'y') then
       ! ... unpack buffer
       do k = grid%is_unique(3), grid%ie_unique(3)
          do j = grid%is_unique(2), grid%ie_unique(2)
             do i = grid%is_unique(1), grid%ie_unique(1)
                l0 = (k-grid%is_unique(3)+grid%nGhostRHS(3,1))*(grid%ie(1)-grid%is(1)+1) &
                     *(grid%ie(2)-grid%is(2)+1) &
                     + (j-grid%is_unique(2)+grid%nGhostRHS(2,1))*(grid%ie(1)-grid%is(1)+1) + grid%nGhostRHS(1,1)+i-grid%is_unique(1)+1
                grid%IBLANK(l0) = IBLANK(i-grid%is_unique(1)+1,j-grid%is_unique(2)+1,k-grid%is_unique(3)+1)
             end do
          end do
       end do
       deallocate(IBLANK)
    end if

    deallocate(X)

  end subroutine ReadGrid_P3D

  subroutine ReadFunc_P3D(NDIM, ND, region, grid, grid_fname, dbuf, gridID, nvarIN)

    USE ModGlobal
    USE ModDataStruct
    USE ModMPI

    Implicit None

    Type(t_region), pointer :: region
    Type(t_grid), Pointer :: grid
    Real(rfreal), Dimension(:,:), Pointer :: dbuf
    Character(LEN=80) :: grid_fname
    integer :: gridID ! ... specify which grid we want to read, JKim 04/2008
    integer :: NDIM   ! ... dimension in grid file
    integer, Dimension(:,:), Pointer :: ND
    integer, optional, intent(IN) :: nvarIN

    ! ... local variables
    Real(rfreal), Dimension(:,:,:,:), Pointer :: X
    Character(LEN=2) :: prec, gf, vf, ib
    Integer :: ngrid, I, J, K, L0, Nx, Ny, Nz, M, ftype, dir, N(MAX_ND)
    Integer :: fh, filetype, p, nvars
    Integer :: offset, var(MAX_ND+2)
    Integer :: ibuf(1), is(MAX_ND), ie(MAX_ND)
    Integer, Parameter :: sizeof_int = 4, sizeof_dbl = 8

    ! ... number of variables
    If (present(nvarIN)) Then
       If (nvarIN > 0 .and. nvarIN<=region%input%nAuxVars) Then
          nvars = nvarIN
       Else
          nvars = region%input%nAuxVars
       End If
    Else
       nvars = region%input%nAuxVars
    End If
  
    ! ... seek to the current array from the beginning of the file
    call FuncFileOffset_P3D(gridID, NDIM, region%nGridsGlobal, grid%AllGridsGlobalSize, nvars, offset)
    
    ! ... extent of this grid (in C-counting)
    is = 0; ie = 0; N = 1;
    do j = 1, grid%ND
       is(j) = grid%is_unique(j)-1
       ie(j) = grid%ie_unique(j)-1
       N(j) = ie(j) - is(j) + 1
    end do

    ! ... allocate buffer
    allocate(X(N(1),N(2),N(3),nvars))

    ! ... read the file in binary mode
!    call read_plot3d_single_func_whole_format_low_mem(NDIM, grid%GlobalSize, offset, is, ie, gridID, nvars, X, grid_fname, len_trim(grid_fname))
    call read_plot3d_single_func_mpiio(grid%comm,NDIM, grid%GlobalSize, offset, is, ie, gridID, nvars, X, grid_fname, len_trim(grid_fname))

    ! ... unpack buffer
    do k = grid%is_unique(3), grid%ie_unique(3)
       do j = grid%is_unique(2), grid%ie_unique(2)
          do i = grid%is_unique(1), grid%ie_unique(1)
             do m = 1, ubound(dbuf,2)
                l0 = (k-grid%is_unique(3)+grid%nGhostRHS(3,1))*(grid%ie(1)-grid%is(1)+1) &
                     *(grid%ie(2)-grid%is(2)+1) &
                     + (j-grid%is_unique(2)+grid%nGhostRHS(2,1))*(grid%ie(1)-grid%is(1)+1) + grid%nGhostRHS(1,1)+i-grid%is_unique(1)+1
                if(m<=nvars) then
                  dbuf(l0,m) = X(i-grid%is_unique(1)+1,j-grid%is_unique(2)+1,k-grid%is_unique(3)+1,m)
                else
                  dbuf(l0,m) = 1d-12
                endif
                if(.not. abs( dbuf(l0,m)) < 1d10) print'(2i5,"Suspicious Value In read_single_func_low_mem",1pe12.4)',l0,m,dbuf(l0,m)
             end do
          end do
       end do
    end do

    deallocate(X)

  end subroutine ReadFunc_P3D

!!$
  subroutine ReadSolution_P3D(NDIM, ND, region, grid, grid_fname, dbuf, gridID)

    USE ModGlobal
    USE ModDataStruct
    USE ModMPI

    Implicit None

    Type(t_region), pointer :: region
    Type(t_grid), Pointer :: grid
    Real(rfreal), Dimension(:,:), Pointer :: dbuf
    Character(LEN=80) :: grid_fname
    integer :: gridID ! ... specify which grid we want to read, JKim 04/2008
    integer :: NDIM   ! ... dimension in grid file
    integer, Dimension(:,:), Pointer :: ND

    ! ... local variables
    Real(rfreal), Dimension(:,:,:,:), Pointer :: X
    Character(LEN=2) :: prec, gf, vf, ib
    Integer :: ngrid, I, J, K, L0, Nx, Ny, Nz, M, ftype, dir, N(MAX_ND)
    Integer :: fh, filetype, p
    Integer :: offset, var(MAX_ND+2)
    Integer :: ibuf(1), is(MAX_ND), ie(MAX_ND)
    Integer, Parameter :: sizeof_int = 4, sizeof_dbl = 8

    ! ... pointer
    Do m = 1, 5
      var(m) = m
    End Do

    If (NDIM == 3 .and. region%input%ND == 2 .and. ND(1,3) == 1) Then
      if (region%myrank == 0) print *, 'PlasComCM: Adjusting for 2D in ReadSolution_P3D.'
      var(4) = 5
    End If

    If (NDIM == 3 .and. region%input%ND == 1 .and. SUM(ND(1,2:3)) == 2) Then
      if (region%myrank == 0) print *, 'PlasComCM: Adjusting for 1D in ReadSolution_P3D.'
      var(3) = 5
    End If

    ! ... seek to the current array from the beginning of the file
    call SolutionFileOffset_P3D(gridID, NDIM, region%nGridsGlobal, grid%AllGridsGlobalSize, offset)
    
    ! ... skip another 2 x sizeof(int) + 4 x sizeof(double)
    offset = offset + 2 * sizeof_int + 4 * sizeof_dbl

    ! ... extent of this grid (in C-counting)
    is = 0; ie = 0; N = 1;
    do j = 1, grid%ND
       is(j) = grid%is_unique(j)-1
       ie(j) = grid%ie_unique(j)-1
       N(j) = ie(j) - is(j) + 1
    end do

    ! ... allocate buffer
    allocate(X(N(1),N(2),N(3),NDIM+2))

    ! ... read the file in binary mode
!    call read_plot3d_single_soln_whole_format_low_mem(NDIM, grid%GlobalSize, offset, is, ie, gridID, X, grid_fname, len_trim(grid_fname))
    call read_plot3d_single_soln_mpiio(grid%comm,NDIM, grid%GlobalSize, offset, is, ie, &
         gridID, X, grid_fname, len_trim(grid_fname))

    ! ... unpack buffer
    do k = grid%is_unique(3), grid%ie_unique(3)
       do j = grid%is_unique(2), grid%ie_unique(2)
          do i = grid%is_unique(1), grid%ie_unique(1)
             do m = 1, grid%ND+2
                l0 = (k-grid%is_unique(3)+grid%nGhostRHS(3,1))*(grid%ie(1)-grid%is(1)+1) &
                     *(grid%ie(2)-grid%is(2)+1) &
                     + (j-grid%is_unique(2)+grid%nGhostRHS(2,1))*(grid%ie(1)-grid%is(1)+1) + grid%nGhostRHS(1,1)+i-grid%is_unique(1)+1
                dbuf(l0,m) = X(i-grid%is_unique(1)+1,j-grid%is_unique(2)+1,k-grid%is_unique(3)+1,var(m))
             end do
          end do
       end do
    end do

    deallocate(X)

  end subroutine ReadSolution_P3D
!!$
  Subroutine GetSpongeInfo_P3D(NDIM, region, grid_fname, gridID, ib)

    USE ModGlobal
    USE ModDataStruct
    USE ModMPI

    Implicit None

    ! ... input data
    TYPE(t_region), pointer :: region
    Integer :: gridID, NDIM
    Character(LEN=2) :: ib
    Character(LEN=80) :: grid_fname

    ! ... local data
    TYPE(t_grid), pointer :: grid
    REAL(rfreal), pointer :: X(:,:,:,:)
    Integer, Dimension(:,:,:), Pointer :: IBLANK
    TYPE(t_mixt_input), pointer :: input
    TYPE(t_patch), pointer :: patch
    Integer :: npatch, N(MAX_ND), Np(MAX_ND), Nc, sgn, normDir, i, j, k, l1, l2, ii, jj, kk, lp
    Integer :: is(MAX_ND), ie(MAX_ND), offset, Ns(MAX_ND)

    ! ... loop through all patches
    do npatch = 1, region%nPatches

       patch => region%patch(npatch)
       grid => region%grid(patch%gridID)

       ! ... only save information if this grid corresponds to ng_in_file
       if ( grid%iGridGlobal == gridID ) then

          ! ... check to see if this is a sponge boundary
          if ( patch%bcType == SPONGE .or. patch%bcType == SPONGE_WITH_LINEARIZED_DISTURBANCE ) then

             N(:) = 1; Np(:) = 1
             do j = 1, grid%ND
                N(j)  =  grid%ie(j) -  grid%is(j) + 1
                Np(j) = patch%ie(j) - patch%is(j) + 1
             end do

             normDir = abs(patch%normDir)
             sgn = normDir / patch%normDir

             ! ... find seek offset to current grid in file
             call GridFileOffset_P3D(gridID, NDIM, region%nGridsGlobal, grid%AllGridsGlobalSize, ib, offset)

             ! ... extent of this ENTIRE sponge zone (in C-counting)
             is(:) = 0; ie(:) = 0; Ns(:) = 1; 
             do j = 1, grid%ND
                is(j) = patch%bcData(4+2*(j-1)  )-1
                ie(j) = patch%bcData(4+2*(j-1)+1)-1
                Ns(j) = ie(j) - is(j) + 1
             end do

             ! ... allocate buffer for this patch
             allocate(X(Ns(1),Ns(2),Ns(3),NDIM))

             ! ... read the file in binary mode
             call read_plot3d_single_grid_mpiio(patch%comm,NDIM, grid%GlobalSize, offset, is, ie, &
                  gridID, X, grid_fname, len_trim(grid_fname))

             ! ... allocate memory for sponge boundary coordinates
             allocate(patch%sponge_xs(patch%prodN,grid%ND))
             allocate(patch%sponge_xe(patch%prodN,grid%ND))

             Do k = patch%is(3), patch%ie(3)
               Do j = patch%is(2), patch%ie(2)
                 Do i = patch%is(1), patch%ie(1)

                   lp = (k-patch%is(3))*Np(1)*Np(2) + (j-patch%is(2))*Np(1) + (i-patch%is(1)+1)

                   Select Case (patch%normDir)

                   Case (+1)
                     jj = j - is(2)
                     kk = k - is(3)
                     patch%sponge_xs(lp,1:grid%ND) = X(Ns(1),jj,kk,1:grid%ND)
                     patch%sponge_xe(lp,1:grid%ND) = X(    1,jj,kk,1:grid%ND)
                   Case (-1)
                     jj = j - is(2)
                     kk = k - is(3)
                     patch%sponge_xs(lp,1:grid%ND) = X(    1,jj,kk,1:grid%ND)
                     patch%sponge_xe(lp,1:grid%ND) = X(Ns(1),jj,kk,1:grid%ND)
                   Case (+2)
                     ii = i - is(1)
                     kk = k - is(3)
                     patch%sponge_xs(lp,1:grid%ND) = X(ii,Ns(2),kk,1:grid%ND)
                     patch%sponge_xe(lp,1:grid%ND) = X(ii,    1,kk,1:grid%ND)
                   Case (-2)
                     ii = i - is(1)
                     kk = k - is(3)
                     patch%sponge_xs(lp,1:grid%ND) = X(ii,    1,kk,1:grid%ND)
                     patch%sponge_xe(lp,1:grid%ND) = X(ii,Ns(2),kk,1:grid%ND)
                   Case (+3)
                     ii = i - is(1)
                     jj = j - is(2)
                     patch%sponge_xs(lp,1:grid%ND) = X(ii,jj,Ns(3),1:grid%ND)
                     patch%sponge_xe(lp,1:grid%ND) = X(ii,jj,    1,1:grid%ND)
                   Case (-3)
                     ii = i - is(1)
                     jj = j - is(2)
                     patch%sponge_xs(lp,1:grid%ND) = X(ii,jj,    1,1:grid%ND)
                     patch%sponge_xe(lp,1:grid%ND) = X(ii,jj,Ns(3),1:grid%ND)                      
                   End Select

                   if (abs(sqrt(dot_product( patch%sponge_xe(lp,1:grid%ND) - patch%sponge_xs(lp,1:grid%ND), patch%sponge_xe(lp,1:grid%ND) - patch%sponge_xs(lp,1:grid%ND)))) <= 1e-10) then
                      print *, 'PlasComCM: sponge length too small ', i, j, k
                   end if
                 End Do
               End Do
             End do

             deallocate(X)

          end if

       end if

    end do

  End Subroutine GetSpongeInfo_P3D
!!$
  SUBROUTINE GridFileOffset_P3D(gridID, NDIM, nGridsGlobal, GlobalSize, ib, offset)

    USE ModGlobal

    IMPLICIT NONE

    ! ... function call data
    Integer :: gridID, NDIM, nGridsGlobal, offset
    Integer, Pointer, Dimension(:,:) :: GlobalSize
    Character(LEN=2) :: ib

    ! ... local data
    Integer, Parameter :: sizeof_int = 4, sizeof_dbl = 8
    Integer :: i

    ! ... seek to the current array from the beginning of the file
    ! ... ngrid + 2 records
    offset = sizeof_int + 2*sizeof_int

    ! ... grid dimensions plus 2 records
    offset = offset + NDIM*nGridsGlobal*sizeof_int + 2*sizeof_int

    ! ... number of grids previous plus their records
    if (ib(1:1) .eq. 'y') then
       do i = 1, gridID-1
          offset = offset + PRODUCT(GlobalSize(i,:))*(NDIM*sizeof_dbl+sizeof_int) + 2*sizeof_int
       end do
    else
       do i = 1, gridID-1
          offset = offset + PRODUCT(GlobalSize(i,:))*NDIM*sizeof_dbl + 2*sizeof_int
       end do
    end if

    ! ... final offset for the first record
    offset = offset + sizeof_int

  End Subroutine GridFileOffset_P3D

  Subroutine SolutionFileOffset_P3D(gridID, NDIM, nGridsGlobal, GlobalSize, offset)

    USE ModGlobal

    Implicit None

    ! ... function call data
    Integer :: gridID, NDIM, nGridsGlobal, offset
    Integer, Pointer, Dimension(:,:) :: GlobalSize
    Character(LEN=2) :: ib

    ! ... local data
    Integer, Parameter :: sizeof_int = 4, sizeof_dbl = 8
    Integer :: i

    ! ... seek to the current array from the beginning of the file
    ! ... ngrid + 2 records
    offset = sizeof_int + 2*sizeof_int

    ! ... grid dimensions plus 2 records
    offset = offset + NDIM*nGridsGlobal*sizeof_int + 2*sizeof_int

    ! ... number of grids previous plus their records
    do i = 1, gridID-1
      offset = offset + 4*sizeof_dbl + 2*sizeof_int
      offset = offset + PRODUCT(GlobalSize(i,:))*(NDIM+2)*sizeof_dbl + 2*sizeof_int
    end do

    ! ... final offset for the first record
    offset = offset + sizeof_int

  End Subroutine SolutionFileOffset_P3D
!!$
  Subroutine FuncFileOffset_P3D(gridID, NDIM, nGridsGlobal, GlobalSize, nVar, offset)

    USE ModGlobal

    Implicit None

    ! ... function call data
    Integer :: gridID, NDIM, nGridsGlobal, offset, nVar
    Integer, Pointer, Dimension(:,:) :: GlobalSize
    Character(LEN=2) :: ib

    ! ... local data
    Integer, Parameter :: sizeof_int = 4, sizeof_dbl = 8
    Integer :: i

    ! ... seek to the current array from the beginning of the file
    ! ... ngrid + 2 records
    offset = sizeof_int + 2*sizeof_int

    ! ... grid dimensions plus nVar plus 2 records
    offset = offset + (NDIM+1)*nGridsGlobal*sizeof_int + 2*sizeof_int

    ! ... number of grids previous plus their records
    do i = 1, gridID-1
      offset = offset + PRODUCT(GlobalSize(i,:))*nvar*sizeof_dbl + 2*sizeof_int
    end do

    ! ... final offset for the first record
    offset = offset + sizeof_int

  End Subroutine FuncFileOffset_P3D


  SUBROUTINE ReadSingleGrid_P3D(region, ng_in_file)

    USE ModGlobal
    USE ModDataStruct
    USE ModMPI
    USE ModDeriv

    Implicit None

    Type (t_region), pointer :: region
    Integer :: ng_in_file

    ! ... local variables
    Real(rfreal), Dimension(:,:,:,:,:), Pointer :: X
    Integer, Dimension(:,:,:,:), Pointer :: IBLANK
    Character(LEN=2) :: prec, gf, vf, ib
    Integer :: ngrid, I, J, K, L0, Nx, Ny, Nz, NDIM, M, ftype, ng, p, dir
    Integer, Dimension(:,:), Pointer :: ND
    Type (t_mixt_input), pointer :: input
    Type (t_grid), pointer :: grid
    Integer :: N(MAX_ND), vds(MAX_ND), ierr, output_flag
    real(rfreal), pointer :: ptr_to_data(:)
    real(rfreal), pointer :: iblank_dble(:)

    output_flag = 0
!    if(region%myrank == 0) output_flag = 1
    
    
    Do ng = 1, region%nGrids
       
       grid  => region%grid(ng)
       input => grid%input
       
       if (grid%iGridGlobal == ng_in_file) then
                
          if ( grid%myrank_inComm == 0) then
             call ReadGridSize_P3D(NDIM, ngrid, ND, input%grid_fname,output_flag, ib)
          endif
          CALL MPI_BCast(NDIM,1,MPI_INTEGER,0,grid%comm,ierr)
          CALL MPI_BCast(ngrid,1,MPI_INTEGER,0,grid%comm,ierr)
          if( grid%myrank_inComm .ne. 0) then
             ALLOCATE(ND(ngrid,3))
          endif
          CALL MPI_BCast(ND,3*ngrid,MPI_INTEGER,0,grid%comm,ierr)
          CALL MPI_BCast(ib,2,MPI_CHARACTER,0,grid%comm,ierr)
          
          ! ... grid%i{s,e} contain beginning & ending indices
          region%mpi_timings(ng)%io_read_grid = MPI_Wtime()
          call ReadGrid_P3D(NDIM, region, input%grid_fname, grid, ng_in_file, ib)
          region%mpi_timings(ng)%io_read_grid = MPI_Wtime() - region%mpi_timings(ng)%io_read_grid
          
          ! ... now get sponge information
          region%mpi_timings(ng)%io_read_sponge = MPI_Wtime()
          call GetSpongeInfo_P3D(NDIM, region, input%grid_fname, ng_in_file, ib)
          region%mpi_timings(ng)%io_read_sponge = MPI_Wtime() - region%mpi_timings(ng)%io_read_sponge
          
          deallocate(ND)
          
       end if

    End Do
       
    
  End Subroutine ReadSingleGrid_P3D

  SUBROUTINE ReadLocalGrid_P3D(region, localGridID)

    USE ModGlobal
    USE ModDataStruct
    USE ModMPI
    USE ModDeriv

    Implicit None

    Type (t_region), pointer :: region
    Integer :: localGridID

    ! ... local variables
    Real(rfreal), Dimension(:,:,:,:,:), Pointer :: X
    Integer, Dimension(:,:,:,:), Pointer :: IBLANK
    Character(LEN=2) :: prec, gf, vf, ib
    Integer :: ngrid, I, J, K, L0, Nx, Ny, Nz, NDIM, M, ftype, ng, p, dir
    Integer, Dimension(:,:), Pointer :: ND
    Type (t_mixt_input), pointer :: input
    Type (t_grid), pointer :: grid
    Integer :: N(MAX_ND), vds(MAX_ND), ierr, output_flag,globalGridID
    real(rfreal), pointer :: ptr_to_data(:)
    real(rfreal), pointer :: iblank_dble(:)

    output_flag = 0
    !    if(region%myrank == 0) output_flag = 1
    
    grid  => region%grid(localGridID)
    input => grid%input
    globalGridID = grid%iGridGlobal

    if ( grid%myrank_inComm == 0) then
       call ReadGridSize_P3D(NDIM, ngrid, ND, input%grid_fname,output_flag, ib)
    endif

    CALL MPI_BCast(NDIM,1,MPI_INTEGER,0,grid%comm,ierr)
    CALL MPI_BCast(ngrid,1,MPI_INTEGER,0,grid%comm,ierr)
    if( grid%myrank_inComm .ne. 0) then
       ALLOCATE(ND(ngrid,3))
    endif
    CALL MPI_BCast(ND,3*ngrid,MPI_INTEGER,0,grid%comm,ierr)
    CALL MPI_BCast(ib,2,MPI_CHARACTER,0,grid%comm,ierr)
          
    ! ... grid%i{s,e} contain beginning & ending indices
    region%mpi_timings(localGridID)%io_read_grid = MPI_Wtime()
    call ReadGrid_P3D(NDIM, region, input%grid_fname, grid, globalGridID, ib)
    region%mpi_timings(localGridID)%io_read_grid = MPI_Wtime() - region%mpi_timings(localGridID)%io_read_grid
          
    ! ... now get sponge information
    region%mpi_timings(localGridID)%io_read_sponge = MPI_Wtime()
    call GetSpongeInfo_P3D(NDIM, region, input%grid_fname, globalGridID, ib)
    region%mpi_timings(localGridID)%io_read_sponge = MPI_Wtime() - region%mpi_timings(localGridID)%io_read_sponge
          
    deallocate(ND)
          
  End Subroutine ReadLocalGrid_P3D

SUBROUTINE WriteP3DFile(region,vars_to_write,restartIn)

  USE ModGlobal
  USE ModDataStruct
  USE ModMPI

  IMPLICIT NONE
  
  TYPE (t_region), POINTER :: region
  CHARACTER(LEN=3), POINTER, DIMENSION(:) :: vars_to_write
  INTEGER, OPTIONAL :: restartIn

  type(t_grid), pointer :: grid
  type(t_mixt), pointer :: state
  real(rfreal) :: tau(4)
  integer :: ng, numGridsInFile, NDIM
  INTEGER :: nVars, i,ii, iter
  LOGICAL :: hasWritten = .false.
  CHARACTER(LEN=3) :: var_to_write
  CHARACTER(LEN=80) :: fname
  INTEGER :: funit = 50, fnamelen, ierr
  INTEGER :: isRestart = FALSE

  iter = region%global%main_ts_loop_index
  
  IF(PRESENT(restartIn)) isRestart = restartIn
  IF(isRestart == TRUE) THEN
     DEALLOCATE(vars_to_write)
     ALLOCATE(vars_to_write(1))
     vars_to_write = 'rst'
  ENDIF
  
  nVars = UBOUND(vars_to_write,1)
  
  DO ii = 1,nVars

     var_to_write = vars_to_write(ii)
     
     IF (region%input%useMPIIO == TRUE) THEN
        CALL write_plot3d_file_mpiio(region, var_to_write, iter)
        hasWritten = .TRUE.
     ELSE IF (region%input%useLowMemIO == TRUE) THEN
        CALL write_plot3d_file_low_mem(region, var_to_write, iter)
        hasWritten = .TRUE.
     ELSE IF (region%input%useLowMemIO == PROCESSOR_IO) THEN
        CALL write_plot3d_file_low_mem_per_processor(region, var_to_write, iter)
        hasWritten = .TRUE.
     ELSE
        ! ... step 1
        ! ... set the filename
        WRITE (fname(1:18),'(A10,I8.8)') 'RocFlo-CM.', iter
        IF ( var_to_write(1:3) == 'cv ' ) THEN
           WRITE(fname(19:20),'(A2)') '.q'
           fnamelen = 20
        ELSE IF ( var_to_write(1:3) == 'rst' ) THEN
           IF (region%input%AdjNS) THEN
              WRITE(fname(19:29),'(A11)') '.av.restart'
              fnamelen = 29
           ELSE
              WRITE(fname(19:26),'(A8)') '.restart'
              fnamelen = 26
           END IF ! region%input%AdjNS
        ELSE IF ( var_to_write(1:3) == 'cvt' ) THEN
           WRITE(fname(19:27),'(A9)') '.target.q'
           fnamelen = 27
        ELSE IF ( var_to_write(1:3) == 'xyz' ) THEN
           WRITE(fname(19:22),'(A4)') '.xyz'
           fnamelen = 22
        ELSE IF ( var_to_write(1:3) == 'jac' ) THEN
           WRITE(fname(19:24),'(A6)') '.jac.q'
           fnamelen = 24
        ELSE IF ( var_to_write(1:3) == 'rhs' ) THEN
           WRITE(fname(19:24),'(A6)') '.rhs.q'
           fnamelen = 24
        ELSE IF ( var_to_write(1:3) == 'met' ) THEN
           WRITE(fname(19:28),'(A10)') '.metrics.q'
           fnamelen = 28
        ELSE IF ( var_to_write(1:3) == 'av ' ) THEN ! ... Adjoint N-S
           WRITE(fname(19:23),'(A5)') '.av.q'
           fnamelen = 23
        ELSE IF ( var_to_write(1:3) == 'avt' ) THEN ! ... Adjoint N-S
           WRITE(fname(19:30),'(A12)') '.av.target.q'
           fnamelen = 30
        ELSE IF ( var_to_write(1:3) == 'ctr' ) THEN ! ... Control optimization
           WRITE(fname(19:25),'(A7)') '.ctrl.q'
           fnamelen = 25
        ELSE IF ( var_to_write(1:3) == 'drv' ) THEN
           WRITE(fname(19:24),'(A6)') '.drv.q'
           fnamelen = 24
        ELSE IF ( var_to_write(1:3) == 'aux' ) THEN
           WRITE(fname(19:24),'(A6)') '.aux.q'
           fnamelen = 24
        ELSE IF ( var_to_write(1:3) == 'phi' ) THEN
           WRITE(fname(19:24),'(A6)') '.phi.q'
           fnamelen = 24
        ELSE IF ( var_to_write(1:3) == 'dr ' ) THEN
           WRITE(fname(19:23),'(A5)') '.dr.q'
           fnamelen = 23
        ELSE IF ( var_to_write(1:3) == 'dv ' ) THEN
           WRITE(fname(19:23),'(A5)') '.dv.q'
           fnamelen = 23
        ELSE IF ( var_to_write(1:3) == 'mid' ) THEN
           WRITE(fname(19:36),'(A18)') '.metric_identity.q'
           fnamelen = 36
        ELSE IF ( var_to_write(1:3) == 'men' ) THEN
           WRITE(fname(19:25),'(A7)') '.mean.q'
           fnamelen = 25
        ELSE IF ( var_to_write(1:3) == 'pp ') THEN
           WRITE(fname(1:30),'(A17,I8.8,A5)') './post/RocFlo-CM.', iter,'.pp.q'
           fnamelen = 30
        END IF
        
        IF (region%myrank == 0) WRITE (*,'(A)') 'PlasComCM: Writing "'//fname(1:fnamelen)//'".'
        
        ! ... step 2
        ! ... agglomerate decomposed data to core Rank for that grid
        !Call Agglomerate_Data_To_Core_Rank(region, var_to_write)
        CALL Agglomerate_Data_To_Core_Rank_box(region, var_to_write)
        CALL MPI_BARRIER(mycomm, IERR)
        
        ! ... step 3
        ! ... write agglomerated data
        Loop1: DO i = 1, region%grid(1)%numGridsInFile
           
           Loop2: DO ng = 1, region%nGrids
              
              grid => region%grid(ng)
              state => region%state(ng)
              
              IF ( (grid%iGridGlobal == i) .AND. (region%myrank == grid%coreRank_inComm) ) THEN
                 
                 tau(1) = REAL(region%global%main_ts_loop_index,rfreal)
                 tau(2) = region%state(1)%t_output_next
                 tau(3) = state%RE
                 tau(4) = state%time(1)
                 NDIM   = 3
                 
                 ! ... if first grid, write header
                 IF ( i == 1 ) THEN
                    IF ( var_to_write(1:3) == 'met' .OR. var_to_write(1:3) == 'mid' .OR. &
                         var_to_write(1:3) == 'ctr' .OR. var_to_write(1:3) == 'pp ' .OR. &
                         var_to_write(1:3) == 'aux' .OR. var_to_write(1:3) == 'phi' .OR. &
                         var_to_write(1:3) == 'dv ' .OR. var_to_write(1:3) == 'jac') THEN
                       CALL Write_Func_Header(NDIM, grid%numGridsInFile, grid%AllGridsGlobalSize, &
                            SIZE(state%DBUF_IO,4), fname, fnamelen, funit)
                    ELSE
                       CALL Write_Grid_Soln_Header(NDIM, grid%numGridsInFile, grid%AllGridsGlobalSize,&
                            fname, fnamelen, funit)
                    END IF
                 END IF
                 
                 
                 ! ... write grids
                 IF ( var_to_write(1:3) == 'cv ' ) CALL Write_Soln_Single_Grid(grid%ND, grid%GlobalSize, &
                      state%DBUF_IO, tau, fname, fnamelen, funit)
                 IF ( var_to_write(1:3) == 'rhs' ) CALL Write_Soln_Single_Grid(grid%ND, grid%GlobalSize, state%DBUF_IO, tau, fname, fnamelen, funit)
                 IF ( var_to_write(1:3) == 'cvt' ) CALL Write_Soln_Single_Grid(grid%ND, grid%GlobalSize, state%DBUF_IO, tau, fname, fnamelen, funit)
                 
                 IF ( var_to_write(1:3) == 'rst' ) CALL Write_Soln_Single_Grid(grid%ND, grid%GlobalSize, state%DBUF_IO, tau, fname, fnamelen, funit)
                 
                 IF ( var_to_write(1:3) == 'xyz' ) CALL Write_Grid_Single_Grid(grid%ND, grid%GlobalSize, state%DBUF_IO, state%IBUF_IO, fname, fnamelen, funit)
                 
                 IF ( var_to_write(1:3) == 'met' ) CALL Write_Func_Single_Grid(grid%ND, grid%GlobalSize, state%DBUF_IO, fname, fnamelen, funit)
                 
                 IF ( var_to_write(1:3) == 'jac' ) CALL Write_Func_Single_Grid(grid%ND, grid%GlobalSize, state%DBUF_IO, fname, fnamelen, funit)
                 
                 IF ( var_to_write(1:3) == 'aux' ) CALL Write_Func_Single_Grid(grid%ND, grid%GlobalSize, state%DBUF_IO, fname, fnamelen, funit)
                 
                 IF ( var_to_write(1:3) == 'phi' ) CALL Write_Func_Single_Grid(grid%ND, grid%GlobalSize, state%DBUF_IO, fname, fnamelen, funit)
                 
                 IF ( var_to_write(1:3) == 'dv ' ) CALL Write_Func_Single_Grid(grid%ND, grid%GlobalSize, state%DBUF_IO, fname, fnamelen, funit)
                 
                 IF ( var_to_write(1:3) == 'pp ' ) CALL Write_Func_Single_Grid(grid%ND, grid%GlobalSize, state%DBUF_IO, fname, fnamelen, funit)
                 
                 IF ( var_to_write(1:3) == 'av ' ) CALL Write_Soln_Single_Grid(grid%ND, grid%GlobalSize, state%DBUF_IO, tau, fname, fnamelen, funit) ! ... Adjoint N-S
                 
                 IF ( var_to_write(1:3) == 'avt' ) CALL Write_Soln_Single_Grid(grid%ND, grid%GlobalSize, state%DBUF_IO, tau, fname, fnamelen, funit) ! ... Adjoint N-S
                 
                 IF ( var_to_write(1:3) == 'ctr' ) CALL Write_Func_Single_Grid(grid%ND, grid%GlobalSize, state%DBUF_IO, fname, fnamelen, funit) ! ... Control optimization
                 
                 IF ( var_to_write(1:3) == 'drv' ) CALL Write_Soln_Single_Grid(grid%ND, grid%GlobalSize, state%DBUF_IO, tau, fname, fnamelen, funit)
                 
                 IF ( var_to_write(1:3) == 'mid' ) CALL Write_Func_Single_Grid(grid%ND, grid%GlobalSize, state%DBUF_IO, fname, fnamelen, funit)
                 
                 IF ( var_to_write(1:3) == 'men' ) CALL Write_Soln_Single_Grid(grid%ND, grid%GlobalSize, state%DBUF_IO, tau, fname, fnamelen, funit)
                 
                 ! ... clean-up
                 DEALLOCATE(state%DBUF_IO)
                 IF ( var_to_write(1:3) == 'xyz' ) DEALLOCATE(state%IBUF_IO)
                 
                 EXIT Loop2
                 
              END IF
              
           END DO Loop2
           
           CALL MPI_BARRIER(mycomm, ierr)
           
        END DO Loop1
        
        CALL MPI_BARRIER(mycomm, ierr)
        IF (region%myrank == 0) WRITE (*,'(A)') 'PlasComCM: Done writing "'//fname(1:fnamelen)//'".'
     END IF
  END DO

  CALL MPI_BARRIER(mycomm,ierr)
  
END SUBROUTINE WriteP3DFile

  subroutine write_plot3d_file(region, var_to_write, iter_in)

    USE ModGlobal
    USE ModDataStruct
    USE ModMPI

    Implicit None

    type(t_region), pointer :: region
    character(len=3) :: var_to_write
    integer, optional :: iter_in

    ! ... local variables
    type(t_grid), pointer :: grid
    type(t_mixt), pointer :: state
    real(rfreal) :: tau(4)
    integer :: i, ng, numGridsInFile, NDIM, iter
    character(len=80) :: fname
    integer, parameter :: funit = 50
    integer :: fnamelen, ierr

    if (present(iter_in) .eqv. .true.) then
      iter = iter_in
    else
      iter = region%global%main_ts_loop_index
    end if

    ! ... return early if in scaling run
    !    if (region%input%caseID(1:7) == 'SCALING') return

!!$    if (var_to_write(1:3) == 'dv ' .and. region%myrank == 0) then
!!$      write (*,'(A,3(E20.8,1X))') 'CENTERLINE', region%state(1)%time(1), region%state(1)%dv(1,1), region%state(1)%dv(1,2)
!!$    end if

    if (region%input%useLowMemIO == TRUE) then
      call write_plot3d_file_low_mem(region, var_to_write, iter)
      return
    end if

    if (region%input%useLowMemIO == PROCESSOR_IO) then
      call write_plot3d_file_low_mem_per_processor(region, var_to_write, iter)
      return
    end if


    ! ... step 1
    ! ... set the filename
    write (fname(1:18),'(A10,I8.8)') 'RocFlo-CM.', iter
    if ( var_to_write(1:3) == 'cv ' ) then
      write(fname(19:20),'(A2)') '.q'
      fnamelen = 20
    ELSE IF ( var_to_write(1:3) == 'rst' ) then
      if (region%input%AdjNS) then
        write(fname(19:29),'(A11)') '.av.restart'
        fnamelen = 29
      else
        write(fname(19:26),'(A8)') '.restart'
        fnamelen = 26
      end if ! region%input%AdjNS
    ELSE IF ( var_to_write(1:3) == 'cvt' ) then
      write(fname(19:27),'(A9)') '.target.q'
      fnamelen = 27
    ELSE IF ( var_to_write(1:3) == 'xyz' ) then
      write(fname(19:22),'(A4)') '.xyz'
      fnamelen = 22
    ELSE IF ( var_to_write(1:3) == 'jac' ) then
      write(fname(19:24),'(A6)') '.jac.q'
      fnamelen = 24
    ELSE IF ( var_to_write(1:3) == 'rhs' ) then
      write(fname(19:24),'(A6)') '.rhs.q'
      fnamelen = 24
    ELSE IF ( var_to_write(1:3) == 'met' ) then
      write(fname(19:28),'(A10)') '.metrics.q'
      fnamelen = 28
    ELSE IF ( var_to_write(1:3) == 'av ' ) then ! ... Adjoint N-S
      write(fname(19:23),'(A5)') '.av.q'
      fnamelen = 23
    ELSE IF ( var_to_write(1:3) == 'avt' ) then ! ... Adjoint N-S
      write(fname(19:30),'(A12)') '.av.target.q'
      fnamelen = 30
    ELSE IF ( var_to_write(1:3) == 'ctr' ) then ! ... Control optimization
      write(fname(19:25),'(A7)') '.ctrl.q'
      fnamelen = 25
    ELSE IF ( var_to_write(1:3) == 'drv' ) then
      write(fname(19:24),'(A6)') '.drv.q'
      fnamelen = 24
    ELSE IF ( var_to_write(1:3) == 'aux' ) then
      write(fname(19:24),'(A6)') '.aux.q'
      fnamelen = 24
    ELSE IF ( var_to_write(1:3) == 'phi' ) then
      write(fname(19:24),'(A6)') '.phi.q'
      fnamelen = 24
    ELSE IF ( var_to_write(1:3) == 'dr ' ) then
      write(fname(19:23),'(A5)') '.dr.q'
      fnamelen = 23
    ELSE IF ( var_to_write(1:3) == 'dv ' ) then
      write(fname(19:23),'(A5)') '.dv.q'
      fnamelen = 23
    ELSE IF ( var_to_write(1:3) == 'mid' ) then
      write(fname(19:36),'(A18)') '.metric_identity.q'
      fnamelen = 36
    ELSE IF ( var_to_write(1:3) == 'men' ) then
      write(fname(19:25),'(A7)') '.mean.q'
      fnamelen = 25
    ELSE IF ( var_to_write(1:3) == 'pp ') then
      write (fname(1:30),'(A17,I8.8,A5)') './post/RocFlo-CM.', iter,'.pp.q'
      fnamelen = 30
    end if

    if (region%myrank == 0) Write (*,'(A)') 'PlasComCM: Writing "'//fname(1:fnamelen)//'".'

    ! ... step 2
    ! ... agglomerate decomposed data to core Rank for that grid
    Call Agglomerate_Data_To_Core_Rank(region, var_to_write)
    Call MPI_BARRIER(mycomm, IERR)

    ! ... step 3
    ! ... write agglomerated data
    Loop1: Do i = 1, region%grid(1)%numGridsInFile

      Loop2: Do ng = 1, region%nGrids

        grid => region%grid(ng)
        state => region%state(ng)

        if ( (grid%iGridGlobal == i) .and. (region%myrank == grid%coreRank_inComm) ) then

          tau(1) = real(region%global%main_ts_loop_index,rfreal)
          tau(2) = region%state(1)%t_output_next 
          tau(3) = state%RE
          tau(4) = state%time(1)
          NDIM   = 3

          ! ... if first grid, write header
          if ( i == 1 ) then

            if ( var_to_write(1:3) == 'met' .or. var_to_write(1:3) == 'mid' .or. var_to_write(1:3) == 'ctr' &
                 .or. var_to_write(1:3) == 'pp ' .or. var_to_write(1:3) == 'aux' .or. var_to_write(1:3) == 'phi' .or. var_to_write(1:3) == 'dv ' &
                 .or. var_to_write(1:3) == 'jac') then

              Call Write_Func_Header(NDIM, grid%numGridsInFile, grid%AllGridsGlobalSize, size(state%DBUF_IO,4), fname, fnamelen, funit)

            else

              Call Write_Grid_Soln_Header(NDIM, grid%numGridsInFile, grid%AllGridsGlobalSize, fname, fnamelen, funit)

            end if

          end if


          ! ... write grids
          if ( var_to_write(1:3) == 'cv ' ) CALL Write_Soln_Single_Grid(grid%ND, grid%GlobalSize, state%DBUF_IO, tau, fname, fnamelen, funit)

          if ( var_to_write(1:3) == 'rhs' ) CALL Write_Soln_Single_Grid(grid%ND, grid%GlobalSize, state%DBUF_IO, tau, fname, fnamelen, funit)

          if ( var_to_write(1:3) == 'cvt' ) CALL Write_Soln_Single_Grid(grid%ND, grid%GlobalSize, state%DBUF_IO, tau, fname, fnamelen, funit)

          if ( var_to_write(1:3) == 'rst' ) CALL Write_Soln_Single_Grid(grid%ND, grid%GlobalSize, state%DBUF_IO, tau, fname, fnamelen, funit)

          if ( var_to_write(1:3) == 'xyz' ) CALL Write_Grid_Single_Grid(grid%ND, grid%GlobalSize, state%DBUF_IO, state%IBUF_IO, fname, fnamelen, funit)

          if ( var_to_write(1:3) == 'met' ) CALL Write_Func_Single_Grid(grid%ND, grid%GlobalSize, state%DBUF_IO, fname, fnamelen, funit)

          if ( var_to_write(1:3) == 'jac' ) CALL Write_Func_Single_Grid(grid%ND, grid%GlobalSize, state%DBUF_IO, fname, fnamelen, funit)

          if ( var_to_write(1:3) == 'aux' ) CALL Write_Func_Single_Grid(grid%ND, grid%GlobalSize, state%DBUF_IO, fname, fnamelen, funit)
          if ( var_to_write(1:3) == 'phi' ) CALL Write_Func_Single_Grid(grid%ND, grid%GlobalSize, state%DBUF_IO, fname, fnamelen, funit)

          if ( var_to_write(1:3) == 'dv ' ) CALL Write_Func_Single_Grid(grid%ND, grid%GlobalSize, state%DBUF_IO, fname, fnamelen, funit)

          if ( var_to_write(1:3) == 'pp ' ) CALL Write_Func_Single_Grid(grid%ND, grid%GlobalSize, state%DBUF_IO, fname, fnamelen, funit)

          if ( var_to_write(1:3) == 'av ' ) CALL Write_Soln_Single_Grid(grid%ND, grid%GlobalSize, state%DBUF_IO, tau, fname, fnamelen, funit) ! ... Adjoint N-S

          if ( var_to_write(1:3) == 'avt' ) CALL Write_Soln_Single_Grid(grid%ND, grid%GlobalSize, state%DBUF_IO, tau, fname, fnamelen, funit) ! ... Adjoint N-S

          if ( var_to_write(1:3) == 'ctr' ) CALL Write_Func_Single_Grid(grid%ND, grid%GlobalSize, state%DBUF_IO, fname, fnamelen, funit) ! ... Control optimization

          if ( var_to_write(1:3) == 'drv' ) CALL Write_Soln_Single_Grid(grid%ND, grid%GlobalSize, state%DBUF_IO, tau, fname, fnamelen, funit)

          if ( var_to_write(1:3) == 'mid' ) CALL Write_Func_Single_Grid(grid%ND, grid%GlobalSize, state%DBUF_IO, fname, fnamelen, funit)

          if ( var_to_write(1:3) == 'men' ) CALL Write_Soln_Single_Grid(grid%ND, grid%GlobalSize, state%DBUF_IO, tau, fname, fnamelen, funit)

          ! ... clean-up
          deallocate(state%DBUF_IO)
          if ( var_to_write(1:3) == 'xyz' ) deallocate(state%IBUF_IO)

          EXIT Loop2

        end if

      End Do Loop2

      Call MPI_BARRIER(mycomm, ierr)

    End Do Loop1

    Call MPI_BARRIER(mycomm, ierr)

    if (region%myrank == 0) Write (*,'(A)') 'PlasComCM: Done writing "'//fname(1:fnamelen)//'".'

  end subroutine write_plot3d_file

  subroutine read_single_grid_low_mem(NDIM, region, grid_fname, grid, gridID, ib)

    USE ModGlobal
    USE ModDataStruct
    USE ModMPI

    Implicit None

    type(t_region), pointer :: region
    type(t_grid), pointer :: grid
    Character(LEN=80) :: grid_fname
    integer :: gridID ! ... specify which grid we want to read, JKim 04/2008
    integer :: NDIM   ! ... dimension in grid file
    Character(LEN=2) :: ib

    ! ... local variables
    Real(rfreal), Dimension(:,:,:,:), Pointer :: X
    Integer, Dimension(:,:,:), Pointer :: IBLANK
    Integer :: ngrid, I, J, K, L0, Nx, Ny, Nz, M, ftype, dir, N(MAX_ND)
    Integer :: fh, filetype
    Integer :: offset
    Integer :: ibuf(1), is(MAX_ND), ie(MAX_ND)

    ! ... seek to the current array from the beginning of the file
    call plot3d_gridfile_offset(gridID, NDIM, region%nGridsGlobal, grid%AllGridsGlobalSize, ib, offset)

    ! ... extent of this grid (in C-counting)
    is = 0; ie = 0; N = 1;
    do j = 1, grid%ND
       is(j) = grid%is_unique(j)-1
       ie(j) = grid%ie_unique(j)-1
       N(j) = ie(j) - is(j) + 1
    end do

    ! ... allocate buffer
    allocate(X(N(1),N(2),N(3),NDIM))
    if (ib(1:1) .eq. 'y') Allocate(IBLANK(N(1),N(2),N(3)))

    ! ... read the file in binary mode
    if (ib(1:1) .eq. 'n') then
       call read_plot3d_single_grid_mpiio(grid%comm,NDIM, grid%GlobalSize, offset, is, ie, &
            gridID, X, grid_fname, len_trim(grid_fname))
       grid%IBLANK(:) = 1
    else
       call read_plot3d_single_grid_ib_mpiio(grid%comm,NDIM, grid%GlobalSize, offset, is, ie, &
            gridID, X, IBLANK, grid_fname, len_trim(grid_fname))
    end if

    ! ... unpack buffer
    do k = grid%is_unique(3), grid%ie_unique(3)
       do j = grid%is_unique(2), grid%ie_unique(2)
          do i = grid%is_unique(1), grid%ie_unique(1)
             do m = 1, grid%ND
                l0 = (k-grid%is_unique(3)+grid%nGhostRHS(3,1))*(grid%ie(1)-grid%is(1)+1) &
                     *(grid%ie(2)-grid%is(2)+1) &
                     + (j-grid%is_unique(2)+grid%nGhostRHS(2,1))*(grid%ie(1)-grid%is(1)+1) + grid%nGhostRHS(1,1)+i-grid%is_unique(1)+1
                grid%XYZ(l0,m) = X(i-grid%is_unique(1)+1,j-grid%is_unique(2)+1,k-grid%is_unique(3)+1,m)
             end do
          end do
       end do
    end do

    if (ib(1:1) .eq. 'y') then
       ! ... unpack buffer
       do k = grid%is_unique(3), grid%ie_unique(3)
          do j = grid%is_unique(2), grid%ie_unique(2)
             do i = grid%is_unique(1), grid%ie_unique(1)
                l0 = (k-grid%is_unique(3)+grid%nGhostRHS(3,1))*(grid%ie(1)-grid%is(1)+1) &
                     *(grid%ie(2)-grid%is(2)+1) &
                     + (j-grid%is_unique(2)+grid%nGhostRHS(2,1))*(grid%ie(1)-grid%is(1)+1) + grid%nGhostRHS(1,1)+i-grid%is_unique(1)+1
                grid%IBLANK(l0) = IBLANK(i-grid%is_unique(1)+1,j-grid%is_unique(2)+1,k-grid%is_unique(3)+1)
             end do
          end do
       end do
    end if

    deallocate(X)
    if (ib(1:1) .eq. 'y') deallocate(IBLANK)

  end subroutine read_single_grid_low_mem
!!$
  subroutine read_single_soln_low_mem(NDIM, ND, region, grid, grid_fname, dbuf, gridID)

    USE ModGlobal
    USE ModDataStruct
    USE ModMPI

    Implicit None

    Type(t_region), pointer :: region
    Type(t_grid), Pointer :: grid
    Real(rfreal), Dimension(:,:), Pointer :: dbuf
    Character(LEN=80) :: grid_fname
    integer :: gridID ! ... specify which grid we want to read, JKim 04/2008
    integer :: NDIM   ! ... dimension in grid file
    integer, Dimension(:,:), Pointer :: ND

    ! ... local variables
    Real(rfreal), Dimension(:,:,:,:), Pointer :: X
    Character(LEN=2) :: prec, gf, vf, ib
    Integer :: ngrid, I, J, K, L0, Nx, Ny, Nz, M, ftype, dir, N(MAX_ND)
    Integer :: fh, filetype, p
    Integer :: offset, var(MAX_ND+2)
    Integer :: ibuf(1), is(MAX_ND), ie(MAX_ND)
    Integer, Parameter :: sizeof_int = 4, sizeof_dbl = 8

    ! ... pointer
    Do m = 1, 5
      var(m) = m
    End Do

    If (NDIM == 3 .and. region%input%ND == 2 .and. ND(1,3) == 1) Then
      if (region%myrank == 0) print *, 'PlasComCM: changing var(4) = 5 in read_single_soln_low_mem().'
      var(4) = 5
    End If

    If (NDIM == 3 .and. region%input%ND == 1 .and. SUM(ND(1,2:3)) == 2) Then
      if (region%myrank == 0) print *, 'PlasComCM: changing var(3) = 5 in read_single_soln_low_mem().'
      var(3) = 5
    End If

    ! ... seek to the current array from the beginning of the file
    call plot3d_solnfile_offset(gridID, NDIM, region%nGridsGlobal, grid%AllGridsGlobalSize, offset)
    
    ! ... skip another 2 x sizeof(int) + 4 x sizeof(double)
    offset = offset + 2 * sizeof_int + 4 * sizeof_dbl

    ! ... extent of this grid (in C-counting)
    is = 0; ie = 0; N = 1;
    do j = 1, grid%ND
       is(j) = grid%is_unique(j)-1
       ie(j) = grid%ie_unique(j)-1
       N(j) = ie(j) - is(j) + 1
    end do

    ! ... allocate buffer
    allocate(X(N(1),N(2),N(3),NDIM+2))

    ! ... read the file in binary mode
!    call read_plot3d_single_soln_whole_format_low_mem(NDIM, grid%GlobalSize, offset, is, ie, gridID, X, grid_fname, len_trim(grid_fname))
    call read_plot3d_single_soln_mpiio(grid%comm,NDIM, grid%GlobalSize, offset, is, ie, &
         gridID, X, grid_fname, len_trim(grid_fname))

    ! ... unpack buffer
    do k = grid%is_unique(3), grid%ie_unique(3)
       do j = grid%is_unique(2), grid%ie_unique(2)
          do i = grid%is_unique(1), grid%ie_unique(1)
             do m = 1, grid%ND+2
                l0 = (k-grid%is_unique(3)+grid%nGhostRHS(3,1))*(grid%ie(1)-grid%is(1)+1) &
                     *(grid%ie(2)-grid%is(2)+1) &
                     + (j-grid%is_unique(2)+grid%nGhostRHS(2,1))*(grid%ie(1)-grid%is(1)+1) + grid%nGhostRHS(1,1)+i-grid%is_unique(1)+1
                dbuf(l0,m) = X(i-grid%is_unique(1)+1,j-grid%is_unique(2)+1,k-grid%is_unique(3)+1,var(m))
             end do
          end do
       end do
    end do

    deallocate(X)

  end subroutine read_single_soln_low_mem

  Subroutine plot3d_gridfile_offset(gridID, NDIM, nGridsGlobal, GlobalSize, ib, offset)

    USE ModGlobal

    Implicit None

    ! ... function call data
    Integer :: gridID, NDIM, nGridsGlobal, offset
    Integer, Pointer, Dimension(:,:) :: GlobalSize
    Character(LEN=2) :: ib

    ! ... local data
    Integer, Parameter :: sizeof_int = 4, sizeof_dbl = 8
    Integer :: i

    ! ... seek to the current array from the beginning of the file
    ! ... ngrid + 2 records
    offset = sizeof_int + 2*sizeof_int

    ! ... grid dimensions plus 2 records
    offset = offset + NDIM*nGridsGlobal*sizeof_int + 2*sizeof_int

    ! ... number of grids previous plus their records
    if (ib(1:1) .eq. 'n') then
       do i = 1, gridID-1
          offset = offset + PRODUCT(GlobalSize(i,:))*NDIM*sizeof_dbl + 2*sizeof_int
       end do
    else
       do i = 1, gridID-1
          offset = offset + PRODUCT(GlobalSize(i,:))*(NDIM*sizeof_dbl+sizeof_int) + 2*sizeof_int
       end do
    end if

    ! ... final offset for the first record
    offset = offset + sizeof_int

  End Subroutine plot3d_gridfile_offset
!!$
!!$
  Subroutine Write_Solution_Grid_Header(grid, tau, file, fnamelen, offset)

    USE ModGlobal
    USE ModDataStruct

    Implicit None

    Type(t_grid), Pointer :: grid
    REAL(RFREAL),DIMENSION(:) :: tau  
    Character(LEN=80) :: file
    Integer :: fnamelen,offset
    INTEGER :: j, ierr, funit
    INTEGER, PARAMETER :: SEEK_SET = 0, SEEK_CUR = 1, SEEK_END = 2
    INTEGER, PARAMETER :: sizeof_int = 4, sizeof_dbl = 8



    IF(grid%myrank_incomm == 0) THEN
       funit = 4
       CALL WRITEDOUBLE(TRIM(file),tau,funit,offset)
    END IF
    
    offset = offset + 4*sizeof_dbl + 2*sizeof_int

  End Subroutine Write_Solution_Grid_Header


  Subroutine AppendGridSolutionHeader(tau, file, fnamelen, funit)

    USE ModGlobal
    USE ModDataStruct

    Implicit None

    REAL(RFREAL),DIMENSION(:) :: tau  
    Character(LEN=80) :: file
    Integer :: funit, fnamelen,i
    
    Open (unit=funit, file=file(1:fnamelen), form='unformatted', status='unknown', position='append')

    Write (funit) (tau(i),i=1,4)

    Close (funit)

  End Subroutine AppendGridSolutionHeader

  Subroutine AppendSolutionGridHeader(grid, tau, file)

    USE ModGlobal
    USE ModDataStruct

    Implicit None

    Type(t_grid), Pointer :: grid
    REAL(RFREAL),DIMENSION(:) :: tau  
    Character(LEN=80) :: file

    INTEGER :: j, ierr, funit
    INTEGER, PARAMETER :: SEEK_SET = 0, SEEK_CUR = 1, SEEK_END = 2
    INTEGER, PARAMETER :: sizeof_int = 4, sizeof_dbl = 8



    IF(grid%myrank_incomm == 0) THEN
       funit = 4
       CALL WRITEDOUBLE(TRIM(file),tau,funit,-1)
    END IF
    
    

  End Subroutine AppendSolutionGridHeader

  Subroutine plot3d_solnfile_offset(gridID, NDIM, nGridsGlobal, GlobalSize, offset)

    USE ModGlobal

    Implicit None

    ! ... function call data
    Integer :: gridID, NDIM, nGridsGlobal, offset
    Integer, Pointer, Dimension(:,:) :: GlobalSize
    Character(LEN=2) :: ib

    ! ... local data
    Integer, Parameter :: sizeof_int = 4, sizeof_dbl = 8
    Integer :: i

    ! ... seek to the current array from the beginning of the file
    ! ... ngrid + 2 records
    offset = sizeof_int + 2*sizeof_int

    ! ... grid dimensions plus 2 records
    offset = offset + NDIM*nGridsGlobal*sizeof_int + 2*sizeof_int

    ! ... number of grids previous plus their records
    do i = 1, gridID-1
      offset = offset + 4*sizeof_dbl + 2*sizeof_int
      offset = offset + PRODUCT(GlobalSize(i,:))*(NDIM+2)*sizeof_dbl + 2*sizeof_int
    end do

    ! ... final offset for the first record
    offset = offset + sizeof_int

  End Subroutine plot3d_solnfile_offset

!!$
  Subroutine plot3d_funcfile_offset(gridID, NDIM, nGridsGlobal, GlobalSize, nVar, offset)

    USE ModGlobal

    Implicit None

    ! ... function call data
    Integer :: gridID, NDIM, nGridsGlobal, offset, nVar
    Integer, Pointer, Dimension(:,:) :: GlobalSize
    Character(LEN=2) :: ib

    ! ... local data
    Integer, Parameter :: sizeof_int = 4, sizeof_dbl = 8
    Integer :: i

    ! ... seek to the current array from the beginning of the file
    ! ... ngrid + 2 records
    offset = sizeof_int + 2*sizeof_int

    ! ... grid dimensions plus nVar plus 2 records
    offset = offset + (NDIM+1)*nGridsGlobal*sizeof_int + 2*sizeof_int

    ! ... number of grids previous plus their records
    do i = 1, gridID-1
      offset = offset + PRODUCT(GlobalSize(i,:))*nvar*sizeof_dbl + 2*sizeof_int
    end do

    ! ... final offset for the first record
    offset = offset + sizeof_int

  End Subroutine plot3d_funcfile_offset


  subroutine write_plot3d_file_low_mem(region, var_to_write, iter_in)

    USE ModGlobal
    USE ModDataStruct
    USE ModMPI

    Implicit None

    type(t_region), pointer :: region
    character(len=3) :: var_to_write
    integer, optional :: iter_in

    ! ... local variables
    type(t_grid), pointer :: grid
    type(t_mixt), pointer :: state
    real(rfreal) :: tau(4)
    integer :: i, ng, numGridsInFile
    character(len=80) :: fname
    integer, parameter :: funit = 50
    integer :: fnamelen
    character(len=2) :: ib
    integer :: offset, NDIM, p, iter, ierr

    NDIM = 3

!    write(*,*) 'WRITE_PLOT3D_FILE_LOWMEM'

    if (present(iter_in) .eqv. .true.) then
      iter = iter_in
    else
      iter = region%global%main_ts_loop_index
    end if

    ! ... step 1
    ! ... set the filename
    write (fname(1:18),'(A10,I8.8)') 'RocFlo-CM.', iter
    if ( var_to_write(1:3) == 'cv ' ) then
      write(fname(19:20),'(A2)') '.q'
      fnamelen = 20
    ELSE IF ( var_to_write(1:3) == 'rst' ) then
      write(fname(19:26),'(A8)') '.restart'
      fnamelen = 26
    ELSE IF ( var_to_write(1:3) == 'cvt' ) then
      write(fname(19:27),'(A9)') '.target.q'
      fnamelen = 27
    ELSE IF ( var_to_write(1:3) == 'xyz' ) then
      write(fname(19:22),'(A4)') '.xyz'
      fnamelen = 22
    ELSE IF ( var_to_write(1:3) == 'met' ) then
      write(fname(19:28),'(A10)') '.metrics.q'
      fnamelen = 28
    ELSE IF ( var_to_write(1:3) == 'jac' ) then
      write(fname(19:24),'(A6)') '.jac.q'
      fnamelen = 24
    ELSE IF ( var_to_write(1:3) == 'rhs' ) then
      write(fname(19:24),'(A6)') '.rhs.q'
      fnamelen = 24
    ELSE IF ( var_to_write(1:3) == 'drv' ) then
      write(fname(19:24),'(A6)') '.drv.q'
      fnamelen = 24
    ELSE IF ( var_to_write(1:3) == 'mid' ) then
      write(fname(19:36),'(A18)') '.metric_identity.q'
      fnamelen = 36
    ELSE IF ( var_to_write(1:3) == 'aux' ) then
      write(fname(19:24),'(A6)') '.aux.q'
      fnamelen = 24
    ELSE IF ( var_to_write(1:3) == 'phi' ) then
      write(fname(19:24),'(A6)') '.phi.q'
      fnamelen = 24
    ELSE IF ( var_to_write(1:3) == 'dv ' ) then
      write(fname(19:23),'(A5)') '.dv.q'
      fnamelen = 23
    ELSE IF ( var_to_write(1:3) == 'men' ) then
      write(fname(19:25),'(A7)') '.mean.q'
      fnamelen = 25
    end if

    if (region%myrank == 0) Write (*,'(A)') 'PlasComCM: Writing "'//fname(1:fnamelen)//'".'

    ! ... step 2
    ! ... write header and pad file with zeros
    if (region%myrank == 0) then

      grid => region%grid(1)
      state => region%state(1)

      if ( var_to_write(1:3) == 'met' ) then

        Call Write_Func_Header_And_Pad(NDIM, grid%numGridsInFile, grid%AllGridsGlobalSize, size(grid%MT1,2), fname, fnamelen, funit)

      ELSE IF ( var_to_write(1:3) == 'met' ) then

        Call Write_Func_Header_And_Pad(NDIM, grid%numGridsInFile, grid%AllGridsGlobalSize, 1, fname, fnamelen, funit)

      ELSE IF ( var_to_write(1:3) == 'aux' ) then

        Call Write_Func_Header_And_Pad(NDIM, grid%numGridsInFile, grid%AllGridsGlobalSize, size(state%auxVars,2), fname, fnamelen, funit)

      ELSE IF ( var_to_write(1:3) == 'phi' ) then

        Call Write_Func_Header_And_Pad(NDIM, grid%numGridsInFile, grid%AllGridsGlobalSize, 1, fname, fnamelen, funit)

      ELSE IF ( var_to_write(1:3) == 'dv ' ) then

        Call Write_Func_Header_And_Pad(NDIM, grid%numGridsInFile, grid%AllGridsGlobalSize, size(state%dv,2), fname, fnamelen, funit)

      ELSE IF ( var_to_write(1:3) == 'mid' ) then

        Call Write_Func_Header_And_Pad(NDIM, grid%numGridsInFile, grid%AllGridsGlobalSize, size(grid%ident,2), fname, fnamelen, funit)

      ELSE IF ( var_to_write(1:3) == 'xyz' ) then

        Call Write_Grid_Header_And_Pad(NDIM, grid%numGridsInFile, grid%AllGridsGlobalSize, fname, fnamelen, funit)

      else

        Call Write_Soln_Header_And_Pad(NDIM, grid%numGridsInFile, grid%AllGridsGlobalSize, fname, fnamelen, funit)

      end if

    end if
    call mpi_barrier(mycomm, ierr)

    ! ... at this point the entire file should be written, but with zeros.  Now we can seek and write
    ! ... to be safe we have one processor writing a time
    Loop1: Do i = 1, region%nGridsGlobal

      Loop2: Do ng = 1, region%nGrids

        grid => region%grid(ng)
        state => region%state(ng)

        if ( grid%iGridGlobal == i) then

          do p = 0, grid%numproc_inComm-1

            if (grid%myrank_inComm == p) then

              tau(1) = real(region%global%main_ts_loop_index,rfreal)
              tau(2) = region%state(1)%t_output_next
              tau(3) = state%RE
              tau(4) = state%time(1)
              ib(1:1) = 'y' ! ... default to iblank

              ! ... write grids
              if ( var_to_write(1:3) == 'cv ' ) Then
                call plot3d_solnfile_offset(i, NDIM, region%nGridsGlobal, grid%AllGridsGlobalSize, offset)
                Call Write_Single_Soln_Low_Mem(grid, state%cv, tau, fname, fnamelen, offset)
              end if

              if ( var_to_write(1:3) == 'rhs' ) Then
                call plot3d_solnfile_offset(i, NDIM, region%nGridsGlobal, grid%AllGridsGlobalSize, offset)
                Call Write_Single_Soln_Low_Mem(grid, state%rhs, tau, fname, fnamelen, offset)
              end if

              if ( var_to_write(1:3) == 'cvt' ) Then
                call plot3d_solnfile_offset(i, NDIM, region%nGridsGlobal, grid%AllGridsGlobalSize, offset)
                Call Write_Single_Soln_Low_Mem(grid, state%cvTarget, tau, fname, fnamelen, offset)
              end if

              if ( var_to_write(1:3) == 'rst' ) Then
                call plot3d_solnfile_offset(i, NDIM, region%nGridsGlobal, grid%AllGridsGlobalSize, offset)
                Call Write_Single_Soln_Low_Mem(grid, state%cv, tau, fname, fnamelen, offset)
              end if

              if ( var_to_write(1:3) == 'xyz' ) Then
                call plot3d_gridfile_offset(i, NDIM, region%nGridsGlobal, grid%AllGridsGlobalSize, ib, offset)
                Call Write_Single_Grid_Low_Mem(grid, fname, fnamelen, offset)
              end if

              if ( var_to_write(1:3) == 'met' ) then
                call plot3d_funcfile_offset(i, NDIM, region%nGridsGlobal, grid%AllGridsGlobalSize, size(grid%MT1,2), offset)
                Call Write_Single_Func_Low_Mem(grid, grid%MT1, fname, fnamelen, offset)
              end if

              if ( var_to_write(1:3) == 'jac' ) then
                call plot3d_funcfile_offset(i, NDIM, region%nGridsGlobal, grid%AllGridsGlobalSize, 1, offset)
                Call Write_Single_Func_Low_Mem(grid, grid%MT1, fname, fnamelen, offset)
              end if

              if ( var_to_write(1:3) == 'aux' ) then
                call plot3d_funcfile_offset(i, NDIM, region%nGridsGlobal, grid%AllGridsGlobalSize, size(state%auxVars,2), offset)
                Call Write_Single_Func_Low_Mem(grid, state%auxVars, fname, fnamelen, offset)
              end if

#ifdef BUILD_ELECTRIC
              if ( var_to_write(1:3) == 'phi' ) then
                call plot3d_funcfile_offset(i, NDIM, region%nGridsGlobal, grid%AllGridsGlobalSize, 1, offset)
                if (region%electric(ng)%grid_has_efield) then
                  region%electric(ng)%phi_data_ptr(:,1) = region%electric(ng)%phi
                end if
                Call Write_Single_Func_Low_Mem(grid, region%electric(ng)%phi_data_ptr, fname, fnamelen, offset)
              end if
#endif

              if ( var_to_write(1:3) == 'dv ' ) then
                call plot3d_funcfile_offset(i, NDIM, region%nGridsGlobal, grid%AllGridsGlobalSize, size(state%dv,2), offset)
                Call Write_Single_Func_Low_Mem(grid, state%dv, fname, fnamelen, offset)
              end if

              if ( var_to_write(1:3) == 'drv' ) Then
                call plot3d_solnfile_offset(i, NDIM, region%nGridsGlobal, grid%AllGridsGlobalSize, offset)
                Call Write_Single_Soln_Low_Mem(grid, state%cv, tau, fname, fnamelen, offset)
              end if

              if ( var_to_write(1:3) == 'mid' ) then
                call plot3d_funcfile_offset(i, NDIM, region%nGridsGlobal, grid%AllGridsGlobalSize, size(grid%ident,2), offset)
                Call Write_Single_Func_Low_Mem(grid, grid%ident, fname, fnamelen, offset)
              end if

            end if

            call MPI_BARRIER(grid%comm, ierr)

          end do

          EXIT Loop2

        end if

      End Do Loop2

      Call MPI_BARRIER(mycomm, ierr)

    End Do Loop1

    Call MPI_BARRIER(mycomm, ierr)

    if (region%myrank == 0) Write (*,'(A)') 'PlasComCM: Done writing "'//fname(1:fnamelen)//'".'

  end subroutine write_plot3d_file_low_mem
  
  SUBROUTINE MakeP3DFileName(input,iter,var_to_write,fileName,nameLen)
    
    USE ModGlobal
    USE ModDataStruct
    
    IMPLICIT NONE
    
    TYPE(t_mixt_input), POINTER :: input
    CHARACTER(LEN=3)            :: var_to_write
    INTEGER                     :: iter,nameLen
    CHARACTER(LEN=80)           :: fileName

    fileName(:)  = ' '

    nameLen = 0
    IF(TRIM(input%outputDirectory) .NE. 'NULL') THEN
       nameLen = LEN(TRIM(input%outputDirectory))
       WRITE(fileName(1:nameLen),*) TRIM(input%outputDirectory)
       WRITE(filename(nameLen+1:nameLen+1),*) '/'
       nameLen = nameLen + 1
    ENDIF

    ! ... step 1
    ! ... set the filename
    write (fileName(nameLen+1:nameLen+18),'(A10,I8.8)') 'RocFlo-CM.', iter
    nameLen = nameLen + 18

    if ( var_to_write(1:3) == 'cv ' ) then
       write(fileName(nameLen+1:nameLen+2),'(A2)') '.q'
       nameLen = nameLen + 2
    ELSE IF ( var_to_write(1:3) == 'rst' ) then
       write(fileName(nameLen+1:nameLen+8),'(A8)') '.restart'
       nameLen = nameLen + 8
    ELSE IF ( var_to_write(1:3) == 'cvt' ) then
       write(fileName(nameLen+1:nameLen+9),'(A9)') '.target.q'
       nameLen = nameLen + 9
    ELSE IF ( var_to_write(1:3) == 'xyz' ) then
       write(fileName(nameLen+1:nameLen+4),'(A4)') '.xyz'
       nameLen = nameLen + 4
    ELSE IF ( var_to_write(1:3) == 'met' ) then
       write(fileName(nameLen+1:nameLen+10),'(A10)') '.metrics.q'
       nameLen = nameLen + 10
    ELSE IF ( var_to_write(1:3) == 'jac' ) then
       write(fileName(nameLen+1:nameLen+6),'(A6)') '.jac.q'
       nameLen = nameLen + 6
    ELSE IF ( var_to_write(1:3) == 'rhs' ) then
       write(fileName(nameLen+1:nameLen+6),'(A6)') '.rhs.q'
       nameLen = nameLen + 6
    ELSE IF ( var_to_write(1:3) == 'drv' ) then
       write(fileName(nameLen+1:nameLen+6),'(A6)') '.drv.q'
       nameLen = nameLen + 6
    ELSE IF ( var_to_write(1:3) == 'mid' ) then
       write(fileName(nameLen+1:nameLen+18),'(A18)') '.metric_identity.q'
       nameLen = nameLen + 18
    ELSE IF ( var_to_write(1:3) == 'aux' ) then
       write(fileName(nameLen+1:nameLen+6),'(A6)') '.aux.q'
       nameLen = nameLen + 6
    ELSE IF ( var_to_write(1:3) == 'phi' ) then
       write(fileName(nameLen+1:nameLen+6),'(A6)') '.phi.q'
       nameLen = nameLen + 6 
    ELSE IF ( var_to_write(1:3) == 'dv ' ) then
       write(fileName(nameLen+1:nameLen+5),'(A5)') '.dv.q'
       nameLen = nameLen + 5
    ELSE IF ( var_to_write(1:3) == 'men' ) then
       write(fileName(nameLen+1:nameLen+7),'(A7)') '.mean.q'
       nameLen = nameLen + 7
    end if

  END SUBROUTINE MakeP3DFileName

  subroutine write_plot3d_file_mpiio(region, var_to_write, iter_in)
    
    USE ModGlobal
    USE ModDataStruct
    USE ModMPI
    
    Implicit None
    
    type(t_region), pointer :: region
    character(len=3) :: var_to_write
    integer, optional :: iter_in
    
    ! ... local variables
    Type(t_mixt_input), Pointer :: input
    type(t_grid), pointer :: grid
    type(t_mixt), pointer :: state
    real(rfreal) :: tau(4),timer
    integer :: i, ng, numGridsInFile
    character(len=80) :: fname
    integer, parameter :: funit = 50
    integer :: fnamelen
    character(len=2) :: ib
    integer :: offset, NDIM, p, iter, ierr
  
    NDIM = 3


    input => region%input

    timer = MPI_WTIME()
    if (present(iter_in) .eqv. .true.) then
       iter = iter_in
    else
       iter = region%global%main_ts_loop_index
    end if

    CALL MakeP3DFileName(input,iter,var_to_write,fname,fnamelen)

    if (region%myrank == 0) Write (*,'(A)') 'PlasComCM: Writing "'//fname(1:fnamelen)//'".'

    ! ... step 2
    ! ... write header and pad file with zeros
    IF (region%myrank == 0) THEN
       
       grid => region%grid(1)
       state => region%state(1)

       IF ( var_to_write(1:3) == 'met' ) THEN
          
          CALL WriteFunctionFileHeader(NDIM, grid%numGridsInFile, grid%AllGridsGlobalSize, size(grid%MT1,2), fname, fnamelen, funit)
          
       ELSE IF ( var_to_write(1:3) == 'jac' ) THEN
          
          CALL WriteFunctionFileHeader(NDIM, grid%numGridsInFile, grid%AllGridsGlobalSize, 1, fname, fnamelen, funit)
          
       ELSE IF ( var_to_write(1:3) == 'aux' ) THEN
          
          CALL WriteFunctionFileHeader(NDIM, grid%numGridsInFile, grid%AllGridsGlobalSize, SIZE(state%auxVars,2), fname, fnamelen, funit)
          
       ELSE IF ( var_to_write(1:3) == 'phi' ) THEN
          
          CALL WriteFunctionFileHeader(NDIM, grid%numGridsInFile, grid%AllGridsGlobalSize, 1, fname, fnamelen, funit)
          
       ELSE IF ( var_to_write(1:3) == 'dv ' ) THEN
          
          CALL WriteFunctionFileHeader(NDIM, grid%numGridsInFile, grid%AllGridsGlobalSize, SIZE(state%dv,2), fname, fnamelen, funit)
          
       ELSE IF ( var_to_write(1:3) == 'mid' ) THEN
          
          CALL Write_Func_Header_And_Pad(NDIM, grid%numGridsInFile, grid%AllGridsGlobalSize, SIZE(grid%ident,2), fname, fnamelen, funit)
          
       ELSE IF ( var_to_write(1:3) == 'xyz' ) then
          
          Call WriteGridFileHeader(NDIM, grid%numGridsInFile, grid%AllGridsGlobalSize, fname, fnamelen, funit)
       ELSE

          Call WriteSolutionFileHeader(NDIM, grid%numGridsInFile, grid%AllGridsGlobalSize, fname, fnamelen, funit)

       end if

    end if
    call mpi_barrier(mycomm, ierr)
    region%mpi_timings(:)%io_setup = region%mpi_timings(:)%io_setup + (MPI_WTIME() - timer)


    ! ... at this point the entire file should be written, but with zeros.  Now we can seek and write
    ! ... to be safe we have one processor writing a time
    Loop1: Do i = 1, region%nGridsGlobal
       
       Loop2: Do ng = 1, region%nGrids
          
          grid => region%grid(ng)
          state => region%state(ng)
          
          if ( grid%iGridGlobal == i) then
             
             !            do p = 0, grid%numproc_inComm-1
             
             !               if (grid%myrank_inComm == p) then
             CALL MPI_BARRIER(grid%comm,ierr)
             tau(1) = real(region%global%main_ts_loop_index,rfreal)
             tau(2) = region%state(1)%t_output_next
             tau(3) = state%RE
             tau(4) = state%time(1)
             ib(1:1) = 'y' ! ... default to iblank
             
             ! ... write grids
             if ( var_to_write(1:3) == 'cv ' ) Then
                IF(grid%myrank_incomm == 0) THEN
                   CALL AppendGridSolutionHeader(tau,fname,fnamelen,funit)
                ENDIF
                CALL AppendSingleSolution_MPIIO(grid, state%cv, fname, fnamelen)
             end if
             
             if ( var_to_write(1:3) == 'rhs' ) Then
                IF(grid%myrank_incomm == 0) THEN
                   CALL AppendGridSolutionHeader(tau,fname,fnamelen,funit)
                ENDIF
                CALL AppendSingleSolution_MPIIO(grid, state%rhs, fname, fnamelen)
             end if
             
             if ( var_to_write(1:3) == 'cvt' ) Then
                IF(grid%myrank_incomm == 0) THEN
                   CALL AppendGridSolutionHeader(tau,fname,fnamelen,funit)
                ENDIF
                CALL AppendSingleSolution_MPIIO(grid, state%cvTarget, fname, fnamelen)
             end if
             
             if ( var_to_write(1:3) == 'rst' ) Then
                IF(grid%myrank_incomm == 0) THEN
                   CALL AppendGridSolutionHeader(tau,fname,fnamelen,funit)
                ENDIF
                CALL AppendSingleSolution_MPIIO(grid, state%cv, fname, fnamelen)
             end if
             
             if ( var_to_write(1:3) == 'xyz' ) Then
                CALL AppendSingleGrid_MPIIO(grid, fname, fnamelen)
             end if
             
             if ( var_to_write(1:3) == 'met' ) then
                CALL AppendSingleFunction_MPIIO(grid, grid%MT1, fname, fnamelen)
             end if
             
             if ( var_to_write(1:3) == 'jac' ) then
                CALL AppendSingleFunction_MPIIO(grid, grid%MT1, fname, fnamelen)
             end if
             
             if ( var_to_write(1:3) == 'aux' ) then
                CALL AppendSingleFunction_MPIIO(grid, state%auxVars, fname, fnamelen)
             end if
             
#ifdef BUILD_ELECTRIC
             if ( var_to_write(1:3) == 'phi' ) then
                if (region%electric(ng)%grid_has_efield) then
                   region%electric(ng)%phi_data_ptr(:,1) = region%electric(ng)%phi
                end if
                CALL AppendSingleFunction_MPIIO(grid, region%electric(ng)%phi_data_ptr, fname, fnamelen)
             end if
#endif
             
             if ( var_to_write(1:3) == 'dv ' ) then
                Call AppendSingleFunction_MPIIO(grid, state%dv, fname, fnamelen)
             end if
             
             if ( var_to_write(1:3) == 'drv' ) Then
                Call AppendSingleSolution_MPIIO(grid, state%cv, fname, fnamelen)
             end if
             
             if ( var_to_write(1:3) == 'mid' ) then
                Call AppendSingleFunction_MPIIO(grid, grid%ident, fname, fnamelen)
             end if
             
             call MPI_BARRIER(grid%comm, ierr)
             
             
             EXIT Loop2
             
          end if
          
       End Do Loop2
       
       Call MPI_BARRIER(mycomm, ierr)
       
    End Do Loop1
    
    Call MPI_BARRIER(mycomm, ierr)

    if (region%myrank == 0) Write (*,'(A)') 'PlasComCM: Done writing "'//fname(1:fnamelen)//'".'
    
  end subroutine write_plot3d_file_mpiio
  
!!$
!!$
  subroutine write_single_grid_low_mem(grid, fname, fnamelen, offset)

    USE ModGlobal
    USE ModDataStruct
    USE ModMPI

    Implicit None

    Type(t_grid), Pointer :: grid
    Character(LEN=80) :: fname
    Integer :: fnamelen, offset
    Integer, Dimension(:), Pointer :: ibuf
    Real(RFREAL), Dimension(:,:), Pointer :: dbuf

    ! ... local variables
    Real(rfreal), Dimension(:,:,:,:), Pointer :: X
    Integer, Dimension(:,:,:), Pointer :: IBLANK
    Character(LEN=2) :: prec, gf, vf, ib
    Integer :: ngrid, I, J, K, L0, Nx, Ny, Nz, M, ftype, dir, N(MAX_ND)
    Integer :: fh, filetype
    Integer :: is(MAX_ND), ie(MAX_ND)

    ! ... extent of this grid (in C-counting)
    is(:) = 0; ie(:) = 0; N(:) = 1;
    do j = 1, grid%ND
       is(j) = grid%is_unique(j)-1
       ie(j) = grid%ie_unique(j)-1
       N(j) = ie(j) - is(j) + 1
    end do

    ! ... allocate buffer
    Allocate(X(N(1),N(2),N(3),3)); ! X(:,:,:,:) = 0.0_rfreal
    Allocate(IBLANK(N(1),N(2),N(3)))

    ! ... pack buffer
    do k = 1, N(3)
       do j = 1, N(2)
          do i = 1, N(1)

             l0 = (k-1)*N(1)*N(2) + (j-1)*N(1) + i

             IBLANK(i,j,k) = grid%IBLANK(l0) 

             do m = 1, grid%ND
                X(i,j,k,m) = grid%XYZ(l0,m) 
             end do

          end do
       end do
    end do

    call write_plot3d_single_grid_whole_format_with_IBLANK_low_mem(grid%GlobalSize, offset, is, ie, X, IBLANK, fname, fnamelen)

    deallocate(X)
    deallocate(IBLANK)

  end subroutine write_single_grid_low_mem

  subroutine write_single_grid_mpiio(grid, fname, fnamelen, offset)

    USE ModGlobal
    USE ModDataStruct
    USE ModMPI

    Implicit None

    Type(t_grid), Pointer :: grid
    Character(LEN=80) :: fname
    Integer :: fnamelen, offset
    Integer, Dimension(:), Pointer :: ibuf
    Real(RFREAL), Dimension(:,:), Pointer :: dbuf

    ! ... local variables
    Real(rfreal), Dimension(:,:,:,:), Pointer :: X
    Integer, Dimension(:,:,:), Pointer :: IBLANK
    Character(LEN=2) :: prec, gf, vf, ib
    Integer :: ngrid, I, J, K, L0, Nx, Ny, Nz, M, ftype, dir, N(MAX_ND)
    Integer :: fh, filetype, ii, jj, kk
    Integer :: is(MAX_ND), ie(MAX_ND)

    ! ... extent of this grid (in C-counting)
    is(:) = 0; ie(:) = 0; N(:) = 1
    do j = 1, grid%ND
       is(j) = grid%is_unique(j)-1
       ie(j) = grid%ie_unique(j)-1
       N(j) = ie(j) - is(j) + 1
    end do

    ! ... allocate buffer
    Allocate(X(N(1),N(2),N(3),3)); ! X(:,:,:,:) = 0.0_rfreal
    Allocate(IBLANK(N(1),N(2),N(3)))

    ! ... pack buffer
    do k = grid%is_unique(3), grid%ie_unique(3)
       do j = grid%is_unique(2), grid%ie_unique(2)
          do i = grid%is_unique(1), grid%ie_unique(1)

             ii = i - grid%is_unique(1) + 1
             jj = j - grid%is_unique(2) + 1
             kk = k - grid%is_unique(3) + 1

             l0 = (kk-1+grid%nGhostRHS(3,1))*(grid%ie(1)-grid%is(1)+1) &
                  *(grid%ie(2)-grid%is(2)+1) &
                  + (jj-1+grid%nGhostRHS(2,1))*(grid%ie(1)-grid%is(1)+1) &
                  + grid%nGhostRHS(1,1)+ii

             IBLANK(ii,jj,kk) = grid%IBLANK(l0) 

             do m = 1, grid%ND
                X(ii,jj,kk,m) = grid%XYZ(l0,m) 
             end do

          end do
       end do
    end do
    if( grid%ND == 2) then
       do k = 1, N(3)
          do j = 1, N(2)
             do i = 1, N(1)
                X(i,j,k,3) = 0.0_RFREAL
             end do
          end do
       end do
    end if

    call write_plot3d_grid_ib_mpiio(grid%comm,3,grid%GlobalSize, offset, is, ie, X, IBLANK, fname, fnamelen)

    deallocate(X)
    deallocate(IBLANK)

  end subroutine write_single_grid_mpiio

  SUBROUTINE AppendSingleGrid_MPIIO(grid, fname, fnamelen)

    USE ModGlobal
    USE ModDataStruct
    USE ModMPI

    Implicit None

    Type(t_grid), Pointer :: grid
    Character(LEN=80) :: fname
    Integer :: fnamelen, offset
    Integer, Dimension(:), Pointer :: ibuf
    Real(RFREAL), Dimension(:,:), Pointer :: dbuf

    ! ... local variables
    Real(rfreal), Dimension(:,:,:,:), Pointer :: X
    Integer, Dimension(:,:,:), Pointer :: IBLANK
    Character(LEN=2) :: prec, gf, vf, ib
    Integer :: ngrid, I, J, K, L0, Nx, Ny, Nz, M, ftype, dir, N(MAX_ND)
    Integer :: fh, filetype, ii, jj, kk
    Integer :: is(MAX_ND), ie(MAX_ND)

    ! ... extent of this grid (in C-counting)
    is(:) = 0; ie(:) = 0; N(:) = 1
    do j = 1, grid%ND
       is(j) = grid%is_unique(j)-1
       ie(j) = grid%ie_unique(j)-1
       N(j) = ie(j) - is(j) + 1
    end do

    ! ... allocate buffer
    Allocate(X(N(1),N(2),N(3),3)); ! X(:,:,:,:) = 0.0_rfreal
    Allocate(IBLANK(N(1),N(2),N(3)))

    ! ... pack buffer
    do k = grid%is_unique(3), grid%ie_unique(3)
       do j = grid%is_unique(2), grid%ie_unique(2)
          do i = grid%is_unique(1), grid%ie_unique(1)

             ii = i - grid%is_unique(1) + 1
             jj = j - grid%is_unique(2) + 1
             kk = k - grid%is_unique(3) + 1

             l0 = (kk-1+grid%nGhostRHS(3,1))*(grid%ie(1)-grid%is(1)+1) &
                  *(grid%ie(2)-grid%is(2)+1) &
                  + (jj-1+grid%nGhostRHS(2,1))*(grid%ie(1)-grid%is(1)+1) &
                  + grid%nGhostRHS(1,1)+ii

             IBLANK(ii,jj,kk) = grid%IBLANK(l0) 

             do m = 1, grid%ND
                X(ii,jj,kk,m) = grid%XYZ(l0,m) 
             end do

          end do
       end do
    end do
    if( grid%ND == 2) then
       do k = 1, N(3)
          do j = 1, N(2)
             do i = 1, N(1)
                X(i,j,k,3) = 0.0_RFREAL
             end do
          end do
       end do
    end if

    call append_plot3d_grid_ib_mpiio(grid%comm,3,grid%GlobalSize, is, ie, X, IBLANK, fname, fnamelen)

    deallocate(X)
    deallocate(IBLANK)

  end subroutine AppendSingleGrid_MPIIO

  subroutine write_single_soln_low_mem(grid, dbuf, tau, fname, fnamelen, offset)

    USE ModGlobal
    USE ModDataStruct
    USE ModMPI

    Implicit None

    Type(t_grid), Pointer :: grid
    Character(LEN=80) :: fname
    Integer :: fnamelen, offset
    Real(RFREAL), Dimension(:,:), Pointer :: dbuf
    Real(RFREAL) :: tau(4)

    ! ... local variables
    Real(rfreal), Dimension(:,:,:,:), Pointer :: X
    Integer, Dimension(:,:,:), Pointer :: IBLANK
    Character(LEN=2) :: prec, gf, vf, ib
    Integer :: ngrid, I, J, K, L0, Nx, Ny, Nz, M, ftype, dir, N(MAX_ND)
    Integer :: fh, filetype
    Integer :: is(MAX_ND), ie(MAX_ND), var(MAX_ND+2)

    ! ... extent of this grid (in C-counting)
    is(:) = 0; ie(:) = 0; N(:) = 1;
    do j = 1, grid%ND
       is(j) = grid%is_unique(j)-1
       ie(j) = grid%ie_unique(j)-1
       N(j) = ie(j) - is(j) + 1
    end do

    ! ... pointer
    Do m = 1, 5
      var(m) = m
    End Do

    If (grid%ND == 1) var(3) = 5
    If (grid%ND == 2) var(4) = 5

    ! ... allocate buffer
    Allocate(X(N(1),N(2),N(3),5)); ! X(:,:,:,:) = 0.0_rfreal

    ! ... pack buffer
    do k = 1, N(3)
       do j = 1, N(2)
          do i = 1, N(1)

             l0 = (k-1)*N(1)*N(2) + (j-1)*N(1) + i

             do m = 1, grid%ND+2
                X(i,j,k,var(m)) = dbuf(l0,m) 
             end do

          end do
       end do
    end do

    call write_plot3d_single_soln_whole_format_low_mem(grid%GlobalSize, offset, is, ie, X, tau, fname, fnamelen)

    deallocate(X)

  end subroutine write_single_soln_low_mem

  subroutine write_single_soln_mpiio(grid, dbuf, fname, fnamelen, offset)

    USE ModGlobal
    USE ModDataStruct
    USE ModMPI

    Implicit None

    Type(t_grid), Pointer :: grid
    Character(LEN=80) :: fname
    Integer :: fnamelen, offset
    Real(RFREAL), Dimension(:,:), Pointer :: dbuf

    ! ... local variables
    Real(rfreal), Dimension(:,:,:,:), Pointer :: X
    Integer, Dimension(:,:,:), Pointer :: IBLANK
    Character(LEN=2) :: prec, gf, vf, ib
    Integer :: ngrid, I, J, K, L0, Nx, Ny, Nz, M, ftype, dir, N(MAX_ND)
    Integer :: fh, filetype, ii, jj, kk, ierr
    Integer :: is(MAX_ND), ie(MAX_ND), var(MAX_ND+2)

    ! ... extent of this grid (in C-counting)
    is(:) = 0; ie(:) = 0; N(:) = 1;
    do j = 1, grid%ND
       is(j) = grid%is_unique(j)-1
       ie(j) = grid%ie_unique(j)-1
       N(j) = ie(j) - is(j) + 1
    end do

    ! ... pointer
    Do m = 1, 5
      var(m) = m
    End Do

    If (grid%ND == 1) var(3) = 5
    If (grid%ND == 2) var(4) = 5

    ! ... allocate buffer
    Allocate(X(N(1),N(2),N(3),5)); ! X(:,:,:,:) = 0.0_rfreal

    CALL MPI_Barrier(grid%comm,ierr)

    ! ... pack buffer
    if(grid%ND == 3) then
       do k = grid%is_unique(3), grid%ie_unique(3)
          do j = grid%is_unique(2), grid%ie_unique(2)
             do i = grid%is_unique(1), grid%ie_unique(1)
                
                ii = i - grid%is_unique(1) + 1
                jj = j - grid%is_unique(2) + 1
                kk = k - grid%is_unique(3) + 1

                l0 = (kk-1+grid%nGhostRHS(3,1))*(grid%ie(1)-grid%is(1)+1) &
                     *(grid%ie(2)-grid%is(2)+1) &
                     + (jj-1+grid%nGhostRHS(2,1))*(grid%ie(1)-grid%is(1)+1) &
                     + grid%nGhostRHS(1,1)+ii
                
                do m = 1, grid%ND+2
                   X(ii,jj,kk,var(m)) = dbuf(l0,m) 
                end do
                
             end do
          end do
       end do
    ELSE IF(grid%ND == 2) then
       do k = grid%is_unique(3),grid%ie_unique(3)
          do j = grid%is_unique(2),grid%ie_unique(2)
             do i = grid%is_unique(1), grid%ie_unique(1)
                
                ii = i - grid%is_unique(1) + 1
                jj = j - grid%is_unique(2) + 1
                kk = k - grid%is_unique(3) + 1

                l0 = (kk-1+grid%nGhostRHS(3,1))*(grid%ie(1)-grid%is(1)+1) &
                     *(grid%ie(2)-grid%is(2)+1) &
                     + (jj-1+grid%nGhostRHS(2,1))*(grid%ie(1)-grid%is(1)+1) &
                     + grid%nGhostRHS(1,1)+ii
                
                do m = 1, 3
                   X(ii,jj,kk,var(m)) = dbuf(l0,m) 
                end do

                X(ii,jj,kk,4) = 0.0_RFREAL
                X(ii,jj,kk,5) = dbuf(l0,4)
             end do
          end do
       end do
    ELSE IF(grid%ND == 1) then
       do k = grid%is_unique(3),grid%ie_unique(3)
          do j = grid%is_unique(2),grid%ie_unique(2)
             do i = grid%is_unique(1), grid%ie_unique(1)
                
                ii = i - grid%is_unique(1) + 1
                jj = j - grid%is_unique(2) + 1
                kk = k - grid%is_unique(3) + 1

                l0 = (kk-1+grid%nGhostRHS(3,1))*(grid%ie(1)-grid%is(1)+1) &
                     *(grid%ie(2)-grid%is(2)+1) &
                     + (jj-1+grid%nGhostRHS(2,1))*(grid%ie(1)-grid%is(1)+1) &
                     + grid%nGhostRHS(1,1)+ii
                
                do m = 1, 2
                   X(ii,jj,kk,var(m)) = dbuf(l0,m) 
                end do
                X(ii,jj,kk,3) = 0.0_RFREAL
                X(ii,jj,kk,4) = 0.0_RFREAL
                X(ii,jj,kk,5) = dbuf(l0,3)
             end do
          end do
       end do
    endif

    call write_plot3d_soln_mpiio(grid%comm,3,grid%GlobalSize, offset, is, ie, X, fname, fnamelen)

    deallocate(X)

  end subroutine write_single_soln_mpiio

  subroutine AppendSingleSolution_MPIIO(grid, dbuf, fname, fnamelen)

    USE ModGlobal
    USE ModDataStruct
    USE ModMPI

    Implicit None

    Type(t_grid), Pointer :: grid
    Character(LEN=80) :: fname
    Integer :: fnamelen, offset
    Real(RFREAL), Dimension(:,:), Pointer :: dbuf

    ! ... local variables
    Real(rfreal), Dimension(:,:,:,:), Pointer :: X
    Integer, Dimension(:,:,:), Pointer :: IBLANK
    Character(LEN=2) :: prec, gf, vf, ib
    Integer :: ngrid, I, J, K, L0, Nx, Ny, Nz, M, ftype, dir, N(MAX_ND)
    Integer :: fh, filetype, ii, jj, kk, ierr
    Integer :: is(MAX_ND), ie(MAX_ND), var(MAX_ND+2)

    ! ... extent of this grid (in C-counting)
    is(:) = 0; ie(:) = 0; N(:) = 1;
    do j = 1, grid%ND
       is(j) = grid%is_unique(j)-1
       ie(j) = grid%ie_unique(j)-1
       N(j) = ie(j) - is(j) + 1
    end do

    ! ... pointer
    Do m = 1, 5
      var(m) = m
    End Do

    If (grid%ND == 1) var(3) = 5
    If (grid%ND == 2) var(4) = 5

    ! ... allocate buffer
    Allocate(X(N(1),N(2),N(3),5)); ! X(:,:,:,:) = 0.0_rfreal

    CALL MPI_Barrier(grid%comm,ierr)

    ! ... pack buffer
    if(grid%ND == 3) then
       do k = grid%is_unique(3), grid%ie_unique(3)
          do j = grid%is_unique(2), grid%ie_unique(2)
             do i = grid%is_unique(1), grid%ie_unique(1)
                
                ii = i - grid%is_unique(1) + 1
                jj = j - grid%is_unique(2) + 1
                kk = k - grid%is_unique(3) + 1

                l0 = (kk-1+grid%nGhostRHS(3,1))*(grid%ie(1)-grid%is(1)+1) &
                     *(grid%ie(2)-grid%is(2)+1) &
                     + (jj-1+grid%nGhostRHS(2,1))*(grid%ie(1)-grid%is(1)+1) &
                     + grid%nGhostRHS(1,1)+ii
                
                do m = 1, grid%ND+2
                   X(ii,jj,kk,var(m)) = dbuf(l0,m) 
                end do
                
             end do
          end do
       end do
    ELSE IF(grid%ND == 2) then
       do k = grid%is_unique(3),grid%ie_unique(3)
          do j = grid%is_unique(2),grid%ie_unique(2)
             do i = grid%is_unique(1), grid%ie_unique(1)
                
                ii = i - grid%is_unique(1) + 1
                jj = j - grid%is_unique(2) + 1
                kk = k - grid%is_unique(3) + 1

                l0 = (kk-1+grid%nGhostRHS(3,1))*(grid%ie(1)-grid%is(1)+1) &
                     *(grid%ie(2)-grid%is(2)+1) &
                     + (jj-1+grid%nGhostRHS(2,1))*(grid%ie(1)-grid%is(1)+1) &
                     + grid%nGhostRHS(1,1)+ii
                
                do m = 1, 3
                   X(ii,jj,kk,var(m)) = dbuf(l0,m) 
                end do

                X(ii,jj,kk,4) = 0.0_RFREAL
                X(ii,jj,kk,5) = dbuf(l0,4)
             end do
          end do
       end do
    ELSE IF(grid%ND == 1) then
       do k = grid%is_unique(3),grid%ie_unique(3)
          do j = grid%is_unique(2),grid%ie_unique(2)
             do i = grid%is_unique(1), grid%ie_unique(1)
                
                ii = i - grid%is_unique(1) + 1
                jj = j - grid%is_unique(2) + 1
                kk = k - grid%is_unique(3) + 1

                l0 = (kk-1+grid%nGhostRHS(3,1))*(grid%ie(1)-grid%is(1)+1) &
                     *(grid%ie(2)-grid%is(2)+1) &
                     + (jj-1+grid%nGhostRHS(2,1))*(grid%ie(1)-grid%is(1)+1) &
                     + grid%nGhostRHS(1,1)+ii
                
                do m = 1, 2
                   X(ii,jj,kk,var(m)) = dbuf(l0,m) 
                end do
                X(ii,jj,kk,3) = 0.0_RFREAL
                X(ii,jj,kk,4) = 0.0_RFREAL
                X(ii,jj,kk,5) = dbuf(l0,3)
             end do
          end do
       end do
    endif

    call append_plot3d_soln_mpiio(grid%comm,3,grid%GlobalSize, is, ie, X, fname, fnamelen)

    deallocate(X)

  end subroutine AppendSingleSolution_MPIIO

!!$
  subroutine write_single_func_low_mem(grid, dbuf, fname, fnamelen, offset)

    USE ModGlobal
    USE ModDataStruct
    USE ModMPI

    Implicit None

    Type(t_grid), Pointer :: grid
    Character(LEN=80) :: fname
    Integer :: fnamelen, offset
    Real(RFREAL), Dimension(:,:), Pointer :: dbuf

    ! ... local variables
    Real(rfreal), Dimension(:,:,:,:), Pointer :: X
    Integer, Dimension(:,:,:), Pointer :: IBLANK
    Character(LEN=2) :: prec, gf, vf, ib
    Integer :: ngrid, I, J, K, L0, Nx, Ny, Nz, M, ftype, dir, N(MAX_ND)
    Integer :: fh, filetype
    Integer :: is(MAX_ND), ie(MAX_ND), nvar

    ! ... extent of this grid (in C-counting)
    is(:) = 0; ie(:) = 0; N(:) = 1;
    do j = 1, 3
       is(j) = grid%is_unique(j)-1
       ie(j) = grid%ie_unique(j)-1
       N(j) = ie(j) - is(j) + 1
    end do

    ! ... # of variables
    nvar = size(dbuf,2)

    ! ... allocate buffer
    Allocate(X(N(1),N(2),N(3),nvar)); ! X(:,:,:,:) = 0.0_rfreal

    ! ... pack buffer
    do k = grid%is(3), grid%ie(3)
       do j = grid%is(2), grid%ie(2)
          do i = grid%is(1), grid%ie(1)

             l0 = (k-grid%is(3))*N(1)*N(2) + (j-grid%is(2))*N(1) + i-grid%is(1)+1

             do m = 1, nvar
                X(i-grid%is(1)+1,j-grid%is(2)+1,k-grid%is(3)+1,m) = dbuf(l0,m) 
             end do

          end do
       end do
    end do

    call write_plot3d_single_func_whole_format_low_mem(grid%GlobalSize, offset, is, ie, nvar, X, fname, fnamelen)

    deallocate(X)

  end subroutine write_single_func_low_mem

  subroutine write_single_func_mpiio(grid, dbuf, fname, fnamelen, offset)

    USE ModGlobal
    USE ModDataStruct
    USE ModMPI

    Implicit None

    Type(t_grid), Pointer :: grid
    Character(LEN=80) :: fname
    Integer :: fnamelen, offset
    Real(RFREAL), Dimension(:,:), Pointer :: dbuf

    ! ... local variables
    Real(rfreal), Dimension(:,:,:,:), Pointer :: X
    Integer, Dimension(:,:,:), Pointer :: IBLANK
    Character(LEN=2) :: prec, gf, vf, ib
    Integer :: ngrid, I, J, K, L0, Nx, Ny, Nz, M, ftype, dir, N(MAX_ND)
    Integer :: fh, filetype, ii, jj, kk
    Integer :: is(MAX_ND), ie(MAX_ND), nvar

    ! ... extent of this grid (in C-counting)
    is(:) = 0; ie(:) = 0; N(:) = 1;
    do j = 1, grid%ND
       is(j) = grid%is_unique(j)-1
       ie(j) = grid%ie_unique(j)-1
       N(j) = ie(j) - is(j) + 1
    end do

    ! ... # of variables
    nvar = size(dbuf,2)

    ! ... allocate buffer
    Allocate(X(N(1),N(2),N(3),nvar)); ! X(:,:,:,:) = 0.0_rfreal

    ! ... pack buffer
    do k = grid%is_unique(3), grid%ie_unique(3)
       do j = grid%is_unique(2), grid%ie_unique(2)
          do i = grid%is_unique(1), grid%ie_unique(1)
             
             ii = i - grid%is_unique(1) + 1
             jj = j - grid%is_unique(2) + 1
             kk = k - grid%is_unique(3) + 1
             
             l0 = (kk-1+grid%nGhostRHS(3,1))*(grid%ie(1)-grid%is(1)+1) &
                  *(grid%ie(2)-grid%is(2)+1) &
                  + (jj-1+grid%nGhostRHS(2,1))*(grid%ie(1)-grid%is(1)+1) &
                  + grid%nGhostRHS(1,1)+ii
             
             do m = 1, nvar
                X(ii,jj,kk,m) = dbuf(l0,m) 
             end do
             
          end do
       end do
    end do

    call write_plot3d_func_mpiio(grid%comm,grid%ND,grid%GlobalSize, offset, is, ie, nvar, X, fname, fnamelen)

    deallocate(X)

  end subroutine write_single_func_mpiio

  SUBROUTINE AppendSingleFunction_MPIIO(grid, dbuf, fname, fnamelen)

    USE ModGlobal
    USE ModDataStruct
    USE ModMPI

    Implicit None

    Type(t_grid), Pointer :: grid
    Character(LEN=80) :: fname
    Integer :: fnamelen, offset
    Real(RFREAL), Dimension(:,:), Pointer :: dbuf

    ! ... local variables
    Real(rfreal), Dimension(:,:,:,:), Pointer :: X
    Integer, Dimension(:,:,:), Pointer :: IBLANK
    Character(LEN=2) :: prec, gf, vf, ib
    Integer :: ngrid, I, J, K, L0, Nx, Ny, Nz, M, ftype, dir, N(MAX_ND)
    Integer :: fh, filetype, ii, jj, kk
    Integer :: is(MAX_ND), ie(MAX_ND), nvar

    ! ... extent of this grid (in C-counting)
    is(:) = 0; ie(:) = 0; N(:) = 1;
    do j = 1, grid%ND
       is(j) = grid%is_unique(j)-1
       ie(j) = grid%ie_unique(j)-1
       N(j) = ie(j) - is(j) + 1
    end do

    ! ... # of variables
    nvar = size(dbuf,2)

    ! ... allocate buffer
    Allocate(X(N(1),N(2),N(3),nvar)); ! X(:,:,:,:) = 0.0_rfreal

    ! ... pack buffer
    do k = grid%is_unique(3), grid%ie_unique(3)
       do j = grid%is_unique(2), grid%ie_unique(2)
          do i = grid%is_unique(1), grid%ie_unique(1)
             
             ii = i - grid%is_unique(1) + 1
             jj = j - grid%is_unique(2) + 1
             kk = k - grid%is_unique(3) + 1
             
             l0 = (kk-1+grid%nGhostRHS(3,1))*(grid%ie(1)-grid%is(1)+1) &
                  *(grid%ie(2)-grid%is(2)+1) &
                  + (jj-1+grid%nGhostRHS(2,1))*(grid%ie(1)-grid%is(1)+1) &
                  + grid%nGhostRHS(1,1)+ii
             
             do m = 1, nvar
                X(ii,jj,kk,m) = dbuf(l0,m) 
             end do
             
          end do
       end do
    end do

    call append_plot3d_func_mpiio(grid%comm,grid%ND,grid%GlobalSize, is, ie, nvar, X, fname, fnamelen)

    deallocate(X)

  end subroutine AppendSingleFunction_MPIIO

  subroutine write_plot3d_file_low_mem_per_processor(region, var_to_write, iter_in)

    USE ModGlobal
    USE ModDataStruct
    USE ModMPI

    Implicit None

    type(t_region), pointer :: region
    character(len=3) :: var_to_write
    integer, optional :: iter_in

    ! ... local variables
    type(t_grid), pointer :: grid
    type(t_mixt), pointer :: state
    real(rfreal) :: tau(4)
    integer :: i, ng, numGridsInFile
    character(len=80) :: fname
    integer, parameter :: funit = 50
    integer :: fnamelen
    character(len=2) :: ib
    integer :: offset, NDIM, p, iter, ierr

    if (present(iter_in) .eqv. .true.) then
      iter = iter_in
    else
      iter = region%global%main_ts_loop_index
    end if

    ! ... intercept restart
    ! ... saves work for automated restarts
    if ( var_to_write(1:3) == 'rst' ) then
      call write_plot3d_file_low_mem(region, var_to_write, iter)
      return
    end if

    ! ... set the base filename
    write (fname(1:18),'(A10,I8.8)') 'RocFlo-CM.', iter
    if ( var_to_write(1:3) == 'cv ' ) then
      write(fname(19:20),'(A2)') '.q'
      fnamelen = 20
    ELSE IF ( var_to_write(1:3) == 'rst' ) then
      write(fname(19:26),'(A8)') '.restart'
      fnamelen = 26
    ELSE IF ( var_to_write(1:3) == 'cvt' ) then
      write(fname(19:27),'(A9)') '.target.q'
      fnamelen = 27
    ELSE IF ( var_to_write(1:3) == 'xyz' ) then
      write(fname(19:22),'(A4)') '.xyz'
      fnamelen = 22
    ELSE IF ( var_to_write(1:3) == 'met' ) then
      write(fname(19:28),'(A10)') '.metrics.q'
      fnamelen = 28
    ELSE IF ( var_to_write(1:3) == 'jac' ) then
      write(fname(19:24),'(A6)') '.jac.q'
      fnamelen = 24
    ELSE IF ( var_to_write(1:3) == 'rhs' ) then
      write(fname(19:24),'(A6)') '.rhs.q'
      fnamelen = 24
    ELSE IF ( var_to_write(1:3) == 'drv' ) then
      write(fname(19:24),'(A6)') '.drv.q'
      fnamelen = 24
    ELSE IF ( var_to_write(1:3) == 'mid' ) then
      write(fname(19:36),'(A18)') '.metric_identity.q'
      fnamelen = 36
    ELSE IF ( var_to_write(1:3) == 'men' ) then
      write(fname(19:25),'(A7)') '.mean.q'
      fnamelen = 25
    ELSE IF ( var_to_write(1:3) == 'dv ' ) then
      write(fname(19:23),'(A5)') '.dv.q'
      fnamelen = 23
    end if

    if (region%myrank == 0) Write (*,'(A)') 'PlasComCM: Writing "'//fname(1:fnamelen)//'".'

    ! ... Each processor writes it's own files
    ! ... naming convention : <BaseName>.processorID.gridID
    Do ng = 1, region%nGrids

      grid  => region%grid(ng)
      state => region%state(ng)

      ! ... update filename
      write (fname(fnamelen+1:fnamelen+12),'(A1,I5.5,A1,I5.5)') '.', grid%myrank_inComm, '.', grid%iGridGlobal
      fnamelen = fnamelen + 12

      ! ... tack on a directory
      if ( var_to_write(1:3) == 'met' .OR. var_to_write(1:3) == 'mid' .or. var_to_write(1:3) == 'jac' ) then
        fname = 'metrics/'//fname(1:fnamelen)
        fnamelen = fnamelen + 8
      ELSE IF ( var_to_write(1:3) == 'xyz' ) then
        fname = 'grid/'//fname(1:fnamelen)
        fnamelen = fnamelen + 5
      ELSE IF ( var_to_write(1:3) == 'cv ' ) then
        fname = 'soln/'//fname(1:fnamelen)
        fnamelen = fnamelen + 5
      ELSE IF ( var_to_write(1:3) == 'rhs' ) then
        fname = 'soln/'//fname(1:fnamelen)
        fnamelen = fnamelen + 5
      ELSE IF ( var_to_write(1:3) == 'cvt' ) then
        fname = 'target/'//fname(1:fnamelen)
        fnamelen = fnamelen + 7
      ELSE IF ( var_to_write(1:3) == 'drv' ) then
        fname = 'soln/'//fname(1:fnamelen)
        fnamelen = fnamelen + 5
      ELSE IF ( var_to_write(1:3) == 'dv ' ) then
        fname = 'soln/'//fname(1:fnamelen)
        fnamelen = fnamelen + 5
      end if

      ! ... all files begin the same
      open(unit=50, file=fname(1:fnamelen), form='unformatted', status='unknown')

      write (50) region%nGridsGlobal, grid%ND
      write (50) (grid%gridToProcMap(p,1:2),p=1,region%nGridsGlobal)
      write (50) (grid%AllGridsGlobalSize(p,1:3),p=1,region%nGridsGlobal)
      write (50) grid%iGridGlobal
      write (50) grid%is(1:3), grid%ie(1:3)
      write (50) real(region%global%main_ts_loop_index,rfreal), state%PR, state%RE, state%time(1)

      if ( var_to_write(1:3) == 'met' ) then

        write (50) grid%MT1

      ELSE IF ( var_to_write(1:3) == 'jac' ) then

        write (50) grid%JAC

      ELSE IF ( var_to_write(1:3) == 'mid' ) then

        write (50) grid%ident

      ELSE IF ( var_to_write(1:3) == 'xyz' ) then

        write (50) grid%XYZ
        write (50) grid%iblank

      ELSE IF ( var_to_write(1:3) == 'cv ' ) then

        write (50) state%cv

      ELSE IF ( var_to_write(1:3) == 'rhs' ) then

        write (50) state%rhs

      ELSE IF ( var_to_write(1:3) == 'cvt' ) then

        write (50) state%cvTarget

      ELSE IF ( var_to_write(1:3) == 'drv' ) then

        write (50) state%cv

      end if

      close (50)

    end do

    call mpi_barrier(mycomm, ierr)

    if (region%myrank == 0) then 
    
      Write (*,'(A)') 'PlasComCM: Done writing "'//fname(1:fnamelen)//'".'
    
      if(var_to_write(1:3) == 'cv ') then 
      
        open(unit=50, file='CurrentSolnIter.tmp', form='formatted', status='unknown')
        write(50,'(I0.8)') region%global%main_ts_loop_index
        close(50)
      end if 
    end if

  end subroutine write_plot3d_file_low_mem_per_processor

  subroutine read_single_func_low_mem(NDIM, ND, region, grid, grid_fname, dbuf, gridID, nvarIN)

    USE ModGlobal
    USE ModDataStruct
    USE ModMPI

    Implicit None

    Type(t_region), pointer :: region
    Type(t_grid), Pointer :: grid
    Real(rfreal), Dimension(:,:), Pointer :: dbuf
    Character(LEN=80) :: grid_fname
    integer :: gridID ! ... specify which grid we want to read, JKim 04/2008
    integer :: NDIM   ! ... dimension in grid file
    integer, Dimension(:,:), Pointer :: ND
    integer, optional, intent(IN) :: nvarIN

    ! ... local variables
    Real(rfreal), Dimension(:,:,:,:), Pointer :: X
    Character(LEN=2) :: prec, gf, vf, ib
    Integer :: ngrid, I, J, K, L0, Nx, Ny, Nz, M, ftype, dir, N(MAX_ND)
    Integer :: fh, filetype, p, nvars
    Integer :: offset, var(MAX_ND+2)
    Integer :: ibuf(1), is(MAX_ND), ie(MAX_ND)
    Integer, Parameter :: sizeof_int = 4, sizeof_dbl = 8

    ! ... number of variables
    If (present(nvarIN)) Then
       If (nvarIN > 0 .and. nvarIN<=region%input%nAuxVars) Then
          nvars = nvarIN
       Else
          nvars = region%input%nAuxVars
       End If
    Else
       nvars = region%input%nAuxVars
    End If
  
    ! ... seek to the current array from the beginning of the file
    call plot3d_funcfile_offset(gridID, NDIM, region%nGridsGlobal, grid%AllGridsGlobalSize, nvars, offset)
    
    ! ... extent of this grid (in C-counting)
    is = 0; ie = 0; N = 1;
    do j = 1, grid%ND
       is(j) = grid%is_unique(j)-1
       ie(j) = grid%ie_unique(j)-1
       N(j) = ie(j) - is(j) + 1
    end do

    ! ... allocate buffer
    allocate(X(N(1),N(2),N(3),nvars))

    ! ... read the file in binary mode
!    call read_plot3d_single_func_whole_format_low_mem(NDIM, grid%GlobalSize, offset, is, ie, gridID, nvars, X, grid_fname, len_trim(grid_fname))
    call read_plot3d_single_func_mpiio(grid%comm,NDIM, grid%GlobalSize, offset, is, ie, gridID, nvars, X, grid_fname, len_trim(grid_fname))

    ! ... unpack buffer
    do k = grid%is_unique(3), grid%ie_unique(3)
       do j = grid%is_unique(2), grid%ie_unique(2)
          do i = grid%is_unique(1), grid%ie_unique(1)
             do m = 1, ubound(dbuf,2)
                l0 = (k-grid%is_unique(3)+grid%nGhostRHS(3,1))*(grid%ie(1)-grid%is(1)+1) &
                     *(grid%ie(2)-grid%is(2)+1) &
                     + (j-grid%is_unique(2)+grid%nGhostRHS(2,1))*(grid%ie(1)-grid%is(1)+1) + grid%nGhostRHS(1,1)+i-grid%is_unique(1)+1
                if(m<=nvars) then
                  dbuf(l0,m) = X(i-grid%is_unique(1)+1,j-grid%is_unique(2)+1,k-grid%is_unique(3)+1,m)
                else
                  dbuf(l0,m) = 1d-12
                endif
                if(.not. abs( dbuf(l0,m)) < 1d10) print'(2i5,"Suspicious Value In read_single_func_low_mem",1pe12.4)',l0,m,dbuf(l0,m)
             end do
          end do
       end do
    end do

    deallocate(X)

  end subroutine read_single_func_low_mem

  subroutine write_plot3d_file_box(region, var_to_write, iter_in)

    USE ModGlobal
    USE ModDataStruct
    USE ModMPI

    Implicit None

    type(t_region), pointer :: region
    character(len=3) :: var_to_write
    integer, optional :: iter_in

    ! ... local variables
    type(t_grid), pointer :: grid
    type(t_mixt), pointer :: state
    real(rfreal) :: tau(4)
    integer :: i, ng, numGridsInFile, NDIM, iter
    character(len=80) :: fname
    integer :: funit = 50, fnamelen, ierr

    if (present(iter_in) .eqv. .true.) then
      iter = iter_in
    else
      iter = region%global%main_ts_loop_index
    end if

    ! ... return early if in scaling run
    !    if (region%input%caseID(1:7) == 'SCALING') return

!!$    if (var_to_write(1:3) == 'dv ' .and. myrank == 0) then
!!$      write (*,'(A,3(E20.8,1X))') 'CENTERLINE', region%state(1)%time(1), region%state(1)%dv(1,1), region%state(1)%dv(1,2)
!!$    end if

    If (region%input%useMPIIO == TRUE) then
      call write_plot3d_file_mpiio(region, var_to_write, iter)
      return
    end if

    if (region%input%useLowMemIO == TRUE) then
      call write_plot3d_file_low_mem(region, var_to_write, iter)
      return
    end if

    if (region%input%useLowMemIO == PROCESSOR_IO) then
      call write_plot3d_file_low_mem_per_processor(region, var_to_write, iter)
      return
    end if


    ! ... step 1
    ! ... set the filename
    write (fname(1:18),'(A10,I8.8)') 'RocFlo-CM.', iter
    if ( var_to_write(1:3) == 'cv ' ) then
      write(fname(19:20),'(A2)') '.q'
      fnamelen = 20
    ELSE IF ( var_to_write(1:3) == 'rst' ) then
      if (region%input%AdjNS) then
        write(fname(19:29),'(A11)') '.av.restart'
        fnamelen = 29
      else
        write(fname(19:26),'(A8)') '.restart'
        fnamelen = 26
      end if ! region%input%AdjNS
    ELSE IF ( var_to_write(1:3) == 'cvt' ) then
      write(fname(19:27),'(A9)') '.target.q'
      fnamelen = 27
    ELSE IF ( var_to_write(1:3) == 'xyz' ) then
      write(fname(19:22),'(A4)') '.xyz'
      fnamelen = 22
    ELSE IF ( var_to_write(1:3) == 'jac' ) then
      write(fname(19:24),'(A6)') '.jac.q'
      fnamelen = 24
    ELSE IF ( var_to_write(1:3) == 'rhs' ) then
      write(fname(19:24),'(A6)') '.rhs.q'
      fnamelen = 24
    ELSE IF ( var_to_write(1:3) == 'met' ) then
      write(fname(19:28),'(A10)') '.metrics.q'
      fnamelen = 28
    ELSE IF ( var_to_write(1:3) == 'av ' ) then ! ... Adjoint N-S
      write(fname(19:23),'(A5)') '.av.q'
      fnamelen = 23
    ELSE IF ( var_to_write(1:3) == 'avt' ) then ! ... Adjoint N-S
      write(fname(19:30),'(A12)') '.av.target.q'
      fnamelen = 30
    ELSE IF ( var_to_write(1:3) == 'ctr' ) then ! ... Control optimization
      write(fname(19:25),'(A7)') '.ctrl.q'
      fnamelen = 25
    ELSE IF ( var_to_write(1:3) == 'drv' ) then
      write(fname(19:24),'(A6)') '.drv.q'
      fnamelen = 24
    ELSE IF ( var_to_write(1:3) == 'aux' ) then
      write(fname(19:24),'(A6)') '.aux.q'
      fnamelen = 24
    ELSE IF ( var_to_write(1:3) == 'phi' ) then
      write(fname(19:24),'(A6)') '.phi.q'
      fnamelen = 24
    ELSE IF ( var_to_write(1:3) == 'dr ' ) then
      write(fname(19:23),'(A5)') '.dr.q'
      fnamelen = 23
    ELSE IF ( var_to_write(1:3) == 'dv ' ) then
      write(fname(19:23),'(A5)') '.dv.q'
      fnamelen = 23
    ELSE IF ( var_to_write(1:3) == 'mid' ) then
      write(fname(19:36),'(A18)') '.metric_identity.q'
      fnamelen = 36
    ELSE IF ( var_to_write(1:3) == 'men' ) then
      write(fname(19:25),'(A7)') '.mean.q'
      fnamelen = 25
    ELSE IF ( var_to_write(1:3) == 'pp ') then
      write (fname(1:30),'(A17,I8.8,A5)') './post/RocFlo-CM.', iter,'.pp.q'
      fnamelen = 30
    end if

    if (region%myrank == 0) Write (*,'(A)') 'PlasComCM: Writing "'//fname(1:fnamelen)//'".'

    ! ... step 2
    ! ... agglomerate decomposed data to core Rank for that grid
    !Call Agglomerate_Data_To_Core_Rank(region, var_to_write)
    Call Agglomerate_Data_To_Core_Rank_box(region, var_to_write)
    Call MPI_BARRIER(mycomm, IERR)

    ! ... step 3
    ! ... write agglomerated data
    Loop1: Do i = 1, region%grid(1)%numGridsInFile

      Loop2: Do ng = 1, region%nGrids

        grid => region%grid(ng)
        state => region%state(ng)

        if ( (grid%iGridGlobal == i) .and. (region%myrank == grid%coreRank_inComm) ) then

          tau(1) = real(region%global%main_ts_loop_index,rfreal)
          tau(2) = region%state(1)%t_output_next
          tau(3) = state%RE
          tau(4) = state%time(1)
          NDIM   = 3

          ! ... if first grid, write header
          if ( i == 1 ) then

            if ( var_to_write(1:3) == 'met' .or. var_to_write(1:3) == 'mid' .or. var_to_write(1:3) == 'ctr' &
                 .or. var_to_write(1:3) == 'pp ' .or. var_to_write(1:3) == 'aux'  .or. var_to_write(1:3) == 'phi' .or. var_to_write(1:3) == 'dv ' &
                 .or. var_to_write(1:3) == 'jac') then

              Call Write_Func_Header(NDIM, grid%numGridsInFile, grid%AllGridsGlobalSize, size(state%DBUF_IO,4), fname, fnamelen, funit)

            else

              Call Write_Grid_Soln_Header(NDIM, grid%numGridsInFile, grid%AllGridsGlobalSize, fname, fnamelen, funit)

            end if

          end if


          ! ... write grids
          if ( var_to_write(1:3) == 'cv ' ) CALL Write_Soln_Single_Grid(grid%ND, grid%GlobalSize, state%DBUF_IO, tau, fname, fnamelen, funit)

          if ( var_to_write(1:3) == 'rhs' ) CALL Write_Soln_Single_Grid(grid%ND, grid%GlobalSize, state%DBUF_IO, tau, fname, fnamelen, funit)

          if ( var_to_write(1:3) == 'cvt' ) CALL Write_Soln_Single_Grid(grid%ND, grid%GlobalSize, state%DBUF_IO, tau, fname, fnamelen, funit)

          if ( var_to_write(1:3) == 'rst' ) CALL Write_Soln_Single_Grid(grid%ND, grid%GlobalSize, state%DBUF_IO, tau, fname, fnamelen, funit)

          if ( var_to_write(1:3) == 'xyz' ) CALL Write_Grid_Single_Grid(grid%ND, grid%GlobalSize, state%DBUF_IO, state%IBUF_IO, fname, fnamelen, funit)

          if ( var_to_write(1:3) == 'met' ) CALL Write_Func_Single_Grid(grid%ND, grid%GlobalSize, state%DBUF_IO, fname, fnamelen, funit)

          if ( var_to_write(1:3) == 'jac' ) CALL Write_Func_Single_Grid(grid%ND, grid%GlobalSize, state%DBUF_IO, fname, fnamelen, funit)

          if ( var_to_write(1:3) == 'aux' ) CALL Write_Func_Single_Grid(grid%ND, grid%GlobalSize, state%DBUF_IO, fname, fnamelen, funit)

          if ( var_to_write(1:3) == 'phi' ) CALL Write_Func_Single_Grid(grid%ND, grid%GlobalSize, state%DBUF_IO, fname, fnamelen, funit)

          if ( var_to_write(1:3) == 'dv ' ) CALL Write_Func_Single_Grid(grid%ND, grid%GlobalSize, state%DBUF_IO, fname, fnamelen, funit)

          if ( var_to_write(1:3) == 'pp ' ) CALL Write_Func_Single_Grid(grid%ND, grid%GlobalSize, state%DBUF_IO, fname, fnamelen, funit)

          if ( var_to_write(1:3) == 'av ' ) CALL Write_Soln_Single_Grid(grid%ND, grid%GlobalSize, state%DBUF_IO, tau, fname, fnamelen, funit) ! ... Adjoint N-S

          if ( var_to_write(1:3) == 'avt' ) CALL Write_Soln_Single_Grid(grid%ND, grid%GlobalSize, state%DBUF_IO, tau, fname, fnamelen, funit) ! ... Adjoint N-S

          if ( var_to_write(1:3) == 'ctr' ) CALL Write_Func_Single_Grid(grid%ND, grid%GlobalSize, state%DBUF_IO, fname, fnamelen, funit) ! ... Control optimization

          if ( var_to_write(1:3) == 'drv' ) CALL Write_Soln_Single_Grid(grid%ND, grid%GlobalSize, state%DBUF_IO, tau, fname, fnamelen, funit)

          if ( var_to_write(1:3) == 'mid' ) CALL Write_Func_Single_Grid(grid%ND, grid%GlobalSize, state%DBUF_IO, fname, fnamelen, funit)

          if ( var_to_write(1:3) == 'men' ) CALL Write_Soln_Single_Grid(grid%ND, grid%GlobalSize, state%DBUF_IO, tau, fname, fnamelen, funit)

          ! ... clean-up
          deallocate(state%DBUF_IO)
          if ( var_to_write(1:3) == 'xyz' ) deallocate(state%IBUF_IO)

          EXIT Loop2

        end if

      End Do Loop2

      Call MPI_BARRIER(mycomm, ierr)

    End Do Loop1

    Call MPI_BARRIER(mycomm, ierr)

    if (region%myrank == 0) Write (*,'(A)') 'PlasComCM: Done writing "'//fname(1:fnamelen)//'".'

  end subroutine write_plot3d_file_box

End Module ModPLOT3D_IO
