! Copyright (c) 2015, University of Illinois at Urbana-Champaign, XPACC
! License: MIT, http://opensource.org/licenses/MIT
!----------------------------------------------------------------------
!
! ModNavierStokes.f90
! 
! - basic code for the evaluation of the Navier-Stokes equations
!
! Revision history
! - 19 Dec 2006 : DJB : initial code
! - 20 Dec 2006 : DJB : continued evolution
! - 20 Mar 2007 : DJB : initial working Poinsot-Lele BC on gen. meshes
! - 20 Apr 2007 : DJB : initial working Poinsot-Lele BC on moving meshes
! - 10 Jul 2007 : DJB : initial CVS submit
! - 11 Jul 2007 : DJB : initial work on ``multi-dimensional'' Poinsot-Lele
! - 01 Feb 2008 : DJB : merged in JKim's LES & 3D bc routines
! - 29 Jul 2008 : DJB : Initial SAT boundary conditions
! 
! flags == FALSE    : compute inviscid fluxes only
! flags == TRUE     : compute dissipative fluxes only
! flags == COMBINED : inviscid + dissipative fluxes
!
! $Header: /cvsroot/genx/Codes/RocfloCM/Source/ModNavierStokesRHS.f90,v 1.22 2011/09/26 21:12:14 bodony Exp $
!
!-----------------------------------------------------------------------
MODULE ModNavierStokesRHS

CONTAINS

  subroutine NS_RHS(region, ng, flags)

    USE ModGlobal
    USE ModDataStruct
    USE ModDeriv
    USE ModMPI
    USE ModFiniteVolume
    USE ModEulerFV


    Implicit None

    ! ... local variables
    type(t_grid), pointer :: grid
    type(t_mixt), pointer :: state
    type(t_mixt_input), pointer :: input
    type(t_region), pointer :: region
    type(t_mpi_timings), pointer :: mpi_timings
    real(rfreal) :: timer, timer2
    integer :: flags, ng, k, i

    ! ... full timer
    timer2 = MPI_Wtime()

    ! ... simplicity
    grid        => region%grid(ng)
    state       => region%state(ng)
    input       => grid%input
    mpi_timings => region%mpi_timings(ng)

    ! ... Inviscid Euler fluxes
    ! ... Time-dependent metric portion is also considered here.
    if (flags == INVISCID_ONLY .or. flags == COMBINED) then
      timer = MPI_WTIME()
      if (input%fv_grid(ng) == FINITE_DIF) then
        if (grid%advection_type == STANDARD_EULER) then
          CALL NS_RHS_Euler(region, ng, flags)
        else if (grid%advection_type == SKEW_SYMMETRIC) then
          CALL NS_RHS_Euler_SKEW(region, ng, flags) 
        end if
      else if (input%fv_grid(ng) == FINITE_VOL) then
        call NS_RHS_Euler_FV(region, grid, state)
      end if
      mpi_timings%rhs_inviscid = mpi_timings%rhs_inviscid + (MPI_WTIME() - timer)
    end if

    ! ... Viscous and heat transfer terms
    if (flags /= INVISCID_ONLY) then
      timer = MPI_WTIME()

      ! ... In inviscid shock-capturing or inviscid LES, state%REinv = 0.0 
      ! ... gets rid of hyperviscosity or LES contribution.  Having state%REinv = 0.0 
      ! ... is not correct in inviscid fluid, but since state%REinv is not used 
      ! ... below, we may conveniently manipulate its value to retain code 
      ! ... formalism, JKim 02/2009
      if (state%RE == 0.0_rfreal) then
        state%REinv = 1.0_rfreal; state%PRinv = 1.0_rfreal
      end if ! state%RE

      if (input%tvCor_in_dt == TRUE) then ! ... viscosity-based artificial dissipation
        if (grid%metric_type == NONORTHOGONAL_WEAKFORM) then
          call NS_RHS_ViscousTerms(region, ng, flags, state%VelGrad1st, state%TempGrad1st, state%tvCor) ! JKim 01/2008
        else if (grid%metric_type == NONORTHOGONAL_WEAKFORM_OPT) then
          call NS_RHS_ViscousTerms_OPT(region, ng, flags, state%VelGrad1st, state%TempGrad1st, state%tvCor) ! JKim 01/2008
        else if (grid%metric_type == CARTESIAN) then
          call NS_RHS_ViscousTerms_CARTESIAN(region, ng, flags, state%VelGrad1st, state%TempGrad1st, state%tvCor) ! JKim 01/2008
        else if (grid%metric_type == NONORTHOGONAL_STRONGFORM) then
          call NS_RHS_ViscousTerms_Strong(region, ng, flags, state%VelGrad1st, state%TempGrad1st, state%tvCor)
	else if (grid%metric_type == STRONGFORM_NARROW) then
          call NS_RHS_ViscousTerms_Narrow(region, ng, flags, state%VelGrad1st, state%TempGrad1st, state%tvCor)

        else
          call graceful_exit(region%myrank, 'ModNavierStokes: Illegal flag: grid%metric_type.')
        end if
      else if (state%RE > 0.0_rfreal) then ! ... or just a purely viscous simulation
        if (grid%metric_type == NONORTHOGONAL_WEAKFORM) then
          call NS_RHS_ViscousTerms(region, ng, flags, state%VelGrad1st, state%TempGrad1st)
        else if (grid%metric_type == NONORTHOGONAL_WEAKFORM_OPT) then
          call NS_RHS_ViscousTerms_OPT(region, ng, flags, state%VelGrad1st, state%TempGrad1st)
        else if (grid%metric_type == CARTESIAN) then
          call NS_RHS_ViscousTerms_CARTESIAN(region, ng, flags, state%VelGrad1st, state%TempGrad1st) ! JKim 01/2008
        else if (grid%metric_type == NONORTHOGONAL_STRONGFORM) then
          call NS_RHS_ViscousTerms_Strong(region, ng, flags, state%VelGrad1st, state%TempGrad1st)
	else if (grid%metric_type == STRONGFORM_NARROW) then
          call NS_RHS_ViscousTerms_Narrow(region, ng, flags, state%VelGrad1st, state%TempGrad1st)

        else

          call graceful_exit(region%myrank, 'ModNavierStokes: Illegal flag: grid%metric_type.')
        end if
      end if ! input%shock

      ! ... state%REinv should be recovered in inviscid limit, JKim 02/2009
      if (state%RE == 0.0_rfreal) then
        state%REinv = 0.0_rfreal; state%PRinv = 1.0_rfreal / state%PR
      end if ! state%RE

      mpi_timings%rhs_viscous = mpi_timings%rhs_viscous + (MPI_WTIME() - timer)

    end if

    if (input%sat_art_diss .eqv. .true.) call NS_SAT_Artificial_Dissipation(region, ng)

! Adding the mass source term which is proportional to 
! pressure fluctuation in the volumetric drag formulation
    If ( input%volumetric_drag .gt. 0) then
      Do i = 1, grid%nCells
        state%rhs(i,1) = state%rhs(i,1) - input%volumetric_drag_coefficient*(state%dv(i,1)-1.0/state%gv(i,1))
      End Do
    End If

    ! ... multiply the entire RHS by the jacobian
    timer = MPI_WTIME()
    do k = 1, input%nCv
      do i = 1, grid%nCells
        state%rhs(i,k) = state%rhs(i,k) * grid%JAC(i)
      end do
    end do
    do k = 1, input%nAuxVars
      do i = 1, grid%nCells
        state%rhs_auxVars(i,k) = state%rhs_auxVars(i,k) * grid%JAC(i)
      end do
    end do
    mpi_timings%rhs_inviscid = mpi_timings%rhs_inviscid + (MPI_WTIME() - timer)

    ! ... stop timer
    mpi_timings%rhs_total = mpi_timings%rhs_total + (MPI_WTime() - timer2)

    return

  end subroutine NS_RHS

  subroutine NS_Allocate_Memory(region, ng, flags, rkStep)

    USE ModGlobal
    USE ModDataStruct
    USE ModDeriv
    USE ModMPI

    Implicit None

    ! ... local variables
    type(t_grid), pointer :: grid
    type(t_mixt), pointer :: state
    type(t_mixt_input), pointer :: input
    type(t_region), pointer :: region
    integer :: flags, rkStep
    integer :: ND, Nc, N(MAX_ND), ng, j, alloc_stat(2)

    ! ... local variables
    integer :: l, m

    ! ... declarations for pointer dereference, WZhang 05/2014
    integer :: gridType, shock, LES, timeScheme
    integer, pointer :: is(:), ie(:)
    real(rfreal) :: RE
    real(rfreal), pointer :: cv(:,:)

    ! ... initialize alloc_stat
    alloc_stat(:) = 0

    ! ... simplicity
    grid  => region%grid(ng)
    state => region%state(ng)
    input => grid%input

    ! ... pointer dereference, WZhang 05/2014
    gridType   = input%gridType
    is        => grid%is
    ie        => grid%ie
    RE         = state%RE
    shock      = input%shock
    LES        = input%LES
    cv        => state%cv
    timeScheme = input%timeScheme

    ND = grid%ND
    Nc = grid%nCells
    N(:)  = 1
    do j = 1, ND
      N(j) = ie(j)-is(j)+1
    end do ! j

    ! ... allocate space for arrays
    if (.not.associated( state%flux) .eqv. .true.) allocate( state%flux(Nc), stat=alloc_stat(1));  state%flux(:) = 0.0_rfreal
    if (.not.associated(state%dflux) .eqv. .true.) allocate(state%dflux(Nc), stat=alloc_stat(2)); state%dflux(:) = 0.0_rfreal
    if (sum(alloc_stat(:)) /= 0)  call graceful_exit(region%myrank, 'Unable to allocate flux, dflux in NS_Allocate_Memory.')

    ! ... allocate space for arrays for
    ! ... physical viscous and heat transfer terms
    ! ... hyperviscosity/diffusivity shock-capturing scheme
    ! ... dynamic Smagorinsky LES model
    ! ... JKim 09/2007
    if (flags /= INVISCID_ONLY) then
      if ( RE > 0.0_rfreal .OR. &
           shock /= 0      .OR. &
           (LES >= 200 .AND. LES < 300)) then
        if (.not.associated(state%VelGrad1st) .eqv. .true.) then
          allocate(state%VelGrad1st(Nc,ND*ND)); state%VelGrad1st(:,:) = 0.0_rfreal
        end if
        if (.not.associated(state%TempGrad1st) .eqv. .true.) then
          allocate(state%TempGrad1st(Nc,ND)); state%TempGrad1st(:,:) = 0.0_rfreal
        end if
      end if ! state%RE
      if ( shock /= 0 .OR. &
           (LES >= 200 .AND. LES < 300)) then
        if (.not.associated(state%MagStrnRt) .eqv. .true.) then
          allocate(state%MagStrnRt(Nc)); state%MagStrnRt(:) = 0.0_rfreal
        end if
        if (.not.associated(state%tvCor) .eqv. .true.) then
          allocate(state%tvCor(Nc,input%nTv)); state%tvCor(:,:) = 0.0_rfreal
        end if
      end if ! input%shock
      ! ... to explicitly store eddy-viscosity, turbulent Prandtl number, 
      ! ... SGS kinetic energy, JKim 03/2009
      if (LES >= 200 .AND. LES < 300) then
        if (timeScheme /= EXPLICIT_RK4 .AND. timeScheme /= EXPLICIT_RK5) & 
             call graceful_exit(region%myrank, '... In LES, some operations depend on rkStep. Need to modify when it is not either RK4 or RK5.')
        if (rkStep == 1) then
          if (.not.associated(state%muT) .eqv. .true.) then
            allocate(state%muT(Nc)); state%muT(:) = 0.0_rfreal
          end if
          if (.not.associated(state%PrT) .eqv. .true.) then
            allocate(state%PrT(Nc)); state%PrT(:) = 0.0_rfreal
          end if
          if (.not.associated(state%SGS_KE) .eqv. .true.) then
            allocate(state%SGS_KE(Nc)); state%SGS_KE(:) = 0.0_rfreal
          end if
        end if ! rkStep
      end if ! input%LES
    end if

  end subroutine NS_Allocate_Memory

  subroutine NS_Deallocate_Memory(region, ng, flags, rkStep)

    USE ModGlobal
    USE ModDataStruct
    USE ModDeriv
    USE ModMPI

    Implicit None

    ! ... local variables
    type(t_grid), pointer :: grid
    type(t_mixt), pointer :: state
    type(t_mixt_input), pointer :: input
    type(t_region), pointer :: region
    integer :: flags, ng, rkStep

    ! ... simplicity
    grid  => region%grid(ng)
    state => region%state(ng)
    input => grid%input

    ! ... deallocate
    deallocate(state%flux, state%dflux)
    nullify(state%flux, state%dflux)
    if (flags /= INVISCID_ONLY) then
      if ( state%RE > 0.0_rfreal .OR. &
           input%shock /= 0      .OR. &
           (input%LES >= 200 .AND. input%LES < 300)) then
        deallocate(state%VelGrad1st, state%TempGrad1st)
        nullify(state%VelGrad1st, state%TempGrad1st)
      end if ! state%RE
      if ( input%shock /= 0 .OR. &
           (input%LES >= 200 .AND. input%LES < 300)) then
        deallocate(state%MagStrnRt, state%tvCor)
        nullify(state%MagStrnRt, state%tvCor)
      end if ! input%shock
      if (input%LES >= 200 .AND. input%LES < 300) then
        if ((input%timeScheme == EXPLICIT_RK4 .AND. rkStep == 4) .OR. &
             (input%timeScheme == EXPLICIT_RK5 .AND. rkStep == 2)) then ! ... JKim 05/2009
          deallocate(state%muT, state%PrT, state%SGS_KE)
          nullify(state%muT, state%PrT, state%SGS_KE)
        end if ! rkStep
      end if ! input%LES
    end if

  end subroutine NS_Deallocate_Memory

  subroutine NS_VelGradTensor(region, ng, flags)

    USE ModGlobal
    USE ModDataStruct
    USE ModDeriv
    USE ModMPI

    Implicit None

    ! ... local variables
    type(t_grid), pointer :: grid
    type(t_mixt), pointer :: state
    type(t_mixt_input), pointer :: input
    type(t_region), pointer :: region
    integer :: flags
    integer :: ng, i, j, k, l, m
    real(rfreal) :: timer

    ! ... start the timer
    timer = MPI_WTime()

    ! ... simplicity
    grid  => region%grid(ng)
    state => region%state(ng)
    input => grid%input

    ! ... Step 1
    ! ... 1st derivative of primitive velocities
    ! ... ignored in purely inviscid calculation
    ! ... JKim 11/2007
    if (flags /= INVISCID_ONLY) then
      if ( state%RE > 0.0_rfreal .OR. &
           input%shock /= 0      .OR. &
           (input%LES >= 200 .AND. input%LES < 300)) then
        do l = 1, grid%ND
          do i = 1, grid%nCells
            state%flux(i) = state%cv(i,region%global%vMap(l+1)) * state%dv(i,3) ! primitive velocity.
          end do
          do m = 1, grid%ND
            call APPLY_OPERATOR_box(region, ng, 1, m, state%flux, state%dflux, .FALSE., 0)
            do i = 1, grid%nCells
              state%VelGrad1st(i,region%global%t2Map(l,m)) = state%dflux(i) ! d(u_l)/d(xi_m) (i, m = 1 ~ 3)
            end do
          end do ! m
        end do ! l
      end if ! state%RE
    end if

    ! ... Step 2
    ! ... magnitude of strain-rate tensor
    ! ... used only for
    ! ... hyperviscosity/diffusivity shock-capturing scheme
    ! ... dynamic Smagorinsky LES model
    ! ... ignored in purely inviscid and purely viscous cases
    if (flags /= INVISCID_ONLY) then
      if ( input%shock /= 0 .OR. (input%LES >= 200 .AND. input%LES < 300)) then
        ! ... Note that there is a factor of root-two difference between Cook &
        ! ... Cabot (JCP 2005) and Moin et al. (PoF 1991) usages.  The subroutine
        ! ... NS_RHS_MagStrnRt below returns a value consistent to Cook & Cabot's
        ! ... definition, i.e. |S|= sqrt( S_ij by S_ij ), JKim 01/2008.
        call NS_RHS_MagStrnRt(region, grid, input, state%VelGrad1st, state%MagStrnRt)
      end if ! input%shock
    end if

    ! ... stop timer
    region%mpi_timings(ng)%rhs_vgrad = region%mpi_timings(ng)%rhs_vgrad + (MPI_WTime() - timer)

    return

  end subroutine NS_VelGradTensor

  subroutine NS_LES_Model(region, ng, flags, rkStep)

    USE ModGlobal
    USE ModDataStruct
    USE ModDeriv
    USE ModMPI

    Implicit None

    ! ... local variables
    type(t_grid), pointer :: grid
    type(t_mixt), pointer :: state
    type(t_mixt_input), pointer :: input
    type(t_region), pointer :: region
    type(t_patch), pointer :: patch
    integer :: flags
    real(rfreal), pointer :: flux(:), dflux(:)
    integer :: ND, Nc, N(MAX_ND), ng, i, j, k, Np(MAX_ND), patchID
    integer :: ip, jp, kp, rkStep
    real(rfreal) :: sqrt_two, inv_sqrt_two, timer

    ! ... local variables and arrays for viscous and heat fluxes, JKim 02/2007
    integer :: l,m

    ! ... timer
    timer = MPI_WTime()

    ! ... simplicity
    grid  => region%grid(ng)
    state => region%state(ng)
    input => grid%input

    if (flags /= INVISCID_ONLY) then

      SELECT CASE( input%LES )

      CASE( 0 ) ! DNS

! no SGS model, yeah :)

      CASE( 101 ) ! the classical Smagorinsky model

! not implemented; exists just for completeness
        call graceful_exit(region%myrank, '... ERROR: Smagorinsky model is not available, sorry lol.')

      CASE( 200:299 ) ! dynamic model of Moin et al.
! Lilly's improvement for solving system of equations
        if (.NOT. input%LES_ONCE .OR. (input%LES_ONCE .AND. rkStep == 1)) then
          sqrt_two = sqrt(2.0_rfreal); inv_sqrt_two = 1.0_rfreal / sqrt_two
          Do i = 1, grid%nCells
            state%MagStrnRt(i) = state%MagStrnRt(i) * sqrt_two ! to be consistent to Moin et al.
          End Do

! TempGrad1st is empty as of now.
          call NS_RHS_LES_Dynamic(region, ng, state%VelGrad1st, state%TempGrad1st, state%MagStrnRt, state%tvCor) ! JKim 01/2008
! TempGrad1st now stores a gradient of Favre-averaged temperature!

          Do i = 1, grid%nCells
            state%MagStrnRt(i) = state%MagStrnRt(i) * inv_sqrt_two ! to be consistent to the original
          End Do
! value computed in NS_RHS_MagStrnRt

        else
          call NS_RHS_LES_Dynamic_Retrieve(region, ng)

        end if ! input%LES_ONCE

      CASE DEFAULT

        call graceful_exit(region%myrank, 'PlasComCM: Wrong LES model; check input%LES.')

      END SELECT ! input%LES

    end if

    ! ... stop timer
    region%mpi_timings(ng)%rhs_les = region%mpi_timings(ng)%rhs_les + (MPI_WTIME() - timer)

  end subroutine NS_LES_Model

  subroutine NS_Shock_Capture(region, ng, flags, rkStep)

    USE ModGlobal
    USE ModDataStruct
    USE ModDeriv
    USE ModMPI

    Implicit None

    ! ... local variables
    type(t_grid), pointer :: grid
    type(t_mixt), pointer :: state
    type(t_mixt_input), pointer :: input
    type(t_region), pointer :: region
    type(t_patch), pointer :: patch
    integer :: flags, rkStep
    real(rfreal), pointer :: flux(:), dflux(:)
    integer :: ND, Nc, N(MAX_ND), ng, i, j, k, Np(MAX_ND), patchID
    integer :: ip, jp, kp
    real(rfreal) :: timer

    ! ... timer
    timer = MPI_WTime()

    ! ... simplicity
    grid  => region%grid(ng)
    state => region%state(ng)
    input => grid%input

    ! ... apply shock-capturing scheme if needed
    ! ... When hyperviscosity/diffusivity shock-capturing scheme is incorporated
    ! ... with dynamic LES model, LES updates thermodynamic variables.  So code
    ! ... should run LES first and then shock-capturing because some of thermo-
    ! ... dynamic variables are used in hyperviscosity/diffusivity-type shock-
    ! ... capturing scheme.
    ! ... JKim 11/2007
    if (flags /= INVISCID_ONLY) then

      SELECT CASE( input%shock )

      CASE( 0 ) ! shock-capturing scheme is disabled

! do nothing, yeah :)

      CASE ( 1 ) ! Updated Kawai & Lele (See Kawai, Shankar, Lele, JCP, 2010)

        call NS_RHS_Shock_Updated_Kawai_Lele(region, ng)

      CASE DEFAULT

        call graceful_exit(region%myrank, 'Wrong shock-capturing scheme; check input%shock.')

      END SELECT ! input%shock

    end if

    ! ... stop timer
    region%mpi_timings(ng)%rhs_shock = region%mpi_timings(ng)%rhs_shock + (MPI_WTIME() - timer)

  end subroutine NS_Shock_Capture

  subroutine NS_Temperature_Gradient(region, ng, flags, rkStep)

    USE ModGlobal
    USE ModDataStruct
    USE ModDeriv
    USE ModMPI

    Implicit None

    ! ... local variables
    type(t_grid), pointer :: grid
    type(t_mixt), pointer :: state
    type(t_mixt_input), pointer :: input
    type(t_region), pointer :: region
    type(t_patch), pointer :: patch
    integer :: flags, rkStep
    real(rfreal), pointer :: flux(:), dflux(:)
    integer :: ND, Nc, N(MAX_ND), ng, i, j, k, Np(MAX_ND), patchID
    integer :: ip, jp, kp
    real(rfreal) :: timer
    integer :: l,m

    ! ... timer
    timer = MPI_WTime()

    ! ... simplicity
    grid  => region%grid(ng)
    state => region%state(ng)
    input => grid%input

    ! ... 1st derivative of temperature
    ! ... ignored in purely inviscid calculation and dynamic LES model
    ! ... When using dynamic LES model, temperature gradient is computed inside
    ! ... of LES subroutine since the model requires it to compute turbulent
    ! ... Prandtl number.
    if (flags /= INVISCID_ONLY) then
      ! ... return when temperature gradient is already computed in LES subroutine
      if (input%LES >= 200 .AND. input%LES < 300) then
        if (.NOT. input%LES_ONCE .OR. &
             (input%LES_ONCE .AND. rkStep == 1)) return
      end if ! input%LES

      if ( state%RE > 0.0_rfreal .OR. &
           input%shock /= 0      .OR. &
           (input%LES >= 200 .AND. input%LES < 300)) then
        do l = 1, grid%ND
          do i = 1, grid%nCells
            state%flux(i) = state%dv(i,2) ! temperature.
          end do ! i
          call APPLY_OPERATOR_box(region, ng, 1, l, state%flux, state%dflux, .FALSE., 0)
          do i = 1, grid%nCells
            state%TempGrad1st(i,l) = state%dflux(i) ! dT/d(xi_l) (l = 1 ~ 3)
          end do
        end do ! l
      end if ! state%RE
    end if

    ! ... stop timer
    region%mpi_timings(ng)%rhs_TGrad = region%mpi_timings(ng)%rhs_TGrad + (MPI_WTIME() - timer)

  end subroutine NS_Temperature_Gradient

  subroutine NS_RHS_StrnRt(region, grid, VelGrad1st, StrnRt)

!   optimized by John L. Larson 8/12/2014

    USE ModGlobal
    USE ModDataStruct

    Implicit None

    ! ... subroutine arguments
    real(rfreal), pointer :: VelGrad1st(:,:), StrnRt(:,:)
    type(t_grid), pointer :: grid
    type(t_region), pointer :: region

    ! ... local variables
    integer :: i, j, k, ii
    integer :: ND

    ! ... declarations for pointer dereference, WZhang 05/2014
    real(rfreal), pointer :: MT1(:,:), JAC(:)

    integer Nc

    ! ... simplicity
    ND = grid%ND

    ! ... pointer dereference, WZhang 05/2014
    Nc   = grid%nCells
#ifdef AXISYMMETRIC
    MT1 => grid%MT0
    JAC => grid%JAC0
#else
    MT1 => grid%MT1
    JAC => grid%JAC
#endif

    ! ... initialize

    if( ND == 2 ) then               ! size(StrnRt,2) = 3
#ifdef ENABLE_SIMD
!DIR$ SIMD
#endif
      do ii = 1, Nc
! diagonal components first
        StrnRt(ii,1) =   JAC(ii)   * (                &
             MT1(ii,1) * VelGrad1st(ii,1) &
             + MT1(ii,2) * VelGrad1st(ii,3) )

        StrnRt(ii,2) =   JAC(ii)   * (                &
             MT1(ii,3) * VelGrad1st(ii,2) &
             + MT1(ii,4) * VelGrad1st(ii,4) )

! upper-half part of strain-rate tensor due to symmetry
        StrnRt(ii,3) =   JAC(ii)   * 0.5_rfreal * (   &
             MT1(ii,3) * VelGrad1st(ii,1) &
             + MT1(ii,1) * VelGrad1st(ii,2) &
             + MT1(ii,4) * VelGrad1st(ii,3) &
             + MT1(ii,2) * VelGrad1st(ii,4) )
      end do

    else if( ND == 3 ) then          ! size(StrnRt,2) = 6
#ifdef ENABLE_SIMD
!DIR$ SIMD
#endif
      do ii = 1, Nc
! diagonal components first
        StrnRt(ii,1) =   JAC(ii)   * (                &
             MT1(ii,1) * VelGrad1st(ii,1) &
             + MT1(ii,2) * VelGrad1st(ii,4) &
             + MT1(ii,3) * VelGrad1st(ii,7) )

! upper-half part of strain-rate tensor due to symmetry
        StrnRt(ii,4) =   JAC(ii)   * 0.5_rfreal * (   &
             MT1(ii,4) * VelGrad1st(ii,1) &
             + MT1(ii,1) * VelGrad1st(ii,2) &
             + MT1(ii,5) * VelGrad1st(ii,4) &
             + MT1(ii,2) * VelGrad1st(ii,5) &
             + MT1(ii,6) * VelGrad1st(ii,7) &
             + MT1(ii,3) * VelGrad1st(ii,8) )
      end do
#ifdef ENABLE_SIMD
!DIR$ SIMD
#endif
      do ii = 1, Nc

        StrnRt(ii,2) =   JAC(ii)   * (                &
             MT1(ii,4) * VelGrad1st(ii,2) &
             + MT1(ii,5) * VelGrad1st(ii,5) &
             + MT1(ii,6) * VelGrad1st(ii,8) )

        StrnRt(ii,6) =   JAC(ii)   * 0.5_rfreal * (   &
             MT1(ii,7) * VelGrad1st(ii,2) &
             + MT1(ii,4) * VelGrad1st(ii,3) &
             + MT1(ii,8) * VelGrad1st(ii,5) &
             + MT1(ii,5) * VelGrad1st(ii,6) &
             + MT1(ii,9) * VelGrad1st(ii,8) &
             + MT1(ii,6) * VelGrad1st(ii,9) )
      end do
#ifdef ENABLE_SIMD
!DIR$ SIMD
#endif
      do ii = 1, Nc

        StrnRt(ii,3) =   JAC(ii)   * (                &
             MT1(ii,7) * VelGrad1st(ii,3) &
             + MT1(ii,8) * VelGrad1st(ii,6) &
             + MT1(ii,9) * VelGrad1st(ii,9) )

        StrnRt(ii,5) =   JAC(ii)   * 0.5_rfreal * (   &
             MT1(ii,7) * VelGrad1st(ii,1) &
             + MT1(ii,1) * VelGrad1st(ii,3) &
             + MT1(ii,8) * VelGrad1st(ii,4) &
             + MT1(ii,2) * VelGrad1st(ii,6) &
             + MT1(ii,9) * VelGrad1st(ii,7) &
             + MT1(ii,3) * VelGrad1st(ii,9) )
      end do

    end if

  end subroutine NS_RHS_StrnRt

  subroutine NS_RHS_MagStrnRt(region, grid, input, VelGrad1st, MagStrnRt)

    USE ModGlobal
    USE ModDataStruct

    Implicit None

    ! ... subroutine arguments
    real(rfreal), pointer :: VelGrad1st(:,:), MagStrnRt(:)
    type(t_region), pointer :: region
    type(t_grid), pointer :: grid
    type(t_mixt_input), pointer :: input

    ! ... local variables
    integer :: i, j, k, ii
    integer :: ND, Nc
    real(rfreal), pointer :: junk(:)

    ! ... simplicity
    ND = grid%ND
    Nc = grid%nCells

    allocate(junk(Nc));
    Do ii = 1, Nc
      MagStrnRt(ii) = 0.0_rfreal ! defensive programming
    End Do

    ! ... This subroutine computes a magnitude of strain-rate tensor defined by
    ! ... sqrt(S_ij by S_ij) (i and j are summation indices).  It doesn't
    ! ... explicitly compute strain-rate tensor but expands all terms and makes
    ! ... use of velocity gradient tensor.

    do i = 1, ND ! diagonal component contribution
      do ii = 1, Nc
        junk(ii) = 0.0_rfreal
      end do
      do k = 1, ND
        do ii = 1, Nc
          junk(ii) = junk(ii) + grid%MT1(ii,region%global%t2Map(k,i)) * VelGrad1st(ii,region%global%t2Map(i,k))
        end do
      end do ! k
      do ii = 1, Nc
        MagStrnRt(ii) = MagStrnRt(ii) + junk(ii) * junk(ii)
      end do
    end do ! i
    do i = 1,ND ! off-diagonal component contribution
      do j = i+1,ND ! upper-triangle components only due to tensor symmetry
        do ii = 1, Nc
          junk(ii) = 0.0_rfreal
        end do
        do k = 1,ND
          do ii = 1, Nc
            junk(ii) = junk(ii) + grid%MT1(ii,region%global%t2Map(k,j)) * VelGrad1st(ii,region%global%t2Map(i,k)) +  &
                 grid%MT1(ii,region%global%t2Map(k,i)) * VelGrad1st(ii,region%global%t2Map(j,k))
          end do
        end do ! k
        do ii = 1, Nc
          MagStrnRt(ii) = MagStrnRt(ii) + 2.0_rfreal * (0.5_rfreal * junk(ii)) * (0.5_rfreal * junk(ii))
        end do
      end do ! j
    end do ! i
    Do ii = 1, Nc
      MagStrnRt(ii) = grid%JAC(ii) * sqrt(MagStrnRt(ii))
    End Do

    deallocate(junk)

    Return

  end subroutine NS_RHS_MagStrnRt

  subroutine NS_RHS_Euler(region, ng, flags)

    USE ModGlobal
    USE ModDataStruct
    USE ModDeriv
    USE ModMPI

    Implicit None

    ! ... local variables
    type(t_grid), pointer :: grid
    type(t_mixt), pointer :: state
    type(t_mixt_input), pointer :: input
    type(t_region), pointer :: region
    type(t_patch), pointer :: patch
    integer :: flags
    real(rfreal), allocatable :: UVWhat(:,:)
    integer :: ND, Nc, N(MAX_ND), ng, i, j, k, Np(MAX_ND), patchID
    integer :: ip, jp, kp, ii
    real(rfreal) :: source, norm(MAX_ND)

    ! ... declarations for pointer dereference, WZhang 05/2014
    integer :: moving, metric_type, iFirstDeriv, nAuxVars, useMetricIdentities
    integer, pointer :: is(:), ie(:), t2Map(:,:)
    real(rfreal), pointer :: rhs(:,:), cv(:,:), INVJAC_TAU(:), MT1(:,:), dv(:,:), XI_TAU(:,:)
    real(rfreal), pointer :: flux(:), dflux(:), auxVars(:,:), rhs_auxVars(:,:), XYZ(:,:), time(:)
    real(rfreal), pointer :: INVJAC(:), gv(:,:), ident(:,:)
    real(rfreal) :: Froude, gravity_angle_phi, gravity_angle_theta, invFroude
#ifdef AXISYMMETRIC
    real(rfreal), pointer :: MT0(:,:),JAC0(:)
#endif

    integer :: ip1, NDp2, t2Mapji
    real(rfreal) :: normj

    ! ... simplicity
    grid  => region%grid(ng)
    state => region%state(ng)
    input => grid%input
    ND = grid%ND
    Nc = grid%nCells
    N  = 1

!   optimized by John L. Larson 7/17/2014

    NDp2 = ND + 2

    ! ... pointer dereference, WZhang 05/2014
    is          => grid%is
    ie          => grid%ie
    moving       = grid%moving
    rhs         => state%rhs
    cv          => state%cv
    INVJAC_TAU  => grid%INVJAC_TAU
    metric_type  = grid%metric_type
    MT1         => grid%MT1
    dv          => state%dv
    XI_TAU      => grid%XI_TAU
    flux        => state%flux
    dflux       => state%dflux
    iFirstDeriv  = input%iFirstDeriv
    nAuxVars     = input%nAuxVars
    auxVars     => state%auxVars
    rhs_auxVars => state%rhs_auxVars
    XYZ         => grid%XYZ
    time        => state%time
    INVJAC      => grid%INVJAC
    gv          => state%gv
    ident       => grid%ident
    Froude       = input%Froude
    invFroude    = input%invFroude
    useMetricIdentities = input%useMetricIdentities
    gravity_angle_phi   = input%gravity_angle_phi
    gravity_angle_theta = input%gravity_angle_theta
    t2Map              => region%global%t2Map
#ifdef AXISYMMETRIC
    JAC0     => grid%JAC0
    MT0         => grid%MT0
#endif

    do j = 1, ND
      N(j) = ie(j)-is(j)+1
    end do ! j

    ! ... allocate space for arrays
    allocate( UVWhat(Nc,ND) );

    ! ... subtract out the time-dependent metric portion
    if (moving == TRUE) then

      if( ND == 2 ) then         !  size(rhs,2) = ND + 2 = 4
#ifdef ENABLE_SIMD
!DIR$ SIMD
#endif
        do ii = 1, Nc
          rhs(ii,1) = rhs(ii,1) - cv(ii,1) * INVJAC_TAU(ii)
          rhs(ii,2) = rhs(ii,2) - cv(ii,2) * INVJAC_TAU(ii)
          rhs(ii,3) = rhs(ii,3) - cv(ii,3) * INVJAC_TAU(ii)
          rhs(ii,4) = rhs(ii,4) - cv(ii,4) * INVJAC_TAU(ii)
        end do

      else if ( ND == 3 ) then   !  size(rhs,2) = ND + 2 = 5
#ifdef ENABLE_SIMD
!DIR$ SIMD
#endif
        do ii = 1, Nc
          rhs(ii,1) = rhs(ii,1) - cv(ii,1) * INVJAC_TAU(ii)
          rhs(ii,2) = rhs(ii,2) - cv(ii,2) * INVJAC_TAU(ii)
          rhs(ii,3) = rhs(ii,3) - cv(ii,3) * INVJAC_TAU(ii)
          rhs(ii,4) = rhs(ii,4) - cv(ii,4) * INVJAC_TAU(ii)
          rhs(ii,5) = rhs(ii,5) - cv(ii,5) * INVJAC_TAU(ii)
        end do

      end if ! ND

    end if ! moving

    ! ... compute the contravariant velocities
    if (metric_type /= CARTESIAN)  then

      if( ND == 2 ) then
#ifdef ENABLE_SIMD
!DIR$ SIMD
#endif
        do ii = 1, Nc

          UVWhat(ii,1) =  ( MT1(ii,1) * cv(ii,2)   &  !  JKim 01/2008
               + MT1(ii,3) * cv(ii,3) ) &  !  JKim 01/2008
               * dv(ii,3) + XI_TAU(ii,1)

          UVWhat(ii,2) =  ( MT1(ii,2) * cv(ii,2)   &  !  JKim 01/2008
               + MT1(ii,4) * cv(ii,3) ) &  !  JKim 01/2008
               * dv(ii,3) + XI_TAU(ii,2)
        end do

      else if( ND == 3 ) then
#ifdef ENABLE_SIMD
!DIR$ SIMD
#endif
        do ii = 1, Nc

          UVWhat(ii,1) =  ( MT1(ii,1) * cv(ii,2)   &  !  JKim 01/2008
               + MT1(ii,4) * cv(ii,3)   &  !  JKim 01/2008
               + MT1(ii,7) * cv(ii,4) ) &  !  JKim 01/2008
               * dv(ii,3) + XI_TAU(ii,1)

          UVWhat(ii,2) =  ( MT1(ii,2) * cv(ii,2)   &  !  JKim 01/2008
               + MT1(ii,5) * cv(ii,3)   &  !  JKim 01/2008
               + MT1(ii,8) * cv(ii,4) ) &  !  JKim 01/2008
               * dv(ii,3) + XI_TAU(ii,2)

          UVWhat(ii,3) =  ( MT1(ii,3) * cv(ii,2)   &  !  JKim 01/2008
               + MT1(ii,6) * cv(ii,3)   &  !  JKim 01/2008
               + MT1(ii,9) * cv(ii,4) ) &  !  JKim 01/2008
               * dv(ii,3) + XI_TAU(ii,3)
        end do

      end if ! ND

    else

      if( ND == 2 ) then
#ifdef ENABLE_SIMD
!DIR$ SIMD
#endif
        do ii = 1, Nc
          UVWhat(ii,1) = MT1(ii,1) * cv(ii,2) * dv(ii,3) + XI_TAU(ii,1)
          UVWhat(ii,2) = MT1(ii,4) * cv(ii,3) * dv(ii,3) + XI_TAU(ii,2)
        end do

      else if( ND == 3 ) then

#ifdef ENABLE_SIMD
!DIR$ SIMD
#endif
        do ii = 1, Nc
          UVWhat(ii,1) = MT1(ii,1) * cv(ii,2) * dv(ii,3) + XI_TAU(ii,1)
          UVWhat(ii,2) = MT1(ii,5) * cv(ii,3) * dv(ii,3) + XI_TAU(ii,2)
          UVWhat(ii,3) = MT1(ii,9) * cv(ii,4) * dv(ii,3) + XI_TAU(ii,3)
        end do

      end if ! ND

    end if ! cartesian

    ! ... inviscid fluxes

    ! ... continuity 

    do i = 1, ND
#ifdef ENABLE_SIMD
!DIR$ SIMD
#endif
      do ii = 1, Nc
        flux(ii) = cv(ii,1) * UVWhat(ii,i)
      end do
      call APPLY_OPERATOR_box(region, ng, iFirstDeriv, i, flux, dflux, .FALSE., 0)
      do ii = 1, Nc
#ifdef AXISYMMETRIC
        if(ND == 2 .and. i == 1  .and. grid%XYZ(ii,1) < grid%DRmin) dflux(ii)=0d0
#endif
        rhs(ii,1) = rhs(ii,1) - dflux(ii)
      end do
      Call Save_Patch_Deriv(region, ng, 1, i, dflux)
    end do ! i

    
#ifndef AXISYMMETRIC
    ! ... momentum
    do i = 1, ND
      ip1 = i+1
      do j = 1, ND
        t2Mapji = t2Map(j,i)
#ifdef ENABLE_SIMD
!DIR$ SIMD
#endif
        do ii = 1, Nc
          flux(ii) = cv(ii,ip1) * UVWhat(ii,j) + MT1(ii,t2Mapji) * dv(ii,1)
        end do
        call APPLY_OPERATOR_box(region, ng, iFirstDeriv, j, flux, dflux, .FALSE., 0)
#ifdef ENABLE_SIMD
!DIR$ SIMD
#endif
        do ii = 1, Nc
          rhs(ii,ip1) = rhs(ii,ip1) - dflux(ii)
        end do
        Call Save_Patch_Deriv(region, ng, i+1, j, dflux)
      end do ! j
    end do ! i
#else
    ! ... momentum
    do i = 1, ND
      ip1 = i+1
      do j = 1, ND
        t2Mapji = t2Map(j,i)
#ifdef ENABLE_SIMD
!DIR$ SIMD
#endif
        do ii = 1, Nc
          flux(ii) = cv(ii,ip1) * UVWhat(ii,j)
        end do
        call APPLY_OPERATOR_box(region, ng, iFirstDeriv, j, flux, dflux, .FALSE., 0)
        do ii = 1, Nc
          if(ND == 2 .and. j == 1  .and. grid%XYZ(ii,1) < grid%DRmin) dflux(ii)=0d0
          rhs(ii,ip1) = rhs(ii,ip1) - dflux(ii)
        end do
        Call Save_Patch_Deriv(region, ng, i+1, j, dflux)
        do ii = 1, Nc
           flux(ii) =  MT0(ii,t2Mapji) * (dv(ii,1) - 1d0/input%GamRef)
        end do
        call APPLY_OPERATOR_box(region, ng, iFirstDeriv, j, flux, dflux, .FALSE., 0)
        do ii = 1, Nc
           dflux(ii) =  dflux(ii)*JAC0(ii)*INVJAC(ii)
        end do
        do ii = 1, Nc
          rhs(ii,ip1) = rhs(ii,ip1) - dflux(ii)
        end do
      end do ! j
      
    end do ! i
#endif

    ! ... energy
    do i = 1, ND
#ifdef ENABLE_SIMD
!DIR$ SIMD
#endif
      do ii = 1, Nc
        flux(ii) = (cv(ii,NDp2) + dv(ii,1)) * UVWhat(ii,i) - XI_TAU(ii,i) * dv(ii,1)
      end do
      call APPLY_OPERATOR_box(region, ng, iFirstDeriv, i, flux, dflux, .FALSE., 0)
      do ii = 1, Nc
#ifdef AXISYMMETRIC
        if(ND == 2 .and. i == 1  .and. grid%XYZ(ii,1) < grid%DRmin) dflux(ii)=0d0
#endif
        rhs(ii,NDp2) = rhs(ii,NDp2) - dflux(ii)
      end do
      Call Save_Patch_Deriv(region, ng, NDp2, i, dflux)
    end do ! i

    ! ... scalar advection
    do k = 1, nAuxVars
      do i = 1, ND
#ifdef ENABLE_SIMD
!DIR$ SIMD
#endif
        do ii = 1, Nc
          flux(ii) = auxVars(ii,k) * UVWhat(ii,i)
        end do
        call APPLY_OPERATOR_box(region, ng, iFirstDeriv, i, flux, dflux, .FALSE., 0)
        do ii = 1, Nc
#ifdef AXISYMMETRIC
          if(ND == 2 .and. i == 1  .and. grid%XYZ(ii,1) < grid%DRmin) dflux(ii)=0d0
#endif
          rhs_auxVars(ii,k) = rhs_auxVars(ii,k) - dflux(ii)
        end do
      end do ! i
      
    end do ! k

#ifdef AXISYMMETRIC
    Call NS_RHS_Axisymmetric_Hopital(ng, Nc, nAuxVars, ND, iFirstDeriv, region, grid, input, flux, dflux, MT0, INVJAC_TAU, JAC0, INVJAC, rhs, rhs_auxVars, cv, dv, auxVars, 0)
#endif

    ! ... deallocate
    deallocate( UVWhat )

! CAA Benchmark 2, Category 1, Problem 1
    if (.false.) then
#ifdef ENABLE_SIMD
!DIR$ SIMD
#endif
      do i = 1, Nc
        source = 0.1_dp * exp(-(log(2.0_dp))*( ((XYZ(i,1)-4.0_dp)**2+XYZ(i,2)**2)/(0.4_dp)**2 )) &
             * dsin(4.0_dp * TWOPI * time(i)) * INVJAC(i)
        rhs(i,4) = rhs(i,4) + source / (gv(i,1)-1.0_dp)
      end do
    end if

    ! ... subtract off (violation of) metric identity

    if (useMetricIdentities == TRUE) then

      if( ND == 2 ) then

#ifdef ENABLE_SIMD
!DIR$ SIMD
#endif
        do ii = 1, Nc
          rhs(ii,1)  = rhs(ii,1)    + cv(ii,2) * ident(ii,1) &
               + cv(ii,3) * ident(ii,2)
          rhs(ii,2)  = rhs(ii,2)    + dv(ii,1) * ident(ii,1)
          rhs(ii,3)  = rhs(ii,3)    + dv(ii,1) * ident(ii,2)
          rhs(ii,4)  = rhs(ii,4)    + (cv(ii,4) + dv(ii,1)) * cv(ii,2)/cv(ii,1) * ident(ii,1) &
               + (cv(ii,4) + dv(ii,1)) * cv(ii,3)/cv(ii,1) * ident(ii,2) 
        end do

      else if( ND == 3 ) then

#ifdef ENABLE_SIMD
!DIR$ SIMD
#endif
        do ii = 1, Nc
          rhs(ii,1)  = rhs(ii,1)    + cv(ii,2) * ident(ii,1) &
               + cv(ii,3) * ident(ii,2) &
               + cv(ii,4) * ident(ii,3)
          rhs(ii,2)  = rhs(ii,2)    + dv(ii,1) * ident(ii,1)
          rhs(ii,3)  = rhs(ii,3)    + dv(ii,1) * ident(ii,2)
          rhs(ii,4)  = rhs(ii,4)    + dv(ii,1) * ident(ii,3)
          rhs(ii,5)  = rhs(ii,5)    + (cv(ii,5) + dv(ii,1)) * cv(ii,2)/cv(ii,1) * ident(ii,1) &
               + (cv(ii,5) + dv(ii,1)) * cv(ii,3)/cv(ii,1) * ident(ii,2) &
               + (cv(ii,5) + dv(ii,1)) * cv(ii,4)/cv(ii,1) * ident(ii,3)
        end do

      end if ! ND

    end if ! useMetricIdentities

    ! ... gravity

    if (Froude > 0.0_DP) then
      norm(1) = sin(gravity_angle_phi) * cos(gravity_angle_theta) * invFroude
      norm(2) = sin(gravity_angle_phi) * sin(gravity_angle_theta) * invFroude

      if( ND == 2 ) then

#ifdef ENABLE_SIMD
!DIR$ SIMD
#endif
        do i = 1, Nc
          rhs(i,2) = rhs(i,2) + cv(i,1) * norm(1) * INVJAC(i)
          rhs(i,3) = rhs(i,3) + cv(i,1) * norm(2) * INVJAC(i)
        end do

      else if( ND == 3 ) then 

        norm(3) = cos(gravity_angle_phi) * invFroude
#ifdef ENABLE_SIMD
!DIR$ SIMD
#endif
        do i = 1, Nc
          rhs(i,2) = rhs(i,2) + cv(i,1) * norm(1) * INVJAC(i)
          rhs(i,3) = rhs(i,3) + cv(i,1) * norm(2) * INVJAC(i)
          rhs(i,4) = rhs(i,4) + cv(i,1) * norm(3) * INVJAC(i)
        end do

      end if ! ND

    end if ! Froude

    return

  end subroutine NS_RHS_Euler

  subroutine NS_RHS_Euler_SKEW(region, ng, flags) ! JByun, 03, 2014

    USE ModGlobal
    USE ModDataStruct
    USE ModDeriv
    USE ModMPI

    Implicit None

    ! ... local variables
    type(t_grid), pointer :: grid
    type(t_mixt), pointer :: state
    type(t_mixt_input), pointer :: input
    type(t_region), pointer :: region
    type(t_patch), pointer :: patch
    integer :: flags
    real(rfreal), allocatable :: UVWhat(:,:)
    real(rfreal), pointer :: dflux_mom(:), dflux_mass(:,:), dflux_energy(:), e_int(:)
    integer :: ND, Nc, N(MAX_ND), ng, i, j, k, Np(MAX_ND), patchID
    integer :: ip, jp, kp, ii
    real(rfreal) :: source, norm(MAX_ND)

    ! ... simplicity
    grid  => region%grid(ng)
    state => region%state(ng)
    input => grid%input
    ND = grid%ND
    Nc = grid%nCells
    N  = 1
    do j = 1, grid%ND
      N(j) = grid%ie(j)-grid%is(j)+1
    end do ! j

    ! ... allocate space for arrays
                                !!$     nullify(dflux_mom, dflux_mass);
    allocate(UVWhat(Nc,ND), dflux_mom(Nc), dflux_energy(Nc), e_int(Nc), dflux_mass(Nc,ND) );
    do j = 1, ND
      do i = 1, Nc
        UVWhat(i,j) = 0.0_rfreal
	dflux_mass(i,j) = 0.0_rfreal
        dflux_mom(i) = 0.0_rfreal
        dflux_energy(i) = 0.0_rfreal
        e_int(i) = 0.0_rfreal
      end do ! i
    end do ! j

    ! ... subtract out the time-dependent metric portion
    if (grid%moving == TRUE) then
      do k = 1, size(state%rhs,2)
        do ii = 1, Nc
          state%rhs(ii,k) = state%rhs(ii,k) - state%cv(ii,k) * grid%INVJAC_TAU(ii)
        end do
      end do ! k
    end if

    ! ... compute the contravariant velocities
    if (grid%metric_type /= CARTESIAN)  then
      do i = 1, ND
        do j = 1, ND
          do ii = 1, Nc
            UVWhat(ii,i) = UVWhat(ii,i) + grid%MT1(ii,region%global%t2Map(i,j)) * state%cv(ii,region%global%vMap(j+1)) ! JKim 01/2008
          end do
        end do ! j
        do ii = 1, Nc
          UVWhat(ii,i) = UVWhat(ii,i) * state%dv(ii,3) + grid%XI_TAU(ii,i)
        end do
      end do ! i
    else
      do i = 1, ND
        do ii = 1, Nc
          UVWhat(ii,i) = grid%MT1(ii,region%global%t2Map(i,i)) * state%cv(ii,region%global%vMap(i+1)) * state%dv(ii,3) + grid%XI_TAU(ii,i)
        end do
      end do ! i
    end if

    ! ... internal energy
    e_int(:) = ( state%cv(:,region%global%vMap(ND+2)) - (state%cv(:,region%global%vMap(2)) ** 2 + state%cv(:,region%global%vMap(3)) ** 2 + state%cv(:,region%global%vMap(4)) ** 2) &
         * state%dv(:,3) * 0.5d0 ) * state%dv(:,3)


    ! ... inviscid fluxes

    ! ... continuity
    do i = 1, ND
       do ii = 1, Nc
          state%flux(ii) = state%cv(ii,region%global%vMap(1)) * UVWhat(ii,i)
       end do
       call APPLY_OPERATOR(region, ng, input%iFirstDeriv, i, state%flux, state%dflux, .FALSE.)
       dflux_mass(:,i) = state%dflux(:)
       do ii = 1, Nc
          state%rhs(ii,region%global%vMap(1)) = state%rhs(ii,region%global%vMap(1)) - state%dflux(ii)
       end do
       Call Save_Patch_Deriv(region, ng, 1, i, state%dflux)
    end do ! i
    
   
   ! ... momentum and energy ... JByun 08/13

   do i = 1, ND  ! differentiation direction index

      call APPLY_OPERATOR(region, ng, input%iFirstDeriv, i, e_int, dflux_energy, .FALSE.) ! ... 1st
      state%flux(:) = state%cv(:,1) * UVWhat(:,i) 
      dflux_energy(:) = state%flux(:) * dflux_energy(:) + e_int(:) * dflux_mass(:,i) ! ... 2nd

      state%flux(:) = state%flux(:) * e_int(:)
      call APPLY_OPERATOR(region, ng, input%iFirstDeriv, i, state%flux, state%dflux, .FALSE.) ! ... 3rd
      dflux_energy(:) = ( dflux_energy(:) + state%dflux(:) ) * 0.5d0

      do k = 1, ND  ! equation index
         ! ************************************
         ! First term
         do ii = 1, Nc
            state%flux(ii) = 0.5d0* state%cv(ii,region%global%vMap(k+1)) * UVWhat(ii,i) + grid%MT1(ii,region%global%t2Map(i,k)) * state%dv(ii,1) ! rho u_k U_i
         end do
         call APPLY_OPERATOR(region, ng, input%iFirstDeriv, i, state%flux, dflux_mom, .FALSE.) ! adding 1st term, nullifying dflux_mom(:)
         
         ! Third term
         do ii = 1, Nc
            state%flux(ii) = state%cv(ii,k+1) * state%dv(ii,3) ! u_k
         end do
         
         dflux_mom(:) = dflux_mom(:) + 0.5d0*state%flux(:) * dflux_mass(:,i) ! ... adding 2nd term
         call APPLY_OPERATOR(region, ng, input%iFirstDeriv, i, state%flux, state%dflux, .FALSE.)
         
         dflux_mom(:) = dflux_mom(:) + 0.5d0*state%cv(:,1) * UVWhat(:,i) * state%dflux(:) ! ... adding 3rd term
 
         do ii = 1, Nc
            state%rhs(ii,region%global%vMap(k+1)) = state%rhs(ii,region%global%vMap(k+1)) -  dflux_mom(ii)
         end do

         Call Save_Patch_Deriv(region, ng, k+1, i, dflux_mom)

         ! ************************************
         ! Pressure term ! ... 4th for energy
         dflux_energy(:) = dflux_energy(:) + grid%MT1(:,region%global%t2Map(i,k)) * state%dv(:,1) * state%dflux(:) &
              + state%dv(:,3) * state%cv(:,k+1) * ( dflux_mom(:) - 0.5d0 * state%cv(:,k+1) * state%dv(:,3) * dflux_mass(:,i) )
      end do ! k      

      state%rhs(:,Nd+2) = state%rhs(:,Nd+2) - dflux_energy(:)

      Call Save_Patch_Deriv(region, ng, Nd+2, i, dflux_energy) 
    end do ! i

    ! ... scalar advection
    do k = 1, input%nAuxVars
      do i = 1, ND
        do ii = 1, Nc
          state%flux(ii) = state%auxVars(ii,k) * UVWhat(ii,i)
        end do
        call APPLY_OPERATOR(region, ng, input%iFirstDeriv, i, state%flux, state%dflux, .FALSE.)
        do ii = 1, Nc
          state%rhs_auxVars(ii,k) = state%rhs_auxVars(ii,k) - state%dflux(ii)
        end do
      end do
    end do

    ! ... deallocate
    deallocate(UVWhat)
    deallocate(dflux_mom)     ! ... Check nullify works or not
    deallocate(dflux_mass)
    deallocate(dflux_energy)
    deallocate(e_int)


! CAA Benchmark 2, Category 1, Problem 1
    if (.false.) then
      do i = 1, Nc
        source = 0.1_dp * exp(-(log(2.0_dp))*( ((grid%XYZ(i,1)-4.0_dp)**2+grid%XYZ(i,2)**2)/(0.4_dp)**2 )) * dsin(4.0_dp * TWOPI * state%time(i)) * grid%INVJAC(i)
        state%rhs(i,4) = state%rhs(i,4) + source / (state%gv(i,1)-1.0_dp)
      end do
    end if

    ! ... subtract off (violation of) metric identity
    if (input%useMetricIdentities == TRUE) Then
      do i = 1, ND
        do ii = 1, Nc
          state%rhs(ii,1)    = state%rhs(ii,1)    + state%cv(ii,i+1) * grid%ident(ii,i)
          state%rhs(ii,i+1)  = state%rhs(ii,i+1)  + state%dv(ii,1) * grid%ident(ii,i)
          state%rhs(ii,ND+2) = state%rhs(ii,ND+2) + (state%cv(ii,ND+2) + state%dv(ii,1)) * state%cv(ii,i+1)/state%cv(ii,1) * grid%ident(ii,i)
        end do
      end do
    End if

    ! ... gravity
    if (input%Froude >= 0.0_DP) then
      norm(1) = sin(input%gravity_angle_phi) * cos(input%gravity_angle_theta) * input%invFroude
      norm(2) = sin(input%gravity_angle_phi) * sin(input%gravity_angle_theta) * input%invFroude
      norm(3) = cos(input%gravity_angle_phi) * input%invFroude
      do j = 1, ND
        do i = 1, Nc
          state%rhs(i,j+1) = state%rhs(i,j+1) + state%cv(i,1) * norm(j) * grid%INVJAC(i)
        end do
      end do
    end if

    return

  end subroutine NS_RHS_Euler_SKEW

  subroutine NS_RHS_LES_Dynamic(region, ng, VelGrad1st, TempGrad1st, MagStrnRt, tvCor) ! JKim 01/2008

    USE ModGlobal
    USE ModDataStruct
    USE ModDeriv
    USE ModMPI

    Implicit None

    ! ... subroutine arguments
    real(rfreal), pointer :: VelGrad1st(:,:), TempGrad1st(:,:)
    real(rfreal), pointer :: MagStrnRt(:)
    real(rfreal), pointer :: tvCor(:,:) ! JKim 01/2008

    ! ... local variables
    type(t_grid), pointer :: grid
    type(t_mixt), pointer :: state
    type(t_mixt_input), pointer :: input
    type(t_region), pointer :: region
    integer :: ND, Nc, N(MAX_ND), ng, i, j, ii, jj
    integer :: k,l,m,p

    ! ... storages for dynamic Smagorinsky LES model
    real(rfreal) :: invND, sqrt_two
    real(rfreal), pointer :: cvFlt(:,:) ! contains (in the order they're stored)
                                ! 1st element:
! inverse of test-filtered density, i.e.
                                ! 1 / rho_bar_hat;
! later overwritten by
                                ! (rho_bar by T_tilde)_hat / rho_bar_hat
                                ! 2nd ~ 4th elements:
                                ! (rho_bar by u_i_tilde)_hat
    real(rfreal), pointer :: TrL(:)     ! Trace of Leonard tensor
    real(rfreal), pointer :: ReuseMe(:), LHS(:), RHS(:)
    real(rfreal), pointer :: DelSq(:) ! squared grid-filter size
    real(rfreal), pointer :: muT(:), PrT(:)
    real(rfreal), pointer :: minusTwoDeltaRhoS(:), twoRhoS(:) ! to reduce number of operations (ad hoc)
    real(rfreal), pointer :: TestVelGrad1st(:,:), TestMagStrnRt(:), &
         TestStrnRt(:,:), TestTempGrad1st(:,:) ! JKim 12/07/2007

    ! ... for debugging mode, JKim 10/30/2007..so spooky
    integer :: ip, jp, kp, fname_len, dir
    character(len=80) :: fname

    ! ... to avoid locally infinite LES constants at the initial time, JKim 10/30/2007
    ! ... If field is perfectly uniform, then LES coefficients are infinite.
    if (region%global%USE_RESTART_FILE == FALSE .AND. region%global%main_ts_loop_index == 1) return

    ! ... simplicity
    grid  => region%grid(ng)
    state => region%state(ng)
    input => grid%input
    ND = grid%ND
    Nc = grid%nCells
    N  = 1
    do j = 1, grid%ND
      N(j) = grid%ie(j)-grid%is(j)+1
    end do ! j
    invND = 1.0_rfreal / real(ND,rfreal)



    ! ... allocate space for arrays
! nullify(flux); allocate(flux(Nc)); flux = 0.0_rfreal
! nullify(dflux); allocate(dflux(Nc)); dflux = 0.0_rfreal

    ! ... gradient tensor of test-filtered Favre-averaged primitive velocity
    ! ... only used to computed strain-rate tensor for test-filtered field and
    ! ... its magnitude
    ! ... so deallocated immediately after they are computed
!     JKim 12/07/2007
    nullify(TestVelGrad1st); allocate(TestVelGrad1st(Nc,ND*ND));
    do l = 1, ND
      do ii = 1, Nc
        state%flux(ii) = state%cv(ii,region%global%vMap(l+1)) * state%dv(ii,3) ! primitive velocity.
      end do ! ii
      call TestFilter(region, ng, state%flux, state%dflux) ! apply test-filter
      do ii = 1, Nc
        state%flux(ii) = state%dflux(ii)
      end do ! ii
      do m = 1, ND
        call APPLY_OPERATOR_BOX(region, ng, 1, m, state%flux, state%dflux, .FALSE., 1)
        do ii = 1, Nc
          TestVelGrad1st(ii,region%global%t2Map(l,m)) = state%dflux(ii) ! d(u_tilde_l)/d(xi_m) (l, m = 1 ~ 3)
        end do ! ii
      end do ! m
    end do ! l

    ! ... magnitude of strain-rate tensor in test-filtered field, JKim 01/2008
    nullify(TestMagStrnRt); allocate(TestMagStrnRt(Nc)); TestMagStrnRt = 0.0_rfreal
    call NS_RHS_MagStrnRt(region, grid, input, TestVelGrad1st, TestMagStrnRt)
    sqrt_two = sqrt(2.0_rfreal)
    do ii = 1, Nc
      TestMagStrnRt(ii) = sqrt_two * TestMagStrnRt(ii)
    end do ! ii

    ! ... strain-rate tensor in test-filtered field, JKim 12/07/2007
    nullify(TestStrnRt); allocate(TestStrnRt(Nc,ND*(ND+1)/2)); TestStrnRt = 0.0_rfreal

    ! ... strain-rate tensor for test-filtered velocity field, JKim 12/07/2007
    call NS_RHS_StrnRt(region, grid, TestVelGrad1st, TestStrnRt)

    deallocate(TestVelGrad1st) ! JKim 12/07/2007

    ! ... allocate space for dynamic Smagorinsky LES model
    nullify(cvFlt); allocate(cvFlt(Nc,ND+1));
    nullify(TrL); allocate(TrL(Nc));
    nullify(ReuseMe); allocate(ReuseMe(Nc));
    nullify(LHS); allocate(LHS(Nc)); 
    nullify(RHS); allocate(RHS(Nc)); 
    nullify(DelSq); allocate(DelSq(Nc)); 
    nullify(muT); allocate(muT(Nc)); 
    nullify(PrT); allocate(PrT(Nc)); 
    nullify(minusTwoDeltaRhoS); allocate(minusTwoDeltaRhoS(Nc));
    nullify(twoRhoS); allocate(twoRhoS(Nc)); 
    nullify(TestTempGrad1st); allocate(TestTempGrad1st(Nc,ND)); ! JKim 12/2007

    ! ... Step 1: precompute and store frequently-used terms
    ! ... Substep 1-1: inverse of test-filtered density
    ! ...              test-filtered mass fluxes
    do ii = 1, Nc
      state%flux(ii) = state%cv(ii,1)
    end do ! ii
    call TestFilter(region, ng, state%flux, state%dflux)
    do ii = 1, Nc
      cvFlt(ii,1) = 1.0_rfreal / state%dflux(ii) ! 1 / (rho_bar)_hat
      minusTwoDeltaRhoS(ii) = state%dflux(ii) ! to save CPU time
    end do ! ii

    do i = 1,ND
      do ii = 1, Nc
        state%flux(ii) = state%cv(ii,1+i)
      end do ! ii
      call TestFilter(region, ng, state%flux, state%dflux)
      do ii = 1, Nc
        cvFlt(ii,1+i) = state%dflux(ii) ! (rho_bar by u_tilde_i)_hat
      end do ! ii
    end do ! i

    ! ... Substep 1-2: trace of Leonard stress tensor, L_ii
    do ii = 1, Nc
      state%flux(ii) = 0.0_rfreal
    end do ! ii
    do k = 1,ND
      do ii = 1, Nc
        state%flux(ii) = state%flux(ii) + state%cv(ii,1+k) * state%cv(ii,1+k) ! JKim 01/2008
      end do ! ii
    end do ! k
    do ii = 1, Nc
      state%flux(ii) = state%flux(ii) * state%dv(ii,3) ! JKim 01/2008
    end do ! ii
    call TestFilter(region, ng, state%flux, state%dflux)
    do ii = 1, Nc
      TrL(ii) = 0.0_rfreal
    end do ! ii
    do k = 1,ND
      do ii = 1, Nc
        TrL(ii) = TrL(ii) + cvFlt(ii,1+k) * cvFlt(ii,1+k)
      end do ! ii
    end do ! k
    do ii = 1, Nc
      TrL(ii) = TrL(ii) * cvFlt(ii,1)
      TrL(ii) = state%dflux(ii) - TrL(ii)
    end do ! ii

    ! ... Substep 1-3: squared grid-filter size: squared grid size, JKim 12/07/2007
    do ii = 1, Nc
      DelSq(ii) = (grid%INVJAC(ii)**invND)*(grid%INVJAC(ii)**invND)
    end do ! ii

    ! ... Substep 1-4: frequently used coefficients, JKim 12/07/2007
    ! ...              -2 * (d_hat/d)^2 * rho_bar_hat * S_tilde_hat_mag
    ! ...               2 * rho_bar * S_tilde_mag
    do ii = 1, Nc
      minusTwoDeltaRhoS(ii) = -2.0_rfreal * input%LES_RatioFilterWidth * input%LES_RatioFilterWidth * &
           minusTwoDeltaRhoS(ii) * TestMagStrnRt(ii)
      twoRhoS(ii)           =  2.0_rfreal * state%cv(ii,1) * MagStrnRt(ii)
    end do ! ii
    ! ... Step 2: model constant C_I and Yoshizawa's SGS kinetic energy
    ! ...         to avoid increasing code-complexity, computed here without
    ! ...         calculating correction to physical viscosities, tvCor and
    ! ...         directly added to state%rhs(:,:)
    ! ...         All thermodynamic variables other than density are corrected.
    ! ... Substep 2-1: directly compute C_I constant
    do ii = 1, Nc
      LHS(ii) = -1.0_rfreal * minusTwoDeltaRhoS(ii) * TestMagStrnRt(ii) ! JKim 12/07/2007
      state%flux(ii) = twoRhoS(ii) * MagStrnRt(ii)
    end do ! ii
    call TestFilter(region, ng, state%flux, state%dflux)
    do ii = 1, Nc
      LHS(ii) = LHS(ii) - state%dflux(ii)
!state%LESC(ii,1)=LHS(ii)* DelSq(ii)
      RHS(ii) = TrL(ii)
!state%LESC(ii,2)=RHS(ii)
    end do ! ii
! *** do spatial-averaging here on LHS and RHS ***
    !...AG...
    do dir =1, MAX_ND
       if (grid%AvgDIR(dir) == 1) then 
          call LineAverage_ag(region%myrank, grid, LHS, RHS, dir, region%AllGridsGlobalSize(ng,dir))
       endif
    enddo
    !...AG...
    do ii = 1, Nc
      ReuseMe(ii) = 0.0_rfreal
    end do ! ii
    DO i = 1,Nc
      IF (LHS(i) .gt. 1E-12) THEN ! ... if C_I is well-defined
!state%LESC(i,3)=LHS(i)* DelSq(i)
!state%LESC(i,4)=RHS(i)
        ReuseMe(i) = RHS(i) / (LHS(i) * DelSq(i)) ! ... completes to evaluate C_I constant dynamically.
      END IF
                                !...AG...
!state%LESC(i,1)=ReuseMe(i) !for debug purpose
      IF (ReuseMe(i) < 0.0_rfreal) ReuseMe(i) = 0.0_rfreal ! ... positive clipping
      IF (input%LES_CLIP .and. (ReuseMe(i) > input%LES_CLIP_MAX_CI)) ReuseMe(i) = input%LES_CLIP_MAX_CI
                                !...AG...
    END DO ! i
    ! ... Substep 2-2: directly compute SGS kinetic energy over (ND * acoustic Reynolds number)
    do ii = 1, Nc
      ReuseMe(ii) = 2.0_rfreal * ReuseMe(ii) * DelSq(ii) * state%cv(ii,1) * &
           MagStrnRt(ii) * MagStrnRt(ii) * invND
    end do ! ii

    ! ... Substep 2-3: update RHS
    do i = 1,ND
      do j = 1,ND
        do ii = 1, Nc
          state%flux(ii) = grid%MT1(ii,region%global%t2Map(j,i)) * ReuseMe(ii)
        end do ! ii
        call APPLY_OPERATOR_BOX(region, ng, 1, j, state%flux, state%dflux, .FALSE., 1)
        do ii = 1, Nc
          state%rhs(ii,region%global%vMap(i+1)) = state%rhs(ii,region%global%vMap(i+1)) - state%dflux(ii)
        end do ! ii
      end do ! j
    end do ! i
    ! ... when SGS is included in viscous work, we need to subtract SGS KE portion, JKim 03/2009
    do l = 1,ND
      do ii = 1, Nc
        state%flux(ii) = 0.0_rfreal
      end do ! ii
      do k = 1,ND
        do ii = 1, Nc
          state%flux(ii) = state%flux(ii) + grid%MT1(ii,region%global%t2Map(l,k)) * state%cv(ii,k+1) * state%dv(ii,3) * ReuseMe(ii)
        end do ! ii
      end do ! k
      call APPLY_OPERATOR_BOX(region, ng, 1, l, state%flux, state%dflux, .FALSE., 1)
      do ii = 1, Nc
        state%rhs(ii,ND+2) = state%rhs(ii,ND+2) - state%dflux(ii)
      end do ! ii
    end do ! l

    do ii = 1, Nc
      ReuseMe(ii) = ReuseMe(ii) * real(ND,rfreal) ! recover SGS kinetic energy
      state%SGS_KE(ii) = ReuseMe(ii) ! store SGS kinetic energy for later use, JKim 03/2009
    end do ! ii
    if (state%RE > 0.0_rfreal) then
      do ii = 1, Nc
        state%SGS_KE(ii) = state%RE * state%SGS_KE(ii) ! ... there is acoustic Reynolds number factor
      end do ! ii
    end if ! state%RE

    ! ... Substep 2-4: correct pressure, temperature, and viscosities
    ! ...              Part of computeDv_IdealGas subroutine is directly copied.
    ! ...              Note that only additional contributions from SGS KE are
    ! ...              added to pressure and temperature.  However viscosities
    ! ...              are totally recomputed.
    ! ...              JKim 11/2007
    ! ... pressure
    do ii = 1, Nc
      state%dv(ii,1) = state%dv(ii,1) &
           - 0.5_rfreal * (state%gv(ii,1)-1.0_rfreal) * ReuseMe(ii)
    end do ! ii

    ! ... temperature
    do ii = 1, Nc
      state%dv(ii,2) = state%dv(ii,2) &
           - 0.5_rfreal * state%gv(ii,1) * ReuseMe(ii) * state%dv(ii,3)
    end do ! ii

    do l = 1, ND
      do ii = 1, Nc
        state%flux(ii) = state%dv(ii,2) ! Favre-averaged temperature
      end do ! ii
      call APPLY_OPERATOR_BOX(region, ng, 1, l, state%flux, state%dflux, .FALSE., 1)
      do ii = 1, Nc
        TempGrad1st(ii,l) = state%dflux(ii) ! d(T_tilde)/d(xi_l)
      end do ! ii

      do ii = 1, Nc
        state%flux(ii) = state%dv(ii,2) ! Favre-averaged temperature again
      end do ! ii
      call TestFilter(region, ng, state%flux, state%dflux)
      do ii = 1, Nc
        state%flux(ii) = state%dflux(ii) ! test-filtered Favre-averaged temperature
      end do ! ii
      call APPLY_OPERATOR_BOX(region, ng, 1, l, state%flux, state%dflux, .FALSE., 1)
      do ii = 1, Nc
        TestTempGrad1st(ii,l) = state%dflux(ii) ! d(T_tilde_hat)/d(xi_l)
      end do ! ii
    end do ! l
    ! ... Step 3: calculate model constant C for dynamic eddy-viscosity model
    ! ...         JKim 12/07/2007
    do ii = 1, Nc
      LHS(ii) = 0.0_rfreal; RHS(ii) = 0.0_rfreal
      muT(ii) = 0.0_rfreal ! tentatively store S_hat_kk / ND (repeated sum on k)
    end do ! ii
    do k = 1,ND
      do ii = 1, Nc
        muT(ii) = muT(ii) + TestStrnRt(ii,region%global%t2MapSym(k,k))
      end do ! ii
    end do ! k
    do ii = 1, Nc
      muT(ii) = muT(ii) * invND
      PrT(ii) = 0.0_rfreal ! tentatively store S_kk / ND (repeated sum on k)
    end do ! ii
    do k = 1,ND
      do l = 1,ND
        do ii = 1, Nc
          PrT(ii) = PrT(ii) + grid%MT1(ii,region%global%t2Map(l,k)) * VelGrad1st(ii,region%global%t2Map(k,l))
        end do ! ii
      end do ! l
    end do ! k
    do ii = 1, Nc
      PrT(ii) = PrT(ii) * grid%JAC(ii)
      PrT(ii) = PrT(ii) * invND
    end do ! ii

    do i = 1,ND
      do j = i,ND ! only upper-half part due to symmetry

        if (j == i) then

          ! ... Substep 3-1: compute S_ii (no repeated sum on i), JKim 12/07/2007
          do ii = 1, Nc
            state%flux(ii) = 0.0_rfreal
          end do ! ii
          do l = 1,ND
            do ii = 1, Nc
              state%flux(ii) = state%flux(ii) + grid%MT1(ii,region%global%t2Map(l,i)) * VelGrad1st(ii,region%global%t2Map(i,l))
            end do ! ii
          end do ! k
          do ii = 1, Nc
            state%flux(ii) = state%flux(ii) * grid%JAC(ii)
          end do ! ii

          ! ... Substep 3-2: calculate M_ii and keep it in 'ReuseMe' array, JKim 12/07/2007
          do ii = 1, Nc
            ReuseMe(ii) = minusTwoDeltaRhoS(ii) * (TestStrnRt(ii,region%global%t2MapSym(i,i)) - muT(ii))
            state%flux(ii) = twoRhoS(ii) * (state%flux(ii) - PrT(ii))
          end do ! ii
          call TestFilter(region, ng, state%flux, state%dflux)
          do ii = 1, Nc
            ReuseMe(ii) = ReuseMe(ii) + state%dflux(ii)
          end do ! ii

          ! ... Substep 3-3: calculate L_ii - (1/ND) L_kk and keep it in 'dflux' array
          do ii = 1, Nc
            state%flux(ii) = state%cv(ii,1+i)**2 * state%dv(ii,3)
          end do ! ii
          call TestFilter(region, ng, state%flux, state%dflux)
          do ii = 1, Nc
            state%dflux(ii) = state%dflux(ii) - (cvFlt(ii,1+i))**2 * cvFlt(ii,1) - TrL(ii) * invND
          end do ! ii

          ! ... Substep 3-4: form LHS and RHS
          do ii = 1, Nc
            LHS(ii) = LHS(ii) + ReuseMe(ii)**2 ! (M_ii)**2
            RHS(ii) = RHS(ii) + state%dflux(ii) * ReuseMe(ii) ! (L_ii - L_kk / ND) * M_ii
          end do ! ii

        else

          ! ... Substep 3-5: calculate off-diagonal S_ij and keep it in 'flux' array
          do ii = 1, Nc
            state%flux(ii) = 0.0_rfreal
          end do ! ii
          do l = 1,ND
            do ii = 1, Nc
              state%flux(ii) = state%flux(ii) + grid%MT1(ii,region%global%t2Map(l,j)) * VelGrad1st(ii,region%global%t2Map(i,l)) + &
                                                grid%MT1(ii,region%global%t2Map(l,i)) * VelGrad1st(ii,region%global%t2Map(j,l))
            end do ! ii
          end do ! l
          do ii = 1, Nc
            state%flux(ii) = state%flux(ii) * 0.5_rfreal * grid%JAC(ii)
          end do ! ii

          ! ... Substep 3-6: calculate off-diagonal M_ij, JKim 12/07/2007
          do ii = 1, Nc
            ReuseMe(ii) = minusTwoDeltaRhoS(ii) * TestStrnRt(ii,region%global%t2MapSym(i,j))
            state%flux(ii) = twoRhoS(ii) * state%flux(ii)
          end do ! ii
          call TestFilter(region, ng, state%flux, state%dflux)
          do ii = 1, Nc
            ReuseMe(ii) = ReuseMe(ii) + state%dflux(ii)
          end do ! ii

          ! ... Substep 3-7: calculate off-diagonal L_ij
          do ii = 1, Nc
            state%flux(ii) = state%cv(ii,1+i) * state%cv(ii,1+j) * state%dv(ii,3)
          end do ! ii
          call TestFilter(region, ng, state%flux, state%dflux)
          do ii = 1, Nc
            state%dflux(ii) = state%dflux(ii) - cvFlt(ii,1+i) * cvFlt(ii,1+j) * cvFlt(ii,1)
          end do ! ii

          ! ... Substep 3-8: form LHS and RHS
          ! ...              tensor symmetry is reflected in the factor 2.0_rfreal
          do ii = 1, Nc
            LHS(ii) = LHS(ii) + 2.0_rfreal * ReuseMe(ii)**2
            RHS(ii) = RHS(ii) + 2.0_rfreal * state%dflux(ii) * ReuseMe(ii)
          end do ! ii

        end if ! j

      end do ! j
    end do ! i
    ! ... Substep 3-9: directly compute model constant C
                                !...AG...
    do dir =1, MAX_ND
       if (grid%AvgDIR(dir) == 1) then 
          call LineAverage_ag(region%myrank, grid, LHS, RHS, dir, region%AllGridsGlobalSize(ng,dir))
       endif
    enddo
                                !...AG...
    do ii = 1, Nc
      muT(ii) = 0.0_rfreal
    end do ! ii
    DO i = 1,Nc
      IF (LHS(i) .gt. 1E-12) THEN ! ... if C is well-defined
        muT(i) = RHS(i) / (LHS(i) * DelSq(i))
      ENDIF
                                !...AG...
!state%LESC(i,2)=(muT(i))
      IF (muT(i) < 0.0_rfreal) muT(i) = 0.0_rfreal ! ... positive clipping
      IF (input%LES_CLIP .and. muT(i) > input%LES_CLIP_MAX_C) muT(i) = input%LES_CLIP_MAX_C
                                !...AG...
    END DO ! i
    ! ... Step 4: calculate turbulent Prandtl number
    do ii = 1, Nc
      LHS(ii) = 0.0_rfreal; RHS(ii) = 0.0_rfreal
    end do ! ii

    ! ... Substep 4-1: compute density times temperature in Favre-sense
    ! ...              overwrite the result on cvFlt(:,1), i.e. 1/rho_bar_hat
    ! ...              since 1/rho_bar_hat is not independently referred anymore
    do ii = 1, Nc
      state%flux(ii) = state%cv(ii,1) * state%dv(ii,2) ! rho_bar by T_tilde
    end do ! ii
    call TestFilter(region, ng, state%flux, state%dflux)
    do ii = 1, Nc
      cvFlt(ii,1) = state%dflux(ii) * cvFlt(ii,1) ! (rho_bar by T_tilde)_hat / rho_bar_hat
    end do ! ii

    do j = 1,ND

      ! ... Substep 4-2: compute the 1st term of N_j  and keep it in 'ReuseMe' array, JKim 12/2007
      do ii = 1, Nc
        state%flux(ii) = 0.0_rfreal
      end do ! ii
      do k = 1,ND
        do ii = 1, Nc
          state%flux(ii) = state%flux(ii) + grid%MT1(ii,region%global%t2Map(k,j)) * TestTempGrad1st(ii,k)
        end do ! ii
      end do ! k
      do ii = 1, Nc
        state%flux(ii) = state%flux(ii) * grid%JAC(ii)
        ReuseMe(ii) = -0.5_rfreal * minusTwoDeltaRhoS(ii) * state%flux(ii)
      end do ! ii

      ! ... Substep 4-3: compute the 2nd term of N_j and accumulate onto 'ReuseMe', JKim 12/2007
      do ii = 1, Nc
        state%flux(ii) = 0.0_rfreal
      end do ! ii
      do k = 1,ND
        do ii = 1, Nc
          state%flux(ii) = state%flux(ii) + grid%MT1(ii,region%global%t2Map(k,j)) * TempGrad1st(ii,k)
        end do ! ii
      end do ! k
      do ii = 1, Nc
        state%flux(ii) = state%flux(ii) * grid%JAC(ii)
        state%flux(ii) = 0.5_rfreal * twoRhoS(ii) * state%flux(ii)
      end do ! ii
      call TestFilter(region, ng, state%flux, state%dflux)
      do ii = 1, Nc
        ReuseMe(ii) = ReuseMe(ii) - state%dflux(ii)
      end do ! ii

      ! ... Substep 4-4: compute K_j
      do ii = 1, Nc
        state%flux(ii) = state%cv(ii,1+j) * state%dv(ii,2)
      end do ! ii
      call TestFilter(region, ng, state%flux, state%dflux)
      do ii = 1, Nc
        state%dflux(ii) = state%dflux(ii) - cvFlt(ii,1+j) * cvFlt(ii,1)
      end do ! ii

      ! ... Substep 4-5: form LHS and RHS
      do ii = 1, Nc
        LHS(ii) = LHS(ii) - state%dflux(ii) * ReuseMe(ii)
        RHS(ii) = RHS(ii) + ReuseMe(ii)**2
      end do ! ii

    end do ! j

! Substep 4-6: directly compute turbulent Prandtl number    
                                !...AG...
    do dir =1, MAX_ND
       if (grid%AvgDIR(dir) == 1) then 
          call LineAverage_ag(region%myrank, grid, LHS, RHS, dir, region%AllGridsGlobalSize(ng,dir))
       endif
    enddo
                                !...AG...
    do ii = 1, Nc
      PrT(ii) = 0.0_rfreal
    end do ! ii
    DO i = 1,Nc
      IF (LHS(i) .gt. 1E-12) THEN ! ... if PrT is well-defined
        PrT(i) = muT(i) * DelSq(i) * RHS(i) / LHS(i) ! At this point, muT(:) contains C.        
      END IF
                                !...AG...
!State%LESC(i,3)=PrT(i)
      IF (PrT(i) < 0.0_rfreal) PrT(i) = 0.0_rfreal ! ... positive clipping
      IF (input%LES_CLIP .and. PrT(i) > input%LES_CLIP_MAX_PrT) PrT(i) = input%LES_CLIP_MAX_PrT
                                !...AG...
    END DO ! i
    ! ... Step 5: compute eddy viscosity and modified turbulent Prandtl number
    ! ...         when some plot of eddy viscosity or modified turbulent Prandtl 
    ! ...         number is required, draw at this stage.
    do ii = 1, Nc
      muT(ii) = muT(ii) * DelSq(ii) * state%cv(ii,1) * MagStrnRt(ii)
    end do ! ii

    if (state%RE > 0.0_rfreal) then
      do ii = 1, Nc
        muT(ii) = muT(ii) * state%RE
        PrT(ii) = PrT(ii) * state%PRinv
      end do ! ii
    end if ! state%RE
    do ii = 1, Nc
      state%muT(ii) = muT(ii) ! store eddy-viscosity for later use, JKim 03/2009
      state%PrT(ii) = PrT(ii) ! store turbulent Prandtl number for later use, JKim 03/2009
    end do ! ii
    ! ... Step 6: compute correction to physical viscosities
    ! ...         state%tv should NOT be updated here because it's used in 
    ! ...         computing viscous dissipation in energy equation where LES 
    ! ...         model should not be incorporated, JKim 01/2008.
    ! ...         Note that Yoshizawa's model for SGS kinetic energy is already 
    ! ...         taken into an account.
    do ii = 1, Nc
      tvCor(ii,1) = muT(ii)      
!State%LESC(ii,4) = muT(ii)
!State%LESC(ii,5) = State%tv(ii,1)    
      tvCor(ii,2) = - 2.0_rfreal * muT(ii) * invND
      if (PrT(ii) .gt. 0.0_rfreal) then
        tvCor(ii,3) =  muT(ii) / PrT(ii)
      else
        tvcor(ii,3) = 0.0_rfreal
      endif
    end do ! ii

    ! ... Step 7: clean-up everything allocated
! deallocate(flux, dflux)
    deallocate(cvFlt, TrL, ReuseMe, LHS, RHS, DelSq)
    deallocate(muT, PrT, minusTwoDeltaRhoS, twoRhoS)
    deallocate(TestMagStrnRt, TestStrnRt, TestTempGrad1st) ! JKim 12/2007
    return
  end subroutine NS_RHS_LES_Dynamic

  subroutine NS_RHS_LES_Dynamic_Retrieve(region, ng)

    USE ModGlobal
    USE ModDataStruct
    USE ModDeriv
    USE ModMPI

    Implicit None

    ! ... subroutine arguments
    type(t_region), pointer :: region
    integer :: ng

    ! ... local variables
    type(t_grid), pointer :: grid
    type(t_mixt), pointer :: state
    type(t_mixt_input), pointer :: input
    integer :: ND, Nc, N(MAX_ND), i, j, k, l, ii
    real(rfreal) :: invND

    ! ... simplicity
    grid  => region%grid(ng)
    state => region%state(ng)
    input => grid%input
    ND = grid%ND
    Nc = grid%nCells
    N  = 1
    do j = 1, grid%ND
      N(j) = grid%ie(j)-grid%is(j)+1
    end do ! j

    invND = 1.0_rfreal / real(ND,rfreal)
    do ii = 1, Nc
      state%SGS_KE(ii) = state%SGS_KE(ii) * invND
    end do ! ii
    if (state%RE > 0.0_rfreal) then
      do ii = 1, Nc
        state%SGS_KE(ii) = state%REinv * state%SGS_KE(ii)
      end do ! ii
    end if ! state%RE

    ! ... update RHS of momentum equations by SGS kinetic energy
    do i = 1,ND
      do j = 1,ND
        do ii = 1, Nc
          state%flux(ii) = grid%MT1(ii,region%global%t2Map(j,i)) * state%SGS_KE(ii)
        end do ! ii
        call APPLY_OPERATOR(region, ng, 1, j, state%flux, state%dflux, .FALSE.)
        do ii = 1, Nc
          state%rhs(ii,region%global%vMap(i+1)) = state%rhs(ii,region%global%vMap(i+1)) - state%dflux(ii)
        end do ! ii
      end do ! j
    end do ! i
    ! ... when SGS is included in viscous work, we need to subtract SGS KE portion, JKim 03/2009
    do l = 1,ND
      do ii = 1, Nc
        state%flux(ii) = 0.0_rfreal
      end do ! ii
      do k = 1,ND
        do ii = 1, Nc
          state%flux(ii) = state%flux(ii) + grid%MT1(ii,region%global%t2Map(l,k)) * state%cv(ii,k+1) * state%dv(ii,3) * state%SGS_KE(ii)
        end do ! ii
      end do ! k
      call APPLY_OPERATOR(region, ng, 1, l, state%flux, state%dflux, .FALSE.)
      do ii = 1, Nc
        state%rhs(ii,ND+2) = state%rhs(ii,ND+2) - state%dflux(ii)
      end do ! ii
    end do ! l

    do ii = 1, Nc
      state%SGS_KE(ii) = state%SGS_KE(ii) * real(ND,rfreal) ! recover SGS kinetic energy
    end do ! ii

    ! ... pressure correction
    do ii = 1, Nc
      state%dv(ii,1) = state%dv(ii,1) &
           - 0.5_rfreal * (state%gv(ii,1)-1.0_rfreal) * state%SGS_KE(ii)
    end do ! ii

    ! ... temperature correction
    do ii = 1, Nc
      state%dv(ii,2) = state%dv(ii,2) &
           - 0.5_rfreal * state%gv(ii,1) * state%SGS_KE(ii) * state%dv(ii,3)
    end do ! ii

    if (state%RE > 0.0_rfreal) then
      do ii = 1, Nc
        state%SGS_KE(ii) = state%RE * state%SGS_KE(ii)
      end do ! ii
    end if ! state%RE

    ! ... temperature gradient to be computed in NS_Temperature_Gradient

    ! ... update using previously computed eddy-viscosity and turbulent Prandtl number
    do ii = 1, Nc
      state%tvCor(ii,1) = state%tvCor(ii,1) + state%muT(ii)
      state%tvCor(ii,2) = state%tvCor(ii,2) - 2.0_rfreal * state%muT(ii) * invND
    end do ! ii
    DO i = 1,Nc
      IF (state%PrT(i) /= 0.0_rfreal) &
           state%tvCor(i,3) = state%tvCor(i,3) + state%muT(i) / state%PrT(i)
    END DO ! i

  end subroutine NS_RHS_LES_Dynamic_Retrieve

  subroutine NS_RHS_ViscousTerms(region, ng, flags, VelGrad1st, TempGrad1st, tvCor) ! JKim 01/2008

    USE ModGlobal
    USE ModDataStruct
    USE ModDeriv
    USE ModMPI

    Implicit None

    ! ... subroutine arguments
    real(rfreal), pointer :: VelGrad1st(:,:), TempGrad1st(:,:)
    real(rfreal), pointer, optional :: tvCor(:,:) ! JKim 01/2008

    ! ... local variables
    type(t_grid), pointer :: grid
    type(t_mixt), pointer :: state
    type(t_mixt_input), pointer :: input
    type(t_region), pointer :: region
    type(t_patch), pointer :: patch
    integer :: flags
    real(rfreal), allocatable :: UVW(:,:)
    integer :: ND, Nc, N(MAX_ND), ng, i, j, Np(MAX_ND), patchID
    integer :: ip, jp, kp, ii

    ! ... local variables and arrays for viscous and heat fluxes, JKim 02/2007
    integer :: k, l, m, p
    real(rfreal) :: viscous_time
    real(rfreal), pointer :: VelGrad2nd(:,:,:), TempGrad2nd(:,:)
    real(rfreal), pointer :: MTProd(:,:)
                                !!$    real(rfreal), pointer :: MTProdBKUP(:,:) ! JKim 01/2008

    ! ... simplicity
    grid  => region%grid(ng)
    state => region%state(ng)
    input => grid%input
    ND = grid%ND
    Nc = grid%nCells
    N  = 1
    do j = 1, grid%ND
      N(j) = grid%ie(j)-grid%is(j)+1
    end do ! j

    if (flags == INVISCID_ONLY) return ! because it's inviscid computation

    if (state%RE == 0.0_rfreal .AND. input%shock == 0 .AND. input%LES == 0) then
      call graceful_exit(region%myrank, 'At least either shock-capturing or LES should be on to compute viscous terms.')
    end if ! state%RE

    ! ... allocate space for arrays
! nullify(flux); allocate(flux(Nc)); flux = 0.0_rfreal
! nullify(dflux); allocate(dflux(Nc)); dflux = 0.0_rfreal
    allocate(UVW(Nc,ND)); UVW = 0.0_rfreal

    ! ... allocate space for arrays for viscous terms, JKim 06/2007
    nullify(VelGrad2nd); allocate(VelGrad2nd(Nc,ND,1+(ND-1)*ND/2)); VelGrad2nd = 0.0_rfreal ! JKim 05/14/2007
    nullify(TempGrad2nd); allocate(TempGrad2nd(Nc,1+(ND-1)*ND/2)); TempGrad2nd = 0.0_rfreal
    nullify(MTProd); allocate(MTProd(Nc,ND*ND*ND)); MTProd = 0.0_rfreal
                                !!$    if (PRESENT(tvCor) .eqv. .true.) then ! JKim 01/2008
                                !!$      nullify(MTProdBKUP); allocate(MTProdBKUP(Nc,ND*ND*ND)); MTProdBKUP = 0.0_rfreal
                                !!$    end if ! input%LES

    ! ... Step 1: viscosity correction is now corrected viscosity
    ! ...         state%tv still not updated, JKim 01/2008
                                !!$    if (PRESENT(tvCor) .eqv. .true.) then
                                !!$      tvCor(:,1:input%nTv) = state%tv(:,1:input%nTv) + tvCor(:,1:input%nTv)
                                !!$    end if ! PRESENT(tvCor)

!viscous_time = mpi_wtime()

    ! ... Step 2: precompute primitive velocities and store in UVW(:,:).
    do l = 1, ND
      do ii = 1, Nc
        UVW(ii,l) = state%cv(ii,region%global%vMap(l+1)) * state%dv(ii,3)
      end do
    end do ! l

    ! ... store viscous fluxes on the boundary
    Call Save_Patch_Viscous_Flux(region, ng, 0, 0, state%flux) ! zero things out first

    do i = 1, ND
      do j = 1, ND

! momentum: (rho u_i)_t = ... (\mu { (u_i)_j + (u_j)_i })_j + (\lambda (u_k)_k)_i
        do k = 1, Nc
          state%flux(k) = state%tv(k,1) * (VelGrad1st(k,region%global%t2Map(i,j)) + VelGrad1st(k,region%global%t2Map(j,i))) * state%REinv &
                        + region%global%delta(i,j) * state%tv(k,2) * 0.5_rfreal * (VelGrad1st(k,region%global%t2Map(i,j)) + VelGrad1st(k,region%global%t2Map(j,i))) * state%REinv
        end do
        Call Save_Patch_Viscous_Flux(region, ng, i+1, j, state%flux)

! energy: (rho E)_t = ... [(\mu { (u_i)_j + (u_j)_i } + \lambda (u_k)_k \delta_{ij}) u_j]_i
        do k = 1, Nc
          state%flux(k) = state%flux(k) * UVW(k,j)
        end do
        Call Save_Patch_Viscous_Flux(region, ng, ND+2, i, state%flux)

      end do

! energy: (rho E)_t = ... (k T_i)_i
      do k = 1, Nc
        state%flux(k) = state%tv(k,3) * TempGrad1st(k,i) * state%REinv * state%PRinv
      end do
      Call Save_Patch_Viscous_Flux(region, ng, ND+2, i, state%flux)

    end do

    ! ... thermally coupled?  Save the temperature gradient on interacting patches
    if(input%QuasiSS == TRUE) then
      Call Save_Patch_TempGrad(region, ng, 0, state%flux) ! zero things out first
      do i = 1, ND
        do k = 1, Nc
          state%flux(k) = TempGrad1st(k,i)
        end do
        Call Save_Patch_TempGrad(region, ng, i, state%flux)
      end do
    end if

    ! ... Step 3: precompute off-diagonal 2nd derivatives of velocity and temperature
    do j = 1,ND
      do m = j+1,ND ! only upper-triangle part
        do l = 1,ND
          do k = 1, Nc
            state%flux(k) = VelGrad1st(k,region%global%t2Map(l,j)) ! d(u_l)/d(xi_j)
          end do
          call APPLY_OPERATOR_BOX(region, ng, 1, m, state%flux, state%dflux, .FALSE., TRUE)
          do k = 1, Nc
            VelGrad2nd(k,l,region%global%t2MapSymVisc(j,m)) = state%dflux(k) ! d^2(u_l)/d(xi_j)/d(xi_m)
          end do
        end do ! l
        do k = 1, Nc
          state%flux(k) = TempGrad1st(k,j) ! dT/d(xi_j)
        end do
        call APPLY_OPERATOR_BOX(region, ng, 1, m, state%flux, state%dflux, .FALSE., TRUE)
        do k = 1, Nc
          TempGrad2nd(k,region%global%t2MapSymVisc(j,m)) = state%dflux(k) ! d^2T/d(xi_j)/d(xi_m)
        end do
      end do ! m
    end do ! j

    ! ... Step 4: start j-loop (j is flux direction and runs from 1 to ND)
    do j = 1, ND

      ! ... Step 5: precompute metric-Jacobian products
      do k = 1, ND
        do l = 1, ND
          do m = 1, ND
            do ii = 1, Nc
              MTProd(ii,region%global%t3Map(k,l,m)) = grid%JAC(ii) * grid%MT1(ii,region%global%t2Map(j,k)) * grid%MT1(ii,region%global%t2Map(l,m)) * state%REinv
            end do
          end do ! m
        end do ! l
      end do ! k

      ! ... Step 6: precompute the diagonal 2nd derivatives of velocity and 
      ! ...         temperature for this j
      do l = 1, ND
        do ii = 1, Nc
          state%flux(ii) = UVW(ii,l) ! u_l
        end do
        call APPLY_OPERATOR_BOX(region, ng, 2, j, state%flux, state%dflux, .FALSE., FALSE)
        do ii = 1, Nc
          VelGrad2nd(ii,l,region%global%t2MapSymVisc(j,j)) = state%dflux(ii)
        end do
      end do ! l
      do ii = 1, Nc
        state%flux(ii) = state%dv(ii,2) ! temperature.
      end do
      call APPLY_OPERATOR_BOX(region, ng, 2, j, state%flux, state%dflux, .FALSE., FALSE)
      do ii = 1, Nc
        TempGrad2nd(ii,region%global%t2MapSymVisc(j,j)) = state%dflux(ii)
      end do

      ! ... Step 7: part of terms in the i-th momentum equations
      ! ...         Viscous fluxes are computed term-by-term with explicit 
      ! ...         nonconservative 2nd derivatives.
      ! ...         Half of viscous terms having 2nd derivatives are computed here.
      do i = 1, ND
        do l = 1, ND
          do m = 1, ND
            do ii = 1, Nc
              state%flux(ii) = MTProd(ii,region%global%t3Map(i,m,l)) * state%tv(ii,2) * VelGrad2nd(ii,l,region%global%t2MapSymVisc(j,m))
              state%rhs(ii,region%global%vMap(i+1)) = state%rhs(ii,region%global%vMap(i+1)) + state%flux(ii)
            end do
          end do ! m
        end do ! l
        do l = 1, ND
          do ii = 1, Nc
            state%flux(ii) = 0.0_rfreal
          end do
          do k = 1, ND
            do ii = 1, Nc
              state%flux(ii) = state%flux(ii) + MTProd(ii,region%global%t3Map(k,l,k))
            end do
          end do ! k
          do ii = 1, Nc
            state%flux(ii) = state%flux(ii) * state%tv(ii,1) * VelGrad2nd(ii,i,region%global%t2MapSymVisc(j,l))
            state%rhs(ii,region%global%vMap(i+1)) = state%rhs(ii,region%global%vMap(i+1)) + state%flux(ii)
          end do
        end do ! l
        do k = 1, ND
          do l = 1, ND
            do ii = 1, Nc
              state%flux(ii) = MTProd(ii,region%global%t3Map(k,l,i)) * state%tv(ii,1) * VelGrad2nd(ii,k,region%global%t2MapSymVisc(j,l))
              state%rhs(ii,region%global%vMap(i+1)) = state%rhs(ii,region%global%vMap(i+1)) + state%flux(ii)
            end do
          end do ! l
        end do ! k
      end do ! i

      ! ... Step 8: viscous work in energy equation
      ! ...         term-by-term computation with explicit nonconservative 2nd 
      ! ...         derivatives.
      do l = 1, ND
        do m = 1, ND
          do ii = 1, Nc
            state%flux(ii) = 0.0_rfreal
          end do ! ii
          do k = 1, ND
            do ii = 1, Nc
              state%flux(ii) = state%flux(ii) + MTProd(ii,region%global%t3Map(k,m,l)) * UVW(ii,k)
            end do ! ii
          end do ! k
          do ii = 1, Nc
            state%flux(ii) = state%flux(ii) * state%tv(ii,2) * VelGrad2nd(ii,l,region%global%t2MapSymVisc(j,m))
            state%rhs(ii,region%global%vMap(ND+2)) = state%rhs(ii,region%global%vMap(ND+2)) + state%flux(ii)
          end do ! ii
        end do ! m
      end do ! l
      do k = 1, ND
        do l = 1, ND
          do ii = 1, Nc
            state%flux(ii) = 0.0_rfreal
          end do ! ii
          do p = 1, ND
            do ii = 1, Nc
              state%flux(ii) = state%flux(ii) + MTProd(ii,region%global%t3Map(k,l,p)) * UVW(ii,p)
            end do ! ii
          end do ! p
          do ii = 1, Nc
            state%flux(ii) = state%flux(ii) * state%tv(ii,1) * VelGrad2nd(ii,k,region%global%t2MapSymVisc(j,l))
            state%rhs(ii,region%global%vMap(ND+2)) = state%rhs(ii,region%global%vMap(ND+2)) + state%flux(ii)
          end do ! ii
        end do ! l
      end do ! k
      do p = 1, ND
        do l = 1, ND
          do ii = 1, Nc
            state%flux(ii) = 0.0_rfreal
          end do ! ii
          do k = 1, ND
            do ii = 1, Nc
              state%flux(ii) = state%flux(ii) + MTProd(ii,region%global%t3Map(k,l,k))
            end do ! ii
          end do ! k
          do ii = 1, Nc
            state%flux(ii) = state%flux(ii) * state%tv(ii,1) * UVW(ii,p) * &
                                 VelGrad2nd(ii,p,region%global%t2MapSymVisc(j,l))
            state%rhs(ii,region%global%vMap(ND+2)) = state%rhs(ii,region%global%vMap(ND+2)) + state%flux(ii)
          end do ! ii
        end do ! l
      end do ! p

      ! ... Step 9: half of heat transfer term.
      do l = 1, ND
        do ii = 1, Nc
          state%flux(ii) = 0.0_rfreal
        end do ! ii
        do k = 1, ND
          do ii = 1, Nc
            state%flux(ii) = state%flux(ii) + MTProd(ii,region%global%t3Map(k,l,k))
          end do ! ii
        end do ! k
!!$        if (PRESENT(tvCor) .eqv. .true.) then ! JKim 01/2008
!!$          state%flux(:) = state%flux(:) * tvCor(:,3) * TempGrad2nd(:,region%global%t2MapSymVisc(j,l))
!!$        else
        do ii = 1, Nc
          state%flux(ii) = state%flux(ii) * state%tv(ii,3) * TempGrad2nd(ii,region%global%t2MapSymVisc(j,l))
        end do ! ii
                                !!$        end if ! PRESENT(tvCor)
!if (state%RE > 0.0_rfreal) 
        do ii = 1, Nc
          state%flux(ii) = state%PRinv * state%flux(ii)
          state%rhs(ii,region%global%vMap(ND+2)) = state%rhs(ii,region%global%vMap(ND+2)) + state%flux(ii)
        end do ! ii
      end do ! l

      ! ... Step 10: dilatation terms in momentum equations
      do i = 1, ND
        do l = 1, ND
          do m = 1, ND
!!$            if (PRESENT(tvCor) .eqv. .true.) then ! JKim 01/2008
!!$              state%flux(:) = MTProd(:,region%global%t3Map(i,m,l)) * tvCor(:,2)
!!$            else
            do ii = 1, Nc
              state%flux(ii) = MTProd(ii,region%global%t3Map(i,m,l)) * state%tv(ii,2)
            end do ! ii
                                !!$            end if ! PRESENT(tvCor)
            call APPLY_OPERATOR_BOX(region, ng, 1, j, state%flux, state%dflux, .FALSE., TRUE)
            do ii = 1, Nc
              state%rhs(ii,region%global%vMap(i+1)) = state%rhs(ii,region%global%vMap(i+1)) + state%dflux(ii) * VelGrad1st(ii,region%global%t2Map(l,m))
            end do ! ii
          end do ! m
        end do ! l
      end do ! i

      ! ... Step 11: dilatation work and part of viscous work in energy equation
      do l = 1, ND
        do m = 1, ND
          do ii = 1, Nc
            state%flux(ii) = 0.0_rfreal
          end do ! ii
          do k = 1, ND
            do ii = 1, Nc
              state%flux(ii) = state%flux(ii) + MTProd(ii,region%global%t3Map(k,m,l)) * UVW(ii,k)
            end do ! ii
          end do ! k
          do ii = 1, Nc
            state%flux(ii) = state%flux(ii) * state%tv(ii,2)
          end do ! ii
          call APPLY_OPERATOR_BOX(region, ng, 1, j, state%flux, state%dflux, .FALSE., TRUE)
          do ii = 1, Nc
            state%rhs(ii,region%global%vMap(ND+2)) = state%rhs(ii,region%global%vMap(ND+2))                  &
                                     + state% dflux(ii) * VelGrad1st(ii,region%global%t2Map(l,m))
          end do ! ii
        end do ! m
      end do ! l
      do k = 1, ND
        do l = 1, ND
          do p = 1, ND
            do ii = 1, Nc
              state%flux(ii) = MTProd(ii,region%global%t3Map(k,l,p)) * state%tv(ii,1) * &
                         VelGrad1st(ii,region%global%t2Map(p,j)) * VelGrad1st(ii,region%global%t2Map(k,l))
              state%rhs(ii,region%global%vMap(ND+2)) = state%rhs(ii,region%global%vMap(ND+2)) + state%flux(ii)
            end do ! ii
          end do ! p
        end do ! l
      end do ! k
      do p = 1, ND
        do l = 1, ND
          do ii = 1, Nc
            state%flux(ii) = 0.0_rfreal
          end do ! ii
          do k = 1, ND
            do ii = 1, Nc
              state%flux(ii) = state%flux(ii) + MTProd(ii,region%global%t3Map(k,l,k))
            end do ! ii
          end do ! k
          do ii = 1, Nc
            state%flux(ii) = state%flux(ii) * state%tv(ii,1) * VelGrad1st(ii,region%global%t2Map(p,j)) * &
                                 VelGrad1st(ii,region%global%t2Map(p,l))
            state%rhs(ii,region%global%vMap(ND+2)) = state%rhs(ii,region%global%vMap(ND+2)) + state%flux(ii)
          end do ! ii
        end do ! l
      end do ! p

      ! ... Step 12: precompute j-derivative of metric-Jacobian product by 
      ! ...          viscosity for momentum equation
      ! ...          For memory-saving, overwrite results onto MTProdBKUP
      ! ...          When tvCor is present, need to get a backup of original 
      ! ...          MTProd, JKim 01/2008
                                !!$      if (PRESENT(tvCor) .eqv. .true.) then
                                !!$        MTProdBKUP = MTProd
                                !!$      end if ! PRESENT(tvCor)
      do k = 1, ND
        do l = 1, ND
          do m = 1, ND
!!$            if (PRESENT(tvCor)  .eqv. .true.) then ! JKim 01/2008
!!$              state%flux(:) = MTProd(:,region%global%t3Map(k,l,m)) * tvCor(:,1)
!!$            else
              do ii = 1, Nc
                state%flux(ii) = MTProd(ii,region%global%t3Map(k,l,m)) * state%tv(ii,1)
              end do ! ii
!!$            end if ! PRESENT(tvCor)
            call APPLY_OPERATOR_BOX(region, ng, 1, j, state%flux, state%dflux, .FALSE., TRUE)
            do ii = 1, Nc
              MTProd(ii,region%global%t3Map(k,l,m)) = state%dflux(ii)
            end do ! ii
          end do ! m
        end do ! l
      end do ! k

      ! ... Step 13: compute rest of viscous terms in momentum equation
      do i = 1, ND
        do l = 1, ND
          do ii = 1, Nc
            state%flux(ii) = 0.0_rfreal
          end do ! ii
          do k = 1, ND
            do ii = 1, Nc
              state%flux(ii) = state%flux(ii) + MTProd(ii,region%global%t3Map(k,l,k))
            end do ! ii
          end do ! k
          do ii = 1, Nc
            state%flux(ii) = state%flux(ii) * VelGrad1st(ii,region%global%t2Map(i,l))
            state%rhs(ii,region%global%vMap(i+1)) = state%rhs(ii,region%global%vMap(i+1)) + state%flux(ii)
          end do ! ii
        end do ! l
        do k = 1, ND
          do l = 1, ND
            do ii = 1, Nc
              state%flux(ii) = MTProd(ii,region%global%t3Map(k,l,i)) * VelGrad1st(ii,region%global%t2Map(k,l))
              state%rhs(ii,region%global%vMap(i+1)) = state%rhs(ii,region%global%vMap(i+1)) + state%flux(ii)
            end do ! ii
          end do ! l
        end do ! k
      end do ! i

      ! ... Step 14: When tvCor is present, need to compute MTProd again because
      ! ...          we don't consider eddy-viscosity-type model in the following
      ! ...          viscous work term in energy equation, JKim 01/2008
!!$      if (PRESENT(tvCor)  .eqv. .true.) then
!!$        MTProd = MTProdBKUP
!!$        do k = 1, ND
!!$          do l = 1, ND
!!$            do m = 1, ND
!!$              state%flux(:) = MTProd(:,region%global%t3Map(k,l,m)) * state%tv(:,1)
!!$              call APPLY_OPERATOR(region, ng, 1, j, state%flux, state%dflux, .FALSE.)
!!$              MTProd(:,region%global%t3Map(k,l,m)) = state%dflux(:)
!!$            end do ! m
!!$          end do ! l
!!$        end do ! k
!!$      end if ! input%LES

      ! ... Step 15: compute rest of viscous work terms in energy equation
      do k = 1, ND
        do l = 1, ND
          do p = 1, ND
            do ii = 1, Nc
              state%flux(ii) = MTProd(ii,region%global%t3Map(k,l,p)) * UVW(ii,p) * VelGrad1st(ii,region%global%t2Map(k,l))
              state%rhs(ii,region%global%vMap(ND+2)) = state%rhs(ii,region%global%vMap(ND+2)) + state%flux(ii)
            end do ! ii
          end do ! p
        end do ! l
      end do ! k
      do p = 1, ND
        do l = 1, ND
          do ii = 1, Nc
            state%flux(ii) = 0.0_rfreal
          end do ! ii
          do k = 1, ND
            do ii = 1, Nc
              state%flux(ii) = state%flux(ii) + MTProd(ii,region%global%t3Map(k,l,k))
            end do ! ii
          end do ! k
          do ii = 1, Nc
            state%flux(ii) = state%flux(ii) * UVW(ii,p) * VelGrad1st(ii,region%global%t2Map(p,l))
            state%rhs(ii,region%global%vMap(ND+2)) = state%rhs(ii,region%global%vMap(ND+2)) + state%flux(ii)
          end do ! ii
        end do ! l
      end do ! p

      ! ... Step 16: When tvCor is present, MTProd should be recomputed due to 
      ! ...          artificial diffusivity, JKim 01/2008
!!$      if (PRESENT(tvCor) .eqv. .true.) then
!!$        MTProd = MTProdBKUP
!!$        do k = 1, ND
!!$          do l = 1, ND
!!$            do m = 1, ND
!!$              state%flux(:) = MTProd(:,region%global%t3Map(k,l,m)) * tvCor(:,3)
!!$              call APPLY_OPERATOR(region, ng, 1, j, state%flux, state%dflux, .FALSE.)
!!$              MTProd(:,region%global%t3Map(k,l,m)) = dflux(:)
!!$            end do ! m
!!$          end do ! l
!!$        end do ! k
!!$      end if ! PRESENT(tvCor)

      ! ... Step 17: complete energy equation
      do l = 1, ND
        do ii = 1, Nc
          state%flux(ii) = 0.0_rfreal
        end do ! ii
        do k = 1, ND
          do ii = 1, Nc
            state%flux(ii) = state%flux(ii) + MTProd(ii,region%global%t3Map(k,l,k))
          end do ! ii
        end do ! k
        do ii = 1, Nc
          state%flux(ii) = state%flux(ii) * TempGrad1st(ii,l)
!if (state%RE > 0.0_rfreal)
          state%flux(ii) = state%PRinv * state%flux(ii)
          state%rhs(ii,region%global%vMap(ND+2)) = state%rhs(ii,region%global%vMap(ND+2)) + state%flux(ii)
        end do ! ii
      end do ! l

      ! ... Step 18: close j-loop.
    end do ! j

    ! ... deallocate
! deallocate(flux, dflux)
    deallocate(UVW, VelGrad2nd, TempGrad2nd, MTProd)

    return

  end subroutine NS_RHS_ViscousTerms

  subroutine NS_Timestep(region)

    USE ModGlobal
    USE ModDataStruct
    USE ModMPI

    Implicit None

    ! ... global variables
    type(t_region), pointer :: region

    ! ... local variables
    integer :: i, j, k, jj, ng, ierr
    real(rfreal) :: UU(MAX_ND), absUU, snd_spd, metric_fac, fac0, dt_inv, dt_vis, dt
    real(rfreal) :: var, var_all, OSXI(MAX_ND), SXI(MAX_ND), dt_tmp(MAX_ND), THETA, RMU
    Type(t_grid), pointer :: grid
    Type(t_mixt), pointer :: state
    Type(t_mixt_input), pointer :: input
    real(rfreal), allocatable :: dt_local(:)
    real(rfreal) :: nu_star, numer, denom, dot_prod, ibfac

    ! ... allocate space for each grid on this processor
    allocate(dt_local(region%nGrids))

    ! ... constant cfl mode
    if (region%input%cfl_mode == CONSTANT_CFL) then

      do ng = 1, region%nGrids

        state => region%state(ng)
        grid  => region%grid(ng)
        input => grid%input

        ! ... initialize
        do i = 1, grid%nCells
          state%cfl(i) = input%cfl
        end do

        ! ... In inviscid shock-capturing or inviscid LES, state%REinv = 0.0 
        ! ... gets rid of hyperviscosity or LES contribution.  Since either 
        ! ... artificial viscosity doesn't carry RE-factor, 
        ! ... we may conveniently manipulate its value to retain code 
        ! ... formalism, JKim 02/2009
        if (state%RE == 0.0_rfreal .and. input%tvCor_in_dt == TRUE) then
          state%REinv = 1.0_rfreal; state%PRinv = 1.0_rfreal
        end if ! state%RE

        ! ... inviscid timestep
        Do k = 1, grid%nCells
          state%dt(k) = 999.0_8
          ibfac = grid%ibfac(k)
          if (ibfac > 0.0_rfreal) then

            ! ... initialize
            numer = 0.0_rfreal
            denom = 0.0_rfreal

            ! ... Cartesian velocity
            UU = 0.0_rfreal
            do i = 1, grid%ND
              UU(i) = state%dv(k,3) * state%cv(k,1+i) + grid%XI_TAU(k,i)
            end do
            do i = 1, grid%ND
              denom = denom + abs(DOT_PRODUCT(UU(1:grid%ND),grid%MT1(k,region%global%t2Map(i,1:grid%ND))))
            end do

            ! ... speed of sound
            snd_spd = sqrt(state%gv(k,1) * state%dv(k,1) * state%dv(k,3))

            dot_prod = 0.0_rfreal
            do i = 1, grid%ND**2
              dot_prod = dot_prod + grid%MT1(k,i) * grid%MT1(k,i)
            end do
            denom = denom + snd_spd * sqrt(dot_prod)

            ! ... put it all together
            dt_inv = grid%INVJAC(k) / (denom * ibfac + (1.0 - ibfac) * 1E-10) * ibfac
            dt_vis = dt_inv

            ! ... for the viscous terms (taken from FDL3DI)
            if (state%RE > 0.0_rfreal .OR. input%tvCor_in_dt == TRUE) then

              nu_star = max(state%tv(k,1)*state%REinv, state%tv(k,2)*state%REinv, state%tv(k,3)*state%REinv*state%PRinv)
              dot_prod = 0.0_rfreal
              do i = 1, grid%ND
                dot_prod = dot_prod + sqrt(DOT_PRODUCT(grid%MT1(k,region%global%t2Map(i,1:grid%ND)),grid%MT1(k,region%global%t2Map(i,1:grid%ND))))
              end do
              denom = denom + 2.0_rfreal * nu_star * grid%JAC(k) * dot_prod**2

              dt_vis = grid%INVJAC(k) / (denom * ibfac + (1.0 - ibfac) * 1E-10) * ibfac

            end if

            ! ... the timestep
            state%dt(k) = state%cfl(k) * MIN(dt_inv, dt_vis) * ibfac
          End If

        End Do

        ! ... save the minimum dt for this grid
        dt_local(ng) = MINVAL(state%dt, MASK = state%dt .gt. 0.0_rfreal)

        ! ... state%REinv should be recovered in inviscid limit, JKim 02/2009
        if (state%RE == 0.0_rfreal .and. input%tvCor_in_dt == TRUE) then
          state%REinv = 0.0_rfreal; state%PRinv = 1.0_rfreal / state%PR
        end if ! state%RE

      end do

!call mpi_barrier(mycomm, ierr)

      ! ... find the minimum dt over all nodes
      var = MINVAL(dt_local, MASK = dt_local .gt. 0.0_rfreal)
      if (input%caseID == 'SCALING_NO_COMM') then
        var_all = var
      else
        Call MPI_AllReduce(var, var_all, 1, MPI_REAL8, MPI_MIN, mycomm, ierr)
      end if

      ! ... copy to all grids on this processor
      do ng = 1, region%nGrids
        state => region%state(ng)
        state%dt(:) = var_all
      end do

    else if (region%input%cfl_mode == CONSTANT_DT) then

      do ng = 1, region%nGrids

        state => region%state(ng)
        grid  => region%grid(ng)
        input => grid%input

        ! ... initialize
        state%dt = input%dt

        ! ... In inviscid shock-capturing or inviscid LES, state%REinv = 0.0 
        ! ... gets rid of hyperviscosity or LES contribution.  Since either 
        ! ... artificial viscosity doesn't carry RE-factor, 
        ! ... we may conveniently manipulate its value to retain code 
        ! ... formalism, JKim 02/2009
        if (state%RE == 0.0_rfreal .and. input%tvCor_in_dt == TRUE) then
          state%REinv = 1.0_rfreal; state%PRinv = 1.0_rfreal
        end if ! state%RE

        ! ... inviscid timestep
        do k = 1, grid%nCells
          ibfac = grid%ibfac(k)
          if (ibfac > 0.0_rfreal) then

            ! ... initialize
            numer = 0.0_rfreal
            denom = 0.0_rfreal

            ! ... Cartesian velocity
            UU = 0.0_rfreal
            do i = 1, grid%ND
              UU(i) = state%dv(k,3) * state%cv(k,1+i) + grid%XI_TAU(k,i)
            end do
            do i = 1, grid%ND
              denom = denom + abs(DOT_PRODUCT(UU(1:grid%ND),grid%MT1(k,region%global%t2Map(i,1:grid%ND))))
            end do

            ! ... speed of sound
            snd_spd = sqrt(state%gv(k,1) * state%dv(k,1) * state%dv(k,3))

            dot_prod = 0.0_rfreal
            do i = 1, grid%ND
              dot_prod = dot_prod + DOT_PRODUCT(grid%MT1(k,region%global%t2Map(i,1:grid%ND)), grid%MT1(k,region%global%t2Map(i,1:grid%ND)))
            end do
            denom = denom + snd_spd * sqrt(dot_prod)

            ! ... put it all together
            dt_inv = grid%INVJAC(k) / (denom * ibfac + (1.0 - ibfac) * 1E-10) * ibfac
            dt_vis = dt_inv

            ! ... for the viscous terms (taken from FDL3DI)
            if (state%RE > 0.0_rfreal .OR. input%tvCor_in_dt == TRUE) then

              nu_star = max(state%tv(k,1)*state%REinv, state%tv(k,2)*state%REinv, state%tv(k,3)*state%REinv*state%PRinv)
              dot_prod = 0.0_rfreal
              do i = 1, grid%ND
                dot_prod = dot_prod + sqrt(DOT_PRODUCT(grid%MT1(k,region%global%t2Map(i,1:grid%ND)),grid%MT1(k,region%global%t2Map(i,1:grid%ND))))
              end do
              denom = denom + 2.0_rfreal * nu_star * grid%JAC(k) * dot_prod * dot_prod

              dt_vis = grid%INVJAC(k) / (denom * ibfac + (1.0 - ibfac) * 1E-10) * ibfac

            end if

            ! ... the cfl
            state%cfl(k) = state%dt(k) / MIN(dt_inv,dt_vis) * ibfac
          else
            state%cfl(k) = -999.0_DP
          end if

        end do

        ! ... save the maximum cfl for this grid
        dt_local(ng) = MAXVAL(state%cfl)

        ! ... state%REinv should be recovered in inviscid limit, JKim 02/2009
        if (state%RE == 0.0_rfreal .and. input%tvCor_in_dt == TRUE) then
          state%REinv = 0.0_rfreal; state%PRinv = 1.0_rfreal / state%PR
        end if ! state%RE

      end do

      ! ... find the maximum cfl over all nodes
      var = MAXVAL(dt_local)
      if (input%caseID == 'SCALING_NO_COMM') then
        var_all = var
      else
        Call MPI_AllReduce(var, var_all, 1, MPI_DOUBLE_PRECISION, MPI_MAX, mycomm, ierr)
      end if

      ! ... copy the cfl back to all the nodes
      do ng = 1, region%nGrids
        grid => region%grid(ng)
        state => region%state(ng)
        do k = 1, grid%nCells
          state%cfl(k) = var_all
        end do
      end do

    end if

    deallocate(dt_local)

  end subroutine NS_Timestep

  subroutine NS_RHS_ViscousTerms_CARTESIAN(region, ng, flags, VelGrad, TempGrad, tvCor)

    USE ModGlobal
    USE ModDataStruct
    USE ModDeriv
    USE ModMPI

    Implicit None

    ! ... subroutine arguments
    real(rfreal), pointer :: VelGrad(:,:), div(:), divTau(:,:), TempGrad(:,:)
    real(rfreal), pointer, optional :: tvCor(:,:) ! JKim 01/2008

    ! ... local variables
    type(t_grid), pointer :: grid
    type(t_mixt), pointer :: state
    type(t_mixt_input), pointer :: input
    type(t_region), pointer :: region
    type(t_patch), pointer :: patch
    integer :: flags
    real(rfreal), allocatable :: UVW(:,:)
    integer :: ND, Nc, N(MAX_ND), ng, i, j, Np(MAX_ND), patchID
    integer :: ip, jp, kp

    ! ... local variables and arrays for viscous and heat fluxes, JKim 02/2007
    integer :: k, l, m, p
    real(rfreal) :: viscous_time
    real(rfreal), pointer :: VelGrad2nd(:,:,:), TempGrad2nd(:,:)
    real(rfreal), pointer :: MTProd(:,:), MTProdBKUP(:,:) ! JKim 01/2008
    real(rfreal), pointer :: tv(:,:)

    ! ... simplicity
    grid  => region%grid(ng)
    state => region%state(ng)
    input => grid%input
    ND = grid%ND
    Nc = grid%nCells
    N  = 1
    do j = 1, grid%ND
      N(j) = grid%ie(j)-grid%is(j)+1
    end do ! j

    if (flags == INVISCID_ONLY) return ! because it's inviscid computation

    if (state%RE == 0.0_rfreal .AND. input%shock == 0 .AND. input%LES == 0) then
      call graceful_exit(region%myrank, 'At least either shock-capturing or LES should be on to compute viscous terms.')
    end if ! state%RE

    ! ... allocate space for arrays
! nullify(flux); allocate(flux(Nc)); flux = 0.0_rfreal
! nullify(dflux); allocate(dflux(Nc)); dflux = 0.0_rfreal
    allocate(UVW(Nc,ND))
    do j = 1, ND
      do i = 1, Nc
        UVW(i,j) = 0.0_rfreal
      end do ! i
    end do ! j

    ! ... Step 1: viscosity correction is now considered
    ! ...         state%tv still not updated, JKim 01/2008
! nullify(tv); allocate(tv(Nc,input%nTv)); tv(:,:) = 0.0_rfreal  ! ... JKim 06/2008
! tv(:,1:input%nTv) = state%tv(:,1:input%nTv)                    ! ... JKim 06/2008
! if (PRESENT(tvCor) .eqv. .true.) then
!   tv(:,1:input%nTv) = tv(:,1:input%nTv) + tvCor(:,1:input%nTv) ! ... JKim 06/2008
! end if ! PRESENT(tvCor)

    ! ... precompute primitive velocities and store in UVW(:,:).
    do l = 1, ND
      do i = 1, Nc
        UVW(i,l) = state%cv(i,region%global%vMap(l+1)) * state%dv(i,3)
      end do
    end do ! l

!!$    ! ... compute the velocity gradient tensor (in CARTESIAN coordinates)
!!$    do i = 1, ND
!!$
!!$      do k = 1, Nc
!!$        state%flux(k) = UVW(k,i)
!!$      end do
!!$
!!$      do j = 1, ND
!!$        state%dflux(:) = 0.0_rfreal
!!$        Call FIRST_DERIV(region, ng, j, state%flux, state%dflux, 1.0_rfreal, .FALSE.)
!!$
!!$        do k = 1, Nc
!!$          VelGrad(k,region%global%t2Map(i,j)) = state%dflux(k)
!!$        end do
!!$      end do
!!$
!!$    end do

    ! ... convert the velocity gradient tensor to (x,y,z) from (xi,eta,zeta)
    do i = 1, ND
      do j = 1, ND
        do k = 1, Nc
          VelGrad(k,region%global%t2Map(i,j)) = VelGrad(k,region%global%t2Map(i,j)) * grid%MT1(k,region%global%t2Map(j,j)) * grid%JAC(k)
        end do
      end do
    end do

    ! ... compute the divergence
    nullify(div); allocate(div(Nc))
    do i = 1, Nc
      div(i) = 0.0_rfreal
    end do ! i
    do i = 1, ND
      do j = 1, Nc
        div(j) = div(j) + VelGrad(j,region%global%t2Map(i,i))
      end do
    end do

    ! ... allocate space for div_j tau_{ij}
    nullify(divTau); allocate(divTau(Nc,ND))
    do j = 1, ND
      do i = 1, Nc
        divTau(i,j) = 0.0_rfreal
      end do ! i
    end do ! j

    ! ... store viscous fluxes on the boundary
    Call Save_Patch_Viscous_Flux(region, ng, 0, 0, state%flux) ! zero things out first

    do i = 1, ND
      do j = 1, ND

! momentum: (rho u_i)_t = ... (\mu { (u_i)_j + (u_j)_i })_j + (\lambda (u_k)_k)_i
        do k = 1, Nc
          state%flux(k) = state%tv(k,1) * (VelGrad(k,region%global%t2Map(i,j)) + VelGrad(k,region%global%t2Map(j,i))) * state%REinv &
                        + region%global%delta(i,j) * state%tv(k,2) * div(k) * state%REinv
        end do
        Call Save_Patch_Viscous_Flux(region, ng, i+1, j, state%flux)

! energy: (rho E)_t = ... [(\mu { (u_i)_j + (u_j)_i } + \lambda (u_k)_k \delta_{ij}) u_j]_i
        do k = 1, Nc
          state%flux(k) = state%flux(k) * UVW(k,j)
        end do
        Call Save_Patch_Viscous_Flux(region, ng, ND+2, i, state%flux)

      end do
    end do

    ! ... compute the divergence of the viscous stress tensor
    ! ... compute (dmu/dx_j (du_i/dx_j + du_j/dx_i)
    do i = 1, Nc
      state%flux(i) = state%tv(i,1)
    end do
    do j = 1, ND
      do i = 1, Nc
        state%dflux(i) = 0.0_rfreal
      end do ! i
      Call FIRST_DERIV(region, ng, j, state%flux, state%dflux, 1.0_rfreal, .FALSE.)
      do i = 1, ND
        do k = 1, Nc
          divTau(k,i) = divTau(k,i) + state%dflux(k) * (VelGrad(k,region%global%t2Map(i,j)) + VelGrad(k,region%global%t2Map(j,i)))
        end do
      end do
    end do

    ! ... compute (dlambda/dx_i div)
    do i = 1, Nc
      state%flux(i) = state%tv(i,2)
    end do
    do i = 1, ND
      do k = 1, Nc
        state%dflux(k) = 0.0_rfreal
      end do ! k
      Call FIRST_DERIV(region, ng, i, state%flux, state%dflux, 1.0_rfreal, .FALSE.)
      do k = 1, Nc
        divTau(k,i) = divTau(k,i) + state%dflux(k) * div(k)
      end do
    end do

                                !!$    ! ... compute mu d2u_j/dx_idx_j + lambda d2u_k/dx_idx_k
                                !!$    do i = 1, Nc
                                !!$      state%flux(i) = div(i)
                                !!$    end do
                                !!$    do i = 1, ND
                                !!$      state%dflux(:) = 0.0_rfreal
                                !!$      call FIRST_DERIV(region, ng, i, state%flux, state%dflux, 1.0_rfreal, .FALSE.)
                                !!$      do k = 1, Nc
                                !!$        divTau(k,i) = divTau(k,i) + (state%tv(k,1) + state%tv(k,2)) * state%dflux(k)
                                !!$      end do
                                !!$    end do

    ! ... compute mu d2u_j/dx_idx_j + lambda d2u_k/dx_idx_k
    do i = 1, ND
      do j = 1, ND
        do k = 1, Nc
          state%flux(k) = UVW(k,j)
          state%dflux(k) = 0.0_rfreal
        end do
        call SECOND_DERIV(region, ng, i, j, state%flux, state%dflux, 1.0_rfreal, .FALSE.)
        do k = 1, Nc
          divTau(k,i) = divTau(k,i) + (state%tv(k,1) + state%tv(k,2)) * state%dflux(k)
        end do
      end do
    end do

    ! ... compute mu d2u_i/dx_jdx_j
    do i = 1, ND
      do k = 1, Nc
        state%flux(k) = UVW(k,i)
      end do
      do j = 1, ND
        do k = 1, Nc
          state%dflux(k) = 0.0_rfreal
        end do ! k
        call SECOND_DERIV(region, ng, j, j, state%flux, state%dflux, 1.0_rfreal, .FALSE.)
        do k = 1, Nc
          divTau(k,i) = divTau(k,i) + state%tv(k,1) * state%dflux(k)
        end do
      end do
    end do

    ! ... multiply by Reinv
    do i = 1, ND
      do k = 1, Nc
        divTau(k,i) = state%REinv * divTau(k,i)
      end do
    end do

    ! ... add in viscous terms to momentum equation
    do i = 1, ND
      do k = 1, Nc
        state%rhs(k,i+1) = state%rhs(k,i+1) + divTau(k,i) * grid%INVJAC(k)
      end do
    end do

    ! ... add in viscous terms to energy equation
    do i = 1, ND
      do k = 1, Nc
        state%rhs(k,ND+2) = state%rhs(k,ND+2) + divTau(k,i) * UVW(k,i)* grid%INVJAC(k)
      end do
      do j = 1, ND
        do k = 1, Nc
          state%flux(k) = state%tv(k,1) * (VelGrad(k,region%global%t2Map(i,j)) + VelGrad(k,region%global%t2Map(j,i))) + region%global%delta(i,j) * state%tv(k,2) * div(k)
          state%rhs(k,ND+2) = state%rhs(k,ND+2) + state%flux(k) * VelGrad(k,region%global%t2Map(i,j)) * state%REinv * grid%INVJAC(k)
        end do
      end do
    end do

    ! ... work on temperature-terms in energy equation
    do j = 1, ND
      do i = 1, Nc
        TempGrad(i,j) = 0.0_rfreal
      end do ! i
    end do ! j
    do i = 1, ND

      ! ... dk/dx_i dT/dx_i
      do k = 1, Nc
        state%flux(k) = state%dv(k,2); state%dflux(k) = 0.0_rfreal
      end do
      Call FIRST_DERIV(region, ng, i, state%flux, state%dflux, 1.0_rfreal, .FALSE.)
      do k = 1, Nc
        TempGrad(k,i) = state%dflux(k)
      end do

! energy: (rho E)_t = ... (k T_i)_i
      do k = 1, Nc
        state%flux(k) = state%tv(k,3) * TempGrad(k,i) * state%REinv * state%PRinv
      end do
      Call Save_Patch_Viscous_Flux(region, ng, ND+2, i, state%flux)

      do k = 1, Nc
        state%flux(k) = state%tv(k,3); state%dflux(k) = 0.0_rfreal
      end do
      Call FIRST_DERIV(region, ng, i, state%flux, state%dflux, 1.0_rfreal, .FALSE.)
      do k = 1, Nc
        state%rhs(k,ND+2) = state%rhs(k,ND+2) + TempGrad(k,i) * state%dflux(k) * grid%INVJAC(k) * state%REinv * state%PRinv
      end do

      ! ... k d2T/dx_idx_i
      do k = 1, Nc
        state%flux(k) = state%dv(k,2); state%dflux(k) = 0.0_rfreal
      end do
      Call SECOND_DERIV(region, ng, i, i, state%flux, state%dflux, 1.0_rfreal, .FALSE.)
      do k = 1, Nc
        state%rhs(k,ND+2) = state%rhs(k,ND+2) + state%tv(k,3) * state%dflux(k) * grid%INVJAC(k) * state%REinv * state%PRinv
      end do

    end do

    ! ... thermally coupled?  Save the temperature gradient on interacting patches
    if(input%QuasiSS == TRUE) then
      Call Save_Patch_TempGrad(region, ng, 0, state%flux) ! zero things out first
      do i = 1, ND
        do k = 1, Nc
          state%flux(k) = TempGrad(k,i)
        end do
        Call Save_Patch_TempGrad(region, ng, i, state%flux)
      end do
    end if

    ! ... deallocate
! deallocate(flux, dflux, tv)
    deallocate(UVW, div, divTau)

    return

  end subroutine NS_RHS_ViscousTerms_CARTESIAN

  subroutine NS_RHS_ViscousTerms_Strong(region, ng, flags, VelGrad, TempGrad, tvCor)

!   optimized by John L. Larson 8/20/2014


    USE ModGlobal
    USE ModDataStruct
    USE ModDeriv
    USE ModMPI

    Implicit None

    ! ... subroutine arguments
    integer :: ng
    integer :: flags
    real(rfreal), pointer :: VelGrad(:,:), TempGrad(:,:)
    real(rfreal), pointer, optional :: tvCor(:,:) ! JKim 01/2008
    type(t_region), pointer :: region

    ! ... local variables
    type(t_grid), pointer :: grid
    type(t_mixt), pointer :: state
    type(t_mixt_input), pointer :: input
    type(t_combustion), pointer :: combustion
    type(t_patch), pointer :: patch
    integer :: ND, Nc, N(MAX_ND), i, j, Np(MAX_ND), patchID
    integer :: ip, jp, kp, imassD, iheatD

    ! ... local variables and arrays for viscous and heat fluxes, JKim 02/2007
    integer :: k, l, m, p, ii, jj
!real(rfreal), pointer :: tv(:,:) ! ... JKim 06/2008 ...commented to avoid conflict with pointer dereference, WZhang 05/2014

    ! ... declarations for pointer dereference, WZhang 05/2014
    integer, pointer :: is(:), ie(:), t2Map(:,:), t2MapSym(:,:), delta(:,:)
    integer :: shock, LES, nauxVars, QuasiSS
    real(rfreal) :: RE, REinv, PRinv, SCinv
    real(rfreal), pointer :: cv(:,:), dv(:,:), flux(:), dflux(:), MT1(:,:), tv(:,:), JAC(:), rhs(:,:)
    real(rfreal), pointer :: auxVars(:,:), rhs_auxVars(:,:)
    real(rfreal), pointer :: tv2div(:), UVW(:,:), StrnRt(:,:), HeatFlux(:,:)
    real(rfreal), pointer :: flux1(:), flux2(:)
#ifdef AXISYMMETRIC
    real(rfreal), pointer :: MT0(:,:),JAC0(:),INVJAC(:),XYZ(:,:)
    real(rfreal) :: twoReInv, factor, drMin
#endif


    integer :: NDp2, ip1
    integer :: t2MapSymi1, t2MapSymi2, t2MapSymi3
    integer :: t2Mapl1, t2Mapl2, t2Mapl3
    integer :: deltai1, deltai2, deltai3

    real(rfreal) :: PRoverSC, REPRinv, RESCinv, dflux1, dflux2, dflux3, tvby2, Mw_m
    real(rfreal) :: viscous_time, PRoverSCMw
    logical :: addHeatContOfMassDiff
    real(rfreal), pointer :: Mwcoeff(:)

    ! ... simplicity
    grid  => region%grid(ng)
    state => region%state(ng)
    input => grid%input
    combustion => state%combustion
    ND = grid%ND
    Nc = grid%nCells
    N  = 1

    if (.not. region%nsrhsInit) then
      allocate(region%nsRHS)
      region%nsRHS%ncSz = 0
      region%nsrhsInit = .true.
    end if


    ! ... pointer dereference, WZhang 05/2014
    is      => grid%is
    ie      => grid%ie
    RE       = state%RE
    shock    = input%shock
    LES      = input%LES
    cv      => state%cv
    dv      => state%dv
    flux    => state%flux
    dflux   => state%dflux
    MT1     => grid%MT1
    JAC     => grid%JAC
    tv      => state%tv
    rhs     => state%rhs
    REinv    = state%REinv
    PRinv    = state%PRinv
    nAuxvars = input%nAuxvars
    auxvars => state%auxvars
    rhs_auxVars => state%rhs_auxVars
    SCinv    = state%SCinv
    QuasiSS  = input%QuasiSS
    t2MapSym => region%global%t2MapSym
    t2Map    => region%global%t2Map
    delta    => region%global%delta
#ifdef AXISYMMETRIC
    MT0     => grid%MT0
    JAC0    => grid%JAC0
    INVJAC      => grid%INVJAC
    XYZ         => grid%XYZ
    drMin = grid%drMin
#endif

    NDp2 = ND + 2
    REPRinv = REinv * PRinv
    RESCinv = REinv * SCinv

    PRoverSC = state%SCinv/state%PRinv

    do j = 1, ND
      N(j) = ie(j)-is(j)+1
    end do ! j

    if (flags == INVISCID_ONLY) return ! because it's inviscid computation

    if (RE == 0.0_rfreal .AND. shock == 0 .AND. LES == 0) then
      call graceful_exit(region%myrank, 'At least either shock-capturing or LES should be on to compute viscous terms.')
    end if ! state%RE

    if (Nc > region%nsRHS%ncSz) then
      if (region%nsRHS%ncSz > 0) deallocate(region%nsRHS%UVW, region%nsRHS%StrnRt, region%nsRHS%div, region%nsRHS%HeatFlux)

      ! ... Allocate arrays
      region%nsRHS%ncSz = Nc
      allocate(region%nsRHS%UVW(Nc,ND))
      allocate(region%nsRHS%StrnRt(Nc,ND*(ND+1)/2))
      allocate(region%nsRHS%div(Nc))
      allocate(region%nsRHS%HeatFlux(Nc,ND));
    endif

    ! ... Pointer dereference
    UVW      => region%nsRHS%UVW
    StrnRt   => region%nsRHS%StrnRt
    tv2div    => region%nsRHS%div
    HeatFlux => region%nsRHS%HeatFlux
    flux1    => region%nsRHS%flux1
    flux2    => region%nsRHS%flux2

    ! ... Step 2: precompute primitive velocities and store in UVW(:,:)
    if ( ND == 2 ) then

#ifdef ENABLE_SIMD
!DIR$ SIMD
#endif
      do ii = 1, Nc
        UVW(ii,1) = cv(ii,2) * dv(ii,3)
        UVW(ii,2) = cv(ii,3) * dv(ii,3)
      end do

    else if ( ND == 3 ) then

#ifdef ENABLE_SIMD
!DIR$ SIMD
#endif
      do ii = 1, Nc
        UVW(ii,1) = cv(ii,2) * dv(ii,3)
        UVW(ii,2) = cv(ii,3) * dv(ii,3)
        UVW(ii,3) = cv(ii,4) * dv(ii,3)
      end do

    end if

    ! ... Step 3: precompute strain-rate tensor using VelGrad

    CALL NS_RHS_StrnRt(region, grid, VelGrad, StrnRt)

    ! ... Step 4: compute the divergence

    if ( ND == 2 ) then

      do ii = 1, Nc
        tv2div(ii) = (StrnRt(ii,1) + StrnRt(ii,2))*tv(ii,2)
#ifdef AXISYMMETRIC
        if(XYZ(ii,1) <= DRmin) then
          tv2div(ii) = tv2div(ii) + StrnRt(ii,1)*tv(ii,2)
        else
          tv2div(ii) = tv2div(ii) + UVW(ii,1)*JAC(ii)/JAC0(ii)*tv(ii,2)
        endif
#endif
      end do

    else if ( ND == 3 ) then

#ifdef ENABLE_SIMD
!DIR$ SIMD
#endif
      do ii = 1, Nc
        tv2div(ii) = (StrnRt(ii,1) + StrnRt(ii,2) + StrnRt(ii,3))*tv(ii,2)
      end do

    end if

    ! ... store viscous fluxes on the boundary
    Call Save_Patch_Viscous_Flux(region, ng, 0, 0, flux) ! zero things out first
    Call Save_Patch_Viscous_RHS(region, ng, 0, flux)

    ! ... Step 5: compute heat-flux vector

    if ( ND == 2 ) then

#ifdef ENABLE_SIMD
!DIR$ SIMD
#endif
      do ii = 1, Nc
        HeatFlux(ii,1) = -tv(ii,3) * JAC(ii) *        &
             ( MT1(ii,1) * TempGrad(ii,1) &
             + MT1(ii,2) * TempGrad(ii,2) )

        HeatFlux(ii,2) = -tv(ii,3) * JAC(ii) *        &
             ( MT1(ii,3) * TempGrad(ii,1) &
             + MT1(ii,4) * TempGrad(ii,2) )
      end do

    else if ( ND == 3 ) then

#ifdef ENABLE_SIMD
!DIR$ SIMD
#endif
      do ii = 1, Nc
        HeatFlux(ii,1) = -tv(ii,3) * JAC(ii) *        &
             ( MT1(ii,1) * TempGrad(ii,1) &
             + MT1(ii,2) * TempGrad(ii,2) &
             + MT1(ii,3) * TempGrad(ii,3) )

        HeatFlux(ii,2) = -tv(ii,3) * JAC(ii) *        &
             ( MT1(ii,4) * TempGrad(ii,1) &
             + MT1(ii,5) * TempGrad(ii,2) &
             + MT1(ii,6) * TempGrad(ii,3) )

        HeatFlux(ii,3) = -tv(ii,3) * JAC(ii) *        &
             ( MT1(ii,7) * TempGrad(ii,1) &
             + MT1(ii,8) * TempGrad(ii,2) &
             + MT1(ii,9) * TempGrad(ii,3) )
      end do

    end if

    ! ... Step 6: directly compute viscous terms in momentum equations
    do i = 1, ND
      ip1 = i + 1
      do l = 1, ND

        if ( ND == 2 ) then 

          t2MapSymi1 = t2MapSym(i,1)
          t2MapSymi2 = t2MapSym(i,2)
          t2Mapl1 = t2Map(l,1)
          t2Mapl2 = t2Map(l,2)
#ifndef AXISYMMETRIC
          deltai1 = delta(i,1)
          deltai2 = delta(i,2) 
#endif
#ifdef ENABLE_SIMD
!DIR$ SIMD
#endif
          do ii = 1, Nc                                         ! viscous stress tensor
#ifndef AXISYMMETRIC
            dflux1 =   deltai1 * tv2div(ii) + tv(ii,1) * 2.0_rfreal * StrnRt(ii,t2MapSymi1) 
            dflux2 =   deltai2 * tv2div(ii) + tv(ii,1) * 2.0_rfreal * StrnRt(ii,t2MapSymi2)
#else
            dflux1 =   tv(ii,1) * 2.0_rfreal * StrnRt(ii,t2MapSymi1) 
            dflux2 =   tv(ii,1) * 2.0_rfreal * StrnRt(ii,t2MapSymi2)
#endif
            flux(ii) = MT1(ii,t2Mapl1) * dflux1 + MT1(ii,t2Mapl2) * dflux2
          end do

        else if ( ND == 3 ) then

          t2MapSymi1 = t2MapSym(i,1)
          t2MapSymi2 = t2MapSym(i,2)
          t2MapSymi3 = t2MapSym(i,3)
          t2Mapl1 = t2Map(l,1)
          t2Mapl2 = t2Map(l,2)
          t2Mapl3 = t2Map(l,3)
          deltai1 = delta(i,1)
          deltai2 = delta(i,2)
          deltai3 = delta(i,3)   ! dflux() is not a function of l !!

#ifdef ENABLE_SIMD
!DIR$ SIMD
#endif
          do ii = 1, Nc                                         ! viscous stress tensor
            dflux1 =   deltai1 * tv2div(ii) + tv(ii,1) * 2.0_rfreal * StrnRt(ii,t2MapSymi1)
            dflux2 =   deltai2 * tv2div(ii) + tv(ii,1) * 2.0_rfreal * StrnRt(ii,t2MapSymi2)
            dflux3 =   deltai3 * tv2div(ii) + tv(ii,1) * 2.0_rfreal * StrnRt(ii,t2MapSymi3)
            flux(ii) = MT1(ii,t2Mapl1) * dflux1 &
                 + MT1(ii,t2Mapl2) * dflux2 &
                 + MT1(ii,t2Mapl3) * dflux3 
          end do

        end if

        call APPLY_OPERATOR_box(region, ng, 1, l, flux, dflux, .FALSE., 0)

        do ii = 1, Nc
#ifdef AXISYMMETRIC
          if(ND == 2 .and. l==1 .and. XYZ(ii,1) <= DRmin) dflux(ii)=0d0
#endif
          rhs(ii,ip1) = rhs(ii,ip1) + dflux(ii) * REinv
          flux(ii)    = flux(ii) * REinv
        end do
        Call Save_Patch_Viscous_Flux(region, ng, ip1, l, flux)

      end do ! l
   end do ! i


    ! ... Step 7: directly compute viscous terms in energy equations
    do l = 1,ND

      if ( ND == 2 ) then

        t2Mapl1 = t2Map(l,1)
        t2Mapl2 = t2Map(l,2)      ! dflux() is not a function of l !!

#ifdef ENABLE_SIMD
!DIR$ SIMD
#endif
        do ii = 1, Nc             ! viscous stress tensor
          tvby2   =   tv(ii,1) * 2.0_rfreal
          dflux1 =   UVW(ii,1) * (tv2div(ii) + tvby2 * StrnRt(ii,1)) &
               + UVW(ii,2) * (         tvby2 * StrnRt(ii,3))

          dflux2 =   UVW(ii,1) * (         tvby2 * StrnRt(ii,3)) &
               + UVW(ii,2) * (tv2div(ii) + tvby2 * StrnRt(ii,2))

          flux(ii) = MT1(ii,t2Mapl1) * dflux1  + MT1(ii,t2Mapl2) * dflux2
        end do

      else if ( ND == 3 ) then

        t2Mapl1 = t2Map(l,1)
        t2Mapl2 = t2Map(l,2)      
        t2Mapl3 = t2Map(l,3)      ! dflux() is not a function of l !!

#ifdef ENABLE_SIMD
!DIR$ SIMD
#endif
        do ii = 1, Nc             ! viscous stress tensor
          tvby2   =   tv(ii,1) * 2.0_rfreal
          dflux1 =   UVW(ii,1) * (tv2div(ii) + tvby2 * StrnRt(ii,1)) &
               + UVW(ii,2) * (         tvby2 * StrnRt(ii,4)) &
               + UVW(ii,3) * (         tvby2 * StrnRt(ii,5))

          dflux2 =   UVW(ii,1) * (         tvby2 * StrnRt(ii,4)) &
               + UVW(ii,2) * (tv2div(ii) + tvby2 * StrnRt(ii,2)) &
               + UVW(ii,3) * (         tvby2 * StrnRt(ii,6))

          dflux3 =   UVW(ii,1) * (         tvby2 * StrnRt(ii,5)) &
               + UVW(ii,2) * (         tvby2 * StrnRt(ii,6)) &
               + UVW(ii,3) * (tv2div(ii) + tvby2 * StrnRt(ii,3))

          flux(ii) = MT1(ii,t2Mapl1) * dflux1 &
               + MT1(ii,t2Mapl2) * dflux2 &
               + MT1(ii,t2Mapl3) * dflux3
        end do

      end if

      call APPLY_OPERATOR_box(region, ng, 1, l, flux, dflux, .FALSE., 0)

      do ii = 1, Nc
#ifdef AXISYMMETRIC
        if(ND == 2 .and.  l==1 .and. XYZ(ii,1) < drMin) dflux(ii)=0d0
#endif
        rhs(ii,NDp2) = rhs(ii,NDp2) + dflux(ii) * REinv
        flux(ii) = flux(ii) * REinv
      end do
      Call Save_Patch_Viscous_Flux(region, ng, NDp2, l, flux)
    end do ! l


#ifdef AXISYMMETRIC
    call NS_RHS_Axisymmetric_Hopital(ng, Nc, nAuxVars, ND, input%iFirstDeriv, region, grid, input, flux, dflux, MT0, JAC, JAC0, INVJAC, rhs, rhs_auxVars, cv, dv, auxVars, 1, REinv, REPRinv, RESCinv, tv, UVW, StrnRt, HeatFlux ,tv2div )
#endif

!LM> insert scalar diffusion in here. Use the UVW and StrnRt buffers; make sure their old values are not used again

    ! ... Step 8: compute scalar diffusion terms in scalar equations
    ! ... reuse UVW & StrnRt arrays
#ifdef HAVE_CANTERA
    if (input%chemistry_model == MODEL0) then
       addHeatContOfMassDiff = .true.
    else if (input%chemistry_model == MODEL1) then
       addHeatContOfMassDiff = .true.
    else
       addHeatContOfMassDiff = .false.
    end if
#endif

    do m = 1, nAuxVars

#ifdef HAVE_CANTERA
      If (addHeatContOfMassDiff) Then
        imassD = min(input%massDiffusivityIndex + m-1, ubound(tv,2))
        if (input%chemistry_model == MODEL1) then
           iheatD = min(input%heatDiffusivityIndex + m-1, ubound(tv,2))
           Mw_m = combustion%model1%canteraInterface%molW(m)
        end if
      Else
        imassD = 1
      End If
#else
      imassD = 1
#endif

      ! ... compute internal derivatives
      if (input%chemistry_model == MODEL1) then
#ifdef HAVE_CANTERA
#ifdef ENABLE_SIMD
!DIR$ SIMD
#endif
         do ii = 1, Nc
            flux(ii) = auxVars(ii,m) / cv(ii,1) * state%MolWeight(ii)/Mw_m
         end do
#endif
      else
#ifdef ENABLE_SIMD
!DIR$ SIMD
#endif
         do ii = 1, Nc
            flux(ii) = auxVars(ii,m) / cv(ii,1)
         end do
      end if

      do j = 1, ND
        call APPLY_OPERATOR_box(region, ng, 1, j, flux, dflux, .FALSE., TRUE)

#ifdef ENABLE_SIMD
!DIR$ SIMD
#endif
        do ii = 1, Nc
          StrnRt(ii,j) = dflux(ii)  !grad Y
        end do
      end do

      ! ... compose Cartesian fluxes dY/dx, dY/dy, dY/dz
      if ( ND == 2 ) then

#ifdef ENABLE_SIMD
!DIR$ SIMD
#endif
        do ii = 1, Nc
          UVW(ii,1) =   MT1(ii,1) * StrnRt(ii,1) &
               + MT1(ii,2) * StrnRt(ii,2)

          UVW(ii,2) =   MT1(ii,3) * StrnRt(ii,1) &
               + MT1(ii,4) * StrnRt(ii,2)
        end do

      else if ( ND == 3 ) then

#ifdef ENABLE_SIMD
!DIR$ SIMD
#endif
        do ii = 1, Nc
          UVW(ii,1) =   MT1(ii,1) * StrnRt(ii,1) &
               + MT1(ii,2) * StrnRt(ii,2) &
               + MT1(ii,3) * StrnRt(ii,3)

          UVW(ii,2) =   MT1(ii,4) * StrnRt(ii,1) &
               + MT1(ii,5) * StrnRt(ii,2) &
               + MT1(ii,6) * StrnRt(ii,3)

          UVW(ii,3) =   MT1(ii,7) * StrnRt(ii,1) &
               + MT1(ii,8) * StrnRt(ii,2) &
               + MT1(ii,9) * StrnRt(ii,3)
        end do

      end if

      do ii = 1, Nc
        flux(ii) = tv(ii,imassD) * JAC(ii)
      end do
      do j = 1, ND
        do ii = 1, Nc
          UVW(ii,j) = UVW(ii,j)* flux(ii)
        enddo
      enddo
      ! ...divide by PRinv, because below Heatflux is multiplied by it
      !... Note on sign:: HeatFlux is on the LHS => HeatContOfMassDiff = sum(rho_i v'_i h_i) = - rho (sum D_i \Grad Y_i h_i). Assuming that rho sum D_i \Grad Y_i = 0, rho D_N2 \Grad Y_N2 = -rho sum_1^(nsp-1) D_i \Grad Y_i => - rho (sum D_i \Grad Y_i h_i) = - rho (sum_1^{nsp-1} D_i \Grad Y_i (h_i-h_N2))
      
#ifdef HAVE_CANTERA
      if(addHeatContOfMassDiff)then
         if (input%chemistry_model == MODEL1) then
            do ii = 1, Nc
               flux(ii) = PRoverSC*tv(ii,iheatD)
            end do
            do j = 1, ND
               do ii = 1, Nc
                  HeatFlux(ii,j) = HeatFlux(ii,j) - UVW(ii,j)*flux(ii)
               enddo
            enddo
         else if (input%chemistry_model == MODEL0) then
            ! ... variable Cp
            Mwcoeff => combustion%Mwcoeff  !Mref/Mi-Mref/M_N2  !Mref is not necessarily M_N2
            PRoverSCMw = Mwcoeff(m)*PRoverSC
            do ii = 1, Nc
               flux(ii) = PRoverSCMw*dv(ii,2)
            end do
            do j = 1, ND
               do ii = 1, Nc
                  HeatFlux(ii,j) = HeatFlux(ii,j) - UVW(ii,j)*flux(ii)
               enddo
            enddo
         end if
      end if
#endif

      ! ... now take div of curvilinear fluxes
      do l = 1, ND

        if ( ND == 2 ) then

          t2Mapl1 = t2Map(l,1)
          t2Mapl2 = t2Map(l,2)

#ifdef ENABLE_SIMD
!DIR$ SIMD
#endif
          do ii = 1, Nc
            flux(ii) =   (MT1(ii,t2Mapl1) * UVW(ii,1) &
                 + MT1(ii,t2Mapl2) * UVW(ii,2))* RESCinv
          end do

        else if ( ND  == 3 ) then

          t2Mapl1 = t2Map(l,1)
          t2Mapl2 = t2Map(l,2)
          t2Mapl3 = t2Map(l,3)

#ifdef ENABLE_SIMD
!DIR$ SIMD
#endif
          do ii = 1, Nc
            flux(ii) =   (MT1(ii,t2Mapl1) * UVW(ii,1) &
                 + MT1(ii,t2Mapl2) * UVW(ii,2) &
                 + MT1(ii,t2Mapl3) * UVW(ii,3))* RESCinv
          end do

        end if

        call APPLY_OPERATOR_box(region, ng, 1, l, flux, dflux, .FALSE., 0)

        do ii = 1, Nc
#ifdef AXISYMMETRIC
          if(ND == 2 .and.  l==1 .and. XYZ(ii,1) < drMin) dflux(ii)=0d0
#endif
          rhs_auxVars(ii,m) = rhs_auxVars(ii,m) + dflux(ii)
        end do
        Call Save_Patch_Viscous_Flux(region, ng, NDp2 +m, l, flux)
        Call Save_Patch_Viscous_RHS(region,  ng,       m,    dflux)
      end do ! l



#ifdef AXISYMMETRIC
      call NS_RHS_Axisymmetric_Hopital(ng, Nc, nAuxVars, ND, input%iFirstDeriv, region, grid, input, flux, dflux, MT0, JAC, JAC0, INVJAC, rhs, rhs_auxVars, cv, dv, auxVars, 10+m, REinv, REPRinv, RESCinv, tv, UVW, StrnRt, HeatFlux ,tv2div )
#endif      
    end do ! m

    ! ... Step 9: directly compute heat transfer terms in energy equations
    do l = 1,ND

      if ( ND == 2 ) then

        t2Mapl1 = t2Map(l,1)
        t2Mapl2 = t2Map(l,2)

#ifdef ENABLE_SIMD
!DIR$ SIMD
#endif
        do ii = 1, Nc
          flux(ii) =   MT1(ii,t2Mapl1) * HeatFlux(ii,1) &
               + MT1(ii,t2Mapl2) * HeatFlux(ii,2)
        end do

      else if ( ND == 3 ) then

        t2Mapl1 = t2Map(l,1)
        t2Mapl2 = t2Map(l,2)
        t2Mapl3 = t2Map(l,3)

#ifdef ENABLE_SIMD
!DIR$ SIMD
#endif
        do ii = 1, Nc
          flux(ii) =   MT1(ii,t2Mapl1) * HeatFlux(ii,1) &
               + MT1(ii,t2Mapl2) * HeatFlux(ii,2) &
               + MT1(ii,t2Mapl3) * HeatFlux(ii,3) 
        end do

      end if

      call APPLY_OPERATOR_box(region, ng, 1, l, flux, dflux, .FALSE., 0)

      do ii = 1, Nc
#ifdef AXISYMMETRIC
        if(ND == 2 .and.  l==1 .and. XYZ(ii,1) < drMin) dflux(ii)=0d0
#endif
        rhs(ii,NDp2) = rhs(ii,NDp2) - dflux(ii) * REPRinv
        flux(ii) = -flux(ii) * REPRinv
      end do
      Call Save_Patch_Viscous_Flux(region, ng, NDp2, l, flux)
    end do ! l


#ifdef AXISYMMETRIC
    call NS_RHS_Axisymmetric_Hopital(ng, Nc, nAuxVars, ND, input%iFirstDeriv, region, grid, input, flux, dflux, MT0, JAC, JAC0, INVJAC, rhs, rhs_auxVars, cv, dv, auxVars, 2, REinv, REPRinv, RESCinv, tv, UVW, StrnRt, HeatFlux ,tv2div )
#endif


    ! ... thermally coupled?  Save the temperature gradient on interacting patches
    if(QuasiSS == TRUE) then
      Call Save_Patch_TempGrad(region, ng, 0, flux) ! zero things out first
      do i = 1, ND

#ifdef ENABLE_SIMD
!DIR$ SIMD
#endif
        do k = 1, Nc
          flux(k) = TempGrad(k,i)
        end do
        Call Save_Patch_TempGrad(region, ng, i, flux)
      end do
    end if

    ! ... deallocate
    !deallocate(region%nsRHS%UVW, region%nsRHS%StrnRt, region%nsRHS%div, region%nsRHS%HeatFlux)
    !nullify(region%nsRHS%StrnRt)

    return

  end subroutine NS_RHS_ViscousTerms_Strong

  subroutine NS_RHS_ViscousTerms_Narrow(region, ng, flags, VelGrad, TempGrad, tvCor)

    USE ModGlobal
    USE ModDataStruct
    USE ModDeriv
    USE ModMPI

    Implicit None

    ! ... subroutine arguments
    integer :: ng
    integer :: flags
    real(rfreal), pointer :: VelGrad(:,:), TempGrad(:,:)
    real(rfreal), pointer, optional :: tvCor(:,:) ! JKim 01/2008
    type(t_region), pointer :: region

    ! ... local variables
    type(t_grid), pointer :: grid
    type(t_mixt), pointer :: state
    type(t_mixt_input), pointer :: input
    type(t_patch), pointer :: patch
    integer :: ND, Nc, N(MAX_ND), i, j, Np(MAX_ND), patchID
    integer :: ip, jp, kp, imassD

    ! ... local variables and arrays for viscous and heat fluxes, JKim 02/2007
    integer :: k, l, m, p, ii, jj, q
    real(rfreal) :: viscous_time
    real(rfreal), pointer :: tv(:,:) ! ... JKim 06/2008

    ! ... simplicity
    grid  => region%grid(ng)
    state => region%state(ng)
    input => grid%input
    ND = grid%ND
    Nc = grid%nCells
    N  = 1

    if (.not. region%nsrhsInit) then
      allocate(region%nsRHS)
      region%nsRHS%ncSz = 0
      region%nsrhsInit = .true.
    end if
    
    do j = 1, grid%ND
      N(j) = grid%ie(j)-grid%is(j)+1
    end do ! j

    if (flags == INVISCID_ONLY) return ! because it's inviscid computation

    if (state%RE == 0.0_rfreal .AND. input%shock == 0 .AND. input%LES == 0) then
      call graceful_exit(region%myrank, 'At least either shock-capturing or LES should be on to compute viscous terms.')
    end if ! state%RE

!    if (Nc > region%nsRHS%ncSz) then
 !     if (region%nsRHS%ncSz > 0) then
   
!      nullify(region%nsRHS%dflux1, region%nsRHS%dflux2, region%nsRHS%flux1)
  !    endif

      ! ... Allocate arrays
   !   region%nsRHS%ncSz = Nc
      allocate(region%nsRHS%UVW(Nc,ND))
      allocate(region%nsRHS%StrnRt(Nc,ND*(ND+1)/2))
      allocate(region%nsRHS%div(Nc))
      allocate(region%nsRHS%A(Nc),region%nsRHS%B(Nc))
      allocate(region%nsRHS%dflux1(Nc))
      allocate(region%nsRHS%dflux2(Nc))
      allocate(region%nsRHS%flux1(Nc))
      allocate(region%nsRHS%flux2(Nc))
      allocate(region%nsRHS%HeatFlux(Nc,ND))

   ! endif

    ! ... allocate space for arrays

    ! ... Step 2: precompute primitive velocities and store in region%nsRHS%UVW(:,:)
    do l = 1, ND
      do ii = 1, Nc
        region%nsRHS%UVW(ii,l) = state%cv(ii,region%global%vMap(l+1)) * state%dv(ii,3)
      end do
    end do ! l

    ! ... Step 3: precompute strain-rate tensor using VelGrad
    CALL NS_RHS_StrnRt(region, grid, VelGrad, region%nsRHS%StrnRt)

    ! ... Step 4: compute the divergence
    do ii = 1, Nc
      region%nsRHS%div(ii) = 0.0_rfreal
    end do
    do i = 1, ND
      do ii = 1, Nc
        region%nsRHS%div(ii) = region%nsRHS%div(ii) + region%nsRHS%StrnRt(ii,region%global%t2MapSym(i,i))
      end do
    end do

    ! ... store viscous fluxes on the boundary
    Call Save_Patch_Viscous_Flux(region, ng, 0, 0, state%flux) ! zero things out first

    ! ... Step 5: compute heat-flux vector
    do j = 1,ND
      do ii = 1, Nc
        region%nsRHS%HeatFlux(ii,j) = 0.0_rfreal ! defensive programming
      end do
      do k = 1,ND
        do ii = 1, Nc
          region%nsRHS%HeatFlux(ii,j) = region%nsRHS%HeatFlux(ii,j) + grid%MT1(ii,region%global%t2Map(k,j)) * TempGrad(ii,k)
        end do
      end do ! k
      do ii = 1, Nc
        region%nsRHS%HeatFlux(ii,j) = -state%tv(ii,3) * grid%JAC(ii) * region%nsRHS%HeatFlux(ii,j)
      end do
    end do ! j

    ! ... Step 6: directly compute viscous terms in momentum equations
    do i = 1, ND
      do l = 1, ND
        do ii = 1, Nc
          state%flux(ii) = 0.0_rfreal
        end do

        do j = 1,ND
          do ii = 1, Nc
            state%dflux(ii) = state%tv(ii,2) * region%global%delta(i,j) * region%nsRHS%div(ii) + & ! viscous stress tensor
                              state%tv(ii,1) * 2.0_rfreal * region%nsRHS%StrnRt(ii,region%global%t2MapSym(i,j))
            state%flux(ii) = state%flux(ii) + grid%MT1(ii,region%global%t2Map(l,j)) * state%dflux(ii)
          end do
        end do ! j
        call APPLY_OPERATOR(region, ng, 1, l, state%flux, state%dflux, .FALSE.)


         call variable_coeff_general(region, ng, l, l, region%nsRHS%A, i, i) ! Return A_jlpq
         !... wide
         
         ! ... A- derivative, momentum equation
         do ii = 1, Nc
            region%nsRHS%flux1(ii) = region%nsRHS%A(ii) * VelGrad(ii,region%global%t2Map(i,l)) 
         end do
  
         call APPLY_OPERATOR_WO_BC(region, ng, 1, l, region%nsRHS%flux1, region%nsRHS%dflux1, .FALSE.)
         call kernel(region, ng, l, l, region%nsRHS%A, region%nsRHS%UVW(:,i), region%nsRHS%dflux2) ! Calculated values are in the state%dflux

        do ii = 1, Nc
          state%rhs(ii,i+1) = state%rhs(ii,i+1) + ( state%dflux(ii) - region%nsRHS%dflux1(ii) + region%nsRHS%dflux2(ii) ) * state%REinv
          state%flux(ii)    = state%flux(ii) * state%REinv
        end do
        Call Save_Patch_Viscous_Flux(region, ng, i+1, l, state%flux)

      end do ! l
    end do ! i

    ! ... Step 7: directly compute viscous terms in energy equations
    do l = 1,ND
      do ii = 1, Nc
        state%flux(ii) = 0.0_rfreal
      end do
      do j = 1,ND
        do ii = 1, Nc
          state%dflux(ii) = 0.0_rfreal
        end do
        do k = 1,ND
          do ii = 1, Nc
            state%dflux(ii) = state%dflux(ii) + region%nsRHS%UVW(ii,k) * &
                      (state%tv(ii,2) * region%global%delta(j,k) * region%nsRHS%div(ii) + & ! viscous stress tensor
                       state%tv(ii,1) * 2.0_rfreal * region%nsRHS%StrnRt(ii,region%global%t2MapSym(j,k)))
          end do
        end do ! k
        do ii = 1, Nc
          state%flux(ii) = state%flux(ii) + grid%MT1(ii,region%global%t2Map(l,j)) * state%dflux(ii)
        end do
      end do ! j
      call APPLY_OPERATOR(region, ng, 1, l, state%flux, state%dflux, .FALSE.)
         call variable_coeff_general(region, ng, l, l, region%nsRHS%A, l, l) ! Return A_jlpq

            do ii = 1, Nc
               region%nsRHS%B(ii) = 0.0d0
            end do
          ! ... B - derivative, energy equation

	    do ii = 1, Nc 
               region%nsRHS%B(ii) = region%nsRHS%B(ii) + region%nsRHS%A(ii) * region%nsRHS%UVW(ii,l) ! to reuse A in the energy equation
            end do

            do ii = 1, Nc
               region%nsRHS%flux1(ii) = region%nsRHS%B(ii) * VelGrad(ii,region%global%t2Map(l,l)) 
            end do
          
           call APPLY_OPERATOR_WO_BC(region, ng, 1, l, region%nsRHS%flux1, region%nsRHS%dflux1, .FALSE.)
           
           call kernel(region, ng, l, l, region%nsRHS%B, region%nsRHS%UVW(:,l), region%nsRHS%dflux2 )

      do ii = 1, Nc
        state%rhs(ii,ND+2) = state%rhs(ii,ND+2) + ( state%dflux(ii) - region%nsRHS%dflux1(ii) + region%nsRHS%dflux2(ii) ) * state%REinv
        state%flux(ii) = state%flux(ii) * state%REinv
      end do
      Call Save_Patch_Viscous_Flux(region, ng, ND+2, l, state%flux)
    end do ! l


    ! ... Step 8: directly compute heat transfer terms in energy equations
    do l = 1,ND
      do ii = 1, Nc
        state%flux(ii) = 0.0_rfreal
      end do
      do j = 1,ND
        do ii = 1, Nc
          state%flux(ii) = state%flux(ii) + grid%MT1(ii,region%global%t2Map(l,j)) * region%nsRHS%HeatFlux(ii,j)
        end do
      end do ! j
      call APPLY_OPERATOR(region, ng, 1, l, state%flux, state%dflux, .FALSE.)

    
	   ! ... Conduction derivative, energy equation
      	   call variable_coeff_general_temp(region, ng, l, l, region%nsRHS%A)
      	   do ii = 1, Nc
              region%nsRHS%flux1(ii) = region%nsRHS%A(ii) * TempGrad(ii,l)
      	   end do
     	   call APPLY_OPERATOR_WO_BC(region, ng, 1, l, region%nsRHS%flux1, region%nsRHS%dflux1, .FALSE.)
      
	   call kernel(region, ng, l, l, region%nsRHS%A, state%dv(:,2), region%nsRHS%dflux2)

      do ii = 1, Nc
        state%rhs(ii,ND+2) = state%rhs(ii,ND+2) +( - state%dflux(ii) - region%nsRHS%dflux1(ii) + region%nsRHS%dflux2(ii) ) * state%REinv * state%PRinv
        state%flux(ii) = -state%flux(ii) * state%REinv * state%PRinv
      end do
      Call Save_Patch_Viscous_Flux(region, ng, ND+2, l, state%flux)
    end do ! l







    ! ... Step 9: compute scalar diffusion terms in scalar equations
    ! ... reuse HeatFlux & StrnRt arrays
    do m = 1, input%nAuxVars
      imassD = min(input%massDiffusivityIndex + m -1, ubound(state%tv,2))
      do j = 1, ND
        do ii = 1, Nc
          region%nsRHS%HeatFlux(ii,j) = 0.0_rfreal
          region%nsRHS%StrnRt(ii,j)   = 0.0_rfreal
        end do
      end do
       do ii = 1, Nc
           region%nsRHS%flux2(ii)   = 0.0_rfreal
        end do
      ! ... compute internal derivatives
      do ii = 1, Nc
        state%flux(ii) = state%auxVars(ii,m) / state%cv(ii,1)
	region%nsRHS%flux2(ii) = state%flux(ii)
      end do
      do j = 1, ND
        call APPLY_OPERATOR(region, ng, 1, j, state%flux, state%dflux, .FALSE.)
        do ii = 1, Nc
          region%nsRHS%StrnRt(ii,j) = state%dflux(ii)
        end do
      end do

      ! ... compose Cartesian fluxes dY/dx, dY/dy, dY/dz
      do l = 1, ND
        do j = 1, ND
          do ii = 1, Nc
            region%nsRHS%HeatFlux(ii,l) = region%nsRHS%HeatFlux(ii,l) + grid%MT1(ii,region%global%t2Map(j,l)) * region%nsRHS%StrnRt(ii,j) * grid%JAC(ii)
          end do
        end do
      end do

      ! ... now take div of curvilinear fluxes
      do l = 1, ND
      	call variable_coeff_general_scalar(region, ng, l, l, region%nsRHS%A)

	do ii = 1, Nc
	   region%nsRHS%flux1(ii) = region%nsRHS%A(ii) * region%nsRHS%StrnRt(ii,l)
	end do

	call APPLY_OPERATOR_WO_BC(region, ng, 1, l, region%nsRHS%flux1, region%nsRHS%dflux1, .FALSE.)

        call kernel(region, ng, l, l, region%nsRHS%A, region%nsRHS%flux2, region%nsRHS%dflux2 )

        do ii = 1, Nc
          state%flux(ii) = 0.0_rfreal
        end do
        do j = 1, ND
          do ii = 1, Nc
            state%flux(ii) = state%flux(ii) + grid%MT1(ii,region%global%t2Map(l,j)) * state%tv(ii,1) * region%nsRHS%HeatFlux(ii,j)
          end do
        end do ! j
        call APPLY_OPERATOR(region, ng, 1, l, state%flux, state%dflux, .FALSE.)
        do ii = 1, Nc
   !       state%rhs_auxVars(ii,m) = state%rhs_auxVars(ii,m) + state%dflux(ii) * state%REinv * state%SCinv
          state%rhs_auxVars(ii,m) = state%rhs_auxVars(ii,m) + ( state%dflux(ii) - region%nsRHS%dflux1(ii) + region%nsRHS%dflux2(ii) ) * state%REinv * state%SCinv
          state%flux(ii) = state%flux(ii) * state%REinv * state%SCinv
        end do
        Call Save_Patch_Viscous_Flux(region, ng, ND+2 + m, l, state%flux)
      end do ! l
    end do ! m



    ! ... thermally coupled?  Save the temperature gradient on interacting patches
    if(input%QuasiSS == TRUE) then
      Call Save_Patch_TempGrad(region, ng, 0, state%flux) ! zero things out first
      do i = 1, ND
        do k = 1, Nc
          state%flux(k) = TempGrad(k,i)
        end do
        Call Save_Patch_TempGrad(region, ng, i, state%flux)
      end do
    end if


    ! ... deallocate
    !deallocate(region%nsRHS%UVW, region%nsRHS%StrnRt, region%nsRHS%div, region%nsRHS%HeatFlux)
    !nullify(region%nsRHS%StrnRt)
   deallocate(region%nsRHS%UVW, region%nsRHS%StrnRt, region%nsRHS%div, region%nsRHS%A, region%nsRHS%B, region%nsRHS%dflux1, region%nsRHS%dflux2, region%nsRHS%flux1, region%nsRHS%flux2, region%nsRHS%HeatFlux)
    return

  end subroutine NS_RHS_ViscousTerms_Narrow


  subroutine variable_coeff_general (region, ng, l, p, A, q, j)
    USE ModGlobal
    USE ModDataStruct
    USE ModMPI
    USE ModDeriv

    IMPLICIT NONE

    ! ... local variables
    real(rfreal) :: A(:)
    integer :: ng, Nc, ND
    type(t_region), pointer :: region
    type(t_grid), pointer :: grid
    type(t_mixt), pointer :: state
    type(t_mixt_input), pointer :: input
    integer :: i, j, l, p, q, ii

    ! ... simplicity
    grid  => region%grid(ng)
    state => region%state(ng)
    input => grid%input
    ND = grid%ND
    Nc = grid%nCells

                                !!$    do ii = 1, Nc
                                !!$
                                !!$       A(ii) = 1
                                !!$    end do
                                !!$
    do ii = 1, Nc      ! initialization
      A(ii) = 0.0d0
    end do ! ii

    ! ... Diagonal term
    if(q == j) then
       do ii = 1, Nc
          do i = 1, ND
             A(ii) = A(ii) + grid%MT1(ii, region%global%t2Map(l,i)) * grid%MT1(ii, region%global%t2Map(p,i))
          end do                ! i
       end do! ii
    end if
    do ii = 1, Nc
       A(ii) = A(ii) + grid%MT1(ii, region%global%t2Map(l,q)) * grid%MT1(ii, region%global%t2Map(p,j))
       A(ii) = state%tv(ii,1) * A(ii) ! multiplying viscosity
       A(ii) = A(ii) + state%tv(ii,2) * grid%MT1(ii, region%global%t2Map(l,j)) * grid%MT1(ii, region%global%t2Map(p,q)) ! multiplying lambda
       A(ii) = grid%JAC(ii) * A(ii)  ! Jacobian for strong conservative form 
    end do                           ! ii      
    RETURN
  end subroutine variable_coeff_general

  subroutine variable_coeff_general_temp (region, ng, l, p, A)
    USE ModGlobal
    USE ModDataStruct
    USE ModMPI
    USE ModDeriv

    IMPLICIT NONE

    ! ... local variables
    real(rfreal) :: A(:)
    integer :: ng, Nc, ND
    type(t_region), pointer :: region
    type(t_grid), pointer :: grid
    type(t_mixt), pointer :: state
    type(t_mixt_input), pointer :: input
    integer ::  i, l, p, ii

    ! ... simplicity
    grid  => region%grid(ng)
    state => region%state(ng)
    input => grid%input
    ND = grid%ND
    Nc = grid%nCells

                                !!$    do ii = 1, Nc
                                !!$       A(ii) = 1
                                !!$    end do
    do ii = 1, Nc      ! initializationx
      A(ii) = 0.0d0
    end do

    do ii = 1, Nc
       do i = 1, ND
          A(ii) = A(ii) + grid%MT1(ii, region%global%t2Map(l,i)) * grid%MT1(ii, region%global%t2Map(p,i))
       end do                   ! i
    end do
    do ii = 1, Nc
      A(ii) = state%tv(ii,3) * A(ii) ! for multiplying kappa
      A(ii) = grid%JAC(ii) * A(ii)  ! Jacobian for strong conservative form
    end do                      ! ii
    RETURN
  end subroutine variable_coeff_general_temp

  subroutine variable_coeff_general_scalar (region, ng, l, p, A)
    USE ModGlobal
    USE ModDataStruct
    USE ModMPI
    USE ModDeriv

    IMPLICIT NONE

    ! ... local variables
    real(rfreal) :: A(:)
    integer :: ng, Nc, ND
    type(t_region), pointer :: region
    type(t_grid), pointer :: grid
    type(t_mixt), pointer :: state
    type(t_mixt_input), pointer :: input
    integer ::  i, l, p, ii

    ! ... simplicity
    grid  => region%grid(ng)
    state => region%state(ng)
    input => grid%input
    ND = grid%ND
    Nc = grid%nCells

                                !!$    do ii = 1, Nc
                                !!$       A(ii) = 1
                                !!$    end do
    do ii = 1, Nc      ! initializationx
      A(ii) = 0.0d0
    end do

    do ii = 1, Nc
       do i = 1, ND
          A(ii) = A(ii) + grid%MT1(ii, region%global%t2Map(l,i)) * grid%MT1(ii, region%global%t2Map(p,i))
       end do                   ! i
    end do
    do ii = 1, Nc
      A(ii) = state%tv(ii,1) * A(ii) ! for multiplying kappa
      A(ii) = grid%JAC(ii) * A(ii)  ! Jacobian for strong conservative form
    end do                      ! ii
    RETURN
  end subroutine variable_coeff_general_scalar

  subroutine NS_SAT_Artificial_Dissipation (region, ng)

    USE ModGlobal
    USE ModDataStruct
    USE ModDeriv
    USE ModMPI

    Implicit None

    ! ... local variables
    type(t_grid), pointer :: grid
    type(t_mixt), pointer :: state
    type(t_mixt_input), pointer :: input
    type(t_region), pointer :: region
    real(rfreal), pointer :: SpectralRadius(:,:)
    integer :: ng, i, ii, var, dir, ND, Nc
    real(rfreal) :: delta_x, diss_power, timer

    ! ... start timer
    timer = MPI_WTime()

    ! ... simplicity
    grid  => region%grid(ng)
    state => region%state(ng)
    input => grid%input
    ND = grid%ND
    Nc = grid%nCells

    ! ... determine diss_power
                                !!$    Select Case ( input%spaceOrder )
                                !!$      Case ( 5 ) diss_power = 8.0_rfreal
                                !!$      Case ( 3 ) diss_power = 4.0_rfreal
                                !!$      Case ( 2 ) diss_power = 2.0_rfreal
                                !!$    End Select

    ! ... compute the spectral radius
    allocate(SpectralRadius(Nc,ND))
    Call NS_Spectral_Radius(region, ng, SpectralRadius)

    ! ... Loop over all variables
    Do var = 1, ND + 2

      ! ... Loop over each direction
      Do dir = 1, ND

        ! ... apply the DI operator first
        Do ii = 1, Nc
          state%flux(ii) = state%cv(ii,var)
        End Do
        Call APPLY_OPERATOR_box(region, ng, input%iSATArtDiss, dir, state%flux, state%dflux, .FALSE., 1)

        ! ... scale the result by the spectral radius
        Do ii = 1, Nc
          state%flux(ii) = state%dflux(ii) * SpectralRadius(ii,dir)
        End Do

        ! ... apply the DI^(Transpose) operator next
        Call APPLY_OPERATOR_box(region, ng, input%iSATArtDissSplit, dir, state%flux, state%dflux, .FALSE., 0)

        ! ... add the result to the rhs
        Do ii = 1, Nc

          ! ... construct the normalized metrics
          !XI_X = grid%MT1(ii,region%global%t2Map(dir,1)); XI_Y = 0.0_rfreal; XI_Z = 0.0_rfreal
          !if (ND >= 2) XI_Y = grid%MT1(ii,region%global%t2Map(dir,2))
          !if (ND == 3) XI_Z = grid%MT1(ii,region%global%t2Map(dir,3))

          ! ... multiply by the Jacobian
! delta_x = grid%INVJAC(ii) / sqrt(XI_X**2 + XI_Y**2 + XI_Z**2)
! state%rhs(ii,var) = state%rhs(ii,var) + input%amount_SAT_diss * (delta_x)**diss_power * state%dflux(ii)
          state%rhs(ii,var) = state%rhs(ii,var) + input%amount_SAT_diss * state%dflux(ii) * grid%INVJAC(ii)

        End Do

      End Do

    End Do

    ! ... clean up
    deallocate(SpectralRadius)

    ! ... stop timer
    region%mpi_timings(ng)%rhs_artdiss = region%mpi_timings(ng)%rhs_artdiss + (MPI_WTime() - timer)

  end subroutine NS_SAT_Artificial_Dissipation

  subroutine NS_Spectral_Radius(region, ng, SpectralRadius, order_in)

    USE ModGlobal
    USE ModDataStruct
    USE ModDeriv
    USE ModMPI

    Implicit None

    ! ... local variables
    type(t_grid), pointer :: grid
    type(t_mixt), pointer :: state
    type(t_mixt_input), pointer :: input
    type(t_region), pointer :: region
    type(t_patch), pointer :: patch
    real(rfreal), pointer :: SpectralRadius(:,:)
    integer :: ng, i, j, k, l0, ii, var, dir, ND, Nc, N(MAX_ND), Np(MAX_ND), npatch, l1, jj, sgn, normDir
    real(rfreal) :: rho, u, v, w, p, gam, en, a, rj11, rj12, rj13, rj21, rj22, rj23, rj31, rj32, rj33
    real(rfreal) :: sq1, sq2, sq3, ucon, vcon, wcon
    integer, allocatable :: inorm(:)
    integer :: length_inorm
    integer, optional :: order_in

    ! ... simplicity
    grid  => region%grid(ng)
    state => region%state(ng)
    input => grid%input
    ND = grid%ND
    Nc = grid%nCells

    ! ... Modeled after Magnus's code (it is not clear this is the right way to do this)
    SELECT CASE (ND)

    CASE (1)

      Do ii = 1, Nc

        gam = state%gv(ii,1)
        rho = state%cv(ii,1)
        u   = state%cv(ii,2) / rho
        en  = state%cv(ii,3) - 0.5_rfreal * rho * (u*u)
        p   = (gam - 1.0_rfreal) * en
        a   = sqrt(gam * p / rho)

        rj11 = grid%MT1(ii,region%global%t2Map(1,1))

        ucon = rj11 * u

        sq1  = sqrt( rj11 * rj11 )

        SpectralRadius(ii,1) = (abs(ucon) + a*sq1)

      End Do

    CASE (2) 

      Do ii = 1, Nc

        gam = state%gv(ii,1)
        rho = state%cv(ii,1)
        u   = state%cv(ii,2) / rho
        v   = state%cv(ii,3) / rho
        en  = state%cv(ii,4) - 0.5_rfreal * rho * (u*u + v*v)
        p   = (gam - 1.0_rfreal) * en
        a   = sqrt(gam * p / rho)

        rj11 = grid%MT1(ii,region%global%t2Map(1,1))
        rj12 = grid%MT1(ii,region%global%t2Map(1,2))

        rj21 = grid%MT1(ii,region%global%t2Map(2,1))
        rj22 = grid%MT1(ii,region%global%t2Map(2,2))

        ucon = rj11 * u + rj12 * v
        vcon = rj21 * u + rj22 * v

        sq1  = sqrt( rj11 * rj11 + rj12 * rj12 )
        sq2  = sqrt( rj21 * rj21 + rj22 * rj22 )

        SpectralRadius(ii,1) = (abs(ucon) + a*sq1)
        SpectralRadius(ii,2) = (abs(vcon) + a*sq2)

      End Do

    Case (3)

      Do ii = 1, Nc

        gam = state%gv(ii,1)
        rho = state%cv(ii,1)
        u   = state%cv(ii,2) / rho
        v   = state%cv(ii,3) / rho
        w   = state%cv(ii,4) / rho
        en  = state%cv(ii,5) - 0.5_rfreal * rho * (u*u + v*v + w*w)
        p   = (gam - 1.0_rfreal) * en
        a   = sqrt(gam * p / rho)

        rj11 = grid%MT1(ii,region%global%t2Map(1,1))
        rj12 = grid%MT1(ii,region%global%t2Map(1,2))
        rj13 = grid%MT1(ii,region%global%t2Map(1,3))

        rj21 = grid%MT1(ii,region%global%t2Map(2,1))
        rj22 = grid%MT1(ii,region%global%t2Map(2,2))
        rj23 = grid%MT1(ii,region%global%t2Map(2,3))

        rj31 = grid%MT1(ii,region%global%t2Map(3,1))
        rj32 = grid%MT1(ii,region%global%t2Map(3,2))
        rj33 = grid%MT1(ii,region%global%t2Map(3,3))

        ucon = rj11 * u + rj12 * v + rj13 * w
        vcon = rj21 * u + rj22 * v + rj23 * w
        wcon = rj31 * u + rj32 * v + rj33 * w

        sq1  = sqrt( rj11 * rj11 + rj12 * rj12 + rj13 * rj13 )
        sq2  = sqrt( rj21 * rj21 + rj22 * rj22 + rj23 * rj23 )
        sq3  = sqrt( rj31 * rj31 + rj32 * rj32 + rj33 * rj33 )

        SpectralRadius(ii,1) = (abs(ucon) + a*sq1)
        SpectralRadius(ii,2) = (abs(vcon) + a*sq2)
        SpectralRadius(ii,3) = (abs(wcon) + a*sq3)

      End Do

    End Select

    loop1 : Do npatch = 1, region%npatches

      patch => region%patch(npatch)
      grid  => region%grid(patch%gridID)
      state => region%state(patch%gridID)
      input => grid%input
      normDir = abs(patch%normDir)
      sgn = normDir / patch%normDir

      if (patch%gridID /= ng) CYCLE loop1

      ! ... the SpectralRadius should diminish near the boundaries
      If (present(order_in) .eqv. .true.) then
        SELECT CASE (order_in)
        CASE (5)
          allocate(inorm(2))    
        CASE DEFAULT
          allocate(inorm(1))
        END SELECT
      Else
        SELECT CASE (input%spaceOrder)
        CASE (5)
          allocate(inorm(2))    
        CASE DEFAULT
          allocate(inorm(1))
        END SELECT
      End If
      length_inorm = size(inorm)

      if ( length_inorm <= 0 ) CYCLE loop1

      N  = 1; Np = 1;
      do j = 1, grid%ND
        N(j)  = grid%ie(j)-grid%is(j)+1
        Np(j) = patch%ie(j) - patch%is(j) + 1
      end do;

      if ( patch%bcType /= SPONGE ) then

        Do k = patch%is(3), patch%ie(3)
          Do j = patch%is(2), patch%ie(2)
            Do i = patch%is(1), patch%ie(1)

              l0 = (k- grid%is(3))* N(1)* N(2) + (j- grid%is(2))* N(1) + (i- grid%is(1)+1)

              ! ... normal indices
              do jj = 1, length_inorm
                if (normDir == 1) then
                  l1 = l0 + sgn*(jj-1)
                else if (normDir == 2) then
                  l1 = l0 + sgn*(jj-1)*N(1)
                else
                  l1 = l0 + sgn*(jj-1)*N(1)*N(2)
                end if
                inorm(jj) = l1
              end do

              SpectralRadius(inorm(1:length_inorm),:) = 0.0_rfreal

            End Do
          End Do
        End Do

      end if

      Deallocate(inorm)

    end do loop1

  End Subroutine NS_Spectral_Radius

  SUBROUTINE LineAverage(myrank, grid, ToBeAveraged, dir)

    USE ModGlobal
    USE ModDataStruct
    USE ModMPI

    Implicit None

    ! ... subroutine arguments
    INTEGER :: myrank
    TYPE(t_grid), POINTER :: grid
    REAL(rfreal), POINTER :: ToBeAveraged(:)
    INTEGER :: dir

    ! ... local variables and arrays
    INTEGER :: i, j, k, l0, l0_onto, N(MAX_ND)

    ! ... depending on "dir" subroutine argument, line-averaging is performed
    ! ... uniform grid in the "dir" direction is assumed

    N  = 1
    DO j = 1,grid%ND
      N(j) = grid%ie(j)-grid%is(j)+1
    END DO ! j

    SELECT CASE( dir )
    CASE( 1 )
      DO k = grid%is(3), grid%ie(3)
        DO j = grid%is(2), grid%ie(2)
          DO i = grid%is(1), grid%ie(1)
            l0 = (k-grid%is(3))*N(1)*N(2) + &
                 (j-grid%is(2))*N(1) + &
                 (i-grid%is(1)+1)
            l0_onto = (k-grid%is(3))*N(1)*N(2) + &
                 (j-grid%is(2))*N(1) + 1
                                !(i-grid%is(1)+1)
            IF (l0_onto == l0) CYCLE
            ToBeAveraged(l0_onto) = ToBeAveraged(l0_onto) + ToBeAveraged(l0)
          END DO ! i
        END DO ! j
      END DO ! k
      DO k = grid%is(3), grid%ie(3)
        DO j = grid%is(2), grid%ie(2)
          DO i = grid%is(1), grid%ie(1)
            l0 = (k-grid%is(3))*N(1)*N(2) + &
                 (j-grid%is(2))*N(1) + &
                 (i-grid%is(1)+1)
            l0_onto = (k-grid%is(3))*N(1)*N(2) + &
                 (j-grid%is(2))*N(1) + 1
                                !(i-grid%is(1)+1)
            ToBeAveraged(l0) = ToBeAveraged(l0_onto)
          END DO ! i
        END DO ! j
      END DO ! k

    CASE( 2 )
      DO k = grid%is(3), grid%ie(3)
        DO j = grid%is(2), grid%ie(2)
          DO i = grid%is(1), grid%ie(1)
            l0 = (k-grid%is(3))*N(1)*N(2) + &
                 (j-grid%is(2))*N(1) + &
                 (i-grid%is(1)+1)
            l0_onto = (k-grid%is(3))*N(1)*N(2) + &
                                !(j-grid%is(2))*N(1) + &
                 (i-grid%is(1)+1)
            IF (l0_onto == l0) CYCLE
            ToBeAveraged(l0_onto) = ToBeAveraged(l0_onto) + ToBeAveraged(l0)
          END DO ! i
        END DO ! j
      END DO ! k
      DO k = grid%is(3), grid%ie(3)
        DO j = grid%is(2), grid%ie(2)
          DO i = grid%is(1), grid%ie(1)
            l0 = (k-grid%is(3))*N(1)*N(2) + &
                 (j-grid%is(2))*N(1) + &
                 (i-grid%is(1)+1)
            l0_onto = (k-grid%is(3))*N(1)*N(2) + &
                                !(j-grid%is(2))*N(1) + &
                 (i-grid%is(1)+1)
            ToBeAveraged(l0) = ToBeAveraged(l0_onto)
          END DO ! i
        END DO ! j
      END DO ! k

    CASE( 3 )
      DO k = grid%is(3), grid%ie(3)
        DO j = grid%is(2), grid%ie(2)
          DO i = grid%is(1), grid%ie(1)
            l0 = (k-grid%is(3))*N(1)*N(2) + &
                 (j-grid%is(2))*N(1) + &
                 (i-grid%is(1)+1)
            l0_onto = &!(k-grid%is(3))*N(1)*N(2) + &
                 (j-grid%is(2))*N(1) + &
                 (i-grid%is(1)+1)
            IF (l0_onto == l0) CYCLE
            ToBeAveraged(l0_onto) = ToBeAveraged(l0_onto) + ToBeAveraged(l0)
          END DO ! i
        END DO ! j
      END DO ! k
      DO k = grid%is(3), grid%ie(3)
        DO j = grid%is(2), grid%ie(2)
          DO i = grid%is(1), grid%ie(1)
            l0 = (k-grid%is(3))*N(1)*N(2) + &
                 (j-grid%is(2))*N(1) + &
                 (i-grid%is(1)+1)
            l0_onto = &!(k-grid%is(3))*N(1)*N(2) + &
                 (j-grid%is(2))*N(1) + &
                 (i-grid%is(1)+1)
            ToBeAveraged(l0) = ToBeAveraged(l0_onto)
          END DO ! i
        END DO ! j
      END DO ! k

    CASE DEFAULT
      CALL graceful_exit(myrank, "... wrong direction of line-averaging")

    END SELECT ! dir

    ToBeAveraged(:) = ToBeAveraged(:) / REAL(N(dir),rfreal)

  END SUBROUTINE LineAverage

!...AG...
SUBROUTINE LineAverage_ag(myrank, grid, ToBeAveraged1, ToBeAveraged2, dir, allN)

    USE ModGlobal
    USE ModDataStruct
    USE ModMPI

    Implicit None

    ! ... subroutine arguments
    INTEGER :: myrank
    TYPE(t_grid), POINTER :: grid
    REAL(rfreal), POINTER :: ToBeAveraged1(:), ToBeAveraged2(:)
    REAL(rfreal), allocatable :: Averaged(:,:,:)
    INTEGER :: dir, allN

    ! ... local variables and arrays
    INTEGER :: i, j, k, l0, N(MAX_ND)

    N  = 1
    DO j = 1,grid%ND
      N(j) = grid%ie(j)-grid%is(j)+1
    END DO ! j

    SELECT CASE( dir )
    CASE( 1 )
      allocate(Averaged(grid%ie(2)-grid%is(2)+1,grid%ie(3)-grid%is(3)+1,2))
      Averaged=0.0
      DO k = grid%is(3), grid%ie(3)
        DO j = grid%is(2), grid%ie(2)
          DO i = grid%is(1), grid%ie(1)
            l0 = (k-grid%is(3))*N(1)*N(2) + &
                 (j-grid%is(2))*N(1) + &
                 (i-grid%is(1)+1)
            Averaged(j-grid%is(2)+1,k-grid%is(3)+1,1)=Averaged(j-grid%is(2)+1,k-grid%is(3)+1,1)+ToBeAveraged1(l0)
            Averaged(j-grid%is(2)+1,k-grid%is(3)+1,2)=Averaged(j-grid%is(2)+1,k-grid%is(3)+1,2)+ToBeAveraged2(l0)
          END DO ! i
        END DO ! j
      END DO ! k
!if (grid%cartdims(dir) .gt. 1) call MPI_dir_avg(grid, Averaged, dir) !work only for even number of processor
      if (grid%cartdims(dir) .gt. 1) call pencil_dir_avg(grid, Averaged, dir)
      DO k = grid%is(3), grid%ie(3)
        DO j = grid%is(2), grid%ie(2)
          DO i = grid%is(1), grid%ie(1)
            l0 = (k-grid%is(3))*N(1)*N(2) + &
                 (j-grid%is(2))*N(1) + &
                 (i-grid%is(1)+1)
            ToBeAveraged1(l0) = Averaged(j-grid%is(2)+1,k-grid%is(3)+1,1)
            ToBeAveraged2(l0) = Averaged(j-grid%is(2)+1,k-grid%is(3)+1,2)
          END DO ! i
        END DO ! j
      END DO ! k
      deallocate(Averaged)
    CASE( 2 )
      allocate(Averaged(grid%ie(1)-grid%is(1)+1,grid%ie(3)-grid%is(3)+1,2))
      Averaged=0.0
      DO k = grid%is(3), grid%ie(3)
        DO j = grid%is(2), grid%ie(2)
          DO i = grid%is(1), grid%ie(1)
            l0 = (k-grid%is(3))*N(1)*N(2) + &
                 (j-grid%is(2))*N(1) + &
                 (i-grid%is(1)+1)
            Averaged(i-grid%is(1)+1,k-grid%is(3)+1,1)=Averaged(i-grid%is(1)+1,k-grid%is(3)+1,1)+ToBeAveraged1(l0)
            Averaged(i-grid%is(1)+1,k-grid%is(3)+1,2)=Averaged(i-grid%is(1)+1,k-grid%is(3)+1,2)+ToBeAveraged2(l0)
          END DO ! i
        END DO ! j
      END DO ! k
!if (grid%cartdims(dir) .gt. 1) call MPI_dir_avg(grid, Averaged, dir) !work only for even number of processor
      if (grid%cartdims(dir) .gt. 1) call pencil_dir_avg(grid, Averaged, dir)
      DO k = grid%is(3), grid%ie(3)
        DO j = grid%is(2), grid%ie(2)
          DO i = grid%is(1), grid%ie(1)
            l0 = (k-grid%is(3))*N(1)*N(2) + &
                 (j-grid%is(2))*N(1) + &
                 (i-grid%is(1)+1)
            ToBeAveraged1(l0) = Averaged(i-grid%is(1)+1,k-grid%is(3)+1,1)
            ToBeAveraged2(l0) = Averaged(i-grid%is(1)+1,k-grid%is(3)+1,2)
          END DO ! i
        END DO ! j
      END DO ! k
      deallocate(Averaged)
    CASE( 3 )
      allocate(Averaged(grid%ie(1)-grid%is(1)+1,grid%ie(2)-grid%is(2)+1,2))
      Averaged=0.0
      DO k = grid%is(3), grid%ie(3)
        DO j = grid%is(2), grid%ie(2)
          DO i = grid%is(1), grid%ie(1)
            l0 = (k-grid%is(3))*N(1)*N(2) + &
                 (j-grid%is(2))*N(1) + &
                 (i-grid%is(1)+1)
            Averaged(i-grid%is(1)+1,j-grid%is(2)+1,1)=Averaged(i-grid%is(1)+1,j-grid%is(2)+1,1)+ToBeAveraged1(l0)
            Averaged(i-grid%is(1)+1,j-grid%is(2)+1,2)=Averaged(i-grid%is(1)+1,j-grid%is(2)+1,2)+ToBeAveraged2(l0)
          END DO ! i
        END DO ! j
      END DO ! k
      !if (grid%cartdims(dir) .gt. 1) call MPI_dir_avg(grid, Averaged, dir) !work only for even number of processor
      if (grid%cartdims(dir) .gt. 1) call pencil_dir_avg(grid, Averaged, dir)
      DO k = grid%is(3), grid%ie(3)
        DO j = grid%is(2), grid%ie(2)
          DO i = grid%is(1), grid%ie(1)
            l0 = (k-grid%is(3))*N(1)*N(2) + &
                 (j-grid%is(2))*N(1) + &
                 (i-grid%is(1)+1)
            ToBeAveraged1(l0) = Averaged(i-grid%is(1)+1,j-grid%is(2)+1,1)
            ToBeAveraged2(l0) = Averaged(i-grid%is(1)+1,j-grid%is(2)+1,2)
          END DO ! i
        END DO ! j
      END DO ! k
      deallocate(Averaged)
    CASE DEFAULT
      CALL graceful_exit(myrank, "... wrong direction of line-averaging")
    END SELECT ! dir
    ToBeAveraged1(:) = ToBeAveraged1(:) / allN
    ToBeAveraged2(:) = ToBeAveraged2(:) / allN
  END SUBROUTINE LineAverage_ag

  SUBROUTINE MPI_dir_avg(grid, x, dir)
    USE ModGlobal
    USE ModDataStruct
    USE ModMPI
    Implicit None
    TYPE(t_grid), POINTER :: grid
    REAL(rfreal) :: x(:,:,:) 
    REAL(rfreal), dimension (:,:,:), allocatable :: xr
    INTEGER :: dir, i, count, stat, ierr
    count=size(x,1)*size(x,2)*size(x,3)
    do i = 0, grid%cartDims(dir)/2-2
      if (grid%cartCoords(dir)==i) call MPI_SEND(x, count, MPI_DOUBLE_PRECISION, grid%right_process(dir), 1, grid%cartComm, ierr)
      if (grid%cartCoords(dir)==(grid%cartDims(dir)-1)-i) call MPI_SEND(x, count, MPI_DOUBLE_PRECISION, grid%left_process(dir), 2, grid%cartComm, ierr)
      if (grid%cartcoords(dir)==i+1) then
        allocate (xr(size(x,1),size(x,2),size(x,3)))
        call MPI_RECV(xr,count, MPI_DOUBLE_PRECISION, grid%left_process(dir), 1, grid%cartcomm, stat, ierr)
        x=x+xr
        deallocate(xr)
      endif
      if (grid%cartcoords(dir)==(grid%cartDims(dir)-1)-i-1) then
        allocate (xr(size(x,1),size(x,2),size(x,3)))
        call MPI_RECV(xr,count, MPI_DOUBLE_PRECISION, grid%right_process(dir), 2, grid%cartcomm, stat, ierr)
        x=x+xr
        deallocate(xr)
      endif
    enddo
    i = grid%cartDims(dir)/2-1
    if (grid%cartcoords(dir) == i) call MPI_SEND(x, count, MPI_DOUBLE_PRECISION, grid%right_process(dir), 1, grid%cartcomm, ierr)
    if (grid%cartcoords(dir) == i+1) then
      allocate (xr(size(x,1),size(x,2),size(x,3)))
      call MPI_RECV(xr,count, MPI_DOUBLE_PRECISION, grid%left_process(dir), 1, grid%cartcomm, stat, ierr)
      x=x+xr
      deallocate(xr)
    endif
    do i = grid%cartDims(dir)/2,grid%cartDims(dir)-2
      if (grid%cartcoords(dir)==i) call MPI_SEND(x, count, MPI_DOUBLE_PRECISION, grid%right_process(dir), 1, grid%cartcomm, ierr)
      if (grid%cartcoords(dir)==(grid%cartDims(dir)-1)-i+1) call MPI_SEND(x, count, MPI_DOUBLE_PRECISION, grid%left_process(dir), 2, grid%cartcomm, ierr)
      if (grid%cartcoords(dir)==i+1) then
        allocate (xr(size(x,1),size(x,2),size(x,3)))
        call MPI_RECV(xr,count, MPI_DOUBLE_PRECISION,grid%left_process(dir), 1, grid%cartcomm, stat, ierr)
        x=xr
        deallocate(xr)
      endif
      if (grid%cartcoords(dir)==(grid%cartDims(dir)-1)-i) then
        allocate (xr(size(x,1),size(x,2),size(x,3)))
        call MPI_RECV(xr,count, MPI_DOUBLE_PRECISION, grid%right_process(dir), 2, grid%cartcomm, stat, ierr)
        x=xr
        deallocate(xr)
      endif
    enddo
    i=grid%cartDims(dir)-1
    if (grid%cartcoords(dir)==(grid%cartDims(dir)-1)-i+1) call MPI_SEND(x, count, MPI_DOUBLE_PRECISION, grid%left_process(dir), 2, grid%cartcomm, ierr)
    if (grid%cartcoords(dir)==(grid%cartDims(dir)-1)-i) then
      allocate (xr(size(x,1),size(x,2),size(x,3)))
      call MPI_RECV(xr,count, MPI_DOUBLE_PRECISION, grid%right_process(dir), 2, grid%cartcomm, stat, ierr)
      x=xr
      deallocate(xr)
    endif
!write(*,*) myrank, grid%cartComm, (grid%cartCoords(i),i=1,grid%ND), grid%left_process(dir), grid%right_process(dir)
  END SUBROUTINE MPI_dir_avg

  SUBROUTINE pencil_dir_avg(grid, x, dir)
    USE ModGlobal
    USE ModDataStruct
    USE ModMPI
    Implicit None
    TYPE(t_grid), POINTER :: grid
    REAL(rfreal) :: x(:,:,:) 
    REAL(rfreal), dimension (:,:,:), allocatable :: xr
    INTEGER :: dir, count, ierr

    count=size(x,1)*size(x,2)*size(x,3) 
    allocate (xr(size(x,1),size(x,2),size(x,3)))
    call MPI_ALLREDUCE(x,xr,count,MPI_DOUBLE_PRECISION,MPI_SUM,grid%pencilComm(dir),ierr) 
    x=xr
    deallocate(xr)
  END SUBROUTINE pencil_dir_avg
                                !...AG...

  Subroutine NS_RHS_Shock_Updated_Kawai_Lele(region, ng)

    USE ModGlobal
    USE ModDataStruct
    USE ModDeriv
    USE ModMPI

    Implicit None

    ! ... global variables
    type(t_region), pointer :: region
    integer :: ng

    ! ... local variables
    type(t_mixt_input), pointer :: input
    type(t_grid), pointer :: grid
    type(t_mixt), pointer :: state
    real(rfreal), pointer :: grad(:,:), div(:), disp(:,:,:), dtmp(:), vort_mag2(:), delta2(:,:), F(:), dF(:)
    real(rfreal) :: omega_x, omega_y, omega_z, gradU(3,3)
    integer :: i, j, k, Nc, ND, l
    real(rfreal) :: fac, f_sw

    ! ... simplicity
    grid  => region%grid(ng)
    state => region%state(ng)
    input => grid%input
    Nc    = grid%nCells
    ND    = grid%ND

    ! ... memory
    allocate(grad(Nc,ND), div(Nc), disp(Nc,ND,ND), dtmp(Nc), vort_mag2(Nc), delta2(Nc,ND))

    ! ... compute the grid displacement
    ! ... disp(:,i,j) = dx_i / dxi_j
    if (ND == 2) then
      do k = 1, Nc
        disp(k,1,1) =   grid%MT1(k,region%global%t2Map(2,2)) ! x_xi
        disp(k,1,2) = - grid%MT1(k,region%global%t2Map(1,2)) ! x_eta
        disp(k,2,1) = - grid%MT1(k,region%global%t2Map(2,1)) ! y_xi 
        disp(k,2,2) =   grid%MT1(k,region%global%t2Map(1,1)) ! y_eta
      end do
    else if (ND == 3) then      
      do k = 1, Nc
        fac = grid%JAC(k)
        disp(k,1,1) = fac * (grid%MT1(k,region%global%t2Map(2,2))*grid%MT1(k,region%global%t2Map(3,3)) - grid%MT1(k,region%global%t2Map(2,3))*grid%MT1(k,region%global%t2Map(3,2))) ! x_xi ! 
        disp(k,1,2) = fac * (grid%MT1(k,region%global%t2Map(1,3))*grid%MT1(k,region%global%t2Map(3,2)) - grid%MT1(k,region%global%t2Map(1,2))*grid%MT1(k,region%global%t2Map(3,3))) ! x_eta !
        disp(k,1,3) = fac * (grid%MT1(k,region%global%t2Map(1,2))*grid%MT1(k,region%global%t2Map(2,3)) - grid%MT1(k,region%global%t2Map(1,3))*grid%MT1(k,region%global%t2Map(2,2))) ! x_xeta ! 
        disp(k,2,1) = fac * (grid%MT1(k,region%global%t2Map(2,3))*grid%MT1(k,region%global%t2Map(3,1)) - grid%MT1(k,region%global%t2Map(2,1))*grid%MT1(k,region%global%t2Map(3,3))) ! y_xi ! 
        disp(k,2,2) = fac * (grid%MT1(k,region%global%t2Map(1,1))*grid%MT1(k,region%global%t2Map(3,3)) - grid%MT1(k,region%global%t2Map(1,3))*grid%MT1(k,region%global%t2Map(3,1))) ! y_eta ! 
        disp(k,2,3) = fac * (grid%MT1(k,region%global%t2Map(1,3))*grid%MT1(k,region%global%t2Map(2,1)) - grid%MT1(k,region%global%t2Map(1,1))*grid%MT1(k,region%global%t2Map(2,3))) ! y_xeta ! 
        disp(k,3,1) = fac * (grid%MT1(k,region%global%t2Map(2,1))*grid%MT1(k,region%global%t2Map(3,2)) - grid%MT1(k,region%global%t2Map(2,2))*grid%MT1(k,region%global%t2Map(3,1))) ! z_xi ! 
        disp(k,3,2) = fac * (grid%MT1(k,region%global%t2Map(1,2))*grid%MT1(k,region%global%t2Map(3,1)) - grid%MT1(k,region%global%t2Map(1,1))*grid%MT1(k,region%global%t2Map(3,2))) ! z_eta ! 
        disp(k,3,3) = fac * (grid%MT1(k,region%global%t2Map(1,1))*grid%MT1(k,region%global%t2Map(2,2)) - grid%MT1(k,region%global%t2Map(1,2))*grid%MT1(k,region%global%t2Map(2,1))) ! z_xeta !       
     end do
    end if

    If (input%HyperCbeta > 0.0_rfreal ) Then
      ! ... compute the divergence from the pre-computed velocity gradient tensor
      Call Compute_Divergence_From_VelGrad1st(region, grid, state, div)

      ! ... compute the vorticity from the pre-computed velocity gradient tensor
      If (ND >= 2) Call Compute_Vorticity_From_VelGrad1st(region, grid, state, grad)

      ! ... now compute the magnitude
      If (ND == 1) Then
        Do i = 1, Nc
          vort_mag2(i) = 0.0_rfreal
        End Do
      Else If (ND == 2) Then
        Do i = 1, Nc
          vort_mag2(i) = grad(i,1)**2
        End Do
      Else If (ND == 3) Then
        Do i = 1, Nc
          vort_mag2(i) = grad(i,1)**2 + grad(i,2)**2 + grad(i,3)**2
        End Do
      End If
    End If

    ! ...
    ! ... work on artificial viscosity for shear
    ! ...
    If (input%HyperCmu > 0.0_rfreal) Then

      ! ... compute the Delta_{l,mu}^2 term
      if (ND == 1) then
        do i = 1, Nc
          delta2(i,1) = disp(i,1,1)**2
        end do
      else if (ND == 2) then
        do i = 1, Nc
          delta2(i,1) = disp(i,1,1)**2 + disp(i,2,1)**2
          delta2(i,2) = disp(i,1,2)**2 + disp(i,2,2)**2
        end do
      else if (ND == 3) then
        do i = 1, Nc
          delta2(i,1) = disp(i,1,1)**2 + disp(i,2,1)**2 + disp(i,3,1)**2
          delta2(i,2) = disp(i,1,2)**2 + disp(i,2,2)**2 + disp(i,3,2)**2
          delta2(i,3) = disp(i,1,3)**2 + disp(i,2,3)**2 + disp(i,3,3)**2
        end do
      end if

      ! ... construct sum_l d{F_mu}/d{x_l} Delta_{l,mu}^2
      do i = 1, Nc
        dtmp(i) = 0.0_rfreal
      end do
      do l = 1, ND
        Call APPLY_OPERATOR(region, ng, input%iFourthDeriv, l, state%MagStrnRt, state%dflux, .FALSE.)
        do i = 1, NC
          dtmp(i) = dtmp(i) + state%dflux(i) * delta2(i,l)
        end do
      end do

      ! ... take absolute value and premultipy result by density
      do i = 1, Nc
        state%dflux(i) = state%cv(i,1) * dabs(dtmp(i))
      end do

      ! ... apply Truncated Gaussian Filter in each coordinate drection
      do j = 1, ND
        do i = 1, Nc
          state%flux(i) = state%dflux(i)
        end do
        Call APPLY_OPERATOR(region, ng, input%iFilterShockTG, j, state%flux, state%dflux, .FALSE.)
      end do

      ! ... save data
      do i = 1, Nc
        state%tvCor(i,1) = state%tvCor(i,1) + input%HyperCmu * state%dflux(i) * 1.0_rfreal/state%REinv
      end do
    End If

    ! ...
    ! ... now work on artificial diffusivity for bulk viscosity
    ! ...
    If (input%HyperCbeta > 0.0_rfreal) Then
      ! ... compute gradient of density in cartesian coordinates
      do i = 1, Nc
        grad(i,1:ND) = 0.0_rfreal
        state%flux(i) = state%cv(i,1)
      end do
      do j = 1, ND
        Call APPLY_OPERATOR(region, ng, 1, j, state%flux, state%dflux, .FALSE.)
        do k = 1, ND
          do i = 1, Nc
            grad(i,k) = grad(i,k) + state%dflux(i) * grid%MT1(i,region%global%t2Map(j,k))
          end do
        end do
      end do

      ! ... normalize
      Call Normalize_Vector(ND, Nc, grad)

      ! ... construct Delta_{l,beta}
      If (ND == 1) Then
        Do i = 1, Nc
          delta2(i,1) = (disp(i,1,1) * grad(i,1))**2
        End Do
      Else If (ND == 2) Then
        Do i = 1, Nc
          delta2(i,1) = (disp(i,1,1) * grad(i,1) + disp(i,2,1) * grad(i,2))**2
          delta2(i,2) = (disp(i,1,2) * grad(i,1) + disp(i,2,2) * grad(i,2))**2
        End Do
      Else If (ND == 3) Then
        Do i = 1, Nc
          delta2(i,1) = (disp(i,1,1) * grad(i,1) + disp(i,2,1) * grad(i,2) + disp(i,3,1) * grad(i,3))**2
          delta2(i,2) = (disp(i,1,2) * grad(i,1) + disp(i,2,2) * grad(i,2) + disp(i,3,2) * grad(i,3))**2 
          delta2(i,3) = (disp(i,1,3) * grad(i,1) + disp(i,2,3) * grad(i,2) + disp(i,3,3) * grad(i,3))**2 
        End Do
      End If

      ! ... initialize
      do i = 1, Nc
        dtmp(i) = 0.0_rfreal
      end do
      ! ... compute fourth derivative of beta detector function
      do l = 1, ND
        Call APPLY_OPERATOR(region, ng, input%iFourthDeriv, l, div, state%dflux, .FALSE.)
        do i = 1, NC
          dtmp(i) = dtmp(i) + state%dflux(i) * delta2(i,l)
        end do
      end do

      do i = 1, Nc
        f_sw = Heaviside(-div(i)) * (div(i)**2) / (div(i)**2 + vort_mag2(i) + TINY) 
        state%dflux(i) = state%cv(i,1) * f_sw * dabs(dtmp(i))
      end do

      ! ... apply Truncated Gaussian Filter in each coordinate direction
      do j = 1, ND
        do i = 1, Nc
          state%flux(i) = state%dflux(i)
        end do
        Call APPLY_OPERATOR(region, ng, input%iFilterShockTG, j, state%flux, state%dflux, .FALSE.)
      end do

      ! ... save data
      do i = 1, Nc
        state%tvCor(i,2) = state%tvCor(i,2) + input%HyperCbeta * state%dflux(i) * 1.0_rfreal/state%REinv
      end do
    End If

    ! ...
    ! ... finish with work on artificial diffusivity for thermal conductivity
    ! ...
    If (input%HyperCkappa > 0.0_rfreal) Then

      ! ... compute gradient of internal energy in cartesian coordinates
      do i = 1, Nc
        grad(i,1:ND) = 0.0_rfreal
        state%flux(i) = state%cv(i,ND+2)
      end do
      do j = 1, ND
        do i = 1, Nc
          state%flux(i) = state%flux(i) - 0.5_rfreal * state%cv(i,j+1) * state%cv(i,j+1) * state%dv(i,3)
        end do
      end do
      do j = 1, ND
        Call APPLY_OPERATOR(region, ng, 1, j, state%flux, state%dflux, .FALSE.)
        do k = 1, ND
          do i = 1, Nc
            grad(i,k) = grad(i,k) + state%dflux(i) * grid%MT1(i,region%global%t2Map(j,k))
          end do
        end do
      end do

      ! ... normalize
      Call Normalize_Vector(ND, Nc, grad)

      ! ... construct Delta_{l,kappa}
      If (ND == 1) Then
        Do i = 1, Nc
          delta2(i,1) = dabs(disp(i,1,1) * grad(i,1))
        End Do
      Else If (ND == 2) Then
        Do i = 1, Nc
          delta2(i,1) = dabs(disp(i,1,1) * grad(i,1) + disp(i,2,1) * grad(i,2))
          delta2(i,2) = dabs(disp(i,1,2) * grad(i,1) + disp(i,2,2) * grad(i,2))
        End Do
      Else If (ND == 3) Then
        Do i = 1, Nc
          delta2(i,1) = dabs(disp(i,1,1) * grad(i,1) + disp(i,2,1) * grad(i,2) + disp(i,3,1) * grad(i,3))
          delta2(i,2) = dabs(disp(i,1,2) * grad(i,1) + disp(i,2,2) * grad(i,2) + disp(i,3,2) * grad(i,3)) 
          delta2(i,3) = dabs(disp(i,1,3) * grad(i,1) + disp(i,2,3) * grad(i,2) + disp(i,3,3) * grad(i,3)) 
        End Do
      End If

      ! ... initialize
      do i = 1, Nc
        dtmp(i) = 0.0_rfreal
      end do
      ! ... compute fourth derivative of kappa detector function
      ! ... state%flux is already internal energy
      do l = 1, ND
        Call APPLY_OPERATOR(region, ng, input%iFourthDeriv, l, state%flux, state%dflux, .FALSE.)
        do i = 1, NC
          dtmp(i) = dtmp(i) + state%dflux(i) * delta2(i,l)
        end do
      end do

      ! ... take absolute value and premultipy by density, speed of sound, and inverse temperature
      do i = 1, Nc
        state%dflux(i) = state%cv(i,1) * dsqrt(state%gv(i,1)*state%dv(i,1)*state%dv(i,3)) / state%dv(i,2) * dabs(dtmp(i))
      end do

      ! ... apply Truncated Gaussian Filter in each direction 
      do j = 1, ND
        do i = 1, Nc
          state%flux(i) = state%dflux(i)
        end do
        Call APPLY_OPERATOR(region, ng, input%iFilterShockTG, j, state%flux, state%dflux, .FALSE.)
      end do

      ! ... save data
      do i = 1, Nc
        state%tvCor(i,3) = state%tvCor(i,3) + input%HyperCkappa * state%dflux(i) * 1.0_rfreal / (state%REinv * state%PRinv)
      end do
    End If

    ! ... clean up
    deallocate(grad, div, disp, dtmp, vort_mag2, delta2)

  End Subroutine NS_RHS_Shock_Updated_Kawai_Lele

  Subroutine NS_RHS_TemporalForcing(region, ng, str, nrm)

    USE ModGlobal
    USE ModDataStruct
    USE ModMetrics
    USE ModDeriv
    USE ModMPI
    USE ModIO

    IMPLICIT NONE

    Type(t_region), pointer :: region
    Integer :: str, nrm, ng

    ! ... local variables
    Type(t_grid), pointer :: grid
    Type(t_mixt), pointer :: state
    Type(t_mixt_input), pointer :: input
    Real(rfreal), pointer :: flux(:), dflux(:)
    Real(rfreal), pointer :: U(:), T(:)
    Real(rfreal), pointer :: UGrad(:,:), TGrad(:,:)
    Real(rfreal) :: REinv, PRinv, gam, gaml1
    Integer :: ND, Nc
    Integer :: j, i

    ! ... simplicity
    grid  => region%grid(ng)
    state => region%state(ng)
    input => grid%input
    ND    =  grid%ND
    Nc    =  grid%nCells
    REinv =  state%REinv
    PRinv =  state%PRinv
    gam   =  input%GamRef
    gaml1 =  gam - 1.0_rfreal

    ! ... allocate arrays
    allocate(flux(Nc), dflux(Nc))
    allocate(U(Nc), T(Nc))
    allocate(TGrad(Nc,ND), UGrad(Nc,ND))

    ! ... initialize
    UGrad(:,:) = 0.0_rfreal
    TGrad(:,:) = 0.0_rfreal
    flux(:)    = 0.0_rfreal
    dflux(:)   = 0.0_rfreal
    U(:)       = 0.0_rfreal
    T(:)       = 0.0_rfreal

    ! ... first precompute U_str from target data (assumed to be mean flow)
    do i = 1, Nc
      U(i) = state%cvTarget(i,str + 1) / state%cvTarget(i,1)
    end do

    ! ... second precompute T from the target data
    do i = 1, Nc
      T(i) = state%cvTarget(i,ND+2)
    end do

    do j = 1, ND
      do i = 1, Nc
        T(i) = T(i) - state%cvTarget(i,j+1)*state%cvTarget(i,j+1)/state%cvTarget(i,1)*0.5_rfreal
      end do
    end do

    do i = 1, Nc
      T(i) = T(i)*gam/state%cvTarget(i,1)
    end do

!    do i = 1, Nc
!       state%cv(i,1) = U(i)
!       state%cv(i,2) = T(i)
!    end do

!    call write_plot3d_file(region,'cv ')
!    call graceful_exit(region%myrank, 'done')

    ! ... third, calculate dU_str / d xi
    do j = 1, ND
      call APPLY_OPERATOR(region, ng , 1, j, U, dflux, .FALSE.)
      do i = 1, Nc
        UGrad(i,j) = dflux(i)
      end do
    end do

    ! ... fourth calculate dT / d xi
    do j = 1, ND
      call APPLY_OPERATOR(region, ng , 1, j, T, dflux, .FALSE.)
      do i = 1, Nc
        TGrad(i,j) = dflux(i)
      end do
    end do

    ! ... now we can begin calculating the forcing terms

    ! ... allocate RHS forcing term array
    allocate(state%RHSz(Nc,2))

    ! ... initialize
    state%RHSz(:,:) = 0.0_rfreal

    ! ... start with momentum term, -d/dx_nrm[mu*dU_str/dx_nrm]
    ! ... calculate dU_str/dx_nrm
    do j = 1, ND
       do i = 1, Nc
          state%RHSz(i,1) = state%RHSz(i,1) + UGrad(i,j)*grid%MT1(i, region%global%t2Map(j,nrm))
       end do
    end do

!    do i = 1, Nc
!       state%cv(i,1) = state%RHSz(i,1)
!       state%cv(i,2) = (T(i)*gaml1)**0.666_rfreal
!       state%cv(i,3) = U(i)
!    end do

!    call write_plot3d_file(region,'cv ')
!    call graceful_exit(region%myrank, 'done')


    ! ... for now, assume power law for viscosity
    do i = 1, Nc
      state%RHSz(i,1) = -1.0_rfreal*state%RHSz(i,1)*grid%Jac(i)*(T(i)*gaml1)**0.666_rfreal*REinv
    end do

    ! ... do energy term before finishing momentum term
    ! ... store dT/dx_nrm in Z2 temporarily
    do j = 1, ND
       do i = 1, Nc
          state%RHSz(i,2) = state%RHSz(i,2) - TGrad(i,j)*grid%MT1(i, region%global%t2Map(j,nrm))*grid%Jac(i)
       end do
    end do

    do i = 1, Nc
      flux(i) = (T(i)*gaml1)**0.666_rfreal * REinv*PRinv*state%RHSz(i,2) + state%RHSz(i,1)*U(i)
      state%RHSz(i,2) = 0.0_rfreal
    end do

    ! ... now finish energy term
    do j = 1, ND 
       call APPLY_OPERATOR(region, ng, 1, j, flux, dflux, .FALSE.)
       do i = 1, Nc
          state%RHSz(i,2) = state%RHSz(i,2) + dflux(i)*grid%MT1(i, region%global%t2Map(j,nrm))
       end do
    end do

    do i = 1, Nc
      state%RHSz(i,2) = state%RHSz(i,2)*grid%Jac(i)
    end do

    ! ... now finish momentum term
    do i = 1, Nc
      flux(i) = state%RHSz(i,1)
      state%RHSz(i,1) = 0.0_rfreal
    end do

    do j = 1, ND 
       call APPLY_OPERATOR(region, ng, 1, j, flux, dflux, .FALSE.)
       do i = 1, Nc
          state%RHSz(i,1) = state%RHSz(i,1) + dflux(i)*grid%MT1(i, region%global%t2Map(j,nrm))
       end do
    end do

    do i = 1, Nc
      state%RHSz(i,1) = state%RHSz(i,1)*grid%Jac(i)
    end do


    ! ... deallocate temporary arrays
    deallocate(U,T,flux,dflux,TGrad,UGrad)

  end Subroutine NS_RHS_TemporalForcing

  subroutine GetAverageHTFlux(region,AveHtFlux,RMSdHtFlux,MaxdHtFlux)
  
  USE ModGlobal
  USE ModDataStruct
  USE ModMetrics
  USE ModEOS
  USE ModDeriv
  USE ModIO
  USE ModMPI
  
  IMPLICIT NONE
  
  TYPE (t_region), POINTER :: region
  real(rfreal) :: AveHtFlux, RMSdHtFlux, MaxdHtFlux
  ! ... local variables
  type(t_patch), pointer :: patch
  type(t_grid), pointer :: grid
  type(t_mixt), pointer :: state
  type(t_mixt_input), pointer :: input

  integer :: count, count2, ierr
  integer :: N(3),Np(3),l0,i,j,k,ii,i2,j2,k2,npatch,lp,m,jj,l1,dir
  real(rfreal) :: NormFlux
  real(rfreal) :: factor, dsgn, gamma_inf, dHtFlux2, MaxdHtFluxLoc
  integer ::  normDir, sgn, ND

  real(rfreal) :: XI_X, XI_Y, XI_Z, denom1
  real(rfreal) :: XI_X_TILDE, XI_Y_TILDE, XI_Z_TILDE
  real(rfreal) :: SurfNorm(3), TempGradCart(3)
  real(rfreal), pointer :: FlxBufIn(:), FlxBufOut(:)

  ! ... dimensionalization factor
  ! ... In RocFlo-CM:
  ! ... q = -mu/(RePr)*dT/dx (all non-dimensional), so
  ! ... q = q*/[{RHORef}*({C_inf}*)^3] 
  ! ...   = q* / [(gamma_inf)(C_inf*)(PresRef*)]
  ! ... PresRef* on a case by case basis (Pa)
  ! ... C_inf* = gamma*R*T where R = 286.9 J/kg-K, free-stream values
  gamma_inf = region%input%GamRef

  factor = (gamma_inf*region%input%SndSpdRef*region%input%PresRef)
  
  allocate(FlxBufIn(3),FlxBufOut(3))

  count2 = 0
  FlxBufIn(:) = 0.0_rfreal
  FlxBufOut(:) = 0.0_rfreal
  AveHtFlux = 0.0_rfreal
  RMSdHtFlux = 0.0_rfreal
  dHtFlux2 = 0.0_rfreal
  MaxdHtFluxLoc = 0.0_rfreal
  
  ! .. number of dimensions
  ND = region%input%ND

    do npatch = 1, region%nPatches

          patch => region%patch(npatch)
          grid  => region%grid(patch%gridID)
          state => region%state(patch%gridID)

          do m = 1, grid%ND
            N(m)  = grid%ie(m) - grid%is(m) + 1
            Np(m) = patch%ie(m) - patch%is(m) + 1
          end do

          normDir = abs(patch%normDir)

          count = 0

          Do k = patch%is(3), patch%ie(3)
             Do j = patch%is(2), patch%ie(2)
                Do i = patch%is(1), patch%ie(1)
                   
                   l0 = (k-grid%is(3))*N(1)*N(2) + (j-grid%is(2))*N(1) + i-grid%is(1)+1
                   
                   lp = (k-patch%is(3))*Np(1)*Np(2) &
                        + (j-patch%is(2))*Np(1) + (i-patch%is(1)+1)
                   
                   ! ... construct the normalized metrics
                   XI_X = grid%MT1(l0,region%global%t2Map(normDir,1)); XI_Y = 0.0_rfreal; XI_Z = 0.0_rfreal
                   if (ND >= 2) XI_Y = grid%MT1(l0,region%global%t2Map(normDir,2))
                   if (ND == 3) XI_Z = grid%MT1(l0,region%global%t2Map(normDir,3))

                   ! ... multiply by the Jacobian
                   XI_X = XI_X * grid%JAC(l0)
                   XI_Y = XI_Y * grid%JAC(l0)
                   XI_Z = XI_Z * grid%JAC(l0)
                   denom1 = 1.0_rfreal/sqrt(XI_X**2+XI_Y**2+XI_Z**2)
                   XI_X_TILDE = XI_X * denom1
                   XI_Y_TILDE = XI_Y * denom1
                   XI_Z_TILDE = XI_Z * denom1
                   ! ... the unit normal is (XI_X_TILDE, XI_Y_TILDE, XI_Z_TILDE)
                   ! ... and points into the domain on + boundaries
                   ! ... and points out of the domain on - boundaries

                   ! ... now just need to take dot product of normal and temperature gradient
                   SurfNorm(:) = 0.0_rfreal
                   SurfNorm(1) = XI_X_TILDE
                   if (ND >= 2) SurfNorm(2) = XI_Y_TILDE
                   if (ND == 3) SurfNorm(3) = XI_Z_TILDE
                   
                   TempGradCart(:) = 0.0_rfreal
                   ! ... need to convert from computational to cartesian coordinates
                   do dir = 1, ND
                      if(grid%metric_type /= CARTESIAN) then
                         do m = 1, ND
                            TempGradCart(dir) = TempGradCart(dir) + patch%TempGrad(lp,m) * grid%MT1(l0,region%global%t2Map(m,dir))
                         end do
                         ! ... multiply by the Jacobian
                         TempGradCart(dir) = TempGradCart(dir) * grid%JAC(l0)
                      else
                         TempGradCart(dir) = patch%TempGrad(lp,dir)
                      end if
                         
                   end do

                   NormFlux = DOT_PRODUCT(TempGradCart(:),SurfNorm(:))

                   ! ... Heat flux leaving through this patch (going into thermal domain) q_out = k * (dt/dn) 
                   ! ... k*dT/dn (dimensional) = dT/dn[nd] *  1/(RePr) * k[nd] * (dimensionalization factor)
                   NormFlux = Normflux/(region%input%Re * region%input%Pr) * state%tv(l0,3)
                   
                   ! ... multiply by the non-dimensionalization factor
                   AveHtFlux = AveHtFlux + NormFlux*factor
                   
                   ! ... sum the squares
                   dHtFlux2 = (NormFlux*factor-patch%HtFlux(lp))

                   ! ... for inf norm
                   MaxdHtFluxLoc = max(MaxdHtFluxLoc,dHtFlux2)

                   dHtFlux2 = dHtFlux2*dHtFlux2

                   ! ... update the heat flux
                   patch%HtFlux(lp) = NormFlux*factor

                   ! ... for the purpose of outputing the surface heat flux
                   ! ... to a solution file
                   

!                   state%cv(l0,1) = patch%HtFlux(lp)

              RMSdHtFlux = RMSdHtFlux + dHtFlux2

              ! ... count of the number of points on this patch to be transferred
              count = count + 1            


            end do
          end do
        end do
!          AveHtFlux = AveHtFlux/dble(count) 
          count2 = count2 + count

  end do

  ! ... now communicate to rank 0
  FlxBufOut(1) = DBLE(count2)
  FlxBufOut(2) = AveHtFlux
  FlxBufOut(3) = RMSdHtFlux
 
  ! ... average and rms 
  call MPI_ALLREDUCE(FlxBufOut,FlxBufIn,3,MPI_DOUBLE_PRECISION,MPI_SUM,mycomm,ierr)
  
  ! ... inf norm
  call MPI_ALLREDUCE(MaxdHtFluxLoc,MaxdHtFlux,1,MPI_DOUBLE_PRECISION,MPI_MAX,mycomm,ierr)


  
!  if (region%myrank == 0) then
     AveHtFlux = FlxBufIn(2)/(FlxBufIn(1))
     RMSdHtFlux = sqrt(FlxBufIn(3))/FlxBufIn(1)
!  end if

    deallocate(FlxBufIn,FlxBufOut)
!call MPI_BARRIER(mycomm,ierr)

!  call write_plot3d_file(region,'cv ')
!  call graceful_exit(region%myrank, 'exiting after output of heat flux file') 

  end subroutine GetAverageHTFlux


  subroutine GetSurfRHS(region,MaxSurfRHS)
  
  USE ModGlobal
  USE ModDataStruct
  USE ModMetrics
  USE ModEOS
  USE ModDeriv
  USE ModIO
  USE ModMPI
  
  IMPLICIT NONE
  
  TYPE (t_region), POINTER :: region
  real(rfreal) ::  MaxSurfRHS
  ! ... local variables
  type(t_patch), pointer :: patch
  type(t_grid), pointer :: grid
  type(t_mixt), pointer :: state
  type(t_mixt_input), pointer :: input

  integer :: count,patchsize, count2, ierr
  integer :: N(3),Np(3),l0,i,j,k,ii,i2,j2,k2,npatch,lp,m,jj,l1,dir
  real(rfreal) :: MaxLocSurfRHS
  integer :: ND

  MaxLocSurfRHS = 0.0_rfreal
  
  ! .. number of dimensions
  ND = region%input%ND

    do npatch = 1, region%nPatches

      ! ... simplicity
      patch => region%patch(npatch)
      grid => region%grid(patch%gridID)
      state => region%state(patch%gridID)

      IF(patch%bcType == NSCBC_WALL_NOSLIP_THERMALLY_COUPLED .OR. &
           patch%bcType == SAT_WALL_NOSLIP_THERMALLY_COUPLED) then
        patchsize=1

        do m = 1, grid%ND
          N(m)  = grid%ie(m) - grid%is(m) + 1
          Np(m) = patch%ie(m) - patch%is(m) + 1
          patchsize = patchsize*Np(m)
        end do

        count = 1

        Do k = patch%is(3), patch%ie(3)
          Do j = patch%is(2), patch%ie(2)
            Do i = patch%is(1), patch%ie(1)

              l0 = (k-grid%is(3))*N(1)*N(2) + (j-grid%is(2))*N(1) + i-grid%is(1)+1

              lp = (k-patch%is(3))*Np(1)*Np(2) &
                   + (j-patch%is(2))*Np(1) + (i-patch%is(1)+1)

              ! ... also check for maximum surface residual
              do ii = 1, grid%ND+2
                MaxLocSurfRHS = max(abs(state%rhs(l0,ii)),MaxLocSurfRHS)
              enddo

              ! ... count of the number of points on this patch to be transferred
              count = count + 1            


            end do
          end do
        end do

      end if
    end do

    call MPI_ALLREDUCE(MaxLocSurfRHS,MaxSurfRHS,1,MPI_DOUBLE_PRECISION,MPI_MAX,mycomm,ierr)

!call MPI_BARRIER(mycomm,ierr)

  End subroutine GetSurfRHS

  subroutine NS_RHS_Euler_NASA(region, ng, flags)

    USE ModGlobal
    USE ModDataStruct
    USE ModDeriv
    USE ModMPI

    Implicit None

    ! ... local variables
    type(t_grid), pointer :: grid
    type(t_mixt), pointer :: state
    type(t_mixt_input), pointer :: input
    type(t_region), pointer :: region
    type(t_patch), pointer :: patch
    integer :: flags
    real(rfreal), allocatable :: UVWhat(:,:), dUdxi(:), dpdxi(:), dxix(:), dxit(:)
    integer :: ND, Nc, N(MAX_ND), ng, i, j, k, Np(MAX_ND), patchID
    integer :: ip, jp, kp, ii
    real(rfreal) :: source, norm(MAX_ND)

    ! ... simplicity
    grid  => region%grid(ng)
    state => region%state(ng)
    input => grid%input
    ND = grid%ND
    Nc = grid%nCells
    N  = 1
    do j = 1, grid%ND
      N(j) = grid%ie(j)-grid%is(j)+1
    end do ! j

    ! ... allocate space for arrays
    allocate(UVWhat(Nc,ND));
    allocate(dUdxi(Nc), dpdxi(Nc), dxix(Nc), dxit(Nc))
    do j = 1, ND
      do i = 1, Nc
        UVWhat(i,j) = 0.0_rfreal
      end do ! i
    end do ! j

    ! ... subtract out the time-dependent metric portion
    if (grid%moving == TRUE) then
      do k = 1, size(state%rhs,2)
        do ii = 1, Nc
          state%rhs(ii,k) = state%rhs(ii,k) - state%cv(ii,k) * grid%INVJAC_TAU(ii)
        end do
      end do ! k
    end if

    ! ... compute the contravariant velocities
    if (grid%metric_type /= CARTESIAN)  then
      do i = 1, ND
        do j = 1, ND
          do ii = 1, Nc
            UVWhat(ii,i) = UVWhat(ii,i) + grid%MT1(ii,region%global%t2Map(i,j)) * state%cv(ii,region%global%vMap(j+1)) ! JKim 01/2008
          end do
        end do ! j
        do ii = 1, Nc
          UVWhat(ii,i) = UVWhat(ii,i) * state%dv(ii,3) + grid%XI_TAU(ii,i)
        end do
      end do ! i
    else
      do i = 1, ND
        do ii = 1, Nc
          UVWhat(ii,i) = grid%MT1(ii,region%global%t2Map(i,i)) * state%cv(ii,region%global%vMap(i+1)) * state%dv(ii,3) + grid%XI_TAU(ii,i)
        end do
      end do ! i
    end if

    ! ... inviscid fluxes in the periodic xi direction
    ! ... compute the common terms dxi_x/dxi, dxi_t/dxi, dUhat/dxi, dp/dxi
    ! ... dxi_x/dxi
    do ii = 1, Nc
      state%flux(ii) = grid%MT1(ii,region%global%t2Map(1,1))
    end do
    call APPLY_OPERATOR(region, ng, input%iFirstDerivBiased, 1, state%flux, state%dflux, .FALSE.)
    do ii = 1, Nc
      dxix(ii) = state%dflux(ii)
    end do

    ! ... dxi_t/dxi
    do ii = 1, Nc
      state%flux(ii) = grid%XI_TAU(ii,1)
    end do
    call APPLY_OPERATOR(region, ng, input%iFirstDerivBiased, 1, state%flux, state%dflux, .FALSE.)
    do ii = 1, Nc
      dxit(ii) = state%dflux(ii)
    end do

    ! ... dp/dxi
    do ii = 1, Nc
      state%flux(ii) = state%dv(ii,1)
    end do
    call APPLY_OPERATOR(region, ng, input%iFirstDeriv, 1, state%flux, state%dflux, .FALSE.)
    do ii = 1, Nc
      dpdxi(ii) = state%dflux(ii)
    end do

    ! ... build dU/dxi
    ! ... dU/dxi = dxi_t/dxi + u * dxi_x/di + ...
    do ii = 1, Nc
      state%flux(ii) = state%cv(ii,2) * state%dv(ii,3)
      dUdxi(ii) = dxit(ii) + state%flux(ii) * dxix(ii)
    end do
    ! ... dU/dxi = ... + xi_x * du/dxi
    call APPLY_OPERATOR(region, ng, input%iFirstDeriv, 1, state%flux, state%dflux, .FALSE.)
    do ii = 1, Nc
      dUdxi(ii) = dUdxi(ii) + state%dflux(ii) * grid%MT1(ii,region%global%t2Map(1,1))
    end do

    ! ... dU/dxi = ... + xi_y * dv/dxi + xi_z * dw/dxi
    do j = 2, 3
      do ii = 1, Nc
        state%flux(ii) = state%cv(ii,j+1) * state%dv(ii,3)
      end do
      call APPLY_OPERATOR(region, ng, input%iFirstDeriv, 1, state%flux, state%dflux, .FALSE.)
      do ii = 1, Nc
        dUdxi(ii) = dUdxi(ii) + state%dflux(ii) * grid%MT1(ii,region%global%t2Map(1,j))
      end do
    end do

    ! ... dU/dxi = ... + v * dxi_y/dxi + w * dxi_z/dxi
    do j = 2, 3
      do ii = 1, Nc
        state%flux(ii) = grid%MT1(ii,region%global%t2Map(1,j))
      end do
      call APPLY_OPERATOR(region, ng, input%iFirstDerivBiased, 1, state%flux, state%dflux, .FALSE.)
      do ii = 1, Nc
        dUdxi(ii) = dUdxi(ii) + state%cv(ii,j+1) * state%dv(ii,3) * state%dflux(ii)
      end do
    end do

    ! ... inviscid fluxes in the periodic xi direction
    ! ... continuity
    do ii = 1, Nc
      state%flux(ii) = state%cv(ii,1)
    end do
    call APPLY_OPERATOR(region, ng, input%iFirstDeriv, 1, state%flux, state%dflux, .FALSE.)
    do ii = 1, Nc
      state%rhs(ii,region%global%vMap(1)) = state%rhs(ii,region%global%vMap(1)) - (state%cv(ii,1) * dUdxi(ii) + UVWhat(ii,1) * state%dflux(ii))
    end do

    ! ... momentum
    do j = 1, ND
      do ii = 1, Nc
        state%flux(ii) = state%cv(ii,j+1)
      end do
      call APPLY_OPERATOR(region, ng, input%iFirstDeriv, 1, state%flux, state%dflux, .FALSE.)
      do ii = 1, Nc
        state%rhs(ii,j+1) = state%rhs(ii,j+1) - ( state%cv(ii,j+1) * dUdxi(ii) + UVWhat(ii,1) * state%dflux(ii) + grid%MT1(ii,region%global%t2Map(j,1)) * dpdxi(ii) ) 
      end do
    end do

    ! ... x momentum
    do ii = 1, Nc
      state%rhs(ii,2) = state%rhs(ii,2) - ( state%dv(ii,3) * dxix(ii) )
    end do

    ! ... y,z momentum
    do j = 2, 3
      do ii = 1, Nc
        state%flux(ii) = grid%MT1(ii,region%global%t2Map(j,1))
      end do
      call APPLY_OPERATOR(region, ng, input%iFirstDerivBiased, 1, state%flux, state%dflux, .FALSE.)
      do ii = 1, Nc
        state%rhs(ii,j+1) = state%rhs(ii,j+1) - ( state%dv(ii,3) * state%dflux(ii) )
      end do
    end do

    ! ... energy
    do ii = 1, Nc
      state%flux(ii) = state%cv(ii,ND+2) + state%dv(ii,1)
    end do
    call APPLY_OPERATOR(region, ng, input%iFirstDeriv, 1, state%flux, state%dflux, .FALSE.)
    do ii = 1, Nc
      state%rhs(ii,ND+2) = state%rhs(ii,ND+2) - ( state%flux(ii) * dUdxi(ii) + UVWhat(ii,1) * state%dflux(ii) - grid%XI_TAU(ii,1) * dpdxi(ii) - state%dv(ii,3) * dxit(ii) )
    end do

    ! ... inviscid fluxes in the eta and zeta directions
    ! ... continuity 
    do i = 2, ND
      do ii = 1, Nc
        state%flux(ii) = state%cv(ii,region%global%vMap(1)) * UVWhat(ii,i)
      end do
      call APPLY_OPERATOR(region, ng, input%iFirstDeriv, i, state%flux, state%dflux, .FALSE.)
      do ii = 1, Nc
        state%rhs(ii,region%global%vMap(1)) = state%rhs(ii,region%global%vMap(1)) - state%dflux(ii)
      end do
      Call Save_Patch_Deriv(region, ng, 1, i, state%dflux)
    end do ! i

    ! ... momentum
    do i = 1, ND
      do j = 2, ND
        do ii = 1, Nc
          state%flux(ii) = state%cv(ii,region%global%vMap(i+1)) * UVWhat(ii,j) + grid%MT1(ii,region%global%t2Map(j,i)) * state%dv(ii,1)
        end do
        call APPLY_OPERATOR(region, ng, input%iFirstDeriv, j, state%flux, state%dflux, .FALSE.)
        do ii = 1, Nc
          state%rhs(ii,region%global%vMap(i+1)) = state%rhs(ii,region%global%vMap(i+1)) - state%dflux(ii)
        end do
        Call Save_Patch_Deriv(region, ng, i+1, j, state%dflux)
      end do ! j
    end do ! i

    ! ... energy
    do i = 2, ND
      do ii = 1, Nc
        state%flux(ii) = (state%cv(ii,region%global%vMap(ND+2)) + state%dv(ii,1)) * UVWhat(ii,i) - grid%XI_TAU(ii,i) * state%dv(ii,1)
      end do
      call APPLY_OPERATOR(region, ng, input%iFirstDeriv, i, state%flux, state%dflux, .FALSE.)
      do ii = 1, Nc
        state%rhs(ii,region%global%vMap(ND+2)) = state%rhs(ii,region%global%vMap(ND+2)) - state%dflux(ii)
      end do
      Call Save_Patch_Deriv(region, ng, ND+2, i, state%dflux)
    end do ! i

    ! ... scalar advection
    do k = 1, input%nAuxVars
      do i = 1, ND
        do ii = 1, Nc
          state%flux(ii) = state%auxVars(ii,k) * UVWhat(ii,i)
        end do
        call APPLY_OPERATOR(region, ng, input%iFirstDeriv, i, state%flux, state%dflux, .FALSE.)
        do ii = 1, Nc
          state%rhs_auxVars(ii,k) = state%rhs_auxVars(ii,k) - state%dflux(ii)
        end do
      end do
    end do

    ! ... deallocate
    deallocate(UVWhat)
    deallocate(dUdxi, dpdxi, dxix, dxit)

! CAA Benchmark 2, Category 1, Problem 1
    if (.false.) then
      do i = 1, Nc
        source = 0.1_dp * exp(-(log(2.0_dp))*( ((grid%XYZ(i,1)-4.0_dp)**2+grid%XYZ(i,2)**2)/(0.4_dp)**2 )) * dsin(4.0_dp * TWOPI * state%time(i)) * grid%INVJAC(i)
        state%rhs(i,4) = state%rhs(i,4) + source / (state%gv(i,1)-1.0_dp)
      end do
    end if

    ! ... subtract off (violation of) metric identity
    if (input%useMetricIdentities == TRUE) Then
      do i = 1, ND
        do ii = 1, Nc
          state%rhs(ii,1)    = state%rhs(ii,1)    + state%cv(ii,i+1) * grid%ident(ii,i)
          state%rhs(ii,i+1)  = state%rhs(ii,i+1)  + state%dv(ii,1) * grid%ident(ii,i)
          state%rhs(ii,ND+2) = state%rhs(ii,ND+2) + (state%cv(ii,ND+2) + state%dv(ii,1)) * state%cv(ii,i+1)/state%cv(ii,1) * grid%ident(ii,i)
        end do
      end do
    End if

    ! ... gravity
    if (input%Froude >= 0.0_DP) then
      norm(1) = sin(input%gravity_angle_phi) * cos(input%gravity_angle_theta) * input%invFroude
      norm(2) = sin(input%gravity_angle_phi) * sin(input%gravity_angle_theta) * input%invFroude
      norm(3) = cos(input%gravity_angle_phi) * input%invFroude
      do j = 1, ND
        do i = 1, Nc
          state%rhs(i,j+1) = state%rhs(i,j+1) + state%cv(i,1) * norm(j) * grid%INVJAC(i)
        end do
      end do
    end if

    return

  end subroutine NS_RHS_Euler_NASA

  Subroutine CalcPower(region)

    USE ModGlobal
    USE ModDataStruct
    USE ModEOS
    USE ModDeriv
    USE ModMPI
    USE ModMatrixVectorOps

    Implicit None

    TYPE (t_region), POINTER :: region

    ! ... local variables
    type(t_patch), pointer :: patch
    type(t_grid), pointer :: grid
    type(t_mixt), pointer :: state
    type(t_mixt_input), pointer :: input
    real(rfreal), pointer :: UVWhat(:,:), gvec(:), var(:)
    integer :: ND, Nc, N(MAX_ND), Ng(MAX_ND), npatch, i, j, l0, k, jj, l1, l2, isign, lp, kk, lp1, l, dir
    real(rfreal) :: XI_X, XI_Y, XI_Z, XI_X_TILDE, XI_Y_TILDE, XI_Z_TILDE, den
    real(rfreal) :: dsgn
    integer :: normDir, tangDir(MAX_ND,MAX_ND-1), Np(MAX_ND), verts(4,MAX_ND), Npg(MAX_ND)
    real(rfreal) :: norm_vec(MAX_ND), tang_vec(2,MAX_ND), CellArea, SurfacePower, CellPower
    integer, allocatable :: vert_ind(:), vert_pind(:)
    integer :: sgn, handedness(MAX_ND), isAux(MAX_ND), ieAux(MAX_ND), nPtsPatch, nPtsPatchGhost
    integer, pointer :: inorm(:)
    real(rfreal), pointer :: PowerInOut(:), PowerFlux(:), PowerFluxAux(:), XYZAux(:,:), XYZAuxTemp(:)
    integer :: ierr

    tangDir(1,:) = (/2, 3/)
    tangDir(2,:) = (/1, 3/)
    tangDir(3,:) = (/1, 2/)
    handedness = (/1, -1, 1/)

    ! ... simplicity
    ND = region%input%ND

    allocate(vert_ind((ND-1)*2),vert_pind((ND-1)*2))

    allocate(PowerInOut(region%nIntegralSurfaces))

    PowerInOut = 0.0_rfreal

    do npatch = 1, region%nPatches

      patch => region%patch(npatch)
      grid  => region%grid(patch%gridID)
      state => region%state(patch%gridID)
      input => grid%input
      normDir = abs(patch%normDir)
      sgn = normDir / patch%normDir
      dsgn = DBLE(sgn)


      ! ... CYCLE if we are finite volume
      if ( input%fv_grid(patch%gridID) == FINITE_VOL .AND. patch%bcType /= SPONGE ) CYCLE

      Nc = grid%nCells
      N  = 1; Ng = 1; Np = 1; Npg = 1
      isAux(:) = 1; ieAux(:) = 1
      nPtsPatch = 1
      nPtsPatchGhost = 1
      do j = 1, grid%ND
        N(j)  =  grid%ie(j) -  grid%is(j) + 1
        Ng(j) = N(j) + patch%SurfIntCommAmt(j,2)
        Np(j) = patch%ie(j) - patch%is(j) + 1
        Npg(j) = Np(j) + patch%SurfIntCommAmt(j,2)
        isAux(j) = patch%is(j)
        ieAux(j) = patch%ie(j) - 1 + patch%SurfIntCommAmt(j,2)

        nPtsPatch = nPtsPatch*Np(j)
        nPtsPatchGhost = nPtsPatchGhost*Npg(j)
      end do;

      ieAux(normdir) = isAux(normdir)

      Allocate(PowerFlux(nPtsPatch))
      Allocate(PowerFluxAux(nPtsPatchGhost))
      Allocate(XYZAux(nPtsPatchGhost,3))
      Allocate(XYZAuxTemp(nPtsPatchGhost))

      PowerFlux(:) = 0.0_rfreal
      PowerFluxAux(:) = 0.0_rfreal
      XYZAux(:,:) = 0.0_rfreal
      XYZAuxTemp(:) = 0.0_rfreal

      if ( patch%bcType == INTEGRAL_SURFACE ) then
         
         SurfacePower = 0.0_rfreal
         
         Do k = patch%is(3), patch%ie(3)
            Do j = patch%is(2), patch%ie(2)
               Do i = patch%is(1), patch%ie(1)

                  l0 = (k- grid%is(3))* N(1)* N(2) + (j- grid%is(2))* N(1) + (i- grid%is(1)+1)
                  lp = (k-patch%is(3))*Np(1)*Np(2) + (j-patch%is(2))*Np(1) + (i-patch%is(1)+1)

                  if ( grid%iblank(l0) == 0 ) CYCLE            
                  
                  ! ... normalized metrics
                  XI_X = grid%MT1(l0,region%global%t2Map(normDir,1)); XI_Y = 0.0_rfreal; XI_Z = 0.0_rfreal
                  if (ND >= 2) XI_Y = grid%MT1(l0,region%global%t2Map(normDir,2))
                  if (ND == 3) XI_Z = grid%MT1(l0,region%global%t2Map(normDir,3))

                  ! ... multiply out Jacobian
                  XI_X = XI_X*grid%Jac(l0)
                  XI_Y = XI_Y*grid%Jac(l0)
                  XI_Z = XI_Z*grid%Jac(l0)
                  
                  ! ... normalize
                  den = sqrt(XI_X*XI_X + XI_Y*XI_Y + XI_Z*XI_Z)
                  XI_X_TILDE = XI_X/den
                  XI_Y_TILDE = XI_Y/den
                  XI_Z_TILDE = XI_Z/den
                  
                  ! ... outward facing normal vector
                  norm_vec(:) = (/XI_X_TILDE, XI_Y_TILDE, XI_Z_TILDE/)
                  norm_vec(:) = -1.0_rfreal * norm_vec(:) * dsgn
                  
                  ! ... n_i*[u_i*(E+p)+Q_i-u_jTau_ij]
                  do dir = 1, ND
                     PowerFlux(lp) = PowerFlux(lp) + norm_vec(dir)*(state%cv(l0,dir+1)*state%dv(l0,3)*(state%cv(l0,ND+2) + state%dv(l0,1)) &
                         - patch%ViscousFlux(ND+2,lp,dir))
                  end do
                  
                  ! ... hardcoded to test
                  !Powerflux(lp) = grid%XYZ(l0,2)

              ! ... hardcoded to test
!Powerflux(lp) = grid%XYZ(l0,2)

            end do ! ... i
          end do ! ... j
        end do ! ... k

        ! ... communicate the necessary data
        do k = patch%is(3), patch%ie(3)
          do j = patch%is(2), patch%ie(2)
            do i = patch%is(1), patch%ie(1)

              l0 = (k-grid%is(3))*N(1)*N(2) + (j-grid%is(2))*N(1) + (i-grid%is(1)) + 1
              lp = (k-patch%is(3))*Np(1)*Np(2) + (j-patch%is(2))*Np(1) + (i-patch%is(1)) + 1
              l1 = (k-patch%is(3))*Npg(1)*Npg(2) + (j-patch%is(2))*Npg(1) + (i-patch%is(1)) + 1

              do dir = 1, ND
                XYZAux(l1,dir) = grid%XYZ(l0,dir)
              end do
              PowerFluxAux(l1) = PowerFlux(lp)
            end do
          end do
        end do

        do dir = 1, ND
          XYZAuxTemp(:) = XYZAux(:,dir)
          Call Ghost_Cell_Exchange_patch(region,XYZAuxTemp,npatch)
          XYZAux(:,dir) = XYZAuxTemp(:)
        end do
        Call Ghost_Cell_Exchange_patch(region,PowerFluxAux,npatch)

        Do k = isAux(3), ieAux(3)
          Do j = isAux(2), ieAux(2)
            Do i = isAux(1), ieAux(1)

              CellPower = 0.0_rfreal

              l0 = (k- grid%is(3))* Ng(1)* Ng(2) + (j- grid%is(2))* Ng(1) + (i- grid%is(1)+1)
              lp = (k-patch%is(3))*Npg(1)*Npg(2) + (j-patch%is(2))*Npg(1) + (i-patch%is(1)+1)


              ! ... first, calculate the cell area
              ! ... get the vertex indices
              do l = 1, (ND-1)*2
                verts(l,:) = (/i,j,k/)
              end do
              verts(2,tangDir(normdir,1)) = verts(2,tangDir(normdir,1))+1

              if(ND == 3) then
                verts(3,tangDir(normdir,1)) = verts(3,tangDir(normdir,1))+1
                verts(3,tangDir(normdir,2)) = verts(3,tangDir(normdir,2))+1

                verts(4,tangDir(normdir,2)) = verts(4,tangDir(normdir,2))+1

                do l = 1, (ND-1)*2
                  vert_ind(l) = (verts(l,3)-grid%is(3))*Ng(2)*Ng(1) + (verts(l,2)-grid%is(2))*Ng(1) + (verts(l,1)-grid%is(1)) + 1
                  vert_pind(l) = (verts(l,3)-patch%is(3))*Npg(2)*Npg(1) + (verts(l,2)-patch%is(2))*Npg(1) + (verts(l,1)-patch%is(1)) + 1
                end do
                do dir = 1, MAX_ND
                  tang_vec(1,dir) = XYZAux(vert_pind(3),dir)-XYZAux(vert_pind(1),dir)
                  tang_vec(2,dir) = XYZAux(vert_pind(2),dir)-XYZAux(vert_pind(4),dir)
                end do

                call cross_product(tang_vec(1,:),tang_vec(2,:),norm_vec)

                CellArea = 0.0_rfreal
                do dir = 1, MAX_ND
                  CellArea = CellArea + norm_vec(dir)*norm_vec(dir)
                end do

                CellArea = sqrt(CellArea)*0.5_rfreal

              elseif(ND == 2) then

                do l = 1, (ND-1)*2
                  vert_ind(l) = (verts(l,2)-grid%is(2))*Ng(1) + (verts(l,1)-grid%is(1)) + 1
                  vert_pind(l) = (verts(l,2)-patch%is(2))*Npg(1) + (verts(l,1)-patch%is(1)) + 1
                end do

                CellArea = 0.0_rfreal
                do dir = 1, ND 
                  tang_vec(1,dir) = XYZAux(vert_pind(2),dir)-XYZAux(vert_pind(1),dir)
                  CellArea = CellArea + tang_vec(1,dir)*tang_vec(1,dir)
                end do
                CellArea = sqrt(CellArea)
              end if



              CellPower = 0.0_rfreal

              do l = 1, (ND-1)*2
                lp1 = vert_pind(l)
                CellPower = CellPower + PowerFluxAux(lp1)
              end do
!write(*,'(A,4(I1,x),2(D13.6))'),'myrank,i,j,k,CellArea,CellPower/dble((ND-1)*2)*CellArea ',myrank,i,j,k,CellArea,CellPower/dble((ND-1)*2)*CellArea                  
              SurfacePower = SurfacePower + CellPower/dble((ND-1)*2)*CellArea

            end Do ! ... i
          end Do ! ... j
        end Do ! ... k

        PowerInOut(patch%iIntegralSurface) = SurfacePower

      end if ! ... patch%bcType == INTEGRAL_SURFACE

      deallocate(PowerFlux,PowerFluxAux,XYZAux,XYZAuxTemp)

    end do ! ... npatch

    region%nIntegralSamples = region%nIntegralSamples + 1
    Call MPI_Allreduce(PowerInOut, region%PowerInOut(1,region%nIntegralSamples), region%nIntegralSurfaces, MPI_DOUBLE_PRECISION, MPI_SUM, mycomm, ierr)

    region%IntegralTime(region%nIntegralSamples) = region%state(1)%time(1)

! do i = 1,region%nIntegralSurfaces
!    if(myrank == 0) write(*,'(A,D13.6,A,I2,A,D13.6)') 'At time = ',region%integralTime(region%nIntegralSamples), ' the integral on surface ',&
!         i,' is ',region%PowerInOut(i,region%nIntegralSamples)
! end do

    deallocate(PowerInOut)

  end Subroutine CalcPower

  Function Heaviside(x)

    USE ModGlobal
    Implicit None

    Real(rfreal) :: x, Heaviside

    Heaviside = 1.0_rfreal
    If (x < 0.0_rfreal) Heaviside = 0.0_rfreal

  End Function Heaviside

  Subroutine Compute_Divergence_From_VelGrad1st(region, grid, state, div)

    USE ModGlobal
    USE ModDataStruct

    Implicit None

    ! ... global variables
    type(t_grid), pointer :: grid
    type(t_region), pointer :: region
    type(t_mixt), pointer :: state
    real(rfreal), pointer :: div(:)

    ! ... local variables
    integer :: i, j, k, Nc, ND

    ! ... simplicity
    Nc = grid%nCells
    ND = grid%ND

    ! ... sort by number of dimensions
    If (ND == 1) Then
      Do i = 1, Nc
        div(i) = grid%MT1(i,region%global%t2Map(1,1)) * state%VelGrad1st(i,region%global%t2Map(1,1)) * grid%JAC(i)
      End Do
      Return
    End If

    If (ND >= 2) Then

      ! ... initialize
      Do i = 1, Nc
        div(i) = 0.0_rfreal
      End Do

      ! ... do the sums
      Do k = 1, ND
        Do j = 1, ND
          Do i = 1, Nc
            div(i) = div(i) + grid%MT1(i,region%global%t2Map(j,k)) * state%VelGrad1st(i,region%global%t2Map(k,j))
          End Do
        End Do
      End Do

      ! ... multiply by Jacobian
      Do i = 1, Nc
        div(i) = div(i) * grid%JAC(i)
      End Do

      Return

    End If

  End Subroutine Compute_Divergence_From_VelGrad1st

  Subroutine Compute_Vorticity_From_VelGrad1st(region, grid, state, vort)

    USE ModGlobal
    USE ModDataStruct

    Implicit None

    ! ... global variables
    type(t_region), pointer :: region
    type(t_grid), pointer :: grid
    type(t_mixt), pointer :: state
    real(rfreal), pointer :: vort(:,:)

    ! ... local variables
    integer :: i, j, k, l, m, Nc, ND, levi(3,3,3)
    real(rfreal) :: fac

    ! ... simplicity
    Nc = grid%nCells
    ND = grid%ND

    ! ... sort by number of dimensions
    If (ND == 1) Then
      Do i = 1, Nc
        vort(i,1) = 0.0_rfreal
      End Do
      Return
    End If


    If (ND >= 2) Then

      ! ... initialize
      Do j = 1, ND
        Do i = 1, Nc
          vort(i,j) = 0.0_rfreal
        End Do
      End Do

      ! ... sort by dimension
      If (ND == 2) Then
        Do i = 1, Nc
          vort(i,1) = (   grid%MT1(i,region%global%t2Map(1,1)) * state%velGrad1st(i,region%global%t2Map(2,1)) + grid%MT1(i,region%global%t2Map(2,1)) * state%velGrad1st(i,region%global%t2Map(2,2)) &
               -   ( grid%MT1(i,region%global%t2Map(1,2)) * state%velGrad1st(i,region%global%t2Map(1,1)) + grid%MT1(i,region%global%t2Map(2,2)) * state%velGrad1st(i,region%global%t2Map(1,2)) ) ) &
                    * grid%JAC(i)
        End Do
        Return
      End If

      If (ND == 3) Then

        ! ... initialize Levi-Cevita pseudotensor
        levi = 0
        levi(1,2,3) = 1
        levi(2,3,1) = 1
        levi(3,1,2) = 1
        levi(3,2,1) = -1
        levi(2,1,3) = -1
        levi(1,3,2) = -1

        ! ... do the sums
        Do m = 1, 3
          Do l = 1, 3
            Do k = 1, 3
              Do j = 1, 3
                If (levi(l,j,k) /= 0) Then
                  fac = dble(levi(l,j,k))    
                  Do i = 1, Nc
                    vort(i,m) = vort(i,m) + fac * grid%MT1(i,region%global%t2Map(m,j)) * state%VelGrad1st(i,region%global%t2Map(k,m))
                  End Do
                End If
              End Do
            End Do
          End Do
        End Do

        ! ... multiply by Jacobian
        Do j = 1, 3
          Do i = 1, Nc
            vort(i,j) = vort(i,j) * grid%JAC(i)
          End Do
        End Do

        Return

      End If

    End If

  End Subroutine Compute_Vorticity_From_VelGrad1st

  Subroutine Normalize_Vector(ND, Nc, vec)

    USE ModGlobal
    USE ModDataStruct

    Implicit None

    ! ... incoming variables
    Integer :: ND, NC
    Real(RFREAL), Pointer :: vec(:,:)

    ! ... local variables
    Integer :: i, j
    Real(RFREAL) :: norm, invnorm

    ! ... Switch on dimension
    Select Case (ND)
    Case (1)
      Do i = 1, Nc
        norm = dabs(vec(i,1)) 
        If (norm > 10.0_rfreal * TINY) Then
          invnorm = 1.0_rfreal / norm
        Else
          invnorm = 0.0_rfreal
        End If
        vec(i,1) = vec(i,1) * invnorm
      End Do
    Case (2)
      Do i = 1, Nc
        norm = dsqrt(vec(i,1)**2 + vec(i,2)**2)
        If (norm > 10.0_rfreal * TINY) Then
          invnorm = 1.0_rfreal / norm
        Else
          invnorm = 0.0_rfreal
        End If
        vec(i,1) = vec(i,1) * invnorm
        vec(i,2) = vec(i,2) * invnorm
      End Do
    Case (3)
      Do i = 1, Nc
        norm = dsqrt(vec(i,1)**2 + vec(i,2)**2 + vec(i,3)**2)
        If (norm > 10.0_rfreal * TINY) Then
          invnorm = 1.0_rfreal / norm
        Else
          invnorm = 0.0_rfreal
        End If
        vec(i,1) = vec(i,1) * invnorm
        vec(i,2) = vec(i,2) * invnorm
        vec(i,3) = vec(i,3) * invnorm
      End Do
    End Select

    Return

  End Subroutine Normalize_Vector

  Subroutine NS_Local_Low_Mach_Preconditioner(region)

    USE ModGlobal
    USE ModDataStruct
    USE ModMPI

    Implicit None

    ! ... incoming variables
    TYPE (t_region), Pointer :: region

    ! ... local variables
    TYPE (t_mixt_input), Pointer :: input
    TYPE (t_mixt), Pointer :: state
    TYPE (t_grid), Pointer :: grid
    Real(RFREAL), Dimension(:,:), Pointer :: rhs, gv, dv, cv, MT1
    Real(RFREAL), Dimension(:), Pointer :: JAC
    Real(RFREAL) :: alpha, gam, u, v, z1, z2, z3, R2, alpha_fac, B2, inv_alpha_fac
    Real(RFREAL) :: gamm1, u2, v2, L2, C2, inv_C2, inv_B2, Q_fac, inv_gamm1
    Real(RFREAL) :: X_x, Y_x, X_y, Y_y, J_fac
    Integer :: nGrids, Nc, ng, i, ND
        
    ! ... error check on dimension
    If (region%input%ND /= 2) &
      Call Graceful_Exit(region%myrank, &
        'PlasComCM: ERROR: USE_LOCAL_PRECONDITIONER only works with ND = 2.')

    nGrids = region%nGrids
    Do ng = 1, nGrids
      grid => region%grid(ng)
      input => grid%input

      ! ... check if this grid has any preconditioners in use
      If (input%USE_LOCAL_LOW_MACH_PRECONDITIONER == TRUE) Then
        alpha = input%LOCAL_LOW_MACH_PRECONDITIONER_PARAM_ALPHA
        If (alpha < 1.0_8) Then
          alpha_fac = 2.0_8 - alpha
        Else
          alpha_fac = alpha
        End If
        inv_alpha_fac = 1.0_8 / alpha_fac
        state => region%state(ng)
        rhs   => state%rhs
        cv    => state%cv
        gv    => state%gv
        dv    => state%dv
        MT1   => grid%MT1
        JAC   => grid%JAC
        Nc    = grid%nCells
        ND    = grid%ND
        Do i = 1, Nc
          gam   = gv(i,1)
          gamm1 = gam - 1.0_8
          inv_gamm1 = 1.0_8 / gamm1
          u   = cv(i,2) / cv(i,1)
          v   = cv(i,3) / cv(i,1)
          u2  = u * u
          v2  = v * v
          R2  = 0.5_8 * (u2 + v2)
          X_x = MT1(i,4)
          X_y = -MT1(i,3)
          Y_x = -MT1(i,2)
          Y_y = MT1(i,1)
          L2  = X_x * X_x + Y_x * Y_x + X_y * X_y + Y_y * Y_y &
              + 2.0_8 * abs(X_x * X_y + Y_x * Y_y)
          C2  = gam * dv(i,1) * dv(i,3)
          B2  = 2.0_8 * R2 * alpha_fac / L2
          inv_C2 = 1.0_8 / C2
          inv_B2 = 1.0_8 / B2
          z1  = gamm1 * (inv_B2 - inv_C2)
          z2 = z1 + gamm1 * alpha * inv_B2
          z3 = z1 * (C2 * inv_gamm1 + R2) + gamm1 * alpha * (u2 + v2) * inv_B2
          Q_fac = B2 * inv_C2 * (R2 * rhs(i,1) - u * rhs(i,2) - v * rhs(i,3) + rhs(i,4))
          rhs(i,1) = rhs(i,1) - Q_fac * z1
          rhs(i,2) = rhs(i,2) - Q_fac * u * z2
          rhs(i,3) = rhs(i,3) - Q_fac * v * z2
          rhs(i,4) = rhs(i,4) - Q_fac * z3
        End Do
      End If
    End Do

    Return

  End Subroutine NS_Local_Low_Mach_Preconditioner

  subroutine NS_RHS_ViscousTerms_OPT(region, ng, flags, &
                                     VelGrad1st, TempGrad1st, tvCor)

    USE ModGlobal
    USE ModDataStruct
    USE ModDeriv
    USE ModMPI

    Implicit None

    ! ... subroutine arguments
    real(rfreal), pointer :: VelGrad1st(:,:), TempGrad1st(:,:)
    real(rfreal), pointer, optional :: tvCor(:,:) ! JKim 01/2008

    ! ... local variables
    type(t_grid), pointer :: grid
    type(t_mixt), pointer :: state
    type(t_mixt_input), pointer :: input
    type(t_region), pointer :: region
    type(t_patch), pointer :: patch
    integer :: flags
    real(rfreal), allocatable :: UVW(:,:)
    integer :: ND, Nc, N(MAX_ND), ng, i, j, Np(MAX_ND), patchID
    integer :: ip, jp, kp, ii

    ! ... local variables and arrays for viscous and heat fluxes, JKim 02/2007
    integer :: k, l, m, p
    real(rfreal) :: viscous_time
    real(rfreal), pointer :: VelGrad2nd(:,:,:), TempGrad2nd(:,:)
    real(rfreal), pointer :: MTProd(:,:)

    ! ... for pointer dereferencing, WZhang 06/2015
    integer :: is(MAX_ND), ie(MAX_ND), shock, LES, QuasiSS, myrank
    integer, pointer :: vMap(:), t2Map(:,:), t3Map(:,:,:)
    integer, pointer :: t2MapSymVisc(:,:), delta(:,:)
    real(rfreal) :: RE, REinv, PRinv
    real(rfreal), pointer :: flux(:), dflux(:), cv(:,:), dv(:,:), tv(:,:)
    real(rfreal), pointer :: MT1(:,:), JAC(:), rhs(:,:)
    type(t_modglobal), pointer :: global

    ! ... for vectorization preparation, WZhang 06/2015
    integer :: vMap1p1, vMap2p1, vMap3p1, vMapip1, vMapNDp2, t2Maplm
    integer :: t2Mapij, t2Mapji, t2Maplj, t2Mapjk, t2Mapl1, t2Mapl2, t2Mapl3
    integer :: t2Map1j, t2Map2j, t2Map3j, t2Mappj, t2Mappl, t2Mapkl, t2Mapil 
    integer :: t2Mapk1, t2Mapk2, t2Mapk3
    integer :: t2MapSymViscjm, t2MapSymViscjj, t2MapSymViscjl, t2MapSymViscj1
    integer :: t2MapSymViscj2, t2MapSymViscj3
    integer :: t3Mapkl1, t3Mapkl2, t3Mapkl3, t3Mapi1l, t3Mapi2l, t3Mapi3l
    integer :: t3Map1l1, t3Map2l2, t3Map3l3
    integer :: t3Mapk1i, t3Mapk2i, t3Mapk3i, t3Map1ml, t3Map2ml, t3Map3ml
    integer :: t3Mapiml, t3Mapklm, deltaij

    ! ... pointer dereferencing, WZhang 06/2015
    grid   => region%grid(ng)
    state  => region%state(ng)
    input  => grid%input
    global => region%global
    is(:)  = grid%is(:)
    ie(:)  = grid%ie(:)
    ND = grid%ND
    Nc = grid%nCells
    N  = 1
    do j = 1, ND
      N(j) = ie(j)-is(j)+1
    end do ! j

    shock   = input%shock
    LES     = input%LES
    QuasiSS = input%QuasiSS
    myrank  = region%myrank
    vMap    => global%vMap
    t2Map   => global%t2Map   
    t3Map   => global%t3Map   
    t2MapSymVisc => global%t2MapSymVisc
    delta   => global%delta   
    RE = state%RE
    REinv = state%REinv
    PRinv = state%PRinv
    flux => state%flux
    dflux => state%dflux
    cv => state%cv
    dv => state%dv
    tv => state%tv
    rhs => state%rhs
    MT1 => grid%MT1
    JAC => grid%JAC
    vMapNDp2 = vMap(ND+2)

    if (flags == INVISCID_ONLY) return ! because it's inviscid computation

    if (RE == 0.0_rfreal .AND. shock == 0 .AND. LES == 0) then
      call graceful_exit(myrank, &
       'At least either shock-capturing or LES should &
       &be on to compute viscous terms.')
    end if ! state%RE

    ! ... allocate space for arrays
    allocate(UVW(Nc,ND)); UVW = 0.0_rfreal

    ! ... allocate space for arrays for viscous terms, JKim 06/2007
    nullify(VelGrad2nd); 
    allocate(VelGrad2nd(Nc,ND,1+(ND-1)*ND/2)); 
    VelGrad2nd = 0.0_rfreal
    nullify(TempGrad2nd);
    allocate(TempGrad2nd(Nc,1+(ND-1)*ND/2)); 
    TempGrad2nd = 0.0_rfreal
    nullify(MTProd); 
    allocate(MTProd(Nc,ND*ND*ND)); 
    MTProd = 0.0_rfreal
   
    ! ... Step 2: precompute primitive velocities and store in UVW(:,:).
    if (ND == 2) then
      vMap1p1 = vMap(1+1)
      vMap2p1 = vMap(2+1)
#ifdef ENABLE_SIMD
!DIR$ SIMD
#endif
      do ii = 1, Nc
        UVW(ii,1) = cv(ii,vMap1p1) * dv(ii,3)
        UVW(ii,2) = cv(ii,vMap2p1) * dv(ii,3)
      end do
    end if
    if (ND == 3) then
      vMap1p1 = vMap(1+1)
      vMap2p1 = vMap(2+1)
      vMap3p1 = vMap(3+1)
#ifdef ENABLE_SIMD
!DIR$ SIMD
#endif
      do ii = 1, Nc
        UVW(ii,1) = cv(ii,vMap1p1) * dv(ii,3)
        UVW(ii,2) = cv(ii,vMap2p1) * dv(ii,3)
        UVW(ii,3) = cv(ii,vMap3p1) * dv(ii,3)
      end do
    end if

    ! ... store viscous fluxes on the boundary
    ! ... zero things out first
    Call Save_Patch_Viscous_Flux(region, ng, 0, 0, flux)

    do i = 1, ND
      do j = 1, ND

        ! ... momentum: (rho u_i)_t = ... (\mu { (u_i)_j + (u_j)_i })_j &
        ! ...                       + (\lambda (u_k)_k)_i
        t2Mapij = t2Map(i,j)
        t2Mapji = t2Map(j,i)
        deltaij = delta(i,j)
#ifdef ENABLE_SIMD
!DIR$ SIMD
#endif
        do k = 1, Nc
          flux(k) = tv(k,1) * (VelGrad1st(k,t2Mapij) &
                    + VelGrad1st(k,t2Mapji)) * REinv &
                    + deltaij * tv(k,2) * 0.5_rfreal * (VelGrad1st(k,t2Mapij) &
                    + VelGrad1st(k,t2Mapji)) * REinv
        end do
        Call Save_Patch_Viscous_Flux(region, ng, i+1, j, flux)

        ! ... energy: (rho E)_t = ... [(\mu { (u_i)_j + (u_j)_i } &
        ! ...                   + \lambda (u_k)_k \delta_{ij}) u_j]_i
#ifdef ENABLE_SIMD
!DIR$ SIMD
#endif
        do k = 1, Nc
          flux(k) = flux(k) * UVW(k,j)
        end do
        Call Save_Patch_Viscous_Flux(region, ng, ND+2, i, flux)

      end do

      ! ... energy: (rho E)_t = ... (k T_i)_i
#ifdef ENABLE_SIMD
!DIR$ SIMD
#endif
      do k = 1, Nc
        flux(k) = tv(k,3) * TempGrad1st(k,i) * REinv * PRinv
      end do
      Call Save_Patch_Viscous_Flux(region, ng, ND+2, i, flux)

    end do

    ! ... thermally coupled?  
    ! ... If so, then save the temperature gradient on interacting patches
    if (QuasiSS == TRUE) then
      Call Save_Patch_TempGrad(region, ng, 0, flux) ! zero things out first
      do i = 1, ND
#ifdef ENABLE_SIMD
!DIR$ SIMD
#endif
        do k = 1, Nc
          flux(k) = TempGrad1st(k,i)
        end do
        Call Save_Patch_TempGrad(region, ng, i, flux)
      end do
    end if

    ! ... Step 3: precompute off-diagonal 2nd derivatives of 
    ! ...         velocity and temperature
    do j = 1,ND
      do m = j+1,ND ! only upper-triangle part
        t2MapSymViscjm = t2MapSymVisc(j,m)
        do l = 1,ND
          t2Maplj = t2Map(l,j)
#ifdef ENABLE_SIMD
!DIR$ SIMD
#endif
          do k = 1, Nc
            flux(k) = VelGrad1st(k,t2Maplj) ! d(u_l)/d(xi_j)
          end do
          call APPLY_OPERATOR_BOX(region, ng, 1, m, flux, dflux, .FALSE., TRUE)
#ifdef ENABLE_SIMD
!DIR$ SIMD
#endif
          do k = 1, Nc
            VelGrad2nd(k,l,t2MapSymViscjm) = dflux(k) ! d^2(u_l)/d(xi_j)/d(xi_m)
          end do
        end do ! l
#ifdef ENABLE_SIMD
!DIR$ SIMD
#endif
        do k = 1, Nc
          flux(k) = TempGrad1st(k,j) ! dT/d(xi_j)
        end do
        call APPLY_OPERATOR_BOX(region, ng, 1, m, flux, dflux, .FALSE., TRUE)
#ifdef ENABLE_SIMD
!DIR$ SIMD
#endif
        do k = 1, Nc
          TempGrad2nd(k,t2MapSymViscjm) = dflux(k) ! d^2T/d(xi_j)/d(xi_m)
        end do
      end do ! m
    end do ! j

    ! ... Step 4: start j-loop (j is flux direction and runs from 1 to ND)
    do j = 1, ND
      t2MapSymViscjj = t2MapSymVisc(j,j)
      ! ... Step 5: precompute metric-Jacobian products
      do k = 1, ND
        do l = 1, ND
          do m = 1, ND
#ifdef ENABLE_SIMD
!DIR$ SIMD
#endif
            do ii = 1, Nc
              MTProd(ii,t3Map(k,l,m)) = JAC(ii) * MT1(ii,t2Map(j,k)) &
                                      * MT1(ii,t2Map(l,m)) * REinv
            end do
          end do ! m
          if (ND == 2) then
            t3Mapkl1 = t3Map(k,l,1)
            t3Mapkl2 = t3Map(k,l,2)
            t2Mapl1 = t2Map(l,1)
            t2Mapl2 = t2Map(l,2)
#ifdef ENABLE_SIMD
!DIR$ SIMD
#endif
            do ii = 1, Nc
              MTProd(ii,t3Mapkl1) = JAC(ii) * MT1(ii,t2Map(j,k)) &
                                  * MT1(ii,t2Mapl1) * REinv
              MTProd(ii,t3Mapkl2) = JAC(ii) * MT1(ii,t2Map(j,k)) &
                                  * MT1(ii,t2Mapl2) * REinv
            end do
          end if
          if (ND == 3) then
            t3Mapkl1 = t3Map(k,l,1)
            t3Mapkl2 = t3Map(k,l,2)
            t3Mapkl3 = t3Map(k,l,3)
            t2Mapl1 = t2Map(l,1)
            t2Mapl2 = t2Map(l,2)
            t2Mapl3 = t2Map(l,3)
#ifdef ENABLE_SIMD
!DIR$ SIMD
#endif
            do ii = 1, Nc
              MTProd(ii,t3Mapkl1) = JAC(ii) * MT1(ii,t2Map(j,k)) &
                                  * MT1(ii,t2Mapl1) * REinv
              MTProd(ii,t3Mapkl2) = JAC(ii) * MT1(ii,t2Map(j,k)) &
                                  * MT1(ii,t2Mapl2) * REinv
              MTProd(ii,t3Mapkl3) = JAC(ii) * MT1(ii,t2Map(j,k)) &
                                  * MT1(ii,t2Mapl3) * REinv
            end do
          end if
        end do ! l
      end do ! k

      ! ... Step 6: precompute the diagonal 2nd derivatives of velocity and 
      ! ...         temperature for this j
      do l = 1, ND
#ifdef ENABLE_SIMD
!DIR$ SIMD
#endif
        do ii = 1, Nc
          flux(ii) = UVW(ii,l) ! u_l
        end do
        call APPLY_OPERATOR_BOX(region, ng, 2, j, flux, dflux, .FALSE., FALSE)
#ifdef ENABLE_SIMD
!DIR$ SIMD
#endif
        do ii = 1, Nc
          VelGrad2nd(ii,l,t2MapSymViscjj) = dflux(ii)
        end do
      end do ! l
#ifdef ENABLE_SIMD
!DIR$ SIMD
#endif
      do ii = 1, Nc
        flux(ii) = dv(ii,2) ! temperature.
      end do
      call APPLY_OPERATOR_BOX(region, ng, 2, j, flux, dflux, .FALSE., FALSE)
#ifdef ENABLE_SIMD
!DIR$ SIMD
#endif
      do ii = 1, Nc
        TempGrad2nd(ii,t2MapSymViscjj) = dflux(ii)
      end do

      ! ... Step 7: part of terms in the i-th momentum equations
      ! ...         Viscous fluxes are computed term-by-term with explicit 
      ! ...         nonconservative 2nd derivatives.
      ! ...         Half of viscous terms having 2nd derivatives are computed 
      ! ...         here.
      do i = 1, ND
        vMapip1 = vMap(i+1)
        do l = 1, ND
          if (ND == 2) then
            t3Mapi1l = t3Map(i,1,l)
            t3Mapi2l = t3Map(i,2,l)
            t2MapSymViscj1 = t2MapSymVisc(j,1)
            t2MapSymViscj2 = t2MapSymVisc(j,2)
#ifdef ENABLE_SIMD
!DIR$ SIMD
#endif
            do ii = 1, Nc
              rhs(ii,vMapip1) = rhs(ii,vMapip1) &
                              + MTProd(ii,t3Mapi1l) * tv(ii,2) &
                              * VelGrad2nd(ii,l,t2MapSymViscj1) &
                              + MTProd(ii,t3Mapi2l) * tv(ii,2) &
                              * VelGrad2nd(ii,l,t2MapSymViscj2)
            end do
          end if
          if (ND == 3) then
            t3Mapi1l = t3Map(i,1,l)
            t3Mapi2l = t3Map(i,2,l)
            t3Mapi3l = t3Map(i,3,l)
            t2MapSymViscj1 = t2MapSymVisc(j,1)
            t2MapSymViscj2 = t2MapSymVisc(j,2)
            t2MapSymViscj3 = t2MapSymVisc(j,3)
#ifdef ENABLE_SIMD
!DIR$ SIMD
#endif
            do ii = 1, Nc
              rhs(ii,vMapip1) = rhs(ii,vMapip1) &
                              + MTProd(ii,t3Mapi1l) * tv(ii,2) &
                              * VelGrad2nd(ii,l,t2MapSymViscj1) &
                              + MTProd(ii,t3Mapi2l) * tv(ii,2) &
                              * VelGrad2nd(ii,l,t2MapSymViscj2) &
                              + MTProd(ii,t3Mapi3l) * tv(ii,2) &
                              * VelGrad2nd(ii,l,t2MapSymViscj3)
            end do
          end if
        end do ! l
        do l = 1, ND
          t2MapSymViscjl = t2MapSymVisc(j,l)
          if (ND == 2) then
            t3Map1l1 = t3Map(1,l,1)
            t3Map2l2 = t3Map(2,l,2)
#ifdef ENABLE_SIMD
!DIR$ SIMD
#endif
            do ii = 1, Nc
              flux(ii) = MTProd(ii,t3Map1l1) + MTProd(ii,t3Map2l2)
            end do
          end if
          if (ND == 3) then
            t3Map1l1 = t3Map(1,l,1)
            t3Map2l2 = t3Map(2,l,2)
            t3Map3l3 = t3Map(3,l,3)
#ifdef ENABLE_SIMD
!DIR$ SIMD
#endif
            do ii = 1, Nc
              flux(ii) = MTProd(ii,t3Map1l1) + MTProd(ii,t3Map2l2) &
                       + MTProd(ii,t3Map3l3)
            end do
          end if
#ifdef ENABLE_SIMD
!DIR$ SIMD
#endif
          do ii = 1, Nc
            rhs(ii,vMapip1) = rhs(ii,vMapip1) &
                            + flux(ii) * tv(ii,1) &
                            * VelGrad2nd(ii,i,t2MapSymViscjl)
          end do
        end do ! l
        do k = 1, ND
          if (ND == 2) then
            t3Mapk1i = t3Map(k,1,i)
            t3Mapk2i = t3Map(k,2,i)
            t2MapSymViscj1 = t2MapSymVisc(j,1)
            t2MapSymViscj2 = t2MapSymVisc(j,2)
#ifdef ENABLE_SIMD
!DIR$ SIMD
#endif
            do ii = 1, Nc
              flux(ii) = MTProd(ii,t3Mapk1i) * tv(ii,1) &
                       * VelGrad2nd(ii,k,t2MapSymViscj1)
              rhs(ii,vMapip1) = rhs(ii,vMapip1) &
                              + MTProd(ii,t3Mapk1i) * tv(ii,1) &
                              * VelGrad2nd(ii,k,t2MapSymViscj1) &
                              + MTProd(ii,t3Mapk2i) * tv(ii,1) &
                              * VelGrad2nd(ii,k,t2MapSymViscj2)
            end do
          end if
          if (ND == 3) then
            t3Mapk1i = t3Map(k,1,i)
            t3Mapk2i = t3Map(k,2,i)
            t3Mapk3i = t3Map(k,3,i)
            t2MapSymViscj1 = t2MapSymVisc(j,1)
            t2MapSymViscj2 = t2MapSymVisc(j,2)
            t2MapSymViscj3 = t2MapSymVisc(j,3)
#ifdef ENABLE_SIMD
!DIR$ SIMD
#endif
            do ii = 1, Nc
              flux(ii) = MTProd(ii,t3Mapk1i) * tv(ii,1) &
                       * VelGrad2nd(ii,k,t2MapSymViscj1)
              rhs(ii,vMapip1) = rhs(ii,vMapip1) &
                              + MTProd(ii,t3Mapk1i) * tv(ii,1) &
                              * VelGrad2nd(ii,k,t2MapSymViscj1) &
                              + MTProd(ii,t3Mapk2i) * tv(ii,1) &
                              * VelGrad2nd(ii,k,t2MapSymViscj2) &
                              + MTProd(ii,t3Mapk3i) * tv(ii,1) &
                              * VelGrad2nd(ii,k,t2MapSymViscj3)
            end do
          end if
        end do ! k
      end do ! i

      ! ... Step 8: viscous work in energy equation
      ! ...         term-by-term computation with explicit nonconservative 2nd 
      ! ...         derivatives.
      do l = 1, ND
        do m = 1, ND
          t2MapSymViscjm = t2MapSymVisc(j,m)
          if (ND == 2) then
            t3Map1ml = t3Map(1,m,l)
            t3Map2ml = t3Map(2,m,l)
#ifdef ENABLE_SIMD
!DIR$ SIMD
#endif
            do ii = 1, Nc
              flux(ii) = MTProd(ii,t3Map1ml) * UVW(ii,1) &
                       + MTProd(ii,t3Map2ml) * UVW(ii,2)
            end do ! ii
          end if
          if (ND == 3) then
            t3Map1ml = t3Map(1,m,l)
            t3Map2ml = t3Map(2,m,l)
            t3Map3ml = t3Map(3,m,l)
#ifdef ENABLE_SIMD
!DIR$ SIMD
#endif
            do ii = 1, Nc
              flux(ii) = MTProd(ii,t3Map1ml) * UVW(ii,1) &
                       + MTProd(ii,t3Map2ml) * UVW(ii,2) &
                       + MTProd(ii,t3Map3ml) * UVW(ii,3)
            end do ! ii
          end if
#ifdef ENABLE_SIMD
!DIR$ SIMD
#endif
          do ii = 1, Nc
            rhs(ii,vMapNDp2) = rhs(ii,vMapNDp2) + flux(ii) * tv(ii,2) &
                             * VelGrad2nd(ii,l,t2MapSymViscjm)
          end do ! ii
        end do ! m
      end do ! l
      do k = 1, ND
        do l = 1, ND
          t2MapSymViscjl = t2MapSymVisc(j,l)
          if (ND == 2) then
            t3Mapkl1 = t3Map(k,l,1) 
            t3Mapkl2 = t3Map(k,l,2) 
#ifdef ENABLE_SIMD
!DIR$ SIMD
#endif
            do ii = 1, Nc
              flux(ii) = MTProd(ii,t3Mapkl1) * UVW(ii,1) &
                       + MTProd(ii,t3Mapkl2) * UVW(ii,2)
            end do ! ii
          end if
          if (ND == 3) then
            t3Mapkl1 = t3Map(k,l,1) 
            t3Mapkl2 = t3Map(k,l,2) 
            t3Mapkl3 = t3Map(k,l,3) 
#ifdef ENABLE_SIMD
!DIR$ SIMD
#endif
            do ii = 1, Nc
              flux(ii) = MTProd(ii,t3Mapkl1) * UVW(ii,1) &
                       + MTProd(ii,t3Mapkl2) * UVW(ii,2) &
                       + MTProd(ii,t3Mapkl3) * UVW(ii,3)
            end do ! ii
          end if
#ifdef ENABLE_SIMD
!DIR$ SIMD
#endif
          do ii = 1, Nc
            flux(ii) = flux(ii) * tv(ii,1) * VelGrad2nd(ii,k,t2MapSymViscjl)
            rhs(ii,vMapNDp2) = rhs(ii,vMapNDp2) + flux(ii)
          end do ! ii
        end do ! l
      end do ! k
      do p = 1, ND
        do l = 1, ND
          t2MapSymViscjl = t2MapSymVisc(j,l)
          if (ND == 2) then
            t3Map1l1 = t3Map(1,l,1)
            t3Map2l2 = t3Map(2,l,2)
#ifdef ENABLE_SIMD
!DIR$ SIMD
#endif
            do ii = 1, Nc
              flux(ii) = MTProd(ii,t3Map1l1) + MTProd(ii,t3Map2l2)
            end do ! ii
          end if
          if (ND == 3) then
            t3Map1l1 = t3Map(1,l,1)
            t3Map2l2 = t3Map(2,l,2)
            t3Map3l3 = t3Map(3,l,3)
#ifdef ENABLE_SIMD
!DIR$ SIMD
#endif
            do ii = 1, Nc
              flux(ii) = MTProd(ii,t3Map1l1) + MTProd(ii,t3Map2l2) &
                       + MTProd(ii,t3Map3l3)
            end do ! ii
          end if
#ifdef ENABLE_SIMD
!DIR$ SIMD
#endif
          do ii = 1, Nc
            rhs(ii,vMapNDp2) = rhs(ii,vMapNDp2) + flux(ii) * tv(ii,1) &
                             * UVW(ii,p) * VelGrad2nd(ii,p,t2MapSymViscjl)
          end do ! ii
        end do ! l
      end do ! p

      ! ... Step 9: half of heat transfer term.
      do l = 1, ND
        t2MapSymViscjl = t2MapSymVisc(j,l)
        if (ND == 2) then
          t3Map1l1 = t3Map(1,l,1)
          t3Map2l2 = t3Map(2,l,2)
#ifdef ENABLE_SIMD
!DIR$ SIMD
#endif
          do ii = 1, Nc
            flux(ii) = MTProd(ii,t3Map1l1) + MTProd(ii,t3Map2l2) 
          end do ! ii
        end if
        if (ND == 3) then
          t3Map1l1 = t3Map(1,l,1)
          t3Map2l2 = t3Map(2,l,2)
          t3Map3l3 = t3Map(3,l,3)
#ifdef ENABLE_SIMD
!DIR$ SIMD
#endif
          do ii = 1, Nc
            flux(ii) = MTProd(ii,t3Map1l1) + MTProd(ii,t3Map2l2) &
                     + MTProd(ii,t3Map3l3)
          end do ! ii
        end if
!!$        if (PRESENT(tvCor) .eqv. .true.) then ! JKim 01/2008
!!$          state%flux(:) = state%flux(:) * tvCor(:,3) * TempGrad2nd(:,region%global%t2MapSymVisc(j,l))
!!$        else
#ifdef ENABLE_SIMD
!DIR$ SIMD
#endif
        do ii = 1, Nc
          flux(ii) = flux(ii) * tv(ii,3) * TempGrad2nd(ii,t2MapSymViscjl)
        end do ! ii
                                !!$        end if ! PRESENT(tvCor)
!if (state%RE > 0.0_rfreal) 
#ifdef ENABLE_SIMD
!DIR$ SIMD
#endif
        do ii = 1, Nc
          rhs(ii,vMapNDp2) = rhs(ii,vMapNDp2) + PRinv * flux(ii) 
        end do ! ii
      end do ! l

      ! ... Step 10: dilatation terms in momentum equations
      do i = 1, ND
        vMapip1 = vMap(i+1)
        do l = 1, ND
          do m = 1, ND
            t3Mapiml = t3Map(i,m,l)
            t2Maplm = t2Map(l,m)
!!$            if (PRESENT(tvCor) .eqv. .true.) then ! JKim 01/2008
!!$              state%flux(:) = MTProd(:,region%global%t3Map(i,m,l)) * tvCor(:,2)
!!$            else
#ifdef ENABLE_SIMD
!DIR$ SIMD
#endif
            do ii = 1, Nc
              flux(ii) = MTProd(ii,t3Mapiml) * tv(ii,2)
            end do ! ii
                                !!$            end if ! PRESENT(tvCor)
            call APPLY_OPERATOR_BOX(region, ng, 1, j, flux, dflux, .FALSE., TRUE)
#ifdef ENABLE_SIMD
!DIR$ SIMD
#endif
            do ii = 1, Nc
              rhs(ii,vMapip1) = rhs(ii,vMapip1) &
                              + dflux(ii) * VelGrad1st(ii,t2Maplm)
            end do ! ii
          end do ! m
        end do ! l
      end do ! i

      ! ... Step 11: dilatation work and part of viscous work in energy equation
      do l = 1, ND
        do m = 1, ND
          t2Maplm = t2Map(l,m)
          if (ND == 2) then
            t3Map1ml = t3Map(1,m,l)
            t3Map2ml = t3Map(2,m,l)
#ifdef ENABLE_SIMD
!DIR$ SIMD
#endif
            do ii = 1, Nc
              flux(ii) = MTProd(ii,t3Map1ml) * UVW(ii,1) + &
                         MTProd(ii,t3Map2ml) * UVW(ii,2)
            end do ! ii
          end if
          if (ND == 3) then
            t3Map1ml = t3Map(1,m,l)
            t3Map2ml = t3Map(2,m,l)
            t3Map3ml = t3Map(3,m,l)
#ifdef ENABLE_SIMD
!DIR$ SIMD
#endif
            do ii = 1, Nc
              flux(ii) = MTProd(ii,t3Map1ml) * UVW(ii,1) + &
                         MTProd(ii,t3Map2ml) * UVW(ii,2) + &
                         MTProd(ii,t3Map3ml) * UVW(ii,3)
            end do ! ii
          end if
#ifdef ENABLE_SIMD
!DIR$ SIMD
#endif
          do ii = 1, Nc
            flux(ii) = flux(ii) * tv(ii,2)
          end do ! ii
          call APPLY_OPERATOR_BOX(region, ng, 1, j, flux, dflux, .FALSE., TRUE)
#ifdef ENABLE_SIMD
!DIR$ SIMD
#endif
          do ii = 1, Nc
            rhs(ii,vMapNDp2) = rhs(ii,vMapNDp2) + dflux(ii) &
                             * VelGrad1st(ii,t2Maplm)
          end do ! ii
        end do ! m
      end do ! l
      do k = 1, ND
        do l = 1, ND
          t2Mapkl = t2Map(k,l)
          if (ND == 2) then
            t3Mapkl1 = t3Map(k,l,1)
            t3Mapkl2 = t3Map(k,l,2)
            t2Map1j = t2Map(1,j)
            t2Map2j = t2Map(2,j)
#ifdef ENABLE_SIMD
!DIR$ SIMD
#endif
            do ii = 1, Nc
              rhs(ii,vMapNDp2) = rhs(ii,vMapNDp2) + &
                                 MTProd(ii,t3Mapkl1) * tv(ii,1) &
                               * VelGrad1st(ii,t2Map1j) &
                               * VelGrad1st(ii,t2Mapkl) + &
                                 MTProd(ii,t3Mapkl2) * tv(ii,1) &
                               * VelGrad1st(ii,t2Map2j) &
                               * VelGrad1st(ii,t2Mapkl)
            end do ! ii
          end if
          if (ND == 3) then
            t3Mapkl1 = t3Map(k,l,1)
            t3Mapkl2 = t3Map(k,l,2)
            t3Mapkl3 = t3Map(k,l,3)
            t2Map1j = t2Map(1,j)
            t2Map2j = t2Map(2,j)
            t2Map3j = t2Map(3,j)
#ifdef ENABLE_SIMD
!DIR$ SIMD
#endif
            do ii = 1, Nc
              rhs(ii,vMapNDp2) = rhs(ii,vMapNDp2) + &
                                 MTProd(ii,t3Mapkl1) * tv(ii,1) &
                               * VelGrad1st(ii,t2Map1j) &
                               * VelGrad1st(ii,t2Mapkl) + &
                                 MTProd(ii,t3Mapkl2) * tv(ii,1) &
                               * VelGrad1st(ii,t2Map2j) &
                               * VelGrad1st(ii,t2Mapkl) + &
                                 MTProd(ii,t3Mapkl3) * tv(ii,1) &
                               * VelGrad1st(ii,t2Map3j) &
                               * VelGrad1st(ii,t2Mapkl)
            end do ! ii
          end if
        end do ! l
      end do ! k
      do p = 1, ND
        t2Mappj = t2Map(p,j)
        do l = 1, ND
          t2Mappl = t2Map(p,l)
          if (ND == 2) then
            t3Map1l1 = t3Map(1,l,1)
            t3Map2l2 = t3Map(2,l,2)
#ifdef ENABLE_SIMD
!DIR$ SIMD
#endif
            do ii = 1, Nc
              flux(ii) = MTProd(ii,t3Map1l1) + MTProd(ii,t3Map2l2) 
            end do ! ii
          end if
          if (ND == 3) then
            t3Map1l1 = t3Map(1,l,1)
            t3Map2l2 = t3Map(2,l,2)
            t3Map3l3 = t3Map(3,l,3)
#ifdef ENABLE_SIMD
!DIR$ SIMD
#endif
            do ii = 1, Nc
              flux(ii) = MTProd(ii,t3Map1l1) + MTProd(ii,t3Map2l2) &
                       + MTProd(ii,t3Map3l3)
            end do ! ii
          end if
#ifdef ENABLE_SIMD
!DIR$ SIMD
#endif
          do ii = 1, Nc
            flux(ii) = flux(ii) * tv(ii,1) * VelGrad1st(ii,t2Mappj) &
                     * VelGrad1st(ii,t2Mappl)
            rhs(ii,vMapNDp2) = rhs(ii,vMapNDp2) + flux(ii)
          end do ! ii
        end do ! l
      end do ! p

      ! ... Step 12: precompute j-derivative of metric-Jacobian product by 
      ! ...          viscosity for momentum equation
      ! ...          For memory-saving, overwrite results onto MTProdBKUP
      ! ...          When tvCor is present, need to get a backup of original 
      ! ...          MTProd, JKim 01/2008
                                !!$      if (PRESENT(tvCor) .eqv. .true.) then
                                !!$        MTProdBKUP = MTProd
                                !!$      end if ! PRESENT(tvCor)
      do k = 1, ND
        do l = 1, ND
          do m = 1, ND
            t3Mapklm = t3Map(k,l,m)
!!$            if (PRESENT(tvCor)  .eqv. .true.) then ! JKim 01/2008
!!$              state%flux(:) = MTProd(:,region%global%t3Map(k,l,m)) * tvCor(:,1)
!!$            else
#ifdef ENABLE_SIMD
!DIR$ SIMD
#endif
            do ii = 1, Nc
              flux(ii) = MTProd(ii,t3Mapklm) * tv(ii,1)
            end do ! ii
!!$            end if ! PRESENT(tvCor)
            call APPLY_OPERATOR_BOX(region, ng, 1, j, flux, dflux, .FALSE., TRUE)
#ifdef ENABLE_SIMD
!DIR$ SIMD
#endif
            do ii = 1, Nc
              MTProd(ii,t3Mapklm) = dflux(ii)
            end do ! ii
          end do ! m
        end do ! l
      end do ! k

      ! ... Step 13: compute rest of viscous terms in momentum equation
      do i = 1, ND
        vMapip1 = vMap(i+1)
        do l = 1, ND
          t2Mapil = t2Map(i,l)
          if (ND == 2) then
            t3Map1l1 = t3Map(1,l,1)
            t3Map2l2 = t3Map(2,l,2)
#ifdef ENABLE_SIMD
!DIR$ SIMD
#endif
            do ii = 1, Nc
              flux(ii) = MTProd(ii,t3Map1l1) + MTProd(ii,t3Map2l2)
            end do ! ii
          end if
          if (ND == 3) then
            t3Map1l1 = t3Map(1,l,1)
            t3Map2l2 = t3Map(2,l,2)
            t3Map3l3 = t3Map(3,l,3)
#ifdef ENABLE_SIMD
!DIR$ SIMD
#endif
            do ii = 1, Nc
              flux(ii) = MTProd(ii,t3Map1l1) + MTProd(ii,t3Map2l2) &
                       + MTProd(ii,t3Map3l3)
            end do ! ii
          end if
#ifdef ENABLE_SIMD
!DIR$ SIMD
#endif
          do ii = 1, Nc
            rhs(ii,vMap(i+1)) = rhs(ii,vMap(i+1)) + &
                                flux(ii) * VelGrad1st(ii,t2Mapil) 
          end do ! ii
        end do ! l
        do k = 1, ND
          if (ND == 2) then
            t3Mapk1i = t3Map(k,1,i)
            t3Mapk2i = t3Map(k,2,i)
            t2Mapk1 = t2Map(k,1)
            t2Mapk2 = t2Map(k,2)
#ifdef ENABLE_SIMD
!DIR$ SIMD
#endif
            do ii = 1, Nc
              rhs(ii,vMapip1) = rhs(ii,vMapip1) + &
                               MTProd(ii,t3Mapk1i) * VelGrad1st(ii,t2Mapk1) + &
                               MTProd(ii,t3Mapk2i) * VelGrad1st(ii,t2Mapk2)
            end do ! ii
          end if
          if (ND == 3) then
            t3Mapk1i = t3Map(k,1,i)
            t3Mapk2i = t3Map(k,2,i)
            t3Mapk3i = t3Map(k,3,i)
            t2Mapk1 = t2Map(k,1)
            t2Mapk2 = t2Map(k,2)
            t2Mapk3 = t2Map(k,3)
#ifdef ENABLE_SIMD
!DIR$ SIMD
#endif
            do ii = 1, Nc
              rhs(ii,vMapip1) = rhs(ii,vMapip1) + &
                               MTProd(ii,t3Mapk1i) * VelGrad1st(ii,t2Mapk1) + &
                               MTProd(ii,t3Mapk2i) * VelGrad1st(ii,t2Mapk2) + &
                               MTProd(ii,t3Mapk3i) * VelGrad1st(ii,t2Mapk3)
            end do ! ii
          end if
        end do ! k
      end do ! i

      ! ... Step 14: When tvCor is present, need to compute MTProd again because
      ! ...          we don't consider eddy-viscosity-type model in the following
      ! ...          viscous work term in energy equation, JKim 01/2008
!!$      if (PRESENT(tvCor)  .eqv. .true.) then
!!$        MTProd = MTProdBKUP
!!$        do k = 1, ND
!!$          do l = 1, ND
!!$            do m = 1, ND
!!$              state%flux(:) = MTProd(:,region%global%t3Map(k,l,m)) * state%tv(:,1)
!!$              call APPLY_OPERATOR(region, ng, 1, j, state%flux, state%dflux, .FALSE.)
!!$              MTProd(:,region%global%t3Map(k,l,m)) = state%dflux(:)
!!$            end do ! m
!!$          end do ! l
!!$        end do ! k
!!$      end if ! input%LES

      ! ... Step 15: compute rest of viscous work terms in energy equation
      do k = 1, ND
        do l = 1, ND
          t2Mapkl = t2Map(k,l)
          if (ND == 2) then
            t3Mapkl1 = t3Map(k,l,1)
            t3Mapkl2 = t3Map(k,l,2)
#ifdef ENABLE_SIMD
!DIR$ SIMD
#endif
            do ii = 1, Nc
              rhs(ii,vMapNDp2) = rhs(ii,vMapNDp2) + &
                                 MTProd(ii,t3Mapkl1) * UVW(ii,1) * VelGrad1st(ii,t2Mapkl) + &
                                 MTProd(ii,t3Mapkl2) * UVW(ii,2) * VelGrad1st(ii,t2Mapkl)
            end do ! ii
          end if
          if (ND == 3) then
            t3Mapkl1 = t3Map(k,l,1)
            t3Mapkl2 = t3Map(k,l,2)
            t3Mapkl3 = t3Map(k,l,3)
#ifdef ENABLE_SIMD
!DIR$ SIMD
#endif
            do ii = 1, Nc
              rhs(ii,vMapNDp2) = rhs(ii,vMapNDp2) + &
                                 MTProd(ii,t3Mapkl1) * UVW(ii,1) &
                               * VelGrad1st(ii,t2Mapkl) + &
                                 MTProd(ii,t3Mapkl2) * UVW(ii,2) &
                               * VelGrad1st(ii,t2Mapkl) + &
                                 MTProd(ii,t3Mapkl3) * UVW(ii,3) &
                               * VelGrad1st(ii,t2Mapkl)
            end do ! ii
          end if
        end do ! l
      end do ! k
      do p = 1, ND
        do l = 1, ND
          t2Mappl = t2Map(p,l)
          if (ND == 2) then
            t3Map1l1 = t3Map(1,l,1)
            t3Map2l2 = t3Map(2,l,2)
#ifdef ENABLE_SIMD
!DIR$ SIMD
#endif
            do ii = 1, Nc
              flux(ii) = MTProd(ii,t3Map1l1) + MTProd(ii,t3Map2l2)
            end do ! ii
          end if
          if (ND == 3) then
            t3Map1l1 = t3Map(1,l,1)
            t3Map2l2 = t3Map(2,l,2)
            t3Map3l3 = t3Map(3,l,3)
#ifdef ENABLE_SIMD
!DIR$ SIMD
#endif
            do ii = 1, Nc
              flux(ii) = MTProd(ii,t3Map1l1) + MTProd(ii,t3Map2l2) &
                       + MTProd(ii,t3Map3l3)
            end do ! ii
          end if
#ifdef ENABLE_SIMD
!DIR$ SIMD
#endif
          do ii = 1, Nc
            rhs(ii,vMapNDp2) = rhs(ii,vMapNDp2) + &
                               flux(ii) * UVW(ii,p) * VelGrad1st(ii,t2Mappl)  
          end do ! ii
        end do ! l
      end do ! p

      ! ... Step 16: When tvCor is present, MTProd should be recomputed due to 
      ! ...          artificial diffusivity, JKim 01/2008
!!$      if (PRESENT(tvCor) .eqv. .true.) then
!!$        MTProd = MTProdBKUP
!!$        do k = 1, ND
!!$          do l = 1, ND
!!$            do m = 1, ND
!!$              state%flux(:) = MTProd(:,region%global%t3Map(k,l,m)) * tvCor(:,3)
!!$              call APPLY_OPERATOR(region, ng, 1, j, state%flux, state%dflux, .FALSE.)
!!$              MTProd(:,region%global%t3Map(k,l,m)) = dflux(:)
!!$            end do ! m
!!$          end do ! l
!!$        end do ! k
!!$      end if ! PRESENT(tvCor)

      ! ... Step 17: complete energy equation
      do l = 1, ND
        if (ND == 2) then
          t3Map1l1 = t3Map(1,l,1)
          t3Map2l2 = t3Map(2,l,2)
#ifdef ENABLE_SIMD
!DIR$ SIMD
#endif
          do ii = 1, Nc
            flux(ii) = MTProd(ii,t3Map1l1) + MTProd(ii,t3Map2l2)
          end do ! ii
        end if
        if (ND == 3) then
          t3Map1l1 = t3Map(1,l,1)
          t3Map2l2 = t3Map(2,l,2)
          t3Map3l3 = t3Map(3,l,3)
#ifdef ENABLE_SIMD
!DIR$ SIMD
#endif
          do ii = 1, Nc
            flux(ii) = MTProd(ii,t3Map1l1) + MTProd(ii,t3Map2l2) &
                     + MTProd(ii,t3Map3l3)
          end do ! ii
        end if
#ifdef ENABLE_SIMD
!DIR$ SIMD
#endif
        do ii = 1, Nc
          flux(ii) = flux(ii) * TempGrad1st(ii,l)
!if (state%RE > 0.0_rfreal)
          rhs(ii,vMapNDp2) = rhs(ii,vMapNDp2) + PRinv * flux(ii)
        end do ! ii
      end do ! l

      ! ... Step 18: close j-loop.
      end do ! j

    !viscous_time = mpi_wtime() - viscous_time
    !if (region%myrank == 0) print *, 'Viscous terms (sec) = ', viscous_time
    !stop

    ! ... deallocate
    deallocate(UVW, VelGrad2nd, TempGrad2nd, MTProd)

    return

  end subroutine NS_RHS_ViscousTerms_OPT

  
#ifdef AXISYMMETRIC
  subroutine NS_RHS_Axisymmetric_Hopital(ng, Nc, nAuxVars, ND, iFirstDeriv, region, grid, input, flux, dflux, MT0, JAC, JAC0, INVJAC, rhs, rhs_auxVars, cv, dv, auxVars, flags, REinv, REPRinv, RESCinv, tv, UVW, StrnRt, HeatFlux,tv2div)


    USE ModGlobal
    USE ModDataStruct
    USE ModDeriv
    USE ModMPI

    Implicit None

    ! ... subroutine arguments
    integer :: ng, Nc, nAuxVars, ND, iFirstDeriv, flags
    type(t_region), pointer :: region
    type(t_grid), pointer :: grid
    type(t_mixt_input), pointer :: input
    real(rfreal), pointer :: flux(:), dflux(:)
    real(rfreal), pointer :: MT0(:,:),JAC0(:),JAC(:),INVJAC(:)
    real(rfreal), pointer :: rhs(:,:), rhs_auxVars(:,:), cv(:,:), dv(:,:), auxVars(:,:), XYZ(:,:)
    real(rfreal),optional :: REinv, REPRinv, RESCinv
    real(rfreal),optional, pointer :: tv(:,:),UVW(:,:), StrnRt(:,:), HeatFlux(:,:),tv2div(:)

    integer :: i, ii,  ip1, j, l, NDp2, k, m
    real(rfreal) :: twoReInv, dflux1, dflux2, tvby2, drMin, factor
    integer :: t2MapSymi1, t2MapSymi2, t2Mapl1, t2Mapl2
    integer, pointer :: t2Map(:,:), t2MapSym(:,:)


    if(ND /= 2) RETURN

    XYZ         => grid%XYZ
    drMin = grid%drMin
    NDp2 = ND+2

    if(flags>0) then 
      t2MapSym => region%global%t2MapSym
      t2Map    => region%global%t2Map
    end if

    if (flags == 0) then

      !... Continuity
      i=1
      do ii = 1, Nc
        flux(ii) = MT0(ii,1) * cv(ii,2)
      end do
      call POLE_DERIV(region, ng, iFirstDeriv, i, flux, dflux, .FALSE.)
      do ii = 1, Nc
        if(XYZ(ii,1) > drMin) dflux(ii)=0d0
        if(XYZ(ii,1) <= drMin) then
          dflux(ii)=dflux(ii)*JAC0(ii)*INVJAC(ii)*2d0
          rhs(ii,1) = rhs(ii,1) - dflux(ii)
        end if
      end do
      Call Save_Patch_Deriv(region, ng, 1, i, dflux)


      !... Momentum
      do i = 1, ND
        ip1 = i+1
        j=1
        do ii = 1, Nc
          flux(ii) = cv(ii,ip1) * MT0(ii,1) * cv(ii,j+1) * dv(ii,3)
        end do
        call POLE_DERIV(region, ng, iFirstDeriv, j, flux, dflux, .FALSE.)
        do ii = 1, Nc
          if(XYZ(ii,1) > DRmin) dflux(ii)=0d0
          if(XYZ(ii,1) <= DRmin) then
            dflux(ii)=dflux(ii)*JAC0(ii)*INVJAC(ii)*2d0
            rhs(ii,ip1) = rhs(ii,ip1) - dflux(ii)
          end if
        end do
        Call Save_Patch_Deriv(region, ng, ip1, j, dflux)
      end do ! i



      !... Energy
      j=NDp2
      i=1
      do ii = 1, Nc
        flux(ii) = (cv(ii,NDp2) + dv(ii,1)) * MT0(ii,1) * cv(ii,i+1) * dv(ii,3)
      end do
      call POLE_DERIV(region, ng, iFirstDeriv, i, flux, dflux, .FALSE.)
      do ii = 1, Nc
        if(XYZ(ii,1) > DRmin) dflux(ii)=0d0
        if(XYZ(ii,1) <= DRmin) then
          dflux(ii)=dflux(ii)*JAC0(ii)*INVJAC(ii)*2d0
          rhs(ii,j) = rhs(ii,j) - dflux(ii)
        end if
      end do
      Call Save_Patch_Deriv(region, ng, j, i, dflux)


      !... Species
      do k = 1, nAuxVars

        i=1
        do ii = 1, Nc
          flux(ii) = auxVars(ii,k) * MT0(ii,1) * cv(ii,i+1) * dv(ii,3)
        end do
        call POLE_DERIV(region, ng, iFirstDeriv, i, flux, dflux, .FALSE.)
        do ii = 1, Nc
          if(XYZ(ii,1) > DRmin) dflux(ii)=0d0
          if(XYZ(ii,1) <= DRmin) then
            dflux(ii)=dflux(ii)*JAC0(ii)*INVJAC(ii)*2d0
            rhs_auxVars(ii,k) = rhs_auxVars(ii,k) - dflux(ii)
          endif
        end do
      enddo

    elseif (flags == 1) then

!... Grad of lambda div u
       do i=1,ND
          ip1=i+1
          l=i
          t2Mapl1 = t2map(l,i)
#ifdef ENABLE_SIMD
!DIR$ SIMD
#endif
          do ii = 1, Nc
            flux(ii) = MT0(ii,t2Mapl1) * tv2div(ii)
          end do
          call APPLY_OPERATOR_box(region, ng, 1, l, flux, dflux, .FALSE., 0)

          do ii = 1, Nc
            factor = JAC0(ii)*INVJAC(ii) * REinv
            if(i==2 .or. XYZ(ii,1) > drMin) rhs(ii,ip1) = rhs(ii,ip1) + dflux(ii) * factor
            flux(ii)    = flux(ii) * factor
         end do
          Call Save_Patch_Viscous_Flux(region, ng, ip1, l, flux)
       end do

      !... momentum-r
      i=1;  !r-momentum
      ip1 = i+1
      twoReInv = 2.0_rfreal * REinv
      do ii = 1, Nc
        if(XYZ(ii,1) > DRmin) &
         rhs(ii,ip1) = rhs(ii,ip1) - tv(ii,1) * twoReInv * UVW(ii,i)*JAC(ii) / (JAC0(ii)*JAC0(ii))
      end do

      !... momentum-z
      l=1;
      do i = 2, ND
        ip1 = i + 1
        t2MapSymi1 = t2MapSym(i,1)
        t2MapSymi2 = t2MapSym(i,2)
        t2Mapl1 = t2Map(l,1)
        t2Mapl2 = t2Map(l,2)

#ifdef ENABLE_SIMD
!DIR$ SIMD
#endif
        do ii = 1, Nc                                         ! viscous stress tensor
          dflux1 =   tv(ii,1) * 2.0_rfreal * StrnRt(ii,t2MapSymi1) 
          dflux2 =   tv(ii,1) * 2.0_rfreal * StrnRt(ii,t2MapSymi2)
          flux(ii) = MT0(ii,t2Mapl1) * dflux1 + MT0(ii,t2Mapl2) * dflux2
        end do
        call POLE_DERIV(region, ng, 1, l, flux, dflux, .FALSE.)
        do ii = 1, Nc
          if(XYZ(ii,1) > drMin) cycle
          rhs(ii,ip1) = rhs(ii,ip1) + dflux(ii) * REinv*JAC0(ii)*INVJAC(ii)*2d0
        end do
      enddo


      !... energy
      l=1
      t2Mapl1 = t2Map(l,1)
      t2Mapl2 = t2Map(l,2)      ! dflux() is not a function of l !!

#ifdef ENABLE_SIMD
!DIR$ SIMD
#endif
      do ii = 1, Nc             ! viscous stress tensor
        tvby2   =   tv(ii,1) * 2.0_rfreal
        dflux1 =   UVW(ii,1) * (tv2div(ii) + tvby2 * StrnRt(ii,1)) + UVW(ii,2) * (tvby2 * StrnRt(ii,3))

        dflux2 =   UVW(ii,1) * (tvby2 * StrnRt(ii,3)) + UVW(ii,2) * (tv2div(ii) + tvby2 * StrnRt(ii,2))

        flux(ii) = MT0(ii,t2Mapl1) * dflux1  + MT0(ii,t2Mapl2) * dflux2
      end do
      call POLE_DERIV(region, ng, 1, l, flux, dflux, .FALSE.)

      do ii = 1, Nc
        if(XYZ(ii,1) <= drMin)  &
             rhs(ii,NDp2) = rhs(ii,NDp2) + dflux(ii) * REinv*JAC0(ii)*INVJAC(ii)*2d0
      end do


      
    elseif (flags >10) then

      
      m=flags-10

      l=1
      t2Mapl1 = t2Map(l,1)
      t2Mapl2 = t2Map(l,2)
      
#ifdef ENABLE_SIMD
!DIR$ SIMD
#endif
      do ii = 1, Nc
         flux(ii) = (MT0(ii,t2Mapl1) * UVW(ii,1) + MT0(ii,t2Mapl2) * UVW(ii,2))* RESCinv
      end do
      call POLE_DERIV(region, ng, 1, l, flux, dflux, .FALSE.)
      do ii = 1, Nc
         if(XYZ(ii,1) > drMin) dflux(ii)=0d0
         if(XYZ(ii,1) <= drMin) rhs_auxVars(ii,m) = rhs_auxVars(ii,m) + dflux(ii)*JAC0(ii)*INVJAC(ii)*2d0
      end do
      Call Save_Patch_Viscous_RHS(region,  ng,       m,    dflux)
   
     
    elseif (flags == 2) then

      !energy
      l=1
      t2Mapl1 = t2Map(l,1)
      t2Mapl2 = t2Map(l,2)

#ifdef ENABLE_SIMD
!DIR$ SIMD
#endif
      do ii = 1, Nc
        flux(ii) =   MT0(ii,t2Mapl1) * HeatFlux(ii,1) + MT0(ii,t2Mapl2) * HeatFlux(ii,2)
      end do
      call POLE_DERIV(region, ng, 1, l, flux, dflux, .FALSE.)
      do ii = 1, Nc
        if(XYZ(ii,1) <= drMin)rhs(ii,NDp2) = rhs(ii,NDp2) - dflux(ii) * REPRinv*JAC0(ii)*INVJAC(ii)*2d0
      end do
    endif

    return
  end subroutine NS_RHS_Axisymmetric_Hopital
#endif

END MODULE ModNavierStokesRHS
