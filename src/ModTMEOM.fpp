! Copyright (c) 2015, University of Illinois at Urbana-Champaign, XPACC
! License: MIT, http://opensource.org/licenses/MIT
!-----------------------------------------------------------------------
!
! ModTMEOM.f90
!
! - basic code for the computation of left and right hand sides for both
!   thermal and structural problems
!
! Revision history
! - 11 Mar 2012 : CMO - initial code
!
!
!-----------------------------------------------------------------------
Module ModTMEOM

CONTAINS

  Subroutine GetKt(region,ng,RHSorLHS)

    USE ModGlobal
    USE ModDataStruct
    USE ModTMInt
    USE ModPETSc

    Type(t_region), pointer :: region
    Integer :: ng, RHSorLHS

    ! ... local variables
    Type(t_mixt_input), pointer :: input
    Type(t_smixt), pointer :: TMstate
    Type(t_sgrid), pointer :: TMgrid
    Integer :: N(MAX_ND), Ne(MAX_ND)
    Integer, pointer :: LM(:,:)
    Integer :: ei, ej, ek, le, l0, dir, lp, i, j, k, p, dir2
    Integer :: NdsPerElem, ND, nGauss, nVars
    Real(rfreal), allocatable :: X(:,:), Xc(:,:), T(:)
    Real(rfreal), pointer :: dShpFcn(:,:,:), Weight(:), kt(:,:), rt_int(:)
    Real(rfreal), pointer :: F(:,:), Cinv(:,:), Jac(:,:)
    Real(rfreal) :: dJac, kppa, JF, PreMult, PreMult2

    ! ... simplicity
    TMgrid  => region%TMgrid(ng)
    TMstate => region%TMstate(ng)
    input   => TMgrid%input
    LM      => TMgrid%LM
    ND      =  TMgrid%ND
    dShpFcn => TMgrid%dShpFcn
    Weight  => TMgrid%Weight
    nVars   =  TMstate%nVars

    ! ... RHSorLHS =
    ! ...            1, insert rt_int only
    ! ...            2, insert kt only
    ! ...            3, insert both
    

    ! ... thermal conductivity
    kppa = input%ThrmCond

    N(:)  = 1
    do dir = 1, TMgrid%ND
       N(dir) = TMgrid%ie(dir)-TMgrid%is(dir)+1
    end do

    ! ... how many elements in each direction
    Ne(:) = 1
    if(input%ElemType == QUADRATIC) then
       do dir = 1, ND 
          Ne(dir) = (N(dir) - 1)/2
       end do
       if(ND == 2) then
          NdsPerElem = 8
          nGauss = 9
       elseif(ND > 2) then
          NdsPerElem = 20
          nGauss = 27
       end if
    elseif(input%ElemType == LINEAR) then
       do dir = 1, ND 
          Ne(dir) = (N(dir) - 1)
       end do
       if(ND == 2) then
          NdsPerElem = 4
          nGauss = 4
       elseif(ND > 2) then
          NdsPerElem = 8
          nGauss = 8
       end if
    end if

    ! ... allocate element local arrays
    ! ... current configuration, element thermal stiffness
    allocate(Xc(NdsPerElem,ND), kt(NdsPerElem,NdsPerElem))
   
     ! ... internal heat load
    allocate(rt_int(NdsPerElem))

    ! ... Temperature
    allocate(T(NdsPerElem))
    
    ! ... reference configuration
    allocate(X(NdsPerElem,ND))

    ! ... Jacobian matrix
    allocate(Jac(ND,ND))

    ! ... deformation gradient, inverse of right Cauchy strain tensor
    allocate(F(ND,ND),Cinv(ND,ND))

    ! ... loop through the elements
    do ek = 1, Ne(3)
       do ej = 1, Ne(2)
          do ei = 1, Ne(1)
             ! ... element index
             le = (ek-1)*Ne(1)*Ne(2) + (ej-1)*Ne(1) + ei

             ! ... fill the local array
             if(input%TMSolve == 3) then
                do dir = 1, ND
                   do lp = 1, NdsPerElem
                      X(lp,dir) = TMgrid%X(LM(lp,le),dir)
                      Xc(lp,dir) = TMstate%q(LM(lp,le),dir)
                   end do
                end do
             else
                do dir = 1, ND
                   do lp = 1, NdsPerElem
                      ! ... current configuration = reference configuration
                      Xc(lp,dir) = TMgrid%X(LM(lp,le),dir)
                      X(lp,dir) = TMgrid%X(LM(lp,le),dir)
                   end do
                end do
             end if

             do lp = 1, NdsPerElem
                T(lp) = TMstate%q(LM(lp,le),nVars)
             end do


             rt_int(:) = 0.0_rfreal
             kt(:,:) = 0.0_rfreal
             ! ... loop through the Gauss points
             do lg = 1, nGauss

                ! ... compute the Jacobian
                Jac(:,:) = 0.0_rfreal
                do j = 1, ND
                   do i = 1, ND
                      do lp = 1, NdsPerElem
                         Jac(i,j) = Jac(i,j) + dShpFcn(lp,j,lg)*X(lp,i)
                      end do
                   end do
                end do

                ! ... compute the determinant and inverse
                call InvertMatrix(Jac,dJac)

                ! ... check for singular matrix
                if(dJac .lt. tiny) write(*,'(A)') 'PlasComCM: Negative Jacobian in solid grid!'

                if(input%TMSolve == 3) then
                   ! ... compute the deformation gradient tensor
                   F(:,:) = 0.0_rfreal
                   do j = 1, ND
                      do i = 1, ND
                         do dir = 1, ND
                            do lp = 1, NdsPerElem
                               F(i,j) = F(i,j) + Xc(lp,i)*dShpFcn(lp,dir,lg)*Jac(dir,j)
                            end do
                         end do
                      end do
                   end do

                   Cinv = MATMUL(TRANSPOSE(F),F)

                   Call InvertMatrix(Cinv)

                   ! ... determinant of the deformation gradient tensor
                   Call MatrixDeterminant(F,JF)

                elseif(input%TMSolve == 1) then
                   Cinv(:,:) = 0.0_rfreal
                   do j = 1, ND
                      Cinv(j,j) = 1.0_rfreal
                   end do
                   JF = 1.0_rfreal
                end if
                
                ! ... add to integral at this Gauss point
                PreMult = kppa*dJac*Weight(lg)*JF
                do dir = 1, ND
                   do dir2 = 1, ND
                      do K = 1, ND
                         do P = 1, ND
                            PreMult2 = Cinv(P,K)*Jac(dir,P)*Jac(dir2,K)*PreMult
                            do j = 1, NdsPerElem
                               do i = 1, NdsPerElem

                                  kt(i,j) = kt(i,j) + dShpFcn(i,dir,lg)*dShpFcn(j,dir2,lg)*PreMult2
                                   !kt(i,j) = kt(i,j) + kppa*Cinv(P,K)*dShpFcn(i,dir,lg)*Jac(dir,P)*&
                                    !    dShpFcn(j,dir2,lg)*Jac(dir2,K)*dJac*Weight(lg)*JF
                               end do
                            end do
                         end do
                      end do
                   end do
                end do
             end do ! ... lg
             do j = 1, NdsPerElem
                do i = 1, NdsPerElem
                   rt_int(i) = rt_int(i) - kt(i,j)*T(j)
                end do
             end do
             ! ... add to PETSc matrix
             ! ... call PETSc here
             if(RHSorLHS > 1) call Insertkt(region, ng, le, kt, NdsPerElem)
             if(RHSorLHS == 1 .or. RHSorLHS == 3) call InsertRt_int(region, ng, NdsPerElem, le, rt_int)
          end do ! ... ei
       end do ! ... ej
    end do ! ... ek
    
    deallocate(F,Cinv,Jac,kt,rt_int)

  end Subroutine GetKt

  Subroutine GetMs(region,ng)

    USE ModGlobal
    USE ModDataStruct
    USE ModTMInt
    USE ModPETSc

    Type(t_region), pointer :: region
    Integer :: ng

    ! ... local variables
    Type(t_mixt_input), pointer :: input
    Type(t_smixt), pointer :: TMstate
    Type(t_sgrid), pointer :: TMgrid
    Integer :: N(MAX_ND), Ne(MAX_ND)
    Integer, pointer :: LM(:,:)
    Integer :: ei, ej, ek, le, l0, dir, lp, i, j, k, p, dir2
    Integer :: NdsPerElem, ND, nGauss, nVars
    Real(rfreal), allocatable :: X(:,:)
    Real(rfreal), pointer :: ShpFcn(:,:), dShpFcn(:,:,:)
    Real(rfreal), pointer :: Weight(:), mselem(:,:)
    Real(rfreal), pointer :: Jac(:,:)
    Real(rfreal) :: dJac, rho

    ! ... simplicity
    TMgrid  => region%TMgrid(ng)
    TMstate => region%TMstate(ng)
    input   => TMgrid%input
    LM      => TMgrid%LM
    ND      =  TMgrid%ND
    ShpFcn  => TMgrid%ShpFcn
    dShpFcn  => TMgrid%dShpFcn
    Weight  => TMgrid%Weight
    nVars   = TMstate%nVars

    ! ... material properties
    rho = input%TMDensity

    N(:)  = 1
    do dir = 1, TMgrid%ND
       N(dir) = TMgrid%ie(dir)-TMgrid%is(dir)+1
    end do

    ! ... how many elements in each direction
    Ne(:) = 1
    if(input%ElemType == QUADRATIC) then
       do dir = 1, ND 
          Ne(dir) = (N(dir) - 1)/2
       end do
       if(ND == 2) then
          NdsPerElem = 8
          nGauss = 9
       elseif(ND > 2) then
          NdsPerElem = 20
          nGauss = 27
       end if
    elseif(input%ElemType == LINEAR) then
       do dir = 1, ND 
          Ne(dir) = (N(dir) - 1)
       end do
       if(ND == 2) then
          NdsPerElem = 4
          nGauss = 4
       elseif(ND > 2) then
          NdsPerElem = 8
          nGauss = 8
       end if
    end if

    ! ... allocate element local arrays
    ! ... element mass matrix
    allocate(mselem(ND*NdsPerElem,ND*NdsPerElem))

    ! ... reference configuration
    allocate(X(NdsPerElem,ND))

    ! ... Jacobian matrix
    allocate(Jac(ND,ND))

    ! ... loop through the elements
    do ek = 1, Ne(3)
       do ej = 1, Ne(2)
          do ei = 1, Ne(1)
             ! ... element index
             le = (ek-1)*Ne(1)*Ne(2) + (ej-1)*Ne(1) + ei

             ! ... fill the element local data
             do dir = 1, ND
                do lp = 1, NdsPerElem
                   X(lp,dir) = TMgrid%X(LM(lp,le),dir)
                end do
             end do
            
             mselem(:,:) = 0.0_rfreal

             ! ... loop through the Gauss points
             do lg = 1, nGauss

                ! ... compute the Jacobian
                Jac(:,:) = 0.0_rfreal
                do j = 1, ND
                   do i = 1, ND
                      do lp = 1, NdsPerElem
                         Jac(i,j) = Jac(i,j) + dShpFcn(lp,j,lg)*X(lp,i)
                      end do
                   end do
                end do

                ! ... compute the determinant and inverse
                call MatrixDeterminant(Jac,dJac)

                ! ... check for singular matrix
                if(dJac .lt. tiny) write(*,'(A)') 'PlasComCM: Negative Jacobian in solid grid!'

                ! ... add to integral at this Gauss point
                do j = 1, NdsPerElem
                   do i = 1, NdsPerElem
                      do dir = 1, ND
                         l1 = (i-1)*ND + dir
                         l2 = (j-1)*ND + dir
                         mselem(l1,l2) = mselem(l1,l2) + rho*ShpFcn(i,lg)*ShpFcn(j,lg)*dJac*Weight(lg)
                      end do
                   end do
                end do
             end do ! ... lg
             ! ... add to PETSc matrix
             ! ... call PETSc here
             call InsertMass(region, ng, le, mselem, NdsPerElem)
          end do ! ... ei
       end do ! ... ej
    end do ! ... ek
    
    deallocate(Jac, mselem)

  end Subroutine GetMs

  Subroutine GetCt(region,ng)

    USE ModGlobal
    USE ModDataStruct
    USE ModTMInt
    USE ModPETSc

    Type(t_region), pointer :: region
    Integer :: ng

    ! ... local variables
    Type(t_mixt_input), pointer :: input
    Type(t_smixt), pointer :: TMstate
    Type(t_sgrid), pointer :: TMgrid
    Integer :: N(MAX_ND), Ne(MAX_ND)
    Integer, pointer :: LM(:,:)
    Integer :: ei, ej, ek, le, l0, dir, lp, i, j, k, p, dir2
    Integer :: NdsPerElem, ND, nGauss, nVars
    Real(rfreal), allocatable :: X(:,:), T(:)
    Real(rfreal), pointer :: dShpFcn(:,:,:),ShpFcn(:,:)
    Real(rfreal), pointer :: Weight(:), ct(:,:)
    Real(rfreal), pointer :: F(:,:), Jac(:,:)
    Real(rfreal) :: dJac, Cp, rho, dt_inv

    ! ... simplicity
    TMgrid  => region%TMgrid(ng)
    TMstate => region%TMstate(ng)
    input   => TMgrid%input
    LM      => TMgrid%LM
    ND      =  TMgrid%ND
    ShpFcn  => TMgrid%ShpFcn
    dShpFcn  => TMgrid%dShpFcn
    Weight  => TMgrid%Weight
    nVars   = TMstate%nVars

    ! ... material properties
    rho = input%TMDensity
    Cp = input%SpecHt

    ! ... we only ever run in CONSTANT_DT
    dt_inv  =  TMstate%dt_thermal

    ! ... invert the timestep
    dt_inv = 1.0_rfreal/dt_inv

    N(:)  = 1
    do dir = 1, TMgrid%ND
       N(dir) = TMgrid%ie(dir)-TMgrid%is(dir)+1
    end do

    ! ... how many elements in each direction
    Ne(:) = 1
    if(input%ElemType == QUADRATIC) then
       do dir = 1, ND 
          Ne(dir) = (N(dir) - 1)/2
       end do
       if(ND == 2) then
          NdsPerElem = 8
          nGauss = 9
       elseif(ND > 2) then
          NdsPerElem = 20
          nGauss = 27
       end if
    elseif(input%ElemType == LINEAR) then
       do dir = 1, ND 
          Ne(dir) = (N(dir) - 1)
       end do
       if(ND == 2) then
          NdsPerElem = 4
          nGauss = 4
       elseif(ND > 2) then
          NdsPerElem = 8
          nGauss = 8
       end if
    end if

    ! ... allocate element local arrays
    ! ... current configuration, element thermal stiffness
    allocate(ct(NdsPerElem,NdsPerElem))

    ! ... reference configuration
    allocate(X(NdsPerElem,ND))
    ! ... current temperature
    allocate(T(NdsPerElem))

    ! ... Jacobian matrix
    allocate(Jac(ND,ND))

    ! ... loop through the elements
    do ek = 1, Ne(3)
       do ej = 1, Ne(2)
          do ei = 1, Ne(1)
             ! ... element index
             le = (ek-1)*Ne(1)*Ne(2) + (ej-1)*Ne(1) + ei

             ! ... fill the element local data
             do dir = 1, ND
                do lp = 1, NdsPerElem
                   X(lp,dir) = TMgrid%X(LM(lp,le),dir)
                   T(lp) = TMstate%q(LM(lp,le),nVars)
                end do
             end do
            
             ct(:,:) = 0.0_rfreal

             ! ... loop through the Gauss points
             do lg = 1, nGauss

                ! ... compute the Jacobian
                Jac(:,:) = 0.0_rfreal
                do j = 1, ND
                   do i = 1, ND
                      do lp = 1, NdsPerElem
                         Jac(i,j) = Jac(i,j) + dShpFcn(lp,j,lg)*X(lp,i)
                      end do
                   end do
                end do

                ! ... compute the determinant and inverse
                call MatrixDeterminant(Jac,dJac)

                ! ... check for singular matrix
                if(dJac .lt. tiny) write(*,'(A)') 'PlasComCM: Negative Jacobian in solid grid!'

                ! ... add to integral at this Gauss point
                do j = 1, NdsPerElem
                   do i = 1, NdsPerElem
                      ct(i,j) = ct(i,j) + Cp*rho*ShpFcn(i,lg)*ShpFcn(j,lg)*dJac*Weight(lg)
                   end do
                end do
             end do ! ... lg

             ! ... add to PETSc matrix
             ! ... call PETSc here
             call Insertct(region, ng, le, ct, NdsPerElem)
          end do ! ... ei
       end do ! ... ej
    end do ! ... ek
    
    deallocate(Jac, ct)

  end Subroutine GetCt

  Subroutine GetInternalLoad(region,ng)

    USE ModGlobal
    USE ModDataStruct
    USE ModTMInt
    USE ModPETSc
    USE ModElasticity
    USE ModMPI

    Type(t_region), pointer :: region
    Integer :: ng

    ! ... local variables
    Type(t_mixt_input), pointer :: input
    Type(t_smixt), pointer :: TMstate
    Type(t_sgrid), pointer :: TMgrid
    Integer :: N(MAX_ND), Ne(MAX_ND)
    Integer, pointer :: LM(:,:)
    Integer :: ei, ej, ek, le, l0, dir, lp, i, j, k, p, dir2
    Integer :: NdsPerElem, ND, nGauss
    Real(rfreal), allocatable :: X(:,:), Xc(:,:), dT(:),dNdX(:,:)
    Real(rfreal), pointer :: dShpFcn(:,:,:), ShpFcn(:,:), Weight(:), rs_int(:), Jac(:,:), dShpdX(:,:)
    Real(rfreal) :: F(MAX_ND,MAX_ND), Cinv(MAX_ND,MAX_ND), Eye(MAX_ND,MAX_ND)
    Real(rfreal) :: Stress(MAX_ND,MAX_ND)
    Real(rfreal) :: dJac, JF, PreMult, ThermalInterpFac
    Real(rfreal) :: alpha, lambda, mu, beta, YngMod, PsnRat

    ! ... simplicity
    TMgrid  => region%TMgrid(ng)
    TMstate => region%TMstate(ng)
    input   => TMgrid%input
    LM      => TMgrid%LM
    ND      =  TMgrid%ND
    dShpFcn => TMgrid%dShpFcn
    ShpFcn  => TMgrid%ShpFcn
    Weight  => TMgrid%Weight
    nVars   =  TMstate%nVars

    ! ... thermal expansion coefficient
    alpha  = input%alpha

    ! ... identity matrix
    Eye(:,:) = 0.0_rfreal
    do i = 1, MAX_ND
       Eye(i,i) = 1.0_rfreal
    end do

    N(:)  = 1
    do dir = 1, TMgrid%ND
       N(dir) = TMgrid%ie(dir)-TMgrid%is(dir)+1
    end do

    ! ... how many elements in each direction
    Ne(:) = 1
    if(input%ElemType == QUADRATIC) then
       do dir = 1, ND 
          Ne(dir) = (N(dir) - 1)/2
       end do
       if(ND == 2) then
          NdsPerElem = 8
          nGauss = 9
       elseif(ND > 2) then
          NdsPerElem = 20
          nGauss = 27
       end if
    elseif(input%ElemType == LINEAR) then
       do dir = 1, ND 
          Ne(dir) = (N(dir) - 1)
       end do
       if(ND == 2) then
          NdsPerElem = 4
          nGauss = 4
       elseif(ND > 2) then
          NdsPerElem = 8
          nGauss = 8
       end if
    end if

    ! ... 
    allocate(dShpdX(NdsPerElem,ND))

    ! ... allocate element local arrays
    ! ... current configuration, element thermal stiffness
    allocate(Xc(NdsPerElem,ND), rs_int(ND*NdsPerElem))
    ! ... reference configuration
    allocate(X(NdsPerElem,ND))

    ! ... allocate Jacobian
    allocate(Jac(MAX_ND,MAX_ND))

    if(input%TMSolve == 3) allocate(dT(NdsPerElem))

    ! ... loop through the elements
    do ek = 1, Ne(3)
       do ej = 1, Ne(2)
          do ei = 1, Ne(1)
             ! ... element index
             le = (ek-1)*Ne(1)*Ne(2) + (ej-1)*Ne(1) + ei

             ! ... fill the local array
             do dir = 1, ND
                do lp = 1, NdsPerElem
                   X(lp,dir) = TMgrid%X(LM(lp,le),dir)
                   Xc(lp,dir) = TMstate%q(LM(lp,le),dir)

                   ! ... currently assumes uniform initial temperature
                   if(input%TMSolve == 3) dT(lp) = TMstate%q(LM(lp,le),nVars) - input%TMInitialTemp
                end do
             end do


             rs_int(:) = 0.0_rfreal

             ! ... loop through the Gauss points
             do lg = 1, nGauss


                ! ... compute beta, the isotropic stretch ratio
                beta = 1.0_rfreal
                if(input%TMSolve == 3) then 
                   do lp = 1, NdsPerElem
                      beta = beta + alpha*ShpFcn(lp,lg)*dT(lp)
                   end do
                end if
                
                ! ... get the material properties
                YngMod = 0.0_rfreal
                PsnRat = 0.0_rfreal
                do lp = 1, NdsPerElem
                   YngMod = YngMod + ShpFcn(lp,lg)*TMstate%YMod(LM(lp,le))
                   PsnRat = PsnRat + ShpFcn(lp,lg)*TMstate%PRat(LM(lp,le))
                end do

                ! ... compute the Jacobian
                Jac(:,:) = 0.0_rfreal
                do j = 1, ND
                   do i = 1, ND
                      do lp = 1, NdsPerElem
                         Jac(i,j) = Jac(i,j) + dShpFcn(lp,j,lg)*X(lp,i)
                      end do
                   end do
                end do

                ! ... plane strain
                if (ND == 2) Jac(MAX_ND,MAX_ND) = 1.0_rfreal

                ! ... compute the determinant and inverse
                call InvertMatrix(Jac,dJac)

                ! ... check for singular matrix
                if(dJac .lt. tiny) write(*,'(A)') 'PlasComCM: Negative Jacobian in solid grid!'

                dShpdX(:,:) = 0.0_rfreal
                ! ... coordinate transformation to Cartesian
                do l = 1, ND
                   do K = 1, ND 
                      do i = 1, NdsPerElem
                         dShpdX(i,K) = dShpdX(i,K) + dShpFcn(i,l,lg)*Jac(l,K)
                      end do
                   end do
                end do

                ! ... compute the deformation gradient tensor
                F(:,:) = 0.0_rfreal
                do j = 1, ND
                   do i = 1, ND
                      do lp = 1, NdsPerElem
                         F(i,j) = F(i,j) + Xc(lp,i)*dShpdX(lp,j)
                      end do
                   end do
                end do

                ! ... plane strain
                if (ND == 2) F(MAX_ND,MAX_ND) = 1.0_rfreal
                F(:,:) = F(:,:)/beta

                ! ... compute the first Piola-Kirchhoff stress tensor
                Call GetStress(region,ng,YngMod,PsnRat,F,Stress)
   
                ! ... add to integral at this Gauss point
                PreMult = beta*beta*dJac*Weight(lg)
                do K = 1, ND
                   do P = 1, ND
                      do i = 1, NdsPerElem
                         l0 = (i-1)*ND + P
                         rs_int(l0) = rs_int(l0) - Stress(P,K)*dShpdX(i,K)*PreMult
                            
                      end do
                   end do
                end do
             end do ! ... lg

             ! ... add to PETSc matrix
             ! ... call PETSc here
             call InsertRs_int(region, ng, NdsPerElem, le, rs_int)
          end do ! ... ei
       end do ! ... ej
    end do ! ... ek

    deallocate(rs_int,Jac,dShpdX)
    
  end Subroutine GetInternalLoad

  Subroutine GetTangentStiffness(region,ng)

    USE ModGlobal
    USE ModDataStruct
    USE ModTMInt
    USE ModPETSc
    USE ModElasticity

    Type(t_region), pointer :: region
    Integer :: ng

    ! ... local variables
    Type(t_mixt_input), pointer :: input
    Type(t_smixt), pointer :: TMstate
    Type(t_sgrid), pointer :: TMgrid
    Integer :: N(MAX_ND), Ne(MAX_ND)
    Integer, pointer :: LM(:,:)
    Integer :: ei, ej, ek, le, l0, dir, lp, i, j, k, p, dir2
    Integer :: NdsPerElem, ND, nGauss
    Real(rfreal), allocatable :: X(:,:), Xc(:,:), dT(:)
    Real(rfreal), pointer :: dShpFcn(:,:,:), ShpFcn(:,:), Weight(:), ks(:,:),Jac(:,:), dShpdX(:,:)
    Real(rfreal) :: F(MAX_ND,MAX_ND), Cinv(MAX_ND,MAX_ND), Eye(MAX_ND,MAX_ND)
    Real(rfreal) :: A(MAX_ND, MAX_ND, MAX_ND, MAX_ND)
    Real(rfreal) :: dJac, PreMult, PreMult2, PreMult3, ThermalInterpFac
    Real(rfreal) :: alpha, lambda, mu, beta, YngMod, PsnRat

    ! ... simplicity
    TMgrid  => region%TMgrid(ng)
    TMstate => region%TMstate(ng)
    input   => TMgrid%input
    LM      => TMgrid%LM
    ND      =  TMgrid%ND
    dShpFcn => TMgrid%dShpFcn
    Weight  => TMgrid%Weight
    nVars   =  TMstate%nVars
    ShpFcn  => TMgrid%ShpFcn

    ! ... thermal expansion coefficient
    alpha  = input%alpha

    ! ... identity matrix
    Eye(:,:) = 0.0_rfreal
    do i = 1, MAX_ND
       Eye(i,i) = 1.0_rfreal
    end do

    N(:)  = 1
    do dir = 1, TMgrid%ND
       N(dir) = TMgrid%ie(dir)-TMgrid%is(dir)+1
    end do

    ! ... how many elements in each direction
    Ne(:) = 1
    if(input%ElemType == QUADRATIC) then
       do dir = 1, ND 
          Ne(dir) = (N(dir) - 1)/2
       end do
       if(ND == 2) then
          NdsPerElem = 8
          nGauss = 9
       elseif(ND > 2) then
          NdsPerElem = 20
          nGauss = 27
       end if
    elseif(input%ElemType == LINEAR) then
       do dir = 1, ND 
          Ne(dir) = (N(dir) - 1)
       end do
       if(ND == 2) then
          NdsPerElem = 4
          nGauss = 4
       elseif(ND > 2) then
          NdsPerElem = 8
          nGauss = 8
       end if
    end if

    ! ... allocate B matrix
    allocate(dShpdX(NdsPerElem,ND))

    ! ... allocate element local arrays
    ! ... current configuration, element thermal stiffness
    allocate(Xc(NdsPerElem,ND), ks(ND*NdsPerElem,ND*NdsPerElem))
    ! ... reference configuration
    allocate(X(NdsPerElem,ND))

    ! ... Jacobian matrix
    allocate(Jac(MAX_ND,MAX_ND))

    if(input%TMSolve == 3) allocate(dT(NdsPerElem))

    ! ... loop through the elements
    do ek = 1, Ne(3)
       do ej = 1, Ne(2)
          do ei = 1, Ne(1)
             ! ... element index
             le = (ek-1)*Ne(1)*Ne(2) + (ej-1)*Ne(1) + ei
             
             ! ... fill the local array
             do dir = 1, ND
                do lp = 1, NdsPerElem
                   X(lp,dir) = TMgrid%X(LM(lp,le),dir)
                  ! print*,'TMgrid%X(LM(lp,le),dir)',TMgrid%X(LM(lp,le),dir),LM(lp,le),dir
                   Xc(lp,dir) = TMstate%q(LM(lp,le),dir)
                   ! ... currently assumes uniform initial temperature
                   if(input%TMSolve == 3) dT(lp) = TMstate%q(LM(lp,le),nVars) - input%TMInitialTemp
                end do
             end do

             ks(:,:) = 0.0_rfreal

             ! ... loop through the Gauss points
             do lg = 1, nGauss

                ! ... compute beta, the isotropic stretch ratio
                beta = 1.0_rfreal
                if(input%TMSolve == 3) then 
                   do lp = 1, NdsPerElem
                      beta = beta + alpha*ShpFcn(lp,lg)*dT(lp)
                   end do
                end if

                ! ... get the material properties
                YngMod = 0.0_rfreal
                PsnRat = 0.0_rfreal
                do lp = 1, NdsPerElem
                   YngMod = YngMod + ShpFcn(lp,lg)*TMstate%YMod(LM(lp,le))
                   PsnRat = PsnRat + ShpFcn(lp,lg)*TMstate%PRat(LM(lp,le))
                end do

                ! ... compute the Jacobian
                Jac(:,:) = 0.0_rfreal
                do j = 1, ND
                   do i = 1, ND
                      do lp = 1, NdsPerElem
                         Jac(i,j) = Jac(i,j) + dShpFcn(lp,j,lg)*X(lp,i)
                      end do
              
                   end do
                end do
                ! ... plane strain
                if (ND == 2) Jac(MAX_ND,MAX_ND) = 1.0_rfreal

                ! ... compute the determinant and inverse
                call InvertMatrix(Jac,dJac)

                ! ... check for singular matrix
                if(dJac .lt. tiny) write(*,'(A)') 'PlasComCM: Negative Jacobian in solid grid!'

                dShpdX(:,:) = 0.0_rfreal
                ! ... coordinate transformation to Cartesian
                do l = 1, ND
                   do K = 1, ND 
                      do i = 1, NdsPerElem
                         dShpdX(i,K) = dShpdX(i,K) + dShpFcn(i,l,lg)*Jac(l,K)
                      end do
                   end do
                end do

                ! ... compute the deformation gradient tensor
                F(:,:) = 0.0_rfreal
                do j = 1, ND
                   do i = 1, ND
                      do lp = 1, NdsPerElem
                         F(i,j) = F(i,j) + Xc(lp,i)*dShpdX(lp,j)
                      end do
                   end do
                end do

                ! ... plane strain
                if (ND == 2) F(MAX_ND,MAX_ND) = 1.0_rfreal
                F(:,:) = F(:,:)/beta

                ! ... compute the elasticity tensor
                Call GetElasticityTens(region,ng,YngMod,PsnRat,F,A)

                ! ... add to integral at this Gauss point
                PreMult = beta*beta*dJac*Weight(lg)
                do dir = 1, ND
                   do dir2 = 1, ND
                      do K = 1, ND
                         do P = 1, ND
                            PreMult2 = A(K,dir,P,dir2)*PreMult
                            do j = 1, NdsPerElem
                               do i = 1, NdsPerElem
                                  l1 = (i-1)*ND + dir
                                  l2 = (j-1)*ND + dir2
                                  ks(l1,l2) = ks(l1,l2) + dShpdX(i,K)*dShpdX(j,P)*PreMult2
                               end do
                            end do
                         end do
                      end do
                   end do
                end do
             end do ! ... lg

             ! ... add to PETSc matrix
             ! ... call PETSc here
             call Insertks(region, ng, le, ks, NdsPerElem)
          end do ! ... ei
       end do ! ... ej
    end do ! ... ek
      
    deallocate(ks, Jac, dShpdX)
    
  end Subroutine GetTangentStiffness

  Subroutine getMaterialProps(region, ng)

    USE ModDataStruct
    USE ModGlobal
   
    implicit none

    Type(t_region), pointer :: region
    Integer :: ng

    ! ... local variables
    Type(t_sgrid), pointer :: TMgrid
    Type(t_mixt_input), pointer :: input
    Type(t_smixt), pointer :: TMstate
    Real(rfreal), pointer :: MtlPrpTbl(:,:)
    Real(rfreal) :: dT
    Integer :: i, j, k, i1, i2
    Integer :: nTblPts, nVars
    

    ! ... simplicity
    TMgrid    => region%TMgrid(ng)
    TMstate   => region%TMstate(ng)
    input     => TMgrid%input
    nVars     =  TMstate%nVars
    

    ! ... fill the arrays
    if(input%TempDpndMtl /= TRUE .or. input%TMSolve == 2) then
       do i = 1, TMgrid%nPtsLoc
          TMstate%YMod(i) = input%YngMod
          TMstate%PRat(i) = input%PsnRto
       end do
       return
    else if(input%TempDpndMtl == TRUE .and. input%TMSolve == 3) then

       MtlPrpTbl => TMstate%MtlPrpTbl
       nTblPts   =  TMstate%nTblPts

       ! ... equally spaced temperatures in table
       dT = (MtlPrpTbl(nTblPts,1) - MtlPrpTbl(1,1))/(nTblPts-1)

       do j = 1, TMgrid%nPtsLoc
          ! ... properties constant outside of lookup table range
          if(TMstate%q(j,nVars) .le. MtlPrpTbl(1,1)) then
             TMstate%YMod(j) = MtlPrpTbl(1,2)
             TMstate%PRat(j) = MtlPrpTbl(1,3)
          elseif(TMstate%q(j,nVars) .ge. MtlPrpTbl(nTblPts,1)) then
             TMstate%YMod(j) = MtlPrpTbl(nTblPts,2)
             TMstate%PRat(j) = MtlPrpTbl(nTblPts,3)
          else
             ! ... linearly interpolate
             i1 = int((TMstate%q(j,nVars) - MtlPrpTbl(1,1))/dT)
             i2 = i1 + 1
             TMstate%YMod(j) = ((TMstate%q(j,nVars) - MtlPrpTbl(i1,1))/(MtlPrpTbl(i2,1) - MtlPrpTbl(i1,1))) &
                  * (MtlPrpTbl(i2,2) - MtlPrpTbl(i1,2)) +  MtlPrpTbl(i1,2)
             TMstate%PRat(j) = ((TMstate%q(j,nVars) - MtlPrpTbl(i1,1))/(MtlPrpTbl(i2,1) - MtlPrpTbl(i1,1))) &
                  * (MtlPrpTbl(i2,3) - MtlPrpTbl(i1,3)) +  MtlPrpTbl(i1,3)
          end if

       end do
    end if
             
  end Subroutine GetMaterialProps

  Subroutine InvertMatrix(A, DetOut)
    
    USE ModGlobal

    implicit none

    Real(rfreal), pointer :: A(:,:)
    Real(rfreal), allocatable :: B(:,:)
    Real(rfreal), optional :: DetOut
    Real(rfreal) :: Det, InvDet
    Integer :: ND, i, j
    
    ND = size(A,1)
    allocate(B(ND,ND))
    
    if(ND == 3) then
       
       Det = A(1,1)*(A(2,2)*A(3,3)-A(3,2)*A(2,3)) &
            -A(1,2)*(A(2,1)*A(3,3)-A(3,1)*A(2,3)) &
            +A(1,3)*(A(2,1)*A(3,2)-A(3,1)*A(2,2))
       
       InvDet = 1.0_rfreal/Det
       
       ! ... now the inverse
       B(1,1) = (A(2,2)*A(3,3)-A(3,2)*A(2,3))*InvDet
       B(1,2) = (A(1,3)*A(3,2)-A(3,3)*A(1,2))*InvDet
       B(1,3) = (A(1,2)*A(2,3)-A(2,2)*A(1,3))*InvDet
       B(2,1) = (A(2,3)*A(3,1)-A(3,3)*A(2,1))*InvDet
       B(2,2) = (A(1,1)*A(3,3)-A(3,1)*A(1,3))*InvDet
       B(2,3) = (A(1,3)*A(2,1)-A(2,3)*A(1,1))*InvDet
       B(3,1) = (A(2,1)*A(3,2)-A(3,1)*A(2,2))*InvDet
       B(3,2) = (A(1,2)*A(3,1)-A(3,2)*A(1,1))*InvDet
       B(3,3) = (A(1,1)*A(2,2)-A(2,1)*A(1,2))*InvDet
       
       ! ... overwrite A
       A(:,:) = B(:,:)
       
       
    elseif(ND == 2) then
       
       Det = A(1,1)*A(2,2)-A(2,1)*A(1,2)
       
       InvDet = 1.0_rfreal/Det
       
       ! ... now the inverse
       B(1,1) =   A(2,2)*InvDet
       B(1,2) = - A(1,2)*InvDet
       B(2,1) = - A(2,1)*InvDet
       B(2,2) =   A(1,1)*InvDet

       ! ... overwrite A
       A(:,:) = B(:,:)
       
    end if

    if(PRESENT(DetOut)) DetOut = Det
  
  end Subroutine InvertMatrix
  Subroutine MatrixDeterminant(A, Det)
    
    USE ModGlobal

    implicit none

    Real(rfreal), pointer :: A(:,:)
    Real(rfreal) :: Det
    Integer :: ND
    
    ND = size(A,1)
    
    if(ND == 3) then
       
       Det = A(1,1)*(A(2,2)*A(3,3)-A(3,2)*A(2,3)) &
            -A(1,2)*(A(2,1)*A(3,3)-A(3,1)*A(2,3)) &
            +A(1,3)*(A(2,1)*A(3,2)-A(3,1)*A(2,2))
       
    elseif(ND == 2) then
       
       Det = A(1,1)*A(2,2)-A(2,1)*A(1,2)
       
    end if

  end Subroutine MatrixDeterminant

end Module ModTMEOM
