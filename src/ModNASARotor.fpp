! Copyright (c) 2015, University of Illinois at Urbana-Champaign, XPACC
! License: MIT, http://opensource.org/licenses/MIT
Module ModNASARotor

Contains

  Subroutine NASA_Rotor_Angle_Change(region, input, time, dtheta, dtheta_dot, dtheta_ddot)

    USE ModGlobal
    USE ModDataStruct
    USE ModMPI

    Implicit None

    ! ... Global Variables
    Real(KIND=8) :: time, dtheta, dtheta_dot, dtheta_ddot
    Type(t_mixt_input), Pointer :: input
    Type(t_region), Pointer :: region

    ! ... Local Variables
    Real(KIND=8) :: targ, tramp, tstart, omega, omega_dot, omega_ddot, omega_inf

    ! ... preliminaries
    omega_inf  = input%initflow_rpm / 60.0_8 * TWOPI * input%LengRef / input%SndSpdRef
    tstart     = input%initflow_tstart
    tramp      = input%initflow_tramp


    ! ... calculat
    targ        = (time - tstart) / tramp
    omega       = 5D-1 * omega_inf * (1.0D0 + tanh(targ))
    omega_dot   = 5D-1 * omega_inf / tramp / (cosh(targ) * cosh(targ))
    omega_ddot  = omega_dot * (2D0 / tramp * tanh(targ))
    dtheta      = omega * time
    dtheta_dot  = omega_dot * time + omega
    dtheta_ddot = omega_ddot * time + 2D0 * omega_dot

  End Subroutine NASA_Rotor_Angle_Change


  Subroutine NASA_Rotor_Grid_Initialize(region)

    USE ModGlobal
    USE ModDataStruct
    USE ModMPI

    Implicit None

    ! ... Global variables 
    Type (t_region), Pointer :: region

    ! ... local variables
    Type (t_grid), Pointer :: grid
    Type (t_mixt_input), Pointer :: input
    Integer :: i, ng
    Real(KIND=8) :: xx, yy, radius, theta, theta_min_local, theta_max_local

    Do ng = 1, region%nGrids

      ! ... Simplicity
      grid => region%grid(ng)

      ! ... allocate and initialize, if necessary
      allocate(grid%XYZOld(grid%nCells,grid%ND))

      ! ... compute the radius and angle coordinates in the (x,y) plane and store them
      do i = 1, grid%nCells
        xx = grid%XYZ(i,1)
        yy = grid%XYZ(i,2)
        radius = sqrt(xx*xx + yy*yy)
        theta  = atan2(yy,xx)
        grid%XYZOld(i,1) = radius
        grid%XYZOld(i,2) = theta
      end do

    End Do

  End Subroutine NASA_Rotor_Grid_Initialize


  Subroutine Interpolate_In_Theta_NASA(region)

    USE ModGlobal
    USE ModDataStruct
    USE ModEOS
    USE ModDeriv
    USE ModIO
    USE ModMPI

    implicit none

    ! ... function call variables
    type(t_region), pointer :: region

    ! ... local variables
    type(t_patch), pointer :: patch
    type(t_mixt), pointer :: state
    type(t_grid), pointer :: grid
    type(t_mixt_input), pointer :: input
    integer :: ngrid, npatch, i, j, k, l0, lp, N(3), Ng(3), nvar, dir, ii, l1, l2, l3, itarget, Np(3)
    integer :: interface_comm_size, interface_comm_rank, sendsize, p, ierr
    integer, pointer :: recvcounts(:), recvdisp(:), gis(:,:), gie(:,:)
    real(rfreal), pointer :: theta(:,:), F_aux(:), sendbuf(:), recvbuf(:)
    real(rfreal) :: theta_target, xx, yy, omega, tstart, time, omega_target, theta_min, theta_max, theta_len
    real(rfreal) :: f1, f2, f3, t1, t2, t3, theta_min_local, theta_max_local, theta_min_global, theta_max_global
    real(rfreal) :: tramp, targ, omega_inf, dtheta, dtheta_target, theta_dot, theta_ddot

    ! ... Interpolate the solution along the interface plane
    ! ... Use the patch%comm communicator
    Do npatch = 1, region%nPatches

      patch => region%patch(npatch)
      if ( ((patch%bcType == SAT_BLOCK_INTERFACE) .or. (patch%bcType == FV_BLOCK_INTERFACE)) .and. &
           patch%comm /= MPI_COMM_NULL) then

        ! ... get details about this communicator
        Call MPI_Comm_Size(patch%comm, interface_comm_size, ierr)
        Call MPI_Comm_Rank(patch%comm, interface_comm_rank, ierr)

        ! ... datastructures
        grid   => region%grid(patch%gridID)
        state  => region%state(patch%gridID)
        input  => grid%input

        ! ... allocate space for send and recv buffers
        Np(:) = patch%ie(:)-patch%is(:)+1
        N(:)  = grid%ie(:)-grid%is(:)+1
        Ng(:) = grid%GlobalSize(:)

        ! ... send data on size of patches
        allocate(gis(3,interface_comm_size),gie(3,interface_comm_size))
        call MPI_Allgather(patch%is, 3, MPI_INTEGER, gis, 3, MPI_INTEGER, patch%comm, ierr)
        call MPI_Allgather(patch%ie, 3, MPI_INTEGER, gie, 3, MPI_INTEGER, patch%comm, ierr)

        ! ... send the original positions of the grid points only once
        if (associated(patch%XYZOld) .eqv. .false.) then

          ! ... for sending the grid data
          sendsize = Np(1)*Np(2)*3
          allocate(recvbuf(Ng(1)*Ng(2)*3))
          allocate(sendbuf(sendsize))
          allocate(recvcounts(interface_comm_size), recvdisp(interface_comm_size))

          ! ... fill the send buf
          Do k = patch%is(3), patch%ie(3)
            Do j = patch%is(2), patch%ie(2)
              Do i = patch%is(1), patch%ie(1)
                Do nvar = 1, 3
                  lp = ((k-patch%is(3))*Np(1)*Np(2) + (j-patch%is(2))*Np(1) + (i-patch%is(1)))*3 + nvar
                  l0 =  (k- grid%is(3))* N(1)* N(2) + (j- grid%is(2))* N(1) + (i- grid%is(1)+1)
                  sendbuf(lp) = grid%XYZOld(l0,nvar)
                End Do
              End Do
            End Do
          End Do

          ! ... use MPI_Allgather to determine the size of the data to be sent
          Call MPI_Allgather(sendsize, 1, MPI_INTEGER, recvcounts, 1, MPI_INTEGER, patch%comm, ierr)
          recvdisp(1) = 0
          do i = 2, interface_comm_size
            recvdisp(i) = recvdisp(i-1) + recvcounts(i-1)
          end do 

          ! ... call allgather
          call MPI_Allgatherv(sendbuf, sendsize, MPI_REAL8, recvbuf, recvcounts, recvdisp, MPI_REAL8, patch%comm, ierr) 

          ! ... save grid
          allocate(patch%XYZOld(Ng(1)*Ng(2),3))
          Do p = 1, interface_comm_size
            Np(:) = gie(:,p) - gis(:,p) + 1
            Do k = gis(3,p), gie(3,p)
              Do j = gis(2,p), gie(2,p)
                Do i = gis(1,p), gie(1,p)
                  Do nvar = 1, 3
                    lp = ((k-gis(3,p))*Np(2)*Np(1) + (j-gis(2,p))*Np(1) + i-gis(1,p))*3 + nvar + recvdisp(p)
                    l0 = (j-1)*Ng(1) + i
                    patch%XYZOld(l0,nvar) = recvbuf(lp)
                  End Do  
                End Do
              End Do
            End Do
          End Do 

          ! ... clear the unused buffers
          call mpi_barrier(patch%comm, ierr)
          deallocate(recvbuf)
          deallocate(sendbuf)
          deallocate(recvcounts)
          deallocate(recvdisp)

        End If

        ! ... for sending the flow data
        Np(:) = patch%ie(:)-patch%is(:)+1
        sendsize = Np(1)*Np(2)*5
        allocate(recvbuf(Ng(1)*Ng(2)*5))
        allocate(sendbuf(sendsize))
        allocate(recvcounts(interface_comm_size), recvdisp(interface_comm_size))

        ! ... fill the send buf
        Do k = patch%is(3), patch%ie(3)
          Do j = patch%is(2), patch%ie(2)
            Do i = patch%is(1), patch%ie(1)
              Do nvar = 1, 5
                lp = ((k-patch%is(3))*Np(1)*Np(2) + (j-patch%is(2))*Np(1) + (i-patch%is(1)))*5 + nvar
                l0 =  (k- grid%is(3))* N(1)* N(2) + (j- grid%is(2))* N(1) + (i- grid%is(1)+1)
                sendbuf(lp) = state%cv(l0,nvar)
              End Do
            End Do
          End Do
        End Do

        ! ... use MPI_Allgather to determine the size of the data to be sent
        Call MPI_Allgather(sendsize, 1, MPI_INTEGER, recvcounts, 1, MPI_INTEGER, patch%comm, ierr)
        recvdisp(1) = 0
        do i = 2, interface_comm_size
          recvdisp(i) = recvdisp(i-1) + recvcounts(i-1)
        end do 

        ! ... call allgather
        call MPI_Allgatherv(sendbuf, sendsize, MPI_REAL8, recvbuf, recvcounts, recvdisp, MPI_REAL8, patch%comm, ierr) 

        ! ... save state
        If (associated(patch%cv_in_global) .eqv. .false.) allocate(patch%cv_in_global(Ng(1)*Ng(2),5))
        Do p = 1, interface_comm_size
          Np(:) = gie(:,p) - gis(:,p) + 1
          Do k = gis(3,p), gie(3,p)
            Do j = gis(2,p), gie(2,p)
              Do i = gis(1,p), gie(1,p)
                Do nvar = 1, 5
                  lp = ((k-gis(3,p))*Np(2)*Np(1) + (j-gis(2,p))*Np(1) + i-gis(1,p))*5 + nvar + recvdisp(p)
                  l0 = (j-1)*Ng(1) + i
                  patch%cv_in_global(l0,nvar) = recvbuf(lp)
                End Do  
              End Do
            End Do
          End Do
        End Do 

        ! ... clear the unused buffers
        call mpi_barrier(patch%comm, ierr)
        deallocate(recvbuf)
        deallocate(sendbuf)
        deallocate(recvcounts)
        deallocate(recvdisp)

        ! ... at this point patch%XYZOld and patch%cv_in_global contain
        ! ... the original grid points and the current solution
        ! ... on the ENTIRE interface, not just on this process'
        ! ... view of the patch.
        ! ...
        ! ... our next step is to interpolate in theta the
        ! ... patch data for all of those points which were ORIGINALLY
        ! ... matched with this process' points
        ! ...
        ! ... Algorithm: 
        ! ... 1.    compute the theta value of all interface points at current time 
        ! ... 2.    compute the theta value of the target points at current time
        ! ... 3.    find theta interval
        ! ... 4.    perform interpolation
        ! ... 5.    store data in interp%cv_in
        ! ...
        ! ... preliminaries
        ! ... set the angular velocity and starting time
        time = state%time(1)
        Call NASA_Rotor_Angle_Change(region, input, time, dtheta, theta_dot, theta_ddot)

        ! ... make angular velocities correct for each grid
        dtheta_target = 0.0_8
        if (grid%iGridGlobal == 1) then
          dtheta_target = dtheta
          dtheta        = 0.0_8
        end if

        ! ... 1.  compute the theta value of all interface points at current time
        allocate(theta(Ng(1),Ng(2)))
        Do j = 1, Ng(2)
          Do i = 1, Ng(1)
            l0 = (j - 1)*Ng(1) + i
            theta(i,j) = patch%XYZOld(l0,2) + dtheta
          End Do
        End Do     

        ! ... loop over points on this processors' view of the interface and
        Do j = patch%is(2), patch%ie(2)
          Do i = patch%is(1), patch%ie(1)
            lp = (j-1)*Ng(1) + i

            ! ... index of target point in 3-D counting
            k  = patch%is(3)
            l0 = (k-grid%is(3))* N(1)* N(2) + (j-grid%is(2))* N(1) + (i-grid%is(1)+1)

            ! ... 2.    compute the theta value of the target points at current time  
            theta_target = patch%XYZOld(lp,2) + dtheta_target

            ! ... min/max theta for this grid line
            theta_min = theta(Ng(1),j)
            theta_max = theta(1,j)
            theta_len = theta_max - theta_min

            ! ... adjust theta_target based on periodicity
            If (theta_target < theta_min) Then
!!$              Write (*,'(I3,1X,A,3(E20.10,1X))') region%myrank, '>>> RFLOCM: Fixing theta_target < theta_min', theta_target, theta_min, theta_max
              Do While (theta_target < theta_min)
                theta_target = theta_target + theta_len
              End Do
!!$              Write (*,'(I3,1X,A,3(E20.10,1X))') region%myrank, '>>> RFLOCM: Fixed theta_target < theta_min', theta_target, theta_min, theta_max
            Else If (theta_target > theta_max) Then
!!$              Write (*,'(I3,1X,A,3(E20.10,1X))') region%myrank, '>>> RFLOCM: Fixing theta_target > theta_max', theta_target, theta_min, theta_max
              Do While (theta_target > theta_max)
                theta_target = theta_target - theta_len
              End Do
!!$              Write (*,'(I3,1X,A,3(E20.10,1X))') region%myrank, '>>> RFLOCM: Fixed theta_target > theta_max', theta_target, theta_min, theta_max
            End If
!!$            if (theta_target < theta_min) Then
!!$              write (*,'(I3,1X,A)') region%myrank, 'theta_target < theta_min'
!!$              stop
!!$            end if 
!!$            if (theta_target > theta_max) Then
!!$              write (*,'(I3,1X,A)') region%myrank, 'theta_target > theta_max'
!!$              stop
!!$            end if
          
            ! ... 3.    find theta interval
            itarget = 0
            Do ii = 1, Ng(1)-1
              if ( ((theta_target <= theta(ii,j)) .or. (dabs(theta_target-theta(ii,j)) <= 1d-8)) .and. &
                   ((theta_target > theta(ii+1,j)) .or. (dabs(theta_target-theta(ii+1,j)) <= 1d-8)) ) then
                itarget = ii
                exit
              end if
            End Do

            ! ... check success of finding an interval
            if (itarget == 0) then
              write (*,'(A)') 'RFLOCM: ERROR: Unable to find interval for theta-interpolation.'
              write (*,'(A,I3,A,I3)') 'RFLOCM: myrank = ', region%myrank, ' interface_comm_rank = ', interface_comm_rank 
              write (*,'(A,E20.10)') 'RFLOCM: theta_target = ', theta_target
              write (*,'(A,E20.10)') 'RFLOCM: theta_min = ', theta_min
              write (*,'(A,E20.10)') 'RFLOCM: theta_max = ', theta_max
              write (*,'(A,E20.10)') 'RFLOCM: theta_len = ', theta_len
              ii = 1
              write (*,'(I3,1X,E20.10)') ii, theta(ii,j)
              ii = Ng(1)
              write (*,'(I3,1X,E20.10)') ii, theta(ii,j)
              stop
            end if
            
            ! ... 4.    perform interpolation  
            ! ... 5.    store data in interp%cv_in
            ! ... make sure we aren't too close to the end
            if (itarget == (Ng(1)-1)) itarget = itarget - 1

            ! ... quadratic interpolation 
            l1 = (j - 1)*Ng(1) + itarget
            t1 = theta(itarget,j)
            l2 = l1 + 1
            t2 = theta(itarget+1,j)
            l3 = l2 + 1
            t3 = theta(itarget+2,j)

            Do nvar = 1, 5
              f1 = patch%cv_in_global(l1,nvar)
              f2 = patch%cv_in_global(l2,nvar)
              f3 = patch%cv_in_global(l3,nvar)
              patch%cv_in(lp,nvar) = f1 * (theta_target - t2) * (theta_target - t3) / ( (t1-t2)*(t1-t3) ) + &
                                     f2 * (theta_target - t1) * (theta_target - t3) / ( (t2-t1)*(t2-t3) ) + &
                                     f3 * (theta_target - t2) * (theta_target - t1) / ( (t3-t1)*(t3-t2) )
            End Do

          End Do
        End Do
        deallocate(gis, gie, theta)
      End If
    End Do
    call mpi_barrier(mycomm, ierr)

  End Subroutine Interpolate_In_Theta_NASA

  Subroutine Setup_Rotor_Stator_Interface_Comms(region)

    USE ModGlobal
    USE ModDataStruct
    USE ModEOS
    USE ModDeriv
    USE ModMPI

    Implicit None

    ! ... Global variables 
    TYPE (t_region), POINTER :: region

    ! ... local variables
    type(t_patch), pointer :: patch
    type(t_grid), pointer :: grid
    type(t_mixt), pointer :: state
    type(t_mixt_input), pointer :: input
    integer :: Nc, N(MAX_ND), Np(MAX_ND), i, j, k, l0, comm_group, interface_comm_group 
    integer :: my_color, npatch, interface_comm_size, ng, interface_comm, ierr
    integer, pointer :: color(:), interface_members(:)

    ! ... loop over all patches on this processor
    ! ... NOTE: this assumes only one grid per processor
    my_color = 0
    do npatch = 1, region%nPatches

      ! ... simplicity
      patch => region%patch(npatch)

      ! ... set the color whether we have SAT_BLOCK_INTERFACE
      if ( (patch%bcType == SAT_BLOCK_INTERFACE) .or. &
           (patch%bcType == FV_BLOCK_INTERFACE) ) my_color = 1

    end do

    ng = 1
    grid => region%grid(ng)

    ! ... find the processors that have the SAT_BLOCK_INTERFACE BC
    allocate(color(grid%numproc_inComm)); color(:) = 0
    Call MPI_Allgather(my_color, 1, MPI_INTEGER, color, 1, MPI_INTEGER, grid%comm, ierr)

    ! ... create a communicator based on those processors with my_color = 1
    interface_comm_size = sum(color(:))
    allocate(interface_members(interface_comm_size))
    i = 1;
    do j = 1, grid%numproc_inComm
      if (color(j) == 1) then
        interface_members(i) = j - 1
        i = i + 1;
      end if
    end do
    call MPI_Comm_Group(grid%comm, comm_group, ierr)
    call MPI_Group_incl(comm_group, interface_comm_size, interface_members, interface_comm_group, ierr)
    call MPI_Comm_Create(grid%comm, interface_comm_group, interface_comm, ierr)

    ! ... save patch communicator
    do npatch = 1, region%nPatches
      patch => region%patch(npatch)
      patch%comm = MPI_COMM_NULL
      if ( (patch%bcType == SAT_BLOCK_INTERFACE) .or. &
           (patch%bcType == FV_BLOCK_INTERFACE) ) patch%comm = interface_comm
    end do

    ! ... clean up
    deallocate(color, interface_members)

  End Subroutine Setup_Rotor_Stator_Interface_Comms


  Subroutine Exchange_Periodic_Data_NASA(region)

    USE ModGlobal
    USE ModDataStruct
    USE ModMPI

    Implicit None

    ! ... global variables
    Type (t_region), Pointer :: region                                                                                                       

    ! ... local variables
    Type(t_patch), Pointer :: patch
    Type(t_grid), Pointer :: grid
    Type(t_mixt), Pointer :: state
    Type(t_mixt_input), Pointer :: input
    Integer :: ng, npatch, Np(3), nnodes, lp, N(3), i, j, k, ioffset(3), ierr
    Integer :: reqs(2), stats(MPI_STATUS_SIZE,2), src, dest, rtag, stag, npatch_id, l0
    Real(RFREAL), Pointer :: sbuf(:), rbuf(:)
    Real(RFREAL) :: time, dtheta, theta_dot, theta_ddot, theta, rho_vr, rho_vt
    Real(RFREAL) :: cos_fac, sin_fac

    ! ... assume only one grid
    ng = 1
    grid  => region%grid(ng)
    input => grid%input
    state => region%state(ng)

    ! ... grid%pencilComm(dir) has the right decomposition
    If (grid%iGridGlobal == 1 .or. grid%iGridGlobal == 2) Then

      If (grid%numproc_inPencilComm(1) == 1) Then
        Call Graceful_Exit(region%myrank, 'ERROR in Subroutine Exchange_Periodic_Data_NASA: Need >= 2 processors in 1-dir on grids 1 & 2.')
      End If

      ! ... find the right patch
      npatch_id = 0
      Do npatch = 1, region%nPatches
        If ( (region%patch(npatch)%bcType == SAT_BLOCK_INTERFACE_PERIODIC) .or. &
             (region%patch(npatch)%bcType == FV_BLOCK_INTERFACE_PERIODIC) ) npatch_id = npatch
      End Do

      ! ... now work on that patch
      If (npatch_id /= 0) Then
  
        patch => region%patch(npatch_id)
        Np(:) = 1
        N(:) = 1
        nnodes = 1
        Do j = 1, grid%ND
          N(j)  = grid%ie(j) - grid%is(j) + 1
          Np(j) = patch%ie(j) - patch%is(j) + 1
          nnodes = nnodes * Np(j)
        End Do

        ! ... allocate space for send and recv buffers
        ! ... nnodes SHOULD be the same along pencil communicator
        Allocate(sbuf(nnodes*(grid%ND+2)),rbuf(nnodes*(grid%ND+2)))

        ! ... find theta offset
        time = state%time(1)
        Call NASA_Rotor_Angle_Change(region, input, time, dtheta, theta_dot, theta_ddot)

        ! ... determine how far into the domain we need to reach
        ioffset = 0
        if (patch%bcType == FV_BLOCK_INTERFACE_PERIODIC) ioffset(abs(patch%normDir)) = patch%normDir / abs(patch%normDir)

        ! ... put the data into the send buffer 
        Do k = patch%is(3), patch%ie(3)
          Do j = patch%is(2), patch%ie(2)
            Do i = patch%is(1), patch%ie(1)

              l0 = (k- grid%is(3) + ioffset(3))* N(1)* N(2) + &
                   (j- grid%is(2) + ioffset(2))* N(1) + &
                    i- grid%is(1)+1+ioffset(1)
              lp = (k-patch%is(3))*Np(1)*Np(2) + (j-patch%is(2))*Np(1) + (i-patch%is(1)+1)

              ! ... convert to radial velocities
              theta = grid%XYZOld(l0,2) + dtheta
              cos_fac = dcos(theta)
              sin_fac = dsin(theta) 
              rho_vr = ( state%cv(l0,2) * cos_fac + state%cv(l0,3) * sin_fac)
              rho_vt = (-state%cv(l0,2) * sin_fac + state%cv(l0,3) * cos_fac)
         
              ! ... fill the buffer
              sbuf((grid%ND+2)*(lp-1) + 1) = state%cv(l0,1)
              sbuf((grid%ND+2)*(lp-1) + 2) = rho_vr
              sbuf((grid%ND+2)*(lp-1) + 3) = rho_vt
              sbuf((grid%ND+2)*(lp-1) + 4) = state%cv(l0,4)
              sbuf((grid%ND+2)*(lp-1) + 5) = state%cv(l0,5)

            End Do
          End Do
        End Do 

        ! ... seelect src and dest
        If (grid%myrank_inPencilComm(1) == 0) Then
          src = grid%numproc_inPencilComm(1) - 1
          dest = grid%numproc_inPencilComm(1) - 1
          stag = 0
          rtag = 1
        Else If (grid%myrank_inPencilComm(1) == (grid%numproc_inPencilComm(1)-1)) Then
          src = 0
          dest = 0
          stag = 1
          rtag = 0
        Else
          write (*,'(A)') 'PlasComCM: ERROR: error in Exchange_Periodic_Data_NASA'
          Stop
        End If

        ! ... post the recv
        Call MPI_Irecv(rbuf, nnodes*(grid%ND+2), MPI_REAL8, src, rtag, grid%pencilComm(1), reqs(1), ierr)

        ! ... post the send
        Call MPI_Isend(sbuf, nnodes*(grid%ND+2), MPI_REAL8, dest, stag, grid%pencilComm(1), reqs(2), ierr)

        ! ... wait until done
        Call MPI_Waitall(2, reqs, stats, ierr)

        ! ... unpack the recv buffer
        ! ... put the data into the send buffer 
        Do k = patch%is(3), patch%ie(3)
          Do j = patch%is(2), patch%ie(2)
            Do i = patch%is(1), patch%ie(1)

              l0 = (k- grid%is(3))* N(1)* N(2) + (j- grid%is(2))* N(1) +  i- grid%is(1)+1
              lp = (k-patch%is(3))*Np(1)*Np(2) + (j-patch%is(2))*Np(1) + (i-patch%is(1)+1)

              ! ... read the buffer
              patch%cv_out(lp,1) = rbuf((grid%ND+2)*(lp-1) + 1)
              rho_vr = rbuf((grid%ND+2)*(lp-1) + 2)
              rho_vt = rbuf((grid%ND+2)*(lp-1) + 3)
              patch%cv_out(lp,4) = rbuf((grid%ND+2)*(lp-1) + 4)
              patch%cv_out(lp,5) = rbuf((grid%ND+2)*(lp-1) + 5)
       
              ! ... convert back to Cartesian velocities
              theta = grid%XYZOld(l0,2) + dtheta
              cos_fac = dcos(theta)
              sin_fac = dsin(theta) 
              patch%cv_out(lp,2) = (rho_vr * cos_fac - rho_vt * sin_fac)
              patch%cv_out(lp,3) = (rho_vr * sin_fac + rho_vt * cos_fac)

            End Do
          End Do
        End Do 

        ! ... deallocate buffers
        deallocate(rbuf,sbuf)

      End If

    End If

    ! ... have everybody wait
    Call MPI_Barrier(mycomm, ierr)

  End Subroutine Exchange_Periodic_Data_NASA

  Subroutine Move_Rotor_Grid_NASA(region)

    USE ModGlobal
    USE ModDataStruct
    USE ModDeriv
    USE ModMPI

    ! ... input variables
    type(t_region), pointer :: region

    ! ... local variables
    type(t_grid), pointer :: grid
    type(t_mixt), pointer :: state
    type(t_mixt_input), pointer :: input
    integer :: ng, offset, i
    real(rfreal) :: fac, time, omega, theta, radius, xx, yy, tstart, arg
    real(rfreal) :: theta_min_local, theta_max_local, theta_min_global, theta_max_global
    real(rfreal) :: omega_inf, tramp, omega_dot, omega_ddot, dtheta, theta_dot, theta_ddot, cos_arg, sin_arg
    integer :: ierr

    ! ... loop through all grids, only moving the rotor mesh
    do ng = 1, region%nGrids

      grid => region%grid(ng)
      state => region%state(ng)
      input => grid%input
      time = state%time(1)

      if (associated(grid%XYZ_TAU) .eqv. .false.) then
        allocate(grid%XYZ_TAU(grid%nCells,grid%ND))
      end if

      if (associated(grid%XYZ_TAU2) .eqv. .false.) then
        allocate(grid%XYZ_TAU2(grid%nCells,grid%ND))
      end if

      ! ... default
      grid%XYZ_TAU(:,:) = 0.0_8
      grid%XYZ_TAU2(:,:) = 0.0_8

      if (input%moveGrid == TRUE .and. (grid%iGridGlobal == 2 .or. grid%iGridGlobal == 3)) then

        Call NASA_Rotor_Angle_Change(region, input, time, dtheta, theta_dot, theta_ddot)

        ! ... theta-position is theta0 + Omega * time
        do i = 1, grid%nCells
          radius  = grid%XYZOld(i,1)
          theta   = grid%XYZOld(i,2)
          arg     = theta + dtheta
          cos_arg = dcos(arg)
          sin_arg = dsin(arg)

          ! ... position
          grid%XYZ(i,1) = radius * cos_arg
          grid%XYZ(i,2) = radius * sin_arg

          ! ... velocity
          grid%XYZ_TAU(i,1) = -radius * theta_dot * sin_arg
          grid%XYZ_TAU(i,2) =  radius * theta_dot * cos_arg
          grid%XYZ_TAU(i,3) = 0.0_8

          ! ... acceleration
          grid%XYZ_TAU2(i,1) = -radius * theta_ddot * sin_arg - radius * theta_dot * theta_dot * cos_arg
          grid%XYZ_TAU2(i,2) =  radius * theta_ddot * cos_arg - radius * theta_dot * theta_dot * sin_arg
          grid%XYZ_TAU2(i,3) = 0.0_8
        end do

      else

        do i = 1, grid%nCells
          grid%XYZ_TAU(i,:) = 0.0_8
          grid%XYZ_TAU2(i,:) = 0.0_8
        end do

      end if

    end do

    ! ... everyone wait
    call mpi_barrier(mycomm, ierr)

  End Subroutine Move_Rotor_Grid_NASA

End Module ModNASARotor
