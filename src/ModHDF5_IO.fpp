! Copyright (c) 2015, University of Illinois at Urbana-Champaign, XPACC
! License: MIT, http://opensource.org/licenses/MIT
Module ModHDF5_IO
  USE ModMPI
#ifdef HAVE_HDF5
  use hdf5 ! lower-case 'use' to trick makedeps
#endif
  
  Implicit None

  Logical, parameter :: m_useChunking = .false., m_collective = .true.
  Character(len=1), parameter :: m_prec = 'd'

  LOGICAL :: hdf5Initd = .false.


Contains

#ifdef HAVE_HDF5

  SUBROUTINE InitHDF5()

    IMPLICIT NONE
    
    INTEGER :: ierror
!    WRITE(*,*) 'INit called'
    IF (hdf5Initd .eqv. .false.) THEN
!       WRITE(*,*) 'H5open called'
       CALL h5open_f(ierror)
       IF (ierror.NE.0) STOP 'h5open_f: failed'    
       hdf5Initd = .true.
    ENDIF

  END SUBROUTINE InitHDF5



  Subroutine write_HDF_file(region, vars_to_write, openCloseFlag, gridFile, iter_in)
    USE ModGlobal
    USE ModDataStruct
    USE ModMPI
    Implicit None

    type(t_region), pointer :: region
    character(len=3),pointer,dimension(:) :: vars_to_write
    integer, optional, Dimension(2),INTENT(IN) :: openCloseFlag
    Character(LEN=80), Intent(In),optional :: gridFile
    integer, optional :: iter_in

    ! Local variables
    character(len=3) :: var_to_write
    type(t_grid), pointer :: grid
    type(t_mixt), pointer :: state
    type(t_electric), pointer :: electric
    Real(KIND=8), Dimension(:), Pointer :: CONFIG,CONFIG_
    INTEGER, Dimension(:), Pointer :: ICONFIG,ICONFIG_
    integer :: nglobal, ng, numGridsInFile, NDIM, openClose(2)
    integer :: funit = 50, fnamelen,nv2w, iter,  ierror, ierr
    logical :: containsMesh, hasGrid, is_restart
    Character(LEN=80) :: gFname
    integer :: use_IB
    integer :: c1,c2

    ! ... return early if scaling run
    !    If (region%input%caseID(1:7) == 'SCALING') Return

    ! Open and/or close the xml file; default to do both
    If (present(openCloseFlag) .eqv. .true.) Then
       openClose = openCloseFlag
    Else
       openClose = 1
    End If


    If (present(iter_in) .eqv. .true.) Then
       iter = iter_in
    Else
       iter = region%global%main_ts_loop_index
    End If

    containsMesh = any(vars_to_write == 'xyz')
    is_restart = any(vars_to_write == 'rst')

! ... step 1
! ... set the filename (note: if changing m_fname, may need to increase its string length)
    write(region%hdf5_io%m_fname(1:18),'(A10,I8.8)') 'RocFlo-CM.', iter
    if (is_restart) then
      write(region%hdf5_io%m_fname(19:29),'(A11)') '.restart.h5'
      region%hdf5_io%m_fname_len = 29
    else
      write(region%hdf5_io%m_fname(19:21),'(A3)') '.h5'
      region%hdf5_io%m_fname_len = 21
    end if

    If(.not. present(gridFile) .and. .not. containsMesh) then
!default to taking the grid from 0000 file
       write(gFname(1:18),'(A10,I8.8)') 'RocFlo-CM.', 0
       write(gFname(19:21),'(A3)') '.h5'
    ElseIf(present(gridFile)) then
       gFname = gridFile
    endIf

    If(openClose(1)>=1) then
       Allocate(CONFIG(1))
       CONFIG(1) = real(region%global%main_ts_loop_index,rfreal)
       Call open_HDF5(CONFIG,region%nGridsGlobal, region%hdf5_io)
       Call openCloseXML(region%myrank, region%hdf5_io, 1)
       Deallocate(CONFIG);
    endIf
    NDIM = region%input%ND

! ... step 3
! ... write grids
    Loop1: Do nglobal = 1, region%nGridsGlobal

       nullify(state)
       hasGrid = .false.
       Loop2: Do ng = 1, region%nGrids
          grid => region%grid(ng)
          state => region%state(ng)
          electric => region%electric(ng)
          If (grid%iGridGlobal == nglobal) then
             hasGrid = .true.
             exit Loop2
          endIf
       enddo Loop2
       If(.not. hasGrid) then
          grid => region%grid(1)
          state => region%state(1)  !a placeholder
          electric => region%electric(1)
       endIf

       Allocate(CONFIG(4))
       Allocate(CONFIG_(4))
       Allocate(ICONFIG(3+NDIM))
       Allocate(ICONFIG_(3+NDIM))
       If (hasGrid) then
          use_IB = merge(1,0,associated(grid%IBLANK))
          CONFIG(1) = state%SC
          CONFIG(2) = state%PR
          CONFIG(3) = state%RE
          CONFIG(4) = state%time(1)
          ICONFIG(1) = use_IB
          ICONFIG(2) = NDIM
          ICONFIG(3:3+NDIM-1) = grid%GlobalSize(1:NDIM)
       Else
          CONFIG=0d0
          ICONFIG=0
       end If
       ICONFIG(3+NDIM) = merge(1,0,hasGrid)

       ! Use allreduce to set config everywhere
       ICONFIG_=ICONFIG
       Call MPI_Allreduce(ICONFIG_, ICONFIG, size(ICONFIG_)-1, MPI_INTEGER, MPI_MAX, mycomm, ierr)
       If(openClose(1)>=1) then
          CONFIG_=CONFIG
          Call MPI_Allreduce(CONFIG_, CONFIG, size(CONFIG_), MPI_DOUBLE_PRECISION, MPI_MAX, mycomm, ierr)
          Call createGroupWriteHeader(ICONFIG, CONFIG, nglobal, region%input%nAuxVars, region%hdf5_io, region%myrank)
       End If
       Deallocate(CONFIG,CONFIG_,ICONFIG_);

       ! Add an existing grid to XML without adding it to the h5 file
       If(present(gridFile) .or. .not. containsMesh) then
          Call addXMLdataset(region%myrank, region%hdf5_io, ICONFIG, 'xyz', nglobal, NDIM, gFname)
       endIf

       ! Write data
       Loop3: Do nv2w = 1, ubound(vars_to_write,1)

          var_to_write = vars_to_write(nv2w)

          If ( var_to_write(1:3) == 'cv ' ) Then
             If (region%myrank == 0) Write (*,'(A)') 'PlasComCM: Writing cv to '//region%hdf5_io%m_fname(1:region%hdf5_io%m_fname_len)
             Call addDataSet(ICONFIG, grid, nglobal, var_to_write, region%hdf5_io, region%myrank, state%cv)
          end If

          If ( var_to_write(1:3) == 'cvt' ) Then
             If (region%myrank == 0) Write (*,'(A)') 'PlasComCM: Writing cvt to '//region%hdf5_io%m_fname(1:region%hdf5_io%m_fname_len)
             Call addDataSet(ICONFIG, grid, nglobal, var_to_write, region%hdf5_io, region%myrank, state%cvTarget)
          end If

          If ( var_to_write(1:3) == 'rst' ) Then
             IF(.NOT.PRESENT(gridFile) .AND. containsMesh) THEN
                var_to_write(1:3) = 'xyz'
                If (region%myrank == 0) Write (*,'(A)') 'PlasComCM: Writing rst(xyz) to '//region%hdf5_io%m_fname(1:region%hdf5_io%m_fname_len)
                Call addDataSet(ICONFIG, grid, nglobal, var_to_write, region%hdf5_io, region%myrank)
             ENDIF
             var_to_write(1:3) = 'cv '
             If (region%myrank == 0) Write (*,'(A)') 'PlasComCM: Writing rst(cv) to '//region%hdf5_io%m_fname(1:region%hdf5_io%m_fname_len)
             Call addDataSet(ICONFIG, grid, nglobal, var_to_write, region%hdf5_io, region%myrank, state%cv)
             var_to_write(1:3) = 'cvt'
             If (region%myrank == 0) Write (*,'(A)') 'PlasComCM: Writing rst(cvt) to '//region%hdf5_io%m_fname(1:region%hdf5_io%m_fname_len)
             Call addDataSet(ICONFIG, grid, nglobal, var_to_write, region%hdf5_io, region%myrank, state%cvTarget)
             if (region%input%nAuxVars > 0) Then
               var_to_write(1:3) = 'aux'
               If (region%myrank == 0) Write (*,'(A)') 'PlasComCM: Writing rst(aux) to '//region%hdf5_io%m_fname(1:region%hdf5_io%m_fname_len)
               Call addDataSet(ICONFIG, grid, nglobal, var_to_write, region%hdf5_io, region%myrank, state%auxVars)
               var_to_write(1:3) = 'aut'
               If (region%myrank == 0) Write (*,'(A)') 'PlasComCM: Writing rst(aut) to '//region%hdf5_io%m_fname(1:region%hdf5_io%m_fname_len)
               Call addDataSet(ICONFIG, grid, nglobal, var_to_write, region%hdf5_io, region%myrank, state%auxVarsTarget)
             end if
          end If

          If ( var_to_write(1:3) == 'xyz' ) Then
             If (region%myrank == 0) Write (*,'(A)') 'PlasComCM: Writing xyz to '//region%hdf5_io%m_fname(1:region%hdf5_io%m_fname_len)
             Call addDataSet(ICONFIG, grid, nglobal, var_to_write, region%hdf5_io, region%myrank)
          end If

          If ( var_to_write(1:3) == 'met' ) then
             If (region%myrank == 0) Write (*,'(A)') 'PlasComCM: Writing met to '//region%hdf5_io%m_fname(1:region%hdf5_io%m_fname_len)
             Call addDataSet(ICONFIG, grid, nglobal, var_to_write, region%hdf5_io, region%myrank, grid%MT1)
          end If

          If ( var_to_write(1:3) == 'jac' ) then
             If (region%myrank == 0) Write (*,'(A)') 'PlasComCM: Writing jac to '//region%hdf5_io%m_fname(1:region%hdf5_io%m_fname_len)
             Call addDataSet(ICONFIG, grid, nglobal, var_to_write, region%hdf5_io, region%myrank, grid%MT1)
          end If

          If ( var_to_write(1:3) == 'aux' ) then
             If (region%myrank == 0) Write (*,'(A)') 'PlasComCM: Writing aux to '//region%hdf5_io%m_fname(1:region%hdf5_io%m_fname_len)
             Call addDataSet(ICONFIG, grid, nglobal, var_to_write, region%hdf5_io, region%myrank, state%auxVars)
          end If

          If ( var_to_write(1:3) == 'aut' ) then
             If (region%myrank == 0) Write (*,'(A)') 'PlasComCM: Writing aut to '//region%hdf5_io%m_fname(1:region%hdf5_io%m_fname_len)
             Call addDataSet(ICONFIG, grid, nglobal, var_to_write, region%hdf5_io, region%myrank, state%auxVarsTarget)
          end If

          If ( var_to_write(1:3) == 'dv ' ) then
             If (region%myrank == 0) Write (*,'(A)') 'PlasComCM: Writing dv to '//region%hdf5_io%m_fname(1:region%hdf5_io%m_fname_len)
             Call addDataSet(ICONFIG, grid, nglobal, var_to_write, region%hdf5_io, region%myrank, state%dv)
          end If

          If ( var_to_write(1:3) == 'drv' ) Then
            If (region%myrank == 0) Write (*,'(A)') 'PlasComCM: Writing drv to '//region%hdf5_io%m_fname(1:region%hdf5_io%m_fname_len)
             Call addDataSet(ICONFIG, grid, nglobal, var_to_write, region%hdf5_io, region%myrank, state%cv)
          end If

          If ( var_to_write(1:3) == 'mid' ) then
             If (region%myrank == 0) Write (*,'(A)') 'PlasComCM: Writing mid to '//region%hdf5_io%m_fname(1:region%hdf5_io%m_fname_len)
             Call addDataSet(ICONFIG, grid, nglobal, var_to_write, region%hdf5_io, region%myrank, grid%ident)
          end If

#ifdef BUILD_ELECTRIC
          If (var_to_write(1:3) == 'phi' ) then
             If (region%myrank == 0) Write (*,'(A)') 'PlasComCM: Writing phi to '//region%hdf5_io%m_fname(1:region%hdf5_io%m_fname_len)
             electric%phi_data_ptr(:,1) = electric%phi
             Call addDataSet(ICONFIG, grid, nglobal, var_to_write, region%hdf5_io, region%myrank, electric%phi_data_ptr)
          end If
#endif

       End Do Loop3

!close the grid in the XML file
       If(region%myrank == 0) Write (region%hdf5_io%m_xmf,'(10a)') '   </Grid>'
       Deallocate(ICONFIG);

    End Do Loop1


    If(openClose(2)>=1) Then
       Call h5fclose_f(region%hdf5_io%m_file_id, ierror)     
       Call openCloseXML(region%myrank, region%hdf5_io, 2)
    End If

  End Subroutine write_HDF_file


  Subroutine WriteHDF5File(region, varsToWrite, restartIn)
    USE ModGlobal
    USE ModDataStruct
    USE ModMPI
    Implicit None

    type(t_region), pointer :: region
    character(len=3),pointer,dimension(:) :: varsToWrite
    INTEGER, OPTIONAL :: restartIn

    ! Local variables
    character(len=3) :: var_to_write
    type(t_grid), pointer :: grid
    type(t_mixt), pointer :: state
    type(t_electric), pointer :: electric
    Real(KIND=8), Dimension(:), Pointer :: CONFIG,CONFIG_
    INTEGER, Dimension(:), Pointer :: ICONFIG,ICONFIG_
    integer :: nglobal, ng, numGridsInFile, NDIM, openClose(2)
    integer :: funit = 50, fnamelen,nv2w, iter,  ierror, ierr
    logical :: writeMesh, hasGrid
    Character(LEN=80) :: gFname
    integer :: use_IB
    integer :: c1,c2
    INTEGER :: isRestart = FALSE
    REAL(RFREAL) :: timer

    iter = region%global%main_ts_loop_index
    writeMesh = any(varsToWrite == 'xyz')
    IF(PRESENT(restartIn)) isRestart = restartIn


! ... step 1
! ... set the filename (note: if changing m_fname, may need to increase its string length)
    write(region%hdf5_io%m_fname(1:18),'(A10,I8.8)') 'RocFlo-CM.', iter
    if (isRestart == TRUE) then
      write(region%hdf5_io%m_fname(19:29),'(A11)') '.restart.h5'
      region%hdf5_io%m_fname_len = 29
    else
      write(region%hdf5_io%m_fname(19:21),'(A3)') '.h5'
      write(region%hdf5_io%m_fname(22:29),'(A8)') '        '
      region%hdf5_io%m_fname_len = 21
    end if

    If(.NOT. writeMesh) then
       write(gFname(1:18),'(A10,I8.8)') 'RocFlo-CM.', 0
       write(gFname(19:21),'(A3)') '.h5'
    endif

    IF(region%input%usempiio == TRUE) THEN
       timer = MPI_WTIME()
       CALL CreateAndFillHDF5(region,varsToWrite)
       CALL MPI_Barrier(mycomm,iError)
       region%mpi_timings(:)%io_setup = region%mpi_timings(:)%io_setup + (MPI_WTIME() - timer)
       CALL WriteHDF5Data_MPIIO(region)
    ELSE
       Allocate(CONFIG(1))
       CONFIG(1) = real(region%global%main_ts_loop_index,rfreal)
       Call open_HDF5(CONFIG,region%nGridsGlobal, region%hdf5_io)
       Call openCloseXML(region%myrank, region%hdf5_io, 1)
       Deallocate(CONFIG);

       NDIM = region%input%ND
       
       ! ... step 3
       ! ... write grids
       Loop1: Do nglobal = 1, region%nGridsGlobal
          
          nullify(state)
          hasGrid = .false.
          Loop2: Do ng = 1, region%nGrids
             grid => region%grid(ng)
             state => region%state(ng)
             electric => region%electric(ng)
             If (grid%iGridGlobal == nglobal) then
                hasGrid = .true.
                exit Loop2
             endIf
          enddo Loop2
          If(.not. hasGrid) then
             grid => region%grid(1)
             state => region%state(1)  !a placeholder
             electric => region%electric(1)
          endIf
          
          Allocate(CONFIG(4))
          Allocate(CONFIG_(4))
          Allocate(ICONFIG(3+NDIM))
          Allocate(ICONFIG_(3+NDIM))
          If (hasGrid) then
             use_IB = merge(1,0,associated(grid%IBLANK))
             CONFIG(1) = state%SC
             CONFIG(2) = state%PR
             CONFIG(3) = state%RE
             CONFIG(4) = state%time(1)
             ICONFIG(1) = use_IB
             ICONFIG(2) = NDIM
             ICONFIG(3:3+NDIM-1) = grid%GlobalSize(1:NDIM)
          Else
             CONFIG=0d0
             ICONFIG=0
          end If
          ICONFIG(3+NDIM) = merge(1,0,hasGrid)
          
          ! Use allreduce to set config everywhere
          ICONFIG_=ICONFIG
          Call MPI_Allreduce(ICONFIG_, ICONFIG, size(ICONFIG_)-1, MPI_INTEGER, MPI_MAX, mycomm, ierr)
          CONFIG_=CONFIG
          Call MPI_Allreduce(CONFIG_, CONFIG, size(CONFIG_), MPI_DOUBLE_PRECISION, MPI_MAX, mycomm, ierr)
          Call createGroupWriteHeader(ICONFIG, CONFIG, nglobal, region%input%nAuxVars, region%hdf5_io, region%myrank)
          Deallocate(CONFIG,CONFIG_,ICONFIG_);
          
          ! Add an existing grid to XML without adding it to the h5 file
          If(.NOT. writeMesh) then
             Call addXMLdataset(region%myrank, region%hdf5_io, ICONFIG, 'xyz', nglobal, NDIM, gFname)
          endIf
          
          
          If ( isRestart == TRUE ) Then
             IF(writeMesh) THEN
                var_to_write(1:3) = 'xyz'
                If (region%myrank == 0) Write (*,'(A)') 'PlasComCM: Writing rst(xyz) to '//region%hdf5_io%m_fname(1:region%hdf5_io%m_fname_len)
                Call addDataSet(ICONFIG, grid, nglobal, var_to_write, region%hdf5_io, region%myrank)
             ENDIF
             var_to_write(1:3) = 'cv '
             If (region%myrank == 0) Write (*,'(A)') 'PlasComCM: Writing rst(cv) to '//region%hdf5_io%m_fname(1:region%hdf5_io%m_fname_len)
             Call addDataSet(ICONFIG, grid, nglobal, var_to_write, region%hdf5_io, region%myrank, state%cv)
             var_to_write(1:3) = 'cvt'
             If (region%myrank == 0) Write (*,'(A)') 'PlasComCM: Writing rst(cvt) to '//region%hdf5_io%m_fname(1:region%hdf5_io%m_fname_len)
             Call addDataSet(ICONFIG, grid, nglobal, var_to_write, region%hdf5_io, region%myrank, state%cvTarget)
             if (region%input%nAuxVars > 0) Then
                var_to_write(1:3) = 'aux'
                If (region%myrank == 0) Write (*,'(A)') 'PlasComCM: Writing rst(aux) to '//region%hdf5_io%m_fname(1:region%hdf5_io%m_fname_len)
                Call addDataSet(ICONFIG, grid, nglobal, var_to_write, region%hdf5_io, region%myrank, state%auxVars)
                var_to_write(1:3) = 'aut'
                If (region%myrank == 0) Write (*,'(A)') 'PlasComCM: Writing rst(aut) to '//region%hdf5_io%m_fname(1:region%hdf5_io%m_fname_len)
                Call addDataSet(ICONFIG, grid, nglobal, var_to_write, region%hdf5_io, region%myrank, state%auxVarsTarget)
             end if
#ifdef BUILD_ELECTRIC
             If (var_to_write(1:3) == 'phi' ) then
                If (region%myrank == 0) Write (*,'(A)') 'PlasComCM: Writing phi to '//region%hdf5_io%m_fname(1:region%hdf5_io%m_fname_len)
                electric%phi_data_ptr(:,1) = electric%phi
                Call addDataSet(ICONFIG, grid, nglobal, var_to_write, region%hdf5_io, region%myrank, electric%phi_data_ptr)
             end If
#endif
          ELSE
             ! Write data
             Loop3: Do nv2w = 1, ubound(varsToWrite,1)
                
                var_to_write = varsToWrite(nv2w)
                
                
                
                If ( var_to_write(1:3) == 'cv ' ) Then
                   If (region%myrank == 0) Write (*,'(A)') 'PlasComCM: Writing cv to '//region%hdf5_io%m_fname(1:region%hdf5_io%m_fname_len)
                   Call addDataSet(ICONFIG, grid, nglobal, var_to_write, region%hdf5_io, region%myrank, state%cv)
                end If
                
                If ( var_to_write(1:3) == 'cvt' ) Then
                   If (region%myrank == 0) Write (*,'(A)') 'PlasComCM: Writing cvt to '//region%hdf5_io%m_fname(1:region%hdf5_io%m_fname_len)
                   Call addDataSet(ICONFIG, grid, nglobal, var_to_write, region%hdf5_io, region%myrank, state%cvTarget)
                end If
                
                If ( var_to_write(1:3) == 'xyz' ) Then
                   If (region%myrank == 0) Write (*,'(A)') 'PlasComCM: Writing xyz to '//region%hdf5_io%m_fname(1:region%hdf5_io%m_fname_len)
                   Call addDataSet(ICONFIG, grid, nglobal, var_to_write, region%hdf5_io, region%myrank)
                end If
                
                If ( var_to_write(1:3) == 'met' ) then
                   If (region%myrank == 0) Write (*,'(A)') 'PlasComCM: Writing met to '//region%hdf5_io%m_fname(1:region%hdf5_io%m_fname_len)
                   Call addDataSet(ICONFIG, grid, nglobal, var_to_write, region%hdf5_io, region%myrank, grid%MT1)
                end If
                
                If ( var_to_write(1:3) == 'jac' ) then
                   If (region%myrank == 0) Write (*,'(A)') 'PlasComCM: Writing jac to '//region%hdf5_io%m_fname(1:region%hdf5_io%m_fname_len)
                   Call addDataSet(ICONFIG, grid, nglobal, var_to_write, region%hdf5_io, region%myrank, grid%MT1)
                end If
                
                If ( var_to_write(1:3) == 'aux' ) then
                   If (region%myrank == 0) Write (*,'(A)') 'PlasComCM: Writing aux to '//region%hdf5_io%m_fname(1:region%hdf5_io%m_fname_len)
                   Call addDataSet(ICONFIG, grid, nglobal, var_to_write, region%hdf5_io, region%myrank, state%auxVars)
                end If
                
                If ( var_to_write(1:3) == 'aut' ) then
                   If (region%myrank == 0) Write (*,'(A)') 'PlasComCM: Writing aut to '//region%hdf5_io%m_fname(1:region%hdf5_io%m_fname_len)
                   Call addDataSet(ICONFIG, grid, nglobal, var_to_write, region%hdf5_io, region%myrank, state%auxVarsTarget)
                end If
                
                If ( var_to_write(1:3) == 'dv ' ) then
                   If (region%myrank == 0) Write (*,'(A)') 'PlasComCM: Writing dv to '//region%hdf5_io%m_fname(1:region%hdf5_io%m_fname_len)
                   Call addDataSet(ICONFIG, grid, nglobal, var_to_write, region%hdf5_io, region%myrank, state%dv)
                end If
                
                If ( var_to_write(1:3) == 'drv' ) Then
                   If (region%myrank == 0) Write (*,'(A)') 'PlasComCM: Writing drv to '//region%hdf5_io%m_fname(1:region%hdf5_io%m_fname_len)
                   Call addDataSet(ICONFIG, grid, nglobal, var_to_write, region%hdf5_io, region%myrank, state%cv)
                end If
                
                If ( var_to_write(1:3) == 'mid' ) then
                   If (region%myrank == 0) Write (*,'(A)') 'PlasComCM: Writing mid to '//region%hdf5_io%m_fname(1:region%hdf5_io%m_fname_len)
                   Call addDataSet(ICONFIG, grid, nglobal, var_to_write, region%hdf5_io, region%myrank, grid%ident)
                end If
                
#ifdef BUILD_ELECTRIC
                If (var_to_write(1:3) == 'phi' ) then
                   If (region%myrank == 0) Write (*,'(A)') 'PlasComCM: Writing phi to '//region%hdf5_io%m_fname(1:region%hdf5_io%m_fname_len)
                   electric%phi_data_ptr(:,1) = electric%phi
                   Call addDataSet(ICONFIG, grid, nglobal, var_to_write, region%hdf5_io, region%myrank, electric%phi_data_ptr)
                end If
#endif
                
             End Do Loop3
          ENDIF
          !close the grid in the XML file
          If(region%myrank == 0) Write (region%hdf5_io%m_xmf,'(10a)') '   </Grid>'
          Deallocate(ICONFIG);
          
       End Do Loop1
    
       
       Call h5fclose_f(region%hdf5_io%m_file_id, ierror)     
       Call openCloseXML(region%myrank, region%hdf5_io, 2)

    END IF

  End Subroutine WriteHDF5File


  Subroutine open_HDF5(CONFIG, ngrids, hdf5_io)
    USE ModGlobal
    USE ModDataStruct
    USE ModMPI

    Implicit None

    Real(KIND=8), Dimension(:), Pointer :: CONFIG
    Integer,INTENT(IN) :: ngrids
    Type(t_hdf5_io), Pointer :: hdf5_io
    Integer          :: ierror
    INTEGER(HID_T) :: dcpl_id ,access_id     ! Property list identIfier 
    Integer(hid_t)   :: dtype_id, itype_id, ctype_id
    Integer(hid_t)   :: header_id, aspace_id, attr_id
    Integer(hsize_t) :: hdims(1), idims(3)

    Integer :: I, J, K, N
    Integer(hid_t)   :: dataset_id
    Integer(hid_t)   :: dataspace_id
    INTEGER(HID_T) :: plist_id      ! Property list identIfier 
    Integer(hid_t) :: memspace      ! Dataspace identIfier in memory
    Character(LEN=10) :: myName

! Initialize FORTRAN predefined datatypes
    CALL InitHDF5

! Create the HDF5 file
    Call h5pcreate_f(H5P_FILE_ACCESS_F, access_id, ierror)
    Call h5pset_fapl_mpio_f(access_id, mycomm, hdf5_io%m_info, ierror)
    Call h5fcreate_f(hdf5_io%m_fname(1:hdf5_io%m_fname_len), H5F_ACC_TRUNC_F, hdf5_io%m_file_id, ierror, access_prp = access_id)
    Call h5pclose_f(access_id, ierror)

! Assign the data file types
    itype_id=H5T_NATIVE_INTEGER
    ctype_id=H5T_NATIVE_CHARACTER
    If (m_prec .eq. 'd') Then
       dtype_id=H5T_NATIVE_DOUBLE
    Else
       dtype_id=H5T_NATIVE_REAL
    End If

    Call h5gopen_f(hdf5_io%m_file_id, '/.', header_id, ierror)

! Write the simulation header (whatever was put in config)
    hdims(1) = size(CONFIG)
    Call h5screate_simple_f(1, hdims, aspace_id, ierror)
    Call h5acreate_f(header_id, "HEADER", dtype_id, aspace_id, &
         attr_id, ierror)
    Call h5awrite_f(attr_id, dtype_id, CONFIG(1:hdims(1)), hdims, ierror)
    Call h5sclose_f(aspace_id, ierror)
    Call h5aclose_f(attr_id, ierror)

! print ngrids (necessary for restart)
    hdims(1) = 1
    Call h5screate_simple_f(1, hdims, aspace_id, ierror)
    Call h5acreate_f(header_id, "numberOfGrids", itype_id, aspace_id, &
         attr_id, ierror)
    Call h5awrite_f(attr_id, itype_id, nGrids, hdims, ierror)
    Call h5sclose_f(aspace_id, ierror)
    Call h5aclose_f(attr_id, ierror)

    Call h5gclose_f(header_id, ierror)

  end Subroutine open_HDF5


  Subroutine createGroupWriteHeader(ICONFIG, CONFIG, ng, nAuxVars, hdf5_io, myrank)

    USE ModGlobal
    USE ModDataStruct
    USE ModMPI

    Implicit None

    Integer, Dimension(:), Pointer :: ICONFIG
    Real(KIND=8), Dimension(:), Pointer :: CONFIG
    Integer, Intent(In) :: ng, nAuxVars, myrank
    Type(t_hdf5_io), Pointer :: hdf5_io
    Integer          :: ierror
    CHARACTER(LEN=8) :: groupname = "Grid"

    INTEGER(HID_T) :: group_id ,aspace_id, attr_id     ! Property list identIfier 
    Integer(hid_t)   :: dtype_id, itype_id, ctype_id
    Integer(hsize_t) :: hdims(1), idims(3)
    Character(LEN=10) :: GridType
    integer :: use_IB,GD(3)

    write (groupname,'("Group",I3.3)') ng
    Call h5gcreate_f(hdf5_io%m_file_id, trim(groupname), group_id, ierror)

! Assign the data file types
    itype_id=H5T_NATIVE_INTEGER
    ctype_id=H5T_NATIVE_CHARACTER
    If (m_prec .eq. 'd') Then
       dtype_id=H5T_NATIVE_DOUBLE
    Else
       dtype_id=H5T_NATIVE_REAL
    End If

! Write the simulation header (i.e., whatever it was put into CONFIG)
    hdims(1) = size(CONFIG)
    Call h5screate_simple_f(1, hdims, aspace_id, ierror)
    Call h5acreate_f(group_id, "HEADER", dtype_id, aspace_id, &
         attr_id, ierror)
    Call h5awrite_f(attr_id, dtype_id, CONFIG, hdims, ierror)
    Call h5sclose_f(aspace_id, ierror)
    Call h5aclose_f(attr_id, ierror)

!write IBflag (necessary to restart)
    use_IB = ICONFIG(1)
    hdims(1) = 1
    Call h5screate_simple_f(1, hdims, aspace_id, ierror)
    Call h5acreate_f(group_id, "useIB", H5T_NATIVE_INTEGER, aspace_id, &
         attr_id, ierror)
    Call h5awrite_f(attr_id, H5T_NATIVE_INTEGER, use_IB, hdims, ierror)
    Call h5sclose_f(aspace_id, ierror)
    Call h5aclose_f(attr_id, ierror)

!write grid dimensions (necessary to restart)
    hdims(1) = ICONFIG(2)
    GD(1:hdims(1)) = ICONFIG(3:3+hdims(1)-1)
    Call h5screate_simple_f(1, hdims, aspace_id, ierror)
    Call h5acreate_f(group_id, "gridSize", H5T_NATIVE_INTEGER, aspace_id, &
         attr_id, ierror)
    Call h5awrite_f(attr_id, H5T_NATIVE_INTEGER, GD, hdims, ierror)
    Call h5sclose_f(aspace_id, ierror)
    Call h5aclose_f(attr_id, ierror)

!write number of AuxVars (restart)
    hdims(1) = 1
    Call h5screate_simple_f(1, hdims, aspace_id, ierror)
    Call h5acreate_f(group_id, "numberOfAuxVars", H5T_NATIVE_INTEGER, aspace_id, &
         attr_id, ierror)
    Call h5awrite_f(attr_id, H5T_NATIVE_INTEGER, nAuxVars, hdims, ierror)
    Call h5sclose_f(aspace_id, ierror)
    Call h5aclose_f(attr_id, ierror)

    Call h5gclose_f(group_id, ierror)

    If(myrank == 0) then
       GridType='UnIform'
       Write (hdf5_io%m_xmf,'(a,i0,1x,10a)') '   <Grid Name="mesh',ng,'" GridType="',trim(GridType),'">'
       Write (hdf5_io%m_xmf,'(a,1pe13.5,10a)') '       <Time Value="',CONFIG(4),'" />'
    endIf
20  Format (10a)

  end Subroutine createGroupWriteHeader

  Subroutine CreateAndFillHDF5(region,varNames)

    USE ModGlobal
    USE ModDataStruct
    USE ModMPI

    IMPLICIT NONE

    
    TYPE(t_region),  POINTER               :: region
    CHARACTER(LEN=3),POINTER,DIMENSION(:)  :: varNames
    
    TYPE(t_grid),    POINTER :: grid
    TYPE(t_mixt),    POINTER :: state
    TYPE(t_hdf5_io), POINTER :: h5io

    INTEGER           :: i,j,k,l,m,n, iVar, numVar,numVarsToWrite,iDim
    INTEGER           :: nGrids, gridIndex, iGrid, varCount, numSolnVars
    INTEGER           :: mpiRequest(2)
    INTEGER           :: ng, nAuxVars, iError, useIB, numPointsInMesh
    INTEGER           :: flagCount, solnCount
    INTEGER           :: iHaveGrid = FALSE
    INTEGER           :: localGridIndex
    INTEGER, TARGET   :: intConfig(5)

    CHARACTER(LEN=8)  :: groupname = "Grid"
    CHARACTER(LEN=10) :: gridType, myName
    CHARACTER(LEN=3)  :: varName
    CHARACTER(LEN=80) :: gFname

    REAL(KIND=8)      :: doubleConfig(4)

    INTEGER(HID_T)    :: fileID, headerID, gridGroupID,rootGroupID,dataSpaceID
    INTEGER(HID_T)    :: attributeID, attributeSpaceID, datasetPropertyListID
    INTEGER(HID_T)    :: dataSetID

    INTEGER(HSIZE_T)  :: hdims(1),gridDimensions(3)

    CHARACTER(LEN=1), DIMENSION(3)            :: xyznames = (/ 'X', 'Y', 'Z' /)
    REAL(RFREAL),     DIMENSION(:,:), POINTER :: solnPtr
    INTEGER,          DIMENSION(:),   POINTER :: intConfigPtr

    LOGICAL :: writeMesh = .FALSE.

    intConfigPtr => intConfig
    h5io => region%hdf5_io
    ALLOCATE(h5io%dataItemNameList(24))
    ALLOCATE(h5io%solutionBuffers(32,region%nGrids))
    ALLOCATE(h5io%flagBuffers(5,region%nGrids))
    h5io%dataItemNameList = '          '
    writeMesh = ANY(varNames == 'xyz')

    IF(.NOT. writeMesh) THEN
       WRITE(gFname(1:18),'(A10,I8.8)') 'RocFlo-CM.', 0
       WRITE(gFname(19:21),'(A3)') '.h5'
    ENDIF

    ! Initialize FORTRAN predefined datatypes
    CALL InitHDF5
    
    IF(region%myrank == 0) THEN
       
       ! Create the HDF5 file
       !       CALL H5Pcreate_f(H5P_FILE_ACCESS_F, accessID, ierror)
       !       CALL H5Pset_fapl_mpio_f(access_id, mycomm, hdf5_io%m_info, ierror)
       CALL H5Fcreate_f(TRIM(h5io%m_fname), H5F_ACC_TRUNC_F,fileID, iError, H5P_DEFAULT_F)
       CALL H5Gopen_f(fileID, '/.', rootGroupID, iError)
       
       hdims(1) = 1
       CALL H5Screate_simple_f(1,hdims,attributeSpaceID,ierror)
       CALL H5Acreate_f(rootGroupID,"HEADER",H5T_NATIVE_DOUBLE,attributeSpaceID,attributeID,iError)
       CALL H5Awrite_f(attributeID,H5T_NATIVE_DOUBLE,REAL(region%global%main_ts_loop_index,RFREAL),hdims,iError)
       CALL H5Sclose_f(attributeSpaceID, iError)
       CALL H5Aclose_f(attributeID,      iError)
       
       ! print ngrids (necessary for restart)
       hdims(1) = 1
       CALL H5Screate_simple_f(1, hdims, attributeSpaceID, iError)
       CALL H5Acreate_f(rootGroupID, "numberOfGrids", H5T_NATIVE_INTEGER, attributeSpaceID, &
            attributeID, iError)
       CALL H5Awrite_f(attributeID,H5T_NATIVE_INTEGER, region%nGridsGlobal, hdims, iError)
       CALL H5Sclose_f(attributeSpaceID, iError)
       CALL H5Aclose_f(attributeID, iError)
       
       ! Create the XML file 
       CALL OpenCloseXML(region%myrank,h5io,1)
    ENDIF
    DO gridIndex = 1, region%nGridsGlobal
       WRITE (groupName,'("Group",I3.3)') gridIndex
       iHaveGrid = FALSE
       DO iGrid = 1, region%nGrids
          grid  => region%grid(iGrid)
          state => region%state(iGrid)
          IF((grid%iGridGlobal == gridIndex)) THEN
             iHaveGrid = TRUE
             localGridIndex = iGrid
             doubleConfig(1) = region%input%SC
             doubleConfig(2) = region%input%PR
             doubleConfig(3) = region%input%RE
             doubleConfig(4) = state%time(1)
             useIB = MERGE(1,0,ASSOCIATED(grid%IBLANK))
             intConfig(1) = useIB
             intConfig(2) = grid%ND
             intConfig(3:2+grid%ND) = grid%GlobalSize(1:grid%ND)
          ENDIF
       END DO
       mpiRequest = MPI_REQUEST_NULL
       if (region%myrank == 0) then
         if (iHaveGrid == FALSE) then
           call MPI_Irecv(doubleConfig, 4, MPI_DOUBLE_PRECISION, region%grid_to_processor(gridIndex), &
            0, mycomm, mpiRequest(1), iError)
           call MPI_Irecv(intConfig, 5, MPI_INTEGER, region%grid_to_processor(gridIndex), &
            1, mycomm, mpiRequest(2), iError)
         end if
       else
         if (iHaveGrid == TRUE .and. region%grid_to_processor(gridIndex) == region%myrank) then
           DO iGrid = 1, region%nGrids
             grid  => region%grid(iGrid)
             IF(grid%iGridGlobal == gridIndex) THEN
                CALL MPI_ISend(doubleConfig,4,MPI_DOUBLE_PRECISION,0,0,mycomm,mpiRequest(1),iError)
                CALL MPI_ISend(intConfig,grid%ND+2,MPI_INTEGER,0,1,mycomm,mpiRequest(2),iError)
             ENDIF
           END DO
         end if
       end if
       CALL MPI_Waitall(2,mpiRequest,MPI_STATUSES_IGNORE,iError)
       IF(region%myrank == 0) THEN
          ! Add an existing grid to XML without adding it to the h5 file
          gridType='Uniform'
          WRITE (h5io%m_xmf,'(a,i0,1x,10a)') '   <Grid Name="mesh',gridIndex,'" GridType="',TRIM(gridType),'">'
          WRITE (h5io%m_xmf,'(a,1pe13.5,10a)') '       <Time Value="',doubleConfig(4),'" />'
          IF(.NOT. writeMesh) THEN
             CALL addXMLdataset(region%myrank,h5io, intConfigPtr, 'xyz', gridIndex, intConfig(2), gFname)
          ENDIF
       ENDIF
       IF((iHaveGrid == TRUE) .OR. region%myrank == 0) THEN
          gridDimensions(1:intConfig(2)) = intConfig(3:2+intConfig(2))
          numPointsInMesh = PRODUCT(gridDimensions(1:intConfig(2)))
          
          IF(region%myrank == 0) THEN
             CALL H5Gcreate_f(fileID, TRIM(groupName), gridGroupID, iError)

             hdims(1) = 4
             CALL H5Screate_simple_f(1,hdims,attributeSpaceID,iError)
             CALL H5Acreate_f(gridGroupID,"HEADER",H5T_NATIVE_DOUBLE,attributeSpaceID, &
                  attributeID,iError)
             CALL H5Awrite_f(attributeID,H5T_NATIVE_DOUBLE, doubleConfig, hdims, iError)
             CALL H5Sclose_f(attributeSpaceID, iError)
             CALL H5Aclose_f(attributeID, iError)
             
             !write IBflag (necessary to restart)
             hdims(1) = 1
             useIB = intConfig(1)
             CALL H5Screate_simple_f(1, hdims, attributeSpaceID, iError)
             CALL H5Acreate_f(gridGroupID, "useIB", H5T_NATIVE_INTEGER, attributeSpaceID, &
                  attributeID, iError)
             CALL H5Awrite_f(attributeID, H5T_NATIVE_INTEGER, useIB, hdims, iError)
             CALL H5Sclose_f(attributeSpaceID, iError)
             CALL H5Aclose_f(attributeID, iError)
             
             !write grid dimensions (necessary to restart)
             hdims(1) = intConfig(2)
             CALL H5Screate_simple_f(1, hdims, attributeSpaceID, iError)
             CALL H5Acreate_f(gridGroupID, "gridSize", H5T_NATIVE_INTEGER, attributeSpaceID, &
                  attributeID, iError)
             CALL H5Awrite_f(attributeID, H5T_NATIVE_INTEGER, intConfig(3), hdims, iError)
             CALL H5Sclose_f(attributeSpaceID, iError)
             CALL H5Aclose_f(attributeID, iError)
             
             !write number of AuxVars (restart)
             hdims(1) = 1
             CALL H5Screate_simple_f(1, hdims, attributeSpaceID, iError)
             CALL H5Acreate_f(gridGroupID, "numberOfAuxVars", H5T_NATIVE_INTEGER, attributeSpaceID, &
                  attributeID, iError)
             CALL H5Awrite_f(attributeID, H5T_NATIVE_INTEGER, region%input%nAuxVars, hdims, iError)
             CALL H5Sclose_f(attributeSpaceID, iError)
             CALL H5Aclose_f(attributeID, iError)
             
             CALL H5Pcreate_f(H5P_DATASET_CREATE_F, datasetPropertyListID, iError)
             CALL H5Pset_alloc_time_f(datasetPropertyListID,H5D_ALLOC_TIME_EARLY_F,iError)
             
             CALL H5Screate_simple_f(grid%ND,gridDimensions,dataSpaceID,iError)
          ENDIF
          numVarsToWrite = SIZE(varNames)
          varCount  = 0
          flagCount = 0
          solnCount = 0
          DO iVar = 1, numVarsToWrite
             varName = varNames(iVar)
             IF(varName == 'xyz') THEN
                DO N=1,intConfig(2)
                   varCount = varCount+1
                   solnCount = solnCount + 1
                   h5io%dataItemNameList(varCount) = TRIM(xyznames(N)) ! //C_NULL_CHAR
                   IF(iHaveGrid == TRUE) &
                        h5io%solutionBuffers(solnCount,localGridIndex)%bufPtr => region%grid(localGridIndex)%xyz(:,N)
                   IF(region%myrank == 0) THEN
                      Call H5Dcreate_f(gridGroupID,TRIM(xyznames(N)),H5T_NATIVE_DOUBLE, dataSpaceID, dataSetID, iError,dataSetPropertyListID)
                      Call H5Dclose_f(dataSetID, iError)
                   END IF
                END DO
                IF(useIB == TRUE) THEN
                   varCount = varCount+1
                   flagCount = flagCount + 1
                   h5io%dataItemNameList(varCount) = 'IBLANK' !//C_NULL_CHAR
                   IF(iHaveGrid == TRUE) &
                        h5io%flagBuffers(flagCount,localGridIndex)%bufPtr => region%grid(localGridIndex)%IBLANK
                   IF(region%myrank == 0) THEN
                      CALL H5Dcreate_f(gridGroupID,'IBLANK',H5T_NATIVE_INTEGER,dataSpaceID,dataSetID,iError,dataSetPropertyListID)
                      CALL H5Dclose_f(dataSetID, iError)
                   ENDIF
                END IF
                CALL addXMLdataset(region%myrank, h5io, intConfigPtr, varName, gridIndex, intConfig(2))
             ELSE IF (varName == 'jac') THEN
                varCount = varCount + 1
                solnCount = solnCount + 1
                IF(region%myrank == 0) THEN
                   CALL H5Dcreate_f(gridGroupID,TRIM(varName),H5T_NATIVE_DOUBLE,dataSpaceID,dataSetID,iError,dataSetPropertyListID)
                   CALL H5Dclose_f(dataSetID, iError)
                ENDIF
                IF(iHaveGrid == TRUE) THEN
                   h5io%solutionBuffers(solnCount,localGridIndex)%bufPtr => region%grid(localGridIndex)%JAC
                ENDIF
                CALL addXMLdataset(region%myrank, h5io, intConfigPtr, varName, gridIndex, 1)               
             ELSE 
                IF((varName == 'cv ')   .OR. (varName == 'cvt') .OR. &
                     (varName == 'av ') .OR. (varName == 'avt') .OR. &
                     (varName == 'rhs')) THEN
                   numVar = 2 + intConfig(2)
                   IF(iHaveGrid == TRUE) THEN
                      IF(varName == 'cv ') THEN
                         solnPtr => region%state(localGridIndex)%cv
                      ELSE IF(varName == 'cvt') THEN
                         solnPtr => region%state(localGridIndex)%cvTarget
                      ELSE IF(varName == 'av ') THEN
                         solnPtr => region%state(localGridIndex)%av
                      ELSE IF(varName == 'avt') THEN
                         solnPtr => region%state(localGridIndex)%avTarget
                      ELSE IF(varName == 'rhs') THEN
                         solnPtr => region%state(localGridIndex)%rhs
                      ENDIF
                   ENDIF
                ELSE IF(varName == 'met') THEN
                   numVar = intConfig(2)*intConfig(2)
                   IF(iHaveGrid == TRUE) solnPtr => region%grid(localGridIndex)%MT1
                ELSE IF(varName == 'aux' .OR. varName == 'aut') THEN
                   numVar = region%input%nAuxVars
                   IF(iHaveGrid == TRUE) THEN
                      IF(varName == 'aux') THEN
                         solnPtr => region%state(localGridIndex)%auxVars
                      ELSE
                         solnPtr => region%state(localGridIndex)%auxVarsTarget
                      ENDIF
                   ENDIF
                ENDIF
                DO N = 1,numVar
                   WRITE (myName,'(a,I2.2)') TRIM(varName),N
                   varCount = varCount + 1
                   solnCount = solnCount + 1
                   h5io%dataItemNameList(varCount) = TRIM(myName) !//C_NULL_CHAR
                   IF(iHaveGrid == TRUE) THEN
                      h5io%solutionBuffers(solnCount,localGridIndex)%bufPtr => solnPtr(:,N)
                   ENDIF
                   IF(region%myrank == 0) THEN
                      CALL H5Dcreate_f(gridGroupID,TRIM(myName),H5T_NATIVE_DOUBLE,dataSpaceID,dataSetID,iError,dataSetPropertyListID)
                      CALL H5Dclose_f(dataSetID, iError)
                   ENDIF
                END DO
                CALL addXMLdataset(region%myrank, h5io, intConfigPtr, varName, gridIndex, numVar)               
             END IF
          END DO
          IF(region%myrank == 0) THEN
             CALL H5Pclose_f(datasetPropertyListID, iError)
             CALL H5Sclose_f(dataSpaceID, iError)
             CALL H5Gclose_f(gridGroupID, iError)
             WRITE (h5io%m_xmf,'(10a)') '   </Grid>'
          ENDIF
          h5io%numDataItems = varCount
          h5io%numFlagItems = flagCount
       END IF
    END DO

    IF(region%myrank == 0) THEN
       CALL H5Gclose_f(rootGroupID, iError)
       CALL H5Fclose_f(fileID,iError)
       CALL OpenCloseXML(region%myrank,h5io,2)
    ENDIF
    
  END SUBROUTINE CreateAndFillHDF5

  SUBROUTINE WriteHDF5Data_MPIIO(region)

    USE ModGlobal
    USE ModDataStruct
    USE ModMPI

    IMPLICIT NONE

    
    TYPE(t_region),  POINTER              :: region
    
    TYPE(t_grid),    POINTER :: grid
    TYPE(t_hdf5_io), POINTER :: h5io

    INTEGER           :: i,j,k,l,m,n, iVar, numVar,numVarsToWrite,numDims
    INTEGER           :: nGrids, gridIndex, iGrid, varCount, numSolnVars
    INTEGER           :: ng, nAuxVars, iError, useIB, numPointsInPartition
    CHARACTER(LEN=32) :: fileName

    CHARACTER(LEN=8)   :: groupname = "Grid"
    CHARACTER(LEN=512) :: nameList

    CHARACTER(LEN=10),DIMENSION(:),  POINTER       :: dataItemNameListPtr
    REAL(RFREAL),     DIMENSION(:,:),ALLOCATABLE   :: solutionBuffer
    INTEGER,          DIMENSION(:,:),ALLOCATABLE   :: flagBuffer

    INTEGER :: partitionStarts(3),partitionSizes(3), listPosition

    h5io => region%hdf5_io
    dataItemNameListPtr => h5io%dataItemNameList
    ! Initialize FORTRAN predefined datatypes
!    CALL InitHDF5
    nameList(:) = ' '
    listPosition = 1
    DO iVar = 1,h5io%numDataItems
       WRITE(nameList(listPosition:listPosition+LEN_TRIM(dataItemNameListPtr(iVar))),'(A,1x)') &
            TRIM(dataItemNameListPtr(iVar))
       listPosition = listPosition + LEN_TRIM(dataItemNameListPtr(iVar)) + 1
    END DO

    DO gridIndex = 1, region%nGridsGlobal
       WRITE (groupName,'("Group",I3.3)') gridIndex
       fileName(1:LEN_TRIM(h5io%fileName)) = TRIM(h5io%fileName)
       DO iGrid = 1, region%nGrids
          grid  => region%grid(iGrid)
          IF((grid%iGridGlobal == gridIndex)) THEN
             !             iHaveGrid = TRUE
             numDims   = grid%ND
!             localGridIndex = iGrid
             DO N=1,numDims
                partitionSizes(N) = grid%ie_unique(N) - grid%is_unique(N) + 1
                partitionStarts(N) = grid%is_unique(N) - 1
             END Do
             numPointsInPartition = PRODUCT(partitionSizes(1:numDims))
             numSolnVars = h5io%numDataItems - h5io%numFlagItems
             ALLOCATE(solutionBuffer(numPointsInPartition,numSolnVars))
             ALLOCATE(flagBuffer(numPointsInPartition,h5io%numFlagItems))
             solutionBuffer = grid%myrank_incomm
             CALL PackWriteBuffers(region,iGrid,solutionBuffer,flagBuffer)
             CALL hdf5writesolutiondata(TRIM(h5io%m_fname),TRIM(groupName),TRIM(nameList),&
                  grid%comm,flagBuffer,solutionBuffer,numDims,partitionStarts,&
                  partitionSizes,grid%GlobalSize,h5io%numDataItems,iError)
             IF(iError > 0) THEN
                Call graceful_exit(region%myrank, 'PlasComCM: ERROR: Failed to write HDF5 data with MPIIO.')
             ENDIF
             DEALLOCATE(solutionBuffer)
             DEALLOCATE(flagBuffer)
          ENDIF
       END DO   
    END DO
    
    DEALLOCATE(h5io%dataItemNameList)
    DEALLOCATE(h5io%solutionBuffers)
    DEALLOCATE(h5io%flagBuffers)

  END SUBROUTINE WriteHDF5Data_MPIIO

  SUBROUTINE PackWriteBuffers(region,iGrid,solutionBuffer,flagBuffer)

    USE ModGlobal
    USE ModDataStruct
    USE ModMPI
    USE ModIOUtil

    Implicit None

    
    TYPE(t_region),  POINTER              :: region
    INTEGER                               :: iGrid
    REAL(RFREAL), DIMENSION(:,:), TARGET  :: solutionBuffer
    INTEGER,      DIMENSION(:,:), TARGET  :: flagBuffer

    TYPE(t_grid),    POINTER :: grid
    TYPE(t_hdf5_io), POINTER :: h5io
    TYPE(t_mixt),    POINTER :: state

    REAL(RFREAL), DIMENSION(:), POINTER :: rBufPtr
    INTEGER,      DIMENSION(:), POINTER :: iBufPtr
    INTEGER                             :: numSolnItems, numFlagItems
    INTEGER                             :: iDataItem
    INTEGER                             :: isFlagData

    grid  => region%grid(iGrid)
    h5io  => region%hdf5_io
    state => region%state(iGrid)
    
    numSolnItems = h5io%numDataItems - h5io%numFlagItems
    numFlagItems = h5io%numFlagItems
    DO iDataItem = 1, numSolnItems
       rBufPtr => solutionBuffer(:,iDataItem)
       CALL rBoxToBuffer(grid,h5io%solutionBuffers(iDataItem,iGrid)%bufPtr,rBufPtr)
    END DO
    DO iDataItem = 1, numFlagItems
       iBufPtr => flagBuffer(:,iDataItem)
       CALL iBoxToBuffer(grid,h5io%flagBuffers(iDataItem,iGrid)%bufPtr,iBufPtr)
    END DO
  END SUBROUTINE PackWriteBuffers

  SUBROUTINE addDataSet(ICONFIG, grid, nglobal, var_to_write, hdf5_io, myrank, dbuf)

    USE ModGlobal
    USE ModDataStruct
    USE ModMPI
    USE ModIOUtil

    Implicit None

    INTEGER, Dimension(:), Pointer :: ICONFIG
    Type(t_grid), Pointer :: grid
    Integer, Intent(In) :: nglobal, myrank
    Real(RFREAL), Dimension(:,:), Optional, Pointer :: dbuf
    character(len=3) :: var_to_write
    Type(t_hdf5_io), Pointer :: hdf5_io

    Integer          :: ierror, NDIM, NVAR
    CHARACTER(LEN=8) :: groupname = "Grid",myName
    INTEGER(HID_T) :: group_id ,aspace_id, attr_id, dataspace_id, dataset_id, plist_id, memspace    ! Property list identIfier 
    Integer(hid_t)   :: dcpl_id ,dtype_id, itype_id, ctype_id
    Integer(hsize_t) :: N, hdims(1), Ddims(3)
!Hyperslab parameters
    Integer(hsize_t), DIMENSION(3) :: Dcount, Doffset, Dstride, Dblock, Dmem
    Character(len=1), Dimension(3) :: xyznames = (/ 'X', 'Y', 'Z' /)
    logical :: use_IB, hasGrid

    Real(rfreal), pointer :: dwbuf(:), ptr_to_box(:), iblank_dble(:)
    Integer, pointer :: iwbuf(:)

    write (groupname,'("Group",I3.3)') nglobal
    Call h5gopen_f(hdf5_io%m_file_id, trim(groupname), group_id, ierror)

! Assign the data file types
    itype_id=H5T_NATIVE_INTEGER
    ctype_id=H5T_NATIVE_CHARACTER
    If (m_prec .eq. 'd') Then
       dtype_id=H5T_NATIVE_DOUBLE
    Else
       dtype_id=H5T_NATIVE_REAL
    End If


! Number of dimensions
    use_IB = ICONFIG(1)==1
    NDIM = ICONFIG(2)
    hasGrid = grid%iGridGlobal == nglobal;  !ICONFIG(3+NDIM) == 1
! Grid dimensions
    Ddims=0;Dcount=0;Doffset=0;
    If(hasGrid) then
       Do N=1,NDIM
          Ddims(N) = grid%GlobalSize(N)
          Dcount(N) = grid%ie_unique(N) - grid%is_unique(N) + 1
          Doffset(N) = grid%is_unique(N) - 1
!Ddims(N) = ND(1,NDIM-N+1)
       End Do
    Else
       Do N=1,NDIM
          Ddims(N) = ICONFIG(3+N-1)
       End Do
    end If
    Dstride = 1
    Dblock = 1

    Call h5pcreate_f(H5P_DATASET_CREATE_F, dcpl_id, ierror)
    If(m_useChunking) then
       !!Call h5pset_chunk_f(dcpl_id, ndim, Dcount, ierror)
    end If

!switch count and block to measure performance
    !Dstride = Dblock
    !Dblock = Dcount
    !Dcount = Dstride
    !Dstride = 1

    Dmem = Dcount*Dblock
    Call h5screate_simple_f(NDIM, Dmem, memspace, ierror)
    If(.not. hasGrid) Call h5sselect_none_f(memspace, ierror)

    Call h5screate_simple_f(ndim, Ddims, dataspace_id, ierror)
    Call h5sselect_hyperslab_f (dataspace_id, H5S_SELECT_SET_F, Doffset, Dcount, ierror, Dstride, Dblock)
    If(.not. hasGrid) Call h5sselect_none_f(dataspace_id, ierror)

    Call h5pcreate_f(H5P_DATASET_XFER_F, plist_id, ierror) 
    If(m_collective) Call h5pset_dxpl_mpio_f(plist_id, H5FD_MPIO_COLLECTIVE_F, ierror)     

    If (present(dbuf) .eqv. .false.) then
       NVAR = NDIM
!Write coordinates and return
       allocate(dwbuf(grid%nCells_unique))
       Do N=1,NDIM
          ptr_to_box => grid%XYZ(:,N)
          Call rBoxToBuffer(grid,ptr_to_box,dwbuf)
          Call h5dcreate_f(group_id, trim(xyznames(N)), dtype_id, dataspace_id, dataset_id, ierror, dcpl_id=dcpl_id)
          Call h5dwrite_f(dataset_id, dtype_id, dwbuf(1), Ddims, ierror, &
               file_space_id = dataspace_id, mem_space_id = memspace, xfer_prp = plist_id)
          Call h5dclose_f(dataset_id, ierror)
       End Do
       deallocate(dwbuf)

! IBLANK
       If (Use_IB) Then
          allocate(iwbuf(grid%nCells_unique))
          allocate(dwbuf(grid%nCells_unique))
          allocate(iblank_dble(size(grid%iblank,1)))
          iblank_dble(:) = dble(grid%IBLANK(:))
          ptr_to_box => iblank_dble
          Call rBoxToBuffer(grid,ptr_to_box,dwbuf)
          iwbuf(:) = int(dwbuf(:))
          Call h5dcreate_f(group_id, "IBLANK", itype_id, dataspace_id, dataset_id, ierror, dcpl_id)

          Call h5dwrite_f(dataset_id, itype_id, iwbuf(1), Ddims, ierror, &
               file_space_id = dataspace_id, mem_space_id = memspace, xfer_prp = plist_id)
          Call h5dclose_f(dataset_id, ierror)
          deallocate(iwbuf)
          deallocate(dwbuf)
          deallocate(iblank_dble)
       End If
       Call addXMLdataset(myrank, hdf5_io, ICONFIG, var_to_write, nglobal, nvar)
    Else

! Otherwise write the variables
       allocate(dwbuf(grid%nCells_unique))
       NVAR = size(dbuf,2)
       Do N = 1, NVAR
          ptr_to_box => dbuf(:,N)
          Call rBoxToBuffer(grid,ptr_to_box,dwbuf)
          write (myName,'(a,I2.2)') trim(var_to_write), N
          Call h5dcreate_f(group_id, trim(myName), dtype_id, dataspace_id, dataset_id, ierror, dcpl_id) 

          Call h5dwrite_f(dataset_id, dtype_id, dwbuf(1), Ddims, ierror, &
               file_space_id = dataspace_id, mem_space_id = memspace, xfer_prp = plist_id) 
          Call h5dclose_f(dataset_id, ierror)
       End Do
       deallocate(dwbuf)
       Call addXMLdataset(myrank, hdf5_io, ICONFIG, var_to_write, nglobal, nvar)
    endIf

    Call h5sclose_f(dataspace_id, ierror)
    Call h5pclose_f(plist_id, ierror)
    Call h5sclose_f(memspace, ierror)
    Call h5pclose_f(dcpl_id, ierror)
    Call h5gclose_f(group_id, ierror)

  end Subroutine addDataSet


  subroutine openCloseXML(myrank, hdf5_io, iOpenClose)
    USE ModGlobal
    USE ModDataStruct
    USE ModMPI
    Implicit none

    integer,INTENT(IN) :: myrank
    Type(t_hdf5_io), Pointer :: hdf5_io
    integer,INTENT(IN) :: iOpenClose
    Character(LEN=80) ::  xmlFileName

    If(myrank/=0) RETURN

    If(iOpenClose == 1) then
! Add .xmf extension to file name
       xmlFileName = trim(hdf5_io%m_fname(1:(hdf5_io%m_fname_len-3))) // '.xmf'

! Open the file and write the XML description of the mesh
       Open (hdf5_io%m_xmf, file=trim(xmlFileName))
       Write (hdf5_io%m_xmf,20) '<?xml version="1.0" ?>'
       Write (hdf5_io%m_xmf,20) '<!DOCTYPE Xdmf SYSTEM "Xdmf.dtd" []>'
       Write (hdf5_io%m_xmf,20) '<Xdmf Version="2.0">'
       Write (hdf5_io%m_xmf,20) ' <Domain>'
    Else
       Write (hdf5_io%m_xmf,20) ' </Domain>'
       Write (hdf5_io%m_xmf,20) '</Xdmf>'
       Close(hdf5_io%m_xmf)
    endIf
20  Format (10a)
33  Format (a,i0,1x,i0,a)

  end subroutine openCloseXML


  subroutine addXMLdataset(myrank, hdf5_io, ICONFIG, var_to_write, nglobal, nvar, gridFile)
    USE ModGlobal
    USE ModDataStruct
    USE ModMPI
    Implicit none

    INTEGER :: myrank
    Type(t_hdf5_io), Pointer :: hdf5_io
    INTEGER, Dimension(:), Pointer :: ICONFIG
    character(len=3) :: var_to_write
    Integer, Intent(In) :: nglobal,nvar
    Character(LEN=80), Intent(In),optional :: gridFile
    Integer :: N, idot
    Integer(hsize_t) :: NDIM,Ddims(3)
    Character(LEN=80) :: fileG
    Character(LEN=80) ::  xmlFileName, groupName
    Character(LEN=10) :: TopologyType, GeometryType
    Character(LEN=10) :: myName
    Character(LEN=20) :: pformat
    Character(len=1), Dimension(3) :: xyznames = (/ 'X', 'Y', 'Z' /)
    Integer :: use_IB, fileG_len

    If(myrank/=0) RETURN

    write (groupname,'("Group",I3.3)') nglobal
! Number of dimensions
    NDIM = ICONFIG(2)
!flip Ddims for visualization purpose
    Ddims = 1
    Do N=1,NDIM
       Ddims(N) = ICONFIG(3+NDIM-N)
    End Do

    write(pformat,'("(a,",i1,"(i0,1x),i0,a)")') NDIM-1

    If ( var_to_write(1:3) == 'xyz' ) Then

!it is possible that the mesh is not contained in the RocfloCm*.h5 file
!in this case provide a separate name for the mesh file
       If(present(gridFile)) then
          fileG = gridFile
          fileG_len = 21
       Else
          fileG = hdf5_io%m_fname(1:hdf5_io%m_fname_len)
          fileG_len = hdf5_io%m_fname_len
       endIf

       use_IB = ICONFIG(1)
! Grid type
       If (NDIM == 2) TopologyType='2DSMesh'
       If (NDIM == 3) TopologyType='3DSMesh'
       If (NDIM == 2) GeometryType='X_Y'
       If (NDIM == 3) GeometryType='X_Y_Z'

       Write (hdf5_io%m_xmf,'(3a,2(i0,1x),i0,a)') '     <Topology TopologyType="',trim(TopologyType),'" NumberOfElements="',Ddims,'"/>'
       Write (hdf5_io%m_xmf,20) '     <Geometry GeometryType="',trim(GeometryType),'">'
! Write the coordinates
       Do N = 1, NDIM
          Write (hdf5_io%m_xmf,trim(pformat)) '       <DataItem Dimensions="',Ddims(1:NDIM),'" NumberType="Float" Precision="8" Format="HDF">'
          Write (hdf5_io%m_xmf,20) '        ',fileG(1:fileG_len),':/',trim(groupname),'/',trim(xyznames(N))
          Write (hdf5_io%m_xmf,20) '       </DataItem>'
       End Do
       Write (hdf5_io%m_xmf,20) '     </Geometry>'
! IBLANK
       If (use_IB==1) Then
          Write (hdf5_io%m_xmf,20) '     <Attribute Name="IBLANK" AttributeType="Scalar" Center="Node">'
          Write (hdf5_io%m_xmf,trim(pformat)) '       <DataItem Dimensions="',Ddims(1:NDIM),'" NumberType="Int" Precision="4" Format="HDF">'
          Write (hdf5_io%m_xmf,20) '        ',fileG(1:fileG_len),':/',trim(groupname),'/','IBLANK'
          Write (hdf5_io%m_xmf,20) '       </DataItem>'
          Write (hdf5_io%m_xmf,20) '     </Attribute>'
       End If
       return
    endIf


! Write the data
    fileG=hdf5_io%m_fname(1:hdf5_io%m_fname_len)
    fileG_len = hdf5_io%m_fname_len
    Do N = 1, NVAR
       write (myName,'(a,I2.2)') trim(var_to_write), N
       Write (hdf5_io%m_xmf,20) '     <Attribute Name="',trim(myName),'" AttributeType="Scalar" Center="Node">'
       Write (hdf5_io%m_xmf,trim(pformat)) '       <DataItem Dimensions="',Ddims(1:NDIM),'" NumberType="Float" Precision="8" Format="HDF">'
       Write (hdf5_io%m_xmf,20) '        ',fileG(1:fileG_len),':/',trim(groupname),'/',trim(myName)
       Write (hdf5_io%m_xmf,20) '       </DataItem>'
       Write (hdf5_io%m_xmf,20) '     </Attribute>'
    End Do

20  Format (10a)
33  Format (a,i0,1x,i0,a)

  end subroutine addXMLdataset


  SUBROUTINE HDF5InFile(fileName,fileID,rootGroupID,inCommunicator)

    IMPLICIT NONE

    CHARACTER(LEN=*)  :: fileName
    INTEGER(hid_t)    ::  fileID, rootGroupID
    INTEGER, OPTIONAL :: inCommunicator

    INTEGER(hid_t) :: access_id
    INTEGER        :: ierror, m_info, mpiCommunicator
    
    m_info = MPI_INFO_NULL
    fileID = -1
    rootGroupID = -1
    IF(PRESENT(inCommunicator) .EQV. .TRUE.) THEN
       mpiCommunicator = inCommunicator
    ELSE
       mpiCommunicator = myComm
    ENDIF

    CALL H5PCreate_f(H5P_FILE_ACCESS_F, access_id, ierror)
    IF(ierror .ne. 0) STOP 'HDF5InFile::Error creating HDF5 access structure.'
    CALL H5PSet_fapl_mpio_f(access_id, mpiCommunicator, m_info, ierror)
    IF(ierror .ne. 0) STOP 'HDF5InFile::Error setting HDF5 access structure.'
    CALL H5FOpen_f(TRIM(fileName), H5F_ACC_RDONLY_F, fileID, ierror, access_prp = access_id)
    IF(ierror .NE. 0) THEN 
       WRITE(*,*) 'HDF5InFile::Error opening HDF5 file [',TRIM(fileName),'].'
       STOP
    ENDIF
    CALL H5PClose_f(access_id, ierror)
    IF(ierror .NE. 0) STOP 'HDF5InFile::Error while destroying HDF5 access structure.'
    CALL H5GOpen_f(fileID, '/.', rootGroupID, ierror)
    IF(ierror .NE. 0) THEN
       WRITE(*,*) 'HDF5InFile::Error while opening root group for HDF5 file [',TRIM(fileName),'].'
       STOP 
    ENDIF
    
  END SUBROUTINE HDF5InFile

  SUBROUTINE ReadGridSize_HDF5(NDIM, ngrid, ND, file, OUTPUT_FLAG, ib)

    USE ModDataStruct

    IMPLICIT NONE

    TYPE(t_hdf5_io), POINTER :: hdf5_io
    INTEGER :: ngrid, NDIM 
    INTEGER, POINTER :: ND(:,:)
    CHARACTER(LEN=*) :: file
    INTEGER, INTENT(IN), OPTIONAL :: OUTPUT_FLAG
    CHARACTER(LEN=2), INTENT(OUT), OPTIONAL :: ib
    INTEGER :: I, J, K, L, Nx, Ny, Nz, M, ftype, ng
    INTEGER :: OMap(3), NMap(3), ND_copy(3)
    REAL(KIND=4), DIMENSION(:,:,:,:), POINTER :: fX
    LOGICAL :: gf_exists
    INTEGER :: ierror, Use_IB, iball
    INTEGER(hid_t)   :: itype_id, m_file_id
    INTEGER(hid_t)   :: group_id, attr_id, aspace_id,rootGroupID  !header
    INTEGER(hid_t)   :: filespace_id, memspace_id,plist_id,access_id !hyperslab
    CHARACTER(LEN=8) :: groupname = "Grid"
    INTEGER(hsize_t) :: NN, hdims(1),hdim(1),hmaxdims(1)
    INTEGER :: m_info = MPI_INFO_NULL
    INTEGER, POINTER :: rbuf(:)

    CALL InitHDF5

    itype_id=H5T_NATIVE_INTEGER

    Inquire(file=Trim(file),exist=gf_exists)
    If (.NOT.(gf_exists.eqv..true.)) Then
       Write (*,'(A,A,A)') 'File ', file(1:LEN_TRIM(file)), ' is not readable'
       Stop
    End If

    hdims=1
    CALL HDF5InFile(file,m_file_id,rootGroupID)

    Call h5aopen_f(rootGroupID, "numberOfGrids", attr_id, ierror)
    Call h5aread_f(attr_id, itype_id, ngrid, hdims, ierror)
    Call h5aclose_f(attr_id, ierror)

    ! Allocate ND
    Allocate(ND(ngrid,3))
    ALLOCATE(rbuf(3))

    If(present(OUTPUT_FLAG)) Then
       If (OUTPUT_FLAG > 0) Then
          Write (*,'(A)') ' '
          Write (*,'(A,I2)') 'PlasComCM: Number of grids: ', ngrid
       End If
    end If
    
!  Read grid sizes
    ND(:,3) = 1
    hdim=1;
    iball=0
    rbuf = 1
    Do ng = 1, ngrid
       write (groupname,'("Group",I3.3)') ng
       Call h5gopen_f(m_file_id, trim(groupname), group_id, ierror)
       Call h5aopen_f(group_id, "gridSize", attr_id, ierror)
! Read number of dimensions
       Call h5aget_space_f(attr_id, aspace_id, ierror)
       Call h5sget_simple_extent_dims_f(aspace_id, hdims, hmaxdims, ierror)
       NDIM = hdims(1)

! Read the grid sizes
       Call h5aread_f(attr_id, itype_id, rbuf, hdims, ierror)
       Call h5aclose_f(attr_id, ierror)
       ND(ng,:) = rbuf(:)
       
       Call h5aopen_f(group_id, "useIB", attr_id, ierror)
       Call h5aread_f(attr_id, itype_id, use_IB, hdim, ierror)
       Call h5aclose_f(attr_id, ierror)
       iball=iball+use_IB

       Call h5gclose_f(group_id, ierror)

    End Do
    If(present(ib)) then
       ib = merge('y','n',iball>=1)
    End If

    If (present(OUTPUT_FLAG)) Then
       If (OUTPUT_FLAG > 0) Then
          Do I = 1, ngrid
             Write (*,'(A,I2,A,3(I5,1X))') 'PlasComCM: Grid ', I, ' is ', (ND(I,J),J=1,NDIM)
          End Do
       End If
    End If

    Call h5gclose_f(rootGroupID, ierror)
    Call h5fclose_f(m_file_id, ierror)

    DEALLOCATE(rbuf)

  End Subroutine ReadGridSize_HDF5


  Subroutine ReadRestart_HDF5(region, fname, isTarget_IN)

    USE ModGlobal
    USE ModDataStruct
    USE ModMPI
    USE ModIOUtil

    Implicit None

    Type (t_region), Pointer :: region
    Character(LEN=80),intent(IN) :: fname
    integer, intent(IN), OPTIONAL :: isTarget_IN

    ! Local variables
    Type (t_mixt), Pointer :: state
    Type (t_grid), Pointer :: grid
    Real(rfreal), pointer :: drbuf(:), ptr_to_box(:), iblank_dble(:)
    Integer, pointer :: irbuf(:)
    
    real(rfreal) :: tau(4)
    character(len=2) :: prec, gf, vf
    Integer :: NDIM, nglobal, ng, N(MAX_ND)
    Integer :: gridID ! ... specIfy which grid we want to read, JKim 04/2008

    ! HDF5 variables
    Real(RFREAL), Dimension(:,:), Pointer :: dbuf
    logical :: gridHasMesh, hasGrid, useIB
    integer :: K, M, NVAR, ierr, ierror, isTarget
    Real(KIND=8), Dimension(:), Pointer :: CONFIG
    INTEGER, Dimension(:), Pointer :: ICONFIG
    ! Hyperslab parameters
    Integer(hsize_t), DIMENSION(3) :: Dcount, Doffset, Dstride, Dblock, Dmem
    CHARACTER(LEN=8) :: groupname = "Grid",myName
    Character(len=1), Dimension(3) :: xyznames = (/ 'X', 'Y', 'Z' /)
    ! Property list identIfiers
    INTEGER(HID_T) :: access_id , group_id ,aspace_id, attr_id, dataspace_id, dataset_id, plist_id, memspace,filespace 
    Integer(hid_t)   :: dtype_id, itype_id, ctype_id, rootGroupID
    Integer(hsize_t) :: NN, hdims(1), hmaxdims(1), Ddims(3),Ddims_(3)
    character(len=3) :: var_to_read
    INTEGER(KIND=8) :: isize

    If(present(isTarget_IN)) then
       isTarget=isTarget_IN
    Else
       isTarget = 0
    end If


    IF(region%input%usempiio == TRUE) THEN
       CALL ReadHDF5Restart_MPIIO(region,fname,isTarget)
    ELSE

       Call mpi_barrier(mycomm,ierr)
       
       
       CALL InitHDF5
       
       ! Assign the data file types
       itype_id=H5T_NATIVE_INTEGER
       ctype_id=H5T_NATIVE_CHARACTER
       dtype_id=H5T_NATIVE_DOUBLE
       
       CALL HDF5InFile(fname,region%hdf5_io%m_file_id,rootGroupID)
       
       
       Call h5aopen_f(rootGroupID, "HEADER", attr_id, ierror)
       ! Get the dataspace
       Call h5aget_space_f(attr_id, aspace_id, ierror)
       Call h5sget_simple_extent_dims_f(aspace_id, hdims, hmaxdims, ierror)
       Call h5sclose_f(aspace_id, ierror)
       ! Read the data
       Call h5aread_f(attr_id, dtype_id, tau, hdims, ierror)
       
       Call h5aclose_f(attr_id, ierror)
       
       Loop1: Do nglobal = 1, region%nGridsGlobal
          
          ! This grid's data
          nullIfy(state)
          hasGrid = .false.
          Loop2: Do ng = 1, region%nGrids
             grid  => region%grid(ng)
             state => region%state(ng)
             If (grid%iGridGlobal == nglobal) then
                hasGrid = .true.
                exit Loop2
             endIf
          enddo Loop2
          If(.not. hasGrid) then
             grid => region%grid(1)
             state => region%state(1)  !a placeholder
          End If
          
          ! Set group
          Write (groupname,'("Group",I3.3)') nglobal
          Call h5gopen_f(region%hdf5_io%m_file_id, trim(groupname), group_id, ierror)
          
          ! Read Grid HEADER
          Call h5aopen_f(group_id, "HEADER", attr_id, ierror)
          ! Get the dataspace
          Call h5aget_space_f(attr_id, aspace_id, ierror)
          Call h5sget_simple_extent_dims_f(aspace_id, hdims, hmaxdims, ierror)
          Call h5sclose_f(aspace_id, ierror) 
          
          ! Get the grid time
          Allocate(CONFIG(hdims(1)))
          Call h5aread_f(attr_id, dtype_id, CONFIG, hdims, ierror)
          tau(4) = CONFIG(4)
          Deallocate(CONFIG) 
          Call h5aclose_f(attr_id, ierror)
          
          ! Set the dimensions
          NDIM = grid%input%ND
          Ddims=0;Dcount=0;Doffset=0;
          If(hasGrid) Then
             Do M=1,NDIM
                Ddims(M) = grid%GlobalSize(M)
                Dcount(M) = grid%ie_unique(M) - grid%is_unique(M) + 1
                Doffset(M) = grid%is_unique(M) - 1
             End Do
          End If
          Dstride = 1
          Dblock = 1
          
          Ddims_=Ddims
          Call MPI_Allreduce(Ddims_, Ddims, size(Ddims_), MPI_INTEGER, MPI_MAX, mycomm, ierr)
          
          ! Switch count and block to measure performance
          !Dstride = Dblock
          !Dblock = Dcount
          !Dcount = Dstride
          !Dstride = 1
          Dmem = Dcount*Dblock
          
          ! Create the hyperslab (memspace with no halo)
          Call h5screate_simple_f(NDIM, Ddims, filespace, ierror)
          Call h5screate_simple_f(NDIM, Dmem, memspace, ierror)
          Call h5sselect_hyperslab_f (filespace, H5S_SELECT_SET_F, Doffset, Dcount, ierror, Dstride, Dblock)
          if(.not. hasGrid) then
             Call h5sselect_none_f(memspace, ierror)
             Call h5sselect_none_f(filespace, ierror)
          endif
          
          Call h5pcreate_f(H5P_DATASET_XFER_F, plist_id, ierror)
          if(m_collective) Call h5pset_dxpl_mpio_f(plist_id, H5FD_MPIO_COLLECTIVE_F, ierror)
          
          ! Check if this grid has a mesh
          Call h5lexists_f(group_id, xyznames(1), gridHasMesh, ierror)
          if(gridHasMesh .and. isTarget == 0) then
             allocate(drbuf(grid%nCells_unique))
             Do M = 1, NDIM
                Call h5dopen_f(group_id, xyznames(M), dataset_id, ierror)
                Call h5dread_f(dataset_id, dtype_id, drbuf(1), Ddims, ierror, &
                     file_space_id = filespace, mem_space_id = memspace, xfer_prp = plist_id)
                Call h5dclose_f(dataset_id, ierror)
                if (hasGrid) then
                   ptr_to_box => grid%XYZ(:,M)
                   Call rBufferToBox(grid,drbuf,ptr_to_box)
                end if
             End Do
             deallocate(drbuf)
             
             Call h5lexists_f(group_id, "IBLANK", useIB, ierror)
             If(useIB) then
                allocate(irbuf(grid%nCells_unique))
                allocate(drbuf(grid%nCells_unique))
                allocate(iblank_dble(size(grid%iblank,1)))
                iblank_dble = 0._rfreal
                Call h5dopen_f(group_id, "IBLANK", dataset_id, ierror)
                Call h5dread_f(dataset_id, itype_id, irbuf(1), Ddims, ierror, &
                     file_space_id = filespace, mem_space_id = memspace, xfer_prp = plist_id)
                Call h5dclose_f(dataset_id, ierror)
                if (hasGrid) then
                   drbuf(:) = dble(irbuf(:))
                   ptr_to_box => iblank_dble
                   Call rBufferToBox(grid,drbuf,ptr_to_box)
                   grid%IBLANK(:) = int(iblank_dble(:))
                end if
                deallocate(irbuf)
                deallocate(drbuf)
                deallocate(iblank_dble)
             Else
                grid%IBLANK(:) = 1
             End If
          End If
          
          ! Read the restart/target file
          If(isTarget == 0) Then
             dbuf=>state%cv
             var_to_read = 'cv '
          Else
             dbuf=>state%cvTarget
             var_to_read = 'cvt'
          End If
          
          allocate(drbuf(grid%nCells_unique))
          NVAR = size(dbuf,2)
          Do M = 1, NVAR
             Write (myName,'(a,I2.2)') trim(var_to_read), M
             Call h5dopen_f(group_id, trim(myName), dataset_id, ierror)
             Call h5dread_f(dataset_id, dtype_id, drbuf(1), Ddims, ierror, &
                  file_space_id = filespace, mem_space_id = memspace, xfer_prp = plist_id)
             Call h5dclose_f(dataset_id, ierror)
             if (hasGrid) then
                ptr_to_box => dbuf(:,M)
                Call rBufferToBox(grid,drbuf,ptr_to_box)
             end if
          End Do
          deallocate(drbuf)
          
          if (region%input%nAuxVars > 0) then
             If(isTarget == 0) then
                dbuf=>state%auxVars
                var_to_read = 'aux'
             Else
                dbuf=>state%auxVarsTarget
                var_to_read = 'aut'
             End If
             Call h5aopen_f(group_id, "numberOfAuxVars", attr_id, ierror)
             hdims=1
             Call h5aread_f(attr_id, itype_id, NVAR, hdims, ierror)
             Call h5aclose_f(attr_id, ierror)
             If(NVAR /= region%input%nAuxVars) Then
                Call graceful_exit(region%myrank, 'PlasComCM: ERROR: Number of auxVars is incorrect in restart/target file.')
             End If
             allocate(drbuf(grid%nCells_unique))
             Do M = 1, NVAR
                Write (myName,'(a,I2.2)') trim(var_to_read), M
                Call h5dopen_f(group_id, trim(myName), dataset_id, ierror)
                ! Read the data
                Call h5dread_f(dataset_id, dtype_id, drbuf(1), Ddims, ierror, &
                     file_space_id = filespace, mem_space_id = memspace, xfer_prp = plist_id)
                Call h5dclose_f(dataset_id, ierror)
                if (hasGrid) then
                   ptr_to_box => dbuf(:,M)
                   Call rBufferToBox(grid,drbuf,ptr_to_box)
                end if
             End Do
             deallocate(drbuf)
          End If
          Call h5gclose_f(group_id, ierror)
          
          If(isTarget==0 .and. hasGrid) Then
             ! Update the time
             Do k = 1, grid%nCells
                state%time(k) = tau(4)
             End Do
             
             ! Update the iteration number
             region%input%nstepi = nint(tau(1))
             
             ! Temporary fix
             grid%input%nstepi = region%input%nstepi
          End If
          
       End Do Loop1
       
       Call h5sclose_f(filespace, ierror)
       Call h5sclose_f(memspace, ierror)
       Call h5pclose_f(plist_id, ierror)
       Call h5gclose_f(rootGroupID, ierror)
       Call h5fclose_f(region%hdf5_io%m_file_id, ierror);
       
       Call MPI_BARRIER(MYCOMM,ierr)
       If ((debug_on .eqv. .true.) .and. (region%myrank == 0)) Write (*,'(A)') 'PlasComCM: Done reading "'//trim(fname)//'".'

    ENDIF
    
  End Subroutine ReadRestart_HDF5
  
  
  
  Subroutine ReadSingleGrid_HDF5(region, ng_in_file)

    USE ModGlobal
    USE ModDataStruct
    USE ModMPI
    USE ModIOUtil

    Implicit None

    Type (t_region), pointer :: region
    integer,intent(IN) :: ng_in_file

! ... local variables
    Integer :: ngrid, NDIM, M, ftype, ng, p, dir
    Integer :: ierror, ierr
    Integer :: ijkInd(MAX_ND)
    Type (t_mixt_input), pointer :: input
    Type (t_grid), pointer :: grid
    Type (t_mixt), Pointer :: state
    Character(LEN=80):: fname
    Real(rfreal), pointer :: drbuf(:), ptr_to_box(:), iblank_dble(:), ptr_to_data(:)
    REAL(RFREAL), POINTER :: gridBuf(:,:)
    Integer, pointer :: irbuf(:)

    logical :: gridHasMesh, hasGrid, useIB
    INTEGER :: hasIBLANK = FALSE
    INTEGER :: gridExists = FALSE

!Hyperslab parameters
    Integer(hsize_t), DIMENSION(3) :: Ddims, Ddims_, Dcount, Doffset, Dstride, Dblock, Dmem
    INTEGER, DIMENSION(3) :: gridSize, partitionSize, partitionStart
    CHARACTER(LEN=8) :: groupname = "Grid"
    Character(len=1), Dimension(3) :: xyznames = (/ 'X', 'Y', 'Z' /)
    INTEGER(HID_T) :: access_id , group_id ,aspace_id, attr_id, dataspace_id, dataset_id, plist_id, memspace,filespace    ! Property list identifier 
    Integer(hid_t)   :: dtype_id, itype_id, ctype_id
    INTEGER(HID_T) :: rootGroupID, gridGroupID

    ! Sponge stuff
    TYPE(t_patch), pointer :: patch
    Integer :: npatch, N(MAX_ND), Np(MAX_ND), Nc, sgn, normDir, i, j, k, l1, l2, lp, l0, ii, jj, kk
    Integer :: is(MAX_ND), ie(MAX_ND), offset, Ns(MAX_ND), byteSwap = FALSE, dataOffsets(4)
    INTEGER :: usempiio = FALSE
    INTEGER(KIND=8) :: isize

    ierror = 0
    input => region%input
    usempiio = input%usempiio
    nullify(input)
    write (groupname,'("Group",I3.3)') ng_in_file
    ! Assign the data file types
    CALL InitHDF5
    itype_id=H5T_NATIVE_INTEGER
    ctype_id=H5T_NATIVE_CHARACTER
    dtype_id=H5T_NATIVE_DOUBLE
    NULLIFY(state)

    IF(usempiio == TRUE) THEN ! subvert HDF5 and use the MPIIO interface to read the data

       DO ng = 1, region%nGrids
          grid  => region%grid(ng)          

          IF (grid%iGridGlobal == ng_in_file) THEN
             
             NDIM = grid%input%ND
             input => grid%input
             fname = TRIM(input%grid_fname)

             CALL ReadSingleHDF5Grid_MPIIO(fname,region,ng_in_file,gridExists,hasIBLANK)

          ENDIF

       END DO

    ELSE 
              
       ! ... this grid's data
       nullify(state)

       hasGrid = .false.
       Loop2: Do ng = 1, region%nGrids
          grid  => region%grid(ng)
          state => region%state(ng)
          if (grid%iGridGlobal == ng_in_file) then
             hasGrid = .true.
             exit Loop2
          endif
       enddo Loop2
       if(.not. hasGrid) then
          grid => region%grid(1)
          state => region%state(1)  !a placeholder
       endif
       input => grid%input
       fname = trim(input%grid_fname)
       
       CALL HDF5InFile(fname,region%hdf5_io%m_file_id,rootGroupID)
       
       !set group
       Call h5gopen_f(region%hdf5_io%m_file_id, trim(groupname), gridGroupID, ierror)
       
       !set the dimensions
       NDIM = grid%input%ND
       Ddims=0;Dcount=0;Doffset=0;
       if(hasGrid) then
          Do M=1,NDIM
             Ddims(M) = grid%GlobalSize(M)
             Dcount(M) = grid%ie_unique(M) - grid%is_unique(M) + 1
             Doffset(M) = grid%is_unique(M) - 1
          End Do
       end if
       Dstride = 1
       Dblock = 1
       
       ! Switch count and block to measure performance
       !Dstride = Dblock
       !Dblock = Dcount
       !Dcount = Dstride
       !Dstride = 1
       Dmem = Dcount*Dblock
       
       ! Create the hyperslab (memspace with no halo)
       Call h5screate_simple_f(NDIM, Ddims, filespace, ierror)
       Call h5screate_simple_f(NDIM, Dmem, memspace, ierror)
       Call h5sselect_hyperslab_f (filespace, H5S_SELECT_SET_F, Doffset, Dcount, ierror, Dstride, Dblock)
       if(.not. hasGrid) then
          Call h5sselect_none_f(memspace, ierror)
          Call h5sselect_none_f(filespace, ierror)
       endif
       
       Call h5pcreate_f(H5P_DATASET_XFER_F, plist_id, ierror)
       if(m_collective) Call h5pset_dxpl_mpio_f(plist_id, H5FD_MPIO_COLLECTIVE_F, ierror)
       
       ! Check if this grid has a mesh
       Call h5lexists_f(gridGroupID, xyznames(1), gridHasMesh, ierror)
       if (gridHasMesh) then
          allocate(drbuf(grid%nCells_unique))
          Do M = 1, NDIM
             Call h5dopen_f(gridGroupID, xyznames(M), dataset_id, ierror)
             Call h5dread_f(dataset_id, dtype_id, drbuf(1), Ddims, ierror, &
                  file_space_id = filespace, mem_space_id = memspace, xfer_prp = plist_id)
             Call h5dclose_f(dataset_id, ierror)
             if (hasGrid) then
                ptr_to_box => grid%XYZ(:,M)
                Call rBufferToBox(grid,drbuf,ptr_to_box)
             end if
          End Do
          deallocate(drbuf)
          
          Call h5lexists_f(gridGroupID, "IBLANK", useIB, ierror)
          if(useIB) then
             allocate(irbuf(grid%nCells_unique))
             allocate(drbuf(grid%nCells_unique))
             allocate(iblank_dble(size(grid%iblank,1)))
             iblank_dble = 0._rfreal
             Call h5dopen_f(gridGroupID, "IBLANK", dataset_id, ierror)
             Call h5dread_f(dataset_id, itype_id, irbuf(1), Ddims, ierror, &
                  file_space_id = filespace, mem_space_id = memspace, xfer_prp = plist_id)
             Call h5dclose_f(dataset_id, ierror)
             if (hasGrid) then
                drbuf(:) = dble(irbuf(:))
                ptr_to_box => iblank_dble
                Call rBufferToBox(grid,drbuf,ptr_to_box)
                grid%IBLANK(:) = int(iblank_dble(:))
             end if
             deallocate(irbuf)
             deallocate(drbuf)
             deallocate(iblank_dble)
          Else
             grid%IBLANK(:) = 1
          endif
       endif
       
       Call h5gclose_f(gridGroupID, ierror)
       Call h5sclose_f(filespace, ierror)
       Call h5sclose_f(memspace, ierror)
       Call h5pclose_f(plist_id, ierror)
       CALL H5GCLOSE_F(rootGroupID,ierror)
       Call h5fclose_f(region%hdf5_io%m_file_id, ierror);

    END IF
       
!    CALL MPI_BARRIER(MYCOMM,ierr)
    
    ! ... Now get sponge information
    ! ... loop through all patches
    do npatch = 1, region%nPatches

       patch => region%patch(npatch)
       grid => region%grid(patch%gridID)

       ! ... only save information if this grid corresponds to ng_in_file
       if ( grid%iGridGlobal == ng_in_file ) then

          ! ... check to see if this is a sponge boundary
          if ( patch%bcType == SPONGE .or. patch%bcType == SPONGE_WITH_LINEARIZED_DISTURBANCE ) then

             N(:) = 1; Np(:) = 1
             do j = 1, grid%ND
                N(j)  =  grid%ie(j) -  grid%is(j) + 1
                Np(j) = patch%ie(j) - patch%is(j) + 1
             end do

             normDir = abs(patch%normDir)
             sgn = normDir / patch%normDir

             ! ... extent of this ENTIRE sponge zone (in C-counting)
             is(:) = 0; ie(:) = 0; Ns(:) = 1; 
             do j = 1, grid%ND
                is(j) = patch%bcData(4+2*(j-1)  )
                ie(j) = patch%bcData(4+2*(j-1)+1)
                Ns(j) = ie(j) - is(j) + 1
             end do

             ! ... allocate memory for sponge boundary coordinates
             allocate(patch%sponge_xs(patch%prodN,grid%ND))
             allocate(patch%sponge_xe(patch%prodN,grid%ND))

             ! ... loop over all of the points and save the number of indeces
             Do k = patch%is(3), patch%ie(3)
                Do j = patch%is(2), patch%ie(2)
                   Do i = patch%is(1), patch%ie(1)

                      lp = (k-patch%is(3))*Np(1)*Np(2) + (j-patch%is(2))*Np(1) + (i-patch%is(1)+1)

                      Select Case (patch%normDir)

                      Case (+1)
                         ijkInd(:) = (/ie(1), j, k/)
                         patch%sponge_xs(lp,1:grid%ND) = DBLE(ijkInd(1:grid%ND))
                         ijkInd(:) = (/is(1), j, k/)
                         patch%sponge_xe(lp,1:grid%ND) = DBLE(ijkInd(1:grid%ND))
                      Case (-1)
                         ijkInd(:) = (/is(1), j, k/)
                         patch%sponge_xs(lp,1:grid%ND) = DBLE(ijkInd(1:grid%ND))
                         ijkInd(:) = (/ie(1), j, k/)
                         patch%sponge_xe(lp,1:grid%ND) = DBLE(ijkInd(1:grid%ND))
                      Case (+2)
                         ijkInd(:) = (/i, ie(2), k/)
                         patch%sponge_xs(lp,1:grid%ND) = DBLE(ijkInd(1:grid%ND))
                         ijkInd(:) = (/i, is(1), k/)
                         patch%sponge_xe(lp,1:grid%ND) = DBLE(ijkInd(1:grid%ND))
                      Case (-2)
                         ijkInd(:) = (/i, is(2), k/)
                         patch%sponge_xs(lp,1:grid%ND) = DBLE(ijkInd(1:grid%ND))
                         ijkInd(:) = (/i, ie(1), k/)
                         patch%sponge_xe(lp,1:grid%ND) = DBLE(ijkInd(1:grid%ND))
                      Case (+3)
                         ijkInd(:) = (/i, j, ie(3)/)
                         patch%sponge_xs(lp,1:grid%ND) = DBLE(ijkInd(1:grid%ND))
                         ijkInd(:) = (/i, j, is(3)/)
                         patch%sponge_xe(lp,1:grid%ND) = DBLE(ijkInd(1:grid%ND))
                      Case (-3)
                         ijkInd(:) = (/i, j, is(3)/)
                         patch%sponge_xs(lp,1:grid%ND) = DBLE(ijkInd(1:grid%ND))
                         ijkInd(:) = (/i, j, ie(3)/)
                         patch%sponge_xe(lp,1:grid%ND) = DBLE(ijkInd(1:grid%ND))
                      End Select

                      if (abs(sqrt(dot_product( patch%sponge_xe(lp,1:grid%ND) - patch%sponge_xs(lp,1:grid%ND), patch%sponge_xe(lp,1:grid%ND) - patch%sponge_xs(lp,1:grid%ND)))) <= 1e-10) then
                         print *, 'PlasComCM: sponge length too small ', i, j, k
                      end if
                   End Do
                End Do
             End do

          end if

       end if

    end do

  End Subroutine ReadSingleGrid_HDF5


             
  Subroutine ReadSingleHDF5Grid_MPIIO(fileName,region,ng_in_file,gridExists,hasIBLANK)
    
    USE ModGlobal
    USE ModDataStruct
    USE ModMPI
    USE ModIOUtil

    IMPLICIT None

    TYPE (t_region), pointer     :: region
    INTEGER,INTENT(IN)           :: ng_in_file
    CHARACTER(LEN=80),INTENT(IN) :: fileName
    INTEGER                      :: gridExists, hasIBLANK

! ... local variables
    INTEGER :: ngrid, NDIM, M, ftype, ng, p, dir
    INTEGER :: ierror, i
    TYPE (t_mixt_input), POINTER :: input
    TYPE (t_grid), POINTER :: grid
    CHARACTER(LEN=80):: fname
    REAL(RFREAL), POINTER :: rBoxBuffer(:), rVarBuffer(:), gridBuf(:,:)
    INTEGER, POINTER :: iBlankBuf(:), iVarBuffer(:)

    LOGICAL :: gridHasMesh, hasGrid, useIB

    INTEGER, DIMENSION(3) :: gridSize, partitionSize, partitionStart
    CHARACTER(LEN=8) :: groupname = "Grid"


    ierror = 0
    gridExists = FALSE
    hasIBLANK = FALSE

    WRITE (groupname,'("Group",I3.3)') ng_in_file

    DO ng = 1, region%nGrids
       grid  => region%grid(ng)

       IF (grid%iGridGlobal == ng_in_file) THEN          
          NDIM = grid%input%ND
          input => grid%input
          
          IF ((debug_on .EQV. .TRUE.) .AND. (grid%myrank_incomm == 0)) THEN
             WRITE (*,'(A)') 'PlasComCM: Reading "'//TRIM(fileName)//'('//TRIM(groupname)//&
                  ')".'
          ENDIF

          !set the dimensions
          gridSize = 0
          partitionSize = 0
          partitionStart = 0
          DO M=1,NDIM
             gridSize(M)       = grid%GlobalSize(M)
             partitionSize(M)  = grid%ie_unique(M) - grid%is_unique(M) + 1
             partitionStart(M) = grid%is_unique(M) - 1
          END DO
          
          
          ALLOCATE(gridBuf(grid%nCells_unique,NDIM))
          ALLOCATE(iBlankBuf(grid%nCells_unique))
          
          gridExists = FALSE
          hasIBLANK = FALSE
          CALL hdf5readgriddata(TRIM(fileName),TRIM(groupname),grid%comm,gridBuf,iBlankBuf,&
               NDIM,partitionStart,partitionSize,gridSize,gridExists,hasIBLANK,ierror)
          
          IF(ierror > 0) THEN
             WRITE (*,'(A)') 'PlasComCM: Read of "'//TRIM(fileName)//'('//TRIM(groupname)//&
                  ')" failed, aborting.'
             CALL graceful_exit(region%myrank, 'PlasComCM: ERROR: Reading grid failed.')
          ENDIF
          
          
          IF ((debug_on .EQV. .TRUE.) .AND. (grid%myrank_incomm == 0)) THEN
             IF(gridExists > 0) WRITE(*,'(A)') 'PlasComCM: Found grid.'
             IF(hasIBLANK  > 0) WRITE(*,'(A)') 'PlasComCM: Found iblank.'
             WRITE (*,'(A)') 'PlasComCM: Done reading "'//TRIM(fileName)//'('//TRIM(groupname)//&
                  ')".'
          ENDIF
          
          IF(gridExists > 0) THEN
             IF ((debug_on .EQV. .TRUE.) .AND. (grid%myrank_incomm == 0)) THEN
                WRITE (*,'(A)') 'PlasComCM: Populating box buffers for grid.'
             ENDIF
             DO M=1,NDIM
                rBoxBuffer  => grid%XYZ(:,M)
                rVarBuffer => gridBuf(:,M)
                CALL rBufferToBox(grid,rVarBuffer,rBoxBuffer)
             END DO
             IF(hasIBLANK > 0) THEN
                IF ((debug_on .EQV. .TRUE.) .AND. (grid%myrank_incomm == 0)) THEN
                   WRITE (*,'(A)') 'PlasComCM: Populating box buffers for iblank.'
                ENDIF
                CALL iBufferToBox(grid,iBlankBuf,grid%IBLANK)                
             ELSE
                IF ((debug_on .EQV. .TRUE.) .AND. (grid%myrank_incomm == 0)) THEN
                   WRITE (*,'(A)') 'PlasComCM: No iblank in file, initializing to 1.'
                ENDIF
                grid%IBLANK(:) = 1
             END IF
          ELSE
             IF ((debug_on .EQV. .TRUE.) .AND. (grid%myrank_incomm == 0)) THEN
                WRITE (*,'(A)') 'PlasComCM: No grid read from "'//TRIM(fileName)//'('//TRIM(groupname)//&
                     ')".'
             ENDIF
          ENDIF ! gridExists
          
          DEALLOCATE(gridBuf)
          DEALLOCATE(iBlankBuf)
          
          IF ((debug_on .EQV. .TRUE.) .AND. (grid%myrank_incomm == 0)) THEN
             WRITE (*,'(A)') 'PlasComCM: Done reading "'//TRIM(fileName)//'('//TRIM(groupname)//&
                  ')".'
          ENDIF
          
       ENDIF ! grid%iGridGlobal == ng_in_file
       
    END DO
    
  End Subroutine READSINGLEHDF5GRID_MPIIO
  
  Subroutine ReadSingleHDF5Solution_MPIIO(fileName,region,ng_in_file,solnName,solnBuffer,numVar)
    
    USE ModGlobal
    USE ModDataStruct
    USE ModMPI
    USE ModIOUtil

    IMPLICIT None

    TYPE (t_region), pointer     :: region
    INTEGER,INTENT(IN)           :: ng_in_file
    CHARACTER(LEN=80),INTENT(IN) :: fileName
    CHARACTER(LEN=3)             :: solnName
    INTEGER,INTENT(IN)           :: numVar
    REAL(RFREAL), DIMENSION(:,:), POINTER :: solnBuffer
    
    ! ... local variables
    INTEGER :: ngrid, NDIM, M, ftype, ng, p, dir
    INTEGER :: ierror, i
    TYPE (t_mixt_input), POINTER :: input
    TYPE (t_grid), POINTER :: grid
    TYPE(t_mixt), POINTER :: state
    CHARACTER(LEN=80):: fname
    REAL(RFREAL), POINTER :: rBoxBuffer(:), rVarBuffer(:), solutionBuf(:,:)
    INTEGER, POINTER :: iBlankBuf(:), iVarBuffer(:)
    
    INTEGER, DIMENSION(3) :: gridSize, partitionSize, partitionStart
    CHARACTER(LEN=8) :: groupname = "Grid"
    
    
    ierror = 0
    
    WRITE (groupname,'("Group",I3.3)') ng_in_file
    
    DO ng = 1, region%nGrids

       grid  => region%grid(ng)
       state => region%state(ng)

       IF (grid%iGridGlobal == ng_in_file) THEN          
          NDIM = grid%input%ND
          input => grid%input
          
          IF ((debug_on .EQV. .TRUE.) .AND. (grid%myrank_incomm == 0)) THEN
             WRITE (*,'(A)') 'PlasComCM: Reading solution from "'//TRIM(fileName)//'('//TRIM(groupname)//&
                  ')".'
          ENDIF
          
          !set the dimensions
          gridSize = 0
          partitionSize = 0
          partitionStart = 0
          DO M=1,NDIM
             gridSize(M)       = grid%GlobalSize(M)
             partitionSize(M)  = grid%ie_unique(M) - grid%is_unique(M) + 1
             partitionStart(M) = grid%is_unique(M) - 1
          END DO
          
          
          ALLOCATE(solutionBuf(grid%nCells_unique,numVar))
          
          CALL hdf5readsolutiondata(TRIM(fileName),TRIM(groupname),TRIM(solnName),grid%comm,solutionBuf, &
               NDIM,partitionStart,partitionSize,gridSize,numVar,ierror)
          
          IF(ierror > 0) THEN
             WRITE (*,'(A)') 'PlasComCM: Solution read of "'//TRIM(fileName)//'('//TRIM(groupname)//&
                  ')" failed, aborting.'
             CALL graceful_exit(region%myrank, 'PlasComCM: ERROR: Reading solution failed.')
          ENDIF
          
          
          IF ((debug_on .EQV. .TRUE.) .AND. (grid%myrank_incomm == 0)) THEN
             WRITE (*,'(A)') 'PlasComCM: Populating box buffers for solution.'
          ENDIF
          
          DO M=1,numVar
             rBoxBuffer  => solnBuffer(:,M)
             rVarBuffer  => solutionBuf(:,M)
             CALL rBufferToBox(grid,rVarBuffer,rBoxBuffer)
          END DO
          
          DEALLOCATE(solutionBuf)
          
          IF ((debug_on .EQV. .TRUE.) .AND. (grid%myrank_incomm == 0)) THEN
             WRITE (*,'(A)') 'PlasComCM: Done reading solution from "'//TRIM(fileName)//'('//TRIM(groupname)//&
                  ')".'
          ENDIF
          
       ENDIF ! grid%iGridGlobal == ng_in_file
       
    END DO
    
  End Subroutine READSINGLEHDF5SOLUTION_MPIIO
  
  SUBROUTINE READHDF5RESTART_MPIIO(region, fname, isTarget_IN)

    USE ModGlobal
    USE ModDataStruct
    USE ModMPI
    USE ModIOUtil

    Implicit None

    Type (t_region), Pointer :: region
    Character(LEN=80),intent(IN) :: fname
    integer, intent(IN), OPTIONAL :: isTarget_IN

    ! Local variables
    Type (t_mixt), Pointer :: state
    Type (t_grid), Pointer :: grid
    
    real(rfreal) :: tau(4)
    character(len=2) :: prec, gf, vf
    Integer :: NDIM, nglobal, ng, N(MAX_ND)
    Integer :: gridID ! ... specIfy which grid we want to read, JKim 04/2008

    Real(RFREAL), Dimension(:,:), Pointer :: solutionBuffer
    integer :: K, M, NVAR, ierr, ierror, isTarget
    CHARACTER(LEN=8) :: groupname = "Grid",myName

    Integer(hsize_t) :: NN, hdims(1), hmaxdims(1), Ddims(3),Ddims_(3)
    character(len=3) :: varName
    INTEGER(KIND=8)  :: isize
    INTEGER :: gridPresent   = FALSE
    INTEGER :: iblankPresent = FALSE

    INTEGER               :: gridRank, numVar
    INTEGER, DIMENSION(3) :: partitionStart, partitionSize
    INTEGER(hid_t)        :: fileID, gridGroupID, rootGroupID, rootHeaderID, gridHeaderID
    INTEGER(hid_t)        :: rootHeaderSpaceID, gridHeaderSpaceID, headerDataTypeID
    INTEGER(hsize_t)      :: headerDims(1), headerMaxDims(1), gridSize(3)
    INTEGER(hid_t)        :: doubleTypeID, intTypeID

    REAL(KIND=8), Dimension(:), Pointer :: gridHeader

    If(present(isTarget_IN)) then
       isTarget=isTarget_IN
    Else
       isTarget = 0
    end If

    doubleTypeID = H5T_NATIVE_DOUBLE
    intTypeID = H5T_NATIVE_INTEGER

    DO nglobal = 1, region%nGridsGlobal
       NULLIFY(state)
       WRITE (groupname,'("Group",I3.3)') nglobal
       DO ng = 1, region%nGrids
          grid  => region%grid(ng)
          state => region%state(ng)
          IF (grid%iGridGlobal == nglobal) THEN
             IF(grid%myrank_inComm == 0) THEN
                If (debug_on .eqv. .true.) Write (*,'(A)') 'PlasComCM: Reading headers for "'//trim(fname)//'('&
                     //TRIM(groupname)//')".'    

                CALL HDF5InFile(fname,fileID,rootGroupID,MPI_COMM_SELF)
                CALL H5Aopen_f(rootGroupID, "HEADER", rootHeaderID, ierror)
                CALL H5Aget_space_f(rootHeaderID, rootHeaderSpaceID, ierror)
                CALL H5Sget_simple_extent_dims_f(rootHeaderSpaceID, headerDims, headerMaxDims, ierror)
                CALL H5Sclose_f(rootHeaderSpaceID, ierror)
                CALL H5Aread_f(rootHeaderID, doubleTypeID, tau, headerDims, ierror)
                CALL H5Aclose_f(rootHeaderID, ierror)
                CALL H5Gclose_f(rootGroupID,  ierror)
                
                CALL H5Gopen_f(fileID, trim(groupname),gridGroupID, ierror)
                CALL H5Aopen_f(gridGroupID, "HEADER", gridHeaderID, ierror)
                CALL H5Aget_space_f(gridHeaderID, gridHeaderSpaceID, ierror)
                CALL H5Sget_simple_extent_dims_f(gridHeaderSpaceID, headerDims, headerMaxDims, ierror)
                CALL H5Sclose_f(gridHeaderSpaceID, ierror) 
      
                ALLOCATE(gridHeader(headerDims(1)))
                CALL H5Aread_f(gridHeaderID,doubleTypeID,gridHeader, headerDims, ierror)
                tau(4) = gridHeader(4)
                DEALLOCATE(gridHeader) 
                CALL H5Aclose_f(gridHeaderID, ierror)
                CALL H5Gclose_f(gridGroupID, ierror)
                CALL H5Fclose_f(fileID, ierror)
                
                
                IF (debug_on .EQV. .TRUE.) THEN
                   WRITE (*,'(A)') 'PlasComCM: Reading headers done for "'//TRIM(fname)//'('&
                        //TRIM(groupName)//')".'
                ENDIF

             ENDIF

             CALL MPI_Bcast(tau,4,MPI_DOUBLE_PRECISION,0,grid%comm,ierr)

             ! Set the dimensions
             NDIM = grid%input%ND
             gridSize = 0
             partitionSize = 0
             partitionStart = 0

             numVar = NDIM+2

             DO M=1,NDIM
                gridSize(M)      = grid%GlobalSize(M)
                partitionSize(M) = grid%ie_unique(M) - grid%is_unique(M) + 1
                partitionStart(M) = grid%is_unique(M) - 1
             END DO

             IF(isTarget == FALSE) THEN
                CALL ReadSingleHDF5Grid_MPIIO(fname,region,nglobal,gridPresent,iblankPresent)
                IF((debug_on .EQV. .TRUE.) .AND. (gridPresent==TRUE) .AND. &
                     (grid%myrank_incomm == 0)) THEN
                   WRITE(*,'(A)') 'PlasComCM: Found grid in "'//TRIM(fname)//'('//TRIM(groupName)//&
                        ')".'
                ENDIF
                solutionBuffer => state%cv
                CALL ReadSingleHDF5Solution_MPIIO(fname,region,nglobal,'cv ',solutionBuffer,numVar)
                IF(region%input%nAuxVars > 0) THEN
                   solutionBuffer => state%auxVars
                   CALL ReadSingleHDF5Solution_MPIIO(fname,region,nglobal,'aux',solutionBuffer,region%input%nAuxVars)
                ENDIF
             ELSE
                solutionBuffer => state%cvTarget
                CALL ReadSingleHDF5Solution_MPIIO(fname,region,nglobal,'cvt',solutionBuffer,numVar)
                IF(region%input%nAuxVars > 0) THEN
                   solutionBuffer => state%auxVarsTarget
                   CALL ReadSingleHDF5Solution_MPIIO(fname,region,nglobal,'aut',solutionBuffer,region%input%nAuxVars)
                ENDIF
             ENDIF
             
             If(isTarget==FALSE) Then
                ! Update the time
                Do k = 1, grid%nCells
                   state%time(k) = tau(4)
                End Do
                
                ! Update the iteration number
                region%input%nstepi = nint(tau(1))
                
                ! Temporary fix
                grid%input%nstepi = region%input%nstepi
             End If

             If ((debug_on .eqv. .true.) .and. (grid%myrank_incomm == 0)) Write (*,'(A)') 'PlasComCM: Done reading "'//trim(fname)//'('&
                  //TRIM(groupname)//')".'    

          END IF
       End Do
    END DO
  END SUBROUTINE READHDF5RESTART_MPIIO
     

  
  
Subroutine write_HDF_stats2D(region)
    USE ModGlobal
    USE ModDataStruct
    USE ModMPI
    Implicit None

    type(t_region), pointer :: region

    ! Local variables
    type(t_grid), pointer :: grid
    type(t_mixt), pointer :: state
    integer :: nglobal, ng, NDIM, N, NVAR, iter, idot, ierror, GD(2), i, l0, j, symdir, ind(2)
    Integer(hsize_t) :: Ddims(2)
    logical :: containsMesh, hasGrid
    Character(LEN=28) :: fname_h5
    Character(LEN=80) :: fname_xml
    Character(LEN=10) :: TopologyType, GeometryType
    Character(len=1), Dimension(2) :: xyznames = (/ 'X', 'Y' /)
    Integer(hsize_t) :: hdims(1)
    Integer(hsize_t), DIMENSION(2) :: Dcount, Doffset, Dstride, Dblock, Dmem
    INTEGER(HID_T) :: dcpl_id ,access_id     ! Property list identIfier 
    Integer(hid_t)   :: dtype_id, itype_id, ctype_id
    INTEGER(HID_T) :: header_id, group_id ,aspace_id, attr_id, dataspace_id, dataset_id, plist_id, memspace, Nsym
    CHARACTER(LEN=8) :: groupname = "Grid"
    Character(LEN=10) :: GridType, MyName
    Character(LEN=20) :: pformat
    REAL(KIND=8), allocatable :: myXYZ(:,:), mySumvar(:,:) 
    REAL(KIND=8) :: norm

    ! ... simplicity
    grid => region%grid(stat2D%statgrid)
    state => region%state(stat2D%statgrid)
    symdir = stat2D%symdir(1)

    ! ... assign dimensions
    NDIM = 2

    ! ... set the HDF5 filename
    iter = region%global%main_ts_loop_index
    write(fname_h5(1:25),'(A17,I8.8)') 'RocFlo-CM.Stat2D.',iter
    write(fname_h5(26:28),'(A3)') '.h5'

    ! ... write to screen
    If (grid%parDir .EQ. FALSE .and. region%myrank .EQ. 0 .or. &
         grid%parDir .NE. FALSE .and. grid%myrank_inComm .EQ. 0) Then
       Write (*,'(A)') 'PlasComCM: Writing 2D stats to '//trim(fname_h5)
    End If

    ! ... initialize FORTRAN predefined datatypes
    CALL InitHDF5
!    Call h5open_f(ierror)
!    If (ierror.ne.0) Stop 'ierror: h5open_f'

    ! ... create the HDF5 file
    Call h5pcreate_f(H5P_FILE_ACCESS_F, access_id, ierror)
    Call h5pset_fapl_mpio_f(access_id, mycomm, region%hdf5_io%m_info, ierror)
    Call h5fcreate_f(trim(fname_h5), H5F_ACC_TRUNC_F, region%hdf5_io%m_file_id, ierror, access_prp = access_id)
    Call h5pclose_f(access_id, ierror)

    ! ... assign the data file types
    itype_id=H5T_NATIVE_INTEGER
    ctype_id=H5T_NATIVE_CHARACTER
    dtype_id=H5T_NATIVE_DOUBLE

    ! ... H5 open
    Call h5gopen_f(region%hdf5_io%m_file_id, '/.', header_id, ierror)

    ! ... write the averaging time Delta_t
    hdims(1) = 1
    Call h5screate_simple_f(1, hdims, aspace_id, ierror)
    Call h5acreate_f(header_id, "Delta_t", dtype_id, aspace_id, &
         attr_id, ierror)
    Call h5awrite_f(attr_id, dtype_id, stat2D%Delta_t, hdims, ierror)
    Call h5sclose_f(aspace_id, ierror)
    Call h5aclose_f(attr_id, ierror)
    Call h5gclose_f(header_id, ierror)

    ! ... group name for specific grid
    write (groupname,'("Group",I3.3)') stat2D%statgrid
    Call h5gcreate_f(region%hdf5_io%m_file_id, trim(groupname), group_id, ierror)

    ! ...From CreateGroupWriteHeader
    ! ...write the header
    hdims(1) = 1
    Call h5screate_simple_f(1, hdims, aspace_id, ierror)
    Call h5acreate_f(group_id, "HEADER", dtype_id, aspace_id, &
          attr_id, ierror)
    Call h5awrite_f(attr_id, dtype_id, state%time(1), hdims, ierror)
    Call h5sclose_f(aspace_id, ierror)
    Call h5aclose_f(attr_id, ierror)
    Call h5gclose_f(group_id, ierror)

   ! ... get grid dimensions
   write (groupname,'("Group",I3.3)') stat2D%statgrid
   Call h5gopen_f(region%hdf5_io%m_file_id, trim(groupname), group_id, ierror)
    Select case(symdir)
    Case(1)
      Ddims(1) = grid%GlobalSize(2)
      Ddims(2) = grid%GlobalSize(3)
      Dcount(1) = grid%ie(2) - grid%is(2) + 1
      Dcount(2) = grid%ie(3) - grid%is(3) + 1
      Nsym = grid%ie(1) - grid%is(1) + 1
      Doffset(1) = grid%is(2) - 1
      Doffset(2) = grid%is(3) - 1
      ind(1) = 2; ind(2) = 3
    Case(2)
      Ddims(1) = grid%GlobalSize(1)
      Ddims(2) = grid%GlobalSize(3)
      Dcount(1) = grid%ie(1) - grid%is(1) + 1
      Dcount(2) = grid%ie(3) - grid%is(3) + 1
      Nsym = grid%ie(2) - grid%is(2) + 1
      Doffset(1) = grid%is(1) - 1
      Doffset(2) = grid%is(3) - 1
      ind(1) = 1; ind(2) = 3
    Case(3)
      Ddims(1) = grid%GlobalSize(1)
      Ddims(2) = grid%GlobalSize(2)
      Dcount(1) = grid%ie(1) - grid%is(1) + 1
      Dcount(2) = grid%ie(2) - grid%is(2) + 1
      Nsym = grid%ie(3) - grid%is(3) + 1 
      Doffset(1) = grid%is(1) - 1
      Doffset(2) = grid%is(2) - 1
      ind(1) = 1; ind(2) = 2
   End Select

   ! ... write the grid
   Dstride = 1
   Dblock = 1
   Call h5pcreate_f(H5P_DATASET_CREATE_F, dcpl_id, ierror)
   Dmem = Dcount*Dblock
   Call h5screate_simple_f(NDIM, Dmem, memspace, ierror)
   
   Call h5screate_simple_f(ndim, Ddims, dataspace_id, ierror)
   Call h5sselect_hyperslab_f (dataspace_id, H5S_SELECT_SET_F, Doffset, Dcount, ierror, Dstride, Dblock)
   
   Call h5pcreate_f(H5P_DATASET_XFER_F, plist_id, ierror) 
   If(m_collective) Call h5pset_dxpl_mpio_f(plist_id, H5FD_MPIO_COLLECTIVE_F, ierror)

   ! ... allocate buffer arrays
   Allocate(myxyz(Dcount(1)*Dcount(2),2), mySumvar(Dcount(1)*Dcount(2),1))
   myXYZ = 0.0D0
   mySumvar = 0.0D0

   ! ... populate buffer arrays
   norm = 1.0_8/stat2D%delta_t
   DO i = 1 , Dcount(1)
      DO j = 1, Dcount(2)
         l0 = i + (j-1)*Dcount(1)*Nsym
         ! ... normalize sum array
         mySumvar(i + Dcount(1)*(j-1),1) = stat2d%sumvar2D(i,j,1)*norm
         myXYZ(i + Dcount(1)*(j-1),1) = grid%XYZ(l0 ,ind(1))
         myXYZ(i + Dcount(1)*(j-1),2) = grid%XYZ(l0 ,ind(2))
      ENDDO
   ENDDO

   ! ... write coordinates
   Do N=1,NDIM
      Call h5dcreate_f(group_id, trim(xyznames(N)), dtype_id, dataspace_id, dataset_id, ierror, dcpl_id=dcpl_id)
      Call h5dwrite_f(dataset_id, dtype_id, myxyz(1,N), Ddims, ierror, &
           file_space_id = dataspace_id, mem_space_id = memspace, xfer_prp = plist_id)
      Call h5dclose_f(dataset_id, ierror)
   End Do
   Call h5pcreate_f(H5P_DATASET_CREATE_F, dcpl_id, ierror)     
  
   ! ... write the stats
   NVAR = stat2D%nvar
   Do N = 1, NVAR
      Call h5dcreate_f(group_id, trim(stat2D%varname(N)), dtype_id, dataspace_id, dataset_id, ierror, dcpl_id) 
      Call h5dwrite_f(dataset_id, dtype_id, mySumvar(1,N), Ddims,ierror, &
           file_space_id = dataspace_id, mem_space_id = memspace, xfer_prp = plist_id) 
      Call h5dclose_f(dataset_id, ierror)
   End Do
  
   ! ... close HDF5 files
   Call h5sclose_f(dataspace_id, ierror)
   Call h5pclose_f(plist_id, ierror)
   Call h5sclose_f(memspace, ierror)
   Call h5pclose_f(dcpl_id, ierror)
   Call h5gclose_f(group_id, ierror)
   
   ! ... final HDF5 close
   Call h5fclose_f(region%hdf5_io%m_file_id, ierror)

   ! ... write the XML file
   If (grid%parDir .EQ. FALSE .and. region%myrank .EQ. 0 &
        .or. grid%parDir .NE. FALSE .and. grid%myrank_inComm .EQ. 0) Then
      idot = index(fname_h5,'.',BACK=.true.)
      fname_xml = trim(fname_h5(1:idot-1)) // '.xmf'
      GridType='Uniform'
      TopologyType='2DSMesh'
      GeometryType='X_Y'
      write(pformat,'("(a,",i1,"(i0,1x),i0,a)")') NDIM-1

      ! ... open the file and write header stuff
      Open (region%hdf5_io%m_xmf, file=trim(fname_xml))
      Write (region%hdf5_io%m_xmf,20) '<?xml version="1.0" ?>'
      Write (region%hdf5_io%m_xmf,20) '<!DOCTYPE Xdmf SYSTEM "Xdmf.dtd" []>'
      Write (region%hdf5_io%m_xmf,20) '<Xdmf Version="2.0">'
      Write (region%hdf5_io%m_xmf,20) ' <Domain>'
      Write (region%hdf5_io%m_xmf,'(a,i0,1x,10a)') '   <Grid Name="mesh',stat2D%statgrid,'" GridType="',trim(GridType),'">'
      Write (region%hdf5_io%m_xmf,'(a,1pe13.5,10a)') '       <Time Value="',state%time(1),'" />'
      Write (region%hdf5_io%m_xmf,'(3a,2(i0,1x),a)') '     <Topology TopologyType="',trim(TopologyType),'" NumberOfElements="',Ddims(2),Ddims(1),'"/>'
      Write (region%hdf5_io%m_xmf,20) '     <Geometry GeometryType="',trim(GeometryType),'">'
      ! Write the coordinates
      Do N=1,NDIM
         Write (region%hdf5_io%m_xmf,trim(pformat)) '       <DataItem Dimensions="',Ddims(2),Ddims(1),'" NumberType="Float" Precision="8" Format="HDF">'
         Write (region%hdf5_io%m_xmf,20) '        ',trim(fname_h5(1:28)),':/',trim(groupname),'/',trim(xyznames(N))
         Write (region%hdf5_io%m_xmf,20) '       </DataItem>'
      End Do
      Write (region%hdf5_io%m_xmf,20) '     </Geometry>'
      ! Write the variables
      Do N = 1, NVAR
         write (myName,'(a,I2.2)') trim(stat2D%varname(N))
         Write (region%hdf5_io%m_xmf,20) '     <Attribute Name="',trim(myName),'" AttributeType="Scalar" Center="Node">'
         Write (region%hdf5_io%m_xmf,trim(pformat)) '       <DataItem Dimensions="',Ddims(2),Ddims(1),'" NumberType="Float" Precision="8" Format="HDF">'
         Write (region%hdf5_io%m_xmf,20) '        ',trim(fname_h5(1:28)),':/',trim(groupname),'/',trim(myName)
         Write (region%hdf5_io%m_xmf,20) '       </DataItem>'
         Write (region%hdf5_io%m_xmf,20) '     </Attribute>'
      End Do
      Write (region%hdf5_io%m_xmf,'(10a)') '   </Grid>'
      Write (region%hdf5_io%m_xmf,20) ' </Domain>'
      Write (region%hdf5_io%m_xmf,20) '</Xdmf>'
      Close(region%hdf5_io%m_xmf)
20    Format (10a)
33    Format (a,i0,1x,i0,a)
   End If

   ! ... clean up
   Deallocate(myXYZ,mySumvar)

 End Subroutine write_HDF_stats2D


  Subroutine read_HDF_stats2D(region,fname_h5)
    USE ModGlobal
    USE ModDataStruct
    USE ModMPI
    Implicit None

    type(t_region), pointer :: region

    ! Local variables
    type(t_grid), pointer :: grid
    type(t_mixt), pointer :: state
    integer :: nglobal, ng, NDIM, N, NVAR, iter, idot, ierror, GD(2), i, l0, j, symdir, ind(2)
    Integer(hsize_t) :: Ddims(2)
    logical :: containsMesh, hasGrid
    Character(LEN=28) :: fname_h5
    Character(LEN=80) :: fname_xml
    Character(LEN=10) :: TopologyType, GeometryType
    Character(len=1), Dimension(2) :: xyznames = (/ 'X', 'Y' /)
    Integer(hsize_t) :: hdims(1)
    Integer(hsize_t), DIMENSION(2) :: Dcount, Doffset, Dstride, Dblock, Dmem
    INTEGER(HID_T) :: dcpl_id ,access_id     ! Property list identIfier 
    Integer(hid_t)   :: dtype_id, itype_id, ctype_id, rootGroupID
    INTEGER(HID_T) :: header_id, group_id ,aspace_id, attr_id, dataspace_id, dataset_id, plist_id, memspace, filespace, Nsym
    CHARACTER(LEN=8) :: groupname = "Grid"
    Character(LEN=10) :: GridType, MyName
    Character(LEN=20) :: pformat
    REAL(KIND=8), allocatable :: myXYZ(:,:), mySumvar(:,:) 
    REAL(KIND=8) :: norm

    ! ... simplicity
    grid => region%grid(stat2D%statgrid)
    state => region%state(stat2D%statgrid)
    symdir = stat2D%symdir(1)

    ! ... assign dimensions
    NDIM = 2

    ! ... write to screen
    If (grid%parDir .EQ. FALSE .and. region%myrank .EQ. 0 .or. &
         grid%parDir .NE. FALSE .and. grid%myrank_inComm .EQ. 0) Then
       Write (*,'(A)') 'PlasComCM: Reading 2D stats '//trim(fname_h5)
    End If

    ! ... initialize FORTRAN predefined datatypes
    CALL InitHDF5


    ! ... assign the data file types
    itype_id=H5T_NATIVE_INTEGER
    ctype_id=H5T_NATIVE_CHARACTER
    dtype_id=H5T_NATIVE_DOUBLE

    ! ... H5 open
    CALL HDF5InFile(fname_h5,region%hdf5_io%m_file_id,rootGroupID)

    ! ... read the averaging time Delta_t
    hdims(1) = 1
    Call h5aopen_f(rootGroupID, "Delta_t", attr_id, ierror)
    Call h5aread_f(attr_id, dtype_id, stat2D%Delta_t, hdims, ierror)
    Call h5aclose_f(attr_id, ierror)

    ! ... group name for specific grid
    write (groupname,'("Group",I3.3)') stat2D%statgrid
    Call h5gcreate_f(region%hdf5_io%m_file_id, trim(groupname), group_id, ierror)

   ! ... get grid dimensions
   write (groupname,'("Group",I3.3)') stat2D%statgrid
   Call h5gopen_f(region%hdf5_io%m_file_id, trim(groupname), group_id, ierror)
    Select case(symdir)
    Case(1)
      Ddims(1) = grid%GlobalSize(2)
      Ddims(2) = grid%GlobalSize(3)
      Dcount(1) = grid%ie(2) - grid%is(2) + 1
      Dcount(2) = grid%ie(3) - grid%is(3) + 1
      Nsym = grid%ie(1) - grid%is(1) + 1
      Doffset(1) = grid%is(2) - 1
      Doffset(2) = grid%is(3) - 1
      ind(1) = 2; ind(2) = 3
    Case(2)
      Ddims(1) = grid%GlobalSize(1)
      Ddims(2) = grid%GlobalSize(3)
      Dcount(1) = grid%ie(1) - grid%is(1) + 1
      Dcount(2) = grid%ie(3) - grid%is(3) + 1
      Nsym = grid%ie(2) - grid%is(2) + 1
      Doffset(1) = grid%is(1) - 1
      Doffset(2) = grid%is(3) - 1
      ind(1) = 1; ind(2) = 3
    Case(3)
      Ddims(1) = grid%GlobalSize(1)
      Ddims(2) = grid%GlobalSize(2)
      Dcount(1) = grid%ie(1) - grid%is(1) + 1
      Dcount(2) = grid%ie(2) - grid%is(2) + 1
      Nsym = grid%ie(3) - grid%is(3) + 1 
      Doffset(1) = grid%is(1) - 1
      Doffset(2) = grid%is(2) - 1
      ind(1) = 1; ind(2) = 2
   End Select

   ! ... read stats file
   Dstride = 1
   Dblock = 1
   Dmem = Dcount*Dblock
   Call h5screate_simple_f(NDIM, Ddims, filespace, ierror)
   Call h5screate_simple_f(NDIM, Dmem, memspace, ierror)
   Call h5sselect_hyperslab_f (filespace, H5S_SELECT_SET_F, Doffset, Dcount, ierror, Dstride, Dblock)
   
   Call h5pcreate_f(H5P_DATASET_XFER_F, plist_id, ierror) 
   If(m_collective) Call h5pset_dxpl_mpio_f(plist_id, H5FD_MPIO_COLLECTIVE_F, ierror)

   ! ... allocate buffer arrays
   Allocate(mySumvar(Dcount(1)*Dcount(2),1))
  
   ! ... read the stats
   NVAR = stat2D%nvar
   Do N = 1, NVAR
      Call h5dopen_f(group_id, trim(stat2D%varname(N)), dataset_id, ierror) 
      Call h5dread_f(dataset_id, dtype_id, mySumvar(1,N), Ddims,ierror, &
           file_space_id = filespace, mem_space_id = memspace, xfer_prp = plist_id) 
      Call h5dclose_f(dataset_id, ierror)
      If (ierror.ne.0) Stop 'ierror: h5dopen_f reading stats'

      ! ... unpack stats
      DO i = 1 , Dcount(1)
         DO j = 1, Dcount(2)
            l0 = i + (j-1)*Dcount(1)*Nsym
            ! ... normalize sum array
            stat2d%sumvar2D(i,j,n) = mySumvar(i + Dcount(1)*(j-1),1)
         ENDDO
      ENDDO
   End Do

   ! Denormalize
   norm = stat2D%delta_t
   stat2d%sumvar2D = stat2d%sumvar2D*norm

   ! ... close HDF5 files
   Call h5gclose_f(group_id, ierror)
   Call h5sclose_f(filespace, ierror)
   Call h5sclose_f(memspace, ierror)
   Call h5pclose_f(plist_id, ierror)
   
   ! ... final HDF5 close
   Call h5gclose_f(group_id, ierror)
   Call h5fclose_f(region%hdf5_io%m_file_id, ierror)

   ! ... clean up
   Deallocate(mySumvar)

 End Subroutine read_HDF_stats2D

#endif

End Module ModHDF5_IO
