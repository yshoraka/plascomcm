! Copyright (c) 2015, University of Illinois at Urbana-Champaign, XPACC
! License: MIT, http://opensource.org/licenses/MIT
!-----------------------------------------------------------------------
!
! ModDataUtils.f90
!
! - Some utilities for data checking
!
! Revision history
! - 28 July 2010 : MTC : initial code
!
! $Header: /cvsroot/genx/Codes/RocfloCM/Source/ModDataUtils.f90
!
!-----------------------------------------------------------------------
MODULE ModDataUtils

CONTAINS

  SUBROUTINE CheckCV(region,fail_on_error)

    USE ModGlobal
    USE ModDataStruct
    USE ModMPI

    Implicit None

    type(t_region), pointer :: region
    LOGICAL :: fail_on_error

    ! ... local variables
    type(t_mixt_input), pointer :: input
    type(t_grid), pointer :: grid
    type(t_mixt), pointer :: state
    type(t_patch), pointer :: patch

    INTEGER :: ngrid, ii, I, J, K, L, l0, ng, N(3), ierr
    Integer, Dimension(:,:), Pointer :: ND
    REAL(RFREAL) :: RHO, U, E, RHO_LOW, RHO_HIGH, SPEED_LOW, SPEED_HIGH, E_LOW, E_HIGH

    input => region%input

    RHO_LOW    = input%DataLimit_LOWER_RHO
    RHO_HIGH   = input%DataLimit_UPPER_RHO
    SPEED_HIGH = input%DataLimit_SPEED
    SPEED_LOW  = -1.0_rfreal * SPEED_HIGH
    E_HIGH     = input%DataLimit_UPPER_ENERGY
    E_LOW      = input%DataLimit_LOWER_ENERGY


    IF(region%myrank == 0) WRITE(*,*) 'PlasComCM: Checking CV data.'
    ! ... loop over all of the grids
    ierr = 0
    DO ng = 1, region%nGrids
      grid  => region%grid(ng)
      state => region%state(ng)
      input => grid%input
      N(:) = 1
      DO ii = 1,grid%ND
         N(ii) = grid%ie(ii) - grid%is(ii) + 1
      ENDDO
      DO K = 1,N(3)
         DO J = 1,N(2)
            DO I = 1,N(1)
               l0 = (K-1)*N(1)*N(2) + (J-1)*N(1) + I
               RHO = state%cv(l0,1)
               IF(RHO < RHO_LOW .OR. RHO > RHO_HIGH .OR. (ISNAN(RHO) .eqv. .true.)) THEN
                  WRITE(*,*) 'PlasComCM: DATA_ERROR: RHO out of bounds,',RHO,ng,I,J,K
                  ierr = ierr + 1
               ENDIF
               U = state%cv(l0,2)/RHO
               IF( U < SPEED_LOW .OR. U > SPEED_HIGH .OR. (ISNAN(U) .eqv. .true.)) THEN
                  WRITE(*,*) 'PlasComCM: DATA_ERROR: U out of bounds, ',U,ng,I,J,K
                  ierr = ierr + 1
               ENDIF
               IF(input%ND >= 2) THEN
                  U = state%cv(l0,3)/RHO
                  IF( U < SPEED_LOW .OR. U > SPEED_HIGH .OR. (ISNAN(U).eqv..true.)) THEN
                     WRITE(*,*) 'PlasComCM: DATA_ERROR: V out of bounds, ',U,ng,I,J,K
                     ierr = ierr + 1
                  ENDIF
                  IF(input%ND > 2) THEN
                     U = state%cv(l0,3)/RHO
                     IF( U < SPEED_LOW .OR. U > SPEED_HIGH .OR. (ISNAN(U).eqv..true.)) THEN
                        WRITE(*,*) 'PlasComCM: DATA_ERROR: W out of bounds, ',U,ng,I,J,K
                        ierr = ierr + 1
                     ENDIF
                  ENDIF
               ENDIF
               E = state%cv(l0,grid%ND+2)/RHO
               IF( E < E_LOW .OR. E > E_HIGH .OR.  (ISNAN(E).eqv..true.)) THEN
                  WRITE(*,*) 'PlasComCM: DATA_ERROR: E out of bounds, ',E,ng,I,J,K
                  ierr = ierr + 1
               ENDIF
               DO L = 1, input%nAuxVars
                 IF (state%auxVars(l0,l) <= 0.0_8) THEN
                   state%auxVars(l0,l) = 1d-10
!!$                   WRITE(*,*) 'PlasComCM: DATA_ERROR: auxVars out of bounds, ', state%AuxVars(l0,l),l,ng,I,J,K
!!$                   ierr = ierr + 1
                 END IF
               END DO
            END DO
         END DO
      END DO
   END DO
   IF(ierr > 0 .and. (fail_on_error .eqv. .true.)) THEN
      CALL GRACEFUL_EXIT(region%myrank, 'DATA OUT OF BOUNDS')
   ENDIF
 END SUBROUTINE CheckCV

 LOGICAL FUNCTION ISNAN(X)
   
   REAL(8), INTENT(IN) :: X
   
   ISNAN = .FALSE. 
   IF ( .NOT.(X > -HUGE(1.0) .AND. X < HUGE(1.0)) ) THEN
      ISNAN = .TRUE.
   ENDIF
   
 END FUNCTION ISNAN
 
 LOGICAL FUNCTION EQL(x,y,wTol)
   
   REAL(8), INTENT(IN) :: x, y
   REAL(8), INTENT(IN), OPTIONAL :: wTol
   
   REAL(8) :: tol 
   
   EQL = .FALSE.
   
   IF ( PRESENT(wTol) .EQV. .TRUE. ) THEN
      tol = wTol
   ELSE
      tol = 10.0_8*EPSILON(1.0_8)
   END IF ! PRESENT                                                                     
   
   IF ( ABS(x-y) <= (1.0_8 + 0.5_8*ABS(x+y))*tol ) THEN
      EQL = .TRUE.
   END IF ! ABS                                                                         
   
 END FUNCTION EQL

 ! Istart is returned +- to indicate whether to copy values frontward or backward
 ! from trg_coords.  Only works for index contiguous patches (i.e. for 2D geometries)
 SUBROUTINE COLLIDECOORDS(src_coords,trg_coords,nsrc,ntrg,npts,sstart,tstart,wTol)
  
   IMPLICIT NONE
   
   REAL(8), POINTER, INTENT(IN)  :: src_coords(:)
   REAL(8), POINTER, INTENT(IN)  :: trg_coords(:)
   INTEGER, INTENT(IN)           :: nsrc,ntrg
   INTEGER, INTENT(OUT)          :: npts,sstart,tstart
   REAL(8), INTENT(IN), OPTIONAL :: wTol

   INTEGER :: scount, tcount,sindex, tindex,tend

   npts = 0
   sstart = 0
   tstart = 0
   tend = 0
   IF (PRESENT(wTol) .EQV. .TRUE. ) THEN
      DO scount = 1, nsrc
         sindex = 3*(scount-1)+1
         DO tcount = 1, ntrg
            tindex = 3*(tcount-1)+1
            IF (EQL(src_coords(sindex),trg_coords(tindex),wTol) .AND. &
                 EQL(src_coords(sindex+1),trg_coords(tindex+1),wTol) .AND. &
                 EQL(src_coords(sindex+2),trg_coords(tindex+2),wTol) ) THEN
               if(npts == 0) then
                  sstart = scount
                  tstart = tcount
               endif               
               npts = npts + 1
               tend = tcount 
               exit
            ENDIF
         END DO
      END DO
   ELSE
      DO scount = 1, nsrc
         sindex = 3*(scount-1)+1
         DO tcount = 1, ntrg
            tindex = 3*(tcount-1)+1
            IF (EQL(src_coords(sindex),trg_coords(tindex)) .AND. &
                 EQL(src_coords(sindex+1),trg_coords(tindex+1)) .AND. &
                 EQL(src_coords(sindex+2),trg_coords(tindex+2)) ) THEN
               if(npts == 0) then
                  sstart = scount
                  tstart = tcount
               endif               
               npts = npts + 1
               tend = tcount 
               exit
            ENDIF
         END DO
      END DO
   END IF
   IF(npts > 0) THEN
      IF((tend - tstart) < 0) tstart = -1*tstart
   ENDIF
 END SUBROUTINE COLLIDECOORDS

 ! ... record the indices of the collision in trg_indices(1:nrc), and npts
 SUBROUTINE COLLIDECOORDS2(src_coords,trg_indices,trg_coords,nsrc,ntrg,npts,wTol)
  
   IMPLICIT NONE
   
   REAL(8), POINTER, INTENT(IN)  :: src_coords(:)
   INTEGER, POINTER, INTENT(IN)  :: trg_indices(:)
   REAL(8), POINTER, INTENT(IN)  :: trg_coords(:)
   INTEGER, INTENT(IN)           :: nsrc,ntrg
   INTEGER, INTENT(OUT)          :: npts
   REAL(8), INTENT(IN), OPTIONAL :: wTol

   INTEGER :: scount, tcount,sindex, tindex,tend

   npts = 0
   IF (PRESENT(wTol) .EQV. .TRUE. ) THEN
      DO scount = 1, nsrc
         sindex = 3*(scount-1)+1
         DO tcount = 1, ntrg
            tindex = 3*(tcount-1)+1
            IF (EQL(src_coords(sindex),trg_coords(tindex),wTol) .AND. &
                 EQL(src_coords(sindex+1),trg_coords(tindex+1),wTol) .AND. &
                 EQL(src_coords(sindex+2),trg_coords(tindex+2),wTol) ) THEN
               npts = npts + 1
               trg_indices(scount) = tcount
               EXIT
            ENDIF
         END DO
      END DO
   ELSE
      DO scount = 1, nsrc
         sindex = 3*(scount-1)+1
         DO tcount = 1, ntrg
            tindex = 3*(tcount-1)+1
            IF (EQL(src_coords(sindex),trg_coords(tindex)) .AND. &
                 EQL(src_coords(sindex+1),trg_coords(tindex+1)) .AND. &
                 EQL(src_coords(sindex+2),trg_coords(tindex+2)) ) THEN
               npts = npts + 1
               trg_indices(scount) = tcount
               EXIT
            ENDIF
         END DO
      END DO
   END IF
 END SUBROUTINE COLLIDECOORDS2
   
 ! ... If any collision is detected, return .true. right away
 LOGICAL FUNCTION COORDSCOLLIDE(src_coords,trg_coords,nsrc,ntrg,wTol)
  
   IMPLICIT NONE
   
   REAL(8), POINTER, INTENT(IN)  :: src_coords(:)
   REAL(8), POINTER, INTENT(IN)  :: trg_coords(:)
   INTEGER, INTENT(IN)           :: nsrc,ntrg
   REAL(8), INTENT(IN), OPTIONAL :: wTol

   INTEGER :: scount, tcount,sindex, tindex

   COORDSCOLLIDE = .FALSE.


   IF (PRESENT(wTol) .EQV. .TRUE. ) THEN
      DO scount = 1, nsrc
         sindex = 3*(scount-1)+1
         DO tcount = 1, ntrg
            tindex = 3*(tcount-1)+1
            IF (EQL(src_coords(sindex),trg_coords(tindex),wTol) .AND. &
                 EQL(src_coords(sindex+1),trg_coords(tindex+1),wTol) .AND. &
                 EQL(src_coords(sindex+2),trg_coords(tindex+2),wTol) ) THEN
               COORDSCOLLIDE = .TRUE.
               EXIT
            ENDIF
         END DO
         IF(COORDSCOLLIDE .EQV. .TRUE.) EXIT
      END DO
   ELSE
      DO scount = 1, nsrc
         sindex = 3*(scount-1)+1
         DO tcount = 1, ntrg
            tindex = 3*(tcount-1)+1
            IF (EQL(src_coords(sindex),trg_coords(tindex)) .AND. &
                 EQL(src_coords(sindex+1),trg_coords(tindex+1)) .AND. &
                 EQL(src_coords(sindex+2),trg_coords(tindex+2)) ) THEN
               COORDSCOLLIDE = .TRUE.
               EXIT
            ENDIF
         END DO
         IF(COORDSCOLLIDE .EQV. .TRUE.) EXIT
      END DO
   END IF
 END FUNCTION COORDSCOLLIDE
   

END MODULE ModDataUtils
