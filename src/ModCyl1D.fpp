! Copyright (c) 2015, University of Illinois at Urbana-Champaign, XPACC
! License: MIT, http://opensource.org/licenses/MIT
!----------------------------------------------------------------------
!
! ModCyl1D.f90
!
! Computes the 1-D Compressible Navier-Stokes Equations in Cylindrical
! Coordinates
! 
! flags == FALSE    : compute inviscid fluxes only
! flags == TRUE     : compute dissipative fluxes only
! flags == COMBINED : inviscid + dissipative fluxes
!
! $Header: /cvsroot/genx/Codes/RocfloCM/Source/ModCyl1D.f90,v 1.1 2011/01/07 04:01:32 bodony Exp $
!
!-----------------------------------------------------------------------
MODULE ModCyl1D

CONTAINS

  subroutine Cyl1D_RHS(region, ng, flags)

    USE ModGlobal
    USE ModDataStruct
    USE ModEOS
    USE ModDeriv
    USE ModIO
    USE ModMPI

    Implicit None

    ! ... local variables
    type(t_grid), pointer :: grid
    type(t_mixt), pointer :: state
    type(t_mixt_input), pointer :: input
    type(t_region), pointer :: region
    type(t_mpi_timings), pointer :: mpi_timings
    integer :: flags, ng, i, k, Nc, ND, is
    real(rfreal), pointer :: flux(:), dflux(:)
    real(rfreal), pointer :: Vr(:), Ve(:), Phi(:), dudr(:), d2udr2(:), dTdr(:), d2Tdr2(:)
    real(rfreal), pointer :: dmudr(:), dlambdadr(:), dkdr(:)
    real(rfreal) :: dudr_origin, rho_origin, p_origin, rhoE_origin, d2Tdr2_origin, dtmp
    integer, parameter :: Npolyfit = 10

    ! ... simplicity
    input       => region%input
    grid        => region%grid(ng)
    state       => region%state(ng)
    mpi_timings => region%mpi_timings(ng)
    Nc          = grid%nCells
    ND          = grid%ND
    is          = 1
    if (grid%is(1) == 1) is = 2

    ! ... buffers
    allocate(flux(Nc),dflux(Nc))

    ! ... continuity : d{rho}/dt = -1/r d{rho U r}/dr
    do i = 1, Nc
      flux(i) = grid%XYZ(i,1) * state%cv(i,2)
    end do
    call APPLY_OPERATOR(region, ng, input%iFirstDeriv, 1, flux, dflux, .FALSE.)
    do i = is, Nc
      state%rhs(i,1) = state%rhs(i,1) - dflux(i) / grid%XYZ(i,1) * grid%JAC(i)
    end do

    ! ... momentum : d{rho U}/dt = -1/r d{(rho U U)r}/dr
    do i = 1, Nc
      flux(i) = state%cv(i,2) * state%cv(i,2) * state%dv(i,3) * grid%XYZ(i,1)
    end do
    call APPLY_OPERATOR(region, ng, input%iFirstDeriv, 1, flux, dflux, .FALSE.)
    do i = is, Nc
      state%rhs(i,2) = state%rhs(i,2) - dflux(i) /  grid%XYZ(i,1) * grid%JAC(i)
    end do

    ! ... momentum : d{rho U}/dt = - d{p}/dr
    do i = 1, Nc
      flux(i) = state%dv(i,1) 
    end do
    call APPLY_OPERATOR(region, ng, input%iFirstDeriv, 1, flux, dflux, .FALSE.)
    do i = is, Nc
      state%rhs(i,2) = state%rhs(i,2) - dflux(i) * grid%JAC(i)
    end do

    ! ... Energy : d{rho E}/dt = -1/r d{(rho E + p) U r}/dr
    do i = 1, Nc
      flux(i) = (state%cv(i,3) + state%dv(i,1)) * state%cv(i,2) * state%dv(i,3) * grid%XYZ(i,1)
    end do
    call APPLY_OPERATOR(region, ng, input%iFirstDeriv, 1, flux, dflux, .FALSE.)
    do i = is, Nc
      state%rhs(i,3) = state%rhs(i,3) - dflux(i) /  grid%XYZ(i,1) * grid%JAC(i)
    end do

    ! ... Centerline
    if (is /= 1) then
      do i = 1, NPolyfit
        flux(i) = grid%XYZ(i,1)
        dflux(i) = state%cv(i,2) * state%dv(i,3)
      end do
      dudr_origin = polyfitvector(NPolyfit, flux, dflux)
      do i = 1, NPolyfit
        dflux(i) = state%cv(i,1)
      end do
      call polyfitscalar(NPolyfit, flux, dflux, rho_origin, dtmp)
      do i = 1, NPolyfit
        dflux(i) = state%dv(i,1)
      end do
      call polyfitscalar(NPolyfit, flux, dflux, p_origin, dtmp)
      do i = 1, NPolyfit
        dflux(i) = state%cv(i,3)
      end do
      call polyfitscalar(NPolyfit, flux, dflux, rhoE_origin, dtmp)

      state%rhs(1,1) = -2.0_8 * rho_origin * dudr_origin
      state%rhs(1,2) =  0.0_8
      state%rhs(1,3) = -2.0_8 * (rhoE_origin + p_origin) * dudr_origin
    end if

    ! ... return if inviscid
    if (input%RE <= 0.0_rfreal) then
      deallocate(flux,dflux)
      return
    end if

    ! ... VISCOUS TERMS
    ! ... storage
    allocate(Vr(Nc), Ve(Nc), Phi(Nc), dudr(Nc), d2udr2(Nc), dTdr(Nc), d2Tdr2(Nc))
    allocate(dmudr(Nc), dlambdadr(Nc), dkdr(Nc))

    ! ... dudr & d2udr2
    do i = 1, Nc
      flux(i) = state%cv(i,2) * state%dv(i,3)
    end do
    call FIRST_AND_SECOND_DERIV(region, ng, 1, 1, flux, dudr, d2udr2, 1.0_8, .FALSE.)

    ! ... dTdr & d2Tdr2
    do i = 1, Nc
      flux(i) = state%dv(i,2)
    end do
    call FIRST_AND_SECOND_DERIV(region, ng, 1, 1, flux, dTdr, d2Tdr2, 1.0_8, .FALSE.)

    ! ... dmudr
    do i = 1, Nc
      flux(i) = state%tv(i,1)
    end do
    call APPLY_OPERATOR(region, ng, input%iFirstDeriv, 1, flux, dflux, .FALSE.)
    do i = 1, Nc
      dmudr(i) = dflux(i) * grid%JAC(i)
    end do

    ! ... dlambdadr
    do i = 1, Nc
      flux(i) = state%tv(i,2)
    end do
    call APPLY_OPERATOR(region, ng, input%iFirstDeriv, 1, flux, dflux, .FALSE.)
    do i = 1, Nc
      dlambdadr(i) = dflux(i) * grid%JAC(i)
    end do

    ! ... dkdr
    do i = 1, Nc
      flux(i) = state%tv(i,3)
    end do
    call APPLY_OPERATOR(region, ng, input%iFirstDeriv, 1, flux, dflux, .FALSE.)
    do i = 1, Nc
      dkdr(i) = dflux(i) * grid%JAC(i)
    end do

    ! ... compute Vr = (1/r)d{r tau_{rr}}{dr}
    do i = is, Nc
      Vr(i) = (2.0_8 * state%tv(i,1) + state%tv(i,2)) * d2udr2(i) 
      Vr(i) = Vr(i) + (2.0_8 * (state%tv(i,1) + state%tv(i,2)) / grid%XYZ(i,1) + 2.0_8 * dmudr(i) + dlambdadr(i)) * dudr(i)
      Vr(i) = Vr(i) + dlambdadr(i) * state%cv(i,2) * state%dv(i,3) / grid%XYZ(i,1)
    end do
    if (is /= 1) Vr(1) = 0.0_8

    ! ... compute Ve = -(1/r)d{r q}/dr
    do i = is, Nc
      Ve(i) = ( (dkdr(i) + state%tv(i,3)/grid%XYZ(i,1))*dTdr(i) + state%tv(i,3)*d2Tdr2(i) )
    end do
    if (is /= 1) then
      do i = 1, NPolyfit
        flux(i) = grid%XYZ(i,1)
        dflux(i) = state%dv(i,2)
      end do
      call polyfitscalar(NPolyfit, flux, dflux, dtmp, d2Tdr2_origin)
      Ve(1) = 4.0_8 * state%tv(1,3) * d2Tdr2_origin
      ! Ve(1) = 2.0_8 * state%tv(1,3) * d2Tdr2(1)
    end if

    ! ... compute Phi = tau_{rr} du/dr
    do i = is, Nc
      Phi(i) = ( (2.0_8 * state%tv(i,1) + state%tv(i,2)) * dudr(i) &
                 + state%tv(i,2) * state%cv(i,2) * state%dv(i,3) / grid%XYZ(i,1) ) * dudr(i)
    end do
    if (is /= 1) then
      Phi(1) = 4.0_8 * (state%tv(1,1) + state%tv(1,2)) * dudr_origin * dudr_origin
    end if

    ! ... update equations
    do i = 1, Nc
      state%rhs(i,2) = state%rhs(i,2) + Vr(i) * state%REinv
      state%rhs(i,3) = state%rhs(i,3) + state%cv(i,2) * state%dv(i,3) * Vr(i) * state%REinv &
                                      + Ve(i) * state%REinv * state%PRinv &
                                      + Phi(i) * state%REinv
    end do
   
    deallocate(Vr, Ve, Phi, dudr, d2udr2, dTdr, d2Tdr2)
    deallocate(dmudr, dlambdadr, dkdr)
    deallocate(flux, dflux)

    return

  end subroutine Cyl1D_RHS

  !
  ! Also includes SAT boundary conditions of Kreiss, Nordstrom, Carpenter, ...
  !
  subroutine Cyl1D_BC(region, ng, flags)

    USE ModGlobal
    USE ModDataStruct
    USE ModMetrics
    USE ModEOS
    USE ModDeriv
    USE ModIO
    USE ModMPI

    implicit none

    TYPE (t_region), POINTER :: region
    INTEGER :: ng
    INTEGER :: flags

    ! ... local variables
    type(t_patch), pointer :: patch
    type(t_grid), pointer :: grid
    type(t_mixt), pointer :: state
    type(t_mixt_input), pointer :: input
    real(rfreal), pointer :: gvec(:), SC(:), Pinv(:,:), Pfor(:,:), Tmat(:,:), Tinv(:,:), cvPredictor(:), rhsPredictor(:)
    real(rfreal), pointer :: cons_inviscid_flux(:,:), L_vector(:), new_normal_flux(:)
    integer :: ND, Nc, N(MAX_ND), npatch, i, j, l0, k, ii, jj, l1, l2, isign, lp, kk
    real(rfreal) :: UU(MAX_ND), norm(MAX_ND), mag_norm, tang(MAX_ND-1,MAX_ND)
    real(rfreal) :: mag_tang, constant, del, eta, rel_pos(MAX_ND), dsign, denom
    integer :: normDir, tangDir(MAX_ND,MAX_ND-1), Np(MAX_ND)
    real(rfreal) :: d(5), wave_amp(3), wave_spd(3), spd_snd
    real(rfreal) :: un, ut(MAX_ND-1)
    real(rfreal) :: norm_vec(MAX_ND), tang_vec(2,MAX_ND), fvec(MAX_ND,2)
    real(rfreal) :: metricInverse(MAX_ND,MAX_ND), invProj(MAX_ND, MAX_ND)
    real(rfreal) :: sponge_amp, bndry_q(MAX_ND+2), bndry_p(MAX_ND+2), bndry_rhs(MAX_ND+2)
    integer :: sgn, handedness(MAX_ND)
    integer, pointer :: inorm(:)
    real(rfreal) :: dundn, drdn, dpdn, drundn, dut1dn, dut2dn, sigma
    real(rfreal) :: bndry_norm(MAX_ND), p_inf, u_inf, du_inf, un_old, metric_fac, T_stag, T_inf, dT_inf, theta, P_stag
    real(rfreal) :: charVar(MAX_ND+2), primVar(MAX_ND+2), dsgn
    real(rfreal) :: V_wall, V_wall_vec(MAX_ND), V_wall_target, V_wall_target_vec(MAX_ND), A_wall, drhodn, dp_inf, T_wall
    real(rfreal) :: Uhat_alpha(MAX_ND), alpha_x(MAX_ND,MAX_ND), alpha_t(MAX_ND)
    real(rfreal) :: x_alpha(MAX_ND,MAX_ND), primVar_RHS(MAX_ND+2), M2(2,2), M3(3,3)
    real(rfreal) :: SPVOL, RHO, UX, UY, UZ, PRESSURE, XI_X, XI_Y, XI_Z, SPD_SND_INV, KE, ENTHALPY, MACH2, GAMMA, V_DOT_XI
    real(rfreal) :: XI_X_TILDE, XI_Y_TILDE, XI_Z_TILDE, epsilon, omega, dUdt_inflow, dTdt_inflow, Mi, Ui, Ti, u_inflow, T_inflow
    real(rfreal) :: w_of_t, dwdt, t0, wi, w_inflow, v_inflow, dVdt_inflow, vi, dWdt_inflow
    real(rfreal) :: radNozz, Tc, u0, r0, coefVel, sigmaI1, sigmaI2, sigma_inter
    real(rfreal) :: junk ! JKim 11/2007
    real(rfreal) :: RHO_TARGET, UX_TARGET, UY_TARGET, UZ_TARGET, PRESSURE_TARGET, U_wall, UVWhat(MAX_ND), UVW(MAX_ND)
    real(rfreal) :: alpha, beta, phi2, ucon, PRESSURE2, bndry_h
    real(rfreal) :: p_in, p_out, sig_v, sigR(MAX_ND+2), sigL(MAX_ND+2), tmp(MAX_ND+2), tmp2(MAX_ND+2), sigma_scale
    real(rfreal) :: dudr, dpdr, drdr, dudt, dpdt, drdt, q1, q2, r1, r2

    ! ... simplicity
    input => region%input
    ND = input%ND

    do npatch = 1, region%nPatches

      patch => region%patch(npatch)
      grid => region%grid(patch%gridID)
      state => region%state(patch%gridID)
      normDir = abs(patch%normDir)
      sgn = normDir / patch%normDir
      dsgn = dble(sgn)

      Nc = grid%nCells
      N  = 1; Np = 1;
      do j = 1, grid%ND
        N(j)  =  grid%ie(j) -  grid%is(j) + 1
        Np(j) = patch%ie(j) - patch%is(j) + 1
      end do;

      if (patch%bcType == NSCBC_OUTFLOW_PERFECT_NONREFLECTION) then

        Do k = patch%is(3), patch%ie(3)
          Do j = patch%is(2), patch%ie(2)
            Do i = patch%is(1), patch%ie(1)

              l0 = (k- grid%is(3))* N(1)* N(2) + (j- grid%is(2))* N(1) +  i- grid%is(1)+1
              lp = (k-patch%is(3))*Np(1)*Np(2) + (j-patch%is(2))*Np(1) + (i-patch%is(1)+1)

              if ( grid%iblank(l0) == 0 ) CYCLE

              ! ... primitive variables
              GAMMA       = state%gv(l0,1)
              RHO         = state%cv(l0,1)
              SPVOL       = 1.0_rfreal / RHO
              PRESSURE    = state%dv(l0,1)
              UX          = SPVOL * state%cv(l0,2);
              SPD_SND     = sqrt(GAMMA * PRESSURE * SPVOL)
              SPD_SND_INV = 1.0_rfreal / SPD_SND

              ! ... derivatives
              if (patch%normDir < 0) then
                r1 = grid%XYZ(l0,1)
                r2 = grid%XYZ(l0-1,1)

                ! ... radial velocity
                q1 = state%cv(l0,2) * state%dv(l0,3)
                q2 = state%cv(l0-1,2) * state%dv(l0-1,3)
                dudr = (q1 - q2) / (r1 - r2)

                ! ... density
                q1 = state%cv(l0,1) 
                q2 = state%cv(l0-1,1) 
                drdr = (q1 - q2) / (r1 - r2)

                ! ... pressure
                q1 = state%dv(l0,1) 
                q2 = state%dv(l0-1,1) 
                dpdr = (q1 - q2) / (r1 - r2)

              end if

              ! ... wave speeds
              wave_spd(1) = UX - SPD_SND
              wave_spd(2) = UX
              wave_spd(3) = UX + SPD_SND

              ! ... wave amplitudes
              wave_amp(1:3) = 0.0_8
              if (wave_spd(1) > 0.0_8) wave_amp(1) = wave_spd(1) * (dpdr - RHO * SPD_SND * dudr)
              if (wave_spd(2) > 0.0_8) wave_amp(2) = wave_spd(2) * (dpdr - SPD_SND * SPD_SND * drdr)
              if (wave_spd(3) > 0.0_8) wave_amp(3) = wave_spd(3) * (dpdr + RHO * SPD_SND * dudr)

              ! ... update primative variables
              dpdt = -0.5_8 * (wave_amp(3) + wave_amp(1)) - RHO * SPD_SND * SPD_SND * UX / grid%XYZ(l0,1)
              dudt = -0.5_8 * SPVOL * SPD_SND_INV * (wave_amp(3) - wave_amp(1))
              drdt = SPD_SND_INV * SPD_SND_INV * (dpdt + wave_amp(2))

              ! ... update conservative varibles
              state%rhs(l0,1) = drdt
              state%rhs(l0,2) = RHO * dudt + UX * drdt
              state%rhs(l0,3) = dpdt / (GAMMA - 1.0_8) + RHO * UX * dudt + 0.5_8 * UX * UX * drdt

            End Do
          End Do
        End Do
      
      End If

      if (patch%bcType == SPONGE) then

        do k = 1, grid%vds(normDir)
          do j = patch%is(normDir), patch%ie(normDir)

            ! ... this point
            l0 = grid%vec_ind(k,j-grid%is(normDir)+1,normDir)

            ! ... distance from boundary to inner sponge start
            rel_pos(1:grid%ND) = patch%sponge_xs(k,1:grid%ND) - patch%sponge_xe(k,1:grid%ND)
            del = sqrt(dot_product(rel_pos(1:grid%ND),rel_pos(1:grid%ND)))

            ! ... distance from inner sponge start to point
            rel_pos(1:grid%ND) = patch%sponge_xs(k,1:grid%ND) - grid%XYZ(l0,1:grid%ND)
            eta = sqrt(dot_product(rel_pos(1:grid%ND),rel_pos(1:grid%ND))) / del

            sponge_amp = 0.0_rfreal
            if (eta .le. 1.0_rfreal) sponge_amp = (input%sponge_amp) * eta**(input%sponge_pow)

            if (grid%iblank(l0) /= 0) then
              state%rhs(l0,:) = state%rhs(l0,:) - sponge_amp * (state%cv(l0,:) - state%cvTarget(l0,:))
            else
              state%rhs(l0,:) = 0.0_rfreal
            end if

          end do
        end do

      end if

    end do

  End Subroutine Cyl1D_BC

  Subroutine PolyFitScalar(N, r, T, a, b)

    USE ModGlobal
   
    Implicit None

    Real(RFREAL), Pointer :: r(:), T(:)
    Real(RFREAL) :: S, Sx, Sxy, Sxx, Sy, a, b
    Integer :: i, N

    S = 0.0_8
    Sx = 0.0_8
    Sy = 0.0_8
    Sxy = 0.0_8
    Sxx = 0.0_8

    Do i = 1, N
      S = S + 1.0_8
      Sx = Sx + r(i)*r(i)
      Sy = Sy + T(i)
      Sxx = Sxx + r(i)*r(i)*r(i)*r(i)
      Sxy = Sxy + r(i)*r(i)*T(i)
    End Do

    a = (Sxx*Sy-Sx*Sxy)/(S*Sxx-Sx*Sx)
    b = (S*Sxy-Sx*Sy)/(S*Sxx-Sx*Sx)
    Return

  End Subroutine PolyFitScalar

  Function PolyFitVector(N, r, T)

    USE ModGlobal
   
    Implicit None

    Real(RFREAL), Pointer :: r(:), T(:)
    Real(RFREAL) :: S, Sx, Sxy, Sxx, Sy, PolyFitVector, r2, r4, r6
    Integer :: i, N

    S = 0.0_8
    Sx = 0.0_8
    Sy = 0.0_8
    Sxy = 0.0_8
    Sxx = 0.0_8

    Do i = 1, N
      r2 = r(i)*r(i)
      r4 = r2 * r2
      r6 = r4 * r2
      S = S + r2
      Sx = Sx + r4
      Sy = Sy + r(i)*T(i)
      Sxx = Sxx + r6
      Sxy = Sxy + r2*r(i)*T(i)
    End Do

    PolyFitVector = (Sxx*Sy-Sx*Sxy)/(S*Sxx-Sx*Sx)
    Return

  End Function PolyFitVector
  
  
END MODULE ModCyl1D
