! Copyright (c) 2015, University of Illinois at Urbana-Champaign, XPACC
! License: MIT, http://opensource.org/licenses/MIT
!-----------------------------------------------------------------------
!
! ModIBLANK.f90
!
! Defines the IBLANK Mask definition
!-----------------------------------------------------------------------

MODULE MODIBLANK

    IMPLICIT NONE
    SAVE

!	.... IBLANK definition taken from Rocsmash ...
!	static const int VOID				 = 0; 	/**< @brief Alias to Dead nodes. */
!	static const int NORMAL 			 = 1; 	/**< @brief Nodes in the normal state can both donate and receive.  */
!	static const int STRICTDONOR 	 	 = 2; 	/**< @brief Nodes in the this state can only donate. */
!	static const int STRICTRCV 			 = 3; 	/**< @brief Nodes in this state can be receivers, but they cannot be donors. */
!	static const int IGNORE				 = 4; 	/**< @brief Nodes in this state are forced to be ignored, cannot donate, or receive. */
!	static const int OUTERBNDRY			 = 5; 	/**< @brief Nodes in the outer-boundary. */
!	static const int PERIODICBNDRY	 	 = 6; 	/**< @brief Denotes a periodic boundary. */
!	static const int ORPHAN				 = 7; 	/**< @brief Denotes orphan nodes. */
!	static const int INTERNALBNDRY  	 = 8; 	/**< @brief Denotes a partition boundary. */
!	static const int GHOST				 = 9;	  /**< @brief Denotes a ghost node. */
!	static const int UNDEFINED			 = 999; /**< @brief Denotes that the IBLANK is undefined. */

    INTEGER, PARAMETER :: void_iblnk                = 0
    INTEGER, PARAMETER :: normal_iblnk              = 1
    INTEGER, PARAMETER :: strictdonor_iblnk         = 2
    INTEGER, PARAMETER :: strictrcv_iblnk           = 3
    INTEGER, PARAMETER :: ignore_iblnk              = 4
    INTEGER, PARAMETER :: outerbndry_iblnk          = 5
    INTEGER, PARAMETER :: peridicbndry_iblnk        = 6
    INTEGER, PARAMETER :: orphan_iblnk              = 7
    INTEGER, PARAMETER :: internalbndry_iblnk       = 8
    INTEGER, PARAMETER :: ghost_iblnk               = 9
    INTEGER, PARAMETER :: sat_block_interface_iblnk = 10
    INTEGER, PARAMETER :: incestious_iblnk          = 11
    INTEGER, PARAMETER :: cutsurface_iblnk          = 12
    INTEGER, PARAMETER :: active_iblnk              = 13
    INTEGER, PARAMETER :: inactive_iblnk            = 14
    INTEGER, PARAMETER :: undefined_iblnk           = 999

END MODULE MODIBLANK
