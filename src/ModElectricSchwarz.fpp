! Copyright (c) 2015, University of Illinois at Urbana-Champaign, XPACC
! License: MIT, http://opensource.org/licenses/MIT
!
!-----------------------------------------------------------------------
module ModElectricSchwarz
CONTAINS

  subroutine ElectricSchwarzSetup(region)

    USE ModDataStruct
    USE ModElectric
    USE ModMPI
    USE ModInterp
    implicit none

    type(t_region), pointer :: region
    integer :: ng
    real(rfreal) :: time

    do ng = 1, region%nGrids
      call ElectricSetup(region, ng)
    end do

    do ng = 1, region%nGrids
      time = MPI_WTIME()
      call ElectricSchwarzFindInterpBoundaries(region, ng)
      region%mpi_timings(ng)%efield_processing = region%mpi_timings(ng)%efield_processing + (MPI_WTIME() - time)
    end do

    call Setup_Interpolated_Phi(region)

  end subroutine ElectricSchwarzSetup

  subroutine ElectricSchwarzSolve(region)

    USE ModDataStruct
    USE ModDeriv
    USE ModElectric
    USE ModElectricState
    USE ModInterp
    USE ModMPI
    implicit none

    type(t_region), pointer :: region
    integer :: niter, ng

    type(t_electric), pointer :: electric
    real(rfreal) :: time1, time2

    ! algorithm:
    !   * solve on each grid
    !   - interpolate
    !   - update boundary conditions on each grid
    !   - goto (*)

    do ng = 1, region%nGrids

      electric => region%electric(ng)
      time1 = MPI_WTIME()

      if (electric%grid_has_efield) then

        if (.not. electric%whole_grid .and. electric%rank_has_efield) then
          call CopyWholeToSubset(region, ng, electric%dcoef, electric%dcoef_subset)
          call CopyWholeToSubset(region, ng, electric%jump, electric%jump_subset)
        end if

      end if

      region%mpi_timings(ng)%efield_solve = region%mpi_timings(ng)%efield_solve + (MPI_WTIME() - time1)

    end do

    do niter=1,region%input%ef_schwarz_iters

        do ng=1,region%nGrids
          time1 = MPI_WTIME()
          call ElectricSolveLocal(region, ng)
          region%mpi_timings(ng)%efield_solve = region%mpi_timings(ng)%efield_solve + (MPI_WTIME() - time1)
        end do

        do ng = 1, region%nGrids
          electric => region%electric(ng)
          time1 = MPI_WTIME()
          if (electric%grid_has_efield) then
            if (.not. electric%whole_grid .and. electric%rank_has_efield) then
              call CopySubsetToWhole(region, ng, electric%phi_subset, electric%phi)
            end if
          end if
          region%mpi_timings(ng)%efield_solve = region%mpi_timings(ng)%efield_solve + (MPI_WTIME() - time1)
        end do

        time1 = MPI_WTIME()
        call Exchange_Interpolated_Phi(region)
        time2 = MPI_WTIME()
        do ng = 1,region%nGrids
          region%mpi_timings(ng)%efield_solve = region%mpi_timings(ng)%efield_solve + (time2 - time1)
        end do

        do ng = 1,region%nGrids
          time1 = MPI_WTIME()
          call ElectricSchwarzUpdateBCs(region, ng)
          region%mpi_timings(ng)%efield_solve = region%mpi_timings(ng)%efield_solve + (MPI_WTIME() - time1)
        end do

    end do

    do ng = 1, region%nGrids

      electric => region%electric(ng)
      time1 = MPI_WTIME()

      if (electric%grid_has_efield) then

        call ElectricExtendSubset(region, ng)

        call Ghost_Cell_Exchange_Box(region, ng, electric%phi)

        call ElectricGradient(region, ng)

      end if

      region%mpi_timings(ng)%efield_solve = region%mpi_timings(ng)%efield_solve + (MPI_WTIME() - time1)

    end do

  end subroutine ElectricSchwarzSolve

  subroutine ElectricSchwarzFindInterpBoundaries(region, ng)

    USE ModDataStruct
    USE ModGlobal
    USE ModElectric
    implicit none

    type(t_region), pointer :: region
    type(t_patch), pointer :: patch
    type(t_epatch), pointer :: epatch
    type(t_grid), pointer :: grid
    type(t_electric), pointer :: electric
    integer :: i, j, k
    integer :: l0
    integer, dimension(MAX_ND) :: N
    integer :: ng, nPatch

    grid => region%grid(ng)
    electric => region%electric(ng)

    if (electric%grid_has_efield) then
      if (electric%rank_has_efield) then

        electric%interp_mask = .false.

        do nPatch = 1, region%nPatches
          patch => region%patch(nPatch)
          epatch => region%EFpatch(nPatch)
          if (patch%gridID == ng) then
            N = 1
            if (electric%whole_grid) then
              do j = 1, grid%ND
                N(j) = grid%ie(j)-grid%is(j)+1
              end do
            else
              do j = 1, electric%subset_ND
                N(j) = electric%subset_ie(j)-electric%subset_is(j)+1
              end do
            end if
            if (patch%bcType == EF_SCHWARZ) then
              do k = epatch%is(3), epatch%ie(3)
                do j = epatch%is(2), epatch%ie(2)
                  do i = epatch%is(1), epatch%ie(1)
                    if (electric%whole_grid) then
                      l0 = 1 + (i-grid%is(1)) + (j-grid%is(2))*N(1) + (k-grid%is(3))*N(1)*N(2)
                    else
                      l0 = electric%subset_to_whole(1+(i-electric%subset_is(1)) + &
                        (j-electric%subset_is(2))*N(1) + (k-electric%subset_is(3))*N(1)*N(2))
                    end if
                    electric%interp_mask(l0) = .true.
                  end do
                end do
              end do
            end if
          end if
        end do

      end if
    end if

  end subroutine ElectricSchwarzFindInterpBoundaries

  subroutine ElectricSchwarzUpdateBCs(region, ng)

    USE ModDataStruct
    USE ModGlobal
    USE ModElectric
    implicit none

    type(t_region), pointer :: region
    type(t_patch), pointer :: patch
    type(t_epatch), pointer :: epatch
    type(t_grid), pointer :: grid
    type(t_electric), pointer :: electric
    type(t_mixt_input), pointer :: input
    integer :: i, j, k
    integer :: l0, l1
    integer, dimension(MAX_ND) :: N
    integer :: ng, nPatch

    grid => region%grid(ng)
    electric => region%electric(ng)
    input => grid%input

    if (electric%grid_has_efield) then
      if (electric%rank_has_efield) then

        do nPatch = 1, region%nPatches
          patch => region%patch(nPatch)
          epatch => region%EFpatch(nPatch)
          if (patch%gridID == ng) then
            N = 1
            if (electric%whole_grid) then
              do j = 1, grid%ND
                N(j) = grid%ie(j)-grid%is(j)+1
              end do
            else
              do j = 1, electric%subset_ND
                N(j) = electric%subset_ie(j)-electric%subset_is(j)+1
              end do
            end if
            if (patch%bcType == EF_SCHWARZ) then
              l1 = 1
              do k = epatch%is(3), epatch%ie(3)
                do j = epatch%is(2), epatch%ie(2)
                  do i = epatch%is(1), epatch%ie(1)
                    if (electric%whole_grid) then
                      l0 = 1 + (i-grid%is(1)) + (j-grid%is(2))*N(1) + (k-grid%is(3))*N(1)*N(2)
                    else
                      l0 = electric%subset_to_whole(1+(i-electric%subset_is(1)) + &
                        (j-electric%subset_is(2))*N(1) + (k-electric%subset_is(3))*N(1)*N(2))
                    end if
                    if (grid%iblank(l0) /= 0) then
                      epatch%dirichlet(l1) = electric%phi(l0)
                    else
                      epatch%dirichlet(l1) = input%ef_plasma_potential/input%ef_voltage_scale
                    end if
                    l1 = l1 + 1
                  end do
                end do
              end do
            end if
          end if
        end do

      end if
    end if

  end subroutine ElectricSchwarzUpdateBCs

end module ModElectricSchwarz
