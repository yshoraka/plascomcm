#ifndef EF_LEVEL_H
#define EF_LEVEL_H

#include <petscvec.h>

#include "ef_metric.h"

/**
 * Data structure containing data
 * needed on each MG level (previously an array because PDE was discretized on each level)
 */
typedef struct {
	Vec dcoef;           /** Permittivity */
	Vec ldcoef;          /** Local vector for dcoef */
	Vec nscale;        /** Neumann scaling for symmetry */
	Vec add_cont;      /** Additive contribution to rhs */
	Vec ladd_cont;     /** Local vector for add_cont */
	Vec pm;            /** Pointwise multiplication for add_cont */
	ef_metric *metric;
	Vec bcoef;
	Vec lbcoef;
	DM dm;
} ef_level;


#endif
