module ModElectricBC
contains

  !> Initializes Efield boundary conditions.
  !!
  !! Allocates and sets initial values for the Efield boundary conditions.
  !! Passes pointers to the boundary condition arrays to the C solver.
  subroutine ElectricInitBC(region, ng)

    USE ModDataStruct
    USE ModGlobal
    USE ModElectricInterface
    use iso_c_binding
    implicit none

    type(t_region), pointer :: region
    type(t_grid), pointer :: grid
    type(t_electric), pointer :: electric
    type(t_patch), pointer :: patch
    type(t_epatch), pointer :: epatch
    integer :: ng, nPatch, N, j
    integer, dimension(MAX_ND) :: dirs

    grid => region%grid(ng)
    electric => region%electric(ng)

    if (electric%grid_has_efield) then
      if (electric%rank_has_efield) then

        if (.not. electric%whole_grid) then
          dirs = electric%subset_dirs
        end if

        do nPatch = 1, region%nPatches
           patch => region%patch(nPatch)
           if (patch%gridID == ng) then
              epatch => region%EFpatch(nPatch)
              if ( patch%bcType == EF_DIRICHLET_G .or.&
                   patch%bcType == EF_DIRICHLET_P .or.&
                   patch%bcType == EF_NEUMANN     .or.&
                   patch%bcType == EF_DIELECTRIC  .or.&
                   patch%bcType == EF_SCHWARZ) then
                 if (electric%whole_grid) then
                   do j=1, ubound(epatch%is,1)
                      epatch%is(j) = max(patch%is(j), grid%is_unique(j))
                      epatch%ie(j) = min(patch%ie(j), grid%ie_unique(j))
                   end do
                 else
                   do j=1, ubound(epatch%is,1)
                     epatch%is(j) = max(patch%is(dirs(j)), max(electric%subset_start(dirs(j)), &
                       grid%is_unique(dirs(j)))) - electric%subset_start(dirs(j)) + 1
                     epatch%ie(j) = min(patch%ie(dirs(j)), min(electric%subset_end(dirs(j)), &
                       grid%ie_unique(dirs(j)))) - electric%subset_start(dirs(j)) + 1
                   end do
                 end if
                 N = (epatch%ie(1) - epatch%is(1) + 1)*(epatch%ie(2) - epatch%is(2) + 1)
                 if (grid%ND == 3) N = N*(epatch%ie(3) - epatch%is(3) + 1)
                 if (patch%bcType == EF_DIRICHLET_G .or.&
                     patch%bcType == EF_SCHWARZ) then
                    allocate(epatch%dirichlet(N))
                    epatch%dirichlet(:) = 0.0
                 else if (patch%bcType == EF_DIRICHLET_P) then
                    allocate(epatch%dirichlet(N))
                    epatch%dirichlet(:) = 1.0
                 else
                    ! dummy pointer
                    allocate(epatch%dirichlet(1))
                 end if
                 epatch%bcType = patch%bcType
                 if (electric%whole_grid) then
                   epatch%normDir = patch%normDir
                 else
                   do j=1, ubound(epatch%is,1)
                     if (abs(patch%normDir) == dirs(j)) then
                       epatch%normDir = merge(j, -j, patch%normDir > 0)
                       exit
                     end if
                   end do
                 end if
                 if (N > 0) then
                    call ef_set_patch(region%electric(ng)%solver, epatch%is, epatch%ie,&
                                      epatch%bcType, epatch%normDir,&
                                      c_loc(epatch%dirichlet(1)))
                 end if
              end if
           end if
        end do

      end if
    end if

  end subroutine ElectricInitBC


  subroutine ElectricCleanupBC(region, ng)

    USE ModDataStruct
    USE ModElectricInterface
    implicit none

    type(t_region), pointer :: region
    type(t_electric), pointer :: electric
    integer :: ng
    type(t_patch), pointer :: patch
    type(t_epatch), pointer :: epatch
    integer :: nPatch, i

    electric => region%electric(ng)

    if (electric%grid_has_efield) then
      if (electric%rank_has_efield) then

        do nPatch = 1, region%nPatches
           patch => region%patch(nPatch)
           if (patch%gridID == ng) then
              epatch => region%EFpatch(nPatch)
              if (patch%bcType == EF_DIRICHLET_G .or.&
                   patch%bcType == EF_DIRICHLET_P .or.&
                   patch%bcType == EF_NEUMANN .or. &
                   patch%bcType == EF_SCHWARZ) then
                 deallocate(epatch%dirichlet)
               else if (patch%bcType == EF_DIELECTRIC) then
                 deallocate(epatch%charge, epatch%chargeOld)
                 deallocate(epatch%chargeRHS)
                 deallocate(epatch%chargeStageRHS)
              end if
           end if
        end do

      end if
    end if

  end subroutine ElectricCleanupBC

end module ModElectricBC
