module ModElectric

  integer, parameter :: EF_WHOLE = 1
  integer, parameter :: EF_SIMPLE_SUBSET = 2
  integer, parameter :: EF_AXISYMMETRIC_SUBSET = 3

CONTAINS

  subroutine ElectricRegionSetup(region)

    USE ModDataStruct
    implicit none
#include "petsc_fortran.h"

    type(t_region), pointer :: region
    type(t_grid), pointer :: grid
    type(t_electric), pointer :: electric
    type(t_mixt_input), pointer :: input
    integer :: alloc_stat, ng
    PetscErrorCode :: ierr

    allocate(region%electric(region%nGrids))
    allocate(region%EFpatch(region%nPatches))

    call PetscInitialize("PETScOptions.txt", ierr)
    CHKERRQ(ierr)

    do ng = 1, region%nGrids
      grid => region%grid(ng)
      electric => region%electric(ng)
      input => grid%input
      allocate(electric%phi_data_ptr(grid%nCells,1))
      electric%phi_data_ptr = input%ef_plasma_potential/input%ef_voltage_scale
      nullify(electric%phi)
      nullify(electric%rhs)
      nullify(electric%bcoef)
      nullify(electric%dcoef)
      nullify(electric%jump)
      nullify(electric%gradphi)
#ifdef AXISYMMETRIC
      nullify(electric%gradphi_pole)
#endif
      nullify(electric%phi_subset)
      nullify(electric%rhs_subset)
      nullify(electric%bcoef_subset)
      nullify(electric%dcoef_subset)
      nullify(electric%jump_subset)
      electric%grid_has_efield = .false.
    end do

  end subroutine ElectricRegionSetup


  !> Initialize data for PETSc efield solver.
  !!
  !! Gather grid partitining data to create a DMDA object to be used
  !! by PETSc solver. Initialize PETSc vectors to be associated with
  !! this topology.
  subroutine ElectricSetup(region, ng)

    USE ModDataStruct
    USE ModElectricBC
    USE ModElectricState
    USE ModElectricInterface
    USE ModMPI, only : graceful_exit
    use iso_c_binding
    implicit none
#include "petsc_fortran.h"

    type(t_region), pointer :: region
    integer :: ng

    type(t_mixt_input), pointer :: input
    type(t_grid), pointer :: grid
    integer :: i
    integer :: io_err
    integer :: strbuf_io_err
    character(len=256) :: strbuf
    integer :: grid_id
    integer :: efield_type
    integer :: is, ie, js, je, ks, ke
    integer, dimension(MAX_ND) :: subset_start, subset_end
    real(rfreal), dimension(MAX_ND) :: axisymmetric_origin, axisymmetric_axis
    real(rfreal) :: time

    integer, parameter :: ef_subset_unit = 454860

    input => region%input
    grid => region%grid(ng)
    time = MPI_WTIME()

    if (.not. input%ef_subset) then

      call ElectricSetupWhole(region, ng)

    else

      open (unit=ef_subset_unit, file=trim(input%ef_subset_fname), status='old', iostat=io_err)
      if (io_err /= 0) then
        call graceful_exit(region%myrank, 'ERROR: Could not find/open electric field subset ' // &
          'file "' // trim(input%ef_subset_fname) // '".')
      end if

      ! Find the entry in the efield subset file corresponding to the current grid (if there is one)
      grid_id = 0
      io_err = FALSE
      do while (io_err == FALSE)
        read (ef_subset_unit,'(a)',iostat=io_err) strbuf
        if ((io_err == FALSE) .AND. (strbuf(1:1) /= '#') .AND. (len_trim(strbuf) /= 0)) Then
          read (strbuf,*,iostat=strbuf_io_err) grid_id, efield_type
          if (strbuf_io_err /= 0) then
            call graceful_exit(region%myrank, 'ERROR: Unable to parse entry in electric field ' // &
              'subset file.')
          end if
          if (grid_id == grid%iGridGlobal) then
            exit
          end if
        end if
      end do

      close(ef_subset_unit)

      if (grid_id == grid%iGridGlobal) then

        select case (efield_type)
        case (EF_WHOLE)
          call ElectricSetupWhole(region, ng)
        case (EF_SIMPLE_SUBSET)
          read (strbuf,*,iostat=strbuf_io_err) grid_id, efield_type, is, ie, js, je, ks, ke
          if (strbuf_io_err /= 0) then
            call graceful_exit(region%myrank, 'ERROR: Unable to parse entry in electric field ' // &
              'subset file.')
          end if
          subset_start = [is,js,ks]
          subset_start = merge(subset_start, grid%GlobalSize+subset_start+1, subset_start > 0)
          subset_end = [ie,je,ke]
          subset_end = merge(subset_end, grid%GlobalSize+subset_end+1, subset_end > 0)
          call ElectricSetupSubset(region, ng, subset_start, subset_end)
        case (EF_AXISYMMETRIC_SUBSET)
          read (strbuf,*,iostat=strbuf_io_err) grid_id, efield_type, is, ie, js, je, ks, ke, &
            axisymmetric_origin, axisymmetric_axis
          if (strbuf_io_err /= 0) then
            call graceful_exit(region%myrank, 'ERROR: Unable to parse entry in electric field ' // &
              'subset file.')
          end if
          subset_start = [is,js,ks]
          subset_start = merge(subset_start, grid%GlobalSize+subset_start+1, subset_start > 0)
          subset_end = [ie,je,ke]
          subset_end = merge(subset_end, grid%GlobalSize+subset_end+1, subset_end > 0)
          call ElectricSetupSubset(region, ng, subset_start, subset_end, axisymmetric=.true., &
            axisymmetric_origin=axisymmetric_origin, axisymmetric_axis=axisymmetric_axis)
        end select

      end if

    end if

    region%mpi_timings(ng)%efield_processing = region%mpi_timings(ng)%efield_processing + (MPI_WTIME() - time)

  end subroutine ElectricSetup


  !> Initialize data for PETSc efield solver (entire grid)
  subroutine ElectricSetupWhole(region, ng)

    USE ModDataStruct
    USE ModElectricBC
    USE ModElectricState
    USE ModElectricInterface
    use iso_c_binding
    implicit none
#include "petsc_fortran.h"

    type(t_region), pointer :: region
    type(t_grid), pointer :: grid
    type(t_electric), pointer :: electric
    type(c_ptr), pointer :: solver

    integer(c_int) :: nLocal(3), nProcs(3), nGlobal(3), stride(3), offset(3)
    integer(c_int) :: ef_periodic(3), cartCoords(3), is(3), ie(3)
    integer(c_int) :: ND, nCells, periodicStorage
    integer(c_int) :: axisymmetric_flag
    integer :: i, ng
    PetscErrorCode :: ierr

    solver => region%electric(ng)%solver
    grid => region%grid(ng)
    electric => region%electric(ng)

    electric%grid_has_efield = .true.
    electric%whole_grid = .true.
    electric%rank_has_efield = .true.

#ifdef AXISYMMETRIC
    axisymmetric_flag = 1
#else
    axisymmetric_flag = 0
#endif

    ef_periodic(:) = 0
    ef_periodic(:grid%ND) = grid%periodic(:grid%ND)
    periodicStorage = grid%periodicStorage
    nGlobal(:grid%ND) = grid%GlobalSize(:grid%ND)
    nProcs(:grid%ND) = grid%cartDims(:grid%ND)
    cartCoords(:grid%ND) = grid%cartCoords(:grid%ND)
    is(:grid%ND) = grid%is(:grid%ND)
    ie(:grid%ND) = grid%ie(:grid%ND)
    ND = grid%ND
    nCells = grid%nCells
    do i=1,grid%ND
       nLocal(i) = (grid%ie(i) - grid%nGhostRHS(i,2)) - &
            (grid%is(i) + grid%nGhostRHS(i,1)) + 1
       stride(i) = grid%ie(i) - grid%is(i) + 1
       offset(i) = grid%nGhostRHS(i,1)
    end do

    call ef_init(solver, grid%Comm, nGlobal, nProcs, nLocal,&
                 offset, stride,&
                 cartCoords, ef_periodic, periodicStorage,&
                 ND, ng, axisymmetric_flag, ierr)
    CHKERRQ(ierr)
    call ef_set_grid(solver, is, ie, nCells, c_loc(grid%XYZ(1,1)), ierr)
    CHKERRQ(ierr)

    call ElectricInitState(region, ng)
    call ElectricInitBC(region, ng)

    call ef_setup_op(solver, ierr)
    CHKERRQ(ierr)

  end subroutine ElectricSetupWhole


  !> Initialize data for PETSc efield solver (subset of grid)
  !> Use arguments subset_start and subset_end to set up the efield on a subset
  !> of a grid (these are global indices)
  !> Optionally also use subset_dirs to choose the ordering of axes in this subset
  !> (e.g., for a 2D subset of a 3D grid, subset_dirs = [2,3,1] uses y (2) and z (3) for the
  !> subset coordinates and x (1) as the slice plane)
  subroutine ElectricSetupSubset(region, ng, subset_start, subset_end, subset_dirs, &
    axisymmetric, axisymmetric_origin, axisymmetric_axis)

    USE ModDataStruct
    USE ModElectricBC
    USE ModElectricState
    USE ModElectricInterface
    USE ModGlobal
    USE ModMPI, only : graceful_exit, numproc, mycomm
    use iso_c_binding
    implicit none
#include "petsc_fortran.h"

    type(t_region), pointer :: region
    integer :: ng
    integer, dimension(MAX_ND) :: subset_start, subset_end
    integer, dimension(MAX_ND), optional :: subset_dirs
    logical, optional :: axisymmetric
    real(rfreal), dimension(MAX_ND), optional :: axisymmetric_origin
    real(rfreal), dimension(MAX_ND), optional :: axisymmetric_axis

    type(t_grid), pointer :: grid
    type(t_electric), pointer :: electric
    type(c_ptr), pointer :: solver
    integer, dimension(MAX_ND) :: dirs
    logical :: axisymmetric_
    real(rfreal), dimension(MAX_ND) :: axisymmetric_origin_
    real(rfreal), dimension(MAX_ND) :: axisymmetric_axis_
    integer :: l_lower, l_middle
    real(rfreal), dimension(MAX_ND) :: axis
    real(rfreal), dimension(MAX_ND) :: xyz_middle
    real(rfreal), dimension(MAX_ND) :: normal
    real(rfreal), dimension(MAX_ND) :: ivec, jvec
    real(rfreal), dimension(MAX_ND) :: ivec_cross_jvec
    logical :: right_handed
    integer(c_int) :: nLocal(3), nProcs(3), nGlobal(3), stride(3), offset(3)
    integer(c_int) :: ef_periodic(3), cartCoords(3), is(3), ie(3)
    integer(c_int) :: ND, nCells, periodicStorage
    integer :: axisymmetric_flag
    integer :: i, j, k
    logical :: contains_subset_start, contains_subset_end
    integer, dimension(MPI_STATUS_SIZE) :: mpi_status
    integer :: start_rank, end_rank
    integer :: comm_color, comm_key
    integer, dimension(MAX_ND) :: cartStride
    integer, dimension(MAX_ND) :: start_cartCoords, end_cartCoords
    integer, dimension(MAX_ND) :: cartCoords_subset
    integer, dimension(MAX_ND) :: cartDims_subset
    integer :: l0, l1
    integer, dimension(MAX_ND) :: stride_whole
    PetscErrorCode :: ierr
    character(len=512) :: error_message
    integer :: error_rank, min_error_rank, global_rank
    integer :: invalid_dir
    character(len=1) :: invalid_dir_name
    real(rfreal), dimension(MAX_ND) :: subset_ivec, subset_jvec
    real(rfreal), dimension(MAX_ND) :: xyz_rel
    real(rfreal) :: r, z

    solver => region%electric(ng)%solver
    grid => region%grid(ng)
    electric => region%electric(ng)

    if (present(axisymmetric)) then
      axisymmetric_ = axisymmetric
      if (axisymmetric_) then
        if (present(axisymmetric_origin)) then
          axisymmetric_origin_ = axisymmetric_origin
        else
          call graceful_exit(region%myrank, "ERROR: Must provide an origin if setting up an " // &
            "axisymmetric electric field on a grid subset.")
        end if
        if (present(axisymmetric_axis)) then
          axisymmetric_axis_ = axisymmetric_axis/sqrt(sum(axisymmetric_axis**2))
        else
          call graceful_exit(region%myrank, "ERROR: Must provide an axis direction if setting " // &
            "up an axisymmetric electric field on a grid subset.")
        end if
      end if
    else
      axisymmetric_ = .false.
    end if

    if (present(subset_dirs)) then

      dirs = subset_dirs

    else

      ! Find slice direction (if there is one)
      dirs(3) = 0
      do i = 1, grid%ND
        if (subset_start(i) == subset_end(i)) then
          if (dirs(3) == 0) then
            dirs(3) = i
          else
            call graceful_exit(region%myrank, "ERROR: Attempted to set up electric field " // &
              "solver on a grid subset of dimension < 2.")
          end if
        end if
      end do

      if (dirs(3) == 0) then
        ! 3D subset
        dirs(3) = 3
      end if

      dirs(1) = modulo(dirs(3), MAX_ND) + 1
      dirs(2) = modulo(dirs(3)+1, MAX_ND) + 1

    end if

    ND = merge(3, 2, subset_start(dirs(3)) /= subset_end(dirs(3)))

    axisymmetric_flag = merge(1, 0, axisymmetric_)

    ef_periodic(:) = 0
    do i = 1, ND
      if (subset_start(dirs(i)) == 1 .and. subset_end(dirs(i)) == grid%GlobalSize(dirs(i))) then
        ef_periodic(i) = grid%periodic(dirs(i))
      end if
    end do

    periodicStorage = grid%periodicStorage

    ! Figure out if the current process contains the lower corner of the subset, and if so, send
    ! its rank to the root process of the grid
    contains_subset_start = all(subset_start(:grid%ND) >= grid%is_unique(:grid%ND)) .and. &
      all(subset_start(:grid%ND) <= grid%ie_unique(:grid%ND))
    if (grid%myrank_inCartComm /= 0) then
      if (contains_subset_start) then
        call MPI_Send(grid%myrank_inCartComm, 1, MPI_INTEGER, 0, 0, grid%CartComm, ierr)
      end if
    else
      if (contains_subset_start) then
        start_rank = 0
      else
        call MPI_Recv(start_rank, 1, MPI_INTEGER, MPI_ANY_SOURCE, 0, grid%CartComm, mpi_status, ierr)
      end if
    end if

    ! Figure out if the current process contains the upper corner of the subset, and if so, send
    ! its rank to the root process of the grid
    contains_subset_end = all(subset_end(:grid%ND) >= grid%is_unique(:grid%ND)) .and. &
      all(subset_end(:grid%ND) <= grid%ie_unique(:grid%ND))
    if (grid%myrank_inCartComm /= 0) then
      if (contains_subset_end) then
        call MPI_Send(grid%myrank_inCartComm, 1, MPI_INTEGER, 0, 1, grid%CartComm, ierr)
      end if
    else
      if (contains_subset_end) then
        end_rank = 0
      else
        call MPI_Recv(end_rank, 1, MPI_INTEGER, MPI_ANY_SOURCE, 1, grid%CartComm, mpi_status, ierr)
      end if
    end if

    ! Send start and end rank to all processes on grid
    call MPI_Bcast(start_rank, 1, MPI_INTEGER, 0, grid%CartComm, ierr)
    call MPI_Bcast(end_rank, 1, MPI_INTEGER, 0, grid%CartComm, ierr)

    if (.not. present(subset_dirs) .and. axisymmetric_) then
      ! Check if the two subset directions form a right-handed coordinate system when transformed
      ! into r and z coordinates; if not, swap them
      if (grid%myrank_inCartComm == start_rank) then
        stride_whole(1) = product(grid%ie(:dirs(1)-1) - grid%is(:dirs(1)-1) + 1)
        stride_whole(2) = product(grid%ie(:dirs(2)-1) - grid%is(:dirs(2)-1) + 1)
        stride_whole(3) = product(grid%ie(:dirs(3)-1) - grid%is(:dirs(3)-1) + 1)
        axis = axisymmetric_axis_
        l_middle = 1 + (grid%ie(dirs(1))-grid%is(dirs(1)))/2 * stride_whole(1) + &
          (grid%ie(dirs(2)) - grid%is(dirs(2)))/2 * stride_whole(2) + &
          (subset_start(dirs(3)) - grid%is(dirs(3))) * stride_whole(3)
        xyz_middle = grid%XYZ(l_middle,:)
        normal(1) = xyz_middle(2)*axis(3) - xyz_middle(3)*axis(2)
        normal(2) = xyz_middle(3)*axis(1) - xyz_middle(1)*axis(3)
        normal(3) = xyz_middle(1)*axis(2) - xyz_middle(2)*axis(1)
        l_lower = 1 + (subset_start(dirs(1)) - grid%is(dirs(1))) * stride_whole(1) + &
          (subset_start(dirs(2)) - grid%is(dirs(2))) * stride_whole(2) + &
          (subset_start(dirs(3)) - grid%is(dirs(3))) * stride_whole(3)
        ivec = grid%XYZ(l_lower + stride_whole(1),:) - grid%XYZ(l_lower,:)
        jvec = grid%XYZ(l_lower + stride_whole(2),:) - grid%XYZ(l_lower,:)
        ivec_cross_jvec(1) = ivec(2)*jvec(3) - ivec(3)*jvec(2)
        ivec_cross_jvec(2) = ivec(3)*jvec(1) - ivec(1)*jvec(3)
        ivec_cross_jvec(3) = ivec(1)*jvec(2) - ivec(2)*jvec(1)
        right_handed = dot_product(ivec_cross_jvec, normal) >= 0._rfreal
        if (.not. right_handed) then
          j = dirs(1)
          dirs(1) = dirs(2)
          dirs(2) = j
        end if
      end if
      call MPI_Bcast(dirs, MAX_ND, MPI_INTEGER, start_rank, grid%CartComm, ierr)
    end if

    electric%grid_has_efield = .true.
    electric%whole_grid = .false.
    electric%rank_has_efield = all(subset_start(:grid%ND) <= grid%ie_unique(:grid%ND)) .and. &
      all(subset_end(:grid%ND) >= grid%is_unique(:grid%ND))
    electric%subset_dirs = dirs
    electric%subset_ND = ND
    electric%subset_start = subset_start
    electric%subset_end = subset_end
    electric%subset_axisymmetric = axisymmetric_
    if (axisymmetric_) then
      electric%subset_axisymmetric_origin = axisymmetric_origin_
      electric%subset_axisymmetric_axis = axisymmetric_axis_
    end if

    ! Compute the cartesian size and coordinates on the subset
    call MPI_Cart_Coords(grid%CartComm, start_rank, MAX_ND, start_cartcoords, ierr)
    call MPI_Cart_Coords(grid%CartComm, end_rank, MAX_ND, end_cartcoords, ierr)

    cartDims_subset(1) = end_cartCoords(dirs(1)) - start_cartCoords(dirs(1)) + 1
    cartDims_subset(2) = end_cartCoords(dirs(2)) - start_cartCoords(dirs(2)) + 1
    cartDims_subset(3) = end_cartCoords(dirs(3)) - start_cartCoords(dirs(3)) + 1

    cartCoords_subset(1) = grid%cartCoords(dirs(1)) - start_cartCoords(dirs(1))
    cartCoords_subset(2) = grid%cartCoords(dirs(2)) - start_cartCoords(dirs(2))
    cartCoords_subset(3) = grid%cartCoords(dirs(3)) - start_cartCoords(dirs(3))

    ! Need to partition the grid communicator into ranks that contain the subset and
    ! those that don't
    comm_color = merge(1, 2, electric%rank_has_efield)

    if (electric%rank_has_efield) then
      comm_key = cartCoords_subset(1) + cartCoords_subset(2) * cartDims_subset(1) + &
        cartCoords_subset(3) * cartDims_subset(1) * cartDims_subset(2)
    else
      comm_key = grid%myrank_inCartComm
    end if

    call MPI_Comm_Split(grid%CartComm, comm_color, comm_key, electric%subset_comm, ierr)
    call MPI_Comm_Rank(electric%subset_comm, electric%subset_rank, ierr)

    if (electric%rank_has_efield) then

      electric%subset_cartCoords = cartCoords_subset
      electric%subset_cartDims = cartDims_subset

      nGlobal(1) = subset_end(dirs(1)) - subset_start(dirs(1)) + 1
      nGlobal(2) = subset_end(dirs(2)) - subset_start(dirs(2)) + 1
      nProcs(1) = cartDims_subset(1)
      nProcs(2) = cartDims_subset(2)
      cartCoords(1) = cartCoords_subset(1)
      cartCoords(2) = cartCoords_subset(2)
      is(1) = max(subset_start(dirs(1)), grid%is_unique(dirs(1))) - subset_start(dirs(1)) + 1
      is(2) = max(subset_start(dirs(2)), grid%is_unique(dirs(2))) - subset_start(dirs(2)) + 1
      is(3) = max(subset_start(dirs(3)), grid%is_unique(dirs(3))) - subset_start(dirs(3)) + 1
      ie(1) = min(subset_end(dirs(1)), grid%ie_unique(dirs(1))) - subset_start(dirs(1)) + 1
      ie(2) = min(subset_end(dirs(2)), grid%ie_unique(dirs(2))) - subset_start(dirs(2)) + 1
      ie(3) = min(subset_end(dirs(3)), grid%ie_unique(dirs(3))) - subset_start(dirs(3)) + 1
      nCells = product(ie(:2) - is(:2) + 1)
      nLocal(1) = ie(1) - is(1) + 1
      nLocal(2) = ie(2) - is(2) + 1
      stride(1) = ie(1) - is(1) + 1
      stride(2) = ie(2) - is(2) + 1
      offset(1) = 0
      offset(2) = 0

      electric%subset_is = is
      electric%subset_ie = ie
      electric%subset_nCells = nCells

      nullify(electric%subset_to_whole)
      allocate(electric%subset_to_whole(nCells))

      stride_whole(1) = product(grid%ie(:dirs(1)-1) - grid%is(:dirs(1)-1) + 1)
      stride_whole(2) = product(grid%ie(:dirs(2)-1) - grid%is(:dirs(2)-1) + 1)
      stride_whole(3) = product(grid%ie(:dirs(3)-1) - grid%is(:dirs(3)-1) + 1)

      l1 = 1
      do k = is(3), ie(3)
        do j = is(2), ie(2)
          do i = is(1), ie(1)
            l0 = 1 + (i + subset_start(dirs(1)) - 1 - grid%is(dirs(1))) * stride_whole(1) + &
              (j + subset_start(dirs(2)) - 1 - grid%is(dirs(2))) * stride_whole(2) + &
              (k + subset_start(dirs(3)) - 1 - grid%is(dirs(3))) * stride_whole(3)
            electric%subset_to_whole(l1) = l0
            l1 = l1 + 1
          end do
        end do
      end do

      nullify(electric%xyz_subset)
      allocate(electric%xyz_subset(nCells,ND))

      if (.not. axisymmetric_) then
        if (grid%ND == 3 .and. ND == 2) then
          ! Project onto a basis of the subset plane
          if (electric%subset_rank == 0) then
            l0 = 1 + (subset_start(dirs(1)) - grid%is(dirs(1))) * stride_whole(1) + &
              (subset_start(dirs(2)) - grid%is(dirs(2))) * stride_whole(2) + &
              (subset_start(dirs(3)) - grid%is(dirs(3))) * stride_whole(3)
            subset_ivec = grid%XYZ(l0 + stride_whole(1),:) - grid%XYZ(l0,:)
            subset_ivec = subset_ivec/sqrt(sum(subset_ivec**2))
            subset_jvec = grid%XYZ(l0 + stride_whole(2),:) - grid%XYZ(l0,:)
            subset_jvec = subset_jvec - dot_product(subset_jvec, subset_ivec)*subset_ivec
            subset_jvec = subset_jvec/sqrt(sum(subset_jvec**2))
          end if
          call MPI_Bcast(subset_ivec, MAX_ND, MPI_REAL8, 0, electric%subset_comm, ierr)
          call MPI_Bcast(subset_jvec, MAX_ND, MPI_REAL8, 0, electric%subset_comm, ierr)
          do i = 1, nCells
            xyz_rel = grid%XYZ(electric%subset_to_whole(i),:) - grid%XYZ(l0,:)
            electric%xyz_subset(i,1) = grid%XYZ(l0,dirs(1)) + dot_product(xyz_rel, subset_ivec)
            electric%xyz_subset(i,2) = grid%XYZ(l0,dirs(2)) + dot_product(xyz_rel, subset_jvec)
          end do
        else
          do j = 1, ND
            do i = 1, nCells
              electric%xyz_subset(i,j) = grid%XYZ(electric%subset_to_whole(i),dirs(j))
            end do
          end do
        end if
      else
        ! Convert to axisymmetric (r and z) coordinates
        do i = 1, nCells
          xyz_rel = grid%XYZ(electric%subset_to_whole(i),:) - axisymmetric_origin_
          z = dot_product(xyz_rel, axisymmetric_axis_)
          r = sqrt(sum(xyz_rel**2) - z**2)
          electric%xyz_subset(i,1) = r
          electric%xyz_subset(i,2) = z
        end do
      end if

    else

      electric%subset_is = 1
      electric%subset_ie = 1
      do i = 1, ND
        electric%subset_is(i) = grid%is_unique(dirs(i))
        electric%subset_ie(i) = grid%is_unique(dirs(i))-1
      end do
      electric%subset_nCells = 0

    end if

    ! Check if any ranks have dimensions that are too small
    error_rank = grid%numproc_inCartComm
    do i = 1, ND
      if (electric%subset_ie(i) - electric%subset_is(i) + 1 == 1) then
        error_rank = grid%myrank_inCartComm
        invalid_dir = dirs(i)
        global_rank = region%myrank
        exit
      end if
    end do

    ! Communicate this to other processes and exit accordingly
    call MPI_Allreduce(error_rank, min_error_rank, 1, MPI_INTEGER, MPI_MIN, grid%CartComm, ierr)
    if (min_error_rank /= grid%numproc_inCartComm) then
      call MPI_Bcast(invalid_dir, 1, MPI_INTEGER, min_error_rank, grid%CartComm, ierr)
      call MPI_Bcast(global_rank, 1, MPI_INTEGER, min_error_rank, grid%CartComm, ierr)
      if (grid%myrank_inCartComm == 0) then
        select case (invalid_dir)
        case (1)
          invalid_dir_name = "i"
        case (2)
          invalid_dir_name = "j"
        case (3)
          invalid_dir_name = "k"
        end select
        write (*, '(2a,i0,3a,i0,a)') "PlasComCM: ERROR: Cannot construct electric field on the ", &
          "specified grid subset using the current decomposition (grid ", grid%iGridGlobal, &
          " would have only 1 point in the ", invalid_dir_name, " direction on rank ", &
          global_rank, "; the minimum is 2)."
        call MPI_Abort(mycomm, 1, ierr)
      end if
    end if

    if (electric%rank_has_efield) then
      call ef_init(solver, electric%subset_comm, nGlobal, nProcs, nLocal,&
                   offset, stride,&
                   cartCoords, ef_periodic,periodicStorage,&
                   ND, ng, axisymmetric_flag, ierr)
      CHKERRQ(ierr)
      call ef_set_grid(solver, is, ie, nCells, c_loc(electric%xyz_subset(1,1)), ierr)
      CHKERRQ(ierr)
    end if

    call ElectricInitState(region, ng)

    call ElectricInitBC(region, ng)

    if (electric%rank_has_efield) then
      call ef_setup_op(solver, ierr)
      CHKERRQ(ierr)
    end if

  end subroutine ElectricSetupSubset


  subroutine ElectricSolve(region, ng)
  ! Runs the electric field solver.
  !
  ! Planned API:
  !
  !    -div dcoef grad phi + bcoef phi = rhs
  !     [n . dcoef grad phi] = jump
  !                      phi = g_{dirichlet} on \Gamma_D
  !               n grad phi = 0 on \Gamma_N
  !
  !   input
  !   -----
  !     dcoef: grid function
  !         default (1) in ModInput
  !         modified by IBlank
  !         set in ElectricInitState
  !         region%electric(ng)%dcoef
  !         fixed for the duration of the run
  !
  !     bcoef: grid function
  !         default (0) in ModInput
  !         set in ElectricInitState
  !         modified in ModElectricState
  !         region%electric(ng)%bcoef
  !         fixed for the duration of the run
  !         must be nonnegative
  !
  !     jump: grid function
  !         default (0) - set where/how?
  !         modified in ModElectricState
  !         region%electric(ng)%jump
  !         fixed for the duration of the run
  !
  !     rhs: grid function
  !         default (0) in ModInput
  !         modified in ModElectricState
  !         region%electric(ng)%rhs
  !         changable, but must set newrhs (bool)
  !         solver updated automatically in efs_updatebc()
  !
  !     (TODO:
  !     newrhs: bool
  !         True if rhs has been updated
  !         False if rhs has not been updated
  !         Solver needs to know if rhs needs to be re-parsed.
  !     )
  !
  !     dirichlet: patch function
  !         modified in ModElectricState
  !         region%efpatch(npatch)%dirichlet for each npatch
  !         changable, but must set newdirichlet (bool)
  !         solver updated automatically in efs_updatebc()
  !
  !     (TODO:
  !     newdirichlet: bool
  !         True if dirichlet has been updated
  !         False if dirichlet has not been updated
  !         Solver needs to know if dirichlet needs to be re-parsed.
  !         Note: this will automatically set newf=True
  !     )
  !
  !   output
  !   ------
  !     phi: grid function
  !         region%electric(ng)%phi
  !         overwritten on each solver call
  !
  !     gradphi: grid function
  !         region%electric(ng)%gradphi
  !         gradient of phi (grad phi, not -grad phi) using default space order (SPACEORDER)
  !         called in ElectricGradient
  !
  !    IO
  !    --
  !      WRITE_PHI : bool
  !         .inp setting
  !         writes phi at each output interval
  !
  !   Settings
  !   --------
  !     Physics set in plascomcm.inp
  !
  !     Phi solved on all grids by default
  !
  !     Enable/disable Phi on individual grids in ef_subset.dat
  !
  !     (TODO:
  !     Set solver details in ef_solver.dat
  !         - PETSc specific options in petscoptions.txt
  !         - Carbon specific options in carbon.json
  !     )
  !
  !   notes
  !   -----
  !     unitless (see the equations above)
  !     Note: electrode at a dielectric interface may be an issue

    use iso_c_binding
    USE ModDataStruct
    USE ModDeriv
    USE ModMPI, ONLY: Ghost_Cell_Exchange_Box
    USE ModElectricInterface
    USE ModElectricState
    implicit none
#include "petsc_fortran.h"

    type(t_region), pointer :: region
    integer :: ng

    type(t_electric), pointer :: electric
    PetscErrorCode :: ierr
    real(rfreal) :: time

    electric => region%electric(ng)
    time = MPI_WTIME()

    if (electric%grid_has_efield) then

      if (.not. electric%whole_grid .and. electric%rank_has_efield) then
        call CopyWholeToSubset(region, ng, electric%dcoef, electric%dcoef_subset)
        call CopyWholeToSubset(region, ng, electric%bcoef, electric%bcoef_subset)
        call CopyWholeToSubset(region, ng, electric%jump, electric%jump_subset)
      end if

      call ElectricSolveLocal(region, ng)

      call ElectricExtendSubset(region, ng)

      Call Ghost_Cell_Exchange_Box(region, ng, electric%phi)

      call ElectricGradient(region, ng)

    end if

    region%mpi_timings(ng)%efield_solve = region%mpi_timings(ng)%efield_solve + (MPI_WTIME() - time)

  end subroutine ElectricSolve


  !> Runs the Efield solver.
  subroutine ElectricSolveLocal(region, ng)

    use iso_c_binding
    USE ModDataStruct
    USE ModGlobal
    USE ModElectricInterface
    implicit none
#include "petsc_fortran.h"

    type(t_region), pointer :: region
    integer :: ng
    PetscErrorCode :: ierr

    type(t_electric), pointer :: electric

    electric => region%electric(ng)

    if (electric%grid_has_efield) then
      if (electric%rank_has_efield) then
        call ef_solve(electric%solver, ierr)
        CHKERRQ(ierr)
      end if
    end if

  end subroutine ElectricSolveLocal


  !> Take data from subset and extend it to the whole grid
  subroutine ElectricExtendSubset(region, ng)

    USE ModDataStruct
    USE ModGlobal
    USE ModElectricState
    implicit none

    type(t_region), pointer :: region
    integer :: ng

    type(t_electric), pointer :: electric

    electric => region%electric(ng)

    if (electric%grid_has_efield .and. .not. electric%whole_grid) then
      if (.not. electric%subset_axisymmetric) then
        if (electric%rank_has_efield) then
          call CopySubsetToWhole(region, ng, electric%phi_subset, electric%phi)
        end if
      else
        call ElectricExtendAxisymmetricSubset(region, ng)
      end if
    end if

  end subroutine ElectricExtendSubset

  !> Take data from axisymmetric subset and extend it to the whole grid
  subroutine ElectricExtendAxisymmetricSubset(region, ng)

    USE ModDataStruct
    USE ModGlobal
    implicit none
#include "petsc_fortran.h"

    type(t_region), pointer :: region
    integer :: ng

    type(t_grid), pointer :: grid
    type(t_electric), pointer :: electric
    integer :: i, j, k, l, m
    integer :: l0, l1
    integer :: ierr
    integer, dimension(MAX_ND) :: dirs
    integer :: subset_z_color, subset_z_rank, subset_z_comm
    integer, dimension(:), allocatable :: nCells_per_proc
    integer, dimension(:), allocatable :: displs
    integer, dimension(MPI_STATUS_SIZE) :: mpi_status
    real(rfreal), dimension(:), allocatable :: r_local
    real(rfreal), dimension(:), allocatable :: phi_local
    integer :: nCells_slice
    real(rfreal), dimension(:), allocatable :: r_gathered
    real(rfreal), dimension(:), allocatable :: phi_gathered
    integer, dimension(:,:), allocatable :: is, ie
    real(rfreal), dimension(:), allocatable :: r_slice
    real(rfreal), dimension(:), allocatable :: phi_slice
    integer :: full_z_color, full_z_rank, full_z_comm
    integer :: data_rank
    integer, dimension(MAX_ND) :: subset_start, subset_end
    integer, dimension(2) :: is_slice, ie_slice
    integer, dimension(MAX_ND) :: stride_whole
    real(rfreal), dimension(MAX_ND) :: origin
    real(rfreal), dimension(MAX_ND) :: axis
    real(rfreal), dimension(MAX_ND) :: xyz_rel
    real(rfreal) :: r, z
    integer :: cell
    integer :: z_offset
    integer :: l_lower, l_upper
    real(rfreal) :: cell_coord

    grid => region%grid(ng)
    electric => region%electric(ng)

    dirs = electric%subset_dirs

    if (electric%rank_has_efield) then

      ! Split axisymmetric communicator in z direction
      subset_z_color = electric%subset_cartCoords(2)
      subset_z_rank = electric%subset_cartCoords(1)

      call MPI_Comm_Split(electric%subset_comm, subset_z_color, subset_z_rank, subset_z_comm, ierr)

      ! Send nCells from each rank to the root
      if (subset_z_rank == 0) then
        allocate(nCells_per_proc(electric%subset_cartDims(1)))
      else
        allocate(nCells_per_proc(0))
      end if

      call MPI_Gather(electric%subset_nCells, 1, MPI_INTEGER, nCells_per_proc, 1, MPI_INTEGER, 0, &
        subset_z_comm, ierr)

      ! Assemble r and phi data for local points and send to the root
      allocate(r_local(electric%subset_nCells))
      allocate(phi_local(electric%subset_nCells))

      do i = 1, electric%subset_nCells
        r_local(i) = electric%xyz_subset(i,1)
        phi_local(i) = electric%phi_subset(i)
      end do

      if (subset_z_rank == 0) then

        nCells_slice = sum(nCells_per_proc)

        allocate(r_gathered(nCells_slice))
        allocate(phi_gathered(nCells_slice))

        allocate(displs(electric%subset_cartDims(1)))
        do i = 1, electric%subset_cartDims(1)
          displs(i) = sum(nCells_per_proc(1:i-1))
        end do

      else

        allocate(r_gathered(0))
        allocate(phi_gathered(0))
        allocate(displs(0))

      end if

      call MPI_Gatherv(r_local, electric%subset_nCells, MPI_REAL8, r_gathered, nCells_per_proc, &
        displs, MPI_REAL8, 0, subset_z_comm, ierr)
      call MPI_Gatherv(phi_local, electric%subset_nCells, MPI_REAL8, phi_gathered, &
        nCells_per_proc, displs, MPI_REAL8, 0, subset_z_comm, ierr)

      if (subset_z_rank == 0) then

        ! Data in r/phi_gathered is in the wrong order (ordered according to rank, but needs to
        ! be ordered like a 2D slice)
        allocate(is(2,electric%subset_cartDims(1)))
        allocate(ie(2,electric%subset_cartDims(1)))

        is(1,1) = electric%subset_start(dirs(1))
        do m = 1, electric%subset_cartDims(1)-1
          ie(1,m) = is(1,m) + nCells_per_proc(m)/(grid%ie_unique(dirs(2)) - grid%is_unique(dirs(2)) + 1) - 1
          is(1,m+1) = ie(1,m)+1
        end do
        ie(1,electric%subset_cartDims(1)) = electric%subset_end(dirs(1))
        is(2,:) = grid%is_unique(dirs(2))
        ie(2,:) = grid%ie_unique(dirs(2))

        allocate(r_slice(nCells_slice))
        allocate(phi_slice(nCells_slice))

        l1 = 1
        do m = 1, electric%subset_cartDims(1)
          do l = 1, nCells_per_proc(m)
            i = is(1,m) + modulo(l-1,ie(1,m)-is(1,m)+1)
            j = is(2,m) + modulo((l-1)/(ie(1,m)-is(1,m)+1),ie(2,m)-is(2,m)+1)
            l0 = 1 + (i - electric%subset_start(dirs(1))) + (j - grid%is_unique(dirs(2))) * &
              (electric%subset_end(dirs(1)) - electric%subset_start(dirs(1)) + 1)
            r_slice(l0) = r_gathered(l1)
            phi_slice(l0) = phi_gathered(l1)
            l1 = l1 + 1
          end do
        end do

        deallocate(r_gathered)
        deallocate(phi_gathered)

      end if

    end if

    ! Now split the full grid communicator in the z direction
    full_z_color = grid%cartCoords(dirs(2))
    full_z_rank = grid%cartCoords(dirs(1)) + grid%cartCoords(dirs(3)) * grid%cartDims(dirs(1))

    call MPI_Comm_Split(grid%CartComm, full_z_color, full_z_rank, full_z_comm, ierr)

    ! Send the rank containing r_slice and phi_slice to the root (if necessary)
    if (full_z_rank == 0) then
      if (electric%rank_has_efield) then
        if (subset_z_rank == 0) then
          data_rank = full_z_rank
        else
          call MPI_Recv(data_rank, 1, MPI_INTEGER, MPI_ANY_SOURCE, 0, full_z_comm, mpi_status, &
            ierr)
        end if
      else
        call MPI_Recv(data_rank, 1, MPI_INTEGER, MPI_ANY_SOURCE, 0, full_z_comm, mpi_status, &
          ierr)
      end if
    else
      if (electric%rank_has_efield) then
        if (subset_z_rank == 0) then
          call MPI_Send(full_z_rank, 1, MPI_INTEGER, 0, 0, full_z_comm, ierr)
        end if
      end if
    end if

    ! Broadcast the data rank to all processes in the full z communicator
    call MPI_Bcast(data_rank, 1, MPI_INTEGER, 0, full_z_comm, ierr)

    ! Broadcast the size of the data
    call MPI_Bcast(nCells_slice, 1, MPI_INTEGER, data_rank, full_z_comm, ierr)

    ! Now broadcast the data
    if (.not. allocated(r_slice)) then
      allocate(r_slice(nCells_slice))
    end if
    if (.not. allocated(phi_slice)) then
      allocate(phi_slice(nCells_slice))
    end if

    call MPI_Bcast(r_slice, nCells_slice, MPI_REAL8, data_rank, full_z_comm, ierr)
    call MPI_Bcast(phi_slice, nCells_slice, MPI_REAL8, data_rank, full_z_comm, ierr)

    ! Broadcast the subset range
    if (electric%rank_has_efield) then
      if (subset_z_rank == 0) then
        subset_start = electric%subset_start
        subset_end = electric%subset_end
      end if
    end if

    call MPI_Bcast(subset_start, MAX_ND, MPI_INTEGER, data_rank, full_z_comm, ierr)
    call MPI_Bcast(subset_end, MAX_ND, MPI_INTEGER, data_rank, full_z_comm, ierr)

    ! Broadcast the axisymmetric origin and axis
    if (electric%rank_has_efield) then
      if (subset_z_rank == 0) then
        origin = electric%subset_axisymmetric_origin
        axis = electric%subset_axisymmetric_axis
      end if
    end if

    call MPI_Bcast(origin, MAX_ND, MPI_REAL8, data_rank, full_z_comm, ierr)
    call MPI_Bcast(axis, MAX_ND, MPI_REAL8, data_rank, full_z_comm, ierr)

    ! Compute radial coordinate for all grid points on this rank and interpolate using
    ! r_slice and phi_slice
    is_slice(1) = subset_start(dirs(1))
    ie_slice(1) = subset_end(dirs(1))
    is_slice(2) = grid%is_unique(dirs(2))
    ie_slice(2) = grid%ie_unique(dirs(2))

    stride_whole(1) = product(grid%ie(:dirs(1)-1) - grid%is(:dirs(1)-1) + 1)
    stride_whole(2) = product(grid%ie(:dirs(2)-1) - grid%is(:dirs(2)-1) + 1)
    stride_whole(3) = product(grid%ie(:dirs(3)-1) - grid%is(:dirs(3)-1) + 1)

    do k = grid%is_unique(dirs(3)), grid%ie_unique(dirs(3))
      do j = grid%is_unique(dirs(2)), grid%ie_unique(dirs(2))
        do i = grid%is_unique(dirs(1)), grid%ie_unique(dirs(1))
          l0 = 1 + (i - grid%is(dirs(1))) * stride_whole(1) + &
            (j - grid%is(dirs(2))) * stride_whole(2) + (k - grid%is(dirs(3))) * stride_whole(3)
          xyz_rel = grid%XYZ(l0,:) - origin
          z = dot_product(xyz_rel, axis)
          r = sqrt(sum(xyz_rel**2) - z**2)
          z_offset = (j-is_slice(2)) * (ie_slice(1)-is_slice(1)+1)
          l_lower = 1 + z_offset
          l_upper = 1 + (ie_slice(1)-is_slice(1)) + z_offset
          do m = l_lower+1, l_upper-2
            cell = m - z_offset
            if (r <= r_slice(m + 1)) then
              exit
            end if
          end do
          cell_coord = CubicCellCoord(r, r_slice(l_lower:l_upper), cell)
          electric%phi(l0) = CubicInterp(cell_coord, phi_slice(l_lower:l_upper), cell)
        end do
      end do
    end do

  end subroutine ElectricExtendAxisymmetricSubset

  !determine the potential gradient
  subroutine ElectricGradient(region, ng)

    USE ModDataStruct
    USE ModDeriv
    USE ModMPI
    implicit none

    type(t_region), pointer :: region
    type(t_electric), pointer :: electric
    type(t_mixt), pointer :: state
    type(t_grid), pointer :: grid
    integer :: ng
    real(rfreal), pointer :: MT1(:,:), JAC(:)
    real(rfreal), allocatable :: gradphi_tangent(:,:)
    integer :: ND,Nc,l,m,ii

    electric => region%electric(ng)
    grid     => region%grid(ng)
    state    => region%state(ng)
    MT1      => grid%MT1
    JAC      => grid%JAC
    ND = grid%ND
    Nc = grid%nCells

    if (electric%grid_has_efield) then

      allocate(gradphi_tangent(Nc,ND))

      if (.not. associated(state%flux)) allocate(state%flux(Nc))
      if (.not. associated(state%dflux)) allocate(state%dflux(Nc))

      !gradient in tangent space
      do l = 1, grid%ND
        do ii = 1, Nc
          state%flux(ii) = electric%phi(ii) ! ePotential.
        end do ! ii
        call APPLY_OPERATOR_box(region, ng, 1, l, state%flux, state%dflux, .FALSE., 0)
        do ii = 1, Nc
          gradphi_tangent(ii,l) = state%dflux(ii) ! dphi/d(xi_l) (l = 1 ~ 3)
        end do
      end do ! l

      !gradient in cartesian space
      if ( ND == 2 ) then

#ifdef ENABLE_SIMD
!DIR$ SIMD
#endif
        do ii = 1, Nc
          electric%gradphi(ii,1) = JAC(ii) *        &
               ( MT1(ii,1) * gradphi_tangent(ii,1) &
               + MT1(ii,2) * gradphi_tangent(ii,2) )

          electric%gradphi(ii,2) = JAC(ii) *        &
               ( MT1(ii,3) * gradphi_tangent(ii,1) &
               + MT1(ii,4) * gradphi_tangent(ii,2) )
        end do

      else if ( ND == 3 ) then

#ifdef ENABLE_SIMD
!DIR$ SIMD
#endif
        do ii = 1, Nc
          electric%gradphi(ii,1) = JAC(ii) *        &
               ( MT1(ii,1) * gradphi_tangent(ii,1) &
               + MT1(ii,2) * gradphi_tangent(ii,2) &
               + MT1(ii,3) * gradphi_tangent(ii,3) )

          electric%gradphi(ii,2) = JAC(ii) *        &
               ( MT1(ii,4) * gradphi_tangent(ii,1) &
               + MT1(ii,5) * gradphi_tangent(ii,2) &
               + MT1(ii,6) * gradphi_tangent(ii,3) )

          electric%gradphi(ii,3) = JAC(ii) *        &
               ( MT1(ii,7) * gradphi_tangent(ii,1) &
               + MT1(ii,8) * gradphi_tangent(ii,2) &
               + MT1(ii,9) * gradphi_tangent(ii,3) )
        end do

      end if

    end if

  end subroutine ElectricGradient

  !> Lagrange interpolation basis (used for interpolating radius values when extending
  !> from axisymmetric subset to full grid)
  pure function CubicInterpBasis(S) result(Basis)

    USE ModGlobal
    implicit none

    real(rfreal), intent(in) :: S
    real(rfreal), dimension(4) :: Basis

    Basis(1) = (-S**3 + 3._rfreal * S**2 - 2._rfreal * S)/6._rfreal
    Basis(2) = (3._rfreal * S**3 - 6._rfreal * S**2 - 3._rfreal * S + 6._rfreal)/6._rfreal
    Basis(3) = (-3._rfreal * S**3 + 3._rfreal * S**2 + 6._rfreal * S)/6._rfreal
    Basis(4) = (S**3 - S)/6._rfreal

  end function CubicInterpBasis

  !> Derivative of Lagrange interpolation basis
  pure function CubicInterpBasisDeriv(S) result(BasisDeriv)

    USE ModGlobal
    implicit none

    real(rfreal), intent(in) :: S
    real(rfreal), dimension(4) :: BasisDeriv

    BasisDeriv(1) = (-3._rfreal * S**2 + 6._rfreal * S - 2._rfreal)/6._rfreal
    BasisDeriv(2) = (9._rfreal * S**2 - 12._rfreal * S - 3._rfreal)/6._rfreal
    BasisDeriv(3) = (-9._rfreal * S**2 + 6._rfreal * S + 6._rfreal)/6._rfreal
    BasisDeriv(4) = (3._rfreal * S**2 - 1._rfreal)/6._rfreal

  end function CubicInterpBasisDeriv

  !> Find the local interpolation cell coordinates given the global coordinate (X),
  !> the global coordinate node data (Xs), and the interpolation cell (Cell)
  function CubicCellCoord(X, Xs, Cell) result(S)

    USE ModGlobal
    implicit none

    real(rfreal), intent(in) :: X
    real(rfreal), dimension(:), intent(in) :: Xs
    integer, intent(in) :: Cell
    real(rfreal) :: S

    integer :: i
    real(rfreal) :: Error
    real(rfreal), dimension(4) :: DerivCoefs
    real(rfreal) :: Deriv

    integer, parameter :: MAX_STEPS = 100
    real(rfreal), parameter :: MAX_ERROR = 1.e-10_rfreal

    S = 0.5_rfreal

    Error = X - CubicInterp(S, Xs, Cell)

    i = 1
    do while (abs(Error) > MAX_ERROR .and. i <= MAX_STEPS)
      DerivCoefs = CubicInterpBasisDeriv(S)
      Deriv = DerivCoefs(1) * Xs(Cell-1) + DerivCoefs(2) * Xs(Cell) + &
        DerivCoefs(3) * Xs(Cell+1) + DerivCoefs(4) * Xs(Cell+2)
      S = S + Error/Deriv
      Error = X - CubicInterp(S, Xs, Cell)
      i = i + 1
    end do

    if (abs(Error) > MAX_ERROR) then
      write (*, '(a)') "WARNING: CubicCellCoord failed to converge"
    end if

  end function CubicCellCoord

  !> Find the interpolated value given the local interpolation cell coordinate (S),
  !> the node data (Values), and the interpolation cell (Cell)
  function CubicInterp(S, Values, Cell) result(Value)

    USE ModGlobal
    implicit none

    real(rfreal), intent(in) :: S
    real(rfreal), dimension(:), intent(in) :: Values
    integer, intent(in) :: Cell
    real(rfreal) :: Value

    real(rfreal), dimension(4) :: Coefs

    Coefs = CubicInterpBasis(S)
    Value = Coefs(1) * Values(Cell-1) + Coefs(2) * Values(Cell) + Coefs(3) * Values(Cell+1) + &
      Coefs(4) * Values(Cell+2)

  end function CubicInterp

  !> Deallocates data created by the Efield solver.
  subroutine ElectricCleanup(region, ng)

    USE ModDataStruct
    USE ModElectricState
    USE ModElectricBC
    USE ModElectricInterface
    implicit none
#include "petsc_fortran.h"

    type(t_region), pointer :: region
    type(t_electric), pointer :: state
    integer :: ng
    PetscErrorCode :: ierr
    real(rfreal) :: time

    state => region%electric(ng)
    time = MPI_WTIME()

    call ElectricCleanupBC(region, ng)
    call ElectricCleanupState(region, ng)

    if (state%rank_has_efield) then
      call ef_cleanup(state%solver, ierr)
    end if

    CHKERRQ(ierr)

    region%mpi_timings(ng)%efield_processing = region%mpi_timings(ng)%efield_processing + (MPI_WTIME() - time)

  end subroutine ElectricCleanup


  subroutine ModElectricRegionCleanup(region)

    USE ModDataStruct
    implicit none
#include "petsc_fortran.h"

    type(t_region), pointer :: region
    integer :: ng
    PetscErrorCode :: ierr

    do ng = 1, region%nGrids
      deallocate(region%electric(ng)%phi_data_ptr)
    end do

    deallocate(region%electric)
    deallocate(region%EFpatch)

    call PetscFinalize(ierr)
    CHKERRQ(ierr)

  end subroutine ModElectricRegionCleanup

end module ModElectric
