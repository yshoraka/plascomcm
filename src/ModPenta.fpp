! Copyright (c) 2001, Charles Pierce
! Copyright (c) 2015, University of Illinois at Urbana-Champaign, XPACC
!
! License:
! All subroutines contained in this file were originally written by
! Mr. Charles D. Pierce from Stanford University as part of his
! 2001 PhD thesis titled "Progress-variable approach for large-eddy
! simulation of turbulent combustion".  Their inclusion in
! PlasComCM for use and for distribution is approved by his PhD advisor,
! Professor Parviz Moin, Stanford University.  Modification and
! distribution outside of this package is prohibited.
!
!=======================================================================
! Parallel pentaDiagonal solvers using MPI
!
! Charles Pierce, January 1997
! Daniel J. Bodony, January 2007
!
! MPI_pentaDiagonalM
!=======================================================================
Module ModPenta

Contains

  subroutine MPI_pentaDiagonalM(asave, bsave, csave, dsave, esave, &
       r, n, lot, icomm)
    USE ModGlobal
    USE ModMPI
    Integer :: lot, n
    !real(rfreal) :: a(lot,n), b(lot,n), c(lot,n), d(lot,n), e(lot,n), 
    real(rfreal) :: r(lot,n)
    real(rfreal), allocatable, dimension(:,:) :: a, b, c, d, e
    !real(rfreal) :: asave(lot,n), bsave(lot,n), csave(lot,n), dsave(lot,n)
    !real(rfreal) :: esave(lot,n)
    real(rfreal), pointer :: asave(:,:), bsave(:,:), csave(:,:), &
         dsave(:,:), esave(:,:)
    real(rfreal) :: const(lot)
    real(rfreal) :: s1(lot,n,2), s2(lot,n,2)
    real(rfreal) :: r1(lot,2), r2(lot,2), swap(lot,2)
    real(rfreal), allocatable :: sendbuf(:,:), recvbuf(:,:)
    real(rfreal), allocatable :: AA(:,:,:)
    integer, allocatable :: ngroup(:)
    integer :: icomm, nlot, i, j
    integer :: nproc, irank, nremain, L, K1, K2, nk
    integer :: igroup, k, m
    integer :: ierr

    if (n < 1) stop "MPI_pentaDiagonalM: n < 1"

    ! Get communicator info
    call MPI_comm_size (icomm, nproc, ierr)
    call MPI_comm_rank (icomm, irank, ierr)

    ! Partition the lot
    if (lot < nproc) stop "MPI_pentaDiagonalM: lot must be >= nproc"
    allocate(ngroup(nproc))
    ngroup(:) = lot/nproc
    nremain = mod(lot,nproc)
    ngroup(1:nremain) = ngroup(1:nremain) + 1
    nlot = ngroup(1)

    allocate(sendbuf(nlot,24*nproc))
    allocate(recvbuf(nlot,24*nproc))

    allocate(a(lot,n),b(lot,n),c(lot,n),d(lot,n),e(lot,n))
    
    do i = 1, lot
      do j = 1, n
        a(i,j) = asave(i,j)
        b(i,j) = bsave(i,j)
        c(i,j) = csave(i,j) 
        d(i,j) = dsave(i,j)
        e(i,j) = esave(i,j)
      end do
    end do

    if (n > 1) then ! do normal stuff

      ! Initialize boundary values
      if (irank == 0) then
        s1(:,1:2,1:2) = 0.
      else
        s1(:,1,1) = a(:,1)
        s1(:,1,2) = b(:,1)
        s1(:,2,1) = 0.
        s1(:,2,2) = a(:,2)
      end if
      if (irank == nproc-1) then
        s2(:,n-1:n,1:2) = 0.
      else
        s2(:,n-1,1) = e(:,n-1)
        s2(:,n-1,2) = 0.
        s2(:,n,1) = d(:,n)
        s2(:,n,2) = e(:,n)
      end if

      ! Forward elimination
      ! Upper boundary in s1(:,i,1:2)
      do i=1,n-2
        ! Eliminate a(i+2)
        const(:) = a(:,i+2)/c(:,i)
        b(:,i+2) = b(:,i+2) - d(:,i)*const(:)
        c(:,i+2) = c(:,i+2) - e(:,i)*const(:)
        r(:,i+2) = r(:,i+2) - r(:,i)*const(:)
        s1(:,i+2,1) = -s1(:,i,1)*const(:)
        s1(:,i+2,2) = -s1(:,i,2)*const(:)

        ! Eliminate b(i+1)
        const(:) = b(:,i+1)/c(:,i)
        c(:,i+1) = c(:,i+1) - d(:,i)*const(:)
        d(:,i+1) = d(:,i+1) - e(:,i)*const(:)
        r(:,i+1) = r(:,i+1) - r(:,i)*const(:)
        s1(:,i+1,1) = s1(:,i+1,1) - s1(:,i,1)*const(:)
        s1(:,i+1,2) = s1(:,i+1,2) - s1(:,i,2)*const(:)
      end do
      ! Eliminate b(n)
      const(:) = b(:,n)/c(:,n-1)
      c(:,n) = c(:,n) - d(:,n-1)*const(:)
      r(:,n) = r(:,n) - r(:,n-1)*const(:)
      s1(:,n,1) = s1(:,n,1) - s1(:,n-1,1)*const(:)
      s1(:,n,2) = s1(:,n,2) - s1(:,n-1,2)*const(:)
      s2(:,n,1) = s2(:,n,1) - s2(:,n-1,1)*const(:)

      ! Backward elimination
      ! Lower boundary in s2(:,i,1:2)
      do i=n,3,-1
        ! Eliminate e(i-2)
        const(:) = e(:,i-2)/c(:,i)
        r(:,i-2) = r(:,i-2) - r(:,i)*const(:)
        s1(:,i-2,1) = s1(:,i-2,1) - s1(:,i,1)*const(:)
        s1(:,i-2,2) = s1(:,i-2,2) - s1(:,i,2)*const(:)
        s2(:,i-2,1) = -s2(:,i,1)*const(:)
        s2(:,i-2,2) = -s2(:,i,2)*const(:)

        ! Eliminate d(i-1)
        const(:) = d(:,i-1)/c(:,i)
        r(:,i-1) = r(:,i-1) - r(:,i)*const(:)
        s1(:,i-1,1) = s1(:,i-1,1) - s1(:,i,1)*const(:)
        s1(:,i-1,2) = s1(:,i-1,2) - s1(:,i,2)*const(:)
        s2(:,i-1,1) = s2(:,i-1,1) - s2(:,i,1)*const(:)
        s2(:,i-1,2) = s2(:,i-1,2) - s2(:,i,2)*const(:)
      end do
      ! Eliminate d(1)
      const(:) = d(:,1)/c(:,2)
      r(:,1) = r(:,1) - r(:,2)*const(:)
      s1(:,1,1) = s1(:,1,1) - s1(:,2,1)*const(:)
      s1(:,1,2) = s1(:,1,2) - s1(:,2,2)*const(:)
      s2(:,1,1) = s2(:,1,1) - s2(:,2,1)*const(:)
      s2(:,1,2) = s2(:,1,2) - s2(:,2,2)*const(:)

    end if ! n > 1

    ! All dependence has been shifted to the boundary elements
    ! Communicate boundary values to root process
    ! and solve reduced 11-diagonal system
    ! USE of 11-diagonal system is more robust than the
    ! reordered (removes zeros) 7-diagonal system

    ! Send rows of 11-diagonal system
    ! (0, 0, 0, a, b, c, 0, 0, 0, d, e; r)
    !    (0, 0, a, b, 0, c, 0, 0, d, e, 0; r)
    !       (0, a, b, 0, 0, c, 0, d, e, 0, 0; r)
    !          (a, b, 0, 0, 0, c, d, e, 0, 0, 0; r)
    ! For efficiency, only send non-zero elements

    L = 1
    k1 = 1
    do igroup=1,nproc

      k2 = k1+ngroup(igroup)-1
      nk = k2-k1+1
 
      if (n > 1) then ! do normal stuff

        do i=1,2
          sendbuf(1:nk, L+0) = c(k1:k2, i)
          sendbuf(1:nk, L+1) = r(k1:k2, i)
          sendbuf(1:nk, L+2) = c(k1:k2, n-2+i)
          sendbuf(1:nk, L+3) = r(k1:k2, n-2+i)
          L = L + 4
        end do
        do i=1,2
          do j=1,2
            sendbuf(1:nk, L+0) = s1(k1:k2, i,j)
            sendbuf(1:nk, L+1) = s2(k1:k2, i,j)
            sendbuf(1:nk, L+2) = s1(k1:k2, n-2+i,j)
            sendbuf(1:nk, L+3) = s2(k1:k2, n-2+i,j)
            L = L + 4
          end do
        end do

      else ! n == 1 special case

        sendbuf(1:nk, L+0) = c(k1:k2, 1)
        sendbuf(1:nk, L+1) = r(k1:k2, 1)
        sendbuf(1:nk, L+2) = 1.
        sendbuf(1:nk, L+3) = 0.
        sendbuf(1:nk, L+4) = 1.
        sendbuf(1:nk, L+5) = 0.
        sendbuf(1:nk, L+6) = c(k1:k2, 1)
        sendbuf(1:nk, L+7) = r(k1:k2, 1)
        sendbuf(1:nk, L+8) = a(k1:k2, 1)
        sendbuf(1:nk, L+9) = d(k1:k2, 1)
        sendbuf(1:nk, L+10) = 0.
        sendbuf(1:nk, L+11) = 0.
        sendbuf(1:nk, L+12) = b(k1:k2, 1)
        sendbuf(1:nk, L+13) = e(k1:k2, 1)
        sendbuf(1:nk, L+14) = -1.
        sendbuf(1:nk, L+15) = 0.
        sendbuf(1:nk, L+16) = 0.
        sendbuf(1:nk, L+17) = -1.
        sendbuf(1:nk, L+18) = a(k1:k2, 1)
        sendbuf(1:nk, L+19) = d(k1:k2, 1)
        sendbuf(1:nk, L+20) = 0.
        sendbuf(1:nk, L+21) = 0.
        sendbuf(1:nk, L+22) = b(k1:k2, 1)
        sendbuf(1:nk, L+23) = e(k1:k2, 1)
        L = L + 24

      end if

      k1 = k2 + 1

    end do

    ! Gather the boundary data
    call MPI_AllToAll (sendbuf, nlot*24, MPI_REAL8, &
         recvbuf, nlot*24, MPI_REAL8, &
         icomm, ierr)
    deallocate(sendbuf)

    ! Build reduced matrix
    allocate(AA(nlot, -5:5 + 1, 4*nproc))
    AA = 0.
    L = 1
    do k=1,nproc

      m = 4*(k-1)

      do i=1,2
        AA(:, 0, m+i)   = recvbuf(:, L+0)    ! c(i)
        AA(:, 6, m+i)   = recvbuf(:, L+1)    ! r(i)
        AA(:, 0, m+i+2) = recvbuf(:, L+2)    ! c(n-2+i)
        AA(:, 6, m+i+2) = recvbuf(:, L+3)    ! r(n-2+i)
        L = L + 4
      end do
      do i=1,2
        do j=1,2
          AA(:, -2+j-i, m+i)   = recvbuf(:, L+0)    ! s1(i,j)
          AA(:,  4+j-i, m+i)   = recvbuf(:, L+1)    ! s2(i,j)
          AA(:, -4+j-i, m+i+2) = recvbuf(:, L+2)    ! s1(n-2+i,j)
          AA(:,  2+j-i, m+i+2) = recvbuf(:, L+3)    ! s2(n-2+i,j)
          L = L + 4
        end do
      end do

    end do

    ! Clear unused values
    nk = ngroup(irank+1)
    AA(nk+1:nlot, :, :) = 0.
    AA(nk+1:nlot, 0, :) = 1.

    ! Solve reduced systems
    call polyDiagonalM_1(5, AA(1,-5,3), (2*nproc-2)*2, nlot)

    ! Move solution to beginning of recvbuf
    recvbuf(:, 1:4*nproc) = AA(:, 6, :)
    deallocate(AA)

    ! Permute the order
    do i=1,nproc-1
      swap(1:nlot,:) = recvbuf(:, 4*i-1:4*i)
      recvbuf(:, 4*i-1:4*i) = recvbuf(:, 4*i+1:4*i+2)
      recvbuf(:, 4*i+1:4*i+2) = swap(1:nlot,:)
    end do

    ! Scatter back the solution
    allocate(sendbuf(nlot,4*nproc))
    call MPI_AllToAll (recvbuf, nlot*4, MPI_DOUBLE_PRECISION, &
         sendbuf, nlot*4, MPI_DOUBLE_PRECISION, &
         icomm, ierr)

    L = 1
    k1 = 1
    do igroup=1,nproc
      k2 = k1+ngroup(igroup)-1
      nk = k2-k1+1

      r1(k1:k2, :) = sendbuf(1:nk, L+0:L+1)
      r2(k1:k2, :) = sendbuf(1:nk, L+2:L+3)
      L = L + 4

      k1 = k2 + 1
    end do

    if (irank == 0) r1 = 0.
    if (irank == nproc-1) r2 = 0.

    if (n > 1) then ! do normal stuff

      do j=1,2
        do i=1,n
          r(:,i) = r(:,i) - s1(:,i,j)*r1(:,j) - s2(:,i,j)*r2(:,j)
        end do
      end do
      r = r / c

    else ! n == 1 special case

      r(:,1) = ( r(:,1) - a(:,1)*r1(:,1) - b(:,1)*r1(:,2) &
           -d(:,1)*r2(:,1) - e(:,1)*r2(:,2) )/c(:,1)

    end if

    deallocate(sendbuf)
    deallocate(recvbuf)
    deallocate(ngroup)
    deallocate(a,b,c,d,e)

    return
  end subroutine MPI_pentaDiagonalM


  !=======================================================================
  subroutine MPI_pentaPeriodicM(asave, bsave, csave, dsave, esave, &
       r, n, lot, icomm)
    USE ModGlobal
    USE ModMPI
    !real(rfreal) :: a(lot,n), b(lot,n), c(lot,n), d(lot,n), e(lot,n)
    Integer :: icomm
    Integer :: lot, n
    real(rfreal) :: r(lot,n)
    real(rfreal), allocatable, dimension(:,:) :: a, b, c, d, e
    !real(rfreal) :: asave(lot,n), bsave(lot,n), csave(lot,n), dsave(lot,n)
    !real(rfreal) :: esave(lot,n)
    real(rfreal), pointer :: asave(:,:), bsave(:,:), csave(:,:), &
         dsave(:,:), esave(:,:)
    real(rfreal) :: const(lot)
    real(rfreal) :: s1(lot,n,2), s2(lot,n,2)
    real(rfreal) :: r1(lot,2), r2(lot,2), swap(lot,2)
    real(rfreal), allocatable :: sendbuf(:,:), recvbuf(:,:)
    real(rfreal), allocatable :: AA(:,:,:)
    integer, allocatable :: ngroup(:)
    integer :: nproc, irank, nremain, nlot, i, j, k, l, m, k1, igroup, k2, nk
    integer :: ierr

    if (n < 1) stop "MPI_pentaPeriodicM: n < 1"

    ! Get communicator info
    call MPI_comm_size (icomm, nproc, ierr)
    call MPI_comm_rank (icomm, irank, ierr)

    ! Partition the lot
    if (lot < nproc) &
         stop "MPI_pentaDiagonalM: lot must be >= nproc"
    allocate(ngroup(nproc))
    ngroup(:) = lot/nproc
    nremain = mod(lot,nproc)
    ngroup(1:nremain) = ngroup(1:nremain) + 1
    nlot = ngroup(1)

    allocate(sendbuf(nlot,24*nproc))
    allocate(recvbuf(nlot,24*nproc))
    
    allocate(a(lot,n),b(lot,n),c(lot,n),d(lot,n),e(lot,n))

    do i = 1, lot
      do j = 1, n
        a(i,j) = asave(i,j)
        b(i,j) = bsave(i,j)
        c(i,j) = csave(i,j) 
        d(i,j) = dsave(i,j)
        e(i,j) = esave(i,j)
      end do
    end do

    if (n > 1) then ! do normal stuff

      ! Initialize boundary values
      s1(:,1,1) = a(:,1)
      s1(:,1,2) = b(:,1)
      s1(:,2,1) = 0.
      s1(:,2,2) = a(:,2)
      s2(:,n-1,1) = e(:,n-1)
      s2(:,n-1,2) = 0.
      s2(:,n,1) = d(:,n)
      s2(:,n,2) = e(:,n)

      ! Forward elimination
      ! Upper boundary in s1(:,i,1:2)
      do i=1,n-2
        ! Eliminate a(i+2)
        const(:) = a(:,i+2)/c(:,i)
        b(:,i+2) = b(:,i+2) - d(:,i)*const(:)
        c(:,i+2) = c(:,i+2) - e(:,i)*const(:)
        r(:,i+2) = r(:,i+2) - r(:,i)*const(:)
        s1(:,i+2,1) = -s1(:,i,1)*const(:)
        s1(:,i+2,2) = -s1(:,i,2)*const(:)

        ! Eliminate b(i+1)
        const(:) = b(:,i+1)/c(:,i)
        c(:,i+1) = c(:,i+1) - d(:,i)*const(:)
        d(:,i+1) = d(:,i+1) - e(:,i)*const(:)
        r(:,i+1) = r(:,i+1) - r(:,i)*const(:)
        s1(:,i+1,1) = s1(:,i+1,1) - s1(:,i,1)*const(:)
        s1(:,i+1,2) = s1(:,i+1,2) - s1(:,i,2)*const(:)
      end do
      ! Eliminate b(n)
      const(:) = b(:,n)/c(:,n-1)
      c(:,n) = c(:,n) - d(:,n-1)*const(:)
      r(:,n) = r(:,n) - r(:,n-1)*const(:)
      s1(:,n,1) = s1(:,n,1) - s1(:,n-1,1)*const(:)
      s1(:,n,2) = s1(:,n,2) - s1(:,n-1,2)*const(:)
      s2(:,n,1) = s2(:,n,1) - s2(:,n-1,1)*const(:)

      ! Backward elimination
      ! Lower boundary in s2(:,i,1:2)
      do i=n,3,-1
        ! Eliminate e(i-2)
        const(:) = e(:,i-2)/c(:,i)
        r(:,i-2) = r(:,i-2) - r(:,i)*const(:)
        s1(:,i-2,1) = s1(:,i-2,1) - s1(:,i,1)*const(:)
        s1(:,i-2,2) = s1(:,i-2,2) - s1(:,i,2)*const(:)
        s2(:,i-2,1) = -s2(:,i,1)*const(:)
        s2(:,i-2,2) = -s2(:,i,2)*const(:)

        ! Eliminate d(i-1)
        const(:) = d(:,i-1)/c(:,i)
        r(:,i-1) = r(:,i-1) - r(:,i)*const(:)
        s1(:,i-1,1) = s1(:,i-1,1) - s1(:,i,1)*const(:)
        s1(:,i-1,2) = s1(:,i-1,2) - s1(:,i,2)*const(:)
        s2(:,i-1,1) = s2(:,i-1,1) - s2(:,i,1)*const(:)
        s2(:,i-1,2) = s2(:,i-1,2) - s2(:,i,2)*const(:)
      end do
      ! Eliminate d(1)
      const(:) = d(:,1)/c(:,2)
      r(:,1) = r(:,1) - r(:,2)*const(:)
      s1(:,1,1) = s1(:,1,1) - s1(:,2,1)*const(:)
      s1(:,1,2) = s1(:,1,2) - s1(:,2,2)*const(:)
      s2(:,1,1) = s2(:,1,1) - s2(:,2,1)*const(:)
      s2(:,1,2) = s2(:,1,2) - s2(:,2,2)*const(:)

    end if ! n > 1

    ! All dependence has been shifted to the boundary elements
    ! Communicate boundary values to root process
    ! and solve reduced 11-diagonal system
    ! USE of 11-diagonal system is more robust than the
    ! reordered (removes zeros) 7-diagonal system

    ! Send rows of 11-diagonal system
    ! (0, 0, 0, a, b, c, 0, 0, 0, d, e; r)
    !    (0, 0, a, b, 0, c, 0, 0, d, e, 0; r)
    !       (0, a, b, 0, 0, c, 0, d, e, 0, 0; r)
    !          (a, b, 0, 0, 0, c, d, e, 0, 0, 0; r)
    ! For efficiency, only send non-zero elements

    L = 1
    k1 = 1
    do igroup=1,nproc
      k2 = k1+ngroup(igroup)-1
      nk = k2-k1+1

      if (n > 1) then ! do normal stuff

        do i=1,2
          sendbuf(1:nk, L+0) = c(k1:k2, i)
          sendbuf(1:nk, L+1) = r(k1:k2, i)
          sendbuf(1:nk, L+2) = c(k1:k2, n-2+i)
          sendbuf(1:nk, L+3) = r(k1:k2, n-2+i)
          L = L + 4
        end do
        do i=1,2
          do j=1,2
            sendbuf(1:nk, L+0) = s1(k1:k2, i,j)
            sendbuf(1:nk, L+1) = s2(k1:k2, i,j)
            sendbuf(1:nk, L+2) = s1(k1:k2, n-2+i,j)
            sendbuf(1:nk, L+3) = s2(k1:k2, n-2+i,j)
            L = L + 4
          end do
        end do


      else ! n == 1 special case

        sendbuf(1:nk, L+0) = c(k1:k2, 1)
        sendbuf(1:nk, L+1) = r(k1:k2, 1)
        sendbuf(1:nk, L+2) = 1.
        sendbuf(1:nk, L+3) = 0.
        sendbuf(1:nk, L+4) = 1.
        sendbuf(1:nk, L+5) = 0.
        sendbuf(1:nk, L+6) = c(k1:k2, 1)
        sendbuf(1:nk, L+7) = r(k1:k2, 1)
        sendbuf(1:nk, L+8) = a(k1:k2, 1)
        sendbuf(1:nk, L+9) = d(k1:k2, 1)
        sendbuf(1:nk, L+10) = 0.
        sendbuf(1:nk, L+11) = 0.
        sendbuf(1:nk, L+12) = b(k1:k2, 1)
        sendbuf(1:nk, L+13) = e(k1:k2, 1)
        sendbuf(1:nk, L+14) = -1.
        sendbuf(1:nk, L+15) = 0.
        sendbuf(1:nk, L+16) = 0.
        sendbuf(1:nk, L+17) = -1.
        sendbuf(1:nk, L+18) = a(k1:k2, 1)
        sendbuf(1:nk, L+19) = d(k1:k2, 1)
        sendbuf(1:nk, L+20) = 0.
        sendbuf(1:nk, L+21) = 0.
        sendbuf(1:nk, L+22) = b(k1:k2, 1)
        sendbuf(1:nk, L+23) = e(k1:k2, 1)
        L = L + 24

      end if

      k1 = k2 + 1
    end do

    ! Gather the boundary data
    call MPI_AllToAll (sendbuf, nlot*24, MPI_REAL8, &
         recvbuf, nlot*24, MPI_REAL8, &
         icomm, ierr)
    deallocate(sendbuf)

    ! Build reduced matrix
    allocate(AA(nlot, -5:5 + 1, 4*nproc))
    AA = 0.
    L = 1
    do k=1,nproc
      m = 4*(k-1)

      do i=1,2
        AA(:, 0, m+i)   = recvbuf(:, L+0)    ! c(i)
        AA(:, 6, m+i)   = recvbuf(:, L+1)    ! r(i)
        AA(:, 0, m+i+2) = recvbuf(:, L+2)    ! c(n-2+i)
        AA(:, 6, m+i+2) = recvbuf(:, L+3)    ! r(n-2+i)
        L = L + 4
      end do
      do i=1,2
        do j=1,2
          AA(:, -2+j-i, m+i)   = recvbuf(:, L+0)    ! s1(i,j)
          AA(:,  4+j-i, m+i)   = recvbuf(:, L+1)    ! s2(i,j)
          AA(:, -4+j-i, m+i+2) = recvbuf(:, L+2)    ! s1(n-2+i,j)
          AA(:,  2+j-i, m+i+2) = recvbuf(:, L+3)    ! s2(n-2+i,j)
          L = L + 4
        end do
      end do

    end do

    ! Clear unused values
    nk = ngroup(irank+1)
    AA(nk+1:nlot, :, :) = 0.
    AA(nk+1:nlot, 0, :) = 1.

    ! Solve reduced systems
    call polyPeriodicM_1(5, AA(1,-5,1), (2*nproc)*2, nlot)

    ! Move solution to beginning of recvbuf
    recvbuf(:, 1:4*nproc) = AA(:, 6, :)
    deallocate(AA)

    ! Permute the order
    do i=1,nproc-1
      swap(1:nlot,:) = recvbuf(:, 4*i-1:4*i)
      recvbuf(:, 4*i-1:4*i) = recvbuf(:, 4*i+1:4*i+2)
      recvbuf(:, 4*i+1:4*i+2) = swap(1:nlot,:)
    end do
    ! Don't forget end points
    swap(1:nlot,:) = recvbuf(:, 4*nproc-1:4*nproc)
    recvbuf(:, 4*nproc-1:4*nproc) = recvbuf(:, 1:2)
    recvbuf(:, 1:2) = swap(1:nlot,:)

    ! Scatter back the solution
    allocate(sendbuf(nlot,4*nproc))
    call MPI_AllToAll (recvbuf, nlot*4, MPI_REAL8, &
         sendbuf, nlot*4, MPI_REAL8, &
         icomm, ierr)

    L = 1
    k1 = 1
    do igroup=1,nproc
      k2 = k1+ngroup(igroup)-1
      nk = k2-k1+1

      r1(k1:k2, :) = sendbuf(1:nk, L+0:L+1)
      r2(k1:k2, :) = sendbuf(1:nk, L+2:L+3)
      L = L + 4

      k1 = k2 + 1
    end do

    if (n > 1) then ! do normal stuff

      do j=1,2
        do i=1,n
          r(:,i) = r(:,i) - s1(:,i,j)*r1(:,j) - s2(:,i,j)*r2(:,j)
        end do
      end do
      r = r / c

    else ! n == 1 special case

      r(:,1) = ( r(:,1) - a(:,1)*r1(:,1) - b(:,1)*r1(:,2) &
           -d(:,1)*r2(:,1) - e(:,1)*r2(:,2) )/c(:,1)

    end if

    deallocate(sendbuf)
    deallocate(recvbuf)
    deallocate(ngroup)
    deallocate(a,b,c,d,e)

    return
  end subroutine MPI_pentaPeriodicM

  !=======================================================================
  subroutine polyDiagonalM_1(nd, A, n, lot)
    USE ModGlobal
    integer :: n, lot, nd, i, j, k
    real(rfreal) :: A(lot, -nd:nd + 1, n)
    real(rfreal) :: const(lot), pivot(lot)

    ! Forward elimination
    do i=1,n-1
      pivot(:) = 1./A(:,0,i)
      do j=1,min(nd,n-i)
        const(:) = A(:,-j,i+j)*pivot(:)
        do k=1,min(nd,n-i)
          A(:,-j+k,i+j) = A(:,-j+k,i+j) - A(:,k,i)*const(:)
        end do
        A(:,nd+1,i+j) = A(:,nd+1,i+j) - A(:,nd+1,i)*const(:)
      end do
    end do

    ! Back-substitution
    do i=n,1,-1
      do j=1,min(nd,n-i)
        A(:,nd+1,i) = A(:,nd+1,i) - A(:,j,i)*A(:,nd+1,i+j)
      end do
      A(:,nd+1,i) = A(:,nd+1,i)/A(:,0,i)
    end do

    return
  end subroutine polyDiagonalM_1



  !=======================================================================
  subroutine polyPeriodicM_1(nd, A, n, lot)

    USE ModGlobal
    Integer :: lot, n, nd
    real(rfreal) :: d(lot,-nd:nd,n), r(lot,n)
    real(rfreal) :: A(lot,-nd:nd + 1, n)
    ! Storage for the "bridge" or "boundary"
    real(rfreal) :: b(lot,nd,n)
    real(rfreal) :: const(lot), pivot(lot)
    integer :: j, i, k, i0

    if (n==1) then
      const(:) = 0.
      do j=-nd,nd
        const(:) = const(:) + A(:,1,j)
      end do
      A(:,nd+1,1) = A(:,nd+1,1) / const(:)
      return
    end if

    !print *, nd, n, lot
    !print *, size(A,1), size(A,2), size(A,3)

    if ((n+1) <= 2*nd) stop 'Increase # of processors for polyPeriodicM_1'

    ! Setup bridge array
    b = 0.
    do i=1,nd
      do j=i,nd
        !print *, -nd+j-i, n-nd-i+1 ! <=== n-nd-i+1 can equal 0 for n too small
        b(:,j,i) = A(:,-nd+j-i,i)
        b(:,j-i+1,n-nd-i+1) = A(:,j,n-nd-i+1)
      end do
    end do
    do i=n-nd+1,n
      do j=1,nd
        b(:,j,i) = A(:,-nd+j-(i-n),i)
      end do
    end do

    ! Forward elimination
    do i=1,n-nd
      pivot(:) = 1./A(:,0,i)
      do j=1,nd
        const(:) = A(:,-j,i+j)*pivot(:)
        do k=1,nd ! min(nd,n-nd-i)
          A(:,-j+k,i+j) = A(:,-j+k,i+j) - A(:,k,i)*const(:)
          ! end do
          ! do k=1,nd
          b(:,k,i+j) = b(:,k,i+j) - b(:,k,i)*const(:)
        end do
        A(:,nd+1,i+j) = A(:,nd+1,i+j) - A(:,nd+1,i)*const(:)
      end do
    end do

    ! Backward elimination
    do i=n-nd,1,-1
      pivot(:) = 1./A(:,0,i)
      do j=1,min(nd,i-1)
        const(:) = A(:,j,i-j)*pivot(:)
        do k=1,nd
          b(:,k,i-j) = b(:,k,i-j) - b(:,k,i)*const(:)
        end do
        A(:,nd+1,i-j) = A(:,nd+1,i-j) - A(:,nd+1,i)*const(:)
      end do
    end do

    ! Eliminate oddball region
    do i=1,nd
      pivot(:) = 1./A(:,0,i)
      do j=i,nd
        const(:) = A(:,j,n-j+i)*pivot(:)
        do k=1,nd
          b(:,k,n-j+i) = b(:,k,n-j+i) - b(:,k,i)*const(:)
        end do
        A(:,nd+1,n-j+i) = A(:,nd+1,n-j+i) - A(:,nd+1,i)*const(:)
      end do
    end do

    ! Elimination for corner matrix
    i0 = n-nd
    do i=1,nd-1
      pivot(:) = 1./b(:,i,i+i0)
      do j=i+1,nd
        const(:) = b(:,i,j+i0)*pivot(:)
        do k=i+1,nd
          b(:,k,j+i0) = b(:,k,j+i0) - b(:,k,i+i0)*const(:)
        end do
        A(:,nd+1,j+i0) = A(:,nd+1,j+i0) - A(:,nd+1,i+i0)*const(:)
      end do
    end do

    ! Back-substitution for corner matrix
    i0 = n-nd
    do i=nd,1,-1
      do j=i+1,nd
        A(:,nd+1,i+i0) = A(:,nd+1,i+i0) - b(:,j,i+i0)*A(:,nd+1,j+i0)
      end do
      A(:,nd+1,i+i0) = A(:,nd+1,i+i0)/b(:,i,i+i0)
    end do

    ! Back-substitution for bridge
    do i=n-nd,1,-1
      do j=1,nd
        A(:,nd+1,i) = A(:,nd+1,i) - b(:,j,i)*A(:,nd+1,n-nd+j)
      end do
      A(:,nd+1,i) = A(:,nd+1,i)/A(:,0,i)
    end do

    return
  end subroutine polyPeriodicM_1

  !=======================================================================
  ! Penta-diagonal solvers
  !
  ! Charles Pierce, June 1995
  !
  ! pentaDiagonalM
  ! pentaDiagonalLUM
  ! pentaLUsolveM
  ! pentaPeriodicM
  ! pentaDiagonal_1
  ! pentaDiagonalM_1
  ! pentaDiagonalLUM_1
  ! pentaLUsolveM_1
  ! pentaPeriodicM_1
  !

  !=======================================================================
  ! pentaDiagonalM
  ! Solves a penta-diagonal system of algebraic equations.  The banded 
  ! matrix given by (a,b,c,d,e) is destroyed, and the rhs vector r is 
  ! replaced by the solution vector.
  !
  ! Multiple systems, different (a,b,c,d,e) for each system.
  !
  subroutine pentaDiagonalM(asave, bsave, csave, dsave, esave, r, n, lot)
    USE ModGlobal
    !real(rfreal) :: a(lot,n), b(lot,n), c(lot,n), &
    !     d(lot,n), e(lot,n), r(lot,n)
    integer :: lot, n
    real(rfreal), allocatable, dimension(:,:) :: a, b, c, d, e
    real(rfreal) :: r(lot,n)
    !real(rfreal) :: asave(lot,n), bsave(lot,n), csave(lot,n), &
    !     dsave(lot,n), esave(lot,n)
    real(rfreal), pointer :: asave(:,:), bsave(:,:), csave(:,:), &
         dsave(:,:), esave(:,:)
    real(rfreal) :: const(lot)
    integer :: i, j

    allocate(a(lot,n),b(lot,n),c(lot,n),d(lot,n),e(lot,n))

    do i = 1, lot
      do j = 1, n
        a(i,j) = asave(i,j)
        b(i,j) = bsave(i,j)
        c(i,j) = csave(i,j) 
        d(i,j) = dsave(i,j)
        e(i,j) = esave(i,j)
      end do
    end do

    if (n == 1) then
      ! Solve 1x1 system
      r(:,1) = r(:,1)/c(:,1)
      return
    else if (n == 2) then
      ! Solve 2x2 system
      const(:) = b(:,2)/c(:,1)
      c(:,2) = c(:,2) - d(:,1)*const(:)
      r(:,2) = r(:,2) - r(:,1)*const(:)
      r(:,2) = r(:,2)/c(:,2)
      r(:,1) = (r(:,1) - d(:,1)*r(:,2))/c(:,1)
      return
    end if

    ! Forward elimination
    do i=1,n-2
      ! Eliminate b(i+1)
      const(:) = b(:,i+1)/c(:,i)
      c(:,i+1) = c(:,i+1) - d(:,i)*const(:)
      d(:,i+1) = d(:,i+1) - e(:,i)*const(:)
      r(:,i+1) = r(:,i+1) - r(:,i)*const(:)

      ! Eliminate a(i+2)
      const(:) = a(:,i+2)/c(:,i)
      b(:,i+2) = b(:,i+2) - d(:,i)*const(:)
      c(:,i+2) = c(:,i+2) - e(:,i)*const(:)
      r(:,i+2) = r(:,i+2) - r(:,i)*const(:)
    end do
    ! Eliminate b(n)
    const(:) = b(:,n)/c(:,n-1)
    c(:,n) = c(:,n) - d(:,n-1)*const(:)
    r(:,n) = r(:,n) - r(:,n-1)*const(:)

    ! Back-substitution
    r(:,n) = r(:,n)/c(:,n)
    r(:,n-1) = (r(:,n-1) - d(:,n-1)*r(:,n))/c(:,n-1)
    do i=n-2,1,-1
      r(:,i) = (r(:,i) - d(:,i)*r(:,i+1) - e(:,i)*r(:,i+2))/c(:,i)
    end do

    deallocate(a,b,c,d,e)

    return
  end subroutine pentaDiagonalM

  !=======================================================================
  ! pentaDiagonalLUM
  ! Computes the LU decomposition if a penta-diagonal matrix using 
  ! Guass elimination.
  !
  ! Multiple systems
  !
  subroutine pentaDiagonalLUM(a, b, c, d, e, n, lot)
    USE ModGlobal
    integer :: n, lot
    real(rfreal) :: a(lot,n), b(lot,n), c(lot,n), d(lot,n), e(lot,n)
    integer :: i

    if (n == 1) then
      ! 1x1 system
      c = 1./c
      return
    else if (n == 2) then
      ! 2x2 system
      b(:,2) = b(:,2)/c(:,1)
      c(:,2) = c(:,2) - d(:,1)*b(:,2)
      c = 1./c
      return
    end if

    ! Forward elimination
    do i=1,n-2
      ! Eliminate b(i+1)
      b(:,i+1) = b(:,i+1)/c(:,i)
      c(:,i+1) = c(:,i+1) - d(:,i)*b(:,i+1)
      d(:,i+1) = d(:,i+1) - e(:,i)*b(:,i+1)

      ! Eliminate a(i+2)
      a(:,i+2) = a(:,i+2)/c(:,i)
      b(:,i+2) = b(:,i+2) - d(:,i)*a(:,i+2)
      c(:,i+2) = c(:,i+2) - e(:,i)*a(:,i+2)
    end do
    ! Eliminate b(n)
    b(:,n) = b(:,n)/c(:,n-1)
    c(:,n) = c(:,n) - d(:,n-1)*b(:,n)

    ! Pre-divide diagonal
    c = 1./c

    return
  end subroutine pentaDiagonalLUM

  !=======================================================================
  ! pentaLUSolveM
  ! Solves a penta-diagonal system, given the LU decomposition.
  !
  ! Multiple systems
  !
  subroutine pentaLUSolveM(a, b, c, d, e, r, n, lot)
    USE ModGlobal
    integer :: n, lot
    real(rfreal) :: a(lot,n), b(lot,n), c(lot,n), &
         d(lot,n), e(lot,n), r(lot,n)
    integer :: i

    if (n == 1) then
      ! Solve 1x1 system
      r(:,1) = r(:,1)*c(:,1)
      return
    else if (n == 2) then
      ! Solve 2x2 system
      r(:,2) = (r(:,2) - r(:,1)*b(:,2))*c(:,2)
      r(:,1) = (r(:,1) - d(:,1)*r(:,2))*c(:,1)
      return
    end if

    ! Forward substitution
    do i=1,n-2
      r(:,i+1) = r(:,i+1) - r(:,i)*b(:,i+1)
      r(:,i+2) = r(:,i+2) - r(:,i)*a(:,i+2)
    end do
    r(:,n) = r(:,n) - r(:,n-1)*b(:,n)

    ! Backward substitution
    ! Diagonal has been pre-divided
    r(:,n) = r(:,n)*c(:,n)
    r(:,n-1) = (r(:,n-1) - d(:,n-1)*r(:,n))*c(:,n-1)
    do i=n-2,1,-1
      r(:,i) = (r(:,i) - d(:,i)*r(:,i+1) - e(:,i)*r(:,i+2))*c(:,i)
    end do

    return
  end subroutine pentaLUSolveM

  !=======================================================================
  ! pentaPeriodicM
  ! Solves multiple periodic penta-diagonal systems
  !
  subroutine pentaPeriodicM(asave, bsave, csave, dsave, esave, r, n, lot)
    USE ModGlobal
    !real(rfreal) :: a(lot,n), b(lot,n), c(lot,n), d(lot,n), e(lot,n)
    integer :: n, lot
    real(rfreal) :: r(lot,n)
    real(rfreal), allocatable, dimension(:,:) :: a, b, c, d, e
    !real(rfreal) :: asave(lot,n), bsave(lot,n), csave(lot,n), &
    !     dsave(lot,n), esave(lot,n)
    real(rfreal), pointer :: asave(:,:), bsave(:,:), csave(:,:), &
         dsave(:,:), esave(:,:)
    real(rfreal) :: s1(lot,n), s2(lot,n)
    real(rfreal) :: const(lot)
    integer :: i, j

    allocate(a(lot,n),b(lot,n),c(lot,n),d(lot,n),e(lot,n))

    do i = 1, lot
      do j = 1, n
        a(i,j) = asave(i,j)
        b(i,j) = bsave(i,j)
        c(i,j) = csave(i,j) 
        d(i,j) = dsave(i,j)
        e(i,j) = esave(i,j)
      end do
    end do

    if (n == 1) then
      ! Solve 1x1 system
      r = r/(a + b + c + d + e)
      return
    else if (n == 2) then
      ! Solve 2x2 system
      c = c + a + e
      d(:,1) = d(:,1) + b(:,1)
      b(:,2) = b(:,2) + d(:,2)
      const(:) = b(:,2)/c(:,1)
      c(:,2) = c(:,2) - d(:,1)*const(:)
      r(:,2) = r(:,2) - r(:,1)*const(:)
      r(:,2) = r(:,2)/c(:,2)
      r(:,1) = (r(:,1) - d(:,1)*r(:,2))/c(:,1)
      return
    else if (n == 3) then
      b = b + e
      d = d + a
      call triPeriodicM(b, c, d, r, n, lot)
      return
    else if (n == 4) then
      a = a + e
      e = 0.
    end if

    ! Initialize boundary data
    s1 = 0.
    s1(:,1) = a(:,1)
    s1(:,n-3) = s1(:,n-3) + e(:,n-3)
    s1(:,n-2) = d(:,n-2)
    s1(:,n-1) = c(:,n-1)
    s1(:,n) = b(:,n)
    s2 = 0.
    s2(:,1) = b(:,1)
    s2(:,2) = a(:,2)
    s2(:,n-2) = s2(:,n-2) + e(:,n-2)
    s2(:,n-1) = d(:,n-1)
    s2(:,n) = c(:,n)

    ! Forward elimination
    do i=1,n-2
      ! Eliminate b(i+1)
      const(:) = b(:,i+1)/c(:,i)
      c(:,i+1) = c(:,i+1) - d(:,i)*const(:)
      d(:,i+1) = d(:,i+1) - e(:,i)*const(:)
      r(:,i+1) = r(:,i+1) - r(:,i)*const(:)
      s1(:,i+1) = s1(:,i+1) - s1(:,i)*const(:)
      s2(:,i+1) = s2(:,i+1) - s2(:,i)*const(:)

      ! Eliminate a(i+2)
      const(:) = a(:,i+2)/c(:,i)
      b(:,i+2) = b(:,i+2) - d(:,i)*const(:)
      c(:,i+2) = c(:,i+2) - e(:,i)*const(:)
      r(:,i+2) = r(:,i+2) - r(:,i)*const(:)
      s1(:,i+2) = s1(:,i+2) - s1(:,i)*const(:)
      s2(:,i+2) = s2(:,i+2) - s2(:,i)*const(:)
    end do

    ! Backward elimination
    do i=n-2,3,-1
      ! Eliminate d(i-1)
      const(:) = d(:,i-1)/c(:,i)
      r(:,i-1) = r(:,i-1) - r(:,i)*const(:)
      s1(:,i-1) = s1(:,i-1) - s1(:,i)*const(:)
      s2(:,i-1) = s2(:,i-1) - s2(:,i)*const(:)

      ! Eliminate e(i-2)
      const(:) = e(:,i-2)/c(:,i)
      r(:,i-2) = r(:,i-2) - r(:,i)*const(:)
      s1(:,i-2) = s1(:,i-2) - s1(:,i)*const(:)
      s2(:,i-2) = s2(:,i-2) - s2(:,i)*const(:)
    end do
    i=2
    ! Eliminate d(i-1)
    const(:) = d(:,i-1)/c(:,i)
    r(:,i-1) = r(:,i-1) - r(:,i)*const(:)
    s1(:,i-1) = s1(:,i-1) - s1(:,i)*const(:)
    s2(:,i-1) = s2(:,i-1) - s2(:,i)*const(:)

    ! Eliminate oddball region
    const(:) = e(:,n-1)/c(:,1)
    r(:,n-1) = r(:,n-1) - r(:,1)*const(:)
    s1(:,n-1) = s1(:,n-1) - s1(:,1)*const(:)
    s2(:,n-1) = s2(:,n-1) - s2(:,1)*const(:)

    const(:) = d(:,n)/c(:,1)
    r(:,n) = r(:,n) - r(:,1)*const(:)
    s1(:,n) = s1(:,n) - s1(:,1)*const(:)
    s2(:,n) = s2(:,n) - s2(:,1)*const(:)

    const(:) = e(:,n)/c(:,2)
    r(:,n) = r(:,n) - r(:,2)*const(:)
    s1(:,n) = s1(:,n) - s1(:,2)*const(:)
    s2(:,n) = s2(:,n) - s2(:,2)*const(:)

    ! Eliminate corner region
    const(:) = s1(:,n)/s1(:,n-1)
    r(:,n) = r(:,n) - r(:,n-1)*const(:)
    s2(:,n) = s2(:,n) - s2(:,n-1)*const(:)

    r(:,n) = r(:,n)/s2(:,n)
    r(:,n-1) = (r(:,n-1) - s2(:,n-1)*r(:,n))/s1(:,n-1)
    do i=n-2,1,-1
      r(:,i) = (r(:,i) - s1(:,i)*r(:,n-1) - s2(:,i)*r(:,n))/c(:,i)
    end do

    deallocate(a,b,c,d,e)

    return
  end subroutine pentaPeriodicM

  !=======================================================================
  ! pentaDiagonal_1
  !
  ! Combined matrix-rhs into single array
  !
  subroutine pentaDiagonal_1(A, n)
    USE ModGlobal
    integer :: n
    real(rfreal) :: A(6,n), const
    integer :: i

    if (n == 1) then
      ! Solve 1x1 system
      A(6,1) = A(6,1)/A(3,1)
      return
    else if (n == 2) then
      ! Solve 2x2 system
      const = A(2,2)/A(3,1)
      A(3,2) = A(3,2) - A(4,1)*const
      A(6,2) = A(6,2) - A(6,1)*const
      A(6,2) = A(6,2)/A(3,2)
      A(6,1) = (A(6,1) - A(4,1)*A(6,2))/A(3,1)
      return
    end if

    ! Forward elimination
    do i=1,n-2
      ! Eliminate A(2,i+1)
      const = A(2,i+1)/A(3,i)
      A(3,i+1) = A(3,i+1) - A(4,i)*const
      A(4,i+1) = A(4,i+1) - A(5,i)*const
      A(6,i+1) = A(6,i+1) - A(6,i)*const

      ! Eliminate A(1,i+2)
      const = A(1,i+2)/A(3,i)
      A(2,i+2) = A(2,i+2) - A(4,i)*const
      A(3,i+2) = A(3,i+2) - A(5,i)*const
      A(6,i+2) = A(6,i+2) - A(6,i)*const
    end do
    ! Eliminate A(2,n)
    const = A(2,n)/A(3,n-1)
    A(3,n) = A(3,n) - A(4,n-1)*const
    A(6,n) = A(6,n) - A(6,n-1)*const

    ! Back-substitution
    A(6,n) = A(6,n)/A(3,n)
    A(6,n-1) = (A(6,n-1) - A(4,n-1)*A(6,n))/A(3,n-1)
    do i=n-2,1,-1
      A(6,i) = (A(6,i) - A(4,i)*A(6,i+1) - A(5,i)*A(6,i+2))/A(3,i)
    end do

    return
  end subroutine pentaDiagonal_1

  !=======================================================================
  ! pentaDiagonalM_1
  !
  ! Combined matrix-rhs into single array
  !
  subroutine pentaDiagonalM_1(A, n, lot)
    USE ModGlobal
    integer :: n, lot
    real(rfreal) :: A(lot,6,n)
    real(rfreal) :: const(lot)
    integer :: i

    if (n == 1) then
      ! Solve 1x1 system
      A(:,6,1) = A(:,6,1)/A(:,3,1)
      return
    else if (n == 2) then
      ! Solve 2x2 system
      const(:) = A(:,2,2)/A(:,3,1)
      A(:,3,2) = A(:,3,2) - A(:,4,1)*const(:)
      A(:,6,2) = A(:,6,2) - A(:,6,1)*const(:)
      A(:,6,2) = A(:,6,2)/A(:,3,2)
      A(:,6,1) = (A(:,6,1) - A(:,4,1)*A(:,6,2))/A(:,3,1)
      return
    end if

    ! Forward elimination
    do i=1,n-2
      ! Eliminate A(2,i+1)
      const(:) = A(:,2,i+1)/A(:,3,i)
      A(:,3,i+1) = A(:,3,i+1) - A(:,4,i)*const(:)
      A(:,4,i+1) = A(:,4,i+1) - A(:,5,i)*const(:)
      A(:,6,i+1) = A(:,6,i+1) - A(:,6,i)*const(:)

      ! Eliminate A(1,i+2)
      const(:) = A(:,1,i+2)/A(:,3,i)
      A(:,2,i+2) = A(:,2,i+2) - A(:,4,i)*const(:)
      A(:,3,i+2) = A(:,3,i+2) - A(:,5,i)*const(:)
      A(:,6,i+2) = A(:,6,i+2) - A(:,6,i)*const(:)
    end do
    ! Eliminate A(2,n)
    const(:) = A(:,2,n)/A(:,3,n-1)
    A(:,3,n) = A(:,3,n) - A(:,4,n-1)*const(:)
    A(:,6,n) = A(:,6,n) - A(:,6,n-1)*const(:)

    ! Back-substitution
    A(:,6,n) = A(:,6,n)/A(:,3,n)
    A(:,6,n-1) = (A(:,6,n-1) - A(:,4,n-1)*A(:,6,n))/A(:,3,n-1)
    do i=n-2,1,-1
      A(:,6,i) = (A(:,6,i) - A(:,4,i)*A(:,6,i+1) - A(:,5,i)*A(:,6,i+2))/A(:,3,i)
    end do

    return
  end subroutine pentaDiagonalM_1

  !=======================================================================
  ! pentaDiagonalLUM_1
  !
  ! Combined matrix-rhs into single array
  !
  subroutine pentaDiagonalLUM_1(A, n, lot)
    USE ModGlobal
    integer :: n, lot
    real(rfreal) :: A(lot,5,n)
    integer :: i

    if (n == 1) then
      ! 1x1 system
      A(:,3,:) = 1./A(:,3,:)
      return
    else if (n == 2) then
      ! 2x2 system
      A(:,2,2) = A(:,2,2)/A(:,3,1)
      A(:,3,2) = A(:,3,2) - A(:,4,1)*A(:,2,2)
      A(:,3,:) = 1./A(:,3,:)
      return
    end if

    ! Forward elimination
    do i=1,n-2
      ! Eliminate b(i+1)
      A(:,2,i+1) = A(:,2,i+1)/A(:,3,i)
      A(:,3,i+1) = A(:,3,i+1) - A(:,4,i)*A(:,2,i+1)
      A(:,4,i+1) = A(:,4,i+1) - A(:,5,i)*A(:,2,i+1)

      ! Eliminate a(i+2)
      A(:,1,i+2) = A(:,1,i+2)/A(:,3,i)
      A(:,2,i+2) = A(:,2,i+2) - A(:,4,i)*A(:,1,i+2)
      A(:,3,i+2) = A(:,3,i+2) - A(:,5,i)*A(:,1,i+2)
    end do
    ! Eliminate b(n)
    A(:,2,n) = A(:,2,n)/A(:,3,n-1)
    A(:,3,n) = A(:,3,n) - A(:,4,n-1)*A(:,2,n)

    ! Pre-divide diagonal
    A(:,3,:) = 1./A(:,3,:)

    return
  end subroutine pentaDiagonalLUM_1

  !=======================================================================
  ! pentaLUSolveM_1
  !
  ! Combined matrix-rhs into single array
  !
  subroutine pentaLUSolveM_1(A, r, n, lot)
    USE ModGlobal
    integer :: n, lot
    real(rfreal) :: A(lot,5,n), r(lot,n)
    integer :: i

    if (n == 1) then
      ! Solve 1x1 system
      r(:,1) = r(:,1)*A(:,3,1)
      return
    else if (n == 2) then
      ! Solve 2x2 system
      r(:,2) = (r(:,2) - r(:,1)*A(:,2,2))*A(:,3,2)
      r(:,1) = (r(:,1) - A(:,4,1)*r(:,2))*A(:,3,1)
      return
    end if

    ! Forward substitution
    do i=1,n-2
      r(:,i+1) = r(:,i+1) - r(:,i)*A(:,2,i+1)
      r(:,i+2) = r(:,i+2) - r(:,i)*A(:,1,i+2)
    end do
    r(:,n) = r(:,n) - r(:,n-1)*A(:,2,n)

    ! Backward substitution
    ! Diagonal has been pre-divided
    r(:,n) = r(:,n)*A(:,3,n)
    r(:,n-1) = (r(:,n-1) - A(:,4,n-1)*r(:,n))*A(:,3,n-1)
    do i=n-2,1,-1
      r(:,i) = (r(:,i) - A(:,4,i)*r(:,i+1) - A(:,5,i)*r(:,i+2))*A(:,3,i)
    end do

    return
  end subroutine pentaLUSolveM_1

  !=======================================================================
  ! pentaPeriodicM_1
  ! Solves multiple periodic penta-diagonal systems
  !
  ! Combined matrix-rhs into single array
  !
  subroutine pentaPeriodicM_1(A, n, lot)
    USE ModGlobal
    integer :: n, lot
    real(rfreal) :: A(lot,6,n)
    real(rfreal) ::  s1(lot,n), s2(lot,n)
    real(rfreal) ::  const(lot)
    integer :: i

    if (n == 1) then
      ! Solve 1x1 system
      A(:,6,:) = A(:,6,:)/( A(:,1,:) + A(:,2,:) + A(:,3,:) &
           +A(:,4,:) + A(:,5,:) )
      return
    else if (n == 2) then
      ! Solve 2x2 system
      A(:,3,:) = A(:,3,:) + A(:,1,:) + A(:,5,:)
      A(:,4,1) = A(:,4,1) + A(:,2,1)
      A(:,2,2) = A(:,2,2) + A(:,4,2)
      const(:) = A(:,2,2)/A(:,3,1)
      A(:,3,2) = A(:,3,2) - A(:,4,1)*const(:)
      A(:,6,2) = A(:,6,2) - A(:,6,1)*const(:)
      A(:,6,2) = A(:,6,2)/A(:,3,2)
      A(:,6,1) = (A(:,6,1) - A(:,4,1)*A(:,6,2))/A(:,3,1)
      return
    else if (n == 3) then
      A(:,2,:) = A(:,2,:) + A(:,5,:)
      A(:,4,:) = A(:,4,:) + A(:,1,:)
      call triPeriodicM(A(:,2,:), A(:,3,:), A(:,4,:), A(:,6,:), n, lot)
      return
    else if (n == 4) then
      A(:,1,:) = A(:,1,:) + A(:,5,:)
      A(:,5,:) = 0.
    end if

    ! Initialize boundary data
    s1 = 0.
    s1(:,1) = A(:,1,1)
    s1(:,n-3) = s1(:,n-3) + A(:,5,n-3)
    s1(:,n-2) = A(:,4,n-2)
    s1(:,n-1) = A(:,3,n-1)
    s1(:,n) = A(:,2,n)
    s2 = 0.
    s2(:,1) = A(:,2,1)
    s2(:,2) = A(:,1,2)
    s2(:,n-2) = s2(:,n-2) + A(:,5,n-2)
    s2(:,n-1) = A(:,4,n-1)
    s2(:,n) = A(:,3,n)

    ! Forward elimination
    do i=1,n-2
      ! Eliminate b(i+1)
      const(:) = A(:,2,i+1)/A(:,3,i)
      A(:,3,i+1) = A(:,3,i+1) - A(:,4,i)*const(:)
      A(:,4,i+1) = A(:,4,i+1) - A(:,5,i)*const(:)
      A(:,6,i+1) = A(:,6,i+1) - A(:,6,i)*const(:)
      s1(:,i+1) = s1(:,i+1) - s1(:,i)*const(:)
      s2(:,i+1) = s2(:,i+1) - s2(:,i)*const(:)

      ! Eliminate a(i+2)
      const(:) = A(:,1,i+2)/A(:,3,i)
      A(:,2,i+2) = A(:,2,i+2) - A(:,4,i)*const(:)
      A(:,3,i+2) = A(:,3,i+2) - A(:,5,i)*const(:)
      A(:,6,i+2) = A(:,6,i+2) - A(:,6,i)*const(:)
      s1(:,i+2) = s1(:,i+2) - s1(:,i)*const(:)
      s2(:,i+2) = s2(:,i+2) - s2(:,i)*const(:)
    end do

    ! Backward elimination
    do i=n-2,3,-1
      ! Eliminate d(i-1)
      const(:) = A(:,4,i-1)/A(:,3,i)
      A(:,6,i-1) = A(:,6,i-1) - A(:,6,i)*const(:)
      s1(:,i-1) = s1(:,i-1) - s1(:,i)*const(:)
      s2(:,i-1) = s2(:,i-1) - s2(:,i)*const(:)

      ! Eliminate e(i-2)
      const(:) = A(:,5,i-2)/A(:,3,i)
      A(:,6,i-2) = A(:,6,i-2) - A(:,6,i)*const(:)
      s1(:,i-2) = s1(:,i-2) - s1(:,i)*const(:)
      s2(:,i-2) = s2(:,i-2) - s2(:,i)*const(:)
    end do
    i=2
    ! Eliminate d(i-1)
    const(:) = A(:,4,i-1)/A(:,3,i)
    A(:,6,i-1) = A(:,6,i-1) - A(:,6,i)*const(:)
    s1(:,i-1) = s1(:,i-1) - s1(:,i)*const(:)
    s2(:,i-1) = s2(:,i-1) - s2(:,i)*const(:)

    ! Eliminate oddball region
    const(:) = A(:,5,n-1)/A(:,3,1)
    A(:,6,n-1) = A(:,6,n-1) - A(:,6,1)*const(:)
    s1(:,n-1) = s1(:,n-1) - s1(:,1)*const(:)
    s2(:,n-1) = s2(:,n-1) - s2(:,1)*const(:)

    const(:) = A(:,4,n)/A(:,3,1)
    A(:,6,n) = A(:,6,n) - A(:,6,1)*const(:)
    s1(:,n) = s1(:,n) - s1(:,1)*const(:)
    s2(:,n) = s2(:,n) - s2(:,1)*const(:)

    const(:) = A(:,5,n)/A(:,3,2)
    A(:,6,n) = A(:,6,n) - A(:,6,2)*const(:)
    s1(:,n) = s1(:,n) - s1(:,2)*const(:)
    s2(:,n) = s2(:,n) - s2(:,2)*const(:)

    ! Eliminate corner region
    const(:) = s1(:,n)/s1(:,n-1)
    A(:,6,n) = A(:,6,n) - A(:,6,n-1)*const(:)
    s2(:,n) = s2(:,n) - s2(:,n-1)*const(:)

    A(:,6,n) = A(:,6,n)/s2(:,n)
    A(:,6,n-1) = (A(:,6,n-1) - s2(:,n-1)*A(:,6,n))/s1(:,n-1)
    do i=n-2,1,-1
      A(:,6,i) = (A(:,6,i) - s1(:,i)*A(:,6,n-1) - s2(:,i)*A(:,6,n))/A(:,3,i)
    end do

    return
  end subroutine pentaPeriodicM_1

!=======================================================================
! Tri-diagonal solvers
!
! Charles Pierce, June 1995
!
! triDiagonal
! triDiagonalM
! triDiagonalMxM
! triDiagonalLUM
! triLUSolveM
! triPeriodic
! triPeriodicM
! triDiagonal_1
! triDiagonalM_1
! triDiagonalLUM_1
! triLUSolveM_1
! triDiagMultM
! triPeriMultM
!

!=======================================================================
! triDiagonal
! Solves a tri-diagonal system of algebraic equations using gauss 
! elimination.  The banded matrix given by (a,b,c) is destroyed, 
! and the rhs vector r is replaced by the solution vector.
!
! Single system, variable a,b,c.
!
  subroutine triDiagonal(a, b, c, r, n)
    USE ModGlobal
    integer :: n
    real(rfreal) ::  a(n), b(n), c(n), r(n)
    integer :: i
    real(rfreal) :: const

    ! Forward elimination
    do i=2,n
      const = a(i)/b(i-1)
      b(i) = b(i) - c(i-1)*const
      r(i) = r(i) - r(i-1)*const
    end do

    ! Back substitution
    r(n) = r(n)/b(n)
    do i=n-1,1,-1
      r(i) = (r(i) - c(i)*r(i+1))/b(i)
    end do

    return
  end subroutine triDiagonal

!=======================================================================
! triDiagonalM
! Solves an array of tri-diagonal systems of algebraic equations using 
! gauss elimination.  The banded matrices given by (a,b,c) are 
! destroyed, and the rhs vectors r are replaced by the solution vectors.
!
! Multiple systems, different (a,b,c) for each system.
!
  subroutine triDiagonalM(a, b, c, r, n, lot)
    USE ModGlobal
    integer :: n, lot
    real(rfreal) :: a(lot,n), b(lot,n), c(lot,n), r(lot,n)
    real(rfreal) :: const(lot)
    integer :: i

    ! Forward elimination
    do i=2,n
      const(:) = a(:,i)/b(:,i-1)
      b(:,i) = b(:,i) - c(:,i-1)*const(:)
      r(:,i) = r(:,i) - r(:,i-1)*const(:)
    end do

    ! Back-substitution
    r(:,n) = r(:,n)/b(:,n)
    do i=n-1,1,-1
      r(:,i) = (r(:,i) - c(:,i)*r(:,i+1))/b(:,i)
    end do

    return
  end subroutine triDiagonalM

!=======================================================================
! triDiagonalMxM
! Solves an array of tri-diagonal systems of algebraic equations using 
! gauss elimination.  The banded matrices given by (a,b,c) are 
! destroyed, and the rhs vectors r are replaced by the solution vectors.
!
! Vectorization over inner and outer indices
!
subroutine triDiagonalMxM(a, b, c, r, n, lot1, lot2)
  USE ModGlobal
  integer :: n, lot1, lot2
  real(rfreal) :: a(lot1,n,lot2), b(lot1,n,lot2), &
       c(lot1,n,lot2), r(lot1,n,lot2)
  real(rfreal) :: const(lot1,lot2)
  integer :: i

  ! Forward elimination
  do i=2,n
    const(:,:) = a(:,i,:)/b(:,i-1,:)
    b(:,i,:) = b(:,i,:) - c(:,i-1,:)*const(:,:)
    r(:,i,:) = r(:,i,:) - r(:,i-1,:)*const(:,:)
  end do

  ! Back-substitution
  r(:,n,:) = r(:,n,:)/b(:,n,:)
  do i=n-1,1,-1
    r(:,i,:) = (r(:,i,:) - c(:,i,:)*r(:,i+1,:))/b(:,i,:)
  end do

  return
end subroutine triDiagonalMxM

!=======================================================================
! triDiagonalLUM
! Computes the LU decomposition if a tri-diagonal matrix using 
! Guass elimination.
!
! Multiple systems
!
subroutine triDiagonalLUM(a, b, c, n, lot)
  USE ModGlobal
  integer :: n, lot
  real(rfreal) :: a(lot,n), b(lot,n), c(lot,n)
  real(rfreal) :: const(lot)
  integer :: i

  ! Forward elimination
  do i=2,n
    const = a(:,i)/b(:,i-1)
    a(:,i) = const(:)
    b(:,i) = b(:,i) - c(:,i-1)*const(:)
  end do

  ! Pre-divide diagonal
  do i=1,n
    b(:,i) = 1./b(:,i)
  end do

  return
end subroutine triDiagonalLUM

!=======================================================================
! triLUSolveM
! Solves a tri-diagonal system, given the LU decomposition.
!
! Multiple systems
!
subroutine triLUSolveM(a, b, c, r, n, lot)
  USE ModGlobal

  integer :: n, lot
  real(rfreal) :: a(lot,n), b(lot,n), c(lot,n), r(lot,n)
  integer :: i

  ! Forward-substitution
  do i=2,n
    r(:,i) = r(:,i) - a(:,i)*r(:,i-1)
  end do

  ! Back-substitution
  r(:,n) = r(:,n)*b(:,n)
  do i=n-1,1,-1
    r(:,i) = (r(:,i) - c(:,i)*r(:,i+1))*b(:,i)
  end do

  return
end subroutine triLUSolveM

!=======================================================================
! triPeriodic
! Solves a periodic, tri-diagonal system of algebraic equations 
! using gauss elimination.  The periodic banded matrix given by 
! (a,b,c) is destroyed, and the rhs vector r is replaced by the 
! solution vector.
!
! Single system, periodic diagonals, variable a,b,c.
!
subroutine triPeriodic(a, b, c, r, n)
  USE ModGlobal
  integer :: n
  real(rfreal) :: a(n), b(n), c(n), r(n)
  real(rfreal) :: const
  integer :: i

  if (n == 1) then
    r = r/(a + b + c)
    return
  else if (n == 2) then
    ! Solve 2x2 system
    c(1) = c(1) + a(1)
    a(2) = a(2) + c(2)
    const = a(2)/b(1)
    b(2) = b(2) - c(1)*const
    r(2) = r(2) - r(1)*const
    r(2) = r(2)/b(2)
    r(1) = (r(1) - c(1)*r(2))/b(1)
    return
  end if

  ! Forward elimination
  do i=2,n-2
    const = a(i)/b(i-1)
    b(i) = b(i) - c(i-1)*const
    r(i) = r(i) - r(i-1)*const
    ! Boundary is stored in a(i)
    a(i) = -a(i-1)*const
  end do
  i=n-1
  const = a(i)/b(i-1)
  b(i) = b(i) - c(i-1)*const
  r(i) = r(i) - r(i-1)*const
  a(i) = c(i) - a(i-1)*const
  i=n
  const = a(i)/b(i-1)
  r(i) = r(i) - r(i-1)*const
  a(i) = b(i) - a(i-1)*const

  ! Backward elimination
  do i=n-2,1,-1
    const = c(i)/b(i+1)
    r(i) = r(i) - r(i+1)*const
    a(i) = a(i) - a(i+1)*const
  end do

  ! Eliminate oddball
  const = c(n)/b(1)
  r(n) = r(n) - r(1)*const
  a(n) = a(n) - a(1)*const

  ! Backward substitution
  r(n) = r(n)/a(n)
  do i=n-1,1,-1
    r(i) = (r(i) - a(i)*r(n))/b(i)
  end do

  return
end subroutine triPeriodic

!=======================================================================
! triPeriodicM
! Solves an array of periodic, tri-diagonal systems of algebraic equations 
! using gauss elimination.  The periodic banded matrix given by 
! (a,b,c) is destroyed, and the rhs vector r is replaced by the 
! solution vector.
!
! Multiple systems, periodic diagonals, variable a,b,c.
! Different a,b,c for each system
!
subroutine triPeriodicM(a, b, c, r, n, lot)
  USE ModGlobal
  integer :: n, lot
  real(rfreal) :: a(lot,n), b(lot,n), c(lot,n), r(lot,n)
  real(rfreal) :: const(lot)
  integer :: i

  if (n == 1) then
    r = r/(a + b + c)
    return
  else if (n == 2) then
    ! Solve 2x2 system
    c(:,1) = c(:,1) + a(:,1)
    a(:,2) = a(:,2) + c(:,2)
    const(:) = a(:,2)/b(:,1)
    b(:,2) = b(:,2) - c(:,1)*const(:)
    r(:,2) = r(:,2) - r(:,1)*const(:)
    r(:,2) = r(:,2)/b(:,2)
    r(:,1) = (r(:,1) - c(:,1)*r(:,2))/b(:,1)
    return
  end if

  ! Forward elimination
  do i=2,n-2
    const(:) = a(:,i)/b(:,i-1)
    b(:,i) = b(:,i) - c(:,i-1)*const(:)
    r(:,i) = r(:,i) - r(:,i-1)*const(:)
    ! Boundary is stored in a(i)
    a(:,i) = -a(:,i-1)*const(:)
  end do
  i=n-1
  const(:) = a(:,i)/b(:,i-1)
  b(:,i) = b(:,i) - c(:,i-1)*const(:)
  r(:,i) = r(:,i) - r(:,i-1)*const(:)
  a(:,i) = c(:,i) - a(:,i-1)*const(:)
  i=n
  const(:) = a(:,i)/b(:,i-1)
  r(:,i) = r(:,i) - r(:,i-1)*const(:)
  a(:,i) = b(:,i) - a(:,i-1)*const(:)

  ! Backward elimination
  do i=n-2,1,-1
    const(:) = c(:,i)/b(:,i+1)
    r(:,i) = r(:,i) - r(:,i+1)*const(:)
    a(:,i) = a(:,i) - a(:,i+1)*const(:)
  end do

  ! Eliminate oddball
  const(:) = c(:,n)/b(:,1)
  r(:,n) = r(:,n) - r(:,1)*const(:)
  a(:,n) = a(:,n) - a(:,1)*const(:)

  ! Backward substitution
  r(:,n) = r(:,n)/a(:,n)
  do i=n-1,1,-1
    r(:,i) = (r(:,i) - a(:,i)*r(:,n))/b(:,i)
  end do

  return
end subroutine triPeriodicM

!=======================================================================
! triDiagonal_1
!
! Combined matrix-rhs into single array
!
subroutine triDiagonal_1(A, n)
  USE ModGlobal
  integer :: n
  real(rfreal) :: A(4,n)
  integer :: i
  real(rfreal) :: const

  if (n == 1) then
    ! Solve 1x1 system
    A(4,1) = A(4,1)/A(2,1)
    return
  end if

  ! Forward elimination
  do i=2,n
    const = A(1,i)/A(2,i-1)
    A(2,i) = A(2,i) - A(3,i-1)*const
    A(4,i) = A(4,i) - A(4,i-1)*const
  end do

  ! Back-substitution
  A(4,n) = A(4,n)/A(2,n)
  do i=n-1,1,-1
    A(4,i) = (A(4,i) - A(3,i)*A(4,i+1))/A(2,i)
  end do

  return
end subroutine triDiagonal_1

subroutine triDiagonalM_1(A, n, lot)
  USE ModGlobal
  integer :: n, lot
  real(rfreal) :: A(lot,4,n)
  real(rfreal) :: const(lot)
  integer :: i

  if (n == 1) then
    ! Solve 1x1 system
    A(:,4,1) = A(:,4,1)/A(:,2,1)
    return
  end if

  ! Forward elimination
  do i=2,n
    const(:) = A(:,1,i)/A(:,2,i-1)
    A(:,2,i) = A(:,2,i) - A(:,3,i-1)*const(:)
    A(:,4,i) = A(:,4,i) - A(:,4,i-1)*const(:)
  end do

  ! Back-substitution
  A(:,4,n) = A(:,4,n)/A(:,2,n)
  do i=n-1,1,-1
    A(:,4,i) = (A(:,4,i) - A(:,3,i)*A(:,4,i+1))/A(:,2,i)
  end do

  return
end subroutine triDiagonalM_1

subroutine triDiagonalLUM_1(A, n, lot)
  USE ModGlobal
  integer :: n, lot
  real(rfreal) :: A(lot,3,n)
  real(rfreal) :: const(lot)
  integer :: i

  ! Forward elimination
  do i=2,n
    const(:) = A(:,1,i)/A(:,2,i-1)
    A(:,1,i) = const(:)
    A(:,2,i) = A(:,2,i) - A(:,3,i-1)*const(:)
  end do

  ! Pre-divide diagonal
  do i=1,n
    A(:,2,i) = 1./A(:,2,i)
  end do

  return
end subroutine triDiagonalLUM_1

subroutine triLUSolveM_1(A, r, n, lot)
  USE ModGlobal
  integer :: n, lot
  real(rfreal) :: A(lot,3,n), r(lot,n)
  integer :: i

  ! Forward-substitution
  do i=2,n
    r(:,i) = r(:,i) - A(:,1,i)*r(:,i-1)
  end do

  ! Back-substitution
  r(:,n) = r(:,n)*A(:,2,n)
  do i=n-1,1,-1
    r(:,i) = (r(:,i) - A(:,3,i)*r(:,i+1))*A(:,2,i)
  end do

  return
end subroutine triLUSolveM_1

!=======================================================================
! triDiagMultM
! Multiplies a vector by a tri-diagonal matrix.
!
subroutine triDiagMultM(a, b, c, r, n, lot)
  USE ModGlobal
  integer :: n, lot
  real(rfreal) :: a(lot,n), b(lot,n), c(lot,n), r(lot,n)
  real(rfreal) :: rr(lot,n)
  integer :: i

  if (n == 1) then
    rr = b*r
  else
    rr(:,1) = b(:,1)*r(:,1) + c(:,1)*r(:,2)
    do i=2,n-1
      rr(:,i) = a(:,i)*r(:,i-1) + b(:,i)*r(:,i) + c(:,i)*r(:,i+1)
    end do
    rr(:,n) = a(:,n)*r(:,n-1) + b(:,n)*r(:,n)
  end if

  r = rr

  return
end subroutine triDiagMultM

!=======================================================================
! triPeriMultM
! Multiplies a vector by periodic tri-diagonal matrix.
!
subroutine triPeriMultM(a, b, c, r, n, lot)
  USE ModGlobal
  integer :: n, lot
  real(rfreal) :: a(lot,n), b(lot,n), c(lot,n), r(lot,n)
  real(rfreal) :: rr(lot,n)
  integer :: i

  if (n==1) then
    rr = r * (a + b + c)
  else
    rr(:,1) = a(:,1)*r(:,n) + b(:,1)*r(:,1) + c(:,1)*r(:,2)
    do i=2,n-1
      rr(:,i) = a(:,i)*r(:,i-1) + b(:,i)*r(:,i) + c(:,i)*r(:,i+1)
    end do
    rr(:,n) = a(:,n)*r(:,n-1) + b(:,n)*r(:,n) + c(:,n)*r(:,1)
  end if

  r = rr

  return
end subroutine triPeriMultM


End Module ModPenta
