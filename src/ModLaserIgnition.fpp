! Copyright (c) 2015, University of Illinois at Urbana-Champaign, XPACC
! License: MIT, http://opensource.org/licenses/MIT
!-----------------------------------------------------------------------
!
! ModLaserIgnition.f90
! 
! - basic code for generating the source term for laser ignition
!
! Revision history
! - 10 Feb 2015 : J. Capecelatro & L. Massa : initial code
!
!-----------------------------------------------------------------------
MODULE ModLaserIgnition

  Use ModGlobal, only:rfreal

CONTAINS

  Subroutine LaserInit(region, ng)

    Use ModGlobal
    Use ModDataStruct
    Use ModMPI

    Implicit None

    TYPE (t_region), POINTER :: region
    INTEGER :: ng

! ... local variables
    Type (t_mixt_input), pointer :: input
    Type (t_mixt), pointer :: state
    Type (t_grid), pointer :: grid
    Real(RFREAL) :: g_of_x, x0, xjet, lx, r, e_source, x, y, z
    Integer :: ND, l0, n

! ... simplicity
    grid  => region%grid(ng)
    input => grid%input
    state => region%state(ng)
    ND = input%ND

! ... Check if we should really be here
    If (input%useLaser == FALSE) return

!... Only apply source to grid #3
    If (grid%iGridGlobal /= 3) return

! ... Error check if not 3-D
    If (ND /= 3) Call Graceful_Exit(region%myRank,'ERROR: No support for 1D/2D in LaserInit [ModLaserIgnition.f90]')

! ... Store spatial distribution of RHS source term
    If (ND == 3) Then

! ... source location and size
       xjet = input%XJET
       x0 = input%LASER_FP
       lx = input%LASER_L
       r  = input%LASER_R

! ... initialize the source term (without temporal dependence)
       Do l0 = 1, grid%nCells

! ... spatial function
          g_of_x = (grid%XYZ(l0,1)-xjet)**2/r**2 + (grid%XYZ(l0,3)-0.5_WP*input%GRID1_LZ)**2/r**2 + (grid%XYZ(l0,2)-x0-0.5_WP*lx)**2/(0.5_WP*lx)**2


! ... energy source
          if (g_of_x.le.1.0D0) then
             e_source = 1.0D0
          else
             e_source = 0.0D0
          end if

! ... add the source
          state%rhs(l0,5) = state%rhs(l0,1) + e_source

       End Do
    End If

  End Subroutine LaserInit

 subroutine LaserSource(region, ng)

    Use ModGlobal
    Use ModDataStruct
    Use ModMetrics
    Use ModEOS
    Use ModDeriv
    Use ModIO
    Use ModMPI

    Implicit None

    TYPE (t_region), POINTER :: region
    INTEGER :: ng
    INTEGER, POINTER :: flags(:)

! ... local variables
    Integer :: act, K
    Type (t_mixt_input), pointer :: input
    Type (t_mixt), pointer :: state
    Type (t_grid), pointer :: grid
    Integer :: ND
    Real(rfreal) :: w_of_t, g_of_z, time, r, f_of_r, nd_act_radius

! ... useful numbers
    Real(rfreal), Parameter :: pi = 3.1415926535D0
    Real(rfreal), Parameter :: half = 0.5D0
    Real(rfreal), Parameter :: three_fourth = 0.75D0


!!$! ... simplicity
!!$    grid => region%grid(ng)
!!$    input => grid%input
!!$    state => region%state(ng)
!!$    ND = input%ND
!!$
!!$! ... loop over all actuators
!!$    Do act = 1, Nact
!!$       Do k = 1, grid%Ncells
!!$
!!$! ... time relative to period of this actuator
!!$          time = mod(state%time(k),Tact(act))
!!$
!!$! ... temporal distribution
!!$          w_of_t = 0.5_rfreal * (tanh((time-delay(act))/tr(act)) - tanh((time-(delay(act)+dc(act)*Tact(act)))/tf(act)))
!!$
!!$! ... spatial distribution
!!$          r = (grid%XYZ(k,1)-x0(act)) * (grid%XYZ(k,1)-x0(act))
!!$          r = r + (grid%XYZ(k,2)-y0(act)) * (grid%XYZ(k,2)-y0(act))
!!$          r = sqrt(r)
!!$          f_of_r = 0.5_rfreal*(tanh(-sigma_xy(act)*(r/radius(act) - 1.0)) + 1.0)
!!$
!!$!g_of_z = -0.5*(tanh(-1*sigma_z*(grid%XYZ(k,2)-y_l)/act_radius)+1) +&
!!$!0.5*(tanh(-1*sigma_z*(grid%XYZ(k,2)-y_r)/act_radius)+1)
!!$
!!$! ... add it in
!!$          state%rhs(k,ND+2) = state%rhs(k,ND+2) + w_of_t * f_of_r * Power(act) * efficiency(act) / volume(act)
!!$       End Do
!!$    End Do

  end subroutine LaserSource


END MODULE ModLaserIgnition
