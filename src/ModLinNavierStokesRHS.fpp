! Copyright (c) 2015, University of Illinois at Urbana-Champaign, XPACC
! License: MIT, http://opensource.org/licenses/MIT
!----------------------------------------------------------------------
!
! ModNavierStokes.f90
! 
! - basic code for the evaluation of the Navier-Stokes equations
!
! Revision history
! - 19 Dec 2006 : DJB : initial code
! - 20 Dec 2006 : DJB : continued evolution
! - 20 Mar 2007 : DJB : initial working Poinsot-Lele BC on gen. meshes
! - 20 Apr 2007 : DJB : initial working Poinsot-Lele BC on moving meshes
! - 10 Jul 2007 : DJB : initial CVS submit
! - 11 Jul 2007 : DJB : initial work on ``multi-dimensional'' Poinsot-Lele
! - 01 Feb 2008 : DJB : merged in JKim's LES & 3D bc routines
! - 29 Jul 2008 : DJB : Initial SAT boundary conditions
! 
! flags == FALSE    : compute inviscid fluxes only
! flags == TRUE     : compute dissipative fluxes only
! flags == COMBINED : inviscid + dissipative fluxes
!
! $Header: /cvsroot/genx/Codes/RocfloCM/Source/ModLinNavierStokesRHS.f90,v 1.5 2011/11/05 11:11:44 bodony Exp $
!
!-----------------------------------------------------------------------
MODULE ModLinNavierStokesRHS

CONTAINS

  subroutine LinNS_RHS(region, ng, flags)

    USE ModGlobal
    USE ModDataStruct
    USE ModDeriv
    USE ModMPI

    ! ... local variables
    type(t_grid), pointer :: grid
    type(t_mixt), pointer :: state
    type(t_mixt_input), pointer :: input
    type(t_region), pointer :: region
    type(t_mpi_timings), pointer :: mpi_timings
    real(rfreal) :: timer
    integer :: flags, ng, k, i

    ! ... simplicity
    grid        => region%grid(ng)
    input       => grid%input
    state       => region%state(ng)
    mpi_timings => region%mpi_timings(ng)

    ! ... inviscid Euler fluxes
    ! ... If LES is on, thermodynamic variables are modified using SGS KE, so 
    ! ... inviscid flux since it contains pressure should be computed here.
    ! ... Time-dependent metric portion is also considered here.
    if (flags == INVISCID_ONLY .or. flags == COMBINED) then
      timer = MPI_WTIME()
      if (input%fv_grid(ng) == FINITE_DIF) then
        call LinNS_RHS_Euler(region, ng, flags)
      else
        call graceful_exit(region%myrank, 'Only finite difference allowed with linear equations.')
      end if
      mpi_timings%rhs_inviscid = mpi_timings%rhs_inviscid + (MPI_WTIME() - timer)
    end if

    if (input%sat_art_diss .eqv. .true.) call LinNS_SAT_Artificial_Dissipation(region, ng)

    ! ... multiply the entire RHS by the jacobian
    if (input%fv_grid(ng) == FINITE_DIF) then
      do k = 1, size(state%rhs,2)
        do i = 1, grid%nCells
          state%rhs(i,k) = state%rhs(i,k) * grid%JAC(i)
        end do
      end do 
    end if

    return

  end subroutine LinNS_RHS

  subroutine LinNS_Allocate_Memory(region, ng, flags, rkStep)

    USE ModGlobal
    USE ModDataStruct
    USE ModDeriv
    USE ModMPI

    ! ... local variables
    type(t_grid), pointer :: grid
    type(t_mixt), pointer :: state
    type(t_mixt_input), pointer :: input
    type(t_region), pointer :: region
    integer :: flags, rkStep
    integer :: ND, Nc, N(MAX_ND), ng, j, alloc_stat(2)

    ! ... local variables
    integer :: l, m

    ! ... initialize alloc_stat
    alloc_stat(:) = 0

    ! ... simplicity
    grid  => region%grid(ng)
    state => region%state(ng)
    input => grid%input

    ND = grid%ND
    Nc = grid%nCells
    N(:)  = 1
    do j = 1, grid%ND
      N(j) = grid%ie(j)-grid%is(j)+1
    end do ! j

    ! ... allocate space for arrays
    if (.not.associated( state%flux) .eqv. .true.) allocate( state%flux(Nc), stat=alloc_stat(1));
    if (.not.associated(state%dflux) .eqv. .true.) allocate(state%dflux(Nc), stat=alloc_stat(2));
    if (sum(alloc_stat(:)) /= 0)  call graceful_exit(region%myrank, 'Unable to allocate flux, dflux in NS_Allocate_Memory.')

  end subroutine LinNS_Allocate_Memory

  subroutine LinNS_Deallocate_Memory(region, ng, flags, rkStep)

    USE ModGlobal
    USE ModDataStruct
    USE ModDeriv
    USE ModMPI

    ! ... local variables
    type(t_grid), pointer :: grid
    type(t_mixt), pointer :: state
    type(t_mixt_input), pointer :: input
    type(t_region), pointer :: region
    integer :: flags, ng, rkStep

    ! ... simplicity
    grid => region%grid(ng)
    state => region%state(ng)
    input => grid%input

    ! ... deallocate
    deallocate(state%flux, state%dflux)
    nullify(state%flux, state%dflux)

  end subroutine LinNS_Deallocate_Memory

  subroutine LinNS_RHS_Euler(region, ng, flags)

    USE ModGlobal
    USE ModDataStruct
    USE ModDeriv
    USE ModMPI

    Implicit None

    ! ... local variables
    type(t_grid), pointer :: grid
    type(t_mixt), pointer :: state, mean
    type(t_mixt_input), pointer :: input
    type(t_region), pointer :: region
    type(t_patch), pointer :: patch
    integer :: flags
    real(rfreal), allocatable :: UVWhat(:,:), UVWhatp(:,:)
    integer :: ND, Nc, N(MAX_ND), ng, i, j, k, Np(MAX_ND), patchID
    integer :: ip, jp, kp, ii
    real(rfreal) :: source, RHO, U, up

    ! ... simplicity
    grid  => region%grid(ng)
    state => region%state(ng)
    input => grid%input
    mean  => region%mean(ng)
    ND = grid%ND
    Nc = grid%nCells
    N  = 1
    do j = 1, grid%ND
      N(j) = grid%ie(j)-grid%is(j)+1
    end do ! j

    ! ... allocate space for arrays
    allocate(UVWhat(Nc,ND),UVWhatp(Nc,ND));
    UVWhat(:,:) = 0.0_rfreal
    UVWhatp(:,:) = 0.0_rfreal

    if (grid%moving == TRUE) then
      do k = 1, size(state%rhs,2)
        do ii = 1, Nc
          state%rhs(ii,k) = state%rhs(ii,k) - state%cv(ii,k) * grid%INVJAC_TAU(ii)
        end do
      end do ! k
    end if

    ! ... compute the contravariant velocities
    do i = 1, ND
      do j = 1, ND
        do ii = 1, Nc
          RHO = mean%cv(ii,1)
          U   = mean%cv(ii,j+1) / RHO
          up  = (state%cv(ii,j+1) - state%cv(ii,1) * U) / RHO
          UVWhat(ii,i)  = UVWhat(ii,i)  + grid%MT1(ii,region%global%t2Map(i,j)) * U
          UVWhatp(ii,i) = UVWhatp(ii,i) + grid%MT1(ii,region%global%t2Map(i,j)) * up
        end do
      end do ! j
      do ii = 1, Nc
        UVWhat(ii,i) = UVWhat(ii,i) + grid%XI_TAU(ii,i)
      end do
    end do ! i

    ! ... inviscid fluxes

    ! ... continuity 
    do i = 1, ND
      do ii = 1, Nc
        state%flux(ii) = state%cv(ii,region%global%vMap(1)) * UVWhat(ii,i) + mean%cv(ii,1) * UVWhatp(ii,i)
      end do
      call APPLY_OPERATOR(region, ng, input%iFirstDeriv, i, state%flux, state%dflux, .FALSE.)
      do ii = 1, Nc
        state%rhs(ii,region%global%vMap(1)) = state%rhs(ii,region%global%vMap(1)) - state%dflux(ii)
      end do
      Call Save_Patch_Deriv(region, ng, 1, i, state%dflux)
    end do ! i

    ! ... momentum
    do i = 1, ND
      do j = 1, ND
        do ii = 1, Nc
          state%flux(ii) = state%cv(ii,region%global%vMap(i+1)) * UVWhat(ii,j) &
                         + mean%cv(ii,i+1) * UVWhatp(ii,j) &
                         + grid%MT1(ii,region%global%t2Map(j,i)) * state%dv(ii,1) 
        end do
        call APPLY_OPERATOR(region, ng, input%iFirstDeriv, j, state%flux, state%dflux, .FALSE.)
        do ii = 1, Nc
          state%rhs(ii,region%global%vMap(i+1)) = state%rhs(ii,region%global%vMap(i+1)) - state%dflux(ii)
        end do
        Call Save_Patch_Deriv(region, ng, i+1, j, state%dflux)
      end do ! j
    end do ! i

    ! ... energy
    do i = 1, ND
      do ii = 1, Nc
        state%flux(ii) = (state%cv(ii,region%global%vMap(ND+2)) + state%dv(ii,1)) * UVWhat(ii,i) - grid%XI_TAU(ii,i) * state%dv(ii,1) & 
                       + (mean%cv(ii,ND+2) + mean%dv(ii,1)) * UVWhatp(ii,i)
      end do
      call APPLY_OPERATOR(region, ng, input%iFirstDeriv, i, state%flux, state%dflux, .FALSE.)
      do ii = 1, Nc
        state%rhs(ii,region%global%vMap(ND+2)) = state%rhs(ii,region%global%vMap(ND+2)) - state%dflux(ii)
      end do
      Call Save_Patch_Deriv(region, ng, ND+2, i, state%dflux)
    end do ! i

    ! ... deallocate
    deallocate(UVWhat)

    ! CAA Benchmark 2, Category 1, Problem 1
    if (.false.) then
      do i = 1, Nc
        source = 0.1_dp * exp(-(log(2.0_dp))*( ((grid%XYZ(i,1)-4.0_dp)**2+grid%XYZ(i,2)**2)/(0.4_dp)**2 )) * dsin(4.0_dp * TWOPI * state%time(i)) * grid%INVJAC(i)
        state%rhs(i,4) = state%rhs(i,4) + source / (state%gv(i,1)-1.0_dp)
      end do
    end if

    ! ... subtract off (violation of) metric identity
    if (input%useMetricIdentities == TRUE) Then
      do i = 1, ND
        do ii = 1, Nc
          state%rhs(ii,1)    = state%rhs(ii,1)    + state%cv(ii,i+1) * grid%ident(ii,i)
          state%rhs(ii,i+1)  = state%rhs(ii,i+1)  + state%dv(ii,1) * grid%ident(ii,i)
          state%rhs(ii,ND+2) = state%rhs(ii,ND+2) + (state%cv(ii,ND+2) + state%dv(ii,1)) * state%cv(ii,i+1)/state%cv(ii,1) * grid%ident(ii,i)
        end do
      end do
    End if

    return

  end subroutine LinNS_RHS_Euler

  subroutine LinNS_Timestep(region)

    USE ModGlobal
    USE ModDataStruct
    USE ModMPI

    Implicit None

    ! ... global variables
    type(t_region), pointer :: region

    ! ... local variables
    integer :: i, j, k, jj, ng, ierr
    real(rfreal) :: UU(MAX_ND), absUU, snd_spd, metric_fac, fac0, dt_inv, dt_vis, dt
    real(rfreal) :: var, var_all, OSXI(MAX_ND), SXI(MAX_ND), dt_tmp(MAX_ND), THETA, RMU
    Type(t_grid), pointer :: grid
    Type(t_mixt), pointer :: state, mean
    Type(t_mixt_input), pointer :: input
    real(rfreal), allocatable :: dt_local(:)
    real(rfreal) :: nu_star, numer, denom, dot_prod, ibfac

    ! ... allocate space for each grid on this processor
    allocate(dt_local(region%nGrids))

    ! ... simplicity
    input => region%input

    ! ... constant cfl mode
    if (input%cfl_mode == CONSTANT_CFL) then

      do ng = 1, region%nGrids

        state => region%state(ng)
        grid  => region%grid(ng)
        mean  => region%mean(ng)

        ! ... initialize
        state%cfl(:) = input%cfl

        ! ... inviscid timestep
        Do k = 1, grid%nCells
          If (grid%JAC(k) > 0.0_DP) Then

            ! ... initialize
            numer = 0.0_rfreal
            denom = 0.0_rfreal
            ibfac = grid%ibfac(k)

            ! ... Cartesian velocity
            UU = 0.0_rfreal
            do i = 1, grid%ND
              UU(i) = mean%dv(k,3) * mean%cv(k,1+i) + grid%XI_TAU(k,i)
            end do
            do i = 1, grid%ND
              denom = denom + abs(DOT_PRODUCT(UU(1:grid%ND),grid%MT1(k,region%global%t2Map(i,1:grid%ND))))
            end do

            ! ... speed of sound
            snd_spd = sqrt(mean%gv(k,1) * mean%dv(k,1) * mean%dv(k,3))

            dot_prod = 0.0_rfreal
            do i = 1, grid%ND**2
              dot_prod = dot_prod + grid%MT1(k,i) * grid%MT1(k,i)
            end do
            denom = denom + snd_spd * sqrt(dot_prod)

            ! ... put it all together
            dt_inv = grid%INVJAC(k) / (denom * ibfac + (1.0 - ibfac) * 1E-10) * ibfac
            dt_vis = dt_inv

            ! ... for the viscous terms (taken from FDL3DI)
            if (state%RE > 0.0_rfreal) then

              nu_star = max(mean%tv(k,1)*state%REinv, mean%tv(k,2)*state%REinv, mean%tv(k,3)*state%REinv*state%PRinv)
              dot_prod = 0.0_rfreal
              do i = 1, grid%ND
                dot_prod = dot_prod + sqrt(DOT_PRODUCT(grid%MT1(k,region%global%t2Map(i,1:grid%ND)),grid%MT1(k,region%global%t2Map(i,1:grid%ND))))
              end do
              denom = denom + 2.0_rfreal * nu_star * grid%JAC(k) * dot_prod**2

              dt_vis = grid%INVJAC(k) / (denom * ibfac + (1.0 - ibfac) * 1E-10) * ibfac

            end if

            ! ... the timestep
            state%dt(k) = state%cfl(k) * MIN(dt_inv, dt_vis) * ibfac
          Else
            state%dt(k) = 999.0_DP
          End If

        End Do

        ! ... save the minimum dt for this grid
        dt_local(ng) = MINVAL(state%dt, MASK = state%dt .gt. 0.0_rfreal)

      end do

      call mpi_barrier(mycomm, ierr)

      ! ... find the minimum dt over all nodes
      var = MINVAL(dt_local, MASK = dt_local .gt. 0.0_rfreal)
      Call MPI_AllReduce(var, var_all, 1, MPI_REAL8, MPI_MIN, mycomm, ierr)

      ! ... copy to all grids on this processor
      do ng = 1, region%nGrids
        state => region%state(ng)
        state%dt(:) = var_all
      end do

    else if (input%cfl_mode == CONSTANT_DT) then

      do ng = 1, region%nGrids

        state => region%state(ng)
        grid  => region%grid(ng)
        mean  => region%mean(ng)

        ! ... initialize
        state%dt = input%dt

        ! ... inviscid timestep
        do k = 1, grid%nCells
          if (grid%JAC(k) > 0.0_DP) then

            ! ... initialize
            numer = 0.0_rfreal
            denom = 0.0_rfreal
            ibfac = grid%ibfac(k)

            ! ... Cartesian velocity
            UU = 0.0_rfreal
            do i = 1, grid%ND
              UU(i) = mean%dv(k,3) * mean%cv(k,1+i) + grid%XI_TAU(k,i)
            end do
            do i = 1, grid%ND
              denom = denom + abs(DOT_PRODUCT(UU(1:grid%ND),grid%MT1(k,region%global%t2Map(i,1:grid%ND))))
            end do

            ! ... speed of sound
            snd_spd = sqrt(mean%gv(k,1) * mean%dv(k,1) * mean%dv(k,3))

            dot_prod = 0.0_rfreal
            do i = 1, grid%ND
              dot_prod = dot_prod + DOT_PRODUCT(grid%MT1(k,region%global%t2Map(i,1:grid%ND)), grid%MT1(k,region%global%t2Map(i,1:grid%ND)))
            end do
            denom = denom + snd_spd * sqrt(dot_prod)

            ! ... put it all together
            dt_inv = grid%INVJAC(k) / (denom * ibfac + (1.0 - ibfac) * 1E-10) * ibfac
            dt_vis = dt_inv

            ! ... for the viscous terms (taken from FDL3DI)
            if (state%RE > 0.0_rfreal) then

              nu_star = max(mean%tv(k,1)*state%REinv, mean%tv(k,2)*state%REinv, mean%tv(k,3)*state%REinv*state%PRinv)
              dot_prod = 0.0_rfreal
              do i = 1, grid%ND
                dot_prod = dot_prod + sqrt(DOT_PRODUCT(grid%MT1(k,region%global%t2Map(i,1:grid%ND)),grid%MT1(k,region%global%t2Map(i,1:grid%ND))))
              end do
              denom = denom + 2.0_rfreal * nu_star * grid%JAC(k) * dot_prod**2

              dt_vis = grid%INVJAC(k) / (denom * ibfac + (1.0 - ibfac) * 1E-10) * ibfac

            end if

            ! ... the cfl
            state%cfl(k) = state%dt(k) / MIN(dt_inv,dt_vis) * ibfac
          else
            state%cfl(k) = -999.0_DP
          end if

        end do

        ! ... save the maximum cfl for this grid
        dt_local(ng) = MAXVAL(state%cfl)

      end do

      ! ... find the maximum cfl over all nodes
      var = MAXVAL(dt_local)
      Call MPI_AllReduce(var, var_all, 1, MPI_DOUBLE_PRECISION, MPI_MAX, mycomm, ierr)

      ! ... copy the cfl back to all the nodes
      do ng = 1, region%nGrids
        state => region%state(ng)
        do k = 1, grid%nCells
          state%cfl(k) = var_all
        end do
      end do

    end if

    deallocate(dt_local)

  end subroutine LinNS_Timestep

  subroutine LinNS_SAT_Artificial_Dissipation (region, ng)

    USE ModGlobal
    USE ModDataStruct
    USE ModDeriv
    USE ModMPI

    Implicit None

    ! ... local variables
    type(t_grid), pointer :: grid
    type(t_mixt), pointer :: state
    type(t_mixt_input), pointer :: input
    type(t_region), pointer :: region
    real(rfreal), pointer :: SpectralRadius(:,:)
    integer :: ng, i, ii, var, dir, ND, Nc
    real(rfreal) :: delta_x, diss_power

    ! ... simplicity
    grid => region%grid(ng)
    input => grid%input
    state => region%state(ng)
    ND = grid%ND
    Nc = grid%nCells

    ! ... determine diss_power
!!$    Select Case ( input%spaceOrder )
!!$      Case ( 5 ) diss_power = 8.0_rfreal
!!$      Case ( 3 ) diss_power = 4.0_rfreal
!!$      Case ( 2 ) diss_power = 2.0_rfreal
!!$    End Select
  
    ! ... compute the spectral radius
    allocate(SpectralRadius(Nc,ND))
    Call LinNS_Spectral_Radius(region, ng, SpectralRadius)

    ! ... Loop over all variables
    Do var = 1, ND + 2

      ! ... Loop over each direction
      Do dir = 1, ND

        ! ... apply the DI operator first
        Do ii = 1, Nc
          state%flux(ii) = state%cv(ii,var)
        End Do
        Call APPLY_OPERATOR(region, ng, input%iSATArtDiss, dir, state%flux, state%dflux, .FALSE.)

        ! ... scale the result by the spectral radius
        Do ii = 1, Nc
          state%flux(ii) = state%dflux(ii) * SpectralRadius(ii,dir)
        End Do

        ! ... apply the DI^(Transpose) operator next
        Call APPLY_OPERATOR(region, ng, input%iSATArtDissSplit, dir, state%flux, state%dflux, .FALSE.)

        ! ... add the result to the rhs
        Do ii = 1, Nc

          ! ... construct the normalized metrics
          !XI_X = grid%MT1(ii,region%global%t2Map(dir,1)); XI_Y = 0.0_rfreal; XI_Z = 0.0_rfreal
          !if (ND >= 2) XI_Y = grid%MT1(ii,region%global%t2Map(dir,2))
          !if (ND == 3) XI_Z = grid%MT1(ii,region%global%t2Map(dir,3))

          ! ... multiply by the Jacobian
          ! delta_x = grid%INVJAC(ii) / sqrt(XI_X**2 + XI_Y**2 + XI_Z**2)
          ! state%rhs(ii,var) = state%rhs(ii,var) + input%amount_SAT_diss * (delta_x)**diss_power * state%dflux(ii)
          state%rhs(ii,var) = state%rhs(ii,var) + input%amount_SAT_diss * state%dflux(ii) * grid%INVJAC(ii)

       End Do

      End Do

    End Do

    ! ... clean up
    deallocate(SpectralRadius)

  end subroutine LinNS_SAT_Artificial_Dissipation

  subroutine LinNS_Spectral_Radius(region, ng, SpectralRadius, order_in)

    USE ModGlobal
    USE ModDataStruct
    USE ModDeriv
    USE ModMPI

    Implicit None

    ! ... local variables
    type(t_grid), pointer :: grid
    type(t_mixt), pointer :: state
    type(t_mixt_input), pointer :: input
    type(t_region), pointer :: region
    type(t_patch), pointer :: patch
    real(rfreal), pointer :: SpectralRadius(:,:)
    integer :: ng, i, j, k, l0, ii, var, dir, ND, Nc, N(MAX_ND), Np(MAX_ND), npatch, l1, jj, sgn, normDir
    real(rfreal) :: rho, u, v, w, p, gam, en, a, rj11, rj12, rj13, rj21, rj22, rj23, rj31, rj32, rj33
    real(rfreal) :: sq1, sq2, sq3, ucon, vcon, wcon
    integer, allocatable :: inorm(:)
    integer :: length_inorm
    integer, optional :: order_in

    ! ... simplicity
    grid  => region%grid(ng)
    state => region%state(ng)
    input => grid%input
    ND = grid%ND
    Nc = grid%nCells

    ! ... Modeled after Magnus's code (it is not clear this is the right way to do this)
    SELECT CASE (ND)

    CASE (1)

      Do ii = 1, Nc

        gam = state%gv(ii,1)
        rho = state%cv(ii,1)
        u   = state%cv(ii,2) / rho
        en  = state%cv(ii,3) - 0.5_rfreal * rho * (u*u)
        p   = (gam - 1.0_rfreal) * en
        a   = sqrt(gam * p / rho)

        rj11 = grid%MT1(ii,region%global%t2Map(1,1))

        ucon = rj11 * u

        sq1  = sqrt( rj11 * rj11 )

        SpectralRadius(ii,1) = (abs(ucon) + a*sq1)

      End Do

    CASE (2) 

      Do ii = 1, Nc

        gam = state%gv(ii,1)
        rho = state%cv(ii,1)
        u   = state%cv(ii,2) / rho
        v   = state%cv(ii,3) / rho
        en  = state%cv(ii,4) - 0.5_rfreal * rho * (u*u + v*v)
        p   = (gam - 1.0_rfreal) * en
        a   = sqrt(gam * p / rho)

        rj11 = grid%MT1(ii,region%global%t2Map(1,1))
        rj12 = grid%MT1(ii,region%global%t2Map(1,2))

        rj21 = grid%MT1(ii,region%global%t2Map(2,1))
        rj22 = grid%MT1(ii,region%global%t2Map(2,2))

        ucon = rj11 * u + rj12 * v
        vcon = rj21 * u + rj22 * v

        sq1  = sqrt( rj11 * rj11 + rj12 * rj12 )
        sq2  = sqrt( rj21 * rj21 + rj22 * rj22 )

        SpectralRadius(ii,1) = (abs(ucon) + a*sq1)
        SpectralRadius(ii,2) = (abs(vcon) + a*sq2)

      End Do

    Case (3)

      Do ii = 1, Nc

        gam = state%gv(ii,1)
        rho = state%cv(ii,1)
        u   = state%cv(ii,2) / rho
        v   = state%cv(ii,3) / rho
        w   = state%cv(ii,4) / rho
        en  = state%cv(ii,5) - 0.5_rfreal * rho * (u*u + v*v + w*w)
        p   = (gam - 1.0_rfreal) * en
        a   = sqrt(gam * p / rho)

        rj11 = grid%MT1(ii,region%global%t2Map(1,1))
        rj12 = grid%MT1(ii,region%global%t2Map(1,2))
        rj13 = grid%MT1(ii,region%global%t2Map(1,3))

        rj21 = grid%MT1(ii,region%global%t2Map(2,1))
        rj22 = grid%MT1(ii,region%global%t2Map(2,2))
        rj23 = grid%MT1(ii,region%global%t2Map(2,3))

        rj31 = grid%MT1(ii,region%global%t2Map(3,1))
        rj32 = grid%MT1(ii,region%global%t2Map(3,2))
        rj33 = grid%MT1(ii,region%global%t2Map(3,3))

        ucon = rj11 * u + rj12 * v + rj13 * w
        vcon = rj21 * u + rj22 * v + rj23 * w
        wcon = rj31 * u + rj32 * v + rj33 * w

        sq1  = sqrt( rj11 * rj11 + rj12 * rj12 + rj13 * rj13 )
        sq2  = sqrt( rj21 * rj21 + rj22 * rj22 + rj23 * rj23 )
        sq3  = sqrt( rj31 * rj31 + rj32 * rj32 + rj33 * rj33 )

        SpectralRadius(ii,1) = (abs(ucon) + a*sq1)
        SpectralRadius(ii,2) = (abs(vcon) + a*sq2)
        SpectralRadius(ii,3) = (abs(wcon) + a*sq3)

      End Do

    End Select

    loop1 : Do npatch = 1, region%npatches

      patch => region%patch(npatch)
      grid => region%grid(patch%gridID)
      state => region%state(patch%gridID)
      input => grid%input
      normDir = abs(patch%normDir)
      sgn = normDir / patch%normDir

      if (patch%gridID /= ng) CYCLE loop1

      ! ... the SpectralRadius should diminish near the boundaries
      If (present(order_in) .eqv. .true.) then
        SELECT CASE (order_in)
        CASE (5)
          allocate(inorm(2))    
        CASE DEFAULT
          allocate(inorm(1))
        END SELECT
      Else
        SELECT CASE (input%spaceOrder)
        CASE (5)
          allocate(inorm(2))    
        CASE DEFAULT
          allocate(inorm(1))
        END SELECT
      End If
      length_inorm = size(inorm)

      if ( length_inorm <= 0 ) CYCLE loop1

      N  = 1; Np = 1;
      do j = 1, grid%ND
        N(j)  = grid%ie(j)-grid%is(j)+1
        Np(j) = patch%ie(j) - patch%is(j) + 1
      end do;

      if ( patch%bcType /= SPONGE ) then

        Do k = patch%is(3), patch%ie(3)
          Do j = patch%is(2), patch%ie(2)
            Do i = patch%is(1), patch%ie(1)

              l0 = (k- grid%is(3))* N(1)* N(2) + (j- grid%is(2))* N(1) + (i- grid%is(1)+1)

              ! ... normal indices
              do jj = 1, length_inorm
                if (normDir == 1) then
                  l1 = l0 + sgn*(jj-1)
                else if (normDir == 2) then
                  l1 = l0 + sgn*(jj-1)*N(1)
                else
                  l1 = l0 + sgn*(jj-1)*N(1)*N(2)
                end if
                inorm(jj) = l1
              end do

              SpectralRadius(inorm(1:length_inorm),:) = 0.0_rfreal

            End Do
          End Do
        End Do

      end if

      Deallocate(inorm)

    end do loop1

  End Subroutine LinNS_Spectral_Radius

END MODULE ModLinNavierStokesRHS
