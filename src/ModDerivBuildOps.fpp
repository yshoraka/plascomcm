! Copyright (c) 2015, University of Illinois at Urbana-Champaign, XPACC
! License: MIT, http://opensource.org/licenses/MIT
!-----------------------------------------------------------------------
! ModDeriv.f90
!  
! - basic code for derivatives
!
! Revision history
! - 18 Dec 2006 : DJB : initial code
! - 29 Dec 2006 : DJB : evolution
! - 10 Jan 2007 : DJB : more evolution---Chuck's Pentadiagonal solvers
! - 29 Oct 2008 : DJB : conversion to operators
! - 20 May 2009 : DJB : low-memory version
!
! $Header: /cvsroot/genx/Codes/RocfloCM/Source/ModDerivBuildOps.f90,v 1.11 2011/05/30 18:46:42 bodony Exp $
!
!-----------------------------------------------------------------------

module ModDerivBuildOps

contains

  !-----------------------------------------------------------------------

  subroutine OPERATOR_SETUP(region, input, grid, err, ng)

    USE ModGlobal
    USE ModDataStruct
    USE ModMPI

    Implicit none

    ! ... function call
    type (t_grid), pointer :: grid
    type (t_mixt_input), pointer :: input
    type (t_region), pointer :: region

    ! ... local variables
    integer :: dir, N, vds, numFilter, j, err, ng, min_stencil_start, max_stencil_end, opID
    logical :: alloc
    integer :: alloc_stat(5), stencil_size
    real(rfreal), pointer :: values(:)
    real(rfreal) :: timer

    ! ... start timer
    timer = MPI_WTIME()

    alloc = .false.
    if ( (.not.associated(grid%penta)) .eqv. .true.) alloc = .true.

    if (alloc .eqv. .true.) then

      ! ... allocate the upper-most operator points
      allocate(grid%penta(MAX_OPERATORS,grid%ND))
!      allocate(grid%B(MAX_OPERATORS,grid%ND))

      ! ... SBP data
      if ( input%SBP_Pdiag(1) /= 0.0_rfreal ) then
        grid%SBP_invPdiag_block1(:) = input%SBP_invPdiag_block1(:)
        grid%SBP_invPdiag_block2(:) = input%SBP_invPdiag_block2(:)
      end if

      ! ... operator type
      grid%operator_type(:) = input%operator_type(:)

    end if

    ! ... right now let's comment things below, WZhang 12/2014
    do dir = 1, grid%ND

      N   = grid%ie_unique(dir)-grid%is_unique(dir)+1
      vds = grid%vds(dir)

      if (alloc .eqv. .true.) then

        ! ... first derivative
        if (grid%operator_type(grid%iFirstDeriv) == IMPLICIT) then      
          alloc_stat(:) = 0
          allocate(grid%penta(input%iFirstDeriv,dir)%a(vds,N),stat=alloc_stat(1))
          allocate(grid%penta(input%iFirstDeriv,dir)%b(vds,N),stat=alloc_stat(2))
          allocate(grid%penta(input%iFirstDeriv,dir)%c(vds,N),stat=alloc_stat(3))
          allocate(grid%penta(input%iFirstDeriv,dir)%d(vds,N),stat=alloc_stat(4))
          allocate(grid%penta(input%iFirstDeriv,dir)%e(vds,N),stat=alloc_stat(5))
          if (sum(alloc_stat(1:5)) /= 0) call graceful_exit(region%myrank, 'Error allocating grid%penta for 1st deriv in ModDeriv.')
        end if

        ! ... Second derivative
        if (grid%operator_type(grid%iSecondDeriv) == IMPLICIT) then      
          alloc_stat(:) = 0
          allocate(grid%penta(input%iSecondDeriv,dir)%a(vds,N),stat=alloc_stat(1))
          allocate(grid%penta(input%iSecondDeriv,dir)%b(vds,N),stat=alloc_stat(2))
          allocate(grid%penta(input%iSecondDeriv,dir)%c(vds,N),stat=alloc_stat(3))
          allocate(grid%penta(input%iSecondDeriv,dir)%d(vds,N),stat=alloc_stat(4))
          allocate(grid%penta(input%iSecondDeriv,dir)%e(vds,N),stat=alloc_stat(5))
          if (sum(alloc_stat(1:5)) /= 0) call graceful_exit(region%myrank, 'Error allocating grid%penta for 2nd deriv in ModDeriv.')
        end if

        ! ... r-th derivative for hyperviscosity
        if (input%shock == 2 .or. input%shock == 5) then ! ... Kawai & Lele (JCP 2008) and Kawai et al. (AIAA P 2009, JCP 2010)
          if (grid%operator_type(input%ArtPropDrvOrder) == IMPLICIT) then
            allocate(grid%penta(input%ArtPropDrvOrder,dir)%a(vds,N))
            allocate(grid%penta(input%ArtPropDrvOrder,dir)%b(vds,N))
            allocate(grid%penta(input%ArtPropDrvOrder,dir)%c(vds,N))
            allocate(grid%penta(input%ArtPropDrvOrder,dir)%d(vds,N))
            allocate(grid%penta(input%ArtPropDrvOrder,dir)%e(vds,N))
          end if ! grid%deriv_type(dir)
        end if ! input%shock

        if (input%timeScheme == BDF_GMRES_IMPLICIT) then
          if (grid%operator_type(input%iLHS_Filter) == IMPLICIT) then
            allocate(grid%penta(input%iLHS_Filter,dir)%a(vds,N))
            allocate(grid%penta(input%iLHS_Filter,dir)%b(vds,N))
            allocate(grid%penta(input%iLHS_Filter,dir)%c(vds,N))
            allocate(grid%penta(input%iLHS_Filter,dir)%d(vds,N))
            allocate(grid%penta(input%iLHS_Filter,dir)%e(vds,N))
          end if
        end if

      end if ! alloc

      ! ... fill the operators
      ! ... first derivative
      if (grid%iFirstDeriv /= FALSE) then
        Call Implicit_Stencil_Fill(region%myrank, input, dir, grid%iFirstDeriv, grid)
        if (region%myrank == 0) &
          Write (*,'(A,I1,A)') 'PlasComCM: Done building first derivative (dir = ',dir,').'
        !Call OPERATOR_MATRIX_FILL(region%myrank, input, dir, grid%iFirstDerivBiased, grid, FALSE, err)
        !if (err /= 0) Return
        !if (region%myrank == 0) &
        !  Write (*,'(A,I1,A)') 'PlasComCM: Done building BIASED first derivative (dir = ',dir,').'
      end if

      ! ... Second derivative
      if (grid%iSecondDeriv /= FALSE) then
        Call Implicit_Stencil_Fill(region%myrank, input, dir, grid%iSecondDeriv, grid)
        if (region%myrank == 0) &
          Write (*,'(A,I1,A)') 'PlasComCM: Done building second derivative (dir = ',dir,').'
      end if

      if (input%shock == TRUE) then
        Call Implicit_Stencil_Fill(region%myrank, input, dir, input%iFourthDeriv, grid)
        Call Implicit_Stencil_Fill(region%myrank, input, dir, input%iFilterGaussianHyperviscosity, grid)
      end if

      ! ... now work on the filters
      if (alloc .eqv. .true.) then

        ! ... solution filter
        if ((grid%iFilter /= FALSE)) then
          if ((grid%operator_type(grid%iFilter) == IMPLICIT)) then
            allocate(grid%penta(grid%iFilter,dir)%a(vds,N))
            allocate(grid%penta(grid%iFilter,dir)%b(vds,N))
            allocate(grid%penta(grid%iFilter,dir)%c(vds,N))
            allocate(grid%penta(grid%iFilter,dir)%d(vds,N))
            allocate(grid%penta(grid%iFilter,dir)%e(vds,N))
          end if
        end if

        ! ... test filter
        if ((grid%iFilterTest /= FALSE)) then
          if ((grid%operator_type(grid%iFilterTest) == IMPLICIT)) then
            allocate(grid%penta(grid%iFilterTest,dir)%a(vds,N))
            allocate(grid%penta(grid%iFilterTest,dir)%b(vds,N))
            allocate(grid%penta(grid%iFilterTest,dir)%c(vds,N))
            allocate(grid%penta(grid%iFilterTest,dir)%d(vds,N))
            allocate(grid%penta(grid%iFilterTest,dir)%e(vds,N))
          end if
        end if

      end if

      ! ... create the filter matrices
      if (grid%iFilter /= FALSE) then
        Call Implicit_Stencil_Fill(region%myrank, input, dir, grid%iFilter, grid)
        if (region%myrank == 0) &
          Write (*,'(A,I1,A)') 'PlasComCM: Done building filter (dir = ',dir,').'
      end if

      ! ... create test filter matrices
      if (grid%iFilterTest /= FALSE) then
        Call Implicit_Stencil_Fill(region%myrank, input, dir, grid%iFilterTest, grid)
      end if

      ! ... create the SAT artificial dissipation operators
      if (grid%iSATArtDiss /= FALSE) then
        Call Implicit_Stencil_Fill(region%myrank, input, dir, grid%iSATArtDiss, grid)
      end if

      if (grid%iSATArtDissSplit /= FALSE) then
        Call Implicit_Stencil_Fill(region%myrank, input, dir, grid%iSATArtDissSplit, grid)
        if (err /= 0) Return
      end if

      ! ... create operators for USE in Jacobian
      if (input%timeScheme == BDF_GMRES_IMPLICIT) then
        Call Implicit_Stencil_Fill(region%myrank, input, dir, input%iFirstDerivImplicit, grid)
        Call Implicit_Stencil_Fill(region%myrank, input, dir, input%iSecondDerivImplicit, grid)
        if (input%Implicit_SAT_Art_Diss == TRUE) Call Implicit_Stencil_Fill(region%myrank, input, dir, input%iSecondDerivImplicitArtDiss, grid)
        if (input%LHS_Filter == TRUE) Call Implicit_Stencil_Fill(region%myrank, input, dir, input%iLHS_Filter, grid)
      end if

    end do

    ! ... clean up
    if (associated(grid%global_iblank) .eqv. .true.) then
      deallocate(grid%global_iblank)
    end if

    ! ... stop timer
    region%mpi_timings(ng)%operator_setup(:,:) = region%mpi_timings(ng)%operator_setup(:,:) + (MPI_WTime() - timer)

    return

  end subroutine OPERATOR_SETUP

  !-----------------------------------------------------------------------

  Function PER_INDEX(I,N)

    Integer :: I, N, PER_INDEX

    If (I .GT. N) Then
      PER_INDEX = I-N
    Else If (I .LT. 1) Then
      PER_INDEX = I+N
    Else
      PER_INDEX = I
    End If

  End Function PER_INDEX

  !-----------------------------------------------------------------------

  Function PER_INDEX2(I,N)

    Integer :: I, N, PER_INDEX2

    If (I .GT. N-1) Then
      PER_INDEX2 = I-N
    Else If (I .LT. 0) Then
      PER_INDEX2 = I+N
    Else
      PER_INDEX2 = I
    End If

  End Function PER_INDEX2

  !-----------------------------------------------------------------------

  subroutine OPERATOR_MATRIX_FILL(myrank, input, dir, opID, grid, periodic_flag, error_sum)

    USE ModGlobal
    USE ModDataStruct
    USE ModMPI
    USE ModMatrixVectorOps

    implicit none

    Integer :: myrank
    Type (t_mixt_input), pointer :: input
    Type (t_grid), pointer :: grid
    Integer :: dir, opID, periodic_flag

    ! ... local variables
    integer :: i, j, k, l0, nza, nzb, N, vds, nzbc, Ng, vdsGhost, ii, jj, kk, iblank, N_global, N1, N2
    integer :: left_bound, right_bound, ks, ke
    real(rfreal), pointer :: Bmat(:,:), Amat(:,:), Bmat_global(:,:), Amat_global(:,:)
    real(rfreal), pointer :: B(:,:)
    type(t_matrix_sparse), pointer :: SparseB(:), SparseBp
    type(t_matrix_sparse), pointer :: SparseA(:)
    type(t_matrix_sparse) :: A_global, B_global
    type(t_matrix_sparse) :: A_local, B_local
    real(rfreal), pointer :: dummy(:)
    integer :: iblank_left, iblank_right, lhs_max_width, rhs_max_width, l_counter, r_counter, counter, csg, ceg, csl, cel, h_counter
    integer :: max_num_boundaries, num_boundaries, N_global_ghost, opType, lhs_op_start, lhs_op_end, rhs_op_start, rhs_op_end, max_op_width
    integer :: bndry_width, bndry_depth, bndry_start, bndry_end, rel_bndry_loc, local_id, global_col_ind, error, error_sum
    integer, pointer :: l_boundary(:), r_boundary(:), global_iblank(:), hole_points(:)
    logical :: debug
    real(rfreal) :: timer
    integer :: stencil_size, smallStencilFound, l_boundary_small, r_boundary_small, iig, jjg, kkg
    integer :: smallStencil_counter, smallStencil_idx
    integer, pointer :: smallStencils(:,:)

    ! ... start timer
    timer = MPI_WTime()

    ! ... error flag
    error = FALSE

    ! ... local grid sizes
    ! ... number of points unique to this processor
    N        = grid%ie(dir) - grid%is(dir) + 1

    ! ... number of points, including ghost points
    Ng       = grid%ie(dir) - grid%is(dir) + 1 + SUM(grid%nGhostRHS(dir,:))

    ! ... number of points in plane with normal in direction dir
    vds      = grid%vds(dir)

    ! ... number of points in plane with normal in direction dir, including ghost
    vdsGhost = grid%vdsGhost(dir)

    ! ... total number points for this grid in direction dir
    N_global = grid%GlobalSize(dir)

    ! ... total number points for this grid in direction dir
    N_global_ghost = grid%GlobalSize(dir) + SUM(grid%nGhostRHS(dir,:))

    ! ... the global Bmatrix needs to include ghost points on both ends
    left_bound  = grid%is(dir) - grid%nGhostRHS(dir,1)
    right_bound = grid%ie(dir) + grid%nGhostRHS(dir,2)

    ! ... operator type (IMPLICIT, EXPLICIT)
    opType = grid%operator_type(opID)

    ! ... operator stencils (interior)
    lhs_op_start = input%lhs_interior_stencil_start(opID)
    lhs_op_end   = input%lhs_interior_stencil_end(opID)
    rhs_op_start = input%rhs_interior_stencil_start(opID)
    rhs_op_end   = input%rhs_interior_stencil_end(opID)
    max_op_width = input%max_operator_width(opID)
    bndry_width  = input%operator_bndry_width(opID)
    bndry_depth  = input%operator_bndry_depth(opID)

    ! ... maximum operator widths
    lhs_max_width = lhs_op_end - lhs_op_start + 1
    rhs_max_width = max_op_width

    ! ... maximum number of boundaries
    ! ... if we find more, then there are too many boundaries for the given stencil size
    max_num_boundaries = N_global/(2*MAX(lhs_max_width,rhs_max_width))+1
    max_num_boundaries = N_global ! ... wasteful but need code to allow for small stencils

    ! ... arrays holding left and right boundaries indices
    allocate(l_boundary(max_num_boundaries),r_boundary(max_num_boundaries))

    ! ... array holding local global_iblank
    allocate(global_iblank(N_global)); 
    allocate(hole_points(N_global));
    allocate(smallStencils(N_global,2))

    ! ... allocate space for all right-hand-side matrices
    Allocate(SparseB(vds)); nzb = -1

    ! ... initialize local matrices
    B_local%m = Ng; B_local%n = Ng;
    allocate(B_local%val(1,Ng*rhs_max_width)); 
    allocate(B_local%col_ind(1,Ng*rhs_max_width)); 
    allocate(B_local%row_ptr(1,Ng+1)); 
    allocate(B_local%nbc_row(1))
    allocate(B_local%bc_row(1,Ng+1))

    ! ... loop over points in the plane whose normal is dir
    do j = 1, grid%vds(dir)

      ! ... reset 
      smallStencilFound = FALSE
      Do i = 1, Ng*rhs_max_width
        B_local%val(1,i) = 0.0_WP
        B_local%col_ind(1,i) = 0
      End Do
      Do i = 1, Ng+1
        B_local%row_ptr(1,i) = 0
        B_local%bc_row(1,i) = 0
      End Do
      B_local%nbc_row(1) = 0

      ! ... back out our cross-plane indices in the global counting
      l0 = grid%vec_ind(j,1,dir)

      ! ... (ii, jj, kk) are the local indices
      N1 = grid%ie(1)-grid%is(1)+1
      N2 = grid%ie(2)-grid%is(2)+1
      ii = mod(l0-1,N1)+1
      jj = mod((l0-ii)/N1,N2)+1
      kk = (l0-ii-(jj-1)*N1)/(N1*N2)+1

      ! ... (ii, jj, kk) are now the global indices
      ii = ii + grid%is(1) - 1
      jj = jj + grid%is(2) - 1
      kk = kk + grid%is(3) - 1

      ! ... save global indices
      iig = ii
      jjg = jj
      kkg = kk

      ! ... extract global iblank for this line of points
      SELECT CASE (dir)
      CASE(1)
        do i = 1, N_global
          global_iblank(i) = grid%global_iblank(i,jj,kk)
        end do
      CASE(2)
        do i = 1, N_global
          global_iblank(i) = grid%global_iblank(ii,i,kk)
        end do
      CASE(3)
        do i = 1, N_global
          global_iblank(i) = grid%global_iblank(ii,jj,i)
        end do
      END SELECT

      ! ... first assume every point is an interior point
      ! ... explicit stencils
      do i = 1, Ng
        csg = 1 + rhs_max_width*(i-1)
        B_local%row_ptr(1,i) = csg
        do k = 1, rhs_op_end-rhs_op_start+1
           B_local%val(1,csg+k-1) = input%rhs_interior(opID,rhs_op_start+(k-1))
           B_local%col_ind(1,csg+k-1) = MIN(MAX(1,i + rhs_op_start + (k-1)),Ng)
        end do
      end do
      B_local%row_ptr(1,Ng+1) = 1 + rhs_max_width*Ng
 
      ! ... implicit stencils
      if ( opType == IMPLICIT ) then
        do i = 1, N
          grid%penta(opID,dir)%a(j,i) = input%lhs_interior(opID,-2)
          grid%penta(opID,dir)%b(j,i) = input%lhs_interior(opID,-1)
          grid%penta(opID,dir)%c(j,i) = input%lhs_interior(opID, 0)
          grid%penta(opID,dir)%d(j,i) = input%lhs_interior(opID,+1)
          grid%penta(opID,dir)%e(j,i) = input%lhs_interior(opID,+2)
        end do
      end if

      ! ... check periodicity for left-most and right-most boundaries
      l_counter = 0
      r_counter = 0
      if (periodic_flag == FALSE .and. global_iblank(1) /= 0) then
        l_counter = l_counter + 1; 
        l_boundary(l_counter) = 1;
      end if
      if (periodic_flag == FALSE .and. global_iblank(N_global) /= 0) then
        r_counter = r_counter + 1;
        r_boundary(r_counter) = N_global;
      end if

      ! ... Adjust the block below for problem-specifc mixes
      ! ... of periodic and wall boundary conditions
      ! if (dir == 2 .and. ii >= 26 .and. ii <= 74) then
      !   l_counter = l_counter + 1; 
      !   l_boundary(l_counter) = 1;
      !   r_counter = r_counter + 1;
      !   r_boundary(r_counter) = N_global;
      ! end if

      ! ... check for boundaries near array bounds when periodic
      if (periodic_flag >= TRUE) then

        ! ... left boundary
        perL: do i = 1, grid%nGhostRHS(dir,1)
          if (global_iblank(N_global-(i-1)) == 0) then
            l_counter = l_counter + 1
            l_boundary(l_counter) = i
            Exit perL
          end if
        end do perL

        ! ... right boundary
        perR: do i = 1, grid%nGhostRHS(dir,2)
          if (global_iblank(i) == 0) then
            r_counter = r_counter + 1
            r_boundary(r_counter) = N_global - (i-1)
            Exit perR
          end if
        end do perR

      end if

      ! ... search for holes
      h_counter = 0
      do i = 1, N_global
        if (global_iblank(i) == 0) then
          h_counter = h_counter + 1
          hole_points(h_counter) = i
        end if
      end do

      ! ... search for all interior boundaries, depending on direction dir
      do i = 1, N_global-1

        iblank_left  = global_iblank(i)
        iblank_right = global_iblank(i+1)

        ! ... right boundary at point i
        if ((iblank_left /= 0) .AND. (iblank_right == 0)) then 
          r_counter = r_counter + 1
          r_boundary(r_counter) = i

          ! ... left boundary at point i+1
        else if ((iblank_left == 0) .AND. (iblank_right /= 0)) then 
          l_counter = l_counter + 1
          l_boundary(l_counter) = i+1

        end if

      end do

      ! ... search for any small stencils
      smallStencil_counter = 0
      do i = 1, l_counter
        stencil_count: do ii = 1, r_counter
          stencil_size = r_boundary(ii) - l_boundary(i) + 1
          if (stencil_size > 0) then
            if (stencil_size < max_op_width) then
              smallStencil_counter = smallStencil_counter + 1
              smallStencils(smallStencil_counter,1) = i
              smallStencils(smallStencil_counter,2) = ii
              exit stencil_count
            end if
          end if
        end do stencil_count
      end do

      ! ... turn off all hole points on this grid
      do i = 1, h_counter
        ii = hole_points(i)
        if (ii >= left_bound .AND. ii <= right_bound) then

          ! ... explicit stencils
          jj = ii - left_bound + 1
          csl = B_local%row_ptr(1,jj)
          cel = B_local%row_ptr(1,jj+1)-1

          ! ... zero all values in this row
          B_local%val(1,csl:cel) = 0.0_rfreal; 
          B_local%col_ind(1,csl:cel) = 0; 

          ! ... give a non-zero value
          B_local%val(1,csl) = 0.0_rfreal;
          B_local%col_ind(1,csl) = jj

          ! ... implicit stencils
          if ( opType == IMPLICIT ) then
            if (ii >= grid%is(dir) .AND. ii <= grid%ie(dir)) then
              jj = ii - grid%is(dir) + 1
              grid%penta(opID,dir)%a(j,jj) = 0.0_rfreal
              grid%penta(opID,dir)%b(j,jj) = 0.0_rfreal
              grid%penta(opID,dir)%c(j,jj) = 1.0_rfreal
              grid%penta(opID,dir)%d(j,jj) = 0.0_rfreal
              grid%penta(opID,dir)%e(j,jj) = 0.0_rfreal
            end if
          end if
        end if
      end do

      ! ... now add left boundaries
      ! ... must be aware of boundaries that may lie on another processor but whose boundary stencil extends to us
      l_count: do i = 1, l_counter

        ! ... beginning and ending indices of points affected by boundary stencil in global counting
        ! ... note: this is not the same as the bndry_width, the size of the boundary stencil
        bndry_start = l_boundary(i)
        bndry_end   = bndry_start + bndry_depth - 1

        ! ... if boundary starts to the right of our extent, we don't care and we are done
        if (bndry_start > grid%ie(dir)) CYCLE l_count

        ! ... if boundary ends to the left of our extent, cycle to next boundary point
        if (bndry_end < grid%is(dir)) CYCLE l_count

!!$        ! ... check for stencils that won't fit and do something about it
!!$        ! ... for each left boundary find the nearest right boundary
!!$        stencil_count: do ii = 1, r_counter
!!$          stencil_size = r_boundary(ii) - l_boundary(i) + 1
!!$          if (stencil_size > 0) then
!!$            if (stencil_size < max_op_width) then
!!$              smallStencilFound = TRUE
!!$              exit stencil_count
!!$            end if
!!$          end if
!!$        end do stencil_count

        ! ... check if this boundary corresponds to a small stencil
        smallStencilFound = FALSE
        do ii = 1, smallStencil_counter
          if (smallStencils(ii,1) == i) then
            smallStencilFound = TRUE
            smallStencil_idx = ii
          end if
        end do
 
!!$        ! ... diagnostic information
!!$        if (smallStencilFound == TRUE) then
!!$          stencil_size = r_boundary(smallStencils(smallStencil_idx,2)) - l_boundary(smallStencils(smallStencil_idx,1)) + 1
!!$          write(*,'(A,I2)') 'PlasComCM: Found stencil_size = ', stencil_size
!!$          write(*,'(A,I4)') 'PlasComCM: l_boundary         = ', l_boundary(smallStencils(smallStencil_idx,1))
!!$          write(*,'(A,I4)') 'PlasComCM: r_boundary         = ', r_boundary(smallStencils(smallStencil_idx,2))
!!$          write(*,'(A,I2)') 'PlasComCM: dir                = ', dir
!!$          write(*,'(A,3(I4,1X))') 'PlasComCM: indices            = ', iig, jjg, kkg
!!$          write(*,'(A,I2)') 'PlasComCM: opID               = ', opID
!!$          write(*,'(A,I2)') 'PlasComCM: max_op_width       = ', max_op_width
!!$        end if

        ! ... don't completely finish this left boundary if too small
        if ((input%fix_stencil_cannot_fit == TRUE) .and. (smallStencilFound == TRUE)) then
          ks = MAX(bndry_start,left_bound)
          ke = MIN(bndry_start,right_bound)
          do k = ks, ke

            ! ... location relative to boundary
            rel_bndry_loc = k - bndry_start + 1

            if ( opType == IMPLICIT ) then
              global_col_ind = k 
              if (global_col_ind >= grid%is(dir) .AND. global_col_ind <= grid%ie(dir)) then
                jj = global_col_ind - grid%is(dir) + 1
                do kk = 1, 5
                  SELECT CASE (kk)
                  CASE (1)
                    grid%penta(opID,dir)%a(j,jj) = input%lhs_block1(opID,rel_bndry_loc,-2)
                  CASE (2)
                    grid%penta(opID,dir)%b(j,jj) = input%lhs_block1(opID,rel_bndry_loc,-1)
                  CASE (3)
                    grid%penta(opID,dir)%c(j,jj) = input%lhs_block1(opID,rel_bndry_loc, 0)
                  CASE (4)
                    grid%penta(opID,dir)%d(j,jj) = input%lhs_block1(opID,rel_bndry_loc,+1)
                  CASE (5)
                    grid%penta(opID,dir)%e(j,jj) = input%lhs_block1(opID,rel_bndry_loc,+2)
                  END SELECT
                end do
              end if
            end if
          end do
          cycle l_count
        end if

        ! ... flag user if overlap is not set large enough
        if ((bndry_start + bndry_width - 1) > right_bound) then
          error = TRUE
          write (*,'(2(A,I4))') 'PlasComCM: ERROR: (1) left boundary stencil on rank ', myrank, &
                 ' cannot fit: increase nOverlap to be ', input%nOverLap + (bndry_start + bndry_width - 1) - right_bound
          write (*,'(A,I4)') 'PlasComCM: dir          = ', dir
          write (*,'(A,I7)') 'PlasComCM: j            = ', j
          write (*,'(A,I4)') 'PlasComCM: opID         = ', opID
          write (*,'(A,I4)') 'PlasComCM: bndry_start  = ', bndry_start
          write (*,'(A,I4)') 'PlasComCM: bndry_end    = ', bndry_end
          write (*,'(A,I4)') 'PlasComCM: bndry_width  = ', bndry_width
          write (*,'(A,I4)') 'PlasComCM: left_bound   = ', left_bound
          write (*,'(A,I4)') 'PlasComCM: right_bound  = ', right_bound
          write (*,'(A,I4)') 'PlasComCM: is           = ', grid%is(dir)
          write (*,'(A,I4)') 'PlasComCM: ie           = ', grid%ie(dir)
          write (*,'(A,I4)') 'PlasComCM: nGhostRHS(1) = ', grid%nGhostRHS(dir,1)
          write (*,'(A,I4)') 'PlasComCM: nGhostRHS(2) = ', grid%nGhostRHS(dir,2)
          write (*,'(A,I4)') 'PlasComCM: nOverlap     = ', input%nOverlap
        else
          ! ... normal stencil
          ! ... add in the boundary stencils to the affected points
          ks = MAX(bndry_start,left_bound)
          ke = MIN(bndry_end,right_bound)
          do k = ks, ke

            ! ... location relative to boundary
            rel_bndry_loc = k - bndry_start + 1

            ! ... location relative to this processor's left boundary
            local_id = k - left_bound + 1

            ! ... zero out existing stencil for this point
            csl = B_local%row_ptr(1,local_id)
            cel = B_local%row_ptr(1,local_id+1)-1
            B_local%val(1,csl:cel) = 0.0_rfreal; 
            B_local%col_ind(1,csl:cel) = 0

            ! ... this is a valid boundary row
            B_local%nbc_row(1) = B_local%nbc_row(1) + 1
            B_local%bc_row(1,B_local%nbc_row(1)) = local_id

            ! ... update RHS stencil, making sure we live only on this processor
            do kk = 1, bndry_width
              global_col_ind = ks + kk - 1
              if (global_col_ind >= left_bound .AND. global_col_ind <= right_bound) then
                B_local%val(1,csl+(kk-1)) = input%rhs_block1(opID,rel_bndry_loc,kk)
                B_local%col_ind(1,csl+(kk-1)) = global_col_ind - left_bound + 1
              end if
            end do

            ! ... update LHS stencil, making sure we live only on this processor
            if ( opType == IMPLICIT ) then
              global_col_ind = k 
              if (global_col_ind >= grid%is(dir) .AND. global_col_ind <= grid%ie(dir)) then
                jj = global_col_ind - grid%is(dir) + 1
                do kk = 1, 5
                  SELECT CASE (kk)
                  CASE (1)
                    grid%penta(opID,dir)%a(j,jj) = input%lhs_block1(opID,rel_bndry_loc,-2)
                  CASE (2)
                    grid%penta(opID,dir)%b(j,jj) = input%lhs_block1(opID,rel_bndry_loc,-1)
                  CASE (3)
                    grid%penta(opID,dir)%c(j,jj) = input%lhs_block1(opID,rel_bndry_loc, 0)
                  CASE (4)
                    grid%penta(opID,dir)%d(j,jj) = input%lhs_block1(opID,rel_bndry_loc,+1)
                  CASE (5)
                    grid%penta(opID,dir)%e(j,jj) = input%lhs_block1(opID,rel_bndry_loc,+2)
                  END SELECT
                end do
              end if
            end if

          end do

        end if

      end do l_count

      ! ... now add right boundaries
      ! ... must be aware of boundaries that may lie on another processor but whose boundary stencil extends to us
      r_count: do i = 1, r_counter

        ! ... beginning and ending indices of points affected by boundary stencil in global counting
        ! ... note: this is not the same as the bndry_width, the size of the boundary stencil
        bndry_end   = r_boundary(i)                ! global counting
        bndry_start = bndry_end - bndry_depth + 1  ! global counting

        ! ... if boundary starts to the right of our extent, we don't care and we are done
        if (bndry_start > grid%ie(dir)) CYCLE r_count

        ! ... if boundary ends to the left of our extent, cycle to next boundary point
        if (bndry_end < grid%is(dir)) CYCLE r_count

        ! ... check if this boundary corresponds to a small stencil
        smallStencilFound = FALSE
        do ii = 1, smallStencil_counter
          if (smallStencils(ii,2) == i) then
            smallStencilFound = TRUE
            smallStencil_idx = ii
          end if
        end do

        ! ... take care of right boundary separately
        if ((input%fix_stencil_cannot_fit == TRUE) .and. (smallStencilFound == TRUE)) then
          ks = MAX(bndry_end,left_bound)
          ke = MIN(bndry_end,right_bound)
          do k = ks, ke
            ! ... location relative to boundary
            rel_bndry_loc = bndry_end - k + 1
            ! ... update LHS stencil, making sure we live only on this processor
            if ( opType == IMPLICIT ) then
              global_col_ind = k 
              if (global_col_ind >= grid%is(dir) .AND. global_col_ind <= grid%ie(dir)) then
                jj = global_col_ind - grid%is(dir) + 1
                do kk = 1, 5
                  SELECT CASE (kk)
                  CASE (1)
                    grid%penta(opID,dir)%a(j,jj) = input%lhs_block2(opID,bndry_depth-rel_bndry_loc+1,-2)
                  CASE (2)
                    grid%penta(opID,dir)%b(j,jj) = input%lhs_block2(opID,bndry_depth-rel_bndry_loc+1,-1)
                  CASE (3)
                    grid%penta(opID,dir)%c(j,jj) = input%lhs_block2(opID,bndry_depth-rel_bndry_loc+1, 0)
                  CASE (4)
                    grid%penta(opID,dir)%d(j,jj) = input%lhs_block2(opID,bndry_depth-rel_bndry_loc+1,+1)
                  CASE (5)
                    grid%penta(opID,dir)%e(j,jj) = input%lhs_block2(opID,bndry_depth-rel_bndry_loc+1,+2)
                  END SELECT
                end do
              end if
            end if
          end do
          CYCLE r_count
        end if

        ! ... flag user if overlap is not set large enough and we should exit on stencil problems
        if ((bndry_end - bndry_width + 1) < left_bound) then
          error = TRUE
          write (*,'(2(A,I4))') 'PlasComCM: ERROR: (1) right boundary stencil on rank ', myrank, &
               ' cannot fit: increase nOverlap to be: ', input%nOverLap + left_bound - (bndry_end - bndry_width + 1)
          write (*,'(A,I4)') 'PlasComCM: dir          = ', dir
          write (*,'(A,I7)') 'PlasComCM: j            = ', j
          write (*,'(A,I4)') 'PlasComCM: opID         = ', opID
          write (*,'(A,I4)') 'PlasComCM: bndry_start  = ', bndry_start
          write (*,'(A,I4)') 'PlasComCM: bndry_end    = ', bndry_end
          write (*,'(A,I4)') 'PlasComCM: bndry_width  = ', bndry_width
          write (*,'(A,I4)') 'PlasComCM: left_bound   = ', left_bound
          write (*,'(A,I4)') 'PlasComCM: right_bound  = ', right_bound
          write (*,'(A,I4)') 'PlasComCM: is           = ', grid%is(dir)
          write (*,'(A,I4)') 'PlasComCM: ie           = ', grid%ie(dir)
          write (*,'(A,I4)') 'PlasComCM: nGhostRHS(1) = ', grid%nGhostRHS(dir,1)
          write (*,'(A,I4)') 'PlasComCM: nGhostRHS(2) = ', grid%nGhostRHS(dir,2)
          write (*,'(A,I4)') 'PlasComCM: nOverlap     = ', input%nOverlap
        else
          ! ... normal stencil
          ! ... add in the boundary stencils to the affected points
          ks = MAX(bndry_start,left_bound)
          ke = MIN(bndry_end,right_bound)
          do k = ks, ke

            ! ... location relative to boundary
            rel_bndry_loc = bndry_end - k + 1

            ! ... location relative to this processor's left boundary
            local_id = k - left_bound + 1

            ! ... zero out existing stencil for this point
            csl = B_local%row_ptr(1,local_id)
            cel = B_local%row_ptr(1,local_id+1)-1
            B_local%val(1,csl:cel) = 0.0_rfreal; 
            B_local%col_ind(1,csl:cel) = 0

            ! ... this is a valid boundary row
            B_local%nbc_row(1) = B_local%nbc_row(1) + 1
            B_local%bc_row(1,B_local%nbc_row(1)) = local_id

            ! ... update stencil, making sure we live only on this processor
            do kk = 1, bndry_width
              global_col_ind = ke - kk + 1
              if (global_col_ind >= left_bound .AND. global_col_ind <= right_bound) then
                B_local%val(1,csl+(kk-1))     = input%rhs_block2(opID,bndry_depth-rel_bndry_loc+1,bndry_width-kk+1)
                B_local%col_ind(1,csl+(kk-1)) = global_col_ind - left_bound + 1
              end if
            end do

            ! ... update LHS stencil, making sure we live only on this processor
            if ( opType == IMPLICIT ) then
              global_col_ind = k 
              if (global_col_ind >= grid%is(dir) .AND. global_col_ind <= grid%ie(dir)) then
                jj = global_col_ind - grid%is(dir) + 1
                do kk = 1, 5
                  SELECT CASE (kk)
                  CASE (1)
                    grid%penta(opID,dir)%a(j,jj) = input%lhs_block2(opID,bndry_depth-rel_bndry_loc+1,-2)
                  CASE (2)
                    grid%penta(opID,dir)%b(j,jj) = input%lhs_block2(opID,bndry_depth-rel_bndry_loc+1,-1)
                  CASE (3)
                    grid%penta(opID,dir)%c(j,jj) = input%lhs_block2(opID,bndry_depth-rel_bndry_loc+1, 0)
                  CASE (4)
                    grid%penta(opID,dir)%d(j,jj) = input%lhs_block2(opID,bndry_depth-rel_bndry_loc+1,+1)
                  CASE (5)
                    grid%penta(opID,dir)%e(j,jj) = input%lhs_block2(opID,bndry_depth-rel_bndry_loc+1,+2)
                  END SELECT
                end do
              end if
            end if
          end do

        end if

      end do r_count

      ! ... may have to fix stencils
      if ((input%fix_stencil_cannot_fit == TRUE) .and. (smallStencil_counter > 0)) then
!!$        write (*,'(A)')    'PlasComCM: Fixing small stencil(s) for ...'
!!$        write (*,'(A,I2)') '        opID = ', opID
!!$        write (*,'(A,I2)') '        dir  = ', dir
!!$        write (*,'(A,I4)') '        iig  = ', iig
!!$        write (*,'(A,I4)') '        jjg  = ', jjg
!!$        write (*,'(A,I4)') '        kkg  = ', kkg
!!$        write (*,'(A,I2)') '        num  = ', smallStencil_counter
        Call Fix_Stencils_Cannot_Fit(input,opID,left_bound,right_bound,B_local,N_global,global_iblank,r_counter,r_boundary,l_counter,l_boundary,smallStencil_counter,smallStencils,grid%is(dir),grid%ie(dir))
      end if

      ! ... compress matrices
      SparseBp => SparseB(j)
      Call Compress_CRS_Matrix(B_local, SparseBp)
      nzb = MAX(nzb,size(SparseBp%val,2))

    end do

    error_sum = error
    if (error_sum /= 0) then
      write (*,'(A)') 'PlasComCM: Error code found in operator creation.  Check output.'
      return
    end if

    ! ... allocate space for the stencils
    allocate(grid%B(opID,dir)%val(nzb,vdsGhost))
    allocate(grid%B(opID,dir)%col_ind(nzb,vdsGhost))
    allocate(grid%B(opID,dir)%row_ptr(vdsGhost,Ng+1))
    allocate(grid%B(opID,dir)%nbc_row(vdsGhost))
    allocate(grid%B(opID,dir)%bc_row(vdsGhost,Ng+1))

    ! ... copy the RHS stencils
    grid%B(opID,dir)%m = Ng;  grid%B(opID,dir)%n = Ng;
    do k = 1, vdsGhost
      nzb = size(SparseB(k)%val,2)
      Do i = 1, nzb
        grid%B(opID,dir)%val(i,k) = SparseB(k)%val(1,i)
        grid%B(opID,dir)%col_ind(i,k) = SparseB(k)%col_ind(1,i)
      End Do
      Do i = 1, Ng+1
        grid%B(opID,dir)%row_ptr(k,i) = SparseB(k)%row_ptr(1,i)
      End Do
      grid%B(opID,dir)%nbc_row(k) = SparseB(k)%nbc_row(1)
      do i = 1, SparseB(k)%nbc_row(1)
        grid%B(opID,dir)%bc_row(k,i) = SparseB(k)%bc_row(1,i)
      end do
    end do

    ! ... clean up
    do j = 1, size(SparseB)
      deallocate(SparseB(j)%val)
      deallocate(SparseB(j)%col_ind)
      deallocate(SparseB(j)%row_ptr)
    end do
    deallocate(SparseB,B_local%row_ptr,B_local%val,B_local%col_ind)
    deallocate(hole_points,l_boundary,r_boundary,global_iblank,smallStencils)

  end subroutine OPERATOR_MATRIX_FILL

  ! ... locally collect edge information for each processor, WZhang 08/2014

  subroutine Collect_Local_Edge_Information(region, gridID, input, dir, grid, periodic_flag)

    USE ModGlobal
    USE ModDataStruct
    USE ModMPI
    USE ModMatrixVectorOps
    USE ModDeriv

    implicit none

    type(t_region), pointer :: region
    type (t_mixt_input), pointer :: input
    type (t_grid), pointer :: grid
    integer :: dir, periodic_flag, gridID

    ! ... local variables
    integer ND, Nc, NGhost, N, Ng, vds, vdsGhost, Ncg, Nglobal(MAX_ND), Nlocal(MAX_ND)
    integer :: NwithHalo(MAX_ND), ijk_gOffset(MAX_ND), bndry_offset(2), bndry_width, bndry_depth
    integer :: scan_extent, iblank_left, iblank_right, my_overlap, ierr
    integer, pointer :: iblank_aux(:)
    real(rfreal), pointer :: iblank_aux_dble(:)
    integer, pointer :: recvcounts(:), recvdisp(:), sendbuf(:), recvbuf(:)
    integer :: nproc, i, j, k, l0, l1, m, ii, jj, kk, l_left, l_right, l_left_nohalo, l_right_nohalo, bndry_extended
    integer :: l_ctr_gl, r_ctr_gl, l_bndry_gl(2*MAX_BNDRY), r_bndry_gl(2*MAX_BNDRY), bndry_overlap(2)
    integer :: l_ctr_gl_rd, r_ctr_gl_rd, l_bndry_gl_rd(MAX_BNDRY), r_bndry_gl_rd(MAX_BNDRY), myrank_inpencil, nproc_inpencil
    real(rfreal), pointer :: F(:)

    ! ... dimensions of the grid
    ND       = grid%ND

    ! ... number of grid points, unique to this processor
    Nc       = grid%nCells

    ! ... number of ghost points
    NGhost   = grid%nGhostRHS(dir,1) + grid%nGhostRHS(dir,2)

    ! ... number of points in direction dir, unique to this processor
    N        = grid%ie(dir) - grid%is(dir) + 1

    ! ... number of points, including ghost points
    Ng       = N + NGhost

    ! ... number of points in plane with normal in direction dir
    vds      = grid%vds(dir)

    ! ... number of points in plane with normal in direction dir, including ghost
    vdsGhost = grid%vdsGhost(dir)

    ! ... number of grid points, including ghost points, unique to this processor
    Ncg      = Nc + NGhost*vdsGhost

    ! ... global grid sizes
    Nglobal(:)       = 1
    Nglobal(1:ND)    = grid%GlobalSize(1:ND)

    ! ... local grid sizes
    Nlocal(:)        = 1
    Nlocal(1:ND)     = grid%ie(1:ND) - grid%is(1:ND) + 1

    ! ... extend in the halo direction
    NwithHalo(:)     = 1
    NwithHalo(1:ND)  = Nlocal(1:ND)
    NwithHalo(dir)   = Nlocal(dir) + NGhost

    ! ... offset for ghost cell exchange in direction dir
    ijk_gOffset(:)   = 0
    ijk_gOffset(dir) = grid%nGhostRHS(dir,1)

    ! ... depth and width of the boundary
    bndry_depth      = maxval(input%operator_bndry_depth(:))
    bndry_width      = maxval(input%operator_bndry_width(:))

    ! ... number of processors
    Call MPI_COMM_SIZE(grid%pencilcomm(dir), nproc_inpencil, ierr)

    ! ... allocate space for storing edge information
    if (.not.allocated(grid%l_ctr_lc) .eqv. .true.) then
      allocate(grid%l_ctr_lc(maxval(grid%vds(:)), MAX_ND))
    end if
    if (.not.allocated(grid%r_ctr_lc) .eqv. .true.) then
      allocate(grid%r_ctr_lc(maxval(grid%vds(:)), MAX_ND))
    end if

    if (.not.allocated(grid%l_bndry_lc) .eqv. .true.) then
      allocate(grid%l_bndry_lc(MAX_BNDRY, maxval(grid%vds(:)), MAX_ND))
    end if
    if (.not.allocated(grid%r_bndry_lc) .eqv. .true.) then
      allocate(grid%r_bndry_lc(MAX_BNDRY, maxval(grid%vds(:)), MAX_ND))
    end if

    if (.not.allocated(grid%l_overlap_lc) .eqv. .true.) then
      allocate(grid%l_overlap_lc(MAX_BNDRY, maxval(grid%vds(:)), MAX_ND))
    end if
    if (.not.allocated(grid%r_overlap_lc) .eqv. .true.) then
      allocate(grid%r_overlap_lc(MAX_BNDRY, maxval(grid%vds(:)), MAX_ND))
    end if

    if (.not.allocated(grid%l_extended) .eqv. .true.) then
      allocate(grid%l_extended(maxval(grid%vds(:)), MAX_ND))
    end if
    if (.not.allocated(grid%r_extended) .eqv. .true.) then
      allocate(grid%r_extended(maxval(grid%vds(:)), MAX_ND))
    end if

    if (.not.allocated(grid%l_extended_size) .eqv. .true.) then
      allocate(grid%l_extended_size(maxval(grid%vds(:)), MAX_ND))
    end if
    if (.not.allocated(grid%r_extended_size) .eqv. .true.) then
      allocate(grid%r_extended_size(maxval(grid%vds(:)), MAX_ND))
    end if

    ! ... temporary storage
    allocate(iblank_aux(Ncg))
    allocate(iblank_aux_dble(Ncg))

    Do i = 1, Ncg
      iblank_aux(i) = 0
    End Do

    ! ... Step 1 ghost cell exchange to obtain one layer iblank value from neighbouring processor
    do k = 1, Nlocal(3)
      do j = 1, Nlocal(2)
        do i = 1, Nlocal(1)

          ! ... local counting
          l0 = (k-1)*Nlocal(2)*Nlocal(1) + (j-1)*Nlocal(1) + i

          ! ... counting in halo coordinates 
          l1 = (k-1+ijk_gOffset(3))*NwithHalo(1)*NwithHalo(2) + (j-1+ijk_gOffset(2))*NwithHalo(1) + i+ijk_gOffset(1)

          ! ... fill the buffer
          iblank_aux(l1) = grid%iblank(l0)

        end do
      end do
    end do

    if (ANY(grid%nGhostRHS(dir,:) .GT. 0)) then
      iblank_aux_dble(:) = dble(iblank_aux(:))
      Call Ghost_Cell_Exchange(region, gridID, dir, iblank_aux_dble)
      iblank_aux(:) = int(iblank_aux_dble(:))
    end if

    do m = 1, grid%vds(dir)

    ! ... Step 2 count the edges in each processor and store the global index

      grid%l_ctr_lc(m,dir) = 0
      grid%r_ctr_lc(m,dir) = 0

      ! ... back out our cross-plane indices in the global counting
      l0 = grid%vec_ind(m,1,dir)

      ! ... (ii, jj, kk) are the local indices
      ii = mod(l0-1,Nlocal(1))+1
      jj = mod((l0-ii)/Nlocal(1),Nlocal(2))+1
      kk = (l0-ii-(jj-1)*Nlocal(1))/(Nlocal(1)*Nlocal(2))+1

      ! ... (ii, jj, kk) are now the global indices
      ii = ii + grid%is(1) - 1
      jj = jj + grid%is(2) - 1
      kk = kk + grid%is(3) - 1

      ! ... check whether we have left-most boundary

      bndry_offset(1) = 0
      bndry_offset(2) = 0

      Select Case (dir)
        Case (1)
          if (ii == 1) then
            if (periodic_flag == FALSE .and. grid%iblank(l0) /= 0) then
              grid%l_ctr_lc(m,1) = grid%l_ctr_lc(m,1) + 1
              grid%l_bndry_lc(grid%l_ctr_lc(m,1),m,1) = 1
              bndry_offset(1) = 1
            else
              bndry_offset(1) = 1
            end if
          end if
        Case (2)
          if (jj == 1) then
            if (periodic_flag == FALSE .and. grid%iblank(l0) /= 0) then
              grid%l_ctr_lc(m,2) = grid%l_ctr_lc(m,2) + 1
              grid%l_bndry_lc(grid%l_ctr_lc(m,2),m,2) = 1
              bndry_offset(1) = 1
            else
              bndry_offset(1) = 1
            end if
          end if
        Case (3)
          if (kk == 1) then 
            if (periodic_flag == FALSE .and. grid%iblank(l0) /= 0) then
              grid%l_ctr_lc(m,3) = grid%l_ctr_lc(m,3) + 1
              grid%l_bndry_lc(grid%l_ctr_lc(m,3),m,3) = 1
              bndry_offset(1) = 1
            else
              bndry_offset(1) = 1
            end if
          end if
      End Select

      ! ... check whether we have right-most boundary
      Select Case (dir)
        Case (1)
          if (ii+Nlocal(1)-1 == Nglobal(1)) then
            if (periodic_flag == FALSE .and. grid%iblank(l0+Nlocal(1)-1) /= 0) then
              grid%r_ctr_lc(m,1) = grid%r_ctr_lc(m,1) + 1
              grid%r_bndry_lc(grid%r_ctr_lc(m,1),m,1) = Nglobal(1)
              bndry_offset(2) = 1
            else
              bndry_offset(2) = 1
            end if
          end if
        Case (2)
          if (jj+Nlocal(2)-1 == Nglobal(2)) then
            if (periodic_flag == FALSE .and. grid%iblank(l0+(Nlocal(2)-1)*Nlocal(1)) /= 0) then
              grid%r_ctr_lc(m,2) = grid%r_ctr_lc(m,2) + 1
              grid%r_bndry_lc(grid%r_ctr_lc(m,2),m,2) = Nglobal(2)
              bndry_offset(2) = 1
            else
              bndry_offset(2) = 1
            end if
          end if
        Case (3)
          if (kk+Nlocal(3)-1 == Nglobal(3)) then
            if (periodic_flag == FALSE .and. grid%iblank(l0+(Nlocal(3)-1)*Nlocal(2)*Nlocal(1)) /= 0) then
              grid%r_ctr_lc(m,3) = grid%r_ctr_lc(m,3) + 1
              grid%r_bndry_lc(grid%r_ctr_lc(m,3),m,3) = Nglobal(3)
              bndry_offset(2) = 1
            else
              bndry_offset(2) = 1
            end if
          end if
      End Select

      ! ... search for all interior boundaries, depending on direction dir
       
      ! ... left boundary
      scan_extent = Nlocal(dir)-bndry_offset(1)-bndry_offset(2)
      do i = 1, scan_extent
        l_left  = grid%vec_indGhost(m,i+ijk_goffset(dir)+bndry_offset(1)-1,dir)
        l_right = grid%vec_indGhost(m,i+ijk_goffset(dir)+bndry_offset(1),  dir)
        iblank_left  = iblank_aux(l_left)
        iblank_right = iblank_aux(l_right)

        ! ... left boundary at point l_right
        if ((iblank_left == 0) .AND. (iblank_right /= 0)) then

          ! ... go back to the index without ghost points       
          l_right_nohalo = grid%vec_ind(m,i+bndry_offset(1),dir)

          ! ... (ii, jj, kk) are the local indices
          ii = mod(l_right_nohalo-1,Nlocal(1))+1
          jj = mod((l_right_nohalo-ii)/Nlocal(1),Nlocal(2))+1
          kk = (l_right_nohalo-ii-(jj-1)*Nlocal(1))/(Nlocal(1)*Nlocal(2))+1

          ! ... (ii, jj, kk) are now the global indices
          ii = ii + grid%is(1) - 1
          jj = jj + grid%is(2) - 1
          kk = kk + grid%is(3) - 1

          grid%l_ctr_lc(m,dir) = grid%l_ctr_lc(m,dir) + 1
          Select Case (dir)
            Case(1)
              grid%l_bndry_lc(grid%l_ctr_lc(m,1),m,1) = ii
            Case(2)
              grid%l_bndry_lc(grid%l_ctr_lc(m,2),m,2) = jj
            Case(3)
              grid%l_bndry_lc(grid%l_ctr_lc(m,3),m,3) = kk
          End Select

        end if

      end do

      ! ... right boundary
      scan_extent = Nlocal(dir)-bndry_offset(1)-bndry_offset(2)
      do i = 1, scan_extent
        l_left  = grid%vec_indGhost(m,i+ijk_goffset(dir)+bndry_offset(1),dir)
        l_right = grid%vec_indGhost(m,i+ijk_goffset(dir)+bndry_offset(1)+1,  dir)
        iblank_left  = iblank_aux(l_left)
        iblank_right = iblank_aux(l_right)

        ! ... right boundary at point l_left
        if ((iblank_left /= 0) .AND. (iblank_right == 0)) then

          ! ... go back to the index without ghost points       
          l_left_nohalo = grid%vec_ind(m,i+bndry_offset(1),dir)

          ! ... (ii, jj, kk) are the local indices
          ii = mod(l_left_nohalo-1,Nlocal(1))+1
          jj = mod((l_left_nohalo-ii)/Nlocal(1),Nlocal(2))+1
          kk = (l_left_nohalo-ii-(jj-1)*Nlocal(1))/(Nlocal(1)*Nlocal(2))+1

          ! ... (ii, jj, kk) are now the global indices
          ii = ii + grid%is(1) - 1
          jj = jj + grid%is(2) - 1
          kk = kk + grid%is(3) - 1

          grid%r_ctr_lc(m,dir) = grid%r_ctr_lc(m,dir) + 1
          Select Case (dir)
            Case(1)
              grid%r_bndry_lc(grid%r_ctr_lc(m,1),m,1) = ii
            Case(2)
              grid%r_bndry_lc(grid%r_ctr_lc(m,2),m,2) = jj
            Case(3)
              grid%r_bndry_lc(grid%r_ctr_lc(m,3),m,3) = kk
          End Select

        end if

      end do
      
    ! ... Step 3 gather information from each processor to have a global view
      Call MPI_BARRIER(grid%pencilcomm(dir), IERR)
      allocate(recvcounts(nproc_inpencil))
      allocate(recvdisp(nproc_inpencil))

      ! ... left boundary
      ! ... use MPI_Allgather to determine the size of the data to be sent
      Call MPI_AllGATHER(grid%l_ctr_lc(m,dir), 1, MPI_INTEGER, recvcounts, 1, MPI_INTEGER, grid%pencilcomm(dir), ierr)
      recvdisp(1) = 0
      do i = 2, nproc_inpencil
        recvdisp(i) = recvdisp(i-1) + recvcounts(i-1)
      end do
      l_ctr_gl = SUM(recvcounts(:))

      ! ... call allgatherv

      allocate(sendbuf(grid%l_ctr_lc(m,dir)))
      allocate(recvbuf(l_ctr_gl))

      do i = 1, grid%l_ctr_lc(m,dir)
        sendbuf(i) = grid%l_bndry_lc(i,m,dir)
      end do

      Call MPI_AllGATHERV(sendbuf, grid%l_ctr_lc(m,dir), MPI_INTEGER, recvbuf, recvcounts, recvdisp, MPI_INTEGER, grid%pencilcomm(dir), ierr)

      do i = 1, l_ctr_gl
        l_bndry_gl(i) = recvbuf(i)
      end do

      deallocate(sendbuf)
      deallocate(recvbuf)

      ! ... right boundary
      ! ... use MPI_Allgather to determine the size of the data to be sent
      Call MPI_AllGATHER(grid%r_ctr_lc(m,dir), 1, MPI_INTEGER, recvcounts, 1, MPI_INTEGER, grid%pencilcomm(dir), ierr)
      recvdisp(1) = 0
      do i = 2, nproc_inpencil
        recvdisp(i) = recvdisp(i-1) + recvcounts(i-1)
      end do
      r_ctr_gl = SUM(recvcounts(:))

      ! ... call allgatherv

      allocate(sendbuf(grid%r_ctr_lc(m,dir)))
      allocate(recvbuf(r_ctr_gl))

      do i = 1, grid%r_ctr_lc(m,dir)
        sendbuf(i) = grid%r_bndry_lc(i,m,dir)
      end do

      Call MPI_AllGATHERV(sendbuf, grid%r_ctr_lc(m,dir), MPI_INTEGER, recvbuf, recvcounts, recvdisp, MPI_INTEGER, grid%pencilcomm(dir), ierr)

      do i = 1, r_ctr_gl
        r_bndry_gl(i) = recvbuf(i)
      end do

      deallocate(sendbuf)
      deallocate(recvbuf)

      deallocate(recvcounts)
      deallocate(recvdisp)

    ! ... Step 4 reduce: search for unique elements
      l_ctr_gl_rd = 0
      r_ctr_gl_rd = 0
      l_bndry_gl_rd(:) = 0
      r_bndry_gl_rd(:) = 0

      ! ... left boundary
      do i = 2, l_ctr_gl
        if (l_bndry_gl(i) /= l_bndry_gl(i-1)) then
          l_ctr_gl_rd = l_ctr_gl_rd + 1
          l_bndry_gl_rd(l_ctr_gl_rd) = l_bndry_gl(i-1)
        end if
      end do

      if (l_ctr_gl > 0) then  ! ... JByun 09/30/14
        l_ctr_gl_rd = l_ctr_gl_rd + 1
        l_bndry_gl_rd(l_ctr_gl_rd) = l_bndry_gl(l_ctr_gl)
      end if

      ! ... right boundary
      do i = 2, r_ctr_gl
        if (r_bndry_gl(i) /= r_bndry_gl(i-1)) then
          r_ctr_gl_rd = r_ctr_gl_rd + 1
          r_bndry_gl_rd(r_ctr_gl_rd) = r_bndry_gl(i-1)
        end if
      end do

      if (r_ctr_gl > 0) then ! ... JByun 09/30/14
        r_ctr_gl_rd = r_ctr_gl_rd + 1
        r_bndry_gl_rd(r_ctr_gl_rd) = r_bndry_gl(r_ctr_gl)
      end if 

    ! ... Step 5 must be aware of boundaries that may lie on another processor but whose boundary stencil extends to us
      grid%l_extended(m,dir) = 0
      grid%l_extended_size(m,dir) = 0
      grid%r_extended(m,dir) = 0
      grid%r_extended_size(m,dir) = 0

      ! ... left boundary
      do i = 1, l_ctr_gl_rd
        if (l_bndry_gl_rd(i) < grid%is(dir)) then
          bndry_extended = l_bndry_gl_rd(i)+bndry_depth-1

          if ((grid%is(dir) <= bndry_extended) .and. (bndry_extended <= grid%ie(dir))) then
            grid%l_extended(m,dir) = 1
            grid%l_extended_size(m,dir) = bndry_extended - grid%is(dir) + 1
          end if

        end if
      end do

      ! ... right boundary
      do i = 1, r_ctr_gl_rd
        if (r_bndry_gl_rd(i) > grid%ie(dir)) then
          bndry_extended = r_bndry_gl_rd(i)-(bndry_depth-1)

          if ((grid%is(dir) <= bndry_extended) .and. (bndry_extended <= grid%ie(dir))) then
            grid%r_extended(m,dir) = 1
            grid%r_extended_size(m,dir) = grid%ie(dir) - bndry_extended + 1
          end if

        end if
      end do

    end do ! ... m

!    do m = 1, grid%vds(dir)
!      write(*,'(A,I4,A,I4,A,I4,A,I4)') 'Add Holes:Processor', region%myrank, ': Found', grid%l_ctr_lc(m,dir), ' left edges for m=', m, ' dir=', dir
!        do i = 1, grid%l_ctr_lc(m,dir)
!          write(*,'(A,I4,A,I4,A,I4)') 'Add Holes:Processor', region%myrank, ': Location of left edge ', i, ' is ', grid%l_bndry_lc(i,m,dir)
!        end do
!      if (grid%l_extended(m,dir) > 0) then
!        write(*,'(A,I4,A,I4)') 'Add Holes:Processor', region%myrank, ': Has extended left edge of size ', grid%l_extended_size(m,dir)
!      end if
!    end do
!
!    do m = 1, grid%vds(dir)
!      write(*,'(A,I4,A,I4,A,I4,A,I4)') 'Add Holes:Processor', region%myrank, ': Found', grid%r_ctr_lc(m,dir), ' right edges for m=', m, ' dir=', dir
!        do i = 1, grid%r_ctr_lc(m,dir)
!          write(*,'(A,I4,A,I4,A,I4)') 'Add Holes:Processor', region%myrank, ': Location of right edge ', i, ' is ', grid%r_bndry_lc(i,m,dir)
!        end do
!      if (grid%r_extended(m,dir) > 0) then
!        write(*,'(A,I4,A,I4)') 'Add Holes:Processor', region%myrank, ': Has extended right edge of size ', grid%r_extended_size(m,dir)
!      end if
!    end do
 
    ! ... Step 6 we need to know the maximum number of ghost points we need
    bndry_overlap(:) = 0

    ! ... left boundary
    grid%l_overlap_lc(:,:,dir) = 0
    do m = 1, grid%vds(dir)
      do i = 1, grid%l_ctr_lc(m,dir)
        my_overlap = bndry_width - (grid%ie(dir) - grid%l_bndry_lc(i,m,dir) + 1)
        if (my_overlap > 0) then
          grid%l_overlap_lc(i,m,dir) = my_overlap
        end if
      end do
      if ((bndry_depth - grid%r_extended_size(m,dir)) > maxval(grid%l_overlap_lc(:,m,dir))) then
        grid%l_overlap_lc(:,m,dir) = bndry_depth - grid%r_extended_size(m,dir)
      end if
    end do

    if (maxval(grid%l_overlap_lc(:,:,dir)) > 0) then
      bndry_overlap(2) = maxval(grid%l_overlap_lc(:,:,dir))
    end if

    ! ... right boundary
    grid%r_overlap_lc(:,:,dir) = 0
    do m = 1, grid%vds(dir)
      do i = 1, grid%r_ctr_lc(m,dir)
        my_overlap = bndry_width - (grid%r_bndry_lc(i,m,dir) - grid%is(dir) + 1)
        if (my_overlap > 0) then
          grid%r_overlap_lc(i,m,dir) = my_overlap
        end if
      end do
      if ((bndry_depth - grid%l_extended_size(m,dir)) > maxval(grid%r_overlap_lc(:,m,dir))) then
        grid%r_overlap_lc(:,m,dir) = bndry_depth - grid%l_extended_size(m,dir)
      end if
    end do
       
    if (maxval(grid%r_overlap_lc(:,:,dir)) > 0 ) then 
      bndry_overlap(1) = maxval(grid%r_overlap_lc(:,:,dir))
    end if

    grid%max_overlap(dir) = maxval(bndry_overlap(:))

    ! ... clean up
    deallocate(iblank_aux)
    deallocate(iblank_aux_dble)

  end subroutine Collect_Local_Edge_Information

  ! ...
  ! ... resort to lower-order stencils if the default stencil cannot fit
  ! ...
  subroutine Fix_Stencils_Cannot_Fit(input,opID,left_bound,right_bound,B,Ng,global_iblank,r_counter,r_boundary,l_counter,l_boundary,smallStencil_counter,smallStencils,is,ie)

    USE ModGlobal
    USE ModDataStruct
    USE ModIO
    USE ModMPI
    USE ModMatrixVectorOps

    implicit none

    ! ... function call variables
    type(t_mixt_input), pointer :: input
    type(t_matrix_sparse) :: B
    integer, pointer :: l_boundary(:), global_iblank(:), r_boundary(:), smallStencils(:,:)
    integer :: r_counter, l_counter, Ng, left_bound, right_bound, opID, smallStencil_counter, is, ie

    ! ... local variables
    integer :: i, j, num_bndry, l_bndry_pt, r_bndry_pt, k, zone_width, bndry_width, csl, cel
    integer :: ks, ke, rel_bndry_loc, local_id, bndry_start, bndry_end
    real(kind=rfreal) :: lhs_alpha

    ! ... march from each left boundary towards the right one
    loop1: do num_bndry = 1, smallStencil_counter

      ! ... current boundary point (in global coordinates)
      l_bndry_pt = l_boundary(smallStencils(num_bndry,1))
      r_bndry_pt = r_boundary(smallStencils(num_bndry,2))
      zone_width = r_bndry_pt - l_bndry_pt + 1

      ! ... check to see if we need to fix, if not, cycle
      if (zone_width >= input%max_operator_width(opID)) CYCLE loop1
      if (zone_width <= 0) CYCLE loop1

      ! ... width of this region
      ! print *, 'Fixing narrow stencil with l_bndry_pt, r_bndry_pt = ', l_bndry_pt, r_bndry_pt

      ! ... by default we now have to resort to a locally-reduced operator
      ! ... switch on opID
      Select Case(opID)

      ! ... first or second derivative
      Case (1:2)

        ! ... if we don't meet the minimum, turn everything off
        if (zone_width == 1) then

          bndry_start = l_bndry_pt ! global coordinates
          bndry_end   = l_bndry_pt ! global coordinates
          ks = MAX(bndry_start,is) ! global coordinates
          ke = MIN(bndry_end,ie)   ! global coordinates
          do k = ks, ke
            ! ... location relative to boundary
            rel_bndry_loc = k - l_bndry_pt + 1 ! global coordinates

            ! ... location relative to this processor's left boundary
            local_id = k - left_bound + 1 ! local coordinates

            ! ... zero out existing stencil for this point
            csl = B%row_ptr(1,local_id)
            cel = B%row_ptr(1,local_id+1)-1
            B%val(1,csl:cel)     = 0.0_rfreal; 
            B%col_ind(1,csl:cel) = local_id
            B%nbc_row(1) = B%nbc_row(1) + 1
            B%bc_row(1,B%nbc_row(1)) = local_id
          end do

        else

          ! ... first set all points to interior scheme
          bndry_start = l_bndry_pt ! global coordinates
          bndry_end   = r_bndry_pt ! global coordinates
          ks = MAX(bndry_start,is) ! global coordinates
          ke = MIN(bndry_end,ie)  ! global coordinates
          do k = ks, ke

            ! ... skip if k == 1 or k == N
            if (k == 1 .OR. k == Ng) CYCLE

            ! ... location relative to boundary
            rel_bndry_loc = k - l_bndry_pt + 1 ! global coordinates

            ! ... location relative to this processor's left boundary
            local_id = k - left_bound + 1 ! local coordinates

            ! ... zero out existing stencil for this point
            csl = B%row_ptr(1,local_id)
            cel = B%row_ptr(1,local_id+1)-1
            B%val(1,csl:cel)     = 0.0_rfreal; 
            B%col_ind(1,csl:cel) = local_id

            ! ... interior scheme
            if (opID == 1) then
              B%val(1,csl+0)     = -0.5_WP
              B%col_ind(1,csl+0) = k - 1 - left_bound + 1
              B%val(1,csl+1)     =  0.5_WP 
              B%col_ind(1,csl+1) = k + 1 - left_bound + 1
            else if (opID == 2) then
              B%val(1,csl+0)     =  1.0_WP
              B%col_ind(1,csl+0) = k - 1 - left_bound + 1
              B%val(1,csl+1)     = -2.0_WP 
              B%col_ind(1,csl+1) = k + 0 - left_bound + 1
              B%val(1,csl+2)     =  1.0_WP 
              B%col_ind(1,csl+2) = k + 1 - left_bound + 1
            end if
            B%nbc_row(1) = B%nbc_row(1) + 1
            B%bc_row(1,B%nbc_row(1)) = local_id

          end do

          ! ... left boundary
          bndry_start = l_bndry_pt
          bndry_end   = l_bndry_pt
          ks = MAX(bndry_start,is)
          ke = MIN(bndry_end,ie)
          do k = ks, ke

            ! ... location relative to boundary
            rel_bndry_loc = k - bndry_start + 1

            ! ... location relative to this processor's left boundary
            local_id = k - left_bound + 1

            ! ... zero out existing stencil for this point
            csl = B%row_ptr(1,local_id)
            cel = B%row_ptr(1,local_id+1)-1
            B%val(1,csl:cel)     = 0.0_rfreal; 
            B%col_ind(1,csl:cel) = local_id

            if (opID == 1) then
              B%val(1,csl+0)       = -1.0_WP
              B%col_ind(1,csl+0)   = k + 0 - left_bound + 1
              B%val(1,csl+1)       =  1.0_WP
              B%col_ind(1,csl+1)   = k + 1 - left_bound + 1
            else if (opID == 2) then
              B%val(1,csl+0)       =  1.0_WP
              B%col_ind(1,csl+0)   = k + 0 - left_bound + 1
              B%val(1,csl+1)       = -2.0_WP
              B%col_ind(1,csl+1)   = k + 1 - left_bound + 1
              B%val(1,csl+2)       =  1.0_WP
              B%col_ind(1,csl+2)   = k + 2 - left_bound + 1
            end if
            B%nbc_row(1) = B%nbc_row(1) + 1
            B%bc_row(1,B%nbc_row(1)) = local_id

          end do

          ! ... right boundary
          bndry_end   = r_bndry_pt
          bndry_start = r_bndry_pt
          ks = MAX(bndry_start,is)
          ke = MIN(bndry_end,ie)
          do k = ks, ke

            ! ... location relative to boundary
            rel_bndry_loc = bndry_end - k + 1

            ! ... location relative to this processor's left boundary
            local_id = k - left_bound + 1

            ! ... zero out existing stencil for this point
            csl = B%row_ptr(1,local_id)
            cel = B%row_ptr(1,local_id+1)-1
            B%val(1,csl:cel)     = 0.0_rfreal; 
            B%col_ind(1,csl:cel) = local_id

            if (opID == 1) then
              B%val(1,csl+0)       = -1.0_WP
              B%col_ind(1,csl+0)   = k - 1 - left_bound + 1
              B%val(1,csl+1)       =  1.0_WP
              B%col_ind(1,csl+1)   = k - left_bound + 1
            else if (opID == 2) then
              B%val(1,csl+0)       =  1.0_WP
              B%col_ind(1,csl+0)   = k - 2 - left_bound + 1
              B%val(1,csl+1)       = -2.0_WP
              B%col_ind(1,csl+1)   = k - 1 - left_bound + 1
              B%val(1,csl+2)       =  1.0_WP
              B%col_ind(1,csl+2)   = k - 0 - left_bound + 1
            end if
            B%nbc_row(1) = B%nbc_row(1) + 1
            B%bc_row(1,B%nbc_row(1)) = local_id

          end do

        end if

      ! ... SAT artificial dissipation
      ! ... by default turn it off
      Case (10:11)

        bndry_start = l_bndry_pt ! global coordinates
        bndry_end   = r_bndry_pt ! global coordinates
        ks = MAX(bndry_start,is) ! global coordinates
        ke = MIN(bndry_end,ie)  ! global coordinates
        do k = ks, ke
          ! ... location relative to boundary
          rel_bndry_loc = k - l_bndry_pt + 1 ! global coordinates

          ! ... location relative to this processor's left boundary
          local_id = k - left_bound + 1 ! local coordinates

          ! ... zero out existing stencil for this point
          csl = B%row_ptr(1,local_id)
          cel = B%row_ptr(1,local_id+1)-1
          B%val(1,csl:cel)     = 0.0_rfreal; 
          B%col_ind(1,csl:cel) = local_id
          B%nbc_row(1) = B%nbc_row(1) + 1
          B%bc_row(1,B%nbc_row(1)) = local_id
        end do

      ! ... filters
      ! ... by default make them 3-point
      Case (20:31)

          ! ... first set all points to interior scheme
          bndry_start = l_bndry_pt ! global coordinates
          bndry_end   = r_bndry_pt ! global coordinates
          ks = MAX(bndry_start,is) ! global coordinates
          ke = MIN(bndry_end,ie)  ! global coordinates
          do k = ks, ke

            ! ... skip if k == 1 or k == N
            if (k == 1 .OR. k == Ng) CYCLE

            ! ... location relative to boundary
            rel_bndry_loc = k - l_bndry_pt + 1 ! global coordinates

            ! ... location relative to this processor's left boundary
            local_id = k - left_bound + 1 ! local coordinates

            ! ... zero out existing stencil for this point
            csl = B%row_ptr(1,local_id)
            cel = B%row_ptr(1,local_id+1)-1
            B%val(1,csl:cel)     = 0.0_rfreal; 
            B%col_ind(1,csl:cel) = local_id

            ! ... turn off filter
            B%val(1,csl)     = 1.0_rfreal
            B%col_ind(1,csl) = k - left_bound + 1

            ! ... interior scheme
            if (opID == 20) then
              lhs_alpha = input%bndry_filter_alphaf
              B%val(1,csl+0)     = (1.0_rfreal + 2.0_rfreal * lhs_alpha) / 4.0_rfreal
              B%col_ind(1,csl+0) = k - 1 - left_bound + 1
              B%val(1,csl+1)     = 2.0_8 * (1.0_rfreal + 2.0_rfreal * lhs_alpha) / 4.0_rfreal
              B%col_ind(1,csl+1) = k + 0 - left_bound + 1
              B%val(1,csl+2)     = (1.0_rfreal + 2.0_rfreal * lhs_alpha) / 4.0_rfreal
              B%col_ind(1,csl+2) = k + 1 - left_bound + 1
            end if
            B%nbc_row(1) = B%nbc_row(1) + 1
            B%bc_row(1,B%nbc_row(1)) = local_id

          end do

          ! ... left boundary
          bndry_start = l_bndry_pt
          bndry_end   = l_bndry_pt
          ks = MAX(bndry_start,is)
          ke = MIN(bndry_end,ie)
          do k = ks, ke

            ! ... location relative to boundary
            rel_bndry_loc = k - bndry_start + 1

            ! ... location relative to this processor's left boundary
            local_id = k - left_bound + 1

            ! ... zero out existing stencil for this point
            csl = B%row_ptr(1,local_id)
            cel = B%row_ptr(1,local_id+1)-1
            B%val(1,csl:cel)     = 0.0_rfreal; 
            B%col_ind(1,csl:cel) = local_id

            if (opID == 20) then
              B%val(1,csl+0)     = 1.0_WP
              B%col_ind(1,csl+0) = k - left_bound + 1
            end if
            B%nbc_row(1) = B%nbc_row(1) + 1
            B%bc_row(1,B%nbc_row(1)) = local_id

          end do

          ! ... right boundary
          bndry_end   = r_bndry_pt
          bndry_start = r_bndry_pt
          ks = MAX(bndry_start,is)
          ke = MIN(bndry_end,ie)

          do k = ks, ke

            ! ... location relative to boundary
            rel_bndry_loc = bndry_end - k + 1

            ! ... location relative to this processor's left boundary
            local_id = k - left_bound + 1

            ! ... zero out existing stencil for this point
            csl = B%row_ptr(1,local_id)
            cel = B%row_ptr(1,local_id+1)-1
            B%val(1,csl:cel)     = 0.0_rfreal; 
            B%col_ind(1,csl:cel) = local_id

            if (opID == 20) then
              B%val(1,csl)       = 1.0_WP
              B%col_ind(1,csl)   = k - left_bound + 1
            end if
            B%nbc_row(1) = B%nbc_row(1) + 1
            B%bc_row(1,B%nbc_row(1)) = local_id

          end do

       End Select

    end do loop1

  end subroutine Fix_Stencils_Cannot_Fit

  Subroutine fill_pade_implicit_operator_values(myrank, input, grid, opID, dir, stencil_size, values)

    use ModGlobal
    use ModDataStruct
    use ModMPI

    Implicit None

    ! ... global variables
    integer :: myrank
    type(t_grid), pointer :: grid
    type(t_mixt_input), pointer :: input
    integer :: opID, stencil_size
    real(rfreal), pointer :: values(:)

    ! ... local variables
    integer :: i, j, k, dir, N, vds, index, l1

    ! ... initialize arrays
    Do i = 1, grid%nCells*stencil_size
      values(i)  = 0.0_rfreal
    End Do

    ! ... switch on size of operator stencil
    if (stencil_size == 3) then

      ! ... loop over the directions
      N   = grid%ie(dir)-grid%is(dir)+1

      ! ... number of points in plane normal to this direction
      vds = grid%vds(dir)

      if (dir == 1) then

        ! ... loop over all the points
        do j = 1, N
          do k = 1, vds

            ! ... index of central point
            l1 = grid%vec_ind(k,j,dir)

            ! ... index & value of stencil point (i-1,j,k)
            index = (l1-1)*stencil_size + input%HYPREStencilIndex(opID, -1, 0, 0) + 1
            values(index) = values(index) + grid%penta(opID,dir)%b(k,j)

            ! ... index & value of stencil point (i,j,k)
            index = (l1-1)*stencil_size + input%HYPREStencilIndex(opID, 0, 0, 0) + 1
            values(index) = values(index) + grid%penta(opID,dir)%c(k,j)

            ! ... index & value of stencil point (i+1,j,k)
            index = (l1-1)*stencil_size + input%HYPREStencilIndex(opID, +1, 0, 0) + 1
            values(index) = values(index) + grid%penta(opID,dir)%d(k,j)

          end do
        end do

      else if (dir == 2) then

        ! ... loop over all the points
        do j = 1, N
          do k = 1, vds

            ! ... index of central point
            l1 = grid%vec_ind(k,j,dir)

            ! ... index & value of stencil point (i,j-1,k)
            index = (l1-1)*stencil_size + input%HYPREStencilIndex(opID, -1, 0, 0) + 1
            values(index) = values(index) + grid%penta(opID,dir)%b(k,j)

            ! ... index & value of stencil point (i,j,k)
            index = (l1-1)*stencil_size + input%HYPREStencilIndex(opID, 0, 0, 0) + 1
            values(index) = values(index) + grid%penta(opID,dir)%c(k,j)

            ! ... index & value of stencil point (i,j+1,k)
            index = (l1-1)*stencil_size + input%HYPREStencilIndex(opID, +1, 0, 0) + 1
            values(index) = values(index) + grid%penta(opID,dir)%d(k,j)

          end do
        end do

      else if (dir == 3) then

        ! ... loop over all the points
        do j = 1, N
          do k = 1, vds

            ! ... index of central point
            l1 = grid%vec_ind(k,j,dir)

            ! ... index & value of stencil point (i,j,k-1)
            index = (l1-1)*stencil_size + input%HYPREStencilIndex(opID, -1, 0, 0) + 1
            values(index) = values(index) + grid%penta(opID,dir)%b(k,j)

            ! ... index & value of stencil point (i,j,k)
            index = (l1-1)*stencil_size + input%HYPREStencilIndex(opID, 0, 0, 0) + 1
            values(index) = values(index) + grid%penta(opID,dir)%c(k,j)

            ! ... index & value of stencil point (i,j,k+1)
            index = (l1-1)*stencil_size + input%HYPREStencilIndex(opID, +1, 0, 0) + 1
            values(index) = values(index) + grid%penta(opID,dir)%d(k,j)

          end do
        end do

      end if

    else if (stencil_size == 5) then

      ! ... number of points along this direction
      N   = grid%ie(dir)-grid%is(dir)+1

      ! ... number of points in plane normal to this direction
      vds = grid%vds(dir)

      if (dir == 1) then

        ! ... loop over all the points
        do j = 1, N
          do k = 1, vds

            ! ... index of central point
            l1 = grid%vec_ind(k,j,dir)

            ! ... index & value of stencil point (i-2,j,k)
            index = (l1-1)*stencil_size + input%HYPREStencilIndex(opID, -2, 0, 0) + 1
            values(index) = values(index) + grid%penta(opID,dir)%a(k,j)

            ! ... index & value of stencil point (i-1,j,k)
            index = (l1-1)*stencil_size + input%HYPREStencilIndex(opID, -1, 0, 0) + 1
            values(index) = values(index) + grid%penta(opID,dir)%b(k,j)

            ! ... index & value of stencil point (i,j,k)
            index = (l1-1)*stencil_size + input%HYPREStencilIndex(opID, 0, 0, 0) + 1
            values(index) = values(index) + grid%penta(opID,dir)%c(k,j)

            ! ... index & value of stencil point (i+1,j,k)
            index = (l1-1)*stencil_size + input%HYPREStencilIndex(opID, +1, 0, 0) + 1
            values(index) = values(index) + grid%penta(opID,dir)%d(k,j)

            ! ... index & value of stencil point (i+2,j,k)
            index = (l1-1)*stencil_size + input%HYPREStencilIndex(opID, +2, 0, 0) + 1
            values(index) = values(index) + grid%penta(opID,dir)%e(k,j)

          end do
        end do

      else if (dir == 2) then

        ! ... loop over all the points
        do j = 1, N
          do k = 1, vds

            ! ... index of central point
            l1 = grid%vec_ind(k,j,dir)

            ! ... index & value of stencil point (i,j-2,k)
            index = (l1-1)*stencil_size + input%HYPREStencilIndex(opID, -2, 0, 0) + 1
            values(index) = values(index) + grid%penta(opID,dir)%a(k,j)

            ! ... index & value of stencil point (i,j-1,k)
            index = (l1-1)*stencil_size + input%HYPREStencilIndex(opID, -1, 0, 0) + 1
            values(index) = values(index) + grid%penta(opID,dir)%b(k,j)

            ! ... index & value of stencil point (i,j,k)
            index = (l1-1)*stencil_size + input%HYPREStencilIndex(opID, 0, 0, 0) + 1
            values(index) = values(index) + grid%penta(opID,dir)%c(k,j)

            ! ... index & value of stencil point (i,j+1,k)
            index = (l1-1)*stencil_size + input%HYPREStencilIndex(opID, +1, 0, 0) + 1
            values(index) = values(index) + grid%penta(opID,dir)%d(k,j)

            ! ... index & value of stencil point (i,j+2,k)
            index = (l1-1)*stencil_size + input%HYPREStencilIndex(opID, +2, 0, 0) + 1
            values(index) = values(index) + grid%penta(opID,dir)%e(k,j)

          end do
        end do

      else if (dir == 3) then

        ! ... loop over all the points
        do j = 1, N
          do k = 1, vds

            ! ... index of central point
            l1 = grid%vec_ind(k,j,dir)

            ! ... index & value of stencil point (i,j,k-2)
            index = (l1-1)*stencil_size + input%HYPREStencilIndex(opID, -2, 0, 0) + 1
            values(index) = values(index) + grid%penta(opID,dir)%a(k,j)

            ! ... index & value of stencil point (i,j,k-1)
            index = (l1-1)*stencil_size + input%HYPREStencilIndex(opID, -1, 0, 0) + 1
            values(index) = values(index) + grid%penta(opID,dir)%b(k,j)

            ! ... index & value of stencil point (i,j,k)
            index = (l1-1)*stencil_size + input%HYPREStencilIndex(opID,  0, 0, 0) + 1
            values(index) = values(index) + grid%penta(opID,dir)%c(k,j)

            ! ... index & value of stencil point (i,j,k+1)
            index = (l1-1)*stencil_size + input%HYPREStencilIndex(opID, +1, 0, 0) + 1
            values(index) = values(index) + grid%penta(opID,dir)%d(k,j)

            ! ... index & value of stencil point (i,j,k+2)
            index = (l1-1)*stencil_size + input%HYPREStencilIndex(opID, +2, 0, 0) + 1
            values(index) = values(index) + grid%penta(opID,dir)%e(k,j)

          end do
        end do

      end if

    else

      call graceful_exit(myrank, 'Unknown size of input%HYPREStencilIndex in fill_pade_implicit_operator_values')

    end if

    ! ... we don't need grid%penta any more
    if (grid%operator_implicit_solver(opID) /= DIRECT_PENTA) then
      deallocate(grid%penta(opID,dir)%a)
      deallocate(grid%penta(opID,dir)%b)
      deallocate(grid%penta(opID,dir)%c)
      deallocate(grid%penta(opID,dir)%d)
      deallocate(grid%penta(opID,dir)%e)
    end if

  end Subroutine fill_pade_implicit_operator_values

  Subroutine HYPRE_PADE_OPERATOR_SETUP(region, ng)

    USE ModGlobal
    USE ModDataStruct
    USE ModMPI

    ! ... global variables
    Type(t_region), Pointer :: Region
    Integer :: ng

#ifdef USE_HYPRE
    ! ... local variables
    Type(t_mixt_input), Pointer :: input
    Type(t_grid), Pointer :: grid
    Integer :: min_stencil_start, max_stencil_end, stencil_size, opID, dir, per(MAX_ND), j
    Real(rfreal), pointer :: values(:)
    Real(rfreal) :: fac, timer

    ! ... start timer
    timer = MPI_Wtime()

    ! ... simplicity
    grid  => region%grid(ng)
    input => grid%input

    ! ... set periodicity
    per(1:MAX_ND) = 0;
    do j = 1, grid%ND
      if (grid%periodic(j) > 0) per(j) = grid%GlobalSize(j)
    end do

    ! ... build HYPRE objects
    Call allocate_pade_hypre_objects(region%nGrids, MAX_OPERATORS, input%ND)

    Do opID = 1, MAX_OPERATORS
      if (grid%operator_type(opID) == IMPLICIT) then
        min_stencil_start = input%lhs_interior_stencil_start(opID)
        max_stencil_end   = input%lhs_interior_stencil_end(opID)
        stencil_size      = max_stencil_end - min_stencil_start + 1
        allocate(values(grid%nCells*stencil_size))
        Do dir = 1, grid%ND
          Call init_pade_hypre_objects(region%nGridsGlobal, grid%iGridGlobal, ng, region%nGrids, &
               grid%myrank_inComm, grid%numproc_inComm, grid%comm, grid%ND, dir, opID, &
               grid%is, grid%ie, stencil_size, per)
          Call fill_pade_implicit_operator_values(input, grid, opID, dir, stencil_size, values)
          Call fill_hypre_pade_implicit_matrix(region%nGridsGlobal, grid%iGridGlobal, ng, region%nGrids, &
               grid%myrank_inComm, grid%numproc_inComm, grid%comm, grid%ND, dir, opID, &
               grid%is, grid%ie, stencil_size, values, 1, 1, per)
          Call assemble_hypre_pade_implicit_matrix(region%nGridsGlobal, grid%iGridGlobal, ng, region%nGrids, &
               grid%myrank_inComm, grid%numproc_inComm, grid%comm, opID, grid%ND, dir, per)
          Call assemble_hypre_pade_vectors(region%nGridsGlobal, grid%iGridGlobal, ng, region%nGrids, &
               grid%myrank_inComm, grid%numproc_inComm, grid%comm, opID, grid%ND, dir)
!!$          Call hypre_setup_solve_pade_implicit(region%nGridsGlobal, grid%iGridGlobal, ng, region%nGrids, &
!!$               grid%myrank_inComm, grid%numproc_inComm, grid%comm, &
!!$               grid%ND, dir, opID, grid%is, grid%ie)
          fac = 1.0_rfreal
          if (opID == input%iFilter) fac = input%filter_alphaf
          if (opID == input%iLHS_Filter) fac = input%LHS_filter_alphaf
          Call hypre_setup_pade_implicit(region%nGridsGlobal, grid%iGridGlobal, ng, region%nGrids, &
                                         grid%myrank_inComm, grid%numproc_inComm, grid%comm, &
                                         grid%ND, dir, opID, fac)
        End Do
        deallocate(values)
      End If
    End Do

   ! ... stop timer
   region%mpi_timings(ng)%operator_setup(:,:) = region%mpi_timings(ng)%operator_setup(:,:) + (MPI_WTime() - timer)
#endif

   Return

  end Subroutine HYPRE_PADE_OPERATOR_SETUP

  ! ... locally collect edge information for each processor (box shape), WZhang 11/2014

  subroutine Collect_Local_Edge_Information_Box(region, gridID, input, dir, grid, periodic_flag)

    USE ModGlobal
    USE ModDataStruct
    USE ModMPI
    USE ModMatrixVectorOps
    USE ModDeriv

    implicit none

    type(t_region), pointer :: region
    type (t_mixt_input), pointer :: input
    type (t_grid), pointer :: grid
    integer :: dir, periodic_flag, gridID

    ! ... local variables
    integer ND, Nglobal(MAX_ND), Nunique(MAX_ND), nGhost, Nbox(MAX_ND), Nmax, Nmax_gl
    integer :: bndry_offset(2), bndry_width, bndry_depth,  bndry_extended, bndry_overlap(2)
    integer :: scan_extent, iblank_left, iblank_right, my_overlap, ierr
    integer, pointer :: recvcounts(:), recvdisp(:), sendbuf(:), recvbuf(:)
    integer :: nproc, i, j, k, l0, l1, l_left, l_right
    integer :: l_ctr_gl_b, r_ctr_gl_b, l_bndry_gl_b(2*MAX_BNDRY), r_bndry_gl_b(2*MAX_BNDRY)
    integer :: l_ctr_gl_rd_b, r_ctr_gl_rd_b, l_bndry_gl_rd_b(MAX_BNDRY), r_bndry_gl_rd_b(MAX_BNDRY), myrank_inpencil, nproc_inpencil
    integer :: ilc, jlc, klc, igl, jgl, kgl

    ND       = grid%ND
    ! ... global grid sizes
    Nglobal(:) = 1
    Nglobal(1:grid%ND) = grid%GlobalSize(1:grid%ND)
    ! ... unique grid sizes
    Nunique(:) = 1
    Nunique(1:grid%ND) = grid%ie_unique(1:grid%ND) - grid%is_unique(1:grid%ND) + 1
    ! ... box grid sizes
    Nbox(:)    = 1
    Nbox(1:grid%ND)    = grid%ie(1:grid%ND) - grid%is(1:grid%ND) + 1
    ! ... number of ghost points
    nGhost = input%nOverLap
    ! ... max value of the dimensions
    Nmax = maxval(Nunique(:)) + 2*nGhost
    Nmax_gl = maxval(Nglobal(:))

    ! ... depth and width of the boundary
    bndry_depth      = maxval(input%operator_bndry_depth(:))
    bndry_width      = maxval(input%operator_bndry_width(:))

    ! ... number of processors
    Call MPI_COMM_SIZE(grid%pencilcomm(dir), nproc_inpencil, ierr)

    ! ... allocate memory
    if (.not.allocated(grid%l_ctr_lc_b) .eqv. .true.) then
      allocate(grid%l_ctr_lc_b(Nmax, Nmax, MAX_ND))
    end if
    if (.not.allocated(grid%r_ctr_lc_b) .eqv. .true.) then
      allocate(grid%r_ctr_lc_b(Nmax, Nmax, MAX_ND))
    end if
    if (.not.allocated(grid%l_bndry_lc_b) .eqv. .true.) then
      allocate(grid%l_bndry_lc_b(MAX_BNDRY, Nmax, Nmax, MAX_ND))
    end if
    if (.not.allocated(grid%r_bndry_lc_b) .eqv. .true.) then
      allocate(grid%r_bndry_lc_b(MAX_BNDRY, Nmax, Nmax, MAX_ND))
    end if
    if (.not.allocated(grid%l_overlap_lc_b) .eqv. .true.) then
      allocate(grid%l_overlap_lc_b(MAX_BNDRY, Nmax, Nmax, MAX_ND))
    end if
    if (.not.allocated(grid%r_overlap_lc_b) .eqv. .true.) then
      allocate(grid%r_overlap_lc_b(MAX_BNDRY, Nmax, Nmax, MAX_ND))
    end if
    if (.not.allocated(grid%l_extended_b) .eqv. .true.) then
      allocate(grid%l_extended_b(Nmax, Nmax, MAX_ND))
    end if
    if (.not.allocated(grid%r_extended_b) .eqv. .true.) then
      allocate(grid%r_extended_b(Nmax, Nmax, MAX_ND))
    end if
    if (.not.allocated(grid%l_extended_size_b) .eqv. .true.) then
      allocate(grid%l_extended_size_b(Nmax, Nmax, MAX_ND))
    end if
    if (.not.allocated(grid%r_extended_size_b) .eqv. .true.) then
      allocate(grid%r_extended_size_b(Nmax, Nmax, MAX_ND))
    end if

    Select Case (dir)
      Case(1)
        do klc =1, Nbox(3)
          do jlc =1, Nbox(2)
            ! ... Step 1 count the edges in each processor and store the global index
            grid%l_ctr_lc_b(jlc,klc,1) = 0
            grid%r_ctr_lc_b(jlc,klc,1) = 0
            ! ... check whether we have left-most and right-most boundary
            bndry_offset(1) = 0
            bndry_offset(2) = 0
            ! ... left
            l0 = (klc-1)*Nbox(2)*Nbox(1) + (jlc-1)*Nbox(1) + 1 + grid%nGhostRHS(1,1)
            if (grid%is_unique(1) == 1) then
              if (periodic_flag == FALSE .and. grid%iblank(l0) /= 0) then
                grid%l_ctr_lc_b(jlc,klc,1) = grid%l_ctr_lc_b(jlc,klc,1) + 1
                grid%l_bndry_lc_b(grid%l_ctr_lc_b(jlc,klc,1),jlc,klc,1) = 1
                bndry_offset(1) = 1
              else
              bndry_offset(1) = 1
              end if
            end if
            ! ... right
            l0 = (klc-1)*Nbox(2)*Nbox(1) + (jlc-1)*Nbox(1) + Nunique(1) + grid%nGhostRHS(1,1)
            if (grid%ie_unique(1) == Nglobal(1)) then
              if (periodic_flag == FALSE .and. grid%iblank(l0) /= 0) then
                grid%r_ctr_lc_b(jlc,klc,1) = grid%r_ctr_lc_b(jlc,klc,1) + 1
                grid%r_bndry_lc_b(grid%r_ctr_lc_b(jlc,klc,1),jlc,klc,1) = Nglobal(1)
                bndry_offset(2) = 1
              else
                bndry_offset(2) = 1
              end if
            end if
            ! ... search for all interior boundaries
            ! ... left boundary at point l_right
            scan_extent = Nunique(1) - bndry_offset(1) - bndry_offset(2)
            do i = 1, scan_extent
              l_left  = (klc-1)*Nbox(2)*Nbox(1) + (jlc-1)*Nbox(1) + i + grid%nGhostRHS(1,1) + bndry_offset(1) - 1
              l_right = (klc-1)*Nbox(2)*Nbox(1) + (jlc-1)*Nbox(1) + i + grid%nGhostRHS(1,1) + bndry_offset(1)
              iblank_left  = grid%iblank(l_left)
              iblank_right = grid%iblank(l_right)

              if ((iblank_left == 0) .AND. (iblank_right /= 0)) then
                ! ... igl is the global index
                igl = grid%is_unique(1) + i + bndry_offset(1) - 1     
                grid%l_ctr_lc_b(jlc,klc,1) = grid%l_ctr_lc_b(jlc,klc,1) + 1
                grid%l_bndry_lc_b(grid%l_ctr_lc_b(jlc,klc,1),jlc,klc,1) = igl
              end if
            end do

            ! ... right boundary at point l_left
            scan_extent = Nunique(1)-bndry_offset(1)-bndry_offset(2)
            do i = 1, scan_extent
              l_left  = (klc-1)*Nbox(2)*Nbox(1) + (jlc-1)*Nbox(1) + i + grid%nGhostRHS(1,1) + bndry_offset(1) 
              l_right = (klc-1)*Nbox(2)*Nbox(1) + (jlc-1)*Nbox(1) + i + grid%nGhostRHS(1,1) + bndry_offset(1) + 1
              iblank_left  = grid%iblank(l_left)
              iblank_right = grid%iblank(l_right)

              if ((iblank_left /= 0) .AND. (iblank_right == 0)) then
                ! ... igl is the global index
                igl = grid%is_unique(1) + i + bndry_offset(1) - 1     
                grid%r_ctr_lc_b(jlc,klc,1) = grid%r_ctr_lc_b(jlc,klc,1) + 1
                grid%r_bndry_lc_b(grid%r_ctr_lc_b(jlc,klc,1),jlc,klc,1) = igl
              end if
            end do

            ! ... Step 2 gather information from each processor to have a global view
            Call MPI_BARRIER(grid%pencilcomm(1), IERR)
            allocate(recvcounts(nproc_inpencil))
            allocate(recvdisp(nproc_inpencil))

            ! ... left boundary
            ! ... use MPI_Allgather to determine the size of the data to be sent
            Call MPI_AllGATHER(grid%l_ctr_lc_b(jlc,klc,1), 1, MPI_INTEGER, recvcounts, 1, MPI_INTEGER, grid%pencilcomm(1), ierr)
            recvdisp(1) = 0
            do i = 2, nproc_inpencil
              recvdisp(i) = recvdisp(i-1) + recvcounts(i-1)
            end do
            l_ctr_gl_b = SUM(recvcounts(:))

            ! ... call allgatherv

            allocate(sendbuf(grid%l_ctr_lc_b(jlc,klc,1)))
            allocate(recvbuf(l_ctr_gl_b))

            do i = 1, grid%l_ctr_lc_b(jlc,klc,1)
              sendbuf(i) = grid%l_bndry_lc_b(i,jlc,klc,1)
            end do

            Call MPI_AllGATHERV(sendbuf, grid%l_ctr_lc_b(jlc,klc,1), MPI_INTEGER, recvbuf, recvcounts, recvdisp, MPI_INTEGER, grid%pencilcomm(1), ierr)

            do i = 1, l_ctr_gl_b
              l_bndry_gl_b(i) = recvbuf(i)
            end do

            deallocate(sendbuf)
            deallocate(recvbuf)

            ! ... right boundary
            ! ... use MPI_Allgather to determine the size of the data to be sent
            Call MPI_AllGATHER(grid%r_ctr_lc_b(jlc,klc,dir), 1, MPI_INTEGER, recvcounts, 1, MPI_INTEGER, grid%pencilcomm(1), ierr)
            recvdisp(1) = 0
            do i = 2, nproc_inpencil
              recvdisp(i) = recvdisp(i-1) + recvcounts(i-1)
            end do
            r_ctr_gl_b = SUM(recvcounts(:))

            ! ... call allgatherv

            allocate(sendbuf(grid%r_ctr_lc_b(jlc,klc,1)))
            allocate(recvbuf(r_ctr_gl_b))

            do i = 1, grid%r_ctr_lc_b(jlc,klc,1)
              sendbuf(i) = grid%r_bndry_lc_b(i,jlc,klc,1)
            end do

            Call MPI_AllGATHERV(sendbuf, grid%r_ctr_lc_b(jlc,klc,1), MPI_INTEGER, recvbuf, recvcounts, recvdisp, MPI_INTEGER, grid%pencilcomm(1), ierr)

            do i = 1, r_ctr_gl_b
              r_bndry_gl_b(i) = recvbuf(i)
            end do

            deallocate(sendbuf)
            deallocate(recvbuf)

            deallocate(recvcounts)
            deallocate(recvdisp)

            ! ... Step 3 reduce: search for unique elements
            l_ctr_gl_rd_b = 0
            r_ctr_gl_rd_b = 0
            l_bndry_gl_rd_b(:) = 0
            r_bndry_gl_rd_b(:) = 0

            ! ... left boundary
            do i = 2, l_ctr_gl_b
              if (l_bndry_gl_b(i) /= l_bndry_gl_b(i-1)) then
                l_ctr_gl_rd_b = l_ctr_gl_rd_b + 1
                l_bndry_gl_rd_b(l_ctr_gl_rd_b) = l_bndry_gl_b(i-1)
              end if
            end do

            if (l_ctr_gl_b > 0) then  ! ... JByun 09/30/14
              l_ctr_gl_rd_b = l_ctr_gl_rd_b + 1
              l_bndry_gl_rd_b(l_ctr_gl_rd_b) = l_bndry_gl_b(l_ctr_gl_b)
            end if

            ! ... right boundary
            do i = 2, r_ctr_gl_b
              if (r_bndry_gl_b(i) /= r_bndry_gl_b(i-1)) then
                r_ctr_gl_rd_b = r_ctr_gl_rd_b + 1
                r_bndry_gl_rd_b(r_ctr_gl_rd_b) = r_bndry_gl_b(i-1)
              end if
            end do

            if (r_ctr_gl_b > 0) then ! ... JByun 09/30/14
              r_ctr_gl_rd_b = r_ctr_gl_rd_b + 1
              r_bndry_gl_rd_b(r_ctr_gl_rd_b) = r_bndry_gl_b(r_ctr_gl_b)
            end if 

            ! ... Step 4 must be aware of boundaries that may lie on another processor but whose boundary stencil extends to us
            grid%l_extended_b(jlc,klc,1) = 0
            grid%l_extended_size_b(jlc,klc,1) = 0
            grid%r_extended_b(jlc,klc,1) = 0
            grid%r_extended_size_b(jlc,klc,1) = 0

            ! ... left boundary
            do i = 1, l_ctr_gl_rd_b
              if (l_bndry_gl_rd_b(i) < grid%is_unique(1)) then
                bndry_extended = l_bndry_gl_rd_b(i)+bndry_depth-1

                if ((grid%is_unique(1) <= bndry_extended) .and. (bndry_extended <= grid%ie_unique(1))) then
                  grid%l_extended_b(jlc,klc,1) = 1
                  grid%l_extended_size_b(jlc,klc,1) = bndry_extended - grid%is_unique(1) + 1
                end if

              end if
            end do

            ! ... right boundary
            do i = 1, r_ctr_gl_rd_b
              if (r_bndry_gl_rd_b(i) > grid%ie_unique(1)) then
                bndry_extended = r_bndry_gl_rd_b(i)-(bndry_depth-1)

                if ((grid%is_unique(1) <= bndry_extended) .and. (bndry_extended <= grid%ie_unique(1))) then
                  grid%r_extended_b(jlc,klc,1) = 1
                  grid%r_extended_size_b(jlc,klc,1) = grid%ie_unique(1) - bndry_extended + 1
                end if

              end if
            end do

          end do
        end do

        ! ... Step 5 we need to know the maximum number of ghost points we need
        bndry_overlap(:) = 0

        ! ... left boundary
        grid%l_overlap_lc_b(:,:,:,1) = 0
        do klc =1, Nbox(3)
          do jlc =1, Nbox(2)
            do i = 1, grid%l_ctr_lc_b(jlc,klc,1)
              my_overlap = bndry_width - (grid%ie_unique(1) - grid%l_bndry_lc_b(i,jlc,klc,1) + 1)
              if (my_overlap > 0) then
                grid%l_overlap_lc_b(i,jlc,klc,1) = my_overlap
              end if
            end do
            if ((bndry_depth - grid%r_extended_size_b(jlc,klc,1)) > maxval(grid%l_overlap_lc_b(:,jlc,klc,1))) then
              grid%l_overlap_lc_b(:,jlc,klc,1) = bndry_depth - grid%r_extended_size_b(jlc,klc,1)
            end if
          end do
        end do

        if (maxval(grid%l_overlap_lc_b(:,:,:,1)) > 0) then
          bndry_overlap(2) = maxval(grid%l_overlap_lc_b(:,:,:,1))
        end if

        ! ... right boundary
        grid%r_overlap_lc_b(:,:,:,1) = 0
        do klc =1, Nbox(3)
          do jlc =1, Nbox(2)
            do i = 1, grid%r_ctr_lc_b(jlc,klc,1)
              my_overlap = bndry_width - (grid%r_bndry_lc_b(i,jlc,klc,1) - grid%is_unique(1) + 1)
              if (my_overlap > 0) then
                grid%r_overlap_lc_b(i,jlc,klc,1) = my_overlap
              end if
            end do
            if ((bndry_depth - grid%l_extended_size_b(jlc,klc,1)) > maxval(grid%r_overlap_lc_b(:,jlc,klc,1))) then
              grid%r_overlap_lc_b(:,jlc,klc,1) = bndry_depth - grid%l_extended_size_b(jlc,klc,1)
            end if
          end do
        end do
           
        if (maxval(grid%r_overlap_lc_b(:,:,:,1)) > 0 ) then 
          bndry_overlap(1) = maxval(grid%r_overlap_lc_b(:,:,:,1))
        end if

        grid%max_overlap_b(1) = maxval(bndry_overlap(:))

      Case(2)
        do klc =1, Nbox(3)
          do ilc =1, Nbox(1)
            ! ... Step 1 count the edges in each processor and store the global index
            grid%l_ctr_lc_b(ilc,klc,2) = 0
            grid%r_ctr_lc_b(ilc,klc,2) = 0
            ! ... check whether we have left-most and right-most boundary
            bndry_offset(1) = 0
            bndry_offset(2) = 0
            ! ... left
            l0 = (klc-1)*Nbox(2)*Nbox(1) + ilc
            if (grid%is_unique(2) == 1) then
              if (periodic_flag == FALSE .and. grid%iblank(l0) /= 0) then
                grid%l_ctr_lc_b(ilc,klc,2) = grid%l_ctr_lc_b(ilc,klc,2) + 1
                grid%l_bndry_lc_b(grid%l_ctr_lc_b(ilc,klc,2),ilc,klc,2) = 1
                bndry_offset(1) = 1
              else
              bndry_offset(1) = 1
              end if
            end if
            ! ... right
            l0 = (klc-1)*Nbox(2)*Nbox(1) + (grid%nGhostRHS(2,1)+Nunique(2)-1)*Nbox(1) + ilc
            if (grid%ie_unique(2) == Nglobal(2)) then
              if (periodic_flag == FALSE .and. grid%iblank(l0) /= 0) then
                grid%r_ctr_lc_b(ilc,klc,2) = grid%r_ctr_lc_b(ilc,klc,2) + 1
                grid%r_bndry_lc_b(grid%r_ctr_lc_b(ilc,klc,2),ilc,klc,2) = Nglobal(2)
                bndry_offset(2) = 1
              else
                bndry_offset(2) = 1
              end if
            end if
            ! ... search for all interior boundaries
            ! ... left boundary at point l_right
            scan_extent = Nunique(2) - bndry_offset(1) - bndry_offset(2)
            do j = 1, scan_extent
              l_left  = (klc-1)*Nbox(2)*Nbox(1) + (grid%nGhostRHS(2,1)+j-1+bndry_offset(1)-1)*Nbox(1) + ilc
              l_right = (klc-1)*Nbox(2)*Nbox(1) + (grid%nGhostRHS(2,1)+j-1+bndry_offset(1))*Nbox(1) + ilc
              iblank_left  = grid%iblank(l_left)
              iblank_right = grid%iblank(l_right)

              if ((iblank_left == 0) .AND. (iblank_right /= 0)) then
                ! ... jgl is the global index
                jgl = grid%is_unique(2) + j + bndry_offset(1) - 1     
                grid%l_ctr_lc_b(ilc,klc,2) = grid%l_ctr_lc_b(ilc,klc,2) + 1
                grid%l_bndry_lc_b(grid%l_ctr_lc_b(ilc,klc,2),ilc,klc,2) = jgl
              end if
            end do

            ! ... right boundary at point l_left
            scan_extent = Nunique(2)-bndry_offset(1)-bndry_offset(2)
            do j = 1, scan_extent
              l_left  = (klc-1)*Nbox(2)*Nbox(1) + (grid%nGhostRHS(2,1)+j-1+bndry_offset(1))*Nbox(1) + ilc 
              l_right = (klc-1)*Nbox(2)*Nbox(1) + (grid%nGhostRHS(2,1)+j-1+bndry_offset(1)+1)*Nbox(1) + ilc
              iblank_left  = grid%iblank(l_left)
              iblank_right = grid%iblank(l_right)

              if ((iblank_left /= 0) .AND. (iblank_right == 0)) then
                ! ... jgl is the global index
                jgl = grid%is_unique(2) + j + bndry_offset(1) - 1     
                grid%r_ctr_lc_b(ilc,klc,2) = grid%r_ctr_lc_b(ilc,klc,2) + 1
                grid%r_bndry_lc_b(grid%r_ctr_lc_b(ilc,klc,2),ilc,klc,2) = jgl
              end if
            end do

            ! ... Step 2 gather information from each processor to have a global view
            Call MPI_BARRIER(grid%pencilcomm(2), IERR)
            allocate(recvcounts(nproc_inpencil))
            allocate(recvdisp(nproc_inpencil))

            ! ... left boundary
            ! ... use MPI_Allgather to determine the size of the data to be sent
            Call MPI_AllGATHER(grid%l_ctr_lc_b(ilc,klc,2), 1, MPI_INTEGER, recvcounts, 1, MPI_INTEGER, grid%pencilcomm(2), ierr)
            recvdisp(1) = 0
            do i = 2, nproc_inpencil
              recvdisp(i) = recvdisp(i-1) + recvcounts(i-1)
            end do
            l_ctr_gl_b = SUM(recvcounts(:))

            ! ... call allgatherv

            allocate(sendbuf(grid%l_ctr_lc_b(ilc,klc,2)))
            allocate(recvbuf(l_ctr_gl_b))

            do i = 1, grid%l_ctr_lc_b(ilc,klc,2)
              sendbuf(i) = grid%l_bndry_lc_b(i,ilc,klc,2)
            end do

            Call MPI_AllGATHERV(sendbuf, grid%l_ctr_lc_b(ilc,klc,2), MPI_INTEGER, recvbuf, recvcounts, recvdisp, MPI_INTEGER, grid%pencilcomm(2), ierr)

            do i = 1, l_ctr_gl_b
              l_bndry_gl_b(i) = recvbuf(i)
            end do

            deallocate(sendbuf)
            deallocate(recvbuf)

            ! ... right boundary
            ! ... use MPI_Allgather to determine the size of the data to be sent
            Call MPI_AllGATHER(grid%r_ctr_lc_b(ilc,klc,2), 1, MPI_INTEGER, recvcounts, 1, MPI_INTEGER, grid%pencilcomm(2), ierr)
            recvdisp(1) = 0
            do i = 2, nproc_inpencil
              recvdisp(i) = recvdisp(i-1) + recvcounts(i-1)
            end do
            r_ctr_gl_b = SUM(recvcounts(:))

            ! ... call allgatherv

            allocate(sendbuf(grid%r_ctr_lc_b(ilc,klc,2)))
            allocate(recvbuf(r_ctr_gl_b))

            do i = 1, grid%r_ctr_lc_b(ilc,klc,2)
              sendbuf(i) = grid%r_bndry_lc_b(i,ilc,klc,2)
            end do

            Call MPI_AllGATHERV(sendbuf, grid%r_ctr_lc_b(ilc,klc,2), MPI_INTEGER, recvbuf, recvcounts, recvdisp, MPI_INTEGER, grid%pencilcomm(2), ierr)

            do i = 1, r_ctr_gl_b
              r_bndry_gl_b(i) = recvbuf(i)
            end do

            deallocate(sendbuf)
            deallocate(recvbuf)

            deallocate(recvcounts)
            deallocate(recvdisp)

            ! ... Step 3 reduce: search for unique elements
            l_ctr_gl_rd_b = 0
            r_ctr_gl_rd_b = 0
            l_bndry_gl_rd_b(:) = 0
            r_bndry_gl_rd_b(:) = 0

            ! ... left boundary
            do i = 2, l_ctr_gl_b
              if (l_bndry_gl_b(i) /= l_bndry_gl_b(i-1)) then
                l_ctr_gl_rd_b = l_ctr_gl_rd_b + 1
                l_bndry_gl_rd_b(l_ctr_gl_rd_b) = l_bndry_gl_b(i-1)
              end if
            end do

            if (l_ctr_gl_b > 0) then  ! ... JByun 09/30/14
              l_ctr_gl_rd_b = l_ctr_gl_rd_b + 1
              l_bndry_gl_rd_b(l_ctr_gl_rd_b) = l_bndry_gl_b(l_ctr_gl_b)
            end if

            ! ... right boundary
            do i = 2, r_ctr_gl_b
              if (r_bndry_gl_b(i) /= r_bndry_gl_b(i-1)) then
                r_ctr_gl_rd_b = r_ctr_gl_rd_b + 1
                r_bndry_gl_rd_b(r_ctr_gl_rd_b) = r_bndry_gl_b(i-1)
              end if
            end do

            if (r_ctr_gl_b > 0) then ! ... JByun 09/30/14
              r_ctr_gl_rd_b = r_ctr_gl_rd_b + 1
              r_bndry_gl_rd_b(r_ctr_gl_rd_b) = r_bndry_gl_b(r_ctr_gl_b)
            end if 

            ! ... Step 4 must be aware of boundaries that may lie on another processor but whose boundary stencil extends to us
            grid%l_extended_b(ilc,klc,2) = 0
            grid%l_extended_size_b(ilc,klc,2) = 0
            grid%r_extended_b(ilc,klc,2) = 0
            grid%r_extended_size_b(ilc,klc,2) = 0

            ! ... left boundary
            do i = 1, l_ctr_gl_rd_b
              if (l_bndry_gl_rd_b(i) < grid%is_unique(2)) then
                bndry_extended = l_bndry_gl_rd_b(i)+bndry_depth-1

                if ((grid%is_unique(2) <= bndry_extended) .and. (bndry_extended <= grid%ie_unique(2))) then
                  grid%l_extended_b(ilc,klc,2) = 1
                  grid%l_extended_size_b(ilc,klc,2) = bndry_extended - grid%is_unique(2) + 1
                end if

              end if
            end do

            ! ... right boundary
            do i = 1, r_ctr_gl_rd_b
              if (r_bndry_gl_rd_b(i) > grid%ie_unique(2)) then
                bndry_extended = r_bndry_gl_rd_b(i)-(bndry_depth-1)

                if ((grid%is_unique(2) <= bndry_extended) .and. (bndry_extended <= grid%ie_unique(2))) then
                  grid%r_extended_b(ilc,klc,2) = 1
                  grid%r_extended_size_b(ilc,klc,2) = grid%ie_unique(2) - bndry_extended + 1
                end if

              end if
            end do

          end do
        end do

        ! ... Step 5 we need to know the maximum number of ghost points we need
        bndry_overlap(:) = 0

        ! ... left boundary
        grid%l_overlap_lc_b(:,:,:,2) = 0
        do klc =1, Nbox(3)
          do ilc =1, Nbox(1)
            do i = 1, grid%l_ctr_lc_b(ilc,klc,2)
              my_overlap = bndry_width - (grid%ie_unique(2) - grid%l_bndry_lc_b(i,ilc,klc,2) + 1)
              if (my_overlap > 0) then
                grid%l_overlap_lc_b(i,ilc,klc,2) = my_overlap
              end if
            end do
            if ((bndry_depth - grid%r_extended_size_b(ilc,klc,2)) > maxval(grid%l_overlap_lc_b(:,ilc,klc,2))) then
              grid%l_overlap_lc_b(:,ilc,klc,2) = bndry_depth - grid%r_extended_size_b(ilc,klc,2)
            end if
          end do
        end do

        if (maxval(grid%l_overlap_lc_b(:,:,:,2)) > 0) then
          bndry_overlap(2) = maxval(grid%l_overlap_lc_b(:,:,:,2))
        end if

        ! ... right boundary
        grid%r_overlap_lc_b(:,:,:,2) = 0
        do klc =1, Nbox(3)
          do ilc =1, Nbox(1)
            do i = 1, grid%r_ctr_lc_b(ilc,klc,2)
              my_overlap = bndry_width - (grid%r_bndry_lc_b(i,ilc,klc,2) - grid%is_unique(2) + 1)
              if (my_overlap > 0) then
                grid%r_overlap_lc_b(i,ilc,klc,2) = my_overlap
              end if
            end do
            if ((bndry_depth - grid%l_extended_size_b(ilc,klc,2)) > maxval(grid%r_overlap_lc_b(:,ilc,klc,2))) then
              grid%r_overlap_lc_b(:,ilc,klc,2) = bndry_depth - grid%l_extended_size_b(ilc,klc,2)
            end if
          end do
        end do
           
        if (maxval(grid%r_overlap_lc_b(:,:,:,2)) > 0 ) then 
          bndry_overlap(1) = maxval(grid%r_overlap_lc_b(:,:,:,2))
        end if

        grid%max_overlap_b(2) = maxval(bndry_overlap(:))

      Case(3)
        do jlc =1, Nbox(2)
          do ilc =1, Nbox(1)
            ! ... Step 1 count the edges in each processor and store the global index
            grid%l_ctr_lc_b(ilc,jlc,3) = 0
            grid%r_ctr_lc_b(ilc,jlc,3) = 0
            ! ... check whether we have left-most and right-most boundary
            bndry_offset(1) = 0
            bndry_offset(2) = 0
            ! ... left
            l0 = (jlc-1)*Nbox(1) + ilc
            if (grid%is_unique(3) == 1) then
              if (periodic_flag == FALSE .and. grid%iblank(l0) /= 0) then
                grid%l_ctr_lc_b(ilc,jlc,3) = grid%l_ctr_lc_b(ilc,jlc,3) + 1
                grid%l_bndry_lc_b(grid%l_ctr_lc_b(ilc,jlc,3),ilc,jlc,3) = 1
                bndry_offset(1) = 1
              else
              bndry_offset(1) = 1
              end if
            end if
            ! ... right
            l0 = (grid%nGhostRHS(3,1)+Nunique(3)-1)*Nbox(2)*Nbox(1) + (jlc-1)*Nbox(1) + ilc
            if (grid%ie_unique(3) == Nglobal(3)) then
              if (periodic_flag == FALSE .and. grid%iblank(l0) /= 0) then
                grid%r_ctr_lc_b(ilc,jlc,3) = grid%r_ctr_lc_b(ilc,jlc,3) + 1
                grid%r_bndry_lc_b(grid%r_ctr_lc_b(ilc,jlc,3),ilc,jlc,3) = Nglobal(3)
                bndry_offset(2) = 1
              else
                bndry_offset(2) = 1
              end if
            end if
            ! ... search for all interior boundaries
            ! ... left boundary at point l_right
            scan_extent = Nunique(3) - bndry_offset(1) - bndry_offset(2)
            do k = 1, scan_extent
              l_left  = (grid%nGhostRHS(3,1)+k-1+bndry_offset(1)-1)*Nbox(2)*Nbox(1) + (jlc-1)*Nbox(1) + ilc
              l_right = (grid%nGhostRHS(3,1)+k-1+bndry_offset(1))*Nbox(2)*Nbox(1) + (jlc-1)*Nbox(1) + ilc
              iblank_left  = grid%iblank(l_left)
              iblank_right = grid%iblank(l_right)

              if ((iblank_left == 0) .AND. (iblank_right /= 0)) then
                ! ... kgl is the global index
                kgl = grid%is_unique(3) + k + bndry_offset(1) - 1     
                grid%l_ctr_lc_b(ilc,jlc,3) = grid%l_ctr_lc_b(ilc,jlc,3) + 1
                grid%l_bndry_lc_b(grid%l_ctr_lc_b(ilc,jlc,3),ilc,jlc,3) = kgl
              end if
            end do

            ! ... right boundary at point l_left
            scan_extent = Nunique(3)-bndry_offset(1)-bndry_offset(2)
            do k = 1, scan_extent
              l_left  = (grid%nGhostRHS(3,1)+k-1+bndry_offset(1))*Nbox(2)*Nbox(1) + (jlc-1)*Nbox(1) + ilc
              l_right = (grid%nGhostRHS(3,1)+k-1+bndry_offset(1)+1)*Nbox(2)*Nbox(1) + (jlc-1)*Nbox(1) + ilc
              iblank_left  = grid%iblank(l_left)
              iblank_right = grid%iblank(l_right)

              if ((iblank_left /= 0) .AND. (iblank_right == 0)) then
                ! ... kgl is the global index
                kgl = grid%is_unique(3) + k + bndry_offset(1) - 1     
                grid%r_ctr_lc_b(ilc,jlc,3) = grid%r_ctr_lc_b(ilc,jlc,3) + 1
                grid%r_bndry_lc_b(grid%r_ctr_lc_b(ilc,jlc,3),ilc,jlc,3) = kgl
              end if
            end do

            ! ... Step 2 gather information from each processor to have a global view
            Call MPI_BARRIER(grid%pencilcomm(3), IERR)
            allocate(recvcounts(nproc_inpencil))
            allocate(recvdisp(nproc_inpencil))

            ! ... left boundary
            ! ... use MPI_Allgather to determine the size of the data to be sent
            Call MPI_AllGATHER(grid%l_ctr_lc_b(ilc,jlc,3), 1, MPI_INTEGER, recvcounts, 1, MPI_INTEGER, grid%pencilcomm(3), ierr)
            recvdisp(1) = 0
            do i = 2, nproc_inpencil
              recvdisp(i) = recvdisp(i-1) + recvcounts(i-1)
            end do
            l_ctr_gl_b = SUM(recvcounts(:))

            ! ... call allgatherv

            allocate(sendbuf(grid%l_ctr_lc_b(ilc,jlc,3)))
            allocate(recvbuf(l_ctr_gl_b))

            do i = 1, grid%l_ctr_lc_b(ilc,jlc,3)
              sendbuf(i) = grid%l_bndry_lc_b(i,ilc,jlc,3)
            end do

            Call MPI_AllGATHERV(sendbuf, grid%l_ctr_lc_b(ilc,jlc,3), MPI_INTEGER, recvbuf, recvcounts, recvdisp, MPI_INTEGER, grid%pencilcomm(3), ierr)

            do i = 1, l_ctr_gl_b
              l_bndry_gl_b(i) = recvbuf(i)
            end do

            deallocate(sendbuf)
            deallocate(recvbuf)

            ! ... right boundary
            ! ... use MPI_Allgather to determine the size of the data to be sent
            Call MPI_AllGATHER(grid%r_ctr_lc_b(ilc,jlc,3), 1, MPI_INTEGER, recvcounts, 1, MPI_INTEGER, grid%pencilcomm(3), ierr)
            recvdisp(1) = 0
            do i = 2, nproc_inpencil
              recvdisp(i) = recvdisp(i-1) + recvcounts(i-1)
            end do
            r_ctr_gl_b = SUM(recvcounts(:))

            ! ... call allgatherv

            allocate(sendbuf(grid%r_ctr_lc_b(ilc,jlc,3)))
            allocate(recvbuf(r_ctr_gl_b))

            do i = 1, grid%r_ctr_lc_b(ilc,jlc,3)
              sendbuf(i) = grid%r_bndry_lc_b(i,ilc,jlc,3)
            end do

            Call MPI_AllGATHERV(sendbuf, grid%r_ctr_lc_b(ilc,jlc,3), MPI_INTEGER, recvbuf, recvcounts, recvdisp, MPI_INTEGER, grid%pencilcomm(3), ierr)

            do i = 1, r_ctr_gl_b
              r_bndry_gl_b(i) = recvbuf(i)
            end do

            deallocate(sendbuf)
            deallocate(recvbuf)

            deallocate(recvcounts)
            deallocate(recvdisp)

            ! ... Step 3 reduce: search for unique elements
            l_ctr_gl_rd_b = 0
            r_ctr_gl_rd_b = 0
            l_bndry_gl_rd_b(:) = 0
            r_bndry_gl_rd_b(:) = 0

            ! ... left boundary
            do i = 2, l_ctr_gl_b
              if (l_bndry_gl_b(i) /= l_bndry_gl_b(i-1)) then
                l_ctr_gl_rd_b = l_ctr_gl_rd_b + 1
                l_bndry_gl_rd_b(l_ctr_gl_rd_b) = l_bndry_gl_b(i-1)
              end if
            end do

            if (l_ctr_gl_b > 0) then  ! ... JByun 09/30/14
              l_ctr_gl_rd_b = l_ctr_gl_rd_b + 1
              l_bndry_gl_rd_b(l_ctr_gl_rd_b) = l_bndry_gl_b(l_ctr_gl_b)
            end if

            ! ... right boundary
            do i = 2, r_ctr_gl_b
              if (r_bndry_gl_b(i) /= r_bndry_gl_b(i-1)) then
                r_ctr_gl_rd_b = r_ctr_gl_rd_b + 1
                r_bndry_gl_rd_b(r_ctr_gl_rd_b) = r_bndry_gl_b(i-1)
              end if
            end do

            if (r_ctr_gl_b > 0) then ! ... JByun 09/30/14
              r_ctr_gl_rd_b = r_ctr_gl_rd_b + 1
              r_bndry_gl_rd_b(r_ctr_gl_rd_b) = r_bndry_gl_b(r_ctr_gl_b)
            end if 

            ! ... Step 4 must be aware of boundaries that may lie on another processor but whose boundary stencil extends to us
            grid%l_extended_b(ilc,jlc,3) = 0
            grid%l_extended_size_b(ilc,jlc,3) = 0
            grid%r_extended_b(ilc,jlc,3) = 0
            grid%r_extended_size_b(ilc,jlc,3) = 0

            ! ... left boundary
            do i = 1, l_ctr_gl_rd_b
              if (l_bndry_gl_rd_b(i) < grid%is_unique(3)) then
                bndry_extended = l_bndry_gl_rd_b(i)+bndry_depth-1

                if ((grid%is_unique(3) <= bndry_extended) .and. (bndry_extended <= grid%ie_unique(3))) then
                  grid%l_extended_b(ilc,jlc,3) = 1
                  grid%l_extended_size_b(ilc,jlc,3) = bndry_extended - grid%is_unique(3) + 1
                end if

              end if
            end do

            ! ... right boundary
            do i = 1, r_ctr_gl_rd_b
              if (r_bndry_gl_rd_b(i) > grid%ie_unique(3)) then
                bndry_extended = r_bndry_gl_rd_b(i)-(bndry_depth-1)

                if ((grid%is_unique(3) <= bndry_extended) .and. (bndry_extended <= grid%ie_unique(3))) then
                  grid%r_extended_b(ilc,jlc,3) = 1
                  grid%r_extended_size_b(ilc,jlc,3) = grid%ie_unique(3) - bndry_extended + 1
                end if

              end if
            end do

          end do
        end do

        ! ... Step 5 we need to know the maximum number of ghost points we need
        bndry_overlap(:) = 0

        ! ... left boundary
        grid%l_overlap_lc_b(:,:,:,3) = 0
        do jlc =1, Nbox(2)
          do ilc =1, Nbox(1)
            do i = 1, grid%l_ctr_lc_b(ilc,jlc,3)
              my_overlap = bndry_width - (grid%ie_unique(3) - grid%l_bndry_lc_b(i,ilc,jlc,3) + 1)
              if (my_overlap > 0) then
                grid%l_overlap_lc_b(i,ilc,jlc,3) = my_overlap
              end if
            end do
            if ((bndry_depth - grid%r_extended_size_b(ilc,jlc,3)) > maxval(grid%l_overlap_lc_b(:,ilc,jlc,3))) then
              grid%l_overlap_lc_b(:,ilc,jlc,3) = bndry_depth - grid%r_extended_size_b(ilc,jlc,3)
            end if
          end do
        end do

        if (maxval(grid%l_overlap_lc_b(:,:,:,3)) > 0) then
          bndry_overlap(2) = maxval(grid%l_overlap_lc_b(:,:,:,3))
        end if

        ! ... right boundary
        grid%r_overlap_lc_b(:,:,:,3) = 0
        do jlc =1, Nbox(2)
          do ilc =1, Nbox(1)
            do i = 1, grid%r_ctr_lc_b(ilc,jlc,3)
              my_overlap = bndry_width - (grid%r_bndry_lc_b(i,ilc,jlc,3) - grid%is_unique(3) + 1)
              if (my_overlap > 0) then
                grid%r_overlap_lc_b(i,jlc,jlc,3) = my_overlap
              end if
            end do
            if ((bndry_depth - grid%l_extended_size_b(ilc,jlc,3)) > maxval(grid%r_overlap_lc_b(:,ilc,jlc,3))) then
              grid%r_overlap_lc_b(:,ilc,jlc,3) = bndry_depth - grid%l_extended_size_b(ilc,jlc,3)
            end if
          end do
        end do
           
        if (maxval(grid%r_overlap_lc_b(:,:,:,3)) > 0 ) then 
          bndry_overlap(1) = maxval(grid%r_overlap_lc_b(:,:,:,3))
        end if

        grid%max_overlap_b(3) = maxval(bndry_overlap(:))

    End Select

!    do m = 1, grid%vds(dir)
!      write(*,'(A,I4,A,I4,A,I4,A,I4)') 'Add Holes:Processor', myrank, ': Found', grid%l_ctr_lc(m,dir), ' left edges for m=', m, ' dir=', dir
!        do i = 1, grid%l_ctr_lc(m,dir)
!          write(*,'(A,I4,A,I4,A,I4)') 'Add Holes:Processor', myrank, ': Location of left edge ', i, ' is ', grid%l_bndry_lc(i,m,dir)
!        end do
!      if (grid%l_extended(m,dir) > 0) then
!        write(*,'(A,I4,A,I4)') 'Add Holes:Processor', myrank, ': Has extended left edge of size ', grid%l_extended_size(m,dir)
!      end if
!    end do
!
!    do m = 1, grid%vds(dir)
!      write(*,'(A,I4,A,I4,A,I4,A,I4)') 'Add Holes:Processor', myrank, ': Found', grid%r_ctr_lc(m,dir), ' right edges for m=', m, ' dir=', dir
!        do i = 1, grid%r_ctr_lc(m,dir)
!          write(*,'(A,I4,A,I4,A,I4)') 'Add Holes:Processor', myrank, ': Location of right edge ', i, ' is ', grid%r_bndry_lc(i,m,dir)
!        end do
!      if (grid%r_extended(m,dir) > 0) then
!        write(*,'(A,I4,A,I4)') 'Add Holes:Processor', myrank, ': Has extended right edge of size ', grid%r_extended_size(m,dir)
!      end if
!    end do

  end subroutine Collect_Local_Edge_Information_Box

  subroutine Implicit_Stencil_Fill(myrank, input, dir, opID, grid)

    use ModGlobal
    use ModDataStruct
    USE ModMatrixVectorOps
    USE ModMPI

    implicit none

    integer :: myrank
    type (t_mixt_input), pointer :: input
    type (t_grid), pointer :: grid
    integer :: dir, opID

    ! ... local variables
    integer :: i, j, k, l0, l1, iis, iie, N1, N2, N3, G1, G2, G3, N1g, N2g, N3g
    integer :: is, ie, js, je, ks, ke
    integer :: ilc, jlc, klc, ii, jj
    integer :: Nc
    integer :: opType
    integer, pointer :: grid_periodic(:), deriv_type(:)
    real(rfreal) :: vals(-2:2)

    ! - edge information of box shape data struct ... WZhang, 10/2014
    INTEGER, POINTER      :: l_ctr_lc_b(:,:), r_ctr_lc_b(:,:)
    ! ... global index of locally detected edges
    INTEGER, POINTER      :: l_bndry_lc_b(:,:,:), r_bndry_lc_b(:,:,:)
    ! ... whether boundary points extended from neighboring processor
    INTEGER, POINTER      :: l_extended_b(:,:), r_extended_b(:,:)
    ! ... number of boundary points extended from neighboring processor
    INTEGER, POINTER      :: l_extended_size_b(:,:), r_extended_size_b(:,:)

    ! ... for boundary stencils
    integer :: bndry_width, bndry_depth_local, bndry_depth_max, bndry_interior
    integer :: bndry_affected, dist, bndry_location, bndry_diff
    integer :: ctr, ba, offset
    real(RFREAL) :: lhs_block1(-2:2,20), lhs_block2(-2:2,20)

    ! ... serial optimizations, WZhang 02/2015
    integer :: slice_k, offset_c, offset_ck, offset_ckj, offset_ckjii, offset_ckjb
    integer :: offset_cki, offset_ckib
    integer :: offset_cji, offset_cjib
    integer :: offset_dist
    integer :: bndry_extent
    real(rfreal) :: value
    integer, pointer :: nGhostRHS(:,:)
    integer :: grid_is(MAX_ND), grid_ie(MAX_ND), grid_is_unique(MAX_ND), grid_ie_unique(MAX_ND)
    integer :: lhs_block_depth

    ! ... pointer dereference
    opType = grid%operator_type(opID)
    grid_periodic => grid%periodic
    deriv_type   => grid%deriv_type
    
    l_ctr_lc_b   => grid%l_ctr_lc_b(:,:,dir)
    r_ctr_lc_b   => grid%r_ctr_lc_b(:,:,dir)
    l_bndry_lc_b => grid%l_bndry_lc_b(:,:,:,dir)
    r_bndry_lc_b => grid%r_bndry_lc_b(:,:,:,dir)
    l_extended_b => grid%l_extended_b(:,:,dir)
    r_extended_b => grid%r_extended_b(:,:,dir)
    l_extended_size_b => grid%l_extended_size_b(:,:,dir)
    r_extended_size_b => grid%r_extended_size_b(:,:,dir)

    nGhostRHS => grid%nGhostRHS
    grid_is(:) = grid%is(:)
    grid_ie(:) = grid%ie(:)
    grid_is_unique(:) = grid%is_unique(:)
    grid_ie_unique(:) = grid%ie_unique(:)

    N1         = grid_ie_unique(1) - grid_is_unique(1) + 1
    N2         = grid_ie_unique(2) - grid_is_unique(2) + 1
    N3         = grid_ie_unique(3) - grid_is_unique(3) + 1
    G1         = nGhostRHS(1,1) + nGhostRHS(1,2)
    G2         = nGhostRHS(2,1) + nGhostRHS(2,2)
    G3         = nGhostRHS(3,1) + nGhostRHS(3,2)
    N1g        = N1 + G1
    N2g        = N2 + G2
    N3g        = N3 + G3

    Nc         = grid%nCells
    slice_k    = N1g*N2g

    ! ... store values of interior stencil
    vals(-2) = input%lhs_interior(opID,-2)
    vals(-1) = input%lhs_interior(opID,-1)
    vals( 0) = input%lhs_interior(opID, 0)
    vals( 1) = input%lhs_interior(opID, 1)
    vals( 2) = input%lhs_interior(opID, 2)

    ! ... store values of the boundary stencil
    do j = 1, 20
      lhs_block1(-2,j) = input%lhs_block1(opID,j,-2)
      lhs_block1(-1,j) = input%lhs_block1(opID,j,-1)
      lhs_block1( 0,j) = input%lhs_block1(opID,j, 0)
      lhs_block1( 1,j) = input%lhs_block1(opID,j, 1)
      lhs_block1( 2,j) = input%lhs_block1(opID,j, 2)
      lhs_block2(-2,j) = input%lhs_block2(opID,j,-2)
      lhs_block2(-1,j) = input%lhs_block2(opID,j,-1)
      lhs_block2( 0,j) = input%lhs_block2(opID,j, 0)
      lhs_block2( 1,j) = input%lhs_block2(opID,j, 1)
      lhs_block2( 2,j) = input%lhs_block2(opID,j, 2)
    end do

    ! ... for boundary stencils
    bndry_width = input%operator_bndry_width(opID)
    bndry_depth_local = input%operator_bndry_depth(opID)
    bndry_depth_max =   maxval(input%operator_bndry_depth(:))
    bndry_interior = bndry_depth_max - bndry_depth_local
    bndry_diff = bndry_width - bndry_depth_local

    ! ... do nothing if we are not using implicit
    if (opType /= IMPLICIT) then
      return
    end if

    Select Case (dir)
      Case (1)
        dist = 1
      Case (2)
        dist = N1g
      Case (3)
        dist = N1g * N2g
    End Select

! ... initialize the implicit stencils stored in grid%penta(opID,dir)%{a,b,c,d,e}, WZhang 05/2015
! ------------------------------------------------------------------------------------

! ... first assume every point is an interior point
    grid%penta(opID,dir)%a(:,:) = vals(-2)
    grid%penta(opID,dir)%b(:,:) = vals(-1)
    grid%penta(opID,dir)%c(:,:) = vals( 0)
    grid%penta(opID,dir)%d(:,:) = vals(+1)
    grid%penta(opID,dir)%e(:,:) = vals(+2)

    Select Case (dir)
      Case (1)
        ks = grid_is_unique(3) - nGhostRHS(3,1)
        ke = grid_ie_unique(3) + nGhostRHS(3,2)
        js = grid_is_unique(2) - nGhostRHS(2,1)
        je = grid_ie_unique(2) + nGhostRHS(2,2)
        is = grid_is_unique(1)
        ie = grid_ie_unique(1)

        ! ... boundary part
        do k = grid_is_unique(3), grid_ie_unique(3)
          do j = grid_is_unique(2), grid_ie_unique(2)

            ! ... local indices on this face
            jlc = j - js + 1
            klc = k - ks + 1
            jj = (k-grid_is_unique(3))*N2 + (j-grid_is_unique(2)) + 1

            ! ... "regular" left edge
            if (l_ctr_lc_b(jlc,klc) > 0) then
              do ctr = 1, l_ctr_lc_b(jlc,klc)
                bndry_location = l_bndry_lc_b(ctr,jlc,klc)
                bndry_extent = ie - bndry_location + 1
                if (bndry_extent < bndry_depth_max) then
                  bndry_affected = bndry_extent
                else
                  bndry_affected = bndry_depth_max
                end if
                do ba = 1, bndry_affected
                  if (ba + bndry_interior <= bndry_depth_max) then
                    lhs_block_depth = ba
                    ii = ba + bndry_location - is
                    grid%penta(opID,dir)%a(jj,ii) = lhs_block1(-2,lhs_block_depth)
                    grid%penta(opID,dir)%b(jj,ii) = lhs_block1(-1,lhs_block_depth)
                    grid%penta(opID,dir)%c(jj,ii) = lhs_block1( 0,lhs_block_depth)
                    grid%penta(opID,dir)%d(jj,ii) = lhs_block1(+1,lhs_block_depth)
                    grid%penta(opID,dir)%e(jj,ii) = lhs_block1(+2,lhs_block_depth)
                  end if
                end do  ! ... ba
              end do  ! ... ctr  
            end if ! ... "regular" left edge

            ! ... "extended" left edge
            if (l_extended_b(jlc,klc) > 0) then
              bndry_location = grid_is_unique(1)
              bndry_affected = l_extended_size_b(jlc,klc)
              offset = bndry_depth_max - bndry_affected
              do ba = 1, bndry_affected
                if (ba + offset + bndry_interior <= bndry_depth_max) then
                  lhs_block_depth = ba + offset
                  ii = ba + bndry_location - is
                  grid%penta(opID,dir)%a(jj,ii) = lhs_block1(-2,lhs_block_depth)
                  grid%penta(opID,dir)%b(jj,ii) = lhs_block1(-1,lhs_block_depth)
                  grid%penta(opID,dir)%c(jj,ii) = lhs_block1( 0,lhs_block_depth)
                  grid%penta(opID,dir)%d(jj,ii) = lhs_block1(+1,lhs_block_depth)
                  grid%penta(opID,dir)%e(jj,ii) = lhs_block1(+2,lhs_block_depth)
                end if
              end do ! ... ba
            end if ! ... "extended" left edge

            ! ... "regular" right edge
            if (r_ctr_lc_b(jlc,klc) > 0) then
              do ctr = 1, r_ctr_lc_b(jlc,klc)
                bndry_location = r_bndry_lc_b(ctr,jlc,klc)
                bndry_extent = bndry_location - is + 1
                if (bndry_extent < bndry_depth_max) then
                  bndry_affected = bndry_extent
                else
                  bndry_affected = bndry_depth_max
                end if
                offset = bndry_depth_max - bndry_affected
                do ba = 1, bndry_affected
                  if ((ba+offset) > bndry_interior ) then
                    lhs_block_depth = ba - bndry_interior + offset
                    ii = ba + bndry_location - bndry_affected - is + 1
                    grid%penta(opID,dir)%a(jj,ii) = lhs_block2(-2,lhs_block_depth)
                    grid%penta(opID,dir)%b(jj,ii) = lhs_block2(-1,lhs_block_depth)
                    grid%penta(opID,dir)%c(jj,ii) = lhs_block2( 0,lhs_block_depth)
                    grid%penta(opID,dir)%d(jj,ii) = lhs_block2(+1,lhs_block_depth)
                    grid%penta(opID,dir)%e(jj,ii) = lhs_block2(+2,lhs_block_depth)
                  end if
                end do  ! ... ba
              end do  ! ... ctr
            end if ! ... "regular" right edge

            ! ... "extended" right edge
            if (r_extended_b(jlc,klc) > 0) then
              bndry_location = grid_ie_unique(1)
              bndry_affected = r_extended_size_b(jlc,klc)
              do ba = 1, bndry_affected
                if (ba > bndry_interior) then
                  lhs_block_depth = ba - bndry_interior
                  ii = ba + bndry_location - bndry_affected - is + 1
                  grid%penta(opID,dir)%a(jj,ii) = lhs_block2(-2,lhs_block_depth)
                  grid%penta(opID,dir)%b(jj,ii) = lhs_block2(-1,lhs_block_depth)
                  grid%penta(opID,dir)%c(jj,ii) = lhs_block2( 0,lhs_block_depth)
                  grid%penta(opID,dir)%d(jj,ii) = lhs_block2(+1,lhs_block_depth)
                  grid%penta(opID,dir)%e(jj,ii) = lhs_block2(+2,lhs_block_depth)
                end if
              end do ! ... ba
            end if ! ... "extended" right edge

          end do ! ... j
        end do ! ... k

      Case (2)
        ks = grid_is_unique(3) - nGhostRHS(3,1)
        ke = grid_ie_unique(3) + nGhostRHS(3,2)
        is = grid_is_unique(1) - nGhostRHS(1,1)
        ie = grid_ie_unique(1) + nGhostRHS(1,2)
        js = grid_is_unique(2)
        je = grid_ie_unique(2)

        ! ... boundary part
        do k = grid_is_unique(3), grid_ie_unique(3)
          do i = grid_is_unique(1), grid_ie_unique(1)

            ! ... local indices on this face
            ilc = i - is + 1
            klc = k - ks + 1
            jj = (k-grid_is_unique(3))*N1 + (i-grid_is_unique(1)) + 1

            ! ... "regular" left edge
            if (l_ctr_lc_b(ilc,klc) > 0) then
              do ctr = 1, l_ctr_lc_b(ilc,klc)
                bndry_location = l_bndry_lc_b(ctr,ilc,klc)
                bndry_extent = je - bndry_location + 1
                if (bndry_extent < bndry_depth_max) then
                  bndry_affected = bndry_extent
                else
                  bndry_affected = bndry_depth_max
                end if
                do ba = 1, bndry_affected
                  if (ba + bndry_interior <= bndry_depth_max) then
                    lhs_block_depth = ba
                    ii = ba + bndry_location - js
                    grid%penta(opID,dir)%a(jj,ii) = lhs_block1(-2,lhs_block_depth)
                    grid%penta(opID,dir)%b(jj,ii) = lhs_block1(-1,lhs_block_depth)
                    grid%penta(opID,dir)%c(jj,ii) = lhs_block1( 0,lhs_block_depth)
                    grid%penta(opID,dir)%d(jj,ii) = lhs_block1(+1,lhs_block_depth)
                    grid%penta(opID,dir)%e(jj,ii) = lhs_block1(+2,lhs_block_depth)
                  end if
                end do  ! ... ba
              end do  ! ... ctr  
            end if ! ... "regular" left edge

            ! ... "extended" left edge
            if (l_extended_b(ilc,klc) > 0) then
              bndry_location = grid_is_unique(2)
              bndry_affected = l_extended_size_b(ilc,klc)
              offset = bndry_depth_max - bndry_affected
              do ba = 1, bndry_affected
                if (ba + offset + bndry_interior <= bndry_depth_max) then
                  lhs_block_depth = ba + offset
                  ii = ba + bndry_location - js
                  grid%penta(opID,dir)%a(jj,ii) = lhs_block1(-2,lhs_block_depth)
                  grid%penta(opID,dir)%b(jj,ii) = lhs_block1(-1,lhs_block_depth)
                  grid%penta(opID,dir)%c(jj,ii) = lhs_block1( 0,lhs_block_depth)
                  grid%penta(opID,dir)%d(jj,ii) = lhs_block1(+1,lhs_block_depth)
                  grid%penta(opID,dir)%e(jj,ii) = lhs_block1(+2,lhs_block_depth)
                end if
              end do ! ... ba
            end if ! ... "extended" left edge

            ! ... "regular" right edge
            if (r_ctr_lc_b(ilc,klc) > 0) then
              do ctr = 1, r_ctr_lc_b(ilc,klc)
                bndry_location = r_bndry_lc_b(ctr,ilc,klc)
                bndry_extent = bndry_location - js + 1
                if (bndry_extent < bndry_depth_max) then
                  bndry_affected = bndry_extent
                else
                  bndry_affected = bndry_depth_max
                end if
                offset = bndry_depth_max - bndry_affected
                do ba = 1, bndry_affected
                  if ((ba+offset) > bndry_interior ) then
                    lhs_block_depth = ba - bndry_interior + offset
                    ii = ba + bndry_location - bndry_affected - js + 1
                    grid%penta(opID,dir)%a(jj,ii) = lhs_block2(-2,lhs_block_depth)
                    grid%penta(opID,dir)%b(jj,ii) = lhs_block2(-1,lhs_block_depth)
                    grid%penta(opID,dir)%c(jj,ii) = lhs_block2( 0,lhs_block_depth)
                    grid%penta(opID,dir)%d(jj,ii) = lhs_block2(+1,lhs_block_depth)
                    grid%penta(opID,dir)%e(jj,ii) = lhs_block2(+2,lhs_block_depth)
                  end if
                end do  ! ... ba
              end do  ! ... ctr
            end if ! ... "regular" right edge

            ! ... "extended" right edge
            if (r_extended_b(ilc,klc) > 0) then
              bndry_location = grid_ie_unique(2)
              bndry_affected = r_extended_size_b(ilc,klc)
              do ba = 1, bndry_affected
                if (ba > bndry_interior) then
                  lhs_block_depth = ba - bndry_interior
                  ii = ba + bndry_location - bndry_affected - js + 1
                  grid%penta(opID,dir)%a(jj,ii) = lhs_block2(-2,lhs_block_depth)
                  grid%penta(opID,dir)%b(jj,ii) = lhs_block2(-1,lhs_block_depth)
                  grid%penta(opID,dir)%c(jj,ii) = lhs_block2( 0,lhs_block_depth)
                  grid%penta(opID,dir)%d(jj,ii) = lhs_block2(+1,lhs_block_depth)
                  grid%penta(opID,dir)%e(jj,ii) = lhs_block2(+2,lhs_block_depth)
                end if
              end do ! ... ba
            end if ! ... "extended" right edge

          end do ! ... i
        end do ! ... k

      Case (3)
        ks = grid_is_unique(3)
        ke = grid_ie_unique(3)
        js = grid_is_unique(2) - nGhostRHS(2,1)
        je = grid_ie_unique(2) + nGhostRHS(2,2)
        is = grid_is_unique(1) - nGhostRHS(1,1)
        ie = grid_ie_unique(1) + nGhostRHS(1,2)

        ! ... boundary part
        do j = grid_is_unique(2), grid_ie_unique(2)
          do i = grid_is_unique(1), grid_ie_unique(1)

            ! ... local indices on this face
            ilc = i - is + 1
            jlc = j - js + 1
            jj = (j-grid_is_unique(2))*N1 + (i-grid_is_unique(1)) + 1

            ! ... "regular" left edge
            if (l_ctr_lc_b(ilc,jlc) > 0) then
              do ctr = 1, l_ctr_lc_b(ilc,jlc)
                bndry_location = l_bndry_lc_b(ctr,ilc,jlc)
                bndry_extent = ke - bndry_location + 1
                if (bndry_extent < bndry_depth_max) then
                  bndry_affected = bndry_extent
                else
                  bndry_affected = bndry_depth_max
                end if
                do ba = 1, bndry_affected
                  if (ba + bndry_interior <= bndry_depth_max) then
                    lhs_block_depth = ba
                    ii = ba + bndry_location - ks
                    grid%penta(opID,dir)%a(jj,ii) = lhs_block1(-2,lhs_block_depth)
                    grid%penta(opID,dir)%b(jj,ii) = lhs_block1(-1,lhs_block_depth)
                    grid%penta(opID,dir)%c(jj,ii) = lhs_block1( 0,lhs_block_depth)
                    grid%penta(opID,dir)%d(jj,ii) = lhs_block1(+1,lhs_block_depth)
                    grid%penta(opID,dir)%e(jj,ii) = lhs_block1(+2,lhs_block_depth)
                  end if
                end do  ! ... ba
              end do  ! ... ctr  
            end if ! ... "regular" left edge

            ! ... "extended" left edge
            if (l_extended_b(ilc,jlc) > 0) then
              bndry_location = grid_is_unique(3)
              bndry_affected = l_extended_size_b(ilc,jlc)
              offset = bndry_depth_max - bndry_affected
              do ba = 1, bndry_affected
                if (ba + offset + bndry_interior <= bndry_depth_max) then
                  lhs_block_depth = ba + offset
                  ii = ba + bndry_location - ks
                  grid%penta(opID,dir)%a(jj,ii) = lhs_block1(-2,lhs_block_depth)
                  grid%penta(opID,dir)%b(jj,ii) = lhs_block1(-1,lhs_block_depth)
                  grid%penta(opID,dir)%c(jj,ii) = lhs_block1( 0,lhs_block_depth)
                  grid%penta(opID,dir)%d(jj,ii) = lhs_block1(+1,lhs_block_depth)
                  grid%penta(opID,dir)%e(jj,ii) = lhs_block1(+2,lhs_block_depth)
                end if
              end do ! ... ba
            end if ! ... "extended" left edge

            ! ... "regular" right edge
            if (r_ctr_lc_b(ilc,jlc) > 0) then
              do ctr = 1, r_ctr_lc_b(ilc,jlc)
                bndry_location = r_bndry_lc_b(ctr,ilc,jlc)
                bndry_extent = bndry_location - ks + 1
                if (bndry_extent < bndry_depth_max) then
                  bndry_affected = bndry_extent
                else
                  bndry_affected = bndry_depth_max
                end if
                offset = bndry_depth_max - bndry_affected
                do ba = 1, bndry_affected
                  if ((ba+offset) > bndry_interior ) then
                    lhs_block_depth = ba - bndry_interior + offset
                    ii = ba + bndry_location - bndry_affected - ks + 1
                    grid%penta(opID,dir)%a(jj,ii) = lhs_block2(-2,lhs_block_depth)
                    grid%penta(opID,dir)%b(jj,ii) = lhs_block2(-1,lhs_block_depth)
                    grid%penta(opID,dir)%c(jj,ii) = lhs_block2( 0,lhs_block_depth)
                    grid%penta(opID,dir)%d(jj,ii) = lhs_block2(+1,lhs_block_depth)
                    grid%penta(opID,dir)%e(jj,ii) = lhs_block2(+2,lhs_block_depth)
                  end if
                end do  ! ... ba
              end do  ! ... ctr
            end if ! ... "regular" right edge

            ! ... "extended" right edge
            if (r_extended_b(ilc,jlc) > 0) then
              bndry_location = grid_ie_unique(3)
              bndry_affected = r_extended_size_b(ilc,jlc)
              do ba = 1, bndry_affected
                if (ba > bndry_interior) then
                  lhs_block_depth = ba - bndry_interior
                  ii = ba + bndry_location - bndry_affected - ks + 1
                  grid%penta(opID,dir)%a(jj,ii) = lhs_block2(-2,lhs_block_depth)
                  grid%penta(opID,dir)%b(jj,ii) = lhs_block2(-1,lhs_block_depth)
                  grid%penta(opID,dir)%c(jj,ii) = lhs_block2( 0,lhs_block_depth)
                  grid%penta(opID,dir)%d(jj,ii) = lhs_block2(+1,lhs_block_depth)
                  grid%penta(opID,dir)%e(jj,ii) = lhs_block2(+2,lhs_block_depth)
                end if
              end do ! ... ba
            end if ! ... "extended" right edge

          end do ! ... i
        end do ! ... j
    End Select

    ! ... turn off the holes
    ks = grid_is_unique(3)
    ke = grid_ie_unique(3)
    js = grid_is_unique(2)
    je = grid_ie_unique(2)
    is = grid_is_unique(1)
    ie = grid_ie_unique(1)
    Select Case (dir)
      Case (1)
        do k = ks, ke
          do j = js, je
            do i = is, ie
              l0 = (k-ks+nGhostRHS(3,1))*slice_k + (j-js+nGhostRHS(2,1))*N1g + (i-is+nGhostRHS(1,1)) + 1
              if (grid%iblank(l0)==0) then
                jj = (k-ks)*N2 + j - js + 1
                grid%penta(opID,dir)%a(jj,i-is+1) = 0.0_rfreal
                grid%penta(opID,dir)%b(jj,i-is+1) = 0.0_rfreal
                grid%penta(opID,dir)%c(jj,i-is+1) = 1.0_rfreal
                grid%penta(opID,dir)%d(jj,i-is+1) = 0.0_rfreal
                grid%penta(opID,dir)%e(jj,i-is+1) = 0.0_rfreal
              end if
            end do
          end do
        end do

      Case (2)
        do k = ks, ke
          do i = is, ie
            do j = js, je
              l0 = (k-ks+nGhostRHS(3,1))*slice_k + (j-js+nGhostRHS(2,1))*N1g + (i-is+nGhostRHS(1,1)) + 1
              if (grid%iblank(l0)==0) then
                jj = (k-ks)*N1 + i - is + 1
                grid%penta(opID,dir)%a(jj,j-js+1) = 0.0_rfreal
                grid%penta(opID,dir)%b(jj,j-js+1) = 0.0_rfreal
                grid%penta(opID,dir)%c(jj,j-js+1) = 1.0_rfreal
                grid%penta(opID,dir)%d(jj,j-js+1) = 0.0_rfreal
                grid%penta(opID,dir)%e(jj,j-js+1) = 0.0_rfreal
              end if
            end do
          end do
        end do

      Case (3)
        do j = js, je
          do i = is, ie
            do k = ks, ke
              l0 = (k-ks+nGhostRHS(3,1))*slice_k + (j-js+nGhostRHS(2,1))*N1g + (i-is+nGhostRHS(1,1)) + 1
              if (grid%iblank(l0)==0) then
                jj = (j-js)*N1 + i - is + 1
                grid%penta(opID,dir)%a(jj,k-ks+1) = 0.0_rfreal
                grid%penta(opID,dir)%b(jj,k-ks+1) = 0.0_rfreal
                grid%penta(opID,dir)%c(jj,k-ks+1) = 1.0_rfreal
                grid%penta(opID,dir)%d(jj,k-ks+1) = 0.0_rfreal
                grid%penta(opID,dir)%e(jj,k-ks+1) = 0.0_rfreal
              end if
            end do
          end do
        end do
    End Select

    return

  end subroutine Implicit_Stencil_Fill

end module ModDerivBuildOps
