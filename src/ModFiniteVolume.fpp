! Copyright (c) 2015, University of Illinois at Urbana-Champaign, XPACC
! License: MIT, http://opensource.org/licenses/MIT
![UNUSED]
MODULE ModFiniteVolume

Contains
  
  Subroutine FVEuler(region,ng,flags)
    
    USE ModGlobal
    USE ModDataStruct
    USE ModMPI
    USE ModEOS
    USE ModFVSetup
    
    Implicit None

    Type(t_region), pointer :: region
    Integer :: ng, flags
    
    ! ... local
    type(t_grid), pointer :: grid
    type(t_mixt_input), pointer :: input
    type(t_fvgrid), pointer :: fv
    type(t_mixt), pointer :: state
    type(t_fvface), pointer :: face
    Real(rfreal), pointer :: flux(:,:)

    Integer :: N(MAX_ND), Ncg(MAX_ND), Nf(MAX_ND), Nc(MAX_ND)
    Integer :: i, j, k, dir, m, l0, lfl, lfr, lc
    
    ! ... simplicity
    grid => region%grid(ng)
    input => grid%input
    state => region%state(ng)

    
    N = 1; Ncg = 1; Nf = 1; Nc = 1
       do i = 1, grid%ND
          N(i) = grid%ie(i)-grid%is(i) + 1
          Ncg(i) = N(i) + SUM(grid%fv%nGhost(i,:))
          Nf(i) = Ncg(i) - 1
          Nc(i) = Nf(i) - 1
       end do

       ! ... we need at least Ncg=3 to enter the loop
       ! ... doesn't matter if it is a lie
       do dir = 1, 3
          Ncg(dir)=max(Ncg(dir),3)
       end do

    call HLLCFlux(region,ng)

    ! ... loop through the directions
    do dir = 1, grid%ND

       ! ... simplicity
       face => grid%fv%face(dir)
       flux => state%sweep(dir)%flux
       
       do k = 2, Ncg(3)-1
          do j = 2, Ncg(2)-1
             do i = 2, Ncg(1)-1
                l0 = (k-1-grid%fv%nGhost(3,1))*N(1)*N(2)*(grid%ND-2) + (j-1-grid%fv%nGhost(2,1))*N(1) + i-grid%fv%nGhost(1,1)
                lc = (k-2)*Nc(1)*Nc(2)*(grid%ND-2) + (j-2)*Nc(1) + i-1

                if(dir == 1) then
                   lfl = (k-2)*Nf(1)*Nc(2) + (j-2)*Nf(1) + i - 1
                   lfr = (k-2)*Nf(1)*Nc(2) + (j-2)*Nf(1) + i
                elseif(dir == 2) then
                   lfl = (k-2)*Nc(1)*Nf(2) + (j-2)*Nc(1) + i - 1
                   lfr = (k-2)*Nc(1)*Nf(2) + (j-1)*Nc(1) + i - 1
                elseif(dir == 3) then
                   lfl = (k-2)*Nc(1)*Nc(2) + (j-2)*Nc(1) + i - 1
                   lfr = (k-1)*Nc(1)*Nc(2) + (j-2)*Nc(1) + i - 1
                end if
    
                do m = 1, grid%ND+2
                   state%rhs(l0,m) = state%rhs(l0,m) + (flux(lfl,m)*face%area(lfl) - flux(lfr,m)*face%area(lfr))/grid%fv%vol(lc)
                end do
    
             end do ! ... i
          end do ! ... j
       end do ! ... k
    end do ! ... dir

  end Subroutine FVEuler


  Subroutine HLLCFlux(region,ng)

    USE ModGlobal
    USE ModDataStruct
    USE ModMPI
    USE ModEOS
    USE ModFVSetup
    USE ModDeriv

    Implicit None
    
    Type(t_region), pointer :: region
    Integer :: ng
    
    ! ... local
    type(t_grid), pointer :: grid
    type(t_mixt_input), pointer :: input
    type(t_fvgrid), pointer :: fv
    type(t_mixt), pointer :: state
    type(t_fvface), pointer :: face
    Real(rfreal), pointer :: SndSpdAux(:), PAux(:), GamAux(:), RhoAux(:), UVWAux(:,:), UAux(:), EAux(:)
    Real(rfreal) :: GamRef
    Real(rfreal), pointer :: flux(:,:), F(:,:), U(:), FTemp(:)
    Integer :: N(MAX_ND), Ncg(MAX_ND)
    Integer, pointer :: Nc(:), Nf(:)
    Integer :: i, j, k, l0, l1, m, nGhostCells, nFaces, dir, lc, lf, ND

    ! ... HLLC variables
    Real(rfreal) :: Pstar, Sstar
    Real(rfreal) :: q(2), S(2)
    Real(rfreal), pointer :: uprime(:,:)
    Integer :: dr, dl, ir, il, ng_copy

    ng_copy=ng
    ! ... simplicity
    grid => region%grid(ng)
    state => region%state(ng)
    input => grid%input
    fv => grid%fv
    ND = grid%ND
    Nc => fv%Nc
    Nf => fv%Nf

    N = 1; Ncg = 1;
    do i = 1, grid%ND
       N(i) = grid%ie(i)-grid%is(i) + 1
       Ncg(i) = N(i) + SUM(grid%fv%nGhost(i,:))
    end do
    

    nGhostCells = product(Ncg(:))
    GamRef = input%GamRef

    ! ... allocate the auxilary arrays
    allocate(SndSpdAux(nGhostCells), PAux(nGhostCells), GamAux(nGhostCells), RhoAux(nGhostCells))
    allocate(UVWAux(nGhostCells,3), UAux(nGhostCells), EAux(nGhostCells))

    do i = 1, nGhostCells
       SndSpdAux(i) = 0.0_rfreal
       PAux(i) = 0.0_rfreal
       GamAux(i) = 0.0_rfreal
       RhoAux(i) = 0.0_rfreal
       UAux(i) = 0.0_rfreal
       EAux(i) = 0.0_rfreal
       do j = 1, 3
          UVWAux(i,j) = 0.0_rfreal
       end do
    end do

    ! ... fill
    do k = 1, N(3)
       do j = 1, N(2)
          do i = 1, N(1)
             l0 = (k-1)*N(1)*N(2) + (j-1)*N(1) + i
             l1 = (k-1+grid%fv%nGhost(3,1))*Ncg(1)*Ncg(2) + &
                  (j-1+grid%fv%nGhost(2,1))*Ncg(1) + &
                  i + grid%fv%nGhost(1,1)
             
             PAux(l1) = state%dv(l0,1)
             RhoAux(l1) = state%cv(l0,1)
             GamAux(l1) = state%gv(l0,1)

             do m = 1, grid%ND
                UVWAux(l1,m) = state%cv(l0,m+1)*state%dv(l0,3)
             end do

             EAux(l1) = state%cv(l0,grid%ND+2)*state%dv(l0,3)
             
          end do
       end do
    end do

    
    ! ... communication
    ! ... pressure
    call Ghost_Cell_Exchange_FV(region,PAux,ng)
    ! ... density
    call Ghost_Cell_Exchange_FV(region,RhoAux,ng)
    ! ... ratio of specific heats
    call Ghost_Cell_Exchange_FV(region,GamAux,ng)
    ! ... velocities
    do m = 1, grid%ND
       do l1 = 1, nGhostCells
          UAux(l1) = UVWAux(l1,m)
       end do
       call Ghost_Cell_Exchange_FV(region,UAux,ng)
       do l1 = 1, nGhostCells
          UVWAux(l1,m) = UAux(l1)
       end do
    end do
    ! ... total energy, E
    call Ghost_Cell_Exchange_FV(region,EAux,ng)

    ! ... calculate the local speed of sound
    do l1 = 1, nGhostCells
       SndSpdAux(l1) =  GamAux(l1) * sqrt(PAux(l1)/(RhoAux(l1)*GamRef))
    end do
    
    ! ... hold left and right face oriented velocity components
    allocate(uprime(2,grid%ND))

    ! ... hold left and right fluxes, F(U_L) and F(U_R), and U = U*-U_k, k=L,R
    allocate(U(grid%ND+2),F(2,grid%ND+2))

    ! ... for rotation back into cartesian
    allocate(FTemp(MAX_ND))

    ! ... initialize
    uprime(:,:) = 0.0_rfreal
    U(:) = 0.0_rfreal
    F(:,:) = 0.0_rfreal
    FTemp(:) = 0.0_rfreal

    ! ... loop through each sweep direction
    do dir = 1, grid%ND
       
       ! ... simplicity
       flux => state%sweep(dir)%flux
       face => grid%fv%face(dir)

       if(dir == 1) then
          nFaces = Nf(1)*Nc(2)*Nc(3)
       elseif(dir == 2) then
          nFaces = Nc(1)*Nf(2)*Nc(3)
       elseif(dir == 3) then
          nFaces = Nc(1)*Nc(2)*Nf(3)
       end if

       do lf = 1, nFaces
          
          il = face%LRindex(lf,1)
          ir = face%LRindex(lf,2)

          ! ... velocities in local coordinate system
          uprime(:,:) = 0.0_rfreal
          do m = 1, grid%ND
             ! ... left state
             uprime(1,1) = uprime(1,1) + UVWAux(il,m)*face%normal_vec(lf,m)
             uprime(1,2) = uprime(1,2) + UVWAux(il,m)*face%tangent_vec(lf,m,1)
             if(grid%ND==3) then
                uprime(1,3) = uprime(1,3) + UVWAux(il,m)*face%tangent_vec(lf,m,2)
             end if
             ! ... right state
             uprime(2,1) = uprime(2,1) + UVWAux(ir,m)*face%normal_vec(lf,m)
             uprime(2,2) = uprime(2,2) + UVWAux(ir,m)*face%tangent_vec(lf,m,1)
             if(grid%ND==3) then
                uprime(2,3) = uprime(2,3) + UVWAux(ir,m)*face%tangent_vec(lf,m,2)
             end if
          end do

          ! ... using primitive variable Riemann solver (PVRS-based) pressure
          Pstar = 0.5_rfreal*(PAux(il)+PAux(ir)) - 0.5_rfreal*(uprime(2,1)-uprime(1,1))* &
               (0.25_rfreal*(RhoAux(il)+RhoAux(ir))*(SndSpdAux(il)+SndSpdAux(ir)))
          Pstar = MAX(Pstar,0.0_rfreal)

          ! ... left wave
          if(Pstar .le. PAux(il)) then
             ! ... expansion
             q(1) = 1.0_rfreal
          elseif(Pstar .gt. Paux(il)) then
             ! ... shock speed
             q(1) = sqrt(1.0_rfreal+(GamAux(il)+1.0_rfreal)/(2.0_rfreal*GamAux(il))*(Pstar/PAux(il)-1.0_rfreal))
          endif
          S(1) = uprime(1,1)-SndSpdAux(il)*q(1)
          
          F(1,1) = RhoAux(il)*uprime(1,1)
          F(1,2) = RhoAux(il)*uprime(1,1)*uprime(1,1) + PAux(il)
          F(1,3) = RhoAux(il)*uprime(1,1)*uprime(1,2)
          if(grid%ND == 3) F(1,4) = RhoAux(il)*uprime(1,1)*uprime(1,3)
          F(1,grid%ND+2) = uprime(1,1)*(RhoAux(il)*EAux(il)+PAux(il))

          if(S(1) .ge. 0.0_rfreal) then
             do m = 1, grid%ND+2
                flux(lf,m) = F(1,m)
             end do
             cycle
          end if
          
          ! ... right wave
          if(Pstar .le. PAux(ir)) then
             ! ... expansion
             q(2) = 1.0_rfreal
          elseif(Pstar .gt. Paux(ir)) then
             ! ... shock speed
             q(2) = sqrt(1.0_rfreal+(GamAux(ir)+1.0_rfreal)/(2.0_rfreal*GamAux(ir))*(Pstar/PAux(ir)-1.0_rfreal))
          endif
          S(2) = uprime(2,1)+SndSpdAux(ir)*q(2)
          
          F(2,1) = RhoAux(ir)*uprime(2,1)
          F(2,2) = RhoAux(ir)*uprime(2,1)*uprime(2,1) + PAux(ir)
          F(2,3) = RhoAux(ir)*uprime(2,1)*uprime(2,2)
          if(grid%ND == 3) F(2,4) = RhoAux(ir)*uprime(2,1)*uprime(2,3)
          F(2,grid%ND+2) = uprime(2,1)*(RhoAux(ir)*EAux(ir)+PAux(ir))

          if(S(2) .le. 0.0_rfreal) then
             do m = 1, grid%ND+2
                flux(lf,m) = F(2,m)
             end do
             cycle
          end if

          ! ... contact wave
          Sstar = RhoAux(il)*(S(1)-uprime(1,1)) - RhoAux(ir)*(S(2)-uprime(2,1))
          Sstar = (PAux(ir)-PAux(il)+RhoAux(il)*uprime(1,1)*(S(1)-uprime(1,1))-RhoAux(ir)*uprime(2,1)*(S(2)-uprime(2,1)))/Sstar
         
          ! ... set the flux
          if(S(1).le.0.0_rfreal .and. Sstar .ge.0.0_rfreal) then
             U(1) = (RhoAux(il)*( (S(1)-uprime(1,1))/(S(1)-Sstar)             - 1.0_rfreal   ))
             U(2) = (RhoAux(il)*( (S(1)-uprime(1,1))/(S(1)-Sstar)*Sstar       - uprime(1,1)  ))
             U(3) = (RhoAux(il)*( (S(1)-uprime(1,1))/(S(1)-Sstar)*uprime(1,2) - uprime(1,2)  ))
             if(grid%ND == 3) U(4) = (RhoAux(il)*( (S(1)-uprime(1,1))/(S(1)-Sstar)*uprime(1,3) - uprime(1,3)  ))
             U(grid%ND+2) = (RhoAux(il)*( (S(1)-uprime(1,1))/(S(1)-Sstar)*(EAux(il) + &
                  (Sstar - uprime(1,1))*(Sstar + PAux(il)/(RhoAux(il)*(S(1)-uprime(1,1))))) - EAux(il)))

             do m = 1,grid%ND+2
                flux(lf,m) = F(1,m) + S(1)*U(m)
             end do
             cycle
          end if

          ! ... set the flux
          if(S(2).ge.0.0_rfreal .and. Sstar .le. 0.0_rfreal) then
             U(1) = (RhoAux(ir)*( (S(2)-uprime(2,1))/(S(2)-Sstar)             - 1.0_rfreal   ))
             U(2) = (RhoAux(ir)*( (S(2)-uprime(2,1))/(S(2)-Sstar)*Sstar       - uprime(2,1)  ))
             U(3) = (RhoAux(ir)*( (S(2)-uprime(2,1))/(S(2)-Sstar)*uprime(2,2) - uprime(2,2)  ))
             if(grid%ND == 3) U(4) = (RhoAux(ir)*( (S(2)-uprime(2,1))/(S(2)-Sstar)*uprime(2,3) - uprime(2,3)  ))
             U(grid%ND+2) = (RhoAux(ir)*( (S(2)-uprime(2,1))/(S(2)-Sstar)*(EAux(ir) + &
                  (Sstar - uprime(2,1))*(Sstar + PAux(ir)/(RhoAux(ir)*(S(2)-uprime(2,1))))) - EAux(ir)))

             do m = 1,grid%ND+2
                flux(lf,m) = F(2,m) + S(2)*U(m)
             end do
             cycle
          end if

       end do ! ... lf
       
       do lf = 1, nFaces
          do m = 1, grid%ND
             FTemp(m) = 0.0_rfreal
             FTemp(m) = flux(lf,m+1)
          end do       

          ! ... rotate back to cartesian coordinates
          do m = 1, grid%ND
             flux(lf,m+1) = FTemp(1)*face%normal_vec(lf,m) + FTemp(2)*face%tangent_vec(lf,m,1) + FTemp(3)*face%tangent_vec(lf,m,2)
          end do

       end do ! ... lf
    end do ! ... dir
 
    deallocate(F,U,uprime,FTemp)
    deallocate(RhoAux,PAux,GamAux,UVWAux,UAux,EAux,SndSpdAux)

  end Subroutine HLLCFlux

END MODULE ModFiniteVolume
![/UNUSED]
