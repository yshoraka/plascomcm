! Copyright (c) 2015, University of Illinois at Urbana-Champaign, XPACC
! License: MIT, http://opensource.org/licenses/MIT
!
! $Header: /cvsroot/genx/Codes/RocfloCM/Source/ModRegion.f90,v 1.44 2011/10/20 16:55:21 bodony Exp $
!
Module ModRegion

Contains

  Subroutine initialize_region(region)

    USE ModGlobal
    USE ModDataStruct
    USE ModInput
    USE ModMPI
    USE ModIO
    USE ModInitialCondition
    USE ModEOS
    USE ModCombustion
#ifdef BUILD_ELECTRIC
    USE ModElectric
#endif

    Implicit None

    Type (t_region), pointer :: region

    ! ... local variables
    integer :: NDIM, ngrid, parDir_loc(1), ps, pe, i, j, ng, num_unique, p, dir
    integer :: numGridsInFile, gs, ge, world_group, grid_group, Ntotal
    integer :: color, comm, core_group, alpha, num_recv
    integer :: error_local, error_global
    integer, pointer :: ND(:,:), gridToProcMap(:,:), ibuf(:)
    integer :: N(MAX_ND), Nf(MAX_ND), Nc(MAX_ND), ierr
    integer :: status(MPI_STATUS_SIZE)
    integer, allocatable :: ranks(:)
    Type (t_mixt_input), pointer :: input
    Type (t_grid), pointer :: grid
    Type (t_fvgrid), pointer :: fv
    Logical :: left_bc, right_bc
    Real(rfreal) :: e_internal,gamma
    logical :: local_decomp_error, decomp_error

    ! Set flags for initialization of save variables to false...
    ! Then when calling allocate on the struct, set its flag to true.
    region%lighthillInit = .false.; region%derivInit     = .false.
    region%nsbcInit      = .false.; region%nsrhsInit     = .false.
    region%rkInit        = .false.; region%eulerfvInit   = .false.
    ! ModEulerFV has many saves initialized in different subroutines
    region%eulerfvInit1  = .false.; region%eulerfvInit2  = .false.
    region%eulerfvInit3  = .false.; region%eulerfvInit4  = .false.

    ! Initialize some module variables
    region%modIOnnz = 0

    ! ... fill the grid array
    do ng = 1, region%nGrids

      ! ... this grid
      grid  => region%grid(ng)
      input => grid%input

      ! ... total number of grids in file
      grid%numGridsInFile = region%nGridsGlobal

      ! ... total size of each grid
      i = size(region%AllGridsGlobalSize,1)
      j = size(region%AllGridsGlobalSize,2)
      allocate(grid%AllGridsGlobalSize(i,j))
      grid%AllGridsGlobalSize(:,:) = region%AllGridsGlobalSize(:,:)

      ! ... mapping of grids to processors
      allocate(grid%gridToProcMap(region%nGridsGlobal,2))
      grid%gridToProcMap(:,:) = region%gridToProcMap(:,:)

      ! ... dimension of the problem
      grid%ND = input%ND

      ! ... nullify
      nullify(grid%XYZ);  
      nullify(grid%XYZOld);
      nullify(grid%penta);
      nullify(grid%global_iblank);
      nullify(grid%global_XYZ);
      nullify(grid%remote_is);
      nullify(grid%remote_ie);
      nullify(grid%area)
      nullify(grid%ibfac)

      ! ... save world communicator
      grid%commWorld = mycomm
      grid%comm      = MPI_COMM_NULL

      ! ... zero out for later
      grid%iGridGlobal = 0

      ! ... save parallelization topology
      grid%parallelTopology = input%parallelTopology

    end do

    ! ... save the original grid ID for this processor's grid
    ! ... save the core processor in control of this grid
    Loop1: Do i = 1, region%nGridsGlobal
      Loop2: Do ng = 1, region%nGrids
        grid => region%grid(ng)
        if ( region%myrank >= region%gridToProcMap(i,1) .and. &
             region%myrank <= region%gridToProcMap(i,2) .and. &
             grid%iGridGlobal == 0) then
          grid%iGridGlobal = i
          grid%coreRank_inComm = region%gridToProcMap(i,1)
          EXIT Loop2
        end if
      End Do Loop2
    End Do Loop1

    ! ... world group
    Call MPI_Comm_Group(mycomm, world_group, ierr)

    ! ... create the communicator for each grid
    Do i = 1, region%nGridsGlobal
      Ntotal = region%gridToProcMap(i,2) - region%gridToProcMap(i,1) + 1
      allocate( ranks(Ntotal) )
      do j = 1, Ntotal
        ranks(j) = region%gridToProcMap(i,1) + (j-1)
      end do
      Call MPI_Group_incl(world_group, Ntotal, ranks, grid_group, ierr)
      Call MPI_Comm_Create(mycomm, grid_group, comm, ierr)
      Do ng = 1, region%nGrids
        grid => region%grid(ng)
        if (grid%iGridGlobal == i) grid%comm = comm
      End Do
      deallocate(ranks)
      Call MPI_Group_Free(grid_group, ierr)
    End Do

    ! ... compute local rank and numproc for each grid comm
    Do ng = 1, region%nGrids
      grid => region%grid(ng)
      input => grid%input
      Call MPI_Comm_size( grid%comm, grid%numproc_inComm, ierr )
      Call MPI_Comm_rank( grid%comm, grid%myrank_inComm, ierr )
      grid%gridType = input%gridType
    End Do

    ! ... create a communicator for the core ranks only
    allocate( ranks(region%nGridsGlobal) )
    region%num_coreRank_this_processor = 0
    Do i = 1, region%nGridsGlobal
      ranks(i) = region%gridToProcMap(i,1)
      if (ranks(i) == region%myrank) &
        region%num_coreRank_this_processor = region%num_coreRank_this_processor + 1
    End Do
    Call i4vec_sorted_unique(region%nGridsGlobal, ranks, num_unique)
    call mpi_group_incl(world_group, num_unique, ranks, core_group, ierr)
    call mpi_comm_create(mycomm, core_group, comm, ierr)
    if ( any(ranks == region%myrank) .eqv. .true. ) then
      region%coreComm = comm
      Call MPI_Comm_size( region%coreComm, region%numproc_inCoreComm, ierr )
      Call MPI_Comm_rank( region%coreComm, region%myrank_inCoreComm, ierr )
    end if

    ! ... now assemble mapping from each grid to the MPI rank in coreComm
!    if (region%myrank == 0) write (*,'(A)') "Entering Grid_To_Processor_Core"
    allocate(region%grid_to_processor_coreComm(region%nGridsGlobal))
    If (region%num_coreRank_this_processor > 0) Then
      ! ... each MPI rank that is a core rank for a grid sends its global rank
      Call MPI_Allgather(region%myrank, 1, MPI_INTEGER, ranks, 1, MPI_INTEGER, region%coreComm, ierr)
      ! ... match that global rank against region%gridToProcMap(i,1)
      Do i = 1, region%nGridsGlobal
        Do j = 1, region%numproc_inCoreComm
          If (ranks(j) == grid%gridToProcMap(i,1)) &
            region%grid_to_processor_coreComm(i) = j-1
        End Do
      End Do
    End If

    ! ... now distribte this information
    Call MPI_Bcast(region%grid_to_processor_coreComm, region%nGridsGlobal, MPI_Integer, 0, mycomm, ierr)
!    if (region%myrank == 0) write (*,'(A)') "Exited Grid_To_Processor_Core"
   
    ! ... output some useful information
    If ( region%myrank == 0 ) Then
      Do I = 1, region%nGridsGlobal
        Write (*,'(A,I3,A,I8.8,A)') 'PlasComCM: ==> The core processor for grid ', i, ' is ', region%gridToProcMap(i,1), '. <=='
      End Do
    End If
!    if (region%myrank == 0) then
!      write (*,'(A)') "New region%grid_to_processor_coreComm"
!      Do i = 1, region%nGridsGlobal
!        write (*,'(A,1X,I2,1X,I2)') "New", i, region%grid_to_processor_coreComm(i)
!      End Do
!    end if

    ! ... perform any last minute modifications to the input here
    Call ModifyInput(region)

    local_decomp_error = .false.

    ! ... assign specific grid points
    Do ng = 1, region%nGrids

      ! ... work on grid ng of this processor
      grid  => region%grid(ng)
      input => grid%input

      ! ... initialize (is,ie) pairs
      grid%is(:) = 1 
      grid%ie(:) = 1

      ! ... periodicity
      grid%periodic(1:MAX_ND)  = input%periodic(1:MAX_ND)
      grid%periodicL(1:MAX_ND) = input%periodicL(1:MAX_ND)

      ! ... periodic storage
      grid%periodicStorage = input%periodicStorage
      if (grid%periodicStorage == NO_OVERLAP_PERIODIC) &
        grid%periodicStorageOffset = 0
      if (grid%periodicStorage ==    OVERLAP_PERIODIC) &
        grid%periodicStorageOffset = 1

      ! ... solver type for Pade operators
      grid%operator_implicit_solver(:) = input%operator_implicit_solver

      if (input%parallelTopology == SLAB) then

         ! ... which dir should we parallelize in?
         if (input%parDir == DEFAULT_VALUE) then
            If (grid%numproc_inComm > 1) Then
               parDir_loc = MAXLOC(region%AllGridsGlobalSize(grid%iGridGlobal,:))
               grid%parDir = parDir_loc(1)
            else
               grid%parDir = FALSE
            end if
         else
            If (grid%numproc_inComm > 1) then
               grid%parDir = input%parDir
            else
               grid%parDir = FALSE
            end if
         end if

        If ( region%myrank == grid%coreRank_inComm ) Then
          If ( grid%parDir /= FALSE ) write (*,'(A,I3,A,I1,A)') 'PlasComCM: ==> Parallelizing grid ', grid%iGridGlobal, ' in direction ', grid%parDir, '. <=='
        End If

        ! ... get local indices
        call assign_local_indices_cube(region%myrank, region%AllGridsGlobalSize, grid, input, ng)

      else

        call assign_local_indices_cube_box(region%myrank, region%AllGridsGlobalSize, grid, input, ng)

      end if

      IF((debug_on .eqv. .true.) .and. (region%myrank == 0)) write(*,'(A,i2)') 'PlasComCM: Initializing stencils for grid ', ng
      ! ... set up grid-specific derivative stencils
      Call initDerivStencils(region, input, grid%myrank_inComm)

      ! ... counting of operators
      grid%iFirstDeriv      = input%iFirstDeriv
      grid%iSecondDeriv     = input%iSecondDeriv
      grid%iFourthDeriv     = input%iFourthDeriv
      grid%iFilter          = input%iFilter
      grid%iFilterTest      = input%iFilterTest
      grid%iFilterGaussianHyperviscosity = input%iFilterGaussianHyperviscosity
      grid%iArtDiss         = input%iArtDiss
      grid%iSATArtDiss      = input%iSATArtDiss
      grid%iSATArtDissSplit = input%iSATArtDissSplit

      call mpi_barrier(grid%comm, ierr)
      IF((debug_on .eqv. .true.) .and. (grid%myRank_InComm == 0)) &
        write(*,'(A,i2)') 'PlasComCM: Initializing stencils done for grid ', grid%iGridGlobal

      ! ... store the unique index
      grid%is_unique(:) = 1
      grid%ie_unique(:) = 1
      do j = 1, grid%ND
        grid%is_unique(j) = grid%is(j)
        grid%ie_unique(j) = grid%ie(j)
      end do

      ! Check that decomposed index ranges are big enough
      if (any(grid%ie_unique(:grid%ND) - grid%is_unique(:grid%ND) + 1 < input%nOverlap)) then
        local_decomp_error = .true.
        exit
      end if

      ! ... allocate space for ghost cell information
      allocate(grid%nGhostRHS(MAX_ND,2))
      allocate(grid%nGhostLHS(MAX_ND,2))
      grid%nGhostRHS(:,:) = 0
      grid%nGhostLHS(:,:) = 0

      ! ... set ghost-cell information
      DO dir = 1, grid%ND
         grid%nGhostRHS(dir,:) = input%nOverLap
         if (grid%left_process(dir)  == MPI_PROC_NULL)  grid%nGhostRHS(dir,1) = 0
         if (grid%right_process(dir) == MPI_PROC_NULL)  grid%nGhostRHS(dir,2) = 0
      END DO

      ! .. update the index into box shape
      do j = 1, grid%ND
        grid%is(j) = grid%is(j) - grid%nGhostRHS(j,1)
        grid%ie(j) = grid%ie(j) + grid%nGhostRHS(j,2)
      end do

      ! ... count the number of cells in this grid
      grid%nCells = 1
      grid%nCells_unique = 1
      do j = 1, grid%ND
        grid%nCells = grid%nCells * (grid%ie(j)-grid%is(j)+1)
        grid%nCells_unique = grid%nCells_unique * (grid%ie_unique(j)-grid%is_unique(j)+1)
      end do

      ! ... count the vds and vdsGhost
      allocate(grid%vds(grid%ND))
      grid%vds(:) = 1

      do i = 1, grid%ND
        do j = 1, grid%ND
          If (j /= i) grid%vds(i) = grid%vds(i) * (grid%ie_unique(j)-grid%is_unique(j)+1)
        end do
      end do

      allocate(grid%vdsGhost(grid%ND))
      do i = 1, grid%ND
        grid%vdsGhost(i) = grid%vds(i)
      end do

      ! ... Finite volume
      if(input%fv_grid(ng) == FINITE_VOL) then
         allocate(grid%fv)

         ! ... fv_periodicity
         allocate(grid%fv%periodic(MAX_ND))
         
         ! ... simplicity
         fv => grid%fv

         allocate(grid%fv%is(MAX_ND), grid%fv%ie(MAX_ND)); 
         grid%fv%is(:) = 1; grid%fv%ie(:) = 1

         ! ... Number of finite volume cells
         allocate(grid%fv%Nc(MAX_ND))
         allocate(grid%fv%Nf(MAX_ND))

         N = 1; Nf = 1
         ! ... set ghost-cell information for finite volume grid
         ! ... let all sides of each FV grid have nOverlap ghost cells
         ! ... this is needed for parallel comm. and boundary conds.
         !if (input%fv_grid(ng) == FINITE_VOL) then
         allocate(grid%fv%nGhost(MAX_ND,2)); grid%fv%nGhost(:,:) = 0
         Do dir = 1, grid%ND
            ! ... in the future, maybe have different flux schemes
            !select case(input%fv_recon)
            ! case(HLLC)
            grid%fv%nGhost(dir,:) = 1
            if (grid%left_process(dir) == MPI_PROC_NULL) &
              grid%fv%nGhost(dir,1) = 0
            if (grid%right_process(dir) == MPI_PROC_NULL) &
              grid%fv%nGhost(dir,2) = 0
            ! end select
         End Do
        
         ! ... dual-cell discretization:
         do dir = 1, grid%ND
            N(dir) = grid%ie(dir)-grid%is(dir)+1

            ! ... number of faces in each direction
            ! ... is one less than number of grid points
            ! ... including ghost points
            fv%Nf(dir) = N(dir) + SUM(grid%fv%nGhost(dir,:)) - 1
         end do

         ! ... number of cells in each direction
         ! ... is two less than the number of grid points
         ! ... including ghost points, one less than the number of faces
         grid%fv%Nc = 1
         do i = 1, grid%ND
            grid%fv%Nc(i) = fv%Nf(i) - 1
         end do

         ! ... faces in each computational direction
         allocate(fv%face(grid%ND))
         do i = 1, grid%ND
            ! ... face area (length in 2D)
            allocate(fv%face(i)%area(fv%Nf(i)*product(fv%Nc)/fv%Nc(i)))
            ! ... face normal
            allocate(fv%face(i)%normal_vec((fv%Nf(i)*product(fv%Nc)/fv%Nc(i)),grid%ND))
            ! ... face tangent(s)
            allocate(fv%face(i)%tangent_vec((fv%Nf(i)*product(fv%Nc)/fv%Nc(i)),grid%ND,2))
         end do
         allocate(fv%vol(product(fv%Nc)))

         ! ... assign local finite volume indicises
         ! ... currently leaving the same to test algorithm
         ! ... later change to n-1 in each dir
         do j=1,grid%ND
            grid%fv%is(j)=grid%is(j)
            grid%fv%ie(j)=grid%ie(j)
         end do

         ! ... count the number of fv cell centers
         grid%fv%nCells = 1;
         do i = 1, grid%ND
            grid%fv%nCells = grid%fv%nCells * grid%fv%Nc(i)
         end do

      end if

      ! ... allocate space for grid points, metrics, ...
      allocate(grid%XYZ(grid%nCells,grid%ND))
      allocate(grid%IBLANK(grid%nCells))
      allocate(grid%ibfac(grid%nCells))
      nullify(grid%JAC)
      nullify(grid%INVJAC)
      nullify(grid%XI_TAU)
      nullify(grid%XYZ_TAU)
      nullify(grid%XYZ_TAU2)
      nullify(grid%XI_TAU_XI)
      nullify(grid%INVJAC_TAU)
      nullify(grid%MT1)
      nullify(grid%MT2)
      nullify(grid%ident)
      nullify(grid%area)
      nullify(grid%S)

      ! ... decide on metric type
      grid%metric_type = input%metricType
      IF(grid%myrank_incomm == 0) then
         if (grid%metric_type == CARTESIAN) &
              write (*,'(2(A,I8.8),A)') 'PlasComCM: Grid ', ng, ' on processor ', &
              region%myrank, ' is CARTESIAN.'
         if (grid%metric_type == NONORTHOGONAL_STRONGFORM) &
              write (*,'(2(A,I8.8),A)') 'PlasComCM: Grid ', ng, ' on processor ', &
              region%myrank, ' is NONORTHOGONAL_STRONGFORM.'
         if (grid%metric_type == NONORTHOGONAL_WEAKFORM) &
              write (*,'(2(A,I8.8),A)') 'PlasComCM: Grid ', ng, ' on processor ', &
              region%myrank, ' is NONORTHOGONAL_WEAKFORM.'
         if (grid%metric_type == STRONGFORM_NARROW) &
              write (*,'(2(A,I8.8),A)') 'PlasComCM: Grid ', ng, ' on processor ', &
              region%myrank, ' is STRONGFORM_NARROW.'
      ENDIF

      ! ... decide on advection type
      grid%advection_type = input%advectionType

      ! ... decide on derivative type
      allocate(grid%deriv_type(grid%ND))
      grid%deriv_type(:) = input%spaceDiscr

      ! ... decide on filter type
      allocate(grid%filter_type(grid%ND))
      grid%filter_type(:) = input%filterDiscr

      ! ... moving grids
      grid%moving = FALSE
      If (input%moveGrid == TRUE) grid%moving = TRUE

      ! ... counting of operators
      grid%iFirstDeriv      = input%iFirstDeriv
      grid%iFirstDerivBiased = input%iFirstDerivBiased
      grid%iSecondDeriv     = input%iSecondDeriv
      grid%iFourthDeriv     = input%iFourthDeriv
      grid%iFilter          = input%iFilter
      grid%iFilterTest      = input%iFilterTest
      grid%iFilterGaussianHyperviscosity = input%iFilterGaussianHyperviscosity
      grid%iArtDiss         = input%iArtDiss
      grid%iSATArtDiss      = input%iSATArtDiss
      grid%iSATArtDissSplit = input%iSATArtDissSplit

    End Do

    call MPI_Allreduce(local_decomp_error, decomp_error, 1, MPI_LOGICAL, MPI_LOR, mycomm, ierr)
    if (decomp_error) then
      call graceful_exit(region%myrank, "ERROR: Invalid decomposition. Number of points in each direction on a given process must be greater than or equal to nOverlap.")
    end if

    ! ... each MPI rank knows the global size of each grid
    Do ng = 1, region%nGrids
      grid => region%grid(ng)
      allocate(grid%GlobalSize(MAX_ND))
      grid%GlobalSize(:) = 1
      grid%GlobalSize(1:grid%ND) = &
        region%AllGridsGlobalSize(grid%iGridGlobal,1:grid%ND)
    End Do

    ! ... each MPI rank reports on the number of grid points 
    ! ... it owns, on each grid it owns
!    do i = 1, region%nGridsGlobal
!      do ng = 1, region%nGrids
!        grid => region%grid(ng)
!        if (grid%iGridGlobal == i) then
!          do p = 0, grid%numproc_inComm-1
!            if (grid%myrank_inComm == p) then
!              write (*,'(A,I8.8,A,I4,A,I8,A)') 'PlasComCM: MPI Rank ', &
!                region%myrank, ' on grid ', i, ' has ', grid%nCells, ' nodes'
!              call f90flush()
!            end if
!            call mpi_barrier(grid%comm, ierr)
!          end do
!        end if
!      end do
!      call mpi_barrier(mycomm, ierr)
!    end do

    IF(region%input%chemistry_model .NE. FALSE) THEN
       if (region%myrank == 0 .and. (debug_on .eqv. .true.)) write (*,'(A)') "PlasComCM: Initializing combustion module."
       region%chem_init_time = MPI_WTime()
       Do ng = 1, region%nGrids
          call combustionInit(region, ng)
       End Do
       call mpi_barrier(mycomm, ierr)
       region%chem_init_time = MPI_WTime() - region%chem_init_time
       if (region%myrank == 0 .and. (debug_on .eqv. .true.)) write (*,'(A)') "PlasComCM: Combustion module initialized."
    ENDIF

    ! ... read boundary information and set up the patches
    if (region%myrank == 0 .and. (debug_on .eqv. .true.)) write (*,'(A)') "PlasComCM: Setting up boundary patches."
    call Set_Up_Boundary_Patches(region)
    call mpi_barrier(mycomm, ierr)
    if (region%myrank == 0 .and. (debug_on .eqv. .true.)) write (*,'(A)') "PlasComCM: Boundary patch setup done."

    ! ... establish what grids each processor holds and collect this 
    ! ... information
    allocate(region%grid_to_processor(region%nGridsGlobal))
    do i = 1, region%nGridsGlobal      
      region%grid_to_processor(i) = region%gridToProcMap(i,1)
    end do

    ! ... check to see if overlap will cross two processor boundaries
    error_local = 0
    Do ng = 1, region%nGrids
      grid  => region%grid(ng)
      input => grid%input
      do i = 1, grid%ND
        if (input%noverlap >= 2*(grid%ie(i)-grid%is(i)+1)) error_local = error_local + 1
      end do
    end do
    Call MPI_Allreduce(error_local, error_global, 1, MPI_INTEGER, MPI_SUM, mycomm, ierr)
    if (error_global /= 0) then
      write (*,'(A)') 'PlasComCM: Input%nOverlap >= 2 x (grid size).'
      Call Output_Rank_As_IBLANK(region)
    end if

    ! ... by default, there is no control actuator in this region
    ! ... definition of sub-actuator found in t_sub_actuator in ModDataStruct.f90
    region%nSubActuators = 0

    ! ... by default, there is no control target points in this region
    ! ... definition of sub-control-target is similar to that of sub-actuator
    region%nSubCtrlTarget = 0

    input => region%input
    ! ... reset iteration count of conjugate gradient to be zero
    region%GLOBAL_ITER = 0
    region%ITMAX = input%AdjOptim_CGMaxIt
    IF((debug_on .eqv. .true.) .and. (region%myrank == 0)) write(*,*) 'PlasComCM: CG ITMAX = ',region%ITMAX

    ! ... by default, no forward solutions files (adjoint/adjoint-based optimization)
    region%numFwdSolnFiles = 0
    if (input%AdjNS .OR. input%AdjOptim) then
      if (input%cfl_mode == CONSTANT_CFL) then
        call graceful_exit(region%myrank, "... ERROR: use constant-DT time-stepping in adjoint N-S")
      else
        region%numFwdSolnFiles = NINT(REAL(input%nstepmax / input%noutput,rfreal)) + 1
      end if ! input%cfl_mode
    end if ! input%AdjNS

!!$ ! ... define control time horizon in terms of iteration and time
!!$ input => region%input
!!$ if (input%AdjOptim) then
!!$   region%ctrlStartIter = input%nstepi
!!$   region%ctrlEndIter   = input%nstepi + input%nstepmax
!!$
!!$   region%ctrlStartTime = region%state(1)%time(1)
!!$   region%ctrlEndTime   = region%state(1)%time(1) + REAL(input%nstepmax, rfreal)*input%dt
!!$ end if ! input%AdjOptim

!!$ call initialize_probe_data(region) ! ... JKim 12/2008

!!$ call Read_And_Interpolate_LST_Eigenmodes(region) ! ... JKim 12/2009

    ! ... flag to state whether we should include modified viscosity in timestep
    Do ng = 1, region%nGrids
      grid  => region%grid(ng)
      input => grid%input

      input%tvCor_in_dt = FALSE
      if (input%shock /= 0 .OR. input%LES /= 0) input%tvCor_in_dt = TRUE
    
      ! ... read and set up lookup table for thermally perfect gas if needed
      ! ... set up lookup table in variable input%TPTable
      if (input%gas_dv_model == DV_MODEL_THERMALLY_PERFECT) then
        call Read_DV_Table_Spline(region, ng)
        if(region%myrank == 0) write(*,'(2(A,f8.3),1X,A)')'PlasComCM: Free stream gamma = ',input%gamref,', @ temperature',input%TempRef, 'Kelvin'
      end if

      if (input%gas_tv_model == TV_MODEL_LOOKUP_TABLE) then
        call Read_TV_Table_Spline(region, ng)
      end if
    End Do

    region%nInteractingPatches = 0
    region%nIntegralSamples = 0
    region%nIntegralSurfaces = 0
    nullify(region%PowerInOut)
    nullify(region%IntegralTime)

#ifdef BUILD_ELECTRIC
    if (input%includeElectric) then
      call ElectricRegionSetup(region)
    end if
#endif

    ! ... allocate space for mpi timings
    Call Initialize_MPI_Timings(region)

  End Subroutine initialize_region


  subroutine Set_Up_Boundary_Patches(region)

    USE ModGlobal
    USE ModDataStruct
    USE ModMPI

    Implicit None

    Type (t_region), pointer :: region

    ! ... local variables
    Type (t_grid), pointer :: grid
    Type (t_mixt_input), pointer :: input
    Type (t_patch), pointer :: patch
    Integer :: ng, j, i, npatches, nbc, io_err, ND, N(MAX_ND), dir, ii, lp, l0
    Logical :: left_bc, right_bc, bc(MAX_ND)
    Integer, Parameter :: bc_unit = 51
    INTEGER :: signdir, bdir, patchColor, patchIndex, dummyComm
    Character(LEN=80) :: strbuf
    Integer :: tmp_bc_data(9), k, prodN, l, ierr
    Integer, pointer :: iPatchGlobal(:), gridID(:)

    ! ... simplicity
    input => region%input
    ND = input%ND

    ! ... step one : read the entire bc file
    Open (unit=bc_unit, file=trim(input%bc_fname), status='old', iostat=io_err)
    if (io_err /= 0) THEN
      call graceful_exit(region%myrank, 'ERROR: Could not find/open BC file "' // &
        trim(input%bc_fname) // '".')
    end if

    nbc = 0; io_err = FALSE
    Do while (io_err == FALSE)
      Read (bc_unit,'(A)',iostat=io_err) strbuf
      If ((io_err == FALSE) .AND. (strbuf(1:1) /= '#') .AND. (len_trim(strbuf) /= 0)) THEN
        Read (strbuf,*) tmp_bc_data(:)
        If (abs(tmp_bc_data(3)) <= ND) nbc = nbc + 1
      End If
    End Do
    Rewind(bc_unit)

    ! ... allocate master boundary condition array
    allocate(input%bc_data(nbc,9))

    ! ... reread
    nbc = 0; io_err = FALSE
    Do while (io_err == FALSE)
      Read (bc_unit,'(A)',iostat=io_err) strbuf
      If ((io_err == FALSE) .AND. (strbuf(1:1) /= '#') .AND. (len_trim(strbuf) /= 0)) THEN
        Read (strbuf,*) tmp_bc_data(:)
        If (abs(tmp_bc_data(3)) <= ND) THEN
          nbc = nbc + 1
          Read (strbuf,*) input%bc_data(nbc,:)
        End If
      End If
    End Do
    Close (bc_unit)

    If (region%myrank == 0) Write (*,'(A,I5,3(A))') &
         'PlasComCM: Found ', nbc, ' boundary conditions in "', trim(input%bc_fname), '"'

    allocate(iPatchGlobal(nbc), gridID(nbc))

    ! ... count how many patches we have to carry
    region%nPatches = 0; bc(:) = .FALSE.
    Do ng = 1, region%nGrids

      grid => region%grid(ng)
      ND = grid%ND

      Do i = 1, nbc

        ! ... if entries are negative, adjust
        Do j = 0, 2*ND-1, 2
          dir = j/2 + 1

          If (grid%iGridGlobal == input%bc_data(i,1)) THEN
            If (input%bc_data(i,4+j) < 0) &
                 input%bc_data(i,4+j) = grid%GlobalSize(dir) + input%bc_data(i,4+j) + 1

            If (input%bc_data(i,5+j) < 0) &
                 input%bc_data(i,5+j) = grid%GlobalSize(dir) + input%bc_data(i,5+j) + 1
          End If

        End Do

      End Do

      Do i = 1, nbc
         If (grid%iGridGlobal == input%bc_data(i,1)) THEN

          ! ... see if this grid contains the (*s,*e) indices
          Do j = 0, 2*ND-1, 2
            dir = j/2 + 1

            If ((input%bc_data(i,4+j) <= grid%ie(dir)) .and. &
                (input%bc_data(i,5+j) >= grid%is(dir))) THEN
              bc(dir) = .TRUE.
            End If

          End Do

          If (ALL(bc(1:ND))) THEN
            region%nPatches = region%nPatches + 1
            iPatchGlobal(region%nPatches) = i
            gridID(region%nPatches) = ng
          End If

          bc = .FALSE.

       End If
    End Do
 End Do


    call mpi_barrier(mycomm, ierr)

    ! ... allocate the memory for the patches
    allocate(region%patch(region%nPatches))

    ! ... save the patch data
    do i = 1, region%nPatches

      grid => region%grid(gridID(i))
      patch => region%patch(i)

      patch%gridID    = gridID(i) !input%bc_data(iPatchGlobal(i),1)
      patch%iPatchGlobal = iPatchGlobal(i)
      patch%bcCoupled = FALSE
      patch%bcType    = input%bc_data(iPatchGlobal(i),2)
      patch%normDir   = input%bc_data(iPatchGlobal(i),3)
      signdir = patch%normDir/abs(patch%normDir)
      bdir = patch%normDir
      If (patch%bctype == NSCBC_WALL_ISOTHERMAL_NOSLIP_COUPLED .OR. &
          patch%bctype == NSCBC_WALL_ADIABATIC_SLIP_COUPLED .OR. &
          patch%bctype == NSCBC_WALL_NOSLIP_THERMALLY_COUPLED .OR. & 
          patch%bctype == SAT_WALL_NOSLIP_THERMALLY_COUPLED .OR. &
          patch%bctype == SAT_SLIP_ADIABATIC_WALL_COUPLED .OR. &
          patch%bctype == NSCBC_WALL_NOSLIP_THERMALLY_COUPLED) THEN
          patch%flipflag = (1+signdir*(-1)**(bdir-1))/2
          !write(*,*)'FLIPFLAG'
          !write(*,*)patch%flipflag,patch%normDir
      END IF

      allocate(patch%is(MAX_ND)); patch%is = 1;
      allocate(patch%ie(MAX_ND)); patch%ie = 1;

      allocate(patch%bcData(size(input%bc_data,2)))
      patch%bcData(:) = input%bc_data(iPatchGlobal(i),:)

      nullify(patch%XYZOld)

      N(:) = 1
      prodN = 1



      Do j = 0, 2*ND-1, 2

        dir = j/2 + 1
!!$        if (dir /= abs(patch%normDir)) THEN
          patch%is(dir) = MIN(MAX(input%bc_data(iPatchGlobal(i),4+j),grid%is(dir)),grid%ie(dir))
          patch%ie(dir) = MAX(MIN(input%bc_data(iPatchGlobal(i),5+j),grid%ie(dir)),grid%is(dir))
!!$        Else
!!$          patch%is(dir) = input%bc_data(iPatchGlobal(i),4+j)
!!$          patch%ie(dir) = input%bc_data(iPatchGlobal(i),5+j)
!!$        End If

        if (patch%ie(dir) .lt. patch%is(dir)) THEN
          print *, iPatchGlobal(i)
          print *, grid%globalsize(1:2)
          print *, 'PlasComCM: error on myrank = ', region%myrank
          print *, region%myrank, patch%gridID, dir, input%bc_data(iPatchGlobal(i),4+j), grid%is(dir)
          print *, region%myrank, patch%gridID, dir, input%bc_data(iPatchGlobal(i),5+j), grid%ie(dir)
          stop
        end if

        N(dir) = patch%ie(dir) - patch%is(dir) + 1
        prodN = prodN * N(dir)

     End Do

      ! ... save prodN
      patch%prodN = prodN

      patch%normIndex = patch%ie(abs(patch%normDir))
      if (patch%normDir == abs(patch%normDir)) patch%normIndex = patch%is(abs(patch%normDir))

      allocate(patch%Deriv(grid%ND+2,prodN,grid%ND))
      do l = 1, grid%ND
        do k = 1, prodN
          do j = 1, grid%ND+2
            patch%Deriv(j,k,l) = 0.0_rfreal
          end do
        end do
      end do

      if (input%AdjNS .OR. input%AdjOptim) THEN
        allocate(patch%DerivAdj(grid%ND+2,prodN,grid%ND))
        do l = 1, grid%ND
          do k = 1, prodN
            do j = 1, grid%ND+2
              patch%DerivAdj(j,k,l) = 0.0_rfreal
            end do
          end do
        end do
      end if ! input%AdjNS

      ! ... used for storing viscous fluxes
      allocate(patch%ViscousFlux(grid%ND+2,prodN,grid%ND))
      allocate(patch%ViscousFluxAux(region%input%nAuxVars,prodN,grid%ND), patch%ViscousRHSAux(region%input%nAuxVars,prodN))
      patch%ViscousRHSAux = 0d0
      do l = 1, grid%ND
        do k = 1, prodN
          do j = 1, grid%ND+2
            patch%ViscousFlux(j,k,l) = 0.0_rfreal
          end do
          do j = 1, region%input%nAuxVars
             patch%ViscousFluxAux(j,k,l) = 0.0_rfreal
          end do
        enddo
      end do

      if(patch%bctype == NSCBC_WALL_NOSLIP_THERMALLY_COUPLED .OR. & 
          patch%bctype == SAT_WALL_NOSLIP_THERMALLY_COUPLED) THEN
         allocate(patch%TempGrad(prodN,grid%ND))
         do l = 1, grid%ND
            do k = 1, prodN
               patch%TempGrad(k,l) = 0.0_rfreal
            end do
         end do
      end if

      ! ... used for storing boundary data for block interfaces
      allocate(patch%cv_in(prodN,grid%ND+2))
      allocate(patch%cv_out(prodN,grid%ND+2))
      do k = 1, grid%ND+2
        do j = 1, prodN
          patch%cv_in(j,k) = 0.0_rfreal
          patch%cv_out(j,k) = 0.0_rfreal
        end do
      end do

      ! ... used for storing viscous fluxes for block interfaces
      allocate(patch%ViscousFlux_out(grid%ND+2,prodN,grid%ND))
      do l = 1, grid%ND
        do k = 1, prodN
          do j = 1, grid%ND+2
            patch%ViscousFlux_out(j,k,l) = 0.0_rfreal
          end do
        end do
      end do

      ! ... derivatives of metrics used for Poinsot-Lele boundary conditions
      allocate(patch%MT2(prodN,grid%ND**2))
      do k = 1, grid%ND**2
        do j = 1, prodN
          patch%MT2(j,k) = 0.0_rfreal
        end do
      end do

      ! ... implicit SAT terms
      nullify(patch%implicit_sat_mat)
      if ( input%timeScheme == BDF_GMRES_IMPLICIT ) THEN
        if ( (patch%bcType == SAT_FAR_FIELD) .OR. &
             (patch%bcType == SAT_PRESSURE) .OR. & !Luca1 09/2014
             (patch%bcType == SAT_NOSLIP_ISOTHERMAL_WALL) .OR. &
             (patch%bcType == SAT_SLIP_ADIABATIC_WALL) .OR. &
             (patch%bcType == SAT_NOSLIP_ADIABATIC_WALL) .OR. &
             (patch%bcType == SAT_BLOCK_INTERFACE) .OR. &
             (patch%bcType == SPONGE_WITH_LINEARIZED_DISTURBANCE) .OR. &
             (patch%bcType == SPONGE) ) THEN
          allocate(patch%implicit_sat_mat(prodN,grid%ND+2,grid%ND+2))
          allocate(patch%implicit_sat_mat_old(prodN,grid%ND+2,grid%ND+2))
        end if
      end if

      ! ... data for inflow specification
      If ( (patch%bcType == NSCBC_INFLOW_VELOCITY_TEMPERATURE_FROM_SPONGE) .OR. &
           (patch%bcType == NSCBC_INFLOW_VELOCITY_TEMPERATURE) .OR. &
           (patch%bcType == NSCBC_INFLOW_VELOCITY_TEMPERATURE_FORCING_SLOT ) ) THEN ! JKim 06/2008
        allocate(patch%u_inflow(prodN))
        allocate(patch%v_inflow(prodN))
        allocate(patch%w_inflow(prodN))
        allocate(patch%T_inflow(prodN))
        allocate(patch%dudt_inflow(prodN))
        allocate(patch%dvdt_inflow(prodN))
        allocate(patch%dwdt_inflow(prodN))
        allocate(patch%dTdt_inflow(prodN))
      End If

      ! ... This is messed up - why is there another section identical to 
      ! ... this one above?
      If (patch%bctype == NSCBC_WALL_ISOTHERMAL_NOSLIP_COUPLED .OR. &
           patch%bctype == NSCBC_WALL_ADIABATIC_SLIP_COUPLED   .OR. &
           patch%bctype == SAT_SLIP_ADIABATIC_WALL_COUPLED     .OR. &
           patch%bctype == SAT_WALL_NOSLIP_THERMALLY_COUPLED   .OR. &
           patch%bctype == SAT_BLOCK_INTERFACE                 .OR. &
           patch%bcType == FV_BLOCK_INTERFACE                  .OR. &
           patch%bctype == NSCBC_WALL_NOSLIP_THERMALLY_COUPLED .OR. &
           patch%bctype == STRUCTURAL_INTERACTING) THEN

         allocate(patch%patch_xyz(3*(prodN)))
         allocate(patch%local_patch_extent(6))
         allocate(patch%global_patch_extent(6))
         patch%local_patch_extent(1)=patch%is(1)
         patch%local_patch_extent(2)=patch%ie(1)
         patch%local_patch_extent(3)=patch%is(2)
         patch%local_patch_extent(4)=patch%ie(2)
         patch%local_patch_extent(5)=patch%is(3)
         patch%local_patch_extent(6)=patch%ie(3)

         Do j = 0, 2*ND-1, 2
            dir = j/2 + 1
            if (dir /= abs(patch%normDir)) THEN
               patch%global_patch_extent(2*dir-1) = input%bc_data(iPatchGlobal(i),4+j)
               patch%global_patch_extent(2*dir)   = input%bc_data(iPatchGlobal(i),5+j)
            Else
               patch%global_patch_extent(2*dir-1) = input%bc_data(iPatchGlobal(i),4+j)
               patch%global_patch_extent(2*dir)   = input%bc_data(iPatchGlobal(i),5+j)
            End If
         END DO

         IF(patch%bcType == SAT_BLOCK_INTERFACE .or. patch%bcType == FV_BLOCK_INTERFACE) THEN
            WRITE(*,*) 'Initializing SBI datasizes'
            ! set these extra initial values for the sat block interface
            patch%nlocal_collisions = 0
            patch%nremote_collisions = 0
            patch%nremote_send = 0
            patch%ncomp = 0
         ENDIF
      END IF

      IF (patch%bctype == NSCBC_WALL_NOSLIP_THERMALLY_COUPLED .OR. &
           patch%bctype == SAT_WALL_NOSLIP_THERMALLY_COUPLED) THEN
         allocate(patch%CellTemp(prodN))
         allocate(patch%HtFlux(prodN))
         ! ... initialize
         patch%HtFlux(:) = 0.0_rfreal
      END IF
      
      IF (patch%bctype == STRUCTURAL_INTERACTING) THEN
         allocate(patch%CellTemp(prodN),patch%dCellTemp(prodN))
         patch%CellTemp(:) = input%bcic_WallTemp * input%Cpref / input%SndSpdRef / input%SndSpdRef
         patch%dCellTemp(:) = 0.0_rfreal
         allocate(patch%dXYZ(prodN,ND*2),patch%dXYZ_TAU(prodN,ND*2))!,patch%dXYZ_TAU2(prodN,ND*2))
!         allocate(grid%XYZ_TAU(grid%nCells,3))
         !grid%XYZ_TAU(:,:) = 0.0_rfreal
         ! ... initialize
         Do k = patch%is(3), patch%ie(3)
            Do j = patch%is(2), patch%ie(2)
               Do ii = patch%is(1), patch%ie(1)
                  
                  l0 = (k-grid%is(3))*(grid%ie(1)-grid%is(1)+1)*(grid%ie(2)-grid%is(2)+1) + (j-grid%is(2))*(grid%ie(1)-grid%is(1)+1) + (ii-grid%is(1)+1)
                  
                  lp = (k-patch%is(3))*N(1)*N(2) + (j-patch%is(2))*N(1) + (ii-patch%is(1)+1)
                  !if(ND==2) patch%dXYZ(lp,2) = 0.1_rfreal*sin((ii-patch%is(1))*TWOPI/0.5_rfreal/(N(1)-1))
                  !if(ND==3) patch%dXYZ(lp,2) = 0.1_rfreal*sin((ii-patch%is(1))*TWOPI/2.0_rfreal/(N(1)-1))*sin((k-patch%is(3))*TWOPI/(max(N(3)-1,1)))
                  patch%dXYZ(lp,2) = 0.0_rfreal
                  patch%dXYZ_TAU(lp,2) = 0.0_rfreal
                  !grid%XYZ_TAU(l0,2) = 0.1_rfreal*sin((ii-patch%is(1))*TWOPI/2.0_rfreal/(N(1)-1))*sin((k-patch%is(3))*TWOPI/(max(N(3)-1,1)))
                  
               end do
            end Do
         end Do
            
      end IF

      
      ! ... allocate memory for implicit portion
      if (input%timeScheme == IMEX_RK4) THEN
        if (patch%bcType == SAT_NOSLIP_ISOTHERMAL_WALL) THEN
          allocate(patch%sat_fac(product(N)), patch%sat_fac_rk(product(N),grid%ND+2,1:6))
        end if
      end if

   end do

   if(.false.) THEN
    if (region%myrank == 0) write (*,'(A)') 'PlasComCM: ========= BC Info ========='
    call mpi_barrier(mycomm,ierr)
    do i = 0, numproc-1
      if (region%myrank == i) THEN
        write (*,'(A,I8.8)') 'PlasComCM: BC for processor ', region%myrank
        do j = 1, region%nPatches
          patch => region%patch(j)
          write (*,'(2(A,I8.8))') 'PlasComCM:   From grid ', patch%gridID, ' on patch ', j
          do k = 1, grid%ND
            write (*,'(A,2(I8.8,1X))') 'PlasComCM: ', patch%is(k), patch%ie(k)
          end do
        end do
      end if
      call mpi_barrier(mycomm,ierr)
    end do
    call mpi_barrier(mycomm,ierr)
    endif
    deallocate(iPatchGlobal, gridID)


    do j = 1,nbc
       patchColor = 0
       patchIndex = 0
       do i = 1, region%nPatches
          patch => region%patch(i)
          if(patch%iPatchGlobal == j) THEN
             patchColor = 1
             patchIndex = i
          endif
       enddo
       if(patchColor == 1) THEN
          patch => region%patch(patchIndex)
          CALL MPI_Comm_split(mycomm,patchColor,region%myrank,patch%comm,ierr)
       else
          CALL MPI_Comm_split(mycomm,patchColor,region%myrank,dummyComm,ierr)
       endif
   end do


    if (region%myrank == 0) write (*,'(A)') 'PlasComCM: ========= END BC Info ========='

    Return

  End Subroutine Set_Up_Boundary_Patches



  subroutine i4vec_sorted_unique ( n, a, nuniq )

    !*****************************************************************************80
    !
    !! I4VEC_SORTED_UNIQUE finds the number of unique elements in a sorted I4VEC.
    !
    !  Modified:
    !
    !    09 July 2000
    !
    !  Author:
    !
    !    John Burkardt
    !
    !  Parameters:
    !
    !    Input, integer N, the number of elements in A.
    !
    !    Input/output, integer A(N).  On input, the sorted
    !    integer array.  On output, the unique elements in A.
    !
    !    Output, integer NUNIQ, the number of unique elements in A.
    !
    implicit none

    integer n

    integer a(n)
    integer itest
    integer nuniq

    nuniq = 0

    if ( n <= 0 ) then
      return
    end if

    nuniq = 1

    do itest = 2, n

      if ( a(itest) /= a(nuniq) ) then
        nuniq = nuniq + 1
        a(nuniq) = a(itest)
      end if

    end do

    return

  end subroutine i4vec_sorted_unique

  subroutine Load_Balance(myrank, nproc, numGridsInFile, ND, gridToProcMap, input)

    USE ModGlobal
    USE ModDataStruct
    USE ModMPI

    Implicit None

    ! ... function values
    Integer :: myrank, nproc, numGridsInFile
    Integer, Pointer :: ND(:,:), gridToProcMap(:,:)
    Type (t_mixt_input), pointer :: input

    ! ... local values
    integer :: p(numGridsInFile), sumN, i, sumP
    integer :: indGrid, numDecomp(MAX_ND), io_err
    logical :: mf_exists

    ! ... get initial mapping

    ! ... add up all of the points
    sumN = 0
    do i = 1, numGridsInFile
      sumN = sumN + product(ND(i,:))
    end do

    ! ... take the simple least squares approach, and round
    do i = 1, numGridsInFile
       p(i) = ceiling(dble(product(ND(i,:)))*dble(nproc)/dble(sumN))
    end do

    ! ... check to see if we match the constant
    i = 1
    do while (sum(p(:)) /= nproc)
      if (p(i) /= 1) p(i) = p(i) - 1
      i = i + 1
      if (i == numGridsInFile+1) i = 1 ! start over in case more iterations are needed (D. Buctha)
    end do

    ! ... update map based on data provided by user
    mf_exists = .FALSE.
    if (input%useDecompMap == TRUE) then
      inquire(file=trim(input%decompMap), exist=mf_exists)
      if (.NOT. mf_exists) call graceful_exit(myrank, "ERROR: domain-decomposition info cannot be found.")
    end if ! input%useDecompMap
    if (mf_exists .and. (input%parallelTopology == CUBE .and. nproc > 1)) then

      if (myrank == 0) write (*,'(A)') 'PlasComCM: Using user-supplied processor map "'//trim(input%decompMap)//'".'

      ! ... read domain-decomposition map
      ! ... if error occurs while reading, we do nothing, haha! -_-a
      open(66, file=trim(input%decompMap)); io_err = 0
      Do while (io_err == 0) 
        read(66,*,IOSTAT=io_err) indGrid, numDecomp(1:MAX_ND)
        p(indGrid) = PRODUCT(numDecomp(:))
      end do ! i
      close(66)

      if (SUM(p(:)) /= nproc) &
           call graceful_exit(myrank, "... total number of decomposition provided by " &
           //trim(input%decompMap)//" different from numproc.")

    End if


    ! ... fill up the gridToProcMap array
    gridToProcMap(1,1) = 0
    gridToProcmap(1,2) = p(1)-1
    do i = 2, numGridsInFile
      gridToProcMap(i,1) = gridToProcMap(i-1,2) + 1
      gridToProcMap(i,2) = gridToProcMap(i,1) + (p(i) - 1)
    end do

    return

  end subroutine Load_Balance

  Subroutine DecomposeDomain(region)

    USE ModGlobal
    USE ModDataStruct
    USE ModMPI
    USE ModGeometry
    USE ModIO

    Implicit None

    Type (t_region), pointer :: region

    ! ... local variables
    integer :: NDIM, ngrid, parDir_loc(1), ps, pe, i, j, ng, num_unique, p, ierr,output_flag
    integer :: numGridsInFile, gs, ge, world_group, grid_group, Ntotal, color, comm, core_group, alpha, num_recv, error_local, error_global
    integer, pointer :: ND(:,:), ibuf(:)
    integer, allocatable :: ranks(:)
    Type (t_mixt_input), pointer :: input
    Type (t_grid), pointer :: grid
    Logical :: left_bc, right_bc

    IF(region%input%generateGrid == TRUE) THEN
       IF(region%myrank == 0) THEN
          WRITE(*,*) 'PlasComCM: Getting generated grid size'
          CALL GetGridSizes(region,NDIM,numGridsInFile,ND)
       ENDIF
       CALL MPI_BCast(NDIM,1,MPI_INTEGER,0,mycomm,ierr)
       CALL MPI_BCast(numGridsInFile,1,MPI_INTEGER,0,mycomm,ierr)
       IF(region%myrank .NE. 0) THEN
          ALLOCATE(ND(numGridsInFile,3))
       ENDIF
       CALL MPI_BCast(ND(:,:),numGridsInFile*3,MPI_INTEGER,0,mycomm,ierr)
    ELSE
       output_flag = 0
       IF(region%myrank == 0) output_flag = 1
       ! ... read the grid file to see how many grids it has
       CALL ReadGridSize(NDIM,numGridsInFile,ND,region%input%grid_fname,output_flag)
    ENDIF

    region%nGridsGlobal = numGridsInFile

    ! ... total size of each grid
    allocate(region%AllGridsGlobalSize(size(ND,1),MAX(3,size(ND,2)))); 
    region%AllGridsGlobalSize(:,:) = 1;
    region%AllGridsGlobalSize(1:size(ND,1),1:size(ND,2)) = ND(:,:)

    ! ... compute the number of processors each grid will get
    allocate(region%gridToProcMap(numGridsInFile,2));

    ! ... allocate grid and some of its arrays
    If (numGridsInFile >= numproc) Then

      ! ... in this case there are at least as many grids as processors
      ! ... each grid will have its own communicator, but that should 
      ! ... never be used
      Do i = 0, numproc-1
        Call MPE_DECOMP1D(numGridsInFile, numproc, i, gs, ge )
        region%gridToProcMap(gs:ge,:) = i
        if ( region%myrank == i ) region%nGrids = ge - gs + 1
      End Do

    Else

      Call Load_Balance(region%myrank, numproc, numGridsInFile, ND, &
                        region%gridToProcMap, region%input)

      ! ... search through gridToProcMap to find this processor's data
      region%nGrids = 0
      Do i = 1, numGridsInFile
        if ( region%myrank >= region%gridToProcMap(i,1) .and. &
             region%myrank <= region%gridToProcMap(i,2) ) &
             region%nGrids = region%nGrids + 1
      End Do

    End If

    ! ... report how many grids each MPI rank owns
    If ( region%myrank == 0 .and. .false.) Then
      Do p = 0, numproc-1
        j = 0        
        Do i = 1, numGridsInFile
          if ( p >= region%gridToProcMap(i,1) .and. &
               p <= region%gridToProcMap(i,2) ) j = j + 1
        End Do
        write (*,'(A,I8.8,A,I3,A)') &
          'PlasComCM: Processor ', p, ' has ', j, ' grids.'
      End Do
    End If
    Call MPI_Barrier(mycomm, ierr)

!    Do I = 0, numproc-1
!      if ( region%myrank == i ) write (*,'(A,I8.8,A,I3,A)') &
!        '<<< PlasComCM: Processor ', region%myrank, ' has ', region%nGrids, &
!        ' grids.'
!      Call MPI_Barrier(mycomm, ierr)
!    End Do

    if ( region%myrank == 0 ) then
      write (*,'(A)') ''
      write (*,'(A)') 'PlasComCM: --------------------------------------------------------------------'
    end if

    Do i = 0, numGridsInFile-1
      if (region%myrank == 0) write (*,'(3(A,I8.8),A)') 'PlasComCM: ==> Grid ', i+1, ' to be distributed to processors ', region%gridToProcMap(i+1,1), ' through ', region%gridToProcMap(i+1,2), '. <=='
    End Do

    if ( region%myrank == 0 ) then
      write (*,'(A)') 'PlasComCM: --------------------------------------------------------------------'
      write (*,'(A)') ''
    end if

    Deallocate(ND)

  End Subroutine DecomposeDomain

  Subroutine ModifyInput(region)

    USE ModGlobal
    USE ModDataStruct
    USE ModMPI
    USE ModIO

    Implicit None

    ! ... inputs 
    Type (t_region), pointer :: region

    ! ... locals
    Type (t_mixt_input), pointer :: input
    Type (t_grid), pointer :: grid
    Integer :: ng
 
    ! ... perform grid-specific modifications here to the *input* structure
    Do ng = 1, region%nGrids

      grid  => region%grid(ng)
      input => grid%input

      select case (input%caseID)
      case ('JETXFLOW')
         if(region%myrank == 0) write (*,'(A)') 'PlasComCM: >>>>>>>>>>>>>>> Setting up JETXFLOW <<<<<<<<<<<<<<'
         input%periodic(:) = FALSE
         if (grid%iGridGlobal == 1) then
            input%periodic(3)  = PLANE_PERIODIC
            input%periodicL(3) = input%GRID1_LZ
            input%periodicStorage = NO_OVERLAP_PERIODIC
         end if
         if (grid%iGridGlobal == 2) then
            input%periodic(3)  = PERIODIC
            input%periodicStorage = OVERLAP_PERIODIC
         end if
         if (grid%iGridGlobal == 4) then
            input%periodic(3)  = PERIODIC
            input%periodicStorage = OVERLAP_PERIODIC
         end if
      case ('FLUIDTESTS_CNS_2D_MULTIPLE')
        if(region%myrank == 0) write (*,'(A)') 'PlasComCM: >>>>>>>>>>>>>>> Setting up CNS2DMultiple Test <<<<<<<<<<<<<<'
        select case (grid%iGridGlobal)
        case (1)
          input%periodic = FALSE
          input%periodic(1) = PLANE_PERIODIC
          input%periodicStorage = OVERLAP_PERIODIC
        case (2)
          input%periodic = FALSE
        end select
      case ('FLUIDTESTS_CNS_3D_MULTIPLE')
        if(region%myrank == 0) write (*,'(A)') 'PlasComCM: >>>>>>>>>>>>>>> Setting up for CNS3DMultiple Test <<<<<<<<<<<<<<'
        select case (grid%iGridGlobal)
        case (1)
          input%periodic = FALSE
          input%periodic(1:2) = PLANE_PERIODIC
          input%periodicStorage = OVERLAP_PERIODIC
        case (2)
          input%periodic = FALSE
        end select
      case ('EX_TWO_GRID_BL_2D')
        if(region%myrank == 0) write (*,'(A)') 'PlasComCM: >>>>>>>>>>>>>>> Setup EXTWOGRIDBL2D <<<<<<<<<<<<<<'
        select case (grid%iGridGlobal)
        case (1)
          input%periodic = FALSE
          input%periodic(1) = PLANE_PERIODIC
          input%periodicStorage = OVERLAP_PERIODIC
        case (2)
          input%periodic = FALSE
        end select
      case ('EX_TWO_GRID_BL_3D')
        if(region%myrank == 0) write (*,'(A)') 'PlasComCM: >>>>>>>>>>>>>>> Setup EXTWOGRIDBL3D <<<<<<<<<<<<<<'
        select case (grid%iGridGlobal)
        case (1)
          input%periodic = FALSE
          input%periodic(:2) = PLANE_PERIODIC
          input%periodicStorage = OVERLAP_PERIODIC
        case (2)
          input%periodic = FALSE
        end select
      case ('EX_FLOW_PAST_CYL')
        if(region%myrank == 0) write (*,'(A)') 'PlasComCM: >>>>>>>>>>>>>>> Setup EXFLOWPASTCYL <<<<<<<<<<<<<<'
        select case (grid%iGridGlobal)
        case (1)
          input%periodic = FALSE
        case (2)
          input%periodic = FALSE
          input%periodic(1) = PERIODIC
          input%periodicStorage = OVERLAP_PERIODIC
        end select
      case ('EX_THREE_GRID_WAVE')
        if(region%myrank == 0) write (*,'(A)') 'PlasComCM: >>>>>>>>>>>>>>> Setup EXTHREEGRIDWAVE <<<<<<<<<<<<<<'
        select case (grid%iGridGlobal)
        case (1)
          input%periodic = FALSE
        case (2)
          input%periodic = FALSE
          input%periodic(2) = PERIODIC
          input%periodicStorage = OVERLAP_PERIODIC
        case (3)
          input%periodic = FALSE
        end select
!       case ('OS_SANDBOX')
!         write (*,'(A)') 'PlasComCM: >>>>>>>>>>>>>>> Modifying grid periodicity <<<<<<<<<<<<<<'
!         select case (grid%iGridGlobal)
!         case (1)
!           input%periodic = FALSE
!           input%periodic(1) = PLANE_PERIODIC
!           input%periodicStorage = OVERLAP_PERIODIC
!         case (2)
!           input%periodic = FALSE
!         end select
      end select

!      write (*,'(A)') 'PlasComCM: >>>>>>>>>>>>>>> Modifying finite difference order <<<<<<<<<<<<<<'
!      if (grid%iGridGlobal == 1) then
!        input%spaceOrder = 5
!      end if

    End Do

  End Subroutine ModifyInput
  
  Subroutine Read_TPGas_Table(region)
    USE ModGlobal
    USE ModEOS
    USE ModDataStruct
    USE ModMPI

    Implicit None

    TYPE(t_region), pointer :: region

    ! ... local
    TYPE(t_mixt_input), pointer :: input
    TYPE(t_grid), pointer :: grid
    real(rfreal) :: TempRef, SndSpd2, GamRef, Deta, GasConst
    real(rfreal), pointer :: TPTable(:,:,:)
    integer :: j, i, ndata, k, ng
    character(len=80) :: fname

    ! ... for calculation of polynomial coefficients
    real(rfreal), dimension(2,2) :: a,b
    real(rfreal), dimension(2) :: c,d

    Do ng = 1, region%nGrids

      ! ... simplicity
      grid => region%grid(ng)
      input => grid%input
      TempRef = input%TempRef
      GamRef = input%GamRef
      SndSpd2 = input%SndSpdRef*input%SndSpdRef
      fname = input%TPTableName

      ! ... need to read in thermally perfect lookup table
      Open(unit=100, file=trim(fname), form='unformatted', status='old')

      ! ... number of data points in table
      Read(100) ndata
      input%nTPTableEntries = ndata

      ! ... gas constant
      Read(100) GasConst

      !  print*,'ndata,GasConst',ndata,GasConst

      ! ... allocate
      Allocate(input%TPTable(ndata,3,2))
      TPTable => input%TPTable

      ! ... read the equally spaced energy table, Ti = T(ei)
      ! ... read the energy column
      Read(100) (TPTable(i,1,1),i=1,ndata)

      ! ... read the temperature column
      Read(100) (TPTable(i,2,1),i=1,ndata)

      ! ... read the ratio of specific heats column
      Read(100) (TPTable(i,3,1),i=1,ndata)

      ! ... read the equally spaced temperature table, ei = e(Ti)
      ! ... read the energy column
      Read(100) (TPTable(i,1,2),i=1,ndata)

      ! ... read the temperature column
      Read(100) (TPTable(i,2,2),i=1,ndata)

      ! ... read the ratio of specific heats column
      Read(100) (TPTable(i,3,2),i=1,ndata)

      ! ... close the file
      Close (100)

      ! ... now non-dimensionalize both tables
      do i = 1, ndata
        do j = 1, 2
          ! ... energy
          TPTable(i,1,j) = TPTable(i,1,j) / SndSpd2

          ! ... temperature
          TPTable(i,2,j) = TPTable(i,2,j) / ((GamRef - 1.0_rfreal) * TempRef)
        end do
      end do

      ! ... now get coefficient values for linear fit
      !        _  _
      !       |e1 1|
      !   a = |e2 1|
      !       -    -

      allocate(input%TPCoeffs(2,2))  
      input%TPCoeffs(:,:) = 0.0_rfreal 
      ! ... do both tables
      do k = 1, 2
        ! ... first point
        i = 1
        d(1) = dble(i)
        a(1,1) = TPTable(i,k,k)
        a(1,2) = 1.0_rfreal

        ! ... last point
        i = ndata
        d(2) = dble(i)
        a(2,1) = TPTable(i,k,k)
        a(2,2) = 1.0_rfreal

        ! ... now the adjugate of a

        b(1,1) =  a(2,2)
        b(1,2) = -a(1,2)
        b(2,1) = -a(2,1) 
        b(2,2) =  a(1,1)

        ! ... determinant of a
        Deta = a(1,1) * a(2,2) - a(1,2) * a(2,1)


        ! ... linear coefficients
        ! ... TPCoeffs(:,1): line for index(internal_energy)
        ! ... TPCoeffs(:,2): line for index(Temperature)



        c(:) = 0.0_rfreal
        do i = 1,2
          do j = 1,2
            c(i) = c(i) + b(i,j) * d(j)
          end do
          input%TPCoeffs(i,k) = c(i) / Deta
        end do

      End Do

    end do

  end Subroutine Read_TPGas_Table

subroutine Initialize_MPI_Timings(region)

    USE ModGlobal
    USE ModDataStruct

    ! ... incoming
    Type (t_region), Pointer :: region

    ! ... local
    Integer :: i

    ! ... allocate
    allocate(region%mpi_timings(region%nGrids))
    do i = 1, region%nGrids
      allocate(region%mpi_timings(i)%operator_setup(MAX_OPERATORS,MAX_ND))
      allocate(region%mpi_timings(i)%operator(MAX_OPERATORS,MAX_ND))
      allocate(region%mpi_timings(i)%operator_solve(MAX_OPERATORS,MAX_ND))
      allocate(region%mpi_timings(i)%operator_interior(MAX_OPERATORS,MAX_ND))
      allocate(region%mpi_timings(i)%operator_boundary(MAX_OPERATORS,MAX_ND))
      allocate(region%mpi_timings(i)%operator_ghostExchange(MAX_OPERATORS,MAX_ND))
    end do

    ! ... initialize
    do i = 1, region%nGrids
      region%mpi_timings(i)%metrics                 = 0.0_RFREAL
      region%mpi_timings(i)%io_total                = 0.0_RFREAL
      region%mpi_timings(i)%rhs_total               = 0.0_RFREAL
      region%mpi_timings(i)%rhs_inviscid            = 0.0_RFREAL
      region%mpi_timings(i)%rhs_viscous             = 0.0_RFREAL
      region%mpi_timings(i)%rhs_shock               = 0.0_RFREAL
      region%mpi_timings(i)%rhs_les                 = 0.0_RFREAL
      region%mpi_timings(i)%rhs_artdiss             = 0.0_RFREAL
      region%mpi_timings(i)%rhs_TGrad               = 0.0_RFREAL
      region%mpi_timings(i)%rhs_ScalarGrad          = 0.0_RFREAL
      region%mpi_timings(i)%rhs_VGrad               = 0.0_RFREAL
      region%mpi_timings(i)%rhs_EOS                 = 0.0_RFREAL
      region%mpi_timings(i)%interp                  = 0.0_RFREAL
      region%mpi_timings(i)%interp_setup            = 0.0_RFREAL
      region%mpi_timings(i)%interp_volx             = 0.0_RFREAL
      region%mpi_timings(i)%interp_nbix             = 0.0_RFREAL
      region%mpi_timings(i)%rhs_bc                  = 0.0_RFREAL
      region%mpi_timings(i)%rhs_bc_fix_value        = 0.0_RFREAL
      region%mpi_timings(i)%operator_solve(:,:)     = 0.0_RFREAL
      region%mpi_timings(i)%operator_setup(:,:)     = 0.0_RFREAL
      region%mpi_timings(i)%operator_interior(:,:)  = 0.0_RFREAL
      region%mpi_timings(i)%operator_boundary(:,:)      = 0.0_RFREAL
      region%mpi_timings(i)%operator_ghostExchange(:,:) = 0.0_RFREAL
      region%mpi_timings(i)%operator(:,:)               = 0.0_RFREAL
      region%mpi_timings(i)%io_read_restart        = 0.0_RFREAL
      region%mpi_timings(i)%io_write_restart       = 0.0_RFREAL
      region%mpi_timings(i)%io_read_grid           = 0.0_RFREAL
      region%mpi_timings(i)%io_read_sponge         = 0.0_RFREAL
      region%mpi_timings(i)%io_read_soln           = 0.0_RFREAL
      region%mpi_timings(i)%io_read_mean           = 0.0_RFREAL
      region%mpi_timings(i)%io_read_target         = 0.0_RFREAL
      region%mpi_timings(i)%io_write_grid          = 0.0_RFREAL
      region%mpi_timings(i)%io_write_soln          = 0.0_RFREAL
      region%mpi_timings(i)%io_read_sponge         = 0.0_RFREAL
      region%mpi_timings(i)%io_read_soln           = 0.0_RFREAL
      region%mpi_timings(i)%io_read_mean           = 0.0_RFREAL
      region%mpi_timings(i)%io_read_target         = 0.0_RFREAL
      region%mpi_timings(i)%io_setup               = 0.0_RFREAL
      
      region%mpi_timings(i)%TM_lhs_thermal         = 0.0_RFREAL
      region%mpi_timings(i)%TM_lhs_structural      = 0.0_RFREAL
      region%mpi_timings(i)%TM_rhs_thermal         = 0.0_RFREAL
      region%mpi_timings(i)%TM_rhs_structural      = 0.0_RFREAL
      region%mpi_timings(i)%TM_solve_thermal       = 0.0_RFREAL
      region%mpi_timings(i)%TM_solve_structural    = 0.0_RFREAL

      region%mpi_timings(i)%efield_solve           = 0.0_RFREAL
      region%mpi_timings(i)%efield_processing      = 0.0_RFREAL
    end do

    region%init_grids_time = 0.0_RFREAL
    region%init_state_time = 0.0_RFREAL
    region%run_time = 0.0_RFREAL
    region%rk_time = 0.0_RFREAL
    region%mainloop_time = 0.0_RFREAL
    region%chem_init_time = 0.0_RFREAL

  End subroutine Initialize_MPI_Timings

  Subroutine ReduceTimingData(timingData,metricData,mpiComm)

    USE ModGlobal
    USE ModMPI
    
    IMPLICIT NONE

    Real(RFREAL) :: timingData,metricData(4)
    Integer :: mpiComm

    Real(RFREAL) :: tempFac, timingData2
    Integer :: numprocs, iError, j
    
    metricData(:) = 0.0_RFREAL


    ! Only care about timings to the usec
    if(timingData < 1.0d-6) timingData = 0.0_RFREAL


    timingData2 = timingData*timingData

    Call MPI_Comm_size(mpiComm,numProcs,iError)

    tempFac = 1.0/REAL(numProcs)


    Call MPI_Reduce(timingData,metricData(1),1,MPI_DOUBLE_PRECISION,MPI_MIN,0,mpiComm,iError)
    Call MPI_Reduce(timingData,metricData(2),1,MPI_DOUBLE_PRECISION,MPI_MAX,0,mpiComm,iError)
    Call MPI_Reduce(timingData,metricData(3),1,MPI_DOUBLE_PRECISION,MPI_SUM,0,mpiComm,iError)
    Call MPI_Reduce(timingData2,metricData(4),1,MPI_DOUBLE_PRECISION,MPI_SUM,0,mpiComm,iError)

    metricData(3) = tempFac*metricData(3)

    timingData2 = tempFac*metricData(4) - metricData(3)*metricData(3)

    if(timingData2 > 1.0d-12) then
       metricData(4)   = SQRT(timingData2)
    else
       metricData(4) = 0.0_RFREAL
    endif

  End Subroutine ReduceTimingData
  
  Subroutine Write_MPI_Timings(region)
    

    USE ModGlobal
    USE ModDataStruct
    USE ModMPI
    
    IMPLICIT NONE 

    ! ... incoming
    Type (t_region), Pointer :: region
    
    ! ... local
    Integer :: ng, p, i, j, np, jj,ierr
    Type(t_grid), pointer :: grid
    Type(t_mpi_timings), pointer :: mpi_timings
    Real(RFREAL) :: io_total, stepFac, gridFac
    Real(RFREAL), ALLOCATABLE :: tmpData(:,:),metricData(:,:),specificStepTime(:,:)
    INTEGER, ALLOCATABLE :: mpiRequest(:),requestStatii(:,:),nPoints(:),nProcGrid(:)
    CHARACTER(len=32) :: timingFileName,gpFormat,gtFormat

    ALLOCATE(metricData(4,12))
    ALLOCATE(tmpData(4,12))
    ALLOCATE(mpiRequest(12))
    ALLOCATE(requestStatii(MPI_STATUS_SIZE,12))
    ALLOCATE(nPoints(region%nGridsGlobal))
    ALLOCATE(nProcGrid(region%nGridsGlobal))
    ALLOCATE(specificStepTime(4,region%nGridsGlobal))
    
    IF(region%myrank == 0) THEN
       WRITE(timingFileName,'(A16,I7.7,A4)') 'PlasComCMTiming_',numproc,'.txt'
       OPEN(unit=703,file=TRIM(timingFileName),form='formatted',status='replace')
       DO p = 1, region%nGridsGlobal
          nPoints(p) = PRODUCT(region%AllGridsGlobalSize(p,:))
       END DO
       WRITE(gpFormat,'("(A22,(",I0,"(I11,1X)))")')   region%nGridsGlobal
       WRITE(gtFormat,'("(A22,(",I0,"(E12.4,1X)))")') region%nGridsGlobal
       DO p = 1, region%nGridsGlobal
          nProcGrid(p) = region%gridToProcMap(p,2) - region%gridToProcMap(p,1) + 1
       END DO
    ENDIF

    metricData(:,:) = 0.0_RFREAL
    
    ! Get grid-specific step time = time/timestep/gridpoint
    DO p = 1,region%nGridsGlobal
       if(region%myrank == 0) then
          CALL MPI_IRecv(metricData(1:4,p),4,MPI_DOUBLE_PRECISION,MPI_ANY_SOURCE,p,mycomm,mpiRequest(p),ierr)
       endif
       Do ng = 1, region%nGrids
          grid => region%grid(ng)
          mpi_timings => region%mpi_timings(ng)
          if (grid%iGridGlobal == p) then
             Call ReduceTimingData(region%rk_time,tmpData(1:4,p),grid%comm)
             if (grid%myrank_inComm == 0) then
                CALL MPI_Send(tmpData(1:4,p),4,MPI_DOUBLE_PRECISION,0,p,mycomm,ierr)      
             endif
          endif
       END DO
    END DO

    IF(region%myrank == 0) THEN
       stepFac = 1.0/REAL(max(region%nsteps_this_run,1))
       CALL MPI_Waitall(region%nGridsGlobal,mpiRequest,requestStatii,ierr)
       DO p = 1, region%nGridsGlobal
          gridFac = 1.0/REAL(nPoints(p))
          DO j = 1,4
             specificStepTime(j,p) = stepFac*gridFac*metricData(j,p)
          END DO
       END DO
    ENDIF


    CALL ReduceTimingData(region%total_time,tmpData(1,1),mycomm)
    CALL ReduceTimingData(region%init_grids_time,tmpData(1,2),mycomm)
    CALL ReduceTimingData(region%init_state_time,tmpData(1,3),mycomm)
    CALL ReduceTimingData(region%run_time,tmpData(1,4),mycomm)
    CALL ReduceTimingData(region%rk_time,tmpData(1,5),mycomm)
    CALL ReduceTimingData(region%init_time,tmpData(1,6),mycomm)
    CALL ReduceTimingData(region%mainloop_time,tmpData(1,7),mycomm)
    CALL ReduceTimingData(region%chem_init_time,tmpData(1,9),mycomm)

    IF(region%myrank == 0) THEN
       gridFac = 1.0/REAL(SUM(nPoints(:)))
       write (703,'(A)') '# PlasComCM Performance Timing'
       write (703,'(A22,I7)') '# Number of procs:    ', numproc
       write (703,'(A22,I7)') '# Number of steps:    ', region%nsteps_this_run
       write (703,'(A22,I7)') '# Number of grids:    ', region%nGridsGlobal
       write (703,gpFormat) '# Number of points:   ', (nPoints(j),j=1,region%nGridsGlobal)
       write (703,gpFormat) '# Procs per grid:     ', (nProcGrid(j),j=1,region%nGridsGlobal)
       write (703,gtFormat) '# Specific step time: ', (specificStepTime(2,j),j=1,region%nGridsGlobal)
       write (703,'(A)') '# ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++'
       write (703,'(A)') '# Name                          MIN          MAX         MEAN'
       write (703,'(A)') '# --------------------------------------------------------------'
       write (703,'(A26,3E12.4)') 'PlasComCM                 ',(tmpData(j,1),j=1,3)
       write (703,'(A26,3E12.4)') 'InitGrids                 ',(tmpData(j,2),j=1,3)
       write (703,'(A26,3E12.4)') 'InitState                 ',(tmpData(j,3),j=1,3)    
       write (703,'(A26,3E12.4)') 'ChemInit                  ',(tmpData(j,9),j=1,3)    
       write (703,'(A26,3E12.4)') 'InitTotal                 ',(tmpData(j,6),j=1,3)    
       write (703,'(A26,3E12.4)') 'RunTime                   ',(tmpData(j,4),j=1,3)    
       write (703,'(A26,3E12.4)') 'MainLoop                  ',(tmpData(j,7),j=1,3)    
       IF(tmpData(2,5) > 1d-6) THEN 
          write (703,'(A26,3E12.4)') 'RungeKutta                ',(tmpData(j,5),j=1,3)    
          write (703,'(A26,3E12.4)') 'SpecificRKTime            ',(gridFac*stepFac*tmpData(j,5),j=1,3)    
       ENDIF
    ENDIF

    mpi_timings => region%mpi_timings(1)
    
    CALL ReduceTimingData(mpi_timings%io_read_restart,tmpData(1,1),mycomm)
    CALL ReduceTimingData(mpi_timings%io_write_restart,tmpData(1,2),mycomm)
    CALL ReduceTimingData(mpi_timings%io_write_soln,tmpData(1,3),mycomm)
    CALL ReduceTimingData(mpi_timings%io_write_grid,tmpData(1,4),mycomm)
    CALL ReduceTimingData(mpi_timings%io_setup,tmpData(1,5),mycomm)

    If (region%myrank == 0) Then
       if(tmpData(2,1) > 1d-6) write (703,'(A,3E12.4)') 'io_read_restart           ',(tmpData(j,1),j=1,3)
       if(tmpData(2,2) > 1d-6) write (703,'(A,3E12.4)') 'io_write_restart          ',(tmpData(j,2),j=1,3)
       if(tmpData(2,3) > 1d-6) write (703,'(A,3E12.4)') 'io_write_soln             ',(tmpData(j,3),j=1,3)
       if(tmpData(2,4) > 1d-6) write (703,'(A,3E12.4)') 'io_write_grid             ',(tmpData(j,4),j=1,3)
       if(tmpData(2,5) > 1d-6) write (703,'(A,3E12.4)') 'io_setup                  ',(tmpData(j,5),j=1,3)
    End If

    ! Grid-specific input timing
    Do p = 1, region%nGridsGlobal
       if(region%myrank == 0) then
          metricData(:,:) = 0
          CALL MPI_Irecv(metricData,20,MPI_DOUBLE_PRECISION,MPI_ANY_SOURCE,p,mycomm,mpiRequest(p),ierr)
       endif
       Do ng = 1, region%nGrids
          grid => region%grid(ng)
          mpi_timings => region%mpi_timings(ng)
          if (grid%iGridGlobal == p) then
             Call ReduceTimingData(mpi_timings%io_read_grid,tmpData(1,1),grid%comm)
             Call ReduceTimingData(mpi_timings%io_read_sponge,tmpData(1,2),grid%comm)
             Call ReduceTimingData(mpi_timings%io_read_soln,tmpData(1,3),grid%comm)
             Call ReduceTimingData(mpi_timings%io_read_mean,tmpData(1,4),grid%comm)
             Call ReduceTimingData(mpi_timings%io_read_target,tmpData(1,5),grid%comm)
             if (grid%myrank_inComm == 0) then
                CALL MPI_Send(tmpData(1,1),20,MPI_DOUBLE_PRECISION,0,p,mycomm,ierr)      
             end if
          end if
       End Do
       if(region%myrank == 0) then
          call MPI_Wait(mpiRequest(p),requestStatii(:,p),ierr)
          if (metricData(2,1) > 1d-3) then
             write (703,'(A13,I2.2,A11,3E12.4)') 'io_read_grid_',p,' ',(metricData(j,1),j=1,3)
          end if
          if (metricData(2,2) > 1d-3) then
             write (703,'(A15,I2.2,A9,3E12.4)') 'io_read_sponge_',p,' ',(metricData(j,2),j=1,3)
          end if
          if (metricData(2,3) > 1d-3) then
             write (703,'(A13,I2.2,A11,3E12.4)') 'io_read_soln_',p,' ',(metricData(j,3),j=1,3)
          end if
          if (metricData(2,4) > 1d-3) then
             write (703,'(A13,I2.2,A11,3E12.4)') 'io_read_mean_',p,' ',(metricData(j,4),j=1,3)
          end if
          if (metricData(2,5) > 1d-3) then
             write (703,'(A15,I2.2,A9,3E12.4)') 'io_read_target_',p,' ',(metricData(j,5),j=1,3)
          end if
       endif
    End Do

    io_total = mpi_timings%io_read_restart 
    io_total = io_total + mpi_timings%io_write_restart 
    io_total = io_total + mpi_timings%io_write_soln 
    io_total = io_total + mpi_timings%io_write_grid
    io_total = io_total + region%init_grids_time
    io_total = io_total + region%init_state_time

    ! Operator setup timings
    Do p = 1, region%nGridsGlobal
       if(region%myrank == 0) then
          metricData(:,:) = 0
          CALL MPI_Irecv(metricData,4,MPI_DOUBLE_PRECISION,MPI_ANY_SOURCE,p,mycomm,mpiRequest(p),ierr)
       endif
       Do ng = 1, region%nGrids
          
          grid => region%grid(ng)
          mpi_timings => region%mpi_timings(ng)
          
          if (grid%iGridGlobal == p) then
             tmpData(1,1) = mpi_timings%operator_setup(1,1)
             Call ReduceTimingData(tmpData(1,1),tmpData(1,2),grid%comm)
             if (grid%myrank_inComm == 0) then
                CALL MPI_Send(tmpData(1,2),4,MPI_DOUBLE_PRECISION,0,p,mycomm,ierr)      
             end if
          end if
       End Do
       if(region%myrank == 0) then
          call MPI_Wait(mpiRequest(p),requestStatii(:,p),ierr)
          if (metricData(2,1) > 1d-6) then
             write (703,'(A20,I2.2,A4,3E12.4)') 'operator_setup_grid_',p,' ',(metricData(j,1),j=1,3)
          end if
       endif
    End Do

    CALL MPI_Barrier(mycomm,ierr)

    ! Operator timings
    Do p = 1, region%nGridsGlobal
       do i = 1,MAX_OPERATORS
          do jj = 1,MAX_ND
             if(region%myrank == 0) then
                metricData(:,:) = 0.0
                mpiRequest(:) = 0
                ierr = 0
                CALL MPI_Irecv(metricData,20,MPI_DOUBLE_PRECISION,MPI_ANY_SOURCE,i*100+jj,mycomm,mpiRequest,ierr)
             endif
             Do ng = 1, region%nGrids
                grid => region%grid(ng)
                mpi_timings => region%mpi_timings(ng)
                
                if (grid%iGridGlobal == p) then
                   Call ReduceTimingData(mpi_timings%operator(i,jj),tmpData(1,1),grid%comm)
                   Call ReduceTimingData(mpi_timings%operator_interior(i,jj),tmpData(1,2),grid%comm)
                   Call ReduceTimingData(mpi_timings%operator_boundary(i,jj),tmpData(1,3),grid%comm)
                   Call ReduceTimingData(mpi_timings%operator_solve(i,jj),tmpData(1,5),grid%comm)
                   Call ReduceTimingData(mpi_timings%operator_ghostExchange(i,jj),tmpData(1,4),grid%comm)
                   if (grid%myrank_inComm == 0) then
                      CALL MPI_Ssend(tmpData(1,1),20,MPI_DOUBLE_PRECISION,0,i*100+jj,mycomm,ierr)      
                   endif
                end if
             end do
             call MPI_Barrier(mycomm,ierr)
             if(region%myrank == 0) then
                requestStatii = 0
                CALL MPI_WAIT(mpiRequest,requestStatii,ierr)
                if (metricData(2,1) > 1d-6)&
                     write (703,'(A,I2.2,A,I3.3,A,I1.1,A9,3E12.4)') &
                     'operator_',p,'_',i,'_',jj,' ',(metricData(j,1),j=1,3)
                if (metricData(2,2) > 1d-6) &
                     write (703,'(A,I2.2,A,I3.3,A,I1.1,A6,3E12.4)') &
                     'op_interior_',p,'_',i,'_',jj,' ',(metricData(j,2),j=1,3)
                if (metricData(2,3) > 1d-6) &
                     write (703,'(A,I2.2,A,I3.3,A,I1.1,A6,3E12.4)') &
                     'op_boundary_',p,'_',i,'_',jj,' ',(metricData(j,3),j=1,3)
                if (metricData(2,5) > 1d-6) &
                     write (703,'(A,I2.2,A,I3.3,A,I1.1,A9,3E12.4)') &
                     'op_solve_',p,'_',i,'_',jj,' ',(metricData(j,5),j=1,3)
                if (metricData(2,4) > 1d-6) &
                     write (703,'(A,I2.2,A,I3.3,A,I1.1,A6,3E12.4)') &
                     'op_exchange_',p,'_',i,'_',jj,' ',(metricData(j,4),j=1,3)
             endif
          end do
       end do
    End Do
    
    
    
    ! grid metrics timings
    Do p = 1, region%nGridsGlobal
       if(region%myrank == 0) then
          metricData(:,:) = 0
          CALL MPI_Irecv(metricData,4,MPI_DOUBLE_PRECISION,MPI_ANY_SOURCE,p,mycomm,mpiRequest(p),ierr)
       endif
       Do ng = 1, region%nGrids
          grid => region%grid(ng)
          mpi_timings => region%mpi_timings(ng)
          if (grid%iGridGlobal == p) then
             Call ReduceTimingData(mpi_timings%metrics,tmpData,grid%comm)
             if (grid%myrank_inComm == 0) then
                CALL MPI_Send(tmpData,4,MPI_DOUBLE_PRECISION,0,p,mycomm,ierr)      
             end if
          end if
       End Do
       if(region%myrank == 0) then
          CALL MPI_Wait(mpiRequest(p),requestStatii(:,p),ierr)
          if(metricData(2,1) > 1d-6) &
               write (703,'(A,I2.2,A11,3E12.4)') 'metrics_grid_',p,' ',(metricData(j,1),j=1,3)
       endif
    End Do
    

    ! Interpolation timings
    Do p = 1, region%nGridsGlobal
       if(region%myrank == 0) then
          CALL MPI_Irecv(metricData,16,MPI_DOUBLE_PRECISION,MPI_ANY_SOURCE,p,mycomm,mpiRequest(p),ierr)
       endif
       Do ng = 1, region%nGrids
     
          grid => region%grid(ng)
          mpi_timings => region%mpi_timings(ng)
          
          if (grid%iGridGlobal == p) then
             CALL ReduceTimingData(mpi_timings%interp_setup,tmpData(1,1),grid%comm)
             CALL ReduceTimingData(mpi_timings%interp,tmpData(1,2),grid%comm)
             CALL ReduceTimingData(mpi_timings%interp_volx,tmpData(1,3),grid%comm)
             CALL ReduceTimingData(mpi_timings%interp_nbix,tmpData(1,4),grid%comm)
             io_total = io_total + mpi_timings%interp_setup
             if (grid%myrank_inComm == 0) then
                CALL MPI_Send(tmpData,16,MPI_DOUBLE_PRECISION,0,p,mycomm,ierr)
             end if
          end if          
       End Do
       if(region%myrank == 0) then
          CALL MPI_Wait(mpiRequest(p),requestStatii(:,p),ierr)
          if(metricData(2,1)  > 1d-6) write (703,'(A,I2.2,A11,3E12.4)') &
               'interp_setup_',p,' ',(metricData(j,1),j=1,3)
          if(metricData(2,2) > 1d-6) write (703,'(A,I2.2,A17,3E12.4)') &
               'interp_',p,' ',(metricData(j,2),j=1,3)
          if(metricData(2,3) > 1d-6) write (703,'(A,I2.2,A12,3E12.4)') &
               'interp_volx_',p,' ',(metricData(j,3),j=1,3)
          if(metricData(2,4) > 1d-6) write (703,'(A,I2.2,A12,3E12.4)') &
               'interp_nbix_',p,' ',(metricData(j,4),j=1,3)
       endif
    End Do

    ! RHS timings
    Do p = 1, region%nGridsGlobal
       if(region%myrank == 0) then
          metricData(:,:) = 0
          CALL MPI_Irecv(metricData,48,MPI_DOUBLE_PRECISION,MPI_ANY_SOURCE,p,mycomm,mpiRequest(p),ierr)
       endif
       Do ng = 1, region%nGrids
          
          grid => region%grid(ng)
          mpi_timings => region%mpi_timings(ng)
          
          if (grid%iGridGlobal == p) then
             CALL ReduceTimingData(mpi_timings%rhs_inviscid,tmpData(1,1),grid%comm)
             CALL ReduceTimingData(mpi_timings%rhs_viscous,tmpData(1,2),grid%comm)
             CALL ReduceTimingData(mpi_timings%rhs_bc,tmpData(1,3),grid%comm)
             CALL ReduceTimingData(mpi_timings%rhs_bc_fix_value,tmpData(1,4),grid%comm)
             CALL ReduceTimingData(mpi_timings%rhs_eos,tmpData(1,5),grid%comm)
             CALL ReduceTimingData(mpi_timings%rhs_VGrad,tmpData(1,6),grid%comm)
             CALL ReduceTimingData(mpi_timings%rhs_TGrad,tmpData(1,7),grid%comm)
             CALL ReduceTimingData(mpi_timings%rhs_ScalarGrad,tmpData(1,8),grid%comm)
             CALL ReduceTimingData(mpi_timings%rhs_shock,tmpData(1,9),grid%comm)
             CALL ReduceTimingData(mpi_timings%rhs_les,tmpData(1,10),grid%comm)
             CALL ReduceTimingData(mpi_timings%rhs_artdiss,tmpData(1,11),grid%comm)
             CALL ReduceTimingData(mpi_timings%rhs_total,tmpData(1,12),grid%comm)
             if (grid%myrank_inComm == 0) then
                Call MPI_Send(tmpData,48,MPI_DOUBLE_PRECISION,0,p,mycomm,ierr)
             endif
          endif
       end do
       if(region%myrank == 0) then
          CALL MPI_Wait(mpiRequest(p),requestStatii(:,p),ierr)
          if(metricData(2,1)>1d-6)  write (703,'(A,I2.2,A11,3E12.4)') 'rhs_inviscid_',p,' ',(metricData(j,1),j=1,3)
          if(metricData(2,2)>1d-6)  write (703,'(A,I2.2,A12,3E12.4)') 'rhs_viscous_',p,' ',(metricData(j,2),j=1,3)
          if(metricData(2,3)>1d-6)  write (703,'(A,I2.2,A17,3E12.4)') 'rhs_bc_',p,' ',(metricData(j,3),j=1,3)
          if(metricData(2,4)>1d-6)  write (703,'(A,I2.2,A7,3E12.4)') 'rhs_bc_fix_value_',p,' ',(metricData(j,4),j=1,3)
          if(metricData(2,5)>1d-6)  write (703,'(A,I2.2,A16,3E12.4)') 'rhs_eos_',p,' ',(metricData(j,5),j=1,3)
          if(metricData(2,6)>1d-6)  write (703,'(A,I2.2,A12,3E12.4)') 'rhs_VelGrad_',p,' ',(metricData(j,6),j=1,3)
          if(metricData(2,7)>1d-6)  write (703,'(A,I2.2,A11,3E12.4)') 'rhs_TempGrad_',p,' ',(metricData(j,7),j=1,3)
          if(metricData(2,8)>1d-6)  write (703,'(A,I2.2,A9,3E12.4)')  'rhs_ScalarGrad_',p,' ',(metricData(j,8),j=1,3)
          if(metricData(2,9)>1d-6)  write (703,'(A,I2.2,A14,3E12.4)') 'rhs_shock_',p,' ',(metricData(j,9),j=1,3)
          if(metricData(2,10)>1d-6) write (703,'(A,I2.2,A16,3E12.4)') 'rhs_les_',p,' ',(metricData(j,10),j=1,3)
          if(metricData(2,11)>1d-6) write (703,'(A,I2.2,A12,3E12.4)') 'rhs_artdiss_',p,' ',(metricData(j,11),j=1,3)
          if(metricData(2,12)>1d-6) write (703,'(A,I2.2,A14,3E12.4)') 'rhs_total_',p,' ',(metricData(j,12),j=1,3)
       end if
    End Do
  
    ! thermomech timings
    if(region%input%TMSolve /= FALSE) then
       Do p = 1, region%nGridsGlobal
          if(region%myrank == 0) then
             metricData(:,:) = 0
             CALL MPI_Irecv(metricData,24,MPI_DOUBLE_PRECISION,MPI_ANY_SOURCE,p,mycomm,mpiRequest(p),ierr)
          endif

          Do ng = 1, region%nGrids
             grid => region%grid(ng)
             mpi_timings => region%mpi_timings(ng)
             
             if (grid%iGridGlobal == p) then
                CALL ReduceTimingData(mpi_timings%TM_rhs_thermal,tmpData(1,1),grid%comm)
                CALL ReduceTimingData(mpi_timings%TM_lhs_thermal,tmpData(1,2),grid%comm)
                CALL ReduceTimingData(mpi_timings%TM_solve_thermal,tmpData(1,3),grid%comm)
                CALL ReduceTimingData(mpi_timings%TM_rhs_structural,tmpData(1,4),grid%comm)
                CALL ReduceTimingData(mpi_timings%TM_lhs_structural,tmpData(1,5),grid%comm)
                CALL ReduceTimingData(mpi_timings%TM_solve_structural,tmpData(1,6),grid%comm)
                if (grid%myrank_inComm == 0) then
                   Call MPI_Send(tmpData,24,MPI_DOUBLE_PRECISION,0,p,mycomm,ierr)
                endif
             endif
          end do
          if (region%myrank == 0) then
             CALL MPI_Wait(mpiRequest(p),requestStatii(:,p),ierr)
             if (metricData(2,1)>1d-6) write (703,'(A,I2.2,A12,3E12.4)') 'rhs_thermal_',p,' ',(metricData(j,1),j=1,3)
             if (metricData(2,2)>1d-6) write (703,'(A,I2.2,A12,3E12.4)') 'lhs_thermal_',p,' ',(metricData(j,2),j=1,3)
             if (metricData(2,3)>1d-6) write (703,'(A,I2.2,A10,3E12.4)') 'thermal_solve_',p,' ',(metricData(j,3),j=1,3)
             if (metricData(2,4)>1d-6) write (703,'(A,I2.2,A13,3E12.4)') 'rhs_struct_',p,' ',(metricData(j,4),j=1,3)
             if (metricData(2,5)>1d-6) write (703,'(A,I2.2,A13,3E12.4)') 'lhs_struct_',p,' ',(metricData(j,5),j=1,3)
             if (metricData(2,6)>1d-6) write (703,'(A,I2.2,A11,3E12.4)') 'struct_solve_',p,' ',(metricData(j,6),j=1,3)
          end if
       End Do
       CALL ReduceTimingData(region%TM_total_time,tmpData,mycomm)
       If (region%myrank == 0) then
          if(tmpData(2,1)>1d-6) write (703,'(A,E12.4,A)') 'thermomech_total          ',(tmpData(j,1),j=1,3)
       end If
    end if
    
#ifdef BUILD_ELECTRIC
    ! efield timings
    if (region%input%includeElectric) then
       CALL ReduceTimingData(region%mpi_timings(1)%efield_processing,tmpData(1,1),mycomm)
       CALL ReduceTimingData(region%mpi_timings(1)%efield_solve,tmpData(1,2),mycomm)
       if(region%myrank == 0) then
          if(tmpData(2,1) > 1d-6) write(703,'(A,3E12.4)') 'efield_total              ',(tmpData(j,1),j=1,3)
          if(tmpData(2,2) > 1d-6) write(703,'(A,3E12.4)') 'efield_solve              ',(tmpData(j,2),j=1,3)
       end if
    end if
#endif
    
    ! io_total timings
    CALL ReduceTimingData(io_total,tmpData,mycomm)
    If (region%myrank == 0) Then
       if(tmpData(2,1) > 1d-6) write (703,'(A,3E12.4)') 'io_total                  ',(tmpData(j,1),j=1,3)
       CLOSE(703)
    End If
    
    call mpi_barrier(mycomm, ierr)
    
    DEALLOCATE(metricData)
    DEALLOCATE(tmpData)
    DEALLOCATE(mpiRequest)
    DEALLOCATE(requestStatii)
    DEALLOCATE(nPoints)
    DEALLOCATE(nProcGrid)
    DEALLOCATE(specificStepTime)

  End Subroutine Write_MPI_Timings
  
  Subroutine Read_DV_Table_Spline(region, ng)

    USE ModGlobal
    USE ModEOS
    USE ModDataStruct
    USE ModMPI

    Implicit None

    TYPE(t_region), pointer :: region

    ! ... local
    TYPE(t_mixt_input), pointer :: input
    TYPE(t_grid), pointer :: grid
    TYPE(t_mixt), pointer :: state
    real(rfreal) :: TempRef, SndSpd2, GamRef, Deta, GasConst, Z
    real(rfreal), pointer :: TPTable(:,:,:)
    integer :: j, i, ndata, k, ng
    character(len=80) :: fname
    TYPE(t_spline), Pointer :: spline

    ! ... simplicity
    grid    => region%grid(ng)
    input   => grid%input
    state   => region%state(ng)
    TempRef = input%TempRef
    fname   = input%DVTableName

    ! ... allocate spline datastructure
    allocate(state%dvSpline(2))
    spline => state%dvSpline(1)

    ! ... need to read in thermally perfect lookup table
    Open(unit=100, file=trim(fname), form='unformatted', status='old')

    ! ... read gas constant
    Read (100) input%gasConstantRef

    ! ... read number of break points
    Read (100) spline%nxb

    ! ... allocate space for breakpoints
    allocate(spline%xb(spline%nxb))

    ! ... read breakpoints
    Read (100) spline%xb
    spline%dxb = spline%xb(2)-spline%xb(1)

    ! ... read coefficients for Cp, Cv, Eint, Gamma, Z (in that order)
    spline%nvars = 5
    allocate(spline%coeffs(spline%nxb-1,4,spline%nvars))
    Do i = 1, spline%nvars
       Read (100) ((spline%coeffs(j,k,i),j=1,spline%nxb-1),k=1,4)
    End Do

    ! ... prepare for T(eint) lookup
    spline => state%dvSpline(2)
    spline%nxb = state%dvSpline(1)%nxb
    allocate(spline%xb(spline%nxb));
    spline%nvars = 1
    allocate(spline%coeffs(spline%nxb-1,4,spline%nvars))

    ! ... read breakpoints
    Read (100) spline%xb
    spline%dxb = spline%xb(2) - spline%xb(1)

    ! ... read coefficients
    Do i = 1, spline%nvars
       Read (100) ((spline%coeffs(j,k,i),j=1,spline%nxb-1),k=1,4)
    End Do

    ! ... close file
    close (100)

    ! ... before we non-dimensionalize, reset some reference parameters
    ! ... density and temperature are held fixed
    spline => state%dvSpline(1)
    input%Cpref     = evaluateSpline(spline%nxb, spline%dxb, spline%xb, spline%coeffs, 1, input%TempRef)
    input%Cvref     = evaluateSpline(spline%nxb, spline%dxb, spline%xb, spline%coeffs, 2, input%TempRef)
    input%eintref   = evaluateSpline(spline%nxb, spline%dxb, spline%xb, spline%coeffs, 3, input%TempRef)
    input%gamref    = evaluateSpline(spline%nxb, spline%dxb, spline%xb, spline%coeffs, 4, input%TempRef)
    Z               = evaluateSpline(spline%nxb, spline%dxb, spline%xb, spline%coeffs, 5, input%TempRef)
    input%presref   = Z * input%gasConstantRef * input%DensRef * input%TempRef
    input%sndspdref = sqrt(input%gamref * input%presref / input%densref)

    Return

  end Subroutine Read_DV_Table_Spline

  Subroutine Read_TV_Table_Spline(region, ng)

    USE ModGlobal
    USE ModEOS
    USE ModDataStruct
    USE ModMPI

    Implicit None

    TYPE(t_region), pointer :: region

    ! ... local
    TYPE(t_mixt_input), pointer :: input
    TYPE(t_grid), pointer :: grid
    TYPE(t_mixt), pointer :: state
    real(rfreal) :: TempRef, SndSpd2, GamRef, Deta, GasConst, Z
    real(rfreal), pointer :: TPTable(:,:,:)
    integer :: j, i, ndata, k, ng
    character(len=80) :: fname
    TYPE(t_spline), pointer :: spline

    ! ... simplicity
    grid    => region%grid(ng)
    input   => grid%input
    state   => region%state(ng)
    fname   = input%TVTableName

    ! ... allocate spline datastructure
    allocate(state%tvSpline)
    spline => state%tvSpline

    ! ... need to read in thermally perfect lookup table
    Open(unit=100, file=trim(fname), form='unformatted', status='old')

    ! ... read number of break points
    Read (100) spline%nxb

    ! ... allocate space for breakpoints
    allocate(spline%xb(spline%nxb))

    ! ... read breakpoints
    Read (100) spline%xb
    spline%dxb = spline%xb(2)-spline%xb(1)

    ! ... read coefficients for mu, lambda, k
    spline%nvars = 3
    allocate(spline%coeffs(spline%nxb-1,4,spline%nvars))
    Do i = 1, spline%nvars
       Read (100) ((spline%coeffs(j,k,i),j=1,spline%nxb-1),k=1,4)
    End Do

    ! ... close file
    close (100)

    ! ... before we non-dimensionalize, find reference viscosities and thermal conductivity
    input%muref     = evaluateSpline(spline%nxb, spline%dxb, spline%xb, spline%coeffs, 1, input%TempRef)
    input%lambdaref = evaluateSpline(spline%nxb, spline%dxb, spline%xb, spline%coeffs, 2, input%TempRef)
    input%kapparef  = evaluateSpline(spline%nxb, spline%dxb, spline%xb, spline%coeffs, 3, input%TempRef)

    ! ... reset Prandtl number
    input%PR = input%muref * input%Cpref / input%kapparef

    Return

  end Subroutine Read_TV_Table_Spline

End Module ModRegion
