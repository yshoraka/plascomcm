! Copyright (c) 2015, University of Illinois at Urbana-Champaign, XPACC
! License: MIT, http://opensource.org/licenses/MIT
!-----------------------------------------------------------------------
!
! ModTMSingleStep.f90
!
! - time stepping for thermal and structural
!
! Revision history
! - 10 Mar 2012 : CMO - initial code
!
!
!-----------------------------------------------------------------------
Module ModTMSingleStep

CONTAINS

  Subroutine TMStep(region, ThermalStep, StructuralStep)

    USE ModGlobal
    USE ModDataStruct
    USE ModTMIO
    USE ModIO
    USE ModPETSc
    USE ModMPI
    USE ModTMInt
    USE ModTMEOM

    Implicit None

    Type(t_region), pointer :: region

    ! ... local variables
    Type(t_sgrid), pointer :: TMgrid
    Type(t_smixt), pointer :: TMstate
    Type(t_mixt_input), pointer :: input
    Integer :: ng, i, j, k, dir, p, main_ts_loop_index
    Integer :: TMSubiter, MaxTMSubits, nPtsLoc, nVars, CouplingStep, StructuralStep, ThermalStep, nRateVars
    Real(rfreal) :: Su, ST, SuDen, STDen, SuOld, STOld, TMError, TMConvCrit, SolnChng, TMErrorAux, timer,ThermInterpFac
    Real(rfreal), allocatable :: StructSolnOld(:,:), ThermSolnOld(:)
    Real(rfreal), pointer :: F(:)
    Logical :: AllocateOldVec

    ! Simplicity
    main_ts_loop_index = region%global%main_ts_loop_index

    do ng = 1, region%nTMGrids

       AllocateOldVec = .false.

       ! ... simplicity
       TMgrid  => region%TMgrid(ng)
       TMstate => region%TMstate(ng)
       input   => TMgrid%input
       nPtsLoc = TMgrid%nPtsLoc
       nVars   = TMstate%nVars
       
       ! ... do the thermal and structural solution couple on this step
       CouplingStep = FALSE
       if(input%TMSolve == 3 .and. mod(main_ts_loop_index-1,TMstate%CoupleFrequency) == 0) CouplingStep = TRUE
       if(input%StructuralTimeScheme == STEADY_SOLN .and.input%ThermalTimeScheme == STEADY_SOLN) CouplingStep = TRUE

       if(input%TMSolve ==3) then
          if(ThermalStep == TRUE) then
             if(region%TMRank == 0) write(*,'(A)') 'Saving current thermal solution data ...'
             do i = 1, nPtsLoc
                TMstate%qOld(i,nVars) = TMstate%q(i,nVars)
             end do
          end if
          
          if(StructuralStep == TRUE) then
             if(region%TMRank == 0) write(*,'(A)') 'Saving current structural solution data ...'
             do i = 1, nPtsLoc
                do dir = 1, TMgrid%ND
                   TMstate%qOld(i,dir) = TMstate%q(i,dir)
                end do
             end do
          end if
       end if

       if(CouplingStep == TRUE) then
          ! ... initialize convergence metric
          SuDen = 1
          STDen = 1
          if(input%TMSolve > 1) then
             SuDen = 0.0_rfreal
             allocate(StructSolnOld(nPtsLoc,TMgrid%ND))
             do i = 1, TMgrid%nPtsLoc
                do dir = 1, TMgrid%ND
                   SuDen = SuDen + TMstate%q(i,dir)*TMstate%q(i,dir)
                   StructSolnOld(i,dir) = TMstate%q(i,dir)
                end do
             end do
          end if
          SuDen = sqrt(SuDen)
          if(input%TMSolve == 1 .or. input%TMSolve == 3) then
             STDen = 0.0_rfreal
             allocate(ThermSolnOld(nPtsLoc))
             do i = 1, TMgrid%nPtsLoc
                STDen = STDen + TMstate%q(i,nVars)*TMstate%q(i,nVars)
                ThermSolnOld(i) = TMstate%q(i,nVars)
             end do
          end if
          STDen = sqrt(STDen)
          if (SuDen .lt. tiny) SuDen = 1.0_rfreal
          if (STDen .lt. tiny) STDen = 1.0_rfreal
          
          Su = SuDen
          ST = STDen
       
          SuDen = 1.0_rfreal/SuDen
          STDen = 1.0_rfreal/STDen     
       end if
       
       ! ... begin subiterations
       ! ... if not coupled thermomechanical problem on not coupled at this step, then no need to iterate
       if(input%TMSolve /= 3 .or. CouplingStep == FALSE) then
          MaxTMSubits = 1
       else
          MaxTMSubits = input%MaxTMSubits
       end if

       TMSubiter = 0
       TMConvCrit = 1.0D-5
       TMError = 1
       
       ! ... compute initial residual on first subiteration
       ! ... for Newton-Rhapson iterations
       TMstate%NewInitRes = .true.

       do while((TMSubiter < MaxTMSubits) .and. (TMError .gt. TMConvCrit))
          ! ... increment subiteration
          TMSubiter = TMSubiter + 1

          ! ... now do the thermal problem
          if(input%TMSolve == 1 .or. input%TMSolve == 3) then
             if(ThermalStep == TRUE) then
                if(input%ThermalTimeScheme == DYNAMIC_SOLN) then
                   if(input%TMSolve == 3) then
                      ! ... reset to beginning of timestep
                      ! ... solve on new geometry
                      do i = 1, nPtsLoc
                         TMstate%q(i,nVars) = TMstate%qOld(i,nVars)
                      end do
                   end if
                   
                   Call DynamicThermal(region, ng)
                elseif(input%ThermalTimeScheme == STEADY_SOLN) then
                      do i = 1, nPtsLoc
                         TMstate%q(i,nVars) = TMstate%qOld(i,nVars)
                      end do
                   
                   Call StaticThermal(region, ng)
                   
                end if
             
                ! ... if interpolating temperature, then record temperature at end of thermal step
                if(input%ThermalSolnFreq /= 1) then
                   do i = 1, nPtsLoc
                      TMstate%Tnew(i) = TMstate%q(i,nVars)
                      TMstate%q(i,nVars) = (TMstate%Tnew(i) - TMstate%qOld(i,nVars))/dble(input%ThermalSolnFreq)+TMstate%qOld(i,nVars)
                   end do
                end if
             elseif(ThermalStep /= TRUE) then
                ! ... update solution with interpolated temperature
                ThermInterpFac = dble(mod(main_ts_loop_index-1,input%ThermalSolnFreq) + 1)/dble(input%ThermalSolnFreq)
                do i = 1, nPtsLoc
                   TMstate%q(i,nVars) = ThermInterpFac * (TMstate%Tnew(i) - TMstate%qOld(i,nVars)) + TMstate%qOld(i,nVars)
                end do
                     
             end if
                             
          end if

          ! ... find the structural solution
          if(input%TMSolve == 2 .or. (input%TMSolve == 3 .and. StructuralStep == TRUE)) then

             ! ... fill possibly temperature dependent material properties arrays
             ! ... Youngs Modulus, Poisson's ratio
             !Call getMaterialProps(region, ng)

             if(input%StructuralTimeScheme == DYNAMIC_SOLN) then
                 if(input%TMSolve == 3) then
                    ! ... reset to beginning of timestep
                    ! ... solve on new geometry
                    do i = 1, nPtsLoc
                       do dir = 1, TMgrid%ND
                          TMstate%q(i,dir) = TMstate%qOld(i,dir)
                       end do
                    end do
                 end if
                 !Call TMwrite_plot3d_file(region, 'TMq',(TMSubiter-1)*2+1)     
                ! ... copy old structural rates
                Call StructuralRates(region,ng,.false.)

                Call DynamicStructural(region, ng)
             elseif(input%StructuralTimeScheme == STEADY_SOLN) then
                Call StaticStructural(region, ng)
                !print*,'Maxdisp',maxval(TMstate%q(:,2))
             end if
          end if

          if(input%TMSolve > 2) then
             ! ... calculate error
             if(CouplingStep == TRUE) then
                if(input%TMSolve > 1) then
                   SuOld = Su
                   Su = 0
                   do i = 1, TMgrid%nPtsLoc
                      do dir = 1, TMgrid%ND
                         SolnChng = TMstate%q(i,dir) - StructSolnOld(i,dir)
                         StructSolnOld(i,dir) = TMstate%q(i,dir)
                         Su = Su + SolnChng*SolnChng
                      end do
                   end do
                end if
                Su = sqrt(Su)
                !print*,'MaxTemp',maxval(TMstate%q(:,nVars))
                if(input%TMSolve == 1 .or. input%TMSolve == 3) then
                   STOld = ST
                   ST = 0
                   do i = 1, TMgrid%nPtsLoc
                      SolnChng = TMstate%q(i,nVars) - ThermSolnOld(i)
                      ThermSolnOld(i) = TMstate%q(i,nVars)
                      ST = ST + SolnChng*SolnChng
                   end do
                end if
                ST = sqrt(ST)
                TMErrorAux = max(ST*STDen,Su*SuDen)
                
                ! ... note: MPI_DOUBLE = 1275070475, MPI_MAX = 1476395009
                CALL MPI_ALLREDUCE(TMErrorAux,TMError,1,1275070475,1476395009,TMgrid%Comm,ierr)

                if(input%TMSolve == 3) then
                   if(region%TMRank == 0) write(*,'(A,I4.0,2(A,D13.6))') 'PlasComCM: TM subiteration = ', &
                        TMSubiter, ', Error: Therm: ',ST*STDen,' Structural: ',Su*SuDen
                end if
             end if
          end if

       end do ! ... do while((TMSubiter < MaxTMSubits) .and. (TMError .gt. TMConvCrit))

       ! ... save the velocity and acceleration from PETSc vectors
       nRateVars = 0
       if(input%TMSolve > 1 .and. input%StructuralTimeScheme == DYNAMIC_SOLN) then
          nRateVars = TMgrid%ND*3
          Call StructuralRates(region,ng,.true.)
       end if
       
       if((input%TMSolve == 1 .or. input%TMSolve == 3) .and. input%ThermalTimeScheme == DYNAMIC_SOLN) then
          nRateVars = nRateVars + 1
          Call UpdateTemperatureRate(region,ng)
       end if

           ! ... exhange the solution
       allocate(F(TMgrid%nPtsLoc))
       do j = 1, nRateVars
          do p = 1, TMgrid%nPtsLoc
             F(p) = dble(TMstate%qdot(p,j))
          end do
          call Ghost_Cell_Exchange_TM(region, F, ng)
          do p = 1, TMgrid%nPtsLoc
             TMstate%qdot(p,j) = F(p)
          end do
       end do
       deallocate(F)
       
    end do ! ... ng

    if(region%TMRank == 0) then
       if(input%TMSolve == 1 .or. (input%TMSolve == 3 .and. (ThermalStep==TRUE .and. StructuralStep == FALSE))) then
          write(*,'(A,I8,2(A,D13.6))') 'PlasComCM: thermal solution iteration = ', &
               main_ts_loop_index, ', dt = ', region%TMstate(1)%dt_thermal, &
               ', time = ', region%TMstate(1)%dt_thermal*dble(main_ts_loop_index)
       elseif(input%TMSolve == 2 .or. (input%TMSolve == 3 .and. (ThermalStep==FALSE .and. StructuralStep == TRUE))) then
          write(*,'(A,I8,3(A,D13.6),A,I2)') 'PlasComCM: structural solution iteration = ', &
               main_ts_loop_index, ', dt = ', region%TMstate(1)%dt_struct, &
               ', time = ', region%TMstate(1)%dt_struct*dble((main_ts_loop_index-1)/region%input%StructSolnFreq),', final residual ',TMstate%FinalRes,', NR iterations ',TMstate%numNRsteps
       elseif(input%TMSolve == 3 .and. (ThermalStep == TRUE .and. StructuralStep == TRUE)) then
          write(*,'(A,I8,4(A,D13.6),A,I2)') 'PlasComCM: thermomechanical solution iteration = ', &
               main_ts_loop_index, ', dt = ', region%input%dt, &
               ', time = ', region%input%dt*dble(main_ts_loop_index),' Coup. soln conv: Therm: ',ST*STDen,' Structural: ',Su*SuDen,' subiters. = ',TMSubiter         
       endif
    end if

  end Subroutine TMStep

  Subroutine StaticThermal(region, ng)
    
    USE ModGlobal
    USE ModDataStruct
    USE ModTMEOM
    USE ModTMEOMBC
    USE ModPETSc
    USE ModTMIO
    USE ModTMInt
    USE ModMPI
    
    Implicit None

    Type(t_region), pointer :: region
    Integer :: ng
    
    ! ... local variables
    Type(t_mixt_input), pointer :: input
    Type(t_sgrid), pointer :: TMgrid
    Type(t_smixt), pointer :: TMstate
    Real(rfreal), pointer :: F(:)
    Real(rfreal) :: timer
    Integer :: i, j, k, p

    ! ... clear the PETSc arrays
    Call ResetMatrix(1)

    ! ... compute the thermal stiffness matrix
    timer = MPI_WTIME_Hack()
    ! ... 2 = compute thermal stiffness matrix only for static thermal
    call GetKt(region,ng,2)
    region%mpi_timings(ng)%TM_lhs_thermal = region%mpi_timings(ng)%TM_lhs_thermal + (MPI_WTIME_Hack() - timer)

    ! ... compute the external thermal load
    timer = MPI_WTIME_Hack()
    call GetExternalHeatLoad(region,ng)
    region%mpi_timings(ng)%TM_rhs_thermal = region%mpi_timings(ng)%TM_rhs_thermal + (MPI_WTIME_Hack() - timer)

    ! ... assemble the LHS matrix, 1 = thermal problem
    call FinalMatrixAssembly(region%input%TMSolve, 1)

    ! ... impose temperature boundary conditions
    timer = MPI_WTIME_Hack()
    call GetTemperatureBCs(region, ng)
    region%mpi_timings(ng)%TM_rhs_thermal = region%mpi_timings(ng)%TM_rhs_thermal + (MPI_WTIME_Hack() - timer)

    ! ... solve it!
    timer = MPI_WTIME_Hack()
    call ThermalSolve(region, ng)
    region%mpi_timings(ng)%TM_solve_thermal = region%mpi_timings(ng)%TM_solve_thermal + (MPI_WTIME_Hack() - timer)

    ! ... get the solution, 1 = thermal solution
    call GetSoln(region, ng, 1)

    ! ... exhange the solution
    TMgrid => region%TMgrid(ng)
    TMstate => region%TMstate(ng)
    allocate(F(TMgrid%nPtsLoc))
    do p = 1, TMgrid%nPtsLoc
       F(p) = dble(TMstate%q(p,TMstate%nVars))
    end do
    call Ghost_Cell_Exchange_TM(region, F, ng)
    do p = 1, TMgrid%nPtsLoc
       TMstate%q(p,TMstate%nVars) = F(p)
    end do
    deallocate(F)


  end Subroutine StaticThermal

  Subroutine DynamicThermal(region, ng)
    
    USE ModGlobal
    USE ModDataStruct
    USE ModTMEOM
    USE ModTMEOMBC
    USE ModPETSc
    USE ModTMIO
    USE ModTMInt
    
    Implicit None

    Type(t_region), pointer :: region
    Integer :: ng
    
    ! ... local variables
    Type(t_mixt_input), pointer :: input
    Type(t_sgrid), pointer :: TMgrid
    Type(t_smixt), pointer :: TMstate
    Real(rfreal), pointer :: F(:)
    Real(rfreal) :: Coefficient, timer
    Integer :: i, j, k, p, TMSolve

    TMSolve = region%input%TMSolve

    Call ResetMatrix(1)
    
    ! ... compute the thermal stiffness matrix and add to the LHS
    timer = MPI_WTIME_Hack()
    ! ... compute kt and kt*T_n for dynamic thermal problem
    call GetKt(region,ng,3)
    region%mpi_timings(ng)%TM_lhs_thermal = region%mpi_timings(ng)%TM_lhs_thermal + (MPI_WTIME_Hack() - timer)

    ! ... compute the thermal capacitance matrix and:
    ! ... add to the LHS
    ! ... multiply by the old temperature and add to the RHS
    !timer = MPI_WTIME_Hack()
    !Call GetCt(region,ng)
    !region%mpi_timings(ng)%TM_lhs_thermal = region%mpi_timings(ng)%TM_lhs_thermal + (MPI_WTIME_Hack() - timer)

    ! ... compute the external thermal load
    timer = MPI_WTIME_Hack()
    call GetExternalHeatLoad(region,ng)
    region%mpi_timings(ng)%TM_rhs_thermal = region%mpi_timings(ng)%TM_rhs_thermal + (MPI_WTIME_Hack() - timer)

    ! ... assemble the LHS matrix, 1 = thermal problem
    call FinalMatrixAssembly(region%input%TMSolve, 1)

    ! ... assemble the LHS and the RHS, 1 = thermal problem
    call GetDynamicLHSRHS(region, ng, 1)

    ! ... impose temperature boundary conditions
    timer = MPI_WTIME_Hack()
    call GetTemperatureBCs(region, ng)
    region%mpi_timings(ng)%TM_rhs_thermal = region%mpi_timings(ng)%TM_rhs_thermal + (MPI_WTIME_Hack() - timer)

    ! ... solve it!
    timer = MPI_WTIME_Hack()
    call ThermalSolve(region, ng)
    region%mpi_timings(ng)%TM_solve_thermal = region%mpi_timings(ng)%TM_solve_thermal + (MPI_WTIME_Hack() - timer)
    
    ! ... get the solution, 1 = thermal solution
    call GetSoln(region, ng, 1)

    ! ... exhange the solution
    TMgrid => region%TMgrid(ng)
    TMstate => region%TMstate(ng)
    allocate(F(TMgrid%nPtsLoc))
    do p = 1, TMgrid%nPtsLoc
       F(p) = dble(TMstate%q(p,TMstate%nVars))
    end do
    call Ghost_Cell_Exchange_TM(region, F, ng)
    do p = 1, TMgrid%nPtsLoc
       TMstate%q(p,TMstate%nVars) = F(p) 
    end do
    deallocate(F)

    
  end Subroutine DynamicThermal

  Subroutine InitializeDynamicThermal(region, ng)
    
    USE ModDataStruct
    USE ModGlobal
    USE ModTMIO
    USE ModPETSc
    USE ModTMEOM
    USE ModTMEOMBC
   
    Implicit None

    Type(t_region), pointer :: region
    Integer :: ng
    Logical :: AllocateOldVec

    ! ... local variables
    Type(t_smixt), pointer :: TMstate
    Integer :: nVars, i, nPtsLoc, dir, ND
    Real(rfreal) :: timer
    Logical :: file_exist

    ! ... simplicity
    TMstate => region%TMstate(ng)
    nVars   =  TMstate%nVars
    nPtsLoc =  region%TMgrid(ng)%nPtsLoc


    ! ... simplicity
    TMstate => region%TMstate(ng)
    ND      =  region%input%ND
    nPtsLoc =  region%TMgrid(ng)%nPtsLoc

    if(region%TMRank == 0) write(*,'(A)') 'Initializing thermal data ... '

    Call ResetMatrix(4)

    ! ... calculate thermal capacitance matrix
    timer = MPI_WTIME_Hack()
    Call GetCt(region,ng)

    region%mpi_timings(ng)%TM_lhs_thermal = region%mpi_timings(ng)%TM_rhs_thermal + (MPI_WTIME_Hack() - timer)

    Call FinalMatrixAssembly(region%input%TMSolve, 4)

    !Allocate(region%TMstate(ng)%qdot(nPtsLoc,2*ND))
    !Allocate(region%TMstate(ng)%P3Ddot(region%TMgrid(ng)%nCells,2*ND))
    
    INQUIRE(file=region%input%TMrestartdot_fname,EXIST=file_exist)

    if(region%input%TMRestart == FALSE .or. file_exist == .false.) then
       ! ... if this is the first timestep,
       ! ... then need to solve for initial temperature rate

       ! ... compute the internal load
       timer = MPI_WTIME_Hack()
       ! ... compute only kt*T_n for initialization
       call Getkt(region,ng,1)
       region%mpi_timings(ng)%TM_rhs_thermal = region%mpi_timings(ng)%TM_rhs_thermal + (MPI_WTIME_Hack() - timer)

       ! ... compute the external load
       timer = MPI_WTIME_Hack()
       call GetExternalHeatLoad(region,ng)
       region%mpi_timings(ng)%TM_rhs_thermal = region%mpi_timings(ng)%TM_rhs_thermal + (MPI_WTIME_Hack() - timer)

       ! ... solve for the temperature rate
       timer = MPI_WTIME_Hack()
       call GetInitialTemperatureRate(region, ng)
       region%mpi_timings(ng)%TM_solve_thermal = region%mpi_timings(ng)%TM_solve_thermal + (MPI_WTIME_Hack() - timer)

       ! ... reset At, bt, xt
       call ResetMatrix(1)

    else
       ! ... fill temperature rate
       ! ... this is a restart and the restart data is present       
       call EnterTemperatureRate(region, ng)
       
    end if       

  end Subroutine InitializeDynamicThermal


  Subroutine StaticStructural(region, ng)
    
    USE ModGlobal
    USE ModDataStruct
    USE ModTMEOM
    USE ModTMEOMBC
    USE ModPETSc
    USE ModTMIO
    USE ModTMInt
    USE ModMPI
    
    Implicit None

    Type(t_region), pointer :: region
    Integer :: ng
    
    ! ... local variables
    Type(t_mixt_input), pointer :: input
    Type(t_sgrid), pointer :: TMgrid
    Type(t_smixt), pointer :: TMstate
    Real(rfreal), pointer :: F(:)
    Real(rfreal) :: InitRes, Residual, NRConvCrit, MaxDisp, timer
    Integer :: NRit
    Integer :: i, j, k, p, dir
   
    ! ... convergence criteria
    NRConvCrit = 1D-10
    MaxDisp = 1.0_rfreal

    ! ... simplicity
    TMgrid => region%TMgrid(ng)
    TMstate => region%TMstate(ng)
    input  => TMgrid%input
    
    ! ... compute the internal load
    timer = MPI_WTIME_Hack()
    call GetInternalLoad(region,ng)    
    region%mpi_timings(ng)%TM_rhs_structural = region%mpi_timings(ng)%TM_rhs_structural + (MPI_WTIME_Hack() - timer)

    ! ... compute the external load
!    call GetExternalLoad(region,ng)
    timer = MPI_WTIME_Hack()
    call GetExternalLoadFromTensor(region,ng)
    region%mpi_timings(ng)%TM_rhs_structural = region%mpi_timings(ng)%TM_rhs_structural + (MPI_WTIME_Hack() - timer)

    ! ... assemble the external load vector
    Call FinalVectorAssembly(input%TMSolve, 2, 4)

    ! ... assemble the RHS vector
    Call FinalVectorAssembly(input%TMSolve, 2, 2)
    
    ! ... calculate the initial residual
    call GetRes(InitRes,TRUE)
    InitRes = max(InitRes,1.0_rfreal)

    if(TMstate%NewInitRes .eqv. .true.) then
       TMstate%InitRes = InitRes
       TMstate%NewInitRes = .false.
    else
       TMstate%InitRes = max(TMstate%InitRes,InitRes)
       InitRes = TMstate%InitRes
    end if

    if(region%TMRank == 0) write(*,'(A,E13.6)')'PlasComCM: initial residual',InitRes

    ! ... initialize the residual
    Residual = InitRes

    NRit = 0
    ! ... Newton-Rhapson loop
    do while(((abs(Residual/InitRes) > NRConvCrit) .and. abs(Residual) > 1.0D-5 .and. (MaxDisp > 1D-14) .and. NRit < 1000) .or. NRit < 2)
    !do while(((abs(Residual/InitRes) > NRConvCrit) .and. abs(Residual) > 1.0D-5 .and. (MaxDisp > 1D-14) .and. NRit < 1) .or. NRit < 1)
       NRit = NRit + 1
       
       ! ... compute the tangent stiffness matrix
       timer = MPI_WTIME_Hack()
       call GetTangentStiffness(region,ng)
       region%mpi_timings(ng)%TM_lhs_structural = region%mpi_timings(ng)%TM_lhs_structural + (MPI_WTIME_Hack() - timer)

       timer = MPI_WTIME_Hack()
       call GetTensorLoadTangent(region, ng)
       region%mpi_timings(ng)%TM_lhs_structural = region%mpi_timings(ng)%TM_lhs_structural + (MPI_WTIME_Hack() - timer)
       
       ! ... assemble the LHS matrix, 1 = thermal problem
       call FinalMatrixAssembly(region%input%TMSolve, 2)

       ! ... impose displacement boundary conditions
       timer = MPI_WTIME_Hack()
       call GetDisplacementBCs(region, ng)
       region%mpi_timings(ng)%TM_rhs_structural = region%mpi_timings(ng)%TM_rhs_structural + (MPI_WTIME_Hack() - timer)

       ! ... solve it!
       timer = MPI_WTIME_Hack()
       call StructuralSolve(region, ng, MaxDisp)
       region%mpi_timings(ng)%TM_solve_structural = region%mpi_timings(ng)%TM_solve_structural + (MPI_WTIME_Hack() - timer)

       ! ... get the solution, 2 = structural solution
       call GetSoln(region, ng, 2)
       
       Call ResetMatrix(2)

       !       call graceful_exit(region%myrank, 'done')
       ! ... exhange the solution
       TMgrid => region%TMgrid(ng)
       TMstate => region%TMstate(ng)
       allocate(F(TMgrid%nPtsLoc))
       do dir = 1, TMgrid%ND
          do p = 1, TMgrid%nPtsLoc
             F(p) = 0.0_rfreal
             F(p) = TMstate%q(p,dir)
          end do
!          print*,'before,dir,region%TMRank,F(:)',dir,region%TMRank,(p,F(p),p=1,TMgrid%nPtsLoc)
          call Ghost_Cell_Exchange_TM(region, F, ng)
          do p = 1, TMgrid%nPtsLoc
             TMstate%q(p,dir) = F(p)
          end do
!          print*,'after,dir,region%TMRank,F(:)',dir,region%TMRank,(p,F(p),p=1,TMgrid%nPtsLoc)
       end do
       deallocate(F)

       ! ... compute the internal load
       timer = MPI_WTIME_Hack()
       call GetInternalLoad(region,ng)
       region%mpi_timings(ng)%TM_rhs_structural = region%mpi_timings(ng)%TM_rhs_structural + (MPI_WTIME_Hack() - timer)
       
       ! ... compute the external load
       timer = MPI_WTIME_Hack()
       call GetExternalLoadFromTensor(region,ng)
       region%mpi_timings(ng)%TM_rhs_structural = region%mpi_timings(ng)%TM_rhs_structural + (MPI_WTIME_Hack() - timer)

       ! ... assemble the external load vector
       Call FinalVectorAssembly(input%TMSolve, 2, 4)

       ! ... assemble the internal load vector
       Call FinalVectorAssembly(input%TMSolve, 2, 2)
       
       ! ... get the residual
       call GetRes(Residual,TRUE)
       InitRes = max(InitRes,Residual)

       if(region%TMRank == 0) write(*,'(A,I2,3(A,D13.6))') 'PlasComCM: NR iteration = ', NRit, ' Residual = ',Residual,&
            ' Relative Residual = ',abs(Residual/InitRes),' Max. Disp. = ',MaxDisp 
       !Call TMwrite_plot3d_file(region, 'TMq',NRit)       
    end do
    
    ! ... record Newton-Rhapson data for output to screen
    TMstate%numNRsteps = NRit
    TMState%FinalRes = Residual
    
  end Subroutine StaticStructural

  Subroutine DynamicStructural(region, ng)
    
    USE ModGlobal
    USE ModDataStruct
    USE ModTMEOM
    USE ModTMEOMBC
    USE ModPETSc
    USE ModTMIO
    USE ModTMInt
    USE ModMPI
    
    Implicit None

    Type(t_region), pointer :: region
    Integer :: ng
    
    ! ... local variables
    Type(t_mixt_input), pointer :: input
    Type(t_sgrid), pointer :: TMgrid
    Type(t_smixt), pointer :: TMstate
    Real(rfreal), pointer :: F(:)
    Real(rfreal) :: InitRes, Residual, NRConvCrit, MaxDisp, timer, PowerIn, KinEnergy
    Integer :: NRit
    Integer :: i, j, k, p, dir
   
    ! ... convergence criteria
    NRConvCrit = 1D-5
    MaxDisp = 1.0_rfreal

    ! ... simplicity
    TMgrid => region%TMgrid(ng)
    TMstate => region%TMstate(ng)
    input  => TMgrid%input
    
    
    ! ... predictor step
    ! ... compute the tangent stiffness matrix from ...
    ! ... intermal loads

    call ResetMatrix(2)

    !print*,'getting stiffness matrix'
    timer = MPI_WTIME_Hack()
    call GetTangentStiffness(region,ng)
    !print*,'getting stiffness matrix',(MPI_WTIME_Hack() - timer)
    region%mpi_timings(ng)%TM_lhs_structural = region%mpi_timings(ng)%TM_lhs_structural + (MPI_WTIME_Hack() - timer)

    ! ... compute the external laod
    timer = MPI_WTIME_Hack()
    call GetExternalLoadFromTensor(region,ng)
    region%mpi_timings(ng)%TM_rhs_structural = region%mpi_timings(ng)%TM_rhs_structural + (MPI_WTIME_Hack() - timer)

    ! ... assemble the external load vector, 2, 4 = structural problem, external load
    Call FinalVectorAssembly(input%TMSolve, 2, 4)

    ! ... external loads
    timer = MPI_WTIME_Hack()
    call GetTensorLoadTangent(region, ng)
    region%mpi_timings(ng)%TM_lhs_structural = region%mpi_timings(ng)%TM_lhs_structural + (MPI_WTIME_Hack() - timer)

    ! ... assemble the LHS matrix, 2 = structural problem
    call FinalMatrixAssembly(region%input%TMSolve, 2)

    ! ... assemble the RHS vector, 2,2 = structural problem, RHS
    Call FinalVectorAssembly(input%TMSolve, 2, 2)

    ! ... add the augmented mass matrix, 4/dt^2*M, to get dynamic stiffness matrix, 2 = structural
    call GetDynamicLHSRHS(region, ng, 2)

    ! ... make initial geuss at solution
    Call InitGuessSoln(region, ng, 2)

    ! ... impose displacement boundary conditions
    timer = MPI_WTIME_Hack()
    call GetDisplacementBCs(region, ng)
    region%mpi_timings(ng)%TM_rhs_structural = region%mpi_timings(ng)%TM_rhs_structural + (MPI_WTIME_Hack() - timer)

    ! ... solve for the predictor
    timer = MPI_WTIME_Hack()
    !print*,'solving'
    call StructuralSolve(region, ng, MaxDisp,.true.)
    region%mpi_timings(ng)%TM_solve_structural = region%mpi_timings(ng)%TM_solve_structural + (MPI_WTIME_Hack() - timer)
    !print*,'solve done',(MPI_WTIME_Hack() - timer)

    ! ... update the velocity and acceleration
    call UpdateStructuralRates(region,ng,1)

    ! ... add the predictor to the current solution
    call GetSoln(region, ng, 2)

    ! ... reset the LHS, solution and RHS
    Call ResetMatrix(2)

    ! ... exhange the solution
    TMgrid => region%TMgrid(ng)
    TMstate => region%TMstate(ng)
    allocate(F(TMgrid%nPtsLoc))
    do dir = 1, TMgrid%ND
       do p = 1, TMgrid%nPtsLoc
          F(p) = 0.0_rfreal
          F(p) = TMstate%q(p,dir)
       end do
       call Ghost_Cell_Exchange_TM(region, F, ng)
       do p = 1, TMgrid%nPtsLoc
          TMstate%q(p,dir) = F(p)
       end do
    end do
    deallocate(F)

    ! ... compute the internal load
    timer = MPI_WTIME_Hack()
    call GetInternalLoad(region,ng)    
    region%mpi_timings(ng)%TM_rhs_structural = region%mpi_timings(ng)%TM_rhs_structural + (MPI_WTIME_Hack() - timer)
    

    ! ... compute the external laod
    timer = MPI_WTIME_Hack()
    call GetExternalLoadFromTensor(region,ng)
    region%mpi_timings(ng)%TM_rhs_structural = region%mpi_timings(ng)%TM_rhs_structural + (MPI_WTIME_Hack() - timer)

    ! ... assemble the RHS vector
    Call FinalVectorAssembly(input%TMSolve, 2, 2)

    ! ... assemble the external load vector
    Call FinalVectorAssembly(input%TMSolve, 2, 4)

    ! ... calculate the residual
    Call GetDynamicResidual(region,ng)
    
    ! ... calculate the initial residual
    call GetRes(InitRes)
    InitRes = max(InitRes,1.0_rfreal)

    if(TMstate%NewInitRes .eqv. .true.) then
       TMstate%InitRes = InitRes
       TMstate%NewInitRes = .false.
    else
       TMstate%InitRes = max(TMstate%InitRes,InitRes)
       InitRes = TMstate%InitRes
    end if

    if(region%TMRank == 0) write(*,'(A,D13.6)') 'Residual after predictor = ',InitRes
    
    ! ... initialize the residual
    Residual = InitRes

    NRit = 0
    ! ... Newton-Rhapson loop
    do while(((abs(Residual/InitRes) > NRConvCrit) .and. abs(Residual) > 1.0D-5 .and. (MaxDisp > 1D-14) .and. NRit < 10) .or. NRit < 2)
       NRit = NRit + 1
       
       ! ... compute the tangent stiffness matrix from ...
       ! ... intermal loads
       timer = MPI_WTIME_Hack()
       !print*,'getting stiffness'
       call GetTangentStiffness(region,ng)
       !print*,'stiff done',(MPI_WTIME_Hack() - timer)
       region%mpi_timings(ng)%TM_lhs_structural = region%mpi_timings(ng)%TM_lhs_structural + (MPI_WTIME_Hack() - timer)
       
       ! ... external loads
       timer = MPI_WTIME_Hack()
       call GetTensorLoadTangent(region, ng)
       region%mpi_timings(ng)%TM_lhs_structural = region%mpi_timings(ng)%TM_lhs_structural + (MPI_WTIME_Hack() - timer)

       ! ... assemble the LHS matrix, 2 = structural problem
       call FinalMatrixAssembly(region%input%TMSolve, 2)
    
       ! ... add the augmented mass matrix, 4/dt^2*M, to get dynamic stiffness matrix, 2 = structual, true = lhs only
       call GetDynamicLHSRHS(region, ng, 2,.true.)

       ! ... make initial geuss at solution
       !Call InitGuessSoln(region, ng, 2)

       ! ... impose displacement boundary conditions
       timer = MPI_WTIME_Hack()
       call GetDisplacementBCs(region, ng)
       region%mpi_timings(ng)%TM_rhs_structural = region%mpi_timings(ng)%TM_rhs_structural + (MPI_WTIME_Hack() - timer)

       ! ... solve it!
       timer = MPI_WTIME_Hack()
       !print*,'solving'
       call StructuralSolve(region, ng, MaxDisp)
       !print*,'solve done',(MPI_WTIME_Hack() - timer)
       region%mpi_timings(ng)%TM_solve_structural = region%mpi_timings(ng)%TM_solve_structural + (MPI_WTIME_Hack() - timer)

       ! ... update the velocity and acceleration
       call UpdateStructuralRates(region,ng,2)

       ! ... Find flux of mechanical power into the plate
       if(region%input%SaveSolidData == TRUE) call ComputeSolidData(PowerIn,KinEnergy)
       
       ! ... update the structural solution
       call GetSoln(region, ng, 2)
       
       Call ResetMatrix(2)

       !       call graceful_exit(region%myrank, 'done')
       ! ... exhange the solution
       TMgrid => region%TMgrid(ng)
       TMstate => region%TMstate(ng)
       allocate(F(TMgrid%nPtsLoc))
       do dir = 1, TMgrid%ND
          do p = 1, TMgrid%nPtsLoc
             F(p) = 0.0_rfreal
             F(p) = TMstate%q(p,dir)
          end do
!          print*,'before,dir,region%TMRank,F(:)',dir,region%TMRank,(p,F(p),p=1,TMgrid%nPtsLoc)
          call Ghost_Cell_Exchange_TM(region, F, ng)
          do p = 1, TMgrid%nPtsLoc
             TMstate%q(p,dir) = F(p)
          end do
!          print*,'after,dir,region%TMRank,F(:)',dir,region%TMRank,(p,F(p),p=1,TMgrid%nPtsLoc)
       end do
       deallocate(F)

       ! ... compute the internal load
       timer = MPI_WTIME_Hack()
       call GetInternalLoad(region,ng)
       region%mpi_timings(ng)%TM_rhs_structural = region%mpi_timings(ng)%TM_rhs_structural + (MPI_WTIME_Hack() - timer)
       
       timer = MPI_WTIME_Hack()
       call GetExternalLoadFromTensor(region,ng)
       region%mpi_timings(ng)%TM_rhs_structural = region%mpi_timings(ng)%TM_rhs_structural + (MPI_WTIME_Hack() - timer)

       Call FinalVectorAssembly(input%TMSolve, 2, 2)
       Call FinalVectorAssembly(input%TMSolve, 2, 4)

       ! ... calculate the residual
       Call GetDynamicResidual(region,ng)

       ! ... get the residual
       call GetRes(Residual)
       InitRes = max(InitRes,Residual)
!       call graceful_exit(region%myrank, 'here')
       if(region%TMRank == 0) write(*,'(A,I2,3(A,D13.6))') 'PlasComCM: NR iteration = ', NRit, ' Residual = ',Residual,&
            ' Relative Residual = ',abs(Residual/InitRes),' Max. Disp. = ',MaxDisp 
       !Call TMwrite_plot3d_file(region, 'TMq',NRit)       
    end do
    ! ... record Newton-Rhapson data for output to screen
    TMstate%numNRsteps = NRit
    TMState%FinalRes = Residual

    ! ... save solid data
    if(input%SaveSolidData == TRUE) then
       region%TMstate(ng)%numDataSample = region%TMstate(ng)%numDataSample + 1
       region%TMstate(ng)%SDTime(region%TMstate(ng)%numDataSample) = region%TMstate(ng)%dt_struct*dble((region%global%main_ts_loop_index-1)/region%input%StructSolnFreq)
       region%TMstate(ng)%PowerIn(region%TMstate(ng)%numDataSample) = PowerIn
       region%TMstate(ng)%KinEnergy(region%TMstate(ng)%numDataSample) = KinEnergy
    end if

!    Call ResetMatrix(2)
  end Subroutine DynamicStructural

  Subroutine InitializeDynamicStructural(region, ng)
    
    USE ModDataStruct
    USE ModGlobal
    USE ModTMEOM
    USE ModTMEOMBC
    USE ModPETSc
    USE ModTMIO    
    USE ModMPI
   
    Implicit None

    Type(t_region), pointer :: region
    Integer :: ng

    ! ... local variables
    Type(t_smixt), pointer :: TMstate
    Real(rfreal) :: MaxAccel, timer
    Integer :: ND, nPtsLoc
    Logical :: file_exist

    ! ... simplicity
    TMstate => region%TMstate(ng)
    ND      =  region%input%ND
    nPtsLoc =  region%TMgrid(ng)%nPtsLoc

    if(region%TMRank == 0) write(*,'(A)') 'Initializing structural data ... '

    Call ResetMatrix(3)

    ! ... calculate mass matrix
    timer = MPI_WTIME_Hack()
    !print*,'getting mass'
    Call GetMs(region,ng)
    !print*,'mass done',(MPI_WTIME_Hack() - timer)
    region%mpi_timings(ng)%TM_lhs_structural = region%mpi_timings(ng)%TM_rhs_structural + (MPI_WTIME_Hack() - timer)

              
    ! ... assemble the mass matrix, 3 = mass matrix
    Call FinalMatrixAssembly(region%input%TMSolve, 3)

    !Allocate(region%TMstate(ng)%qdot(nPtsLoc,2*ND))
    !Allocate(region%TMstate(ng)%P3Ddot(region%TMgrid(ng)%nCells,2*ND))
    
    INQUIRE(file=region%input%TMrestartdot_fname,EXIST=file_exist)

    if(region%input%TMRestart == TRUE .and. file_exist == .true.) then

       ! ... this is a restart and the restart data is present       
       ! ... read acceleration, velocity from restart
       ! ... fill velocity
       call EnterStructuralRates(region, ng, 1)
       ! ... fill acceleration
       call EnterStructuralRates(region, ng, 2)
       ! ... fill external load
       call EnterStructuralRates(region, ng, 3)

    elseif(region%input%TMRestart == FALSE .or. file_exist == .false.) then
       ! ... if this is the first timestep,
       ! ... then need to solve for initial acceleration
       ! ... initial velocity assumed to be zero

       ! ... compute the external load, needed at timestep n = 1
       timer = MPI_WTIME_Hack()
       call GetExternalLoadFromTensor(region,ng)
       region%mpi_timings(ng)%TM_rhs_structural = region%mpi_timings(ng)%TM_rhs_structural + (MPI_WTIME_Hack() - timer)
       
       ! ... assemble the external load vector, 2 = structural problem
       ! ... 5 = external load vector, save as bs_extOld 
       Call FinalVectorAssembly(region%input%TMSolve, 2, 5)

       ! ... compute the internal load
       timer = MPI_WTIME_Hack()
       call GetInternalLoad(region,ng)
       region%mpi_timings(ng)%TM_rhs_structural = region%mpi_timings(ng)%TM_rhs_structural + (MPI_WTIME_Hack() - timer)

       ! ... solve for the acceleration
       timer = MPI_WTIME_Hack()
       !print*,'solving'
       call GetInitialAcceleration(region, ng, MaxAccel)
       !print*,'solve done',(MPI_WTIME_Hack() - timer)
       region%mpi_timings(ng)%TM_solve_structural = region%mpi_timings(ng)%TM_solve_structural + (MPI_WTIME_Hack() - timer)

       ! ... reset As, bs, xs
        call ResetMatrix(2)

       if(region%input%TMInitialCondition == TRUE)then
          ! ... velocity initial condition
          ! ... fill velocity
          call EnterStructuralRates(region, ng, 1)

       end if
    end if


  end Subroutine InitializeDynamicStructural

End Module ModTMSingleStep
