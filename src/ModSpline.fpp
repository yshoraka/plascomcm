! Copyright (c) 2015, University of Illinois at Urbana-Champaign, XPACC
! License: MIT, http://opensource.org/licenses/MIT
module ModSpline

  USE ModDataStruct
  USE ModGlobal
  implicit none

  private

  public :: GenerateSpline
  public :: SplineInterp

contains

  subroutine GenerateSpline(Xs, Ys, Spline, DerivLower, DerivUpper)

    real(rfreal), dimension(:), intent(in) :: Xs
    real(rfreal), dimension(:), intent(in) :: Ys
    type(t_nat_spline), intent(out) :: Spline
    real(rfreal), intent(in), optional :: DerivLower, DerivUpper

    integer :: i
    integer :: N
    real(rfreal), dimension(:), allocatable :: APrev, ADiag, ANext
    real(rfreal), dimension(:), allocatable :: B

    N = size(Xs)

    Spline%n = N

    allocate(Spline%x(N))
    allocate(Spline%y(N))
    allocate(Spline%dx(N-1))
    allocate(Spline%second_deriv(N))

    Spline%x = Xs
    Spline%y = Ys

    do i = 1, N-1
      Spline%dx(i) = Xs(i+1) - Xs(i)
    end do

    allocate(APrev(2:N))
    allocate(ADiag(N))
    allocate(ANext(1:N-1))
    allocate(B(N))

    ! Use specified first derivative if present, otherwise set second derivative to 0
    if (present(DerivLower)) then
      ADiag(1) = 2._rfreal * Spline%dx(1)
      ANext(1) = Spline%dx(1)
      B(1) = 6._rfreal * ((Ys(2) - Ys(1))/Spline%dx(1) - DerivLower)
    else
      ADiag(1) = 1._rfreal
      ANext(1) = 0._rfreal
      B(1) = 0._rfreal
    end if

    ! Use specified first derivative if present, otherwise set second derivative to 0
    if (present(DerivUpper)) then
      APrev(N) = Spline%dx(N-1)
      ADiag(N) = 2._rfreal * Spline%dx(N-1)
      B(N) = 6._rfreal * (DerivUpper - (Ys(N) - Ys(N-1))/Spline%dx(N-1))
    else
      APrev(N) = 0._rfreal
      ADiag(N) = 1._rfreal
      B(N) = 0._rfreal
    end if

    do i = 2, N-1
      APrev(i) = Spline%dx(i-1)
      ADiag(i) = 2._rfreal * (Spline%dx(i-1) + Spline%dx(i))
      ANext(i) = Spline%dx(i)
      B(i) = 6._rfreal * ((Ys(i+1) - Ys(i))/Spline%dx(i) - (Ys(i) - Ys(i-1))/Spline%dx(i-1))
    end do

    call TriDiagonalSolve(APrev, ADiag, ANext, B, Spline%second_deriv)

  end subroutine GenerateSpline

  subroutine TriDiagonalSolve(APrev, ADiag, ANext, B, X)

    real(rfreal), dimension(2:), intent(in) :: APrev
    real(rfreal), dimension(:), intent(in) :: ADiag
    real(rfreal), dimension(:), intent(in) :: ANext
    real(rfreal), dimension(:), intent(in) :: B
    real(rfreal), dimension(:), intent(out) :: X

    integer :: i
    integer :: N
    real(rfreal), dimension(:), allocatable :: C
    real(rfreal), dimension(:), allocatable :: D

    N = size(ADiag)

    allocate(C(N-1))
    allocate(D(N))

    C(1) = ANext(1)/ADiag(1)
    do i = 2, N-1
      C(i) = ANext(i)/(ADiag(i) - APrev(i)*C(i-1))
    end do

    D(1) = B(1)/ADiag(1)
    do i = 2, N
      D(i) = (B(i) - APrev(i)*D(i-1))/(ADiag(i) - APrev(i)*C(i-1))
    end do

    X(N) = D(N)
    do i = N-1, 1, -1
      X(i) = D(i) - C(i)*X(i+1)
    end do

  end subroutine TriDiagonalSolve

  function SplineInterp(Spline, X) result(Y)

    type(t_nat_spline), intent(in) :: Spline
    real(rfreal), intent(in) :: X
    real(rfreal) :: Y

    integer :: i
    real(rfreal) :: Alpha
    real(rfreal), dimension(4) :: C

    if (X < Spline%x(1)) then
      Y = Spline%y(1)
      return
    else if (X > Spline%x(Spline%n)) then
      Y = Spline%y(Spline%n)
      return
    end if

    i = FindInterval(Spline%x, X)

    Alpha = (X - Spline%x(i))/Spline%dx(i)

    C(1) = 1._rfreal - Alpha
    C(2) = Alpha
    C(3) = -(Spline%dx(i)**2)/6._rfreal * Alpha * (Alpha - 1._rfreal) * (Alpha - 2._rfreal)
    C(4) = (Spline%dx(i)**2)/6._rfreal * (Alpha + 1._rfreal) * Alpha * (Alpha - 1._rfreal)

    Y = C(1) * Spline%y(i) + C(2) * Spline%y(i+1) + C(3) * Spline%second_deriv(i) + &
      C(4) * Spline%second_deriv(i+1)

  end function SplineInterp

  function FindInterval(Xs, X) result(Interval)

    real(rfreal), dimension(:), intent(in) :: Xs
    real(rfreal), intent(in) :: X
    integer :: Interval

    integer :: NextInterval
    integer :: CandidateInterval

    Interval = 1
    NextInterval = size(Xs)

    do while (NextInterval > Interval+1)

      CandidateInterval = (Interval + NextInterval)/2

      if (X > Xs(CandidateInterval)) then
        Interval = CandidateInterval
      else
        NextInterval = CandidateInterval
      end if

    end do

  end function FindInterval

end module ModSpline
