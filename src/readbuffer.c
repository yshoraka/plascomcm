/* Copyright (c) 2015, University of Illinois at Urbana-Champaign, XPACC */
/* License: MIT, http://opensource.org/licenses/MIT */
#ifdef CMAKE_BUILD
#include "FC.h"
#else /* CMAKE_BUILD */
#include "plascomcmconf.h"
#endif /* CMAKE_BUILD */
#include <stdio.h>
#include <stdlib.h>

#ifdef CMAKE_BUILD
#define READBUFFER_FC FC_GLOBAL(readbuffer, READBUFFER)
#else /* CMAKE_BUILD */
#define READBUFFER_FC FC_FUNC(readbuffer, READBUFFER)
#endif /* CMAKE_BUILD */
void READBUFFER_FC(char *path, int *offset, int *max_pos, int *char_buffer_len, char *char_buffer,
  int *iat_eof)
{
  FILE *fp = NULL;
  int n = 0, j; 
  char single_char;

  /* open file */
  if ( (fp = fopen(path,"r")) == NULL ) perror("Unable to open file in readbuffer");

  /* fseek */
  fseek(fp, (*offset)*sizeof(char), SEEK_SET);

  /* read while we can */
  while (fscanf(fp,"%c",&single_char) != EOF) {
    char_buffer[n++] = single_char;
    if (n == (*char_buffer_len)) {
      *max_pos = n;
      fclose(fp);
      return;
    } 
  } 

  *max_pos = n;
  fclose(fp); 
  
  return;

}
