#ifndef EF_OPERATOR_H
#define EF_OPERATOR_H

#include <petscdmda.h>

#include "ef_metric.h"
#include "ef_level.h"
#include "ef_fd.h"

/**
 * @file: ef_operator.h
 *
 * This is used to create an operator object that is
 * responsible for assembling the matrix for all rows
 * that are not owned by boundary conditions.
 */

static inline double have(double v1, double v2)
{
	return 2.0/((1.0/v1) + (1.0/v2));
}


typedef struct ef_operator_{
	PetscErrorCode (*assemble)(struct ef_operator_ *op, Mat A, DM da);
	ef_level *level;
	ef_fd *fd;
	int axisymmetric;
} ef_operator;


/**
 * Constructor for the operator object
 *
 * @param[out] efop operator object to create
 * @param level level object
 * @param fd finite difference coefficient object
 * @param nd number of dimensions
 */
PetscErrorCode ef_operator_create(ef_operator **efop, ef_level *level, ef_fd *fd, int nd);


/**
 * Used to assemble the interior of the matrix
 * (everywhere not owned by a boundary condition)
 *
 * @param op operator object
 * @param A PETSc matrix
 * @param da PETSc DM object
 */
PetscErrorCode ef_operator_assemble(ef_operator *op, Mat A, DM da);


/**
 * Destroys data structures owned by the operator
 */
PetscErrorCode ef_operator_destroy(ef_operator*);


#endif
