! Copyright (c) 2015, University of Illinois at Urbana-Champaign, XPACC
! License: MIT, http://opensource.org/licenses/MIT
!-----------------------------------------------------------------------
!
! ModInitialCondition.f90
!
! - basic code for the initial condition
!
! Revision history
! - 01 Oct 2008 : DJB : initial code
!
! $Header: /cvsroot/genx/Codes/RocfloCM/Source/ModInitialCondition.f90,v 1.38 2011/10/20 16:55:21 bodony Exp $
!
!-----------------------------------------------------------------------
Module ModInitialCondition

CONTAINS

  Subroutine InitialCondition(region)

    USE ModGlobal
    USE ModDataStruct
    USE ModMPI

    Implicit None

    type(t_region), pointer :: region

    ! ... local variables
    type(t_mixt_input), pointer :: input
    type(t_grid), pointer :: grid
    type(t_mixt), pointer :: state
    Real(rfreal), Dimension(:,:,:,:,:), Pointer :: X
    Integer, Dimension(:,:,:,:), Pointer :: IBLANK
    Character(LEN=2) :: prec, gf, vf, ib
    Integer :: ngrid, ii, I, J, K, L0, Nx, Ny, Nz, NDIM, M, ftype, ng, alloc_stat
    Integer, Dimension(:,:), Pointer :: ND
    Real(rfreal) :: exp_fac, vortex_u, vortex_v, vortex_rho, vortex_p, M_vortex, gas, gas1, gas2
    Real(rfreal) :: L_vortex, gamma, bg_u, bg_v, bg_rho, bg_p, x0, y0, z0, r, u_inf, eta, Xcomp, Ycomp, Zcomp
    Real(rfreal) :: L_pulse_x, L_pulse_y, L_pulse_z, L_pulse, A_pulse, tanh_fac, gamma1, gamma2
    Real(rfreal) :: bg_temperature, hotspot_pressure, hotspot_ux, hotspot_uy, hotspot_temperature
    Real(rfreal) :: hotspot_density, hotspot_rhoE, new_pressure, new_temperature, new_ux, new_uy, ux, uy, uz
    Real(rfreal) :: new_density, new_rhoE, delta1, fac, rho1, rho2, p1, p2, u1, u2, y1, y2, yf, a1, t1, t2, M1, M2
    Real(rfreal), pointer :: temperature(:)
    Real(rfreal) :: Mach, pinf, Tinf, Tjet, pjet, rjet, cjet, ujet, radius, rinf, rhoE1, rhoE2, y_wall


    ! ... loop over all of the grids
    do ng = 1, region%nGrids

      grid  => region%grid(ng)
      state => region%state(ng)
      input => grid%input

      do ii = 1, size(state%cv,1)

        ! ... set the time
        state%time(ii) = 0.0_rfreal

        ! ... initialize gas variables
        state%gv(ii,1) = input%GamRef

        ! ... initialize CFL
        state%cfl(ii) = input%CFL

        ! ... set the initial condition to a quiescent state
        state%cv(ii,1) = 1.0_rfreal
        state%cv(ii,2) = 0.00_rfreal
        if (input%ND >= 2) state%cv(ii,3) = 0.00_rfreal
        if (input%ND == 3) state%cv(ii,4) = 0.00_rfreal * state%cv(ii,1)
        state%cv(ii,grid%ND+2) = input%EintRef/(input%sndspdref*input%sndspdref)

      enddo

      do k = 1, input%nAuxVars
        do ii = 1, grid%nCells
          state%auxVars(ii,k) = 0.0_rfreal
        end do
      end do

      ! ... These cases are selected by the input parameter INITFLOW_NAME in the PlasComCM.inp file
      SELECT CASE( TRIM(input%initflow_name) )
      CASE ("ARN2")
        IF(region%myrank == 0) WRITE(*,*) 'PlasComCM: Initializing flow for ARN2 nozzle.'  
        ! ... ARN2 nozzle
        !        if ( .false. ) then
        do ii = 1, size(state%cv,1)
          ! ... tanh-variable
          delta1 =  1.0_rfreal * 1.00_rfreal   ! ... thickness
          x0     = -4.0_rfreal * 1.00_rfreal   ! ... initial condition
          fac    = 0.5_rfreal / tanh(x0/delta1)

          if (grid%XYZ(ii,2) <= 3) then

            ! ... density
            rho1 = 1.0_8
            rho2 = 1.5_8
            state%cv(ii,1) = fac * (rho2-rho1) * tanh((grid%XYZ(ii,1)-x0)/delta1) + 0.5_rfreal * (rho2 + rho1)

            ! ... pressure
            p1   = 1.0_8 / state%gv(ii,1)
            p2   = 2.5_8 * p1
            state%cv(ii,grid%ND+2) = fac * (p2-p1) * tanh((grid%XYZ(ii,1)-x0)/delta1) + 0.5_rfreal * (p2 + p1)

            ! ... velocity
            u1   = 0.0_rfreal
            u2   = 0.1_rfreal
            state%cv(ii,2) = fac * (u2-u1) * tanh((grid%XYZ(ii,1)-x0)/delta1) + 0.5_rfreal * (u2 + u1)
            if (grid%ND >= 2) state%cv(ii,3) = 0.0_rfreal
            if (grid%ND == 3) state%cv(ii,4) = 0.0_rfreal

            ! ... convert back to conservative variables
            state%cv(ii,2) = state%cv(ii,1) * state%cv(ii,2)
            if (grid%ND >= 2) state%cv(ii,3) = state%cv(ii,1) * state%cv(ii,3)
            if (grid%ND == 3) state%cv(ii,4) = state%cv(ii,1) * state%cv(ii,4)
            state%cv(ii,grid%ND+2) = state%cv(ii,grid%ND+2) / (state%gv(ii,1)-1.0_8)
            do j = 1, grid%ND
              state%cv(ii,grid%ND+2) = state%cv(ii,grid%ND+2) + 0.5_8 * state%cv(ii,1+j) * state%cv(ii,1+j) / state%cv(ii,1)
            end do
          endif
        enddo

      CASE("UNIFORM")

        M1 = input%initflow_mach
        If (input%caseID == 'UNIFORM_TIME_RAMP') Then
          M1 = 0.5_rfreal * input%initflow_mach * (1.0_rfreal + tanh((0.0_rfreal - input%initflow_tstart)/input%initflow_tramp))
        End If
        new_temperature = input%TempRef
        gamma = input%GamRef

        IF(region%myrank == 0) then
          WRITE(*,*) 'PlasComCM: Initializing uniform flow.'  
          WRITE(*,*) 'PlasComCM: Setting initial flow to M = ',M1,'theta = ', &
               input%initflow_theta, 'phi = ', input%initflow_phi
        ENDIF

        ! ... convert input values to radians for trig functions
        input%initflow_theta = input%initflow_theta*4.0_8*ATAN(1.0_8)/180.0_rfreal
        input%initflow_phi   = input%initflow_phi*4.0_8*ATAN(1.0_8)/180.0_rfreal

        ! ... calculate the directional modifiers
        Xcomp = COS(input%initflow_phi) * SIN(input%initflow_theta)
        Ycomp = SIN(input%initflow_phi) * SIN(input%initflow_theta)
        Zcomp = COS(input%initflow_theta)

        do ii = 1, size(state%cv,1)

          ! ... initialize flow velocity with specified direction
          state%cv(ii,2) = M1 * Xcomp * state%cv(ii,1)
          IF(grid%ND >= 2) THEN
            state%cv(ii,3) =  M1 * Ycomp * state%cv(ii,1)
            IF(grid%ND > 2) THEN
              state%cv(ii,4) = M1 * Zcomp * state%cv(ii,1)
            ENDIF
          ENDIF

          ! ... Initialize E 
          state%cv(ii,grid%ND+2) = (1.0_rfreal/gamma)/(gamma-1.0_rfreal)
          ! ... add kinetic energy to E
          do j = 1, grid%ND
            state%cv(ii,grid%ND+2) = state%cv(ii,grid%ND+2) + 0.5_8 * state%cv(ii,1) * state%cv(ii,j+1) * state%cv(ii,j+1)

          end do

        end do

      CASE("LINER-1")
        ! ... liner boundary layer

        if(region%myrank == 0) print*,'PlasComCM: using a liner BL with freestream M = ',M1
        M1     = input%initflow_mach
        gamma = input%GamRef
        delta1 =  0.067_8
        y0     =  0.0_8
        do ii = 1, size(state%cv,1)
          eta    = (grid%XYZ(ii,2) - y0) / delta1
          if (eta <= 1.0_8) then
            state%cv(ii,2) = state%cv(ii,1) * M1 *(2.0_8 * eta - 2.0_8 * eta * eta * eta + eta * eta * eta * eta)
          else
            state%cv(ii,2) = state%cv(ii,1) * M1
          end if
          state%cv(ii,3) = 0.00_rfreal * state%cv(ii,1)
          state%cv(ii,4) = 0.00_rfreal * state%cv(ii,1)
          state%cv(ii,grid%ND+2) = (1.0_rfreal/gamma)/(gamma-1.0_rfreal)
          do j = 1, grid%ND
            state%cv(ii,grid%ND+2) = state%cv(ii,grid%ND+2) + 0.5_rfreal * state%cv(ii,1+j) * state%cv(ii,1+j) / state%cv(ii,1)
          end do
        end do

        ! ... liner boundary layer
      CASE("LINER-2")
        IF(region%myrank == 0) WRITE(*,*) 'PlasComCM: Initializing flow for liner-2.'  
        do ii = 1, size(state%cv,1)
          state%cv(ii,2) = 0.10_rfreal * (MAX(0.4_rfreal,grid%XYZ(ii,3)) - 0.4_rfreal) * state%cv(ii,1) + 0.00_rfreal
          state%cv(ii,3) = 0.00_rfreal * state%cv(ii,1)
          state%cv(ii,4) = 0.00_rfreal * state%cv(ii,1)
          state%cv(ii,grid%ND+2) = (1.0_rfreal/1.4_rfreal)/(1.4_rfreal-1.0_rfreal)
          do j = 1, grid%ND
            state%cv(ii,grid%ND+2) = state%cv(ii,grid%ND+2) + 0.5_rfreal * state%cv(ii,1+j)**2 / state%cv(ii,1)
          end do
        end do

      CASE("POHLHAUSEN-BL")
        If (region%myrank == 0) Write(*,'(A)') 'PlasComCM: Initializing flow for POHLHAUSEN-BL'
        gamma = input%GamRef
        y_wall = 0.0_8
        Do ii = 1, grid%nCells
          If (input%caseID == 'WSMR') Then
            y_wall = 1.0_8 * exp(-(grid%XYZ(ii,1)**2/1.0_8**2))
          End If
          eta = (grid%XYZ(ii,2)-y_wall) / (8.0_8 / 3.0_8 * input%initflow_deltastar)
          If (eta <= 1.0_8) Then
            Ux  = (1.5_8 * eta - 0.5_8 * eta * eta * eta) * input%initflow_mach
          Else
            Ux  = input%initflow_mach
          End If
          Uy  = 0.0_8
          Uz  = 0.0_8
          state%cv(ii,1) = 1.0_8
          state%cv(ii,2) = state%cv(ii,1) * Ux
          state%cv(ii,3) = state%cv(ii,1) * Uy
          state%cv(ii,4) = state%cv(ii,1) * Uz
          state%cv(ii,grid%ND+2) = (1.0_8/gamma)/(gamma-1.0_8) &
                                 + 0.5_8 * (state%cv(ii,2) * Ux + state%cv(ii,3) * Uy + state%cv(ii,4) * Uz)
          state%cvTarget(ii,1:grid%ND+2) = state%cv(ii,1:grid%ND+2)
        End Do

        ! ... rotor-stator initial conditions
      CASE("ROTOR-STATOR")
        IF(region%myrank == 0) WRITE(*,*) 'PlasComCM: Initializing flow for rotor-stator.'  
        do ii = 1, size(state%cv,1)
          tanh_fac = 1.0_rfreal-0.5_rfreal*(1.0_rfreal + tanh(grid%XYZ(ii,1) - 5.0_rfreal))
          state%cv(ii,2) = 0.33_rfreal * state%cv(ii,1)
          state%cv(ii,3) = 0.58_rfreal * state%cv(ii,1) * tanh_fac
          do j = 1, grid%ND
            state%cv(ii,grid%ND+2) = state%cv(ii,grid%ND+2) + 0.5_rfreal * state%cv(ii,1+j)**2 / state%cv(ii,1)
          end do
        end do

        ! ... set the initial target
        if ( .false. ) then
          state%cvTarget(ii,1) = 1.0_rfreal
          state%cvTarget(ii,2) = 0.50_rfreal
          state%cvTarget(ii,3) = 0.00_rfreal
          state%cvTarget(ii,4) = 0.00_rfreal * state%cvTarget(ii,1)
          state%cvTarget(ii,grid%ND+2) = (1.0_rfreal/1.4_rfreal)/(1.4_rfreal-1.0_rfreal)
          do j = 1, grid%ND
            state%cvTarget(ii,grid%ND+2) = state%cvTarget(ii,grid%ND+2) &
                 + 0.5_rfreal * state%cvTarget(ii,1+j)**2 / state%cvTarget(ii,1)
          end do
        end if

        ! ... NASA Jet
      CASE("NASA_JET")
        IF(region%myrank == 0) WRITE(*,*) 'PlasComCM: Initializing flow for NASA Jet'

        ! ... compute the target state first 
        gamma  = input%gamref
        Mach   = input%initflow_Mach
        delta1 = input%initflow_delta
        rinf   = 1.0_8
        pinf   = 1.0_8 / gamma
        Tinf   = 1.0_8 / (gamma - 1.0_8)
        Tjet   = Tinf / (1.0_8 + 0.5_8 * (gamma-1.0_8) * Mach**2)
        pjet   = pinf
        rjet   = gamma * pjet / (gamma - 1.0_8) / Tjet
        cjet   = dsqrt(gamma * pjet/rjet)
        ujet   = Mach * cjet   

        do ii = 1, size(state%cv,1)

          ! ... compute radius 
          radius = dsqrt(grid%XYZ(ii,1)**2+grid%XYZ(ii,2)**2)
          eta = (0.5_8 - radius) / delta1

          ! ... check if in nozzle
          if (radius <= 0.5_8 .and. grid%XYZ(ii,3) <= 0.0_8) then
            if (eta <= 1.0_8) then
              state%cvTarget(ii,1) = rjet
              state%cvTarget(ii,2) = rjet * 0.0_8
              state%cvTarget(ii,3) = rjet * 0.0_8
              state%cvTarget(ii,4) = rjet * ujet * (1.5_8 * eta - 0.5_8 * eta**3)
              state%cvTarget(ii,5) = pjet / (gamma - 1.0_8)
            else
              state%cvTarget(ii,1) = rjet
              state%cvTarget(ii,2) = rjet * 0.0_8
              state%cvTarget(ii,3) = rjet * 0.0_8
              state%cvTarget(ii,4) = rjet * ujet
              state%cvTarget(ii,5) = pjet / (gamma - 1.0_8)
            end if
          else
            state%cvTarget(ii,1) = rinf
            state%cvTarget(ii,2) = rinf * 0.0_8
            state%cvTarget(ii,3) = rinf * 0.0_8
            state%cvTarget(ii,4) = rinf * 0.01_8
            state%cvTarget(ii,5) = pinf / (gamma - 1.0_8)
          end if

          ! ... compute total energy
          do j = 1, grid%ND
            state%cvTarget(ii,5) = state%cvTarget(ii,5) + 0.5_rfreal * state%cvTarget(ii,1+j)**2 / state%cvTarget(ii,1)
          end do

        end do

        ! ... repeat but for the initial condition
        gamma  = input%gamref
        Mach   = 0.01_8
        delta1 = input%initflow_delta
        rinf   = 1.0_8
        pinf   = 1.0_8 / gamma
        Tinf   = 1.0_8 / (gamma - 1.0_8)
        Tjet   = Tinf / (1.0_8 + 0.5_8 * (gamma-1.0_8) * Mach**2)
        pjet   = pinf
        rjet   = gamma * pjet / (gamma - 1.0_8) / Tjet
        cjet   = dsqrt(gamma * pjet/rjet)
        ujet   = Mach * cjet   

        do ii = 1, size(state%cv,1)

          ! ... compute radius 
          radius = dsqrt(grid%XYZ(ii,1)**2+grid%XYZ(ii,2)**2)
          eta = (0.5_8 - radius) / delta1

          ! ... check if in nozzle
          if (radius <= 0.5_8 .and. grid%XYZ(ii,3) <= 0.0_8) then
            if (eta <= 1.0_8) then
              state%cv(ii,1) = rjet
              state%cv(ii,2) = rjet * 0.0_8
              state%cv(ii,3) = rjet * 0.0_8
              state%cv(ii,4) = rjet * ujet * (1.5_8 * eta - 0.5_8 * eta**3)
              state%cv(ii,5) = pjet / (gamma - 1.0_8)
            else
              state%cv(ii,1) = rjet
              state%cv(ii,2) = rjet * 0.0_8
              state%cv(ii,3) = rjet * 0.0_8
              state%cv(ii,4) = rjet * ujet
              state%cv(ii,5) = pjet / (gamma - 1.0_8)
            end if
          else
            state%cv(ii,1) = rinf
            state%cv(ii,2) = rinf * 0.0_8
            state%cv(ii,3) = rinf * 0.0_8
            state%cv(ii,4) = rinf * 0.01_8
            state%cv(ii,5) = pinf / (gamma - 1.0_8)
          end if

          ! ... compute total energy
          do j = 1, grid%ND
            state%cv(ii,5) = state%cv(ii,5) + 0.5_rfreal * state%cv(ii,1+j)**2 / state%cv(ii,1)
          end do

        end do


        ! CAA Benchmark 2, Category 1, Problem 2
      CASE("CAA2_1_2")
        IF(region%myrank == 0) WRITE(*,*) 'PlasComCM: Initializing flow with CAA Benchmark 2, Category 1, Problem 2.'  
!        x0        = 4.0_dp
!        y0        = 0.0_dp
!        L_pulse_x = 0.2_dp
!        L_pulse_y = 0.2_dp
!        A_pulse   = 0.001_dp
        x0 = input%initflow_xloc
        y0 = input%initflow_yloc
        L_pulse_x = input%initflow_xwidth
        L_pulse_y = input%initflow_ywidth
        A_pulse   = input%initflow_Amplitude
        do k = 1, grid%nCells
          r = (grid%XYZ(k,1) - x0)**2/L_pulse_x**2 + (grid%XYZ(k,2) - y0)**2/L_pulse_y**2
          state%cv(k,1) = 1.0_rfreal + A_pulse * exp(-r*log(2.0_rfreal))
          state%cv(k,2) = 0.0_rfreal * state%cv(k,1)
          state%cv(k,3) = 0.0_rfreal
          state%cv(k,grid%ND+2) = (A_pulse*exp(-r*log(2.0_rfreal))+1.0/state%gv(k,1)) / (state%gv(k,1) - 1.0_rfreal) + 0.5 * state%cv(k,2)**2 / state%cv(k,1)
        end do


        ! ... CAA Benchmark 1, Category 4, Problem 1
      CASE("CAA1_4_1")
        IF(region%myrank == 0) WRITE(*,*) 'PlasComCM: Initializing flow with CAA Benchmark 1, Category 4, Problem 1.'  
        x0 = input%initflow_xloc
        y0 = input%initflow_yloc
        L_pulse_x = input%initflow_xwidth
        L_pulse_y = input%initflow_ywidth
        A_pulse   = input%initflow_Amplitude
!        x0        =    0.0_rfreal
!        y0        =   25.0_rfreal
!        L_pulse_x =    5.0_rfreal
!        L_pulse_y =    5.0_rfreal
!        A_pulse   =  0.001_rfreal
        do k = 1, grid%nCells
          r = (grid%XYZ(k,1) - x0)**2/L_pulse_x**2 + (grid%XYZ(k,2) - y0)**2/L_pulse_y**2
          state%cv(k,1) = 1.0_rfreal + A_pulse * exp(-r*log(2.0_rfreal))
          state%cv(k,2) = 0.5_rfreal * state%cv(k,1)
          state%cv(k,3) = 0.0_rfreal
          state%cv(k,grid%ND+2) = (A_pulse*exp(-r*log(2.0_rfreal))+1.0/state%gv(k,1)) / (state%gv(k,1) - 1.0_rfreal) + 0.5 * state%cv(k,2)**2 / state%cv(k,1)
        end do

        ! ... CAA Benchmark 3, Category 1, Problem 2
      CASE("CAA3_1_2")
        IF(region%myrank == 0) WRITE(*,*) 'PlasComCM: Initializing flow with CAA Benchmark 3, Category 1, Problem 2.'  
        p1 = 1.0_dp / 1.4_dp
        p2 = 0.61_dp
        rho1 = 1.0_dp
        rho2 = 0.8746_dp
        u1   = 0.2006533_dp
        u2   = 0.4274_dp
        do k = 1, grid%nCells
          if (grid%XYZ(k,1) >= 0) then
            grid%area(k) = 0.536572_dp - 0.198086_dp * exp(-log(2.0_dp)*(grid%xyz(k,1)/0.6_dp)**2)
          else
            grid%area(k) = 1.0_dp - 0.661514_dp * exp(-log(2.0_dp)*(grid%xyz(k,1)/0.6_dp)**2)
          end if
          state%cv(k,1) = rho1 + (rho2-rho1)*(grid%xyz(k,1)+10.0_dp)/20.0_dp
          state%cv(k,2) = (u1 + (u2-u1)*(grid%xyz(k,1)+10.0_dp)/20.0_dp)*state%cv(k,1)
          state%cv(k,grid%ND+2) = (p1 + (p2-p1)*(grid%xyz(k,1)+10.0_dp)/20.0_dp) / (state%gv(k,1) - 1.0_rfreal) + 0.5 * state%cv(k,2)**2 / state%cv(k,1)
        end do

        ! ... set the initial perturbation
        ! ... isentropic pulse with zero mean flow
      CASE("ISENTROPIC_PULSE")
        IF(region%myrank == 0) WRITE(*,*) 'PlasComCM: Initializing flow to isentropic pulse.'  

!        x0        =   0.0_rfreal
!        y0        =   0.0_rfreal
!        z0        =   0.0_rfreal
!        L_pulse_x =   0.10_rfreal
!        L_pulse_y =   0.10_rfreal
!        L_pulse_z =   0.20_rfreal
!        A_pulse   =  0.01_rfreal

        x0 = input%initflow_xloc
        y0 = input%initflow_yloc
        z0 = input%initflow_zloc
        L_pulse_x = input%initflow_xwidth
        L_pulse_y = input%initflow_ywidth
        L_pulse_z = input%initflow_zwidth
        A_pulse   = input%initflow_Amplitude
       do k = 1, grid%nCells

          SELECT CASE (grid%ND)
          CASE (3)
            r = (grid%XYZ(k,1) - x0)**2/L_pulse_x**2 + (grid%XYZ(k,2) - y0)**2/L_pulse_y**2 + (grid%XYZ(k,3) - z0)**2/L_pulse_z**2
            uz = state%cv(k,4) / state%cv(k,1)
            uy = state%cv(k,3) / state%cv(k,1)
            ux = state%cv(k,2) / state%cv(k,1)
          CASE (2)
            r = (grid%XYZ(k,1) - x0)**2/L_pulse_x**2 + (grid%XYZ(k,2) - y0)**2/L_pulse_y**2
            uy = state%cv(k,3) / state%cv(k,1)
            ux = state%cv(k,2) / state%cv(k,1)
          CASE (1)
            r = (grid%XYZ(k,1) - x0)**2/L_pulse_x**2
            ux = state%cv(k,2) / state%cv(k,1)
          END SELECT

          state%cv(k,1) = state%cv(k,1) + A_pulse * exp(-r)

          SELECT CASE (grid%ND)
          CASE (3)
            state%cv(k,2) = state%cv(k,1) * ux
            state%cv(k,3) = state%cv(k,1) * uy
            state%cv(k,4) = state%cv(k,1) * uz
          CASE (2)
            state%cv(k,2) = state%cv(k,1) * ux
            state%cv(k,3) = state%cv(k,1) * uy
          CASE (1)
            state%cv(k,2) = state%cv(k,1) * ux
          END SELECT

          state%cv(k,grid%ND+2) = (state%cv(k,1)**1.4_rfreal)/(1.4_rfreal)/(1.4_rfreal-1.0_rfreal)
          do j = 1, grid%ND
            state%cv(k,grid%ND+2) = state%cv(k,grid%ND+2) + 0.5_rfreal * state%cv(k,1+j)**2 / state%cv(k,1)
          end do

        end do

      CASE("ENTROPIC_PULSE")
        IF(region%myrank == 0) WRITE(*,*) 'PlasComCM: Initializing flow to entropic pulse.'  

        x0 = input%initflow_xloc
        y0 = input%initflow_yloc
        z0 = input%initflow_zloc
        L_pulse_x = input%initflow_xwidth
        L_pulse_y = input%initflow_ywidth
        L_pulse_z = input%initflow_zwidth
        A_pulse   = input%initflow_Amplitude
        gamma  = input%gamref

        ! ... convert input values to radians for trig functions
        input%initflow_theta = input%initflow_theta*4.0_8*ATAN(1.0_8)/180.0_rfreal
        input%initflow_phi   = input%initflow_phi*4.0_8*ATAN(1.0_8)/180.0_rfreal

        ! ... calculate the directional modifiers
        Xcomp = COS(input%initflow_phi) * SIN(input%initflow_theta)
        Ycomp = SIN(input%initflow_phi) * SIN(input%initflow_theta)
        Zcomp = COS(input%initflow_theta)

        do k = 1, grid%nCells

          SELECT CASE (grid%ND)
          CASE (3)
            r = (grid%XYZ(k,1) - x0)**2/L_pulse_x**2 + (grid%XYZ(k,2) - y0)**2/L_pulse_y**2 + (grid%XYZ(k,3) - z0)**2/L_pulse_z**2
          CASE (2)
            r = (grid%XYZ(k,1) - x0)**2/L_pulse_x**2 + (grid%XYZ(k,2) - y0)**2/L_pulse_y**2
          CASE (1)
            r = (grid%XYZ(k,1) - x0)**2/L_pulse_x**2
          END SELECT

          state%cv(k,1) = state%cv(k,1) + A_pulse * exp(-r)
          state%cv(k,2) = input%initflow_Mach * Xcomp * state%cv(k,1)
          if (grid%ND >= 2) state%cv(k,3) = input%initflow_Mach * Ycomp * state%cv(k,1)
          if (grid%ND == 3) state%cv(k,4) = input%initflow_Mach * Zcomp * state%cv(k,1)
          state%cv(k,grid%ND+2) = 1.0_8 / gamma / (gamma - 1.0_8)
          do j = 1, grid%ND
            state%cv(k,grid%ND+2) = state%cv(k,grid%ND+2) + 0.5_rfreal * state%cv(k,1+j)**2 / state%cv(k,1)
          end do

        end do


        ! ... set the initial condition for the Sample Problem (gzagaris)
        ! ... isentropic pulse with zero mean flow
      CASE("GAUSSIAN_PULSE")
        IF(region%myrank == 0) WRITE(*,*) 'PlasComCM: Initializing flow to Gaussian pulse.'  
        x0 = input%initflow_xloc
        y0 = input%initflow_yloc
        z0 = input%initflow_zloc
        L_pulse_x = input%initflow_xwidth
        L_pulse_y = input%initflow_ywidth
        L_pulse_z = input%initflow_Zwidth
        A_pulse   = input%initflow_Amplitude
!        x0        =   0.5_rfreal
!        y0        =   0.75_rfreal
!        L_pulse_x =   0.1_rfreal
!        L_pulse_y =   0.1_rfreal
!        A_pulse   =   0.01_rfreal
        do k = 1, grid%nCells
          gamma = input%GamRef
          r = (grid%XYZ(k,1) - x0)**2/L_pulse_x**2 
          IF(grid%ND >= 2) r = r + (grid%XYZ(k,2) - y0)**2/L_pulse_y**2
          IF(grid%ND > 2) r = r + (grid%XYZ(k,3) - z0)**2/L_pulse_z**2
          state%cv(k,1) = state%cv(k,1) + A_pulse * exp(-r)
          state%cv(k,grid%ND+2) = (state%cv(k,1)**gamma)/(gamma)/(gamma-1.0_rfreal)
        end do

      CASE("UNIFORM_FLOW_WITH_GAUSSIAN_SCALAR")
        IF(region%myrank == 0) WRITE(*,*) 'PlasComCM: Initializing flow to UNIFORM_FLOW_WITH_GAUSSIAN_SCALAR.'  
        M1    = input%initflow_mach
        gamma = input%GamRef

        IF(region%myrank == 0) then
          WRITE(*,*) 'PlasComCM: Initializing uniform flow.'  
          WRITE(*,*) 'PlasComCM: Setting initial flow to M = ',M1,'theta = ', &
               input%initflow_theta, 'phi = ', input%initflow_phi
        ENDIF

        ! ... convert input values to radians for trig functions
        input%initflow_theta = input%initflow_theta*4.0_8*ATAN(1.0_8)/180.0_rfreal
        input%initflow_phi   = input%initflow_phi*4.0_8*ATAN(1.0_8)/180.0_rfreal

        ! ... calculate the directional modifiers
        Xcomp = COS(input%initflow_phi) * SIN(input%initflow_theta)
        Ycomp = SIN(input%initflow_phi) * SIN(input%initflow_theta)
        Zcomp = COS(input%initflow_theta)

        do ii = 1, size(state%cv,1)

          ! ... initialize flow velocity with specified direction
          state%cv(ii,2) = M1 * Xcomp * state%cv(ii,1)
          IF(grid%ND >= 2) THEN
            state%cv(ii,3) =  M1 * Ycomp * state%cv(ii,1)
            IF(grid%ND > 2) THEN
              state%cv(ii,4) = M1 * Zcomp * state%cv(ii,1)
            ENDIF
          ENDIF

          ! ... Initialize E 
          state%cv(ii,grid%ND+2) = (1.0_rfreal/gamma)/(gamma-1.0_rfreal)
          ! ... add kinetic energy to E
          do j = 1, grid%ND
            state%cv(ii,grid%ND+2) = state%cv(ii,grid%ND+2) + 0.5_8 * state%cv(ii,1) * state%cv(ii,j+1) * state%cv(ii,j+1)
          end do

        end do

        ! ... scalar gaussian
        x0 = input%initflow_xloc
        y0 = input%initflow_yloc
        z0 = input%initflow_zloc
        L_pulse_x = input%initflow_xwidth
        L_pulse_y = input%initflow_ywidth
        L_pulse_z = input%initflow_Zwidth
        A_pulse   = input%initflow_Amplitude
        do k = 1, input%nAuxVars
          do ii = 1, grid%nCells
            r = (grid%XYZ(ii,1) - x0)**2/L_pulse_x**2 
            IF(grid%ND >= 2) r = r + (grid%XYZ(ii,2) - y0)**2/L_pulse_y**2
            IF(grid%ND >  2) r = r + (grid%XYZ(ii,3) - z0)**2/L_pulse_z**2
            state%auxVars(ii,k) = state%cv(ii,1) * A_pulse * exp(-r)
          end do
        end do

        ! ... set the initial perturbation
        ! ... sinusoidal
      CASE("SINUSOIDAL_PULSE")
         IF(region%myrank == 0) WRITE(*,*) 'PlasComCM: Initializing flow to sinusoidal pulse.'  
         x0 = input%initflow_xloc
         y0 = input%initflow_yloc
         z0 = input%initflow_zloc
         L_pulse_x = input%initflow_xwidth
         L_pulse_y = input%initflow_ywidth
         L_pulse_z = input%initflow_Zwidth
         A_pulse   = input%initflow_Amplitude
!         x0        =   0.0_rfreal
!        y0        =   0.0_rfreal
!        z0        =   0.0_rfreal
!        L_pulse_x =   0.1_rfreal
!        L_pulse_y =   0.2_rfreal
!        L_pulse_z =   0.1_rfreal
!        A_pulse   =   0.01_rfreal
        do k = 1, grid%nCells
          state%cv(k,3) = state%cv(k,3) + state%cv(k,1) * A_pulse * &
               SIN(1.0_rfreal * TWOPI / grid%periodicL(1) * grid%XYZ(k,1)) * EXP(-grid%XYZ(k,2)**2/L_pulse_y**2)
          state%cv(k,grid%ND+2) = (state%cv(k,1)**1.4_rfreal)/(1.4_rfreal)/(1.4_rfreal-1.0_rfreal)
        end do


        ! ... convecting vortex with non-zero mean flow
      CASE("CONVECTING_VORTEX")
        IF(region%myrank == 0) WRITE(*,*) 'PlasComCM: Initializing flow to convecting vortex with non-zero mean flow.'  
        M_vortex = 0.05_rfreal ! vortex strength
        L_vortex = 1.00_rfreal ! vortex size
        x0       = -5.0_rfreal
        y0       =  0.0_rfreal
        do k = 1, grid%nCells
          r = (grid%XYZ(k,1)-x0)**2 + (grid%XYZ(k,2)-y0)**2
          exp_fac  = exp((1.0_rfreal - r/L_vortex**2)/2.0_rfreal)

          bg_u = state%cv(k,2) / state%cv(k,1)
          bg_v = state%cv(k,3) / state%cv(k,1)

          gamma = 1.4_rfreal

          vortex_u   = -M_vortex * (grid%XYZ(k,2)-y0) * exp_fac + bg_u
          vortex_v   =  M_vortex * (grid%XYZ(k,1)-x0) * exp_fac + bg_v
          vortex_p   = (1.0_rfreal/gamma)*(1.0_rfreal - (gamma - 1.0_rfreal)/2.0_rfreal * M_vortex**2 * exp_fac**2)**(gamma/(gamma-1.0_rfreal))
          vortex_rho = (gamma * vortex_p)**(1.0_rfreal/gamma)

          state%cv(k,1) = vortex_rho
          state%cv(k,2) = vortex_rho * vortex_u
          state%cv(k,3) = vortex_rho * vortex_v
          state%cv(k,4) = vortex_p/(gamma-1.0_rfreal) + 0.5_rfreal * vortex_rho * (vortex_u**2 + vortex_v**2)
        end do


        ! ... 1-D shock
      CASE("1D_SHOCK")
        IF(region%myrank == 0) WRITE(*,*) 'PlasComCM: Initializing flow with 1-D shock.'  

        ! ... baseline properties
        gas1   = 286.9_8
        gas2   = 286.9_8
        ! gas2   = 2077.0_8
        rho1   = 1.225_8
        p1     = 101325.0_8
        t1     = p1 / (rho1 * gas1)
        gamma1 = 1.40_8
        gamma2 = 1.40_8
        ! gamma2 = 1.66_8
        a1     = sqrt(gamma1 * gas1 * t1)

        ! ... one-dimensional shock
        delta1 =  0.01_rfreal * 1.00_rfreal   ! ... thickness
        x0     = -4.70_rfreal * 1.00_rfreal   ! ... initial condition
        fac    =  0.50_rfreal / tanh(x0/delta1)

        ! ... temperature array
        allocate(temperature(size(state%cv,1)))

        ! ... ratio of specific heats
        do k = 1, grid%nCells
          state%gv(k,1) = fac * (gamma2-gamma1) * tanh((grid%XYZ(k,1)-x0)/delta1) + 0.5_rfreal * (gamma2+gamma1)
          state%levelSet(k) = state%gv(k,1)
        end do

        ! ... pressure (dimensional)
        p2   = 8.5_8 * p1
        do k = 1, grid%nCells
          state%cv(k,grid%ND+2) = fac * (p2-p1) * tanh((grid%XYZ(k,1)-x0)/delta1) + 0.5_rfreal * (p2 + p1)
        end do

        ! ... compute the temperature
        do k = 1, grid%nCells
          temperature(k) = t1
        end do

        ! ... density
        ! rho2 = 8.5_8 * rho1
        do k = 1, grid%nCells
          ! ... local gas constant
          gas = fac * (gas2-gas1) * tanh((grid%XYZ(k,1)-x0)/delta1) + 0.5_rfreal * (gas2+gas1)
          ! state%cv(k,1) = fac * (rho2-rho1) * tanh((grid%XYZ(k,1)-x0)/delta1) + 0.5_rfreal * (rho2 + rho1)
          state%cv(k,1) = state%cv(k,grid%ND+2) / (gas * temperature(k))
        end do

        ! ... velocity
        u1   = 0.0_rfreal
        u2   = 0.0_rfreal ! 247.152_8
        do k = 1, grid%nCells
          state%cv(k,2) = fac * (u2-u1) * tanh((grid%XYZ(k,1)-x0)/delta1) + 0.5_rfreal * (u2 + u1)
          if (grid%ND >= 2) state%cv(k,3) = 0.0_rfreal
          if (grid%ND == 3) state%cv(k,4) = 0.0_rfreal
        end do

        ! ... force temperature to wall temperature
!!$        if (grid%ND >= 2) then
!!$          y0 = 0.0_rfreal
!!$          yf = 1.0_rfreal
!!$          y1 = y0 + 0.10_rfreal * (yf-y0)
!!$          y2 = y0 + 0.90_rfreal * (yf-y0)
!!$          temperature(:) = (temperature(:) - t1) * 0.5_rfreal * (tanh(5.0_rfreal*(grid%XYZ(:,2)-y1)/(y1-y0))+tanh(5.0_rfreal*(y2-grid%XYZ(:,2))/(yf-y2))) + t1
!!$        end if

        ! ... change density such pressure is constant
        do k = 1, grid%nCells

          ! ... conservative variables
          state%cv(k,2) = state%cv(k,1) * state%cv(k,2)
          if (grid%ND >= 2) state%cv(k,3) = state%cv(k,1) * state%cv(k,3)
          if (grid%ND == 3) state%cv(k,4) = state%cv(k,1) * state%cv(k,4)
          state%cv(k,grid%ND+2) = state%cv(k,grid%ND+2) / (state%gv(k,1) - 1.0_rfreal) + 0.5_rfreal * (state%cv(k,2)*state%cv(k,2))/state%cv(k,1)
          if (grid%ND >= 2) state%cv(k,grid%ND+2) = state%cv(k,grid%ND+2) + 0.5_rfreal * (state%cv(k,3)*state%cv(k,3))/state%cv(k,1)
          if (grid%ND == 3) state%cv(k,grid%ND+2) = state%cv(k,grid%ND+2) + 0.5_rfreal * (state%cv(k,4)*state%cv(k,3))/state%cv(k,1)
        end do

        ! ... non-dimensionalize
        do k = 1, grid%nCells
          state%cv(k,1) = state%cv(k,1) / rho1
          state%cv(k,2) = state%cv(k,2) / (rho1 * a1)
          if (grid%ND >= 2) state%cv(k,3) = state%cv(k,3) / (rho1 * a1)
          if (grid%ND == 3) state%cv(k,4) = state%cv(k,4) / (rho1 * a1)
          state%cv(k,grid%ND+2) = state%cv(k,grid%ND+2) / (rho1 * a1 * a1)
        end do

        deallocate(temperature)


        ! ... Joanna's post-shock state
      CASE("POST-SHOCK")
        IF(region%myrank == 0) WRITE(*,*) 'PlasComCM: Initializing flow to post-shock state.'  

        do i = 1, grid%nCells
          gamma  = state%gv(i,1)
          R      = 287.0_8
          rho1   = 1.225_rfreal
          p1     = 101325.0_rfreal
          t1     = p1 / (rho1 * R)
          M1     = (29.49_8) * grid%XYZ(i,1)**(-0.8827_8) + 1.0_8
          M2     = sqrt(((gamma-1.0_8)*M1**2+2)/(2.0_8*gamma*M1**2-(gamma-1.0_8)))
          U1     = M1 * sqrt(gamma*p1/rho1);
          rho2   = rho1 * (gamma+1.0_8)*M1**2/((gamma-1.0_8)*M1**2+2)
          U2     = U1 / (rho2/rho1)
          P2     = p1 * (2.0_8*gamma*M1**2-(gamma-1.0_8))/(gamma+1.0_8)
          T2     = p2 / (rho2 * R)
          U2     = U1 - U2;

          ! ... non-dimensionalize
          a1 = sqrt(1.4_rfreal * p1 / rho1)         
          state%cv(i,1) = rho2 / rho1
          state%cv(i,2) = rho2*U2 / (rho1 * a1)
          If (input%ND >= 2) state%cv(i,3) = 0.0_8
          state%cv(i,grid%ND+2) = (p2/(gamma-1.0_8) + 0.5_8*rho2*u2**2) / (rho1 * a1**2)
        end do

      CASE("STATIONARY_NORMAL_SHOCK")
        if (region%myrank == 0) Write (*,'(A)') 'PlasComCM: Initializing with stationary normal shock'
        gamma = input%GamRef
        rho1  = 1.0_rfreal
        p1    = 1.0_rfreal / gamma
        M1    = input%initflow_Mach
        u1    = M1
        rhoE1 = p1 / (gamma - 1.0_rfreal) + 0.5_rfreal * rho1 * u1 * u1
        rho2  = rho1 * (gamma + 1.0_rfreal) * M1**2 / ( (gamma - 1.0_rfreal) * M1**2 + 2)
        u2    = u1 * (rho1 / rho2)
        p2    = p1 * (2.0_rfreal * gamma * M1**2 - (gamma - 1.0_rfreal)) / (gamma + 1.0_rfreal)
        rhoE2 = p2 / (gamma - 1.0_rfreal) + 0.5_rfreal * rho2 * u2 * u2
 
        do i = 1, grid%nCells
          fac = 0.5_rfreal * (1.0_rfreal + tanh((grid%XYZ(i,1) - input%initflow_xloc)/input%initflow_xwidth))
          state%cv(i,1) = rho1 + (rho2-rho1) * fac
          state%cv(i,2) = rho1*u1 + (rho2*u2 - rho1*u1) * fac
          if (grid%ND >= 2) state%cv(i,3) = 0.0_rfreal 
          if (grid%ND == 3) state%cv(i,4) = 0.0_rfreal
          state%cv(i,grid%ND+2) = rhoE1 + (rhoE2 - rhoE1) * fac
        end do 

      CASE("QUIESCENT")
      CASE("quiescient")
      CASE DEFAULT
        IF(region%myrank == 0) WRITE(*,*) 'PlasComCM: Warning:: Flow initialized to quiescient state.'  
      END SELECT
    end do

    ! ... Adjoint N-S
    ! ... loop over all of the grids
    if (input%AdjNS .OR. input%AdjOptim) then
      do ng = 1, region%nGrids

        grid => region%grid(ng)
        state => region%state(ng)

        ! ... by default, all adjoint variables are zero
        do ii = 1, size(state%av,1)
          state%av(ii,1) = 0.0_rfreal
          do j = 1,grid%ND
            state%av(ii,j+1) = 0.0_rfreal
          end do ! j
          state%av(ii,grid%ND+2) = 0.0_rfreal
        end do ! ii

        ! ... adjoint sound wave
        if (.false.) then
          A_pulse = 0.0001_rfreal ! ... magnitude of adjoint sound wave
          L_pulse_x = 1.0_rfreal ! ... wavelength of adjoint sound wave
          state%time(:) = 10000.0_rfreal ! ... for backward time integration

          state%av(:,grid%ND+2) = A_pulse * DSIN(TWOPI/L_pulse_x * (grid%xyz(:,1) - state%time(:)))
          state%av(:,2) = 1.0_rfreal/(state%gv(:,1) - 1.0_rfreal) * state%av(:,grid%ND+2)
        end if ! .false.

      end do ! ng
    end if ! input%AdjNS

  End Subroutine InitialCondition

  Subroutine Add_Hotspot(region)

    USE ModGlobal
    USE ModDataStruct
    USE ModMPI

    type(t_region), pointer :: region

    ! ... local variables
    type(t_mixt_input), pointer :: input
    type(t_grid), pointer :: grid
    type(t_mixt), pointer :: state
    Real(rfreal), Dimension(:,:,:,:,:), Pointer :: X
    Integer, Dimension(:,:,:,:), Pointer :: IBLANK
    Character(LEN=2) :: prec, gf, vf, ib
    Integer :: ngrid, ii, I, J, K, L0, Nx, Ny, Nz, NDIM, M, ftype, ng, alloc_stat
    Integer, Dimension(:,:), Pointer :: ND
    Real(rfreal) :: exp_fac, vortex_u, vortex_v, vortex_rho, vortex_p, M_vortex
    Real(rfreal) :: L_vortex, gamma, bg_u, bg_v, bg_rho, bg_p, x0, y0, r, u_inf
    Real(rfreal) :: L_pulse_x, L_pulse_y, A_pulse
    Real(rfreal) :: bg_temperature, hotspot_pressure, hotspot_ux, hotspot_uy, hotspot_temperature
    Real(rfreal) :: hotspot_density, hotspot_rhoE, new_pressure, new_temperature, new_ux, new_uy
    Real(rfreal) :: new_density, new_rhoE, bg_rhoE, bg_density, bg_ux, bg_uy, bg_pressure

    ! ... simplicity
    input => region%input

    ! ... hot spot parameters
    A_pulse   =  0.10_rfreal  ! fraction of mean temperature
    x0        = -0.50_rfreal  ! initial x-location
    y0        =  0.00_rfreal  ! initial y-location
    L_pulse_x =  0.25_rfreal  ! x-width
    L_pulse_y =  0.25_rfreal  ! y-width

    ! ... loop over all of the grids
    do ng = 1, region%nGrids

      grid => region%grid(ng)
      state => region%state(ng)

      !  ADD HOT SPOT VARIABLES HERE (replace l0 with k)
      !  ADD VARIABLES to real(rfreal) ... line above
      do k = 1, grid%nCells

        ! ... original values
        gamma          = state%gv(k,1)
        bg_rhoE        = state%cv(k,4)
        bg_density     = state%cv(k,1)
        bg_ux          = state%cv(k,2) / bg_density
        bg_uy          = state%cv(k,3) / bg_density
        bg_pressure    = (bg_rhoE - 0.5_rfreal*bg_density*(bg_ux**2+bg_uy**2))*(gamma-1.0)
        bg_temperature = bg_pressure/bg_density*gamma/(gamma-1.0)

        ! ... define the hotspot
        hotspot_pressure    = 0.0
        hotspot_ux          = 0.0
        hotspot_uy          = 0.0
        hotspot_temperature = A_pulse * exp(-((grid%XYZ(k,1) - x0)/L_pulse_x)**2 - ((grid%XYZ(k,2) - y0)/L_pulse_y)**2)
        hotspot_density     = (bg_pressure+hotspot_pressure)*gamma/((gamma-1.0_rfreal)*(bg_temperature+hotspot_temperature))-bg_density
        hotspot_rhoE        = (bg_pressure+hotspot_pressure)/(gamma-1.0_rfreal) + 0.5_rfreal*(hotspot_density+bg_density)*(bg_ux**2+bg_uy**2)-bg_rhoE

        ! ... add the hotspot to the state
        new_pressure    = bg_pressure + hotspot_pressure
        new_temperature = bg_temperature + hotspot_temperature
        new_ux          = bg_ux + hotspot_ux
        new_uy          = bg_uy + hotspot_uy
        new_density     = bg_density + hotspot_density
        new_rhoE        = bg_rhoE + hotspot_rhoE

        ! ... update the state
        state%cv(k,1) = new_density
        state%cv(k,2) = new_density * new_ux
        state%cv(k,3) = new_density * new_uy
        state%cv(k,4) = new_rhoE

      end do

    end do

  End Subroutine Add_Hotspot

  Subroutine LinInitialCondition(region)

    USE ModGlobal
    USE ModDataStruct
    USE ModMPI

    Implicit None

    type(t_region), pointer :: region

    ! ... local variables
    type(t_mixt_input), pointer :: input
    type(t_grid), pointer :: grid
    type(t_mixt), pointer :: state, mean
    Real(rfreal), Dimension(:,:,:,:,:), Pointer :: X
    Integer, Dimension(:,:,:,:), Pointer :: IBLANK
    Character(LEN=2) :: prec, gf, vf, ib
    Integer :: ngrid, ii, I, J, K, L0, Nx, Ny, Nz, NDIM, M, ftype, ng, alloc_stat
    Integer, Dimension(:,:), Pointer :: ND
    Real(rfreal) :: exp_fac, vortex_u, vortex_v, vortex_rho, vortex_p, M_vortex, gas, gas1, gas2
    Real(rfreal) :: L_vortex, gamma, bg_u, bg_v, bg_rho, bg_p, x0, y0, z0, r, u_inf
    Real(rfreal) :: L_pulse_x, L_pulse_y, L_pulse_z, A_pulse, tanh_fac, gamma1, gamma2
    Real(rfreal) :: bg_temperature, hotspot_pressure, hotspot_ux, hotspot_uy, hotspot_temperature
    Real(rfreal) :: hotspot_density, hotspot_rhoE, new_pressure, new_temperature, new_ux, new_uy, ux, uy, uz
    Real(rfreal) :: new_density, new_rhoE, delta1, fac, rho1, rho2, p1, p2, u1, u2, y1, y2, yf, a1, t1, t2, M1, M2
    Real(rfreal), pointer :: temperature(:)

    ! ... loop over all of the grids
    do ng = 1, region%nGrids

      grid  => region%grid(ng)
      state => region%state(ng)
      input => grid%input
      mean  => region%mean(ng)

      do ii = 1, size(state%cv,1)

        ! ... set the time
        state%time(ii) = 0.0_rfreal

        ! ... initialize gas variables
        state%gv(ii,1) = input%GamRef

        ! ... initialize CFL
        state%cfl(ii) = input%CFL

        ! ... set the initial condition to a quiescent state
        state%cv(ii,1) = 0.00_rfreal
        state%cv(ii,2) = 0.00_rfreal
        if (input%ND >= 2) state%cv(ii,3) = 0.00_rfreal
        if (input%ND == 3) state%cv(ii,4) = 0.00_rfreal
        state%cv(ii,grid%ND+2) = 0.00_rfreal

        ! ... zero disturbance target
        state%cvTarget(ii,1) = 0.00_rfreal
        state%cvTarget(ii,2) = 0.00_rfreal
        if (input%ND >= 2) state%cvTarget(ii,3) = 0.00_rfreal
        if (input%ND == 3) state%cvTarget(ii,4) = 0.00_rfreal
        state%cvTarget(ii,grid%ND+2) = 0.00_rfreal

      end do

      ! ... isentropic pulse
      if ( .false. ) then

        x0        =  40.0_rfreal
        y0        =   0.0_rfreal
        z0        =   0.0_rfreal
        L_pulse_x =   1.10_rfreal
        L_pulse_y =   1.10_rfreal
        L_pulse_z =   0.20_rfreal
        A_pulse   =  0.01_rfreal

        do k = 1, grid%nCells

          SELECT CASE (grid%ND)
          CASE (3)
            r = (grid%XYZ(k,1) - x0)**2/L_pulse_x**2 + (grid%XYZ(k,2) - y0)**2/L_pulse_y**2 + (grid%XYZ(k,3) - z0)**2/L_pulse_z**2
            uz = mean%cv(k,4) / mean%cv(k,1)
            uy = mean%cv(k,3) / mean%cv(k,1)
            ux = mean%cv(k,2) / mean%cv(k,1)
          CASE (2)
            r = (grid%XYZ(k,1) - x0)**2/L_pulse_x**2 + (grid%XYZ(k,2) - y0)**2/L_pulse_y**2
            uy = mean%cv(k,3) / mean%cv(k,1)
            ux = mean%cv(k,2) / mean%cv(k,1)
          CASE (1)
            r = (grid%XYZ(k,1) - x0)**2/L_pulse_x**2
            ux = mean%cv(k,2) / mean%cv(k,1)
          END SELECT

          state%cv(k,1) = A_pulse * exp(-r)

          SELECT CASE (grid%ND)
          CASE (3)
            state%cv(k,2) = state%cv(k,1) * ux
            state%cv(k,3) = state%cv(k,1) * uy
            state%cv(k,4) = state%cv(k,1) * uz
          CASE (2)
            state%cv(k,2) = state%cv(k,1) * ux
            state%cv(k,3) = state%cv(k,1) * uy
          CASE (1)
            state%cv(k,2) = state%cv(k,1) * ux
          END SELECT

          state%cv(k,grid%ND+2) = mean%gv(k,1)*mean%dv(k,1)*mean%dv(k,3)*state%cv(k,1)

        end do
      end if
    end do

End Subroutine LinInitialCondition

Subroutine read_eigenfunction(myrank, grid, input)

  USE ModGlobal
  USE ModDataStruct
  USE ModMPI

  Implicit None

  ! ... global variables
  integer :: myrank
  type(t_mixt_input), pointer :: input
  type(t_grid), pointer :: grid

  ! ... local variables
  integer :: N_read(MAX_ND), j, m, l, n

  ! ... open file
  Open(unit=10,file=trim(input%bcic_eigenfunction_fname),status='old',form='unformatted')

  ! ... read and check dimensions
  Read(10) N_read(:)
  Do j = 1, MAX_ND
    If (N_read(j) /= grid%GlobalSize(j)) Then
      Call Graceful_Exit(myrank, 'PlasComCM: ERROR in dimensions in eigenfunction file.')
    End If
  End Do

  ! ... number of spanwise and frequency modes
  Read(10) input%M_mode, input%N_mode

  ! ... allocate
  allocate(input%qp_hat(1,grid%GlobalSize(2),5,2*input%M_mode,input%N_mode))
  allocate(input%freq_pert(input%M_mode,input%N_mode))
  allocate(input%alpha_pert(1,input%M_mode,input%N_mode))
  allocate(input%beta_pert(input%M_mode,input%N_mode))
  allocate(input%amp_pert(input%M_mode,input%N_mode))
  allocate(input%phase_pert(2*input%M_mode,input%N_mode))

  ! ... read data
  Read(10) ((((input%Qp_hat(1,J,L,M,N), J = 1, N_read(2)), L = 1, 5), M = 1, 2*input%M_mode), N = 1, input%N_mode)
  Read(10) ((input%alpha_pert(1,M,N), M = 1, input%M_mode), N = 1, input%N_mode)
  Read(10) ((   input%beta_pert(M,N), M = 1, input%M_mode), N = 1, input%N_mode)
  Read(10) ((   input%freq_pert(M,N), M = 1, input%M_mode), N = 1, input%N_mode)
  Read(10) ((    input%amp_pert(M,N), M = 1, input%M_mode), N = 1, input%N_mode)
  Read(10) ((  input%phase_pert(M,N), M = 1, 2*input%M_mode), N = 1, input%N_mode)

  ! ... close the file
  Close (10)

  Return

End Subroutine read_eigenfunction

Subroutine read_PSEeigenfunction(grid, input)

  USE ModGlobal
  USE ModDataStruct
  USE ModMPI

  Implicit None

  ! ... global variables
  type(t_mixt_input), pointer :: input
  type(t_grid), pointer :: grid

  ! ... local variables
  integer :: i, j, m, l, n, imax, Nx, Ny
  real(kind=8), pointer :: freq_pert(:,:)

  ! ... open file
  Open(unit=10,file=trim(input%bcic_eigenfunction_fname),status='old',form='unformatted')

  ! ... number of spanwise and frequency modes
  Read(10) input%M_mode, input%N_mode

  ! ... size of PSE information
  Read(10) Nx, Ny

  ! ... allocate
  allocate(input%qp_hat(Nx,Ny,5,input%M_mode,input%N_mode))
  allocate(input%freq_pert(input%M_mode,input%N_mode))
  allocate(freq_pert(input%M_mode,input%N_mode))
  allocate(input%amp_pert(input%M_mode,input%N_mode))

  ! ... read data
  Do m = 1, input%M_mode
    Do n = 1, input%N_mode
  
      ! ... frequency
      Read(10) freq_pert(M,N)
      input%freq_pert(M,N) = dcmplx(freq_pert(M,N),0.0_8)

      ! ... frequency
      Read(10) input%amp_pert(M,N)

      ! ... rho'
      Read(10) ((input%Qp_hat(i,j,1,m,n),j=1,Ny),i=1,Nx)

      ! ... (rhoux)'
      Read(10) ((input%Qp_hat(i,j,2,m,n),j=1,Ny),i=1,Nx)

      ! ... (rhouy)'
      Read(10) ((input%Qp_hat(i,j,3,m,n),j=1,Ny),i=1,Nx)

      ! ... (rhouz)'
      Read(10) ((input%Qp_hat(i,j,4,m,n),j=1,Ny),i=1,Nx)

      ! ... (rhoE)'
      Read(10) ((input%Qp_hat(i,j,5,m,n),j=1,Ny),i=1,Nx)

    End Do
  End Do

  ! ... close the file
  Close (10)

  deallocate(freq_pert)

  Return

End Subroutine read_PSEeigenfunction

Subroutine Model0_to_Model1(input,grid,state) 
! PPP: this subroutine converts Q and Qaux to account for different definitions of rhoE and rho in Model0 and Model1. The user should initialize using the
! Model0 definitions (perfect gas with constant \gamma, rhoE is sensible enthalpy), and this routine will convert rho, rhoE, and the other variables to their
! Model1 form (variable \gamma, rhoE is sensible enthalpy + enthalpy of formation)

  USE ModDataStruct
  USE ModCombustion
  USE ModModel1
  USE ModEOS

  Implicit None

  type(t_mixt_input), pointer :: input
  type(t_grid), pointer :: grid
  type(t_mixt), pointer :: state

#ifdef HAVE_CANTERA

  type(t_model1), pointer :: mod1
  real(rfreal), dimension(:,:),pointer :: cv, aux
  real(rfreal), dimension(:),allocatable :: Qaux, Qv
  real(rfreal) :: PresRef, gamma, pressure, temperature
  integer :: ND, Nc, ienergy, i

  cv => state%cv
  aux => state%auxVars
  mod1 => state%combustion%model1

  PresRef = input%PresRef
  gamma = input%GamRef
  state%gv = gamma

  ND = grid%ND
  Nc = grid%nCells
  ienergy = ND+2

  allocate(Qaux(ubound(aux,2)))
  allocate(Qv(ubound(cv,2)))

  do i = 1,Nc
     if(grid%iblank(i) == 0) cycle
     state%dv(i,3) = 1.0d0/cv(i,1) ! gets specific volume from \rho
     Qaux = aux(i,:) ! species density fractions
     pressure = (gamma - 1.0d0)*(cv(i,ND+2) - 0.5_rfreal * sum(cv(i,2:ND+1)**2)/cv(i,1) ) ! gets pressure from rhoE, rhoU
     state%dv(i,1) = pressure ! stores pressure in state%dv
     Call TRhoAux2Pgamma(1.0d0/pressure, cv(i,1), Qaux, mod1, temperature)
     temperature = 1.0d0/temperature
! TRhoAux2Pgamma gets p from p=RT. The above two lines run that in reverse, getting T from 1/T = R(1/p) (this is the model0 temp.)
     state%dv(i,2) = temperature ! model0 temp
     cv(i,ienergy) = TRhoAux2Energy(temperature, cv(i,1), Qaux, mod1) + 0.5d0 * sum(cv(i,2:ND+1)**2)/cv(i,1) 
! gets the model1 enthalpy, including the enthalpy of formations
     Qv = cv(i,:) ! only cv(ND+2) is updated from the model0 IC
     state%dv(i,2) = poly4Temperature(Qv, Qaux, mod1, input, state%dv(i,2)) ! gets the model1 temperature, starting from the model0 guess
     Call TRhoAux2Pgamma(state%dv(i,2), Qv(1), Qaux, mod1, state%dv(i,1), state%gv(i,1)) ! gets the model1 pressure and gamma
  end do
! this finishes the conversion on the interior of the IC

! now to convert the target: same idea, but we're only updating cv(:,ienergy)
  cv => state%cvTarget
  aux => state%auxVarsTarget
  do i = 1,Nc
    if(grid%iblank(i) == 0) cycle
    Qaux =aux(i,:) ! species dens. fractions
    pressure = (gamma - 1.0d0)*(cv(i,ND+2) - 0.5_rfreal * sum(cv(i,2:ND+1)**2)/cv(i,1) ) ! gets pressure from rhoE, rhoU
    Call TRhoAux2Pgamma(1.0d0/pressure, cv(i,1), Qaux, mod1, temperature)
    temperature = 1.0d0/temperature ! do the 1/p, 1/T bit
    cv(i,ienergy) = TRhoAux2Energy(temperature, cv(i,1), Qaux, mod1) + 0.5d0 * sum(cv(i,2:ND+1)**2)/cv(i,1) 
  end do
  state%cvTargetOld = state%cvTarget

#endif

End Subroutine Model0_to_Model1

End Module ModInitialCondition
