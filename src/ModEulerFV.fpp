! Copyright (c) 2015, University of Illinois at Urbana-Champaign, XPACC
! License: MIT, http://opensource.org/licenses/MIT
!----------------------------------------------------------------------
!
! ModEulerFV.f90
! 
! - code for evaluation of the right-hand side of Euler equations in curvilinear
!   coordinates using the low density Roe scheme of Einfeldt:
! 
! Reference:
! Einfeldt, et al., "On Godunov-Type Methods near Low Densities.", J. Computational Physics, 92, 273-295 (1991).
!
! Revision history
! - 06 Feb 2014 : RV : initial code
!-----------------------------------------------------------------------
MODULE ModEulerFV

  IMPLICIT NONE

  INTERFACE prepare_ghost_vector
     MODULE PROCEDURE prepare_ghost_vector_allocate_and_copy_only1
     MODULE PROCEDURE prepare_ghost_vector_allocate_and_copy_only2
     MODULE PROCEDURE prepare_ghost_vector_full
  END INTERFACE prepare_ghost_vector

CONTAINS

  SUBROUTINE exchange_ghost_cells(grid, direction, exchange_buffer)

    USE ModMPI
    USE ModDataStruct

    IMPLICIT NONE

    ! Arguments
    TYPE(t_grid), POINTER :: grid
    INTEGER, INTENT(IN) :: direction
    REAL(WP), DIMENSION(:,:), POINTER :: exchange_buffer

    ! Local variables
    INTEGER :: number_of_scalars
    INTEGER, DIMENSION(2) :: exchange_data_size
    REAL(WP), DIMENSION(:), ALLOCATABLE :: send_next, recv_prev, send_prev, recv_next
    INTEGER, DIMENSION(2) :: periodic_offset
    INTEGER :: tag_prev, tag_next, ierr
    INTEGER, DIMENSION(4) :: request
    INTEGER :: i, j, k
    INTEGER, DIMENSION(3) :: local_size, local_size_with_ghost_points

    local_size = grid%ie - grid%is + 1
    local_size_with_ghost_points = local_size + sum(grid%nGhostRHS, dim = 2)

    number_of_scalars = size(exchange_buffer, 2)

    exchange_data_size = grid%nGhostRHS(direction, :) * grid%vds(direction) * number_of_scalars
    
    ALLOCATE(send_next(MAXVAL(exchange_data_size)), recv_prev(MAXVAL(exchange_data_size)))
    ALLOCATE(send_prev(MAXVAL(exchange_data_size)), recv_next(MAXVAL(exchange_data_size)))

    periodic_offset = 0
    IF (grid%periodicStorage == OVERLAP_PERIODIC .AND. grid%periodic(direction) >= TRUE) THEN
       IF (grid%ie(direction) == grid%GlobalSize(direction)) periodic_offset(1) = 1
       IF (grid%is(direction) == 1) periodic_offset(2) = 1
    END IF

    tag_prev = direction * 10
    tag_next = tag_prev + 1

    IF (grid%cartDims(direction) > 1) THEN
       CALL MPI_Irecv(recv_prev, exchange_data_size(1), MPI_DOUBLE_PRECISION, grid%left_process(direction), tag_prev, grid%cartComm, request(1), ierr)
       CALL MPI_Irecv(recv_next, exchange_data_size(2), MPI_DOUBLE_PRECISION, grid%right_process(direction), tag_next, grid%cartComm, request(2), ierr)
    END IF

    DO k = 1, number_of_scalars
       DO j = 1, grid%vds(direction)
          DO i = local_size(direction) - grid%nGhostRHS(direction, 2) + 1, local_size(direction)
             send_next(i - (local_size(direction) - grid%nGhostRHS(direction, 2)) + grid%nGhostRHS(direction, 2) * (j - 1 + grid%vds(direction) * (k - 1))) = &
               exchange_buffer(grid%cell2ghost(grid%vec_ind(j, i - periodic_offset(1), direction), direction), k)
          END DO
       END DO
    END DO

    DO k = 1, number_of_scalars
       DO j = 1, grid%vds(direction)
          DO i = 1, grid%nGhostRHS(direction, 1)
             send_prev(i + grid%nGhostRHS(direction, 1) * (j - 1 + grid%vds(direction) * (k - 1))) = exchange_buffer(grid%cell2ghost(grid%vec_ind(j, i + periodic_offset(2), direction), direction), k)
          END DO
       END DO
    END DO

    IF (grid%cartDims(direction) > 1) THEN
       CALL MPI_Isend(send_next, exchange_data_size(2), MPI_DOUBLE_PRECISION, grid%right_process(direction), tag_prev, grid%cartComm, request(3), ierr)
       CALL MPI_Isend(send_prev, exchange_data_size(1), MPI_DOUBLE_PRECISION, grid%left_process(direction), tag_next, grid%cartComm, request(4), ierr)
       CALL MPI_Waitall(4, request, MPI_STATUSES_IGNORE, ierr)
    ELSE
       DO i = 1, exchange_data_size(2)
          recv_next(i) = send_prev(i)
       END DO
       DO i = 1, exchange_data_size(1)
          recv_prev(i) = send_next(i)
       END DO
    END IF

    DO k = 1, number_of_scalars
       DO j = 1, grid%vds(direction)
          DO i = 1, grid%nGhostRHS(direction, 1)
             exchange_buffer(grid%vec_indGhost(j, i, direction), k) = recv_prev(i + grid%nGhostRHS(direction, 1) * (j - 1 + grid%vds(direction) * (k - 1)))
          END DO
       END DO
    END DO

    DO k = 1, number_of_scalars
       DO i = local_size_with_ghost_points(direction) - grid%nGhostRHS(direction, 2) + 1, local_size_with_ghost_points(direction)
          DO j = 1, grid%vds(direction)
             exchange_buffer(grid%vec_indGhost(j, i, direction), k) = &
               recv_next(i - (local_size_with_ghost_points(direction) - grid%nGhostRHS(direction, 2)) + grid%nGhostRHS(direction, 2) * (j - 1 + grid%vds(direction) * (k - 1)))
          END DO
       END DO
    END DO
    
    DEALLOCATE(send_next, recv_prev)
    DEALLOCATE(send_prev, recv_next)
    
  END SUBROUTINE exchange_ghost_cells

  SUBROUTINE prepare_ghost_vector_full(grid, x, x_ghost, direction, interior_start, interior_end, ghost_offset, local_size_with_ghost_points, is, ie, js, je, ks, ke)

    USE ModGlobal
    USE ModDataStruct

    IMPLICIT NONE

    ! Arguments
    TYPE(t_grid), POINTER :: grid
    REAL(WP), DIMENSION(:,:), POINTER :: x, x_ghost
    INTEGER, INTENT(IN) :: direction, interior_start, interior_end
    INTEGER, DIMENSION(3), INTENT(OUT) :: ghost_offset, local_size_with_ghost_points
    INTEGER, INTENT(OUT) :: is, ie, js, je, ks, ke

    ! Local variables
    INTEGER :: i, j, k, l, number_of_scalars, last_index_offset
    INTEGER, DIMENSION(3) :: N, Ng

    N = grid%ie - grid%is + 1
    Ng = N + sum(grid%nGhostRHS, dim = 2)

    number_of_scalars = size(x, 2)

    ghost_offset = 0
    ghost_offset(direction) = grid%nGhostRHS(direction, 1)

    local_size_with_ghost_points = N
    local_size_with_ghost_points(direction) = Ng(direction)

    NULLIFY(x_ghost)
    ALLOCATE(x_ghost(product(local_size_with_ghost_points), number_of_scalars))

    last_index_offset = lbound(x, 2)

    DO l = 1, number_of_scalars
       DO k = 1, N(3)
          DO j = 1, N(2)
             DO i = 1, N(1)
                x_ghost(i + ghost_offset(1) + local_size_with_ghost_points(1) * (j - 1 + ghost_offset(2) + local_size_with_ghost_points(2) * (k - 1 + ghost_offset(3))), l) = &
                  x(i + N(1) * (j - 1 + N(2) * (k - 1)), l + last_index_offset - 1)
             END DO
          END DO
       END DO
    END DO

    IF (any(grid%nGhostRHS(direction,:) > 0)) CALL exchange_ghost_cells(grid, direction, x_ghost)

    is = 1 + ghost_offset(1)
    ie = N(1) + ghost_offset(1)
    js = 1 + ghost_offset(2)
    je = N(2) + ghost_offset(2)
    ks = 1 + ghost_offset(3)
    ke = N(3) + ghost_offset(3)

    IF (grid%nGhostRHS(direction, 1) == 0) THEN
       IF (direction == 1) THEN
          is = is + interior_start
       ELSE IF (direction == 2) THEN
          js = js + interior_start
       ELSE
          ks = ks + interior_start
       END IF
    END IF

    IF (grid%nGhostRHS(direction, 2) == 0) THEN
       IF (direction == 1) THEN
          ie = ie - interior_end
       ELSE IF (direction == 2) THEN
          je = je - interior_end
       ELSE
          ke = ke - interior_end
       END IF
    END IF
    
  END SUBROUTINE prepare_ghost_vector_full

  SUBROUTINE prepare_ghost_vector_allocate_and_copy_only1(grid, x, x_ghost, direction)

    USE ModGlobal
    USE ModDataStruct

    IMPLICIT NONE

    ! Arguments
    TYPE(t_grid), POINTER :: grid
    REAL(WP), DIMENSION(:,:), POINTER :: x, x_ghost
    INTEGER, INTENT(IN) :: direction

    ! Local variables
    INTEGER :: i, j, k, l, number_of_scalars, last_index_offset
    INTEGER, DIMENSION(3) :: ghost_offset, local_size_with_ghost_points
    INTEGER, DIMENSION(3) :: N, Ng

    N = grid%ie - grid%is + 1
    Ng = N + sum(grid%nGhostRHS, dim = 2)

    number_of_scalars = size(x, 2)

    ghost_offset = 0
    ghost_offset(direction) = grid%nGhostRHS(direction, 1)

    local_size_with_ghost_points = N
    local_size_with_ghost_points(direction) = Ng(direction)

    NULLIFY(x_ghost)
    ALLOCATE(x_ghost(product(local_size_with_ghost_points), number_of_scalars))

    last_index_offset = lbound(x, 2)

    DO l = 1, number_of_scalars
       DO k = 1, N(3)
          DO j = 1, N(2)
             DO i = 1, N(1)
                x_ghost(i + ghost_offset(1) + local_size_with_ghost_points(1) * (j - 1 + ghost_offset(2) + local_size_with_ghost_points(2) * (k - 1 + ghost_offset(3))), l) = &
                  x(i + N(1) * (j - 1 + N(2) * (k - 1)), l + last_index_offset - 1)
             END DO
          END DO
       END DO
    END DO

    IF (any(grid%nGhostRHS(direction,:) > 0)) CALL exchange_ghost_cells(grid, direction, x_ghost)

  END SUBROUTINE prepare_ghost_vector_allocate_and_copy_only1

  SUBROUTINE prepare_ghost_vector_allocate_and_copy_only2(grid, x, x_ghost, direction)

    USE ModGlobal
    USE ModDataStruct

    IMPLICIT NONE

    ! Arguments
    TYPE(t_grid), POINTER :: grid
    REAL(WP), DIMENSION(:), POINTER :: x
    REAL(WP), DIMENSION(:,:), POINTER :: x_ghost
    INTEGER, INTENT(IN) :: direction

    ! Local variables
    INTEGER :: i, j, k, l, number_of_scalars
    INTEGER, DIMENSION(3) :: ghost_offset, local_size_with_ghost_points
    INTEGER, DIMENSION(3) :: N, Ng

    N = grid%ie - grid%is + 1
    Ng = N + sum(grid%nGhostRHS, dim = 2)

    number_of_scalars = 1

    ghost_offset = 0
    ghost_offset(direction) = grid%nGhostRHS(direction, 1)

    local_size_with_ghost_points = N
    local_size_with_ghost_points(direction) = Ng(direction)

    NULLIFY(x_ghost)
    ALLOCATE(x_ghost(product(local_size_with_ghost_points), number_of_scalars))

    DO l = 1, number_of_scalars
       DO k = 1, N(3)
          DO j = 1, N(2)
             DO i = 1, N(1)
                x_ghost(i + ghost_offset(1) + local_size_with_ghost_points(1) * (j - 1 + ghost_offset(2) + local_size_with_ghost_points(2) * (k - 1 + ghost_offset(3))), l) = x(i + N(1) * (j - 1 + N(2) * (k - 1)))
             END DO
          END DO
       END DO
    END DO

    IF (any(grid%nGhostRHS(direction,:) > 0)) CALL exchange_ghost_cells(grid, direction, x_ghost)

  END SUBROUTINE prepare_ghost_vector_allocate_and_copy_only2

  SUBROUTINE NS_RHS_Euler_FV(region, grid, state)

    USE ModGlobal
    USE ModDataStruct

    IMPLICIT NONE
    
    ! Arguments
    TYPE(t_region), POINTER :: region
    TYPE(t_grid), POINTER :: grid
    TYPE(t_mixt), POINTER :: state

    ! Local variables
    TYPE(t_mixt_input), POINTER :: input
    INTEGER :: i, j, k, l, is, ie, js, je, ks, ke
    INTEGER :: direction, grid_vector_index, ghost_vector_index
    REAL(WP), DIMENSION(:), POINTER :: local_inviscid_flux_at_rgt_edge
    REAL(WP), DIMENSION(:,:), POINTER :: q_ghost, metrics_ghost, Jacobian_ghost
    REAL(WP), DIMENSION(:,:), POINTER :: inviscid_flux_at_rgt_edge
    REAL(WP), DIMENSION(:,:), POINTER :: inviscid_flux_at_lft_boundary
    INTEGER, DIMENSION(3) :: ghost_offset, local_size_with_ghost_points
    REAL(WP), DIMENSION(3) :: maximum_wave_speed_each_direction
    REAL(WP) :: local_Jacobian
    INTEGER, DIMENSION(3) :: N, Ng, stride

    ! ... initialize
    N  = 1
    Ng = 1
    N(1:grid%ND) = grid%ie(1:grid%ND) - grid%is(1:grid%ND) + 1
    Ng(1:grid%ND) = N(1:grid%ND) + sum(grid%nGhostRHS(1:grid%ND,:), dim = 2)
    stride(1) = 1 ; stride(2) = N(1) ; stride(3) = N(1) * N(2)
    maximum_wave_speed_each_direction = 0.0_WP
    input => grid%input

    ALLOCATE(inviscid_flux_at_rgt_edge(product(Ng), grid%ND + 2))
    
    ! ... Check if arrays have been allocated yet or not
    IF (.NOT. region%eulerfvInit1) THEN
       IF (.NOT. region%eulerfvInit) THEN
           ALLOCATE(region%eulerfv)
           region%eulerfvInit = .TRUE.
       END IF
       ALLOCATE(region%eulerfv%local_reconstructed_state_lft(grid%ND + 2), &
                region%eulerfv%local_reconstructed_state_rgt(grid%ND + 2), &
                region%eulerfv%q_local(-1:2))
       ALLOCATE(region%eulerfv%local_metric_single_direction(grid%ND))
       region%eulerfvInit1 = .TRUE.
    END IF

    DO direction = 1, grid%ND

       NULLIFY(inviscid_flux_at_lft_boundary)
       IF (grid%nGhostRHS(direction, 1) == 0) &
         ALLOCATE(inviscid_flux_at_lft_boundary(grid%vds(direction), grid%ND + 2))

       CALL prepare_ghost_vector(grid, grid%MT1, metrics_ghost, direction)
       CALL prepare_ghost_vector(grid, grid%JAC, Jacobian_ghost, direction)

       SELECT CASE (input%reconstruction_scheme)

       CASE (PIECE_CNST)

          CALL prepare_ghost_vector(grid, state%cv, q_ghost, direction, 0, 1, ghost_offset, local_size_with_ghost_points, is, ie, js, je, ks, ke)

          DO k = ks, ke
             DO j = js, je
                DO i = is, ie

                   grid_vector_index = i - ghost_offset(1) + N(1) * (j - 1 - ghost_offset(2) + N(2) * (k - 1 - ghost_offset(3)))
                   ghost_vector_index = i + local_size_with_ghost_points(1) * (j - 1 + local_size_with_ghost_points(2) * (k - 1))

                   DO l = 1, grid%ND + 2
                      region%eulerfv%local_reconstructed_state_rgt(l) = q_ghost(ghost_vector_index + stride(direction), l)
                      region%eulerfv%local_reconstructed_state_lft(l) = q_ghost(ghost_vector_index, l)
                   END DO

                   region%eulerfv%local_metric_single_direction = 0.5_WP * (metrics_ghost(ghost_vector_index, direction::grid%ND) + metrics_ghost(ghost_vector_index + stride(direction), direction::grid%ND))
                   local_Jacobian = 0.5_WP * (Jacobian_ghost(ghost_vector_index, 1) + Jacobian_ghost(ghost_vector_index + stride(direction), 1))
                   local_inviscid_flux_at_rgt_edge => inviscid_flux_at_rgt_edge(ghost_vector_index,:)
                   CALL compute_Roe_local_flux(region, region%eulerfv%local_reconstructed_state_lft, region%eulerfv%local_reconstructed_state_rgt, region%eulerfv%local_metric_single_direction, local_Jacobian, &
                     input%GamRef, grid%ND, local_inviscid_flux_at_rgt_edge, maximum_wave_speed_each_direction(direction))

                END DO
             END DO
          END DO

       CASE (THIRD_ORDER_MUSCL)

          CALL prepare_ghost_vector(grid, state%cv, q_ghost, direction, 1, 2, ghost_offset, local_size_with_ghost_points, is, ie, js, je, ks, ke)

          DO k = ks, ke
             DO j = js, je
                DO i = is, ie

                   grid_vector_index = i - ghost_offset(1) + N(1) * (j - 1 - ghost_offset(2) + N(2) * (k - 1 - ghost_offset(3)))
                   ghost_vector_index = i + local_size_with_ghost_points(1) * (j - 1 + local_size_with_ghost_points(2) * (k - 1))

                   DO l = 1, grid%ND + 2
                      region%eulerfv%q_local(-1:2) = q_ghost(ghost_vector_index - stride(direction) : ghost_vector_index + 2 * stride(direction) : stride(direction), l)
                      region%eulerfv%local_reconstructed_state_rgt(l) = region%eulerfv%q_local(1) - 0.25_WP * (2.0_WP * van_Leer_harmonic_limiter(region%eulerfv%q_local(1) - &
                        region%eulerfv%q_local(0), region%eulerfv%q_local(2) - region%eulerfv%q_local(1)) + 4.0_WP * van_Leer_harmonic_limiter(region%eulerfv%q_local(2) - &
                        region%eulerfv%q_local(1), region%eulerfv%q_local(1) - region%eulerfv%q_local(0))) / 3.0_WP
                      region%eulerfv%local_reconstructed_state_lft(l) = region%eulerfv%q_local(0) + 0.25_WP * (2.0_WP * van_Leer_harmonic_limiter(region%eulerfv%q_local(1) - &
                        region%eulerfv%q_local(0), region%eulerfv%q_local(0) - region%eulerfv%q_local(-1)) + 4.0_WP * &
                        van_Leer_harmonic_limiter(region%eulerfv%q_local(0) - region%eulerfv%q_local(-1), region%eulerfv%q_local(1) - region%eulerfv%q_local(0))) / 3.0_WP
                   END DO

                   region%eulerfv%local_metric_single_direction = 0.5_WP * (metrics_ghost(ghost_vector_index, direction::grid%ND) + metrics_ghost(ghost_vector_index + stride(direction), direction::grid%ND))
                   local_Jacobian = 0.5_WP * (Jacobian_ghost(ghost_vector_index, 1) + Jacobian_ghost(ghost_vector_index + stride(direction), 1))
                   local_inviscid_flux_at_rgt_edge => inviscid_flux_at_rgt_edge(ghost_vector_index,:)
                   CALL compute_Roe_local_flux(region, region%eulerfv%local_reconstructed_state_lft, region%eulerfv%local_reconstructed_state_rgt, region%eulerfv%local_metric_single_direction, local_Jacobian, &  
                     input%GamRef, grid%ND, local_inviscid_flux_at_rgt_edge, maximum_wave_speed_each_direction(direction))

                END DO
             END DO
          END DO

       END SELECT

       CALL compute_Roe_inviscid_flux_near_boundaries(region, grid, direction, q_ghost, metrics_ghost, Jacobian_ghost, inviscid_flux_at_rgt_edge)
       CALL compute_Roe_inviscid_flux_at_boundaries(region, grid, direction, q_ghost, metrics_ghost, Jacobian_ghost, inviscid_flux_at_lft_boundary, inviscid_flux_at_rgt_edge)
       DEALLOCATE(q_ghost, metrics_ghost, Jacobian_ghost)
       IF (any(grid%nGhostRHS(direction,:) > 0)) CALL exchange_ghost_cells(grid, direction, inviscid_flux_at_rgt_edge)

       is = 1 ; ie = N(1)
       js = 1 ; je = N(2)
       ks = 1 ; ke = N(3)

       IF (ghost_offset(direction) == 0) THEN

          IF (direction == 1) THEN

             i = 1 ; is = 2
             DO l = 1, grid%ND + 2
                DO k = 1, N(3)
                   DO j = 1, N(2)
                      grid_vector_index = i + N(1) * (j - 1 + N(2) * (k - 1))
                      ghost_vector_index = i + grid%nGhostRHS(1,1) + Ng(1) * (j - 1 + N(2) * (k - 1))
                      state%rhs(grid_vector_index,l) = state%rhs(grid_vector_index,l) - inviscid_flux_at_rgt_edge(ghost_vector_index,l) + inviscid_flux_at_lft_boundary(j+N(2)*(k-1),l)
                   END DO
                END DO
             END DO

          ELSE IF (direction == 2) THEN

             j = 1 ; js = 2
             DO l = 1, grid%ND + 2
                DO k = 1, N(3)
                   DO i = 1, N(1)
                      grid_vector_index = i + N(1) * N(2) * (k - 1)
                      ghost_vector_index = i + N(1) * (j + grid%nGhostRHS(2,1) - 1 + Ng(2) * (k - 1))
                      state%rhs(grid_vector_index,l) = state%rhs(grid_vector_index,l) - inviscid_flux_at_rgt_edge(ghost_vector_index,l) + inviscid_flux_at_lft_boundary(i+N(1)*(k-1),l)
                   END DO
                END DO
             END DO

          ELSE IF (direction == 3) THEN

             k = 1 ; ks = 2
             DO l = 1, grid%ND + 2
                DO j = 1, N(2)
                   DO i = 1, N(1)
                      grid_vector_index = i + N(1) * (j - 1)
                      ghost_vector_index = i + N(1) * (j - 1 + N(2) * (k + grid%nGhostRHS(3,1) - 1))
                      state%rhs(grid_vector_index,l) = state%rhs(grid_vector_index,l) - inviscid_flux_at_rgt_edge(ghost_vector_index,l) + inviscid_flux_at_lft_boundary(i+N(1)*(j-1),l)
                   END DO
                END DO
             END DO

          END IF

          DEALLOCATE(inviscid_flux_at_lft_boundary)

       END IF

       DO l = 1, grid%ND + 2
          DO k = ks, ke
             DO j = js, je
                DO i = is, ie
                   grid_vector_index = i + N(1) * (j - 1 + N(2) * (k - 1))
                   ghost_vector_index = i + ghost_offset(1) + local_size_with_ghost_points(1) * (j + ghost_offset(2) - 1 + local_size_with_ghost_points(2) * (k + ghost_offset(3) - 1))
                   state%rhs(grid_vector_index,l) = state%rhs(grid_vector_index,l) - inviscid_flux_at_rgt_edge(ghost_vector_index,l) + inviscid_flux_at_rgt_edge(ghost_vector_index - stride(direction), l)
                END DO
             END DO
          END DO
       END DO

    END DO

    DEALLOCATE(inviscid_flux_at_rgt_edge)

  END SUBROUTINE NS_RHS_Euler_FV


  SUBROUTINE compute_Roe_inviscid_flux_near_boundaries(region, grid, direction, q_ghost, metrics_ghost, Jacobian_ghost, inviscid_flux_at_rgt_edge)

    USE ModGlobal
    USE ModDataStruct

    IMPLICIT NONE

    ! Arguments
    TYPE(t_region), POINTER :: region
    TYPE(t_grid), POINTER :: grid
    INTEGER, INTENT(IN) :: direction
    REAL(WP), DIMENSION(:,:), POINTER, INTENT(IN) :: q_ghost, metrics_ghost, Jacobian_ghost, inviscid_flux_at_rgt_edge

    ! Local variables
    TYPE(t_mixt_input), POINTER :: input
    TYPE(t_patch), POINTER :: patch
    TYPE(t_grid), POINTER :: patch_grid
    INTEGER :: i, j, k, l, local_patch_index, grid_vector_index, ghost_vector_index
    REAL(WP), DIMENSION(:), POINTER :: local_inviscid_flux_at_rgt_edge
    REAL(WP) :: local_Jacobian
    integer, dimension(3) :: N, Ng, stride

    N = grid%ie - grid%is + 1
    Ng = N + sum(grid%nGhostRHS, dim = 2)
    stride(1) = 1 ; stride(2) = N(1) ; stride(3) = N(1) * N(2)

    input => grid%input

    ! Check if arrays have been allocated yet or not
    IF (.NOT. region%eulerfvInit1) THEN
       IF (.NOT. region%eulerfvInit) THEN
           ALLOCATE(region%eulerfv)
           region%eulerfvInit = .TRUE.
       END IF
       ALLOCATE(region%eulerfv%local_reconstructed_state_lft(grid%ND + 2), region%eulerfv%local_reconstructed_state_rgt(grid%ND + 2), region%eulerfv%q_local(-1:2))
       ALLOCATE(region%eulerfv%local_metric_single_direction(grid%ND))
       region%eulerfvInit1 = .TRUE.
    END IF

    DO local_patch_index = 1, region%nPatches

       patch => region%patch(local_patch_index)
       patch_grid => region%grid(patch%gridID)
       IF (patch%bcType == SPONGE .OR. patch_grid%iGridGlobal /= grid%iGridGlobal .OR. abs(patch%normDir) /= direction .OR. grid%periodic(direction) >= TRUE) cycle

       SELECT CASE (input%reconstruction_scheme)

       CASE (THIRD_ORDER_MUSCL)
          
          SELECT CASE (patch%normDir)

          CASE (+1)

             i = patch%is(1)
             DO k = patch%is(3), patch%ie(3)
                DO j = patch%is(2), patch%ie(2)

                   grid_vector_index = i - grid%is(1) + 1 + N(1) * (j - grid%is(2) + N(2) * (k - grid%is(3)))
                   ghost_vector_index = i - grid%is(1) + 1 + grid%nGhostRHS(1,1) + Ng(1) * (j - grid%is(2) + N(2) * (k - grid%is(3)))

                   DO l = 1, grid%ND + 2
                      region%eulerfv%q_local(-1:2) = q_ghost(ghost_vector_index : ghost_vector_index + 3 * stride(direction) : stride(direction), l)
                      region%eulerfv%local_reconstructed_state_rgt(l) = 2.0_WP * region%eulerfv%q_local(0) - (region%eulerfv%q_local(0) + 0.25_WP * (2.0_WP * &
                        van_Leer_harmonic_limiter(region%eulerfv%q_local(1) - region%eulerfv%q_local(0), region%eulerfv%q_local(0) - region%eulerfv%q_local(-1)) + &
                        4.0_WP * van_Leer_harmonic_limiter(region%eulerfv%q_local(0) - region%eulerfv%q_local(-1), region%eulerfv%q_local(1) - region%eulerfv%q_local(0))) / 3.0_WP)
                      region%eulerfv%local_reconstructed_state_lft(l) = region%eulerfv%q_local(-1)
                   END DO

                   region%eulerfv%local_metric_single_direction = 0.5_WP * (metrics_ghost(ghost_vector_index, direction::grid%ND) + &
                     metrics_ghost(ghost_vector_index + stride(direction), direction::grid%ND))
                   local_Jacobian = 0.5_WP * (Jacobian_ghost(ghost_vector_index, 1) + Jacobian_ghost(ghost_vector_index + stride(direction), 1))
                   local_inviscid_flux_at_rgt_edge => inviscid_flux_at_rgt_edge(ghost_vector_index,:)
                   CALL compute_Roe_local_flux(region, region%eulerfv%local_reconstructed_state_lft, region%eulerfv%local_reconstructed_state_rgt, &
                     region%eulerfv%local_metric_single_direction, local_Jacobian, input%GamRef, grid%ND, local_inviscid_flux_at_rgt_edge)

                END DO
             END DO

          CASE (-1)

             i = patch%ie(1) - 1
             DO k = patch%is(3), patch%ie(3)
                DO j = patch%is(2), patch%ie(2)
                      
                   grid_vector_index = i - grid%is(1) + 1 + N(1) * (j - grid%is(2) + N(2) * (k - grid%is(3)))
                   ghost_vector_index = i - grid%is(1) + 1 + grid%nGhostRHS(1,1) + Ng(1) * (j - grid%is(2) + N(2) * (k - grid%is(3)))

                   DO l = 1, grid%ND + 2
                      region%eulerfv%q_local(-1:2) = q_ghost(ghost_vector_index - 2 * stride(direction) : ghost_vector_index + stride(direction) : stride(direction), l)
                      region%eulerfv%local_reconstructed_state_lft(l) = 2.0_WP * region%eulerfv%q_local(1) - (region%eulerfv%q_local(1) - 0.25_WP * (2.0_WP * &
                        van_Leer_harmonic_limiter(region%eulerfv%q_local(1) - region%eulerfv%q_local(0), region%eulerfv%q_local(2) - region%eulerfv%q_local(1)) + &
                        4.0_WP * van_Leer_harmonic_limiter(region%eulerfv%q_local(2) - region%eulerfv%q_local(1), region%eulerfv%q_local(1) - region%eulerfv%q_local(0))) / 3.0_WP)
                      region%eulerfv%local_reconstructed_state_rgt(l) = region%eulerfv%q_local(2)
                   END DO

                   region%eulerfv%local_metric_single_direction = 0.5_WP * (metrics_ghost(ghost_vector_index, direction::grid%ND) + &
                     metrics_ghost(ghost_vector_index + stride(direction), direction::grid%ND))
                   local_Jacobian = 0.5_WP * (Jacobian_ghost(ghost_vector_index, 1) + Jacobian_ghost(ghost_vector_index + stride(direction), 1))
                   local_inviscid_flux_at_rgt_edge => inviscid_flux_at_rgt_edge(ghost_vector_index,:)
                   CALL compute_Roe_local_flux(region, region%eulerfv%local_reconstructed_state_lft, region%eulerfv%local_reconstructed_state_rgt, &
                     region%eulerfv%local_metric_single_direction, local_Jacobian, input%GamRef, grid%ND, local_inviscid_flux_at_rgt_edge)

                END DO
             END DO

          CASE (+2)

             j = patch%is(2)
             DO k = patch%is(3), patch%ie(3)
                DO i = patch%is(1), patch%ie(1)
                      
                   grid_vector_index = i - grid%is(1) + 1 + N(1) * (j - grid%is(2) + N(2) * (k - grid%is(3)))
                   ghost_vector_index = i - grid%is(1) + 1 + N(1) * (j - grid%is(2) + grid%nGhostRHS(2,1) + Ng(2) * (k - grid%is(3)))

                   DO l = 1, grid%ND + 2
                      region%eulerfv%q_local(-1:2) = q_ghost(ghost_vector_index : ghost_vector_index + 3 * stride(direction) : stride(direction), l)
                      region%eulerfv%local_reconstructed_state_rgt(l) = 2.0_WP * region%eulerfv%q_local(0) - (region%eulerfv%q_local(0) + 0.25_WP * (2.0_WP * &
                        van_Leer_harmonic_limiter(region%eulerfv%q_local(1) - region%eulerfv%q_local(0), region%eulerfv%q_local(0) - region%eulerfv%q_local(-1)) + &
                        4.0_WP * van_Leer_harmonic_limiter(region%eulerfv%q_local(0) - region%eulerfv%q_local(-1), region%eulerfv%q_local(1) - region%eulerfv%q_local(0))) / 3.0_WP)
                      region%eulerfv%local_reconstructed_state_lft(l) = region%eulerfv%q_local(-1)
                   END DO

                   region%eulerfv%local_metric_single_direction = 0.5_WP * (metrics_ghost(ghost_vector_index, direction::grid%ND) + &
                     metrics_ghost(ghost_vector_index + stride(direction), direction::grid%ND))
                   local_Jacobian = 0.5_WP * (Jacobian_ghost(ghost_vector_index, 1) + Jacobian_ghost(ghost_vector_index + stride(direction), 1))
                   local_inviscid_flux_at_rgt_edge => inviscid_flux_at_rgt_edge(ghost_vector_index,:)
                   CALL compute_Roe_local_flux(region, region%eulerfv%local_reconstructed_state_lft, region%eulerfv%local_reconstructed_state_rgt, &
                     region%eulerfv%local_metric_single_direction, local_Jacobian, input%GamRef, grid%ND, local_inviscid_flux_at_rgt_edge)

                END DO
             END DO

          CASE (-2)

             j = patch%ie(2) - 1
             DO k = patch%is(3), patch%ie(3)
                DO i = patch%is(1), patch%ie(1)
                      
                   grid_vector_index = i - grid%is(1) + 1 + N(1) * (j - grid%is(2) + N(2) * (k - grid%is(3)))
                   ghost_vector_index = i - grid%is(1) + 1 + N(1) * (j - grid%is(2) + grid%nGhostRHS(2,1) + Ng(2) * (k - grid%is(3)))

                   DO l = 1, grid%ND + 2
                      region%eulerfv%q_local(-1:2) = q_ghost(ghost_vector_index - 2 * stride(direction) : ghost_vector_index + stride(direction) : stride(direction), l)
                      region%eulerfv%local_reconstructed_state_lft(l) = 2.0_WP * region%eulerfv%q_local(1) - (region%eulerfv%q_local(1) - 0.25_WP * (2.0_WP * &
                        van_Leer_harmonic_limiter(region%eulerfv%q_local(1) - region%eulerfv%q_local(0), region%eulerfv%q_local(2) - region%eulerfv%q_local(1)) + &
                        4.0_WP * van_Leer_harmonic_limiter(region%eulerfv%q_local(2) - region%eulerfv%q_local(1), region%eulerfv%q_local(1) - region%eulerfv%q_local(0))) / 3.0_WP)
                      region%eulerfv%local_reconstructed_state_rgt(l) = region%eulerfv%q_local(2)
                   END DO

                   region%eulerfv%local_metric_single_direction = 0.5_WP * (metrics_ghost(ghost_vector_index, direction::grid%ND) + &
                     metrics_ghost(ghost_vector_index + stride(direction), direction::grid%ND))
                   local_Jacobian = 0.5_WP * (Jacobian_ghost(ghost_vector_index, 1) + Jacobian_ghost(ghost_vector_index + stride(direction), 1))
                   local_inviscid_flux_at_rgt_edge => inviscid_flux_at_rgt_edge(ghost_vector_index,:)
                   CALL compute_Roe_local_flux(region, region%eulerfv%local_reconstructed_state_lft, region%eulerfv%local_reconstructed_state_rgt, &
                     region%eulerfv%local_metric_single_direction, local_Jacobian, input%GamRef, grid%ND, local_inviscid_flux_at_rgt_edge)

                END DO
             END DO

          CASE (+3)

             k = patch%is(3)
             DO j = patch%is(2), patch%ie(2)
                DO i = patch%is(1), patch%ie(1)
                      
                   grid_vector_index = i - grid%is(1) + 1 + N(1) * (j - grid%is(2) + N(2) * (k - grid%is(3)))
                   ghost_vector_index = i - grid%is(1) + 1 + N(1) * (j - grid%is(2) + N(2) * (k - grid%is(3) + grid%nGhostRHS(3,1)))

                   DO l = 1, grid%ND + 2
                      region%eulerfv%q_local(-1:2) = q_ghost(ghost_vector_index : ghost_vector_index + 3 * stride(direction) : stride(direction), l)
                      region%eulerfv%local_reconstructed_state_rgt(l) = 2.0_WP * region%eulerfv%q_local(0) - (region%eulerfv%q_local(0) + 0.25_WP * (2.0_WP * &
                        van_Leer_harmonic_limiter(region%eulerfv%q_local(1) - region%eulerfv%q_local(0), region%eulerfv%q_local(0) - region%eulerfv%q_local(-1)) + &
                        4.0_WP * van_Leer_harmonic_limiter(region%eulerfv%q_local(0) - region%eulerfv%q_local(-1), region%eulerfv%q_local(1) - region%eulerfv%q_local(0))) / 3.0_WP)
                      region%eulerfv%local_reconstructed_state_lft(l) = region%eulerfv%q_local(-1)
                   END DO

                   region%eulerfv%local_metric_single_direction = 0.5_WP * (metrics_ghost(ghost_vector_index, direction::grid%ND) + &
                     metrics_ghost(ghost_vector_index + stride(direction), direction::grid%ND))
                   local_Jacobian = 0.5_WP * (Jacobian_ghost(ghost_vector_index, 1) + Jacobian_ghost(ghost_vector_index + stride(direction), 1))
                   local_inviscid_flux_at_rgt_edge => inviscid_flux_at_rgt_edge(ghost_vector_index,:)
                   CALL compute_Roe_local_flux(region, region%eulerfv%local_reconstructed_state_lft, region%eulerfv%local_reconstructed_state_rgt, &
                     region%eulerfv%local_metric_single_direction, local_Jacobian, input%GamRef, grid%ND, local_inviscid_flux_at_rgt_edge)

                END DO
             END DO

          CASE (-3)

             k = patch%ie(3) - 1
             DO j = patch%is(2), patch%ie(2)
                DO i = patch%is(1), patch%ie(1)
                      
                   grid_vector_index = i - grid%is(1) + 1 + N(1) * (j - grid%is(2) + N(2) * (k - grid%is(3)))
                   ghost_vector_index = i - grid%is(1) + 1 + N(1) * (j - grid%is(2) + N(2) * (k - grid%is(3) + grid%nGhostRHS(3,1)))

                   DO l = 1, grid%ND + 2
                      region%eulerfv%q_local(-1:2) = q_ghost(ghost_vector_index - 2 * stride(direction) : ghost_vector_index + stride(direction) : stride(direction), l)
                      region%eulerfv%local_reconstructed_state_lft(l) = 2.0_WP * region%eulerfv%q_local(1) - (region%eulerfv%q_local(1) - 0.25_WP * (2.0_WP * &
                        van_Leer_harmonic_limiter(region%eulerfv%q_local(1) - region%eulerfv%q_local(0), region%eulerfv%q_local(2) - region%eulerfv%q_local(1)) + &
                        4.0_WP * van_Leer_harmonic_limiter(region%eulerfv%q_local(2) - region%eulerfv%q_local(1), region%eulerfv%q_local(1) - region%eulerfv%q_local(0))) / 3.0_WP)
                      region%eulerfv%local_reconstructed_state_rgt(l) = region%eulerfv%q_local(2)
                   END DO

                   region%eulerfv%local_metric_single_direction = 0.5_WP * (metrics_ghost(ghost_vector_index, direction::grid%ND) + &
                     metrics_ghost(ghost_vector_index + stride(direction), direction::grid%ND))
                   local_Jacobian = 0.5_WP * (Jacobian_ghost(ghost_vector_index, 1) + Jacobian_ghost(ghost_vector_index + stride(direction), 1))
                   local_inviscid_flux_at_rgt_edge => inviscid_flux_at_rgt_edge(ghost_vector_index,:)
                   CALL compute_Roe_local_flux(region, region%eulerfv%local_reconstructed_state_lft, region%eulerfv%local_reconstructed_state_rgt, &
                     region%eulerfv%local_metric_single_direction, local_Jacobian, input%GamRef, grid%ND, local_inviscid_flux_at_rgt_edge)

                END DO
             END DO

          END SELECT

       END SELECT
    
    END DO

  END SUBROUTINE compute_Roe_inviscid_flux_near_boundaries

  SUBROUTINE compute_Roe_inviscid_flux_at_boundaries(region, grid, direction, q_ghost, metrics_ghost, Jacobian_ghost, inviscid_flux_at_lft_boundary, inviscid_flux_at_rgt_edge)

    USE ModGlobal
    USE ModDataStruct

    IMPLICIT NONE

    ! Arguments
    TYPE(t_region), POINTER :: region
    TYPE(t_grid), POINTER :: grid
    INTEGER, INTENT(IN) :: direction
    REAL(WP), DIMENSION(:,:), POINTER, INTENT(IN) :: q_ghost, metrics_ghost, Jacobian_ghost, inviscid_flux_at_lft_boundary, inviscid_flux_at_rgt_edge

    ! Local variables
    TYPE(t_mixt_input), POINTER :: input
    TYPE(t_patch), POINTER :: patch
    TYPE(t_grid), POINTER :: patch_grid
    INTEGER :: i, j, k, local_patch_index, grid_vector_index, ghost_vector_index, cross_plane_vector_index, lp
    REAL(WP), DIMENSION(:), POINTER :: local_inviscid_flux, local_metrics
    REAL(WP) :: local_Jacobian
    integer, dimension(3) :: N, Ng, stride, Np

    N = grid%ie - grid%is + 1
    Ng = N + sum(grid%nGhostRHS, dim = 2)
    stride(1) = 1 ; stride(2) = N(1) ; stride(3) = N(1) * N(2)

    input => grid%input

    ! Check if arrays have been allocated yet or not
    IF (.NOT. region%eulerfvInit2) THEN
       IF (.NOT. region%eulerfvInit) THEN
           ALLOCATE(region%eulerfv)
           region%eulerfvInit = .TRUE.
       END IF
       IF (.NOT. region%eulerfvInit1) THEN
           ALLOCATE(region%eulerfv%local_reconstructed_state_lft(grid%ND + 2), region%eulerfv%local_reconstructed_state_rgt(grid%ND + 2))
           ALLOCATE(region%eulerfv%local_metric_single_direction(grid%ND))
           region%eulerfvInit1 = .TRUE.
       END IF
       ALLOCATE(region%eulerfv%local_state(grid%ND + 2), region%eulerfv%local_ghost_state(grid%ND + 2))
       region%eulerfvInit2 = .TRUE.
    END IF

    DO local_patch_index = 1, region%nPatches

       patch => region%patch(local_patch_index)
       patch_grid => region%grid(patch%gridID)
       IF (patch%bcType == SPONGE .OR. patch_grid%iGridGlobal /= grid%iGridGlobal .OR. abs(patch%normDir) /= direction .OR. grid%periodic(direction) >= TRUE) cycle

       Np = 1
       Do j = 1, patch_grid%ND
         Np(j) = patch%ie(j) - patch%is(j) + 1
       End Do

       DO k = patch%is(3), patch%ie(3)
          DO j = patch%is(2), patch%ie(2)
             DO i = patch%is(1), patch%ie(1)

                grid_vector_index = i - grid%is(1) + 1 + N(1) * (j - grid%is(2) + N(2) * (k - grid%is(3)))

                SELECT CASE (abs(patch%normDir))

                CASE (1)
                   ghost_vector_index = i - grid%is(1) + 1 + grid%nGhostRHS(1,1) + Ng(1) * (j - grid%is(2) + N(2) * (k - grid%is(3)))
                   cross_plane_vector_index = j - grid%is(2) + 1 + N(2) * (k - grid%is(3))
                   
                CASE (2)
                   ghost_vector_index = i - grid%is(1) + 1 + N(1) * (j - grid%is(2) + grid%nGhostRHS(2,1) + Ng(2) * (k - grid%is(3)))
                   cross_plane_vector_index = i - grid%is(1) + 1 + N(1) * (k - grid%is(3))

                CASE (3)
                   ghost_vector_index = i - grid%is(1) + 1 + N(1) * (j - grid%is(2) + N(2) * (k - grid%is(3) + grid%nGhostRHS(3,1)))
                   cross_plane_vector_index = i - grid%is(1) + 1 + N(1) * (j - grid%is(2))

                END SELECT

                IF (patch%normDir > 0) THEN
                   region%eulerfv%local_metric_single_direction = (3.0_WP * metrics_ghost(ghost_vector_index, direction::grid%ND) - metrics_ghost(ghost_vector_index + stride(direction), direction::grid%ND)) / 2.0_WP
                   local_Jacobian = (3.0_WP * Jacobian_ghost(ghost_vector_index, 1) - Jacobian_ghost(ghost_vector_index + stride(direction), 1)) / 2.0_WP
                ELSE
                   region%eulerfv%local_metric_single_direction = (3.0_WP * metrics_ghost(ghost_vector_index, direction::grid%ND) - metrics_ghost(ghost_vector_index - stride(direction), direction::grid%ND)) / 2.0_WP
                   local_Jacobian = (3.0_WP * Jacobian_ghost(ghost_vector_index, 1) - Jacobian_ghost(ghost_vector_index - stride(direction), 1)) / 2.0_WP
                END IF

                SELECT CASE (patch%bcType)
          
                CASE (FV_NEUMANN, FV_DIRICHLET)

                   region%eulerfv%local_state = q_ghost(ghost_vector_index,:)
                   region%eulerfv%local_ghost_state = region%eulerfv%local_state

                   IF (patch%normDir > 0) THEN
                      IF (patch%is(direction) == 1) THEN
                         local_inviscid_flux => inviscid_flux_at_lft_boundary(cross_plane_vector_index,:)
                      ELSE
                         local_inviscid_flux => inviscid_flux_at_rgt_edge(ghost_vector_index-stride(direction),:)
                      END IF
                      CALL compute_Roe_local_flux(region, region%eulerfv%local_ghost_state, region%eulerfv%local_state, region%eulerfv%local_metric_single_direction, local_Jacobian, input%GamRef, grid%ND, local_inviscid_flux)
                   ELSE
                      local_inviscid_flux => inviscid_flux_at_rgt_edge(ghost_vector_index,:)
                      CALL compute_Roe_local_flux(region, region%eulerfv%local_state, region%eulerfv%local_ghost_state, region%eulerfv%local_metric_single_direction, local_Jacobian, input%GamRef, grid%ND, local_inviscid_flux)
                   END IF

                CASE (FV_BLOCK_INTERFACE, FV_BLOCK_INTERFACE_PERIODIC)

                   lp = (k-patch%is(3))*Np(1)*Np(2) + (j-patch%is(2))*Np(1) + (i-patch%is(1)+1)
                   region%eulerfv%local_state = q_ghost(ghost_vector_index,:)
                   region%eulerfv%local_ghost_state = patch%cv_out(lp,:)

                   IF (patch%normDir > 0) THEN
                      IF (patch%is(direction) == 1) THEN
                         local_inviscid_flux => inviscid_flux_at_lft_boundary(cross_plane_vector_index,:)
                      ELSE
                         local_inviscid_flux => inviscid_flux_at_rgt_edge(ghost_vector_index-stride(direction),:)
                      END IF
                      CALL compute_Roe_local_flux(region, region%eulerfv%local_ghost_state, region%eulerfv%local_state, region%eulerfv%local_metric_single_direction, local_Jacobian, input%GamRef, grid%ND, local_inviscid_flux)
                   ELSE
                      local_inviscid_flux => inviscid_flux_at_rgt_edge(ghost_vector_index,:)
                      CALL compute_Roe_local_flux(region, region%eulerfv%local_state, region%eulerfv%local_ghost_state, region%eulerfv%local_metric_single_direction, local_Jacobian, input%GamRef, grid%ND, local_inviscid_flux)
                   END IF

                CASE (FV_SYMMETRY)

                   region%eulerfv%local_state = q_ghost(ghost_vector_index,:)
                   local_metrics => metrics_ghost(ghost_vector_index,:)
                   CALL compute_local_symmetric_state(region, region%eulerfv%local_state, region%eulerfv%local_metric_single_direction, local_metrics, direction, grid%ND, region%eulerfv%local_ghost_state)
                   IF (patch%normDir > 0) THEN
                      IF (patch%is(direction) == 1) THEN
                         local_inviscid_flux => inviscid_flux_at_lft_boundary(cross_plane_vector_index,:)
                      ELSE
                         local_inviscid_flux => inviscid_flux_at_rgt_edge(ghost_vector_index-stride(direction),:)
                      END IF
                      CALL compute_Roe_local_flux(region, region%eulerfv%local_ghost_state, region%eulerfv%local_state, region%eulerfv%local_metric_single_direction, local_Jacobian, input%GamRef, grid%ND, local_inviscid_flux)
                   ELSE
                      local_inviscid_flux => inviscid_flux_at_rgt_edge(ghost_vector_index,:)
                      CALL compute_Roe_local_flux(region, region%eulerfv%local_state, region%eulerfv%local_ghost_state, region%eulerfv%local_metric_single_direction, local_Jacobian, input%GamRef, grid%ND, local_inviscid_flux)
                   END IF

                END SELECT

             END DO
          END DO
       END DO

    END DO

  END SUBROUTINE compute_Roe_inviscid_flux_at_boundaries

  SUBROUTINE compute_local_symmetric_state(region, local_state, metric_along_direction, metrics, direction, number_of_dimensions, local_symmetric_state)

    USE ModGlobal
    USE ModDataStruct
    USE ModMatrixVectorOps

    IMPLICIT NONE

    ! Arguments
    TYPE(t_region), POINTER :: region
    REAL(WP), DIMENSION(:), ALLOCATABLE, INTENT(IN) :: local_state, metric_along_direction
    REAL(WP), DIMENSION(:), POINTER, INTENT(IN) :: metrics
    INTEGER, INTENT(IN) :: direction, number_of_dimensions
    REAL(WP), DIMENSION(:), ALLOCATABLE, INTENT(INOUT) :: local_symmetric_state

    ! Local variables
    REAL(WP) :: normal_velocity

    IF (number_of_dimensions == 1) THEN
       local_symmetric_state = local_state
       local_symmetric_state(2) = - local_symmetric_state(2)
       RETURN
    END IF

    ! Check if arrays have been allocated yet or not
    IF (.NOT. region%eulerfvInit3) THEN
       IF (number_of_dimensions >= 2) THEN
          ALLOCATE(region%eulerfv%tangential_velocity(number_of_dimensions - 1))
          ALLOCATE(region%eulerfv%tangential_vector(number_of_dimensions, number_of_dimensions - 1))
          ALLOCATE(region%eulerfv%normal_vector(number_of_dimensions))
       END IF
       region%eulerfvInit3 = .TRUE.       
    END IF
    
    region%eulerfv%normal_vector = metric_along_direction
    region%eulerfv%normal_vector = region%eulerfv%normal_vector / SQRT(DOT_PRODUCT(region%eulerfv%normal_vector, region%eulerfv%normal_vector))
    normal_velocity = DOT_PRODUCT(local_state(2:number_of_dimensions+1), region%eulerfv%normal_vector) / local_state(1)

    IF (number_of_dimensions == 2) THEN
       region%eulerfv%tangential_vector(:,1) = (/ - region%eulerfv%normal_vector(2), region%eulerfv%normal_vector(1) /)
       region%eulerfv%tangential_vector(:,1) = region%eulerfv%tangential_vector(:,1) / SQRT(DOT_PRODUCT(region%eulerfv%tangential_vector(:,1), region%eulerfv%tangential_vector(:,1)))
       region%eulerfv%tangential_velocity(1) = DOT_PRODUCT(local_state(2:number_of_dimensions+1), region%eulerfv%tangential_vector(:,1)) / local_state(1)
    ELSE IF (number_of_dimensions == 3) THEN
       region%eulerfv%tangential_vector(:,2) = metrics(mod(direction,number_of_dimensions)+1::number_of_dimensions)
       CALL cross_product(region%eulerfv%normal_vector, region%eulerfv%tangential_vector(:,2), region%eulerfv%tangential_vector(:,1))
       region%eulerfv%tangential_vector(:,1) = region%eulerfv%tangential_vector(:,1) / SQRT(DOT_PRODUCT(region%eulerfv%tangential_vector(:,1), region%eulerfv%tangential_vector(:,1)))
       region%eulerfv%tangential_velocity(1) = DOT_PRODUCT(local_state(2:number_of_dimensions+1), region%eulerfv%tangential_vector(:,1)) / local_state(1)
       CALL cross_product(region%eulerfv%normal_vector, region%eulerfv%tangential_vector(:,1), region%eulerfv%tangential_vector(:,2))
       region%eulerfv%tangential_vector(:,2) = region%eulerfv%tangential_vector(:,2) / SQRT(DOT_PRODUCT(region%eulerfv%tangential_vector(:,2), region%eulerfv%tangential_vector(:,2)))
       region%eulerfv%tangential_velocity(2) = DOT_PRODUCT(local_state(2:number_of_dimensions+1), region%eulerfv%tangential_vector(:,2)) / local_state(1)
    END IF

    normal_velocity = - normal_velocity

    local_symmetric_state(1) = local_state(1)
    local_symmetric_state(2:number_of_dimensions+1) = local_state(2:number_of_dimensions+1) / local_state(1)

    IF (number_of_dimensions == 2) THEN
       local_symmetric_state(2) = region%eulerfv%normal_vector(1) * normal_velocity - region%eulerfv%normal_vector(2) * region%eulerfv%tangential_velocity(1)
       local_symmetric_state(3) = region%eulerfv%normal_vector(2) * normal_velocity + region%eulerfv%normal_vector(1) * region%eulerfv%tangential_velocity(1)
    ELSE IF (number_of_dimensions == 3) THEN
       local_symmetric_state(2) = DOT_PRODUCT((/ normal_velocity, region%eulerfv%tangential_velocity(1), region%eulerfv%tangential_velocity(2) /), &
         (/ region%eulerfv%normal_vector(1), region%eulerfv%tangential_vector(1,1), region%eulerfv%tangential_vector(1,2) /))
       local_symmetric_state(3) = DOT_PRODUCT((/ normal_velocity, region%eulerfv%tangential_velocity(1), region%eulerfv%tangential_velocity(2) /), &
         (/ region%eulerfv%normal_vector(2), region%eulerfv%tangential_vector(2,1), region%eulerfv%tangential_vector(2,2) /))
       local_symmetric_state(4) = DOT_PRODUCT((/ normal_velocity, region%eulerfv%tangential_velocity(1), region%eulerfv%tangential_velocity(2) /), &
         (/ region%eulerfv%normal_vector(3), region%eulerfv%tangential_vector(3,1), region%eulerfv%tangential_vector(3,2) /))
    END IF

    local_symmetric_state(number_of_dimensions+2) = local_state(number_of_dimensions+2) + 0.5_WP * local_state(1) * &
       (SUM(local_symmetric_state(2:number_of_dimensions+1) ** 2) - SUM(local_state(2:number_of_dimensions+1) ** 2) / local_state(1) ** 2)
    local_symmetric_state(2:number_of_dimensions+1) = local_symmetric_state(1) * local_symmetric_state(2:number_of_dimensions+1)

  END SUBROUTINE compute_local_symmetric_state

  SUBROUTINE compute_Roe_local_flux(region, lft_state, rgt_state, metric_along_direction, Jacobian, gamma, number_of_dimensions, flux_at_rgt_edge, maximum_wave_speed)

    USE ModGlobal
    USE ModDataStruct
    
    IMPLICIT NONE

    ! Arguments
    TYPE(t_region), POINTER :: region
    REAL(WP), DIMENSION(:), ALLOCATABLE, INTENT(IN) :: lft_state, rgt_state, metric_along_direction
    REAL(WP), INTENT(IN) :: Jacobian, gamma
    INTEGER, INTENT(IN) :: number_of_dimensions
    REAL(WP), DIMENSION(:), POINTER, INTENT(IN) :: flux_at_rgt_edge
    REAL(WP), INTENT(INOUT), OPTIONAL :: maximum_wave_speed

    ! Local variables
    REAL(WP) :: sqrt_density_lft, sqrt_density_rgt
    REAL(WP) :: metric_normalization_factor
    REAL(WP) :: speed_of_sound_Roe, speed_of_sound_lft, speed_of_sound_rgt
    REAL(WP) :: signal_velocity_lft, signal_velocity_rgt, delta_coeff
    INTEGER :: i

    ! Check if arrays have been allocated yet or not
    IF (.NOT. region%eulerfvInit4) THEN
       ALLOCATE(region%eulerfv%flux_at_lft_state(number_of_dimensions + 2), region%eulerfv%flux_at_rgt_state(number_of_dimensions + 2))
       ALLOCATE(region%eulerfv%Roe_state(number_of_dimensions + 2))
       ALLOCATE(region%eulerfv%rotation_matrix_Roe_state(number_of_dimensions + 2, number_of_dimensions + 2))
       ALLOCATE(region%eulerfv%rotation_matrix_inverse_Roe_state(number_of_dimensions + 2, number_of_dimensions + 2), region%eulerfv%s_Roe(number_of_dimensions + 2))
       region%eulerfvInit4 = .TRUE.
    END IF

    sqrt_density_lft = SQRT(lft_state(1))
    sqrt_density_rgt = SQRT(rgt_state(1))

    speed_of_sound_lft = SQRT(gamma * (gamma - 1.0_WP) / lft_state(1) * (lft_state(number_of_dimensions+2) - 0.5_WP / lft_state(1) * SUM(lft_state(2:number_of_dimensions+1) ** 2)))
    speed_of_sound_rgt = SQRT(gamma * (gamma - 1.0_WP) / rgt_state(1) * (rgt_state(number_of_dimensions+2) - 0.5_WP / rgt_state(1) * SUM(rgt_state(2:number_of_dimensions+1) ** 2)))

    region%eulerfv%Roe_state(1) = sqrt_density_lft * sqrt_density_rgt
    region%eulerfv%Roe_state(2:number_of_dimensions+1) = (sqrt_density_rgt * lft_state(2:number_of_dimensions+1) + sqrt_density_lft * &
                                                         rgt_state(2:number_of_dimensions+1)) / (sqrt_density_lft + sqrt_density_rgt)
    speed_of_sound_Roe = SQRT((gamma - 1.0_WP) * (((gamma * lft_state(number_of_dimensions+2) - 0.5_WP * (gamma - 1.0_WP) / lft_state(1) * &
                         SUM(lft_state(2:number_of_dimensions+1) ** 2)) / sqrt_density_lft + (gamma * rgt_state(number_of_dimensions+2) - 0.5_WP * (gamma - 1.0_WP) / rgt_state(1) * &
                         SUM(rgt_state(2:number_of_dimensions+1) ** 2)) / sqrt_density_rgt) / (sqrt_density_lft + sqrt_density_rgt) - 0.5 / region%eulerfv%Roe_state(1) ** 2 * &
                         SUM(region%eulerfv%Roe_state(2:number_of_dimensions+1) ** 2)))
    region%eulerfv%Roe_state(number_of_dimensions+2) = speed_of_sound_Roe ** 2 * region%eulerfv%Roe_state(1) / (gamma * (gamma - 1.0_WP)) + 0.5 / region%eulerfv%Roe_state(1) * &
                                                       SUM(region%eulerfv%Roe_state(2:number_of_dimensions+1) ** 2)

    metric_normalization_factor = SQRT(SUM(metric_along_direction ** 2))

    signal_velocity_lft = Jacobian * MIN(DOT_PRODUCT(region%eulerfv%Roe_state(2:number_of_dimensions+1) / region%eulerfv%Roe_state(1), metric_along_direction) - &
                          speed_of_sound_Roe * metric_normalization_factor, DOT_PRODUCT(lft_state(2:number_of_dimensions+1) / lft_state(1), metric_along_direction) - &
                          speed_of_sound_lft * metric_normalization_factor)
    signal_velocity_rgt = Jacobian * MAX(DOT_PRODUCT(region%eulerfv%Roe_state(2:number_of_dimensions+1) / region%eulerfv%Roe_state(1), metric_along_direction) + speed_of_sound_Roe * &
                          metric_normalization_factor, DOT_PRODUCT(rgt_state(2:number_of_dimensions+1) / rgt_state(1), metric_along_direction) + speed_of_sound_rgt * &
                          metric_normalization_factor)

    IF (PRESENT(maximum_wave_speed)) maximum_wave_speed = MAX(maximum_wave_speed, abs(signal_velocity_lft), abs(signal_velocity_rgt))

    IF (signal_velocity_lft >= 0.0_WP) THEN

       CALL compute_local_inviscid_flux(lft_state, metric_along_direction, gamma, number_of_dimensions, flux_at_rgt_edge)

    ELSE IF (signal_velocity_rgt <= 0.0_WP) THEN

       CALL compute_local_inviscid_flux(rgt_state, metric_along_direction, gamma, number_of_dimensions, flux_at_rgt_edge)

    ELSE

       CALL compute_inviscid_flux_Jacobian_eigensystem(region%eulerfv%Roe_state, metric_along_direction, 1.0_WP, gamma, number_of_dimensions, &
                                                       P = region%eulerfv%rotation_matrix_Roe_state, Pinv = region%eulerfv%rotation_matrix_inverse_Roe_state)
       CALL compute_local_inviscid_flux(lft_state, metric_along_direction, gamma, number_of_dimensions, region%eulerfv%flux_at_lft_state)
       CALL compute_local_inviscid_flux(rgt_state, metric_along_direction, gamma, number_of_dimensions, region%eulerfv%flux_at_rgt_state)

       region%eulerfv%s_Roe = matmul(region%eulerfv%rotation_matrix_inverse_Roe_state, (rgt_state - lft_state) / Jacobian)
       delta_coeff = speed_of_sound_Roe / (speed_of_sound_Roe + 0.5_WP * Jacobian * abs(signal_velocity_lft + signal_velocity_rgt))
       flux_at_rgt_edge = 0.0_WP
       DO i = 1, number_of_dimensions
          flux_at_rgt_edge = flux_at_rgt_edge + region%eulerfv%s_Roe(i) * region%eulerfv%rotation_matrix_Roe_state(:,i)
       END DO
       flux_at_rgt_edge = ((signal_velocity_rgt * region%eulerfv%flux_at_lft_state - signal_velocity_lft * region%eulerfv%flux_at_rgt_state) + signal_velocity_lft * &
                            signal_velocity_rgt * ((rgt_state - lft_state) / Jacobian - delta_coeff * flux_at_rgt_edge)) / (signal_velocity_rgt - signal_velocity_lft)
       
    END IF

  END SUBROUTINE compute_Roe_local_flux

  SUBROUTINE compute_local_inviscid_flux(q, metric_along_direction, gamma, number_of_dimensions, inviscid_flux)

    USE ModGlobal

    IMPLICIT NONE
    
    ! Arguments
    REAL(WP), DIMENSION(:), ALLOCATABLE, INTENT(IN) :: q, metric_along_direction
    REAL(WP), INTENT(IN) :: gamma
    INTEGER, INTENT(IN) :: number_of_dimensions
    REAL(WP), DIMENSION(:), POINTER, INTENT(IN) :: inviscid_flux

    ! Local variables
    INTEGER :: i
    REAL(WP) :: specific_volume, pressure

    specific_volume = 1.0_WP / q(1)
    pressure = (gamma - 1.0_WP) * (q(number_of_dimensions+2) - 0.5_WP * specific_volume * SUM(q(2:number_of_dimensions+1) ** 2))

    inviscid_flux(1) = 0.0_WP
    DO i = 1, number_of_dimensions
       inviscid_flux(1) = inviscid_flux(1) + metric_along_direction(i) * q(i+1)
    END DO

    DO i = 2, number_of_dimensions + 1
       inviscid_flux(i) = inviscid_flux(1) * q(i) * specific_volume + metric_along_direction(i-1) * pressure
    END DO

    inviscid_flux(number_of_dimensions+2) = inviscid_flux(1) * specific_volume * (q(number_of_dimensions+2) + pressure)

  END SUBROUTINE compute_local_inviscid_flux

  SUBROUTINE compute_inviscid_flux_Jacobian_eigensystem(q, metric_along_direction, Jacobian, gamma, number_of_dimensions, P, Lambda, Pinv)

    USE ModGlobal

    IMPLICIT NONE

    ! Arguments
    REAL(WP), DIMENSION(:), ALLOCATABLE, INTENT(IN) :: q, metric_along_direction
    REAL(WP), INTENT(IN) :: Jacobian, gamma
    INTEGER, INTENT(IN) :: number_of_dimensions
    REAL(WP), DIMENSION(:,:), ALLOCATABLE, INTENT(INOUT), OPTIONAL :: P
    REAL(WP), DIMENSION(:), ALLOCATABLE, INTENT(INOUT), OPTIONAL :: Lambda
    REAL(WP), DIMENSION(:,:), ALLOCATABLE, INTENT(INOUT), OPTIONAL :: Pinv

    ! Local variables
    REAL(WP) :: u, v, w, KE, c, k_x, k_y, k_z, k_abs, nu, rho, alpha, beta, theta

    rho = q(1) ; nu = 1.0_WP / rho
    u = nu * q(2) ; v = 0.0_WP ; w = 0.0_WP
    IF (number_of_dimensions >= 2) v = nu * q(3)
    IF (number_of_dimensions == 3) w = nu * q(4)
    KE = 0.5_WP * (u ** 2 + v ** 2 + w ** 2)
    c = SQRT(gamma * (gamma - 1.0_WP) * nu * (q(number_of_dimensions + 2) - rho * KE))
    
    k_x = metric_along_direction(1) ; k_y = 0.0_WP ; k_z = 0.0_WP
    IF (number_of_dimensions >= 2) k_y = metric_along_direction(2)
    IF (number_of_dimensions == 3) k_z = metric_along_direction(3)

    k_x = k_x * Jacobian
    k_y = k_y * Jacobian
    k_z = k_z * Jacobian

    k_abs = SQRT(k_x ** 2 + k_y ** 2 + k_z ** 2)

    IF (PRESENT(Lambda)) THEN
       Lambda(1:number_of_dimensions) = k_x * u + k_y * v + k_z * w
       Lambda(number_of_dimensions+1) = Lambda(1) + c * k_abs
       Lambda(number_of_dimensions+2) = Lambda(1) - c * k_abs
    END IF

    IF (.NOT. PRESENT(P) .AND. .NOT. PRESENT(Pinv)) RETURN

    k_x = k_x / k_abs
    k_y = k_y / k_abs
    k_z = k_z / k_abs

    theta = k_x * u + k_y * v + k_z * w
    alpha = rho / c / SQRT(2.0_WP)
    beta = nu / c / SQRT(2.0_WP)

    SELECT CASE (number_of_dimensions)

    CASE (1)

       IF (PRESENT(P)) THEN

          P(1,1) = 1.0_WP
          P(1,2) = 0.0_WP
          P(1,3) = alpha

          P(2,1) = u
          P(2,2) = 0.0_WP
          P(2,3) = alpha * (u - k_x * c)

          P(3,1) = KE
          P(3,2) = 0.0_WP
          P(3,3) = alpha * (KE + c ** 2 / (gamma - 1.0_WP) - c * theta)

       END IF

       IF (PRESENT(Pinv)) THEN

          Pinv(1,1) = 1.0_WP - (gamma - 1.0_WP) * KE / c ** 2
          Pinv(2,1) = 0.0_WP
          Pinv(3,1) = beta * ((gamma - 1.0_WP) * KE + c * theta)

          Pinv(1,2) = u * (gamma - 1.0_WP) / c ** 2
          Pinv(2,2) = 0.0_WP
          Pinv(3,2) = - beta * (k_x * c + u * (gamma - 1.0_WP))

          Pinv(1,3) = - (gamma - 1.0_WP) / c ** 2
          Pinv(2,3) = 0.0_WP
          Pinv(3,3) = beta * (gamma - 1.0_WP)

       END IF

    CASE (2)

       IF (PRESENT(P)) THEN

          P(1,1) = 1.0_WP
          P(1,2) = 0.0_WP
          P(1,3) = alpha
          P(1,4) = alpha

          P(2,1) = u
          P(2,2) = k_y * rho
          P(2,3) = alpha * (u + k_x * c)
          P(2,4) = alpha * (u - k_x * c)

          P(3,1) = v
          P(3,2) = - k_x * rho
          P(3,3) = alpha * (v + k_y * c)
          P(3,4) = alpha * (v - k_y * c)

          P(4,1) = KE
          P(4,2) = rho * (k_y * u - k_x * v)
          P(4,3) = alpha * (KE + c ** 2 / (gamma - 1.0_WP) + c * theta)
          P(4,4) = alpha * (KE + c ** 2 / (gamma - 1.0_WP) - c * theta)

       END IF

       IF (PRESENT(Pinv)) THEN

          Pinv(1,1) = 1.0_WP - (gamma - 1.0_WP) * KE / c ** 2
          Pinv(2,1) = - nu * (k_y * u - k_x * v)
          Pinv(3,1) = beta * ((gamma - 1.0_WP) * KE - c * theta)
          Pinv(4,1) = beta * ((gamma - 1.0_WP) * KE + c * theta)

          Pinv(1,2) = u * (gamma - 1.0_WP) / c ** 2
          Pinv(2,2) = nu * k_y
          Pinv(3,2) = beta * (k_x * c - u * (gamma - 1.0_WP))
          Pinv(4,2) = - beta * (k_x * c + u * (gamma - 1.0_WP))

          Pinv(1,3) = v * (gamma - 1.0_WP) / c ** 2
          Pinv(2,3) = - nu * k_x
          Pinv(3,3) = beta * (k_y * c - v * (gamma - 1.0_WP))
          Pinv(4,3) = - beta * (k_y * c + v * (gamma - 1.0_WP))

          Pinv(1,4) = - (gamma - 1.0_WP) / c ** 2
          Pinv(2,4) = 0.0_WP
          Pinv(3,4) = beta * (gamma - 1.0_WP)
          Pinv(4,4) = beta * (gamma - 1.0_WP)

       END IF

    CASE (3)

       IF (PRESENT(P)) THEN

          P(1,1) = k_x
          P(1,2) = k_y
          P(1,3) = k_z
          P(1,4) = alpha
          P(1,5) = alpha

          P(2,1) = k_x * u
          P(2,2) = k_y * u - k_z * rho
          P(2,3) = k_z * u + k_y * rho
          P(2,4) = alpha * (u + k_x * c)
          P(2,5) = alpha * (u - k_x * c)

          P(3,1) = k_x * v + k_z * rho
          P(3,2) = k_y * v
          P(3,3) = k_z * v - k_x * rho
          P(3,4) = alpha * (v + k_y * c)
          P(3,5) = alpha * (v - k_y * c)

          P(4,1) = k_x * w - k_y * rho
          P(4,2) = k_y * w + k_x * rho
          P(4,3) = k_z * w
          P(4,4) = alpha * (w + k_z * c)
          P(4,5) = alpha * (w - k_z * c)

          P(5,1) = k_x * KE + rho * (k_z * v - k_y * w)
          P(5,2) = k_y * KE + rho * (k_x * w - k_z * u)
          P(5,3) = k_z * KE + rho * (k_y * u - k_x * v)
          P(5,4) = alpha * (KE + c ** 2 / (gamma - 1.0_WP) + c * theta)
          P(5,5) = alpha * (KE + c ** 2 / (gamma - 1.0_WP) - c * theta)

       END IF

       IF (PRESENT(Pinv)) THEN

          Pinv(1,1) = k_x * (1.0_WP - (gamma - 1.0_WP) * KE / c ** 2) - nu * (k_z * v - k_y * w)
          Pinv(2,1) = k_y * (1.0_WP - (gamma - 1.0_WP) * KE / c ** 2) - nu * (k_x * w - k_z * u)
          Pinv(3,1) = k_z * (1.0_WP - (gamma - 1.0_WP) * KE / c ** 2) - nu * (k_y * u - k_x * v)
          Pinv(4,1) = beta * ((gamma - 1.0_WP) * KE - c * theta)
          Pinv(5,1) = beta * ((gamma - 1.0_WP) * KE + c * theta)

          Pinv(1,2) = k_x * u * (gamma - 1.0_WP) / c ** 2
          Pinv(2,2) = k_y * u * (gamma - 1.0_WP) / c ** 2 - nu * k_z
          Pinv(3,2) = k_z * u * (gamma - 1.0_WP) / c ** 2 + nu * k_y
          Pinv(4,2) = + beta * (k_x * c - (gamma - 1.0_WP) * u)
          Pinv(5,2) = - beta * (k_x * c + (gamma - 1.0_WP) * u)

          Pinv(1,3) = k_x * v * (gamma - 1.0_WP) / c ** 2 + nu * k_z
          Pinv(2,3) = k_y * v * (gamma - 1.0_WP) / c ** 2
          Pinv(3,3) = k_z * v * (gamma - 1.0_WP) / c ** 2 - nu * k_x
          Pinv(4,3) = + beta * (k_y * c - (gamma - 1.0_WP) * v)
          Pinv(5,3) = - beta * (k_y * c + (gamma - 1.0_WP) * v)

          Pinv(1,4) = k_x * w * (gamma - 1.0_WP) / c ** 2 - nu * k_y
          Pinv(2,4) = k_y * w * (gamma - 1.0_WP) / c ** 2 + nu * k_x
          Pinv(3,4) = k_z * w * (gamma - 1.0_WP) / c ** 2
          Pinv(4,4) = + beta * (k_z * c - (gamma - 1.0_WP) * w)
          Pinv(5,4) = - beta * (k_z * c + (gamma - 1.0_WP) * w)

          Pinv(1,5) = - k_x * (gamma - 1.0_WP) / c ** 2
          Pinv(2,5) = - k_y * (gamma - 1.0_WP) / c ** 2
          Pinv(3,5) = - k_z * (gamma - 1.0_WP) / c ** 2
          Pinv(4,5) = beta * (gamma - 1.0_WP)
          Pinv(5,5) = beta * (gamma - 1.0_WP)

       END IF

    END SELECT

  END SUBROUTINE compute_inviscid_flux_jacobian_eigensystem

  ELEMENTAL FUNCTION van_Leer_harmonic_limiter(p, q) RESULT(L)

    use ModGlobal

    IMPLICIT NONE

    REAL(WP), INTENT(IN) :: p, q
    REAL(WP) :: L

    IF (p * q <= 0.0_WP) THEN
       L = 0.0_WP
    ELSE
       L = 2.0_WP * p * q / (p + q)
    END IF

  END FUNCTION van_Leer_harmonic_limiter

END MODULE ModEulerFV
