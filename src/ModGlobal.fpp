! Copyright (c) 2015, University of Illinois at Urbana-Champaign, XPACC
! License: MIT, http://opensource.org/licenses/MIT
!-----------------------------------------------------------------------
! ModGlobal.f90
!  
! - global data
!
! Revision history
! - 18 Dec 2006 : DJB : initial code
!
! $Header: /cvsroot/genx/Codes/RocfloCM/Source/ModGlobal.f90,v 1.26 2011/10/20 16:55:21 bodony Exp $
!
!-----------------------------------------------------------------------

MODULE ModGlobal

  Implicit None

  ! ... defined constants
  INTEGER, PARAMETER :: TRUE                       = 1, &
                        FALSE                      = 0, &
                        IMPLICIT                   = 1, &
                        EXPLICIT                   = 0, &
                        COMBINED                   = 2, & 
                        PLANE_PERIODIC             = 2, &
                        PERIODIC                   = 1, &
                        EXPLICIT_RK4               = 2, &
                        EXPLICIT_RK5               = 3, &
                        IMPLICIT_RK5               = 4, &
                        IMEX_RK4                   = 5, &
                        INVISCID_ONLY              = 0, &
                        VISCOUS_ONLY               = 1, &
                        DEFAULT_VALUE              = 0, &
                        BELLERO                    = 0, &
                        ROCSMASH                   = 1, & 
                        OGEN                       = 3, &
                        SLAB                       = 0, &
                        CUBE                       = 1, &
                        CNS                        = 0, &
                        Q1D                        = 1, &
                        BDF_RK5                    = 6, &
                        PROCESSOR_IO               = 2, &
                        BDF_GMRES_IMPLICIT         = 7, &
                        BDF_GMRES_SEMIIMPLICIT     = 8, &
                        EXPLICIT_ARK2_RK4          = 9, &
                        APPROXIMATE_LINEAR_OPERATOR = 10, &
                        APPROXIMATE_LINEAR_OPERATOR_POWER_METHOD = 11, &
                        PIECE_CNST                 = 0, &
                        MINMOD                     = 1, &
                        PPM                        = 2, &
                        PQM                        = 3, &
                        THIRD_ORDER_MUSCL          = 4, &
                        LLF                        = 0, &
                        HLLC                       = 1, &
                        ENO                        = 2, &
                        WENO                       = 3, & 
                        FINITE_DIF                 = 0, &
                        FINITE_VOL                 = 1, &
                        NO_OVERLAP_PERIODIC        = 0, &
                        OVERLAP_PERIODIC           = 1, &
                        LINCNS                     = 2, &
                        PSE                        = 2, &
                        VOLUME_INTERP              = 0, &
                        SURFACE_INTERP             = 1, &
                        CYL1D                      = 3, &
                        GMRES                      = 0, &
                        DIRECT_PENTA               = 1, &
                        TV_MODEL_POWERLAW          = 0, &
                        TV_MODEL_GLASS_AND_HUNT    = 1, &
                        TV_MODEL_LOOKUP_TABLE      = 2, &
                        TV_MODEL_EXTERNAL          = 3, &
                        DV_MODEL_IDEALGAS          = 0, &
                        DV_MODEL_GLASS_AND_HUNT    = 1, &
                        DV_MODEL_THERMALLY_PERFECT = 2, &
                        DV_MODEL_IDEALGAS_MIXTURE  = 3, &
                        RIGID                      = 0, &
                        DEFORMING                  = 1, &
                        QUADRATIC                  = 2, &
                        LINEAR                     = 1, &
                        DYNAMIC_SOLN               = 1, &
                        STEADY_SOLN                = 0, &
                        SVK                        = 1, &
                        NH                         = 2, &
                        FLUID2SOLID                = 1, &
                        SOLID2FLUID                = 2

  ! ... defined constants specific to adjoint control optimization
  INTEGER, PARAMETER :: FUNCTIONAL_SOUND = 1
  INTEGER, PARAMETER :: CONTROL_OFF      = 0, &
                        CONTROL_ON       = 1

  ! ... controller type
  INTEGER, PARAMETER :: CONTROL_MASS             =  1, &
                        CONTROL_BODY_FORCE_X     =  2, &
                        CONTROL_BODY_FORCE_Y     =  3, &
                        CONTROL_BODY_FORCE_Z     =  4, &
                        CONTROL_BODY_FORCE_XY    =  5, &
                        CONTROL_BODY_FORCE_YZ    =  6, &
                        CONTROL_BODY_FORCE_ZX    =  7, &
                        CONTROL_BODY_FORCE_XYZ   =  8, &
                        CONTROL_BODY_FORCE_R     =  9, &
                        CONTROL_BODY_FORCE_THETA = 10, &
                        CONTROL_INTERNAL_ENERGY  = 11

  ! ... constrained optimization
  INTEGER, PARAMETER :: CONTROL_UNCONSTRAINED     = 0, &
                        CONTROL_CONSTRAINED_SPACE = 1, &
                        CONTROL_CONSTRAINED_TIME  = 2

  ! ... data type
  INTEGER, PARAMETER :: RFREAL = SELECTED_REAL_KIND(PRECISION(1.0D0)), &
                        DP     = SELECTED_REAL_KIND(PRECISION(1.0D0)), &
                        WP     = SELECTED_REAL_KIND(PRECISION(1.0D0))

  ! ... matrix storage types
  INTEGER, PARAMETER :: CDS = 0, & ! compressed diagonal storage
                        CRS = 1    ! compressed row storage

  ! ... small, large numbers
  REAL(RFREAL), PARAMETER :: TINY = 1E-16_rfreal

  ! ... constants
  REAL(RFREAL), PARAMETER :: TWOPI = 2.0_rfreal * acos(-1.0_rfreal)

  ! ... maximum number of dimensions
  INTEGER, PARAMETER :: MAX_ND = 3

  ! ... maximum number of boundaries
  INTEGER, PARAMETER :: MAX_BNDRY = 10

  ! ... timestep modes
  INTEGER, PARAMETER :: CONSTANT_CFL = 0, &
                        CONSTANT_DT  = 1

  ! ... output modes
  INTEGER, PARAMETER :: ITERATION = 0, &
                        SAMPLE_TIME = 1

  ! ... lu_state modes
  INTEGER, PARAMETER :: NOT_FACTORED = 0, &
                        FACTORED = 1

!!$  ! ... boundary conditions
!!$  INTEGER, PARAMETER :: SLIP_ADIABATIC_WALL = 0, &
!!$                        SPONGE = 99

  INTEGER, PARAMETER :: OUTER_BOUNDARY                                  = 00,  &
                        CUTTING_SURFACE                                 = 01,  &
                        O_GRID_PERIODICITY                              = 10,  &
                        C_MESH                                          = 11,  &
                        NSCBC_INFLOW_VELOCITY_TEMPERATURE               = 31,  &
                        NSCBC_INFLOW_VELOCITY_TEMPERATURE_FROM_SPONGE   = 32,  &
                        NSCBC_INFLOW_VELOCITY_TEMPERATURE_FORCING_SLOT  = 33,  & ! JKim 06/2008
                        NSCBC_OUTFLOW_PERFECT_NONREFLECTION             = 41,  &
                        NSCBC_OUTFLOW_PERFECT_REFLECTION                = 42,  & 
                        NSCBC_WALL_ADIABATIC_SLIP                       = 51,  &
                        NSCBC_WALL_ISOTHERMAL_NOSLIP                    = 52,  &
                        NSCBC_WALL_ADIABATIC_SLIP_COUPLED               = 53,  &
                        NSCBC_WALL_ISOTHERMAL_NOSLIP_COUPLED            = 54,  &
                        NSCBC_WALL_NOSLIP_THERMALLY_COUPLED             = 55,  &
                        SAT_SLIP_ADIABATIC_WALL_COUPLED                 = 56,  &
                        NSCBC_SUBSONIC_INFLOW                           = 61,  &
                        NSCBC_SUPERSONIC_INFLOW                         = 62,  &
                        SAT_SLIP_ADIABATIC_WALL                         = 21,  &
                        SAT_NOSLIP_ISOTHERMAL_WALL                      = 22,  &
                        SAT_NOSLIP_ADIABATIC_WALL                       = 23,  &
                        SAT_FAR_FIELD                                   = 24,  &
                        SAT_BLOCK_INTERFACE                             = 25,  & 
                        SAT_WALL_NOSLIP_THERMALLY_COUPLED               = 26,  & 
                        SAT_BLOCK_INTERFACE_PERIODIC                    = 27,  &
                        SAT_PRESSURE                                    = 28,  & !Luca1 09/2014
                        SAT_INJECTION                                   = 29,  & !Luca1 09/2014
                        SPONGE                                          = 99,  &
                        SPONGE_WITH_LINEARIZED_DISTURBANCE              = 98,  &
                        FV_TRANSMISSIVE_EXTRAPOLATION_0TH_ORDER         = 41,  &
                        FV_PERFECT_REFLECTION                           = 42,  &
                        FV_WALL_SLIP_EXTRAPOLATION_0TH_ORDER            = 51,  &
                        FV_WALL_ISOTHERMAL_NOSLIP                       = 52,  &
                        FV_SUPERSONIC_INFLOW                            = 62,  &
                        FV_PERIODICITY                                  = 10,  &
                        FV_DIRICHLET                                    = 91,  &
                        FV_NEUMANN                                      = 92,  &
                        FV_SYMMETRY                                     = 93,  &
                        FV_BLOCK_INTERFACE                              = 94,  &
                        FV_BLOCK_INTERFACE_PERIODIC                     = 95,  &
                        THERMAL_TEMPERATURE                             = 101, &
                        THERMAL_HEAT_FLUX                               = 102, &
                        STRUCTURAL_DISPLACEMENT                         = 103, &
                        STRUCTURAL_TRACTION                             = 104, &
                        STRUCTURAL_PRESSURE                             = 105, &
                        STRUCTURAL_INTERACTING                          = 106, &
                        INTEGRAL_SURFACE                                = 200, &
                        SAT_AXISYMMETRIC                                = 300, &
                        EF_DIRICHLET_G                                  = 600, &
                        EF_DIRICHLET_P                                  = 601, &
                        EF_NEUMANN                                      = 602, &
                        EF_SCHWARZ                                      = 698, & ! indicates interpolating boundaries for Schwarz solver
                        EF_DIELECTRIC                                   = 699, & ! charged particles BC at the walls
                        EF_ELECTRODE                                    = 601 ! overloading


  ! ... boundary definitions
  INTEGER, PARAMETER :: LEFT  = 0, &
                        RIGHT = 1, &
                        END   = -1

  ! ... mapping of the 26 neighbor process for box shape ghost cell exchange
  INTEGER, PARAMETER :: PROC_W     =  1, &
                        PROC_E     =  2, &
                        PROC_S     =  3, &
                        PROC_N     =  4, &
                        PROC_SW    =  5, &
                        PROC_SE    =  6, &
                        PROC_NW    =  7, &
                        PROC_NE    =  8, &
                        PROC_D     =  9, &
                        PROC_U     = 10, &
                        PROC_D_W   = 11, &
                        PROC_D_E   = 12, &
                        PROC_D_S   = 13, &
                        PROC_D_N   = 14, &
                        PROC_D_SW  = 15, &
                        PROC_D_SE  = 16, &
                        PROC_D_NW  = 17, &
                        PROC_D_NE  = 18, &
                        PROC_U_W   = 19, &
                        PROC_U_E   = 20, &
                        PROC_U_S   = 21, &
                        PROC_U_N   = 22, &
                        PROC_U_SW  = 23, &
                        PROC_U_SE  = 24, &
                        PROC_U_NW  = 25, &
                        PROC_U_NE  = 26

  ! ... grid types
  INTEGER, PARAMETER :: MONOLITHIC = 0, &
                        CHIMERA    = 1, &
                        NATIVE_SBI = 2

  ! ... metric types
  INTEGER, PARAMETER :: NONORTHOGONAL_WEAKFORM   = 0, &
                        NONORTHOGONAL_STRONGFORM = 1, &
                        CARTESIAN                = 2, &
                        STRONGFORM_NARROW        = 3, &
                        NONORTHOGONAL_WEAKFORM_OPT = 4

  ! ... advection types
  INTEGER, PARAMETER :: STANDARD_EULER = 0, &
                        SKEW_SYMMETRIC = 1

  ! ... maximum number of operators
  INTEGER, PARAMETER :: MAX_OPERATORS = 40

  ! ... maximum width of interior stencil
  INTEGER, PARAMETER :: MAX_INTERIOR_STENCIL_WIDTH = 7

  ! ... string length constants
  INTEGER, PARAMETER :: CMD_ARG_LENGTH = 256
  INTEGER, PARAMETER :: PATH_LENGTH = 256

  ! ... debugging
  LOGICAL :: debug_on = .false.

  ! Unit tests require MPI_Abort to be called if the program exits via an error condition
#ifdef BUILD_TESTS
  LOGICAL, PARAMETER :: abort_on_exit = .true.
#else
  LOGICAL, PARAMETER :: abort_on_exit = .false.
#endif

  ! ... combustion models
  ! ... (value 0 reserved for FALSE, aka no chemistry)
  INTEGER, PARAMETER :: ONESTEP           = 1, &
                        MODEL0            = 2, &
                        MODEL1            = 3

END MODULE ModGlobal
