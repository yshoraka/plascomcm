module ModElectricInterface

  interface
     subroutine ef_set_patch(solver, is, ie, bc_type, norm_dir,&
          dirichlet) bind(C)

       use iso_c_binding
       type(C_PTR) :: solver
       integer(C_INT) :: is(*), ie(*)
       integer(C_INT) :: bc_type
       integer(C_INT) :: norm_dir
       type(C_PTR) :: dirichlet

     end subroutine ef_set_patch
  end interface


  interface
     subroutine ef_init(solver, fcomm, nGlobal, nProcs, nLocal,&
                        offset, stride, cartCoord, periodic,&
                        periodicStorage, nd, ng, axisymmetric, ierr)&
                        bind(C)

       use iso_c_binding
       type(C_PTR) :: solver
       integer :: fcomm
       integer(C_INT) :: nGlobal(*), nProcs(*), nLocal(*)
       integer(C_INT) :: offset(*), stride(*), cartCoord(*), periodic(*)
       integer(C_INT) :: nd, ng
       integer(C_INT), VALUE :: axisymmetric
       integer(C_INT), VALUE :: periodicStorage
       integer(C_INT) :: ierr

     end subroutine ef_init
  end interface


  interface
     subroutine ef_set_state(solver, phi, dcoef, bcoef, jump, ierr) BIND(C)

       use iso_c_binding
       type(C_PTR) :: solver
       type(C_PTR) :: phi, dcoef, bcoef, jump
       integer(C_INT) :: ierr

     end subroutine ef_set_state
  end interface


  interface
     subroutine ef_set_rhs(solver, rhs, ierr) BIND(C)

       use iso_c_binding
       type(C_PTR) :: solver
       type(C_PTR) :: rhs
       integer(C_INT) :: ierr

     end subroutine ef_set_rhs
  end interface


  interface
     subroutine ef_set_sol(solver, sol, ierr) BIND(C)

       use iso_c_binding
       type(C_PTR) :: solver
       real(C_DOUBLE) :: sol(*)
       integer(C_INT) :: ierr

     end subroutine ef_set_sol
  end interface


  interface
     subroutine ef_set_grid(solver, is, ie, nCells, xyz, ierr) BIND(C)

       use iso_c_binding
       type(C_PTR) :: solver
       integer(C_INT) :: is(*), ie(*)
       integer(C_INT) :: nCells
       type(C_PTR) :: xyz
       integer(C_INT) :: ierr

     end subroutine ef_set_grid
  end interface


  interface
     subroutine ef_solve(solver, ierr) BIND(C)

       use iso_c_binding
       type(C_PTR) :: solver
       integer(C_INT) :: ierr

     end subroutine ef_solve
  end interface


  interface
     subroutine ef_setup_op(solver, ierr) BIND(C)

       use iso_c_binding
       type(C_PTR) :: solver
       integer(C_INT) :: ierr

     end subroutine ef_setup_op
  end interface


  interface
     subroutine ef_cleanup(solver, ierr) BIND(C)

       use iso_c_binding
       type(C_PTR) :: solver
       integer(C_INT) :: ierr

     end subroutine ef_cleanup
  end interface


  interface
     subroutine ef_plot_sol(solver, idx, ierr) BIND(C)

       use iso_c_binding
       type(C_PTR) :: solver
       integer(C_INT) :: idx
       integer(C_INT) :: ierr

     end subroutine ef_plot_sol
  end interface


end module ModElectricInterface
