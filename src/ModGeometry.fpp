! Copyright (c) 2015, University of Illinois at Urbana-Champaign, XPACC
! License: MIT, http://opensource.org/licenses/MIT
!-----------------------------------------------------------------------
!
! ModGeometry.f90
!
! - Code for on-the-fly grid generation
!
!-----------------------------------------------------------------------
MODULE ModGeometry

  integer, parameter :: MAX_TYPES = 500

  integer, parameter :: GRID_TYPE_EXTERNAL = 1
  integer, parameter :: GRID_TYPE_UNIFORM = 2
  integer, parameter :: GRID_TYPE_CYLINDER = 3

  integer, parameter :: TRANSFORM_TYPE_IBLANK_RANGE = 501
  integer, parameter :: TRANSFORM_TYPE_MOVE = 502
  integer, parameter :: TRANSFORM_TYPE_ROTATE = 503
  integer, parameter :: TRANSFORM_TYPE_SCALE = 504

  integer, parameter :: ENTRY_LENGTH = 256

CONTAINS

  SUBROUTINE GeometryInit(region)

    USE ModDataStruct
    USE ModGlobal
    USE ModMPI
    USE ModString

    IMPLICIT NONE

    TYPE(t_region), POINTER :: region

    type(t_mixt_input), pointer :: input
    type(t_gengrid), pointer :: genGrid
    integer :: i, ierr
    integer :: io_err
    integer :: strbuf_io_err
    character(len=ENTRY_LENGTH) :: strbuf
    integer :: numEntries
    character(len=ENTRY_LENGTH), allocatable :: entryStrings(:)
    character(len=ENTRY_LENGTH) :: headerString
    character(len=ENTRY_LENGTH) :: paramString
    character(len=256) :: error_string
    integer :: gridID, GridOrTransformType, gridType, transformType
    integer :: gridParamIndex, transformParamIndex
    logical, allocatable :: gridEntryRead(:)
    integer, allocatable :: numGridsOfType(:), numTransformsOfType(:)
    integer, allocatable :: nextGridParamIndex(:), nextTransformParamIndex(:)
    integer, allocatable :: nextTransform(:)

    integer, parameter :: geometry_unit = 682500

    input => region%input

    allocate(region%genGrid)
    genGrid => region%genGrid

    if (input%geometryFileName /= '') then

      if (region%myrank == 0) then

        open (unit=geometry_unit, file=trim(input%geometryFileName), status='old', iostat=io_err)
        if (io_err /= 0) then
          call graceful_exit(region%myrank, 'ERROR: Could not find/open geometry file "' // &
            trim(input%geometryFileName) // '".')
        end if

        numEntries = 0

        ! Validate and count entries
        io_err = FALSE
        do while (io_err == FALSE)
          read (geometry_unit,'(a)',iostat=io_err) strbuf
          if ((io_err == FALSE) .AND. (strbuf(1:1) /= '#') .AND. (len_trim(strbuf) /= 0)) Then
            read (strbuf,*,iostat=strbuf_io_err) gridID, GridOrTransformType
            if (strbuf_io_err /= 0) then
              call graceful_exit(region%myrank, 'ERROR: Unable to parse entry in geometry file.')
            end if
            if (.not. ValidGridType(GridOrTransformType) .and. .not. &
              ValidTransformType(GridOrTransformType)) then
              write (error_string, '(a,i0,a)') 'ERROR: Unrecognized grid or transform type ', &
                GridOrTransformType, ' in geometry file.'
              call graceful_exit(region%myrank, error_string)
            end if
            numEntries = numEntries + 1
          end if
        end do

        rewind (geometry_unit)

      end if

      Call MPI_Bcast(numEntries, 1, MPI_INTEGER, 0, mycomm, ierr)
      allocate (entryStrings(numEntries))

      if (region%myrank == 0) then

        ! Read entries
        io_err = FALSE
        i = 1
        do while (io_err == FALSE)
          read (geometry_unit,'(a)',iostat=io_err) strbuf
          if ((io_err == FALSE) .AND. (strbuf(1:1) /= '#') .AND. (len_trim(strbuf) /= 0)) Then
            entryStrings(i) = strbuf
            i = i + 1
          end if
        end do

        close (geometry_unit)

      end if

      Call MPI_Bcast(entryStrings(:), numEntries*ENTRY_LENGTH, MPI_CHARACTER, 0, mycomm, ierr)

      ! Count total number of grids and number of each type of grid
      genGrid%numGrids = 0

      allocate(numGridsOfType(1:MAX_TYPES))
      numGridsOfType = 0

      do i = 1, numEntries
        read (entryStrings(i),*) gridID, gridType
        if (ValidGridType(gridType)) then
          genGrid%numGrids = genGrid%numGrids + 1
          numGridsOfType(gridType) = numGridsOfType(gridType) + 1
        end if
      end do

      ! Allocate data structures for grid parameters
      allocate(genGrid%gridType(genGrid%numGrids))
      allocate(genGrid%gridParamIndex(genGrid%numGrids))

      do gridType = 1, MAX_TYPES
        call AllocateGridParams(region, gridType, numGridsOfType(gridType))
      end do

      ! Read grid information
      allocate(gridEntryRead(genGrid%numGrids))
      gridEntryRead = .false.

      allocate(nextGridParamIndex(1:MAX_TYPES))
      nextGridParamIndex = 1

      do i = 1, numEntries
        read (entryStrings(i),*) gridID, gridType
        if (ValidGridType(gridType)) then
          if (gridEntryRead(gridID)) then
            write (error_string, '(a,i0,2a)') 'ERROR: Grid ', gridID, ' specified multiple times', &
              ' in geometry file.'
            call graceful_exit(region%myrank, error_string)
          end if
          if (gridType == GRID_TYPE_EXTERNAL) then
            call graceful_exit(region%myrank, "ERROR: GRID_TYPE_EXTERNAL isn't currently supported.")
          end if
          call bisect_string(entryStrings(i), 2, headerString, paramString)
          genGrid%gridType(gridID) = gridType
          genGrid%gridParamIndex(gridID) = nextGridParamIndex(gridType)
          call ParseGridParams(region, paramString, gridType, nextGridParamIndex(gridType))
          nextGridParamIndex(gridType) = nextGridParamIndex(gridType) + 1
          gridEntryRead(gridID) = .true.
        end if
      end do

      ! Count number of transforms for each grid and total number of each type of transform
      allocate(genGrid%numTransforms(genGrid%numGrids))
      genGrid%numTransforms = 0

      allocate(numTransformsOfType(MAX_TYPES+1:2*MAX_TYPES))
      numTransformsOfType = 0

      io_err = FALSE
      do i = 1, numEntries
        read (entryStrings(i),*) gridID, transformType
        if (ValidTransformType(transformType)) then
          genGrid%numTransforms(gridID) = genGrid%numTransforms(gridID) + 1
          numTransformsOfType(transformType) = numTransformsOfType(transformType) + 1
        end if
      end do

      genGrid%maxTransforms = maxval(genGrid%numTransforms)

      allocate(genGrid%transformType(genGrid%maxTransforms,genGrid%numGrids))
      allocate(genGrid%transformParamIndex(genGrid%maxTransforms,genGrid%numGrids))

      do transformType = MAX_TYPES+1, 2*MAX_TYPES
        call AllocateTransformParams(region, transformType, numTransformsOfType(transformType))
      end do

      ! Read transform information
      allocate(nextTransform(genGrid%numGrids))
      nextTransform = 1

      allocate(nextTransformParamIndex(MAX_TYPES+1:2*MAX_TYPES))
      nextTransformParamIndex = 1

      do i = 1, numEntries
        read (entryStrings(i),*) gridID, transformType
        if (ValidTransformType(transformType)) then
          call bisect_string(entryStrings(i), 2, headerString, paramString)
          genGrid%transformType(nextTransform(gridID),gridID) = transformType
          genGrid%transformParamIndex(nextTransform(gridID),gridID) = &
            nextTransformParamIndex(transformType)
          call ParseTransformParams(region, paramString, transformType, &
            nextTransformParamIndex(transformType))
          nextTransform(gridID) = nextTransform(gridID) + 1
          nextTransformParamIndex(transformType) = nextTransformParamIndex(transformType) + 1
        end if
      end do

    else

      ! Keeping this here temporarily for backwards compatibility (delete later)
      genGrid%numGrids = input%numGenGrid
      allocate(genGrid%gridType(genGrid%numGrids))
      allocate(genGrid%gridParamIndex(genGrid%numGrids))
      select case (input%geometryName)
      case ('UNIFORM')
        genGrid%gridType = GRID_TYPE_UNIFORM
        genGrid%gridParamIndex = 1
        allocate(genGrid%params_uniform(1))
        genGrid%params_uniform(1)%nx = input%nxGenGrid
        if (input%ND >= 2) genGrid%params_uniform(1)%ny = input%nyGenGrid
        if (input%ND == 3) genGrid%params_uniform(1)%nz = input%nzGenGrid
        genGrid%params_uniform(1)%xmin = input%xminGenGrid/input%LengRef
        genGrid%params_uniform(1)%xmax = input%xmaxGenGrid/input%LengRef
        if (input%ND >= 2) then
          genGrid%params_uniform(1)%ymin = input%yminGenGrid/input%LengRef
          genGrid%params_uniform(1)%ymax = input%ymaxGenGrid/input%LengRef
        end if
        if (input%ND == 3) then
          genGrid%params_uniform(1)%zmin = input%zminGenGrid/input%LengRef
          genGrid%params_uniform(1)%zmax = input%zmaxGenGrid/input%LengRef
        end if
      allocate(genGrid%numTransforms(1))
      genGrid%numTransforms(1) = 0
      genGrid%maxTransforms = 0
      end select

    end if

  END SUBROUTINE GeometryInit

  FUNCTION ValidGridType(gridType)

    implicit none

    integer, intent(in) :: gridType
    logical :: ValidGridType

    select case (gridType)
    case ( &
      GRID_TYPE_EXTERNAL, &
      GRID_TYPE_UNIFORM, &
      GRID_TYPE_CYLINDER &
    )
      ValidGridType = .true.
    case default
      ValidGridType = .false.
    end select

  END FUNCTION ValidGridType

  FUNCTION ValidTransformType(transformType)

    implicit none

    integer, intent(in) :: transformType
    logical :: ValidTransformType

    select case (transformType)
    case ( &
      TRANSFORM_TYPE_IBLANK_RANGE, &
      TRANSFORM_TYPE_MOVE, &
      TRANSFORM_TYPE_ROTATE, &
      TRANSFORM_TYPE_SCALE &
    )
      ValidTransformType = .true.
    case default
      ValidTransformType = .false.
    end select

  END FUNCTION ValidTransformType

  SUBROUTINE AllocateGridParams(region, gridType, numParams)

    USE ModDataStruct

    implicit none

    type(t_region), pointer :: region
    integer :: gridType
    integer :: numParams

    type(t_gengrid), pointer :: genGrid

    genGrid => region%genGrid

    select case (gridType)
    case (GRID_TYPE_UNIFORM)
      allocate(genGrid%params_uniform(numParams))
    case (GRID_TYPE_CYLINDER)
      allocate(genGrid%params_cylinder(numParams))
    end select

  END SUBROUTINE AllocateGridParams

  SUBROUTINE ParseGridParams(region, paramString, gridType, paramIndex)

    USE ModDataStruct

    implicit none

    type(t_region), pointer :: region
    character(len=ENTRY_LENGTH) :: paramString
    integer :: gridType
    integer :: paramIndex

    type(t_gengrid), pointer :: genGrid

    genGrid => region%genGrid

    select case (gridType)
    case (GRID_TYPE_UNIFORM)
      call ParseGridParams_Uniform(region, paramString, genGrid%params_uniform(paramIndex))
    case (GRID_TYPE_CYLINDER)
      call ParseGridParams_Cylinder(region, paramString, genGrid%params_cylinder(paramIndex))
    end select

  END SUBROUTINE ParseGridParams

  SUBROUTINE ParseGridParams_Uniform(region, paramString, params)

    USE ModDataStruct
    USE ModMPI

    IMPLICIT NONE

    type(t_region), pointer :: region
    character(len=ENTRY_LENGTH) :: paramString
    type(t_gengrid_uniform) :: params

    type(t_mixt_input), pointer :: input
    integer :: io_err

    input => region%input

    select case (input%ND)
    case (1)
      read (paramString, *, iostat=io_err) params%nx, params%xmin, params%xmax
    case (2)
      read (paramString, *, iostat=io_err) params%nx, params%ny, params%xmin, params%xmax, &
        params%ymin, params%ymax
    case (3)
      read (paramString, *, iostat=io_err) params%nx, params%ny, params%nz, params%xmin, &
        params%xmax, params%ymin, params%ymax, params%zmin, params%zmax
    end select

    ! Non-dimensionalize
    params%xmin = params%xmin/input%LengRef
    params%xmax = params%xmax/input%LengRef
    if (input%ND >= 2) then
      params%ymin = params%ymin/input%LengRef
      params%ymax = params%ymax/input%LengRef
    end if
    if (input%ND == 3) then
      params%zmin = params%zmin/input%LengRef
      params%zmax = params%zmax/input%LengRef
    end if

    if (io_err /= 0) then
      call graceful_exit(region%myrank, "ERROR: Unable to parse parameters for grid type UNIFORM.")
    end if

  END SUBROUTINE ParseGridParams_Uniform

  SUBROUTINE ParseGridParams_Cylinder(region, paramString, params)

    USE ModDataStruct
    USE ModGlobal
    USE ModMPI

    IMPLICIT NONE

    type(t_region), pointer :: region
    character(len=ENTRY_LENGTH) :: paramString
    type(t_gengrid_cylinder) :: params

    type(t_mixt_input), pointer :: input
    integer :: io_err

    input => region%input

    select case (input%ND)
    case (1)
      read (paramString, *, iostat=io_err) params%nr, params%x0, params%rmin, params%rmax, &
        params%uniform_cell_aspect_ratio
    case (2)
      read (paramString, *, iostat=io_err) params%nr, params%nt, params%x0, params%y0, &
        params%rmin, params%rmax, params%tmin, params%tmax, params%uniform_cell_aspect_ratio
    case (3)
      read (paramString, *, iostat=io_err) params%nr, params%nt, params%nz, params%x0, params%y0, &
        params%rmin, params%rmax, params%tmin, params%tmax, params%zmin, params%zmax, &
        params%uniform_cell_aspect_ratio
    end select

    if (io_err /= 0) then
      call graceful_exit(region%myrank, "ERROR: Unable to parse parameters for grid type CYLINDER.")
    end if

    ! Non-dimensionalize
    params%rmin = params%rmin/input%LengRef
    params%rmax = params%rmax/input%LengRef
    params%x0 = params%x0/input%LengRef
    if (input%ND >= 2) then
      params%y0 = params%y0/input%LengRef
    end if
    if (input%ND == 3) then
      params%zmin = params%zmin/input%LengRef
      params%zmax = params%zmax/input%LengRef
    end if

    ! Rescale theta to radians
    if (input%ND >= 2) then
      params%tmin = params%tmin/2._rfreal * TWOPI
      params%tmax = params%tmax/2._rfreal * TWOPI
    end if

  END SUBROUTINE ParseGridParams_Cylinder

  SUBROUTINE GetGridSizes(region,NDIM, numGrids, ND)

    USE ModDataStruct

    IMPLICIT NONE

    TYPE(t_region), POINTER :: region
    INTEGER :: NDIM, numGrids
    INTEGER, POINTER :: ND(:,:)

    ! ... local variables
    TYPE(t_mixt_input), POINTER :: input
    TYPE(t_gengrid), POINTER :: genGrid
    INTEGER :: iGrid

    input => region%input
    genGrid => region%genGrid

    WRITE(*,*) 'PlasComCM: Using grid generation.'

    NDIM = input%ND
    numGrids = genGrid%numGrids

    WRITE(*,*) 'PlasComCM: Dimensions:',NDIM
    WRITE(*,*) 'PlasComCM: Number of Grids:',numGrids

    ALLOCATE(ND(numGrids,3))

    do iGrid = 1, numGrids
      ND(iGrid,:) = GridSize(region, iGrid)
    end do
    
    WRITE(*,*) 'PlasComCM: Grid dimensions:'
    DO iGrid = 1,numGrids
       WRITE(*,*) 'PlasComCM: Grid ',iGrid,': ',ND(iGrid,1:3)
    END DO

  END SUBROUTINE GetGridSizes

  FUNCTION GridSize(region, iGridGlobal)

    USE ModDataStruct

    implicit none

    type(t_region), pointer :: region
    integer :: iGridGlobal
    integer :: GridSize(MAX_ND)

    type(t_gengrid), pointer :: genGrid
    integer :: ND
    integer :: gridType
    integer :: paramIndex

    genGrid => region%genGrid
    ND = region%input%ND

    gridType = genGrid%gridType(iGridGlobal)
    paramIndex = genGrid%gridParamIndex(iGridGlobal)

    GridSize = 1

    select case (gridType)
    case (GRID_TYPE_UNIFORM)
      GridSize(1) = genGrid%params_uniform(paramIndex)%nx
      if (ND >= 2) GridSize(2) = genGrid%params_uniform(paramIndex)%ny
      if (ND == 3) GridSize(3) = genGrid%params_uniform(paramIndex)%nz
    case (GRID_TYPE_CYLINDER)
      GridSize(1) = genGrid%params_cylinder(paramIndex)%nr
      if (ND >= 2) GridSize(2) = genGrid%params_cylinder(paramIndex)%nt
      if (ND == 3) GridSize(3) = genGrid%params_cylinder(paramIndex)%nz
    end select

  END FUNCTION GridSize

  SUBROUTINE AllocateTransformParams(region, transformType, numParams)

    USE ModDataStruct

    implicit none

    type(t_region), pointer :: region
    integer :: transformType
    integer :: numParams

    type(t_gengrid), pointer :: genGrid

    genGrid => region%genGrid

    select case (transformType)
    case (TRANSFORM_TYPE_IBLANK_RANGE)
      allocate(genGrid%params_iblankRange(numParams))
    case (TRANSFORM_TYPE_MOVE)
      allocate(genGrid%params_move(numParams))
    case (TRANSFORM_TYPE_ROTATE)
      allocate(genGrid%params_rotate(numParams))
    case (TRANSFORM_TYPE_SCALE)
      allocate(genGrid%params_scale(numParams))
    end select

  END SUBROUTINE AllocateTransformParams

  SUBROUTINE ParseTransformParams(region, paramString, transformType, paramIndex)

    USE ModDataStruct

    implicit none

    type(t_region), pointer :: region
    character(len=ENTRY_LENGTH) :: paramString
    integer :: transformType
    integer :: paramIndex

    type(t_gengrid), pointer :: genGrid

    genGrid => region%genGrid

    select case (transformType)
    case (TRANSFORM_TYPE_IBLANK_RANGE)
      call ParseTransformParams_IBlankRange(region, paramString, &
        genGrid%params_iblankRange(paramIndex))
    case (TRANSFORM_TYPE_MOVE)
      call ParseTransformParams_Move(region, paramString, genGrid%params_move(paramIndex))
    case (TRANSFORM_TYPE_ROTATE)
      call ParseTransformParams_Rotate(region, paramString, genGrid%params_rotate(paramIndex))
    case (TRANSFORM_TYPE_SCALE)
      call ParseTransformParams_Scale(region, paramString, genGrid%params_scale(paramIndex))
    end select

  END SUBROUTINE ParseTransformParams

  SUBROUTINE ParseTransformParams_IBlankRange(region, paramString, params)

    USE ModDataStruct
    USE ModMPI

    IMPLICIT NONE

    type(t_region), pointer :: region
    character(len=ENTRY_LENGTH) :: paramString
    type(t_gengrid_iblank_range) :: params

    type(t_mixt_input), pointer :: input
    integer :: io_err

    input => region%input

    params%is = 1
    params%ie = 1

    select case (input%ND)
    case (1)
      read (paramString, *, iostat=io_err) params%iblank_value, params%is(1), params%ie(1)
    case (2)
      read (paramString, *, iostat=io_err) params%iblank_value, params%is(1), params%ie(1), &
        params%is(2), params%ie(2)
    case (3)
      read (paramString, *, iostat=io_err) params%iblank_value, params%is(1), params%ie(1), &
        params%is(2), params%ie(2), params%is(3), params%ie(3)
    end select

    if (io_err /= 0) then
      call graceful_exit(region%myrank, "ERROR: Unable to parse parameters for transformation " // &
        "type IBLANK_RANGE.")
    end if

  END SUBROUTINE ParseTransformParams_IBlankRange

  SUBROUTINE ParseTransformParams_Move(region, paramString, params)

    USE ModDataStruct
    USE ModMPI

    IMPLICIT NONE

    type(t_region), pointer :: region
    character(len=ENTRY_LENGTH) :: paramString
    type(t_gengrid_move) :: params

    type(t_mixt_input), pointer :: input
    integer :: io_err

    input => region%input

    select case (input%ND)
    case (1)
      read (paramString, *, iostat=io_err) params%dx
    case (2)
      read (paramString, *, iostat=io_err) params%dx, params%dy
    case (3)
      read (paramString, *, iostat=io_err) params%dx, params%dy, params%dz
    end select

    ! Non-dimensionalize
    params%dx = params%dx/input%LengRef
    if (input%ND >= 2) then
      params%dy = params%dy/input%LengRef
    end if
    if (input%ND == 3) then
      params%dz = params%dz/input%LengRef
    end if

    if (io_err /= 0) then
      call graceful_exit(region%myrank, "ERROR: Unable to parse parameters for transformation " // &
        "type MOVE.")
    end if

  END SUBROUTINE ParseTransformParams_Move

  SUBROUTINE ParseTransformParams_Rotate(region, paramString, params)

    USE ModDataStruct
    USE ModMPI

    IMPLICIT NONE

    type(t_region), pointer :: region
    character(len=ENTRY_LENGTH) :: paramString
    type(t_gengrid_rotate) :: params

    type(t_mixt_input), pointer :: input
    integer :: io_err

    input => region%input

    select case (input%ND)
    case (2)
      read (paramString, *, iostat=io_err) params%angle, params%x0, params%y0
    case (3)
      read (paramString, *, iostat=io_err) params%angle, params%x0, params%y0, params%z0, &
        params%nx, params%ny, params%nz
    end select

    ! Non-dimensionalize
    params%x0 = params%x0/input%LengRef
    if (input%ND >= 2) then
      params%y0 = params%y0/input%LengRef
    end if
    if (input%ND == 3) then
      params%z0 = params%z0/input%LengRef
    end if

    if (io_err /= 0) then
      call graceful_exit(region%myrank, "ERROR: Unable to parse parameters for transformation " // &
        "type ROTATE.")
    end if

    ! Rescale angle to radians
    params%angle = params%angle/2._rfreal * TWOPI

  END SUBROUTINE ParseTransformParams_Rotate

  SUBROUTINE ParseTransformParams_Scale(region, paramString, params)

    USE ModDataStruct
    USE ModMPI

    IMPLICIT NONE

    type(t_region), pointer :: region
    character(len=ENTRY_LENGTH) :: paramString
    type(t_gengrid_scale) :: params

    type(t_mixt_input), pointer :: input
    integer :: io_err

    input => region%input

    select case (region%input%ND)
    case (1)
      read (paramString, *, iostat=io_err) params%x0, params%sx
    case (2)
      read (paramString, *, iostat=io_err) params%x0, params%y0, params%sx, params%sy
    case (3)
      read (paramString, *, iostat=io_err) params%x0, params%y0, params%z0, params%sx, params%sy, &
        params%sz
    end select

    ! Non-dimensionalize
    params%x0 = params%x0/input%LengRef
    if (input%ND >= 2) then
      params%y0 = params%y0/input%LengRef
    end if
    if (input%ND == 3) then
      params%z0 = params%z0/input%LengRef
    end if

    if (io_err /= 0) then
      call graceful_exit(region%myrank, "ERROR: Unable to parse parameters for transformation " // &
        "type SCALE.")
    end if

  END SUBROUTINE ParseTransformParams_Scale

  SUBROUTINE GenerateGrid(region, ng)

    USE ModGlobal
    USE ModDataStruct
    USE ModMPI

    IMPLICIT NONE

    TYPE(t_region), POINTER :: region
    INTEGER :: ng

    ! ... local variables
    TYPE(t_grid), POINTER :: grid
    TYPE(t_gengrid), POINTER :: genGrid
    integer :: gridType
    integer :: gridParamIndex

    grid  => region%grid(ng)
    genGrid => region%genGrid

    gridType = genGrid%gridType(grid%iGridGlobal)
    gridParamIndex = genGrid%gridParamIndex(grid%iGridGlobal)

    SELECT CASE (gridType)
    CASE (GRID_TYPE_UNIFORM)
       CALL GenerateGrid_Uniform(region,grid,genGrid%params_uniform(gridParamIndex))
    CASE (GRID_TYPE_CYLINDER)
       CALL GenerateGrid_Cylinder(region,grid,genGrid%params_cylinder(gridParamIndex))
    END SELECT

  END SUBROUTINE GenerateGrid

  SUBROUTINE GenerateGrid_Uniform(region,grid,params)

    USE ModGlobal
    USE ModDataStruct
    USE ModMPI

    IMPLICIT NONE

    TYPE(t_region), POINTER :: region
    TYPE(t_grid), POINTER :: grid
    type(t_gengrid_uniform) :: params

    ! ... local variables
    TYPE(t_mixt_input), POINTER :: input
    INTEGER :: i, j, k, l0, ierr

    input => grid%input

    IF(grid%myrank_incomm == 0) THEN
       WRITE(*,'(a,i0)') 'PlasComCM: Generating UNIFORM on grid ', grid%iGridGlobal
    ENDIF

    DO k = grid%is(3), grid%ie(3)
       DO j = grid%is(2), grid%ie(2)
          DO i = grid%is(1), grid%ie(1)
             l0 = (k-grid%is(3))*(grid%ie(1)-grid%is(1)+1)*(grid%ie(2)-grid%is(2)+1) &
                  + (j-grid%is(2))*(grid%ie(1)-grid%is(1)+1) &
                  + (i-grid%is(1))+1
             grid%XYZ(l0,1) = params%xmin &
                  + (params%xmax - params%xmin) * DBLE(i-1)/ DBLE(params%nx-1)
             IF (input%ND >= 2) grid%XYZ(l0,2) = params%ymin &
                  + (params%ymax - params%ymin) * DBLE(j-1)/ DBLE(params%ny-1)
             IF (input%ND == 3) grid%XYZ(l0,3) = params%zmin &
                  + (params%zmax - params%zmin) * DBLE(k-1)/ DBLE(params%nz-1)
             grid%iblank(l0) = 1
          END DO
       END DO
    END DO

    IF(grid%myrank_incomm == 0) THEN
       WRITE(*,'(a,i0)') 'PlasComCM: Done generating UNIFORM on grid ', grid%iGridGlobal
    ENDIF

    CALL MPI_BARRIER(grid%comm,ierr)

  END SUBROUTINE GenerateGrid_Uniform

  SUBROUTINE GenerateGrid_Cylinder(region,grid,params)

    USE ModGlobal
    USE ModDataStruct
    USE ModMPI

    IMPLICIT NONE

    TYPE(t_region), POINTER :: region
    TYPE(t_grid), POINTER :: grid
    type(t_gengrid_cylinder) :: params

    ! ... local variables
    TYPE(t_mixt_input), POINTER :: input
    INTEGER :: i, j, k, l0, ierr
    real(rfreal) :: U, V, W
    real(rfreal) :: r, theta, z

    input => grid%input

    IF(grid%myrank_incomm == 0) THEN
       WRITE(*,'(a,i0)') 'PlasComCM: Generating CYLINDER on grid ', grid%iGridGlobal
    ENDIF

    DO k = grid%is(3), grid%ie(3)
       DO j = grid%is(2), grid%ie(2)
          DO i = grid%is(1), grid%ie(1)
             l0 = (k-grid%is(3))*(grid%ie(1)-grid%is(1)+1)*(grid%ie(2)-grid%is(2)+1) &
                  + (j-grid%is(2))*(grid%ie(1)-grid%is(1)+1) &
                  + (i-grid%is(1))+1
             U = DBLE(i-1)/DBLE(params%nr-1)
             if (input%ND >= 2) then
               if (grid%periodicStorage == OVERLAP_PERIODIC) then
                 V = DBLE(j-1)/DBLE(params%nt-1)
               else
                 V = DBLE(j-1)/DBLE(params%nt)
               end if
             else
               V = 0._rfreal
             end if
             if (input%ND == 3) then
               W = DBLE(k-1)/DBLE(params%nz-1)
             else
               W = 0._rfreal
             end if
             if (params%uniform_cell_aspect_ratio) then
               r = params%rmin * (params%rmax/params%rmin)**U
             else
               r = (1._rfreal - U) * params%rmin + U * params%rmax
             end if
             if (input%ND >= 2) then
               theta = (1._rfreal - V) * params%tmin + V * params%tmax
             else
               theta = 0._rfreal
             end if
             if (input%ND == 3) z = (1._rfreal - W) * params%zmin + W * params%zmax
             grid%XYZ(l0,1) = params%x0 + r * cos(theta)
             IF (input%ND >= 2) grid%XYZ(l0,2) = params%y0 + r * sin(theta)
             IF (input%ND == 3) grid%XYZ(l0,3) = z
             grid%iblank(l0) = 1
          END DO
       END DO
    END DO

    IF(grid%myrank_incomm == 0) THEN
       WRITE(*,'(a,i0)') 'PlasComCM: Done generating CYLINDER on grid ', grid%iGridGlobal
    ENDIF

    CALL MPI_BARRIER(grid%comm,ierr)

  END SUBROUTINE GenerateGrid_Cylinder

  SUBROUTINE TransformGrid(region, ng)

    USE ModGlobal
    USE ModDataStruct
    USE ModMPI

    IMPLICIT NONE

    TYPE(t_region), POINTER :: region
    INTEGER :: ng

    ! ... local variables
    integer :: i
    TYPE(t_grid), POINTER :: grid
    TYPE(t_gengrid), POINTER :: genGrid
    integer :: transformType
    integer :: transformParamIndex

    grid  => region%grid(ng)
    genGrid => region%genGrid

    do i = 1, genGrid%numTransforms(grid%iGridGlobal)
      transformType = genGrid%transformType(i,grid%iGridGlobal)
      transformParamIndex = genGrid%transformParamIndex(i,grid%iGridGlobal)
      select case (transformType)
      case (TRANSFORM_TYPE_IBLANK_RANGE)
        call TransformGrid_IBlankRange(region, grid, genGrid%params_iblankRange(transformParamIndex))
      case (TRANSFORM_TYPE_MOVE)
        call TransformGrid_Move(region, grid, genGrid%params_move(transformParamIndex))
      case (TRANSFORM_TYPE_ROTATE)
        call TransformGrid_Rotate(region, grid, genGrid%params_rotate(transformParamIndex))
      case (TRANSFORM_TYPE_SCALE)
        call TransformGrid_Scale(region, grid, genGrid%params_scale(transformParamIndex))
      end select
    end do

  END SUBROUTINE TransformGrid

  SUBROUTINE TransformGrid_IBlankRange(region, grid, params)

    USE ModGlobal
    USE ModDataStruct
    USE ModMPI

    IMPLICIT NONE

    TYPE(t_region), POINTER :: region
    TYPE(t_grid), POINTER :: grid
    type(t_gengrid_iblank_range) :: params

    ! ... local variables
    TYPE(t_mixt_input), POINTER :: input
    INTEGER :: i, j, k, l0, ierr
    integer :: is_intersection(MAX_ND), ie_intersection(MAX_ND)

    input => grid%input

    IF(grid%myrank_incomm == 0) THEN
       WRITE(*,'(a,i0)') 'PlasComCM: Applying transformation IBLANK_RANGE to grid ', grid%iGridGlobal
    ENDIF

    is_intersection = max(params%is, grid%is)
    ie_intersection = min(params%ie, grid%ie)

    DO k = is_intersection(3), ie_intersection(3)
      DO j = is_intersection(2), ie_intersection(2)
        DO i = is_intersection(1), ie_intersection(1)
          l0 = (k-grid%is(3))*(grid%ie(1)-grid%is(1)+1)*(grid%ie(2)-grid%is(2)+1) &
            + (j-grid%is(2))*(grid%ie(1)-grid%is(1)+1) + (i-grid%is(1))+1
          grid%iblank(l0) = params%iblank_value
        END DO
      END DO
    END DO

    IF(grid%myrank_incomm == 0) THEN
       WRITE(*,'(a,i0)') 'PlasComCM: Done applying transformation IBLANK_RANGE to grid ', grid%iGridGlobal
    ENDIF

    CALL MPI_BARRIER(grid%comm,ierr)

  END SUBROUTINE TransformGrid_IBlankRange

  SUBROUTINE TransformGrid_Move(region, grid, params)

    USE ModGlobal
    USE ModDataStruct
    USE ModMPI

    IMPLICIT NONE

    TYPE(t_region), POINTER :: region
    TYPE(t_grid), POINTER :: grid
    type(t_gengrid_move) :: params

    ! ... local variables
    TYPE(t_mixt_input), POINTER :: input
    INTEGER :: i, j, k, l0, ierr

    input => grid%input

    IF(grid%myrank_incomm == 0) THEN
       WRITE(*,'(a,i0)') 'PlasComCM: Applying transformation MOVE to grid ', grid%iGridGlobal
    ENDIF

    DO k = grid%is(3), grid%ie(3)
      DO j = grid%is(2), grid%ie(2)
        DO i = grid%is(1), grid%ie(1)
          l0 = (k-grid%is(3))*(grid%ie(1)-grid%is(1)+1)*(grid%ie(2)-grid%is(2)+1) &
            + (j-grid%is(2))*(grid%ie(1)-grid%is(1)+1) + (i-grid%is(1))+1
          grid%xyz(l0,1) = grid%xyz(l0,1) + params%dx
          if (input%ND >= 2) grid%xyz(l0,2) = grid%xyz(l0,2) + params%dy
          if (input%ND == 3) grid%xyz(l0,3) = grid%xyz(l0,3) + params%dz
        END DO
      END DO
    END DO

    IF(grid%myrank_incomm == 0) THEN
       WRITE(*,'(a,i0)') 'PlasComCM: Done applying transformation MOVE to grid ', grid%iGridGlobal
    ENDIF

    CALL MPI_BARRIER(grid%comm,ierr)

  END SUBROUTINE TransformGrid_Move

  SUBROUTINE TransformGrid_Rotate(region, grid, params)

    USE ModGlobal
    USE ModDataStruct
    USE ModMPI

    IMPLICIT NONE

    TYPE(t_region), POINTER :: region
    TYPE(t_grid), POINTER :: grid
    type(t_gengrid_rotate) :: params

    ! ... local variables
    TYPE(t_mixt_input), POINTER :: input
    INTEGER :: i, j, k, l0, ierr
    real(rfreal) :: C, S
    real(rfreal) :: axisLength
    real(rfreal) :: nx, ny, nz
    real(rfreal), allocatable :: Rmat(:,:)
    real(rfreal), allocatable :: origin(:), xyz_rel(:)

    input => grid%input

    IF(grid%myrank_incomm == 0) THEN
       WRITE(*,'(a,i0)') 'PlasComCM: Applying transformation ROTATE to grid ', grid%iGridGlobal
    ENDIF

    select case (input%ND)
    case (2)
      C = cos(params%angle)
      S = sin(params%angle)
      allocate(Rmat(2,2))
      Rmat(1,:) = [C, -S]
      Rmat(2,:) = [S, C]
      allocate(origin(2),xyz_rel(2))
      origin = [params%x0,params%y0]
      DO j = grid%is(2), grid%ie(2)
        DO i = grid%is(1), grid%ie(1)
          l0 = (j-grid%is(2))*(grid%ie(1)-grid%is(1)+1) + (i-grid%is(1))+1
          xyz_rel = grid%xyz(l0,:) - origin
          grid%xyz(l0,:) = origin + matmul(Rmat, xyz_rel)
        END DO
      END DO
    case (3)
      axisLength = sqrt(params%nx**2 + params%ny**2 + params%nz**2)
      nx = params%nx/axisLength
      ny = params%ny/axisLength
      nz = params%nz/axisLength
      C = cos(params%angle)
      S = sin(params%angle)
      allocate(Rmat(3,3))
      Rmat(1,:) = [C+nx*nx*(1._rfreal-C), nx*ny*(1._rfreal-C)-nz*S, nx*nz*(1._rfreal-C)+ny*S]
      Rmat(2,:) = [ny*nx*(1._rfreal-C)+nz*S, C+ny*ny*(1._rfreal-C), ny*nz*(1._rfreal-C)-nx*S]
      Rmat(3,:) = [nz*nx*(1._rfreal-C)-ny*S, nz*ny*(1._rfreal-C)+nx*S, C+nz*nz*(1._rfreal-C)]
      allocate(origin(3),xyz_rel(3))
      origin = [params%x0,params%y0,params%z0]
      DO k = grid%is(3), grid%ie(3)
        DO j = grid%is(2), grid%ie(2)
          DO i = grid%is(1), grid%ie(1)
            l0 = (k-grid%is(3))*(grid%ie(1)-grid%is(1)+1)*(grid%ie(2)-grid%is(2)+1) &
              + (j-grid%is(2))*(grid%ie(1)-grid%is(1)+1) + (i-grid%is(1))+1
            xyz_rel = grid%xyz(l0,:) - origin
            grid%xyz(l0,:) = origin + matmul(Rmat, xyz_rel)
          END DO
        END DO
      END DO
    end select

    IF(grid%myrank_incomm == 0) THEN
       WRITE(*,'(a,i0)') 'PlasComCM: Done applying transformation ROTATE to grid ', grid%iGridGlobal
    ENDIF

    CALL MPI_BARRIER(grid%comm,ierr)

  END SUBROUTINE TransformGrid_Rotate

  SUBROUTINE TransformGrid_Scale(region, grid, params)

    USE ModGlobal
    USE ModDataStruct
    USE ModMPI

    IMPLICIT NONE

    TYPE(t_region), POINTER :: region
    TYPE(t_grid), POINTER :: grid
    type(t_gengrid_scale) :: params

    ! ... local variables
    TYPE(t_mixt_input), POINTER :: input
    INTEGER :: i, j, k, l0, ierr

    input => grid%input

    IF(grid%myrank_incomm == 0) THEN
       WRITE(*,'(a,i0)') 'PlasComCM: Applying transformation SCALE to grid ', grid%iGridGlobal
    ENDIF

    DO k = grid%is(3), grid%ie(3)
      DO j = grid%is(2), grid%ie(2)
        DO i = grid%is(1), grid%ie(1)
          l0 = (k-grid%is(3))*(grid%ie(1)-grid%is(1)+1)*(grid%ie(2)-grid%is(2)+1) &
            + (j-grid%is(2))*(grid%ie(1)-grid%is(1)+1) + (i-grid%is(1))+1
          grid%xyz(l0,1) = params%x0 + params%sx * (grid%xyz(l0,1) - params%x0)
          if (input%ND >= 2) grid%xyz(l0,2) = params%y0 + params%sy * (grid%xyz(l0,2) - params%y0)
          if (input%ND == 3) grid%xyz(l0,3) = params%z0 + params%sz * (grid%xyz(l0,3) - params%z0)
        END DO
      END DO
    END DO

    IF(grid%myrank_incomm == 0) THEN
       WRITE(*,'(a,i0)') 'PlasComCM: Done applying transformation SCALE to grid ', grid%iGridGlobal
    ENDIF

    CALL MPI_BARRIER(grid%comm,ierr)

  END SUBROUTINE TransformGrid_Scale

END MODULE ModGeometry
