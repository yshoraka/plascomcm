/* Copyright (c) 2015, University of Illinois at Urbana-Champaign, XPACC */
/* License: MIT, http://opensource.org/licenses/MIT */
/*
 * Automatically detect the type of PLOT3D file to be read
 *
 * $Header: /cvsroot/genx/Codes/RocfloCM/plot3d_utils/plot3d_format.c,v 1.13 2009/09/23 13:12:20 bodony Exp $
 *
 */

#ifdef CMAKE_BUILD
#include "FC.h"
#else /* CMAKE_BUILD */
#include "plascomcmconf.h"
#endif /* CMAKE_BUILD */
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "mesh_io.h"

#define GRID_FILE 0
#define SOLN_FILE 1

#define little_endian 1
#define big_endian    2

/**
 * @brief Detects the endian for the current architecture
 * @return a the endian
 * @post a == litle_endian or a == big_endian
 */
int detectEndian( )
{
	unsigned char swaptest[ 2 ] = { 1, 0 };
	if( *(short *)swaptest == 1 )
	   return little_endian;
	return big_endian;
}

void endianSwap(long int, int, int);

size_t safe_fread(long int, int, int, int, FILE *);
size_t safe_fwrite(long int, int, int, int, FILE *);

#ifdef CMAKE_BUILD
#define WRITEDOUBLE_FC FC_GLOBAL_(writedouble,WRITEDOUBLE)
#else /* CMAKE_BUILD */
#define WRITEDOUBLE_FC FC_FUNC_(writedouble,WRITEDOUBLE)
#endif /* CMAKE_BUILD */
void WRITEDOUBLE_FC(char *fileNameIn,double *dataVals,int *numVals,int *offset,int nameLen)
{
  char *fileName;
  FILE *outputFile;
  char *a;
  double *d;
  int elSize = sizeof(double);
  int swapIt = (detectEndian() == little_endian);

  fileName = (char *)malloc((nameLen+1)*sizeof(char));
  strncpy(fileName,fileNameIn,nameLen);
  fileName[nameLen] = '\0';

  d = dataVals;

  if(swapIt){
    a = (char *)dataVals;
    endianSwap((long int)a, *numVals, elSize);
    d = (double *)a;
  }

  
  outputFile = fopen(fileName,"r+b");
  if(*offset >= 0)
    fseek(outputFile,*offset,SEEK_SET);
  else
    fseek(outputFile,0,SEEK_END);

  fwrite(d,elSize,*numVals,outputFile);
  fclose(outputFile);

  if(swapIt){
    a = (char *)d;
    endianSwap((long int)a, *numVals, elSize); 
  }

  free(fileName);
}

#ifdef CMAKE_BUILD
#define WRITEINTEGER_FC FC_GLOBAL_(writeinteger,WRITEINTEGER)
#else /* CMAKE_BUILD */
#define WRITEINTEGER_FC FC_FUNC_(writeinteger,WRITEINTEGER)
#endif /* CMAKE_BUILD */
void WRITEINTEGER_FC(char *fileNameIn,int *dataVals,int *numVals,int *offset,int nameLen)
{
  char *fileName;
  FILE *outputFile;
  char *a;
  int *d;
  int elSize = sizeof(int);
  int swapIt = (detectEndian() == little_endian);

  fileName = (char *)malloc((nameLen+1)*sizeof(char));
  strncpy(fileName,fileNameIn,nameLen);
  fileName[nameLen] = '\0';

  d = dataVals;

  if(swapIt){
    a = (char *)dataVals;
    endianSwap((long int)a, *numVals, elSize);
    d = (int *)a;
  }

  
  outputFile = fopen(fileName,"r+b");
  if(*offset >= 0)
    fseek(outputFile,*offset,SEEK_SET);
  else
    fseek(outputFile,0,SEEK_END);

  fwrite(d,elSize,*numVals,outputFile);
  fclose(outputFile);

  if(swapIt){
    a = (char *)d;
    endianSwap((long int)a, *numVals, elSize); 
  }

  free(fileName);
}

#ifdef CMAKE_BUILD
#define P3D_DETECT_FC FC_GLOBAL_(p3d_detect, P3D_DETECT)
#else /* CMAKE_BUILD */
#define P3D_DETECT_FC FC_FUNC_(p3d_detect, P3D_DETECT)
#endif /* CMAKE_BUILD */
void P3D_DETECT_FC(int *len, char *fname, char *prec, int *ND, char *gf, char *vf, char *ib,
  int *ftype)
{

  FILE *in;
  int i, record, nzones, *N, npts, el_size;
  char error_str[256];
  char *fname_nullterm;

  /* terminate string */
  fname_nullterm = (char *)malloc((*len+1)*sizeof(char));
  strncpy(fname_nullterm, fname, *len);
  fname_nullterm[*len] = '\0';

  if ((in = fopen(fname_nullterm,"rb")) == NULL) {
    sprintf(error_str, "Unable to open file %s", fname_nullterm);
    perror(error_str);
    goto cleanup1;
  }

  int endian = detectEndian( );

  /* read first record: if record == sizeof(int): multizone, else single zone */
  safe_fread((long int)&record,sizeof(int),1, endian, in);

  /* if we have a multi-zone file */
  if (record == sizeof(int)) {
    *gf = 'm';
    safe_fread((long int)&nzones,sizeof(int),1, endian, in);
    safe_fread((long int)&record,sizeof(int),1, endian, in);
    safe_fread((long int)&record,sizeof(int),1, endian, in);

  } else { /* we have a single-zone file */
    *gf = 's';
    nzones = 1;
  }

  /* check number of dimensions: ND = record / (nzones * sizeof(int)) */
  *(ND) = record / (nzones * sizeof(int));

  /* read the zonal dimensions */
  N = (int *)malloc((size_t)record);
  safe_fread((long int)N, sizeof(int), (size_t)(record/sizeof(int)), endian, in);
  safe_fread((long int)&record,sizeof(int),1, endian, in);

  /* are we a solution file or a grid file */
  safe_fread((long int)&record,sizeof(int),1, endian, in);

  if ((record == (4 * sizeof(float))) || (record == (4 * sizeof(double)))) {
    *(ftype) = SOLN_FILE;
  } else {
    *(ftype) = GRID_FILE;
  }
  fseek(in,(long)(-sizeof(int)),SEEK_CUR);

  if (*(ftype) == GRID_FILE) {

    /* figure out if we're single/double precision and whole or planes */
    npts = 1; for (i = 0; i < *(ND); i++) npts *= N[i];
    safe_fread((long int)&record,sizeof(int),1, endian, in);
    if (record == (*(ND) * npts * sizeof(float) + npts * sizeof(int))) {
      *vf = 'w';
      *prec = 's';
      *ib = 'y';
      goto cleanup2;
    } else if (record == (*(ND) * npts * sizeof(double) + npts * sizeof(int))) {
      *vf = 'w';
      *prec = 'd';
      *ib = 'y';
      goto cleanup2;
    } else if (record == (*(ND) * npts * sizeof(float))) {
      *vf = 'w';
      *prec = 's';
      *ib = 'n';
      goto cleanup2;
    } else if (record == (*(ND) * npts * sizeof(double))) {
      *vf = 'w';
      *prec = 'd';
      *ib = 'n';
      goto cleanup2;
    }

    /* we must be planes */
    npts = 1; for (i = 0; i < *(ND)-1; i++) npts *= N[i];
    if (record == (*(ND) * npts * sizeof(float)) + npts * sizeof(int)) {
      *vf = 'p';
      *prec = 's';
      *ib = 'y';
      goto cleanup2;
    } else if (record == (*(ND) * npts * sizeof(double) + npts * sizeof(int))) {
      *vf = 'p';
      *prec = 'd';
      *ib = 'y';
      goto cleanup2;
    } else if (record == (*(ND) * npts * sizeof(float))) {
      *vf = 'p';
      *prec = 's';
      *ib = 'n';
      goto cleanup2;
    } else if (record == (*(ND) * npts * sizeof(double))) {
      *vf = 'p';
      *prec = 'd';
      *ib = 'n';
      goto cleanup2;
    }


  }

  if (*(ftype) == SOLN_FILE) {

    /* figure out if we're single/double precision */
    *prec = 'u';
    safe_fread((long int)&record,sizeof(int),1, endian, in);
    if (record == ( 4 * sizeof(float) )) {
      *prec = 's';
      el_size = 4;
    } else if (record == ( 4 * sizeof(double) )) {
      *prec = 'd';
      el_size = 8;
    } else *prec = 'u';
    fseek(in,record,SEEK_CUR);
    safe_fread((long int)&record,sizeof(int),1, endian, in);

    /* figure out if we're planes or whole */
    npts = 1; for (i = 0; i < *(ND); i++) npts *= N[i];
    safe_fread((long int)&record,sizeof(int),1, endian, in);
    if (record == ((*(ND)+2) * npts * el_size)) {
      *vf = 'w';
      goto cleanup2;
    }

    /* we must be planes */
    npts = 1; for (i = 0; i < *(ND)-1; i++) npts *= N[i];
    if (record == ((*(ND)+2) * npts * el_size)) {
      *vf = 'p';
      goto cleanup2;
    }

    *vf = 'u';

  }

  cleanup2:
    fclose(in);
    free(N);

  cleanup1:
    free(fname_nullterm);

  return;

}

size_t safe_fread(long int addr, int size_el, int num_el, int endian, FILE *in)
{
  size_t retval;
  char *a;

  a = (char *)addr;

  retval = fread(a, size_el, num_el, in);

  if( endian == little_endian )
	endianSwap((long int)a, num_el, size_el);

  return(retval);
}

size_t safe_fwrite(long int addr, int size_el, int num_el, int endian, FILE *out)
{
  size_t retval;
  char *a;
  double *d;
  int *i;

  a = (char *)addr;

  if( endian == little_endian )
	endianSwap((long int)a, num_el, size_el);

  if (size_el == sizeof(int)) {
    i = (int *)a;
    retval = fwrite(i, size_el, num_el, out);
    if (retval == 0) perror("ERROR Writing.\n");
  }

  if (size_el == sizeof(double)) {
    d = (double *)a;
    retval = fwrite(d, size_el, num_el, out);
    if (retval == 0) perror("ERROR Writing.\n");
  }

  return(retval);
 

}


/*********************************************************************
 *
 * endianSwap(addr,num_el,size_el): Changes between big- and
 * little-endian by address-swapping.
 *
 * addr: (long int) address of variable to be swapped.
 * num_el: (int) number of elements in array pointed to by addr
 * size_el: (int) size of individual elements of *addr
 *
 * Written by Daniel J. Bodony (bodony@Stanford.EDU)
 * Copyright (c) 2001
 *
 * WARNING: This is only works on arrays that contiguous in memory.
 *
 *********************************************************************/

#define SWAP(a,b) temp=(a); (a)=(b); (b)=temp;

void endianSwap (long int addr, int num_el, int size_el)
{
   int i, x;
   char *a;
   char temp;

   a = (char *)addr;

   for (i = 0; i < num_el; i++) {
      a = (char *)(addr + i*size_el);
      for (x = 0; x < size_el/2; x++) {
         SWAP(a[x],a[size_el-x-1]);
      }
   }
}


/* read a plot3d grid in whole format without IBLANK */
#ifdef CMAKE_BUILD
#define READ_PLOT3D_SINGLE_GRID_WHOLE_FORMAT_NO_IBLANK_LOW_MEM_FC FC_GLOBAL_( \
  read_plot3d_single_grid_whole_format_no_iblank_low_mem, \
  READ_PLOT3D_SINGLE_GRID_WHOLE_FORMAT_NO_IBLANK_LOW_MEM)
#else /* CMAKE_BUILD */
#define READ_PLOT3D_SINGLE_GRID_WHOLE_FORMAT_NO_IBLANK_LOW_MEM_FC FC_FUNC_( \
  read_plot3d_single_grid_whole_format_no_iblank_low_mem, \
  READ_PLOT3D_SINGLE_GRID_WHOLE_FORMAT_NO_IBLANK_LOW_MEM)
#endif /* CMAKE_BUILD */
void READ_PLOT3D_SINGLE_GRID_WHOLE_FORMAT_NO_IBLANK_LOW_MEM_FC(int *NDIM, int *ND, int *offset, int *is,
  int *ie, int *gridID, double *Q, char *fname, int *len)
{

  FILE *gridFile = NULL;
  int i, j, k, l, chunk[3], chunk_total, endian, grid_size, N[3], l0;
  char error_str[256];
  int sizeof_int, sizeof_dbl;
  double *dbuf = NULL;
  char *fname_nullterm;

  /* terminate string */
  fname_nullterm = (char *)malloc((*len+1)*sizeof(char));
  strncpy(fname_nullterm, fname, *len);
  fname_nullterm[*len] = '\0';

  /* error on failed open */
  if ((gridFile = fopen(fname_nullterm,"rb")) == NULL) {
    sprintf(error_str,"%s %s %s","Unable to open file ", fname_nullterm, ".\n");
    perror(error_str);
    goto cleanup1;
  }

  /* check endian :: default is big endian */
  endian = detectEndian( );

  /* seek past offset */
  if (fseek(gridFile, (long int)(*offset), SEEK_SET) != 0) {
    sprintf(error_str,"%s %s %s %s %s","ERROR in fseek on file ", fname_nullterm, "in", __FILE__,".\n");
    perror(error_str);
    goto cleanup2;
  }

  /* this block is written in the FORTRAN format */
  /* Write (10) ((((X(L,I,J,K,M),I=1,ND(L,1)),J=1,ND(L,2)),K=1,ND(L,3)),M=1,NDIM) */

  /* now start reading, in chunks when possible */
  chunk_total = 0; grid_size = 1;
  for (i = 0; i < *NDIM; i++) { 
    N[i] = ie[i] - is[i] + 1;
    chunk[i] = N[i] - ND[i]; 
    chunk_total += chunk[i]; 
    grid_size *= N[i];
  }


  /* we can read the entire block */
  if (chunk_total == 0) { safe_fread((long int)Q, sizeof(double), grid_size*(*NDIM), endian, gridFile); }
  
  else {

    dbuf = (double *)malloc(ND[0]*sizeof(double));
    for (l = 0; l < (*NDIM); l++) {
    for (k = 0; k < ND[2]; k++) {
    for (j = 0; j < ND[1]; j++) {
      safe_fread((long int)dbuf, sizeof(double), ND[0], endian, gridFile);
      if ((j >= is[1] && j <= ie[1]) && (k >= is[2] && k <= ie[2])) {
        for (i = is[0]; i <= ie[0]; i++) {
          l0 = N[1]*N[0]*(k-is[2]) + N[0]*(j-is[1]) + i-is[0];
          Q[l*grid_size + l0] = dbuf[i];
        }
      }
    }}}
    free(dbuf);

  }

  cleanup2:
    fclose(gridFile);

  cleanup1:
    free(fname_nullterm);

  return;

}


/* read a plot3d grid in whole format without IBLANK */
#ifdef CMAKE_BUILD
#define READ_PLOT3D_SINGLE_GRID_MPIIO_FC FC_GLOBAL_( \
  read_plot3d_single_grid_mpiio, \
  READ_PLOT3D_SINGLE_GRID_MPIIO)
#else /* CMAKE_BUILD */
#define READ_PLOT3D_SINGLE_GRID_MPIIO_FC FC_FUNC_( \
  read_plot3d_single_grid_mpiio, \
  READ_PLOT3D_SINGLE_GRID_MPIIO)
#endif /* CMAKE_BUILD */
void READ_PLOT3D_SINGLE_GRID_MPIIO_FC(int *inCommunicator,int *NDIM, int *ND, int *offset, int *is,
				      int *ie, int *gridID, double *Q, char *fname, int *len)
{

  char *fname_nullterm;
  MPI_Comm gridCommunicator;
  MPI_Info info;
  MPI_File grid_inf;
  int amode = MPI_MODE_RDONLY;
  int *meshSizes;
  int *localStart;
  int *fileStart;
  int rank;
  int endian;
  int numPointsGlobal = 1;
  int numPointsLocal = 1;
  int currentOffset = *offset;
  int iDim;

  meshSizes  = (int *)malloc((*NDIM)*sizeof(int));
  localStart = (int *)malloc((*NDIM)*sizeof(int));
  fileStart  = (int *)malloc((*NDIM)*sizeof(int));
  for(iDim=0;iDim < *NDIM;iDim++){
    meshSizes[iDim]  = (ie[iDim] - is[iDim] + 1);
    localStart[iDim] = 0;
    fileStart[iDim]  = is[iDim];
    numPointsGlobal  = numPointsGlobal * ND[iDim];
    numPointsLocal   = numPointsLocal * meshSizes[iDim];
  }

  gridCommunicator = MPI_Comm_f2c(*inCommunicator);

  /* terminate string */
  fname_nullterm = (char *)malloc((*len+1)*sizeof(char));
  strncpy(fname_nullterm, fname, *len);
  fname_nullterm[*len] = '\0';

  MPI_Info_create(&info);
  /*
  MPI_Info_set(info, "striping_factor", "4");
  MPI_Info_set(info, "striping_unit", "1048576");
  MPI_Info_set(info, "collective_buffering", "true");
  MPI_Info_set(info, "cb_block_size", "1048576");
  */
  MPI_Comm_rank(gridCommunicator, &rank);
  /*  printf("Rank %d opening %s\n",rank,fname_nullterm); */
  int returnCode = MPI_File_open(gridCommunicator, fname_nullterm, amode, info, &grid_inf);
  if(returnCode != MPI_SUCCESS){
    perror("MPI_File_open failed");
    exit(1);
  }

  /* File is big endian */
  endian = MESH_IO_BIG_ENDIAN;

  /*  printf("Rank %d has %d points of %d points.\n",rank,numPointsLocal,numPointsGlobal); */
  /* this block is written in the FORTRAN format */
  /* Write (10) ((((X(L,I,J,K,M),I=1,ND(L,1)),J=1,ND(L,2)),K=1,ND(L,3)),M=1,NDIM) */
  for(iDim = 0;iDim < *NDIM;iDim++){
    /*    printf("Rank %d has offset %d.\n",rank,currentOffset); */
    returnCode = Mesh_IO_read(grid_inf,currentOffset,MPI_DOUBLE,endian,&Q[iDim*numPointsLocal],*NDIM,
			      meshSizes,ND,fileStart,MPI_ORDER_FORTRAN,
			      meshSizes,localStart,MPI_ORDER_FORTRAN,gridCommunicator);
    if(returnCode != MPI_SUCCESS){
      perror("Mesh_IO_read failed");    
      exit(1);
    }
    currentOffset = currentOffset + (numPointsGlobal*sizeof(double));
  }
  
  MPI_File_close(&grid_inf);
  MPI_Info_free(&info);
  free(meshSizes);
  free(localStart);
  free(fileStart);
  free(fname_nullterm);

  return;
}

/* read a plot3d grid in whole format with an IBLANK */
#ifdef CMAKE_BUILD
#define READ_PLOT3D_SINGLE_GRID_IB_MPIIO_FC FC_GLOBAL_( \
  read_plot3d_single_grid_ib_mpiio, \
  READ_PLOT3D_SINGLE_GRID_IB_MPIIO)
#else /* CMAKE_BUILD */
#define READ_PLOT3D_SINGLE_GRID_IB_MPIIO_FC FC_FUNC_( \
  read_plot3d_single_grid_ib_mpiio, \
  READ_PLOT3D_SINGLE_GRID_IB_MPIIO)
#endif
void READ_PLOT3D_SINGLE_GRID_IB_MPIIO_FC(int *inCommunicator,int *NDIM, int *ND, int *offset, int *is,
					 int *ie, int *gridID, double *Q, int *IB,char *fname, int *len)
{

  char *fname_nullterm;
  MPI_Comm gridCommunicator;
  MPI_Info info;
  MPI_File grid_inf;
  int amode = MPI_MODE_RDONLY;
  int *meshSizes;
  int *localStart;
  int *fileStart;
  int rank;
  int endian;
  int numPointsGlobal = 1;
  int numPointsLocal = 1;
  int currentOffset = *offset;
  int iDim;

  meshSizes  = (int *)malloc((*NDIM)*sizeof(int));
  localStart = (int *)malloc((*NDIM)*sizeof(int));
  fileStart  = (int *)malloc((*NDIM)*sizeof(int));
  for(iDim=0;iDim < *NDIM;iDim++){
    meshSizes[iDim]  = (ie[iDim] - is[iDim] + 1);
    localStart[iDim] = 0;
    fileStart[iDim]  = is[iDim];
    numPointsGlobal *= ND[iDim];
    numPointsLocal  *= meshSizes[iDim];
  }
 
  gridCommunicator = MPI_Comm_f2c(*inCommunicator);

  /* terminate string */
  fname_nullterm = (char *)malloc((*len+1)*sizeof(char));
  strncpy(fname_nullterm, fname, *len);
  fname_nullterm[*len] = '\0';

  MPI_Info_create(&info);
  /*
  MPI_Info_set(info, "striping_factor", "4");
  MPI_Info_set(info, "striping_unit", "1048576");
  MPI_Info_set(info, "collective_buffering", "true");
  MPI_Info_set(info, "cb_block_size", "1048576");
  */
  MPI_Comm_rank(gridCommunicator, &rank);
  /*  printf("Rank %d opening %s\n",rank,fname_nullterm); */
  int returnCode = MPI_File_open(gridCommunicator, fname_nullterm, amode, info, &grid_inf);
  if(returnCode != MPI_SUCCESS){
    perror("MPI_File_open failed");
    exit(1);
  }

  /* File is big endian */
  endian = MESH_IO_BIG_ENDIAN;

/*   printf("Rank %d has %d points of %d points.\n",rank,numPointsLocal,numPointsGlobal);  */
  /* this block is written in the FORTRAN format */
  /* Write (10) ((((X(L,I,J,K,M),I=1,ND(L,1)),J=1,ND(L,2)),K=1,ND(L,3)),M=1,NDIM) */
  for(iDim = 0;iDim < *NDIM;iDim++){
/*     printf("Rank %d has offset %d.\n",rank,currentOffset);  */
    returnCode = Mesh_IO_read(grid_inf,currentOffset,MPI_DOUBLE,endian,&Q[iDim*numPointsLocal],*NDIM,
			      meshSizes,ND,fileStart,MPI_ORDER_FORTRAN,
			      meshSizes,localStart,MPI_ORDER_FORTRAN,gridCommunicator);
    if(returnCode != MPI_SUCCESS){
      perror("Mesh_IO_read failed");
      exit(1);
    }
    currentOffset = currentOffset + (numPointsGlobal*sizeof(double));
  }
/*   printf("Rank %d has iblank offset %d.\n",rank,currentOffset);  */
  returnCode = Mesh_IO_read(grid_inf,currentOffset,MPI_INT,endian,&IB[0],*NDIM,
			    meshSizes,ND,fileStart,MPI_ORDER_FORTRAN,
			    meshSizes,localStart,MPI_ORDER_FORTRAN,gridCommunicator);
  if(returnCode != MPI_SUCCESS){
    perror("Mesh_IO_read iblank failed");
    exit(1);
  }

  MPI_File_close(&grid_inf);
  MPI_Info_free(&info);
  free(meshSizes);
  free(localStart);
  free(fileStart);
  free(fname_nullterm);
  return;
}

/* read a plot3d grid in whole format with IBLANK */
#ifdef CMAKE_BUILD
#define READ_PLOT3D_SINGLE_GRID_WHOLE_FORMAT_WITH_IBLANK_LOW_MEM_FC FC_GLOBAL_( \
  read_plot3d_single_grid_whole_format_with_iblank_low_mem, \
  READ_PLOT3D_SINGLE_GRID_WHOLE_FORMAT_WITH_IBLANK_LOW_MEM)
#else /* CMAKE_BUILD */
#define READ_PLOT3D_SINGLE_GRID_WHOLE_FORMAT_WITH_IBLANK_LOW_MEM_FC FC_FUNC_( \
  read_plot3d_single_grid_whole_format_with_iblank_low_mem, \
  READ_PLOT3D_SINGLE_GRID_WHOLE_FORMAT_WITH_IBLANK_LOW_MEM)
#endif /* CMAKE_BUILD */
void READ_PLOT3D_SINGLE_GRID_WHOLE_FORMAT_WITH_IBLANK_LOW_MEM_FC(int *NDIM, int *ND, int *offset,
  int *is, int *ie, int *gridID, double *Q, int *IB, char *fname, int *len)
{

  FILE *gridFile = NULL;
  int i, j, k, l, chunk[3], chunk_total, endian, grid_size, N[3], l0;
  char error_str[256];
  int sizeof_int, sizeof_dbl;
  double *dbuf = NULL;
  int *ibuf = NULL;
  char *fname_nullterm;

  /* terminate string */
  fname_nullterm = (char *)malloc((*len+1)*sizeof(char));
  strncpy(fname_nullterm, fname, *len);
  fname_nullterm[*len] = '\0';

  /* error on failed open */
  if ((gridFile = fopen(fname_nullterm,"rb")) == NULL) {
    sprintf(error_str,"%s %s %s","Unable to open file ", fname_nullterm, ".\n");
    perror(error_str);
    goto cleanup1;
  }

  /* check endian :: default is big endian */
  endian = detectEndian( );

  /* seek past offset */
  if (fseek(gridFile, (long int)(*offset), SEEK_SET) != 0) {
    sprintf(error_str,"%s %s %s %s %s","ERROR in fseek on file ", fname_nullterm, "in", __FILE__,".\n");
    perror(error_str);
    goto cleanup2;
  }

  /* this block is written in the FORTRAN format */
  /* Write (10) ((((X(L,I,J,K,M),I=1,ND(L,1)),J=1,ND(L,2)),K=1,ND(L,3)),M=1,NDIM) */

  /* now start reading, in chunks when possible */
  chunk_total = 0; grid_size = 1;
  for (i = 0; i < *NDIM; i++) { 
    N[i] = ie[i] - is[i] + 1;
    chunk[i] = N[i] - ND[i]; 
    chunk_total += chunk[i]; 
    grid_size *= N[i];
  }

  /* we can read the entire block */
  if (chunk_total == 0) { 
    safe_fread((long int)Q, sizeof(double), grid_size*(*NDIM), endian, gridFile); 
    safe_fread((long int)IB, sizeof(int), grid_size, endian, gridFile);
  }
  
  else {

    dbuf = (double *)malloc(ND[0]*sizeof(double));
    for (l = 0; l < (*NDIM); l++) {
    for (k = 0; k < ND[2]; k++) {
    for (j = 0; j < ND[1]; j++) {
      safe_fread((long int)dbuf, sizeof(double), ND[0], endian, gridFile);
      if ((j >= is[1] && j <= ie[1]) && (k >= is[2] && k <= ie[2])) {
        for (i = is[0]; i <= ie[0]; i++) {
          l0 = N[1]*N[0]*(k-is[2]) + N[0]*(j-is[1]) + i-is[0];
          Q[l*grid_size + l0] = dbuf[i];
        }
      }
    }}}
    free(dbuf);

    ibuf = (int *)malloc(ND[0]*sizeof(int));
    for (k = 0; k < ND[2]; k++) {
    for (j = 0; j < ND[1]; j++) {
      safe_fread((long int)ibuf, sizeof(int), ND[0], endian, gridFile);
      if ((j >= is[1] && j <= ie[1]) && (k >= is[2] && k <= ie[2])) {
        for (i = is[0]; i <= ie[0]; i++) {
          l0 = N[1]*N[0]*(k-is[2]) + N[0]*(j-is[1]) + i-is[0];
          IB[l0] = ibuf[i];
        }
      }
    }}
    free(ibuf);

  }

  cleanup2:
    fclose(gridFile);

  cleanup1:
    free(fname_nullterm);

  return;

}

/* read a plot3d solution in whole format without IBLANK */
#ifdef CMAKE_BUILD
#define READ_PLOT3D_SINGLE_SOLN_MPIIO_FC FC_GLOBAL_( \
  read_plot3d_single_soln_mpiio, \
  READ_PLOT3D_SINGLE_SOLN_MPIIO)
#else /* CMAKE_BUILD */
#define READ_PLOT3D_SINGLE_SOLN_MPIIO_FC FC_FUNC_( \
  read_plot3d_single_soln_mpiio, \
  READ_PLOT3D_SINGLE_SOLN_MPIIO)
#endif /* CMAKE_BUILD */
void READ_PLOT3D_SINGLE_SOLN_MPIIO_FC(int *inCommunicator,int *NDIM, int *ND, int *offset, int *is,
				      int *ie, int *gridID, double *Q, char *fname, int *len)
{

  char *fname_nullterm;
  MPI_Comm gridCommunicator;
  MPI_Info info;
  MPI_File grid_inf;
  int amode = MPI_MODE_RDONLY;
  int *meshSizes;
  int *localStart;
  int *fileStart;
  int rank;
  int endian;
  int numPointsGlobal = 1;
  int numPointsLocal = 1;
  int currentOffset = *offset;
  int iDim;

  meshSizes  = (int *)malloc((*NDIM)*sizeof(int));
  localStart = (int *)malloc((*NDIM)*sizeof(int));
  fileStart  = (int *)malloc((*NDIM)*sizeof(int));
  for(iDim=0;iDim < *NDIM;iDim++){
    meshSizes[iDim]  = (ie[iDim] - is[iDim] + 1);
    localStart[iDim] = 0;
    fileStart[iDim]  = is[iDim];
    numPointsGlobal  = numPointsGlobal * ND[iDim];
    numPointsLocal   = numPointsLocal * meshSizes[iDim];
  }
 
  gridCommunicator = MPI_Comm_f2c(*inCommunicator);

  /* terminate string */
  fname_nullterm = (char *)malloc((*len+1)*sizeof(char));
  strncpy(fname_nullterm, fname, *len);
  fname_nullterm[*len] = '\0';

  MPI_Info_create(&info);
  /*
  MPI_Info_set(info, "striping_factor", "4");
  MPI_Info_set(info, "striping_unit", "1048576");
  MPI_Info_set(info, "collective_buffering", "true");
  MPI_Info_set(info, "cb_block_size", "1048576");
  */
  MPI_Comm_rank(gridCommunicator, &rank);
  /*  printf("Rank %d opening %s\n",rank,fname_nullterm); */
  int returnCode = MPI_File_open(gridCommunicator, fname_nullterm, amode, info, &grid_inf);
  if(returnCode != MPI_SUCCESS){
    perror("MPI_File_open failed");
    exit(1);
  }

  /* File is big endian */
  endian = MESH_IO_BIG_ENDIAN;

  /*  printf("Rank %d has %d points of %d points.\n",rank,numPointsLocal,numPointsGlobal); */
  /* this block is written in the FORTRAN format */
  /* Write (10) ((((X(L,I,J,K,M),I=1,ND(L,1)),J=1,ND(L,2)),K=1,ND(L,3)),M=1,NDIM) */
  for(iDim = 0;iDim < *NDIM+2;iDim++){
    /*    printf("Rank %d has offset %d.\n",rank,currentOffset); */
    returnCode = Mesh_IO_read(grid_inf,currentOffset,MPI_DOUBLE,endian,&Q[iDim*numPointsLocal],*NDIM,
			      meshSizes,ND,fileStart,MPI_ORDER_FORTRAN,
			      meshSizes,localStart,MPI_ORDER_FORTRAN,gridCommunicator);
    if(returnCode != MPI_SUCCESS){
      perror("Mesh_IO_read failed");    
      exit(1);
    }
    currentOffset = currentOffset + (numPointsGlobal*sizeof(double));
  }
  
  MPI_File_close(&grid_inf);
  MPI_Info_free(&info);
  free(meshSizes);
  free(localStart);
  free(fileStart);
  free(fname_nullterm);
  return;
}


/* write a plot3d solution in whole format without IBLANK */
#ifdef CMAKE_BUILD
#define WRITE_PLOT3D_SOLN_MPIIO_FC FC_GLOBAL_( \
					    write_plot3d_soln_mpiio,	\
					    WRITE_PLOT3D_SOLN_MPIIO)
#else
#define WRITE_PLOT3D_SOLN_MPIIO_FC FC_FUNC_( \
					    write_plot3d_soln_mpiio,	\
					    WRITE_PLOT3D_SOLN_MPIIO)
#endif
void WRITE_PLOT3D_SOLN_MPIIO_FC(int *inCommunicator,int *NDIM, int *ND, int *offset, int *is,
				int *ie, double *Q, char *fname, int *len)
{

  char *fname_nullterm;
  MPI_Comm gridCommunicator;
  MPI_Info info;
  MPI_File grid_inf;
  int amode = MPI_MODE_WRONLY|MPI_MODE_APPEND;
  int *meshSizes;
  int *localStart;
  int *fileStart;
  int rank;
  int endian;
  int numPointsGlobal = 1;
  int numPointsLocal = 1;
  int currentOffset = *offset;
  int iDim;
  char errorString[MPI_MAX_ERROR_STRING];
  int errorSize = 0;

  meshSizes  = (int *)malloc((*NDIM)*sizeof(int));
  localStart = (int *)malloc((*NDIM)*sizeof(int));
  fileStart  = (int *)malloc((*NDIM)*sizeof(int));
  for(iDim=0;iDim < *NDIM;iDim++){
    meshSizes[iDim]  = (ie[iDim] - is[iDim] + 1);
    localStart[iDim] = 0;
    fileStart[iDim]  = is[iDim];
    numPointsGlobal  = numPointsGlobal * ND[iDim];
    numPointsLocal   = numPointsLocal * meshSizes[iDim];
  }
  
  gridCommunicator = MPI_Comm_f2c(*inCommunicator);
  
  /* terminate string */
  fname_nullterm = (char *)malloc((*len+1)*sizeof(char));
  strncpy(fname_nullterm, fname, *len);
  fname_nullterm[*len] = '\0';
  
  MPI_Info_create(&info);
  /*
    MPI_Info_set(info, "striping_factor", "4");
    MPI_Info_set(info, "striping_unit", "1048576");
    MPI_Info_set(info, "collective_buffering", "true");
    MPI_Info_set(info, "cb_block_size", "1048576");
  */
  MPI_Comm_rank(gridCommunicator, &rank);
  int returnCode = MPI_File_open(gridCommunicator, fname_nullterm, amode, info, &grid_inf);
  if(returnCode != MPI_SUCCESS){
    MPI_Error_string(returnCode,errorString,&errorSize);
    errorString[errorSize] = '\0';
    printf("MPI_File_open: %s\n",errorString);
    /*   perror("MPI_File_open failed"); */
    exit(1);
  }
  
  /* File is big endian */
  endian = MESH_IO_BIG_ENDIAN;
  
  /*  printf("Rank %d has %d points of %d points.\n",rank,numPointsLocal,numPointsGlobal); */
  /* this block is written in the FORTRAN format */
  /* Write (10) ((((X(L,I,J,K,M),I=1,ND(L,1)),J=1,ND(L,2)),K=1,ND(L,3)),M=1,NDIM) */
  /*  printf("MPIIO with NDIM = %d.\n",*NDIM); */
  for(iDim = 0;iDim < *NDIM+2;iDim++){
    /*    printf("Rank %d has offset %d.\n",rank,currentOffset); */
    returnCode = Mesh_IO_write(grid_inf,currentOffset,MPI_DOUBLE,endian,&Q[iDim*numPointsLocal],*NDIM,
			       meshSizes,ND,fileStart,MPI_ORDER_FORTRAN,
			       meshSizes,localStart,MPI_ORDER_FORTRAN,gridCommunicator);
    if(returnCode != MPI_SUCCESS){
      printf("Mesh_IO_write failed.\n");
      exit(1);
    }
    currentOffset = currentOffset + (numPointsGlobal*sizeof(double));
  }
  
  MPI_File_close(&grid_inf);
  MPI_Info_free(&info);
  free(meshSizes);
  free(localStart);
  free(fileStart);
  free(fname_nullterm);
  return;
}


/* write a plot3d solution in whole format without IBLANK */
#ifdef CMAKE_BUILD
#define APPEND_PLOT3D_SOLN_MPIIO_FC FC_GLOBAL_( \
					    append_plot3d_soln_mpiio,	\
					    APPEND_PLOT3D_SOLN_MPIIO)
#else
#define APPEND_PLOT3D_SOLN_MPIIO_FC FC_FUNC_( \
					    append_plot3d_soln_mpiio,	\
					    APPEND_PLOT3D_SOLN_MPIIO)
#endif
void APPEND_PLOT3D_SOLN_MPIIO_FC(int *inCommunicator,int *NDIM, int *ND, int *is,
				int *ie, double *Q, char *fname, int *len)
{

  char *fname_nullterm;
  MPI_Comm gridCommunicator;
  MPI_Info info;
  MPI_File grid_inf;
  FILE *solnFile;
  int amode = MPI_MODE_WRONLY|MPI_MODE_APPEND;
  int *meshSizes;
  int *localStart;
  int *fileStart;
  int rank;
  int endian;
  int systemEndian;
  int numPointsGlobal = 1;
  int numPointsLocal = 1;
  int currentOffset = 0;
  int iDim;
  char errorString[MPI_MAX_ERROR_STRING];
  int errorSize = 0;
  int totalWriteSize = 0;
  int offset = 0;

  systemEndian = detectEndian();

  meshSizes  = (int *)malloc((*NDIM)*sizeof(int));
  localStart = (int *)malloc((*NDIM)*sizeof(int));
  fileStart  = (int *)malloc((*NDIM)*sizeof(int));
  for(iDim=0;iDim < *NDIM;iDim++){
    meshSizes[iDim]  = (ie[iDim] - is[iDim] + 1);
    localStart[iDim] = 0;
    fileStart[iDim]  = is[iDim];
    numPointsGlobal  = numPointsGlobal * ND[iDim];
    numPointsLocal   = numPointsLocal * meshSizes[iDim];
  }
  totalWriteSize = numPointsGlobal*sizeof(double)*5;

  gridCommunicator = MPI_Comm_f2c(*inCommunicator);
  
  /* terminate string */
  fname_nullterm = (char *)malloc((*len+1)*sizeof(char));
  strncpy(fname_nullterm, fname, *len);
  fname_nullterm[*len] = '\0';
  
  MPI_Info_create(&info);
  /*
    MPI_Info_set(info, "striping_factor", "4");
    MPI_Info_set(info, "striping_unit", "1048576");
    MPI_Info_set(info, "collective_buffering", "true");
    MPI_Info_set(info, "cb_block_size", "1048576");
  */
  MPI_Comm_rank(gridCommunicator, &rank);
  if(rank == 0){
    solnFile = fopen(fname_nullterm,"ab");
    safe_fwrite((long)&totalWriteSize,4,1,systemEndian,solnFile);
    fseek(solnFile,0,SEEK_END);
    offset = ftell(solnFile);
    fclose(solnFile);
  }
  MPI_Bcast(&offset,1,MPI_INTEGER,0,gridCommunicator);
  currentOffset = offset;

  int returnCode = MPI_File_open(gridCommunicator, fname_nullterm, amode, info, &grid_inf);
  if(returnCode != MPI_SUCCESS){
    MPI_Error_string(returnCode,errorString,&errorSize);
    errorString[errorSize] = '\0';
    printf("MPI_File_open: %s\n",errorString);
    /*   perror("MPI_File_open failed"); */
    exit(1);
  }
  
  /* File is big endian */
  endian = MESH_IO_BIG_ENDIAN;
  
  /*  printf("Rank %d has %d points of %d points.\n",rank,numPointsLocal,numPointsGlobal); */
  /* this block is written in the FORTRAN format */
  /* Write (10) ((((X(L,I,J,K,M),I=1,ND(L,1)),J=1,ND(L,2)),K=1,ND(L,3)),M=1,NDIM) */
  /*  printf("MPIIO with NDIM = %d.\n",*NDIM); */
  for(iDim = 0;iDim < *NDIM+2;iDim++){
    /*    printf("Rank %d has offset %d.\n",rank,currentOffset); */
    returnCode = Mesh_IO_write(grid_inf,currentOffset,MPI_DOUBLE,endian,&Q[iDim*numPointsLocal],*NDIM,
			       meshSizes,ND,fileStart,MPI_ORDER_FORTRAN,
			       meshSizes,localStart,MPI_ORDER_FORTRAN,gridCommunicator);
    if(returnCode != MPI_SUCCESS){
      printf("Mesh_IO_write failed.\n");
      exit(1);
    }
    currentOffset = currentOffset + (numPointsGlobal*sizeof(double));
  }

  MPI_File_close(&grid_inf);
  MPI_Info_free(&info);

  if(rank == 0){
    solnFile = fopen(fname_nullterm,"ab");
    safe_fwrite((long)&totalWriteSize,4,1,systemEndian,solnFile);
    fclose(solnFile);
  }
  
  free(meshSizes);
  free(localStart);
  free(fileStart);
  free(fname_nullterm);
  return;
}

/* read a plot3d solution in whole format without IBLANK */
#ifdef CMAKE_BUILD
#define READ_PLOT3D_SINGLE_FUNC_MPIIO_FC FC_GLOBAL_( \
  read_plot3d_single_func_mpiio, \
  READ_PLOT3D_SINGLE_FUNC_MPIIO)
#else /* CMAKE_BUILD */
#define READ_PLOT3D_SINGLE_FUNC_MPIIO_FC FC_FUNC_( \
  read_plot3d_single_func_mpiio, \
  READ_PLOT3D_SINGLE_FUNC_MPIIO)
#endif /* CMAKE_BUILD */
void READ_PLOT3D_SINGLE_FUNC_MPIIO_FC(int *inCommunicator,int *NDIM, int *ND, int *offset, int *is,
				      int *ie, int *gridID, int *nvars,double *Q, char *fname, int *len)
{

  char *fname_nullterm;
  MPI_Comm gridCommunicator;
  MPI_Info info;
  MPI_File grid_inf;
  int amode = MPI_MODE_RDONLY;
  int *meshSizes;
  int *localStart;
  int *fileStart;
  int rank;
  int endian;
  int numPointsGlobal = 1;
  int numPointsLocal = 1;
  int currentOffset = *offset;
  int numVars = *nvars;
  int iDim;

  meshSizes  = (int *)malloc((*NDIM)*sizeof(int));
  localStart = (int *)malloc((*NDIM)*sizeof(int));
  fileStart  = (int *)malloc((*NDIM)*sizeof(int));
  for(iDim=0;iDim < *NDIM;iDim++){
    meshSizes[iDim]  = (ie[iDim] - is[iDim] + 1);
    localStart[iDim] = 0;
    fileStart[iDim]  = is[iDim];
    numPointsGlobal  = numPointsGlobal * ND[iDim];
    numPointsLocal   = numPointsLocal * meshSizes[iDim];
  }
 
  gridCommunicator = MPI_Comm_f2c(*inCommunicator);

  /* terminate string */
  fname_nullterm = (char *)malloc((*len+1)*sizeof(char));
  strncpy(fname_nullterm, fname, *len);
  fname_nullterm[*len] = '\0';

  MPI_Info_create(&info);
  /*
  MPI_Info_set(info, "striping_factor", "4");
  MPI_Info_set(info, "striping_unit", "1048576");
  MPI_Info_set(info, "collective_buffering", "true");
  MPI_Info_set(info, "cb_block_size", "1048576");
  */
  MPI_Comm_rank(gridCommunicator, &rank);
  /*  printf("Rank %d opening %s\n",rank,fname_nullterm); */
  int returnCode = MPI_File_open(gridCommunicator, fname_nullterm, amode, info, &grid_inf);
  if(returnCode != MPI_SUCCESS){
    perror("MPI_File_open failed");
    exit(1);
  }

  /* File is big endian */
  endian = MESH_IO_BIG_ENDIAN;

  /*  printf("Rank %d has %d points of %d points.\n",rank,numPointsLocal,numPointsGlobal); */
  /* this block is written in the FORTRAN format */
  /* Write (10) ((((X(L,I,J,K,M),I=1,ND(L,1)),J=1,ND(L,2)),K=1,ND(L,3)),M=1,NDIM) */
  for(iDim = 0;iDim < numVars;iDim++){
    /*    printf("Rank %d has offset %d.\n",rank,currentOffset); */
    returnCode = Mesh_IO_read(grid_inf,currentOffset,MPI_DOUBLE,endian,&Q[iDim*numPointsLocal],*NDIM,
			      meshSizes,ND,fileStart,MPI_ORDER_FORTRAN,
			      meshSizes,localStart,MPI_ORDER_FORTRAN,gridCommunicator);
    if(returnCode != MPI_SUCCESS){
      perror("Mesh_IO_read failed");    
      exit(1);
    }
    currentOffset = currentOffset + (numPointsGlobal*sizeof(double));
  }
  
  MPI_File_close(&grid_inf);
  MPI_Info_free(&info);
  free(meshSizes);
  free(localStart);
  free(fileStart);
  free(fname_nullterm);
  return;
}

/* write a plot3d solution in whole format without IBLANK */
#ifdef CMAKE_BUILD
#define WRITE_PLOT3D_FUNC_MPIIO_FC FC_GLOBAL( \
  write_plot3d_func_mpiio, \
  WRITE_PLOT3D_FUNC_MPIIO)
#else /* CMAKE_BUILD */
#define WRITE_PLOT3D_FUNC_MPIIO_FC FC_FUNC_( \
  write_plot3d_func_mpiio, \
  WRITE_PLOT3D_FUNC_MPIIO)
#endif /* CMAKE_BUILD */
void WRITE_PLOT3D_FUNC_MPIIO_FC(int *inCommunicator,int *NDIM, int *ND, int *offset, int *is,
				int *ie, int *nvars,double *Q, char *fname, int *len)
{

  char *fname_nullterm;
  MPI_Comm gridCommunicator;
  MPI_Info info;
  MPI_File grid_inf;
  int amode = MPI_MODE_WRONLY|MPI_MODE_APPEND;
  int *meshSizes;
  int *localStart;
  int *fileStart;
  int rank;
  int endian;
  int numPointsGlobal = 1;
  int numPointsLocal = 1;
  int currentOffset = *offset;
  int numVars = *nvars;
  int iDim;

  meshSizes  = (int *)malloc((*NDIM)*sizeof(int));
  localStart = (int *)malloc((*NDIM)*sizeof(int));
  fileStart  = (int *)malloc((*NDIM)*sizeof(int));
  for(iDim=0;iDim < *NDIM;iDim++){
    meshSizes[iDim]  = (ie[iDim] - is[iDim] + 1);
    localStart[iDim] = 0;
    fileStart[iDim]  = is[iDim];
    numPointsGlobal  = numPointsGlobal * ND[iDim];
    numPointsLocal   = numPointsLocal * meshSizes[iDim];
  }
 
  gridCommunicator = MPI_Comm_f2c(*inCommunicator);

  /* terminate string */
  fname_nullterm = (char *)malloc((*len+1)*sizeof(char));
  strncpy(fname_nullterm, fname, *len);
  fname_nullterm[*len] = '\0';

  MPI_Info_create(&info);
  /*
  MPI_Info_set(info, "striping_factor", "4");
  MPI_Info_set(info, "striping_unit", "1048576");
  MPI_Info_set(info, "collective_buffering", "true");
  MPI_Info_set(info, "cb_block_size", "1048576");
  */
  MPI_Comm_rank(gridCommunicator, &rank);
  /*  printf("Rank %d opening %s\n",rank,fname_nullterm); */
  int returnCode = MPI_File_open(gridCommunicator, fname_nullterm, amode, info, &grid_inf);
  if(returnCode != MPI_SUCCESS){
    perror("MPI_File_open failed");
    exit(1);
  }

  /* File is big endian */
  endian = MESH_IO_BIG_ENDIAN;

  /*  printf("Rank %d has %d points of %d points.\n",rank,numPointsLocal,numPointsGlobal); */
  /* this block is written in the FORTRAN format */
  /* Write (10) ((((X(L,I,J,K,M),I=1,ND(L,1)),J=1,ND(L,2)),K=1,ND(L,3)),M=1,NDIM) */
  for(iDim = 0;iDim < numVars;iDim++){
    /*    printf("Rank %d has offset %d.\n",rank,currentOffset); */
    returnCode = Mesh_IO_write(grid_inf,currentOffset,MPI_DOUBLE,endian,&Q[iDim*numPointsLocal],*NDIM,
			      meshSizes,ND,fileStart,MPI_ORDER_FORTRAN,
			      meshSizes,localStart,MPI_ORDER_FORTRAN,gridCommunicator);
    if(returnCode != MPI_SUCCESS){
      printf("Mesh_IO_write failed.\n");
      exit(1);
    }
    currentOffset = currentOffset + (numPointsGlobal*sizeof(double));
  }
  
  MPI_File_close(&grid_inf);
  MPI_Info_free(&info);
  free(meshSizes);
  free(localStart);
  free(fileStart);
  free(fname_nullterm);
  return;
}

/* write a plot3d solution in whole format without IBLANK */
#ifdef CMAKE_BUILD
#define APPEND_PLOT3D_FUNC_MPIIO_FC FC_GLOBAL( \
  append_plot3d_func_mpiio, \
  APPEND_PLOT3D_FUNC_MPIIO)
#else /* CMAKE_BUILD */
#define APPEND_PLOT3D_FUNC_MPIIO_FC FC_FUNC_( \
  append_plot3d_func_mpiio, \
  APPEND_PLOT3D_FUNC_MPIIO)
#endif /* CMAKE_BUILD */
void APPEND_PLOT3D_FUNC_MPIIO_FC(int *inCommunicator,int *NDIM, int *ND, int *is,
				int *ie, int *nvars,double *Q, char *fname, int *len)
{

  char *fname_nullterm;
  FILE *funcFile;
  MPI_Comm gridCommunicator;
  MPI_Info info;
  MPI_File grid_inf;
  int amode = MPI_MODE_WRONLY|MPI_MODE_APPEND;
  int *meshSizes;
  int *localStart;
  int *fileStart;
  int rank;
  int endian;
  int numPointsGlobal = 1;
  int numPointsLocal = 1;
  int currentOffset = 0;
  int offset;
  int totalWriteSize = 0;
  int numVars = *nvars;
  int iDim;
  int systemEndian = detectEndian();

  meshSizes  = (int *)malloc((*NDIM)*sizeof(int));
  localStart = (int *)malloc((*NDIM)*sizeof(int));
  fileStart  = (int *)malloc((*NDIM)*sizeof(int));
  for(iDim=0;iDim < *NDIM;iDim++){
    meshSizes[iDim]  = (ie[iDim] - is[iDim] + 1);
    localStart[iDim] = 0;
    fileStart[iDim]  = is[iDim];
    numPointsGlobal  = numPointsGlobal * ND[iDim];
    numPointsLocal   = numPointsLocal * meshSizes[iDim];
  }
  totalWriteSize = numPointsGlobal*sizeof(double)*numVars;

  gridCommunicator = MPI_Comm_f2c(*inCommunicator);

  /* terminate string */
  fname_nullterm = (char *)malloc((*len+1)*sizeof(char));
  strncpy(fname_nullterm, fname, *len);
  fname_nullterm[*len] = '\0';

  MPI_Info_create(&info);
  /*
  MPI_Info_set(info, "striping_factor", "4");
  MPI_Info_set(info, "striping_unit", "1048576");
  MPI_Info_set(info, "collective_buffering", "true");
  MPI_Info_set(info, "cb_block_size", "1048576");
  */
  MPI_Comm_rank(gridCommunicator, &rank);
  if(rank == 0){
    funcFile = fopen(fname_nullterm,"ab");
    safe_fwrite((long)&totalWriteSize,4,1,systemEndian,funcFile);
    fseek(funcFile,0,SEEK_END);
    offset = ftell(funcFile);
    fclose(funcFile);
  }
  MPI_Bcast(&offset,1,MPI_INTEGER,0,gridCommunicator);
  currentOffset = offset;

  /*  printf("Rank %d opening %s\n",rank,fname_nullterm); */
  int returnCode = MPI_File_open(gridCommunicator, fname_nullterm, amode, info, &grid_inf);
  if(returnCode != MPI_SUCCESS){
    perror("MPI_File_open failed");
    exit(1);
  }

  /* File is big endian */
  endian = MESH_IO_BIG_ENDIAN;

  /*  printf("Rank %d has %d points of %d points.\n",rank,numPointsLocal,numPointsGlobal); */
  /* this block is written in the FORTRAN format */
  /* Write (10) ((((X(L,I,J,K,M),I=1,ND(L,1)),J=1,ND(L,2)),K=1,ND(L,3)),M=1,NDIM) */
  for(iDim = 0;iDim < numVars;iDim++){
    /*    printf("Rank %d has offset %d.\n",rank,currentOffset); */
    returnCode = Mesh_IO_write(grid_inf,currentOffset,MPI_DOUBLE,endian,&Q[iDim*numPointsLocal],*NDIM,
			      meshSizes,ND,fileStart,MPI_ORDER_FORTRAN,
			      meshSizes,localStart,MPI_ORDER_FORTRAN,gridCommunicator);
    if(returnCode != MPI_SUCCESS){
      printf("Mesh_IO_write failed.\n");
      exit(1);
    }
    currentOffset = currentOffset + (numPointsGlobal*sizeof(double));
  }
  MPI_File_close(&grid_inf);
  MPI_Info_free(&info);

  if(rank == 0){
    funcFile = fopen(fname_nullterm,"ab");
    safe_fwrite((long)&totalWriteSize,4,1,systemEndian,funcFile);
    fclose(funcFile);
  }

  free(meshSizes);
  free(localStart);
  free(fileStart);
  free(fname_nullterm);
  return;
}


/* read a plot3d solution in whole format */
#ifdef CMAKE_BUILD
#define READ_PLOT3D_SINGLE_SOLN_WHOLE_FORMAT_LOW_MEM_FC FC_GLOBAL_( \
  read_plot3d_single_soln_whole_format_low_mem, \
  READ_PLOT3D_SINGLE_SOLN_WHOLE_FORMAT_LOW_MEM)
#else /* CMAKE_BUILD */
#define READ_PLOT3D_SINGLE_SOLN_WHOLE_FORMAT_LOW_MEM_FC FC_FUNC_( \
  read_plot3d_single_soln_whole_format_low_mem, \
  READ_PLOT3D_SINGLE_SOLN_WHOLE_FORMAT_LOW_MEM)
#endif /* CMAKE_BUILD */
void READ_PLOT3D_SINGLE_SOLN_WHOLE_FORMAT_LOW_MEM_FC(int *NDIM, int *ND, int *offset, int *is,
  int *ie, int *gridID, double *Q, char *fname, int *len)
{

  FILE *solnFile = NULL;
  int i, j, k, l, chunk[3], chunk_total, endian, grid_size, N[3], l0;
  char error_str[256];
  int sizeof_int, sizeof_dbl;
  double *dbuf = NULL;
  char *fname_nullterm;

  /* terminate string */
  fname_nullterm = (char *)malloc((*len+1)*sizeof(char));
  strncpy(fname_nullterm, fname, *len);
  fname_nullterm[*len] = '\0';

  /* error on failed open */
  if ((solnFile = fopen(fname_nullterm,"rb")) == NULL) {
    sprintf(error_str,"%s %s %s","Unable to open file ", fname_nullterm, ".\n");
    perror(error_str);
    goto cleanup1;
  }

  /* check endian :: default is big endian */
  endian = detectEndian( );

  /* seek past offset */
  if (fseek(solnFile, (long int)(*offset), SEEK_SET) != 0) {
    sprintf(error_str,"%s %s %s %s %s","ERROR in fseek on file ", fname_nullterm, "in", __FILE__,".\n");
    perror(error_str);
    goto cleanup2;
  }

  /* this block is written in the FORTRAN format */
  /* Write (10) ((((X(L,I,J,K,M),I=1,ND(L,1)),J=1,ND(L,2)),K=1,ND(L,3)),M=1,NDIM+2) */

  /* now start reading, in chunks when possible */
  chunk_total = 0; grid_size = 1;
  for (i = 0; i < *NDIM; i++) { 
    N[i] = ie[i] - is[i] + 1;
    chunk[i] = N[i] - ND[i]; 
    chunk_total += chunk[i]; 
    grid_size *= N[i];
  }

  /* we can read the entire block */
  if (chunk_total == 0) { 
    safe_fread((long int)Q, sizeof(double), grid_size*((*NDIM)+2), endian, solnFile); 
  }
  
  else {

    dbuf = (double *)malloc(ND[0]*sizeof(double));
    for (l = 0; l < (*NDIM)+2; l++) {
    for (k = 0; k < ND[2]; k++) {
    for (j = 0; j < ND[1]; j++) {
      safe_fread((long int)dbuf, sizeof(double), ND[0], endian, solnFile);
      if ((j >= is[1] && j <= ie[1]) && (k >= is[2] && k <= ie[2])) {
        for (i = is[0]; i <= ie[0]; i++) {
          l0 = N[1]*N[0]*(k-is[2]) + N[0]*(j-is[1]) + i-is[0];
          Q[l*grid_size + l0] = dbuf[i];
        }
      }
    }}}
    free(dbuf);

  }

  cleanup2:
    fclose(solnFile);

  cleanup1:
    free(fname_nullterm);

  return;

}

/* read a plot3d grid in whole format without IBLANK */
/* Need to pass in total size for file so that file can be sized
   collectively 
*/
#ifdef CMAKE_BUILD
#define WRITE_PLOT3D_GRID_MPIIO_FC FC_GLOBAL_(	\
  write_plot3d_grid_mpiio, \
  WRITE_PLOT3D_GRID_MPIIO)
#else /* CMAKE_BUILD */
#define WRITE_PLOT3D_GRID_MPIIO_FC FC_FUNC_(	\
  write_plot3d_grid_mpiio, \
  WRITE_PLOT3D_GRID_MPIIO)
#endif /* CMAKE_BUILD */
void WRITE_PLOT3D_GRID_MPIIO_FC(int *inCommunicator,int *NDIM, int *ND, int *offset, int *is,
				int *ie, double *Q, char *fname, int *len)
{

  char *fname_nullterm;
  MPI_Comm gridCommunicator;
  MPI_Info info;
  MPI_File grid_inf;
  int amode = MPI_MODE_WRONLY|MPI_MODE_APPEND;
  int *meshSizes;
  int *localStart;
  int *fileStart;
  int rank;
  int endian;
  int numPointsGlobal = 1;
  int numPointsLocal = 1;
  int currentOffset = *offset;
  int iDim;

  meshSizes  = (int *)malloc((*NDIM)*sizeof(int));
  localStart = (int *)malloc((*NDIM)*sizeof(int));
  fileStart  = (int *)malloc((*NDIM)*sizeof(int));
  for(iDim=0;iDim < *NDIM;iDim++){
    meshSizes[iDim]  = (ie[iDim] - is[iDim] + 1);
    localStart[iDim] = 0;
    fileStart[iDim]  = is[iDim];
    numPointsGlobal  = numPointsGlobal * ND[iDim];
    numPointsLocal   = numPointsLocal * meshSizes[iDim];
  }

  gridCommunicator = MPI_Comm_f2c(*inCommunicator);

  /* terminate string */
  fname_nullterm = (char *)malloc((*len+1)*sizeof(char));
  strncpy(fname_nullterm, fname, *len);
  fname_nullterm[*len] = '\0';

  MPI_Info_create(&info);
  /*
  MPI_Info_set(info, "striping_factor", "4");
  MPI_Info_set(info, "striping_unit", "1048576");
  MPI_Info_set(info, "collective_buffering", "true");
  MPI_Info_set(info, "cb_block_size", "1048576");
  */
  MPI_Comm_rank(gridCommunicator, &rank);
  /*  printf("Rank %d opening %s\n",rank,fname_nullterm); */
  int returnCode = MPI_File_open(gridCommunicator, fname_nullterm, amode, info, &grid_inf);
  if(returnCode != MPI_SUCCESS){
    perror("MPI_File_open failed");
    exit(1);
  }

  /* File is big endian */
  endian = MESH_IO_BIG_ENDIAN;

  /*  printf("Rank %d has %d points of %d points.\n",rank,numPointsLocal,numPointsGlobal); */
  /* this block is written in the FORTRAN format */
  /* Write (10) ((((X(L,I,J,K,M),I=1,ND(L,1)),J=1,ND(L,2)),K=1,ND(L,3)),M=1,NDIM) */
  for(iDim = 0;iDim < *NDIM;iDim++){
    /*    printf("Rank %d has offset %d.\n",rank,currentOffset); */
    returnCode = Mesh_IO_write(grid_inf,currentOffset,MPI_DOUBLE,endian,&Q[iDim*numPointsLocal],*NDIM,
			      meshSizes,ND,fileStart,MPI_ORDER_FORTRAN,
			      meshSizes,localStart,MPI_ORDER_FORTRAN,gridCommunicator);
    if(returnCode != MPI_SUCCESS){
      printf("Mesh_IO_write failed.\n");
      exit(1);
    }
    currentOffset = currentOffset + (numPointsGlobal*sizeof(double));
  }
  
  MPI_File_close(&grid_inf);
  MPI_Info_free(&info);
  free(meshSizes);
  free(localStart);
  free(fileStart);
  free(fname_nullterm);

  return;
}

/* read a plot3d grid in whole format without IBLANK */
/* Need to pass in total size for file so that file can be sized
   collectively 
*/
#ifdef CMAKE_BUILD
#define WRITE_PLOT3D_GRID_IB_MPIIO_FC FC_GLOBAL_(	\
					       write_plot3d_grid_ib_mpiio, \
					       WRITE_PLOT3D_GRID_IB_MPIIO)
#else /* CMAKE_BUILD */
#define WRITE_PLOT3D_GRID_IB_MPIIO_FC FC_FUNC_(	\
					       write_plot3d_grid_ib_mpiio, \
					       WRITE_PLOT3D_GRID_IB_MPIIO)
#endif /* CMAKE_BUILD */
void WRITE_PLOT3D_GRID_IB_MPIIO_FC(int *inCommunicator,int *NDIM, int *ND, int *offset, int *is,
				   int *ie, double *Q, int *IB,char *fname, int *len)
{

  char *fname_nullterm;
  MPI_Comm gridCommunicator;
  MPI_Info info;
  MPI_File grid_inf;
  int amode = MPI_MODE_WRONLY|MPI_MODE_APPEND;
  int *meshSizes;
  int *localStart;
  int *fileStart;
  int rank;
  int endian;
  int numPointsGlobal = 1;
  int numPointsLocal = 1;
  int currentOffset = *offset;
  int iDim;

  meshSizes  = (int *)malloc((*NDIM)*sizeof(int));
  localStart = (int *)malloc((*NDIM)*sizeof(int));
  fileStart  = (int *)malloc((*NDIM)*sizeof(int));
  for(iDim=0;iDim < *NDIM;iDim++){
    meshSizes[iDim]  = (ie[iDim] - is[iDim] + 1);
    localStart[iDim] = 0;
    fileStart[iDim]  = is[iDim];
    numPointsGlobal  = numPointsGlobal * ND[iDim];
    numPointsLocal   = numPointsLocal * meshSizes[iDim];
  }
  /*  printf("WRite grid w/iblank mpiio\n"); */
  gridCommunicator = MPI_Comm_f2c(*inCommunicator);

  /* terminate string */
  fname_nullterm = (char *)malloc((*len+1)*sizeof(char));
  strncpy(fname_nullterm, fname, *len);
  fname_nullterm[*len] = '\0';

  MPI_Info_create(&info);
  /*
  MPI_Info_set(info, "striping_factor", "4");
  MPI_Info_set(info, "striping_unit", "1048576");
  MPI_Info_set(info, "collective_buffering", "true");
  MPI_Info_set(info, "cb_block_size", "1048576");
  */
  MPI_Comm_rank(gridCommunicator, &rank);
  /*  printf("Rank %d opening %s\n",rank,fname_nullterm); */
  int returnCode = MPI_File_open(gridCommunicator, fname_nullterm, amode, info, &grid_inf);
  if(returnCode != MPI_SUCCESS){
    perror("MPI_File_open failed");
    exit(1);
  }

  /* File is big endian */
  endian = MESH_IO_BIG_ENDIAN;

  /*  printf("Rank %d has %d points of %d points.\n",rank,numPointsLocal,numPointsGlobal); */
  /* this block is written in the FORTRAN format */
  /* Write (10) ((((X(L,I,J,K,M),I=1,ND(L,1)),J=1,ND(L,2)),K=1,ND(L,3)),M=1,NDIM) */
  for(iDim = 0;iDim < *NDIM;iDim++){
    /*    printf("Rank %d has offset %d.\n",rank,currentOffset); */
    returnCode = Mesh_IO_write(grid_inf,currentOffset,MPI_DOUBLE,endian,&Q[iDim*numPointsLocal],*NDIM,
			      meshSizes,ND,fileStart,MPI_ORDER_FORTRAN,
			      meshSizes,localStart,MPI_ORDER_FORTRAN,gridCommunicator);
    if(returnCode != MPI_SUCCESS){
      printf("Mesh_IO_write failed.\n");    
      exit(1);
    }
    currentOffset = currentOffset + (numPointsGlobal*sizeof(double));
  }
  returnCode = Mesh_IO_write(grid_inf,currentOffset,MPI_INT,endian,&IB[0],*NDIM,
			     meshSizes,ND,fileStart,MPI_ORDER_FORTRAN,
			     meshSizes,localStart,MPI_ORDER_FORTRAN,gridCommunicator);
  if(returnCode != MPI_SUCCESS){
    printf("Mesh_IO_write iblank failed.\n");
    exit(1);
  }
  
  MPI_File_close(&grid_inf);
  MPI_Info_free(&info);
  free(meshSizes);
  free(localStart);
  free(fileStart);
  free(fname_nullterm);

  return;
}

/* read a plot3d grid in whole format without IBLANK */
/* Need to pass in total size for file so that file can be sized
   collectively 
*/
#ifdef CMAKE_BUILD
#define APPEND_PLOT3D_GRID_IB_MPIIO_FC FC_GLOBAL_(	\
					       append_plot3d_grid_ib_mpiio, \
					       APPEND_PLOT3D_GRID_IB_MPIIO)
#else /* CMAKE_BUILD */
#define APPEND_PLOT3D_GRID_IB_MPIIO_FC FC_FUNC_(	\
					       append_plot3d_grid_ib_mpiio, \
					       APPEND_PLOT3D_GRID_IB_MPIIO)
#endif /* CMAKE_BUILD */
void APPEND_PLOT3D_GRID_IB_MPIIO_FC(int *inCommunicator,int *NDIM, int *ND,int *is,
				   int *ie, double *Q, int *IB,char *fname, int *len)
{

  char *fname_nullterm;
  FILE *gridFile;
  MPI_Comm gridCommunicator;
  MPI_Info info;
  MPI_File grid_inf;
  int amode = MPI_MODE_WRONLY|MPI_MODE_APPEND;
  int *meshSizes;
  int *localStart;
  int *fileStart;
  int rank;
  int endian;
  int offset;
  int numPointsGlobal = 1;
  int numPointsLocal = 1;
  int currentOffset = 0;
  int iDim;
  int totalWriteSize = 0;
  int systemEndian = detectEndian();

  meshSizes  = (int *)malloc((*NDIM)*sizeof(int));
  localStart = (int *)malloc((*NDIM)*sizeof(int));
  fileStart  = (int *)malloc((*NDIM)*sizeof(int));
  for(iDim=0;iDim < *NDIM;iDim++){
    meshSizes[iDim]  = (ie[iDim] - is[iDim] + 1);
    localStart[iDim] = 0;
    fileStart[iDim]  = is[iDim];
    numPointsGlobal  = numPointsGlobal * ND[iDim];
    numPointsLocal   = numPointsLocal * meshSizes[iDim];
  }

  totalWriteSize = numPointsGlobal*(sizeof(double)*3 + sizeof(int));

  /*  printf("WRite grid w/iblank mpiio\n"); */
  gridCommunicator = MPI_Comm_f2c(*inCommunicator);

  /* terminate string */
  fname_nullterm = (char *)malloc((*len+1)*sizeof(char));
  strncpy(fname_nullterm, fname, *len);
  fname_nullterm[*len] = '\0';

  MPI_Info_create(&info);
  /*
  MPI_Info_set(info, "striping_factor", "4");
  MPI_Info_set(info, "striping_unit", "1048576");
  MPI_Info_set(info, "collective_buffering", "true");
  MPI_Info_set(info, "cb_block_size", "1048576");
  */
  MPI_Comm_rank(gridCommunicator, &rank);
  if(rank == 0){
    gridFile = fopen(fname_nullterm,"ab");
    safe_fwrite((long)&totalWriteSize,4,1,systemEndian,gridFile);
    fseek(gridFile,0,SEEK_END);
    offset = ftell(gridFile);
    fclose(gridFile);
  }
  MPI_Bcast(&offset,1,MPI_INTEGER,0,gridCommunicator);

  currentOffset = offset;

  /*  printf("Rank %d opening %s\n",rank,fname_nullterm); */
  int returnCode = MPI_File_open(gridCommunicator, fname_nullterm, amode, info, &grid_inf);
  if(returnCode != MPI_SUCCESS){
    perror("MPI_File_open failed");
    exit(1);
  }

  /* File is big endian */
  endian = MESH_IO_BIG_ENDIAN;

  /*  printf("Rank(%d): WS(%d), O(%d), NP(%d), NPG(%d), SI(%d,%d,%d), MS(%d,%d,%d)\n",
	 rank,totalWriteSize,currentOffset,numPointsLocal,numPointsGlobal,fileStart[0],
	 fileStart[1],fileStart[2],meshSizes[0],meshSizes[1],meshSizes[2]);
      fflush(stdout);
      MPI_Barrier(gridCommunicator);
  */
  /*  printf("Rank %d has %d points of %d points.\n",rank,numPointsLocal,numPointsGlobal); */
  /* this block is written in the FORTRAN format */
  /* Write (10) ((((X(L,I,J,K,M),I=1,ND(L,1)),J=1,ND(L,2)),K=1,ND(L,3)),M=1,NDIM) */
  for(iDim = 0;iDim < *NDIM;iDim++){
    /*    printf("Rank %d has offset %d.\n",rank,currentOffset); */
    returnCode = Mesh_IO_write(grid_inf,currentOffset,MPI_DOUBLE,endian,&Q[iDim*numPointsLocal],*NDIM,
			      meshSizes,ND,fileStart,MPI_ORDER_FORTRAN,
			      meshSizes,localStart,MPI_ORDER_FORTRAN,gridCommunicator);
    if(returnCode != MPI_SUCCESS){
      printf("Mesh_IO_write failed.\n");    
      exit(1);
    }
    currentOffset = currentOffset + (numPointsGlobal*sizeof(double));
  }
  returnCode = Mesh_IO_write(grid_inf,currentOffset,MPI_INT,endian,&IB[0],*NDIM,
			     meshSizes,ND,fileStart,MPI_ORDER_FORTRAN,
			     meshSizes,localStart,MPI_ORDER_FORTRAN,gridCommunicator);
  if(returnCode != MPI_SUCCESS){
    printf("Mesh_IO_write iblank failed.\n");
    exit(1);
  }
  
  MPI_File_close(&grid_inf);
  MPI_Info_free(&info);

  if(rank == 0){
    gridFile = fopen(fname_nullterm,"ab");
    safe_fwrite((long)&totalWriteSize,4,1,systemEndian,gridFile);
    fclose(gridFile);
  }

  free(meshSizes);
  free(localStart);
  free(fileStart);
  free(fname_nullterm);

  return;
}

/* write a plot3d solution in whole format */
#ifdef CMAKE_BUILD
#define WRITE_PLOT3D_SINGLE_SOLN_WHOLE_FORMAT_LOW_MEM_FC FC_GLOBAL_( \
  write_plot3d_single_soln_whole_format_low_mem, WRITE_PLOT3D_SINGLE_SOLN_WHOLE_FORMAT_LOW_MEM)
#else /* CMAKE_BUILD */
#define WRITE_PLOT3D_SINGLE_SOLN_WHOLE_FORMAT_LOW_MEM_FC FC_FUNC_( \
  write_plot3d_single_soln_whole_format_low_mem, WRITE_PLOT3D_SINGLE_SOLN_WHOLE_FORMAT_LOW_MEM)
#endif /* CMAKE_BUILD */
void WRITE_PLOT3D_SINGLE_SOLN_WHOLE_FORMAT_LOW_MEM_FC(int *ND, int *offset, int *is, int *ie,
  double *Q, double *tau, char *fname, int *len)
{

  FILE *gridFile = NULL;
  int i, j, k, l, chunk[3], chunk_total, endian, grid_size, N[3], l0;
  char error_str[256];
  int sizeof_int, sizeof_dbl, byteSkip_ks, byteSkip_ke, byteSkip_is, byteSkip_ie, byteSkip_js, byteSkip_je;
  double *dbuf = NULL;
  int *ibuf = NULL;
  char *fname_nullterm;

  /* terminate string */
  fname_nullterm = (char *)malloc((*len+1)*sizeof(char));
  strncpy(fname_nullterm, fname, *len);
  fname_nullterm[*len] = '\0';

  /* variable sizes */
  sizeof_int = sizeof(int);
  sizeof_dbl = sizeof(double);

  /* error on failed open */
  if ((gridFile = fopen(fname_nullterm,"rb+")) == NULL) {
    sprintf(error_str,"%s %s %s","Unable to open file \"", fname_nullterm, "\".");
    perror(error_str);
    goto cleanup1;
  }

  /* check endian :: default is big endian */
  endian = detectEndian( );

  /* seek past offset */
  if (fseek(gridFile, (long int)(*offset), SEEK_SET) != 0) {
    sprintf(error_str,"%s %s %s %s %s","ERROR in fseek on file ", fname_nullterm, "in", __FILE__,".\n");
    perror(error_str);
    goto cleanup2;
  }

  /* now start reading, in chunks when possible */
  chunk_total = 0; grid_size = 1;
  for (i = 0; i < 3; i++) { 
    N[i] = ie[i] - is[i] + 1;
    chunk[i] = N[i] - ND[i]; 
    chunk_total += chunk[i]; 
    grid_size *= N[i];
  }

  /* Write header */
  safe_fwrite((long int)tau, sizeof_dbl, 4, endian, gridFile);
  fseek(gridFile, (long int)(8), SEEK_CUR);

  /* this block is written in the FORTRAN format */
  /* Write (10) ((((X(L,I,J,K,M),I=1,ND(L,1)),J=1,ND(L,2)),K=1,ND(L,3)),M=1,NDIM) */

  /* we can write the entire block */
  if (chunk_total == 0) { 
    safe_fwrite((long int)Q, sizeof_dbl, grid_size*5, endian, gridFile);
  }
  
  else {

    /* have to be careful here so we don't overwrite other data */
    /* must seek to the correct location to write out i-chunk */
    /* yuck! */

    /* seeking ... */
    /* ... to first k-location */
    byteSkip_ks = is[2] * ND[0] * ND[1] * sizeof_dbl;

    /* ... to first j-location */
    byteSkip_js = is[1] * ND[0] * sizeof_dbl;

    /* ... to first i-loc */
    byteSkip_is = is[0] * sizeof_dbl;

    /* ... from last written i-location to end of i */
    byteSkip_ie = (ND[0] - ie[0] - 1) * sizeof_dbl;

    /* ... from last written j-location to end of j */
    byteSkip_je = (ND[1] - ie[1] - 1) * ND[0] * sizeof_dbl;

    /* ... from last written k-location to end of k */
    byteSkip_ke = (ND[2] - ie[2] - 1) * ND[0] * ND[1] * sizeof_dbl;

    /* buffer for this i-chunk */
    dbuf = (double *)malloc(N[0]*sizeof_dbl);

    /* skip over k first, then j, then i */
    for (l = 0; l < 5; l++) {

      fseek(gridFile, (long int)byteSkip_ks, SEEK_CUR);

      for (k = 0; k < N[2]; k++) {

        fseek(gridFile, (long int)byteSkip_js, SEEK_CUR);

        for (j = 0; j < N[1]; j++) {

          fseek(gridFile, (long int)byteSkip_is, SEEK_CUR);

          for (i = 0; i < N[0]; i++) {
            l0 = N[1]*N[0]*k + N[0]*j + i;
            dbuf[i] = Q[l*grid_size + l0];
          }

          safe_fwrite((long int)dbuf, sizeof_dbl, N[0], endian, gridFile);

          fseek(gridFile, (long int)byteSkip_ie, SEEK_CUR);
 
        }
      
        fseek(gridFile, (long int)byteSkip_je, SEEK_CUR);

      }

      fseek(gridFile, (long int)byteSkip_ke, SEEK_CUR);

    }

    free(dbuf);

  }

  cleanup2:
    fclose(gridFile);

  cleanup1:
    free(fname_nullterm);

  return;

}

/* write a plot3d grid in whole format with IBLANK */
#ifdef CMAKE_BUILD
#define WRITE_PLOT3D_SINGLE_GRID_WHOLE_FORMAT_WITH_IBLANK_LOW_MEM_FC FC_GLOBAL_( \
  write_plot3d_single_grid_whole_format_with_iblank_low_mem, \
  WRITE_PLOT3D_SINGLE_GRID_WHOLE_FORMAT_WITH_IBLANK_LOW_MEM)
#else /* CMAKE_BUILD */
#define WRITE_PLOT3D_SINGLE_GRID_WHOLE_FORMAT_WITH_IBLANK_LOW_MEM_FC FC_FUNC_( \
  write_plot3d_single_grid_whole_format_with_iblank_low_mem, \
  WRITE_PLOT3D_SINGLE_GRID_WHOLE_FORMAT_WITH_IBLANK_LOW_MEM)
#endif /* CMAKE_BUILD */
void WRITE_PLOT3D_SINGLE_GRID_WHOLE_FORMAT_WITH_IBLANK_LOW_MEM_FC(int *ND, int *offset, int *is,
  int *ie, double *Q, int *IB, char *fname, int *len)
{

  FILE *gridFile = NULL;
  int i, j, k, l, chunk[3], chunk_total, endian, grid_size, N[3], l0;
  char error_str[256];
  int sizeof_int, sizeof_dbl, byteSkip_ks, byteSkip_ke, byteSkip_is, byteSkip_ie, byteSkip_js, byteSkip_je;
  double *dbuf = NULL;
  int *ibuf = NULL;
  char *fname_nullterm;

  /* terminate string */
  fname_nullterm = (char *)malloc((*len+1)*sizeof(char));
  strncpy(fname_nullterm, fname, *len);
  fname_nullterm[*len] = '\0';

  /* variable sizes */
  sizeof_int = sizeof(int);
  sizeof_dbl = sizeof(double);

  /* error on failed open */
  if ((gridFile = fopen(fname_nullterm,"rb+")) == NULL) {
    sprintf(error_str,"%s %s %s","Unable to open file ", fname_nullterm, ".\n");
    perror(error_str);
    goto cleanup1;
  }

  /* check endian :: default is big endian */
  endian = detectEndian( );

  /* seek past offset */
  if (fseek(gridFile, (long int)(*offset), SEEK_SET) != 0) {
    sprintf(error_str,"%s %s %s %s %s","ERROR in fseek on file ", fname_nullterm, "in", __FILE__,".\n");
    perror(error_str);
    goto cleanup2;
  }

  /* now start reading, in chunks when possible */
  chunk_total = 0; grid_size = 1;
  for (i = 0; i < 3; i++) { 
    N[i] = ie[i] - is[i] + 1;
    chunk[i] = N[i] - ND[i]; 
    chunk_total += chunk[i]; 
    grid_size *= N[i];
  }

  /* this block is written in the FORTRAN format */
  /* Write (10) ((((X(L,I,J,K,M),I=1,ND(L,1)),J=1,ND(L,2)),K=1,ND(L,3)),M=1,NDIM) */

  /* we can write the entire block */
  if (chunk_total == 0) { 
    safe_fwrite((long int)Q, sizeof_dbl, grid_size*3, endian, gridFile);
    safe_fwrite((long int)IB, sizeof_int, grid_size, endian, gridFile);
  }
  
  else {

    /* have to be careful here so we don't overwrite other data */
    /* must seek to the correct location to write out i-chunk */
    /* yuck! */

    /* seeking ... */
    /* ... to first k-location */
    byteSkip_ks = is[2] * ND[0] * ND[1] * sizeof_dbl;

    /* ... to first j-location */
    byteSkip_js = is[1] * ND[0] * sizeof_dbl;

    /* ... to first i-loc */
    byteSkip_is = is[0] * sizeof_dbl;

    /* ... from last written i-location to end of i */
    byteSkip_ie = (ND[0] - ie[0] - 1) * sizeof_dbl;

    /* ... from last written j-location to end of j */
    byteSkip_je = (ND[1] - ie[1] - 1) * ND[0] * sizeof_dbl;

    /* ... from last written k-location to end of k */
    byteSkip_ke = (ND[2] - ie[2] - 1) * ND[0] * ND[1] * sizeof_dbl;

    /* buffer for this i-chunk */
    dbuf = (double *)malloc(N[0]*sizeof_dbl);

    /* skip over k first, then j, then i */
    for (l = 0; l < 3; l++) {

      fseek(gridFile, (long int)byteSkip_ks, SEEK_CUR);

      for (k = 0; k < N[2]; k++) {

        fseek(gridFile, (long int)byteSkip_js, SEEK_CUR);

        for (j = 0; j < N[1]; j++) {

          fseek(gridFile, (long int)byteSkip_is, SEEK_CUR);

          for (i = 0; i < N[0]; i++) {
            l0 = N[1]*N[0]*k + N[0]*j + i;
            dbuf[i] = Q[l*grid_size + l0];
          }

          safe_fwrite((long int)dbuf, sizeof_dbl, N[0], endian, gridFile);

          fseek(gridFile, (long int)byteSkip_ie, SEEK_CUR);
 
        }
      
        fseek(gridFile, (long int)byteSkip_je, SEEK_CUR);

      }

      fseek(gridFile, (long int)byteSkip_ke, SEEK_CUR);

    }

    free(dbuf);

    /* 
     * repeat for IBLANK
     */
    /* ... to first k-location */
    byteSkip_ks = is[2] * ND[0] * ND[1] * sizeof_int;

    /* ... to first j-location */
    byteSkip_js = is[1] * ND[0] * sizeof_int;

    /* ... to first i-loc */
    byteSkip_is = is[0] * sizeof_int;

    /* ... from last written i-location to end of i */
    byteSkip_ie = (ND[0] - ie[0] - 1) * sizeof_int;

    /* ... from last written j-location to end of j */
    byteSkip_je = (ND[1] - ie[1] - 1) * ND[0] * sizeof_int;

    /* ... from last written k-location to end of k */
    byteSkip_ke = (ND[2] - ie[2] - 1) * ND[0] * ND[1] * sizeof_int;

    /* buffer for this i-chunk */
    ibuf = (int *)malloc(N[0]*sizeof_int);

    /* skip over k first, then j, then i */
    fseek(gridFile, (long int)byteSkip_ks, SEEK_CUR);

    for (k = 0; k < N[2]; k++) {

      fseek(gridFile, (long int)byteSkip_js, SEEK_CUR);

      for (j = 0; j < N[1]; j++) {

        fseek(gridFile, (long int)byteSkip_is, SEEK_CUR);

        for (i = 0; i < N[0]; i++) {
          l0 = N[1]*N[0]*k + N[0]*j + i;
          ibuf[i] = IB[l0];
        }

        safe_fwrite((long int)ibuf, sizeof_int, N[0], endian, gridFile);

        fseek(gridFile, (long int)byteSkip_ie, SEEK_CUR);
 
        }
      
      fseek(gridFile, (long int)byteSkip_je, SEEK_CUR);

    }

    fseek(gridFile, (long int)byteSkip_ke, SEEK_CUR);

    free(ibuf);

  }

  cleanup2:
    fclose(gridFile);

  cleanup1:
    free(fname_nullterm);

  return;

}

/*
  char *grid_file, *cube_list_file;
  int amode, max_cubelets_per_rank;
  MPI_File grid_inf;
  Cubelet *cubelet_list;
  int cubelet_list_count;
  MPI_Info info;
  size_t bytes_read;
  double start_time, elapsed;
  
  MPI_Init(&argc, &argv);
  MPI_Comm_size(MPI_COMM_WORLD, &np);
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);

  if (argc != 3) printHelp();

  start_time = MPI_Wtime();  
  grid_file = argv[1];
  cube_list_file = argv[2];

  defineCubeletDefType();

  /* read the list of cubelets and scatter it to all the ranks
     if there is an error reading the file, all processors should exit */
/*
  if (!readAndScatterCubeletFile(&cubelet_list, &cubelet_list_count,
				 &max_cubelets_per_rank, cube_list_file))
    goto fail;

  if (rank == 0)
    printf("%.3f ms to call readAndScatterCubeletFile\n",
	   1000 * (MPI_Wtime() - start_time));



  /* open the grid file */
/*
  amode = MPI_MODE_RDONLY; /* | MPI_MODE_UNIQUE_OPEN; */
/*
  MPI_Info_create(&info);

  /*
  MPI_Info_set(info, "striping_factor", "4");
  MPI_Info_set(info, "striping_unit", "1048576");
  MPI_Info_set(info, "collective_buffering", "true");
  MPI_Info_set(info, "cb_block_size", "1048576");
  */
/*
  MPI_File_open(MPI_COMM_WORLD, grid_file, amode, info, &grid_inf);
  MPI_Info_free(&info);
*/

/* write a plot3d function file in whole format */
#ifdef CMAKE_BUILD
#define WRITE_PLOT3D_SINGLE_FUNC_WHOLE_FORMAT_LOW_MEM_FC FC_GLOBAL_( \
  write_plot3d_single_func_whole_format_low_mem, WRITE_PLOT3D_SINGLE_FUNC_WHOLE_FORMAT_LOW_MEM)
#else /* CMAKE_BUILD */
#define WRITE_PLOT3D_SINGLE_FUNC_WHOLE_FORMAT_LOW_MEM_FC FC_FUNC_( \
  write_plot3d_single_func_whole_format_low_mem, WRITE_PLOT3D_SINGLE_FUNC_WHOLE_FORMAT_LOW_MEM)
#endif /* CMAKE_BUILD */
void WRITE_PLOT3D_SINGLE_FUNC_WHOLE_FORMAT_LOW_MEM_FC(int *ND, int *offset, int *is, int *ie,
  int *nvar, double *Q, char *fname, int *len)
{

  FILE *gridFile = NULL;
  int i, j, k, l, chunk[3], chunk_total, endian, grid_size, N[3], l0;
  char error_str[256];
  int sizeof_int, sizeof_dbl, byteSkip_ks, byteSkip_ke, byteSkip_is, byteSkip_ie, byteSkip_js, byteSkip_je;
  double *dbuf = NULL;
  int *ibuf = NULL;
  char *fname_nullterm;

  /* terminate string */
  fname_nullterm = (char *)malloc((*len+1)*sizeof(char));
  strncpy(fname_nullterm, fname, *len);
  fname_nullterm[*len] = '\0';

  /* variable sizes */
  sizeof_int = sizeof(int);
  sizeof_dbl = sizeof(double);

  /* error on failed open */
  if ((gridFile = fopen(fname_nullterm,"rb+")) == NULL) {
    sprintf(error_str,"%s %s %s","Unable to open file \"", fname_nullterm, "\".");
    perror(error_str);
    goto cleanup1;
  }

  /* check endian :: default is big endian */
  endian = detectEndian( );

  /* seek past offset */
  if (fseek(gridFile, (long int)(*offset), SEEK_SET) != 0) {
    sprintf(error_str,"%s %s %s %s %s","ERROR in fseek on file ", fname_nullterm, "in", __FILE__,".\n");
    perror(error_str);
    goto cleanup2;
  }

  /* now start reading, in chunks when possible */
  chunk_total = 0; grid_size = 1;
  for (i = 0; i < 3; i++) { 
    N[i] = ie[i] - is[i] + 1;
    chunk[i] = N[i] - ND[i]; 
    chunk_total += chunk[i]; 
    grid_size *= N[i];
  }

  /* this block is written in the FORTRAN format */
  /* Write (10) ((((X(L,I,J,K,M),I=1,ND(L,1)),J=1,ND(L,2)),K=1,ND(L,3)),M=1,NDIM) */

  /* we can write the entire block */
  if (chunk_total == 0) { 
    safe_fwrite((long int)Q, sizeof_dbl, grid_size*(*nvar), endian, gridFile);
  }
  
  else {

    /* have to be careful here so we don't overwrite other data */
    /* must seek to the correct location to write out i-chunk */
    /* yuck! */

    /* seeking ... */
    /* ... to first k-location */
    byteSkip_ks = is[2] * ND[0] * ND[1] * sizeof_dbl;

    /* ... to first j-location */
    byteSkip_js = is[1] * ND[0] * sizeof_dbl;

    /* ... to first i-loc */
    byteSkip_is = is[0] * sizeof_dbl;

    /* ... from last written i-location to end of i */
    byteSkip_ie = (ND[0] - ie[0] - 1) * sizeof_dbl;

    /* ... from last written j-location to end of j */
    byteSkip_je = (ND[1] - ie[1] - 1) * ND[0] * sizeof_dbl;

    /* ... from last written k-location to end of k */
    byteSkip_ke = (ND[2] - ie[2] - 1) * ND[0] * ND[1] * sizeof_dbl;

    /* buffer for this i-chunk */
    dbuf = (double *)malloc(N[0]*sizeof_dbl);

    /* skip over k first, then j, then i */
    for (l = 0; l < *nvar; l++) {

      fseek(gridFile, (long int)byteSkip_ks, SEEK_CUR);

      for (k = 0; k < N[2]; k++) {

        fseek(gridFile, (long int)byteSkip_js, SEEK_CUR);

        for (j = 0; j < N[1]; j++) {

          fseek(gridFile, (long int)byteSkip_is, SEEK_CUR);

          for (i = 0; i < N[0]; i++) {
            l0 = N[1]*N[0]*k + N[0]*j + i;
            dbuf[i] = Q[l*grid_size + l0];
          }

          safe_fwrite((long int)dbuf, sizeof_dbl, N[0], endian, gridFile);

          fseek(gridFile, (long int)byteSkip_ie, SEEK_CUR);
 
        }
      
        fseek(gridFile, (long int)byteSkip_je, SEEK_CUR);

      }

      fseek(gridFile, (long int)byteSkip_ke, SEEK_CUR);

    }

    free(dbuf);

  }

  cleanup2:
    fclose(gridFile);

  cleanup1:
    free(fname_nullterm);

  return;

}

#ifdef CMAKE_BUILD
#define F90FLUSH_FC FC_GLOBAL(f90flush, F90FLUSH)
#else /* CMAKE_BUILD */
#define F90FLUSH_FC FC_FUNC(f90flush, F90FLUSH)
#endif /* CMAKE_BUILD */
void F90FLUSH_FC(void)
{

  fflush(stdout);
  fflush(stderr);

}


/* read a plot3d function file in whole format */
#ifdef CMAKE_BUILD
#define READ_PLOT3D_SINGLE_FUNC_WHOLE_FORMAT_LOW_MEM_FC FC_GLOBAL_( \
  read_plot3d_single_func_whole_format_low_mem, READ_PLOT3D_SINGLE_FUNC_WHOLE_FORMAT_LOW_MEM)
#else /* CMAKE_BUILD */
#define READ_PLOT3D_SINGLE_FUNC_WHOLE_FORMAT_LOW_MEM_FC FC_FUNC_( \
  read_plot3d_single_func_whole_format_low_mem, READ_PLOT3D_SINGLE_FUNC_WHOLE_FORMAT_LOW_MEM)
#endif /* CMAKE_BUILD */
void READ_PLOT3D_SINGLE_FUNC_WHOLE_FORMAT_LOW_MEM_FC(int *NDIM, int *ND, int *offset, int *is,
  int *ie, int *gridID, int *nvars, double *Q, char *fname, int *len)
{

  FILE *solnFile = NULL;
  int i, j, k, l, chunk[3], chunk_total, endian, grid_size, N[3], l0;
  char error_str[256];
  int sizeof_int, sizeof_dbl;
  double *dbuf = NULL;
  char *fname_nullterm;

  /* terminate string */
  fname_nullterm = (char *)malloc((*len+1)*sizeof(char));
  strncpy(fname_nullterm, fname, *len);
  fname_nullterm[*len] = '\0';

  /* error on failed open */
  if ((solnFile = fopen(fname_nullterm,"rb")) == NULL) {
    sprintf(error_str,"%s %s %s","Unable to open file ", fname_nullterm, ".\n");
    perror(error_str);
    goto cleanup1;
  }

  /* check endian :: default is big endian */
  endian = detectEndian( );

  /* seek past offset */
  if (fseek(solnFile, (long int)(*offset), SEEK_SET) != 0) {
    sprintf(error_str,"%s %s %s %s %s","ERROR in fseek on file ", fname_nullterm, "in", __FILE__,".\n");
    perror(error_str);
    goto cleanup2;
  }

  /* this block is written in the FORTRAN format */
  /* Write (10) ((((X(L,I,J,K,M),I=1,ND(L,1)),J=1,ND(L,2)),K=1,ND(L,3)),M=1,NDIM+2) */

  /* now start reading, in chunks when possible */
  chunk_total = 0; grid_size = 1;
  for (i = 0; i < *NDIM; i++) { 
    N[i] = ie[i] - is[i] + 1;
    chunk[i] = N[i] - ND[i]; 
    chunk_total += chunk[i]; 
    grid_size *= N[i];
  }

  /* we can read the entire block */
  if (chunk_total == 0) { 
    safe_fread((long int)Q, sizeof(double), grid_size*(*nvars), endian, solnFile); 
  }
  
  else {

    dbuf = (double *)malloc(ND[0]*sizeof(double));
    for (l = 0; l < (*nvars); l++) {
    for (k = 0; k < ND[2]; k++) {
    for (j = 0; j < ND[1]; j++) {
      safe_fread((long int)dbuf, sizeof(double), ND[0], endian, solnFile);
      if ((j >= is[1] && j <= ie[1]) && (k >= is[2] && k <= ie[2])) {
        for (i = is[0]; i <= ie[0]; i++) {
          l0 = N[1]*N[0]*(k-is[2]) + N[0]*(j-is[1]) + i-is[0];
          Q[l*grid_size + l0] = dbuf[i];
        }
      }
    }}}
    free(dbuf);

  }

  cleanup2:
    fclose(solnFile);

  cleanup1:
    free(fname_nullterm);

  return;

}


