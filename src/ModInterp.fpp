! Copyright (c) 2015, University of Illinois at Urbana-Champaign, XPACC
! License: MIT, http://opensource.org/licenses/MIT
!-----------------------------------------------------------------------------------------
!
! ModInterp - code for exchanging data between grids via interpolation
!
! API:
!   * Setup_Interpolation
!     - Called once at the beginning
!     - Reads interpolation data from files and sets up exchanges
!   * Setup_Interpolated_<State,Phi,...>
!     - Called once during initialization of associated data structure
!     - Allocates arrays for receiver data
!   * Exchange_Interpolated_<State,Phi,...>
!     - Called whenever associated data is updated and needs to be communicated between grids
!     - Interpolates data and sends it to other grids
!
! Algorithm overview:
!   * Setup:
!     1) Read in the complete donor/receiver data on the core process of each grid
!     2) Send out lists of each grid's receivers and donors to all processes on the
!        grid's communicator, have each process fill in its rank for points it contains,
!        then collect the ranks back on the core process
!     3) Exchange the collected rank data between all core grid processes, so that now
!        the core grid processes know the locations of each receiver point and donor
!        cell on all grids
!     4) On each grid communicator, send out the grid's receiver and donor information
!        (including the rank lists from the previous step) and have each process pick
!        off and store the data that belongs to the points it contains
!     5) On each rank, assemble data structures to represent exchanges; one exchange
!        is done per (donor-grid/rank,receiver-grid/rank) pair
!   * Exchange:
!     1) Donating processes interpolate the state and send it to the receiving processes
!     2) Receiving processes accumulate the sent data (possibly from multiple sources
!        for a given receiver point, if the donor cell is split across processes) into
!        the full interpolated state
!
!-----------------------------------------------------------------------------------------
MODULE ModInterp

  USE ModGlobal

  private

  public :: Setup_Interpolation

  public :: Setup_Interpolated_State
#ifdef BUILD_ELECTRIC
  public :: Setup_Interpolated_Phi
#endif

  public :: Exchange_Interpolated_State
#ifdef BUILD_ELECTRIC
  public :: Exchange_Interpolated_Phi
#endif

  TYPE t_xintout
    integer :: ngrd, ipall, igall, ipip, ipbp
    integer, allocatable, dimension(:) :: ieng, jeng, keng
    integer, allocatable, dimension(:) :: iipnts, ibpnts, iisptr, iieptr
    integer, allocatable, dimension(:,:) :: iit, jit, kit, nit, njt, nkt
    integer, allocatable, dimension(:,:) :: ibt, jbt, kbt, ibct
    real(rfreal), allocatable, dimension(:,:) :: dxit, dyit, dzit
    real(rfreal), allocatable, dimension(:,:,:,:) :: coeffit
  END TYPE t_xintout

  type t_data_ptr
    real(rfreal), pointer :: ptr(:,:)
  end type t_data_ptr

CONTAINS

  subroutine Setup_Interpolation(region)

    USE ModGlobal
    USE ModDataStruct
    USE ModMPI

    implicit none

    ! ... function call variables
    type (t_region), pointer :: region

    ! ... local variables
    integer :: ierr
    real(rfreal) :: timer

    call MPI_Barrier(mycomm, ierr)

    allocate(region%interp)

    call Read_Interpolation_File(region)
    call Generate_Communication_Maps(region)
    call Distribute_Receiver_Data(region)
    call Distribute_Donor_Data(region)
    call Setup_Receive_Exchanges(region)
    call Setup_Donate_Exchanges(region)
    call Cleanup_Interpolation_Setup_Data(region)

    call MPI_Barrier(mycomm, ierr)

  end subroutine Setup_Interpolation

  subroutine Read_Interpolation_File(region)

    USE ModGlobal
    USE ModDataStruct
    USE ModMPI

    IMPLICIT NONE

    ! ... function call variables
    TYPE (t_region), POINTER :: region

    ! ... local variables
    type (t_mixt_input), pointer :: input
    type (t_interp), pointer :: interp
    type (t_xintout) :: xintout
    real(rfreal) :: timer1, timer2
    integer :: i, j, m, n
    integer :: ierr
    integer :: ndonor_ranks_max
    integer :: max_i, max_j, max_k
    integer, allocatable :: nstencil_max(:)
    integer, allocatable :: receiver_to_donor_grid(:,:), donor_to_receiver_grid(:,:)
    integer, allocatable :: receiver_to_donor(:,:), donor_to_receiver(:,:)
    integer, allocatable :: idonor(:)

    input => region%input
    interp => region%interp

    ! Assume a donor cell can be shared between up to two ranks in each direction
    ndonor_ranks_max = 2**input%ND

    call MPI_Barrier(mycomm, ierr)
    timer1 = MPI_Wtime()

    ! Read the entire interpolation data file(s) on the core rank of each grid
    if (any(region%grid_to_processor == region%myrank)) then

      allocate(interp%global_receivers(region%nGridsGlobal))
      allocate(interp%global_donors(region%nGridsGlobal))

      select case (input%interpType)
      case (BELLERO)
        call Read_XINTOUT(region, input%xintout_ho_fname, input%xintout_x_fname, xintout)
      case (OGEN)
        call Read_XINTOUT_OGEN(region, input%xintout_ho_fname, xintout)
      case default
        call graceful_exit(region%myrank, 'PlasComCM: ERROR: Unknown region%input%interpType.  Only BELLERO or OGEN are valid.')
      end select

    end if

    call MPI_Barrier(mycomm, ierr)
    timer2 = MPI_Wtime()
    region%interpio_time = timer2 - timer1

    if (any(region%grid_to_processor == region%myrank)) then

      ! Now convert to internal data representation

      ! Figure out mappings between donors and receivers
      allocate(receiver_to_donor_grid(xintout%ipbp,xintout%ngrd))
      allocate(receiver_to_donor(xintout%ipbp,xintout%ngrd))
      allocate(donor_to_receiver_grid(xintout%ipip,xintout%ngrd))
      allocate(donor_to_receiver(xintout%ipip,xintout%ngrd))
      do n = 1, region%nGridsGlobal
        do i = 1, xintout%ibpnts(n)
          m = nggrid(xintout%ibct(i,n), xintout%iisptr, xintout%iieptr, &
            xintout%ipbp, region%nGridsGlobal)
          j = xintout%ibct(i,n) - xintout%iisptr(m) + 1
          receiver_to_donor_grid(i,n) = m
          receiver_to_donor(i,n) = j
          donor_to_receiver_grid(j,m) = n
          donor_to_receiver(j,m) = i
        end do
      end do

      ! Fill receiver data
      do n = 1, region%nGridsGlobal
        interp%global_receivers(n)%nreceivers = xintout%ibpnts(n)
        allocate(interp%global_receivers(n)%point(MAX_ND,interp%global_receivers(n)%nreceivers))
        allocate(interp%global_receivers(n)%rank(interp%global_receivers(n)%nreceivers))
        allocate(interp%global_receivers(n)%donor_grid(interp%global_receivers(n)%nreceivers))
        allocate(interp%global_receivers(n)%donor_index(interp%global_receivers(n)%nreceivers))
        interp%global_receivers(n)%ndonor_ranks_max = ndonor_ranks_max
        allocate(interp%global_receivers(n)%ndonor_ranks(interp%global_receivers(n)%nreceivers))
        allocate(interp%global_receivers(n)%donor_rank( &
          interp%global_receivers(n)%ndonor_ranks_max,interp%global_receivers(n)%nreceivers))
        do i = 1, xintout%ibpnts(n)
          interp%global_receivers(n)%point(:,i) = [xintout%ibt(i,n), xintout%jbt(i,n), &
            xintout%kbt(i,n)]
          interp%global_receivers(n)%donor_grid(i) = receiver_to_donor_grid(i,n)
          interp%global_receivers(n)%donor_index(i) = receiver_to_donor(i,n)
        end do
      end do

      ! Allocate donor data
      do m = 1, region%nGridsGlobal
        interp%global_donors(m)%ndonors = xintout%iipnts(m)
        max_i = maxval(xintout%nit(:xintout%iipnts(m),m))
        max_j = maxval(xintout%njt(:xintout%iipnts(m),m))
        max_k = maxval(xintout%nkt(:xintout%iipnts(m),m))
        interp%global_donors(m)%nstencil_max = max(max_i, max_j, max_k)
        allocate(interp%global_donors(m)%lower_point(MAX_ND,interp%global_donors(m)%ndonors))
        allocate(interp%global_donors(m)%upper_point(MAX_ND,interp%global_donors(m)%ndonors))
        allocate(interp%global_donors(m)%coef(interp%global_donors(m)%nstencil_max,input%ND, &
          interp%global_donors(m)%ndonors))
        interp%global_donors(m)%nranks_max = ndonor_ranks_max
        allocate(interp%global_donors(m)%nranks(interp%global_donors(m)%ndonors))
        allocate(interp%global_donors(m)%rank(interp%global_donors(m)%nranks_max, &
          interp%global_donors(m)%ndonors))
        allocate(interp%global_donors(m)%receiver_grid(interp%global_donors(m)%ndonors))
        allocate(interp%global_donors(m)%receiver_index(interp%global_donors(m)%ndonors))
        allocate(interp%global_donors(m)%receiver_rank(interp%global_donors(m)%ndonors))
      end do

      ! Fill donor data, sorted by receivers so it's automatically in the correct order when
      ! setting up exchanges
      allocate(idonor(region%nGridsGlobal))
      idonor = 1

      do n = 1, region%nGridsGlobal
        do i = 1, xintout%ibpnts(n)
          m = interp%global_receivers(n)%donor_grid(i)
          j = interp%global_receivers(n)%donor_index(i)
          interp%global_donors(m)%lower_point(:,idonor(m)) = [xintout%iit(j,m),xintout%jit(j,m), &
            xintout%kit(j,m)]
          interp%global_donors(m)%upper_point(:,idonor(m)) = [xintout%iit(j,m) + xintout%nit(j,m) - 1, &
            xintout%jit(j,m) + xintout%njt(j,m) - 1, xintout%kit(j,m) + xintout%nkt(j,m) - 1]
          interp%global_donors(m)%coef(:,:,idonor(m)) = xintout%coeffit(j,:,:input%ND,m)
          interp%global_donors(m)%receiver_grid(idonor(m)) = donor_to_receiver_grid(j,m)
          interp%global_donors(m)%receiver_index(idonor(m)) = donor_to_receiver(j,m)
          idonor(m) = idonor(m) + 1
        end do
      end do

    end if

    call MPI_Barrier(mycomm, ierr)
    timer2 = MPI_Wtime()
    region%mpi_timings(:)%interp_setup = region%mpi_timings(:)%interp_setup + (timer2 - timer1)

  end subroutine Read_Interpolation_File

  subroutine Generate_Communication_Maps(region)

    USE ModGlobal
    USE ModDataStruct
    USE ModMPI

    IMPLICIT NONE

    ! ... function call variables
    TYPE (t_region), POINTER :: region

    ! ... local variables
    type (t_mixt_input), pointer :: input
    type (t_interp), pointer :: interp
    type (t_grid), pointer :: grid
    real(rfreal) :: timer1, timer2
    integer :: i, j, l, m, n
    integer :: ng
    integer :: nbatches
    integer :: ibatch
    integer :: ierr
    integer :: ndonor_ranks_max
    integer, allocatable :: npoints_per_batch(:)
    integer :: nreceivers
    integer :: ndonors
    integer :: point(MAX_ND)
    integer :: lower_point(MAX_ND), upper_point(MAX_ND)
    integer, allocatable :: batch_size(:), batch_start(:), batch_end(:)
    integer, allocatable :: receiver_point(:,:)
    integer, allocatable :: receiver_rank(:)
    integer, allocatable :: donor_lower_point(:,:), donor_upper_point(:,:)
    integer, allocatable :: donor_rank(:)
    integer, allocatable :: min_donor_rank(:)

    integer, parameter :: ikl = selected_int_kind(18)

    integer(ikl) :: global_size(MAX_ND)

    input => region%input
    interp => region%interp

    call MPI_Barrier(mycomm, ierr)
    timer1 = MPI_Wtime()

    if (region%myrank == 0) THEN
       write (*, '(a)') "PlasComCM: Generating communication maps."
    end if

    ! Idea: Send interpolation data to non-core ranks in batches (to avoid excessive memory use),
    !   have them fill in their rank for points that belong to them, then collect rank data back
    !   to the core grid ranks and exchange between grids

    allocate(npoints_per_batch(region%nGrids))

    ! Figure out how large each batch should be (3x the approximate size of a rank's
    ! decomposition seems like a good baseline)
    do ng = 1, region%nGrids
      grid => region%grid(ng)
      n = grid%iGridGlobal
      global_size = int(grid%GlobalSize,kind=ikl)
      npoints_per_batch(ng) = 3 * int(product(global_size)/grid%numProc_inComm)
    end do

    ! Send receiver points and collect ranks
    do ng = 1, region%nGrids

      grid => region%grid(ng)
      n = grid%iGridGlobal

      if (grid%myRank_inComm == 0) then
        nreceivers = interp%global_receivers(n)%nreceivers
      end if
      call MPI_Bcast(nreceivers, 1, MPI_INTEGER, 0, grid%Comm, ierr)

      nbatches = (nreceivers + npoints_per_batch(ng) - 1)/npoints_per_batch(ng)

      allocate(batch_size(nbatches))
      allocate(batch_start(nbatches))
      allocate(batch_end(nbatches))
      call batch_divide(nreceivers, nbatches, batch_size, batch_start, batch_end)

      if (grid%myRank_inComm == 0) then
        interp%global_receivers(n)%rank = -1
        do ibatch = 1, nbatches
          allocate(receiver_rank(batch_size(ibatch)))
          call MPI_Bcast(interp%global_receivers(n)%point(:,batch_start(ibatch): &
            batch_end(ibatch)), MAX_ND*batch_size(ibatch), MPI_INTEGER, 0, grid%Comm, ierr)
          do i = 1, batch_size(ibatch)
            j = i + batch_start(ibatch) - 1
            if (all(interp%global_receivers(n)%point(:,j) >= grid%is_unique) .and. &
              all(interp%global_receivers(n)%point(:,j) <= grid%ie_unique)) then
              interp%global_receivers(n)%rank(j) = region%myrank
            end if
          end do
          call MPI_Reduce(interp%global_receivers(n)%rank(batch_start(ibatch): &
            batch_end(ibatch)), receiver_rank, batch_size(ibatch), MPI_INTEGER, MPI_MAX, 0, &
            grid%Comm, ierr)
          interp%global_receivers(n)%rank(batch_start(ibatch):batch_end(ibatch)) = receiver_rank
          deallocate(receiver_rank)
        end do
      else
        do ibatch = 1, nbatches
          allocate(receiver_point(MAX_ND,batch_size(ibatch)))
          allocate(receiver_rank(batch_size(ibatch)))
          call MPI_Bcast(receiver_point, MAX_ND*batch_size(ibatch), MPI_INTEGER, 0, grid%Comm, ierr)
          receiver_rank = -1
          do i = 1, batch_size(ibatch)
            point = receiver_point(:,i)
            if (all(point >= grid%is_unique) .and. all(point <= grid%ie_unique)) then
              receiver_rank(i) = region%myrank
            end if
          end do
          call MPI_Reduce(receiver_rank, 0, batch_size(ibatch), MPI_INTEGER, &
            MPI_MAX, 0, grid%Comm, ierr)
          deallocate(receiver_point)
          deallocate(receiver_rank)
        end do
      end if

      deallocate(batch_size)
      deallocate(batch_start)
      deallocate(batch_end)

    end do

    ! Send donor points and collect ranks
    do ng = 1, region%nGrids

      grid => region%grid(ng)
      m = grid%iGridGlobal

      if (grid%myRank_inComm == 0) then
        ndonors = interp%global_donors(m)%ndonors
      end if
      call MPI_Bcast(ndonors, 1, MPI_INTEGER, 0, grid%Comm, ierr)

      nbatches = (ndonors + npoints_per_batch(ng) - 1)/npoints_per_batch(ng)

      allocate(batch_size(nbatches))
      allocate(batch_start(nbatches))
      allocate(batch_end(nbatches))
      call batch_divide(ndonors, nbatches, batch_size, batch_start, batch_end)

      if (grid%myRank_inComm == 0) then
        interp%global_donors(m)%rank = -1
        interp%global_donors(m)%nranks = 0
        do ibatch = 1, nbatches
          call MPI_Bcast(interp%global_donors(m)%lower_point(:,batch_start(ibatch): &
            batch_end(ibatch)),MAX_ND*batch_size(ibatch), MPI_INTEGER, 0, grid%Comm, ierr)
          call MPI_Bcast(interp%global_donors(m)%upper_point(:,batch_start(ibatch): &
            batch_size(ibatch)), MAX_ND*batch_size(ibatch), MPI_INTEGER, 0, grid%Comm, ierr)
          allocate(donor_rank(batch_size(ibatch)))
          allocate(min_donor_rank(batch_size(ibatch)))
          donor_rank = numproc
          do i = 1, batch_size(ibatch)
            j = i + batch_start(ibatch) - 1
            lower_point = interp%global_donors(m)%lower_point(:,j)
            upper_point = interp%global_donors(m)%upper_point(:,j)
            if (overlaps_local(grid, lower_point, upper_point)) then
              donor_rank(i) = region%myrank
            end if
          end do
          ! Collect ranks by finding the minimum overlapping rank, removing it, then repeating
          call MPI_Bcast(interp%global_donors(m)%nranks_max, 1, MPI_INTEGER, 0, grid%Comm, ierr)
          do l = 1, interp%global_donors(m)%nranks_max
            call MPI_Allreduce(donor_rank, min_donor_rank, batch_size(ibatch), MPI_INTEGER, &
              MPI_MIN, grid%Comm, ierr)
            do i = 1, batch_size(ibatch)
              j = i + batch_start(ibatch) - 1
              if (min_donor_rank(i) /= numproc) then
                interp%global_donors(m)%nranks(j) = interp%global_donors(m)%nranks(j) + 1
                interp%global_donors(m)%rank(interp%global_donors(m)%nranks(j),j) = &
                  min_donor_rank(i)
              end if
            end do
            do i = 1, batch_size(ibatch)
              if (min_donor_rank(i) == region%myrank) then
                donor_rank(i) = numproc
              end if
            end do
          end do
          deallocate(donor_rank)
          deallocate(min_donor_rank)
        end do
      else
        do ibatch = 1, nbatches
          allocate(donor_lower_point(MAX_ND,batch_size(ibatch)))
          allocate(donor_upper_point(MAX_ND,batch_size(ibatch)))
          allocate(donor_rank(batch_size(ibatch)))
          allocate(min_donor_rank(batch_size(ibatch)))
          call MPI_Bcast(donor_lower_point, MAX_ND*batch_size(ibatch), MPI_INTEGER, 0, grid%Comm, &
            ierr)
          call MPI_Bcast(donor_upper_point, MAX_ND*batch_size(ibatch), MPI_INTEGER, 0, grid%Comm, &
            ierr)
          donor_rank = numproc
          do i = 1, batch_size(ibatch)
            if (overlaps_local(grid, donor_lower_point(:,i), donor_upper_point(:,i))) then
              donor_rank(i) = region%myrank
            end if
          end do
          ! Collect ranks by finding the minimum overlapping rank, removing it, then repeating
          call MPI_Bcast(ndonor_ranks_max, 1, MPI_INTEGER, 0, grid%Comm, ierr)
          do l = 1, ndonor_ranks_max
            call MPI_Allreduce(donor_rank, min_donor_rank, batch_size(ibatch), MPI_INTEGER, &
              MPI_MIN, grid%Comm, ierr)
            do i = 1, batch_size(ibatch)
              if (min_donor_rank(i) == region%myrank) then
                donor_rank(i) = numproc
              end if
            end do
          end do
          deallocate(donor_lower_point)
          deallocate(donor_upper_point)
          deallocate(donor_rank)
          deallocate(min_donor_rank)
        end do
      end if

      deallocate(batch_size)
      deallocate(batch_start)
      deallocate(batch_end)

    end do

    if (any(region%grid_to_processor == region%myrank)) then

      ! Now exchange donor/receiver rank data between grid core ranks
      do n = 1, region%nGridsGlobal
        call MPI_Bcast(interp%global_receivers(n)%rank, interp%global_receivers(n)%nreceivers, &
          MPI_INTEGER, region%grid_to_processor_coreComm(n), region%coreComm, ierr)
      end do
      do m = 1, region%nGridsGlobal
        call MPI_Bcast(interp%global_donors(m)%nranks_max, 1, MPI_INTEGER, &
          region%grid_to_processor_coreComm(m), region%coreComm, ierr)
        call MPI_Bcast(interp%global_donors(m)%nranks, interp%global_donors(m)%ndonors, &
          MPI_INTEGER, region%grid_to_processor_coreComm(m), region%coreComm, ierr)
        call MPI_Bcast(interp%global_donors(m)%rank, interp%global_donors(m)%nranks_max* &
          interp%global_donors(m)%ndonors, MPI_INTEGER, region%grid_to_processor_coreComm(m), &
          region%coreComm, ierr)
      end do

      ! Fill in receivers' donor rank information and donors' receiver rank information
      do n = 1, region%nGridsGlobal
        do i = 1, interp%global_receivers(n)%nreceivers
          m = interp%global_receivers(n)%donor_grid(i)
          j = interp%global_receivers(n)%donor_index(i)
          interp%global_receivers(n)%ndonor_ranks(i) = interp%global_donors(m)%nranks(j)
          interp%global_receivers(n)%donor_rank(:,i) = interp%global_donors(m)%rank(:,j)
        end do
      end do
      do m = 1, region%nGridsGlobal
        do i = 1, interp%global_donors(m)%ndonors
          n = interp%global_donors(m)%receiver_grid(i)
          j = interp%global_donors(m)%receiver_index(i)
          interp%global_donors(m)%receiver_rank(i) = interp%global_receivers(n)%rank(j)
        end do
      end do

    end if

    call MPI_Barrier(mycomm, ierr)
    timer2 = MPI_Wtime()
    region%mpi_timings(:)%interp_setup = region%mpi_timings(:)%interp_setup + (timer2 - timer1)

    if (region%myrank == 0) THEN
       write (*, '(a)') "PlasComCM: Done generating communication maps."
    end if

  end subroutine Generate_Communication_Maps

  subroutine Distribute_Receiver_Data(region)

    USE ModGlobal
    USE ModDataStruct
    USE ModMPI

    IMPLICIT NONE

    ! ... function call variables
    TYPE (t_region), POINTER :: region

    ! ... local variables
    type (t_mixt_input), pointer :: input
    type (t_interp), pointer :: interp
    type (t_grid), pointer :: grid
    real(rfreal) :: timer1, timer2
    integer :: i, j, l, m, n
    integer :: ng
    integer, allocatable :: ndonor_ranks_max(:)
    integer :: nbatches
    integer :: ibatch
    integer :: ierr
    integer, allocatable :: npoints_per_batch(:)
    integer :: nreceivers
    integer :: point(MAX_ND)
    integer, allocatable :: irecv(:)
    integer, allocatable :: nreceivers_local(:)
    integer, allocatable :: batch_size(:), batch_start(:), batch_end(:)
    integer, allocatable :: receiver_point(:,:)
    integer, allocatable :: donor_grid(:)
    integer, allocatable :: donor_index(:)
    integer, allocatable :: ndonor_ranks(:)
    integer, allocatable :: donor_rank(:,:)

    integer, parameter :: ikl = selected_int_kind(18)

    integer(ikl) :: global_size(MAX_ND)

    input => region%input
    interp => region%interp

    call MPI_Barrier(mycomm, ierr)
    timer1 = MPI_Wtime()

    if (region%myrank == 0) THEN
       write (*, '(a)') "PlasComCM: Distributing receiver data."
    end if

    ! Idea: Again send interpolation data to non-core ranks in chunks (now including the
    !   donor/receiver ranks mapped out in the previous step), and have them identify and store
    !   the data they need

    allocate(npoints_per_batch(region%nGrids))

    ! Figure out how large each batch should be (3x the approximate size of a rank's
    ! decomposition seems like a good baseline)
    do ng = 1, region%nGrids
      grid => region%grid(ng)
      n = grid%iGridGlobal
      global_size = int(grid%GlobalSize,kind=ikl)
      npoints_per_batch(ng) = 3 * int(product(global_size)/grid%numProc_inComm)
    end do

    ! Send max number of donor ranks
    allocate(ndonor_ranks_max(region%nGrids))

    do ng = 1, region%nGrids
      grid => region%grid(ng)
      m = grid%iGridGlobal
      if (grid%myRank_inComm == 0) then
        ndonor_ranks_max(ng) = interp%global_donors(m)%nranks_max
      end if
      call MPI_Bcast(ndonor_ranks_max(ng), 1, MPI_INTEGER, 0, grid%Comm, ierr)
    end do

    ! Determine the number of local receivers on each rank
    allocate(nreceivers_local(region%nGrids))
    nreceivers_local = 0

    do ng = 1, region%nGrids

      grid => region%grid(ng)
      n = grid%iGridGlobal

      if (grid%myRank_inComm == 0) then
        nreceivers = interp%global_receivers(n)%nreceivers
      end if
      call MPI_Bcast(nreceivers, 1, MPI_INTEGER, 0, grid%Comm, ierr)

      nbatches = (nreceivers + npoints_per_batch(ng) - 1)/npoints_per_batch(ng)

      allocate(batch_size(nbatches))
      allocate(batch_start(nbatches))
      allocate(batch_end(nbatches))
      call batch_divide(nreceivers, nbatches, batch_size, batch_start, batch_end)

      if (grid%myRank_inComm == 0) then
        do ibatch = 1, nbatches
          call MPI_Bcast(interp%global_receivers(n)%point(:,batch_start(ibatch): &
            batch_end(ibatch)), MAX_ND*batch_size(ibatch), MPI_INTEGER, 0, grid%Comm, ierr)
          do i = 1, batch_size(ibatch)
            j = i + batch_start(ibatch) - 1
            point = interp%global_receivers(n)%point(:,j)
            if (all(point >= grid%is_unique) .and. all(point <= grid%ie_unique)) then
              nreceivers_local(ng) = nreceivers_local(ng) + 1
            end if
          end do
        end do
      else
        do ibatch = 1, nbatches
          allocate(receiver_point(MAX_ND,batch_size(ibatch)))
          call MPI_Bcast(receiver_point, MAX_ND*batch_size(ibatch), MPI_INTEGER, 0, grid%Comm, ierr)
          do i = 1, batch_size(ibatch)
            point = receiver_point(:,i)
            if (all(point >= grid%is_unique) .and. all(point <= grid%ie_unique)) then
              nreceivers_local(ng) = nreceivers_local(ng) + 1
            end if
          end do
          deallocate(receiver_point)
        end do
      end if

      deallocate(batch_size)
      deallocate(batch_start)
      deallocate(batch_end)

    end do

    ! Now allocate data structures to hold the local receivers
    allocate(interp%local_receivers(region%nGrids))

    do ng = 1, region%nGrids
      interp%local_receivers(ng)%nreceivers = nreceivers_local(ng)
      allocate(interp%local_receivers(ng)%point(MAX_ND,interp%local_receivers(ng)%nreceivers))
      allocate(interp%local_receivers(ng)%rank(interp%local_receivers(ng)%nreceivers))
      allocate(interp%local_receivers(ng)%donor_grid(interp%local_receivers(ng)%nreceivers))
      allocate(interp%local_receivers(ng)%donor_index(interp%local_receivers(ng)%nreceivers))
      interp%local_receivers(ng)%ndonor_ranks_max = ndonor_ranks_max(ng)
      allocate(interp%local_receivers(ng)%ndonor_ranks(interp%local_receivers(ng)%nreceivers))
      allocate(interp%local_receivers(ng)%donor_rank(interp%local_receivers(ng)%ndonor_ranks_max, &
        interp%local_receivers(ng)%nreceivers))
    end do

    ! Fill in local receiver data
    allocate(irecv(region%nGrids))
    irecv = 1

    do ng = 1, region%nGrids

      grid => region%grid(ng)
      n = grid%iGridGlobal

      if (grid%myRank_inComm == 0) then
        nreceivers = interp%global_receivers(n)%nreceivers
      end if
      call MPI_Bcast(nreceivers, 1, MPI_INTEGER, 0, grid%Comm, ierr)

      nbatches = (nreceivers + npoints_per_batch(ng) - 1)/npoints_per_batch(ng)

      allocate(batch_size(nbatches))
      allocate(batch_start(nbatches))
      allocate(batch_end(nbatches))
      call batch_divide(nreceivers, nbatches, batch_size, batch_start, batch_end)

      if (grid%myRank_inComm == 0) then
        do ibatch = 1, nbatches
          call MPI_Bcast(interp%global_receivers(n)%point(:,batch_start(ibatch): &
            batch_end(ibatch)), MAX_ND*batch_size(ibatch), MPI_INTEGER, 0, grid%Comm, ierr)
          call MPI_Bcast(interp%global_receivers(n)%donor_grid(batch_start(ibatch): &
            batch_end(ibatch)), batch_size(ibatch), MPI_INTEGER, 0, grid%Comm, ierr)
          call MPI_Bcast(interp%global_receivers(n)%donor_index(batch_start(ibatch): &
            batch_end(ibatch)), batch_size(ibatch), MPI_INTEGER, 0, grid%Comm, ierr)
          call MPI_Bcast(interp%global_receivers(n)%ndonor_ranks(batch_start(ibatch): &
            batch_end(ibatch)), batch_size(ibatch), MPI_INTEGER, 0, grid%Comm, ierr)
          call MPI_Bcast(interp%global_receivers(n)%donor_rank(:,batch_start(ibatch): &
            batch_end(ibatch)), interp%global_receivers(n)%ndonor_ranks_max*batch_size(ibatch), &
            MPI_INTEGER, 0, grid%Comm, ierr)
          do i = 1, batch_size(ibatch)
            j = i + batch_start(ibatch) - 1
            point = interp%global_receivers(n)%point(:,j)
            if (all(point >= grid%is_unique) .and. all(point <= grid%ie_unique)) then
              interp%local_receivers(ng)%point(:,irecv(ng)) = point
              interp%local_receivers(ng)%rank(irecv(ng)) = region%myrank
              interp%local_receivers(ng)%donor_grid(irecv(ng)) = &
                interp%global_receivers(n)%donor_grid(j)
              interp%local_receivers(ng)%donor_index(irecv(ng)) = &
                interp%global_receivers(n)%donor_index(j)
              interp%local_receivers(ng)%ndonor_ranks(irecv(ng)) = &
                interp%global_receivers(n)%ndonor_ranks(j)
              interp%local_receivers(ng)%donor_rank(:,irecv(ng)) = &
                interp%global_receivers(n)%donor_rank(:,j)
              irecv(ng) = irecv(ng) + 1
            end if
          end do
        end do
      else
        do ibatch = 1, nbatches
          allocate(receiver_point(MAX_ND,batch_size(ibatch)))
          allocate(donor_grid(batch_size(ibatch)))
          allocate(donor_index(batch_size(ibatch)))
          allocate(ndonor_ranks(batch_size(ibatch)))
          allocate(donor_rank(ndonor_ranks_max(ng),batch_size(ibatch)))
          call MPI_Bcast(receiver_point, MAX_ND*batch_size(ibatch), MPI_INTEGER, 0, grid%Comm, ierr)
          call MPI_Bcast(donor_grid, batch_size(ibatch), MPI_INTEGER, 0, grid%Comm, ierr)
          call MPI_Bcast(donor_index, batch_size(ibatch), MPI_INTEGER, 0, grid%Comm, ierr)
          call MPI_Bcast(ndonor_ranks, batch_size(ibatch), MPI_INTEGER, 0, grid%Comm, ierr)
          call MPI_Bcast(donor_rank, ndonor_ranks_max(ng)*batch_size(ibatch), MPI_INTEGER, 0, &
            grid%Comm, ierr)
          do i = 1, batch_size(ibatch)
            point = receiver_point(:,i)
            if (all(point >= grid%is_unique) .and. all(point <= grid%ie_unique)) then
              interp%local_receivers(ng)%point(:,irecv(ng)) = point
              interp%local_receivers(ng)%rank(irecv(ng)) = region%myrank
              interp%local_receivers(ng)%donor_grid(irecv(ng)) = donor_grid(i)
              interp%local_receivers(ng)%donor_index(irecv(ng)) = donor_index(i)
              interp%local_receivers(ng)%ndonor_ranks(irecv(ng)) = ndonor_ranks(i)
              interp%local_receivers(ng)%donor_rank(:,irecv(ng)) = donor_rank(:,i)
              irecv(ng) = irecv(ng) + 1
            end if
          end do
          deallocate(receiver_point)
          deallocate(donor_grid)
          deallocate(donor_index)
          deallocate(donor_rank)
          deallocate(ndonor_ranks)
        end do
      end if

      deallocate(batch_size)
      deallocate(batch_start)
      deallocate(batch_end)

    end do

    call MPI_Barrier(mycomm, ierr)
    timer2 = MPI_Wtime()
    region%mpi_timings(:)%interp_setup = region%mpi_timings(:)%interp_setup + (timer2 - timer1)

    if (region%myrank == 0) THEN
       write (*, '(a)') "PlasComCM: Done distributing receiver data."
    end if

  end subroutine Distribute_Receiver_Data

  subroutine Distribute_Donor_Data(region)

    USE ModGlobal
    USE ModDataStruct
    USE ModMPI

    IMPLICIT NONE

    ! ... function call variables
    TYPE (t_region), POINTER :: region

    ! ... local variables
    type (t_mixt_input), pointer :: input
    type (t_interp), pointer :: interp
    type (t_grid), pointer :: grid
    real(rfreal) :: timer1, timer2
    integer :: i, j, l, m, n
    integer :: ng
    integer, allocatable :: nstencil_max(:)
    integer, allocatable :: ndonor_ranks_max(:)
    integer :: nbatches
    integer :: ibatch
    integer :: ierr
    integer, allocatable :: npoints_per_batch(:)
    integer :: ndonors
    integer :: point(MAX_ND)
    integer :: lower_point(MAX_ND), upper_point(MAX_ND)
    integer, allocatable :: idonor(:)
    integer, allocatable :: ndonors_local(:)
    integer, allocatable :: batch_size(:), batch_start(:), batch_end(:)
    integer, allocatable :: donor_lower_point(:,:), donor_upper_point(:,:)
    real(rfreal), allocatable :: donor_coef(:,:,:)
    integer, allocatable :: ndonor_ranks(:)
    integer, allocatable :: donor_rank(:,:)
    integer, allocatable :: receiver_grid(:)
    integer, allocatable :: receiver_index(:)
    integer, allocatable :: receiver_rank(:)

    integer, parameter :: ikl = selected_int_kind(18)

    integer(ikl) :: global_size(MAX_ND)

    input => region%input
    interp => region%interp

    call MPI_Barrier(mycomm, ierr)
    timer1 = MPI_Wtime()

    if (region%myrank == 0) THEN
       write (*, '(a)') "PlasComCM: Distributing donor data."
    end if

    ! Idea: Again send interpolation data to non-core ranks in chunks (now including the
    !   donor/receiver ranks mapped out in the previous step), and have them identify and store
    !   the data they need

    allocate(npoints_per_batch(region%nGrids))

    ! Figure out how large each batch should be (3x the approximate size of a rank's
    ! decomposition seems like a good baseline)
    do ng = 1, region%nGrids
      grid => region%grid(ng)
      n = grid%iGridGlobal
      global_size = int(grid%GlobalSize,kind=ikl)
      npoints_per_batch(ng) = 3 * int(product(global_size)/grid%numProc_inComm)
    end do

    ! Send max stencil size
    allocate(nstencil_max(region%nGrids))

    do ng = 1, region%nGrids
      grid => region%grid(ng)
      m = grid%iGridGlobal
      if (grid%myRank_inComm == 0) then
        nstencil_max(ng) = interp%global_donors(m)%nstencil_max
      end if
      call MPI_Bcast(nstencil_max(ng), 1, MPI_INTEGER, 0, grid%Comm, ierr)
    end do

    ! Send max number of donor ranks
    allocate(ndonor_ranks_max(region%nGrids))

    do ng = 1, region%nGrids
      grid => region%grid(ng)
      m = grid%iGridGlobal
      if (grid%myRank_inComm == 0) then
        ndonor_ranks_max(ng) = interp%global_donors(m)%nranks_max
      end if
      call MPI_Bcast(ndonor_ranks_max(ng), 1, MPI_INTEGER, 0, grid%Comm, ierr)
    end do

    ! Determine the number of local donors on each rank
    allocate(ndonors_local(region%nGrids))
    ndonors_local = 0

    do ng = 1, region%nGrids

      grid => region%grid(ng)
      m = grid%iGridGlobal

      if (grid%myRank_inComm == 0) then
        ndonors = interp%global_donors(m)%ndonors
      end if
      call MPI_Bcast(ndonors, 1, MPI_INTEGER, 0, grid%Comm, ierr)

      nbatches = (ndonors + npoints_per_batch(ng) - 1)/npoints_per_batch(ng)

      allocate(batch_size(nbatches))
      allocate(batch_start(nbatches))
      allocate(batch_end(nbatches))
      call batch_divide(ndonors, nbatches, batch_size, batch_start, batch_end)

      if (grid%myRank_inComm == 0) then
        do ibatch = 1, nbatches
          call MPI_Bcast(interp%global_donors(m)%lower_point(:,batch_start(ibatch): &
            batch_end(ibatch)), MAX_ND*batch_size(ibatch), MPI_INTEGER, 0, grid%Comm, ierr)
          call MPI_Bcast(interp%global_donors(m)%upper_point(:,batch_start(ibatch): &
            batch_size(ibatch)), MAX_ND*batch_size(ibatch), MPI_INTEGER, 0, grid%Comm, ierr)
          do i = 1, batch_size(ibatch)
            j = i + batch_start(ibatch) - 1
            lower_point = interp%global_donors(m)%lower_point(:,j)
            upper_point = interp%global_donors(m)%upper_point(:,j)
            if (overlaps_local(grid, lower_point, upper_point)) then
              ndonors_local(ng) = ndonors_local(ng) + 1
            end if
          end do
        end do
      else
        do ibatch = 1, nbatches
          allocate(donor_lower_point(MAX_ND,batch_size(ibatch)))
          allocate(donor_upper_point(MAX_ND,batch_size(ibatch)))
          call MPI_Bcast(donor_lower_point, MAX_ND*batch_size(ibatch), MPI_INTEGER, 0, grid%Comm, &
            ierr)
          call MPI_Bcast(donor_upper_point, MAX_ND*batch_size(ibatch), MPI_INTEGER, 0, grid%Comm, &
            ierr)
          do i = 1, batch_size(ibatch)
            if (overlaps_local(grid, donor_lower_point(:,i), donor_upper_point(:,i))) then
              ndonors_local(ng) = ndonors_local(ng) + 1
            end if
          end do
          deallocate(donor_lower_point)
          deallocate(donor_upper_point)
        end do
      end if

      deallocate(batch_size)
      deallocate(batch_start)
      deallocate(batch_end)

    end do

    ! Now allocate data structures to hold the local donors
    allocate(interp%local_donors(region%nGrids))

    do ng = 1, region%nGrids
      interp%local_donors(ng)%ndonors = ndonors_local(ng)
      interp%local_donors(ng)%nstencil_max = nstencil_max(ng)
      allocate(interp%local_donors(ng)%lower_point(MAX_ND,interp%local_donors(ng)%ndonors))
      allocate(interp%local_donors(ng)%upper_point(MAX_ND,interp%local_donors(ng)%ndonors))
      allocate(interp%local_donors(ng)%coef(interp%local_donors(ng)%nstencil_max,input%ND, &
        interp%local_donors(ng)%ndonors))
      interp%local_donors(ng)%nranks_max = ndonor_ranks_max(ng)
      allocate(interp%local_donors(ng)%nranks(interp%local_donors(ng)%ndonors))
      allocate(interp%local_donors(ng)%rank(interp%local_donors(ng)%nranks_max, &
        interp%local_donors(ng)%ndonors))
      allocate(interp%local_donors(ng)%receiver_grid(interp%local_donors(ng)%ndonors))
      allocate(interp%local_donors(ng)%receiver_index(interp%local_donors(ng)%ndonors))
      allocate(interp%local_donors(ng)%receiver_rank(interp%local_donors(ng)%ndonors))
    end do

    ! Fill in local donor data
    allocate(idonor(region%nGrids))
    idonor = 1

    do ng = 1, region%nGrids

      grid => region%grid(ng)
      m = grid%iGridGlobal

      if (grid%myRank_inComm == 0) then
        ndonors = interp%global_donors(m)%ndonors
      end if
      call MPI_Bcast(ndonors, 1, MPI_INTEGER, 0, grid%Comm, ierr)

      nbatches = (ndonors + npoints_per_batch(ng) - 1)/npoints_per_batch(ng)

      allocate(batch_size(nbatches))
      allocate(batch_start(nbatches))
      allocate(batch_end(nbatches))
      call batch_divide(ndonors, nbatches, batch_size, batch_start, batch_end)

      if (grid%myRank_inComm == 0) then
        do ibatch = 1, nbatches
          call MPI_Bcast(interp%global_donors(m)%lower_point(:,batch_start(ibatch): &
            batch_end(ibatch)), MAX_ND*batch_size(ibatch), MPI_INTEGER, 0, grid%Comm, ierr)
          call MPI_Bcast(interp%global_donors(m)%upper_point(:,batch_start(ibatch): &
            batch_end(ibatch)), MAX_ND*batch_size(ibatch), MPI_INTEGER, 0, grid%Comm, ierr)
          call MPI_Bcast(interp%global_donors(m)%coef(:,:,batch_start(ibatch):batch_end(ibatch)), &
            interp%global_donors(m)%nstencil_max*input%ND*batch_size(ibatch), MPI_REAL8, 0, &
            grid%Comm, ierr)
          call MPI_Bcast(interp%global_donors(m)%nranks(batch_start(ibatch):batch_end(ibatch)), &
            batch_size(ibatch), MPI_INTEGER, 0, grid%Comm, ierr)
          call MPI_Bcast(interp%global_donors(m)%rank(:,batch_start(ibatch):batch_end(ibatch)), &
            interp%global_donors(m)%nranks_max*batch_size(ibatch), MPI_INTEGER, 0, grid%Comm, ierr)
          call MPI_Bcast(interp%global_donors(m)%receiver_grid(batch_start(ibatch): &
            batch_end(ibatch)), batch_size(ibatch), MPI_INTEGER, 0, grid%Comm, ierr)
          call MPI_Bcast(interp%global_donors(m)%receiver_index(batch_start(ibatch): &
            batch_end(ibatch)), batch_size(ibatch), MPI_INTEGER, 0, grid%Comm, ierr)
          call MPI_Bcast(interp%global_donors(m)%receiver_rank(batch_start(ibatch): &
            batch_end(ibatch)), batch_size(ibatch), MPI_INTEGER, 0, grid%Comm, ierr)
          do i = 1, batch_size(ibatch)
            j = i + batch_start(ibatch) - 1
            lower_point = interp%global_donors(m)%lower_point(:,j)
            upper_point = interp%global_donors(m)%upper_point(:,j)
            if (overlaps_local(grid, lower_point, upper_point)) then
              interp%local_donors(ng)%lower_point(:,idonor(ng)) = &
                interp%global_donors(m)%lower_point(:,j)
              interp%local_donors(ng)%upper_point(:,idonor(ng)) = &
                interp%global_donors(m)%upper_point(:,j)
              interp%local_donors(ng)%coef(:,:,idonor(ng)) = &
                interp%global_donors(m)%coef(:,:,j)
              interp%local_donors(ng)%nranks(idonor(ng)) = &
                interp%global_donors(m)%nranks(j)
              interp%local_donors(ng)%rank(:,idonor(ng)) = &
                interp%global_donors(m)%rank(:,j)
              interp%local_donors(ng)%receiver_grid(idonor(ng)) = &
                interp%global_donors(m)%receiver_grid(j)
              interp%local_donors(ng)%receiver_index(idonor(ng)) = &
                interp%global_donors(m)%receiver_index(j)
              interp%local_donors(ng)%receiver_rank(idonor(ng)) = &
                interp%global_donors(m)%receiver_rank(j)
              idonor(ng) = idonor(ng) + 1
            end if
          end do
        end do
      else
        do ibatch = 1, nbatches
          allocate(donor_lower_point(MAX_ND,batch_size(ibatch)))
          allocate(donor_upper_point(MAX_ND,batch_size(ibatch)))
          allocate(donor_coef(nstencil_max(ng),input%ND,batch_size(ibatch)))
          allocate(ndonor_ranks(batch_size(ibatch)))
          allocate(donor_rank(ndonor_ranks_max(ng),batch_size(ibatch)))
          allocate(receiver_grid(batch_size(ibatch)))
          allocate(receiver_index(batch_size(ibatch)))
          allocate(receiver_rank(batch_size(ibatch)))
          call MPI_Bcast(donor_lower_point, MAX_ND*batch_size(ibatch), MPI_INTEGER, 0, grid%Comm, &
            ierr)
          call MPI_Bcast(donor_upper_point, MAX_ND*batch_size(ibatch), MPI_INTEGER, 0, grid%Comm, &
            ierr)
          call MPI_Bcast(donor_coef, nstencil_max(ng)*input%ND*batch_size(ibatch), MPI_REAL8, 0, &
            grid%Comm, ierr)
          call MPI_Bcast(ndonor_ranks, batch_size(ibatch), MPI_INTEGER, 0, grid%Comm, ierr)
          call MPI_Bcast(donor_rank, ndonor_ranks_max(ng)*batch_size(ibatch), MPI_INTEGER, 0, &
            grid%Comm, ierr)
          call MPI_Bcast(receiver_grid, batch_size(ibatch), MPI_INTEGER, 0, grid%Comm, ierr)
          call MPI_Bcast(receiver_index, batch_size(ibatch), MPI_INTEGER, 0, grid%Comm, ierr)
          call MPI_Bcast(receiver_rank, batch_size(ibatch), MPI_INTEGER, 0, grid%Comm, ierr)
          do i = 1, batch_size(ibatch)
            if (overlaps_local(grid, donor_lower_point(:,i), donor_upper_point(:,i))) then
              interp%local_donors(ng)%lower_point(:,idonor(ng)) = donor_lower_point(:,i)
              interp%local_donors(ng)%upper_point(:,idonor(ng)) = donor_upper_point(:,i)
              interp%local_donors(ng)%coef(:,:,idonor(ng)) = donor_coef(:,:,i)
              interp%local_donors(ng)%nranks(idonor(ng)) = ndonor_ranks(i)
              interp%local_donors(ng)%rank(:,idonor(ng)) = donor_rank(:,i)
              interp%local_donors(ng)%receiver_grid(idonor(ng)) = receiver_grid(i)
              interp%local_donors(ng)%receiver_index(idonor(ng)) = receiver_index(i)
              interp%local_donors(ng)%receiver_rank(idonor(ng)) = receiver_rank(i)
              idonor(ng) = idonor(ng) + 1
            end if
          end do
          deallocate(donor_lower_point)
          deallocate(donor_upper_point)
          deallocate(donor_coef)
          deallocate(ndonor_ranks)
          deallocate(donor_rank)
          deallocate(receiver_grid)
          deallocate(receiver_index)
          deallocate(receiver_rank)
        end do
      end if

      deallocate(batch_size)
      deallocate(batch_start)
      deallocate(batch_end)

    end do

    call MPI_Barrier(mycomm, ierr)
    timer2 = MPI_Wtime()
    region%mpi_timings(:)%interp_setup = region%mpi_timings(:)%interp_setup + (timer2 - timer1)

    if (region%myrank == 0) THEN
       write (*, '(a)') "PlasComCM: Done distributing donor data."
    end if

  end subroutine Distribute_Donor_Data

  subroutine Setup_Receive_Exchanges(region)

    USE ModGlobal
    USE ModDataStruct
    USE ModMPI

    IMPLICIT NONE

    ! ... function call variables
    TYPE (t_region), POINTER :: region

    ! ... local variables
    type (t_mixt_input), pointer :: input
    type (t_interp), pointer :: interp
    type (t_grid), pointer :: grid
    real(rfreal) :: timer1, timer2
    integer :: ierr
    integer :: i, j, l, m, n, r
    integer :: ng
    integer :: ig, ir
    integer :: iexch
    integer :: ndonor_grids
    integer :: ndonor_ranks
    integer, allocatable :: irecv(:)
    logical, allocatable :: receives_from_grid(:)
    logical, allocatable :: receives_from_rank(:)
    logical, allocatable :: receives_from_grid_on_rank(:,:,:)
    integer, allocatable :: donor_grid_index(:)
    integer, allocatable :: donor_rank_index(:)
    integer, allocatable :: exchange_index(:,:,:)

    input => region%input
    interp => region%interp

    call MPI_Barrier(mycomm, ierr)
    timer1 = MPI_Wtime()

    if (region%myrank == 0) THEN
       write (*, '(a)') "PlasComCM: Setting up receiver-side exchanges."
    end if

    allocate(receives_from_grid(region%nGridsGlobal))
    allocate(receives_from_rank(0:numproc-1))
    receives_from_grid = .false.
    receives_from_rank = .false.

    ! Figure out which grids and ranks the local receivers receive from, and set up
    ! local indexing for them
    do ng = 1, region%nGrids
      do i = 1, interp%local_receivers(ng)%nreceivers
        receives_from_grid(interp%local_receivers(ng)%donor_grid(i)) = .true.
        do j = 1, interp%local_receivers(ng)%ndonor_ranks(i)
          receives_from_rank(interp%local_receivers(ng)%donor_rank(j,i)) = .true.
        end do
      end do
    end do

    allocate(donor_grid_index(region%nGridsGlobal))
    donor_grid_index = 0

    ig = 1
    do m = 1, region%nGridsGlobal
      if (receives_from_grid(m)) then
        donor_grid_index(m) = ig
        ig = ig + 1
      end if
    end do

    ndonor_grids = ig - 1

    allocate(donor_rank_index(0:numproc-1))
    donor_rank_index = 0

    ir = 1
    do r = 0, numproc-1
      if (receives_from_rank(r)) then
        donor_rank_index(r) = ir
        ir = ir + 1
      end if
    end do

    ndonor_ranks = ir - 1

    ! Figure out how many receive exchanges will be done on the current rank and
    ! set up indexing for them
    allocate(receives_from_grid_on_rank(ndonor_grids,ndonor_ranks,region%nGrids))
    receives_from_grid_on_rank = .false.

    do ng = 1, region%nGrids
      do i = 1, interp%local_receivers(ng)%nreceivers
        ig = donor_grid_index(interp%local_receivers(ng)%donor_grid(i))
        do j = 1, interp%local_receivers(ng)%ndonor_ranks(i)
          ir = donor_rank_index(interp%local_receivers(ng)%donor_rank(j,i))
          receives_from_grid_on_rank(ig,ir,ng) = .true.
        end do
      end do
    end do

    allocate(exchange_index(ndonor_grids,ndonor_ranks,region%nGrids))

    iexch = 1
    do ng = 1, region%nGrids
      do ir = 1, ndonor_ranks
        do ig = 1, ndonor_grids
          if (receives_from_grid_on_rank(ig,ir,ng)) then
            exchange_index(ig,ir,ng) = iexch
            iexch = iexch + 1
          end if
        end do
      end do
    end do

    interp%nreceives = iexch - 1

    ! Set up and fill the receive exchange data structures
    allocate(interp%receive(interp%nreceives))

    do iexch = 1, interp%nreceives
      interp%receive(iexch)%nreceivers = 0
    end do

    do ng = 1, region%nGrids
      grid => region%grid(ng)
      n = grid%iGridGlobal
      do i = 1, interp%local_receivers(ng)%nreceivers
        m = interp%local_receivers(ng)%donor_grid(i)
        ig = donor_grid_index(m)
        do j = 1, interp%local_receivers(ng)%ndonor_ranks(i)
          r = interp%local_receivers(ng)%donor_rank(j,i)
          ir = donor_rank_index(r)
          iexch = exchange_index(ig,ir,ng)
          interp%receive(iexch)%nreceivers = interp%receive(iexch)%nreceivers + 1
          interp%receive(iexch)%receiver_grid = n
          interp%receive(iexch)%receiver_grid_local = ng
          interp%receive(iexch)%donor_grid = m
          interp%receive(iexch)%donor_rank = r
        end do
      end do
    end do

    do iexch = 1, interp%nreceives
      allocate(interp%receive(iexch)%local_receiver_index(interp%receive(iexch)%nreceivers))
    end do

    allocate(irecv(interp%nreceives))
    irecv = 1

    do ng = 1, region%nGrids
      grid => region%grid(ng)
      do i = 1, interp%local_receivers(ng)%nreceivers
        ig = donor_grid_index(interp%local_receivers(ng)%donor_grid(i))
        do j = 1, interp%local_receivers(ng)%ndonor_ranks(i)
          ir = donor_rank_index(interp%local_receivers(ng)%donor_rank(j,i))
          iexch = exchange_index(ig,ir,ng)
          interp%receive(iexch)%local_receiver_index(irecv(iexch)) = i
          irecv(iexch) = irecv(iexch) + 1
        end do
      end do
    end do

    call MPI_Barrier(mycomm, ierr)
    timer2 = MPI_Wtime()
    region%mpi_timings(:)%interp_setup = region%mpi_timings(:)%interp_setup + (timer2 - timer1)

    if (region%myrank == 0) THEN
       write (*, '(a)') "PlasComCM: Done setting up receiver-side exchanges."
    end if

  end subroutine Setup_Receive_Exchanges

  subroutine Setup_Donate_Exchanges(region)

    USE ModGlobal
    USE ModDataStruct
    USE ModMPI

    IMPLICIT NONE

    ! ... function call variables
    TYPE (t_region), POINTER :: region

    ! ... local variables
    type (t_mixt_input), pointer :: input
    type (t_interp), pointer :: interp
    type (t_grid), pointer :: grid
    real(rfreal) :: timer1, timer2
    integer :: ierr
    integer :: i, j, l, m, n, p, q, r
    integer :: ng
    integer :: ig, ir
    integer :: iexch
    integer :: ipoint
    integer :: point(MAX_ND)
    integer :: npoints_local(MAX_ND)
    integer :: lower_point(MAX_ND), upper_point(MAX_ND)
    integer :: nreceiver_grids
    integer :: nreceiver_ranks
    integer, allocatable :: idonor(:)
    logical, allocatable :: donates_to_grid(:)
    logical, allocatable :: donates_to_rank(:)
    logical, allocatable :: donates_to_grid_on_rank(:,:,:)
    integer, allocatable :: receiver_grid_index(:)
    integer, allocatable :: receiver_rank_index(:)
    integer, allocatable :: exchange_index(:,:,:)

    input => region%input
    interp => region%interp

    call MPI_Barrier(mycomm, ierr)
    timer1 = MPI_Wtime()

    if (region%myrank == 0) THEN
       write (*, '(a)') "PlasComCM: Setting up donor-side exchanges."
    end if

    allocate(donates_to_grid(region%nGridsGlobal))
    allocate(donates_to_rank(0:numproc-1))
    donates_to_grid = .false.
    donates_to_rank = .false.

    ! Figure out which grids and ranks the local donors donate to, and set up local indexing
    ! for them
    do ng = 1, region%nGrids
      do i = 1, interp%local_donors(ng)%ndonors
        donates_to_grid(interp%local_donors(ng)%receiver_grid(i)) = .true.
        donates_to_rank(interp%local_donors(ng)%receiver_rank(i)) = .true.
      end do
    end do

    allocate(receiver_grid_index(region%nGridsGlobal))
    receiver_grid_index = 0

    ig = 1
    do n = 1, region%nGridsGlobal
      if (donates_to_grid(n)) then
        receiver_grid_index(n) = ig
        ig = ig + 1
      end if
    end do

    nreceiver_grids = ig - 1

    allocate(receiver_rank_index(0:numproc-1))
    receiver_rank_index = 0

    ir = 1
    do r = 0, numproc-1
      if (donates_to_rank(r)) then
        receiver_rank_index(r) = ir
        ir = ir + 1
      end if
    end do

    nreceiver_ranks = ir - 1

    ! Figure out how many donate exchanges will be done on the current rank and
    ! set up indexing for them
    allocate(donates_to_grid_on_rank(nreceiver_grids,nreceiver_ranks,region%nGrids))
    donates_to_grid_on_rank = .false.

    do ng = 1, region%nGrids
      do i = 1, interp%local_donors(ng)%ndonors
        ig = receiver_grid_index(interp%local_donors(ng)%receiver_grid(i))
        ir = receiver_rank_index(interp%local_donors(ng)%receiver_rank(i))
        donates_to_grid_on_rank(ig,ir,ng) = .true.
      end do
    end do

    allocate(exchange_index(nreceiver_grids,nreceiver_ranks,region%nGrids))

    iexch = 1
    do ng = 1, region%nGrids
      do ir = 1, nreceiver_ranks
        do ig = 1, nreceiver_grids
          if (donates_to_grid_on_rank(ig,ir,ng)) then
            exchange_index(ig,ir,ng) = iexch
            iexch = iexch + 1
          end if
        end do
      end do
    end do

    interp%ndonates = iexch - 1

    ! Set up and fill the donate exchange data structures
    allocate(interp%donate(interp%ndonates))

    do iexch = 1, interp%ndonates
      interp%donate(iexch)%ndonors = 0
    end do

    do ng = 1, region%nGrids
      grid => region%grid(ng)
      m = grid%iGridGlobal
      do i = 1, interp%local_donors(ng)%ndonors
        n = interp%local_donors(ng)%receiver_grid(i)
        ig = receiver_grid_index(n)
        r = interp%local_donors(ng)%receiver_rank(i)
        ir = receiver_rank_index(r)
        iexch = exchange_index(ig,ir,ng)
        interp%donate(iexch)%ndonors = interp%donate(iexch)%ndonors + 1
        interp%donate(iexch)%npoints_max = interp%local_donors(ng)%nstencil_max**input%ND
        interp%donate(iexch)%donor_grid = m
        interp%donate(iexch)%donor_grid_local = ng
        interp%donate(iexch)%receiver_grid = n
        interp%donate(iexch)%receiver_rank = r
      end do
    end do

    do iexch = 1, interp%ndonates
      m = interp%donate(iexch)%donor_grid
      allocate(interp%donate(iexch)%npoints(interp%donate(iexch)%ndonors))
      allocate(interp%donate(iexch)%local_grid_point_index(interp%donate(iexch)%npoints_max, &
        interp%donate(iexch)%ndonors))
      allocate(interp%donate(iexch)%weight(interp%donate(iexch)%npoints_max, &
        interp%donate(iexch)%ndonors))
    end do

    allocate(idonor(interp%ndonates))
    idonor = 1

    do ng = 1, region%nGrids
      grid => region%grid(ng)
      npoints_local = grid%ie - grid%is + 1
      do i = 1, interp%local_donors(ng)%ndonors
        ig = receiver_grid_index(interp%local_donors(ng)%receiver_grid(i))
        ir = receiver_rank_index(interp%local_donors(ng)%receiver_rank(i))
        iexch = exchange_index(ig,ir,ng)
        interp%donate(iexch)%npoints(idonor(iexch)) = 0
        lower_point = interp%local_donors(ng)%lower_point(:,i)
        upper_point = interp%local_donors(ng)%upper_point(:,i)
        ipoint = 1
        do r = lower_point(3), upper_point(3)
          do q = lower_point(2), upper_point(2)
            do p = lower_point(1), upper_point(1)
              point = [p,q,r]
              point = periodic_adjust(grid%GlobalSize, grid%periodic, grid%periodicStorage, point)
              if (all(point >= grid%is_unique) .and. all(point <= grid%ie_unique)) then
                l = (point(3) - grid%is(3)) * npoints_local(1) * npoints_local(2) + &
                  (point(2) - grid%is(2)) * npoints_local(1) + (point(1) - grid%is(1) + 1)
                interp%donate(iexch)%local_grid_point_index(ipoint,idonor(iexch)) = l
                select case (input%ND)
                case (2)
                  interp%donate(iexch)%weight(ipoint,idonor(iexch)) = &
                    interp%local_donors(ng)%coef(p-lower_point(1)+1,1,i)* &
                    interp%local_donors(ng)%coef(q-lower_point(2)+1,2,i)
                case (3)
                  interp%donate(iexch)%weight(ipoint,idonor(iexch)) = &
                    interp%local_donors(ng)%coef(p-lower_point(1)+1,1,i)* &
                    interp%local_donors(ng)%coef(q-lower_point(2)+1,2,i)* &
                    interp%local_donors(ng)%coef(r-lower_point(3)+1,3,i)
                end select
                ipoint = ipoint + 1
              end if
            end do
          end do
        end do
        interp%donate(iexch)%npoints(idonor(iexch)) = ipoint - 1
        idonor(iexch) = idonor(iexch) + 1
      end do
    end do

    call MPI_Barrier(mycomm, ierr)
    timer2 = MPI_Wtime()
    region%mpi_timings(:)%interp_setup = region%mpi_timings(:)%interp_setup + (timer2 - timer1)

    if (region%myrank == 0) THEN
       write (*, '(a)') "PlasComCM: Done setting up donor-side exchanges."
    end if

  end subroutine Setup_Donate_Exchanges

  subroutine Cleanup_Interpolation_Setup_Data(region)

    USE ModGlobal
    USE ModDataStruct
    USE ModMPI

    IMPLICIT NONE

    ! ... function call variables
    TYPE (t_region), POINTER :: region

    ! ... local variables
    type (t_interp), pointer :: interp
    real(rfreal) :: timer1, timer2
    integer :: ierr

    interp => region%interp

    call MPI_Barrier(mycomm, ierr)
    timer1 = MPI_Wtime()

    if (any(region%grid_to_processor == region%myrank)) then
      deallocate(interp%global_receivers)
      deallocate(interp%global_donors)
    end if

    call MPI_Barrier(mycomm, ierr)
    timer2 = MPI_Wtime()
    region%mpi_timings(:)%interp_setup = region%mpi_timings(:)%interp_setup + (timer2 - timer1)

  end subroutine Cleanup_Interpolation_Setup_Data

  subroutine Exchange_Interpolated_Data(region, ptrs_to_source_data, ptrs_to_interpolated_data)

    USE ModGlobal
    USE ModDataStruct
    USE ModMPI

    implicit none

    ! ... function call variables
    type(t_region), pointer :: region
    type(t_data_ptr) :: ptrs_to_source_data(:)
    type(t_data_ptr) :: ptrs_to_interpolated_data(:)

    ! ... local variables
    type(t_mixt_input), pointer :: input
    type(t_interp), pointer :: interp
    type(t_grid), pointer :: grid
    type(t_mixt), pointer :: state
    type(t_electric), pointer :: electric
    real(rfreal) :: timer1, timer2
    integer :: i, j, l, m, n, r
    integer :: ng
    integer :: nvars
    integer :: iexch, jexch
    integer :: m_donate, m_receive, n_donate, n_receive
    integer :: ierr
    integer, allocatable :: requests(:)

    CALL MPI_Barrier(mycomm,ierr)
    timer1 = MPI_WTime()

    ! ... simplicity
    interp => region%interp

    nvars = size(ptrs_to_source_data(1)%ptr, dim=2)

    ! Allocate buffers
    do iexch = 1, interp%nreceives
      allocate(interp%receive(iexch)%buffer(nvars,interp%receive(iexch)%nreceivers))
    end do
    do iexch = 1, interp%ndonates
      allocate(interp%donate(iexch)%buffer(nvars,interp%donate(iexch)%ndonors))
    end do

    ! Allocate requests
    allocate(requests(interp%nreceives + interp%ndonates))
    requests = MPI_REQUEST_NULL

    ! Initiate receives (no-op if receiving from same rank)
    do iexch = 1, interp%nreceives
      n = interp%receive(iexch)%receiver_grid
      m = interp%receive(iexch)%donor_grid
      r = interp%receive(iexch)%donor_rank
      if (r /= region%myrank) then
        call MPI_Irecv(interp%receive(iexch)%buffer, nvars*interp%receive(iexch)%nreceivers, &
          MPI_REAL8, r, n, mycomm, requests(iexch), ierr)
      end if
    end do

    ! Interpolate data and pack it into donate buffers
    do iexch = 1, interp%ndonates
      m = interp%donate(iexch)%donor_grid
      ng = interp%donate(iexch)%donor_grid_local
      n = interp%donate(iexch)%receiver_grid
      r = interp%donate(iexch)%receiver_rank
      do i = 1, interp%donate(iexch)%ndonors
        interp%donate(iexch)%buffer(:,i) = 0._rfreal
        do j = 1, interp%donate(iexch)%npoints(i)
          l = interp%donate(iexch)%local_grid_point_index(j,i)
          interp%donate(iexch)%buffer(:,i) = interp%donate(iexch)%buffer(:,i) + &
            interp%donate(iexch)%weight(j,i) * ptrs_to_source_data(ng)%ptr(l,:)
        end do
      end do
    end do

    ! Initiate sends (no-op if donating to same rank)
    do iexch = 1, interp%ndonates
      m = interp%donate(iexch)%donor_grid
      n = interp%donate(iexch)%receiver_grid
      r = interp%donate(iexch)%receiver_rank
      if (r /= region%myrank) then
        call MPI_Isend(interp%donate(iexch)%buffer, nvars*interp%donate(iexch)%ndonors, &
          MPI_REAL8, r, n, mycomm, requests(interp%nreceives + iexch), ierr)
      end if
    end do

    ! For exchanges on the same rank, just copy the send buffer to the receive buffer
    do iexch = 1, interp%ndonates
      if (interp%donate(iexch)%receiver_rank == region%myrank) then
        m_donate = interp%donate(iexch)%donor_grid
        n_donate = interp%donate(iexch)%receiver_grid
        do jexch = 1, interp%nreceives
          if (interp%receive(jexch)%donor_rank == region%myrank) then
            m_receive = interp%receive(jexch)%donor_grid
            n_receive = interp%receive(jexch)%receiver_grid
            if (m_donate == m_receive .and. n_donate == n_receive) then
              interp%receive(jexch)%buffer = interp%donate(iexch)%buffer
              exit
            end if
          end if
        end do
      end if
    end do

    ! Complete the exchanges
    call MPI_Waitall(interp%nreceives + interp%ndonates, requests, MPI_STATUSES_IGNORE, ierr)

    ! Unpack interpolated data
    do ng = 1, region%nGrids
      ptrs_to_interpolated_data(ng)%ptr = 0._rfreal
    end do
    do iexch = 1, interp%nreceives
      ng = interp%receive(iexch)%receiver_grid_local
      do i = 1, interp%receive(iexch)%nreceivers
        j = interp%receive(iexch)%local_receiver_index(i)
        ptrs_to_interpolated_data(ng)%ptr(j,:) = ptrs_to_interpolated_data(ng)%ptr(j,:) + &
          interp%receive(iexch)%buffer(:,i)
      end do
    end do

    ! Deallocate requests
    deallocate(requests)

    ! Deallocate buffers
    do iexch = 1, interp%nreceives
      deallocate(interp%receive(iexch)%buffer)
    end do
    do iexch = 1, interp%ndonates
      deallocate(interp%donate(iexch)%buffer)
    end do

    call MPI_Barrier(mycomm, ierr)
    timer2 = MPI_Wtime()
    region%mpi_timings(:)%interp_volx = region%mpi_timings(:)%interp_volx + (timer2 - timer1)

  end subroutine Exchange_Interpolated_Data

  subroutine Setup_Interpolated_State(region)

    USE ModGlobal
    USE ModDataStruct

    implicit none

    ! ... function call variables
    type(t_region), pointer :: region

    ! ... local variables
    type(t_mixt_input), pointer :: input
    type(t_interp), pointer :: interp
    type(t_grid), pointer :: grid
    type(t_mixt), pointer :: state
    integer :: ng

    input => region%input
    interp => region%interp

    do ng = 1, region%nGrids
      grid => region%grid(ng)
      state => region%state(ng)
      allocate(state%cv_interp(interp%local_receivers(ng)%nreceivers,input%nCv))
      if (input%nAuxVars > 0) then
        allocate(state%auxVars_interp(interp%local_receivers(ng)%nreceivers,input%nAuxVars))
      end if
    end do

  end subroutine Setup_Interpolated_State

#ifdef BUILD_ELECTRIC
  subroutine Setup_Interpolated_Phi(region)

    USE ModGlobal
    USE ModDataStruct

    implicit none

    ! ... function call variables
    type(t_region), pointer :: region

    ! ... local variables
    type(t_mixt_input), pointer :: input
    type(t_interp), pointer :: interp
    type(t_grid), pointer :: grid
    type(t_electric), pointer :: electric
    integer :: ng

    input => region%input
    interp => region%interp

    do ng = 1, region%nGrids
      grid => region%grid(ng)
      electric => region%electric(ng)
      allocate(electric%phi_interp(interp%local_receivers(ng)%nreceivers,1))
    end do

  end subroutine Setup_Interpolated_Phi
#endif

  subroutine Exchange_Interpolated_State(region)

    USE ModGlobal
    USE ModDataStruct

    implicit none

    ! ... function call variables
    type(t_region), pointer :: region

    ! ... local variables
    type(t_mixt_input), pointer :: input
    type(t_interp), pointer :: interp
    type(t_grid), pointer :: grid
    type(t_mixt), pointer :: state
    integer :: i, l
    integer :: ng
    integer :: point(MAX_ND)
    integer :: npoints_local(MAX_ND)
    type(t_data_ptr), allocatable :: ptrs_to_source_data(:)
    type(t_data_ptr), allocatable :: ptrs_to_interpolated_data(:)

    input => region%input
    interp => region%interp

    allocate(ptrs_to_source_data(region%nGrids))
    allocate(ptrs_to_interpolated_data(region%nGrids))

    do ng = 1, region%nGrids
      state => region%state(ng)
      ptrs_to_source_data(ng)%ptr => state%cv
      ptrs_to_interpolated_data(ng)%ptr => state%cv_interp
    end do

    call Exchange_Interpolated_Data(region, ptrs_to_source_data, ptrs_to_interpolated_data)

    if (region%input%nAuxVars > 0) then

      do ng = 1, region%nGrids
        state => region%state(ng)
        ptrs_to_source_data(ng)%ptr => state%auxVars
        ptrs_to_interpolated_data(ng)%ptr => state%auxVars_interp
      end do

      call Exchange_Interpolated_Data(region, ptrs_to_source_data, ptrs_to_interpolated_data)

    end if

    do ng = 1, region%nGrids
      grid => region%grid(ng)
      state => region%state(ng)
      npoints_local = grid%ie - grid%is + 1
      do i = 1, interp%local_receivers(ng)%nreceivers
        point = interp%local_receivers(ng)%point(:,i)
        l = (point(3) - grid%is(3)) * npoints_local(1) * npoints_local(2) + &
          (point(2) - grid%is(2)) * npoints_local(1) + (point(1) - grid%is(1) + 1)
        state%cv(l,:) = state%cv_interp(i,:)
      end do
    end do

    if (region%input%nAuxVars > 0) then

      do ng = 1, region%nGrids
        grid => region%grid(ng)
        state => region%state(ng)
        npoints_local = grid%ie - grid%is + 1
        do i = 1, interp%local_receivers(ng)%nreceivers
          point = interp%local_receivers(ng)%point(:,i)
          l = (point(3) - grid%is(3)) * npoints_local(1) * npoints_local(2) + &
            (point(2) - grid%is(2)) * npoints_local(1) + (point(1) - grid%is(1) + 1)
          state%auxVars(l,:) = state%auxVars_interp(i,:)
        end do
      end do

    end if

  end subroutine Exchange_Interpolated_State

#ifdef BUILD_ELECTRIC
  subroutine Exchange_Interpolated_Phi(region)

    USE ModGlobal
    USE ModDataStruct

    implicit none

    ! ... function call variables
    type(t_region), pointer :: region

    ! ... local variables
    type(t_mixt_input), pointer :: input
    type(t_interp), pointer :: interp
    type(t_grid), pointer :: grid
    type(t_electric), pointer :: electric
    integer :: i, l
    integer :: ng
    integer :: point(MAX_ND)
    integer :: npoints_local(MAX_ND)
    type(t_data_ptr), allocatable :: ptrs_to_source_data(:)
    type(t_data_ptr), allocatable :: ptrs_to_interpolated_data(:)

    interp => region%interp

    allocate(ptrs_to_source_data(region%nGrids))
    allocate(ptrs_to_interpolated_data(region%nGrids))

    do ng = 1, region%nGrids
      electric => region%electric(ng)
      if (electric%grid_has_efield) then
        electric%phi_data_ptr(:,1) = electric%phi
      end if
      ptrs_to_source_data(ng)%ptr => electric%phi_data_ptr
      ptrs_to_interpolated_data(ng)%ptr => electric%phi_interp
    end do

    call Exchange_Interpolated_Data(region, ptrs_to_source_data, ptrs_to_interpolated_data)

    do ng = 1, region%nGrids
      grid => region%grid(ng)
      electric => region%electric(ng)
      npoints_local = grid%ie - grid%is + 1
      if (electric%grid_has_efield) then
        do i = 1, interp%local_receivers(ng)%nreceivers
          point = interp%local_receivers(ng)%point(:,i)
          l = (point(3) - grid%is(3)) * npoints_local(1) * npoints_local(2) + &
            (point(2) - grid%is(2)) * npoints_local(1) + (point(1) - grid%is(1) + 1)
          if (electric%interp_mask(l)) then
            electric%phi(l) = electric%phi_interp(i,1)
          end if
        end do
      end if
    end do

  end subroutine Exchange_Interpolated_Phi
#endif

  subroutine batch_divide(npoints, nbatches, batch_size, batch_start, batch_end)

    implicit none

    integer, intent(in) :: npoints
    integer, intent(in) :: nbatches
    integer, intent(out) :: batch_size(:)
    integer, intent(out) :: batch_start(:), batch_end(:)

    integer :: i

    if (nbatches == 0) then
      return
    end if

    do i = 1, nbatches
      batch_size(i) = npoints/nbatches + merge(1, 0, i <= mod(npoints, nbatches))
    end do

    batch_start(1) = 1
    do i = 1, nbatches-1
      batch_end(i) = batch_start(i) + batch_size(i) - 1
      batch_start(i+1) = batch_end(i) + 1
    end do
    batch_end(nbatches) = npoints

  end subroutine batch_divide

  function periodic_adjust(grid_size, grid_periodic, grid_periodic_storage, point) &
    result(periodic_point)

    USE ModGlobal

    implicit none

    integer, intent(in) :: grid_size(MAX_ND)
    integer, intent(in) :: grid_periodic(MAX_ND)
    integer, intent(in) :: grid_periodic_storage
    integer, intent(in) :: point(MAX_ND)
    integer :: periodic_point(MAX_ND)

    integer :: grid_period(MAX_ND)

    grid_period = merge(grid_size-1, grid_size, grid_periodic >= TRUE .and. &
      grid_periodic_storage == OVERLAP_PERIODIC)
    periodic_point = merge(modulo(point-1, grid_period)+1, point, grid_periodic >= TRUE)

  end function periodic_adjust

  function overlaps_local(grid, range_lower, range_upper) result(overlaps)

    USE ModDataStruct
    USE ModGlobal

    type(t_grid), intent(in) :: grid
    integer, intent(in) :: range_lower(MAX_ND), range_upper(MAX_ND)
    logical :: overlaps

    integer :: i, j, k
    integer :: grid_period(MAX_ND)
    integer :: point(MAX_ND)

    grid_period = merge(grid%GlobalSize-1, grid%GlobalSize, grid%periodic >= TRUE .and. &
      grid%periodicStorage == OVERLAP_PERIODIC)

    if (any(range_upper > grid_period)) then
      overlaps = .false.
      do k = range_lower(3), range_upper(3)
        do j = range_lower(2), range_upper(2)
          do i = range_lower(1), range_upper(1)
            point = [i,j,k]
            point = periodic_adjust(grid%GlobalSize, grid%periodic, grid%periodicStorage, &
              point)
            if (all(point >= grid%is_unique) .and. all(point <= grid%ie_unique)) then
              overlaps = .true.
            end if
          end do
        end do
      end do
    else
      overlaps = all(range_upper >= grid%is_unique) .and. all(range_lower <= grid%ie_unique)
    end if

  end function overlaps_local

  integer function nggrid(IBCVAL, iisptr, iieptr, ipbp, ngrd)

    implicit none

    integer, intent(in) :: ibcval, ipbp, ngrd
    integer, intent(in) :: iisptr(:), iieptr(:)

    ! ... local variables
    integer :: j, nmax

    DO J=1,NGRD-1
      IF( IBCVAL.LT.IISPTR(J+1).AND.IBCVAL.GE.IISPTR(J) ) THEN
        NGGRID = J
        RETURN
      ENDIF
    ENDDO

    NMAX = IPBP*NGRD

    IF( IBCVAL.LE.NMAX.AND.IBCVAL.GE.IISPTR(NGRD) ) THEN
      NGGRID = NGRD
    ELSE
      STOP 'Failed in NGGRID().'
    ENDIF

    RETURN
  END function nggrid

  subroutine Read_XINTOUT(region, HOfname, Xfname, xintout)

    USE ModGlobal
    USE ModDataStruct
    USE ModMPI

    IMPLICIT NONE

    ! ... function call variables
    TYPE (t_region), POINTER :: region
    CHARACTER(len=80) :: HOfname, Xfname
    type (t_xintout) :: xintout

    ! ... local variables
    type (t_mixt_input), pointer :: input
    integer :: i
    integer :: io
    integer :: ng
    integer :: io_err
    integer :: max_i, max_j, max_k
    real(rfreal) :: junkr
    integer :: junki
    integer :: ih(3), ihx, idir, ijk

    INTEGER, PARAMETER :: xintout_ho_unit = 50
    INTEGER, PARAMETER :: xintout_x_unit = 40

    IF (region%myrank == 0) THEN
       WRITE(*,'(A)') 'PlasComCM: Reading XINTOUT.HO file "'//TRIM(HOfname)//'"'
    ENDIF

    input => region%input

    OPEN (unit=xintout_ho_unit, file=HOfname(1:LEN_TRIM(HOfname)), form='unformatted', status='old', &
      iostat=io_err)
    IF (io_err /= 0) THEN
      CALL graceful_exit(region%myrank, 'ERROR: Could not find/open XINTOUT.HO file "' // &
        TRIM(HOfname) // '".')
    ENDIF

    READ (xintout_ho_unit) xintout%ngrd, xintout%ipall, xintout%igall, xintout%ipip, xintout%ipbp
    IF (xintout%ngrd /= region%nGridsGlobal) &
      CALL graceful_exit(region%myrank, 'ERROR: '//TRIM(HOfname)//' has ngrd /= region%nGridsGlobal')

    ! ... allocate memory for global pointers
    ALLOCATE(xintout%iipnts(xintout%ngrd))
    ALLOCATE(xintout%ibpnts(xintout%ngrd))
    ALLOCATE(xintout%iisptr(xintout%ngrd))
    ALLOCATE(xintout%iieptr(xintout%ngrd))
    ALLOCATE(xintout%ieng(xintout%ngrd))
    ALLOCATE(xintout%jeng(xintout%ngrd))
    ALLOCATE(xintout%keng(xintout%ngrd))

    ! ... allocate memory for receiver information
    ALLOCATE(xintout%ibt(xintout%ipbp,xintout%ngrd))
    ALLOCATE(xintout%jbt(xintout%ipbp,xintout%ngrd))
    ALLOCATE(xintout%kbt(xintout%ipbp,xintout%ngrd))
    ALLOCATE(xintout%ibct(xintout%ipbp,xintout%ngrd))

    ! ... allocate memory for donor information
    ALLOCATE(xintout%iit(xintout%ipip,xintout%ngrd))
    ALLOCATE(xintout%jit(xintout%ipip,xintout%ngrd))
    ALLOCATE(xintout%kit(xintout%ipip,xintout%ngrd))
    ALLOCATE(xintout%dxit(xintout%ipip,xintout%ngrd))
    ALLOCATE(xintout%dyit(xintout%ipip,xintout%ngrd))
    ALLOCATE(xintout%dzit(xintout%ipip,xintout%ngrd))

    DO ng = 1, xintout%ngrd

      READ (xintout_ho_unit) xintout%ibpnts(ng), xintout%iipnts(ng), xintout%iieptr(ng), &
       xintout%iisptr(ng), xintout%ieng(ng), xintout%jeng(ng), xintout%keng(ng)

      READ (xintout_ho_unit) &
        ( xintout%iit(i,ng), i = 1, xintout%iipnts(ng)), &
        ( xintout%jit(i,ng), i = 1, xintout%iipnts(ng)), &
        ( xintout%kit(i,ng), i = 1, xintout%iipnts(ng)), &
        (xintout%dxit(i,ng), i = 1, xintout%iipnts(ng)), &
        (xintout%dyit(i,ng), i = 1, xintout%iipnts(ng)), &
        (xintout%dzit(i,ng), i = 1, xintout%iipnts(ng))

      READ (xintout_ho_unit) &
        ( xintout%ibt(i,ng), i = 1, xintout%ibpnts(ng)), &
        ( xintout%jbt(i,ng), i = 1, xintout%ibpnts(ng)), &
        ( xintout%kbt(i,ng), i = 1, xintout%ibpnts(ng)), &
        (xintout%ibct(i,ng), i = 1, xintout%ibpnts(ng))

      ! ... IBLANK data (unused)
      IF(input%xintout_has_iblank) THEN
        READ(xintout_ho_unit) (junki, i = 1, xintout%ieng(ng)*xintout%jeng(ng)*xintout%keng(ng))
      ENDIF

    ENDDO

    CLOSE (xintout_ho_unit)

    IF(region%myrank == 0) THEN
       WRITE(*,'(A)') 'PlasComCM: Done reading XINTOUT.HO file "'//TRIM(HOfname)//'"'
    ENDIF

    IF (region%myrank == 0) THEN
       WRITE(*,'(A)') 'PlasComCM: Reading XINTOUT.X file "'//TRIM(Xfname)//'"'
    ENDIF

    OPEN (unit=xintout_x_unit, file=Xfname(1:LEN_TRIM(Xfname)), form='unformatted', status='old', &
      iostat=io_err)
    IF (io_err /= 0) THEN
      CALL graceful_exit(region%myrank, 'ERROR: Could not find/open XINTOUT.X file "' // &
        TRIM(Xfname) // '".')
    ENDIF

    ! Skip over the header (already read from HO file)
    READ (xintout_x_unit) junki, junki, junki, junki, junki

    ALLOCATE(xintout%nit(xintout%ipip,xintout%ngrd))
    ALLOCATE(xintout%njt(xintout%ipip,xintout%ngrd))
    ALLOCATE(xintout%nkt(xintout%ipip,xintout%ngrd))

    DO ng = 1, xintout%ngrd

      ! ... read stencil size
      READ (xintout_x_unit) &
        (xintout%nit(i,ng), i = 1, xintout%iipnts(ng)), &
        (xintout%njt(i,ng), i = 1, xintout%iipnts(ng)), &
        (xintout%nkt(i,ng), i = 1, xintout%iipnts(ng))

      READ (xintout_x_unit) &
        ((junkr, io = 1, xintout%nit(i,ng)), i = 1, xintout%iipnts(ng)), &
        ((junkr, io = 1, xintout%njt(i,ng)), i = 1, xintout%iipnts(ng)), &
        ((junkr, io = 1, xintout%nkt(i,ng)), i = 1, xintout%iipnts(ng))

      READ (xintout_x_unit) ih(1), ih(2), ih(3)
      IF (ANY(ih(:) .GT. 0)) THEN
        READ (xintout_x_unit) &
          (((junki,ihx=1,ih(idir)),ijk=1,3),idir=1,3), &
          (((junki,ihx=1,ih(idir)),ijk=1,3),idir=1,3)
      ENDIF

    ENDDO ! ng

    max_i = 0
    max_j = 0
    max_k = 0
    DO ng = 1, xintout%ngrd
      max_i = MAX(max_i, MAXVAL(xintout%nit(:xintout%iipnts(ng),ng)))
      max_j = MAX(max_j, MAXVAL(xintout%njt(:xintout%iipnts(ng),ng)))
      max_k = MAX(max_k, MAXVAL(xintout%nkt(:xintout%iipnts(ng),ng)))
    END DO

    ALLOCATE(xintout%coeffit(xintout%ipip,max(max_i,max_j,max_k),MAX_ND,xintout%ngrd))

    ! Now need to re-read the file for interpolation coefficients
    REWIND(xintout_x_unit)

    ! Skip the header again
    READ (xintout_x_unit) junki, junki, junki, junki, junki

    DO ng = 1, xintout%ngrd

      READ (xintout_x_unit) &
        (junki, i = 1, xintout%iipnts(ng)), &
        (junki, i = 1, xintout%iipnts(ng)), &
        (junki, i = 1, xintout%iipnts(ng))

      ! ... read stencil coefficients
      READ (xintout_x_unit) &
        ((xintout%coeffit(i,io,1,ng), io = 1, xintout%nit(i,ng)), i = 1, xintout%iipnts(ng)), &
        ((xintout%coeffit(i,io,2,ng), io = 1, xintout%njt(i,ng)), i = 1, xintout%iipnts(ng)), &
        ((xintout%coeffit(i,io,3,ng), io = 1, xintout%nkt(i,ng)), i = 1, xintout%iipnts(ng))

      READ (xintout_x_unit) ih(1), ih(2), ih(3)
      IF (ANY(ih(:) .GT. 0)) THEN
        READ (xintout_x_unit) &
          (((junki,ihx=1,ih(idir)),ijk=1,3),idir=1,3), &
          (((junki,ihx=1,ih(idir)),ijk=1,3),idir=1,3)
      ENDIF

    END DO ! ng

    CLOSE (xintout_x_unit)

    IF (region%myrank == 0) WRITE(*,'(A)') 'PlasComCM: Done reading XINTOUT.X file "'//TRIM(Xfname)//'"'

  end subroutine Read_XINTOUT

  subroutine Read_XINTOUT_OGEN(region, fname, xintout)

    USE ModGlobal
    USE ModDataStruct
    USE ModMPI

    implicit none

    ! ... function call variables
    type (t_region), pointer :: region
    character(len=80) :: fname
    type (t_xintout) :: xintout

    ! ... local variables
    type (t_mixt_input), pointer :: input
    integer :: i
    integer :: ng
    integer :: io_err
    integer :: nstencil_max
    real(rfreal) :: junkr
    integer :: junki
    real(rfreal) :: xi

    integer, parameter :: xintout_ho_unit = 50

    if (region%myrank == 0) write(*,'(A)') 'PlasComCM: Reading XINTOUT_OGEN file "'//trim(fname)//'"'

    Open (unit=xintout_ho_unit, file=fname(1:len_trim(fname)), form='unformatted', status='old', &
      iostat=io_err)
    if (io_err /= 0) then
      call graceful_exit(region%myrank, 'ERROR: Could not find/open XINTOUT_OGEN file "' // &
        trim(fname) // '".')
    end if

    input => region%input

    Read (xintout_ho_unit) xintout%ngrd, xintout%ipall, xintout%igall, xintout%ipip, xintout%ipbp

    If (xintout%ngrd /= region%nGridsGlobal) &
      Call graceful_exit(region%myrank, &
        'PlasComCM: ERROR: '//trim(fname)//' has ngrd /= region%nGridsGlobal')

    ! ... allocate memory for global pointers
    allocate(xintout%iipnts(xintout%ngrd))
    allocate(xintout%ibpnts(xintout%ngrd))
    allocate(xintout%iisptr(xintout%ngrd))
    allocate(xintout%iieptr(xintout%ngrd))
    allocate(xintout%ieng(xintout%ngrd))
    allocate(xintout%jeng(xintout%ngrd))
    allocate(xintout%keng(xintout%ngrd))

    ! ... allocate memory for receiver information
    allocate(xintout%ibt(xintout%ipbp,xintout%ngrd))
    allocate(xintout%jbt(xintout%ipbp,xintout%ngrd))
    allocate(xintout%kbt(xintout%ipbp,xintout%ngrd))
    allocate(xintout%ibct(xintout%ipbp,xintout%ngrd))

    ! ... allocate memory for donor information
    allocate(xintout%iit(xintout%ipip,xintout%ngrd))
    allocate(xintout%jit(xintout%ipip,xintout%ngrd))
    allocate(xintout%kit(xintout%ipip,xintout%ngrd))
    allocate(xintout%dxit(xintout%ipip,xintout%ngrd))
    allocate(xintout%dyit(xintout%ipip,xintout%ngrd))
    allocate(xintout%dzit(xintout%ipip,xintout%ngrd))

    Do ng = 1, xintout%ngrd

      Read (xintout_ho_unit) xintout%ibpnts(ng), xintout%iipnts(ng), xintout%iieptr(ng), &
        xintout%iisptr(ng), xintout%ieng(ng), xintout%jeng(ng), xintout%keng(ng)

      Read (xintout_ho_unit) ( xintout%iit(i,ng), i = 1, xintout%iipnts(ng)), &
                             ( xintout%jit(i,ng), i = 1, xintout%iipnts(ng)), &
                             ( xintout%kit(i,ng), i = 1, xintout%iipnts(ng)), &
                             (xintout%dxit(i,ng), i = 1, xintout%iipnts(ng)), &
                             (xintout%dyit(i,ng), i = 1, xintout%iipnts(ng)), &
                             (xintout%dzit(i,ng), i = 1, xintout%iipnts(ng))

      ! ... read receiver information
      Read (xintout_ho_unit) ( xintout%ibt(i,ng), i = 1, xintout%ibpnts(ng)), &
                             ( xintout%jbt(i,ng), i = 1, xintout%ibpnts(ng)), &
                             ( xintout%kbt(i,ng), i = 1, xintout%ibpnts(ng)), &
                             (xintout%ibct(i,ng), i = 1, xintout%ibpnts(ng))

      ! ... IBLANK data (unused)
      IF(input%xintout_has_iblank) THEN
        READ(xintout_ho_unit) (junki, i = 1, xintout%ieng(ng)*xintout%jeng(ng)*xintout%keng(ng))
      ENDIF

    End Do

    Close (xintout_ho_unit)

    if (region%myrank == 0) write(*,'(A)') 'PlasComCM: Done reading XINTOUT_OGEN file "'//trim(fname)//'"'

    ! ... now work on filling in the data that is not provided by the XINTOUT_OGEN file

    select case (input%OgenOrder)
    case (2)
      nstencil_max = 3
    case (4)
      nstencil_max = 5
    end select

    allocate(xintout%nit(xintout%ipip,xintout%ngrd))
    allocate(xintout%njt(xintout%ipip,xintout%ngrd))
    allocate(xintout%nkt(xintout%ipip,xintout%ngrd))
    allocate(xintout%coeffit(xintout%ipip,nstencil_max,MAX_ND,xintout%ngrd))

    Do ng = 1, xintout%ngrd

      ! ... default for quadratic interpolation
      If (input%OgenOrder == 2) Then
        Do i = 1, xintout%iipnts(ng)
          xintout%nit(i,ng) = 3
          xintout%njt(i,ng) = 3
          if (input%ND == 2) then
            xintout%nkt(i,ng) = 1
          else
            xintout%nkt(i,ng) = 3
          end if
        End Do
      Else If (input%OgenOrder == 4) Then
        Do i = 1, xintout%iipnts(ng)
          xintout%nit(i,ng) = 5
          xintout%njt(i,ng) = 5
          if (input%ND == 2) then
            xintout%nkt(i,ng) = 1
          else
            xintout%nkt(i,ng) = 5
          end if
        End Do
      End If

      ! ... compute the stencil coefficients from (dxit, dyit, dzit)
      If (input%OgenOrder == 2) Then
        Do i = 1, xintout%iipnts(ng)

          ! ... x-interpolation
          xi = xintout%dxit(i,ng)
          xintout%coeffit(i,1,1,ng) = 0.5_8 * (xi - 1.0_8) * (xi - 2.0_8)
          xintout%coeffit(i,2,1,ng) = xi * (2.0_8 - xi)
          xintout%coeffit(i,3,1,ng) = 0.5_8 * xi * (xi - 1.0_8)

          ! ... y-interpolation
          xi = xintout%dyit(i,ng)
          xintout%coeffit(i,1,2,ng) = 0.5_8 * (xi - 1.0_8) * (xi - 2.0_8)
          xintout%coeffit(i,2,2,ng) = xi * (2.0_8 - xi)
          xintout%coeffit(i,3,2,ng) = 0.5_8 * xi * (xi - 1.0_8)

          ! ... z-interpolation
          if (input%ND == 3) then
            xi = xintout%dzit(i,ng)
            xintout%coeffit(i,1,3,ng) = 0.5_8 * (xi - 1.0_8) * (xi - 2.0_8)
            xintout%coeffit(i,2,3,ng) = xi * (2.0_8 - xi)
            xintout%coeffit(i,3,3,ng) = 0.5_8 * xi * (xi - 1.0_8)
          else
            xintout%coeffit(i,1,3,ng) = 1.0_8
            xintout%coeffit(i,2,3,ng) = 1.0_8
            xintout%coeffit(i,3,3,ng) = 1.0_8
          end if

        End Do
      Else If (input%OgenOrder == 4) Then
        Do i = 1, xintout%iipnts(ng)

          ! ... x-interpolation
          xi = xintout%dxit(i,ng)
          xintout%coeffit(i,1,1,ng) = 1.0_8/24.0_8*(xi-1.0_8)*(xi-2.0_8)*(xi-3.0_8)*(xi-4.0_8)
          xintout%coeffit(i,2,1,ng) = -1.0_8/6.0_8*xi*(xi-2.0_8)*(xi-3.0_8)*(xi-4.0_8)
          xintout%coeffit(i,3,1,ng) = 0.25_8*xi*(xi-1.0_8)*(xi-3.0_8)*(xi-4.0_8)
          xintout%coeffit(i,4,1,ng) = -1.0_8/6.0_8*xi*(xi-1.0_8)*(xi-2.0_8)*(xi-4.0_8)
          xintout%coeffit(i,5,1,ng) = 1.0_8/24.0_8*xi*(xi-1.0_8)*(xi-2.0_8)*(xi-3.0_8)

          ! ... y-interpolation
          xi = xintout%dyit(i,ng)
          xintout%coeffit(i,1,2,ng) = 1.0_8/24.0_8*(xi-1.0_8)*(xi-2.0_8)*(xi-3.0_8)*(xi-4.0_8)
          xintout%coeffit(i,2,2,ng) = -1.0_8/6.0_8*xi*(xi-2.0_8)*(xi-3.0_8)*(xi-4.0_8)
          xintout%coeffit(i,3,2,ng) = 0.25_8*xi*(xi-1.0_8)*(xi-3.0_8)*(xi-4.0_8)
          xintout%coeffit(i,4,2,ng) = -1.0_8/6.0_8*xi*(xi-1.0_8)*(xi-2.0_8)*(xi-4.0_8)
          xintout%coeffit(i,5,2,ng) = 1.0_8/24.0_8*xi*(xi-1.0_8)*(xi-2.0_8)*(xi-3.0_8)

          ! ... z-interpolation
          if (input%ND == 3) then
            xi = xintout%dzit(i,ng)
            xintout%coeffit(i,1,3,ng) = 1.0_8/24.0_8*(xi-1.0_8)*(xi-2.0_8)*(xi-3.0_8)*(xi-4.0_8)
            xintout%coeffit(i,2,3,ng) = -1.0_8/6.0_8*xi*(xi-2.0_8)*(xi-3.0_8)*(xi-4.0_8)
            xintout%coeffit(i,3,3,ng) = 0.25_8*xi*(xi-1.0_8)*(xi-3.0_8)*(xi-4.0_8)
            xintout%coeffit(i,4,3,ng) = -1.0_8/6.0_8*xi*(xi-1.0_8)*(xi-2.0_8)*(xi-4.0_8)
            xintout%coeffit(i,5,3,ng) = 1.0_8/24.0_8*xi*(xi-1.0_8)*(xi-2.0_8)*(xi-3.0_8)
          else
            xintout%coeffit(i,1,3,ng) = 1.0_8
            xintout%coeffit(i,2,3,ng) = 1.0_8
            xintout%coeffit(i,3,3,ng) = 1.0_8
            xintout%coeffit(i,4,3,ng) = 1.0_8
            xintout%coeffit(i,5,3,ng) = 1.0_8
          end if

        End Do

      End If

    End Do

  End Subroutine Read_XINTOUT_OGEN

  Subroutine TransfiniteInterp(region)

    USE ModGlobal
    USE ModDataStruct
    USE ModMPI
    
    Implicit None

    Type(t_region), pointer :: region
    
    ! ... local variables
    Type(t_grid), pointer :: grid
    Type(t_patch), pointer :: patch
    Type(t_mixt_input), pointer :: input
    Real(rfreal), pointer :: dXBnd(:,:), dXYZ(:,:), dXYZ2(:,:), dXYZ3(:,:), dXBnd2(:,:), dXBnd3(:,:)
    Real(rfreal), pointer :: dXBnd_TAU(:,:), dXYZ_TAU(:,:), dXYZ2_TAU(:,:), dXYZ3_TAU(:,:), dXBnd_TAU2(:,:), dXBnd_TAU3(:,:)
    Integer :: N(MAX_ND), Np(MAX_ND), Nvds(MAX_ND)
    Integer :: i, j, k, dir, l0, lp, l1
    Integer :: ng, npatch, normDir, sgn, faceNum, ND, Nc, ierr

    ! ... loop through the grids
    do ng = 1, region%nGrids
       
       ! ... simplicity
       grid => region%grid(ng)
       ND   =  grid%ND
       Nc   =  grid%nCells

       ! ... grid size
       N(:) = 1
       do dir = 1, ND 
          N(dir) = grid%ie(dir) - grid%is(dir) + 1
       end do
       
       ! ... allocate space for boundary movement data
       Allocate(dXBnd(maxval(grid%vds(:))*ND,ND*2))
       Allocate(dXBnd_TAU(maxval(grid%vds(:))*ND,ND*2))

       ! ... allocate space for updated boundary data
       if(ND >= 2) then
          Allocate(dXBnd2(grid%vds(2)*ND,2))
          Allocate(dXBnd_TAU2(grid%vds(2)*ND,2))
       end if

       if(ND == 3) then
          Allocate(dXBnd3(grid%vds(3)*ND,2))
          Allocate(dXBnd_TAU3(grid%vds(3)*ND,2))
       end if

       
       ! ... initialize
       do i = 1, maxval(grid%vds(:))*ND
          do j = 1, ND*2
             dXBnd(i,j) = 0.0_rfreal
             dXBnd_TAU(i,j) = 0.0_rfreal
          end do
       end do
       
       ! ... loop through patches to get data from moving boundaries
       do npatch = 1, region%nPatches
          
          ! ... simplicity
          patch   => region%patch(npatch)
          normDir =  ABS(patch%normDir)
          sgn     =  normDir / patch%normDir
          
          ! ... partition face number
          faceNum = 1
          if(sgn < 0) faceNum = 2
          faceNum = faceNum + (normDir - 1)*2

          Np(:) = 1
          do dir = 1, ND
             Np(dir) = patch%ie(dir) - patch%is(dir) + 1
          end do

          Nvds = N
          Nvds(normDir) = 1

          ! ... only visit patches on this grid
          if(ng /= patch%GridID) cycle
          
          ! ... only concerned with moving boundaries
          if(patch%BCType /= STRUCTURAL_INTERACTING) cycle
          
          do k = patch%is(3), patch%ie(3)
             do j = patch%is(2), patch%ie(2) 
                do i = patch%is(1), patch%ie(1)

                   ! ... patch index
                   lp = (k-patch%is(3))*Np(2)*Np(1) + (j-patch%is(2))*Np(1) + (i-patch%is(1)) + 1

                   ! ... grid face index
                   l1 = (k-grid%is(3))*Nvds(2)*Nvds(1) + (j-grid%is(2))*Nvds(1) + (i-grid%is(1)) + 1

                   ! ... grid index
                   l0 = (k-grid%is(3))*N(2)*N(1) + (j-grid%is(2))*N(1) + (i-grid%is(1)) + 1
                   
                   do dir = 1, ND
                      ! ... store moving boundary data

                      dXBnd((l1-1)*ND + dir, faceNum) = patch%dXYZ(lp,dir)
                      dXBND_TAU((l1-1)*ND + dir, faceNum) = patch%dXYZ_TAU(lp,dir)
                      !if(region%myrank == 1) write(*,'(8(I3,x),dXBnd)')i,j,k,dir,l0,lp,(l1-1)*ND + dir,faceNum,dXBnd((l1-1)*ND + dir, faceNum)
                   end do
                   
                end do ! ... i 
             end do ! ... j
          end do ! ... k
       end do ! ... npatch

       Call MPI_BARRIER(mycomm, ierr)

       ! ... use pencil communicators to communicate moving boundary data
       do dir = 1, ND
          if(grid%numproc_inPencilComm(dir) > 1) then
             
             faceNum = (dir-1)*2+1
             ! ... broadcast the data right from the first process in this pencil
             Call MPI_BCAST(dXBnd(1,faceNum), grid%vds(dir)*ND, MPI_DOUBLE_PRECISION, 0, grid%PencilComm(dir), ierr)
             
             ! ... broadcast the velocity data right from the first process in this pencil
             Call MPI_BCAST(dXBnd_TAU(1,faceNum), grid%vds(dir)*ND, MPI_DOUBLE_PRECISION, 0, grid%PencilComm(dir), ierr)

             faceNum = (dir-1)*2+2
             ! ... broadcast the data left from the last process in this pencil
             Call MPI_BCAST(dXBnd(1,faceNum), grid%vds(dir)*ND, MPI_DOUBLE_PRECISION, grid%numproc_inPencilComm(dir)-1, grid%PencilComm(dir), ierr)

             ! ... broadcast the velocity data left from the last process in this pencil
             Call MPI_BCAST(dXBnd_TAU(1,faceNum), grid%vds(dir)*ND, MPI_DOUBLE_PRECISION, grid%numproc_inPencilComm(dir)-1, grid%PencilComm(dir), ierr)

          end if
       end do ! ... dir

       ! ... now every process has the data needed to move the grid via transfinite interpolation
       ! ... grid point movement for every grid point
       allocate(dXYZ(Nc,ND),dXYZ_TAU(Nc,ND))
       if(ND >= 2) allocate(dXYZ2(Nc,ND),dXYZ2_TAU(Nc,ND))
       if(ND == 3) allocate(dXYZ3(Nc,ND),dXYZ3_TAU(Nc,ND))

       ! ... compute the displacements along direction 1
       normDir = 1
       faceNum = (normDir-1)*2+1
       do l1 = 1, grid%vds(normDir)
          do i = 1, N(normDir)
             
             l0 = grid%vec_ind(l1,i,normDir)

             ! ... ND velocity and displacement components
             do dir = 1, ND

                lp = (l1-1)*ND+dir
                
                ! ... displacement
                dXYZ(l0,dir) = (1.0_rfreal - grid%S(l0,normDir))*dXBnd(lp,faceNum) + grid%S(l0,normDir)*dXBnd(lp,faceNum + 1)
                
                ! ... velocity
                dXYZ_TAU(l0,dir) = (1.0_rfreal - grid%S(l0,normDir))*dXBnd_TAU(lp,faceNum) + grid%S(l0,normDir)*dXBnd_TAU(lp,faceNum + 1)
                

             end do ! ... dir
          end do ! ... i
       end do ! ... l1

       if(ND >= 2) then
          ! ... correct the displacements along direction 2
          normDir = 2

          ! ... fill the update boundary data (to be broadcast from first and last processors in pencil communicator)
          do l1 = 1, grid%vds(normDir)
             do dir = 1, ND
                lp = (l1-1)*ND+dir
                ! ... left boundary
                l0 = grid%vec_ind(l1,1,normDir)
                dXBnd2(lp,1) = dXYZ(l0,dir)
                dXBnd_TAU2(lp,1) = dXYZ_TAU(l0,dir)

                ! ... right boundary
                l0 = grid%vec_ind(l1,N(normDir),normDir)
                dXBnd2(lp,2) = dXYZ(l0,dir)
                dXBnd_TAU2(lp,2) = dXYZ_TAU(l0,dir)
             end do
          end do
       
          ! ... use pencil communicators to communicate moving boundary data
          !do dir = 1, ND
          dir = 2
             if(grid%numproc_inPencilComm(dir) > 1) then
             
                faceNum = 1
                ! ... broadcast the data right from the first process in this pencil
                Call MPI_BCAST(dXBnd2(1,faceNum), grid%vds(dir)*ND, MPI_DOUBLE_PRECISION, 0, grid%PencilComm(dir), ierr)
             
                ! ... broadcast the velocity data right from the first process in this pencil
                Call MPI_BCAST(dXBnd_TAU2(1,faceNum), grid%vds(dir)*ND, MPI_DOUBLE_PRECISION, 0, grid%PencilComm(dir), ierr)

                faceNum = 2
                ! ... broadcast the data left from the last process in this pencil
                Call MPI_BCAST(dXBnd2(1,faceNum), grid%vds(dir)*ND, MPI_DOUBLE_PRECISION, grid%numproc_inPencilComm(dir)-1, grid%PencilComm(dir), ierr)

                ! ... broadcast the velocity data left from the last process in this pencil
                Call MPI_BCAST(dXBnd_TAU2(1,faceNum), grid%vds(dir)*ND, MPI_DOUBLE_PRECISION, grid%numproc_inPencilComm(dir)-1, grid%PencilComm(dir), ierr)

             end if
          !end do ! ... dir    

          faceNum = (normDir-1)*2+1
          do l1 = 1, grid%vds(normDir)
             do i = 1, N(normDir)

                l0 = grid%vec_ind(l1,i,normDir)

                ! ... ND velocity and displacement components
                do dir = 1, ND

                   lp = (l1-1)*ND+dir

!                    ! ... displacement
!                    dXYZ2(l0,dir) = dXYZ(l0,dir) + (1.0_rfreal - grid%S(l0,normDir))*(dXBnd(lp,faceNum)-dXYZ(grid%vec_ind(l1,1,normDir),dir)) + &
!                         grid%S(l0,normDir)*(dXBnd(lp,faceNum+1)-dXYZ(grid%vec_ind(l1,N(normDir),normDir),dir))

!                    ! ... velocity
!                    dXYZ2_TAU(l0,dir) = dXYZ_TAU(l0,dir) + (1.0_rfreal - grid%S(l0,normDir))*(dXBnd_TAU(lp,faceNum)-dXYZ_TAU(grid%vec_ind(l1,1,normDir),dir)) + &
!                         grid%S(l0,normDir)*(dXBnd_TAU(lp,faceNum+1)-dXYZ_TAU(grid%vec_ind(l1,N(normDir),normDir),dir))

                   ! ... displacement
                   dXYZ2(l0,dir) = dXYZ(l0,dir) + (1.0_rfreal - grid%S(l0,normDir))*(dXBnd(lp,faceNum)-dXBnd2(lp,1)) + &
                        grid%S(l0,normDir)*(dXBnd(lp,faceNum+1)-dXBnd2(lp,2))

                   ! ... velocity
                   dXYZ2_TAU(l0,dir) = dXYZ_TAU(l0,dir) + (1.0_rfreal - grid%S(l0,normDir))*(dXBnd_TAU(lp,faceNum)-dXBnd_TAU2(lp,1)) + &
                        grid%S(l0,normDir)*(dXBnd_TAU(lp,faceNum+1)-dXBnd_TAU2(lp,2))

                end do ! ... dir
             end do ! ... i
          end do ! ... l1

       deallocate(dXBnd2,dXBnd_TAU2)

       end if ! ... ND >= 2


       if(ND == 3) then
          ! ... correct the displacements along direction 3
          normDir = 3

          ! ... fill the update boundary data (to be broadcast from first and last processors in pencil communicator)
          do l1 = 1, grid%vds(normDir)
             do dir = 1, ND
                lp = (l1-1)*ND+dir
                ! ... left boundary
                l0 = grid%vec_ind(l1,1,normDir)
                dXBnd3(lp,1) = dXYZ2(l0,dir)
                dXBnd_TAU3(lp,1) = dXYZ2_TAU(l0,dir)

                ! ... right boundary
                l0 = grid%vec_ind(l1,N(normDir),normDir)
                dXBnd3(lp,2) = dXYZ2(l0,dir)
                dXBnd_TAU3(lp,2) = dXYZ2_TAU(l0,dir)
             end do
          end do
       
          ! ... use pencil communicators to communicate moving boundary data
          !do dir = 1, ND
          dir = 3
             if(grid%numproc_inPencilComm(dir) > 1) then
             
                faceNum = 1
                ! ... broadcast the data right from the first process in this pencil
                Call MPI_BCAST(dXBnd3(1,faceNum), grid%vds(dir)*ND, MPI_DOUBLE_PRECISION, 0, grid%PencilComm(dir), ierr)
             
                ! ... broadcast the velocity data right from the first process in this pencil
                Call MPI_BCAST(dXBnd_TAU3(1,faceNum), grid%vds(dir)*ND, MPI_DOUBLE_PRECISION, 0, grid%PencilComm(dir), ierr)

                faceNum = 2
                ! ... broadcast the data left from the last process in this pencil
                Call MPI_BCAST(dXBnd3(1,faceNum), grid%vds(dir)*ND, MPI_DOUBLE_PRECISION, grid%numproc_inPencilComm(dir)-1, grid%PencilComm(dir), ierr)

                ! ... broadcast the velocity data left from the last process in this pencil
                Call MPI_BCAST(dXBnd_TAU3(1,faceNum), grid%vds(dir)*ND, MPI_DOUBLE_PRECISION, grid%numproc_inPencilComm(dir)-1, grid%PencilComm(dir), ierr)

             end if
          !end do ! ... dir    

          faceNum = (normDir-1)*2+1
          do l1 = 1, grid%vds(normDir)
             do i = 1, N(normDir)

                l0 = grid%vec_ind(l1,i,normDir)

                ! ... ND velocity and displacement components
                do dir = 1, ND

                   lp = (l1-1)*ND+dir

                   ! ... displacement
                   dXYZ3(l0,dir) = dXYZ2(l0,dir) + (1.0_rfreal - grid%S(l0,normDir))*(dXBnd(lp,faceNum)-dXBnd3(lp,1)) + &
                        grid%S(l0,normDir)*(dXBnd(lp,faceNum+1)-dXBnd3(lp,2))

                   ! ... velocity
                   dXYZ3_TAU(l0,dir) = dXYZ2_TAU(l0,dir) + (1.0_rfreal - grid%S(l0,normDir))*(dXBnd_TAU(lp,faceNum)-dXBnd_TAU3(lp,1)) + &
                        grid%S(l0,normDir)*(dXBnd_TAU(lp,faceNum+1)-dXBnd_TAU3(lp,2))

!                    dXYZ3(l0,dir) = dXYZ2(l0,dir) + (1.0_rfreal - grid%S(l0,normDir))*(dXBnd(lp,faceNum)-dXYZ2(grid%vec_ind(l1,1,normDir),dir)) + &
!                         grid%S(l0,normDir)*(dXBnd(lp,faceNum+1)-dXYZ2(grid%vec_ind(l1,N(normDir),normDir),dir))

!                   if(region%myrank == 1 .and. i == 1) print*,'l0,lp,dir,dXYZ3(l0,dir)',l0,lp,dir,dXYZ3(l0,dir),dXBnd(lp,faceNum),dXBnd(lp,faceNum+1),dXYZ2(l0,dir)
                


!                    ! ... velocity
!                    dXYZ3_TAU(l0,dir) = dXYZ2_TAU(l0,dir) + (1.0_rfreal - grid%S(l0,normDir))*(dXBnd_TAU(lp,faceNum)-dXYZ2_TAU(grid%vec_ind(l1,1,normDir),dir)) + &
!                         grid%S(l0,normDir)*(dXBnd_TAU(lp,faceNum+1)-dXYZ2_TAU(grid%vec_ind(l1,N(normDir),normDir),dir))

                end do ! ... dir
             end do ! ... i
          end do ! ... l1

          deallocate(dXBnd3,dXBnd_TAU3)

       end if ! ... ND == 3 

       deallocate(dXBnd,dXBnd_TAU)
          
       if(ND >= 2) then
          deallocate(dXYZ,dXYZ_TAU)

          if(ND == 2) then
             dXYZ => dXYZ2
             dXYZ_TAU => dXYZ2_TAU
          end if
          if(ND == 3) then
             dXYZ => dXYZ3
             dXYZ_TAU => dXYZ3_TAU
          end if
       end if

       do k = 1, N(3)
          do j = 1, N(2)
             do i = 1, N(1)
                l0 = (k-1)*N(2)*N(1) + (j-1)*N(1) + (i-1) + 1
                do dir = 1, ND
                   grid%XYZ(l0,dir) = grid%XYZ(l0,dir) + dXYZ(l0,dir)
                   grid%XYZ_TAU(l0,dir) = grid%XYZ_TAU(l0,dir) + dXYZ_TAU(l0,dir)

                   ! ... test to see whether grid velocities calculated correctly
!                   region%state(ng)%cv(l0,dir) = grid%XYZ_TAU(l0,dir)
                end do
             end do
          end do
       end do

       if(ND >= 2) deallocate(dXYZ2,dXYZ2_TAU)
       if(ND == 3) deallocate(dXYZ3,dXYZ3_TAU)
       
    end do ! ... ng
    
  End Subroutine TransfiniteInterp             

END MODULE ModInterp
