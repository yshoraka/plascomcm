module ModElectricState

CONTAINS

  !> Initializes the state variables for the Efield.
  !!
  !! Allocates and sets initial values for the Efield state variables,
  !! and passes pointers to these variables to the C solver.
  subroutine ElectricInitState(region, ng)

    USE ModDataStruct
    USE ModDeriv
    USE ModMPI, ONLY: ghost_cell_exchange_box
    USE ModElectricInterface
    use iso_c_binding
    implicit none
#include "petsc_fortran.h"

    type(t_region), pointer :: region
    type(t_electric), pointer :: electric
    type(t_grid), pointer :: grid
    type(t_mixt_input), pointer :: input
    integer :: ng
    PetscErrorCode :: ierr
    Real(rfreal), Parameter :: pi = 3.1415926535D0

    real(rfreal) :: x, y, z, dielEps
    integer :: i, j, k, ind, nIonPass
    integer :: l0

    electric => region%electric(ng)
    grid => region%grid(ng)
    input => grid%input
    dielEps = input%ef_dielectric_permittivity

    if (electric%grid_has_efield) then

      allocate(electric%phi(grid%nCells))
      allocate(electric%rhs(grid%nCells))
      allocate(electric%bcoef(grid%nCells))
      allocate(electric%dcoef(grid%nCells))
      allocate(electric%jump(grid%nCells))
      allocate(electric%gradphi(grid%nCells,grid%ND))
      allocate(electric%interp_mask(grid%nCells))

      electric%rhs(:) = 0.0
      electric%phi(:) = 0.0
      electric%bcoef(:) = 0.0
      electric%dcoef(:) = 0.0
      electric%gradphi(:,:) = 0.0
      electric%jump(:) = 0.0

#ifdef AXISYMMETRIC
      allocate(electric%gradphi_pole(grid%nCells,grid%ND))
      electric%gradphi_pole(:,:) = 0.0
#endif

      if (electric%rank_has_efield .and. .not. electric%whole_grid) then

        allocate(electric%phi_subset(electric%subset_nCells))
        allocate(electric%rhs_subset(electric%subset_nCells))
        allocate(electric%bcoef_subset(electric%subset_nCells))
        allocate(electric%dcoef_subset(electric%subset_nCells))
        allocate(electric%jump_subset(electric%subset_nCells))

        electric%rhs_subset(:) = 0.0
        electric%phi_subset(:) = 0.0
        electric%bcoef_subset(:) = 0.0
        electric%dcoef_subset(:) = 0.0
        electric%jump_subset(:) = 0.0

      end if

      do i = 1, grid%nCells
        electric%dcoef(i) = merge(dielEps, 1._rfreal, grid%IBLANK(i) == 0)
      enddo

      if (input%ef_screening_length > 0.0) then
        electric%bcoef(:) = 1.0/(input%ef_screening_length / input%LengRef)**2
        do i = 1, grid%nCells
          electric%bcoef(i) = merge(electric%bcoef(i), 0._rfreal, grid%IBLANK(i) /= 0)
        enddo
      else
        electric%bcoef(:) = 0.0
      end if

      Call Ghost_Cell_Exchange_Box(region, ng, electric%dcoef)
      Call Ghost_Cell_Exchange_Box(region, ng, electric%bcoef)
      Call Ghost_Cell_Exchange_Box(region, ng, electric%jump)

      if (electric%rank_has_efield) then

        if (electric%whole_grid) then
          call ef_set_state(electric%solver,&
                            c_loc(electric%phi(1)), c_loc(electric%dcoef(1)),&
                            c_loc(electric%bcoef(1)), c_loc(electric%jump(1)),ierr)
          CHKERRQ(ierr)
        else
          call CopyWholeToSubset(region, ng, electric%dcoef, electric%dcoef_subset)
          call CopyWholeToSubset(region, ng, electric%bcoef, electric%bcoef_subset)
          call ef_set_state(electric%solver,&
                            c_loc(electric%phi_subset(1)), c_loc(electric%dcoef_subset(1)),&
                            c_loc(electric%bcoef_subset(1)), c_loc(electric%jump_subset(1)),ierr)
          CHKERRQ(ierr)
        end if

      end if

    end if

  end subroutine ElectricInitState

  subroutine ElectricRHS(region, ng)

    USE ModDataStruct
    USE ModDeriv
    USE ModMPI, ONLY: ghost_cell_exchange_box
    USE ModElectricInterface
    use iso_c_binding
    implicit none
#include "petsc_fortran.h"

    type(t_region), pointer :: region
    integer :: ng

    type(t_grid), pointer :: grid
    type(t_electric), pointer :: electric
    type(t_mixt_input), pointer :: input
    integer :: i, j, k
    integer :: l0
    real(rfreal) :: x, y, z, r, m
    integer, allocatable :: N(:)
    PetscErrorCode :: ierr
    real(rfreal) :: time

    real(rfreal), parameter :: PI = 0.5_rfreal * TWOPI

    grid => region%grid(ng)
    electric => region%electric(ng)
    input => grid%input
    time = MPI_WTIME()

    allocate(N(grid%ND))

    N = 1
    do j = 1, grid%ND
      N(j) = grid%ie(j)-grid%is(j)+1
    end do

    if (electric%grid_has_efield) then

      ! Screened equation RHS
      do k = grid%is(3), grid%ie(3)
        do j = grid%is(2), grid%ie(2)
          do i = grid%is(1), grid%ie(1)
            l0 = (k-grid%is(3))*N(1)*N(2) + (j-grid%is(2))*N(1) + (i-grid%is(1)+1)
            electric%rhs(l0) = (input%ef_plasma_potential/input%ef_voltage_scale)*electric%bcoef(l0)
          end do
        end do
      end do

      ! Manufactured RHS for test cases
      select case (input%caseID)
      case ('ELECTRICTESTS_2D_SINGLE','ELECTRICTESTS_2D_MULTIPLE')
        do k = grid%is(3), grid%ie(3)
          do j = grid%is(2), grid%ie(2)
            do i = grid%is(1), grid%ie(1)
              l0 = (k-grid%is(3))*N(1)*N(2) + (j-grid%is(2))*N(1) + (i-grid%is(1)+1)
              x = grid%xyz(l0,1)
              y = grid%xyz(l0,2)
              electric%rhs(l0) = 4._rfreal - 2._rfreal * (x**2 + y**2)
            end do
          end do
        end do
      case ('ELECTRICTESTS_PERIODIC_OVERLAP')
        do k = grid%is(3), grid%ie(3)
          do j = grid%is(2), grid%ie(2)
            do i = grid%is(1), grid%ie(1)
              l0 = (k-grid%is(3))*N(1)*N(2) + (j-grid%is(2))*N(1) + (i-grid%is(1)+1)
              x = grid%xyz(l0,1)
              y = grid%xyz(l0,2)
              electric%rhs(l0) = 4._rfreal * (3._rfreal - 2._rfreal/(x**2+y**2)) * sin(atan2(y,x) + &
                PI/4._rfreal)
            end do
          end do
        end do
      case ('ELECTRICTESTS_PERIODIC_NO_OVERLAP')
        do k = grid%is(3), grid%ie(3)
          do j = grid%is(2), grid%ie(2)
            do i = grid%is(1), grid%ie(1)
              l0 = (k-grid%is(3))*N(1)*N(2) + (j-grid%is(2))*N(1) + (i-grid%is(1)+1)
              x = grid%xyz(l0,1)
              y = grid%xyz(l0,2)
              z = grid%xyz(l0,3)
              electric%rhs(l0) = 2._rfreal * (PI**2 * (z+1._rfreal) * (z-1._rfreal) - 1._rfreal) * &
                sin(PI * (x + 0.25_rfreal)) * sin(PI * (y + 0.25_rfreal))
            end do
          end do
        end do
      case ('ELECTRICTESTS_3D_SINGLE','ELECTRICTESTS_3D_MULTIPLE')
        do k = grid%is(3), grid%ie(3)
          do j = grid%is(2), grid%ie(2)
            do i = grid%is(1), grid%ie(1)
              l0 = (k-grid%is(3))*N(1)*N(2) + (j-grid%is(2))*N(1) + (i-grid%is(1)+1)
              x = grid%xyz(l0,1)
              y = grid%xyz(l0,2)
              z = grid%xyz(l0,3)
              electric%rhs(l0) = -6._rfreal - 2._rfreal * (x**2*y**2 + y**2*z**2 + z**2*x**2) &
                + 4._rfreal * (x**2 + y**2 + z**2)
            end do
          end do
        end do
      case ('ELECTRICTESTS_SCREENED')
        do k = grid%is(3), grid%ie(3)
          do j = grid%is(2), grid%ie(2)
            do i = grid%is(1), grid%ie(1)
              l0 = (k-grid%is(3))*N(1)*N(2) + (j-grid%is(2))*N(1) + (i-grid%is(1)+1)
              x = grid%xyz(l0,1)
              electric%rhs(l0) = -2._rfreal + 1/(0.2_rfreal**2) * x**2
            end do
          end do
        end do
      case ('ELECTRICTESTS_SUBSET')
        electric%rhs = -2._rfreal
      end select

      Call Ghost_Cell_Exchange_Box(region, ng, electric%rhs)

      if (electric%rank_has_efield) then

        if (electric%whole_grid) then
          call ef_set_rhs(electric%solver, c_loc(electric%rhs(1)), ierr)
          CHKERRQ(ierr)
        else
          call CopyWholeToSubset(region, ng, electric%rhs, electric%rhs_subset)
          call ef_set_rhs(electric%solver, c_loc(electric%rhs_subset(1)), ierr)
          CHKERRQ(ierr)
        end if

      end if

    end if

    region%mpi_timings(ng)%efield_processing = region%mpi_timings(ng)%efield_processing + (MPI_WTIME() - time)

  end subroutine ElectricRHS

  !> Deallocates electric state.
  subroutine ElectricCleanupState(region, ng)

    USE ModDataStruct
    implicit none

    type(t_region), pointer :: region
    integer :: ng
    type(t_electric), pointer :: electric

    electric => region%electric(ng)

    if (electric%grid_has_efield) then

      deallocate(electric%rhs)
      deallocate(electric%phi)
      deallocate(electric%bcoef)
      deallocate(electric%dcoef)
      deallocate(electric%gradphi)
      deallocate(electric%jump)
      deallocate(electric%interp_mask)

#ifdef AXISYMMETRIC
      deallocate(electric%gradphi_pole)
#endif

      if (electric%rank_has_efield .and. .not. electric%whole_grid) then
        deallocate(electric%rhs_subset)
        deallocate(electric%phi_subset)
        deallocate(electric%bcoef_subset)
        deallocate(electric%dcoef_subset)
        deallocate(electric%jump_subset)
      end if

    end if

  end subroutine ElectricCleanupState

  !> Copies data from subset arrays to full grid arrays
  subroutine CopySubsetToWhole(region, ng, subset_ptr, whole_ptr)

    USE ModDataStruct
    USE ModGlobal
    implicit none

    type(t_region), pointer :: region
    integer :: ng
    real(rfreal), dimension(:), pointer :: subset_ptr
    real(rfreal), dimension(:), pointer :: whole_ptr

    type(t_grid), pointer :: grid
    type(t_electric), pointer :: electric
    integer :: i, j
    integer :: stride
    integer :: offset

    electric => region%electric(ng)

    do i = 1, electric%subset_nCells
      whole_ptr(electric%subset_to_whole(i)) = subset_ptr(i)
    end do

  end subroutine CopySubsetToWhole


  !> Copies data from full grid arrays to subset arrays
  subroutine CopyWholeToSubset(region, ng, whole_ptr, subset_ptr)

    USE ModDataStruct
    USE ModGlobal
    implicit none

    type(t_region), pointer :: region
    integer :: ng
    real(rfreal), dimension(:), pointer :: whole_ptr
    real(rfreal), dimension(:), pointer :: subset_ptr

    type(t_electric), pointer :: electric
    integer :: i

    electric => region%electric(ng)

    do i = 1, electric%subset_nCells
      subset_ptr(i) = whole_ptr(electric%subset_to_whole(i))
    end do

  end subroutine CopyWholeToSubset

end module ModElectricState
