! Copyright (c) 2015, University of Illinois at Urbana-Champaign, XPACC
! License: MIT, http://opensource.org/licenses/MIT
!-----------------------------------------------------------------------
!
! ModPlasmaActuator.f90
! 
! - basic code for the imposition of a plasma actuator
!
! Revision history
! - 08 Nov 2007 : DJB : initial code
!
!-----------------------------------------------------------------------
MODULE ModActuator

CONTAINS

  subroutine Plasma_Actuator(region, ng)

    Use ModGlobal
    Use ModDataStruct
    Use ModMetrics
    Use ModEOS
    Use ModDeriv
    Use ModIO
    Use ModMPI

    Implicit None

    TYPE (t_region), POINTER :: region
    INTEGER :: ng
    INTEGER, POINTER :: flags(:)

    ! ... local variables
    Integer :: act, K
    Type (t_mixt_input), pointer :: input
    Type (t_mixt), pointer :: state
    Type (t_grid), pointer :: grid
    Integer :: ND
    Real(rfreal) :: w_of_t, g_of_z, time, r, f_of_r, nd_act_radius

    ! ... fixed number of actuator
    Integer, Parameter :: Nact = 1

    ! ... local parameters
    Real(rfreal), Dimension(Nact) :: x0
    Real(rfreal), Dimension(Nact) :: y0
    Real(rfreal), Dimension(Nact) :: z0
    Real(rfreal), Dimension(Nact) :: dx
    Real(rfreal), Dimension(Nact) :: dy
    Real(rfreal), Dimension(Nact) :: Power
    Real(rfreal), Dimension(Nact) :: tr 
    Real(rfreal), Dimension(Nact) :: tf 
    Real(rfreal), Dimension(Nact) :: dc
    Real(rfreal), Dimension(Nact) :: freq
    Real(rfreal), Dimension(Nact) :: Tact
    Real(rfreal), Dimension(Nact) :: delay
    Real(rfreal), Dimension(Nact) :: sigma_xy, sigma_z
    Real(rfreal), Dimension(Nact) :: y_r, y_l, z_r, z_l
    Real(rfreal), Dimension(Nact) :: radius, efficiency 
    Real(rfreal), Dimension(Nact) :: length
    Real(rfreal), Dimension(Nact) :: volume

    ! ... useful numbers
    Real(rfreal), Parameter :: pi = 3.1415926535D0
    Real(rfreal), Parameter :: half = 0.5D0
    Real(rfreal), Parameter :: three_fourth = 0.75D0


    ! ... simplicity
    grid => region%grid(ng)
    input => grid%input
    state => region%state(ng)
    ND = input%ND

    ! ... initialize parameters
    Do act = 1, Nact
      x0(act)         = input%actuator_loc_x / input%LengRef
      y0(act)         = input%actuator_loc_y / input%LengRef
      z0(act)         = input%actuator_loc_z / input%LengRef
      radius(act)     = input%actuator_radius / input%LengRef
      length(act)     = input%actuator_length / input%LengRef
      efficiency(act) = input%actuator_efficiency
      freq(act)       = input%actuator_frequency*(input%LengRef/input%SndSpdRef)
      Tact(act)       = 1.0_8 / freq(act)
      tr(act)         = input%actuator_rise_time/(input%LengRef/input%SndSpdRef) 
      tf(act)         = input%actuator_fall_time/(input%LengRef/input%SndSpdRef) 
      delay(act)      = input%actuator_delay/(input%LengRef/input%SndSpdRef)   
      dc(act)         = input%actuator_duty_cycle
      Power(act)      = input%actuator_max_power*input%LengRef/(input%DensRef*input%SndSpdRef**3)   
      sigma_xy(act)   = input%actuator_sigma_xy
      sigma_z(act)    = input%actuator_sigma_z
      volume(act)     = pi*radius(act)**2*length(act)*input%LengRef**3  
    End Do

    ! ... loop over all actuators
    Do act = 1, Nact
      Do k = 1, grid%Ncells

        ! ... time relative to period of this actuator
        time = mod(state%time(k),Tact(act))

        ! ... temporal distribution
        w_of_t = 0.5_rfreal * (tanh((time-delay(act))/tr(act)) - tanh((time-(delay(act)+dc(act)*Tact(act)))/tf(act)))

        ! ... spatial distribution
        r = (grid%XYZ(k,1)-x0(act)) * (grid%XYZ(k,1)-x0(act))
        r = r + (grid%XYZ(k,2)-y0(act)) * (grid%XYZ(k,2)-y0(act))
        r = sqrt(r)
        f_of_r = 0.5_rfreal*(tanh(-sigma_xy(act)*(r/radius(act) - 1.0)) + 1.0)

        !g_of_z = -0.5*(tanh(-1*sigma_z*(grid%XYZ(k,2)-y_l)/act_radius)+1) +&
        !0.5*(tanh(-1*sigma_z*(grid%XYZ(k,2)-y_r)/act_radius)+1)

        ! ... add it in
        state%rhs(k,ND+2) = state%rhs(k,ND+2) + w_of_t * f_of_r * Power(act) * efficiency(act) / volume(act)
      End Do
    End Do

  end subroutine Plasma_Actuator

  subroutine PlaceActuator4Optimization(region, fname_stdin_actPtID)

    USE ModGlobal
    USE ModDataStruct
    USE ModMPI

    Implicit None

    ! ... subroutine arguments
    Type(t_region), Pointer :: region
    Character(LEN=30) :: fname_stdin_actPtID

    ! ... local variables and arrays
    Type(t_grid), Pointer :: grid
    Type(t_mixt_input), Pointer :: input
    Type(t_sub_actuator), Pointer :: act
    Integer :: ng, l0, i, np, na
    Integer :: numPts, actPtID, idum_actPtID
    Integer, Pointer :: index(:)
    Real(rfreal) :: xMin, xMax, yMin, yMax, zMin, zMax, xProd, yProd, zProd
    Real(rfreal) :: sigma, x0, y0, z0, dx, dy, dz, rdum_xyz(MAX_ND), fac_x, fac_y, fac_z
    Real(rfreal) :: rLoc, rMin, rMax, r0, rProd, facL_x, facR_x, facL_r, facR_r, fac_r, fac_theta, theta
    Logical :: pf_exists
    Integer :: io_err, found, ierr

    ! ... simplicity
    input => region%input

    ! ... given information on location of generic actuators by user, create 
    ! ... 't_sub_actuator'-type object if any and fill up part of information

    SELECT CASE( input%AdjOptim_typeOfOptim )
    CASE( 1:2 ) ! ... a 2-D rectangular-shape controller
                ! ... invented for model controller test
      ! ... geometry of actuator (2-D rectangle)
      xMin = -0.5_rfreal; xMax = 0.5_rfreal
      yMin = -0.5_rfreal; yMax = 0.5_rfreal

      ! ... first, check how many grids in this processor divide this controller in space
      ngLoop: do ng = 1, region%nGrids
        grid => region%grid(ng)

        do l0 = 1, grid%nCells
          xProd = (grid%xyz(l0,1) - xMin) * (xMax - grid%xyz(l0,1))
          yProd = (grid%xyz(l0,2) - yMin) * (yMax - grid%xyz(l0,2))

          if (xProd >= 0.0_rfreal .AND. yProd >= 0.0_rfreal) then
            region%nSubActuators = region%nSubActuators + 1
            CYCLE ngLoop ! ... finding a single point is okay because we have a single actuator
          end if ! xProd
        end do ! l0
      end do ngLoop

      if (region%nSubActuators > 0) then
        nullify(region%subActuator); allocate(region%subActuator(region%nSubActuators))

        region%nSubActuators = 0
        ! ... count them again and complete to fill out information
        do ng = 1, region%nGrids
          grid => region%grid(ng)

          nullify(index); allocate(index(grid%nCells)); index = 0

          numPts = 0 ! ... now we gotta count all actuation points
          do l0 = 1, grid%nCells
            xProd = (grid%xyz(l0,1) - xMin) * (xMax - grid%xyz(l0,1))
            yProd = (grid%xyz(l0,2) - yMin) * (yMax - grid%xyz(l0,2))

            if (xProd >= 0.0_rfreal .AND. yProd >= 0.0_rfreal) then
              numPts = numPts + 1
              index(numPts) = l0
            end if ! xProd
          end do ! l0

          if (numPts > 0) then ! ... fill up the actuator information
            region%nSubActuators = region%nSubActuators + 1

            nullify(act)
            act => region%subActuator(region%nSubActuators)

            act%subActID = region%nSubActuators
            act%actID = 1 ! ... we have a single model actuator globally
            act%gridID = ng
            act%gridID_global = grid%iGridGlobal
            act%numPts = numPts
            nullify(act%index); allocate(act%index(numPts)); act%index = 0
            do i = 1,numPts
              act%index(i) = index(i)
            end do ! i
            act%switch = CONTROL_ON
            nullify(act%distFunc); allocate(act%distFunc(numPts)); act%distFunc = 1.0_rfreal
            nullify(act%ptID); allocate(act%ptID(numPts)); act%ptID = 0
          end if ! numPts

          deallocate(index)
        end do ! ng
      end if ! region%nSubActuators

    CASE( 3 ) ! ... 2-D anti-sound cancellation controller
              ! ... for smooth spatial control distribution, control region has 
              ! ... a support everywhere

      region%nSubActuators = region%nGrids ! ... because every point is a controller

      if (region%nSubActuators > 0) then
        nullify(region%subActuator); allocate(region%subActuator(region%nSubActuators))

        region%nSubActuators = 0
        ! ... count them again and complete to fill out information
        do ng = 1, region%nGrids
          grid => region%grid(ng)

          nullify(index); allocate(index(grid%nCells)); index = 0

          numPts = 0 ! ... now we gotta count all actuation points
          do l0 = 1, grid%nCells
!!$         xProd = (grid%xyz(l0,1) - xMin) * (xMax - grid%xyz(l0,1))
!!$         yProd = (grid%xyz(l0,2) - yMin) * (yMax - grid%xyz(l0,2))

!!$         if (xProd >= 0.0_rfreal .AND. yProd >= 0.0_rfreal) then
              numPts = numPts + 1
              index(numPts) = l0
!!$         end if ! xProd
          end do ! l0

          if (numPts > 0) then ! ... fill up the actuator information
            region%nSubActuators = region%nSubActuators + 1

            nullify(act)
            act => region%subActuator(region%nSubActuators)

            act%subActID = region%nSubActuators
            act%actID = 1 ! ... we have a single model actuator globally
            act%gridID = ng
            act%gridID_global = grid%iGridGlobal
            act%numPts = numPts
            nullify(act%index); allocate(act%index(numPts)); act%index = 0
            do i = 1,numPts
              act%index(i) = index(i)
            end do ! i
            act%switch = CONTROL_ON
            nullify(act%distFunc); allocate(act%distFunc(numPts)); act%distFunc = 1.0_rfreal
            ! ... Gaussian distribution for control variables
            sigma = 1.0_rfreal
            x0 = 3.0_rfreal
            do i = 1,numPts
              l0 = act%index(i)

              act%distFunc(i) = EXP(-sigma * ((grid%xyz(l0,1)-x0)**2 + grid%xyz(l0,2)**2))
            end do ! i
            nullify(act%ptID); allocate(act%ptID(numPts)); act%ptID = 0
          end if ! numPts

          deallocate(index)
        end do ! ng
      end if ! region%nSubActuators

    CASE( 4 ) ! ... anti-sound cancellation across 2-D mixing layer
              ! ... unlike the anti-sound in quiet fluid, controller doesn't have 
              ! ... support everywhere

      ! ... first, find out if any grid in this region supports actuator
      yMin =-2000.0_rfreal
      yMax =-1500.0_rfreal
      y0 = 0.5_rfreal * (yMin + yMax)
      dy = (yMax - yMin) / REAL(10-1,rfreal) ! ... 10 points in [-2000, -1500]

      ngLoop2: do ng = 1, region%nGrids
        grid => region%grid(ng)

        do l0 = 1,grid%nCells
          yProd = (grid%xyz(l0,2) - yMin) * (yMax - grid%xyz(l0,2))

          if (yProd >= 0.0_rfreal) then
            region%nSubActuators = region%nSubActuators + 1
            CYCLE ngLoop2 ! ... finding a single point is okay because we have a single actuator
          end if ! yProd
        end do ! l0
      end do ngLoop2

      if (region%nSubActuators > 0) then
        nullify(region%subActuator); allocate(region%subActuator(region%nSubActuators))

        region%nSubActuators = 0
        ! ... count them again and complete to fill out information
        do ng = 1, region%nGrids
          grid => region%grid(ng)

          nullify(index); allocate(index(grid%nCells)); index = 0

          numPts = 0 ! ... now we gotta count all actuation points
          do l0 = 1, grid%nCells
            yProd = (grid%xyz(l0,2) - yMin) * (yMax - grid%xyz(l0,2))

            if (yProd >= 0.0_rfreal) then
              numPts = numPts + 1
              index(numPts) = l0
            end if ! yProd
          end do ! l0

          if (numPts > 0) then ! ... fill up the actuator information
            region%nSubActuators = region%nSubActuators + 1

            nullify(act)
            act => region%subActuator(region%nSubActuators)

            act%subActID = region%nSubActuators
            act%actID = 1 ! ... we have a single model actuator globally
            act%gridID = ng
            act%gridID_global = grid%iGridGlobal
            act%numPts = numPts
            nullify(act%index); allocate(act%index(numPts)); act%index = 0
            do i = 1,numPts
              act%index(i) = index(i)
            end do ! i
            act%switch = CONTROL_ON
            nullify(act%distFunc); allocate(act%distFunc(numPts)); act%distFunc = 1.0_rfreal
            ! ... Gaussian distribution for control variables
            do i = 1,numPts
              l0 = act%index(i)

              act%distFunc(i) = EXP(-(grid%xyz(l0,2)-y0)**2 / (2.0_rfreal*dy)**2)
            end do ! i
            nullify(act%ptID); allocate(act%ptID(numPts)); act%ptID = 0
          end if ! numPts

          deallocate(index)
        end do ! ng
      end if ! region%nSubActuators

    CASE( 5 ) ! ... anti-sound cancellation of 3-D M=1.3 cold jet

      ! ... first, find out if any grid in this region supports actuator
      xMin = 11.5_rfreal
      xMax = 14.5_rfreal
      x0 = 0.5_rfreal * (xMin + xMax)
      dx = (xMax - xMin) / REAL(52-1,rfreal) ! ... 52 points in [11.5, 14.5] in x
      yMin = 3.5_rfreal
      yMax = 6.5_rfreal
      y0 = 0.5_rfreal * (yMin + yMax)
      dy = (yMax - yMin) / REAL(23-1,rfreal) ! ... 23 points in [3.5, 6.5] in y
      zMin = -3.0_rfreal
      zMax = 3.0_rfreal
      z0 = 0.5_rfreal * (zMin + zMax)
      dz = (zMax - zMin) / REAL(7-1,rfreal) ! ... 7 points in [-3.0, 3.0] in z

      ngLoop3: do ng = 1, region%nGrids
        grid => region%grid(ng)

        do l0 = 1,grid%nCells
          xProd = (grid%xyz(l0,1) - xMin) * (xMax - grid%xyz(l0,1))
          yProd = (grid%xyz(l0,2) - yMin) * (yMax - grid%xyz(l0,2))
          zProd = (grid%xyz(l0,3) - zMin) * (zMax - grid%xyz(l0,3))

          if (xProd >= 0.0_rfreal .AND. yProd >= 0.0_rfreal .AND. zProd >= 0.0_rfreal) then
            region%nSubActuators = region%nSubActuators + 1
            CYCLE ngLoop3 ! ... finding a single point is okay because we have a single actuator
          end if ! xProd
        end do ! l0
      end do ngLoop3

      if (region%nSubActuators > 0) then
        nullify(region%subActuator); allocate(region%subActuator(region%nSubActuators))

        region%nSubActuators = 0
        ! ... count them again and complete to fill out information
        do ng = 1, region%nGrids
          grid => region%grid(ng)

          nullify(index); allocate(index(grid%nCells)); index = 0

          numPts = 0 ! ... now we gotta count all actuation points
          do l0 = 1, grid%nCells
            xProd = (grid%xyz(l0,1) - xMin) * (xMax - grid%xyz(l0,1))
            yProd = (grid%xyz(l0,2) - yMin) * (yMax - grid%xyz(l0,2))
            zProd = (grid%xyz(l0,3) - zMin) * (zMax - grid%xyz(l0,3))

            if (xProd >= 0.0_rfreal .AND. yProd >= 0.0_rfreal .AND. zProd >= 0.0_rfreal) then
              numPts = numPts + 1
              index(numPts) = l0
            end if ! xProd
          end do ! l0

          if (numPts > 0) then ! ... fill up the actuator information
            region%nSubActuators = region%nSubActuators + 1

            nullify(act)
            act => region%subActuator(region%nSubActuators)

            act%subActID = region%nSubActuators
            act%actID = 1 ! ... we have a single model actuator globally
            act%gridID = ng
            act%gridID_global = grid%iGridGlobal
            act%numPts = numPts
            nullify(act%index); allocate(act%index(numPts)); act%index = 0
            do i = 1,numPts
              act%index(i) = index(i)
            end do ! i
            act%switch = CONTROL_ON
            nullify(act%distFunc); allocate(act%distFunc(numPts)); act%distFunc = 1.0_rfreal
            ! ... Gaussian distribution for control variables
            do i = 1,numPts
              l0 = act%index(i)

              fac_x = EXP(-(grid%xyz(l0,1)-x0)**2 / (4.0_rfreal*dx)**2)
              fac_y = EXP(-(grid%xyz(l0,2)-y0)**2 / (2.0_rfreal*dy)**2)
              fac_z = EXP(-(grid%xyz(l0,3)-z0)**2 / (1.0_rfreal*dz)**2)
              act%distFunc(i) = fac_x * fac_y * fac_z
            end do ! i
            nullify(act%ptID); allocate(act%ptID(numPts)); act%ptID = 0
          end if ! numPts

          deallocate(index)
        end do ! ng
      end if ! region%nSubActuators

    CASE( 6 ) ! ... flow control of 3-D M=1.3 cold jet without a nozzle

      ! ... first, find out if any grid in this region supports actuator
      xMin = 1.0_rfreal
      xMax = 3.0_rfreal
      x0 = 0.5_rfreal * (xMin + xMax)
      facL_x = 5.0_rfreal ! ... determines how steep the hyperbolic tangent is
      facR_x = 5.0_rfreal ! ... determines how steep the hyperbolic tangent is
      rMin = 0.3_rfreal
      rMax = 0.7_rfreal
      r0 = 0.5_rfreal * (rMin + rMax)
      facL_r = 15.0_rfreal
      facR_r = 15.0_rfreal

      ngLoop4: do ng = 1, region%nGrids
        grid => region%grid(ng)

        do l0 = 1,grid%nCells
          xProd = (grid%xyz(l0,1) - xMin) * (xMax - grid%xyz(l0,1))
          rLoc = SQRT((grid%xyz(l0,2))**2 + (grid%xyz(l0,3))**2)
          rProd = (rLoc - rMin) * (rMax - rLoc)

          if (xProd >= 0.0_rfreal .AND. rProd >= 0.0_rfreal) then
            region%nSubActuators = region%nSubActuators + 1
            CYCLE ngLoop4 ! ... finding a single point is okay because we have a single actuator
          end if ! xProd
        end do ! l0
      end do ngLoop4

      if (region%nSubActuators > 0) then
        nullify(region%subActuator); allocate(region%subActuator(region%nSubActuators))

        region%nSubActuators = 0
        ! ... count them again and complete to fill out information
        do ng = 1, region%nGrids
          grid => region%grid(ng)

          nullify(index); allocate(index(grid%nCells)); index = 0

          numPts = 0 ! ... now we gotta count all actuation points
          do l0 = 1, grid%nCells
            xProd = (grid%xyz(l0,1) - xMin) * (xMax - grid%xyz(l0,1))
            rLoc = SQRT((grid%xyz(l0,2))**2 + (grid%xyz(l0,3))**2)
            rProd = (rLoc - rMin) * (rMax - rLoc)

            if (xProd >= 0.0_rfreal .AND. rProd >= 0.0_rfreal) then
              numPts = numPts + 1
              index(numPts) = l0
            end if ! xProd
          end do ! l0

          if (numPts > 0) then ! ... fill up the actuator information
            region%nSubActuators = region%nSubActuators + 1

            nullify(act)
            act => region%subActuator(region%nSubActuators)

            act%subActID = region%nSubActuators
            act%actID = 1 ! ... we have a single model actuator globally
            act%gridID = ng
            act%gridID_global = grid%iGridGlobal
            act%numPts = numPts
            nullify(act%index); allocate(act%index(numPts)); act%index = 0
            do i = 1,numPts
              act%index(i) = index(i)
            end do ! i
            act%switch = CONTROL_ON
            nullify(act%distFunc); allocate(act%distFunc(numPts)); act%distFunc = 1.0_rfreal
            ! ... Gaussian distribution for control variables
            do i = 1,numPts
              l0 = act%index(i)

              fac_x = TANH(facL_x * (grid%xyz(l0,1) - xMin)) + TANH(facR_x * (xMax - grid%xyz(l0,1))) - 1.0_rfreal
              rLoc = SQRT((grid%xyz(l0,2))**2 + (grid%xyz(l0,3))**2)
              fac_r = TANH(facL_r * (rLoc - rMin)) + TANH(facR_r * (rMax - rLoc)) - 1.0_rfreal
              act%distFunc(i) = fac_x * fac_r
            end do ! i
            ! ... more spatial weighting function in a spatially-constrained optimization
            if (input%AdjOptim_Constraint == CONTROL_CONSTRAINED_SPACE) then
              do i = 1,numPts
                l0 = act%index(i)

                ! ... four actuators in the circumferential direction
                sigma = 6.0_rfreal
                theta = ATAN2(grid%xyz(l0,3), grid%xyz(l0,2)) ! ... a reference axis measuring theta is +y
                fac_theta = EXP(-sigma* (SIN(2.0_rfreal * theta))**6)
                act%distFunc(i) = act%distFunc(i) * fac_theta
              end do ! i
            end if ! input%AdjOptim_Constraint

            nullify(act%ptID); allocate(act%ptID(numPts)); act%ptID = 0
          end if ! numPts

          deallocate(index)
        end do ! ng
      end if ! region%nSubActuators

    CASE DEFAULT
      call graceful_exit(region%myrank, "... placing actuator failed: unknown type of test optimization")

    END SELECT ! input%AdjOptim_typeOfOptim
    call mpi_barrier(mycomm, ierr)



    ! ... read or define unique IDs for every actuating point
    if (input%AdjOptim_typeOfOptim > 2) then
      ! ... first check if we can find the file
      inquire(file=trim(fname_stdin_actPtID), exist=pf_exists)

      if (pf_exists) then ! ... just read'em by every processor at once
        if (region%nSubActuators > 0) then
          do na = 1, region%nSubActuators
            act => region%subActuator(na)

            open(30, file=trim(fname_stdin_actPtID), STATUS = 'OLD')
            do i = 1,act%numPts
              l0 = act%index(i)

              rewind(30)
              io_err = FALSE; found = FALSE
              do while(io_err == FALSE .AND. found == FALSE)
                rdum_xyz(:) = 0.0_rfreal
                read(30,*,iostat=io_err) idum_actPtID, rdum_xyz(1:input%ND)
                if (io_err == FALSE) then
                  rdum_xyz(1:input%ND) = ABS(rdum_xyz(1:input%ND) - grid%xyz(l0,1:input%ND))
                  if (SUM(rdum_xyz(:)) < 1E-6) then ! ... we found the match
                    act%ptID(i) = idum_actPtID
                    found = TRUE
                  end if ! SUM(rdum_xyz(:))
                end if ! io_err
              end do ! io_err
              if (found == FALSE) call graceful_exit(region%myrank, "... ERROR: something wrong in finding actuating point ID")
            end do ! i
            close(30)

          end do ! na
        end if ! region%nSubActuators

      else ! ... we need to come up with a new one
        actPtID = 0
        do np = 0,numproc-1
          if (region%myrank == np) then
            if (region%nSubActuators > 0) then
              open(30, file=trim(fname_stdin_actPtID), position = 'APPEND')
              do na = 1,region%nSubActuators
                act => region%subActuator(na)
                ng = act%gridID
                grid => region%grid(ng)

                do i = 1,act%numPts
                  l0 = act%index(i)

                  actPtID = actPtID + 1
                  act%ptID(i) = actPtID ! ... each point keeps its own
                  write(30,*) actPtID, grid%xyz(l0,1:input%ND) ! ... and save it as well
                end do ! i
              end do ! na
              close(30)
            end if ! region%nSubActuators
          end if ! myrank
          ! ... share the updated actPtID
          call mpi_barrier(mycomm, ierr)
          call mpi_bcast(actPtID,1,MPI_INTEGER,np,mycomm,ierr)
        end do ! np

      end if ! pf_exists
    end if ! input%AdjOptim_typeOfOptim
    call mpi_barrier(mycomm, ierr)

  end subroutine PlaceActuator4Optimization

  subroutine computeInstantaneousControl(region, act)

    USE ModGlobal
    USE ModDataStruct
    USE ModMPI

    Implicit None

    ! ... subroutine arguments
    Type(t_region), Pointer :: region
    Type(t_sub_actuator), Pointer :: act

    ! ... local variables and arrays
    Type(t_grid), Pointer :: grid
    Type(t_mixt), Pointer :: state
    Type(t_mixt_input), Pointer :: input
    Integer :: ng, i, j, k, l0
    Real(rfreal) :: denom, fac1, fac2, spaceDist, tempDist

    ! ... simplicity
    input => region%input
    ng = act%gridID
    grid => region%grid(ng)
    state => region%state(ng)

    if (act%switch == CONTROL_OFF) then
      act%phi(:) = 0.0_rfreal

    else if (act%switch == CONTROL_ON) then
      ! ... control input is interpolated in time from act%gPhi
      ! ... Case 1: linear interpolation in time
      i = state%numCur_cvFile; j = state%numCur_cvFile + 1
      denom = 1.0_rfreal / DABS(state%time_cvFiles(j) - state%time_cvFiles(i))
      fac1  = (state%time_cvFiles(j) - state%time(1)) * denom
      fac2  = (state%time(1) - state%time_cvFiles(i)) * denom
      do k = 1,act%numPts
        act%phi(k) = fac1 * act%gPhi(i,k) + fac2 * act%gPhi(j,k)
      end do ! k

      ! ... impose space-time distribution if there is any
      tempDist = temporalConstraintOnControl(state%time(1), region%ctrlStartTime, region%ctrlEndTime)
      do k = 1,act%numPts
        spaceDist = act%distFunc(k)

        act%phi(k) = act%phi(k) * spaceDist * tempDist
      end do ! k
    end if ! act%switch

  end subroutine computeInstantaneousControl

  function temporalConstraintOnControl(curTime, minTime, maxTime)

    USE ModGlobal
    USE ModDataStruct
    USE ModMPI

    Implicit None

    ! ... function arguments
    Real(rfreal) :: curTime, minTime, maxTime
    Real(rfreal) :: temporalConstraintOnControl

    ! ... local variables and arrays
    Real(rfreal) :: locCtrlHorizon

    temporalConstraintOnControl = 1.0_rfreal

    ! ... compute a temporal ramping function linearly varying between 
    ! ... 0 and 1 for the initial & the last 5% of control time horizon
    locCtrlHorizon = (curTime - minTime) / &
                     (maxTime - minTime)

    ! ... check where we are
    if (locCtrlHorizon <= 0.05_rfreal) then ! ... initial 5%?
      temporalConstraintOnControl = locCtrlHorizon / 0.05_rfreal
    else if (locCtrlHorizon >= 0.95_rfreal) then ! ... last 5%?
      temporalConstraintOnControl = (1.0_rfreal - locCtrlHorizon) / 0.05_rfreal
    else
      temporalConstraintOnControl = 1.0_rfreal
    end if ! locCtrlHorizon

    ! ... put more constraints here for time-constrained optimization

  end function temporalConstraintOnControl

  subroutine OperateActuator4Optimization(region, ng)

    USE ModGlobal
    USE ModDataStruct
    USE ModMPI

    Implicit None

    ! ... subroutine arguments
    Type(t_region), Pointer :: region
    Integer :: ng

    ! ... local variables and arrays
    Type(t_grid), Pointer :: grid
    Type(t_mixt), Pointer :: state
    Type(t_mixt_input), Pointer :: input
    Type(t_sub_actuator), Pointer :: act
    Integer :: na, i, j, l0, ind2D(2)
    Real(rfreal) :: F(MAX_ND+2), phi, theta, fac2D(2)

    ! ... simplicity
    input => region%input
    grid => region%grid(ng)
    state => region%state(ng)

    if (.NOT. input%AdjOptim .OR. region%global%USE_RESTART_FILE == FALSE) return ! ... defensive programming

    if (region%GLOBAL_ITER == 0) return ! ... uncontrolled forward run

    ! ... move the cv-file pointer into the relevant location
    i = state%numCur_cvFile
    if (state%time(1) > state%time_cvFiles(i+1) + 0.1_rfreal*input%dt) then
      state%numCur_cvFile = state%numCur_cvFile + 1

      ! ... defensive programming; check if we're jumping over more than one interval
      i = state%numCur_cvFile
      if (state%time(1) > state%time_cvFiles(i+1) + 0.1_rfreal*input%dt) &
        call graceful_exit(region%myrank, "... ERROR: cannot jump over more than one interval")
    end if ! state%time(1)

    if (region%nSubActuators > 0) then
      do na = 1,region%nSubActuators
        act => region%subActuator(na)

        if (act%switch == CONTROL_OFF) CYCLE
        if (act%gridID /= ng) CYCLE ! ... operated only in this grid, ng
        call computeInstantaneousControl(region, act)

        ! ... F-vector constructed following Table 1 of Wei & Freund
        F(:) = 0.0_rfreal
        SELECT CASE( input%AdjOptim_CtrlType )
        CASE( CONTROL_MASS )
          F(1) = 1.0_rfreal
          F(grid%ND+2) = 1.0_rfreal / 0.4_rfreal / 1.4_rfreal

          do i = 1,act%numPts
            l0 = act%index(i)

            phi = act%phi(i)
            do j = 1,grid%ND+2
              state%rhs(l0,j) = state%rhs(l0,j) + F(j) * phi
            end do ! j
          end do ! i

        CASE( CONTROL_BODY_FORCE_X:CONTROL_BODY_FORCE_Z )
          F(input%AdjOptim_CtrlType) = 1.0_rfreal

          do i = 1,act%numPts
            l0 = act%index(i)

            F(grid%ND+2) = state%cv(l0,input%AdjOptim_CtrlType) * state%dv(l0,3)

            phi = act%phi(i)
            do j = 1,grid%ND+2
              state%rhs(l0,j) = state%rhs(l0,j) + F(j) * phi
            end do ! j
          end do ! i

        CASE( CONTROL_BODY_FORCE_XY:CONTROL_BODY_FORCE_ZX )
          ind2D(:) = 0
          if (input%AdjOptim_CtrlType == CONTROL_BODY_FORCE_XY) then
            ind2D(1) = 1; ind2D(2) = 2
          else if (input%AdjOptim_CtrlType == CONTROL_BODY_FORCE_YZ) then
            ind2D(1) = 2; ind2D(2) = 3
          else if (input%AdjOptim_CtrlType == CONTROL_BODY_FORCE_ZX) then
            ind2D(1) = 3; ind2D(2) = 1
          end if ! input%AdjOptim_CtrlType
          ind2D(:) = ind2D(:) + 1

          do i = 1,act%numPts
            l0 = act%index(i)

            F(:) = 0.0_rfreal
            do j = 1,2
              F(ind2D(j))  = 1.0_rfreal
              F(grid%ND+2) = F(grid%ND+2) + state%cv(l0,ind2D(j)) * state%dv(l0,3)
            end do ! j

            phi = act%phi(i)
            do j = 1,grid%ND+2
              state%rhs(l0,j) = state%rhs(l0,j) + F(j) * phi
            end do ! j
          end do ! i

        CASE( CONTROL_BODY_FORCE_XYZ )
          do i = 1,act%numPts
            l0 = act%index(i)

            F(:) = 0.0_rfreal
            do j = 1,3 ! ... forcing in every direction; assumes 3-D
              F(j+1) = 1.0_rfreal
              F(5)   = F(5) + state%cv(l0,j+1) * state%dv(l0,3)
            end do ! j

            phi = act%phi(i)
            do j = 1,grid%ND+2
              state%rhs(l0,j) = state%rhs(l0,j) + F(j) * phi
            end do ! j
          end do ! i

        CASE( CONTROL_BODY_FORCE_R )
          ind2D(:) = 0
          if (input%AdjOptim_axis_of_rot == 1) then
            ind2D(1) = 2; ind2D(2) = 3
          else if (input%AdjOptim_axis_of_rot == 2) then
            ind2D(1) = 3; ind2D(2) = 1
          else if (input%AdjOptim_axis_of_rot == 3) then
            ind2D(1) = 1; ind2D(2) = 2
          end if ! input%AdjOptim_CtrlType
          ind2D(:) = ind2D(:) + 1

          do i = 1,act%numPts
            l0 = act%index(i)

            theta = ATAN2(grid%xyz(l0,ind2D(2)-1), grid%xyz(l0,ind2D(1)-1))
            fac2D(1) =  COS(theta)
            fac2D(2) = -SIN(theta)

            F(:) = 0.0_rfreal
            F(ind2D(1))  = fac2D(1)
            F(ind2D(2))  = fac2D(2)
            F(grid%ND+2) = state%cv(l0,ind2D(1)) * state%dv(l0,3) * fac2D(1) + &
                           state%cv(l0,ind2D(2)) * state%dv(l0,3) * fac2D(2)

            phi = act%phi(i)
            do j = 1,grid%ND+2
              state%rhs(l0,j) = state%rhs(l0,j) + F(j) * phi
            end do ! j
          end do ! i

        CASE( CONTROL_BODY_FORCE_THETA )
          ind2D(:) = 0
          if (input%AdjOptim_axis_of_rot == 1) then
            ind2D(1) = 2; ind2D(2) = 3
          else if (input%AdjOptim_axis_of_rot == 2) then
            ind2D(1) = 3; ind2D(2) = 1
          else if (input%AdjOptim_axis_of_rot == 3) then
            ind2D(1) = 1; ind2D(2) = 2
          end if ! input%AdjOptim_CtrlType
          ind2D(:) = ind2D(:) + 1

          do i = 1,act%numPts
            l0 = act%index(i)

            theta = ATAN2(grid%xyz(l0,ind2D(2)-1), grid%xyz(l0,ind2D(1)-1))
            fac2D(1) =  SIN(theta)
            fac2D(2) =  COS(theta)

            F(:) = 0.0_rfreal
            F(ind2D(1))  = fac2D(1)
            F(ind2D(2))  = fac2D(2)
            F(grid%ND+2) = state%cv(l0,ind2D(1)) * state%dv(l0,3) * fac2D(1) + &
                           state%cv(l0,ind2D(2)) * state%dv(l0,3) * fac2D(2)

            phi = act%phi(i)
            do j = 1,grid%ND+2
              state%rhs(l0,j) = state%rhs(l0,j) + F(j) * phi
            end do ! j
          end do ! i

        CASE( CONTROL_INTERNAL_ENERGY )
          F(grid%ND+2) = 1.0_rfreal

          do i = 1,act%numPts
            l0 = act%index(i)

            phi = act%phi(i)
            do j = 1,grid%ND+2
              state%rhs(l0,j) = state%rhs(l0,j) + F(j) * phi
            end do ! j
          end do ! i

        CASE DEFAULT

        END SELECT ! input%AdjOptim_CtrlType

      end do ! na
    end if ! region%nSubActuators

  end subroutine OperateActuator4Optimization

  ! ... add source terms to adjoint N-S equations
  ! ... sources are chosen such that given a specific type of cost functional, 
  ! ... adjoint variables can produce gradient to control input.
  subroutine AdjNS_Source(region, ng)

    USE ModGlobal
    USE ModDataStruct
    USE ModMPI

    Implicit None

    ! ... subroutine arguments
    Type(t_region), Pointer :: region
    Integer :: ng

    ! ... local variables and arrays
    Type(t_grid), Pointer :: grid
    Type(t_mixt), Pointer :: state
    Type(t_mixt_input), Pointer :: input
    Type(t_sub_ctrl_target), Pointer :: targ
    Type(t_sub_actuator), Pointer :: act
    Integer :: nt, na, i, j, l0, ind2D(2)
    Real(rfreal) :: Astar(MAX_ND+2), Fstar(MAX_ND+2), phi, theta, fac2D(2)
    Real(rfreal) :: spaceDist, tempDist

    ! ... simplicity
    input => region%input
    grid => region%grid(ng)
    state => region%state(ng)

    if (.NOT. input%AdjOptim .OR. region%global%USE_RESTART_FILE == FALSE) return ! ... defensive programming

    ! ... depending on types of actuation, subtract adjoint of F'; see Eq. (3.12) of Wei & Freund
    if (region%nSubActuators > 0) then
      do na = 1,region%nSubActuators
        act => region%subActuator(na)

        if (act%gridID /= ng) CYCLE ! ... sources only in this grid, ng

        call computeInstantaneousControl(region, act)
        if (act%switch == CONTROL_OFF) CYCLE

        SELECT CASE( input%AdjOptim_CtrlType )
        CASE( CONTROL_BODY_FORCE_X:CONTROL_BODY_FORCE_Z )
          do i = 1,act%numPts
            l0 = act%index(i)

            Astar(:) = 0.0_rfreal
            Astar(input%AdjOptim_CtrlType) = state%av(l0,grid%ND+2)

            phi = act%phi(i)
            do j = 1,grid%ND+2
              state%rhs(l0,j) = state%rhs(l0,j) - Astar(j) * phi
            end do ! j
          end do ! i

        CASE( CONTROL_BODY_FORCE_XY:CONTROL_BODY_FORCE_ZX )
          ind2D(:) = 0
          if (input%AdjOptim_CtrlType == CONTROL_BODY_FORCE_XY) then
            ind2D(1) = 1; ind2D(2) = 2
          else if (input%AdjOptim_CtrlType == CONTROL_BODY_FORCE_YZ) then
            ind2D(1) = 2; ind2D(2) = 3
          else if (input%AdjOptim_CtrlType == CONTROL_BODY_FORCE_ZX) then
            ind2D(1) = 3; ind2D(2) = 1
          end if ! input%AdjOptim_CtrlType
          ind2D(:) = ind2D(:) + 1

          do i = 1,act%numPts
            l0 = act%index(i)

            Astar(:) = 0.0_rfreal
            do j = 1,2
              Astar(ind2D(j)) = state%av(l0,grid%ND+2)
            end do ! j

            phi = act%phi(i)
            do j = 1,grid%ND+2
              state%rhs(l0,j) = state%rhs(l0,j) - Astar(j) * phi
            end do ! j
          end do ! i

        CASE( CONTROL_BODY_FORCE_XYZ )
          do i = 1,act%numPts
            l0 = act%index(i)

            Astar(:) = 0.0_rfreal
            do j = 1,3 ! ... forcing in every direction; assumes 3-D
              Astar(j+1) = state%av(l0,5)
            end do ! j

            phi = act%phi(i)
            do j = 1,grid%ND+2
              state%rhs(l0,j) = state%rhs(l0,j) - Astar(j) * phi
            end do ! j
          end do ! i

        CASE( CONTROL_BODY_FORCE_R )
          ind2D(:) = 0
          if (input%AdjOptim_axis_of_rot == 1) then
            ind2D(1) = 2; ind2D(2) = 3
          else if (input%AdjOptim_axis_of_rot == 2) then
            ind2D(1) = 3; ind2D(2) = 1
          else if (input%AdjOptim_axis_of_rot == 3) then
            ind2D(1) = 1; ind2D(2) = 2
          end if ! input%AdjOptim_CtrlType
          ind2D(:) = ind2D(:) + 1

          do i = 1,act%numPts
            l0 = act%index(i)

            theta = ATAN2(grid%xyz(l0,ind2D(2)-1), grid%xyz(l0,ind2D(1)-1))
            fac2D(1) =  COS(theta)
            fac2D(2) = -SIN(theta)

            Astar(:) = 0.0_rfreal
            Astar(ind2D(1)) = state%av(l0,grid%ND+2) * fac2D(1)
            Astar(ind2D(2)) = state%av(l0,grid%ND+2) * fac2D(2)

            phi = act%phi(i)
            do j = 1,grid%ND+2
              state%rhs(l0,j) = state%rhs(l0,j) - Astar(j) * phi
            end do ! j
          end do ! i

        CASE( CONTROL_BODY_FORCE_THETA )
          ind2D(:) = 0
          if (input%AdjOptim_axis_of_rot == 1) then
            ind2D(1) = 2; ind2D(2) = 3
          else if (input%AdjOptim_axis_of_rot == 2) then
            ind2D(1) = 3; ind2D(2) = 1
          else if (input%AdjOptim_axis_of_rot == 3) then
            ind2D(1) = 1; ind2D(2) = 2
          end if ! input%AdjOptim_CtrlType
          ind2D(:) = ind2D(:) + 1

          do i = 1,act%numPts
            l0 = act%index(i)

            theta = ATAN2(grid%xyz(l0,ind2D(2)-1), grid%xyz(l0,ind2D(1)-1))
            fac2D(1) =  SIN(theta)
            fac2D(2) =  COS(theta)

            Astar(:) = 0.0_rfreal
            Astar(ind2D(1)) = state%av(l0,grid%ND+2) * fac2D(1)
            Astar(ind2D(2)) = state%av(l0,grid%ND+2) * fac2D(2)

            phi = act%phi(i)
            do j = 1,grid%ND+2
              state%rhs(l0,j) = state%rhs(l0,j) - Astar(j) * phi
            end do ! j
          end do ! i

        CASE DEFAULT
          ! ... mass and internal energy control have identically zero A* vector

        END SELECT ! input%AdjOptim_CtrlType

      end do ! na
    end if ! region%nSubActuators

    ! ... depending on definition of cost functional, add adjoint source term, F*
    if (region%nSubCtrlTarget > 0) then ! ... if there is any control target in this region
      do nt = 1, region%nSubCtrlTarget
        targ => region%subCtrlTarget(nt)

        if (targ%gridID /= ng) CYCLE ! ... add sources only when target is in this grid, ng

        tempDist = temporalConstraintOnControl(state%time(1), region%ctrlStartTime, region%ctrlEndTime)

        SELECT CASE( input%AdjOptim_Func )
        CASE( FUNCTIONAL_SOUND )

          do i = 1,targ%numPts
            l0 = targ%index(i)

            ! ... directly compute adjoint N-S source terms
            Fstar(:) = 0.0_rfreal
            Fstar(grid%ND+2) = -2.0_rfreal * (state%dv(l0,1) - targ%meanQ(i)) ! ... Eq. (3.18) of Wei & Freund

            ! ... give spatial distribution to the source
            spaceDist = targ%distFunc(i)
            Fstar(:) = Fstar(:) * spaceDist * tempDist

            do j = 1,grid%ND+2
              state%rhs(l0,j) = state%rhs(l0,j) + Fstar(j)
            end do ! j
          end do ! i

          CASE DEFAULT

          END SELECT ! input%AdjOptim_Func
      end do ! nt
    end if ! region%nSubCtrlTarget

  end subroutine AdjNS_Source

  Subroutine AcousticSource(region, ng)

    Use ModGlobal
    Use ModDataStruct
    Use ModMPI

    Implicit None

    TYPE (t_region), POINTER :: region
    INTEGER :: ng

    ! ... local variables
    Type (t_mixt_input), pointer :: input
    Type (t_mixt), pointer :: state
    Type (t_grid), pointer :: grid
    Real(RFREAL) :: g_of_x, f_of_t, X0(3), LX(3), rho_source, p_source, freq_ref
    Integer :: ND, l0, n

    ! ... simplicity
    grid  => region%grid(ng)
    input => grid%input
    state => region%state(ng)
    ND = input%ND

    ! ... Check if we should really be here
    If (input%acoustic_source == FALSE) return

    ! ... frequency scaling
    freq_ref = input%LengRef / input%SndSpdRef

    ! ... Error check for 3-D
    If (ND == 3) Call graceful_exit(region%myrank, 'ERROR: No support for 3-D yet in AcousticSource [ModActuator.f90]')

    ! ... Add RHS of the form exp[-(x-x0)^2/L_x^2] Sum[a_n cos(2 pi f_n t + phi_n)]
    If (ND == 2) Then

      ! ... source location and width
      x0(1:2) = input%acoustic_source_x0(1:2)
      lx(1:2) = input%acoustic_source_lx(1:2)

      ! ... source time function
      f_of_t  = 0.0_8
      Do n = 1, input%acoustic_source_nfreq
        f_of_t = f_of_t + input%acoustic_source_amp(n) * dcos(TWOPI * input%acoustic_source_freq(n) * freq_ref * state%time(1) + input%acoustic_source_phase(n))
      End Do

      ! ... finish up
      Do l0 = 1, grid%nCells

        ! ... spatial function
        g_of_x = dexp(-((grid%XYZ(l0,1)-x0(1))/lx(1))**2-((grid%XYZ(l0,2)-x0(2))/lx(2))**2)

        ! ... density source
        rho_source = f_of_t * g_of_x

        ! ... pressure source
        p_source = state%gv(l0,1) * state%dv(l0,1) * state%dv(l0,3) * rho_source

        ! ... add the source
        state%rhs(l0,1) = state%rhs(l0,1) + rho_source
        state%rhs(l0,ND+2) = state%rhs(l0,ND+2) + p_source / (state%gv(l0,1) - 1.0_8)

      End Do
    End If

  End Subroutine AcousticSource

  subroutine Plasma_MomentumTransfer_Actuator(region, ng)

    USE ModGlobal
    USE ModDataStruct
    USE ModMetrics
    USE ModEOS
    USE ModDeriv
    USE ModIO
    USE ModMPI
    USE ModPLOT3D_IO

    Implicit None

    TYPE (t_region), POINTER :: region
    INTEGER :: ng
    INTEGER, POINTER :: flags(:)

    ! ... local variables
    Integer :: act, K
    Type (t_mixt_input), pointer :: input
    Type (t_mixt), pointer :: state
    Type (t_grid), pointer :: grid
    Type (t_combustion), pointer :: combustion
    Type (t_electric), pointer :: electric
    Integer :: NDIM, nvar, idim, nvarFile,myRank  !NDIM -> dimensions in grid file; nvar dimensions in computations (maybe they are the same)
    Real(rfreal) :: freq, omega, factor, time, Th, tr,tf, dc,delay,timeH,w_of_t, period

    integer, Dimension(:,:), Pointer :: meshSizes
    INTEGER :: gridID
    character(len=80) :: forceFileName

    ! ... useful numbers
    Real(rfreal), Parameter :: pi = 3.1415926535D0
    Real(rfreal), Parameter :: half = 0.5D0
    Real(rfreal), Parameter :: three_fourth = 0.75D0


    ! ... simplicity
    grid => region%grid(ng)
    input => grid%input
    state => region%state(ng)
    electric => region%electric(ng)
    combustion => state%combustion
    nvar = input%ND
    NDIM = 3
    myrank = region%myRank
    gridID = grid%iGridGlobal

    ! this should be executed the fist call for each grid
    if (.not.associated(electric%momentumTerm) .eqv. .true.) then
       forceFileName = input%momentum_fname
       if(myRank == 0) print*,'accessing momentum transfer info in ', trim(forceFileName)
       CALL ReadFuncHeader_P3D(NDIM, ng, forceFileName, nvarFile)
       if(nvarFile /= nvar) call graceful_exit(myRank,'Plasma_MomentumTransfer_Actuator inconsistent dimensions')
       allocate(electric%momentumTerm(grid%nCells, nvar))
       if(myRank == 0) print'("Plasma_MomentumTransfer_Actuator NDIM, Nvar",123i5)',NDIM,nvar
       call ReadFunc_P3D(NDIM, meshSizes, region, grid, forceFileName, electric%momentumTerm, grid%iGridGlobal, nvar)
       if(myRank == 0) print'("Plasma_MomentumTransfer_Actuator NDIM, Nvar",123i5)',NDIM,nvar
       if(myRank == 0) print*,'finished reading ', trim(forceFileName)
       !startTime will be also reset in the radical analog
       if(input%electric_start_time < 0d0) then
          electric%startTime = state%time(1)
       else
          electric%startTime = input%electric_start_time
       endif
       if(myRank == 0) print*,'ELECTRIC_START_TIME',electric%startTime
    endif
    
    freq = input%momentum_frequency*combustion%model0%DBDTimeRef
    omega = 2d0*pi*freq
    k=1;  !only one time for all grids
    ! ... dimensionless time relative to the beginning
    time = state%time(k)-electric%startTime

    period = 1d0/freq
    timeH = mod(time,period)/period
    w_of_t = 1d0!merge(1d0,0d0,timeH > 0.3d0 .and. timeH < 0.7d0)

    factor = combustion%model0%momentumScaling*sin(omega*time)*sin(omega*time)*w_of_t;
    ! ... add it in
    !!Do idim=1,1   !lucacancel (use the line below)
    Do idim=1,nvar
      Do k = 1, grid%Ncells
        if(grid%XYZ(k,1) < 1d-5) continue
        state%rhs(k,1+idim) = state%rhs(k,1+idim) + electric%momentumTerm(k,idim)*factor
      End Do
    End Do

  end subroutine Plasma_MomentumTransfer_Actuator


  subroutine Plasma_Radical_Actuator(region, ng)

    USE ModGlobal
    USE ModDataStruct
    USE ModMetrics
    USE ModEOS
    USE ModDeriv
    USE ModIO
    USE ModPLOT3D_IO
    USE ModMPI

    Implicit None

    TYPE (t_region), POINTER :: region
    INTEGER :: ng
    INTEGER, POINTER :: flags(:)

    ! ... local variables
    Integer :: act, K
    Type (t_mixt_input), pointer :: input
    Type (t_mixt), pointer :: state
    Type (t_grid), pointer :: grid
    Type (t_electric), pointer :: electric
    Type (t_combustion), pointer :: combustion
    Integer :: NDIM, nvar, idim 

    Real(rfreal) :: freq, omega, time, factorE, factorK, factorS, dimTempFactor, dimPressFactor, EbyN, kD, ST, rhoH2sq
    Real(rfreal) :: Emax,Emin,kmax, period,tr,tf,timeH,w_of_t,delay,dc,Th
    integer, Dimension(:,:), Pointer :: meshSizes
    character(len=80) :: eMagFileName
    integer :: ih, ih2, iE, i,j, myRank

    ! ... useful numbers
    Real(rfreal), Parameter :: pi = 3.1415926535D0
    Real(rfreal), Parameter :: half = 0.5D0
    Real(rfreal), Parameter :: three_fourth = 0.75D0
    Real(rfreal), Parameter :: kB = 1.3806488D-23
    Real(rfreal), Parameter :: Vmsq2Td = 1.0D21
    real(rfreal), dimension(:,:),Pointer :: dv, rhsSP, aux, rhs
    TYPE(t_spline), Pointer :: spline


    ! ... simplicity
    grid => region%grid(ng)
    input => grid%input
    state => region%state(ng)
    electric => region%electric(ng)
    combustion => state%combustion
    dv => state%dv
    rhsSP => state%rhs_auxVars
    rhs => state%rhs
    aux => state%auxVars
    nvar = 1
    NDIM = 3
    myrank = region%myRank

!   initialize
    if (.not.associated(electric%eMag) .eqv. .true.) then
      eMagFileName = input%eMag_fname
      if(myRank == 0) print*,'accessing efield-magnitude info in ', trim(eMagFileName)
      call ReadFuncHeader_P3D(NDIM, ng, eMagFileName, nvar)
      allocate(electric%eMag(grid%nCells, nvar))
      call ReadFunc_P3D(NDIM, meshSizes, region, grid, eMagFileName, electric%eMag, grid%iGridGlobal, nvar)
      if(myRank == 0) print'("Plasma_Radical_Actuator NDIM, Nvar",123i5)',NDIM,nvar
      if(myRank == 0) print*,'finished reading ', trim(eMagFileName)
      if(input%electric_start_time < 0d0) then
        electric%startTime = state%time(1)
      else
        electric%startTime = input%electric_start_time
      endif
      if(myRank == 0) print*,'ELECTRIC_START_TIME',electric%startTime
!read in the splined values from bolsig...
!only one variable,
      allocate(electric%BolsigSplines(1))
      spline => electric%BolsigSplines(1)
      Open(unit=100, file=trim(input%BolsigTableName), form='unformatted', status='old')
      Read (100) spline%nxb
      Read (100) spline%nvars
      allocate(spline%xb(spline%nxb));
      Read (100) spline%xb
      spline%dxb = spline%xb(2)-spline%xb(1)
      allocate(spline%coeffs(spline%nxb-1,4,spline%nvars))
      Do i = 1, spline%nvars
        Read (100) ((spline%coeffs(j,k,i),j=1,spline%nxb-1),k=1,4)
      End Do
      !... close file
      close (100)
      if(myRank == 0) print*,'finished reading ', trim(input%BolsigTableName), 'Nvars = ',spline%nvars
    endif
    spline => electric%BolsigSplines(1)

    freq = input%momentum_frequency*combustion%model0%DBDTimeRef
    omega = 2d0*pi*freq
    k=1;  !only one time for all grids
    ! ... dimensionless time relative to the beginning
    time = state%time(k)-electric%startTime
    period = 1d0/freq

                                !! The DBD works in glow discharge mode @ change in polarity (twice per cycle). The lines below set this trend.
    if(input%Radical_duty_halfcycle < 0.5) then
      Th=0.5d0*period;
      tr=input%Radical_rise_time*period;
      tf=tr;
      dc=input%Radical_duty_halfcycle*period;delay=input%Radical_delay*period;
      timeH = mod(time-delay+Th/2d0,Th)-Th/2d0;
      w_of_t = 0.5d0* (tanh((timeH+dc/2)/tr) - tanh((timeH-dc/2)/tf))
    else
      w_of_t = 1d0
    endif

    dimTempFactor = (input%gamref-1.0d0)*input%tempref  !K
    dimPressFactor = input%presref*input%GamRef               !Pascal
                                !!use this option for pressure/temperature dependent N. 
   !factorE  = kB*dimTempFactor/dimPressFactor*Vmsq2Td* abs(sin(omega*time))*w_of_t
    factorE  = kB*input%tempref/input%presref*Vmsq2Td* abs(sin(omega*time))*w_of_t
    factorK = combustion%model0%radicalSourceScaling
    factorS = combustion%model0%energySourceScaling
    ih = combustion%model0%iH
    ih2 = combustion%model0%iH2
    iE = input%ND+2
    ! ... add it in
    Emin = spline%xb(1)
    Emax = spline%xb(spline%nxb)

    Do k = 1, grid%Ncells
     !EbyN=electric%eMag(k,1)*dv(k,2)/dv(k,1)*factorE  !dminesional, Td
      EbyN=electric%eMag(k,1)*factorE  !dminesional, Td
      if(EbyN > Emin) then
        if(EbyN > Emax) EbyN=Emax
        rhoH2sq = aux(k, ih2)*aux(k, ih2)
        kD = evaluateSpline(spline%nxb, spline%dxb, spline%xb, spline%coeffs, 1, EbyN)
        kD = kD*rhoH2sq*factorK          !dimensionless
        rhsSP(k, ih) = rhsSP(k,ih) +  kD
        rhsSP(k, ih2) = rhsSP(k,ih2) - kD
        ST = evaluateSpline(spline%nxb, spline%dxb, spline%xb, spline%coeffs, 2, EbyN)
        ST = ST*rhoH2sq*factorS          !dimensionless
        rhs(k,iE) = rhs(k,iE) + ST
      endif
    End Do

  end subroutine Plasma_Radical_Actuator
END MODULE ModActuator
