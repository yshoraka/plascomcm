! Copyright (c) 2015, University of Illinois at Urbana-Champaign, XPACC
! License: MIT, http://opensource.org/licenses/MIT
!-----------------------------------------------------------------------
!
! ModInitTM.f90
!
! - basic code for the initialization of the solid solver (thermal and mechanical)
!
! Revision history
! - 29 Feb 2012 : CMO - initial code
!
!
!-----------------------------------------------------------------------
Module ModInitTM

CONTAINS

  Subroutine LoadSolidData(region)

    USE ModGlobal
    USE ModDataStruct
    USE ModMPI
    USE ModIO
    USE ModPLOT3D_IO
    USE ModInput
    USE ModTMInt
    USE ModTMIO
    USE ModPETSc
    USE ModTMEOMBC
    USE ModTMEOM
    
    Implicit None

    Type(t_region), pointer :: region

    ! ... local variables
    Type(t_mixt_input), pointer :: input
    Type(t_sgrid), pointer :: TMgrid
    Type(t_smixt), pointer :: TMstate
    Character(LEN=2) :: prec, gf, vf, ib
    Integer :: ngrid, NDIM, ftype, alloc_stat, numGridsInFile
    Integer :: p, i, gs, ge, ng, dir, j, k, lg
    Integer, Dimension(:,:), Pointer :: ND
    Integer, pointer :: nnz_d(:), nnz_o(:), row_ptr(:), col_ptr(:)

    ! ... MPI
    Integer :: TMComm, TMRank
    Integer :: numTMproc, TMcolor

    ! ... simplicity
    input => region%input

    numTMproc = input%numTMproc

    if(region%myrank == 0) then
       write(*,'(A)') 'PlasComCM: ==> Initializing thermomechanical problem. <=='
    end if

    ! ... create the new communicator

    if(numTMproc > numproc) then
       call graceful_exit(region%myrank, 'PlasComCM: numTMproc > numproc. Exiting...')
    end if

    TMcolor = FALSE
    ! ... just include first numTMproc ranks from world
    ! ... communicator in worker group
   ! do p = 1, numTMproc
     if(region%myrank < numTMproc) then
        TMcolor = TRUE
     end if

    call MPI_BARRIER(mycomm,ierr)
    region%TMColor = TMColor

    ! ... split the communicator
    Call MPI_Comm_Split(mycomm, TMcolor, 0, TMComm, ierr)
    region%TMComm = TMComm
    
    ! ... get new ranks
    Call MPI_Comm_Rank(TMComm, TMRank, ierr)
    region%TMRank = TMRank
 
    ! ... flag: worker or idle
    region%TMWorker = TMcolor

    if(region%myrank == 0) then
       write(*,'(A,I3,A,I3,A)') 'PlasComCM: ==> Using ',numTMproc,' out of ',numproc,' processors in thermomechanical domain. <=='
    end if

    ! ... all non-thermomechanical procs wait at the end
    if(TMcolor == TRUE) then

       do p = 0, numTMproc-1
          if(TMRank == p) write(*,'(A,I3,A)') '   Rank ',region%myrank,' is in the thermomechanical communicator'
       end do

       ! ... decompose the domain
       IF((debug_on .eqv. .true.) .and. (TMRank == 0)) write(*,'(A)') 'PlasComCM: Decomposing solid domain.'
       Call DecomposeTMDomain(region)
       Call MPI_BARRIER(TMComm, ierr)
       IF((debug_on .eqv. .true.) .and. (TMRank == 0)) write(*,'(A)') 'PlasComCM: Decomposing solid domain done.'


       ! ... initial fill of grid%input 
       IF((debug_on .eqv. .true.) .and. (TMRank == 0)) write(*,'(A)') 'PlasComCM: Reading solid grid-wise input.'
       allocate(region%TMgrid(region%nTMGrids))
       allocate(region%TMstate(region%nTMGrids))
       Do ng = 1, region%nTMGrids
          TMgrid  => region%TMgrid(ng)
          TMState => region%TMstate(ng)
          allocate(TMgrid%input)
          input => TMgrid%input
          Call read_input(region, input, .false.)
          
          ! ... calculate timesteps
          TMstate%dt_struct = 0.0_rfreal
          TMstate%dt_thermal = 0.0_rfreal
          if(input%TMSolve > 1 .and. input%StructuralTimeScheme == DYNAMIC_SOLN) then
             ! ... if there is no fluid solution, the timestep in the input deck is dimensional
             TMstate%dt_struct = input%dt*dble(input%StructSolnFreq)
             ! ... otherwise,it is the non-dimensional fluid timestep, and must be dimensionalized
             if(input%TMOnly == FALSE) TMstate%dt_struct = input%dt*dble(input%StructSolnFreq)*input%LengRef/input%SndSpdRef
             if(TMRank == 0) write(*,'(A,D13.6,A)') 'PlasComCM: Structural timestep = ',TMstate%dt_struct,' s.'
          end if
          if((input%TMSolve == 1 .or. input%TMSolve == 3) .and. input%ThermalTimeScheme == DYNAMIC_SOLN) then
             TMstate%dt_thermal = input%dt*dble(input%ThermalSolnFreq)
             ! ... otherwise,it is the non-dimensional fluid timestep, and must be dimensionalized
             if(input%TMOnly == FALSE) TMstate%dt_thermal = input%dt*dble(input%ThermalSolnFreq)*input%LengRef/input%SndSpdRef
             if(TMRank == 0) write(*,'(A,D13.6,A)') 'PlasComCM: Thermal timestep = ',TMstate%dt_thermal,' s.'
          end if
          
          call AdjustSolnFreq(region,ng)
       End Do

       call mpi_barrier(TMComm, ierr)
       IF((debug_on .eqv. .true.) .and. (TMRank == 0)) write(*,'(A)') 'PlasComCM: Reading solid grid-wise input done.'

       ! ... set up the region
       IF((debug_on .eqv. .true.) .and. (TMRank == 0)) write(*,'(A)') 'PlasComCM: Initializing solid region.'
       Call initialize_TMregion(region,input%TMgrid_fname)
       call mpi_barrier(TMComm, ierr)
       IF((debug_on .eqv. .true.) .and. (TMRank == 0)) write(*,'(A)') 'PlasComCM: Initializing solid region done.'

       IF((debug_on .eqv. .true.) .and. (TMRank == 0)) write(*,'(A)') 'PlasComCM: Creating element table.'
       Call ElementTable(region)
       call mpi_barrier(TMComm, ierr)
       IF((debug_on .eqv. .true.) .and. (TMRank == 0)) write(*,'(A)') 'PlasComCM: Creating element table done.'

       ! ... set up send and recieve buffers
       IF((debug_on .eqv. .true.) .and. (TMRank == 0)) write(*,'(A)') 'PlasComCM: Setting up send and receive buffers.'
       Call SetUpExchange(region)
       call mpi_barrier(TMComm, ierr)
       IF((debug_on .eqv. .true.) .and. (TMRank == 0)) write(*,'(A)') 'PlasComCM: Setting up send and receive buffers done.'

       ! ... map local to global nodes
       IF((debug_on .eqv. .true.) .and. (TMRank == 0)) write(*,'(A)') 'PlasComCM: Creating local to global node map.'
       Call LocaltoGlobal(region)
       call mpi_barrier(TMComm, ierr)
       IF((debug_on .eqv. .true.) .and. (TMRank == 0)) write(*,'(A)') 'PlasComCM: Creating local to global node map done.'

       ! ... initialize PETSc and allocate arrays
       IF((debug_on .eqv. .true.) .and. (TMRank == 0)) write(*,'(A)') 'PlasComCM: Initializing PETSc.'
       Call SetUpLinearSolver(region)
       IF((debug_on .eqv. .true.) .and. (TMRank == 0)) write(*,'(A)') 'PlasComCM: Initializing PETSc done.'
       
       !        do p = 0, input%numTMproc-1
       !           if(TMRank == p) then
       !              do ng = 1, region%nTMGrids
       !                 do i = 1, size(region%TMgrid(ng)%L2G)
       !                    print*,'Rank ',p,' local node ',i,' is global node', region%TMgrid(ng)%L2G(i)
       !                 end do
       !              end do
       !              call f90flush()
       !              call MPI_BARRIER(TMComm, ierr)
       !           end if
       !       end do

       IF((debug_on .eqv. .true.) .and. (TMRank == 0)) write(*,'(A)') 'PlasComCM: Reading solid grids.'
       ! ... now read in grids, third optional argument == TMFlag
       do i = 1, region%nTMGridsGlobal

          ! ... only read if TMgrid%iGridGlobal == i
          Call Read_and_distribute_single_TMgrid(region,i)

          ! ... have everyone wait to save memory
          Call MPI_BARRIER(TMComm, IERR)
       end do
       IF((debug_on .eqv. .true.) .and. (TMRank == 0)) write(*,'(A)') 'PlasComCM: Reading solid grids done.'

       ! ... QUADRATIC ELEMENTS: set up map from local nodes to plot3D nodes
       IF((debug_on .eqv. .true.) .and. (TMRank == 0)) write(*,'(A)') 'PlasComCM: Creating local to solution file node map.'
       Call LocaltoP3D(region)
       Call P3DElementTable(region)
       Call LM2DtoPatchP3D(region)
       call mpi_barrier(TMComm, ierr)
       IF((debug_on .eqv. .true.) .and. (TMRank == 0)) write(*,'(A)') 'PlasComCM: Creating local to solution file node map done.'


       ! ... set up dirichlet boundary conditions
       IF((debug_on .eqv. .true.) .and. (TMRank == 0)) write(*,'(A)') 'PlasComCM: Initializing Dirichlet BCs.'
       Call SetDirichletDofs(region)

       ! ... create a vector to filter out Dirichlet nodes for calculation of residual
       if(region%input%TMSolve /= 1) then
          do ng = 1, region%nTMGrids
             TMstate => region%TMstate(ng)
             Call CreateBCZeros(region,ng)
          end do
       end if

       call mpi_barrier(TMComm, ierr)
       IF((debug_on .eqv. .true.) .and. (TMRank == 0)) write(*,'(A)') 'PlasComCM: Initializing Dirichlet BCs done.'


       ! ... initialize the stat
       IF((debug_on .eqv. .true.) .and. (TMRank == 0)) write(*,'(A)') 'PlasComCM: Initializing the thermomechanical state.'
       Call InitializeTMState(region,region%input%TMgrid_fname)
       call mpi_barrier(TMComm, ierr)
       IF((debug_on .eqv. .true.) .and. (TMRank == 0)) write(*,'(A)') 'PlasComCM: Initializing the thermomechanical state done.'

       !       ! ... initialize 
       !       IF((debug_on .eqv. .true.) .and. (TMRank == 0)) write(*,'(A)') 'PlasComCM: Write initial thermomechanical state.'
       !       Call TMwrite_plot3d_file(region, 'TMq', 0)
       !       call mpi_barrier(TMComm, ierr)
       !       IF((debug_on .eqv. .true.) .and. (TMRank == 0)) write(*,'(A)') 'PlasComCM: Write initial thermomechanical state done.'

       ! ... set up the shape functions
       IF((debug_on .eqv. .true.) .and. (TMRank == 0)) write(*,'(A)') 'PlasComCM: Getting shape functions.'
       Call GetShpFcn(region)
       Call GetdShpFcn(region)
       Call GetBndShpFcn(region)
       Call GetdBndShpFcn(region)
       call mpi_barrier(TMComm, ierr)
       IF((debug_on .eqv. .true.) .and. (TMRank == 0)) write(*,'(A)') 'PlasComCM: Getting shape functions done.'

       ! ... temperature dependent mechanical properties
       If(input%TempDpndMtl == TRUE) then
          call SetUpMaterialLookupTable(region)
       end if

       do ng = 1, region%nTMGrids
          ! ... initial fill of material property vectors
          ! ... Youngs modulus, Poisson's ratio
          !print*, 'getting material properties'
          Call getMaterialProps(region, ng)
       end do

       ! ... do we want to record useful information (power in, kinetic energy)
       if(input%SaveSolidData == TRUE) Call SetupSolidDataRecord(region)
       
       ! do ng = 1, region%nTMGrids
       
       !   do i = 1, region%TMgrid(ng)%nPtsLoc
       !      region%TMstate(ng)%q(i,1) = region%TMstate(ng)%YMod(i)
       !      region%TMstate(ng)%q(i,2) = region%TMstate(ng)%PRat(i)
       !   end do
       !end do
          
          call TMwrite_plot3d_file(region, 'TMq',region%input%nstepi)
          

    end if ! ... TMcolor == TRUE

 

    Call MPI_BARRIER(mycomm, ierr)


    !call graceful_exit(region%myrank, 'Done with thermomechanical processor set up')

  end Subroutine LoadSolidData


  Subroutine DecomposeTMDomain(region)

    USE ModGlobal
    USE ModDataStruct
    USE ModMPI
    USE ModIO
    USE ModPLOT3D_IO

    Implicit None

    Type(t_region), pointer :: region

    ! ... local variables
    Type(t_mixt_input), pointer :: input
    Integer :: NDIM, numGridsInFile
    Integer :: i, gs, ge, NumEl
    Integer, Dimension(:,:), Pointer :: ND

    ! ... MPI
    Integer :: TMRank, TMComm
    Integer :: numTMproc

    ! ... simplicity
    input => region%input
    numTMproc = input%numTMproc
    TMRank = region%TMRank
    TMComm = region%TMComm


    ! ... read the solid grid size to determine partitioning
    call ReadGridSize(NDIM, numGridsInFile, ND, input%TMgrid_fname, 1)

    ! ... total number of solid grids in file
    region%nTMGridsGlobal = numGridsInFile

    ! ... total size of each grid
    allocate(region%AllTMGridsGlobalSize(size(ND,1),MAX(3,size(ND,2)))); region%AllTMGridsGlobalSize(:,:) = 1;
    region%AllTMGridsGlobalSize(1:size(ND,1),1:size(ND,2)) = ND(:,:)

    ! ... compute the number of preocessors each grid will get
    allocate(region%TMgridToProcMap(numGridsInFile,2));


    ! ... allocate grid and some of its arrays
    If (numGridsInFile >= numTMproc) Then

       ! ... in this case there are at least as many grids as processors
       ! ... each grid will have its own communicator, but that should never be used

       Do i = 0, numTMproc-1
          Call MPE_DECOMP1D(numGridsInFile, numTMproc, i, gs, ge )
          region%TMgridToProcMap(gs:ge,:) = i
          if ( TMrank == i ) region%nTMGrids = ge - gs + 1
       End Do

    Else

       Call TMLoad_Balance(numTMproc, numGridsInFile, ND, region%TMgridToProcMap, TMRank, input)

       ! ... search through TMgridToProcMap to find this processor's data
       region%nTMGrids = 0
       Do i = 1, numGridsInFile
          if (TMRank >= region%TMgridToProcMap(i,1) .and. TMRank <= region%gridToProcMap(i,2)) region%nTMGrids = region%nTMGrids + 1
       End Do

    end If ! ... numGridsInFile >= numTMproc

    do i = 0, numTMproc-1
       if ( TMRank == i ) write (*,'(A,I3,A,I3,A)') 'PlasComCM: Processor ', TMRank, ' has ', region%nTMGrids, ' solid grids.'
       Call MPI_BARRIER(TMComm,ierr)
    end do

    if ( TMRank == 0 ) then
       write (*,'(A)') ''
       write (*,'(A)') 'PlasComCM: --------------------------------------------------------------------------'
    end if

    Do i = 0, numGridsInFile-1
       if (TMRank == 0) write (*,'(3(A,I4),A)') 'PlasComCM: ==> Solid grid ', i+1, ' to be distributed to processors ', region%TMgridToProcMap(i+1,1), ' through ', region%TMgridToProcMap(i+1,2), '. <=='
    End Do

    if ( TMRank == 0 ) then
       write (*,'(A)') 'PlasComCM: --------------------------------------------------------------------------'
       write (*,'(A)') ''
    end if

    Deallocate(ND)

  end Subroutine DecomposeTMDomain


  subroutine TMLoad_Balance(numTMproc, numGridsInFile, ND, TMgridToProcMap, TMRank, input)

    USE ModGlobal
    USE ModDataStruct
    USE ModMPI

    Implicit None

    ! ... function values
    Integer :: numTMproc, numGridsInFile, TMRank
    Integer, Pointer :: ND(:,:), TMgridToProcMap(:,:)
    Type (t_mixt_input), pointer :: input

    ! ... local values
    integer :: p(numGridsInFile), sumN, i, sumP
    integer :: indGrid, numDecomp(MAX_ND), io_err
    logical :: mf_exists

    ! ... get initial mapping

    ! ... add up all of the points
    sumN = 0
    do i = 1, numGridsInFile
       sumN = sumN + product(ND(i,:))
    end do

    ! ... take the simple least squares approach, and round
    do i = 1, numGridsInFile
       p(i) = ceiling(dble(product(ND(i,:)))*dble(numproc)/dble(sumN))
    end do

    ! ... check to see if we match the constant
    i = 1
    do while (sum(p(:)) /= numTMproc)
       if (p(i) /= 1) p(i) = p(i) - 1
       i = i + 1
       if (i == numGridsInFile+1) i = 1 ! start over in case more iterations are needed (D. Buctha)
    end do

    ! ... update map based on data provided by user
    mf_exists = .FALSE.
    if (input%useTMDecompMap == TRUE) then
       inquire(file=trim(input%TMdecompMap), exist=mf_exists)
       if (.NOT. mf_exists) call graceful_exit(region%myrank, "ERROR: domain-decomposition info cannot be found.")
    end if ! input%useDecompMap
    if (mf_exists .and. (input%TMparallelTopology == CUBE .and. numTMproc > 1)) then

       if (TMRank == 0) write (*,'(A)') 'PlasComCM: Using user-supplied processor map "'//trim(input%TMdecompMap)//'".'

       ! ... read domain-decomposition map
       ! ... if error occurs while reading, we do nothing, haha! -_-a
       open(66, file=trim(input%TMdecompMap)); io_err = 0
       Do while (io_err == 0) 
          read(66,*,IOSTAT=io_err) indGrid, numDecomp(1:MAX_ND)
          p(indGrid) = PRODUCT(numDecomp(:))
       end do ! i
       close(66)

       if (SUM(p(:)) /= numTMproc) &
            call graceful_exit(region%myrank, "... total number of decomposition provided by " &
            //trim(input%TMdecompMap)//" different from numproc.")

    End if


    ! ... fill up the gridToProcMap array
    TMgridToProcMap(1,1) = 0
    TMgridToProcmap(1,2) = p(1)-1
    do i = 2, numGridsInFile
       TMgridToProcMap(i,1) = TMgridToProcMap(i-1,2) + 1
       TMgridToProcMap(i,2) = TMgridToProcMap(i,1) + (p(i) - 1)
    end do

    return

  end subroutine TMLoad_Balance


  Subroutine initialize_TMregion(region, restart_fname)

    USE ModGlobal
    USE ModDataStruct
    USE ModInput
    USE ModMPI
    USE ModIO
    USE ModPLOT3D_IO
    USE ModInitialCondition
    USE ModEOS

    Implicit None

    Type (t_region), pointer :: region
    Character(LEN=PATH_LENGTH) :: restart_fname

    ! ... local variables
    integer :: NDIM, ngrid, parDir_loc(1), ps, pe, i, j, ng, p, dir
    integer :: numGridsInFile, gs, ge, world_group, grid_group, Ntotal, color, comm, core_group, alpha, num_recv, error_local, error_global
    integer, allocatable :: ranks(:)
    integer, pointer :: ND(:,:), gridToProcMap(:,:), ibuf(:)
    integer :: N(MAX_ND)
    Type (t_mixt_input), pointer :: input
    Type (t_sgrid), pointer :: TMgrid
    Logical :: left_bc, right_bc

    ! ... MPI
    integer :: TMRank, TMComm, numTMproc

    TMComm = region%TMComm
    TMRank = region%TMRank
    numTMproc = region%input%numTMproc

    ! ... fill the grid array
    do ng = 1, region%nTMGrids

       ! ... this grid
       TMgrid => region%TMgrid(ng)
       input  => TMgrid%input

       ! ... total number of grids in file
       TMgrid%numGridsInFile = region%nTMGridsGlobal

       ! ... total size of each grid
       allocate(TMgrid%AllGridsGlobalSize(size(region%AllTMGridsGlobalSize,1),size(region%AllTMGridsGlobalSize,2))); TMgrid%AllGridsGlobalSize(:,:) = 1;
       TMgrid%AllGridsGlobalSize(:,:) = region%AllTMGridsGlobalSize(:,:)

       ! ... mapping of grids to processors
       allocate(TMgrid%gridToProcMap(region%nTMGridsGlobal,2))
       TMgrid%gridToProcMap(:,:) = region%TMgridToProcMap(:,:)

       ! ... dimension of the problem
       TMgrid%ND = input%ND

       ! ... nullify
       nullify(TMgrid%XYZ);  

       ! ... save world communicator
       TMgrid%commWorld = region%TMComm
       TMgrid%comm      = MPI_COMM_NULL

       ! ... zero out for later
       TMgrid%iGridGlobal = 0

       ! ... save parallelization topology
       TMgrid%parallelTopology = input%parallelTopology

    end do


    ! ... save the original grid ID for this processor's grid
    ! ... save the core processor in control of this grid
    Loop1: Do i = 1, region%nTMGridsGlobal
       Loop2: Do ng = 1, region%nTMGrids
          TMgrid => region%TMgrid(ng)
          if ( TMRank >= region%TMgridToProcMap(i,1) .and. TMRank <= region%TMgridToProcMap(i,2).and. TMgrid%iGridGlobal == 0) then
             TMgrid%iGridGlobal = i
             TMgrid%coreRank_inComm = region%TMgridToProcMap(i,1)
             EXIT Loop2
          end if
       End Do Loop2
    End Do Loop1

    ! ... world group
    Call MPI_Comm_Group(TMComm, world_group, ierr)

    ! ... create the communicator for each grid
    Do i = 1, region%nTMGridsGlobal
       Ntotal = region%TMgridToProcMap(i,2) - region%TMgridToProcMap(i,1) + 1
       allocate( ranks(Ntotal) )
       do j = 1, Ntotal
          ranks(j) = region%gridToProcMap(i,1) + (j-1)
       end do
       Call MPI_Group_incl(world_group, Ntotal, ranks, grid_group, ierr)
       Call MPI_Comm_Create(TMComm, grid_group, comm, ierr)
       Do ng = 1, region%nTMGrids
          TMgrid => region%TMgrid(ng)
          if (TMgrid%iGridGlobal == i) TMgrid%comm = comm
       End Do
       deallocate(ranks)
       Call MPI_Group_Free(grid_group, ierr)
    End Do

    ! ... compute local rank and numproc for each grid comm
    Do ng = 1, region%nTMGrids
       TMgrid => region%TMgrid(ng)
       input => TMgrid%input

       Call MPI_Comm_size( TMgrid%comm, TMgrid%numproc_inComm, ierr )
       Call MPI_Comm_rank( TMgrid%comm, TMgrid%myrank_inComm, ierr )

    End Do

    ! ... assign specific grid points
    Do ng = 1, region%nTMGrids

       ! ... work on grid ng of this processor
       TMgrid  => region%TMgrid(ng)
       input => TMgrid%input

       ! ... initialize (is,ie) pairs
       TMgrid%is(:) = 1; 
       TMgrid%ie(:) = 1

       if (input%TMparallelTopology == SLAB) then

          ! ... which dir should we parallelize in?
          if (input%TMparDir == DEFAULT_VALUE) then
             If (TMgrid%numproc_inComm > 1) Then
                parDir_loc = MAXLOC(region%AllTMGridsGlobalSize(TMgrid%iGridGlobal,:))
                TMgrid%parDir = parDir_loc(1)
             else
                TMgrid%parDir = FALSE
             end if
          else
             If (TMgrid%numproc_inComm > 1) then
                TMgrid%parDir = input%TMparDir
             else
                TMgrid%parDir = FALSE
             end if
          end if

          If ( TMRank == TMgrid%coreRank_inComm ) Then
             If ( TMgrid%parDir /= FALSE ) write (*,'(A,I3,A,I1,A)') 'PlasComCM: ==> Parallelizing solid grid ', TMgrid%iGridGlobal, ' in direction ', TMgrid%parDir, '. <=='
          End If

          ! ... get local indices
          call TMassign_local_indices_cube(region%AllTMGridsGlobalSize, TMgrid, input, ng)

       else

          call TMassign_local_indices_cube(region%AllTMGridsGlobalSize, TMgrid, input, ng)

       end if

       !       do i = 0, numTMproc

       !          if(TMRank == i) then
       !             print*,TMRank,region%TMgrid(1)%cartCoords,TMgrid%is,TMgrid%ie
       !             print*,'   '
       !          end if
       !          call MPI_BARRIER(TMComm, ierr)
       !       end do

       ! ... count the number of cells in this grid
       TMgrid%nCells = 1;
       do j = 1, TMgrid%ND
          TMgrid%nCells = TMgrid%nCells * (TMgrid%ie(j)-TMgrid%is(j)+1)
       end do

       ! ... allocate space for ghost cell information
       ! ... these data are set in read_and_distribute_single_grid
       allocate(TMgrid%nGhostRHS(TMgrid%ND,2));
       allocate(TMgrid%nGhostLHS(TMgrid%ND,2));
       do i = 1, TMgrid%ND
          do j = 1, 2
             TMgrid%nGhostRHS(i,j) = 0
             TMgrid%nGhostLHS(i,j) = 0
          end do
       end do

       ! ... allocate space for grid points, metrics, ...
       allocate(TMgrid%XYZ(TMgrid%nCells,TMgrid%ND))
       allocate(TMgrid%IBLANK(TMgrid%nCells))
       !       nullify(grid%JAC)
       !       nullify(grid%INVJAC)
       !       nullify(grid%XI_TAU)
       !       nullify(grid%XYZ_TAU)
       !       nullify(grid%XYZ_TAU2)
       !       nullify(grid%XI_TAU_XI)
       !       nullify(grid%INVJAC_TAU)
       !       nullify(grid%MT1)
       !       nullify(grid%MT2)
       !       nullify(grid%ident)
       !       nullify(grid%area)

       !       ! ... decide on metric type
       !       grid%metric_type = input%metricType
       !       if (grid%metric_type == CARTESIAN)                write (*,'(2(A,I3),A)') 'PlasComCM: Grid ', ng, ' on processor ', region%myrank, ' is CARTESIAN.'
       !       if (grid%metric_type == NONORTHOGONAL_STRONGFORM) write (*,'(2(A,I3),A)') 'PlasComCM: Grid ', ng, ' on processor ', region%myrank, ' is NONORTHOGONAL_STRONGFORM.'
       !       if (grid%metric_type == NONORTHOGONAL_WEAKFORM)   write (*,'(2(A,I3),A)') 'PlasComCM: Grid ', ng, ' on processor ', region%myrank, ' is NONORTHOGONAL_WEAKFORM.'

       !       ! ... decide on derivative type
       !       allocate(grid%deriv_type(grid%ND))
       !       grid%deriv_type(:) = input%spaceDiscr

       !       ! ... decide on filter type
       !       allocate(grid%filter_type(grid%ND))
       !       grid%filter_type(:) = input%filterDiscr

       !       ! ... moving grids
       !       grid%moving = FALSE
       !       If (input%moveGrid == TRUE) grid%moving = TRUE


    End Do

    ! ... save global grid size
    Do ng = 1, region%nTMGrids
       TMgrid => region%TMgrid(ng)
       allocate(TMgrid%GlobalSize(MAX_ND))
       TMgrid%GlobalSize(:) = 1
       TMgrid%GlobalSize(1:TMgrid%ND) = region%AllTMGridsGlobalSize(TMgrid%iGridGlobal,1:TMgrid%ND)
    End Do

    ! ... report on sizes
    do i = 1, region%nTMGridsGlobal
       do ng = 1, region%nTMGrids
          TMgrid => region%TMgrid(ng)
          if (TMgrid%iGridGlobal == i) then
             do p = 0, TMgrid%numproc_inComm-1
                if (TMgrid%myrank_inComm == p) then
                   write (*,'(A,I4,A,I4,A,I8,A)') 'Solid processor ', TMRank, ' on solid grid ', i, ' has ', TMgrid%nCells, ' nodes'
                   call f90flush()
                end if
                call mpi_barrier(TMgrid%comm, ierr)
             end do
          end if
       end do
       call mpi_barrier(TMComm, ierr)
    end do

    !     ! ... read boundary information and set up the patches
    call Set_Up_TMBoundary_Patches(region)
    call mpi_barrier(TMComm, ierr)

    ! ... establish what grids each processor holds and collect this information
    allocate(region%TMgrid_to_processor(region%nTMGridsGlobal))
    do i = 1, region%nTMGridsGlobal      
       region%TMgrid_to_processor(i) = region%TMgridToProcMap(i,1)
    end do

    ! ... establish core rank to grid mapping
    allocate(region%TMgrid_to_processor_coreComm(region%nTMGridsGlobal), ibuf(region%nTMGridsGlobal))
    region%TMnum_coreRank_this_processor = 0
    do ng = 1, region%nTMGrids
       TMgrid => region%TMgrid(ng)
       if ( TMgrid%myrank_inComm == 0 ) then
          region%TMnum_coreRank_this_processor = region%TMnum_coreRank_this_processor + 1
          region%TMgrid_to_processor_coreComm(region%TMnum_coreRank_this_processor) = region%TMRank_inCoreComm
       end if
    end do
    if ( TMRank == 0 ) then
       !region%grid_to_processor_coreComm(1:region%num_coreRank_this_processor) = ibuf(1:region%num_coreRank_this_processor)
       alpha = region%TMnum_coreRank_this_processor
       do p = 1, numTMproc-1
          call mpi_recv(num_recv, 1, MPI_INTEGER, p, p, TMComm, status, ierr)
          call mpi_recv(ibuf, num_recv, MPI_INTEGER, p, numTMproc + p, TMComm, status, ierr)
          region%TMgrid_to_processor_coreComm(alpha+1:alpha+num_recv) = ibuf(1:num_recv)
          alpha = alpha + num_recv
       end do
    else
       call mpi_send(region%TMnum_coreRank_this_processor, 1, MPI_INTEGER, 0, TMRank, TMComm, ierr)
       call mpi_send(region%TMgrid_to_processor_coreComm, region%TMnum_coreRank_this_processor, MPI_INTEGER, 0, numTMproc+TMRank, TMComm, ierr)
    end if
    call mpi_bcast(region%TMgrid_to_processor_coreComm, region%nTMGridsGlobal, MPI_INTEGER, 0, TMComm, ierr)

    ! ... output running decomposition
    Do i = 1, region%nTMGridsGlobal
       Do ng = 1, region%nTMGrids
          TMgrid => region%TMgrid(ng)
          If (TMgrid%iGridGlobal == i) Then
             Do p = 0, TMgrid%numproc_inComm
                If (TMgrid%myrank_inComm == p) Then
                   If (p == 0 .and. i == 1) then
                      open (unit=10, file='PlasComCMTM.map', status='unknown')
                   else
                      open (unit=10, file='PlasComCMTM.map', status='unknown', position='append')
                   End If
                   write (10,'(8(I5,1X))') TMRank, TMgrid%iGridGlobal, (TMgrid%is(j), TMgrid%ie(j), j = 1, MAX_ND)
                   close (10)
                End If
                Call MPI_Barrier(TMgrid%comm, ierr)
             End Do
          End If
       End Do
       Call MPI_Barrier(TMComm, ierr)
    End Do

  End Subroutine initialize_TMregion


  subroutine TMassign_local_indices_cube(ND, grid, input, ng)

    USE ModGlobal
    USE ModDataStruct
    USE ModMPI

    Implicit None

    type(t_sgrid), pointer :: grid
    integer, pointer :: ND(:,:)
    type(t_mixt_input), pointer :: input
    integer :: ng

    ! ... local vars
    integer :: i, is, ie, ndims, dir
    logical :: isperiodic(MAX_ND), reorder, dims(MAX_ND)
    integer :: indGrid, numDecomp(MAX_ND), io_err, NumEl
    logical :: mf_exists

    ! ... initialize
    do i = 1, input%ND
       grid%is(i) = 1
       grid%ie(i) = ND(grid%iGridGlobal,i)
    end do

    ! ... use MPI_DIMS_CREATE to determine the decomposition
    grid%cartDims(1:grid%ND) = 0
    grid%cartCoords(:) = 0

    ! ... if slab (1-D) we parallize in direction parDir
    if (grid%parallelTopology == SLAB .and. grid%numproc_inComm > 1) then
       grid%cartDims(grid%parDir) = grid%numproc_inComm

       ! ... if cube (N-D) check for decomposition file
    else if (grid%parallelTopology == CUBE .and. grid%numproc_inComm > 1) then
       ! ... read domain-decomposition map
       ! ... if error occurs while reading, we do nothing, haha! -_-a
       mf_exists = .FALSE.
       if (input%useTMDecompMap == TRUE) inquire(file=trim(input%TMdecompMap), exist=mf_exists)
       if (mf_exists) then
          open(66, file=trim(input%TMdecompMap)); io_err = 0
          Do while (io_err == 0) 
             read(66,*,IOSTAT=io_err) indGrid, numDecomp(1:MAX_ND)
             if (indGrid == grid%iGridGlobal) grid%cartDims(:) = numDecomp(:)
          end do ! i
          close(66)
       end if ! mf_exists
    end if ! grid%parallelTopology

    ! ... decompose
    call MPI_DIMS_CREATE(grid%numproc_inComm, grid%ND, grid%cartDims, ierr)

    ! ... initialize data for MPI_CART_CREATE
    ! ... never periodic in solid domain
    isperiodic(:) = .FALSE.
    reorder = .TRUE.

    ! ... use MPI_CART_CREATE to get the cell decomposition
    Call MPI_CART_CREATE(grid%comm, grid%ND, grid%cartDims(1:grid%ND), isperiodic(1:grid%ND), reorder, grid%cartComm, ierr)

    ! ... retain basic information about Deriv communicators
    Call MPI_Comm_size(grid%cartComm, grid%numproc_inCartComm, ierr)
    Call MPI_Comm_rank(grid%cartComm, grid%myrank_inCartComm, ierr)
    Call MPI_Cart_Coords(grid%cartComm, grid%myrank_inCartComm, grid%ND, grid%cartCoords, ierr)

    ! ... loop over the directions to get neighbors
    do dir = 1, grid%ND
       Call MPI_Cart_Shift(grid%cartComm, dir-1, 1, grid%left_process(dir), grid%right_process(dir), ierr)
    end do

    ! ... now loop over the directions to get our size
    do dir = 1, grid%ND

       !if((ND(grid%iGridGlobal,dir)/2)*2 == ND(grid%iGridGlobal,dir) .and. grid%ElemType == QUADRATIC) call graceful_exit(region%myrank, 'PlasComCM: Solid grid indices must be odd. Exiting...')       

       if(grid%numproc_inComm > 1) then
          if(input%ElemType == QUADRATIC) then
             ! ... quadratic elements
             NumEl =  (ND(grid%iGridGlobal,dir)-1)/2
          elseif(input%ElemType == LINEAR) then
             ! ... linear elements
             NumEl =  (ND(grid%iGridGlobal,dir)-1)
          end if

          if(grid%cartDims(dir) > NumEl) then
             if(grid%myrank_inComm == 0) write(*,'(A,I1,A)') 'PlasComCM: Number of elements exeeded by number of processors in direction ',dir,'.'
             call graceful_exit(region%myrank, 'PlasComCM: Exiting...')       
          end if

          call MPE_Decomp1D(NumEl, grid%cartDims(dir), grid%cartCoords(dir), grid%is(dir), grid%ie(dir))

          if(input%ElemType == QUADRATIC) then
             ! ... quadratic elements
             grid%is(dir) = (grid%is(dir)-1)*2+1
             grid%ie(dir) = (grid%ie(dir))*2+1
          elseif(input%ElemType == LINEAR) then
             ! ... linear elements
             grid%is(dir) = (grid%is(dir)-1)+1
             grid%ie(dir) = (grid%ie(dir))+1
          end if
       end if
    end do


  end subroutine TMassign_local_indices_cube


  subroutine Set_Up_TMBoundary_Patches(region)

    USE ModGlobal
    USE ModDataStruct
    USE ModMPI

    Implicit None

    Type (t_region), pointer :: region

    ! ... local variables
    Type (t_sgrid), pointer :: TMgrid
    Type (t_mixt_input), pointer :: input
    Type (t_spatch), pointer :: TMpatch
    Integer :: ng, j, i, npatches, nbc, io_err, ND, N(MAX_ND), dir, ei, ej, ek, le, lpe
    Integer :: Npe(MAX_ND), Ne(MAX_ND), Npee(MAX_ND), Npes(MAX_ND), Nes(MAX_ND), Nee(MAX_ND)
    Logical :: left_bc, right_bc, bc(MAX_ND)
    Integer, Parameter :: bc_unit = 51
    INTEGER :: signdir, bdir, normDir, sgn, FaceNum, ElemType
    Character(LEN=100) :: strbuf
    Integer :: tmp_bc_data(9), k, prodN, l
    Integer, pointer :: iPatchGlobal(:), gridID(:), LM(:,:)
    Real(rfreal) ::  BCValTmp(12)


    ! ... MPI
    INTEGER :: TMComm, TMRank, numTMproc


    TMComm = region%TMComm
    numTMproc = region%input%numTMproc
    TMRank = region%TMRank

    ! ... simplicity
    input => region%input
    ND = input%ND

    ! ... step one : read the entire bc file
    Open (unit=bc_unit, file=trim(input%TMbc_fname), status='old', iostat=io_err)
    if (io_err /= 0) then
      call graceful_exit(region%myrank, 'ERROR: Could not find/open thermomechanical BC file "' // &
        trim(input%TMbc_fname) // '".')
    end if

    nbc = 0; io_err = FALSE
    Do while (io_err == FALSE)
       Read (bc_unit,'(A)',iostat=io_err) strbuf
       If ((io_err == FALSE) .AND. (strbuf(1:1) /= '#') .AND. (len_trim(strbuf) /= 0)) Then
          Read (strbuf,*) tmp_bc_data(:)
          If (abs(tmp_bc_data(3)) <= ND) nbc = nbc + 1
       End If
    End Do
    Rewind(bc_unit)

    ! ... allocate master boundary condition array
    allocate(input%TMbc_data(nbc,9))
    allocate(input%TMbc_Val(nbc,3))

    ! ... reread
    nbc = 0; io_err = FALSE
    Do while (io_err == FALSE)
       Read (bc_unit,'(A)',iostat=io_err) strbuf
       If ((io_err == FALSE) .AND. (strbuf(1:1) /= '#') .AND. (len_trim(strbuf) /= 0)) Then
          Read (strbuf,*) tmp_bc_data(:)
          If (abs(tmp_bc_data(3)) <= ND) Then
             nbc = nbc + 1
             Read (strbuf,*) input%TMbc_data(nbc,:)
             Read (strbuf,*) BCValTmp(:)
             input%TMbc_Val(nbc,1:3) = BCValTmp(10:12)
          End If
       End If
    End Do
    Close (bc_unit)

    If (TMRank == 0) Write (*,'(A,I3,3(A))') &
         'PlasComCM: Found ', nbc, ' boundary conditions in "', trim(input%TMbc_fname), '"'

    allocate(iPatchGlobal(nbc), gridID(nbc))

    ! ... count how many patches we have to carry
    region%nTMPatches = 0; bc(:) = .FALSE.
    Do ng = 1, region%nTMGrids

       TMgrid => region%TMgrid(ng)
       ND = TMgrid%ND

       Call SetElemFaceInformation(TMgrid)

       Do i = 1, nbc

          ! ... if entries are negative, adjust
          Do j = 0, 2*ND-1, 2
             dir = j/2 + 1

             If (TMgrid%iGridGlobal == input%TMbc_data(i,1)) Then
                If (input%TMbc_data(i,4+j) < 0) &
                     input%TMbc_data(i,4+j) = TMgrid%GlobalSize(dir) + input%TMbc_data(i,4+j) + 1

                If (input%TMbc_data(i,5+j) < 0) &
                     input%TMbc_data(i,5+j) = TMgrid%GlobalSize(dir) + input%TMbc_data(i,5+j) + 1
             End If

          End Do

       End Do

       Do i = 1, nbc

          If (TMgrid%iGridGlobal == input%TMbc_data(i,1)) Then

             ! ... see if this grid contains the (*s,*e) indices
             Do j = 0, 2*ND-1, 2
                dir = j/2 + 1

                If ((input%TMbc_data(i,4+j) <= TMgrid%ie(dir)) .and. &
                     (input%TMbc_data(i,5+j) >= TMgrid%is(dir))) Then
                   bc(dir) = .TRUE.
                End If

             End Do

             If (ALL(bc(1:ND))) Then
                region%nTMPatches = region%nTMPatches + 1
                iPatchGlobal(region%nTMPatches) = i
                gridID(region%nTMPatches) = ng
             End If

             bc = .FALSE.

          End If
       End Do
    End Do
    call mpi_barrier(TMComm, ierr)

    ! ... allocate the memory for the patches
    allocate(region%TMpatch(region%nTMPatches))

    ! ... save the patch data
    do i = 1, region%nTMPatches

       TMgrid => region%TMgrid(gridID(i))
       TMpatch => region%TMpatch(i)

       TMpatch%gridID    = gridID(i) !input%bc_data(iPatchGlobal(i),1)
       TMpatch%iPatchGlobal = iPatchGlobal(i)
       TMpatch%bcCoupled = FALSE
       TMpatch%bcType    = input%TMbc_data(iPatchGlobal(i),2)
       TMpatch%bcVal(:)  = input%TMbc_Val(iPatchGlobal(i),:)
       TMpatch%normDir   = input%TMbc_data(iPatchGlobal(i),3)
       signdir = TMpatch%normDir/abs(TMpatch%normDir)
       bdir = TMpatch%normDir

       allocate(TMpatch%is(MAX_ND)); TMpatch%is = 1;
       allocate(TMpatch%ie(MAX_ND)); TMpatch%ie = 1;

       allocate(TMpatch%bcData(size(input%TMbc_data,2)))
       TMpatch%bcData(:) = input%TMbc_data(iPatchGlobal(i),:)

       N(:) = 1
       prodN = 1

       Do j = 0, 2*ND-1, 2

          dir = j/2 + 1
          TMpatch%is(dir) = MIN(MAX(input%TMbc_data(iPatchGlobal(i),4+j),TMgrid%is(dir)),TMgrid%ie(dir))
          TMpatch%ie(dir) = MAX(MIN(input%TMbc_data(iPatchGlobal(i),5+j),TMgrid%ie(dir)),TMgrid%is(dir))

          if (TMpatch%ie(dir) .lt. TMpatch%is(dir)) then
             print *, iPatchGlobal(i)
             print *, TMgrid%globalsize(1:2)
             print *, 'PlasComCM: error on TMRank = ', TMRank
             print *, TMRank, TMpatch%gridID, dir, input%TMbc_data(iPatchGlobal(i),4+j), TMgrid%is(dir)
             print *, TMRank, TMpatch%gridID, dir, input%TMbc_data(iPatchGlobal(i),5+j), TMgrid%ie(dir)
             stop
          end if

          N(dir) = TMpatch%ie(dir) - TMpatch%is(dir) + 1
          prodN = prodN * N(dir)

       End Do

       ! ... save prodN
       TMpatch%prodN = prodN

       TMpatch%normIndex = TMpatch%ie(abs(TMpatch%normDir))
       if (TMpatch%normDir == abs(TMpatch%normDir)) TMpatch%normIndex = TMpatch%is(abs(TMpatch%normDir))


       If (TMpatch%bctype == STRUCTURAL_INTERACTING) THEN

          allocate(TMpatch%local_patch_extent(6))
          allocate(TMpatch%global_patch_extent(6))
          TMpatch%local_patch_extent(1)=TMpatch%is(1)
          TMpatch%local_patch_extent(2)=TMpatch%ie(1)
          TMpatch%local_patch_extent(3)=TMpatch%is(2)
          TMpatch%local_patch_extent(4)=TMpatch%ie(2)
          TMpatch%local_patch_extent(5)=TMpatch%is(3)
          TMpatch%local_patch_extent(6)=TMpatch%ie(3)

          Do j = 0, 2*ND-1, 2
             dir = j/2 + 1
             if (dir /= abs(TMpatch%normDir)) Then
                TMpatch%global_patch_extent(2*dir-1) = input%TMbc_data(iPatchGlobal(i),4+j)
                TMpatch%global_patch_extent(2*dir)   = input%TMbc_data(iPatchGlobal(i),5+j)
             Else
                TMpatch%global_patch_extent(2*dir-1) = input%TMbc_data(iPatchGlobal(i),4+j)
                TMpatch%global_patch_extent(2*dir)   = input%TMbc_data(iPatchGlobal(i),5+j)
             End If
          END DO
       end If

    end do ! ... nTMPatches

    
                
    if (TMRank == 0) write (*,'(A)') 'PlasComCM: ====== Solid Domain BC Info ======'
    call mpi_barrier(TMComm,ierr)
    do i = 0, numTMproc-1
       if (TMRank == i) then
          write (*,'(A,I3)') 'PlasComCM: BC for solid processor ', TMRank
          do j = 1, region%nTMPatches
             TMpatch => region%TMpatch(j)
             write (*,'(2(A,I3))') 'PlasComCM:   From solid grid ', TMpatch%gridID, ' on patch ', j
             do k = 1, TMgrid%ND
                write (*,'(A,2(I5,1X))') 'PlasComCM: ', TMpatch%is(k), TMpatch%ie(k)
             end do
          end do
       end if
       call mpi_barrier(TMComm,ierr)
    end do
    call f90flush()
    call mpi_barrier(TMComm,ierr)

        
    
    if (TMRank == 0) write (*,'(A)') 'PlasComCM: ==== END Solid Domain BC Info ===='

    Return

  End Subroutine Set_Up_TMBoundary_Patches

  subroutine ElementTable(region)

    USE ModGlobal
    USE ModDataStruct

    Implicit None

    Type(t_region), pointer :: region

    ! ... local variables
    Type(t_sgrid), pointer :: TMgrid
    Type(t_mixt_input), pointer :: input
    Type (t_spatch), pointer :: TMpatch
    Integer :: ND, N(MAX_ND), Ne(MAX_ND)
    Integer :: nTMGrids, NdsPer, NdsPerFace
    Integer :: ng, i, j, k, dir, le, koffset, m
    Integer, allocatable :: ElNde(:)
    Integer, pointer :: LM(:,:)
    Integer :: npatches, nbc, io_err, ei, ej, ek, lpe
    Integer :: Npe(MAX_ND), Npee(MAX_ND), Npes(MAX_ND), Nes(MAX_ND), Nee(MAX_ND)
    INTEGER :: signdir, bdir, normDir, sgn, FaceNum, ElemType, gridID
    Character(LEN=80) :: strbuf
    Integer, pointer :: iPatchGlobal(:)


    ! ... simplicity
    ND = region%input%ND
    nTMGrids = region%nTMGrids
    N(:) = 1
    Ne(:) = 1

    ! ... loop through grids
    do ng = 1, nTMGrids

       ! ... simplicity
       TMgrid => region%TMgrid(ng)
       input => TMgrid%input

       ! ... number of plot3D points on grid (not neccessarily number of nodes)
       do dir = 1, ND
          N(dir) = TMgrid%ie(dir) - TMgrid%is(dir) + 1
       end do

       ! ... number of elements
       if(input%ElemType == LINEAR) then
          do dir = 1, ND
             Ne(dir) = (N(dir) - 1)
          end do
       elseif(input%ElemType == QUADRATIC) then
          do dir = 1, ND
             Ne(dir) = (N(dir) - 1)/2;
          end do
       end if

       ! ... number of nodes per element
       if(input%ElemType == LINEAR) then
          NdsPer = 4 + (ND-2)*4
       elseif(input%ElemType == QUADRATIC) then
          NdsPer = 8 + (ND-2)*12
       end if

       TMgrid%NdsPerElem = NdsPer


       ! ... local element table
       allocate(TMgrid%LM(NdsPer,PRODUCT(Ne)))
       allocate(ElNde(NdsPer))
       ! ... initialize
       TMgrid%LM(:,:) = 0
       ElNde(:) = 0
       if(input%ElemType == QUADRATIC) then
          do k = 1, Ne(3)
             do j = 1, Ne(2)
                do i = 1, Ne(1)

                   ! ... element index 
                   le = (k-1)*Ne(2)*Ne(1) + (j-1)*Ne(1) + (i-1) + 1

                   ! ... element local k = 0 plane
                   koffset = (k-1)*(N(1)*((N(2)-1)/2 + 1) + ((N(1)-1)/2+1)*(N(2)-1)/2 + ((N(1)-1)/2+1)*((N(2)-1)/2+1))

                   ! ... find local node index for this element
                   ElNde(1) = koffset + (j-1)*(N(1) + (N(1)-1)/2 + 1) + (i-1)*2 + 1
                   ElNde(2) = koffset + (j-1)*(N(1) + (N(1)-1)/2 + 1) + (i  )*2 + 1
                   ElNde(3) = koffset + (j  )*(N(1) + (N(1)-1)/2 + 1) + (i  )*2 + 1
                   ElNde(4) = koffset + (j  )*(N(1) + (N(1)-1)/2 + 1) + (i-1)*2 + 1

                   ElNde(1+2**ND) = koffset + (j-1)*(N(1) + (N(1)-1)/2 + 1) + (i-1)*2 + 2
                   ElNde(2+2**ND) = koffset + (j-1)*(N(1) + (N(1)-1)/2 + 1) + (i  )   + 1 + N(1)
                   ElNde(3+2**ND) = koffset + (j  )*(N(1) + (N(1)-1)/2 + 1) + (i-1)*2 + 2
                   ElNde(4+2**ND) = koffset + (j-1)*(N(1) + (N(1)-1)/2 + 1) + (i-1)   + 1 + N(1)

                   ! ... only have 8 nodes in element if 2D, onto the next element
                   if(ND == 2) then
                      do m = 1, NdsPer
                         TMgrid%LM(m,le) = ElNde(m)
                      end do
                      cycle
                   end if

                   ! ... moving in the third dimension
                   ! ... element local k = 1/2 plane
                   koffset = (k-1)*(N(1)*((N(2)-1)/2 + 1) + ((N(1)-1)/2+1)*(N(2)-1)/2 + ((N(1)-1)/2+1)*((N(2)-1)/2+1))&
                        + N(1)*((N(2)-1)/2 + 1) + ((N(1)-1)/2+1)*(N(2)-1)/2

                   ElNde(13) = koffset + (j-1)*((N(1)-1)/2 + 1) + (i-1) + 1
                   ElNde(14) = koffset + (j-1)*((N(1)-1)/2 + 1) + (i  ) + 1
                   ElNde(15) = koffset + (j-1)*((N(1)-1)/2 + 1) + (i  ) + 1 + (N(1)-1)/2 + 1
                   ElNde(16) = koffset + (j-1)*((N(1)-1)/2 + 1) + (i-1) + 1 + (N(1)-1)/2 + 1

                   ! ... element local k = 1 plane
                   koffset = (k  )*(N(1)*((N(2)-1)/2 + 1) + ((N(1)-1)/2+1)*(N(2)-1)/2 + ((N(1)-1)/2+1)*((N(2)-1)/2+1))

                   ! ... find local node index for this element
                   ElNde(5) = koffset + (j-1)*(N(1) + (N(1)-1)/2 + 1) + (i-1)*2 + 1
                   ElNde(6) = koffset + (j-1)*(N(1) + (N(1)-1)/2 + 1) + (i  )*2 + 1
                   ElNde(7) = koffset + (j  )*(N(1) + (N(1)-1)/2 + 1) + (i  )*2 + 1
                   ElNde(8) = koffset + (j  )*(N(1) + (N(1)-1)/2 + 1) + (i-1)*2 + 1

                   ElNde(17) = koffset + (j-1)*(N(1) + (N(1)-1)/2 + 1) + (i-1)*2 + 2
                   ElNde(18) = koffset + (j-1)*(N(1) + (N(1)-1)/2 + 1) + (i  )   + 1 + N(1)
                   ElNde(19) = koffset + (j  )*(N(1) + (N(1)-1)/2 + 1) + (i-1)*2 + 2
                   ElNde(20) = koffset + (j-1)*(N(1) + (N(1)-1)/2 + 1) + (i-1)   + 1 + N(1)

                   do m = 1, NdsPer
                      TMgrid%LM(m,le) = ElNde(m)
                   end do

                end do
             end do
          end do
       elseif(input%ElemType == LINEAR) then
          do k = 1, Ne(3)
             do j = 1, Ne(2)
                do i = 1, Ne(1)
                   
                   ! ... element index 
                   le = (k-1)*Ne(2)*Ne(1) + (j-1)*Ne(1) + (i-1) + 1

                   ! ... element local k = 0 plane
                   koffset = (k-1)*N(1)*N(2)

                   ! ... find local node index for this element
                   ElNde(1) = koffset + (j-1)*N(1) + (i-1) + 1
                   ElNde(2) = koffset + (j-1)*N(1) + (i  ) + 1
                   ElNde(3) = koffset + (j  )*N(1) + (i  ) + 1
                   ElNde(4) = koffset + (j  )*N(1) + (i-1) + 1

                   ! ... only have 4 nodes in element if 2D, onto the next element
                   if(ND == 2) then
                      do m = 1, NdsPer
                         TMgrid%LM(m,le) = ElNde(m)
                      end do
                      cycle
                   end if

                   ! ... moving in the third dimension
                   ! ... element local k = 0 plane
                   koffset = (k  )*N(1)*N(2)

                   ! ... find local node index for this element
                   ElNde(5) = koffset + (j-1)*N(1) + (i-1) + 1
                   ElNde(6) = koffset + (j-1)*N(1) + (i  ) + 1
                   ElNde(7) = koffset + (j  )*N(1) + (i  ) + 1
                   ElNde(8) = koffset + (j  )*N(1) + (i-1) + 1


                   do m = 1, NdsPer
                      TMgrid%LM(m,le) = ElNde(m)
                   end do

                end do
             end do
          end do
       end if
    end do

    ! ... set up boundary element information
    do i = 1, region%nTMPatches

       ! ... simplicity
       TMpatch => region%TMpatch(i)
       gridID  =  TMpatch%gridID
       TMgrid  => region%TMgrid(gridID)
       input   => TMgrid%input
       LM      => TMgrid%LM
       ND      =  TMgrid%ND

       normDir = abs(TMpatch%normDir)
       sgn = normDir / TMpatch%normDir

       ! ... select which face of the 3D element this patch is on
       if(sgn > 0) then
          FaceNum = 1
       elseif(sgn < 0) then
          FaceNum = 2
       end if
       
       ! ... number of elements in grid
       Ne(:) = 1
       if(input%ElemType == QUADRATIC) then
          do dir = 1, ND
             Ne(dir) = (TMgrid%ie(dir)-TMgrid%is(dir))/2
          end do
       elseif(input%ElemType == LINEAR) then
          do dir = 1, ND
             Ne(dir) = (TMgrid%ie(dir)-TMgrid%is(dir))
          end do
       end if

       ! ... Number of elements along the patch
       Npe(:) = 1
       if(input%ElemType == QUADRATIC) then
          do dir = 1, ND
             Npe(dir) = (TMpatch%ie(dir)-TMpatch%is(dir))/2
          end do
       elseif(input%ElemType == LINEAR) then
          do dir = 1, ND
             Npe(dir) = (TMpatch%ie(dir)-TMpatch%is(dir))
          end do
       end if


       ! ... starting and ending elements in this process' partition of the grid
       Nes(:) = 1; Nee(:) = 1
       if(input%ElemType == QUADRATIC) then
          do dir = 1, ND
             Nes(dir) = (TMgrid%is(dir)-1)/2+1
             Nee(dir) = (TMgrid%ie(dir)-1)/2
          end do
       elseif(input%ElemType == LINEAR) then
          do dir = 1, ND
             Nes(dir) = TMgrid%is(dir)
             Nee(dir) = TMgrid%ie(dir)-1
          end do          
       end if
       
       ! ... starting and ending elements in the patch
       Npes(:) = 1; Npee(:) = 1
       do dir = 1, ND
          if(Npe(dir) == 0) then
             ! ... keep counting correct if patch is on face with normDir == dir
             if(sgn<1) then
                Npes(dir) = Nee(dir)
                Npee(dir) = Nee(dir)
             else
                Npes(dir) = Nes(dir)
                Npee(dir) = Nes(dir)
             end if
             ! ... to keep product(Npe) >= 1
             Npe(dir) = 1
          else
             if(input%ElemType == QUADRATIC) then
                Npes(dir) = (TMpatch%is(dir)-1)/2+1
                Npee(dir) = (TMpatch%ie(dir)-1)/2
             elseif(input%ElemType == LINEAR) then
                Npes(dir) = TMpatch%is(dir)
                Npee(dir) = TMpatch%ie(dir)-1                
             end if
          end if
       end do

       allocate(TMpatch%LM2D(TMgrid%NdsPerFace,product(Npe)))

       do ek = Npes(3), Npee(3)
          do ej = Npes(2), Npee(2)
             do ei = Npes(1), Npee(1)
                
                le  = (ek-Nes(3) )*Ne(2) *Ne(1)  + (ej-Nes(2) )*Ne(1)  + (ei-Nes(1))  + 1
                lpe = (ek-Npes(3))*Npe(2)*Npe(1) + (ej-Npes(2))*Npe(1) + (ei-Npes(1)) + 1

                do k = 1, TMgrid%NdsPerFace
                   TMpatch%LM2D(k,lpe) = LM(TMgrid%FaceNodes(k,normDir,FaceNum),le)
                end do
             end do
          end do
       end do
      
    end do

  end subroutine ElementTable

  subroutine LocaltoGlobal(region)

    USE ModGlobal
    USE ModDataStruct
    USE ModMPI
    USE ModTMInt

    Implicit None

    Type(t_region), pointer :: region

    ! ... local variables 
    Type(t_sgrid), pointer :: TMgrid
    Type(t_mixt_input), pointer :: input
    Integer, pointer :: LM(:,:), L2G(:)
    Integer :: ND, NG(MAX_ND), N(MAX_ND), Ne(MAX_ND)
    Integer :: Eoffset(MAX_ND), ioffset(MAX_ND), nPtsLoc, nPtsGlb
    Integer, allocatable :: mvec(:), flag(:)
    Integer, pointer :: OwnedNodes(:), OwnedNodesGlobal(:)
    Integer :: istrt, i, j, k, le, m, ldof, ng1, dir, mm, p
    Real(rfreal), pointer :: F(:)

    ! ... simplicity
    ND = region%input%ND

    ! ... loop through the grids on this processor 
    Do ng1 = 1, region%nTMGrids

       ! ... simplicity
       TMgrid => region%TMgrid(ng1)
       input  => TMgrid%input
       LM     => TMgrid%LM
       nPtsLoc = TMgrid%nPtsLoc

       N(:) = 1
       ! ... number of plot3D grid points in each direction
       do dir = 1, ND
          N(dir) = TMgrid%ie(dir) - TMgrid%is(dir) + 1
       end do

       ! ... global number of plot3D grid points in each direction
       NG(:) = 1
       do dir = 1, ND
          NG(dir) = TMgrid%GlobalSize(dir)
       end do

       Eoffset(:) = 0
       ! ... how many elements are owned by lower ranks
       if(input%ElemType == QUADRATIC) then
          do dir = 1, ND 
             Eoffset(dir) = (TMgrid%is(dir) - 1)/2
          end do
       elseif(input%ElemType == LINEAR) then
          do dir = 1, ND 
             Eoffset(dir) = (TMgrid%is(dir) - 1)
          end do
       end if

       ! ... how many elements in each direction
       Ne(:) = 1
       if(input%ElemType == QUADRATIC) then
          do dir = 1, ND 
             Ne(dir) = (N(dir) - 1)/2
          end do
       elseif(input%ElemType == LINEAR) then
          do dir = 1, ND 
             Ne(dir) = (N(dir) - 1)
          end do
       end if

       if(input%ElemType == LINEAR) then
          allocate(mvec(2 + (ND-2)*2))

          ! ... now fill the L2G map
          allocate(flag(NptsLoc))
          allocate(TMgrid%L2G(nPtsLoc))

          ! ... simplicity
          L2G => TMgrid%L2G

          flag(:) = 0
          L2G(:) = -1
          ! ... depending on location, shift
          ! ... check i = 1 processor boundary
          if(Eoffset(1) > 0) then
             if(ND > 2) then
                mvec = (/1, 4, 5, 8/)
             elseif(ND == 2) then
                mvec = (/1, 4/)
             end if

             do k = 1, Ne(3)
                do j = 1, Ne(2)
                   i = 1;
                   le = (k-1)*Ne(2)*Ne(1) + (j-1)*Ne(1) + i-1 + 1;
                   do mm = 1, size(mvec)
                      m = mvec(mm);
                      ldof = LM(m,le);
                      flag(ldof) = 1;
                   end do
                end do
             end do
          end if

          ! ... check j = 1 processor boundary
          if(Eoffset(2) > 0) then
             if(ND > 2) then
                mvec = (/1, 2, 5, 6/)
             elseif(ND == 2) then
                mvec = (/1, 2/)
             end if
             do k = 1, Ne(3)
                j = 1
                do i = 1, Ne(1)
                   le = (k-1)*Ne(2)*Ne(1) + (j-1)*Ne(1) + i-1 + 1
                   do mm = 1, size(mvec)
                      m = mvec(mm)
                      ldof = LM(m,le)
                      flag(ldof) = 1
                   end do
                end do
             end do
          end if

          ! ... check k = 1 processor boundary
          if(Eoffset(3) > 0) then
             ! ... can only be ND > 2
             mvec = (/1, 2, 3, 4/)
             k = 1
             do j = 1, Ne(2)
                do i = 1, Ne(1)
                   le = (k-1)*Ne(2)*Ne(1) + (j-1)*Ne(1) + i-1 + 1
                   do mm = 1, size(mvec)
                      m = mvec(mm)
                      ldof = LM(m,le)
                      flag(ldof) = 1
                   end do
                end do
             end do
          end if
          
       elseif(input%ElemType == QUADRATIC) then
          allocate(mvec(3 + (ND-2)*5))

          ! ... now fill the L2G map
          allocate(flag(NptsLoc))
          allocate(TMgrid%L2G(nPtsLoc))

          ! ... simplicity
          L2G => TMgrid%L2G

          flag(:) = 0
          L2G(:) = -1
          ! ... depending on location, shift
          ! ... check i = 1 processor boundary
          if(Eoffset(1) > 0) then
             if(ND > 2) then
                mvec = (/1, 4, 5, 8, 12, 13, 16, 20/)
             elseif(ND == 2) then
                mvec = (/1, 4, 8/)
             end if

             do k = 1, Ne(3)
                do j = 1, Ne(2)
                   i = 1;
                   le = (k-1)*Ne(2)*Ne(1) + (j-1)*Ne(1) + i-1 + 1;
                   do mm = 1, size(mvec)
                      m = mvec(mm);
                      ldof = LM(m,le);
                      flag(ldof) = 1;
                   end do
                end do
             end do
          end if

          ! ... check j = 1 processor boundary
          if(Eoffset(2) > 0) then
             if(ND > 2) then
                mvec = (/1, 2, 5, 6, 9, 13, 14, 17/)
             elseif(ND == 2) then
                mvec = (/1, 2, 5/)
             end if
             do k = 1, Ne(3)
                j = 1
                do i = 1, Ne(1)
                   le = (k-1)*Ne(2)*Ne(1) + (j-1)*Ne(1) + i-1 + 1
                   do mm = 1, size(mvec)
                      m = mvec(mm)
                      ldof = LM(m,le)
                      flag(ldof) = 1
                   end do
                end do
             end do
          end if

          ! ... check k = 1 processor boundary
          if(Eoffset(3) > 0) then
             ! ... can only be ND > 2
             mvec = (/1, 2, 3, 4, 9, 10, 11, 12/)
             k = 1
             do j = 1, Ne(2)
                do i = 1, Ne(1)
                   le = (k-1)*Ne(2)*Ne(1) + (j-1)*Ne(1) + i-1 + 1
                   do mm = 1, size(mvec)
                      m = mvec(mm)
                      ldof = LM(m,le)
                      flag(ldof) = 1
                   end do
                end do
             end do
          end if
       end if ! ... ElemType == LINEAR

          ! ... find how many nodes this processor owns
          allocate(OwnedNodes(TMgrid%numproc_inComm),OwnedNodesGlobal(TMgrid%numproc_inComm))
          OwnedNodes(:) = 0
          OwnedNodesGlobal(:) = 0
          TMgrid%nPtsOwned = nPtsLoc-SUM(flag)
          OwnedNodes(TMgrid%myrank_inComm+1) = TMgrid%nPtsOwned

          ! ... communicate this information to everyone
          call MPI_AllReduce(OwnedNodes, OwnedNodesGlobal, TMgrid%numproc_inComm, MPI_INTEGER, MPI_SUM, TMgrid%Comm, ierr)

          istrt = 0
          ! ... now find global starting index for this process
          if(TMgrid%myRank_inComm > 0) then
             istrt = SUM(OwnedNodesGlobal(1:TMgrid%myRank_inComm))
          end if

          ! ... total number of pointes in this grid
          TMgrid%nPtsGlb = SUM(OwnedNodesGlobal(:))

          ! ... first node on this process
          TMgrid%nStart = istrt + 1
          nPtsGlb = TMgrid%nPtsGlb
          allocate(TMgrid%G2L(nPtsGlb))

          deallocate(OwnedNodes,OwnedNodesGlobal)

          ! ... now fill in global degrees of freedom
          do k = 1, Ne(3)
             do j = 1, Ne(2)
                do i = 1, Ne(1)
                   le = (k-1)*Ne(2)*Ne(1) + (j-1)*Ne(1) + i-1 + 1 
                   do m = 1, TMgrid%NdsPerElem
                      ldof = LM(m,le)
                      if(flag(ldof) == 1) then 
                         L2G(ldof) = -1
                      else
                         L2G(ldof) = ldof + istrt - SUM(flag(1:ldof))
                      end if
                   end do
                end do
             end do
          end do
       !end if ! ... input%ElemType == LINEAR
    end do ! ... ng

    ! ... now fill in missing global ids
    do ng1 = 1, region%nTMGrids
       TMgrid => region%TMgrid(ng1)
       allocate(F(TMgrid%nPtsLoc))
       do p = 1, TMgrid%nPtsLoc
          F(p) = dble(TMgrid%L2G(p))
       end do
       call Ghost_Cell_Exchange_TM(region, F, ng1)
       do p = 1, TMgrid%nPtsLoc
          TMgrid%L2G(p) = Int(F(p)) 
       end do
       deallocate(F)
    end do

    ! ... fill global to local
    do p = 1, TMgrid%nPtsGlb
       TMgrid%G2L(p) = -1
    end do
    do p = 1, TMgrid%nPtsLoc
       TMgrid%G2L(TMgrid%L2G(p)) = p
    end do

  end subroutine LocaltoGlobal

  subroutine InitializeTMState(region, restart_fname)

    USE ModGlobal
    USE ModDataStruct
    USE ModMPI
    USE ModPLOT3D_IO
    USE ModTMIO

    Implicit None

    type(t_region), pointer :: region
    character(len=PATH_LENGTH) :: restart_fname

    ! ... local variables
    type(t_mixt_input), pointer :: input
    type(t_sgrid), pointer :: TMgrid
    type(t_smixt), pointer :: TMstate
    Real(rfreal), Dimension(:,:,:,:,:), Pointer :: X
    Integer, Dimension(:,:,:,:), Pointer :: IBLANK
    Character(LEN=2) :: prec, gf, vf, ib
    Integer :: ngrid, I, J, K, L0, Nx, Ny, Nz, NDIM, M, ftype, ng, alloc_stat, nVars, TMRank, nRateVars
    Integer, Dimension(:,:), Pointer :: ND

    ! ... allocate state datatype
    if (associated(region%TMstate) .eqv. .false.) allocate(region%TMstate(region%nGrids))

    ! ... setup the state
    do ng = 1, region%nTMGrids

       ! ... this grid
       TMgrid  => region%TMgrid(ng)
       TMstate => region%TMstate(ng)
       input => TMgrid%input
       TMRank = region%TMRank


       ! ... global parameters
       if(input%TMSolve == 1) then
          TMState%nVars = 1
          if(TMRank == 0) write(*,'(A)') ' PlasComCM: Thermal-only analysis in solid'
       elseif(input%TMSolve > 1 .and. input%TMSolve <= 3) then
          TMState%nVars = TMgrid%ND-2 + input%TMSolve
          if(input%TMSolve == 3) then
             if(TMRank == 0) write(*,'(A)') ' PlasComCM: Thermomechanical analyis in solid domain'
          else
             if(TMRank == 0) write(*,'(A)') ' PlasComCM: Structural-only analysis in solid domain'
          end if
       else
          call graceful_exit(region%myrank, ' PlasComCM: 1 > input%TMSolve and input%TMSolve > 3, exiting ...')
       end if

       nVars = TMstate%nVars

       ! ... array for structural and thermal solution
       allocate(TMstate%q(TMgrid%nPtsLoc,nVars),stat=alloc_stat)
       if (alloc_stat /= 0) call graceful_exit(region%myrank, 'PlasComCM: could not allocate TMstate%q');
       allocate(TMstate%P3D(TMgrid%nCells,TMgrid%ND+2),stat=alloc_stat)
       if (alloc_stat /= 0) call graceful_exit(region%myrank, 'PlasComCM: could not allocate TMstate%P3D');
       ! ... array to hold initial configuration
       allocate(TMgrid%X(TMgrid%nPtsLoc,TMgrid%ND), stat=alloc_stat)
       if (alloc_stat /= 0) call graceful_exit(region%myrank, 'PlasComCM: could not allocate TMgrid%X');

       ! ... material property array
       allocate(TMstate%YMod(TMgrid%nPtsLoc))
       allocate(TMstate%PRat(TMgrid%nPtsLoc))
       
       ! ... nullify a number of pointers for better memory management
       nullify(TMstate%qOld)
       nullify(TMstate%qdot)
       nullify(TMstate%Tnew)

       ! ... allocate space for new temperature
       ! ... useful when interpolating between thermal steps
       if(input%TMSolve == 1 .or. input%TMSolve == 3) then
          allocate(TMstate%Tnew(TMgrid%nPtsLoc),stat=alloc_stat)
          if (alloc_stat /= 0) call graceful_exit(region%myrank, 'PlasComCM: could not allocate TMstate%Tnew');
       end if
       
       if(input%StructuralTimeScheme == DYNAMIC_SOLN .or. input%ThermalTimeScheme == DYNAMIC_SOLN) then
          nRateVars = 0
          ! ... Dynamic structural: Record velocity, acceleration, and external load vectors
          if(input%StructuralTimeScheme == DYNAMIC_SOLN) nRateVars = nRateVars + 3*TMgrid%ND
          ! ... Dynamic thermal: Record dT/dt
          if(input%ThermalTimeScheme == DYNAMIC_SOLN) nRateVars = nRateVars + 1
          allocate(TMstate%qdot(TMgrid%nPtsLoc,nRateVars),stat=alloc_stat)
          if (alloc_stat /= 0) call graceful_exit(region%myrank, 'PlasComCM: could not allocate TMstate%qdot');
          ! ... initialize
          TMstate%qdot(:,:) = 0.0_rfreal
          allocate(TMstate%P3Ddot(TMgrid%nCells,nRateVars),stat=alloc_stat)
          if (alloc_stat /= 0) call graceful_exit(region%myrank, 'PlasComCM: could not allocate TMstate%P3D');
       end if
                 
       ! ... record the initial configuration
       do j = 1, TMgrid%ND
          do i = 1, TMgrid%nPtsLoc
             TMgrid%X(i,j) = TMgrid%XYZ(TMgrid%L2P3D(i),j)
          end do
       end do

       ! ... initialize state
       IF((debug_on .eqv. .true.) .and. (region%myrank == 0)) write(*,*) 'PlasComCM: Setting IC'
       call TMInitialCondition(region)
       IF((debug_on .eqv. .true.) .and. (region%myrank == 0)) write(*,*) 'PlasComCM: IC done.'
       
    end do
    
    call TMwrite_plot3d_file(region, 'TMq',region%input%nstepi)

    CALL MPI_BARRIER(region%TMComm,ierr)
    IF((debug_on.eqv..true.).and.(region%myrank == 0)) write(*,*) 'PlasComCM: All processors done with InitializeTMState'
  end subroutine InitializeTMState


  Subroutine TMInitialCondition(region)

    USE ModGlobal
    USE ModDataStruct
    USE ModTMIO
    USE ModTMEOM

    implicit none

    Type(t_region), pointer :: region

    ! ... local variables
    Type(t_mixt_input), pointer :: input
    Type(t_sgrid), pointer :: TMgrid
    Type(t_smixt), pointer :: TMstate
    Integer, pointer :: G2L(:)
    Integer :: l0, ng, dir, ND, nPtsLoc, i, j
    Real(rfreal) :: T0, Tx, Lx, x, x1, x2, v0, vx, EigK, C, Denom

    do ng = 1, region%nGrids

       ! ... simplicity
       TMgrid  => region%TMgrid(ng)
       TMstate => region%TMstate(ng)
       input   => TMgrid%input
       ND      =  TMgrid%ND
       nPtsLoc =  TMgrid%nPtsLoc
       G2L     => TMgrid%G2L
       T0 = input%TMInitialTemp


       if(input%TMrestart == TRUE) then
          IF((debug_on .eqv. .true.) .and. (region%TMRank == 0)) write(*,*) 'PlasComCM: This is a restart'
          ! ... this is a restart, read from file
          call read_TMrestart(region)
       elseif(input%TMrestart == FALSE) then
          IF((debug_on .eqv. .true.) .and. (region%TMRank == 0)) write(*,*) 'PlasComCM: Not a restart'
          ! ... for now, initial condition is boring
          ! ... structural IC is the initial configuration
          if(input%TMSolve > 1) then
             do dir = 1, TMgrid%ND
                do l0 = 1, nPtsLoc
                   TMstate%q(l0,dir) = TMgrid%X(l0,dir)
                end do
             end do
             if(TMstate%DispBCNum > 0) then
                do i = 1, size(TMstate%BCDispInd)
                   j = G2L((TMstate%BCDispInd(i)-1)/ND+1)
                   dir = mod(TMstate%BCDispInd(i)-1,ND)+1
                   !             print*,'region%TMRank,i,j,dir,TMstate%BCDispInd(i)',region%TMRank,i,j,dir,TMstate%BCDispInd(i)
                   TMstate%q(j,dir) = TMgrid%X(j,dir) + TMstate%BCDispVal(i)
                   
                end do
             end if
             !do j = 1, nPtsLoc
             !   TMstate%q(j,2) = TMgrid%X(j,2) + .5*sin(TMgrid%X(j,1)*TWOPI/2.0_rfreal)*sin(TMgrid%X(j,3)*TWOPI/2.0_rfreal)
             !   print*,j,dir,TMstate%q(j,2),TMgrid%X(j,1),TMgrid%X(j,3)
             !end do

             if(input%TMSolve == 3) then
                if(.true.) then
                   do l0 = 1, nPtsLoc
                      TMstate%q(l0,TMstate%nVars) = T0
                   end do
                elseif(.false.) then
                   x1 = minval(TMgrid%X(:,1))
                   x2 = maxval(TMgrid%X(:,1))
                   Lx = x2-x1
                   do l0 = 1, nPtsLoc
                      x = TMgrid%X(l0,1)
                      if(x .le. (x2+x1)*0.5_rfreal) Tx = 2.0_rfreal*(x-x1)/Lx*T0
                      if(x .gt. (x2+x1)*0.5_rfreal) Tx = (1.0_rfreal - 2.0_rfreal*((x-x1)-(Lx*0.5_rfreal))/Lx)*T0
                      TMstate%q(l0,TMstate%nVars) = Tx
                   end do
                end if
             end if
          elseif(input%TMSolve == 1) then
             ! ... uniform
             if(.false.) then
                do l0 = 1, nPtsLoc
                   TMstate%q(l0,1) = T0
                end do
             
             elseif(.true.) then
                ! ... triangular temperature distribution
                x1 = minval(TMgrid%X(:,1))
                x2 = maxval(TMgrid%X(:,1))
                Lx = x2-x1
                do l0 = 1, nPtsLoc
                   x = TMgrid%X(l0,1)
                   if(x .le. (x2+x1)*0.5_rfreal) Tx = 2.0_rfreal*x/Lx*T0
                   if(x .gt. (x2+x1)*0.5_rfreal) Tx = (1.0_rfreal - 2.0_rfreal*(x-(Lx*0.5_rfreal))/Lx)*T0
                   TMstate%q(l0,1) = Tx
                end do
             end if
          end if
       end if
    
       ! ... if coupled solve, allocate vector to hold old data
       if(input%TMSolve == 3) then
          !             if(region%TMRank == 0) write(*,'(A)') 'Allocating old solution vector for coupled solve'
          Allocate(TMstate%qOld(nPtsLoc,TMstate%nVars))
          
          ! ... initialize the old solution array
          do j = 1, TMstate%nVars
             do i = 1, nPtsLoc
                TMstate%qOld(i,j) = TMstate%q(i,j)
             end do
          end do
          
       end if

       ! ... give the panel an initial "ping"
       if(input%TMInitialCondition == TRUE .and. input%TMrestart == FALSE) then
          if(region%TMRank == 0) write(*,'(A)') 'Prescribing initial velocity to panel'
          x1 = minval(TMgrid%X(:,1))
          x2 = maxval(TMgrid%X(:,1))
          Lx = x2-x1
          v0 = input%TMInitialVelocity
          ! ... approximation of mode k eigenvalue
          EigK = (dble(input%BeamMode)+0.5_rfreal)*TWOPI*0.5_rfreal/Lx
          C = (cosh(EigK*Lx)-cos(EigK*Lx))/(sinh(EigK*Lx)-sin(EigK*Lx))
          !print*,'Length of panel = ',Lx
          Denom = 0.0_rfreal
          do l0 = 1, nPtsLoc
             x = TMgrid%X(l0,1)
             vx = cosh(EigK*(x-x1))-cos(EigK*(x-x1)) - C*(sinh(EigK*(x-x1))-sin(EigK*(x-x1)))
             Denom = max(Denom,vx);
             TMstate%qdot(l0,2) = vx
             !print*,'X= ',l0,TMgrid%X(l0,1),' v = ',TMstate%qdot(l0,2)
          end do
          ! ... normalize
          do l0 = 1, nPtsLoc
             TMstate%qdot(l0,2) = TMstate%qdot(l0,2)/Denom*v0
          end do
          
       end if

    end do
    
  end Subroutine TMInitialCondition

  Subroutine SetElemFaceInformation(TMgrid)
    
    USE ModGlobal
    USE ModDataStruct
    
    Implicit None
    
    Type(t_sgrid), pointer :: TMgrid
    Integer :: ng
    
    ! ... local variables
    Type(t_mixt_input), pointer :: input
    Integer, pointer :: FaceNodes(:,:,:)
    Integer :: NdsPerFace, ND, ElemType

    ! ... simplicity
    input    => TMgrid%input
    ND       =  TMgrid%ND
    ElemType =  input%ElemType 
    
    if(ElemType == QUADRATIC) then
       if(ND == 2) then
          NdsPerFace = 3
          TMgrid%NdsPerFace = NdsPerFace
          allocate(TMgrid%FaceNodes(NdsPerFace,ND,2))
          FaceNodes => TMgrid%FaceNodes
          FaceNodes(:,1,1) = (/1, 4, 8/)
          FaceNodes(:,1,2) = (/2, 3, 6/)
          FaceNodes(:,2,1) = (/1, 2, 5/)
          FaceNodes(:,2,2) = (/4, 3, 7/)
       elseif(ND == 3) then
          NdsPerFace = 8
          TMgrid%NdsPerFace = NdsPerFace
          allocate(TMgrid%FaceNodes(NdsPerFace,ND,2))
          FaceNodes => TMgrid%FaceNodes
          FaceNodes(:,1,1) = (/1, 5, 8, 4, 13, 20, 16, 12/)
          FaceNodes(:,1,2) = (/2, 6, 7, 3, 14, 18, 15, 10/)
          FaceNodes(:,2,1) = (/1, 2, 6, 5,  9, 14, 17, 13/)
          FaceNodes(:,2,2) = (/4, 3, 7, 8, 11, 15, 19, 16/)
          FaceNodes(:,3,1) = (/2, 1, 4, 3,  9, 12, 11, 10/)
          FaceNodes(:,3,2) = (/6, 5, 8, 7, 17, 20, 19, 18/)
       end if
    elseif(ElemType == LINEAR) then
       if(ND == 2) then
          NdsPerFace = 2
          TMgrid%NdsPerFace = NdsPerFace
          allocate(TMgrid%FaceNodes(NdsPerFace,ND,2))
          FaceNodes => TMgrid%FaceNodes
          FaceNodes(:,1,1) = (/1, 4/)
          FaceNodes(:,1,2) = (/2, 3/)
          FaceNodes(:,2,1) = (/1, 2/)
          FaceNodes(:,2,2) = (/4, 3/)
       elseif(ND == 3) then
          NdsPerFace = 4
          TMgrid%NdsPerFace = NdsPerFace
          allocate(TMgrid%FaceNodes(NdsPerFace,ND,2))
          FaceNodes => TMgrid%FaceNodes
          FaceNodes(:,1,1) = (/1, 5, 8, 4/)
          FaceNodes(:,1,2) = (/2, 6, 7, 3/)
          FaceNodes(:,2,1) = (/1, 2, 6, 5/)
          FaceNodes(:,2,2) = (/4, 3, 7, 8/)
          FaceNodes(:,3,1) = (/2, 1, 4, 3/)
          FaceNodes(:,3,2) = (/6, 5, 8, 7/)
       end if
    end if
  end Subroutine SetElemFaceInformation
          
  Subroutine SparsityData(region,ng,nnz_d,nnz_o,row_ptr,col_ptr)
    
    USE ModDataStruct
    USE ModGlobal
    USE ModMPI
    
    implicit none

    Type(t_region), pointer :: region
    Integer, pointer :: nnz_d(:), nnz_o(:),nnz_o2(:)
    Integer :: ng

    ! ... local variables
    Type(t_sgrid), pointer :: TMgrid
    Type(t_mixt_input), pointer :: input
    Integer, pointer :: LM(:,:),L2G(:), sndbuf(:,:), rcvbuf(:,:)
    Integer, pointer :: LocalNodeConnections(:), TotalNodeConnections(:), InterfaceNodes(:), ArrayTmp(:)
    Integer, pointer :: col_tmp(:), col_ptr(:),row_ptr(:)
    Integer, Allocatable :: req(:)
    Integer, Allocatable :: B(:,:)
    Integer :: le, m, n, cnt, cntMax, i, j, is, ie
    Integer :: ND, NdsPer, nPtsLoc, BufSize, nStart, nPtsGlb, nPtsOwned, numTMproc, TMRank
    Integer :: Eoffset, cnt2, ldof

    ! ... simplicity
    TMgrid    => region%TMgrid(ng)
    input     => TMgrid%input
    LM        => TMgrid%LM
    L2G       => TMgrid%L2G
    nStart    =  TMgrid%nStart
    ND        =  TMgrid%ND
    nPtsLoc   =  TMgrid%nPtsLoc
    nPtsGlb   =  TMgrid%nPtsGlb
    nPtsOwned =  TMgrid%nPtsOwned
    numTMproc =  input%numTMproc
    TMRank    =  region%TMRank

    ! ... number of nodes per element
    if(input%ElemType == LINEAR) then
       NdsPer = 4 + (ND-2)*4
       ! ... maximum number of nodes per row
       if(ND == 3) BufSize = 8*8!27
       if(ND == 2) BufSize = 4*4!9
    elseif(input%ElemType == QUADRATIC) then
       NdsPer = 8 + (ND-2)*12
       ! ... maximum number of nodes per row
       if(ND == 3) BufSize = 20*8!81
       if(ND == 2) BufSize = 8*4!21
    end if

    Allocate(B(BufSize,nPtsLoc))
    Allocate(nnz_o2(nPtsLoc))
    nnz_o2 = 0
    B(:,:) = -1

    Allocate(LocalNodeConnections(nPtsGlb),TotalNodeConnections(nPtsGlb))
    LocalNodeConnections(:) = 0
    TotalNodeConnections(:) = 0


!     ! ... loop through each element and record connections
!     do le = 1, TMgrid%nElLoc
!        do m = 1, NdsPer
!           do n = 1, NdsPer
!              cnt = 1
!              do while(cnt < BufSize)
!                 ! ... don't count self
!                 if(m==n) exit
!                 ! ... check for empty spot in B
!                 if(B(cnt,LM(m,le)) == -1) then
!                    ! ... record connection
!                    B(cnt,LM(m,le)) = L2G(LM(n,le))
!                    ! ... determine whether on or off diagonal
!                    if((L2G(LM(n,le)) >= nStart) .and. (L2G(LM(n,le)) < nStart + nPtsOwned)) then
!                       ! ... on diagonal
!                       !         nnz_d(LM(m,le)) = nnz_d(LM(m,le)) + 1
!                       LocalNodeConnections(L2G(LM(m,le))) = LocalNodeConnections(L2G(LM(m,le))) + 1
!                    else
!                       ! ... off diagonal
!                       nnz_o2(LM(m,le)) = nnz_o2(LM(m,le)) + 1
!                    end if
!                    exit 
!                 elseif(B(cnt,LM(m,le)) == L2G(LM(n,le))) then
!                    ! ... already recorded this connection
!                    exit
!                 else
!                    cnt = cnt + 1
!                 end if
!              end do
!           end do
!        end do
!     end do

    ! ... loop through each element and record connections
    do le = 1, TMgrid%nElLoc
       do m = 1, NdsPer
          do n = 1, NdsPer
             cnt = 1
             do while(cnt < BufSize)
                ! ... don't count self
                if(m==n) exit
                ! ... check for empty spot in B
                if(B(cnt,LM(m,le)) == -1) then
                   ! ... record connection
                   B(cnt,LM(m,le)) = L2G(LM(n,le))

                   LocalNodeConnections(L2G(LM(m,le))) = LocalNodeConnections(L2G(LM(m,le))) + 1

                elseif(B(cnt,LM(m,le)) == L2G(LM(n,le))) then
                   ! ... already recorded this connection
                   exit
                else
                   cnt = cnt + 1
                end if
             end do
          end do
       end do
    end do

!            if(TMRank == 3) then
!               do i = 1, nPtsLoc
!                  print*,i,(B(j,i),j=1, BufSize)
!               end do
!            end if
!call graceful_exit(region%myrank, 'done')

    ! ... communicate this information to everyone
    call MPI_AllReduce(LocalNodeConnections, TotalNodeConnections, nPtsGlb, MPI_INTEGER, MPI_SUM, TMgrid%Comm, ierr)

!    if(region%TMRank == 1) print*,region%TMRank,'LN',LocalNodeConnections,'TN',TotalNodeConnections,'TNp',TotalNodeConnections+nnz_o2

!     Allocate(ArrayTmp(nPtsOwned))
!     ArrayTmp(:) = -1
!     cnt = 0
!     do n = 1, nPtsLoc
!        if((L2G(n) >= nStart) .and. (L2G(n) < nStart + nPtsOwned)) then
!           m = L2G(n) - nStart + 1
!           nnz_o(m) = TotalNodeConnections(L2G(n))-LocalNodeConnections(L2G(n))+nnz_o2(n)
!           nnz_d(m) = LocalNodeConnections(L2G(n)) + 1
!           cnt2 = nnz_o(m)+nnz_d(m)-1
!           ! ... find out which nodes you need connection information for
!           if(cnt2-(LocalNodeConnections(L2G(n))+nnz_o2(n))>0) then
!              cnt = cnt + 1
!              ArrayTmp(cnt) = L2G(n)
!           end if
!        end if
!     end do

    Allocate(ArrayTmp(nPtsOwned))
    ArrayTmp(:) = -1
    cnt = 0
    do n = 1, nPtsLoc
       if((L2G(n) >= nStart) .and. (L2G(n) < nStart + nPtsOwned)) then
          m = L2G(n) - nStart + 1
          if(LocalNodeConnections(L2G(n))/= TotalNodeConnections(L2G(n))) then
             cnt = cnt + 1
             ArrayTmp(cnt) = L2G(n)
          end if
       end if
    end do

    if(input%numTMproc > 1) then
       ! ... each process will allocate the same ammount of space for sends and receives
       call MPI_AllReduce(cnt, cntMax, 1, MPI_INTEGER, MPI_MAX, TMgrid%Comm, ierr)
       
!       if(TMRank == 1) print*,ArrayTmp
!        if(TMRank == 1) print*,'done'
       Allocate(InterfaceNodes(cntMax))
       InterfaceNodes(:) = -1
       do i = 1, cnt
          InterfaceNodes(i) = ArrayTmp(i)
       end do

       
       ! ... determine size of buffers
       if(region%input%ElemType == LINEAR) then
          if(ND == 3) Eoffset = 9
          if(ND == 2) Eoffset = 3
       elseif(region%input%ElemType == QUADRATIC) then
          if(ND == 3) Eoffset = 28
          if(ND == 2) Eoffset = 8
       end if
       Eoffset = BufSize
       
       ! ... allocate communication buffers
       ! ... numTMproc receives and sends
       allocate(req(2*numTMproc))
       allocate(sndbuf(cntMax*Eoffset,numTMproc),rcvbuf(cntMax*EoffSet,numTMproc))
       ! ... tell every other process which information is needed
       do i = 1, cnTMax
          sndbuf(i,1) = InterfaceNodes(i)
       end do
       
!       if(TMRank == 1) print*,'snd',sndbuf
!call graceful_exit(region%myrank, 'done')

       Call MPI_BARRIER(TMgrid%Comm, ierr)

       ! ... post receive buffers
       do i = 0, numTMproc - 1
          Call MPI_IRECV(rcvbuf(1,i+1),cntMax*Eoffset,MPI_INTEGER,i,10,TMgrid%Comm,req(i*2+1),ierr)
       end do

       ! ... now send
       do i = 0, numTMproc - 1
          Call MPI_ISEND(sndbuf(1,1),cntMax*Eoffset,MPI_INTEGER,i,10,TMgrid%Comm,req(i*2+2),ierr)
       end do

       Call MPI_Waitall(2*numTMproc,req, MPI_STATUSES_IGNORE, ierr)

 !           ! report
!            do i = 1, numTMproc
!               if (TMrank == i-1) then
!                  do j = 1, numTMproc
!                     print*,TMRank,': Proc ',j-1,' needs ',rcvbuf(1:cntMax,j)
!                  end do
!               end if
!               call MPI_BARRIER(TMgrid%Comm,ierr)
!            end do

       sndbuf(:,:) = -1
       ! ... run through each processor's needs, and provide information if available
       do i = 1, numTMproc
          do j = 1, cntMax
             if(rcvbuf(j,i) /= -1) then
                ldof = TMgrid%G2L(rcvbuf(j,i))
                if(ldof/=-1) then
                   !print*,TMRank,i,j,ldof,rcvbuf(j,i)
                   ! ... this is a local node, and may have additional info on this process
                   cnt2 = 1
                   do while(B(cnt2,ldof)/=-1)
                      !if(B(cnt2,ldof) < nStart + nPtsOwned .and. B(cnt2,ldof) >= nStart) then
                         ! ... record to be sent back to process i-1
                         sndbuf((j-1)*Eoffset + cnt2,i) = B(cnt2,ldof)
                         
                      !end if
                      cnt2 = cnt2 + 1
                      !print*,TMRank,cnt2,B(cnt2,ldof),ldof
                   end do
                end if
             end if
          end do
       end do

       
!        do i = 1, nPtsLoc
!           do j = 1, BufSize
!              if(region%myrank == 0) print*,i,j,B(j,i)
!           end do
!        end do
!       call graceful_exit(region%myrank, 'done')
!            call mpi_barrier(TMgrid%Comm, ierr)
!            report
!            do i = 1, numTMproc
!               if (TMrank == i-1) then
!                  do j = 1, numTMproc
!                     print*,TMRank,': Proc ',j-1,' needs ',sndbuf(:,j)
!                  end do
!               end if
!               call f90flush()
!               call mpi_barrier(TMgrid%Comm, ierr)
!           end do
!call graceful_exit(region%myrank, 'done')
       rcvbuf(:,:) = -1
       ! ... now send the information back to the requesting process
       ! ... post receive buffers
       do i = 0, numTMproc - 1
          Call MPI_IRECV(rcvbuf(1,i+1),cntMax*Eoffset,MPI_INTEGER,i,10,TMgrid%Comm,req(i*2+1),ierr)
       end do

       ! ... now send
       do i = 0, numTMproc - 1
          Call MPI_ISEND(sndbuf(1,i+1),cntMax*Eoffset,MPI_INTEGER,i,10,TMgrid%Comm,req(i*2+2),ierr)
       end do


       Call MPI_Waitall(2*numTMproc,req, MPI_STATUSES_IGNORE, ierr)

       ! ... now add back to B
       do j = 1, cntMax
          m = InterfaceNodes(j)
          
          if(m/=-1) then 
             B(:,TMgrid%G2L(m)) = -1
             cnt = 0
             do i = 1, numTMproc
                do n = 1, EOffset
                   if(rcvbuf((j-1)*Eoffset+n,i) /= -1) then
                      cnt = cnt+1
                      B(cnt,TMgrid%G2L(m)) = rcvbuf((j-1)*Eoffset+n,i)
                   end if
                end do
             end do
          end if
       end do

!            if(TMRank == 1) then
!               do i = 1, nPtsLoc
!                  print*,i,(B(j,i),j=1, BufSize)
!               end do
!            end if

       ! ... communication done
       deallocate(sndbuf,rcvbuf,req)

       ! ... all information is known
       deallocate(InterfaceNodes)

    end if
    deallocate(ArrayTmp)

       ! ... now create connectivity graph
       Allocate(ArrayTmp(BufSize))
       Allocate(col_tmp(nPtsOwned*BufSize))
       Allocate(row_ptr(nPtsOwned+1))
       do j = 1, BufSize
          ArrayTmp(j) = -1
          do i = 1, nPtsOwned
             col_tmp((i-1)*BufSize + j) = - 1
          end do
       end do

       cnt2 = 0
       row_ptr(:) = -1
       row_ptr(1) = 0
       do i = nStart, nStart + nPtsOwned - 1
          ! ... need to sort the data
          ArrayTmp(:) = -1
          cnt= 1
          do while(B(cnt,TMgrid%G2L(i)) /= -1)
             ArrayTmp(cnt) = B(cnt,TMgrid%G2L(i))
!             if(TMRank==3) print*,'cnt',cnt,ArrayTmp(cnt)
             cnt = cnt + 1
          end do
          !call graceful_exit(region%myrank, 'done')
          cnt = cnt - 1
 !                if(TMRank==3) print*,TMRank,'before',cnt,ArrayTmp
          Call InsertionSort(ArrayTmp,cnt,.true.)
  !              if(TMRank==3)print*,TMRank,'after',cnt,ArrayTmp
                 !call graceful_exit(region%myrank, 'done')
                cnt = 1
                do j = 1, Bufsize
                   if(ArrayTmp(j) == -1) then
                      cnt = cnt - 1
                      exit
                   else
                      cnt = cnt + 1
                   end if
                end do

          do j = 1, cnt
             cnt2 = cnt2 + 1
             ! ... this is in C counting
             col_tmp(cnt2) = ArrayTmp(j)-1
          end do
          ! ... this is in C counting
          row_ptr(i-nStart + 2) = cnt2
       end do

       Allocate(col_ptr(cnt2))
       do i = 1, cnt2
          col_ptr(i) = col_tmp(i)
       end do

       deallocate(col_tmp)


!      do i = 1, numTMproc
!         if(TMRank == i-1) then
!            print*,'rank',TMRank
!            print*,'col_ptr',(col_ptr(j),j=1,cnt2)
!            print*,'row_ptr',(row_ptr(j),j=1,size(row_ptr))
!         end if
!         call mpi_barrier(TMgrid%comm,ierr)
!      end do
!       call graceful_exit(region%myrank, 'done')


       !     do n = 1, region%input%numTMproc
       !        if(region%TMRank == n-1) then
       !           print*,region%TMRank,'LN',LocalNodeConnections,'TN',TotalNodeConnections
       !        end if
       !        call MPI_BARRIER(region%TMComm,ierr)
       !     end do
       
 
!       call graceful_exit(region%myrank, 'done')

    ! ... get global non-zero patterns
    ! ... loop through all the points that this process ownes
    do n = 1, nPtsOwned
       ! ... global index (original ordering)
       m = n + nStart - 1
       is = row_ptr(n) + 1
       ie = row_ptr(n+1)
       do i = is, ie
          if(col_ptr(i)+1 >= nStart .and. col_ptr(i)+1 < nStart + nPtsOwned) then
             nnz_d(n) = nnz_d(n) + 1
          else
             nnz_o(n) = nnz_o(n) + 1
          end if
       end do
       ! ... add point on the diagonal
       nnz_d(n) = nnz_d(n) + 1
    end do

    deallocate(nnz_o2,LocalNodeConnections,TotalNodeConnections,ArrayTmp)

  end Subroutine SparsityData

  Subroutine SetUpLinearSolver(region)
    
    USE ModGlobal
    USE ModDataStruct
    USE ModPETSc

    Implicit None

    Type(t_region), pointer :: region

    ! ... local variables
    Type(t_sgrid), pointer :: TMgrid
    Type(t_mixt_input), pointer :: input
    Integer, pointer :: nnz_d(:), nnz_o(:), row_ptr(:), col_ptr(:), isVec(:), isgVec(:)
    Integer, pointer :: nnz_dLoc(:), nnz_dGlb(:), nnz_oLoc(:), nnz_oGlb(:)
    Integer :: ng, i, j, k, m, n, p, is, ie, ierr
    Integer :: nPtsOwned, nStart, NewProc, NewNode, nPtsGlb, nStartRenum, nPtsOwnedRenum

    do ng = 1, region%nTMGrids

       ! ... simplicity
       TMgrid    => region%TMgrid(ng)
       input     => TMgrid%input
       nPtsOwned =  TMgrid%nPtsOwned
       nStart    =  TMgrid%nStart
       nPtsGlb   =  TMgrid%nPtsGlb

       ! ... initialize petsc on this grid
       Call InitializePETSc(region,ng)

       TMgrid => region%TMgrid(ng)
       allocate(nnz_d(nPtsOwned),nnz_o(nPtsOwned))
       do n = 1, nPtsOwned
          nnz_d(n) = 0
          nnz_o(n) = 0
       end do


       Call SparsityData(region,ng,nnz_d,nnz_o, row_ptr, col_ptr)

       !          do i = 1, region%input%numTMproc
       !             if(region%TMRank == i-1) then
       !                print*,TMRank,'nnz_d',(nnz_d(j), j = 1,TMgrid%nPtsOwned),'nnz_o', (nnz_o(j), j = 1, TMgrid%nPtsOwned) 
       !             end if
       !             call MPI_BARRIER(TMgrid%Comm,ierr)
       !          end do

       if(input%UseParMetis == TRUE) then
          
          IF((debug_on .eqv. .true.) .and. (region%TMRank == 0)) write(*,'(A)') 'PlasComCM: Calling ParMetis.'
          ! ... get new mapping from ParMetis
          Call RepartitionMatrix(region, ng, col_ptr, row_ptr, isVec, isgVec)
          IF((debug_on .eqv. .true.) .and. (region%TMRank == 0)) write(*,'(A)') 'PlasComCM: Calling ParMetis done.'

!           do i = 1, nPtsGlb
!              if(region%TMRank == 0)print*,i,isVec(i),isgVec(i)
!           end do

          allocate(nnz_dLoc(nPtsGlb),nnz_oLoc(nPtsGlb))
          allocate(nnz_dGlb(nPtsGlb),nnz_oGlb(nPtsGlb))

          do i = 1, nPtsGlb
             nnz_dLoc(i) = 0
             nnz_oLoc(i) = 0
             nnz_oGlb(i) = -1
             nnz_dGlb(i) = -1
          end do

          ! ... get global non-zero patterns
          ! ... loop through all the points that this process ownes
          do n = 1, nPtsOwned
             ! ... global index (original ordering)
             m = n + nStart - 1
             is = row_ptr(n) + 1
             ie = row_ptr(n+1)
             ! ... new processor and number for node m
             NewProc = isVec(m)
             NewNode = isgVec(m)+1

             do i = is, ie
                ! ... new global node number, (1 based counting)
                j = isgVec(col_ptr(i)+1)+1
                ! ... check to see if connecting nodes also lie in NewProc's partition
                if(isVec(col_ptr(i)+1) == NewProc) then
                   nnz_dLoc(NewNode) = nnz_dLoc(NewNode) + 1
                elseif(isVec(col_ptr(i)+1) /= NewProc) then
                   nnz_oLoc(NewNode) = nnz_oLoc(NewNode) + 1
                end if
             end do
          end do

!           if(region%TMRank == 0) then
!              do i = 1, nPtsGlb
!                 print*,'blah',nnz_oLoc(i),nnz_dLoc(i)
!              end do
!           end if

          call MPI_AllReduce(nnz_dLoc, nnz_dGlb, nPtsGlb, MPI_INTEGER, MPI_SUM, TMgrid%Comm, ierr)

          ! ... add A_(ii) = nnz to nnz_dGlb
          do i = 1, nPtsGlb
             nnz_dGlb(i) = nnz_dGlb(i) + 1
          end do

          call MPI_AllReduce(nnz_oLoc, nnz_oGlb, nPtsGlb, MPI_INTEGER, MPI_SUM, TMgrid%Comm, ierr)
!           do i = 1, nPtsGlb
!              if(region%TMrank == 0) print*,nnz_dGlb(i),nnz_oGlb(i)
!           end do

          ! ... determine new processor ownership range
          nStartRenum = nPtsGlb
          nPtsOwnedRenum = 0
          do i = 1, nPtsGlb
             j = isgVec(i)+1
             if(isVec(i) == region%TMRank) then 
                nStartRenum = min(j,nStartRenum)
                nPtsOwnedRenum = max(j,nPtsOwnedRenum)
             end if
          end do
          nPtsOwnedRenum = nPtsOwnedRenum - nStartRenum + 1
          TMgrid%nStartRenum = nStartRenum
          TMgrid%nPtsOwnedRenum = nPtsOwnedRenum

          deallocate(nnz_d,nnz_o)
          allocate(nnz_d(nPtsOwnedRenum), nnz_o(nPtsOwnedRenum))
          ! ... fill local process range non-zero pattern
          do n = 1, nPtsOwnedRenum
             m = n + nStartRenum - 1
             nnz_d(n) = nnz_dGlb(m)
             nnz_o(n) = nnz_oGlb(m)
          end do
          deallocate(nnz_dLoc,nnz_oLoc,nnz_dGlb,nnz_oGlb)

          ! ... create local to renumbered global map
          Allocate(TMgrid%L2GRenum(TMgrid%nPtsLoc))
          do i = 1, TMgrid%nPtsLoc
             ! ... original global node number
             j = TMgrid%L2G(i)
 
             TMgrid%L2GRenum(i) = isgVec(j)+1
          end do

          else
          
             ! ... if not using ParMetis, there is no renumbering for linear solve
             TMgrid%nPtsOwnedRenum = nPtsOwned
             TMgrid%nStartRenum = nStart
          
          end if ! ... input%UseParMetis == TRUE

       Call InitializePETScArrays(region, ng, nnz_d, nnz_o)

       deallocate(nnz_d,nnz_o)
       !call allocate_petsc_objects(region%nTMGrids,TMgrid%ND, TMgrid%comm, region%input%TMSolve,TMRank)

       !call init_petsc_objects(TMgrid%nPtsGlb, TMgrid%nPtsOwned, region%input%TMSolve, TMGrid%ND,TMRank, ng, nnz_d, nnz_o)
    end do

  end Subroutine SetUpLinearSolver

  Subroutine AdjustSolnFreq(region,ng)

    USE ModGlobal
    USE ModDataStruct
    
    Implicit None

    Type(t_region), pointer :: region
    Integer :: ng

    ! ... local variabls
    Type(t_sgrid), pointer :: TMgrid
    Type(t_smixt), pointer :: TMstate
    Type(t_mixt_input), pointer :: input
    Integer :: a0, b0, a, b, t, lcm, gcd

    ! ... intialize
    TMgrid  => region%TMgrid(ng)
    TMstate => region%TMstate(ng)
    input   => TMgrid%input

    ! ... intialize thermal solution frequency
    a0 = 1
    if(input%TMSolve == 1 .or. input%TMSolve == 3) a0 = input%ThermalSolnFreq
    a = a0

    ! ... intialize structural solution frequency
    b0 = 1
    if(input%TMSolve > 1) b0 = input%StructSolnFreq
    b = b0

    ! .. initialize the coupling frequency
    TMstate%CoupleFrequency = 1

    if(a0*b0 == 1) return
    
    ! ... find the lowest common denominator
    do while(b /= 0)
       
       t = b
       b = mod(a,b)
       a = t

    end do

    ! ... greatest common denominator
    gcd = a
    
    ! ... least common multiple
    lcm = a0*b0/gcd

    ! ... set the thermomechanical coupling frequency
    TMstate%CoupleFrequency = lcm

    ! ... adjust restart frequency
    a = input%nrestart
    a = ceiling(dble(max(a,lcm))/dble(lcm))*lcm
    if(a /= input%nrestart) then
       if(region%TMRank == 0) write(*,'(A,I0.1,A,I0.1)') 'PlasComCM: Thermal/structural solution frequency of ',lcm,' does not comply with restart frequency of ',input%nrestart
       if(region%TMRank == 0) write(*,'(A,I0.1)') 'RFlOCM: Adjusting restart frequency to ',a
       input%nrestart = a
       region%input%nrestart = a
    end if

    ! ... adjust the maximum number of steps
    a = input%nstepmax
    a = ceiling(dble(max(a,lcm))/dble(lcm))*lcm
    if(a /= input%nstepmax) then
       if(region%TMRank == 0) write(*,'(A,I0.1,A,I0.1)') 'PlasComCM: Thermal/structural solution frequency of ',lcm,' does not comply with nstepmax of ',input%nstepmax
       if(region%TMRank == 0) write(*,'(A,I0.1)') 'RFlOCM: Adjusting nstepmax to ',a
       input%nstepmax = a
       region%input%nstepmax = a
    end if

  End Subroutine AdjustSolnFreq
    
  Subroutine SetUpMaterialLookupTable(region)
    
    USE ModGlobal
    USE ModDataStruct
    USE ModIO

    Implicit None

    type(t_region), pointer :: region
    type(t_sgrid), pointer :: TMgrid
    type(t_mixt_input), pointer :: input
    type(t_smixt), pointer :: TMstate
    
    Integer, Parameter :: prp_unit = 51
    Character(LEN=80) :: strbuf
    real(rfreal) :: tmp_prp_data(3)
    Integer :: ng, EntryNum, io_err
    Integer :: nPrpPts, nTblPts, i, j, k, i1, i2
    real(rfreal), pointer :: MtlPrpTbl(:,:)
    Integer, pointer :: indices(:)


    do ng = 1, region%nTMGrids
    
       TMgrid  => region%TMgrid(ng)
       TMstate => region%TMstate(ng)
       input   => TMgrid%input 

       if(region%TMRank == 0) write(*,'(A,I2)') 'PlasComCM: Reading temperature dependent material table "'//trim(input%prp_fname)//'" on solid grid ',ng

       ! ... step one : read the entire material property file
       Open (unit=prp_unit, file=trim(input%prp_fname), status='old')

       nPrpPts = 0; io_err = FALSE
       Do while (io_err == FALSE)
          Read (prp_unit,'(A)',iostat=io_err) strbuf
          If ((io_err == FALSE) .AND. (strbuf(1:1) /= '#') .AND. (len_trim(strbuf) /= 0)) Then
             Read (strbuf(:),*) k, tmp_prp_data(:)
             nPrpPts = nPrpPts + 1
          End If
       End Do
       Rewind(prp_unit)

       ! ... record number of data points
       input%nPrpPts = nPrpPts
    
       ! ... allocate master boundary condition array
       allocate(input%prp_data(nPrpPts,3))

       ! ... reread
       nPrpPts = 0; io_err = FALSE
       Do while (io_err == FALSE)
          Read (prp_unit,'(A)',iostat=io_err) strbuf
          If ((io_err == FALSE) .AND. (strbuf(1:1) /= '#') .AND. (len_trim(strbuf) /= 0)) Then
             Read (strbuf(:),*) k, tmp_prp_data(:)
             nPrpPts = nPrpPts + 1
             Read (strbuf(:),*) k, input%prp_data(nPrpPts,:)
          End If
       End Do
       Close (prp_unit)

       !write(*,'(A,I2,A)') 'There are ',nPrpPts,' data points in the material property file'
       !do i = 1, nPrpPts
       !   write(*,'(A,I2,3(x,E16.7))') 'Data point: ',i,input%prp_data(i,:)
       !end do
    
       ! ... now fill in the lookup table
       TMstate%nTblPts = input%nTblPts
       
       ! ... allocate space for the lookup table
       ! ... [Temperature, Young's Modulus, Poisson's Ratio]
       allocate(TMstate%MtlPrpTbl(TMstate%nTblPts,3))
       
       ! ... simplicity
       MtlPrpTbl => TMstate%MtlPrpTbl
       nTblPts   =  TMstate%nTblPts
       
       ! ... set the indices corresponding to the input data
       allocate(indices(nPrpPts))

       indices(1) = 1
       indices(nPrpPts) = nTblPts
       
       do i = 1, 3
          MtlPrpTbl(1,i)       = input%prp_data(1,i)
          MtlPrpTbl(nTblPts,i) = input%prp_data(nPrpPts,i)
       end do
       
       do i = 2, nPrpPts-1
          indices(i) = int((input%prp_data(i,1)-input%prp_data(1,1))/(input%prp_data(nPrpPts,1)-input%prp_data(1,1))*nTblPts) + 1
          ! ... adjust temperature to result in an equally spaced table
          MtlPrpTbl(indices(i),1) = dble(indices(i)-1)/dble(nTblPts-1) * (MtlPrpTbl(nTblPts,1)-MtlPrpTbl(1,1)) + MtlPrpTbl(1,1)
          MtlPrpTbl(indices(i),2) = input%prp_data(i,2)
          MtlPrpTbl(indices(i),3) = input%prp_data(i,3)
       end do

       ! ... fill in the rest of the table
       do i = 2, nPrpPts
          i1 = indices(i-1)
          i2 = indices(i)
          do j = i1, i2
             ! ... temperature
             MtlPrpTbl(j,1) = dble(j-i1)/dble(i2-i1)*(MtlPrpTbl(i2,1)-MtlPrpTbl(i1,1)) + MtlPrpTbl(i1,1)
             ! ... Young's Modulus
             MtlPrpTbl(j,2) = dble(j-i1)/dble(i2-i1)*(MtlPrpTbl(i2,2)-MtlPrpTbl(i1,2)) + MtlPrpTbl(i1,2)
             ! ... Poisson's Ratio
             MtlPrpTbl(j,3) = dble(j-i1)/dble(i2-i1)*(MtlPrpTbl(i2,3)-MtlPrpTbl(i1,3)) + MtlPrpTbl(i1,3)
          end do
       end do

    end do

    ! if(region%TMrank == 0) then
    !    write(*,'(A,I3,A)') 'There are ',nTblPts,' in the lookup table'
    !    write(*,'(A)') ' #   Temperature  Youngs Modulus  Poisson Ratio'
    !    do i = 1, nTblPts
    !       write(*,'(I,3(x,D11.4))') i,(MtlPrpTbl(i,j),j=1,3)
    !    end do
    ! end if
       
    

  end Subroutine SetUpMaterialLookupTable
    
  Subroutine SetUpSolidDataRecord(region)
    
    USE ModDataStruct
    USE ModGlobal
    
    Implicit None
    
    Type(t_region), pointer :: region
    
    ! ... local variables
    Type(t_smixt), pointer :: TMstate
    Type(t_mixt_input), pointer :: input
    Integer :: ng, numSamplesTotal

    do ng = 1, region%nTMGrids
       
       ! ... simplicity
       input   => region%input
       TMstate => region%TMstate(ng)
       
       ! ... total number of samples 
       numSamplesTotal = input%noutput/input%StructSolnFreq

       allocate(TMstate%SDTime(numSamplesTotal))
       allocate(TMstate%PowerIn(numSamplesTotal))

       allocate(TMstate%KinEnergy(NumSamplesTotal))

       ! ... initialize counter
       TMstate%numDataSample = 0
      
    end do

  end Subroutine SetUpSolidDataRecord

    
  Subroutine InsertionSort(Array,Num,RemoveDuplicates)

    USE ModGlobal
    USE ModDataStruct

    Implicit None

    Integer, pointer :: Array(:), ArrayTmp(:)
    Integer :: Num, iCur, TmpVal, j, cnt
    Logical :: RemoveDuplicates

    iCur = 2
    do while(iCur <= Num)
       TmpVal = Array(iCur)
       do j = iCur - 1, 1, -1
          if(TmpVal < Array(j)) then
             Array(j+1) = Array(j)
             Array(j) = TmpVal
          elseif(TmpVal > Array(j)) then 
             Array(j+1) = TmpVal
             exit
          end if
       end do
       iCur = iCur + 1
    end do
    
    if(RemoveDuplicates .eqv. .true.) then
       Allocate(ArrayTmp(size(Array)))
       ArrayTmp(1) = Array(1)
       cnt = 1
       do j = 2, Num
          if(Array(j) == Array(j-1)) cycle
          cnt = cnt+1
          ArrayTmp(cnt) = Array(j)
       end do

          
       do j = 1, Num
          Array(j) = -1
       end do
       
       Do j = 1, cnt
          Array(j) = ArrayTmp(j)
       end do

       deallocate(ArrayTmp)
    end if

  end Subroutine InsertionSort

  Subroutine CleanUpThermomechanical(region)

    USE ModGlobal
    USE ModDataStruct
    USE ModPETSc

    Implicit None

    Type(t_region), pointer :: region
    
    ! ... local variables
    Integer :: ng

    do ng = 1, region%nTMGrids
       
       call CleanUpPetSc(region,ng)
    end do
    
  end Subroutine CleanUpThermomechanical
    
end Module ModInitTM
