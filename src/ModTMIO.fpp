! Copyright (c) 2015, University of Illinois at Urbana-Champaign, XPACC
! License: MIT, http://opensource.org/licenses/MIT
!-----------------------------------------------------------------------
!
! ModTMIO.f90
!
! - subroutines to handle thermomechanical solver I/O
!
! Revision history
! - 07 Mar 2012 : CMO - initial code
!
!
!-----------------------------------------------------------------------
Module ModTMIO

CONTAINS

  subroutine read_TMrestart(region)

    USE ModGlobal
    USE ModDataStruct
    USE ModMPI
    USE ModPLOT3D_IO

    Implicit None

    Type (t_region), Pointer :: region

    ! ... local variables
    Type (t_smixt), Pointer :: TMstate
    Type (t_sgrid), Pointer :: TMgrid
    real(rfreal), Dimension(:,:,:,:,:), pointer :: Y ! ... read the whole grid for MONOLITHIC, JKim 04/2008
    real(rfreal), Dimension(:,:,:,:), pointer :: X ! ... reduction in rank for CHIMERA, JKim 04/2008
    real(rfreal) :: tau(4), timer(2)
    character(len=2) :: prec, gf, vf
    Integer, Dimension(:,:), Pointer :: ND
    Integer :: NDIM, ngrid, ng, var(5), m, k, j, i, l0, N(MAX_ND), p, TMRank, TMComm, l1
    Integer :: gridID ! ... specify which grid we want to read, JKim 04/2008
    Real(rfreal), Dimension(:,:), pointer :: ptr_to_data ! ... Adjoint N-S
    Character(LEN=80) :: TMrestart_fname, TMrestartdot_fname
    Logical :: file_exists
    Integer :: ierr

    timer(1) = MPI_WTime()

    TMRank = region%TMRank
    TMComm = region%TMComm

    call mpi_barrier(TMComm,ierr)
    if((debug_on .eqv. .true.) .and. (TMRank == 0)) write(*,*) 'PlasComCM: ReadGridSize'
    CALL ReadGridSize(NDIM, ngrid, ND, region%input%TMrestart_fname, TMRank)
    call mpi_barrier(TMComm,ierr)
    if((debug_on .eqv. .true.) .and. (TMRank == 0)) write(*,*) 'PlasComCM: read_restart done with grid size'
    CALL read_soln_header(NDIM, ngrid, ND, region%input%TMrestart_fname, tau)
    call mpi_barrier(TMComm,ierr)
    if((debug_on .eqv. .true.) .and. (TMRank == 0)) write(*,*) 'PlasComCM: read_restart done with soln header'

 
    if (TMRank == 0) Write (*,'(A)') 'PlasComCM: Reading "'//trim(region%input%TMrestart_fname)//'".'

    do p = 0, region%input%numTMproc-1

      if (TMRank == p) then

        Do ng = 1, region%nTMGrids

          ! ... this grid's data
          TMgrid  => region%TMgrid(ng)
          TMstate => region%TMstate(ng)

          ! ... nullify
          nullify(ptr_to_data)
          ptr_to_data => TMstate%P3D

          call read_single_TMsoln_low_mem(NDIM, ND, region, TMgrid, region%input%TMrestart_fname, ptr_to_data, TMgrid%iGridGlobal)

          call mpi_barrier(TMComm, ierr)

          ! ... update the time
          !do k = 1, grid%nCells
            !TMstate%time(:) = tau(4)
          !end do

          ! ... make sure that restart is at the same iteration as the fluid
          if(TMgrid%input%TMOnly /= TRUE) then
             if(nint(tau(1)) /= region%input%nstepi) then
                write(*,'(A)') 'PlasComCM: Inconsistent restart iteration numbers between fluid and solid'
             end if
          else
             ! ... update the iteration number
             region%input%nstepi = nint(tau(1))

          end if
          
          ! ... temporary fix
          TMgrid%input%nstepi = region%input%nstepi
          
        end do

      end if

    end do

    ! ... now fill solution vector
    do j = 1, size(TMstate%q,2)
       do i = 1, TMgrid%nPtsLoc
          TMstate%q(i,j) = TMstate%P3D(TMgrid%L2P3D(i),j)
          !print*,'from restart: i,j,TMstate%(i,j)',i,j,TMstate%q(i,j)
       end do
    end do
    
    deallocate(ND)

    if ((debug_on .eqv. .true.) .and. (TMRank == 0)) Write (*,'(A)') 'PlasComCM: Done reading "'//trim(region%input%TMrestart_fname)//'".'

    if(region%input%TMSolve > 1 .and. (region%input%StructuralTimeScheme == DYNAMIC_SOLN .or. region%input%ThermalTimeScheme == DYNAMIC_SOLN)) then
       ! ... check to see that the file is there
       INQUIRE(file=region%input%TMrestartdot_fname,EXIST=file_exists)
       if(file_exists) then
          
          if (TMRank == 0) Write (*,'(A)') 'PlasComCM: Reading "'//trim(region%input%TMrestartdot_fname)//'".'
       
          do p = 0, TMgrid%input%numTMproc-1
             
             if (TMRank == p) then

                call Read_Func(NDIM, ng, ND, Y, region%input%TMrestartdot_fname)
                                
                Do ng = 1, region%nTMGrids
                   
                   ! ... this grid's data
                   TMgrid  => region%TMgrid(ng)
                   TMstate => region%TMstate(ng)
                   
                   N(:) = 1
                   do i = 1, TMgrid%ND
                      N(i) = TMgrid%ie(i) - TMgrid%is(i) + 1
                   end do
                   
!                   nullify(ptr_to_data)
!                   ptr_to_data => TMstate%P3Ddot(ng,:,:,:,:)

                   call mpi_barrier(TMComm, ierr)
                   
                   do k = TMgrid%is(3), TMgrid%ie(3)
                      do j = TMgrid%is(2), TMgrid%ie(2)
                         do i = TMgrid%is(1), TMgrid%ie(1)
                            l0 = (k-1)*ND(ng,1)*ND(ng,2) + (j-1)*ND(ng,1) + (i-1) + 1
                            l1 = (k-TMgrid%is(3))*N(2)*N(1) + (j-TMgrid%is(2))*N(1) + (i-TMgrid%is(1)) + 1
                            do m  = 1, ND(ng,4)
                               TMstate%P3Ddot(l1,m) = Y(ng,i,j,k,m)
                            end do
                         end do
                      end do
                   end do

                end do
                
                deallocate(Y,ND)
                
                ! ... now fill solution vector
                do j = 1, size(TMstate%qdot,2)
                   do i = 1, TMgrid%nPtsLoc
                      TMstate%qdot(i,j) = TMstate%P3Ddot(TMgrid%L2P3D(i),j)
                      !print*,'from restart: i,j,TMstate%qdot(i,j)',i,j,TMstate%qdot(i,j)
                   end do
                end do
                
             end if
             
          end do
          if ((debug_on .eqv. .true.) .and. (TMRank == 0)) Write (*,'(A)') 'PlasComCM: Done reading "'//trim(region%input%TMrestartdot_fname)//'".'
       else
          if(region%TMRank == 0) write(*,'(A)') 'PlasComCM: File "'//trim(region%input%TMrestartdot_fname)//'" missing, assuming static initial condition.'
       end if
    end if

    

    call MPI_BARRIER(TMgrid%Comm,ierr)


    return

  end subroutine read_TMrestart

  subroutine Read_And_Distribute_Single_TMGrid(region, ng_in_file)

    USE ModGlobal
    USE ModDataStruct
    USE ModMPI
    USE ModPLOT3D_IO
    Implicit None

    Type (t_region), pointer :: region

    ! ... local variables
    Real(rfreal), Dimension(:,:,:,:,:), Pointer :: X
    Integer, Dimension(:,:,:,:), Pointer :: IBLANK
    Character(LEN=2) :: prec, gf, vf, ib
    Integer :: ngrid, I, J, K, L0, Nx, Ny, Nz, NDIM, M, ftype, ng, p, dir
    Integer, Dimension(:,:), Pointer :: ND
    Type (t_mixt_input), pointer :: input
    Type (t_sgrid), pointer :: TMgrid
    Integer :: N(MAX_ND), vds(MAX_ND), ng_in_file, ierr

    Do ng = 1, region%nTMGrids

       TMgrid  => region%TMgrid(ng)
       input => TMgrid%input

       if (TMgrid%iGridGlobal == ng_in_file) then

          do p = 0, TMgrid%numproc_inComm-1

             if ( TMgrid%myrank_inComm == p ) then

                ! ... recheck grid size
                call ReadGridSize(NDIM, ngrid, ND, input%TMgrid_fname, region%myrank, ib)

                ! ... grid%i{s,e} contain beginning & ending indices
                call read_single_TMgrid_low_mem(NDIM, region, input%TMgrid_fname, TMgrid, ng_in_file, ib)

                deallocate(ND)

             end if

             if ( TMgrid%numproc_inComm > 1 ) Call MPI_BARRIER(TMgrid%comm, IERR)

          end do

       end if

    End Do

  End Subroutine Read_And_Distribute_Single_TMGrid

  subroutine read_single_TMgrid_low_mem(NDIM, region, grid_fname, TMgrid, gridID, ib)

    USE ModGlobal
    USE ModDataStruct
    USE ModMPI
    USE ModPLOT3D_IO
    USE ModIO
    Implicit None

    type(t_region), pointer :: region
    type(t_sgrid), pointer :: TMgrid
    Character(LEN=80) :: grid_fname
    integer :: gridID ! ... specify which grid we want to read, JKim 04/2008
    integer :: NDIM   ! ... dimension in grid file
    Character(LEN=2) :: ib

    ! ... local variables
    Real(rfreal), Dimension(:,:,:,:), Pointer :: X
    Integer, Dimension(:,:,:), Pointer :: IBLANK
    Integer :: ngrid, I, J, K, L0, Nx, Ny, Nz, M, ftype, dir, N(MAX_ND)
    Integer :: fh, filetype
    Integer :: offset
    Integer :: ibuf(1), is(MAX_ND), ie(MAX_ND)

    ! ... seek to the current array from the beginning of the file
    call plot3d_gridfile_offset(gridID, NDIM, region%nTMGridsGlobal, TMgrid%AllGridsGlobalSize, ib, offset)

    ! ... extent of this grid (in C-counting)
    is = 0; ie = 0; N = 1;
    do j = 1, TMgrid%ND
       is(j) = TMgrid%is(j)-1
       ie(j) = TMgrid%ie(j)-1
       N(j) = ie(j) - is(j) + 1
    end do

    ! ... allocate buffer
    allocate(X(N(1),N(2),N(3),3))
    if (ib(1:1) .eq. 'y') Allocate(IBLANK(N(1),N(2),N(3)))

    ! ... read the file in binary mode
    if (ib(1:1) .eq. 'n') then
       call read_plot3d_single_grid_whole_format_no_IBLANK_low_mem(NDIM, TMgrid%GlobalSize, offset, is, ie, gridID, X, grid_fname, len_trim(grid_fname))
       TMgrid%IBLANK(:) = 1
    else
       call read_plot3d_single_grid_whole_format_with_IBLANK_low_mem(NDIM, TMgrid%GlobalSize, offset, is, ie, gridID, X, IBLANK, grid_fname, len_trim(grid_fname))
    end if

    ! ... unpack buffer
    do k = TMgrid%is(3), TMgrid%ie(3)
       do j = TMgrid%is(2), TMgrid%ie(2)
          do i = TMgrid%is(1), TMgrid%ie(1)
             do m = 1, TMgrid%ND
                l0 = (k-TMgrid%is(3))*(TMgrid%ie(1)-TMgrid%is(1)+1) &
                     *(TMgrid%ie(2)-TMgrid%is(2)+1) &
                     + (j-TMgrid%is(2))*(TMgrid%ie(1)-TMgrid%is(1)+1) + i-TMgrid%is(1)+1
                TMgrid%XYZ(l0,m) = X(i-TMgrid%is(1)+1,j-TMgrid%is(2)+1,k-TMgrid%is(3)+1,m)
             end do
          end do
       end do
    end do

    if (ib(1:1) .eq. 'y') then
       ! ... unpack buffer
       do k = TMgrid%is(3), TMgrid%ie(3)
          do j = TMgrid%is(2), TMgrid%ie(2)
             do i = TMgrid%is(1), TMgrid%ie(1)
                l0 = (k-TMgrid%is(3))*(TMgrid%ie(1)-TMgrid%is(1)+1) &
                     *(TMgrid%ie(2)-TMgrid%is(2)+1) &
                     + (j-TMgrid%is(2))*(TMgrid%ie(1)-TMgrid%is(1)+1) + i-TMgrid%is(1)+1
                TMgrid%IBLANK(l0) = IBLANK(i-TMgrid%is(1)+1,j-TMgrid%is(2)+1,k-TMgrid%is(3)+1)
             end do
          end do
       end do
    end if

    deallocate(X)
    if (ib(1:1) .eq. 'y') deallocate(IBLANK)

  end subroutine read_single_TMgrid_low_mem

!$
  subroutine read_single_TMsoln_low_mem(NDIM, ND, region, TMgrid, grid_fname, dbuf, gridID)



    USE ModGlobal
    USE ModDataStruct
    USE ModMPI
    USE ModPLOT3D_IO
    USE ModIO

    Implicit None

    Type(t_region), pointer :: region
    Type(t_sgrid), Pointer :: TMgrid
    Real(rfreal), Dimension(:,:), Pointer :: dbuf
    Character(LEN=80) :: grid_fname
    integer :: gridID ! ... specify which grid we want to read, JKim 04/2008
    integer :: NDIM   ! ... dimension in grid file
    integer, Dimension(:,:), Pointer :: ND

    ! ... local variables
    Real(rfreal), Dimension(:,:,:,:), Pointer :: X
    Character(LEN=2) :: prec, gf, vf, ib
    Integer :: ngrid, I, J, K, L0, Nx, Ny, Nz, M, ftype, dir, N(MAX_ND)
    Integer :: fh, filetype, p
    Integer :: offset, var(MAX_ND+2)
    Integer :: ibuf(1), is(MAX_ND), ie(MAX_ND)
    Integer, Parameter :: sizeof_int = 4, sizeof_dbl = 8

    ! ... pointer
    Do m = 1, 5
      var(m) = m
    End Do

    If (NDIM == 3 .and. region%input%ND == 2 .and. ND(1,3) == 1) Then
      if (region%myrank == 0) print *, 'PlasComCM: changing var(4) = 5 in read_single_TMsoln_low_mem().'
      var(4) = 5
    End If


    If (NDIM == 3 .and. region%input%ND == 1 .and. SUM(ND(1,2:3)) == 2) Then
      if (region%myrank == 0) print *, 'PlasComCM: changing var(3) = 5 in read_single_TMsoln_low_mem().'
      var(3) = 5
    End If


    ! ... seek to the current array from the beginning of the file
    call plot3d_solnfile_offset(gridID, NDIM, region%nGridsGlobal, TMgrid%AllGridsGlobalSize, offset)
    
    ! ... skip another 2 x sizeof(int) + 4 x sizeof(double)
    offset = offset + 2 * sizeof_int + 4 * sizeof_dbl

    ! ... extent of this grid (in C-counting)
    is = 0; ie = 0; N = 1;
    do j = 1, TMgrid%ND
       is(j) = TMgrid%is(j)-1
       ie(j) = TMgrid%ie(j)-1
       N(j) = ie(j) - is(j) + 1
    end do

    ! ... allocate buffer
    allocate(X(N(1),N(2),N(3),5))

    ! ... read the file in binary mode
    call read_plot3d_single_soln_whole_format_low_mem(NDIM, TMgrid%GlobalSize, offset, is, ie, gridID, X, grid_fname, len_trim(grid_fname))

    ! ... unpack buffer
    do k = TMgrid%is(3), TMgrid%ie(3)
       do j = TMgrid%is(2), TMgrid%ie(2)
          do i = TMgrid%is(1), TMgrid%ie(1)
             do m = 1, TMgrid%ND+2
                l0 = (k-TMgrid%is(3))*(TMgrid%ie(1)-TMgrid%is(1)+1) &
                     *(TMgrid%ie(2)-TMgrid%is(2)+1) &
                     + (j-TMgrid%is(2))*(TMgrid%ie(1)-TMgrid%is(1)+1) + i-TMgrid%is(1)+1
                dbuf(l0,m) = X(i-TMgrid%is(1)+1,j-TMgrid%is(2)+1,k-TMgrid%is(3)+1,var(m))
             end do
          end do
       end do
    end do

    deallocate(X)

  end subroutine read_single_TMsoln_low_mem

  subroutine LocaltoP3D(region)

    USE ModGlobal
    USE ModDataStruct

    Implicit None

    Type(t_region), pointer :: region

    ! ... local variables
    Type(t_sgrid), pointer :: TMgrid
    Type(t_mixt_input), pointer :: input
    Integer :: ND, N(MAX_ND), Ne(MAX_ND)
    Integer :: nTMGrids, NdsPer, nPtsLoc
    Integer :: ng, i, j, k, dir, le, koffset, m
    Integer, allocatable :: ElNde(:)
    Integer, pointer :: LM(:,:)

    ! ... when using quadratic elements, need map from Plot3D nodes to element nodes

    ! ... simplicity
    ND = region%input%ND
    nTMGrids = region%nTMGrids
    N(:) = 1
    Ne(:) = 1

    ! ... loop through grids
    do ng = 1, nTMGrids

       ! ... simplicity
       TMgrid  => region%TMgrid(ng)
       input   => TMgrid%input
       LM      => TMgrid%LM
       nPtsLoc =  TMgrid%nPtsLoc

       ! ... number of plot3D points on grid (not neccessarily number of nodes)
       do dir = 1, ND
          N(dir) = TMgrid%ie(dir) - TMgrid%is(dir) + 1
       end do

       ! ... number of elements
       if(input%ElemType == LINEAR) then
          do dir = 1, ND
             Ne(dir) = (N(dir) - 1)
          end do
       elseif(input%ElemType == QUADRATIC) then
          do dir = 1, ND
             Ne(dir) = (N(dir) - 1)/2;
          end do
       end if

       ! ... number of nodes per element
       if(input%ElemType == LINEAR) then
          NdsPer = 4 + (ND-2)*4
       elseif(input%ElemType == QUADRATIC) then
          NdsPer = 8 + (ND-2)*12
       end if

       ! ... allocate
       allocate(ElNde(NdsPer))
       allocate(TMgrid%L2P3D(nPtsLoc))

       ! ... initialize
       TMgrid%L2P3D(:) = 0
       ElNde(:) = 0
       if(input%ElemType == QUADRATIC) then
          do k = 1, Ne(3)
             do j = 1, Ne(2)
                do i = 1, Ne(1)

                   ! ... element index 
                   le = (k-1)*Ne(2)*Ne(1) + (j-1)*Ne(1) + (i-1) + 1

                   ! ... element local k = 0 plane
                   koffset = (k-1)*N(1)*N(2)*2

                   ! ... find local node index for this element
                   ElNde(1) = koffset + (j-1)*N(1)*2 + (i-1)*2 + 1
                   ElNde(2) = koffset + (j-1)*N(1)*2 + (i  )*2 + 1
                   ElNde(3) = koffset + (j  )*N(1)*2 + (i  )*2 + 1
                   ElNde(4) = koffset + (j  )*N(1)*2 + (i-1)*2 + 1

                   ElNde(1+2**ND) = koffset + (j-1)*N(1)*2 + (i-1)*2 + 2
                   ElNde(2+2**ND) = koffset + (j-1)*N(1)*2 + (i  )*2 + 1 + N(1)
                   ElNde(3+2**ND) = koffset + (j  )*N(1)*2 + (i-1)*2 + 2
                   ElNde(4+2**ND) = koffset + (j-1)*N(1)*2 + (i-1)*2 + 1 + N(1)

                   ! ... only have 8 nodes in element if 2D, onto the next element
                   if(ND == 2) then
                      do m = 1, NdsPer
                         TMgrid%L2P3D(LM(m,le)) = ElNde(m)
                      end do
                      cycle
                   end if

                   ! ... moving in the third dimension
                   ! ... element local k = 1/2 plane
                   koffset = (k-1)*N(1)*N(2)*2 + N(1)*N(2)

                   ElNde(13) = koffset + (j-1)*N(1)*2 + (i-1)*2 + 1
                   ElNde(14) = koffset + (j-1)*N(1)*2 + (i  )*2 + 1
                   ElNde(15) = koffset + (j  )*N(1)*2 + (i  )*2 + 1
                   ElNde(16) = koffset + (j  )*N(1)*2 + (i-1)*2 + 1

                   ! ... element local k = 1 plane
                   koffset = (k)*N(1)*N(2)*2

                   ! ... find local node index for this element
                   ElNde(5) = koffset + (j-1)*N(1)*2 + (i-1)*2 + 1
                   ElNde(6) = koffset + (j-1)*N(1)*2 + (i  )*2 + 1
                   ElNde(7) = koffset + (j  )*N(1)*2 + (i  )*2 + 1
                   ElNde(8) = koffset + (j  )*N(1)*2 + (i-1)*2 + 1

                   ElNde(17) = koffset + (j-1)*N(1)*2 + (i-1)*2 + 2
                   ElNde(18) = koffset + (j-1)*N(1)*2 + (i  )*2 + 1 + N(1)
                   ElNde(19) = koffset + (j  )*N(1)*2 + (i-1)*2 + 2
                   ElNde(20) = koffset + (j-1)*N(1)*2 + (i-1)*2 + 1 + N(1)

                   do m = 1, NdsPer
                      TMgrid%L2P3D(LM(m,le)) = ElNde(m)
                   end do
                end do
             end do
          end do
       elseif(input%ElemType == LINEAR) then 
          do i = 1, TMgrid%nPtsLoc
             TMgrid%L2P3D(i) = i
          end do
       end if
       
    end do

  end subroutine LocaltoP3D


  subroutine LM2DtoPatchP3D(region)

    USE ModGlobal
    USE ModDataStruct

    Implicit None

    Type(t_region), pointer :: region

    ! ... local variables
    Type(t_sgrid), pointer :: TMgrid
    Type(t_spatch), pointer :: TMpatch
    Type(t_mixt_input), pointer :: input
    Integer :: ND, Np(MAX_ND), Npe(MAX_ND),Nes(MAX_ND), Nee(MAX_ND),Npes(MAX_ND), Npee(MAX_ND)
    Integer :: nTMGrids, NdsPer, nPtsLoc, normDir, sgn
    Integer :: gridID, ei, ej, ek, dir, lpe, m, npatch, j
    Integer, allocatable :: ElNde(:)

    ! ... when using quadratic elements, need map from Plot3D nodes to element nodes

    ! ... simplicity
    ND = region%input%ND
    nTMGrids = region%nTMGrids
    Np(:) = 1
    Npe(:) = 1
    Nes(:) = 1
    Nee(:) = 1
    Npes(:) = 1
    Npee(:) = 1

    ! ... number of nodes in surface element
    if(region%input%ElemType == LINEAR) then
       NdsPer = 2 + (ND-2)*2
    elseif(region%input%ElemType == QUADRATIC) then
       NdsPer = 3 + (ND-2)*5
    end if

       ! ... allocate
       allocate(ElNde(NdsPer))


    ! ... loop through the patches
    do npatch = 1, region%nTMPatches

       TMpatch => region%TMpatch(npatch)
       TMgrid  => region%TMgrid(TMpatch%gridID)
       input   => TMgrid%input
       normDir = abs(TMpatch%normDir)
       sgn     = normDir/TMpatch%normDir

       ! ... how many points in the patch and grid on this partition
       do j = 1, TMgrid%ND
          Np(j) = TMpatch%ie(j) - TMpatch%is(j) + 1
       end do

       ! ... how many elements are on this patch
       Npe(:) = 1
       if(input%ElemType == QUADRATIC) then
          do dir = 1, ND
             Npe(dir) = (TMpatch%ie(dir)-TMpatch%is(dir))/2
          end do
       elseif(input%ElemType == LINEAR) then
          do dir = 1, ND
             Npe(dir) = (TMpatch%ie(dir)-TMpatch%is(dir))
          end do
       end if

       ! ... starting and ending elements in this process' partition of the grid
       Nes(:) = 1; Nee(:) = 1
       if(input%ElemType == QUADRATIC) then
          do dir = 1, ND
             Nes(dir) = (TMgrid%is(dir)-1)/2+1
             Nee(dir) = (TMgrid%ie(dir)-1)/2
          end do
       elseif(input%ElemType == LINEAR) then
          do dir = 1, ND
             Nes(dir) = TMgrid%is(dir)
             Nee(dir) = TMgrid%ie(dir)-1
          end do
       end if
       
       ! ... starting and ending elements in the patch
       Npes(:) = 1; Npee(:) = 1
       do dir = 1, ND
          if(Npe(dir) == 0) then
             ! ... keep counting correct if patch is on face with normDir == dir
             if(sgn<0) then
                Npes(dir) = Nee(dir)
                Npee(dir) = Nee(dir)
             else
                Npes(dir) = Nes(dir)
                Npee(dir) = Nes(dir)
             end if
             ! ... to keep product(Npe) >= 1
             Npe(dir) = 1
          else
             if(input%ElemType == QUADRATIC) then
                Npes(dir) = (TMpatch%is(dir)-1)/2+1
                Npee(dir) = (TMpatch%ie(dir)-1)/2
             elseif(input%ElemType == LINEAR) then
                Npes(dir) = TMpatch%is(dir)
                Npee(dir) = TMpatch%ie(dir)-1                
             end if
          end if
       end do

       ! ... allocate 
       allocate(TMpatch%LMP3D2D(NdsPer,product(Npe(:))))
       
       ! ... initialize
       TMpatch%LMP3D2D(:,:) = 0
       ElNde(:) = 0

       if(input%ElemType == QUADRATIC) then
          do ek = Npes(3), Npee(3)
             do ej = Npes(2), Npee(2)
                do ei = Npes(1), Npee(1)
                   
                   lpe = (ek-Npes(3))*Npe(2)*Npe(1) + (ej-Npes(2))*Npe(1) + (ei-Npes(1)) + 1
                   
                   if(normDir == 1) then

                      ElNde(1) = (ek-Npes(3))*Np(2)*2 + (ej-Npes(2)  )*2 + 1
                      ElNde(2) = (ek-Npes(3))*Np(2)*2 + (ej-Npes(2)+1)*2 + 1
                      
                      ElNde(3+(ND-2)*2) = (ek-Npes(3))*Np(2)*2 + (ej-Npes(2))*2 + 2

                      ! ... only have 3 nodes in surface element if 2D, onto the next element
                      if(ND == 2) then
                         do m = 1, NdsPer
                            TMpatch%LMP3D2D(m,lpe) = ElNde(m)
                         end do
                         cycle
                      end if

                      ElNde(3) = (ek-Npes(3))*Np(2)*2 + (ej-Npes(2)+1)*2 + 1
                      ElNde(4) = (ek-Npes(3))*Np(2)*2 + (ej-Npes(2)  )*2 + 1

                      ElNde(6) = (ek-Npes(3)  )*Np(2)*2 + Np(2) + (ej-Npes(2)+1)*2 + 1
                      ElNde(7) = (ek-Npes(3)+1)*Np(2)*2 + (ej-Npes(2))*2 + 2
                      ElNde(8) = (ek-Npes(3)  )*Np(2)*2 + Np(2) + (ej-Npes(2)+1)*2 + 1

                   elseif(normDir == 2) then

                      ElNde(1) = (ek-Npes(3))*Np(1)*2 + (ei-Npes(1))*2 + 1
                      ElNde(2) = (ek-Npes(3))*Np(1)*2 + (ei - Npes(1) + 1)*2 + 1
                      
                      ElNde(3+(ND-2)*2) = (ek-Npes(3))*Np(1)*2 + (ei-Npes(1))*2 + 2

                      ! ... only have 3 nodes in surface element if 2D, onto the next element
                      if(ND == 2) then
                         do m = 1, NdsPer
                            TMpatch%LMP3D2D(m,lpe) = ElNde(m)
                         end do
                         cycle
                      end if

                      ElNde(3) = (ek-Npes(3)+1)*Np(1)*2 + (ei-Npes(1)+1)*2 + 1
                      ElNde(4) = (ek-Npes(3)+1)*Np(1)*2 + (ei-Npes(1)  )*2 + 1

                      ElNde(6) = (ek-Npes(3)  )*Np(1)*2 + Np(1) + (ei-Npes(1)+1)*2 + 1
                      ElNde(7) = (ek-Npes(3)+1)*Np(1)*2 + (ei-Npes(1))*2 + 2
                      ElNde(8) = (ek-Npes(3)  )*Np(1)*2 + Np(1) + (ei-Npes(1)  )*2 + 1
                      
                   elseif(normDir == 3) then

                      ElNde(1) = (ej-Npes(2))*Np(1)*2 + (ei-Npes(1)  )*2 + 1
                      ElNde(2) = (ej-Npes(2))*Np(1)*2 + (ei-Npes(1)+1)*2 + 1
                      
                      ElNde(5) = (ej-Npes(2))*Np(1)*2 + (ei-Npes(1))*2 + 2

                      ElNde(3) = (ej-Npes(2)+1)*Np(1)*2 + (ei-Npes(1)+1)*2 + 1
                      ElNde(4) = (ej-Npes(2)+1)*Np(1)*2 + (ei-Npes(1)  )*2 + 1

                      ElNde(6) = (ej-Npes(2)  )*Np(1)*2 + Np(1) + (ei-Npes(1)+1)*2 + 1
                      ElNde(7) = (ej-Npes(2)+1)*Np(1)*2 + (ei-Npes(1))*2 + 2
                      ElNde(8) = (ej-Npes(2)  )*Np(1)*2 + Np(1) + (ei-Npes(1))*2 + 1
                      
                   end if

                   do m = 1, NdsPer
                      TMpatch%LMP3D2D(m,lpe) = ElNde(m)
                   end do
                   
                end do ! ... ei
             end do ! ... ej
          end do ! ... ek
          
       elseif(input%ElemType == LINEAR) then
          do ek = Npes(3), Npee(3)
             do ej = Npes(2), Npee(2)
                do ei = Npes(1), Npee(1)
                   
                   lpe = (ek-Npes(3))*Npe(2)*Npe(1) + (ej-Npes(2))*Npe(1) + (ei-Npes(1)) + 1
                   
                   if(normDir == 1) then
                      
                      ElNde(1) = (ek-1)*Np(2) + (ej-1) + 1
                      ElNde(2) = (ek-1)*Np(2) + (ej  ) + 1

                      ! ... only have 2 nodes in surface element if 2D, onto the next element
                      if(ND == 2) then
                         do m = 1, NdsPer
                            TMpatch%LMP3D2D(m,lpe) = ElNde(m)
                         end do
                         cycle
                      end if
                      
                      ElNde(3) = (ek  )*Np(2) + (ej  ) + 1
                      ElNde(4) = (ek  )*Np(2) + (ej-1) + 1

                   elseif(normDir == 2) then
                      
                      ElNde(1) = (ek-1)*Np(1) + (ei-1) + 1
                      ElNde(2) = (ek-1)*Np(1) + (ei  ) + 1

                      ! ... only have 2 nodes in surface element if 2D, onto the next element
                      if(ND == 2) then
                         do m = 1, NdsPer
                            TMpatch%LMP3D2D(m,lpe) = ElNde(m)
                         end do
                         cycle
                      end if
                      
                      ElNde(3) = (ek  )*Np(1) + (ei  ) + 1
                      ElNde(4) = (ek  )*Np(1) + (ei-1) + 1

                   elseif(normDir == 3) then
                      
                      ElNde(1) = (ej-1)*Np(1) + (ei-1) + 1
                      ElNde(2) = (ej-1)*Np(1) + (ei  ) + 1
                      ElNde(3) = (ej  )*Np(1) + (ei  ) + 1
                      ElNde(4) = (ej  )*Np(1) + (ei-1) + 1
                      
                   end if
          
                   do m = 1, NdsPer
                      TMpatch%LMP3D2D(m,lpe) = ElNde(m)
                   end do

                end do ! ... ei
             end do ! ... ej
          end do ! ... ek
          
       end if ! ... ElemType == QUADRATIC
       
    end do ! ... npatch

  end subroutine LM2DtoPatchP3D

  subroutine P3DElementTable(region)

    USE ModGlobal
    USE ModDataStruct

    Implicit None

    Type(t_region), pointer :: region

    ! ... local variables
    Type(t_sgrid), pointer :: TMgrid
    Type(t_mixt_input), pointer :: input
    Integer :: ND, N(MAX_ND), Ne(MAX_ND)
    Integer :: nTMGrids, NdsPer, nPtsLoc
    Integer :: ng, i, j, k, dir, le, koffset, m
    Integer, allocatable :: ElNde(:)
    Integer, pointer :: LM(:,:)

    ! ... when using quadratic elements, need map from Plot3D nodes to element nodes

    ! ... simplicity
    ND = region%input%ND
    nTMGrids = region%nTMGrids
    N(:) = 1
    Ne(:) = 1

    ! ... loop through grids
    do ng = 1, nTMGrids

       ! ... simplicity
       TMgrid  => region%TMgrid(ng)
       input   => TMgrid%input
       LM      => TMgrid%LM
       nPtsLoc =  TMgrid%nPtsLoc

       ! ... number of plot3D points on grid (not neccessarily number of nodes)
       do dir = 1, ND
          N(dir) = TMgrid%ie(dir) - TMgrid%is(dir) + 1
       end do

       ! ... number of elements
       if(input%ElemType == LINEAR) then
          do dir = 1, ND
             Ne(dir) = (N(dir) - 1)
          end do
       elseif(input%ElemType == QUADRATIC) then
          do dir = 1, ND
             Ne(dir) = (N(dir) - 1)/2;
          end do
       end if

       ! ... number of nodes per element
       if(input%ElemType == LINEAR) then
          NdsPer = 4 + (ND-2)*4
       elseif(input%ElemType == QUADRATIC) then
          NdsPer = 9 + (ND-2)*18
       end if

       ! ... allocate
       allocate(ElNde(NdsPer))
       allocate(TMgrid%P3DLM(NdsPer,product(Ne)))

       ! ... initialize
       TMgrid%P3DLM(:,:) = 0
       ElNde(:) = 0
       if(input%ElemType == QUADRATIC) then
          do k = 1, Ne(3)
             do j = 1, Ne(2)
                do i = 1, Ne(1)
                   ! ... element index 
                   le = (k-1)*Ne(2)*Ne(1) + (j-1)*Ne(1) + (i-1) + 1

                   ! ... element local k = 0 plane
                   koffset = (k-1)*N(1)*N(2)*2

                   ! ... find local node index for this element
                   ElNde(1) = koffset + (j-1)*N(1)*2 + (i-1)*2 + 1
                   ElNde(2) = koffset + (j-1)*N(1)*2 + (i-1)*2 + 2
                   ElNde(3) = koffset + (j-1)*N(1)*2 + (i  )*2 + 1
                   ElNde(4) = koffset + (j-1)*N(1)*2 + (i-1)*2 + 1 + N(1)
                   ElNde(5) = koffset + (j-1)*N(1)*2 + (i-1)*2 + 2 + N(1)
                   ElNde(6) = koffset + (j-1)*N(1)*2 + (i  )*2 + 1 + N(1)
                   ElNde(7) = koffset + (j  )*N(1)*2 + (i-1)*2 + 1
                   ElNde(8) = koffset + (j  )*N(1)*2 + (i-1)*2 + 2
                   ElNde(9) = koffset + (j  )*N(1)*2 + (i  )*2 + 1

                   ! ... only have 8 nodes in element if 2D, onto the next element
                   if(ND == 2) then
                      do m = 1, NdsPer
                         TMgrid%P3DLM(m,le) = ElNde(m)
                      end do
                      cycle
                   end if

                   ! ... moving in the third dimension
                   ! ... element local k = 1/2 plane
                   koffset = (k-1)*N(1)*N(2)*2 + N(1)*N(2)

                   ElNde(10) = koffset + (j-1)*N(1)*2 + (i-1)*2 + 1
                   ElNde(11) = koffset + (j-1)*N(1)*2 + (i-1)*2 + 2
                   ElNde(12) = koffset + (j-1)*N(1)*2 + (i  )*2 + 1
                   ElNde(13) = koffset + (j-1)*N(1)*2 + (i-1)*2 + 1 + N(1)
                   ElNde(14) = koffset + (j-1)*N(1)*2 + (i-1)*2 + 2 + N(1)
                   ElNde(15) = koffset + (j-1)*N(1)*2 + (i  )*2 + 1 + N(1)
                   ElNde(16) = koffset + (j  )*N(1)*2 + (i-1)*2 + 1
                   ElNde(17) = koffset + (j  )*N(1)*2 + (i-1)*2 + 2
                   ElNde(18) = koffset + (j  )*N(1)*2 + (i  )*2 + 1

                   ! ... element local k = 1 plane
                   koffset = (k)*N(1)*N(2)*2

                   ElNde(19) = koffset + (j-1)*N(1)*2 + (i-1)*2 + 1
                   ElNde(20) = koffset + (j-1)*N(1)*2 + (i-1)*2 + 2
                   ElNde(21) = koffset + (j-1)*N(1)*2 + (i  )*2 + 1
                   ElNde(22) = koffset + (j-1)*N(1)*2 + (i-1)*2 + 1 + N(1)
                   ElNde(23) = koffset + (j-1)*N(1)*2 + (i-1)*2 + 2 + N(1)
                   ElNde(24) = koffset + (j-1)*N(1)*2 + (i  )*2 + 1 + N(1)
                   ElNde(25) = koffset + (j  )*N(1)*2 + (i-1)*2 + 1
                   ElNde(26) = koffset + (j  )*N(1)*2 + (i-1)*2 + 2
                   ElNde(27) = koffset + (j  )*N(1)*2 + (i  )*2 + 1

                   do m = 1, NdsPer
                      TMgrid%P3DLM(m,le) = ElNde(m)
                   end do

                end do
             end do
          end do
       elseif(input%ElemType == LINEAR) then
          do k = 1, Ne(3)
             do j = 1, Ne(2)
                do i = 1, Ne(1)
                   ! ... element index 
                   le = (k-1)*Ne(2)*Ne(1) + (j-1)*Ne(1) + (i-1) + 1

                   do m = 1, NdsPer
                      TMgrid%P3DLM(m,le) = TMgrid%LM(m,le) 
                   end do

                end do
             end do
          end do
       end if
    end do

  end subroutine P3DElementTable

  subroutine TMwrite_plot3d_file(region, var_to_write, iter_in)

    USE ModGlobal
    USE ModDataStruct
    USE ModMPI
    USE ModPLOT3D_IO

    Implicit None

    type(t_region), pointer :: region
    character(len=3) :: var_to_write
    integer, optional :: iter_in

    ! ... local variables
    type(t_sgrid), pointer :: TMgrid
    type(t_smixt), pointer :: TMstate
    real(rfreal) :: tau(4)
    integer :: i, ng, numGridsInFile, NDIM, iter, j
    character(len=80) :: fname
    integer :: funit = 50, fnamelen

    ! ... MPI
    integer :: TMRank, TMComm, ierr
    
    ! ... simplicity
    TMComm = region%TMComm
    TMRank = region%TMRank

    if (present(iter_in) .eqv. .true.) then
      iter = iter_in
    else
      iter = region%global%main_ts_loop_index
    end if

!     if (region%input%useLowMemIO == TRUE) then
!       call write_plot3d_file_low_mem(region, var_to_write, iter)
!       return
!     end if

!     if (region%input%useLowMemIO == PROCESSOR_IO) then
!       call write_plot3d_file_low_mem_per_processor(region, var_to_write, iter)
!       return
!     end if

    ! ... step 1
    ! ... set the filename
    write (fname(1:21),'(A13,I8.8)') 'RocFlo-CM.TM.', iter
    if ( var_to_write(1:3) == 'TMq' ) then
      write(fname(22:23),'(A2)') '.q'
      fnamelen = 23
    else if ( var_to_write(1:3) == 'dTM' ) then
       write(fname(22:24),'(A3)') '.dq'
       fnamelen = 24
    else if ( var_to_write(1:3) == 'xyz' ) then
      write(fname(22:25),'(A4)') '.xyz'
      fnamelen = 25
    end if

    if (TMRank == 0) Write (*,'(A)') 'PlasComCM: Writing "'//fname(1:fnamelen)//'".'

    ! ... fill in array for I/O
    call FillP3D(region)
    ! ... need rates too if this is a structurally dynamic simulation
    if(region%input%TMSolve > 1 .and. region%input%StructuralTimeScheme == DYNAMIC_SOLN) call FillP3DRate(region)
     
    ! ... step 2
    ! ... agglomerate decomposed data to core Rank for that grid
    Call Agglomerate_TMData_To_Core_Rank(region, var_to_write)
    Call MPI_BARRIER(TMComm, IERR)

    ! ... step 3
    ! ... write agglomerated data
    Loop1: Do i = 1, region%TMgrid(1)%numGridsInFile

      Loop2: Do ng = 1, region%nTMGrids

        TMgrid => region%TMgrid(ng)
        TMstate => region%TMstate(ng)

        if ( (TMgrid%iGridGlobal == i) .and. (TMRank == TMgrid%coreRank_inComm) ) then

           tau(1) = real(region%global%main_ts_loop_index,rfreal)
           tau(2) = 0.0
           tau(3) = 0.0
          if(region%input%TMOnly == TRUE) then
             tau(4) = region%global%main_ts_loop_index*region%input%dt
          else
             tau(4) = region%state(1)%time(1)
          end if
          NDIM   = 3

          ! ... if first grid, write header
          if ( i == 1 ) then
             if ( var_to_write(1:3) == 'dTM') then
                
                Call Write_Func_Header(NDIM, TMgrid%numGridsInFile, TMgrid%AllGridsGlobalSize, size(TMstate%DBUF_IO,4), fname, fnamelen, funit)

             else
                
                Call Write_Grid_Soln_Header(NDIM, TMgrid%numGridsInFile, TMgrid%AllGridsGlobalSize, fname, fnamelen, funit)
                
             end if

          end if

          ! ... write grids
          if ( var_to_write(1:3) == 'TMq' ) Call Write_Soln_Single_Grid(TMgrid%ND, TMgrid%GlobalSize, TMstate%DBUF_IO, tau, fname, fnamelen, funit)

          if ( var_to_write(1:3) == 'dTM' ) Call Write_Func_Single_Grid(TMgrid%ND, TMgrid%GlobalSize, TMstate%DBUF_IO, fname, fnamelen, funit)

          if ( var_to_write(1:3) == 'xyz' ) Call Write_Grid_Single_Grid(TMgrid%ND, TMgrid%GlobalSize, TMstate%DBUF_IO, TMstate%IBUF_IO, fname, fnamelen, funit)

          ! ... clean-up
          deallocate(TMstate%DBUF_IO)
          if ( var_to_write(1:3) == 'xyz' ) deallocate(TMstate%IBUF_IO)

          EXIT Loop2

        end if

      End Do Loop2

      Call MPI_BARRIER(TMComm, ierr)

    End Do Loop1

    Call MPI_BARRIER(TMComm, ierr)

    if (TMRank == 0) Write (*,'(A)') 'PlasComCM: Done writing "'//fname(1:fnamelen)//'".'

  end subroutine TMwrite_plot3d_file

  Subroutine Agglomerate_TMData_To_Core_Rank(region, var_to_write)

    USE ModGlobal
    USE ModDataStruct
    USE ModMPI

    Implicit None

    ! ... function arguments
    type(t_region), pointer :: region
    character(len=3) :: var_to_write

    ! ... local variables
    real(rfreal), pointer :: ptr_to_data(:,:), buf2(:,:)
    type(t_sgrid), pointer :: TMgrid
    type(t_smixt), pointer :: TMstate
    integer :: ng, i, j, k, p, l0, ii, jj, kk, na, nt, TMRank
    integer, pointer :: ND(:,:), buf(:,:), remote_is(:,:), remote_ie(:,:)
    integer :: status(MPI_STATUS_SIZE), ierr

    Do ng = 1, region%nGrids

      TMgrid => region%TMgrid(ng)
      TMstate => region%TMstate(ng)
      TMRank = region%TMRank

      ! ... nullify
      nullify(ptr_to_data)

      ! ... point pointer
      If ( var_to_write(1:3) == 'TMq' ) Then
        ptr_to_data => TMstate%P3D
      Else If ( var_to_write(1:3) == 'dTM' ) Then
         ptr_to_data => TMstate%P3Ddot
      Else If ( var_to_write(1:3) == 'xyz' ) Then
        ptr_to_data => TMgrid%XYZ
      Else
        Write(*,'(A,A)') 'PlasComCM: ERROR: Unknown value of var_to_write: ', var_to_write
        Stop 'Stopping in Agglomerate_Data_To_Core_Rank'
      End If

      ! ... coreRank must allocate buffer space
      if ( TMRank == TMgrid%coreRank_inComm ) Then
        allocate(TMstate%DBUF_IO(TMgrid%GlobalSize(1),TMgrid%GlobalSize(2),TMgrid%GlobalSize(3),size(ptr_to_data,2)))
        if ( var_to_write(1:3) == 'xyz' ) allocate(TMstate%IBUF_IO(TMgrid%GlobalSize(1),TMgrid%GlobalSize(2),TMgrid%GlobalSize(3)))
      end if

      ! ... have all core processors fill the buf with their own data
      if ( TMRank == TMgrid%coreRank_inComm ) Then

        ! ... field data
        do k = TMgrid%is(3), TMgrid%ie(3)
          do j = TMgrid%is(2), TMgrid%ie(2)
            do i = TMgrid%is(1), TMgrid%ie(1)
              l0 = (k-TMgrid%is(3))*(TMgrid%ie(2)-TMgrid%is(2)+1)&
                   *(TMgrid%ie(1)-TMgrid%is(1)+1) + &
                   (j-TMgrid%is(2))*(TMgrid%ie(1)-TMgrid%is(1)+1) + &
                   (i-TMgrid%is(1)+1)
              TMstate%DBUF_IO(i,j,k,:) = ptr_to_data(l0,:)
            end do
          end do
        end do

        ! ... iblank data
        if ( var_to_write(1:3) == 'xyz' ) then

          do k = TMgrid%is(3), TMgrid%ie(3)
            do j = TMgrid%is(2), TMgrid%ie(2)
              do i = TMgrid%is(1), TMgrid%ie(1)
                l0 = (k-TMgrid%is(3))*(TMgrid%ie(2)-TMgrid%is(2)+1)&
                     *(TMgrid%ie(1)-TMgrid%is(1)+1) + &
                     (j-TMgrid%is(2))*(TMgrid%ie(1)-TMgrid%is(1)+1) + &
                     (i-TMgrid%is(1)+1)
                TMstate%IBUF_IO(i,j,k) = TMgrid%IBLANK(l0)
              end do
            end do
          end do

        end if

      end if


      ! ... have all non-core processors send their data to the core
      ! ... first have the core processor figure everybody's size
      If (region%myrank == TMgrid%coreRank_inComm) Then

        allocate(remote_is(MAX_ND,0:TMgrid%numproc_inComm-1)); remote_is(:,:) = 1
        allocate(remote_ie(MAX_ND,0:TMgrid%numproc_inComm-1)); remote_ie(:,:) = 1
        allocate(buf(TMgrid%ND,2))

        remote_is(1:TMgrid%ND,0) = TMgrid%is(1:TMgrid%ND)
        remote_ie(1:TMgrid%ND,0) = TMgrid%ie(1:TMgrid%ND)

        do p = 1, TMgrid%numproc_inComm-1
          Call MPI_RECV(buf, 2*TMgrid%ND, MPI_INTEGER, p, p, &
               TMgrid%comm, status, ierr)
          remote_is(1:TMgrid%ND,p) = buf(1:TMgrid%ND,1)
          remote_ie(1:TMgrid%ND,p) = buf(1:TMgrid%ND,2)
        end do

      
        deallocate(buf)

      Else

        allocate(buf(TMgrid%ND,2));
        do p = 1, TMgrid%ND
          buf(p,1) = TMgrid%is(p)
          buf(p,2) = TMgrid%ie(p)
        end do
        call MPI_Send(buf, 2*TMgrid%ND, MPI_INTEGER, 0, TMgrid%myrank_inComm, &
             TMgrid%comm, ierr)
        deallocate(buf)

      End If


      ! ... now send and receive the data
      If (region%myrank == TMgrid%coreRank_inComm) Then

        allocate(ND(MAX_ND,TMgrid%numproc_inComm)); ND(:,:) = 1;

        ! ... get the data from the other processors
        do p = 1, TMgrid%numproc_inComm-1

          ND(1:TMgrid%ND,p) = remote_ie(1:TMgrid%ND,p) - remote_is(1:TMgrid%ND,p) + 1

          allocate(buf2(ND(1,p)*ND(2,p)*ND(3,p),size(ptr_to_data,2)))

          Call MPI_Recv(buf2, PRODUCT(ND(:,p))*(size(ptr_to_data,2)), MPI_DOUBLE_PRECISION, &
               p, p, TMgrid%comm, status, ierr)

          do k = remote_is(3,p), remote_ie(3,p)
            do j = remote_is(2,p), remote_ie(2,p)
              do i = remote_is(1,p), remote_ie(1,p)

                l0 = (k-remote_is(3,p))*ND(1,p)*ND(2,p) + (j-remote_is(2,p))*ND(1,p) + (i-remote_is(1,p)+1)
                TMstate%DBUF_IO(i,j,k,:) = buf2(l0,:)

              end do
            end do
          end do

          deallocate(buf2)

        end do

        deallocate(ND)

      Else

        Call MPI_Send(ptr_to_data, size(ptr_to_data,1)*size(ptr_to_data,2), &
             MPI_DOUBLE_PRECISION, 0, TMgrid%myrank_inComm, TMgrid%comm, ierr)

      End If


      ! ... repeat the process for iblank data
      if ( var_to_write(1:3) == 'xyz' ) then

        If (region%myrank == TMgrid%coreRank_inComm) Then

          ! ... get the data from the other processors
          do p = 1, TMgrid%numproc_inComm-1

            allocate(buf(ND(1,p)*ND(2,p)*ND(3,p),1))

            Call MPI_Recv(buf, PRODUCT(ND(:,p)), MPI_INTEGER, &
                 p, p, TMgrid%comm, status, ierr)

            do k = remote_is(3,p), remote_ie(3,p)
              do j = remote_is(2,p), remote_ie(2,p)
                do i = remote_is(1,p), remote_ie(1,p)

                  l0 = (k-remote_is(3,p))*ND(1,p)*ND(2,p) + (j-remote_is(2,p))*ND(1,p) + (i-remote_is(1,p)+1)
                  TMstate%IBUF_IO(i,j,k) = buf(l0,1)

                end do
              end do
            end do

            deallocate(buf)

          end do

          deallocate(ND, remote_is, remote_ie)

        Else

          Call MPI_Send(TMgrid%iblank, size(TMgrid%iblank,1), &
               MPI_INTEGER, 0, TMgrid%myrank_inComm, TMgrid%comm, ierr)

        End If

      end if

    End Do

  End Subroutine Agglomerate_TMData_To_Core_Rank

  Subroutine FillP3D(region)
    
    USE ModGlobal
    USE ModDataStruct
    USE ModTMInt
    
    implicit none
    
    Type(t_region), pointer :: region

    ! ... local variables
    Type(t_mixt_input), pointer :: input
    Type(t_sgrid), pointer :: TMgrid
    Type(t_smixt), pointer :: TMstate
    Real(rfreal), allocatable :: ShpFcn(:), xi(:), eta(:), zeta(:)
    Integer :: ng, i, j, k, le, ei, ej, ek, n, g, var, ND, nVars, NdsPerElem, Ne(3), dir
    Integer, allocatable :: NodeNums(:)
    Logical, allocatable :: FillFlag(:)

    do ng = 1, region%nTMGrids
       ! ... simplicity
       TMgrid  => region%TMgrid(ng)
       TMstate => region%TMstate(ng)
       input   => TMgrid%input
       ND = TMgrid%ND
       nVars = TMstate%nVars
       
       Allocate(FillFlag(size(TMstate%P3D,1)))
       do i = 1, size(TMstate%P3D,1)
          FillFlag(i) = .false.
       end do

       ! ... put solution into plot3D array
       ! ... start with direct injection
       TMstate%P3D(:,:) = 0.0_rfreal
       do j = 1, size(TMstate%q,2)
          do i = 1, TMgrid%nPtsLoc
             TMstate%P3D(TMgrid%L2P3D(i),j) = TMstate%q(i,j)
             !print*,'from writing: i,j,TMstate%q(i,j)',i,j,TMstate%q(i,j)
             FillFlag(TMgrid%L2P3D(i)) = .true.
          end do
       end do

       if(input%ElemType == QUADRATIC) then
          if(ND == 2) then
             allocate(NodeNums(1),xi(1),eta(1))
             allocate(ShpFcn(8))
             NodeNums = 5
             xi = 0.0_rfreal
             eta = 0.0_rfreal 
             NdsPerElem = 8
          elseif(ND > 2) then
             allocate(NodeNums(7),xi(7),eta(7),zeta(7))
             allocate(ShpFcn(20))
             NodeNums = (/5, 11, 13, 14, 15, 17, 23/)
             xi = (/0.0_rfreal,  0.0_rfreal, -1.0_rfreal, 0.0_rfreal, 1.0_rfreal, 0.0_rfreal, 0.0_rfreal/) 
             eta = (/0.0_rfreal, -1.0_rfreal, 0.0_rfreal, 0.0_rfreal, 0.0_rfreal, 1.0_rfreal, 0.0_rfreal/) 
             zeta = (/-1.0_rfreal, 0.0_rfreal, 0.0_rfreal, 0.0_rfreal, 0.0_rfreal, 0.0_rfreal, 1.0_rfreal/) 
             NdsPerElem = 20
          end if

          do dir = 1, 3
             Ne(dir) = ((TMgrid%ie(dir)-TMgrid%is(dir) + 1)-1)/2
          end do
          if(ND==2) Ne(3) = 1
          
          do ek = 1, Ne(3)
             do ej = 1, Ne(2)
                do ei = 1, Ne(1)
                   le = (ek-1)*product(Ne(1:2)) + (ej-1)*Ne(1) + (ei-1) + 1
                   do n = 1, size(NodeNums,1)
                      i = TMgrid%P3DLM(NodeNums(n),le)
                      ! ... don't add to the same node point
                      if(FillFlag(i) .eqv. .true.) cycle
                      FillFlag(i) = .true.
                      if(ND == 2) call Quad8(ShpFcn,xi(n),eta(n))
                      if(ND > 2 ) call Hex20(ShpFcn,xi(n),eta(n),zeta(n))
                      do var = 1, nVars
                         do g = 1, NdsPerElem
                            TMstate%P3D(i,var) = TMstate%P3D(i,var) + TMstate%q(TMgrid%LM(g,le),var) * ShpFcn(g)
                         end do
                      end do
                   end do
                end do
             end do
          end do
       elseif(input%ElemType == LINEAR) then
          do var = 1, nVars
             do g = 1, TMgrid%nPtsLoc
                TMstate%P3D(g,var) = TMstate%q(g,var)
             end do
          end do   
       endif

       end do

  end Subroutine FillP3D

  Subroutine FillP3DRate(region)
    
    USE ModGlobal
    USE ModDataStruct
    USE ModTMInt
    
    implicit none
    
    Type(t_region), pointer :: region

    ! ... local variables
    Type(t_mixt_input), pointer :: input
    Type(t_sgrid), pointer :: TMgrid
    Type(t_smixt), pointer :: TMstate
    Real(rfreal), allocatable :: ShpFcn(:), xi(:), eta(:), zeta(:)
    Integer :: ng, i, j, k, le, ei, ej, ek, n, g, var, ND, nVars, NdsPerElem, Ne(3), dir
    Integer, allocatable :: NodeNums(:)
    Logical, allocatable :: FillFlag(:)

    do ng = 1, region%nTMGrids
       ! ... simplicity
       TMgrid  => region%TMgrid(ng)
       TMstate => region%TMstate(ng)
       input   => TMgrid%input
       ND = TMgrid%ND
       nVars = TMstate%nVars
       
       Allocate(FillFlag(size(TMstate%P3D,1)))
       do i = 1, size(TMstate%P3D,1)
          FillFlag(i) = .false.
       end do

       ! ... put solution into plot3D array
       ! ... start with direct injection
       TMstate%P3Ddot(:,:) = 0.0_rfreal
       do j = 1, size(TMstate%qdot,2)
          do i = 1, TMgrid%nPtsLoc
             TMstate%P3Ddot(TMgrid%L2P3D(i),j) = TMstate%qdot(i,j)
             !print*,'i,j,TMstate%qdot(i,j)',i,j,TMstate%qdot(i,j)
             FillFlag(TMgrid%L2P3D(i)) = .true.
          end do
       end do

       if(input%ElemType == QUADRATIC) then
          if(ND == 2) then
             allocate(NodeNums(1),xi(1),eta(1))
             allocate(ShpFcn(8))
             NodeNums = 5
             xi = 0.0_rfreal
             eta = 0.0_rfreal 
             NdsPerElem = 8
          elseif(ND > 2) then
             allocate(NodeNums(7),xi(7),eta(7),zeta(7))
             allocate(ShpFcn(20))
             NodeNums = (/5, 11, 13, 14, 15, 17, 23/)
             xi = (/0.0_rfreal,  0.0_rfreal, -1.0_rfreal, 0.0_rfreal, 1.0_rfreal, 0.0_rfreal, 0.0_rfreal/) 
             eta = (/0.0_rfreal, -1.0_rfreal, 0.0_rfreal, 0.0_rfreal, 0.0_rfreal, 1.0_rfreal, 0.0_rfreal/) 
             zeta = (/-1.0_rfreal, 0.0_rfreal, 0.0_rfreal, 0.0_rfreal, 0.0_rfreal, 0.0_rfreal, 1.0_rfreal/) 
             NdsPerElem = 20
          end if

          do dir = 1, 3
             Ne(dir) = ((TMgrid%ie(dir)-TMgrid%is(dir) + 1)-1)/2
          end do
          if(ND==2) Ne(3) = 1
          
          do ek = 1, Ne(3)
             do ej = 1, Ne(2)
                do ei = 1, Ne(1)
                   le = (ek-1)*product(Ne(1:2)) + (ej-1)*Ne(1) + (ei-1) + 1
                   do n = 1, size(NodeNums,1)
                      i = TMgrid%P3DLM(NodeNums(n),le)
                      ! ... don't add to the same node point
                      if(FillFlag(i) .eqv. .true.) cycle
                      FillFlag(i) = .true.
                      if(ND == 2) call Quad8(ShpFcn,xi(n),eta(n))
                      if(ND > 2 ) call Hex20(ShpFcn,xi(n),eta(n),zeta(n))
                      do var = 1, 3*ND
                         do g = 1, NdsPerElem
                            TMstate%P3Ddot(i,var) = TMstate%P3Ddot(i,var) + TMstate%qdot(TMgrid%LM(g,le),var) * ShpFcn(g)
                         end do
                      end do
                   end do
                end do
             end do
          end do
       elseif(input%ElemType == LINEAR) then
          do var = 1, 3*ND
             do g = 1, TMgrid%nPtsLoc
                TMstate%P3Ddot(g,var) = TMstate%qdot(g,var)
             end do
          end do   
       endif

       end do

  end Subroutine FillP3DRate

  Subroutine DataProbe(region)
    
    USE ModDataStruct
    USE ModGlobal
    
    Implicit None
    
    Type(t_region), pointer :: region

    ! ... local variables
    Character(len=21) :: fname
    Integer :: funit = 551
    
    if(region%global%main_ts_loop_index == 1) then
       open(unit=551,file='TMProbe.txt',status='unknown')
    else
       open(unit=551,file='TMProbe.txt',status='unknown', position='append')
    end if
    if(region%global%main_ts_loop_index == 1)     write(551,'(E13.6,X,E13.6)') 0.0_rfreal,region%TMstate(1)%q(11,2)
    write(551,'(E13.6,X,E13.6)') region%input%dt*(region%global%main_ts_loop_index),region%TMstate(1)%q(11,2)
    close(551)

  end Subroutine DataProbe


  Subroutine WriteSolidData(region)

    USE ModGlobal
    USE ModDataStruct
    USE ModMPI

    ! ... subroutine arguments
    Type (t_region), Pointer :: region

    ! ... local variables and arrays
    Integer :: funit, ng, TMrank
    Character(LEN=80) :: fname
    Type (t_sgrid), Pointer :: TMgrid
    Type(t_smixt), Pointer :: TMstate
    Type(t_mixt_input), Pointer :: input

    ! ... simplicity
    input => region%input
    funit = 60
    TMRank = region%TMrank

    fname(1:10) = 'SolidData.'
    Write(fname(11:22),'(I8.8,A4)') region%global%main_ts_loop_index, '.dat'

    ! ... only rank 0 needs to write
    If (TMrank == 0) Then
      Open(unit=funit, file=trim(fname), form='UNFORMATTED')

      do ng = 1, region%nTMGrids
         ! ... simplicity
         TMstate => region%TMstate(ng)

         Write(funit) ng
         Write(funit) TMstate%numDataSample
         Write(funit) (TMstate%SDTime(k),k=1,TMstate%numDataSample)
         Write(funit) (TMstate%PowerIn(k),k=1,TMstate%numDataSample)
         Write(funit) (TMstate%KinEnergy(k),k=1,TMstate%numDataSample)
         
      end do
      
      Close(unit=funit)

   end if

   do ng = 1, region%nTMGrids
      ! ... simplicity
      TMstate => region%TMstate(ng)
      ! ... reinitialize everything
      Deallocate(TMstate%PowerIn,TMstate%KinEnergy)
      Nullify(TMstate%PowerIn,TMstate%KinEnergy); Allocate(TMstate%PowerIn(TMstate%numDataSample),TMstate%KinEnergy(TMstate%numDataSample))

      TMstate%NumDataSample = 0
   End Do ! ng
   
   Call MPI_BARRIER(region%TMcomm, ierr)

  End Subroutine WriteSolidData

  Real(kind=8) Function MPI_WTIME_Hack()

    USE ModMPI
    
    Implicit None
    
    MPI_WTIME_Hack = MPI_WTIME()

  end Function MPI_WTIME_Hack
  
  End Module ModTMIO
