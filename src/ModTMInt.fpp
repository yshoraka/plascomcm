! Copyright (c) 2015, University of Illinois at Urbana-Champaign, XPACC
! License: MIT, http://opensource.org/licenses/MIT
!-----------------------------------------------------------------------
!
! ModTMInt.f90
!
! - routines for communication, integration related routines
!
! Revision history
! - 05 Mar 2012 : CMO - initial code
!
!
!-----------------------------------------------------------------------
Module ModTMInt

CONTAINS

  Subroutine Ghost_Cell_Exchange_TM(region, F, gridID)

    USE ModGlobal
    USE ModDataStruct
    USE ModMPI

    implicit none

    Type(t_region), pointer :: region
    Integer :: gridID
    Real(rfreal), pointer :: F(:)

    ! ... local variables
    Integer :: gOffset(2), lp, rp, req(4), dir, var, nVar, nPtsLoc
    Integer :: status_array(MPI_STATUS_SIZE,4),i, j, k, l0, l1
    Integer :: N(MAX_ND), Ne(MAX_ND)
    Type(t_sgrid), pointer :: TMgrid
    Real(rfreal), pointer :: sbuf(:), rbuf(:)
    Integer, pointer :: nExchng(:), ExchngInd(:,:,:)
    Integer :: leftTag, rightTag
    Integer :: TMRank, ierr

    ! ... simplicity
    TMgrid    => region%TMgrid(gridID)
    nExchng   => TMgrid%nExchng
    ExchngInd => TMgrid%ExchngInd
    nPtsLoc = TMgrid%nPtsLoc
    TMRank = region%TMRank

    do dir = 1, TMgrid%ND

       ! ... left and right processors
       if ( TMgrid%cartDims(dir) > 1 ) then
          lp = TMgrid%left_process(dir)
          rp = TMgrid%right_process(dir)


          ! ... allocate send and recv buffers
          allocate(sbuf(nExchng(dir)))
          allocate(rbuf(nExchng(dir)))
          sbuf = 0
          rbuf = 0

          ! ... fill the send buffer, loop through rightmost elements
          ! ... if there is a process to our right
          if(rp >= 0) then
             do i = 1, nExchng(dir)
                sbuf(i) = F(ExchngInd(i,2,dir))
             end do
          end if


          ! ... exchange from the left
          Call MPI_IRECV(rbuf,nExchng(dir), MPI_DOUBLE_PRECISION, &
               lp, 0, TMgrid%cartComm, req(1), ierr)
          Call MPI_ISEND(sbuf,nExchng(dir),MPI_DOUBLE_PRECISION, &
               rp, 0, TMgrid%cartComm, req(2), ierr)
          Call MPI_WaitAll(2, req, status_array, ierr)

          ! ... store the data from the receive buffer
          ! ... if there is a process to our left
          if(lp >= 0) then
             do i = 1, nExchng(dir)
                F(ExchngInd(i,1,dir)) = rbuf(i)
             end do
          end if

          ! ... deallocate
          deallocate(sbuf,rbuf)

       end if

    end do ! ... dir

  end Subroutine Ghost_Cell_Exchange_TM

  Subroutine SetUpExchange(region)

    USE ModGlobal
    USE ModDataStruct
    USE ModMPI

    implicit none

    Type(t_region), pointer :: region

    ! ... local variables
    Integer :: lp, rp, dir, nPtsLoc
    Integer :: i, j, k, counter, mm, m, le, ng
    Integer :: N(MAX_ND), Ne(MAX_ND)
    Integer, allocatable :: mvec(:), flag(:)
    Type(t_sgrid), pointer :: TMgrid
    Type(t_mixt_input), pointer :: input
    Integer, pointer :: nExchng(:), ExchngInd(:,:,:), LM(:,:)


    ! ... loop through the grids
    do ng = 1, region%nTMGrids

       ! ... simplicity
       TMgrid  => region%TMgrid(ng)
       input   => TMgrid%input
       LM      => TMgrid%LM

       N(:)  = 1
       do dir = 1, TMgrid%ND
          N(dir) = TMgrid%ie(dir)-TMgrid%is(dir)+1
       end do

       ! ... how many elements in each direction
       Ne(:) = 1
       if(input%ElemType == QUADRATIC) then
          do dir = 1, TMgrid%ND 
             Ne(dir) = (N(dir) - 1)/2
          end do
        nPtsLoc = ((N(3)-1)/2+1)*(N(1)*((N(2)-1)/2+1)+((N(1)-1)/2+1)*(N(2)-1)/2) + (N(3)-1)/2*((N(1)-1)/2+1)*((N(2)-1)/2+1)
        
        ! ... record number of local points and elements
        TMgrid%nPtsLoc = nPtsLoc
        TMgrid%nElLoc = product(Ne)

       elseif(input%ElemType == LINEAR) then
          do dir = 1, TMgrid%ND 
             Ne(dir) = (N(dir) - 1)
          end do

          nPtsLoc = N(3)*N(2)*N(1)
          
          ! ... record number of local points and elements
          TMgrid%nPtsLoc = nPtsLoc
          TMgrid%nElLoc = product(Ne)

       end if

       allocate(TMgrid%nExchng(TMgrid%ND))
       nExchng => TMgrid%nExchng
       nExchng(:) = 0
       ! ... get number to exchange in each direction
       if(input%ElemType == QUADRATIC) then
          dir = 1
          nExchng(dir) = (N(2)*((N(3)-1)/2+1)+((N(2)-1)/2+1)*((N(3)-1)/2))
          dir = 2
          nExchng(dir) = (N(1)*((N(3)-1)/2+1)+((N(1)-1)/2+1)*((N(3)-1)/2))
          if(TMgrid%ND > 2) then
             dir = 3
             nExchng(dir) = (N(2)*((N(1)-1)/2+1)+((N(2)-1)/2+1)*((N(1)-1)/2))
          end if
       elseif(input%ElemType == LINEAR) then
          dir = 1
          nExchng(dir) = N(2)*N(3)
          dir = 2
          nExchng(dir) = N(1)*N(3)
          if(TMgrid%ND > 2) then
             dir = 3
             nExchng(dir) = N(1)*N(2)
          end if
       end if

       ! ... array to hold indices for exchange in each direction
       allocate(TMgrid%ExchngInd(MAXVAL(nExchng),2,TMgrid%ND))
       ExchngInd => TMgrid%ExchngInd

       ! ... vector with element local node ids
       if(input%ElemType == QUADRATIC) then
          allocate(mvec(3+(TMgrid%ND-2)*5))
       elseif(input%ElemType == LINEAR) then
          allocate(mvec(2+(TMgrid%ND-2)*2))
       end if
       ! ... Note: mvec fill order like a 2-D element (or 1-D, TMgrid%ND = 2) 

       ! ... flag to keep track of which local nodes have been recorded
       allocate(flag(TMgrid%nPtsLoc))

       ! ... fill it
       dir = 1

       ! ... left and right processors
       if ( TMgrid%cartDims(dir) > 1 ) then
          lp = TMgrid%left_process(dir)
          rp = TMgrid%right_process(dir)
       else
          lp = region%myrank
          rp = region%myrank
       end if


       flag(:) = 0

       ! ... left side first
       if(input%ElemType == QUADRATIC) then
          if(TMgrid%ND > 2) then
             mvec = (/1, 5, 8, 4, 13, 20, 16, 12/)
          else
             mvec = (/1, 4, 8/)
          end if
       elseif(input%ElemType == LINEAR) then
          if(TMgrid%ND > 2) then
             mvec = (/1, 5, 8, 4/)
          else
             mvec = (/1, 4/)
          end if
       end if

       counter = 0
       ! ... indices for the receive buffer, loop through leftmost elements
       i = 1
       do j = 1, Ne(2)
          do k = 1, Ne(3)

             le = (k-1)*Ne(2)*Ne(1) + (j-1)*Ne(1) + (i-1) + 1;

             do  mm = 1, size(mvec)
                m = mvec(mm)
                if(flag(LM(m,le)) == 0) then
                   flag(LM(m,le)) = 1
                   counter = counter + 1
                   ExchngInd(counter,1,dir) = LM(m,le)
                end if
             end do
          end do
       end do



       flag(:) = 0

       ! ... right side
       if(input%ElemType == QUADRATIC) then
          if(TMgrid%ND > 2) then
             mvec = (/2, 6, 7, 3, 14, 18, 15, 10/)
          else
             mvec = (/2, 3, 6/)
          end if
       elseif(input%ElemType == LINEAR) then
          if(TMgrid%ND > 2) then
             mvec = (/2, 6, 7, 3/)
          else
             mvec = (/2, 3/)
          end if
       end if


       counter = 0
       ! ... indices for the send buffer, loop through rightmost elements
       i = Ne(1)
       do j = 1, Ne(2)
          do k = 1, Ne(3)
             le = (k-1)*Ne(2)*Ne(1) + (j-1)*Ne(1) + (i-1) + 1;
             do  mm = 1, size(mvec)
                m = mvec(mm)
                if(flag(LM(m,le)) == 0) then
                   flag(LM(m,le)) = 1
                   counter = counter + 1
                   ExchngInd(counter,2,dir) = LM(m,le)
                end if
             end do
          end do
       end do


       ! ... now second direction
       dir = 2

       ! ... left and right processors
       if ( TMgrid%cartDims(dir) > 1 ) then
          lp = TMgrid%left_process(dir)
          rp = TMgrid%right_process(dir)
       else
          lp = region%myrank
          rp = region%myrank
       end if

       flag(:) = 0

       ! ... left side first
       if(input%ElemType == QUADRATIC) then
          if(TMgrid%ND > 2) then
             mvec = (/2, 1, 5, 6, 9, 13, 17, 14/)
          else
             mvec = (/2, 1, 5/)
          end if
       elseif(input%ElemType == LINEAR) then
          if(TMgrid%ND > 2) then
             mvec = (/2, 1, 5, 6/)
          else
             mvec = (/2, 1/)
          end if
       end if

       counter = 0
       ! ... indices for the receive buffer, loop through leftmost elements
       do i = 1, Ne(1)
          j = 1
          do k = 1, Ne(3)

             le = (k-1)*Ne(2)*Ne(1) + (j-1)*Ne(1) + (i-1) + 1;

             do  mm = 1, size(mvec)
                m = mvec(mm)
                if(flag(LM(m,le)) == 0) then
                   flag(LM(m,le)) = 1
                   counter = counter + 1
                   ExchngInd(counter,1,dir) = LM(m,le)
                end if
             end do
          end do
       end do


       flag(:) = 0

       ! ... right side
       if(input%ElemType == QUADRATIC) then
          if(TMgrid%ND > 2) then
             mvec = (/3, 4, 8, 7, 11, 16, 19, 15/)
          else
             mvec = (/3, 4, 7/)
          end if
       elseif(input%ElemType == LINEAR) then
          if(TMgrid%ND > 2) then
             mvec = (/3, 4, 8, 7/)
          else
             mvec = (/3, 4/)
          end if
       end if


       counter = 0
       ! ... indices for the send buffer, loop through rightmost elements
       do i = 1, Ne(1)
          j = Ne(2)
          do k = 1, Ne(3)

             le = (k-1)*Ne(2)*Ne(1) + (j-1)*Ne(1) + (i-1) + 1;

             do  mm = 1, size(mvec)
                m = mvec(mm)
                if(flag(LM(m,le)) == 0) then
                   flag(LM(m,le)) = 1
                   counter = counter + 1
                   ExchngInd(counter,2,dir) = LM(m,le)
                end if
             end do
          end do
       end do


       ! ... now third direction, if 3D
       if(TMgrid%ND > 2) then
          dir = 3

          ! ... left and right processors
          if ( TMgrid%cartDims(dir) > 1 ) then
             lp = TMgrid%left_process(dir)
             rp = TMgrid%right_process(dir)
          else
             lp = region%myrank
             rp = region%myrank
          end if

          flag(:) = 0

          ! ... left side first
          if(input%ElemType == QUADRATIC) then
             mvec = (/2, 1, 4, 3, 9, 12, 11, 10/)
          elseif(input%ElemType == LINEAR) then
             mvec = (/2, 1, 4, 3/)
          end if

          counter = 0
          ! ... indices for the receive buffer, loop through leftmost elements
          do i = 1, Ne(1)
             do j = 1, Ne(2)
                k = 1

                le = (k-1)*Ne(2)*Ne(1) + (j-1)*Ne(1) + (i-1) + 1;

                do mm = 1, size(mvec)
                   m = mvec(mm)
                   if(flag(LM(m,le)) == 0) then
                      flag(LM(m,le)) = 1
                      counter = counter + 1
                      ExchngInd(counter,1,dir) = LM(m,le)
                   end if
                end do
             end do
          end do


          flag(:) = 0

          ! ... right side
          if(input%ElemType == QUADRATIC) then
             mvec = (/6, 5, 8, 7, 17, 20, 19, 18/)
          elseif(input%ElemType == LINEAR) then
             mvec = (/6, 5, 8, 7/)
          end if


          counter = 0
          ! ... indices for the send buffer, loop through rightmost elements
          do i = 1, Ne(1)
             do j = 1, Ne(2)
                k = Ne(3)

                le = (k-1)*Ne(2)*Ne(1) + (j-1)*Ne(1) + (i-1) + 1;

                do mm = 1, size(mvec)
                   m = mvec(mm)
                   if(flag(LM(m,le)) == 0) then
                      flag(LM(m,le)) = 1
                      counter = counter + 1
                      ExchngInd(counter,2,dir) = LM(m,le)
                   end if
                end do
             end do
          end do

       end if

    end do ! ... ng
  end Subroutine SetUpExchange

  Subroutine GetShpFcn(region)

    USE ModGlobal
    USE ModDataStruct
    USE ModMPI
    
    implicit none

    Type(t_region), pointer :: region

    ! ... local variables
    Type(t_mixt_input), pointer :: input
    Type(t_sgrid), pointer :: TMgrid
    Real(rfreal), pointer :: ShpFcn(:,:), Weight(:), W(:)
    Real(rfreal), allocatable :: xi(:), ShpFcnTmp(:)
    Integer :: i, j, k, ng, gp, gi, gj, gk, nGauss(3), lg, NdsPerElem

    
    do ng = 1, region%nTMGrids
       
       ! ... simplicity
       TMgrid => region%TMgrid(ng)
       input  => TMgrid%input
       
       if(input%ElemType == QUADRATIC) then
          ! ... 3rd order Gauss quadrature
          allocate(xi(3))
          xi(1:3) = (/-sqrt(0.6_rfreal), 0.0_rfreal, sqrt(0.6_rfreal)/) 
          allocate(W(3))
          W(:) = (/5.0_rfreal/9.0_rfreal, 8.0_rfreal/9.0_rfreal, 5.0_rfreal/9.0_rfreal/)
          !if(region%TMRank == 0) write(*,'(A)') 'PlasComCM: Using QUADRATIC elements'
          if(TMgrid%ND == 2) then
             nGauss(:) = (/3, 3, 1/)
             NdsPerElem = 8
          elseif(TMgrid%ND == 3) then
             nGauss(:) = (/3, 3, 3/)
             NdsPerElem = 20
          end if
       elseif(input%ElemType == LINEAR) then
          ! ... 2nd order Gauss quadrature
          allocate(xi(2))
          xi(1:2) = (/-1.0_rfreal/sqrt(3.0_rfreal), 1.0_rfreal/sqrt(3.0_rfreal)/)
          allocate(W(2))
          W(:) = (/1.0_rfreal, 1.0_rfreal/)
          !if(region%TMRank == 0) write(*,'(A)') 'PlasComCM: Using LINEAR elements'
          if(TMgrid%ND == 2) then
             nGauss(:) = (/2, 2, 1/)
             NdsPerElem = 4
          elseif(TMgrid%ND == 3) then
             nGauss(:) = (/2, 2, 2/)
             NdsPerElem = 8
          end if
       end if

       allocate(TMgrid%ShpFcn(NdsPerElem,PRODUCT(nGauss)))
       allocate(ShpFcnTmp(NdsPerElem))
       allocate(TMgrid%Weight(PRODUCT(nGauss)))

       ! ... simplicity
       Weight => TMgrid%Weight
       ShpFcn => TMgrid%ShpFcn

       ! ... find the values of the shape functions at each Gauss point
       ! ... don't want to loop over gk for 2D
       do gk = 1, nGauss(3)
          do gj = 1, nGauss(2)
             do gi = 1, nGauss(1)
                lg = (gk-1)*PRODUCT(nGauss(1:2)) + (gj-1)*nGauss(1) + (gi-1) + 1
                
                if(input%ElemType == QUADRATIC) then
                   if(TMgrid%ND == 2) then
                      call Quad8(ShpFcnTmp,xi(gi),xi(gj))
                      ! ... fill Gauss point weights
                      Weight(lg) = W(gi)*W(gj)
                   end if
                   if(TMgrid%ND == 3) then
                      call Hex20(ShpFcnTmp,xi(gi),xi(gj),xi(gk))
                      ! ... fill Gauss point weights
                      Weight(lg) = W(gi)*W(gj)*W(gk)
                   end if
                elseif(input%ElemType == LINEAR) then
                   if(TMgrid%ND == 2) then
                      call Quad4(ShpFcnTmp,xi(gi),xi(gj))
                      ! ... fill Gauss point weights
                      Weight(lg) = W(gi)*W(gj)
                   end if
                   if(TMgrid%ND == 3) then
                      call Hex8(ShpFcnTmp,xi(gi),xi(gj),xi(gk))
                      Weight(lg) = W(gi)*W(gj)*W(gk)
                   end if
                end if
                
                do i = 1, NdsPerElem
                   ShpFcn(i,lg) = ShpFcnTmp(i)
                end do
                
             end do
          end do
       end do
    end do

  end Subroutine GetShpFcn

  Subroutine GetdShpFcn(region)

    USE ModGlobal
    USE ModDataStruct
    USE ModMPI
    
    implicit none

    Type(t_region), pointer :: region

    ! ... local variables
    Type(t_mixt_input), pointer :: input
    Type(t_sgrid), pointer :: TMgrid
    Real(rfreal), pointer :: dShpFcn(:,:,:)
    Real(rfreal), allocatable :: xi(:), dShpFcnTmp(:,:)
    Integer :: i, j, k, ng, gp, gi, gj, gk, nGauss(3), lg, NdsPerElem
    
    do ng = 1, region%nTMGrids
       
       ! ... simplicity
       TMgrid => region%TMgrid(ng)
       input  => TMgrid%input
       
       if(input%ElemType == QUADRATIC) then
          ! ... 3rd order Gauss quadrature
          allocate(xi(3))
          xi(1:3) = (/-sqrt(0.6_rfreal), 0.0_rfreal, sqrt(0.6_rfreal)/) 
          nGauss = 3
          if(TMgrid%ND == 2) then
             nGauss(:) = (/3, 3, 1/)
             NdsPerElem = 8
          elseif(TMgrid%ND == 3) then
             nGauss(:) = (/3, 3, 3/)
             NdsPerElem = 20
          end if
       elseif(input%ElemType == LINEAR) then
          ! ... 2nd order Gauss quadrature
          allocate(xi(2))
          xi(1:2) = (/-1.0_rfreal/sqrt(3.0_rfreal), 1.0_rfreal/sqrt(3.0_rfreal)/)
          if(TMgrid%ND == 2) then
             nGauss(:) = (/2, 2, 1/)
             NdsPerElem = 4
          elseif(TMgrid%ND == 3) then
             nGauss(:) = (/2, 2, 2/)
             NdsPerElem = 8
          end if
       end if

       allocate(TMgrid%dShpFcn(NdsPerElem,TMgrid%ND,PRODUCT(nGauss)))
       allocate(dShpFcnTmp(NdsPerElem,TMgrid%ND))


       dShpFcn => TMgrid%dShpFcn


       ! ... find the values of the shape functions at each Gauss point
       ! ... don't want to loop over gk for 2D
       do gk = 1, nGauss(3)
          do gj = 1, nGauss(2)
             do gi = 1, nGauss(1)
                lg = (gk-1)*PRODUCT(nGauss(1:2)) + (gj-1)*nGauss(1) + (gi-1) + 1
                
                if(input%ElemType == QUADRATIC) then
                   if(TMgrid%ND == 2) call dQuad8(dShpFcnTmp,xi(gi),xi(gj))
                   if(TMgrid%ND == 3) call dHex20(dShpFcnTmp,xi(gi),xi(gj),xi(gk))
                elseif(input%ElemType == LINEAR) then
                   if(TMgrid%ND == 2) call dQuad4(dShpFcnTmp,xi(gi),xi(gj))
                   if(TMgrid%ND == 3) call dHex8(dShpFcnTmp,xi(gi),xi(gj),xi(gk))
                end if
                
                do j = 1, TMgrid%ND
                   do i = 1, NdsPerElem
                      dShpFcn(i,j,lg) = dShpFcnTmp(i,j)
                   end do
                end do

             end do
          end do
       end do
    end do
  end Subroutine GetdShpFcn


  Subroutine GetBndShpFcn(region)

    USE ModGlobal
    USE ModDataStruct
    USE ModMPI
    
    implicit none

    Type(t_region), pointer :: region

    ! ... local variables
    Type(t_mixt_input), pointer :: input
    Type(t_sgrid), pointer :: TMgrid
    Type(t_spatch), pointer :: TMpatch
    Real(rfreal), pointer :: VolShpFcn(:,:),BndShpFcn(:,:), Weight(:), W(:)
    Real(rfreal), allocatable :: xi(:), VolShpFcnTmp(:),BndShpFcnTmp(:)
    Integer :: i, j, k, ng, gp, gi, gj, gk, nGauss(3), lg, NdsPerElem, npatch, NdsPerFace
    Integer :: normDir, sgn, GridID, ND
    

     
    do npatch = 1, region%nTMPatches
       
       ! ... simplicity
       TMpatch => region%TMpatch(npatch)
       GridID  =  TMpatch%GridID
       TMgrid  => region%TMgrid(GridID)
       input   => TMgrid%input
       ND      =  TMgrid%ND

       normDir = abs(TMpatch%normDir)
       sgn = normDir / TMpatch%normDir
       
       ! ... record tangent directions
       j = 0
       allocate(TMpatch%TanDir(ND))
       do i = 1, ND
          if(i == normDir) cycle
          j = j+1
          TMpatch%TanDir(j) = i
       end do

       nGauss(:) = 1
       if(input%ElemType == QUADRATIC) then
          ! ... 3rd order Gauss quadrature
          allocate(xi(3))
          xi(1:3) = (/-sqrt(0.6_rfreal), 0.0_rfreal, sqrt(0.6_rfreal)/) 
          allocate(W(3))
          W(:) = (/5.0_rfreal/9.0_rfreal, 8.0_rfreal/9.0_rfreal, 5.0_rfreal/9.0_rfreal/)
          !if(region%TMRank == 0) write(*,'(A)') 'PlasComCM: Using QUADRATIC elements'
          if(TMgrid%ND == 2) then
             nGauss(1:2) = (/3, 3/)
             nGauss(normDir) = 1
             NdsPerElem = 8
             NdsPerFace = 3
          elseif(TMgrid%ND == 3) then
             nGauss = 3
             nGauss(normDir) = 1
             NdsPerElem = 20
             NdsPerFace = 8
          end if
       elseif(input%ElemType == LINEAR) then
          ! ... 2nd order Gauss quadrature
          allocate(xi(2))
          xi(1:2) = (/-1.0_rfreal/sqrt(3.0_rfreal), 1.0_rfreal/sqrt(3.0_rfreal)/)
          allocate(W(2))
          W(:) = (/1.0_rfreal, 1.0_rfreal/)
          !if(region%TMRank == 0) write(*,'(A)') 'PlasComCM: Using LINEAR elements'
          if(TMgrid%ND == 2) then
             nGauss(1:2) = (/2, 2/)
             nGauss(normDir) = 1             
             NdsPerElem = 4
             NdsPerFace = 2
          elseif(TMgrid%ND == 3) then
             nGauss(:) = 2
             nGauss(normDir) = 1
             NdsPerElem = 8
             NdsPerFace = 4
          end if
       end if

       allocate(TMpatch%VolShpFcn(NdsPerElem,PRODUCT(nGauss)))
       allocate(TMpatch%BndShpFcn(NdsPerFace,PRODUCT(nGauss)))
       allocate(VolShpFcnTmp(NdsPerElem))
       allocate(BndShpFcnTmp(NdsPerFace))       
       allocate(TMpatch%Weight(PRODUCT(nGauss)))

       ! ... simplicity
       Weight    => TMpatch%Weight
       VolShpFcn => TMpatch%VolShpFcn
       BndShpFcn => TMpatch%BndShpFcn

       ! ... find the values of the shape functions at each Gauss point
       ! ... don't want to loop over gk for 2D
       do gk = 1, nGauss(3)
          do gj = 1, nGauss(2)
             do gi = 1, nGauss(1)
                lg = (gk-1)*PRODUCT(nGauss(1:2)) + (gj-1)*nGauss(1) + (gi-1) + 1
                
                if(input%ElemType == QUADRATIC) then
                   if(TMgrid%ND == 2) then
                      call Quad8(VolShpFcnTmp,xi(gi),xi(gj))
                      if(normDir == 1) then
                         call Line3(BndShpFcnTmp,xi(gj))
                         Weight(lg) = W(gj)
                      elseif(normDir == 2) then 
                         call Line3(BndShpFcnTmp,xi(gi))
                         Weight(lg) = W(gi)
                      end if
                   end if
                   if(TMgrid%ND == 3) then
                      call Hex20(VolShpFcnTmp,xi(gi),xi(gj),xi(gk))
                      if(normDir == 1) then
                         call Quad8(BndShpFcnTmp,xi(gj),xi(gk))
                         Weight(lg) = W(gj)*W(gk)
                      elseif(normDir == 2) then
                         call Quad8(BndShpFcnTmp,xi(gi),xi(gk))
                         Weight(lg) = W(gi)*W(gk)
                      elseif(normDir == 3) then
                         call Quad8(BndShpFcnTmp,xi(gi),xi(gj))
                         Weight(lg) = W(gi)*W(gj)
                      end if
                   end if
                elseif(input%ElemType == LINEAR) then
                   if(TMgrid%ND == 2) then
                      call Quad4(VolShpFcnTmp,xi(gi),xi(gj))
                      if(normDir == 1) then
                         call Line2(BndShpFcnTmp,xi(gj))
                         Weight(lg) = W(gj)
                      elseif(normDir == 2) then 
                         call Line2(BndShpFcnTmp,xi(gi))
                         Weight(lg) = W(gi)
                      end if
                   end if
                   if(TMgrid%ND == 3) then
                      call Hex8(VolShpFcnTmp,xi(gi),xi(gj),xi(gk))
                      if(normDir == 1) then
                         call Quad4(BndShpFcnTmp,xi(gj),xi(gk))
                         Weight(lg) = W(gj)*W(gk)
                      elseif(normDir == 2) then
                         call Quad4(BndShpFcnTmp,xi(gi),xi(gk))
                         Weight(lg) = W(gi)*W(gk)
                      elseif(normDir == 3) then
                         call Quad4(BndShpFcnTmp,xi(gi),xi(gj))
                         Weight(lg) = W(gi)*W(gj)
                      end if
                      !call graceful_exit(region%myrank, 'Hex8 not yet ready ... Exiting')
                   end if
                end if
                
                do i = 1, NdsPerElem
                   VolShpFcn(i,lg) = VolShpFcnTmp(i)
                end do
                do i = 1, NdsPerFace
                   BndShpFcn(i,lg) = BndShpFcnTmp(i)
                end do

             end do
          end do
       end do
       deallocate(xi,W,VolShpFcnTmp,BndShpFcnTmp)
    end do

  end Subroutine GetBndShpFcn

  Subroutine GetdBndShpFcn(region)

    USE ModGlobal
    USE ModDataStruct
    USE ModMPI
    
    implicit none

    Type(t_region), pointer :: region

    ! ... local variables
    Type(t_mixt_input), pointer :: input
    Type(t_sgrid), pointer :: TMgrid
    Type(t_spatch), pointer :: TMpatch
    Real(rfreal), pointer :: dVolShpFcn(:,:,:),dBndShpFcn(:,:,:)
    Real(rfreal), allocatable :: xi(:),eta(:),zeta(:), dVolShpFcnTmp(:,:),dBndShpFcnTmp(:,:)
    Integer :: i, j, k, ng, gp, gi, gj, gk, nGauss(3), lg, NdsPerElem, npatch, NdsPerFace
    Integer :: normDir, sgn, GridID, ND, dir
    
     
    do npatch = 1, region%nTMPatches
       
       ! ... simplicity
       TMpatch => region%TMpatch(npatch)
       GridID  =  TMpatch%GridID
       TMgrid  => region%TMgrid(GridID)
       input   => TMgrid%input
       ND      =  TMgrid%ND

       normDir = abs(TMpatch%normDir)
       sgn = normDir / TMpatch%normDir
       
       nGauss(:) = 1
       if(input%ElemType == QUADRATIC) then
          ! ... 3rd order Gauss quadrature
          allocate(xi(3),eta(3),zeta(3))
            xi(1:3) = (/-sqrt(0.6_rfreal), 0.0_rfreal, sqrt(0.6_rfreal)/)
           eta(1:3) = (/-sqrt(0.6_rfreal), 0.0_rfreal, sqrt(0.6_rfreal)/)
          zeta(1:3) = (/-sqrt(0.6_rfreal), 0.0_rfreal, sqrt(0.6_rfreal)/)
          !if(region%TMRank == 0) write(*,'(A)') 'PlasComCM: Using QUADRATIC elements'
          if(ND == 2) then
             if(normDir == 1)  xi(:) = -1.0_rfreal*DBLE(sgn)
             if(normDir == 2) eta(:) = -1.0_rfreal*DBLE(sgn)
             nGauss(1:2) = (/3, 3/)
             nGauss(normDir) = 1
             NdsPerElem = 8
             NdsPerFace = 3
          elseif(ND == 3) then
             if(normDir == 1)   xi(:) = -1.0_rfreal*DBLE(sgn)
             if(normDir == 2)  eta(:) = -1.0_rfreal*DBLE(sgn)
             if(normDir == 3) zeta(:) = -1.0_rfreal*DBLE(sgn)
             nGauss = 3
             nGauss(normDir) = 1
             NdsPerElem = 20
             NdsPerFace = 8
          end if
       elseif(input%ElemType == LINEAR) then
          ! ... 2nd order Gauss quadrature
          allocate(xi(2),eta(2),zeta(2))
            xi(1:2) = (/-1.0_rfreal/sqrt(3.0_rfreal), 1.0_rfreal/sqrt(3.0_rfreal)/)
           eta(1:2) = (/-1.0_rfreal/sqrt(3.0_rfreal), 1.0_rfreal/sqrt(3.0_rfreal)/)
          zeta(1:2) = (/-1.0_rfreal/sqrt(3.0_rfreal), 1.0_rfreal/sqrt(3.0_rfreal)/)
          !if(region%TMRank == 0) write(*,'(A)') 'PlasComCM: Using LINEAR elements'
          if(ND == 2) then
             if(normDir == 1)   xi(:) = -1.0_rfreal*DBLE(sgn)
             if(normDir == 2)  eta(:) = -1.0_rfreal*DBLE(sgn)
             nGauss(1:2) = (/2, 2/)
             nGauss(normDir) = 1             
             NdsPerElem = 4
             NdsPerFace = 2
          elseif(ND == 3) then
             if(normDir == 1)   xi(:) = -1.0_rfreal*DBLE(sgn)
             if(normDir == 2)  eta(:) = -1.0_rfreal*DBLE(sgn)
             if(normDir == 3) zeta(:) = -1.0_rfreal*DBLE(sgn)
             nGauss(:) = 2
             nGauss(normDir) = 1
             NdsPerElem = 8
             NdsPerFace = 4
          end if
       end if

       allocate(TMpatch%dVolShpFcn(NdsPerElem,ND,PRODUCT(nGauss)))
       allocate(dVolShpFcnTmp(NdsPerElem,ND))

       ! ... simplicity
       dVolShpFcn => TMpatch%dVolShpFcn

       ! ... find the values of the shape functions at each Gauss point
       ! ... don't want to loop over gk for 2D
       do gk = 1, nGauss(3)
          do gj = 1, nGauss(2)
             do gi = 1, nGauss(1)
                lg = (gk-1)*PRODUCT(nGauss(1:2)) + (gj-1)*nGauss(1) + (gi-1) + 1
                
                if(input%ElemType == QUADRATIC) then
                   if(TMgrid%ND == 2) then
                      call dQuad8(dVolShpFcnTmp,xi(gi),eta(gj))
!                      if(normDir == 1) call dLine3(dBndShpFcnTmp,xi(gj))
!                      if(normDir == 2) call dLine3(dBndShpFcnTmp,xi(gi))
                   end if
                   if(TMgrid%ND == 3) then
                      call dHex20(dVolShpFcnTmp,xi(gi),eta(gj),zeta(gk))
!                      if(normDir == 1) call dQuad8(dBndShpFcnTmp,xi(gj),xi(gk))
!                      if(normDir == 2) call dQuad8(dBndShpFcnTmp,xi(gi),xi(gk))
!                      if(normDir == 3) call dQuad8(dBndShpFcnTmp,xi(gi),xi(gj))
                   end if
                elseif(input%ElemType == LINEAR) then
                   if(TMgrid%ND == 2) then
                      call dQuad4(dVolShpFcnTmp,xi(gi),eta(gj))
                   end if
                   if(TMgrid%ND == 3) then
                      call dHex8(dVolShpFcnTmp,xi(gi),eta(gj),zeta(gk))
                   end if
                end if
                
                do dir = 1, ND
                   do i = 1, NdsPerElem
                      dVolShpFcn(i,dir,lg) = dVolShpFcnTmp(i,dir)
                   end do
                end do
                
             end do
          end do
       end do
       deallocate(xi,eta,zeta,dVolShpFcnTmp)
    end do

  end Subroutine GetdBndShpFcn


  subroutine Line2(ShpFcn,xi)

    USE ModGlobal
    USE ModDataStruct
    
    Implicit None

    Real(rfreal) :: ShpFcn(2), xi

    ShpFcn(1) = -0.5_rfreal*(xi - 1.0_rfreal)
    ShpFcn(2) =  0.5_rfreal*(xi + 1.0_rfreal)

  end subroutine Line2

  subroutine Line3(ShpFcn,xi)

    USE ModGlobal
    USE ModDataStruct
    
    Implicit None

    Real(rfreal) :: ShpFcn(3), xi

    ShpFcn(1) = 0.5_rfreal*(-xi + xi*xi)
    ShpFcn(2) = 0.5_rfreal*( xi + xi*xi)
    ShpFcn(3) = (1.0_rfreal-xi*xi)

  end subroutine Line3

  subroutine Quad4(ShpFcn,xi,eta)
    
    USE ModGlobal
    USE ModDataStruct
    
    Implicit None
    
    Real(rfreal) :: ShpFcn(4), xi, eta
    
    ShpFcn(1) =  (xi-1.0_rfreal)*(eta-1.0_rfreal)*0.25_rfreal;
    ShpFcn(2) = -(xi+1.0_rfreal)*(eta-1.0_rfreal)*0.25_rfreal;
    ShpFcn(3) =  (xi+1.0_rfreal)*(eta+1.0_rfreal)*0.25_rfreal;
    ShpFcn(4) = -(xi-1.0_rfreal)*(eta+1.0_rfreal)*0.25_rfreal;

  end subroutine Quad4

  subroutine Quad8(ShpFcn,xi,eta)
    
    USE ModGlobal
    USE ModDataStruct

    Implicit None

    Real(rfreal) :: ShpFcn(8), xi, eta
    
    ! ... nodes 5 to 8
    ShpFcn(5) = 0.5_rfreal*(1.0_rfreal- xi*xi)*(1.0_rfreal- eta    )
    ShpFcn(6) = 0.5_rfreal*(1.0_rfreal+ xi   )*(1.0_rfreal- eta*eta)
    ShpFcn(7) = 0.5_rfreal*(1.0_rfreal- xi*xi)*(1.0_rfreal+ eta    )
    ShpFcn(8) = 0.5_rfreal*(1.0_rfreal- xi   )*(1.0_rfreal- eta*eta)

    ! ... nodes 1 to 4
    ShpFcn(1) = 0.25_rfreal*(1.0_rfreal- xi)*(1.0_rfreal- eta) - 0.5_rfreal*(ShpFcn(8) + ShpFcn(5))
    ShpFcn(2) = 0.25_rfreal*(1.0_rfreal+ xi)*(1.0_rfreal- eta) - 0.5_rfreal*(ShpFcn(5) + ShpFcn(6))
    ShpFcn(3) = 0.25_rfreal*(1.0_rfreal+ xi)*(1.0_rfreal+ eta) - 0.5_rfreal*(ShpFcn(6) + ShpFcn(7))
    ShpFcn(4) = 0.25_rfreal*(1.0_rfreal- xi)*(1.0_rfreal+ eta) - 0.5_rfreal*(ShpFcn(7) + ShpFcn(8))

  end subroutine Quad8

  subroutine Hex8(ShpFcn,xi,eta,zeta)
    
    USE ModGlobal
    USE ModDataStruct

    Implicit None

    Real(rfreal) :: ShpFcn(8), xi, eta, zeta

    ! ... 1 to 4
    ShpFcn(1) = -(xi-1.0_rfreal)*(eta-1.0_rfreal)*(zeta-1.0_rfreal)*0.125_rfreal;
    ShpFcn(2) =  (xi+1.0_rfreal)*(eta-1.0_rfreal)*(zeta-1.0_rfreal)*0.125_rfreal;
    ShpFcn(3) = -(xi+1.0_rfreal)*(eta+1.0_rfreal)*(zeta-1.0_rfreal)*0.125_rfreal;
    ShpFcn(4) =  (xi-1.0_rfreal)*(eta+1.0_rfreal)*(zeta-1.0_rfreal)*0.125_rfreal;

    ! ... 5 to 8
    ShpFcn(5) =  (xi-1.0_rfreal)*(eta-1.0_rfreal)*(zeta+1.0_rfreal)*0.125_rfreal;
    ShpFcn(6) = -(xi+1.0_rfreal)*(eta-1.0_rfreal)*(zeta+1.0_rfreal)*0.125_rfreal;
    ShpFcn(7) =  (xi+1.0_rfreal)*(eta+1.0_rfreal)*(zeta+1.0_rfreal)*0.125_rfreal;
    ShpFcn(8) = -(xi-1.0_rfreal)*(eta+1.0_rfreal)*(zeta+1.0_rfreal)*0.125_rfreal;    
    
  end subroutine Hex8 

  subroutine Hex20(ShpFcn,xi,eta,zeta)
    
    USE ModGlobal
    USE ModDataStruct

    Real(rfreal) :: ShpFcn(20), xi, eta, zeta
    
    ! ... local variables
    Integer :: i, j, k, ivec(4)
    Real(rfreal) :: XiVal(20,3)

    ! ... natural coordinates of the isoparametric element nodes
    XiVal(1,:)  = (/-1.0_rfreal, -1.0_rfreal, -1.0_rfreal/)
    XiVal(2,:)  = (/ 1.0_rfreal, -1.0_rfreal, -1.0_rfreal/)
    XiVal(3,:)  = (/ 1.0_rfreal,  1.0_rfreal, -1.0_rfreal/)
    XiVal(4,:)  = (/-1.0_rfreal,  1.0_rfreal, -1.0_rfreal/)
    XiVal(5,:)  = (/-1.0_rfreal, -1.0_rfreal,  1.0_rfreal/)
    XiVal(6,:)  = (/ 1.0_rfreal, -1.0_rfreal,  1.0_rfreal/)
    XiVal(7,:)  = (/ 1.0_rfreal,  1.0_rfreal,  1.0_rfreal/)
    XiVal(8,:)  = (/-1.0_rfreal,  1.0_rfreal,  1.0_rfreal/)
    XiVal(9,:)  = (/ 0.0_rfreal, -1.0_rfreal, -1.0_rfreal/)
    XiVal(10,:) = (/ 1.0_rfreal,  0.0_rfreal, -1.0_rfreal/)
    XiVal(11,:) = (/ 0.0_rfreal,  1.0_rfreal, -1.0_rfreal/)
    XiVal(12,:) = (/-1.0_rfreal,  0.0_rfreal, -1.0_rfreal/)
    XiVal(13,:) = (/-1.0_rfreal, -1.0_rfreal,  0.0_rfreal/)
    XiVal(14,:) = (/ 1.0_rfreal, -1.0_rfreal,  0.0_rfreal/)
    XiVal(15,:) = (/ 1.0_rfreal,  1.0_rfreal,  0.0_rfreal/)
    XiVal(16,:) = (/-1.0_rfreal,  1.0_rfreal,  0.0_rfreal/)
    XiVal(17,:) = (/ 0.0_rfreal, -1.0_rfreal,  1.0_rfreal/)
    XiVal(18,:) = (/ 1.0_rfreal,  0.0_rfreal,  1.0_rfreal/)
    XiVal(19,:) = (/ 0.0_rfreal,  1.0_rfreal,  1.0_rfreal/)
    XiVal(20,:) = (/-1.0_rfreal,  0.0_rfreal,  1.0_rfreal/)
    
    ! ... nodes 1 to 8
    do i = 1, 8
       ShpFcn(i) = 0.125_rfreal*(1.0_rfreal+ xi*XiVal(i,1))*(1.0_rfreal+ eta*XiVal(i,2))*(1.0_rfreal+ zeta*XiVal(i,3))&
            *(xi*XiVal(i,1) + eta*XiVal(i,2) + zeta*XiVal(i,3) - 2.0_rfreal)
    end do

    ! ... nodes 9, 11, 17, 19
    ivec = (/9, 11, 17, 19/)
    do j = 1, 4
       i = ivec(j)
       ShpFcn(i) = 0.25_rfreal*(1.0_rfreal- xi*xi)*(1.0_rfreal+ eta*XiVal(i,2))*(1.0_rfreal+ zeta*XiVal(i,3))
    end do

    ! ... nodes 10, 12, 18, 20
    ivec = (/10, 12, 18, 20/)
    do j = 1, 4
       i = ivec(j)
       ShpFcn(i) = 0.25_rfreal*(1.0_rfreal+ xi*XiVal(i,1))*(1.0_rfreal- eta*eta)*(1.0_rfreal+ zeta*XiVal(i,3))
    end do

    ! ... nodes 13 to 16
    do i = 13, 16 
       ShpFcn(i) = 0.25_rfreal*(1.0_rfreal+ xi*XiVal(i,1))*(1.0_rfreal+ eta*XiVal(i,2))*(1.0_rfreal- zeta*zeta)
    end do
    
  end subroutine Hex20

  subroutine dLine3(dShpFcn,xi)

    USE ModGlobal
    USE ModDataStruct
    
    Implicit None

    Real(rfreal) :: dShpFcn(3), xi

    dShpFcn(1) = 0.5_rfreal*(-1.0_rfreal + 2.0_rfreal*xi)
    dShpFcn(2) = 0.5_rfreal*( 1.0_rfreal + 2.0_rfreal*xi)
    dShpFcn(3) = (-2.0_rfreal*xi)

  end subroutine dLine3

  subroutine dQuad4(dShpFcn,xi,eta)
    
    USE ModGlobal
    USE ModDataStruct

    Real(rfreal) :: dShpFcn(4,2), xi, eta

    ! ... d/d xi
    ! ... nodes 1 to 4
    dShpFcn(1,1) = - 0.25_rfreal*(1.0_rfreal- eta)
    dShpFcn(2,1) =   0.25_rfreal*(1.0_rfreal- eta)
    dShpFcn(3,1) =   0.25_rfreal*(1.0_rfreal+ eta)
    dShpFcn(4,1) = - 0.25_rfreal*(1.0_rfreal+ eta)
 
    ! ... d/d eta
    ! ... nodes 1 to 4
    dShpFcn(1,2) = - 0.25_rfreal*(1.0_rfreal- xi)
    dShpFcn(2,2) = - 0.25_rfreal*(1.0_rfreal+ xi)
    dShpFcn(3,2) =   0.25_rfreal*(1.0_rfreal+ xi)
    dShpFcn(4,2) =   0.25_rfreal*(1.0_rfreal- xi)
   
  end subroutine dQuad4

  subroutine dQuad8(dShpFcn,xi,eta)
    
    USE ModGlobal
    USE ModDataStruct

    Real(rfreal) :: dShpFcn(8,2), xi, eta
    
    ! ... d/d xi
    ! ... nodes 5 to 8
    dShpFcn(5,1) = - xi*(1.0_rfreal- eta    )
    dShpFcn(6,1) =   0.5_rfreal*(1.0_rfreal- eta*eta)
    dShpFcn(7,1) = - xi*(1.0_rfreal+ eta    )
    dShpFcn(8,1) = - 0.5_rfreal*(1.0_rfreal- eta*eta)

    ! ... nodes 1.0_rfrealto 4
    dShpFcn(1,1) = - 0.25_rfreal*(1.0_rfreal- eta) - 0.5_rfreal*(dShpFcn(8,1) + dShpFcn(5,1))
    dShpFcn(2,1) =   0.25_rfreal*(1.0_rfreal- eta) - 0.5_rfreal*(dShpFcn(5,1) + dShpFcn(6,1))
    dShpFcn(3,1) =   0.25_rfreal*(1.0_rfreal+ eta) - 0.5_rfreal*(dShpFcn(6,1) + dShpFcn(7,1))
    dShpFcn(4,1) = - 0.25_rfreal*(1.0_rfreal+ eta) - 0.5_rfreal*(dShpFcn(7,1) + dShpFcn(8,1))
 
    ! ... d/d eta
    ! ... nodes 5 to 8
    dShpFcn(5,2) = - 0.5_rfreal*(1.0_rfreal- xi*xi)
    dShpFcn(6,2) = -(1.0_rfreal+ xi   )*(eta)
    dShpFcn(7,2) = 0.5_rfreal*(1.0_rfreal- xi*xi)
    dShpFcn(8,2) = -(1.0_rfreal- xi   )*(eta)

    ! ... nodes 1 to 4
    dShpFcn(1,2) = - 0.25_rfreal*(1.0_rfreal- xi) - 0.5_rfreal*(dShpFcn(8,2) + dShpFcn(5,2))
    dShpFcn(2,2) = - 0.25_rfreal*(1.0_rfreal+ xi) - 0.5_rfreal*(dShpFcn(5,2) + dShpFcn(6,2))
    dShpFcn(3,2) =   0.25_rfreal*(1.0_rfreal+ xi) - 0.5_rfreal*(dShpFcn(6,2) + dShpFcn(7,2))
    dShpFcn(4,2) =   0.25_rfreal*(1.0_rfreal- xi) - 0.5_rfreal*(dShpFcn(7,2) + dShpFcn(8,2))
   
  end subroutine dQuad8

  subroutine dHex8(dShpFcn,xi,eta,zeta)
    
    USE ModGlobal
    USE ModDataStruct

    Real(rfreal) :: dShpFcn(8,3), xi, eta, zeta

    ! ... d/d xi
    ! ... nodes 1 to 4
    dShpFcn(1,1) = -(eta-1.0_rfreal)*(zeta-1.0_rfreal)*0.125_rfreal;
    dShpFcn(2,1) =  (eta-1.0_rfreal)*(zeta-1.0_rfreal)*0.125_rfreal;
    dShpFcn(3,1) = -(eta+1.0_rfreal)*(zeta-1.0_rfreal)*0.125_rfreal;
    dShpFcn(4,1) =  (eta+1.0_rfreal)*(zeta-1.0_rfreal)*0.125_rfreal;

    ! ... 5 to 8
    dShpFcn(5,1) =  (eta-1.0_rfreal)*(zeta+1.0_rfreal)*0.125_rfreal;
    dShpFcn(6,1) = -(eta-1.0_rfreal)*(zeta+1.0_rfreal)*0.125_rfreal;
    dShpFcn(7,1) =  (eta+1.0_rfreal)*(zeta+1.0_rfreal)*0.125_rfreal;
    dShpFcn(8,1) = -(eta+1.0_rfreal)*(zeta+1.0_rfreal)*0.125_rfreal;    
 
    ! ... d/d eta
    ! ... nodes 1 to 4
    dShpFcn(1,2) = -(xi-1.0_rfreal)*(zeta-1.0_rfreal)*0.125_rfreal;
    dShpFcn(2,2) =  (xi+1.0_rfreal)*(zeta-1.0_rfreal)*0.125_rfreal;
    dShpFcn(3,2) = -(xi+1.0_rfreal)*(zeta-1.0_rfreal)*0.125_rfreal;
    dShpFcn(4,2) =  (xi-1.0_rfreal)*(zeta-1.0_rfreal)*0.125_rfreal;

    ! ... 5 to 8
    dShpFcn(5,2) =  (xi-1.0_rfreal)*(zeta+1.0_rfreal)*0.125_rfreal;
    dShpFcn(6,2) = -(xi+1.0_rfreal)*(zeta+1.0_rfreal)*0.125_rfreal;
    dShpFcn(7,2) =  (xi+1.0_rfreal)*(zeta+1.0_rfreal)*0.125_rfreal;
    dShpFcn(8,2) = -(xi-1.0_rfreal)*(zeta+1.0_rfreal)*0.125_rfreal;    

    ! ... d/d zeta
    ! ... nodes 1 to 4
    dShpFcn(1,3) = -(xi-1.0_rfreal)*(eta-1.0_rfreal)*0.125_rfreal;
    dShpFcn(2,3) =  (xi+1.0_rfreal)*(eta-1.0_rfreal)*0.125_rfreal;
    dShpFcn(3,3) = -(xi+1.0_rfreal)*(eta+1.0_rfreal)*0.125_rfreal;
    dShpFcn(4,3) =  (xi-1.0_rfreal)*(eta+1.0_rfreal)*0.125_rfreal;

    ! ... 5 to 8
    dShpFcn(5,3) =  (xi-1.0_rfreal)*(eta-1.0_rfreal)*0.125_rfreal;
    dShpFcn(6,3) = -(xi+1.0_rfreal)*(eta-1.0_rfreal)*0.125_rfreal;
    dShpFcn(7,3) =  (xi+1.0_rfreal)*(eta+1.0_rfreal)*0.125_rfreal;
    dShpFcn(8,3) = -(xi-1.0_rfreal)*(eta+1.0_rfreal)*0.125_rfreal;    
   
  end subroutine dHex8
    
  subroutine dHex20(dShpFcn,xi,eta,zeta)
    
    USE ModGlobal
    USE ModDataStruct

    Real(rfreal) :: dShpFcn(20,3), xi, eta, zeta
    
    ! ... local variables
    Integer :: i, j, k, ivec(4)
    Real(rfreal) :: XiVal(20,3)

    ! ... natural coordinates of the isoparametric element nodes
    XiVal(1,:)  = (/-1.0_rfreal, -1.0_rfreal, -1.0_rfreal/)
    XiVal(2,:)  = (/ 1.0_rfreal, -1.0_rfreal, -1.0_rfreal/)
    XiVal(3,:)  = (/ 1.0_rfreal,  1.0_rfreal, -1.0_rfreal/)
    XiVal(4,:)  = (/-1.0_rfreal,  1.0_rfreal, -1.0_rfreal/)
    XiVal(5,:)  = (/-1.0_rfreal, -1.0_rfreal,  1.0_rfreal/)
    XiVal(6,:)  = (/ 1.0_rfreal, -1.0_rfreal,  1.0_rfreal/)
    XiVal(7,:)  = (/ 1.0_rfreal,  1.0_rfreal,  1.0_rfreal/)
    XiVal(8,:)  = (/-1.0_rfreal,  1.0_rfreal,  1.0_rfreal/)
    XiVal(9,:)  = (/ 0.0_rfreal, -1.0_rfreal, -1.0_rfreal/)
    XiVal(10,:) = (/ 1.0_rfreal,  0.0_rfreal, -1.0_rfreal/)
    XiVal(11,:) = (/ 0.0_rfreal,  1.0_rfreal, -1.0_rfreal/)
    XiVal(12,:) = (/-1.0_rfreal,  0.0_rfreal, -1.0_rfreal/)
    XiVal(13,:) = (/-1.0_rfreal, -1.0_rfreal,  0.0_rfreal/)
    XiVal(14,:) = (/ 1.0_rfreal, -1.0_rfreal,  0.0_rfreal/)
    XiVal(15,:) = (/ 1.0_rfreal,  1.0_rfreal,  0.0_rfreal/)
    XiVal(16,:) = (/-1.0_rfreal,  1.0_rfreal,  0.0_rfreal/)
    XiVal(17,:) = (/ 0.0_rfreal, -1.0_rfreal,  1.0_rfreal/)
    XiVal(18,:) = (/ 1.0_rfreal,  0.0_rfreal,  1.0_rfreal/)
    XiVal(19,:) = (/ 0.0_rfreal,  1.0_rfreal,  1.0_rfreal/)
    XiVal(20,:) = (/-1.0_rfreal,  0.0_rfreal,  1.0_rfreal/)
    
    ! ... nodes 1 to 8
    do i = 1, 8
       ! ... d/d xi
       dShpFcn(i,1) = 0.125_rfreal*(1.0_rfreal+ eta*XiVal(i,2))*(1.0_rfreal+ zeta*XiVal(i,3))&
            *(2.0_rfreal*xi*XiVal(i,1)**2 + Xival(i,1)*(eta*XiVal(i,2) + zeta*XiVal(i,3) - 1.0_rfreal))
       ! ... d/d eta
       dShpFcn(i,2) = 0.125_rfreal*(1.0_rfreal+ xi*XiVal(i,1))*(1.0_rfreal+ zeta*XiVal(i,3))&
            *(2.0_rfreal*eta*XiVal(i,2)**2 + Xival(i,2)*(xi*XiVal(i,1) + zeta*XiVal(i,3) - 1.0_rfreal))
       ! ... d/d zeta
       dShpFcn(i,3) = 0.125_rfreal*(1.0_rfreal+ xi*XiVal(i,1))*(1.0_rfreal+ eta*XiVal(i,2))&
            *(2.0_rfreal*zeta*XiVal(i,3)**2 + Xival(i,3)*(xi*XiVal(i,1) + eta*XiVal(i,2) - 1.0_rfreal))
    end do

    ! ... nodes 9, 11, 17, 19
    ivec = (/9, 11, 17, 19/)
    do j = 1, 4
       i = ivec(j)
       ! ... d/d xi
       dShpFcn(i,1) = 0.25_rfreal*(-2.0_rfreal*xi)*(1.0_rfreal+ eta*XiVal(i,2))*(1.0_rfreal+ zeta*XiVal(i,3))
       ! ... d/d eta
       dShpFcn(i,2) = 0.25_rfreal*(1.0_rfreal- xi*xi)*(XiVal(i,2))*(1.0_rfreal+ zeta*XiVal(i,3))
       ! ... d/d zeta
       dShpFcn(i,3) = 0.25_rfreal*(1.0_rfreal- xi*xi)*(1.0_rfreal+ eta*XiVal(i,2))*(XiVal(i,3))
    end do

    ! ... nodes 10, 12, 18, 20
    ivec = (/10, 12, 18, 20/)
    do j = 1, 4
       i = ivec(j)
       ! ... d/d xi
       dShpFcn(i,1) = 0.25_rfreal*(XiVal(i,1))*(1.0_rfreal- eta*eta)*(1.0_rfreal+ zeta*XiVal(i,3))
       ! ... d/d eta
       dShpFcn(i,2) = 0.25_rfreal*(1.0_rfreal+ xi*XiVal(i,1))*(-2.0_rfreal*eta)*(1.0_rfreal+ zeta*XiVal(i,3))
       ! ... d/d zeta
       dShpFcn(i,3) = 0.25_rfreal*(1.0_rfreal+ xi*XiVal(i,1))*(1.0_rfreal- eta*eta)*(XiVal(i,3))
    end do

    ! ... nodes 13 to 16
    do i = 13, 16 
       ! ... d/d xi
       dShpFcn(i,1) = 0.25_rfreal*(XiVal(i,1))*(1.0_rfreal+ eta*XiVal(i,2))*(1.0_rfreal- zeta*zeta)
       ! ... d/d eta
       dShpFcn(i,2) = 0.25_rfreal*(1.0_rfreal+ xi*XiVal(i,1))*(XiVal(i,2))*(1.0_rfreal- zeta*zeta)
       ! ... d/d zeta
       dShpFcn(i,3) = 0.25_rfreal*(1.0_rfreal+ xi*XiVal(i,1))*(1.0_rfreal+ eta*XiVal(i,2))*(-2.0_rfreal*zeta)
    end do
    
  end subroutine dHex20
    
end Module ModTMInt
