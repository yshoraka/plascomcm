# Copyright (c) 2015, University of Illinois at Urbana-Champaign, XPACC
# License: MIT, http://opensource.org/licenses/MIT
# This directory contains the PlasComCM source files, both Fortran and C
# The Fortran files end in .fpp and are to be pre-processed into files that
# end if .f90 prior to compilation
#
# Some of the file compiled depend on options passed to ./configure, including
#
# --enable-tmsolver : include the files necessary to use the thermo-
#                     mechanical solver, which themselves depend on PETSc
#
# Automake expects certain variables to defined, including
#
# bin_PROGRAMS = plascomcm : name of executable
# plascomcm_SOURCES = {list files that are *compiled* and *distributed*}
# nodist_plascomcm_SOURCES = {list of files that are *compiled* and
#                             *not distributed*}
# EXTRA_DIST = extra files that should also be distributed
#
# Make sure automake knows where to look for the local macros for configure
ACLOCAL_AMFLAGS = -I m4
# Build the object files in the soucre directory instead of the top
# diretory (subdir-objects) and don't act like a GNU tool (foreign)
# If you change the subdir-objects, change the FC_DEPDIR option to match
AUTOMAKE_OPTIONS = subdir-objects foreign

# Directory for Fortran module files
MODDIR = mod

# Initialize variables
BUILT_SOURCES =
EXTRA_DIST =
CLEAN_LOCAL_TARGETS =
DISTCLEAN_LOCAL_TARGETS =
deps_fpp_src =
deps_c_src =
FPPFLAGS =

# For suffix rules
SUFFIXES  = .fpp .$(FC_MODEXT)

# The actual names of files are listed in Makefile.mk, in the current
# working directory, to keep this file clean
include Makefile.mk

plascomcm_fc_src = $(plascomcm_fpp_src:.fpp=.f90)

# Generate dependencies
deps_fpp_src += $(plascomcm_fpp_src)
deps_c_src += $(plascomcm_c_src)

# Main program is PlasComCM
bin_PROGRAMS = plascomcm
plascomcm_DEPENDENCIES = ../amos/libamos.a
plascomcm_LDADD = -L../amos -lamos

# Define those source files that are not pre-processed that are to be
# included in the distribution 
plascomcm_SOURCES = $(plascomcm_c_src)
nodist_plascomcm_SOURCES = $(plascomcm_fc_src)
EXTRA_DIST += $(plascomcm_fpp_src)
EXTRA_DIST += $(plascomcm_dist)

# Cleanup rules
clean-local: $(CLEAN_LOCAL_TARGETS)
	rm -rf $(MODDIR)
	rm -f $(plascomcm_fc_src)
	rm -rf *.dSYM

distclean-local: $(DISTCLEAN_LOCAL_TARGETS)

#
# --------------------------------------------------------------------------
# Most of the rest of this file is common to all similar projects
# See VPATH support, however
# --------------------------------------------------------------------------

FC_MODEXT   = @FC_MODEXT@
FC_MODINC   = @FC_MODINC@
FC_MODOUT   = @FC_MODOUT@
FC_MODCASE  = @FC_MODCASE@
FC_ASM      = @FC_ASM@
AM_FCFLAGS  = $(FC_MODOUT)$(MODDIR) $(FC_MODINC). $(FC_ASM) $(FC_MODINC)$(MODDIR) $(FC_MODINC)$(srcdir)

# aggressive optimization flags
# The strange form here protects these conditionals from automake,
# and ensures that the are executed by make, not automake.
@MK@ifeq "$(USE_AGGRESSIVE_OPTS)" "1"
@MK@ifeq "$(COMPILER_FAMILY)" "intel"
@MK@  AM_CFLAGS   += -O2
@MK@  AM_CXXFLAGS += -O2
@MK@  AM_FCFLAGS  += -O3 -ip -ipo -xHost
@MK@endif
@MK@ifeq "$(COMPILER_FAMILY)" "gnu"
@MK@  AM_CFLAGS   += -O2
@MK@  AM_FCFLAGS  += -fomit-frame-pointer -O3 -ftree-vectorize -ffast-math -funroll-loops
@MK@  AM_CXXFLAGS += -O2
@MK@endif
@MK@ifeq "$(COMPILER_FAMILY)" "cray"
@MK@  AM_CFLAGS   += -O
@MK@endif
@MK@endif

# Fortran preprocessing
FPPFLAGS += @FPPFLAGS@

# --------------------------------------------------------------------------
# VPATH support
# Create directories and copy files that may be needed
BUILT_SOURCES += $(MODDIR)
$(MODDIR):
	test -d $(MODDIR) || @MKDIR_P@ $(MODDIR)

# We must use specific directories to ensure that the file (link) is in the
# build directory - VPATH is happy to find it in the source directory and
# leave it at that
.PHONY: readytorun
readytorun: $(abs_top_builddir)/bc.dat $(abs_top_builddir)/plascomcm.inp
$(abs_top_builddir)/bc.dat: $(abs_top_srcdir)/bc.dat
	@LN_S@ $(abs_top_srcdir)/bc.dat
$(abs_top_builddir)/plascomcm.inp: $(abs_top_srcdir)/plascomcm.inp
	@LN_S@ $(abs_top_srcdir)/plascomcm.inp

# --------------------------------------------------------------------------

# Rule for processing .fpp files to .f90
# Note that Make has trouble combining implicit with explicit rules.
# Thus, a change to an .fpp file will not force a recompilation of all of the
# modules that make use of the module(s) created by that .fpp file unless
# we include that as an explicit dependency
.fpp.f90:
	$(FPP) $(FPPFLAGS) $< >$@
	-@rm -f $*.s
	$(FC_DEPCOMP) --modext=$(FC_MODEXT) --modcase=$(FC_MODCASE) --depdir=$(DEPDIR) --subdirobj --modulelist=_fcmods --origsrc=$*.fpp $*.f90

# FC dependencies
# Since automake won't generate one for each Fortran source file,
# we create one.   Make sure that we have DEPDIR 
# (automake will add this for us if wel use C or C++)
DEPDIR = @DEPDIR@
# Do dependency generation first
BUILT_SOURCES += $(DEPDIR)/stamp_fclist

# If the module is not found, it is expected in MODDIR
# Include $(plascomcm_fc_src) on the dependencies, but then always rescanned
# everyfile on a change to any of them.
# Do need to add a rescan when a file is changed
# Note the two pass algorithm.  First find all of the module names defined
# by the package, then use those to create the dependency files.
# Updates to individual files will update the module list (_fcmods)
FC_DEPCOMP = @FC_DEPCOMP@

$(DEPDIR)/stamp_fclist: $(deps_fpp_src) $(deps_c_src)
	rm -f $(DEPDIR)/_fclist $(DEPDIR)/_fcmods
	touch $(DEPDIR)/_fclist
	for file in $(deps_fpp_src); do \
	    bfile=`echo $$file | sed 's/\.fpp/.f90/'`; \
	    $(MAKE) $$bfile; \
	    $(FC_DEPCOMP) --modext=$(FC_MODEXT) --modcase=$(FC_MODCASE) --depdir=$(DEPDIR) \
        --moddir=$(MODDIR) --subdirobj --subdirmod --modulelist=_fcmods --origsrc=$$file $$bfile; \
	done
	for file in $(deps_fpp_src:.fpp=.f90); do \
	    bfile=`basename $$file .f90`; \
	    sfile=`echo $$file | sed 's/\.f90/.fpp/'`; \
	    $(FC_DEPCOMP) --modext=$(FC_MODEXT) --modcase=$(FC_MODCASE) --depdir=$(DEPDIR) \
        --moddir=$(MODDIR) --subdirobj --subdirmod --modulelist=_fcmods --origsrc=$$sfile $$file; \
	    echo "include $(DEPDIR)/$$bfile.Po" >> $(DEPDIR)/_fclist; \
	done
	echo "Stamp for FC dependencies" > $(DEPDIR)/stamp_fclist

.PHONY: deps-clean deps-distclean

# _fclist and C .Po files are created by configure, so 'clean' target shouldn't delete them
# (just empty them); Fortran .Po files are created by make, so they should be deleted
CLEAN_LOCAL_TARGETS += deps-clean
deps-clean:
	rm -f $(DEPDIR)/_fcmods
	rm -f $(DEPDIR)/stamp_fclist
	echo "# dummy" > $(DEPDIR)/_fclist
	(cd $(DEPDIR) && for file in $(deps_c_src:.c=.Po); do \
	  echo "# dummy" > "$$file"; \
	done)
	(cd $(DEPDIR) && for file in $(deps_fpp_src:.fpp=.Po); do \
	  rm -f "$$file"; \
	done)

DISTCLEAN_LOCAL_TARGETS += deps-distclean
deps-distclean:
	rm -rf $(DEPDIR)

#Include dependency files
@AMDEP_TRUE@@am__include@ @am__quote@$(DEPDIR)/_fclist@am__quote@
