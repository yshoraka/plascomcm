!> @file ModDataStruct.fpp
! Copyright (c) 2015, University of Illinois at Urbana-Champaign, XPACC
! License: MIT, http://opensource.org/licenses/MIT
!-----------------------------------------------------------------------
! ModDataStruct.f90
!
! - basic data structure for a solution region
!
! Revision history
! - 18 Dec 2006 : DJB : initial code
! - 20 Dec 2006 : DJB : evolution of data structures
!
!-----------------------------------------------------------------------

MODULE ModDataStruct

  USE ModGlobal

#ifdef HAVE_CANTERA
  use cantera, only: phase_t ! lower-case 'use'
#endif
#ifdef HAVE_HDF5
  use hdf5 ! lower-case 'use' to trick makedeps
#endif
#ifdef BUILD_ELECTRIC
  use iso_c_binding
#endif

  TYPE, PUBLIC :: t_global

    CHARACTER(LEN=200) :: prefx   ! I/O file prefix
    INTEGER :: prefx_lngth        ! length of prefix

    REAL(RFREAL), DIMENSION(:), POINTER :: temp1
    REAL(RFREAL), DIMENSION(:), POINTER :: temp2

    INTEGER, DIMENSION(:), POINTER :: itemp1
    INTEGER, DIMENSION(:), POINTER :: itemp2
  
  END TYPE t_global

  TYPE t_region

    INTEGER, POINTER :: iRegionGlobal(:), iGridGlobal, grid_to_processor(:), grid_to_processor_coreComm(:)
    INTEGER, POINTER :: gridToProcMap(:,:), AllGridsGlobalSize(:,:)
    INTEGER :: nGrids, nGridsGlobal, movingGrid, nPatches, num_coreRank_this_processor
    INTEGER :: myrank_inCoreComm, numproc_inCoreComm, coreComm,nsteps_this_run
    REAL(RFREAL) :: total_time, TM_total_time, init_grids_time, init_state_time
    REAL(RFREAL) :: init_time, run_time,mainloop_time, rk_time, interpio_time,chem_init_time

    TYPE(t_grid), POINTER :: grid(:), gridOld(:)
    TYPE(t_mixt), POINTER :: state(:), mean(:)
    TYPE(t_patch), POINTER :: patch(:)
    TYPE(t_interp), POINTER :: interp
    TYPE(t_mixt_input), POINTER :: input
    TYPE(t_mpi_timings), POINTER :: mpi_timings(:)
    TYPE(t_sgrid), POINTER :: TMgrid(:)
    TYPE(t_smixt), POINTER :: TMstate(:)
    TYPE(t_spatch), POINTER :: TMpatch(:)
    TYPE(t_epatch), POINTER :: EFpatch(:)
    TYPE(t_fsi), POINTER :: ipatch(:)
  
    ! ... module variables
    TYPE(t_optimization), POINTER :: optimization
    TYPE(t_modglobal), POINTER :: global
    TYPE(t_param), POINTER :: first_param_ptr
    TYPE(t_sat_descriptor), POINTER :: sat_descriptor
    TYPE(t_hdf5_io), POINTER :: hdf5_io
    TYPE(t_lighthill), POINTER :: lighthill
    TYPE(t_deriv), POINTER :: deriv
    TYPE(t_navierstokesrhs), POINTER :: nsRHS
    TYPE(t_rk), POINTER :: rk
    TYPE(t_eulerfv), POINTER :: eulerfv
    TYPE(t_electric), POINTER :: electric(:)
#ifdef USE_AMPI
    TYPE(t_ampi), POINTER :: ampi
#endif
    TYPE (t_gengrid), POINTER :: genGrid
    INTEGER :: modIOnnz
    
    ! ... MPI rank
    INTEGER :: myrank
    
    ! ... logical variables to test if a module has been initialized
    LOGICAL :: lighthillInit, derivInit, nsbcInit, nsrhsInit, rkInit, ioInitd
    LOGICAL :: eulerfvInit, eulerfvInit1, eulerfvInit2, eulerfvInit3, eulerfvInit4
  
    ! ... specific to control/optimization
    INTEGER :: GLOBAL_ITER, ITMAX, numFwdSolnFiles
    INTEGER :: nSubActuators, nSubCtrlTarget
    INTEGER :: ctrlStartIter, ctrlEndIter
    REAL(rfreal) :: ctrlStartTime, ctrlEndTime
    TYPE(t_sub_actuator), POINTER :: subActuator(:)
    TYPE(t_sub_ctrl_target), POINTER :: subCtrlTarget(:)

    ! ... to get pointwise time history, JKim 12/2008
    INTEGER :: numProbeLocal, & ! ... number of probes within this processor
               numProbeGlobal   ! ... total number of probes over all processors
    TYPE(t_probe), POINTER :: probe(:)

    ! ... thermomechanical
    INTEGER :: TMWorker, TMRank, TMComm, TMColor
    INTEGER :: nTMGridsGlobal, nTMGrids, nTMPatches
    INTEGER, POINTER :: AllTMGridsGlobalSize(:,:), TMgridToProcMap(:,:)
    INTEGER, POINTER :: iTMRegionGlobal(:), iTMGridGlobal, TMgrid_to_processor(:), TMgrid_to_processor_coreComm(:)
    INTEGER :: TMnum_coreRank_this_processor
    INTEGER :: TMRank_inCoreComm, numTMproc_inCoreComm, TMcoreComm

    ! ... fluid-thermal-structural interaction
    Integer :: nInteractingPatches

    ! ... surface over which to integrate solution
    Integer :: nIntegralSurfaces, nIntegralSamples
    REAL(rfreal), POINTER :: PowerInOut(:,:), IntegralTime(:) 



  END TYPE t_region

  TYPE t_level
    TYPE(t_region), DIMENSION(:), POINTER :: regions
  END TYPE t_level

  !-----------------------------------------------------------------------
  ! .... Combustion dependencies

#ifdef HAVE_CANTERA
  TYPE t_canteraInterface
     !cantera variables
     real(rfreal) :: muref, lambdaref, alpharef
     integer :: CkR=1,CkF=1,CkO=1,CkN=1,CkW=1
     real(rfreal), pointer :: mfrac(:), diff(:)
     integer, pointer :: X2C(:) !mapping
     real(rfreal),pointer, dimension(:) :: molW
     type(phase_t) :: Cgas
  end type t_canteraInterface
#endif

 TYPE t_onestep

     Integer :: numSpecies              ! ... Number of species
     INTEGER :: iH2                     ! ... Index for H2
     INTEGER :: iO2                     ! ... Index for O2
     INTEGER :: iN2                     ! ... Index for N2
     REAL(RFREAL), POINTER :: sCoef(:)  ! ... stoichiometric coefficients
     REAL(RFREAL) :: sRatio             ! ... Mass stoichiometric ratio
     REAL(RFREAL) :: Da                 ! ... Damkohler number
     REAL(RFREAL) :: Zeldovich          ! ... Zel'dovich factor (non-dim activation energy)
     REAL(RFREAL) :: hRelease           ! ... Heat release parameter
     REAL(RFREAL) :: YO0                ! ... Oxidizer mass fraction in fuel stream
     REAL(RFREAL) :: YF0                ! ... Fuel mass fraction in fuel stream
     REAL(RFREAL) :: YFs                ! ... Stoichiometric fuel mass fraction
     
  END TYPE t_onestep

  TYPE t_model0

#ifdef HAVE_CANTERA
     Type (t_canteraInterface), pointer :: canteraInterface
#endif
     Type (t_jetCrossFlow), pointer :: jetCrossFlow

     integer :: numSpecies, numReactions, iH2,iO2,iH,iH2O,iN2
     real(rfreal), pointer,dimension(:)  :: E,theta, b, Da,Qg 
     real(rfreal), pointer, dimension(:,:) :: order !d log(k_i)/ d log(Y_j)
     real(rfreal), pointer, dimension(:,:) :: massMatrix
     real(rfreal), pointer, dimension(:,:) :: thirdBodyEff !third body efficiencies
     logical, pointer, dimension(:) :: is3BR
     real(rfreal), pointer,dimension(:)  :: conc, tBconc, omega, kr
     real(rfreal) :: momentumScaling, radicalSourceScaling, energySourceScaling, DBDTimeRef
     real(rfreal), pointer :: MwCoeff(:)
     real(rfreal) :: MwCoeffRho
  
     real(rfreal) :: Qg1=-216269086.94209d0! J/kg assigned based on thermodynamics

     real(rfreal) :: gamma
     
     real(rfreal) :: radicalYield = 0d0  !radical production in diffusion flame
     real(rfreal) :: oxygenFuelMassRatio = 8.0d0  !
     real(rfreal) :: nitrogenOxygenMoleRatio = 3.76d0  !
     real(rfreal) :: YO          !mass fraction of oxigen in air
     
     real(rfreal) :: T0 = 298d0    !Kelvin  (fresh)
     real(rfreal) :: Tf = 2.2d3  !Kelvin   (flame)

     logical :: isUnreactive = .false.
     logical :: isConstTV = .false.
     logical :: isInviscid=.false.  
     logical :: isChemistryInitialized = .false.
     logical :: clipSpecies = .true.
     
     real(rfreal) :: dimVelFactor, dimTempFactor, dimDensFactor, dimMomFactor
     real(rfreal),pointer, dimension(:) :: massDens2Conc
     real(rfreal),pointer, dimension(:) :: dimDividers
     
  END TYPE t_model0

   TYPE t_model1

#ifdef HAVE_CANTERA
     Type (t_canteraInterface), pointer :: canteraInterface
#endif
     Type (t_jetCrossFlow), pointer :: jetCrossFlow

     integer :: numSpecies, numReactions, iH2,iO2,iH,iH2O,iN2,nspecies
     real(rfreal), pointer, dimension(:,:) :: order !d log(k_i)/ d log(Y_j)
     real(rfreal) :: momentumScaling, radicalSourceScaling, energySourceScaling, DBDTimeRef, dimPresFactor
     real(rfreal), pointer :: MwCoeff(:)
     real(rfreal) :: MwCoeffRho
     
     real(rfreal) :: radicalYield = 0d0  !radical production in diffusion flame
     real(rfreal) :: oxygenFuelMassRatio = 8.0d0  !
     real(rfreal) :: nitrogenOxygenMoleRatio = 3.76d0  !
     real(rfreal) :: YO          !mass fraction of oxigen in air
     
     real(rfreal) :: T0 = 298d0    !Kelvin  (fresh)
     real(rfreal) :: Tf = 2.2d3  !Kelvin   (flame)

     logical :: isUnreactive = .false.
     logical :: isConstTV = .false.
     logical :: isInviscid=.false.  
     logical :: isChemistryInitialized = .false.
     logical :: clipSpecies = .true.
     
     real(rfreal) :: dimVelFactor, dimTempFactor, dimDensFactor, dimMomFactor
     real(rfreal),pointer, dimension(:) :: massDens2Conc
     real(rfreal),pointer, dimension(:) :: dimDividers

     integer :: jO=-1,jOH=-1,jH2O2=-1,isteadyB,isteadyE
     integer,pointer :: partners(:,:)
     real(rfreal), pointer,dimension(:)  :: E,theta, b, Da,Qg, kr, conc, omega, tBconc, AK, AJ, Rcoeff,AmatL
     type(t_kF0),  pointer, dimension(:)  :: k0
     type(t_NASApolynomials),  pointer, dimension(:)  :: polynomials
     real(rfreal), pointer, dimension(:,:) :: products
     real(rfreal), pointer, dimension(:,:) :: massMatrix, AmatK,AmatJ,Amat
     real(rfreal), pointer, dimension(:,:) :: thirdBodyEff !third body efficiencies
     logical, pointer, dimension(:) :: is3BR, isFO
     character(len=10),dimension(:),pointer :: speciesArray

     real(rfreal) :: Qg1=-217986263d0! J/kg assigned based on thermodynamics

     real(rfreal) :: gamma
     
  END TYPE t_model1

  ! ---------------------------------------------------------
  ! ... Combustion module
  TYPE t_combustion

     ! Chemistry models
     Type(t_onestep), pointer :: onestep
     Type(t_model0), pointer :: model0
     Type(t_model1), pointer :: model1

     ! Molecular weights used in ideal gas mixture ModEOS & ModNavierStokesBC
     real(rfreal), pointer :: MwCoeff(:)
     real(rfreal) :: MwCoeffRho

     integer :: iRKstage = 1

  END TYPE t_combustion

 TYPE t_jetCrossFlow
     real(rfreal) :: injectionVelocity = 1.0d0, crossVelocity=15.0d0 !m/s
     real(rfreal) :: MachScale = 0.3d0
     !air quantities at Pref, Tref
    real(rfreal) :: airSndSpdRef=300.0d0,airmuref,airRref !m/s
     !je quantities at Pref, Tref
    real(rfreal) :: jetSndSpdRef=300.0d0, jetmuref, jetRref !m/s
  end type t_jetCrossFlow

  TYPE t_kF0
     real(rfreal) :: E,b,Da, theta
     real(rfreal), pointer,dimension(:)  ::FallOff
  end type t_kF0
 TYPE t_NASApolynomials
     real(rfreal),pointer :: coeffs(:,:)
     real(rfreal),pointer :: cMod(:,:)
     real(rfreal),pointer :: cH(:,:)
     real(rfreal),pointer :: cp(:,:)
     real(rfreal) :: Delta
  end type t_NASApolynomials

  TYPE t_mixt_input

    !> caseID documentation
    !> Can I do this here as well?
    CHARACTER(LEN=80) :: caseID        !> @ingroup inputVariables @var caseID description

    INTEGER :: flowModel, nstepi, nstepmax, maxsubits, ND, gridType, metricType, advectionType, Sub1, massDiffusivityIndex, heatDiffusivityIndex, chemistry_model
    LOGICAL :: includeElectric, chemModelConvert
    LOGICAL :: externalBC, filter, sat_art_diss
    INTEGER :: moveGrid, nDv, nTv, nGv, nCv, nGrad, indCp, indMol, parDir, tvCor_in_dt, parallelTopology, nScalars, nAuxVars, nAuxVarsSteady, motionType    
    REAL(RFREAL) :: prLam, scnLam, RE, PR, periodicL(3), SC, X1,X2
    CHARACTER(LEN=80) :: grid_fname, bc_fname, target_fname, decompMap, mean_fname, outputDirectory
    CHARACTER(LEN=80) :: combustion_model_fname, momentum_fname, eMag_fname,combustion_model_mix
    CHARACTER(LEN=80) :: xintout_ho_fname, xintout_x_fname, xintout_ogen_fname
    LOGICAL :: xintout_has_iblank
    INTEGER :: periodic(3), noutput, cfl_mode, nOverLap, nrestart, readTargetOnStartup, output_mode, useHDF5
    INTEGER :: useLowMemIO, nOverlapOverride, useDecompMap, fixBelleroOPeriodic
    INTEGER :: periodicStorage, useLocalDecomp, useMPIIO
    INTEGER :: belleroOPeriodicOverlap, use_lowmem_operator_setup, OgenOrder, UseTFI
    INTEGER :: write_xyz, write_cv, write_dv, write_cvt, write_met, write_jac, write_mid, write_rhs, write_targ
    INTEGER :: write_nothing, read_nothing
#ifdef BUILD_ELECTRIC
    INTEGER :: write_phi
#endif
    INTEGER, POINTER :: bc_data(:,:)

    ! ... data for grid generation
    INTEGER           :: generateGrid
    CHARACTER(LEN=80) :: geometryFileName  !> @ingroup inputVariables @var geometryFileName description

    ! old grid generation parameters (delete later)
    CHARACTER(LEN=80) :: geometryName      !> @ingroup inputVariables @var geometryName description
    INTEGER           :: nxGenGrid, nyGenGrid, nzGenGrid, numGenGrid
    REAL(RFREAL)      :: xminGenGrid, xmaxGenGrid
    REAL(RFREAL)      :: yminGenGrid, ymaxGenGrid
    REAL(RFREAL)      :: zminGenGrid, zmaxGenGrid

    ! ... selective frequency damping
    INTEGER :: SelectiveFrequencyDamping
    REAL(RFREAL) :: SFD_X, SFD_D

    ! ... local preconditioning for low Mach number stuff
    INTEGER :: USE_LOCAL_LOW_MACH_PRECONDITIONER
    REAL(RFREAL) :: LOCAL_LOW_MACH_PRECONDITIONER_PARAM_ALPHA

    ! ... output the infinity-norm of the residual
    INTEGER :: RESIDUAL_INF_NORM_CALCULATE
    INTEGER :: RESIDUAL_INF_NORM_OUTPUT_FILE
    CHARACTER(LEN=80) :: RESIDUAL_INF_NORM_OUTPUT_FILENAME
    INTEGER :: RESIDUAL_INF_NORM_STOP

    ! - Instability wave
    COMPLEX(KIND=RFREAL), Pointer :: Qp_hat(:,:,:,:,:), freq_pert(:,:), alpha_pert(:,:,:)
    REAL(RFREAL), Pointer :: beta_pert(:,:), amp_pert(:,:), phase_pert(:,:), x_pert(:)
    INTEGER :: BCIC_EIGENFUNCTION, N_mode, M_Mode, bcic_eigenfunction_imax
    CHARACTER(LEN=80) :: BCIC_EIGENFUNCTION_FNAME

    ! - Interpolation between grids - Rocsmash
    INTEGER :: interpType, fringe, bcfringe, volumeIntersection, boundaryIntersection, bgGridGlobalId
    INTEGER :: useNativeSBI

    ! - numerics
    INTEGER :: spaceDiscr, spaceOrder, interpOrder, filterDiscr
    INTEGER :: timeScheme, nrkSteps, nrkLevels, sponge_pow
    REAL(RFREAL) :: cfl, dt, sponge_amp, t_output
    LOGICAL :: bndry_filter(2)
    REAL(RFREAL) :: filter_alphaf, bndry_filter_alphaf, filter_fac, approximate_linear_operator_epsilon
    INTEGER :: approximate_linear_operator_power_method_steps

    ! Shock-capturing scheme input parameters, JKim 06/2007
    INTEGER :: shock ! Types of shock-capturing scheme
                     ! 0 : No shock-capturing
                     ! 1 : Cook & Cabot (JCP 2005) and Cook (PoF 2007)
                     ! 2 : Kawai & Lele (JCP 2008)
                     ! 3 : Bogey et al. (JCP 2009)
                     ! 4 : Mani et al. (JCP 2009)
                     ! 5 : Kawai et al. (AIAA P 2009, JCP 2010)
    INTEGER :: ArtPropDrvOrder ! Derivative order of artificial property (should be EVEN)
                               ! Proposed value by Cook (Pof 2007) is 4.
                               ! Optimal value of r considering spatial order of accuracy should be found.
    INTEGER :: nArtProp ! Number of artificial property:
                        ! 1 for only artificial viscosity (Cook & Cabot, JCP 2005)
                        ! 2 for both artificial viscosity and thermal conductivity (Cook, PoF 2007)
                        ! 3 for all artificial shear & bulk viscosities and thermal conductivity (Kawai et al., AIAA P 2009 & JCP 2010)
    REAL(RFREAL) :: HyperCmu,HyperCbeta,HyperCKappa ! HyperCmu: C_mu for artificial shear viscosity
                                                    ! HyperCbeta: C_beta for artificial bulk viscosity
                                                    ! HyperCKappa: C_kappa for artificial thermal conductivity
                                                    ! See Cook (PoF 2007) for their real names :)
    LOGICAL :: shock_clip ! ... .TRUE. for clipping hyperviscosities to the specified maximum
    REAL(RFREAL) :: MaxHyperMu, MaxHyperBeta, MaxHyperKappa ! ... maximum values of hyperviscosities w/o Re-factor
                                                            ! ... default values from inviscid Shu-Osher problem
    REAL(RFREAL) :: ShockFilterRTH ! cut-off value of Bogey et al shock filtering

    ! Large-Eddy Simulation input parameters, JKim 11/2007
    INTEGER :: LES ! Types of LES model
                   ! 0: DNS
                   ! 101: Classical Smagorinsky model (not implemented; just for complete-ness)
                   ! 201: Dynamic model of Moin et al. with Lilly and Yoshizawa's improvement.
                   !      Test filter from prof Bodony's thesis, p168.
                   ! 202: Dynamic model of Moin et al. with Lilly and Yoshizawa's improvement.
                   !      Test filter from Spyropoulos & Blaisdell (AIAA J 1996).
                   !      See Table 2, p996: 7-pt explicit, least-square filter, JKim 12/2007
                   ! 203: Dynamic model of Moin et al. with Lilly and Yoshizawa's improvement.
                   !      Test filter from Spyropoulos & Blaisdell (AIAA J 1996).
                   !      See Table 2, p996: 5-pt implicit, 7-pt explicit, compromise, JKim 12/2007
                   ! 204: Dynamic model of Moin et al. with Lilly and Yoshizawa's improvement.
                   !      Test filter (TFo15p-pi/2) from Bogey & Bailly (JCP 2004)
                   ! Germano-type dynamic LES model; main references are 
                   !     Moin, Squires, Cabot, and Lee (PoF 1991)
                   !     Lilly (PoF 1991)
                   !     Spyropoulos & Blaisdell (AIAA J 1996)
                   ! For implementation in generalized coordinate, see
                   !     Rizzetta, Visbal, and Blaisdell (Int. J for Num. Methods in Fluids, 2003)
                   ! Reading
                   !   Nagarajan, Lele, and Ferziger (JCP 2003)
                   ! is recommended as well, especially Appendix A.
                   ! JKim 09/2007
    CHARACTER(LEN=80) :: LESAvgDir
    REAL(RFREAL) :: LES_RatioFilterWidth ! The only free parameter of dynamic LES model
                                         ! This is designated as alpha in Moin et al. and
                                         ! alpha of 2 is suggested by Germano et al. (PoF 1991).
    LOGICAL :: LES_ONCE ! If it's .TRUE., LES model is computed just once at the 
                        ! 1st RK stage and its model coefficients are stored and 
                        ! reused, JKim 03/2009
    LOGICAL :: LES_CLIP ! If it's .TRUE., LES model coefficients C, C_I, Pr_T are 
                        ! not allowed to be greater than the upper limits 
                        ! specified by the following values, JKim 03/2009
    REAL(RFREAL) :: LES_CLIP_MAX_C, LES_CLIP_MAX_CI, LES_CLIP_MAX_PrT

    ! Input parameters for filtering, JKim 09/2007
    INTEGER :: numFilter   ! to control global number of filter, JKim 09/2007
    INTEGER :: iFilter     ! denote index of filter filtering conserved variables every time step
    INTEGER :: filt_freq   ! Filter frequency, JByun 03/2014
    INTEGER :: iFilterTest ! denote index of test filter for dynamic LES model
    INTEGER :: iFilterGaussianHyperviscosity ! ... denote index of truncated Gaussian filter for hyperviscosity shock capturing
    INTEGER :: iArtDiss    ! index of non-SAT-based artificial dissipation
    INTEGER :: iSATArtDiss ! index of SAT-based artificial dissipation
    INTEGER :: iSATArtDissSplit ! 2nd index needed for split-form of SAT artificial dissipation
    INTEGER :: iFirstDeriv, iFirstDerivBiased ! index for first-derivative operator
    INTEGER :: iSecondDeriv ! index for second-derivative operator
    INTEGER :: iFourthDeriv ! index for fourth-derivative operator
    INTEGER :: iFilterShockTG ! index for `Truncated Gaussian' filter used by Cook & Cabot
    INTEGER :: iShockFilterDetect ! index for shock detection operator
    INTEGER :: iShockFilterFluxForward ! index for 'optimized 2nd order filter' by Bogey et al.
    INTEGER :: iShockFilterFluxBackward ! index for 'optimized 2nd order filter' by Bogey et al.
    INTEGER :: iShockFilterCoeffForward ! index for 'optimized 2nd order filter' by Bogey et al. (filter coeff)
    INTEGER :: iShockFilterCoeffBackward ! index for 'optimized 2nd order filter' by Bogey et al. (filter coeff)
    INTEGER :: iShockFilterSensorForward ! index for 'optimized 2nd order filter' by Bogey et al. (filter coeff)
    INTEGER :: iShockFilterSensorBackward ! index for 'optimized 2nd order filter' by Bogey et al. (filter coeff)
    INTEGER :: iFirstDerivImplicit ! index for first-derivative operator used for implicit time advancement
    INTEGER :: iSecondDerivImplicit ! index for second-derivative operator used for implicit time advancement
    INTEGER :: iForwardDiff ! basic foward difference x_{i+1} - x_{i}
    INTEGER :: iSecondDerivImplicitArtDiss ! SAT artificial dissipation for LHS
    INTEGER :: Implicit_SAT_Art_Diss
    REAL(RFREAL) :: Amount_Implicit_SAT_Art_Diss
    INTEGER :: LHS_Filter, iLHS_Filter
    REAL(RFREAL) :: LHS_Filter_Fac, LHS_Filter_AlphaF

    ! Basic parameters for IC or BC data
    INTEGER :: bcic_planewave, outputBC_asIBLANK
    REAL(RFREAL) :: bcic_frequency, bcic_amplitude
    REAL(RFREAL) :: bcic_angle_phi, bcic_angle_theta
    REAL(RFREAL) :: bcic_WallTemp, bcic_wall_vel_x, bcic_wall_vel_y, bcic_wall_vel_z

    ! ... Acoustic source paraemeters
    INTEGER :: ACOUSTIC_SOURCE, ACOUSTIC_SOURCE_NFREQ 
    REAL(RFREAL), Dimension(:), Pointer :: ACOUSTIC_SOURCE_FREQ, ACOUSTIC_SOURCE_AMP, ACOUSTIC_SOURCE_PHASE
    Real(RFREAL) :: ACOUSTIC_SOURCE_X0(MAX_ND), ACOUSTIC_SOURCE_LX(MAX_ND)

    ! Finite-volume parameters, DBuchta 06/2009
    INTEGER :: fv_grid(50), fv_recon, fv_flux
    CHARACTER(LEN=80) :: fv_grid_array
    INTEGER :: fv_periodic(3)

    ! Input parameters for volumetric drag
    INTEGER :: volumetric_drag
    REAL(RFREAL) :: volumetric_drag_coefficient

    ! ... Flow initialization params
    CHARACTER(LEN=80) :: initflow_name
    REAL(RFREAL) :: initflow_theta, initflow_phi, initflow_radius
    REAL(RFREAL) :: initflow_xloc, initflow_yloc, initflow_zloc
    REAL(RFREAL) :: initflow_xwidth, initflow_ywidth, initflow_zwidth
    REAL(RFREAL) :: initflow_amplitude, initflow_frequency, initflow_mach, initflow_rpm, initflow_tstart
    REAL(RFREAL) :: initflow_delta, initflow_tramp, initflow_deltastar

    ! ... Generalization to multiple fluid models
    INTEGER :: fluidModel

    ! ---------------------------------------------------------
    ! ... derivative & filter stencils
    INTEGER      :: overlap(MAX_OPERATORS), operator_type(MAX_OPERATORS)
    INTEGER      :: max_operator_width(MAX_OPERATORS), operator_bndry_width(MAX_OPERATORS)
    INTEGER      :: operator_bndry_depth(MAX_OPERATORS)
    real(rfreal) :: lhs_interior(MAX_OPERATORS,-2:2)
    real(rfreal) :: rhs_interior(MAX_OPERATORS,-MAX_INTERIOR_STENCIL_WIDTH:MAX_INTERIOR_STENCIL_WIDTH)
    real(rfreal) :: rhs_block1(MAX_OPERATORS,20,12),   rhs_block2(MAX_OPERATORS,20,12)
    real(rfreal) :: lhs_block1(MAX_OPERATORS,20,-2:2), lhs_block2(MAX_OPERATORS,20,-2:2)
    real(rfreal) :: SBP_Pdiag(8), SBP_invPdiag(8), SBP_invPdiag_block1(8), SBP_invPdiag_block2(8)
    real(rfreal) :: bndry_explicit_deriv(12), amount_SAT_diss, SAT_sigmaI1, SAT_sigmaI2
    real(rfreal) :: SAT_sigmaI1_FF, SAT_sigmaI2_FF, SAT_sigmaI1_BI, SAT_sigmaI2_BI
    INTEGER      :: rhs_interior_stencil_start(MAX_OPERATORS), rhs_interior_stencil_end(MAX_OPERATORS)
    INTEGER      :: max_stencil_start(MAX_OPERATORS), max_stencil_end(MAX_OPERATORS)
    INTEGER      :: lhs_interior_stencil_start(MAX_OPERATORS), lhs_interior_stencil_end(MAX_OPERATORS)
    INTEGER      :: fix_stencil_cannot_fit
    INTEGER      :: operator_implicit_solver
    ! ---------------------------------------------------------

    ! - Adjoint N-S
    LOGICAL :: AdjNS
    LOGICAL :: AdjNS_ReadNSSoln ! ... if .TRUE., interpolate state%cv from existing *.q files
    CHARACTER(LEN=80) :: AdjNS_SolnList ! ... a text file book-keeping *.q files and solution time

    ! - conjugate gradient optimization
    LOGICAL :: AdjOptim ! ... if .TRUE., perform conjugate gradient optimization
                        ! ... note that input%AdjNS initially set to be .FALSE. then
    INTEGER :: AdjOptim_typeOfOptim ! ... types of optimization: 
                                    ! ... 1 for quadratic functional of control variable;
                                    ! ...       therefore exact with known minimum
                                    ! ... 2 for quartic functional of control variable; 
                                    ! ...       still minimum is known though
                                    ! ... 3 for anti-sound cancellation in quiet flow
                                    ! ...       N-S and adjoint N-S are solved
                                    ! ... 4 for anti-sound cancellation across 2-D mixing layer
                                    ! ...       N-S and adjoint N-S are solved
                                    ! ... 5 for anti-sound cancellation of 3-D M=1.3 cold jet
                                    ! ...       N-S and adjoint N-S are solved
                                    ! ... 6 for flow control of 3-D M=1.3 cold jet
                                    ! ...       N-S and adjoint N-S are solved
    INTEGER :: AdjOptim_CGMaxIt     ! ... Maximum iterations for CG
    INTEGER :: AdjOptim_DBrentMaxIt ! ... Maximum iterations for NR DBrent
    LOGICAL :: AdjOptim_Restart ! ... if .TRUE., existing restart files for optimization are read
    INTEGER :: AdjOptim_Func ! ... FUNCTIONAL_SOUND, cost in terms of pressure fluctuation in time domain
    CHARACTER(80) :: AdjOptim_mean ! ... when a functional is defined by fluctuations, specify a file with time-mean
    INTEGER :: AdjOptim_CtrlType ! ... control source type (see Table 1 of Wei & Freund, 2005)
    INTEGER :: AdjOptim_Constraint ! ... whether to do constrained optimization or not
    INTEGER :: AdjOptim_axis_of_rot ! ... for axisymmetric geometry, specify the axis of rotation
    REAL(rfreal) :: AdjOptim_eps ! ... epsilon for cost functional
    REAL(rfreal) :: AdjOptim_deps ! ... epsilon for difference in cost functional between 
                                  ! ... two consecutive iterations
    REAL(rfreal) :: AdjOptim_dbrent_eps ! ... epsilon for dbrent line-search refinement
    REAL(rfreal) :: AdjOptim_initAlpha ! ... initial guess for generalized distance in 
                                       ! ... control variable, phi space
    
    ! pointwise time history
    LOGICAL :: GetTrace
    INTEGER :: numProbeGlobal, sampleRate
    CHARACTER(80) :: probe_fname

    ! inflow forcing using the linear stability theory in an axisymmetric domain
    INTEGER :: LST_num_mode_temporal, LST_num_mode_azimuthal
    REAL(RFREAL) :: LST_amp, LST_x0
    CHARACTER(80) :: LST_fname

    ! ... GCL enforcement
    INTEGER :: useMetricIdentities

    ! ... Free stream dimensional parameters for coupled simulation with
    ! ... dimensional solvers
    REAL(RFREAL) :: TempRef, PresRef, LengRef, GamRef, SndSpdRef, DensRef, gasConstantRef, TimeRef, machFactor =1.0d0 
    REAL(RFREAL) :: muRef, lambdaRef, kappaRef, EintRef, Cpref, Cvref, UnivGasConstant
    REAL(RFREAL) :: plasmaDensityRef
    REAL(RFREAL) :: Vamplitude, Vphase

    ! ... temporal coupling configuration
    INTEGER :: QuasiSS
    REAL(RFREAL) :: ConvCrit

    ! ... GMRES options
    INTEGER :: numGMRESInnerIters, numGMRESRestarts, KDim, EuclidLevel
    INTEGER, POINTER :: HYPREStencilIndex(:,:,:,:) ! mapping needed for SStruct stencils in HYPRE
    REAL(RFREAL) :: GMRESConvThresh

    ! ... Dual-time step parameters
    REAL(RFREAL) :: DTauDtRatio, DTauDecreaseFac, Lu, MachInf, DTauIncFac
    INTEGER :: ImplicitSteadyState, DTauIncThresh,DivThresh, ImplicitTrueSteadyState, Converged

    ! ... Parameters to pass to hypre.c
    ! ... All parameters packed into arrays to minimize number of arguments
    ! ... Reals -- 1: GMRESConvThresh, 2: ParaSailsThresh, 3: ParaSailsFilter
    REAL(RFREAL), DIMENSION(3) :: HypreParamsReal
    ! ... Integers -- 1: Preconditioner Number, 2:numGMRESIters, 3:KrylovDim
    ! ... Integers -- 4: EuclidLevel, 5:BlockJacobi(1)or PILU(0)
    ! ... Integers -- 6: ParaSailsNlevels, 7:GMRESPrintLevel, 8: PreCondPrint
    INTEGER, DIMENSION(8) :: HypreParamsInt
    ! ... Preconditioners:
    ! ... 0) None
    ! ... 1) Euclid
    ! ... 2) ParaSails
    ! ... 3) BoomerAMG
    ! ... 4) Jacobi
    INTEGER :: writeSubItData

    ! ... For data bounds checking - turned off by default
    INTEGER :: CheckingLevel
    LOGICAL :: FailOnDataError
    REAL(RFREAL) :: DataLimit_LOWER_RHO, DataLimit_UPPER_RHO, DataLimit_SPEED 
    REAL(RFREAL) :: DataLimit_LOWER_ENERGY, DataLimit_UPPER_ENERGY

    ! ... gas models
    INTEGER :: gas_tv_model, gas_dv_model, nTPTableEntries, plasmaModel
    CHARACTER(len=80) :: TPTableName, DVTableName, TVTableName, BolsigTableName
    REAL(RFREAL), POINTER :: TPCoeffs(:,:), TPTable(:,:,:) 

    ! ... post processing
    INTEGER :: PPTau, PPYplus, PPHtFlux, PPrhs, PPTemp, PPReAnlg, PPPressure, PPVort, PPDilat
    INTEGER :: usePP, PP_Case, startIter, stopIter, skipIter

    ! ...on-the-fly statistics
    INTEGER :: use_stats,stat_nvar,stat_grid,stat_noutput,stat_symdir1,stat_symdir2,stat_symdir3

    ! ... plasma actuators
    INTEGER :: use_plasma_actuator, use_momentum_actuator, use_radical_actuator
    REAL(RFREAL) :: actuator_loc_x, actuator_loc_y, actuator_loc_z
    REAL(RFREAL) :: actuator_radius, actuator_length, actuator_efficiency
    REAL(RFREAL) :: actuator_max_power, actuator_duty_cycle, actuator_frequency, actuator_delay
    REAL(RFREAL) :: actuator_rise_time, actuator_fall_time, actuator_sigma_xy, actuator_sigma_z
    REAL(RFREAL) :: momentum_frequency,  momentumScaling, Radical_rise_time, Radical_duty_halfcycle, Radical_delay, electric_start_time

    ! ... laser ignition and vorticity
    Integer :: UseLaser      ! ... Laser ignition source
    REAL(RFREAL) :: laser_loc_x, laser_loc_y, laser_loc_z
    REAL(RFREAL) :: laser_radius, laser_length
    REAL(RFREAL) :: laser_absorbed_energy, laser_rise_time, laser_pulse_time, laser_start_time, laser_end_time
    REAL(RFREAL) :: laser_shock_Mach, laser_shock_width

    ! ... gravity
    REAL(RFREAL) :: Froude, gravity_angle_phi, gravity_angle_theta, invFroude

    ! ... thermomechanical solver
    INTEGER :: TMSolve, TMOnly, UseParMetis, TMrestart
    INTEGER :: numTMproc
    CHARACTER(len=80) :: TMgrid_fname, TMdecompMap, TMbc_fname, TMrestart_fname, TMrestartdot_fname
    INTEGER :: useTMdecompMap
    INTEGER :: TMparallelTopology, TMparDir
    INTEGER, POINTER :: TMbc_data(:,:)
    REAL(RFREAL), POINTER :: TMbc_Val(:,:)
    INTEGER :: ElemType
    INTEGER :: ConstModel

    ! ... initial conditions
    REAL(RFREAL) :: TMInitialTemp, TMInitialVelocity
    INTEGER :: TMInitialCondition, BeamMode

    ! ... special boundary conditions
    INTEGER :: PinnedBC

    ! ... thermomechanical temporal advancement
    INTEGER :: ThermalTimeScheme, StructuralTimeScheme
    INTEGER :: StructSolnFreq, ThermalSolnFreq

    ! ... coupled solver convergence
    INTEGER :: MaxTMSubits
    REAL(RFREAL) :: TMConvCrit

    ! ... petsc
    REAL(RFREAL) :: PetscRtol

    ! ... material properties
    REAL(RFREAL) :: YngMod          ! ... Youngs modulus (Pa)
    REAL(RFREAL) :: TMDensity       ! ... density (kg/m^3)
    REAL(RFREAL) :: PsnRto          ! ... Poisson's ratio
    REAL(RFREAL) :: SpecHt          ! ... specific heat capacity (J/kg/K)
    REAL(RFREAL) :: ThrmCond        ! ... thermal conductivity (W/m/K)
    REAL(RFREAL) :: alpha           ! ... thermal expansion coefficient

    ! ... temperature dependent mechanical properties
    Integer :: TempDpndMtl, nPrpPts, nTblPts
    Character(len=80) :: prp_fname
    Real(rfreal), pointer :: prp_data(:,:)

    ! ... solid data record
    Integer :: SaveSolidData

    ! ... RHS forcing for temporal simulation
    Integer :: TemporalRHSForcing
    Integer :: StrmDir, NrmDir

    ! ... EulerFV parameters
    Integer :: reconstruction_scheme

    ! Jet in crossflow parameters
    REAL(RFREAL) :: LASER_FP ! ... Laser focul point
    REAL(RFREAL) :: LASER_R  ! ... Laser radius
    REAL(RFREAL) :: LASER_L  ! ... Laser length
    REAL(RFREAL) :: XJET     ! ... Streamwise position of jet center
    REAL(RFREAL) :: GRID1_LZ ! ... Length in z without periodic overlap

    logical :: useActivityStandardPressure

    !Electric Field
    integer :: ef_solve_interval
    real(RFREAL) :: ef_voltage_scale
    real(RFREAL) :: ef_dielectric_permittivity
    real(RFREAL) :: ef_screening_length
    real(RFREAL) :: ef_plasma_potential
    integer :: ef_schwarz_iters
    logical :: ef_subset
    character(len=80) :: ef_subset_fname
  END TYPE t_mixt_input

  TYPE t_gengrid_uniform
    integer :: nx, ny, nz
    real(rfreal) :: xmin, xmax, ymin, ymax, zmin, zmax
  END TYPE t_gengrid_uniform

  TYPE t_gengrid_cylinder
    integer :: nr, nt, nz
    real(rfreal) :: x0, y0, rmin, rmax, tmin, tmax, zmin, zmax
    logical :: uniform_cell_aspect_ratio
  END TYPE t_gengrid_cylinder

  TYPE t_gengrid_iblank_range
    integer :: iblank_value
    integer :: is(MAX_ND), ie(MAX_ND)
  END TYPE t_gengrid_iblank_range

  TYPE t_gengrid_move
    real(rfreal) :: dx, dy, dz
  END TYPE t_gengrid_move

  TYPE t_gengrid_rotate
    real(rfreal) :: angle
    real(rfreal) :: x0, y0, z0
    real(rfreal) :: nx, ny, nz
  END TYPE t_gengrid_rotate

  TYPE t_gengrid_scale
    real(rfreal) :: x0, y0, z0
    real(rfreal) :: sx, sy, sz
  END TYPE t_gengrid_scale

  TYPE t_gengrid
    integer :: numGrids
    integer, allocatable :: gridType(:)
    integer, allocatable :: gridParamIndex(:)
    type(t_gengrid_uniform), allocatable :: params_uniform(:)
    type(t_gengrid_cylinder), allocatable :: params_cylinder(:)
    integer, allocatable :: numTransforms(:)
    integer :: maxTransforms
    integer, allocatable :: transformType(:,:)
    integer, allocatable :: transformParamIndex(:,:)
    type(t_gengrid_iblank_range), allocatable :: params_iblankRange(:)
    type(t_gengrid_move), allocatable :: params_move(:)
    type(t_gengrid_rotate), allocatable :: params_rotate(:)
    type(t_gengrid_scale), allocatable :: params_scale(:)
  END TYPE t_gengrid

  !-----------------------------------------------------------------------

    TYPE t_fvgrid
      REAL(RFREAL), POINTER :: vol(:)               ! ... cell volume
      TYPE(t_fvface), POINTER :: face(:)            ! ... face information in sweep direction: area, orientation
      INTEGER, POINTER :: nGhost(:,:)               ! ... number of ghost points in each direction
      INTEGER, POINTER :: periodic(:), is(:), ie(:) 
      INTEGER, POINTER :: Nc(:), Nf(:)              ! ... number of cells, faces in each direction
      INTEGER :: nCells                             ! ... number of cells in the domain
    END TYPE t_fvgrid

    TYPE t_fvface
       REAL(RFREAL), POINTER :: normal_vec(:,:)    ! ... normal vector in the sweep direction
       REAL(RFREAL), POINTER :: tangent_vec(:,:,:) ! ... tangent vector(s) for the corresponding sweep direction
       REAL(RFREAL), POINTER :: area(:)            ! ... face area in the sweep direction
       INTEGER, POINTER :: LRindex(:,:)            ! ... index of data points on left and right of face
    END TYPE t_fvface
  !-----------------------------------------------------------------------

  TYPE t_epatch
#ifdef BUILD_ELECTRIC
     REAL(C_DOUBLE), ALLOCATABLE :: dirichlet(:)
     REAL(RFREAL), POINTER :: charge(:), chargeOld(:), chargeRHS(:,:), chargeRHSF(:,:), chargeStageRHS(:,:)
     INTEGER(C_INT) :: is(3), ie(3)
     INTEGER(C_INT) :: normDir, bcType
#endif
  END TYPE t_epatch
    
    TYPE t_electric
       LOGICAL :: grid_has_efield
       REAL(RFREAL), POINTER :: eField(:,:),momentumTerm(:,:), eMag(:,:), laserEnergy(:,:)
       REAL(RFREAL) :: eps, startTime, exposedVoltage_Lref,exposedVoltage, dtAmat
       TYPE(t_spline), POINTER :: BolsigSplines(:)
#ifdef BUILD_ELECTRIC
       REAL(C_DOUBLE), POINTER :: bcoef(:)
       REAL(C_DOUBLE), POINTER ::  phi(:), dcoef(:), jump(:), rhs(:)
       TYPE(C_PTR) :: solver
       REAL(RFREAL), POINTER :: Mv(:,:), Te(:,:)
       logical,pointer :: isIon(:)
       REAL(RFREAL), POINTER :: phi_data_ptr(:,:)
       REAL(RFREAL), POINTER :: gradphi(:,:)
#ifdef AXISYMMETRIC
       REAL(RFREAL), POINTER :: gradphi_pole(:,:)
#endif
       REAL(RFREAL), POINTER :: phi_interp(:,:)
       logical, pointer :: interp_mask(:)
       LOGICAL :: whole_grid
       LOGICAL :: rank_has_efield
       INTEGER :: subset_ND
       INTEGER, DIMENSION(MAX_ND) :: subset_dirs
       INTEGER, DIMENSION(MAX_ND) :: subset_start, subset_end ! global start and end (ordered by grid dirs)
       LOGICAL :: subset_axisymmetric
       REAL(RFREAL), DIMENSION(MAX_ND) :: subset_axisymmetric_origin
       REAL(RFREAL), DIMENSION(MAX_ND) :: subset_axisymmetric_axis
       INTEGER, DIMENSION(MAX_ND) :: subset_is, subset_ie ! local start and end (ordered by subset dirs)
       INTEGER :: subset_nCells
       INTEGER :: subset_comm
       INTEGER :: subset_rank
       INTEGER, DIMENSION(MAX_ND) :: subset_cartCoords
       INTEGER, DIMENSION(MAX_ND) :: subset_cartDims
       INTEGER, POINTER :: subset_to_whole(:)
       REAL(C_DOUBLE), POINTER :: xyz_subset(:,:)
       REAL(C_DOUBLE), POINTER :: phi_subset(:), bcoef_subset(:), dcoef_subset(:), jump_subset(:), rhs_subset(:)
#endif
    END type t_electric

  TYPE t_mixt

    INTEGER :: NVARS, ND, tcnt, sampler
    REAL(RFREAL) :: RE, REinv, PR, PRinv, SC, SCinv, xshock
    REAL(RFREAL) :: DTAU, DTAUold, InitialRHS, CurrentRHSMax, AveHtFluxOld, t_output_next
    REAL(RFREAL), POINTER :: time(:), dt(:), timeOld(:), cfl(:)
    REAL(RFREAL), POINTER :: cv(:,:), cvOld(:,:), dv(:,:), tv(:,:), gv(:,:), cvOld2(:,:), cvOld1(:,:)
    REAL(RFREAL), POINTER :: rhs(:,:), rk_rhs(:,:,:), cvTarget(:,:), cvTargetOld(:,:), auxVarsTargetOld(:,:), RHSz(:,:), auxVarsSteady(:,:), MolWeight(:)
    REAL(RFREAL), POINTER :: VelGrad1st(:,:), TempGrad1st(:,:), MagStrnRt(:), tvCor(:,:)
    REAL(RFREAL), POINTER :: flux(:), dflux(:), muT(:), PrT(:), SGS_KE(:)
    REAL(RFREAL) :: MaxHyperMu, MaxHyperBeta, MaxHyperKappa ! ... maximum hyperviscosity at each time step
    REAL(RFREAL), POINTER :: auxVars(:,:), auxVarsOld(:,:)
    REAL(RFREAL), POINTER :: rhs_AuxVars(:,:), rk_rhs_AuxVars(:,:,:), jac_AuxVars(:,:,:)
    REAL(RFREAL), POINTER :: levelSet(:), levelSetOld(:), rhs_levelSet(:), rk_rhs_levelSet(:,:)
    REAL(RFREAL), POINTER :: precond(:,:), auxVarsTarget(:,:), rhs_explicit(:,:,:), rhs_implicit(:,:,:)
    REAL(RFREAL), POINTER :: rhs_auxVars_explicit(:,:,:), rhs_auxVars_implicit(:,:,:)
    

    ! - Finite Volume
    TYPE(t_fvsweep), pointer :: sweep(:)

    ! - IO buffers
    REAL(RFREAL), POINTER :: DBUF_IO(:,:,:,:)
    INTEGER, POINTER :: IBUF_IO(:,:,:)

    ! - Adjoint N-S
    REAL(RFREAL), POINTER :: av(:,:), avOld(:,:)
    REAL(RFREAL), POINTER :: avTarget(:,:), avTargetOld(:,:)
    REAL(RFREAL), POINTER :: cvNew(:,:), auxNew(:,:), auxOld(:,:)
    INTEGER :: num_cvFiles, numCur_cvFile
    INTEGER, POINTER :: iter_cvFiles(:)
    REAL(RFREAL), POINTER :: time_cvFiles(:)

    ! - Combustion
    TYPE(t_combustion) :: combustion

    ! ... post-processing
    REAL(RFREAL), POINTER :: pp(:,:), Vort(:,:), Dilat(:)

    ! ... spline coefficients for EOS and transport variables
    TYPE(t_spline), POINTER :: dvSpline(:)   ! Cv(T), Cp(T), Gamma(T), Z(T), T(e_int) (in that order)
    TYPE(t_spline), POINTER :: tvSpline      ! mu(T), lambda(T), k(T)

    ! ... for using box shape data structure
    REAL(RFREAL), POINTER :: cv_unique(:,:)
    REAL(RFREAL), POINTER :: tv_unique(:,:)
    REAL(RFREAL), POINTER :: dv_unique(:,:)
    REAL(RFREAL), POINTER :: gv_unique(:,:)
    REAL(RFREAL), POINTER :: time_unique(:)
    REAL(RFREAL), POINTER :: cfl_unique(:)
    REAL(RFREAL), POINTER :: cvtarget_unique(:,:)

    REAL(RFREAL), POINTER :: cv_interp(:,:)
    REAL(RFREAL), POINTER :: auxVars_interp(:,:)

  END TYPE t_mixt
  
  TYPE t_fvsweep
     REAL(RFREAL), POINTER :: flux(:,:)
  END TYPE t_fvsweep

  !-----------------------------------------------------------------------

  TYPE t_grid

    ! ... grid-specific input
    Type (t_mixt_input), pointer :: input

    ! - MPI data
    INTEGER :: Comm, numProc_inComm, coreRank_inComm, myRank_inComm, CommWorld
    INTEGER :: numGridsInFile, gridType, parallelTopology
    INTEGER, POINTER :: gridToProcMap(:,:), AllGridsGlobalSize(:,:), GlobalSize(:)
    INTEGER :: cartComm, myrank_inCartComm, numproc_inCartComm, left_process(MAX_ND), right_process(MAX_ND)
    INTEGER :: box_nbs(26)
    INTEGER :: cartDims(MAX_ND), cartCoords(MAX_ND), periodicStorage, periodicStorageOffset
    REAL(RFREAL) :: periodicL(MAX_ND)
    INTEGER :: pencilComm(MAX_ND), myrank_inPencilComm(MAX_ND), numproc_inPencilComm(MAX_ND)

    ! - Basic grid quantities
    INTEGER :: indGs, nCells, N, moving, nGridsGlobal, gridID, metric_type, advection_type
    INTEGER :: parDir, ND, iGridGlobal
    INTEGER :: periodic(MAX_ND), is(MAX_ND), ie(MAX_ND), AvgDIR(MAX_ND)
    INTEGER, POINTER :: deriv_type(:), filter_type(:), remote_is(:,:), remote_ie(:,:)
    INTEGER, POINTER :: nGhostRHS(:,:), nGhostLHS(:,:)
    INTEGER :: operator_type(MAX_OPERATORS), max_operator_width(MAX_OPERATORS)
    INTEGER :: operator_bndry_width(MAX_OPERATORS), operator_bndry_depth(MAX_OPERATORS)
    INTEGER, allocatable :: vds(:), vec_ind(:,:,:)
    INTEGER, allocatable :: vdsGhost(:), vec_indGhost(:,:,:)
    INTEGER, allocatable :: ghost2cell(:,:), cell2ghost(:,:)

    ! - Rocstar Integration
    ! - Adding total number of ghost cells (Rocstar integration)
    INTEGER :: nCellsGhost,partID
    INTEGER, POINTER :: xyz_ghost(:,:)

    ! - Grid motion
    REAL(RFREAL), POINTER :: XI_TAU(:,:)            ! ... grid velocity (xi,eta,zeta)
    REAL(RFREAL), POINTER :: XI_TAU_XI(:,:)         ! ... xi-derivative of grid velocity (xi,eta,zeta)
    REAL(RFREAL), POINTER :: XYZ_TAU(:,:)           ! ... grid velocity (x,y,z)
    REAL(RFREAL), POINTER :: XYZ_TAU2(:,:)          ! ... grid acceleration (x,y,z)
    REAL(RFREAL), POINTER :: INVJAC_TAU(:)          ! ... tau-deriv of Jacobian

    ! - Geometric information
    REAL(RFREAL), POINTER :: XYZ(:,:)               ! ... physical coordinates xyz
    REAL(RFREAL), POINTER :: XYZOld(:,:)            ! ... physical coordinates xyz (of older/original configuration)
    INTEGER, POINTER      :: IBLANK(:)              ! ... iblank data
    REAL(RFREAL), POINTER :: JAC(:)                 ! ... Jacbian (|diff(x,xi)|)
    REAL(RFREAL), POINTER :: ibfac(:)               ! ... = 1 || 0
    REAL(RFREAL), POINTER :: INVJAC(:)              ! ... inverse jacobian
    REAL(RFREAL), POINTER :: INVJACOld1(:)          ! ... inverse jacobian (1 time step old)
    REAL(RFREAL), POINTER :: INVJACOld2(:)          ! ... inverse jacobian (2 time steps old)
    REAL(RFREAL), POINTER :: MT1(:,:)               ! ... first order metrics
    REAL(RFREAL), POINTER :: MT2(:,:)               ! ... second order metrics
    REAL(RFREAL), POINTER :: MT2a(:,:)              ! ... another form of the second order metrics
    REAL(RFREAL), POINTER :: cartMetric(:,:)        ! ... 2nd order metrics for Cartesian grids
    INTEGER, POINTER      :: global_iblank(:,:,:)   ! ... iblank data for entire grid
    REAL(RFREAL), POINTER :: global_XYZ(:,:,:,:)    ! ... coordinate data for entire grid
    REAL(RFREAL), POINTER :: S(:,:)                 ! ... normalized arc-length coordinates in each direction
    
#ifdef AXISYMMETRIC
    REAL(RFREAL), POINTER :: JAC0(:)                ! ... Jacbian (|diff(x,xi)|)
    REAL(RFREAL), POINTER :: MT0(:,:)               ! ... first order metrics
    REAL(RFREAL) :: drMin
#endif

    ! - For finding local edge information ... WZhang, 08/2014
    ! ... local number of detected edges
    INTEGER, ALLOCATABLE      :: l_ctr_lc(:,:), r_ctr_lc(:,:)
    ! ... global index of locally detected edges
    INTEGER, ALLOCATABLE      :: l_bndry_lc(:,:,:), r_bndry_lc(:,:,:)
    ! ... overlap (number of ghost points needed) for each edge
    INTEGER, ALLOCATABLE      :: l_overlap_lc(:,:,:), r_overlap_lc(:,:,:)
    ! ... whether boundary points extended from neighboring processor
    INTEGER, ALLOCATABLE      :: l_extended(:,:), r_extended(:,:)
    ! ... number of boundary points extended from neighboring processor
    INTEGER, ALLOCATABLE      :: l_extended_size(:,:), r_extended_size(:,:)
    ! ... we may need more layers of ghost points
    INTEGER               :: max_overlap(MAX_ND)

    ! - edge information of box shape data struct ... WZhang, 10/2014
    ! ... local number of detected edges
    INTEGER, ALLOCATABLE      :: l_ctr_lc_b(:,:,:), r_ctr_lc_b(:,:,:)
    ! ... global index of locally detected edges
    INTEGER, ALLOCATABLE      :: l_bndry_lc_b(:,:,:,:), r_bndry_lc_b(:,:,:,:)
    ! ... overlap (number of ghost points needed) for each edge
    INTEGER, ALLOCATABLE      :: l_overlap_lc_b(:,:,:,:), r_overlap_lc_b(:,:,:,:)
    ! ... whether boundary points extended from neighboring processor
    INTEGER, ALLOCATABLE      :: l_extended_b(:,:,:), r_extended_b(:,:,:)
    ! ... number of boundary points extended from neighboring processor
    INTEGER, ALLOCATABLE      :: l_extended_size_b(:,:,:), r_extended_size_b(:,:,:)
    ! ... we may need more layers of ghost points
    INTEGER               :: max_overlap_b(MAX_ND)

    ! - Derivative
    TYPE (t_matrix_sparse), pointer :: B(:,:), Bf(:,:), SAT_DI(:), SAT_DITr(:)
    TYPE (t_penta), pointer :: penta(:,:), filter(:,:)
    REAL(RFREAL) :: SBP_invPdiag_block1(8), SBP_invPdiag_block2(8)

    ! - Interpolation
    INTEGER :: nDonor, nReceiver
    INTEGER, POINTER :: iDonor(:,:), iReceiver(:,:), nStencil(:)
    REAL(RFREAL), POINTER :: interpWeights(:,:)

    INTEGER :: iFilter          ! denote index of filter filtering conserved variables every time step
    INTEGER :: iFilterTest      ! denote index of test filter for dynamic LES model
    INTEGER :: iFilterGaussianHyperviscosity ! ... denote index of truncated Gaussian filter for hyperviscosity shock capturing
    INTEGER :: iArtDiss         ! index of non-SAT-based artificial dissipation
    INTEGER :: iSATArtDiss      ! index of SAT-based artificial dissipation
    INTEGER :: iSATArtDissSplit ! 2nd index needed for split-form of SAT artificial dissipation
    INTEGER :: iFirstDeriv, iFirstDerivBiased    ! index for first-derivative operator
    INTEGER :: iSecondDeriv     ! index for second-derivative operator
    INTEGER :: iFourthDeriv     ! index for fourth-derivative operator
    INTEGER :: HYPREStencilIndex(MAX_OPERATORS,-1:1,-1:1,-1:1) ! mapping needed for SStruct stencils in HYPRE
    INTEGER :: operator_implicit_solver(MAX_OPERATORS)


    real(rfreal) :: bndry_explicit_deriv(12)

    ! ... GCL enforcement
    Real(RFREAL), POINTER :: ident(:,:)

    ! ... data for Q1D
    Real(RFREAL), POINTER :: area(:)

    ! - Finite volume geometric information
    TYPE (t_fvgrid) , pointer :: fv

    ! ... Additive RK terms
    Integer :: ARK2_nStages 
    Real(RFREAL), Pointer :: ARK2_a_mat_exp(:,:), ARK2_a_mat_imp(:,:)
    Real(RFREAL), Pointer :: ARK2_b_vec_exp(:), ARK2_b_vec_imp(:), ARK2_c_vec(:)

    ! ... for using box shape data structure
    INTEGER :: nCells_unique
    INTEGER :: is_unique(MAX_ND), ie_unique(MAX_ND)
    REAL(RFREAL), POINTER :: XYZ_unique(:,:)               ! ... physical coordinates xyz
    INTEGER, POINTER      :: IBLANK_unique(:)              ! ... iblank data

  END TYPE t_grid

  TYPE t_rk

    integer :: nSteps
    integer, pointer :: nLevels(:), flags(:)
    real(rfreal), pointer :: alpha(:,:), beta(:,:), timefac(:,:)

  END TYPE t_rk

  TYPE t_matrix_sparse

    integer :: m, n, p, q, type, nnr
    integer, pointer :: row_ptr(:,:), col_ind(:,:), nnz(:), bc_row(:,:), nbc_row(:)
    real(rfreal), pointer :: val(:,:)
    type (t_matrix_sparse), pointer :: Bc
    integer :: lu_state, lu_factors(8), lu_info

  END TYPE t_matrix_sparse

  TYPE t_penta

    integer :: n
    real(rfreal), pointer :: a(:,:), b(:,:), c(:,:), d(:,:), e(:,:)

  END TYPE t_penta

  TYPE t_patch

    INTEGER :: bcType, bcCoupled, iPatchGlobal, gridID
    INTEGER :: normDir, normIndex, comm
    REAL(RFREAL), POINTER :: Deriv(:,:,:), MT2(:,:), sponge_xs(:,:), sponge_xe(:,:), ViscousFlux(:,:,:), ViscousFluxAux(:,:,:), ViscousRHSAux(:,:)
    REAL(RFREAL), POINTER :: DerivAdj(:,:,:),  TempGrad(:,:), dXYZ(:,:), dXYZ_TAU(:,:), dXYZ_TAU2(:,:)
    INTEGER, POINTER :: is(:), ie(:), bcData(:)
    REAL(RFREAL), POINTER :: u_inflow(:), v_inflow(:), w_inflow(:), T_inflow(:), CellTemp(:), HtFlux(:), dCellTemp(:)
    REAL(RFREAL), POINTER :: dudt_inflow(:), dvdt_inflow(:), dwdt_inflow(:), dTdt_inflow(:)
    REAL(RFREAL), POINTER :: sat_fac(:), sat_fac_rk(:,:,:), cv_out(:,:), ViscousFlux_out(:,:,:), cv_in(:,:), cv_in_global(:,:), XYZOld(:,:)

    ! ... inflow forcing using the linear stability theory in an axisymmetric domain
    INTEGER :: LST_num_mode_temporal, LST_num_mode_azimuthal
    REAL(RFREAL) :: LST_amp, LST_x0
    REAL(RFREAL), POINTER :: LST_freq_angular(:), LST_wavenum_azimuthal(:) ! ... spatially developing instability
    REAL(RFREAL), POINTER :: LST_phase(:,:), LST_phase_dir(:,:)
    COMPLEX(RFREAL), POINTER :: LST_wavenum_streamwise(:,:) ! ... spatially developing instability
    COMPLEX(RFREAL), POINTER :: LST_eigenf(:,:,:,:)

    ! Rocstar integration
    REAL(RFREAL), POINTER :: pressure_cell_center(:)
    REAL(RFREAL), POINTER :: patch_xyz(:)
    INTEGER,POINTER :: local_patch_extent(:)
    INTEGER,POINTER :: global_patch_extent(:)
    REAL(RFREAL), POINTER :: rxyz(:)
    INTEGER :: flipflag

    ! ... Implicit SAT arrays
    INTEGER :: prodN
    REAL(RFREAL), POINTER :: implicit_sat_mat(:,:,:), implicit_sat_mat_old(:,:,:)

    ! ... SAT_BLOCK_INTERFACE auxilliary data
    !    INTEGER, POINTER :: sat_remote_ranks(:), sat_npts(:), sat_index(:)
    INTEGER :: nlocal_collisions
    INTEGER :: nremote_collisions
    INTEGER :: nremote_send, ncomp
    INTEGER, POINTER :: remote_ranks(:)
    INTEGER, POINTER :: local_collisions(:)
    INTEGER, POINTER :: local_buffer_indices(:,:)
    INTEGER, POINTER :: remote_collisions(:)
    INTEGER, POINTER :: remote_buffer_indices(:,:)
    REAL(RFREAL), POINTER :: sat_send_buffer(:)
    REAL(RFREAL), POINTER :: sat_recv_buffer(:)
    !    INTEGER :: nsat_remote
    
    ! ... surface for integral of solution
    INTEGER :: SurfIntCommAmt(MAX_ND,2), iIntegralSurface

    ! - post processing
    REAL(RFREAL), POINTER :: SurfTract(:,:), YPlus(:,:)



  END TYPE t_patch

  TYPE t_interp_receivers
    integer :: nreceivers
    integer, allocatable :: point(:,:)
    integer, allocatable :: rank(:)
    integer, allocatable :: donor_grid(:)
    integer, allocatable :: donor_index(:)
    integer :: ndonor_ranks_max
    integer, allocatable :: ndonor_ranks(:)
    integer, allocatable :: donor_rank(:,:)
  END TYPE t_interp_receivers

  TYPE t_interp_donors
    integer :: ndonors
    integer :: nstencil_max
    integer, allocatable :: lower_point(:,:)
    integer, allocatable :: upper_point(:,:)
    real(rfreal), allocatable :: coef(:,:,:)
    integer :: nranks_max
    integer, allocatable :: nranks(:)
    integer, allocatable :: rank(:,:)
    integer, allocatable :: receiver_grid(:)
    integer, allocatable :: receiver_index(:)
    integer, allocatable :: receiver_rank(:)
  END TYPE t_interp_donors

  TYPE t_interp_receiver_exchange
    integer :: nreceivers
    integer, allocatable :: local_receiver_index(:)
    integer :: receiver_grid
    integer :: receiver_grid_local
    integer :: donor_grid
    integer :: donor_rank
    real(rfreal), allocatable :: buffer(:,:)
  END TYPE t_interp_receiver_exchange

  TYPE t_interp_donor_exchange
    integer :: ndonors
    integer :: npoints_max
    integer, allocatable :: npoints(:)
    integer, allocatable :: local_grid_point_index(:,:)
    real(rfreal), allocatable :: weight(:,:)
    integer :: donor_grid
    integer :: donor_grid_local
    integer :: receiver_grid
    integer :: receiver_rank
    real(rfreal), allocatable :: buffer(:,:)
  END TYPE t_interp_donor_exchange

  TYPE t_interp
    type(t_interp_receivers), allocatable :: global_receivers(:)
    type(t_interp_donors), allocatable :: global_donors(:)
    type(t_interp_receivers), allocatable :: local_receivers(:)
    type(t_interp_donors), allocatable :: local_donors(:)
    integer :: nreceives
    integer :: ndonates
    type(t_interp_receiver_exchange), allocatable :: receive(:)
    type(t_interp_donor_exchange), allocatable :: donate(:)
  END TYPE t_interp

  TYPE t_mpi_timings

    REAL(RFREAL) :: metrics, rhs_inviscid, rhs_viscous, rhs_shock, rhs_bc
    REAL(RFREAL) :: rhs_les, interp, rhs_total, rhs_artdiss, rhs_bc_fix_value
    REAL(RFREAL) :: rhs_bc_fix_value_total, interp_setup
    REAL(RFREAL) :: interp_volx, interp_nbix
    REAL(RFREAL) :: io_read_restart, io_write_restart, io_read_grid, io_write_grid, io_write_soln
    REAL(RFREAL) :: io_read_sponge,io_read_soln,io_read_mean,io_read_target, io_setup
    REAL(RFREAL), Pointer :: operator_setup(:,:), operator(:,:), operator_total(:), operator_solve(:,:)
    REAL(RFREAL), Pointer :: operator_interior(:,:), operator_boundary(:,:), operator_ghostExchange(:,:)
    REAL(RFREAL) :: metrics_total

!   ... New Timing Block
!    REAL(RFREAL) :: rhs_inviscid_mean, rhs_inviscid_max, rhs_inviscid_min, rhs_inviscid_stdev
!    REAL(RFREAL) :: rhs_viscous_mean, rhs_viscous_max, rhs_viscous_min, rhs_viscous_stdev
!    REAL(RFREAL) :: rhs_bc_mean, rhs_bc_max, rhs_bc_min, rhs_bc_stdev
!    REAL(RFREAL) :: rhs_bc_fix_value_mean, rhs_bc_fix_value_max, rhs_bc_fix_value_min, rhs_bc_fix_value_stdev
!    REAL(RFREAL) :: interp_mean, interp_max, interp_min, interp_stdev
!    REAL(RFREAL) :: interp_setup_mean, interp_setup_max, interp_setup_min, interp_setup_stdev
!   ...

    REAL(RFREAL) :: rhs_viscous_total, rhs_shock_total
    REAL(RFREAL) :: rhs_bc_total, rhs_les_total, interp_total, rhs_total_total, io_total, rhs_TGrad, rhs_VGrad, rhs_EOS
    REAL(RFREAL) :: rhs_ScalarGrad

    ! ... thermomechanical
    REAL(RFREAL) :: TM_lhs_thermal, TM_lhs_structural, TM_rhs_thermal
    REAL(RFREAL) :: TM_rhs_structural, TM_solve_thermal, TM_solve_structural
    REAL(RFREAL) :: TM_lhs_thermal_total, TM_lhs_structural_total
    REAL(RFREAL) :: TM_rhs_thermal_total, TM_rhs_structural_total
    REAL(RFREAL) :: TM_solve_thermal_total, TM_solve_structural_total
    ! Regarding E-field
    REAL(RFREAL) :: efield_solve, efield_processing

  END TYPE t_mpi_timings

  TYPE t_sub_actuator

    ! ... the term "sub-actuator" comes from the fact that points in a single 
    ! ... actuator may be distributed over more than one processor, JKim 07/2009
    INTEGER :: subActID ! ... ID of a sub-actuator in this region (processor)
    INTEGER :: actID ! ... ID of an actuator AS A WHOLE to which this sub-actuator belongs
    INTEGER :: gridID ! ... to which grid in this region (processor) this sub-actuator belongs
    INTEGER :: gridID_global ! ... to which grid in the grid file this sub-actuator belongs
    INTEGER :: numPts ! ... how many points this sub-actuator contains
    INTEGER, POINTER :: index(:) ! ... 1-D local indices of sub-actuator points
    INTEGER :: switch ! ... if this sub-actuator is on or off
    REAL(RFREAL), POINTER :: distFunc(:) ! ... distribution function for the purpose of smoothing 
                                         ! ... actuating region
    INTEGER, POINTER :: ptID(:) ! ... global point ID

    ! ... optimal control parameters following Mingjun and Randy's notations
    ! ... available only when input%AdjOptim = .TRUE.
    REAL(RFREAL), POINTER :: gPhi(:,:) ! ... control input time series
    REAL(RFREAL), POINTER :: phi(:) ! ... instantaneous control input applied to forward N-S
                                    ! ... time-interpolated using gPhi(:,:)
    REAL(RFREAL), POINTER :: gPhiCtrl(:,:) ! ... optimized control input; 
                                           ! ... first kept as an optimal input 
                                           ! ... from the previous iteration and 
                                           ! ... updated after line-minimization (dlinmin)
    REAL(RFREAL), POINTER :: gradient(:,:) ! ... gradient of control input
    REAL(RFREAL), POINTER :: xi(:,:) ! ... working array for either tentative 
                                     ! ... gradient or h-vector of conjugate gradient
    REAL(RFREAL), POINTER :: g(:,:), h(:,:) ! ... conjugate gradient vectors
    REAL(RFREAL) :: gAlpha, gAlpha_old ! ... optimized generalized distance in phi-space

  END TYPE t_sub_actuator

  TYPE t_sub_ctrl_target

    ! ... the term "sub-control-target" comes from the fact that points in a single 
    ! ... target zone may be distributed over more than one processor, JKim 07/2009
    INTEGER :: subTargID ! ... ID of a sub-target in this region (processor)
    INTEGER :: targID ! ... ID of a target region AS A WHOLE to which this sub-target belongs
    INTEGER :: gridID ! ... to which grid in this region (processor) this sub-target belongs
    INTEGER :: gridID_global ! ... to which grid in the grid file this target belongs
    INTEGER :: numPts ! ... how many points this sub-target contains
    INTEGER, POINTER :: index(:) ! ... 1-D local indices of sub-target points
    REAL(RFREAL), POINTER :: distFunc(:) ! ... distribution function for the purpose of smoothing 
                                         ! ... target region

    REAL(RFREAL) :: gCost, gCost_old, gCost_initial ! ... global functional of all targets
    REAL(RFREAL), POINTER :: meanQ(:) ! ... functional in terms of fluctuation needs a reference value
                                      ! ... e.g. time-mean pressure prior to control

  END TYPE t_sub_ctrl_target

  TYPE t_probe ! ... JKim 12/2008

    INTEGER :: probeID ! ... global probe ID
    INTEGER :: gridID  ! ... global grid ID to which this probe belongs
    INTEGER :: ng      ! ... local grid ID to which this probe belongs

    INTEGER :: ijk(MAX_ND) ! ... i, j, k indices of this probe in a grid
    INTEGER :: l0      ! ... 1D index to access grid and state data

    INTEGER :: sampleRate ! ... sampling rate of this probe
                          ! ... e.g. we measure at every 'sampleRate' time step
    INTEGER :: numSample ! ... how many samples this probe has
    REAL(RFREAL) :: sampleDT ! ... sampling time of this probe
                             ! ... simply 'sampleRate * state%dt' assuming 
                             ! ... constant-DT time-stepping

    REAL(RFREAL) :: xyz(MAX_ND)

    INTEGER :: numVar
    REAL(RFREAL), POINTER :: var(:,:)

    INTEGER :: iter_min
    REAL(RFREAL) :: dt, time_min

  END TYPE t_probe

  ! ... data structure to store spline data
  TYPE t_spline

    INTEGER :: nxb, nvars
    REAL(RFREAL) :: dxb
    REAL(RFREAL), POINTER :: xb(:)
    REAL(RFREAL), POINTER :: coeffs(:,:,:)

  END TYPE t_spline

  ! Data structure for spline with optional 'natural' endpoint conditions (zero second derivative)
  TYPE t_nat_spline
    INTEGER :: n
    REAL(RFREAL), DIMENSION(:), ALLOCATABLE :: x
    REAL(RFREAL), DIMENSION(:), ALLOCATABLE :: y
    REAL(RFREAL), DIMENSION(:), ALLOCATABLE :: dx
    REAL(RFREAL), DIMENSION(:), ALLOCATABLE :: second_deriv
  END TYPE t_nat_spline

  TYPE t_sgrid

    ! ... grid-specific input
    Type (t_mixt_input), pointer :: input

    ! - MPI data
    INTEGER :: Comm, numProc_inComm, coreRank_inComm, myRank_inComm, CommWorld
    INTEGER :: numGridsInFile, parallelTopology
    INTEGER, POINTER :: gridToProcMap(:,:), AllGridsGlobalSize(:,:), GlobalSize(:)
    INTEGER :: cartComm, myrank_inCartComm, numproc_inCartComm, left_process(MAX_ND), right_process(MAX_ND)
    INTEGER :: cartDims(MAX_ND), cartCoords(MAX_ND)

    ! - Basic grid quantities
    INTEGER :: nCells
    INTEGER :: parDir, ND, iGridGlobal
    INTEGER :: is(MAX_ND), ie(MAX_ND)
    INTEGER, POINTER :: nGhostRHS(:,:), nGhostLHS(:,:)

    ! - Geometric information
    REAL(RFREAL), POINTER :: XYZ(:,:)               ! ... physical coordinates xyz, plot3D nodes
    REAL(RFREAL), POINTER :: X(:,:)                 ! ... physical coordinates xyz, reference configuration
    REAL(RFREAL), POINTER :: XYZOld(:,:)            ! ... physical coordinates xyz (of older/original configuration)
    INTEGER, POINTER      :: IBLANK(:)              ! ... iblank data
    INTEGER, POINTER      :: global_iblank(:,:,:)   ! ... iblank data for entire grid
    REAL(RFREAL), POINTER :: global_XYZ(:,:,:,:)    ! ... coordinate data for entire grid

    ! - Finite element information
    INTEGER, POINTER :: LM(:,:)                ! ... element table
    INTEGER, POINTER :: L2G(:)                 ! ... local to global node map
    INTEGER, POINTER :: G2L(:)                 ! ... global to local node map
    INTEGER, POINTER :: L2P3D(:)               ! ... map from local nodes to plot3D nodes (for I/O)
    INTEGER, POINTER :: P3DLM(:,:)             ! ... "element table" containing all plot3D nodes, (k-1)*N(2)*N(1)+(j-1)*N(1) + i order
    INTEGER, POINTER :: ExchngInd(:,:,:)       ! ... map of send and receive buffers to local nodes
    INTEGER, POINTER :: nExchng(:)             ! ... number to exchange in each direction
    REAL(RFREAL), POINTER :: ShpFcn(:,:)       ! ... shape function
    REAL(RFREAL), POINTER :: dShpFcn(:,:,:)    ! ... derivative of shape function
    REAL(RFREAL), POINTER :: Weight(:)         ! ... Gauss point weights 
    INTEGER :: nPtsLoc, nElLoc                 ! ... number of local nodes, elements
    INTEGER :: nStart                          ! ... global node number of first node
    INTEGER :: nPtsGlb                         ! ... global number of nodes
    INTEGER :: nPtsOwned                       ! ... number of nodes owned by this process /= nPtsLoc
    INTEGER :: NdsPerElem                      ! ... number of nodes per elements
    INTEGER :: nStartRenum                     ! ... global node number of first node after using ParMetis
    INTEGER :: nPtsOwnedRenum                  ! ... number of nodes owned by this process after using ParMetis
    INTEGER, POINTER :: L2GRenum(:)            ! ... local to global node map after using ParMetis

    ! ... element face info
    INTEGER :: NdsPerFace            ! ... number of nodes per face
    INTEGER, POINTER :: FaceNodes(:,:,:)      ! ... element local node numbers for each face


  END TYPE t_sgrid

  TYPE t_smixt

    INTEGER :: NVARS, ND, CoupleFrequency
    REAL(RFREAL) :: dt_struct, dt_thermal
    REAL(RFREAL), POINTER :: time(:)
    REAL(RFREAL), POINTER :: q(:,:), qOld(:,:), qdot(:,:), P3D(:,:), P3Ddot(:,:), Tnew(:)

    ! ... Dirichlet boundary condition data
    REAL(RFREAL), POINTER :: BCDispVal(:), BCTempVal(:)
    INTEGER, POINTER :: BCDispInd(:), BCTempInd(:)
    INTEGER :: TempBCNum, DispBCNum

    ! ... Newton Rhapson
    Logical :: NewInitRes
    REAL(RFREAL) :: InitRes, FinalRes
    Integer :: numNRsteps
    
    ! ... temperature dependent material properties
    INTEGER :: nTblPts
    REAL(RFREAL), POINTER :: MtlPrpTbl(:,:)
    REAL(RFREAL), POINTER :: YMod(:), PRat(:)

    ! - IO buffers
    REAL(RFREAL), POINTER :: DBUF_IO(:,:,:,:)
    INTEGER, POINTER :: IBUF_IO(:,:,:)

    ! ... data records
    REAL(RFREAL), POINTER :: PowerIn(:), KinEnergy(:), SDTime(:)
    Integer :: numDataSample

  END TYPE t_smixt

  TYPE t_spatch

    INTEGER :: bcType, bcCoupled, iPatchGlobal, gridID
    INTEGER :: normDir, normIndex, prodN
    REAL(RFREAL) :: bcVal(MAX_ND)
    REAL(RFREAL), POINTER :: Traction(:,:), Displacement(:,:), FluidStressTns(:,:), FluidHtFlux(:)
    INTEGER, POINTER :: is(:), ie(:), bcData(:)
    REAL(RFREAL), POINTER :: CellTemp(:), HtFlux(:)

    INTEGER,POINTER :: local_patch_extent(:)
    INTEGER,POINTER :: global_patch_extent(:)

    ! ... Element Info
    INTEGER, POINTER :: LM2D(:,:)              ! ... patch element table
    INTEGER, POINTER :: LMP3D2D(:,:)           ! ... element table from patch to i,j,k ordered nodes on patch
    INTEGER, POINTER :: TanDir(:)              ! ... patch tangent directions
    
    REAL(RFREAL), POINTER :: VolShpFcn(:,:)    ! ... volume shape function
    REAL(RFREAL), POINTER :: dVolShpFcn(:,:,:) ! ... derivative of volume shape function
    REAL(RFREAL), POINTER :: BndShpFcn(:,:)    ! ... boundary shape function
    REAL(RFREAL), POINTER :: dBndShpFcn(:,:,:) ! ... derivative of boundary shape function    
    REAL(RFREAL), POINTER :: Weight(:)         ! ... boundary Gauss point weights 

  END TYPE t_spatch

  TYPE t_fsi

     ! ... MPI 
     INTEGER :: Comm, rank, numproc_inComm, fluid_comm
     
     ! ... patch geometry
     INTEGER, POINTER :: FluidExt(:,:), SolidExt(:,:)
     INTEGER, POINTER :: FluidNeighbor(:), FluidNeighExt(:,:)
     INTEGER, POINTER :: SolidNeighbor(:), SolidNeighExt(:,:)
     INTEGER :: nSolidNeighbor, nFluidNeighbor
     INTEGER :: patchID, gridID, TMpatchID, TMgridID, ipatchColor

     ! ... data to pass
     REAL(RFREAL), POINTER :: data(:,:)

  END TYPE t_fsi

  ! ---------------------------------------------------------
  ! ... on-the-fly statistics (J. Capecelatro)
  TYPE t_stats
     CHARACTER(LEN=80), POINTER :: varname(:)   ! ... variable names
     Integer :: nvar                            ! ... number of variables
     Integer :: statgrid                        ! ... which grid to compute stats on
     Integer :: sampler                         ! ... number of iterations since last output
     Integer, POINTER :: dim (:)                ! ... dimensions
     Integer, POINTER :: symdir(:)              ! ... symmetric directions
     REAL(RFREAL) :: spacenorm                  ! ... normalization factor (in space)
     REAL(RFREAL) :: Delta_t                    ! ... how long averaging has occured (for normalization)
     REAL(RFREAL), POINTER :: sumvar1D(:,:)     ! ... 1D stats
     REAL(RFREAL), POINTER :: sumvar2D(:,:,:)   ! ... 2D stats
     REAL(RFREAL), POINTER :: sumvar3D(:,:,:,:) ! ... 3D stats
     
  END TYPE t_stats

  ! Define stats type (only 2D for now)
  type(t_stats) :: stat2D

  TYPE t_lighthill
  
    INTEGER :: numGridFiles
    CHARACTER(LEN=80), POINTER :: grid_fname(:)
    REAL(RFREAL), POINTER :: gridTime(:)
    type(t_nat_spline) :: spline
    REAL(RFREAL), POINTER :: Force(:,:)
    REAL(RFREAL), POINTER :: QLighthill(:,:), dQ(:), Q(:)
  
  END TYPE t_lighthill

  TYPE t_deriv

    INTEGER :: bufSz
    REAL(RFREAL), POINTER :: sbuf(:,:), rbuf(:,:)

  END TYPE t_deriv

  TYPE t_navierstokesrhs
  
    INTEGER :: ncSz
    REAL(RFREAL), ALLOCATABLE :: UVW(:,:), HeatFlux(:,:), div(:), flux2(:)
    REAL(RFREAL), POINTER :: StrnRt(:,:), dflux1(:), dflux2(:), flux1(:)
    REAL(RFREAL), ALLOCATABLE :: A(:), B(:)
      
  END TYPE t_navierstokesrhs

  TYPE t_eulerfv
 
    REAL(WP), DIMENSION(:), ALLOCATABLE :: q_local, local_reconstructed_state_lft 
    REAL(WP), DIMENSION(:), ALLOCATABLE :: local_reconstructed_state_rgt, local_metric_single_direction
    REAL(WP), DIMENSION(:), ALLOCATABLE :: local_state, local_ghost_state
    REAL(WP), DIMENSION(:), ALLOCATABLE :: normal_vector, tangential_velocity
    REAL(WP), DIMENSION(:,:), ALLOCATABLE :: tangential_vector
    REAL(WP), DIMENSION(:), ALLOCATABLE :: Roe_state, s_Roe
    REAL(WP), DIMENSION(:), POINTER :: flux_at_lft_state, flux_at_rgt_state
    REAL(WP), DIMENSION(:,:), ALLOCATABLE :: rotation_matrix_Roe_state, rotation_matrix_inverse_Roe_state
    
  END TYPE t_eulerfv
  
  TYPE t_optimization

    INTEGER :: getMean ! ... functional in terms of fluctuation requires to compute 
                       ! ... reference mean field in target region ONCE prior to control
    CHARACTER(LEN=30) :: mnbrak_suffix ! ... used only by mnbrak to name directories 
                                       ! ... containing forward solutions
    INTEGER :: solveNS ! ... flag determining if we're actually solving forward & 
                       ! ... adjoint N-S equations during optimization
    CHARACTER(LEN=3) :: whichNS ! ... FWD when solving forward N-S equations;
                                ! ... ADJ when solving adjoint N-S equations
    INTEGER :: curRunningMode ! ... which stage of optimization we are at?
    INTEGER :: subIter ! ... for mnbrak and dbrent, count number of sub-iterations
                       ! ... before they converge; 0 otherwise
    INTEGER :: restart_GLOBAL_ITER, &
               restart_curRunningMode, &
               restart_subIter, &
               restart_main_ts_loop_index
    CHARACTER(LEN=3) :: restart_whichNS
  
  END TYPE t_optimization

  TYPE t_modglobal

    ! ... mappings useful in "vectorizing" arrays
    INTEGER, POINTER :: vMap(:), t2Map(:,:), t3Map(:,:,:), delta(:,:)
    INTEGER, POINTER :: t2MapSym(:,:), t2MapSymVisc(:,:)
    INTEGER :: USE_RESTART_FILE ! ... use restart file
    CHARACTER(LEN=PATH_LENGTH) :: restart_fname(2)
    INTEGER :: nStencilMax ! ... maximum stencil size for interpolation
    LOGICAL :: rk_alloc ! ... Previously done with statics (Rocstar)
    INTEGER :: filtercount(MAX_OPERATORS)
    INTEGER :: main_ts_loop_index ! ... time marching parameters

  END TYPE t_modglobal
  
  TYPE t_param
    
    CHARACTER(LEN=256) :: line ! 256 = MAX_LINE_LEN
    INTEGER :: argc
    INTEGER, POINTER :: argv(:) ! 1:argc+1
    INTEGER :: request_count
    TYPE(t_param), POINTER :: next
    INTEGER :: recursion_level
    LOGICAL :: lineInit, nextInit ! true if initialized

  END TYPE t_param

  TYPE t_comm_buf
    
    INTEGER :: srank, rrank, srequest, rrequest
    INTEGER :: sid, rid, comm, nsend, nrecv
    REAL(8), POINTER :: sbuffer(:), rbuffer(:)
  
  END TYPE t_comm_buf

  TYPE t_sat_descriptor
  
    INTEGER :: comm, rank, color, np
    INTEGER :: nlocal_sats, nremote_sats, nrequests
    INTEGER, POINTER :: requests(:)
    INTEGER, POINTER :: statuses(:,:)
    INTEGER, POINTER :: local_sats(:)
    INTEGER, POINTER :: remote_sat_info(:) ! sat_rank,tag,nrecv
    TYPE(t_comm_buf), POINTER :: comm_buffers(:) 
  
  END type t_sat_descriptor

  TYPE t_real_buffer
     REAL(RFREAL), DIMENSION(:), POINTER :: bufPtr
  END TYPE t_real_buffer
  TYPE t_flag_buffer
     INTEGER, DIMENSION(:), POINTER :: bufPtr
  END TYPE t_flag_buffer

  TYPE t_io_descriptor
     CHARACTER(LEN=32) :: fileNameBase
     CHARACTER(LEN=32) :: fileName
     REAL(RFREAL)      :: dConfig(8)
     INTEGER           :: fConfig(8)
     INTEGER           :: numDataItems, numFlagItems
     CHARACTER(LEN=3)  :: varNames(20)
     CHARACTER(LEN=10),   DIMENSION(:),   ALLOCATABLE :: dNameList
     CHARACTER(LEN=10),   DIMENSION(:),   ALLOCATABLE :: fNameList
     TYPE(t_real_buffer), DIMENSION(:,:), ALLOCATABLE :: dBuffers
     TYPE(t_flag_buffer), DIMENSION(:,:), ALLOCATABLE :: fBuffers
  END TYPE t_io_descriptor

  TYPE t_hdf5_io
#ifdef HAVE_HDF5
    INTEGER(hid_t) :: m_file_id ! hid_t is defined in hdf5
#else
    INTEGER :: m_file_id
#endif
    CHARACTER(LEN=29) :: m_fname
    INTEGER :: m_fname_len
    INTEGER :: m_xmf
    LOGICAL :: m_HDFinitialized
    INTEGER :: m_info
    CHARACTER(LEN=10),DIMENSION(:), ALLOCATABLE      :: dataItemNameList
    INTEGER :: numDataItems, numFlagItems
    REAL(RFREAL) :: doubleConfig(8)
    INTEGER :: intConfig(8)
    CHARACTER(LEN=32) :: fileName
    TYPE(t_real_buffer), DIMENSION(:,:), ALLOCATABLE :: solutionBuffers
    TYPE(t_flag_buffer), DIMENSION(:,:), ALLOCATABLE :: flagBuffers
  END TYPE t_hdf5_io

#ifdef USE_AMPI
  TYPE t_ampi

    LOGICAL :: migrate       ! ... AMPI migration on/off
    INTEGER :: migrationStep ! ... AMPI migration frequency
    LOGICAL :: checkpoint    ! ... AMPI checkpoint on/off

  END TYPE t_ampi
#endif

END MODULE ModDataStruct
