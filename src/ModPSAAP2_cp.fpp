! Copyright (c) 2015, University of Illinois at Urbana-Champaign, XPACC
! License: MIT, http://opensource.org/licenses/MIT
![UNUSED]
Module ModPSAAP2_CP
  
  ! universal constants
  real(8), parameter, private :: rgas = 8314.0_8 
  
  ! conversion factors
  real(8), parameter, private  ::  HConv = 4.184d6   ! from Kcal/mol to J/kmol   !
  real(8), parameter, private  ::  CpConv = 4.184d3  ! from cal/mol to J/kmol   !
  real(8), parameter, private  ::  EConv = 1.d6/rgas ! from KJ/mol to J/kmol to K (by dividing by rgas)
  real(8), parameter, private  ::  RConv = 1.d-3     ! from kg/m^3 to g/cm^3   !
  
  ! reference temperature
  real(8), parameter, private  :: T0 = 298.0_8
  
  ! species are ordered as: (H2, O2, H2O, H, HO2, N2, OH, O)
  ! ... molecular weights (kg/kmol, g/mol)
  real(8), parameter, private  :: W(8) = (/  2.0_8, &    ! H2
       32.0_8, &    ! O2
       18.0_8, &    ! H2O
       1.0_8, &    ! H
       33.0_8, &    ! HO2
       28.0_8, &    ! N2
       17.0_8, &    ! OH
       16.0_8 /)    ! O
  
  ! ... enthalpies of formation at T = 298 K = 25 deg C, in J/kg
  real(8), parameter, private  :: dh(8) = (/  0.00000d0, &    ! H2
       0.00000d0, &    ! O2
       -1.34283d7, &    ! H2O
       2.17978d8, &    ! H
       3.80364d5, &    ! HO2
       0.00000d0, &    ! N2
       2.19291d6, &    ! OH
       1.155749d7/)    ! O
  
  ! ... specific heats in J/(kg K)
  real(8), parameter, private  :: Cp(8) = (/  15062.4_8,  &    ! H2
       1085.23_8, &    ! O2
       2294.23_8, &    ! H2O
       20752.6_8,  &    ! H
       1442.85_8, &    ! HO2
       1237.27_8, &    ! N2
       1806.5_8,  &    ! OH
       1304.89_8 /)    ! O

    real(8), parameter, private  ::  A1f = 3.52d16
    real(8), parameter, private  ::  A1b = 7.04d13
    real(8), parameter, private  ::  n1f = -0.7
    real(8), parameter, private  ::  n1b = -0.26
    real(8), parameter, private  ::  T1f = 71.42*EConv
    real(8), parameter, private  ::  T1b = 0.6*EConv

    real(8), parameter, private  ::  A2f = 5.06d4
    real(8), parameter, private  ::  A2b = 3.03d4
    real(8), parameter, private  ::  n2f = 2.67
    real(8), parameter, private  ::  n2b = 2.63
    real(8), parameter, private  ::  T2f = 26.32*EConv
    real(8), parameter, private  ::  T2b = 20.23*EConv

    real(8), parameter, private  ::  A3f = 1.17d9
    real(8), parameter, private  ::  A3b = 1.28d10
    real(8), parameter, private  ::  n3f = 1.3
    real(8), parameter, private  ::  n3b = 1.19
    real(8), parameter, private  ::  T3f = 15.21*EConv
    real(8), parameter, private  ::  T3b = 78.25*EConv

    real(8), parameter, private  ::  A4o = 5.75d19
    real(8), parameter, private  ::  A4inf = 4.65d12
    real(8), parameter, private  ::  n4o = -1.4
    real(8), parameter, private  ::  n4inf = 0.44
    real(8), parameter, private  ::  Fc4 = 0.5
    real(8), dimension(8), parameter, private  ::  X4 = (/ 2.5, 1., 16., 1., 1., 1., 1., 1. /)

    real(8), parameter, private  ::  A5 = 7.08d13
    real(8), parameter, private  ::  T5 = 1.23*EConv

    real(8), parameter, private  ::  A6f = 1.66d13
    real(8), parameter, private  ::  A6b = 2.69d12
    real(8), parameter, private  ::  n6b = 0.36
    real(8), parameter, private  ::  T6f = 3.44*EConv
    real(8), parameter, private  ::  T6b = 231.86*EConv

    real(8), parameter, private  ::  A7 = 2.89d13
    real(8), parameter, private  ::  T7 = -2.08*EConv

    real(8), parameter, private  ::  A8f = 4d22
    real(8), parameter, private  ::  A8b = 1.03d23
    real(8), parameter, private  ::  n8f = -2
    real(8), parameter, private  ::  n8b = -1.75
    real(8), parameter, private  ::  T8b = 496.14*EConv
    real(8), dimension(8), parameter, private  ::   X8 = (/ 2.5, 1., 12., 1., 1., 1., 1., 1. /)

    real(8), parameter, private  ::  A9f = 1.3d18
    real(8), parameter, private  ::  A9b = 3.04d17
    real(8), parameter, private  ::  n9f = -1
    real(8), parameter, private  ::  n9b = -0.65
    real(8), parameter, private  ::  T9b = 433.09*EConv

    real(8), parameter, private  ::  A10 = 3.02E12
    real(8), parameter, private  ::  T10 = 5.8*EConv

    real(8), parameter, private  ::  A11 = 1.62d11
    real(8), parameter, private  ::  n11 = 0.61
    real(8), parameter, private  ::  T11 = 100.14*EConv
  
Contains

  Subroutine PSAAP2_2D_Init(input, grid, state)

    USE ModGlobal
    USE ModDataStruct
    USE ModMPI

    Implicit None

    ! ... global variables
    type(t_mixt_input), pointer :: input
    type(t_grid), pointer :: grid
    type(t_mixt), pointer :: state

    ! ... local variables
    integer :: i, k
    real(rfreal) :: GamRef, TempRef, DensRef, SndSpdRef, PresRef, Y(8), MMW, Cp_mix, Cv_mix, eta, etay, etax
    real(rfreal) :: temperature, pressure, density, ibfac

    ! ... reference values
    TempRef   = input%tempref
    DensRef   = input%densref
    SndSpdRef = input%sndspdref
    PresRef   = input%presref
    GamRef    = input%gamref

    do i = 1, grid%nCells

      ibfac = grid%ibfac(i)
      
      ! ... crossflow
      if (grid%XYZ(i,2) >= 1.0_8) then
        eta = (grid%XYZ(i,2) - 1.0_8)/1.0_8
        if (eta <= 1.0_8) then
          state%cv(i,2) = (2.0_8 * (1.0_8 - eta*eta)*eta + eta*eta*eta*eta)*input%initflow_mach
          if (input%nAuxVars >= 2) state%auxvars(i,2) = 0.23_8
        else
          state%cv(i,2) = input%initflow_mach
          if (input%nAuxVars >= 2) state%auxvars(i,2) = 0.23_8 
        end if
        state%cv(i,3) = 0.0_8
        if (input%nAuxVars >= 1) state%auxVars(i,1) = 1d-6
        if (input%nAuxVars >= 3) state%auxVars(i,3) = 1d-6
        if (input%nAuxVars >= 4) state%auxVars(i,4) = 1d-6
        if (input%nAuxVars >= 5) state%auxVars(i,5) = 1d-6
        if (input%nAuxVars >= 6) state%auxVars(i,6) = 1d-6
        if (input%nAuxVars >= 7) state%auxVars(i,7) = 1d-6
      endif

      ! ... transverse jet
      if (grid%XYZ(i,2) <= 1.0_8) then
        etax = (0.5_8 - dabs(5.5_8 - grid%XYZ(i,1))) / 0.2_8
        etay = (1.0_8 - grid%XYZ(i,2))**2
        state%cv(i,2) = 0.0_8
        if (etax <= 1.0_8) then
          state%cv(i,3) = 0.0_8
          if (input%nAuxVars >= 1) state%auxVars(i,1) = 1d-6
          if (input%nAuxVars >= 2) state%auxVars(i,2) = 0.23_8
          if (input%nAuxVars >= 3) state%auxVars(i,3) = 1d-6
          if (input%nAuxVars >= 4) state%auxVars(i,4) = 1d-6
          if (input%nAuxVars >= 5) state%auxVars(i,5) = 1d-6
          if (input%nAuxVars >= 6) state%auxVars(i,6) = 1d-6
          if (input%nAuxVars >= 7) state%auxVars(i,7) = 1d-6
        else
          state%cv(i,3) = 0.0_8
          if (input%nAuxVars >= 1) state%auxVars(i,1) = 1d-6
          if (input%nAuxVars >= 2) state%auxVars(i,2) = 0.23_8
          if (input%nAuxVars >= 3) state%auxVars(i,3) = 1d-6
          if (input%nAuxVars >= 4) state%auxVars(i,4) = 1d-6
          if (input%nAuxVars >= 5) state%auxVars(i,5) = 1d-6
          if (input%nAuxVars >= 6) state%auxVars(i,6) = 1d-6
          if (input%nAuxVars >= 7) state%auxVars(i,7) = 1d-6
        end if
      end if

      ! ... zero out solution in blanked-out regions
      state%cv(i,2) = state%cv(i,2) * ibfac
      state%cv(i,3) = state%cv(i,3) * ibfac

      ! ... set viable solution in blanked-out region
      if (input%nAuxVars >= 1) state%auxVars(i,1) = state%auxVars(i,1) * ibfac + (1.0_8 - ibfac) * 1d-6
      if (input%nAuxVars >= 2) state%auxVars(i,2) = state%auxVars(i,2) * ibfac + (1.0_8 - ibfac) * 0.23_8
      if (input%nAuxVars >= 3) state%auxVars(i,3) = state%auxVars(i,3) * ibfac + (1.0_8 - ibfac) * 1d-6
      if (input%nAuxVars >= 4) state%auxVars(i,4) = state%auxVars(i,4) * ibfac + (1.0_8 - ibfac) * 1d-6
      if (input%nAuxVars >= 5) state%auxVars(i,5) = state%auxVars(i,5) * ibfac + (1.0_8 - ibfac) * 1d-6
      if (input%nAuxVars >= 6) state%auxVars(i,6) = state%auxVars(i,6) * ibfac + (1.0_8 - ibfac) * 1d-6
      if (input%nAuxVars >= 7) state%auxVars(i,7) = state%auxVars(i,7) * ibfac + (1.0_8 - ibfac) * 1d-6

    end do

    ! ... set density and total energy, including chemical potential
    do i = 1, grid%nCells

      ibfac = grid%ibfac(i)

      ! ... mass fractions
      Y(1) = state%auxVars(i,1) ! H2
      Y(2) = state%auxVars(i,2) ! O2
      Y(3) = state%auxVars(i,3) ! H2O
      Y(4) = state%auxVars(i,4) ! H
      Y(5) = state%auxVars(i,5) ! HO2
      Y(7) = state%auxVars(i,6) ! OH
      Y(8) = state%auxVars(i,7) ! O
      Y(6) = 1.0_8 - (Y(1)+Y(2)+Y(3)+Y(4)+Y(5)+Y(7)+Y(8)) ! N2

      ! ... compute the mean molecular weight
      MMW = 0.0_8
      do k = 1, 8
        MMW = MMW + Y(k)/W(k)
      end do
      MMW = 1.0_8 / MMW

      ! ... compute the Cp_mix
      Cp_mix = 0.0_8
      do k = 1, 8
        Cp_mix = Cp_mix + Y(k) * Cp(k)
      end do

      ! ... compute Cv_mix
      Cv_mix = Cp_mix - rgas / MMW

      ! ... save gamma
      state%gv(i,1) = Cp_mix / Cv_mix * ibfac + (1.0_8 - ibfac) * GamRef

      ! ... DIMENSIONAL TEMPERATURE
      temperature = TempRef

      ! ... DIMENSIONAL PRESSURE
      pressure = PresRef

      ! ... DIMENSIONAL density
      density = pressure / (rgas/MMW * temperature)
      state%cv(i,1) = density 

      ! ... DIMENSIONAL total energy
      state%cv(i,4) = Cv_mix * (temperature - T0) - rgas * T0 / MMW

      ! ... add in enthalpies of formation
      do k = 1, 8
        state%cv(i,4) = state%cv(i,4) + Y(k) * dh(k)
      end do

      ! ... add in kinetic energy
      state%cv(i,4) = state%cv(i,4) + 0.5_8 * (state%cv(i,2) * state%cv(i,2) + state%cv(i,3) * state%cv(i,3)) * SndSpdRef * SndSpdRef

      ! ... make an energy density
      state%cv(i,4) = state%cv(i,1) * state%cv(i,4)

      ! ... non-dimensionalize
      state%cv(i,1) = state%cv(i,1) / DensRef
      state%cv(i,4) = state%cv(i,4) / (DensRef * SndSpdRef * SndSpdRef)

      ! ... zero out solution in blanked-out regions
      state%cv(i,1) = state%cv(i,1) * ibfac + (1.0_8 - ibfac) * 1.0_8
      state%cv(i,4) = state%cv(i,4) * ibfac + (1.0_8 - ibfac) * (pressure / (DensRef * SndSpdRef * SndSpdRef))

      ! ... multiply density into momentum, Y
      state%cv(i,2) = state%cv(i,2) * state%cv(i,1)
      state%cv(i,3) = state%cv(i,3) * state%cv(i,1)
      do k = 1, input%nAuxVars
        state%auxVars(i,k) = state%auxVars(i,k) * state%cv(i,1)
      end do

    end do


  End Subroutine PSAAP2_2D_Init

  Subroutine PSAAP2_Chemical_Energy(input, grid, state, i, ChemEng, eso)

    USE ModGlobal
    USE ModDataStruct
    USE ModMPI

    Implicit None

    ! ... global variables
    type(t_mixt_input), pointer :: input
    type(t_grid), pointer :: grid
    type(t_mixt), pointer :: state
    integer :: i
    real(rfreal) :: ChemEng, eso

    ! ... local variables
    real(rfreal) :: spvol, Y(8), Cp_mix
    real(rfreal) :: MMW, Cv_mix, gasConstant, gamref
    integer :: k

    ! ... mass fractions
    spvol = state%dv(i,3)
    Y(1)  = state%auxVars(i,1) * spvol ! H2
    Y(2)  = state%auxVars(i,2) * spvol ! O2
    Y(3)  = state%auxVars(i,3) * spvol ! H2O
    Y(4)  = state%auxVars(i,4) * spvol ! H
    Y(5)  = state%auxVars(i,5) * spvol ! HO2
    Y(7)  = state%auxVars(i,6) * spvol ! OH
    Y(8)  = state%auxVars(i,7) * spvol ! O
    Y(6)  = 1.0_8 - (Y(1)+Y(2)+Y(3)+Y(4)+Y(5)+Y(7)+Y(8)) ! N2

    ! ... compute the mean molecular weight
    MMW = 0.0_8
    do k = 1, 8
      MMW = MMW + Y(k)/W(k)
    end do
    MMW = 1.0_8 / MMW

    ! ... compute the Cp_mix
    Cp_mix = 0.0_8
    do k = 1, 8
      Cp_mix = Cp_mix + Y(k) * Cp(k)
    end do

    ! ... compute Cv_mix
    Cv_mix = Cp_mix - rgas / MMW

    ChemEng = -rgas * T0 / MMW
    do k = 1, 8
      ChemEng = ChemEng + Y(k) * dh(k)
    end do

    ChemEng = ChemEng / (input%sndspdref * input%sndspdref)

    ! ... compute internal energy offset
    gasConstant = input%GasConstantRef
    gamref      = input%GamRef
    eso         = (gamref - 1.0_8)/gamref * Cv_mix / gasConstant * T0 / ((gamref-1.0_8) * input%TempRef)

  End Subroutine PSAAP2_Chemical_Energy

  Subroutine PSAAP2_Internal_Energy(input, grid, state, i, temperature, eint)

    USE ModGlobal
    USE ModDataStruct
    USE ModMPI

    Implicit None

    ! ... global variables
    type(t_mixt_input), pointer :: input
    type(t_grid), pointer :: grid
    type(t_mixt), pointer :: state
    integer :: i
    real(rfreal) :: eint, temperature

    ! ... local variables
    real(rfreal) :: spvol, Y(8), Cp_mix
    real(rfreal) :: MMW, Cv_mix, dim_temp_scale
    integer :: k

    ! ... mass fractions
    spvol = state%dv(i,3)
    Y(1)  = state%auxVars(i,1) * spvol ! H2
    Y(2)  = state%auxVars(i,2) * spvol ! O2
    Y(3)  = state%auxVars(i,3) * spvol ! H2O
    Y(4)  = state%auxVars(i,4) * spvol ! H
    Y(5)  = state%auxVars(i,5) * spvol ! HO2
    Y(7)  = state%auxVars(i,6) * spvol ! OH
    Y(8)  = state%auxVars(i,7) * spvol ! O
    Y(6)  = 1.0_8 - (Y(1)+Y(2)+Y(3)+Y(4)+Y(5)+Y(7)+Y(8)) ! N2

    ! ... compute the mean molecular weight
    MMW = 0.0_8
    do k = 1, 8
      MMW = MMW + Y(k)/W(k)
    end do
    MMW = 1.0_8 / MMW

    ! ... compute the Cp_mix
    Cp_mix = 0.0_8
    do k = 1, 8
      Cp_mix = Cp_mix + Y(k) * Cp(k)
    end do

    ! ... compute Cv_mix
    Cv_mix = Cp_mix - rgas / MMW
 
    ! ... compute internal energy
    dim_temp_scale = (input%GamRef - 1.0_8) * input%TempRef
    eint = Cv_mix * (temperature * dim_temp_scale - T0) / (input%sndspdref * input%sndspdref)

  End Subroutine PSAAP2_Internal_Energy

  Subroutine PSAAP2_Combustion_Sources(Nx, T, rho, Y, WOm)

    IMPLICIT none

    integer :: Nx
    integer, parameter :: Ns = 6
    real(8), dimension(Nx)      :: T
    real(8), dimension(Nx)      :: rho
    real(8), dimension(Nx,Ns)   :: Y
    real(8), dimension(Nx,Ns)   :: WOm
    real(8)                     :: max_rate

    real(8), dimension(Nx,Ns+2) :: C

    real(8), dimension(Nx,2,11) :: om
    real(8), dimension(Nx,3)    :: OOm


    real(8), dimension(Nx,2,11) :: kr

    real(8)                     :: cho2p, cho2c, B, Lam, A0, A1, A2

    integer                  :: i,k

    do k = 1, Ns
      do i = 1, Nx
        C(i,k) = RConv*rho(i)*Y(i,k)/W(k)
        C(i,k) = max(dabs(C(i,k)),1d-16)
      end do
    end do

    call PSAAP2_Combustion_Rates(Nx,T,kr,C)
    
    Do i = 1, Nx
      A0 = C(i,1)*kr(i,1,2)*(2.*kr(i,1,1)*C(i,4)*C(i,2) + kr(i,2,3)*C(i,4)*C(i,3) &
                          + 2.*kr(i,1,5)*C(i,4)*C(i,5) + 2.*kr(i,1,10)*C(i,5)**2 &
                          + 2.*kr(i,1,11)*C(i,5)*C(i,1) + kr(i,2,8)*C(i,3))

      A1 = C(i,1)*kr(i,1,2)*(kr(i,1,8)*C(i,4) + kr(i,1,7)*C(i,5) + kr(i,1,3)*C(i,1)) &
               - kr(i,2,1)*(kr(i,2,3)*C(i,4)*C(i,3) + 2.*kr(i,1,5)*C(i,4)*C(i,5) &
               + 2.*kr(i,1,10)*C(i,5)**2 + 2.*kr(i,1,11)*C(i,5)*C(i,1) + kr(i,2,8)*C(i,3))

      A2 = kr(i,2,1)*(2.*kr(i,2,2)*C(i,4) + kr(i,1,3)*C(i,1) + kr(i,1,7)*C(i,5) &
                  + kr(i,1,8)*C(i,4))
   
      C(i,7) = MAX((SQRT(dabs(A1**2 + 4.*A0*A2)) - A1)/(2.*A2),0.0_8)
      C(i,8) = MAX((kr(i,1,1)*C(i,4)*C(i,2) + kr(i,2,2)*C(i,7)*C(i,4))/(kr(i,2,1)*C(i,7) + kr(i,1,2)*C(i,1)),0.0_8)


      om(i,1,1) = kr(i,1,1)*C(i,4)*C(i,2) 
      om(i,2,1) = kr(i,2,1)*C(i,7)*C(i,8) 

      om(i,1,2) = kr(i,1,2)*C(i,1)*C(i,8) 
      om(i,2,2) = kr(i,2,2)*C(i,7)*C(i,4) 

      om(i,1,3) = kr(i,1,3)*C(i,1)*C(i,7) 
      om(i,2,3) = kr(i,2,3)*C(i,3)*C(i,4) 

      om(i,1,4) = kr(i,1,4)*C(i,4)*C(i,2)
      om(i,2,4) = 0.

      om(i,1,5) = kr(i,1,5)*C(i,5)*C(i,4)
      om(i,2,5) = 0.

      om(i,1,6) = kr(i,1,6)*C(i,5)*C(i,4) 
      om(i,2,6) = kr(i,2,6)*C(i,1)*C(i,2) 

      om(i,1,7) = kr(i,1,7)*C(i,5)*C(i,7)
      om(i,2,7) = 0.

      om(i,1,8) = kr(i,1,8)*C(i,4)*C(i,7) 
      om(i,2,8) = kr(i,2,8)*C(i,3)

      om(i,1,9) = kr(i,1,9)*C(i,4)**2  
      om(i,2,9) = kr(i,2,9)*C(i,1)

      om(i,1,10) = kr(i,1,10)*C(i,5)**2 
      om(i,2,10) = 0.

      om(i,1,11) = kr(i,1,11)*C(i,5)*C(i,1)
      om(i,2,11) = 0.


      OOm(i,1) = om(i,1,1) - om(i,2,1) + om(i,1,5) + om(i,1,10)+ om(i,1,11)
      OOm(i,2) = om(i,1,4) + om(i,1,8) - om(i,2,8) + om(i,1,9) - om(i,2,9) - om(i,1,10) - om(i,1,11)
      OOm(i,3) = om(i,1,4) - om(i,1,5) - om(i,1,6) + om(i,2,6) - om(i,1,7) - 2.0_8*om(1,1,10) - om(i,1,11)
    End Do

!!$ Carlos: I am commenting this out because it makes the calculation of the Jacobian more complicated and 
!!$         the errors are only modest for our intended application. 

!!$    do i = 1, Nx
!!$       cho2p = om(i,1,4) + om(i,2,6)
!!$       cho2c = om(i,1,5) + om(i,1,6) + om(i,1,7) + 2.*om(i,1,10) + om(i,1,11)
!!$       if (dabs(cho2p) < 1d-10) CYCLE
!!$       if (ABS(cho2p - cho2c)/cho2p .gt. 0.05) then
!!$          B = 4.*kr(i,1,1)*C(i,2)*(kr(i,1,1)*C(i,2) + (kr(i,1,2) + kr(i,1,3))*C(i,1))/(kr(i,1,2)*kr(i,1,3)*C(i,1)**2)
!!$          Lam = MAX((SQRT(1.+2.*B)-1.)/B,0.)
!!$          OOm(i,:) = OOm(i,:)*Lam
!!$       end if
!!$    end do
    
    Do i = 1, Nx
      WOm(i,1) = W(1)*(-3.0_8*OOm(i,1) + OOm(i,2) - OOm(i,3))
      WOm(i,2) = W(2)*(-OOm(i,2) - OOm(i,3))
      WOm(i,3) = W(3)*(2.0_8*OOm(i,1))
      WOm(i,4) = W(4)*(2.0_8*OOm(i,1) -2.0_8*OOm(i,2) + OOm(i,3))
      WOm(i,5) = W(5)*(OOm(i,3))
      WOm(i,6) = 0.0_8
      WOm(i,:) = WOm(i,:)/RConv
    End Do

  End Subroutine PSAAP2_Combustion_Sources

  Subroutine PSAAP2_Combustion_Rates(Nx,T,kr,C)

    IMPLICIT None

    integer                     :: Nx
    real(8), dimension(Nx)      :: T
    real(8), dimension(Nx,8)    :: C
    real(8), dimension(Nx,2,11) :: kr

    real(8), dimension(Nx)      :: k0
    real(8), dimension(Nx)      :: kinf
    real(8), dimension(Nx)      :: Cm,pr,F

    integer                     :: i,k

    Do i = 1, Nx
      kr(i,1,1) = A1f*T(i)**n1f*EXP(-T1f/T(i))
      kr(i,2,1) = A1b*T(i)**n1b*EXP(-T1b/T(i))

      kr(i,1,2) = A2f*T(i)**n2f*EXP(-T2f/T(i))
      kr(i,2,2) = A2b*T(i)**n2b*EXP(-T2b/T(i))

      kr(i,1,3) = A3f*T(i)**n3f*EXP(-T3f/T(i))
      kr(i,2,3) = A3b*T(i)**n3b*EXP(-T3b/T(i))
    End Do

    Cm(:) = 0.0_8
    do k = 1,6   !  neglect C(7) and C(8)
       do i = 1,Nx
         Cm(i) = Cm(i) + X4(k)*C(i,k)
       end do
    end do
    
    do i = 1,Nx
      k0(i) = A4o*T(i)**n4o  
      kinf(i) = A4inf*T(i)**n4inf  
      pr(i) = k0(i)*Cm(i)/kinf(i)
    end do

    call getFTroe(Nx,Fc4,pr,F)

    Do i = 1,Nx
      kr(i,1,4) = kinf(i)*F(i)*pr(i)/(1.+pr(i))
      kr(i,2,4) = 0.0_8
    
      kr(i,1,5) = A5*EXP(-T5/T(i))
      kr(i,2,5) = 0.0_8

      kr(i,1,6) = A6f*EXP(-T6f/T(i))
      kr(i,2,6) = A6b*T(i)**n6b*EXP(-T6b/T(i))

      kr(i,1,7) = A7*EXP(-T7/T(i))
      kr(i,2,7) = 0.0_8
    End Do

    do i = 1,Nx
      Cm(i) = 0.0_8
      do k = 1,6  ! neglect C(7) and C(8)
         Cm(i) = Cm(i) + X8(k)*C(i,k)
      end do
    end do

    do i = 1,Nx
      kr(i,1,8) = A8f*T(i)**n8f*Cm(i)
      kr(i,2,8) = A8b*T(i)**n8b*Cm(i)*EXP(-T8b/T(i))

      kr(i,1,9) = A9f*T(i)**n9f*Cm(i) 
      kr(i,2,9) = A9b*T(i)**n9b*Cm(i)*EXP(-T9b/T(i))

      kr(i,1,10) = A10*EXP(-T10/T(i))
      kr(i,2,10) = 0.0_8

      kr(i,1,11) = A11*T(i)**n11*EXP(-T11/T(i))
      kr(i,2,11) = 0.0_8
    end do

  END SUBROUTINE PSAAP2_Combustion_Rates

  SUBROUTINE getFTroe(Nx,Fc,pr,F)

    integer                   :: Nx
    real(8)                   :: Fc
    real(8), dimension(Nx)    :: pr,F

    integer                   :: i
    real(8), dimension(Nx)    :: lpr
    real(8)                   :: cc,nn

    cc = -0.4 - 0.67*LOG10(Fc)
    nn = -0.75 - 1.27*LOG10(Fc)

    do i = 1, Nx
      lpr(i) = LOG10(pr(i) + 1d-14)
      F(i) = Fc*10**( -1. - ((lpr(i)+cc)/(nn - 0.14*(lpr(i)+cc)))**2 )
    end do
    
  END SUBROUTINE getFTroe

  Subroutine PSAAP2_2D_Update_Target(input, grid, state)

    USE ModGlobal
    USE ModDataStruct
    USE ModMPI

    Implicit None

    ! ... global variables
    type(t_mixt_input), pointer :: input
    type(t_grid), pointer :: grid
    type(t_mixt), pointer :: state

    ! ... local variables
    integer :: i, k
    real(rfreal) :: TempRef, DensRef, SndSpdRef, PresRef, Y(8), MMW, Cp_mix, Cv_mix, eta, etay, etax
    real(rfreal) :: temperature, pressure, density, ibfac
    real(rfreal), parameter :: tstart = 0.0_8, tend = 100.0_8
    real(rfreal) :: Ystart(8), Yend(8), Uystart, Uyend, Uy, Tempstart, Tempend, Temp, delt

    ! ... reference values
    TempRef   = input%tempref
    DensRef   = input%densref
    SndSpdRef = input%sndspdref
    PresRef   = input%presref

    ! ... Starting and Ending Values
    Ystart(1)   = 1.0d-6;  Yend(1)   = 1.0_8 - 7d-6;
    Ystart(2)   = 2.3d-1;  Yend(2)   = 1d-6;
    Ystart(3:7) = 1.0d-6;  Yend(3:7) = 1d-6;
    Uystart     = 0.0d0;   Uyend     = 3.0_8*input%initflow_mach
    Tempstart   = TempRef; TempEnd   = 1000.0_8
    
    ! ... current values
    if (state%time(1) .gt. tend) then
      Y(1:7) = Yend(1:7)
      Uy     = Uyend
      temperature = TempEnd
    else
      delt = (state%time(1) - tstart)
      Y(1:7) = Ystart(1:7) + (Yend(1:7)-Ystart(1:7))/(tend-tstart)*delt
      Uy     = Uystart + (Uyend - Uystart)/(tend-tstart)*delt
      temperature = TempStart + (TempEnd-TempStart)/(tend-tstart)*delt
    end if

    do i = 1, grid%nCells

      ibfac = grid%ibfac(i)
      
      ! ... transverse jet
      if (grid%XYZ(i,2) <= 1.0_8 .and. grid%XYZ(i,1) >= 5.0_8 .and. grid%XYZ(i,1) <= 6.0_8) then
        etax = (0.5_8 - dabs(5.5_8 - grid%XYZ(i,1))) / 0.2_8
        state%cvTarget(i,2) = 0.0_8
        if (etax <= 1.0_8) then
          state%cvTarget(i,3) = (2.0_8 * (1.0_8 - etax*etax)*etax + etax*etax*etax*etax)*Uy
          if (input%nAuxVars >= 1) state%auxVarsTarget(i,1) = Y(1)
          if (input%nAuxVars >= 2) state%auxVarsTarget(i,2) = Y(2)
          if (input%nAuxVars >= 3) state%auxVarsTarget(i,3) = Y(3)
          if (input%nAuxVars >= 4) state%auxVarsTarget(i,4) = Y(4)
          if (input%nAuxVars >= 5) state%auxVarsTarget(i,5) = Y(5)
          if (input%nAuxVars >= 6) state%auxVarsTarget(i,6) = Y(6)
          if (input%nAuxVars >= 7) state%auxVarsTarget(i,7) = Y(7)
        else
          state%cvTarget(i,3) = Uy
          if (input%nAuxVars >= 1) state%auxVarsTarget(i,1) = Y(1)
          if (input%nAuxVars >= 2) state%auxVarsTarget(i,2) = Y(2)
          if (input%nAuxVars >= 3) state%auxVarsTarget(i,3) = Y(3)
          if (input%nAuxVars >= 4) state%auxVarsTarget(i,4) = Y(4)
          if (input%nAuxVars >= 5) state%auxVarsTarget(i,5) = Y(5)
          if (input%nAuxVars >= 6) state%auxVarsTarget(i,6) = Y(6)
          if (input%nAuxVars >= 7) state%auxVarsTarget(i,7) = Y(7)
        end if
      end if
    end do

    ! ... set density and total energy, including chemical potential
    do i = 1, grid%nCells

      ! ... transverse jet
      if (grid%XYZ(i,2) <= 1.0_8 .and. grid%XYZ(i,1) >= 5.0_8 .and. grid%XYZ(i,1) <= 6.0_8) then

        ! ... mass fractions
        Y(1) = state%auxVarsTarget(i,1) ! H2
        Y(2) = state%auxVarsTarget(i,2) ! O2
        Y(3) = state%auxVarsTarget(i,3) ! H2O
        Y(4) = state%auxVarsTarget(i,4) ! H
        Y(5) = state%auxVarsTarget(i,5) ! HO2
        Y(7) = state%auxVarsTarget(i,6) ! OH
        Y(8) = state%auxVarsTarget(i,7) ! O
        Y(6) = 1.0_8 - (Y(1)+Y(2)+Y(3)+Y(4)+Y(5)+Y(7)+Y(8)) ! N2

        ! ... compute the mean molecular weight
        MMW = 0.0_8
        do k = 1, 8
          MMW = MMW + Y(k)/W(k)
        end do
        MMW = 1.0_8 / MMW

        ! ... compute the Cp_mix
        Cp_mix = 0.0_8
        do k = 1, 8
          Cp_mix = Cp_mix + Y(k) * Cp(k)
        end do

        ! ... compute Cv_mix
        Cv_mix = Cp_mix - rgas / MMW

        ! ... DIMENSIONAL PRESSURE
        pressure = PresRef

        ! ... DIMENSIONAL density
        density = pressure / (rgas/MMW * temperature)
        state%cvTarget(i,1) = density 

        ! ... DIMENSIONAL total energy
        state%cvTarget(i,4) = Cv_mix * (temperature - T0) - rgas * T0 / MMW

        ! ... add in enthalpies of formation
        do k = 1, 8
          state%cvTarget(i,4) = state%cvTarget(i,4) + Y(k) * dh(k)
        end do

        ! ... add in kinetic energy
        state%cvTarget(i,4) = state%cvTarget(i,4) + 0.5_8 * (state%cvTarget(i,2) * state%cvTarget(i,2) + state%cvTarget(i,3) * state%cvTarget(i,3)) * SndSpdRef * SndSpdRef

        ! ... make an energy density
        state%cvTarget(i,4) = state%cvTarget(i,1) * state%cvTarget(i,4)

        ! ... non-dimensionalize
        state%cvTarget(i,1) = state%cvTarget(i,1) / DensRef
        state%cvTarget(i,4) = state%cvTarget(i,4) / (DensRef * SndSpdRef * SndSpdRef)

        ! ... multiply density into momentum, Y
        state%cvTarget(i,2) = state%cvTarget(i,2) * state%cvTarget(i,1)
        state%cvTarget(i,3) = state%cvTarget(i,3) * state%cvTarget(i,1)
        do k = 1, input%nAuxVars
          state%auxVarsTarget(i,k) = state%auxVarsTarget(i,k) * state%cvTarget(i,1)
        end do
      end if
    end do

  End Subroutine PSAAP2_2D_Update_Target

End Module ModPSAAP2_CP
![/UNUSED]
