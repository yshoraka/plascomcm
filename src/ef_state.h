#ifndef EF_STATE_H
#define EF_STATE_H


/**
 * Contains PlasComCM points to state variables
 * see ef_interface.h for descriptions of members
 */
typedef struct {
	double *phi;
	double *jump;
	double *dcoef;
	double *bcoef;
	double *rhs;
	double *sol;
} ef_state;


#endif
