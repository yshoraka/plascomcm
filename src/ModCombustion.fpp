! Copyright (c) 2015, University of Illinois at Urbana-Champaign, XPACC
! License: MIT, http://opensource.org/licenses/MIT
Module ModCombustion

  USE ModOnestep
  USE ModModel0
  USE ModModel1

Contains

  Subroutine combustionInit(region, ng)
    USE ModGlobal
    USE ModDataStruct

    Implicit None

    Type(t_region), pointer :: region
    Integer, intent(in) :: ng

    ! ... local parameters
    Type(t_mixt_input), pointer :: input, regionInput
    Type(t_combustion), pointer :: combustion
    Type(t_param), pointer :: first_param_ptr
    Integer :: myrank, gridComm

    ! ... simplify
    input           => region%grid(ng)%input
    regionInput     => region%input
    combustion      => region%state(ng)%combustion
    first_param_ptr => region%first_param_ptr
    myrank = region%myrank
    gridComm = region%grid(ng)%comm

    ! ... initialize the combustion routines
    select case (input%chemistry_model)

    case (ONESTEP)

       allocate(combustion%onestep)
       call OnestepInit(combustion%onestep, input, first_param_ptr, myrank)

    case (MODEL0)

       allocate(combustion%model0)
       if (ng == 1) then
          call Model0Init(combustion%model0, input, first_param_ptr, myrank, gridComm, regionInput)
       else
          call Model0Init(combustion%model0, input, first_param_ptr, myrank, gridComm)
       end if

       ! Store the molecular weights to be used later
       combustion%MwCoeff => combustion%model0%MwCoeff
       combustion%MwCoeffRho = combustion%model0%MwCoeffRho

    case (MODEL1)

       allocate(combustion%model1)
       if (ng == 1) then
          call Model1Init(combustion%model1, input, first_param_ptr, myrank, gridComm, regionInput)
       else
          call Model1Init(combustion%model1, input, first_param_ptr, myrank, gridComm)
       end if

       ! Store the molecular weights to be used later
       combustion%MwCoeff => combustion%model1%MwCoeff
       combustion%MwCoeffRho = combustion%model1%MwCoeffRho

    end select

  End Subroutine combustionInit

  Subroutine combustionSource(region, ng)
    USE ModGlobal
    USE ModDataStruct

    Implicit None

    Type(t_region), pointer :: region
    Integer, intent(in) :: ng

    ! ... local parameters
    Type(t_mixt_input), pointer :: input
    Type(t_mixt), pointer :: state
    Type(t_grid), pointer :: grid
    Type(t_combustion), pointer :: combustion
    Integer :: myrank

    ! ... simplify
    input => region%input
    grid  => region%grid(ng)
    state => region%state(ng)
    combustion => state%combustion
    myrank = region%myrank

    ! ... initialize the combustion routines
    select case (input%chemistry_model)

    case (ONESTEP)

       call OnestepSource(combustion%onestep, state%cv, state%dv, state%auxVars, state%rhs, state%rhs_AuxVars, grid%iblank, input)

    case (MODEL0)

       call Model0Source(combustion%model0, grid%xyz,state%cv, state%dv, state%auxVars, state%rhs, state%rhs_AuxVars, grid%iblank, input, myrank)

    case (MODEL1)

       call Model1Source(combustion%model1, grid%xyz, state%cv, state%dv, state%auxVars, state%auxVarsSteady, state%rhs_AuxVars, grid%iblank, input, myrank, combustion%iRKstage)

    end select

  End Subroutine combustionSource

  Subroutine combustionTv(grid, state)
    USE ModGlobal
    USE ModDataStruct

    Implicit None

    Type(t_mixt), pointer :: state
    Type(t_grid), pointer :: grid

    ! ... local parameters
    Type(t_mixt_input), pointer :: input
    Type(t_combustion), pointer :: combustion

    ! ... simplify
    input => grid%input
    combustion => state%combustion

    ! ... initialize the combustion routines
    select case (input%chemistry_model)

    case (ONESTEP)

       ! ... nothing to do
       return

    case (MODEL0)

       call Model0Tv(combustion%model0, grid, state)

    case (MODEL1)

       call Model1Tv(combustion%model1, grid, state)

    end select

  End Subroutine combustionTv
  
End Module ModCombustion
