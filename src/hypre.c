/* Copyright (c) 2015, University of Illinois at Urbana-Champaign, XPACC */
/* License: MIT, http://opensource.org/licenses/MIT */
/*
 * hypre.c --- contains all necessary routines for using HYPRE with PlasComCM for implicit time advancement
 */

#include <stdio.h>
#include <stdlib.h>
#ifndef USE_HYPRE
#include "mpi.h"
#endif

/* HYPRE-specific headers */
#ifdef USE_HYPRE
#include "HYPRE_utilities.h"
#include "HYPRE_sstruct_ls.h"
#include "HYPRE_krylov.h"


/* HYPRE-specific variables */
HYPRE_SStructGrid     *grid = NULL;
HYPRE_SStructGraph    *graph = NULL;
HYPRE_SStructStencil  *stencil_r  = NULL;
HYPRE_SStructStencil  *stencil_ru = NULL;
HYPRE_SStructStencil  *stencil_rv = NULL;
HYPRE_SStructStencil  *stencil_rw = NULL;
HYPRE_SStructStencil  *stencil_rE = NULL;
HYPRE_SStructMatrix   *A = NULL;
HYPRE_SStructVector   *b = NULL;
HYPRE_SStructVector   *x = NULL;

/* We are using struct solvers for this example */
HYPRE_StructSolver *solver = NULL;
HYPRE_StructSolver *pc = NULL;

/* PARCSR data */
HYPRE_ParCSRMatrix   *par_A = NULL;
HYPRE_ParVector      *par_b = NULL;
HYPRE_ParVector      *par_x = NULL;
HYPRE_Solver         *par_solver = NULL;
HYPRE_Solver         *par_pc = NULL;
HYPRE_Solver         *eu = NULL;
#endif

/* include global variables here */
int *nparts = NULL;
int *part   = NULL;
int *nvars  = NULL;

void check_hypre_ierr(int ierr, int line, char *fname)
{

  if (ierr != 0) {
    printf("RFLOCM: HYPRE: ERROR: ierr != 0 at line %d in file %s\n", line, fname);
    exit(-1);
  }

}


void allocate_hypre_objects_(int *region_nGrids, int *myrank, int *ndim) 
{
 
#ifdef USE_HYPRE
  /* allocate memory, if needed */
  if (nparts == NULL) {

     nparts = (int *)malloc((*region_nGrids)*sizeof(int));
      nvars = (int *)malloc((*region_nGrids)*sizeof(int));
       grid = (HYPRE_SStructGrid *)malloc((*region_nGrids)*sizeof(HYPRE_SStructGrid));
      graph = (HYPRE_SStructGraph *)malloc((*region_nGrids)*sizeof(HYPRE_SStructGraph));
          A = (HYPRE_SStructMatrix *)malloc((*region_nGrids)*sizeof(HYPRE_SStructMatrix));
          b = (HYPRE_SStructVector *)malloc((*region_nGrids)*sizeof(HYPRE_SStructVector));
          x = (HYPRE_SStructVector *)malloc((*region_nGrids)*sizeof(HYPRE_SStructVector));
     solver = (HYPRE_StructSolver *)malloc((*region_nGrids)*sizeof(HYPRE_StructSolver));
         pc = (HYPRE_StructSolver *)malloc((*region_nGrids)*sizeof(HYPRE_StructSolver));
      par_A = (HYPRE_ParCSRMatrix *)malloc((*region_nGrids)*sizeof(HYPRE_ParCSRMatrix));
      par_b = (HYPRE_ParVector *)malloc((*region_nGrids)*sizeof(HYPRE_ParVector));
      par_x = (HYPRE_ParVector *)malloc((*region_nGrids)*sizeof(HYPRE_ParVector));
 par_solver = (HYPRE_Solver *)malloc((*region_nGrids)*sizeof(HYPRE_Solver));
     par_pc = (HYPRE_Solver *)malloc((*region_nGrids)*sizeof(HYPRE_Solver));
         eu = (HYPRE_Solver *)malloc((*region_nGrids)*sizeof(HYPRE_Solver));

	/* stencil arrays, which vary by dimension of problem */
        if ( (*ndim) == 1) {
          stencil_r  = (HYPRE_SStructStencil *)malloc((*region_nGrids)*sizeof(HYPRE_SStructStencil));
          stencil_ru = (HYPRE_SStructStencil *)malloc((*region_nGrids)*sizeof(HYPRE_SStructStencil));
          stencil_rE = (HYPRE_SStructStencil *)malloc((*region_nGrids)*sizeof(HYPRE_SStructStencil));
        } else if ( (*ndim) == 2) {
          stencil_r  = (HYPRE_SStructStencil *)malloc((*region_nGrids)*sizeof(HYPRE_SStructStencil));
          stencil_ru = (HYPRE_SStructStencil *)malloc((*region_nGrids)*sizeof(HYPRE_SStructStencil));
          stencil_rv = (HYPRE_SStructStencil *)malloc((*region_nGrids)*sizeof(HYPRE_SStructStencil));
          stencil_rE = (HYPRE_SStructStencil *)malloc((*region_nGrids)*sizeof(HYPRE_SStructStencil));
        } else if ( (*ndim) == 3) {
          stencil_r  = (HYPRE_SStructStencil *)malloc((*region_nGrids)*sizeof(HYPRE_SStructStencil));
          stencil_ru = (HYPRE_SStructStencil *)malloc((*region_nGrids)*sizeof(HYPRE_SStructStencil));
          stencil_rv = (HYPRE_SStructStencil *)malloc((*region_nGrids)*sizeof(HYPRE_SStructStencil));
          stencil_rw = (HYPRE_SStructStencil *)malloc((*region_nGrids)*sizeof(HYPRE_SStructStencil));
          stencil_rE = (HYPRE_SStructStencil *)malloc((*region_nGrids)*sizeof(HYPRE_SStructStencil));
        }

    // if (*myrank == 0) printf("RFLOCM: HYPRE: Allocated HYPRE High-level objects for implicit time advancement.\n");

  }
#endif
  
}


void init_hypre_objects_(int *nGridsGlobal, int *iGridGlobal, int *ng, int *region_nGrids, 
                         int *myrank, int *numproc, MPI_Fint *fcomm, int *fndim, int *is, int *ie, 
                         int *stencil_start, int *stencil_end, int *periodic) 
{

#ifdef USE_HYPRE
  int i, j, k, count, ierr, ndim, stencil_size, var, part;
  MPI_Comm comm;

  /* default values */
           ndim = *fndim;
  nparts[*ng-1] = 1;
   nvars[*ng-1] = ndim + 2;

  /* convert communicator */
  comm = (MPI_Comm)MPI_Comm_f2c(*fcomm);

  /* create the empty grid object */
  ierr = HYPRE_SStructGridCreate(comm, ndim, nparts[*ng-1], &(grid[*ng-1]));
  // printf("RFLOCM: HYPRE: Created empty SStruct Grid object on processor %d for grid %d\n", *myrank, *iGridGlobal);

  if (ierr != 0) {
    printf("RFLOCM: HYPRE: ERROR: ierr != 0 at line %d in file %s\n", __LINE__, __FILE__);
    exit(-1);
  }

  /* set the grid extents */
  int *ilower = NULL, *iupper = NULL;
  ilower = (int *)malloc(ndim*sizeof(int));
  iupper = (int *)malloc(ndim*sizeof(int));
  for (i = 0; i < ndim; i++) {
    ilower[i] = is[i]-1;
    iupper[i] = ie[i]-1;
  }
  for (part = 0; part < nparts[*ng-1]; part++) ierr += HYPRE_SStructGridSetExtents(grid[*ng-1], part, ilower, iupper);
  // printf("RFLOCM: HYPRE: Set grid extents on processor %d for grid %d\n", *myrank, *iGridGlobal);
  check_hypre_ierr(ierr, __LINE__, __FILE__);

  /* apply periodicity : assumes 1-D stencils */
  int dir;
  for (dir = 1; dir <= ndim; dir ++) {
  if (periodic[dir-1] > 0) {

    int cur_exts[3][2], new_exts[3][2];
    int index_map[3] = {0, 1, 2};
    int index_dir[3] = {1, 1, 1};

    /* initialize extents */
    for (j = 0; j < ndim; j++) {
      cur_exts[0][j] = is[j]-1;
      cur_exts[1][j] = ie[j]-1;
      new_exts[0][j] = cur_exts[0][j];
      new_exts[1][j] = cur_exts[1][j];
    }

    /* sitting on the right, looking to the far left */
    /* assumes a simple 3-D decomposition */
    if (ie[dir-1] == periodic[dir-1]) {

      cur_exts[0][dir-1] = periodic[dir-1];
      cur_exts[1][dir-1] = periodic[dir-1];
      new_exts[0][dir-1] = 1;
      new_exts[1][dir-1] = 1;

      /* new indices are along the right edge */
      ierr += HYPRE_SStructGridSetNeighborPart(grid[*ng-1], 0, cur_exts[0], cur_exts[1], 0, new_exts[0], new_exts[1], index_map, index_dir);
      check_hypre_ierr(ierr, __LINE__, __FILE__);

    }

    /* initialize extents */
    for (j = 0; j < ndim; j++) {
      cur_exts[0][j] = is[j]-1;
      cur_exts[1][j] = ie[j]-1;
      new_exts[0][j] = cur_exts[0][j];
      new_exts[1][j] = cur_exts[1][j];
    }

    /* sitting on the left, looking to the far right */
    /* assumes a simple 3-D decomposition */
    if (is[dir-1] == 1) {
      cur_exts[0][dir-1] = -1;
      cur_exts[1][dir-1] = -1;
      new_exts[0][dir-1] = periodic[dir-1]-2;
      new_exts[1][dir-1] = periodic[dir-1]-2;

      /* new indices are along the right edge */
      ierr += HYPRE_SStructGridSetNeighborPart(grid[*ng-1], 0, cur_exts[0], cur_exts[1], 0, new_exts[0], new_exts[1], index_map, index_dir);
      check_hypre_ierr(ierr, __LINE__, __FILE__);
    }
  }
  }


  /* set the variable type for each part */
  HYPRE_SStructVariable *vartypes = NULL;
  vartypes = (HYPRE_SStructVariable *)malloc(nvars[*ng-1]*sizeof(HYPRE_SStructVariable));
  for (i = 0; i < nvars[*ng-1]; i++) vartypes[i] = HYPRE_SSTRUCT_VARIABLE_CELL;
  // for (i = 0; i < nvars[*ng-1]; i++) vartypes[i] = HYPRE_SSTRUCT_VARIABLE_NODE;
  for (part = 0; part < nparts[*ng-1]; part++) 
    ierr += HYPRE_SStructGridSetVariables(grid[*ng-1], part, nvars[*ng-1], vartypes);
  // printf("RFLOCM: HYPRE: Set variable type on processor %d for grid %d\n", *myrank, *iGridGlobal);
  check_hypre_ierr(ierr, __LINE__, __FILE__);
  free(vartypes);

  // int nghost[6] = {3,3,3,3,3,3};
  // ierr += HYPRE_SStructGridSetNumGhost(grid[*ng-1], nghost);
  // check_hypre_ierr(ierr, __LINE__, __FILE__);

  /* This is a collective call finalizing the grid assembly.
     The grid is now ``ready to be used'' */
  ierr += HYPRE_SStructGridAssemble(grid[*ng-1]);
  // printf("RFLOCM: HYPRE: Assembled SStruct grid on processor %d for grid %d\n", *myrank, *iGridGlobal);
  check_hypre_ierr(ierr, __LINE__, __FILE__);

  /* setup the stencil object */
  /* assumes no cross derivatives */
  stencil_size = ndim * ( (*stencil_end) - (*stencil_start) ) + 1; 
  ierr += HYPRE_SStructStencilCreate(ndim, stencil_size*(ndim+2), &(stencil_r[*ng-1]));
  ierr += HYPRE_SStructStencilCreate(ndim, stencil_size*(ndim+2), &(stencil_ru[*ng-1]));
  ierr += HYPRE_SStructStencilCreate(ndim, stencil_size*(ndim+2), &(stencil_rE[*ng-1]));
  if ( ndim >= 2 ) ierr += HYPRE_SStructStencilCreate(ndim, stencil_size*(ndim+2), &(stencil_rv[*ng-1]));
  if ( ndim == 3 ) ierr += HYPRE_SStructStencilCreate(ndim, stencil_size*(ndim+2), &(stencil_rw[*ng-1]));
  // printf("RFLOCM: HYPRE: Created stencil object on processor %d for grid %d with %d points\n", *myrank, *iGridGlobal, stencil_size);
  check_hypre_ierr(ierr, __LINE__, __FILE__);

  /* setup the stencil itself */
  /* counting: point 0 is always self, 
     then from min-to-max in direction 1, 
     then from min-to-max in direction 2, 
     then from min-to-max in direction 3 */
  int stencil1[9][1] = {{0},{-1},{1},  /* rho */
                        {0},{-1},{1},  /* rho u_x */
                        {0},{-1},{1}}; /* rho E */

  int **stencil2 = NULL;
  stencil2 = (int **)malloc(nvars[*ng-1]*(stencil_size)*sizeof(int *));
  for (i = 0; i < nvars[*ng-1]*(stencil_size); i++) stencil2[i] = (int *)malloc(2*sizeof(int));

  if (ndim == 2) {

    /* fill in the self indices */
    for (var = 0; var < nvars[*ng-1]; var++) {
      stencil2[var*stencil_size][0] = 0;
      stencil2[var*stencil_size][1] = 0;
    }
    
    /* fill in the x-stencils */
    for (var = 0; var < nvars[*ng-1]; var++) {
      count = 1;
      for (i = (*stencil_start); i <= (*stencil_end); i++) { 
        if (i != 0) {
          stencil2[var*stencil_size+count][0] = i;
          stencil2[var*stencil_size+count][1] = 0;
          count++;
        }
      }
    }

    /* fill in the y-stencils */
    for (var = 0; var < nvars[*ng-1]; var++) {
      count = 1+(*stencil_end)-(*stencil_start);
      for (i = (*stencil_start); i <= (*stencil_end); i++) { 
        if (i != 0) {
          stencil2[var*stencil_size+count][0] = 0;
          stencil2[var*stencil_size+count][1] = i;
          count++;
        }
      }
    }

  }

/* for (var = 0; var < nvars[*ng-1]; var++) { */
/*     for (i = 0; i < stencil_size; i++) { */
/*       printf("{%d,%d}, ", stencil2[var*stencil_size+i][0], stencil2[var*stencil_size+i][1]); */
/*     } */
/*     printf("\n"); */
/*   } */
/*  exit(0); */



  int **stencil3 = NULL;
  stencil3 = (int **)malloc(nvars[*ng-1]*(stencil_size)*sizeof(int *));
  for (i = 0; i < nvars[*ng-1]*(stencil_size); i++) stencil3[i] = (int *)malloc(3*sizeof(int));

  if (ndim == 3) {

    /* fill in the self indices */
    for (var = 0; var < nvars[*ng-1]; var++) {
      stencil3[var*stencil_size][0] = 0;
      stencil3[var*stencil_size][1] = 0;
      stencil3[var*stencil_size][2] = 0;
    }
    
    /* fill in the x-stencils */
    for (var = 0; var < nvars[*ng-1]; var++) {
      count = 1;
      for (i = (*stencil_start); i <= (*stencil_end); i++) { 
        if (i != 0) {
          stencil3[var*stencil_size+count][0] = i;
          stencil3[var*stencil_size+count][1] = 0;
          stencil3[var*stencil_size+count][2] = 0;
          count++;
        }
      }
    }

    /* fill in the y-stencils */
    for (var = 0; var < nvars[*ng-1]; var++) {
      count = 1+(*stencil_end)-(*stencil_start);
      for (i = (*stencil_start); i <= (*stencil_end); i++) { 
        if (i != 0) {
          stencil3[var*stencil_size+count][0] = 0;
          stencil3[var*stencil_size+count][1] = i;
          stencil3[var*stencil_size+count][2] = 0;
          count++;
        }
      }
    }

   /* fill in the z-stencils */
    for (var = 0; var < nvars[*ng-1]; var++) {
      count = 1+2*((*stencil_end)-(*stencil_start));
      for (i = (*stencil_start); i <= (*stencil_end); i++) { 
        if (i != 0) {
          stencil3[var*stencil_size+count][0] = 0;
          stencil3[var*stencil_size+count][1] = 0;
          stencil3[var*stencil_size+count][2] = i;
          count++;
        }
      }
    }
  }

  /* for (var = 0; var < nvars[*ng-1]; var++) { */
  /*   for (i = 0; i < stencil_size; i++) { */
  /*     printf("{%d,%d,%d}, ", stencil3[var*stencil_size+i][0], stencil3[var*stencil_size+i][1],stencil3[var*stencil_size+i][2]); */
  /*   } */
  /*   printf("\n"); */
  /* } */


  if (ndim == 1) {
    /* flux jabobian matrix is full; each variable depends on the others */
    for (var = 0; var < nvars[*ng-1]; var++) {
      for (j = 0; j < stencil_size; j++) {
        ierr += HYPRE_SStructStencilSetEntry( stencil_r[*ng-1], var*stencil_size+j, stencil1[j], var);
        ierr += HYPRE_SStructStencilSetEntry(stencil_ru[*ng-1], var*stencil_size+j, stencil1[j], var);
        ierr += HYPRE_SStructStencilSetEntry(stencil_rE[*ng-1], var*stencil_size+j, stencil1[j], var);
      }
    }
  } else if (ndim == 2) {
    /* flux jabobian matrix is full; each variable depends on the others */
    for (var = 0; var < nvars[*ng-1]; var++) {
      for (j = 0; j < stencil_size; j++) {
        ierr += HYPRE_SStructStencilSetEntry( stencil_r[*ng-1], var*stencil_size+j, stencil2[j], var);
        ierr += HYPRE_SStructStencilSetEntry(stencil_ru[*ng-1], var*stencil_size+j, stencil2[j], var);
        ierr += HYPRE_SStructStencilSetEntry(stencil_rv[*ng-1], var*stencil_size+j, stencil2[j], var);
        ierr += HYPRE_SStructStencilSetEntry(stencil_rE[*ng-1], var*stencil_size+j, stencil2[j], var);
      }
    }
  } else if (ndim == 3) {
    /* flux jabobian matrix is full; each variable depends on the others */
    for (var = 0; var < nvars[*ng-1]; var++) {
      for (j = 0; j < stencil_size; j++) {
        ierr += HYPRE_SStructStencilSetEntry( stencil_r[*ng-1], var*stencil_size+j, stencil3[j], var);
        ierr += HYPRE_SStructStencilSetEntry(stencil_ru[*ng-1], var*stencil_size+j, stencil3[j], var);
        ierr += HYPRE_SStructStencilSetEntry(stencil_rv[*ng-1], var*stencil_size+j, stencil3[j], var);
        ierr += HYPRE_SStructStencilSetEntry(stencil_rw[*ng-1], var*stencil_size+j, stencil3[j], var);
        ierr += HYPRE_SStructStencilSetEntry(stencil_rE[*ng-1], var*stencil_size+j, stencil3[j], var);
      }
    }
  }
  // printf("RFLOCM: HYPRE: Setup the stencils on processor %d for grid %d with %d points\n", *myrank, *iGridGlobal, stencil_size);
  check_hypre_ierr(ierr, __LINE__, __FILE__);

  /* set up the graph */
  ierr += HYPRE_SStructGraphCreate(comm, grid[*ng-1], &(graph[*ng-1]));
  // printf("RFLOCM: HYPRE: Created graph object on processor %d for grid %d with %d points\n", *myrank, *iGridGlobal, stencil_size);
  check_hypre_ierr(ierr, __LINE__, __FILE__);
  for (part = 0; part < nparts[*ng-1]; part++) {
    var = 0;      ierr += HYPRE_SStructGraphSetStencil(graph[*ng-1], part, var,  stencil_r[*ng-1]);
    var = 1;      ierr += HYPRE_SStructGraphSetStencil(graph[*ng-1], part, var, stencil_ru[*ng-1]);
    var = ndim+1; ierr += HYPRE_SStructGraphSetStencil(graph[*ng-1], part, var, stencil_rE[*ng-1]);
    if (ndim >= 2) {var = 2; ierr += HYPRE_SStructGraphSetStencil(graph[*ng-1], part, var, stencil_rv[*ng-1]);}
    if (ndim == 3) {var = 3; ierr += HYPRE_SStructGraphSetStencil(graph[*ng-1], part, var, stencil_rw[*ng-1]);}
  }
  // printf("RFLOCM: HYPRE: Created graph stencil on processor %d for grid %d with %d points\n", *myrank, *iGridGlobal, stencil_size);
  check_hypre_ierr(ierr, __LINE__, __FILE__);
  ierr += HYPRE_SStructGraphAssemble(graph[*ng-1]);
  // printf("RFLOCM: HYPRE: Assembled graph on processor %d for grid %d with %d points\n", *myrank, *iGridGlobal, stencil_size);
  check_hypre_ierr(ierr, __LINE__, __FILE__);

  /* Create the operator A */
  ierr += HYPRE_SStructMatrixCreate(comm, graph[*ng-1], &(A[*ng-1]));
  // printf("RFLOCM: HYPRE: Created empty operator A on processor %d for grid %d.\n", *myrank, *iGridGlobal);
  check_hypre_ierr(ierr, __LINE__, __FILE__);
  //  int object_type = HYPRE_SSTRUCT;
  int object_type = HYPRE_PARCSR;
  ierr += HYPRE_SStructMatrixSetObjectType(A[*ng-1], object_type);
  // printf("RFLOCM: HYPRE: Set operator A for object_type = HYPRE_STRUCT on processor %d for grid %d.\n", *myrank, *iGridGlobal);
  check_hypre_ierr(ierr, __LINE__, __FILE__);
  ierr += HYPRE_SStructMatrixInitialize(A[*ng-1]);
  // printf("RFLOCM: HYPRE: Initialized operator A on processor %d for grid %d.\n", *myrank, *iGridGlobal);
  check_hypre_ierr(ierr, __LINE__, __FILE__);

  /* setup solution and rhs vectors */
  ierr += HYPRE_SStructVectorCreate(comm, grid[*ng-1], &(b[*ng-1]));
  ierr += HYPRE_SStructVectorCreate(comm, grid[*ng-1], &(x[*ng-1]));
  // printf("RFLOCM: HYPRE: Created solution & rhs vectors on processor %d for grid %d.\n", *myrank, *iGridGlobal);
  check_hypre_ierr(ierr, __LINE__, __FILE__);

  /* Set the object type (by default HYPRE_SSTRUCT). This determines the
     data structure used to store the matrix.  If you want to use unstructured
     solvers, e.g. BoomerAMG, the object type should be HYPRE_PARCSR.
     If the problem is purely structured (with one part), you may want to use
     HYPRE_STRUCT to access the structured solvers. */
  ierr += HYPRE_SStructVectorSetObjectType(b[*ng-1], object_type);
  ierr += HYPRE_SStructVectorSetObjectType(x[*ng-1], object_type);
  ierr += HYPRE_SStructVectorInitialize(b[*ng-1]);
  ierr += HYPRE_SStructVectorInitialize(x[*ng-1]);
  // printf("RFLOCM: HYPRE: Set solution & rhs vectors for object_type = HYPRE_STRUCT on processor %d for grid %d.\n", *myrank, *iGridGlobal);
  check_hypre_ierr(ierr, __LINE__, __FILE__);

#endif

}

void fill_hypre_implicit_jacobian_matrix_(int *nGridsGlobal, int *iGridGlobal, int *ng, int *region_nGrids, 
                                          int *myrank, int *numproc, MPI_Fint *fcomm, 
                                          int *fndim, int *is, int *ie, 
                                          int *stencil_start, int *stencil_end,
                                          double *values_in, int *lhs_var_in, int *rhs_var_in)
{

#ifdef USE_HYPRE
  int     ierr = 0;
  int     var, part;
  int     i, j;
  int    *indices = NULL;
  int     ndim, nentries, stencil_size, nvalues, lhs_var, rhs_var;
  double *values = NULL;

  /* set the indices */
  ndim = (*fndim);
  stencil_size = ndim * ( (*stencil_end) - (*stencil_start) ) + 1;  /* assumes no cross derivatives */
  lhs_var = (*lhs_var_in)-1;
  rhs_var = (*rhs_var_in)-1;
  indices = (int *)malloc(stencil_size*sizeof(int));
  for (i = 0; i < stencil_size; i++) {
    indices[i] = rhs_var*stencil_size + i;
  }

  /* set the grid extents */
  int *ilower = NULL, *iupper = NULL;
  ilower = (int *)malloc(ndim*sizeof(int));
  iupper = (int *)malloc(ndim*sizeof(int));
  for (i = 0; i < ndim; i++) {
    ilower[i] = is[i]-1;
    iupper[i] = ie[i]-1;
  }

  /* fill in the values */
  for (part = 0; part < nparts[*ng-1]; part++) {
    ierr += HYPRE_SStructMatrixSetBoxValues(A[*ng-1], part, ilower, iupper, lhs_var, stencil_size, indices, values_in);
  }
  // printf("RFLOCM: HYPRE: Filled implicit Jacobian matrix A on processor %d for grid %d.\n", *myrank, *iGridGlobal);
  check_hypre_ierr(ierr, __LINE__, __FILE__);

  free(indices);
  free(ilower);
  free(iupper);
#endif

}

void fill_hypre_implicit_rhs_vector_(int *nGridsGlobal, int *iGridGlobal, int *ng, int *region_nGrids, int *myrank, int *numproc, MPI_Fint *fcomm, int *fndim, int *is, int *ie, 
                                     int *stencil_start, int *stencil_end, double *values_in, double *buf)
{

#ifdef USE_HYPRE

  /* fill in the values */
  int     ierr = 0;
  int     var, part;
  int     i, j;
  int    *indices = NULL;
  int     ndim, nentries, stencil_size, nvalues;

  /* set the indices */
  ndim = (*fndim);

  /* set the values */
  nvalues = 1;
  for (i = 0; i < ndim; i++) nvalues *= (ie[i]-is[i]+1);

  /* set the grid extents */
  int *ilower = NULL, *iupper = NULL;
  ilower = (int *)malloc(ndim*sizeof(int));
  iupper = (int *)malloc(ndim*sizeof(int));
  for (i = 0; i < ndim; i++) {
    ilower[i] = is[i]-1;
    iupper[i] = ie[i]-1;
  }

  /* fill in the values for b */
  for (part = 0; part < nparts[*ng-1]; part++) {
    for (var = 0; var < nvars[*ng-1]; var++) {
      for (i = 0; i < nvalues; i++) { buf[i] = values_in[var*nvalues + i]; };
      ierr += HYPRE_SStructVectorSetBoxValues(b[*ng-1], part, ilower, iupper, var, buf);
    }
  }
  // printf("RFLOCM: HYPRE: Filled right-hand-side vector on processor %d for grid %d.\n", *myrank, *iGridGlobal);
  check_hypre_ierr(ierr, __LINE__, __FILE__);
  
  /* assemble the vectors */
  ierr += HYPRE_SStructVectorAssemble(b[*ng-1]);
  // printf("RFLOCM: HYPRE: Assembled right-hand-side vector on processor %d for grid %d.\n", *myrank, *iGridGlobal);
  check_hypre_ierr(ierr, __LINE__, __FILE__);
 
  free(ilower);
  free(iupper);

#endif

}

void fill_hypre_implicit_soln_vector_(int *nGridsGlobal, int *iGridGlobal, int *ng, int *region_nGrids, int *myrank, int *numproc, MPI_Fint *fcomm, int *fndim, int *is, int *ie, 
                                      int *stencil_start, int *stencil_end, double *values_r, double *values_ru, double *values_rv, double *values_rw, double *values_rE)
{

#ifdef USE_HYPRE

  /* fill in the values */
  int     ierr = 0;
  int     var, part;
  int     i, j;
  int    *indices = NULL;
  int     ndim, nentries, stencil_size, nvalues;

  /* set the indices */
  ndim = (*fndim);

  /* set the values */
  nvalues = 1;
  for (i = 0; i < ndim; i++) nvalues *= (ie[i]-is[i]+1);

  /* set the grid extents */
  int *ilower = NULL, *iupper = NULL;
  ilower = (int *)malloc(ndim*sizeof(int));
  iupper = (int *)malloc(ndim*sizeof(int));
  for (i = 0; i < ndim; i++) {
    ilower[i] = is[i]-1;
    iupper[i] = ie[i]-1;
  }

  /* fill in the values for x */
  for (part = 0; part < nparts[*ng-1]; part++) {

    var = 0;
    ierr += HYPRE_SStructVectorSetBoxValues(x[*ng-1], part, ilower, iupper, var, values_r);

    var = 1;
    ierr += HYPRE_SStructVectorSetBoxValues(x[*ng-1], part, ilower, iupper, var, values_ru);

    if (ndim >= 2) {
      var = 2;
      ierr += HYPRE_SStructVectorSetBoxValues(x[*ng-1], part, ilower, iupper, var, values_rv);
    }

    if (ndim == 3) {
      var = 3;
      ierr += HYPRE_SStructVectorSetBoxValues(x[*ng-1], part, ilower, iupper, var, values_rw);
    }

    var = ndim + 1;
    ierr += HYPRE_SStructVectorSetBoxValues(x[*ng-1], part, ilower, iupper, var, values_rE);

  }
  // printf("RFLOCM: HYPRE: Initialized solution vector on processor %d for grid %d.\n", *myrank, *iGridGlobal);
  check_hypre_ierr(ierr, __LINE__, __FILE__);
 
  /* assemble the vectors */
  ierr += HYPRE_SStructVectorAssemble(x[*ng-1]);
  // printf("RFLOCM: HYPRE: Assembled solution vector on processor %d for grid %d.\n", *myrank, *iGridGlobal);
  check_hypre_ierr(ierr, __LINE__, __FILE__);
 
  free(ilower);
  free(iupper);

#endif

}

/** void hypre_solve_implicit_time_(int *nGridsGlobal, int *iGridGlobal, int *ng, int *region_nGrids, int *myrank, int *numproc, MPI_Fint *fcomm, int *fndim, int *is, int *ie,  **/
/**                                 int *stencil_start, int *stencil_end, int *its) **/
/** { **/

/** #ifdef USE_HYPRE **/

/**   /\* fill in the values *\/ **/
/**   int      ierr = 0; **/
/**   int      var, part; **/
/**   double  *values = NULL; **/
/**   int      i, j; **/
/**   int     *indices = NULL; **/
/**   int      ndim, nentries, stencil_size, nvalues; **/
/**   MPI_Comm comm; **/
/**   double   final_res_norm; **/
/**   int      n_pre, n_post; **/
/**   char    *descr; **/


/**   /\* set the dimension *\/ **/
/**   ndim = (*fndim); **/

/**   /\* convert communicator *\/ **/
/**   comm = (MPI_Comm)MPI_Comm_f2c(*fcomm); **/

/**   /\* set solver *\/ **/
/**   ierr += HYPRE_SStructGMRESCreate(comm, &(solver[*ng-1])); **/
/**   check_hypre_ierr(ierr, __LINE__, __FILE__); **/

/**   /\* GMRES parameters *\/ **/
/**   ierr += HYPRE_SStructGMRESSetMaxIter(solver[*ng-1], 50000); **/
/**   check_hypre_ierr(ierr, __LINE__, __FILE__); **/
/**   ierr += HYPRE_SStructGMRESSetTol(solver[*ng-1], 1.0e-10); **/
/**   check_hypre_ierr(ierr, __LINE__, __FILE__); **/
/**   ierr += HYPRE_SStructGMRESSetAbsoluteTol(solver[*ng-1], 1.0e-10); **/
/**   check_hypre_ierr(ierr, __LINE__, __FILE__); **/
/**   ierr += HYPRE_SStructGMRESSetPrintLevel(solver[*ng-1], 2); **/
/**   check_hypre_ierr(ierr, __LINE__, __FILE__); **/
/**   ierr += HYPRE_SStructGMRESSetLogging(solver[*ng-1], 0); **/
/**   check_hypre_ierr(ierr, __LINE__, __FILE__); **/
/**   ierr += HYPRE_SStructGMRESSetKDim(solver[*ng-1],30); **/
/**   // check_hypre_ierr(ierr, __LINE__, __FILE__); **/

/** #if 0 **/
/**   /\* use SysPFMG as precondititioner *\/ **/
/**   HYPRE_SStructSysPFMGCreate(comm, &(pc[*ng-1])); **/

/**   /\* Set sysPFMG parameters *\/ **/
/**   n_pre  = 1; **/
/**   n_post = 1; **/
/**   HYPRE_SStructSysPFMGSetTol(pc[*ng-1], 1.0e-5); **/
/**   HYPRE_SStructSysPFMGSetMaxIter(pc[*ng-1], 1); **/
/**   HYPRE_SStructSysPFMGSetNumPreRelax(pc[*ng-1], n_pre); **/
/**   HYPRE_SStructSysPFMGSetNumPostRelax(pc[*ng-1], n_post); **/
/**   HYPRE_SStructSysPFMGSetPrintLevel(pc[*ng-1], 2); **/
/**   HYPRE_SStructSysPFMGSetZeroGuess(pc[*ng-1]); **/

/**   /\* Set the preconditioner*\/ **/
/**   HYPRE_SStructGMRESSetPrecond(solver[*ng-1], HYPRE_SStructSysPFMGSolve, HYPRE_SStructSysPFMGSetup, pc[*ng-1]); **/
/** #endif **/

/** #if 0 **/
/**   n_pre  = 1; **/
/**   n_post = 1; **/
/**   HYPRE_StructSMGCreate(comm, &(pc[*ng-1])); **/
/**   HYPRE_StructSMGSetMemoryUse(pc[*ng-1], 0); **/
/**   HYPRE_StructSMGSetMaxIter(pc[*ng-1], 1); **/
/**   HYPRE_StructSMGSetTol(pc[*ng-1], 0.0); **/
/**   HYPRE_StructSMGSetZeroGuess(pc[*ng-1]); **/
/**   HYPRE_StructSMGSetNumPreRelax(pc[*ng-1], n_pre); **/
/**   HYPRE_StructSMGSetNumPostRelax(pc[*ng-1], n_post); **/
/**   HYPRE_StructSMGSetPrintLevel(pc[*ng-1], 0); **/
/**   HYPRE_StructSMGSetLogging(pc[*ng-1], 0); **/
/**   HYPRE_StructGMRESSetPrecond(solver[*ng-1], HYPRE_StructSMGSolve, HYPRE_StructSMGSetup, pc[*ng-1]); **/
/** #endif  **/

/** #if 0 **/
/**   /\* use two-step Jacobi as preconditioner *\/ **/
/**   HYPRE_StructJacobiCreate(comm, &(pc[*ng-1])); **/
/**   HYPRE_StructJacobiSetMaxIter(pc[*ng-1], 2); **/
/**   HYPRE_StructJacobiSetTol(pc[*ng-1], 0.0); **/
/**   HYPRE_StructJacobiSetZeroGuess(pc[*ng-1]); **/
/**   HYPRE_StructGMRESSetPrecond(solver[*ng-1], HYPRE_StructJacobiSolve, HYPRE_StructJacobiSetup, pc[*ng-1]); **/
/** #endif **/

/** #if 0 **/
/**   pc[*ng-1] = NULL; **/
/**   HYPRE_SStructGMRESSetPrecond(solver[*ng-1], HYPRE_StructDiagScale, HYPRE_StructDiagScaleSetup, pc[*ng-1]); **/
/** #endif  **/
/**   printf("Doing setup"); **/
/**   /\* do the setup *\/ **/
/**   ierr += HYPRE_SStructGMRESSetup(solver[*ng-1], A[*ng-1], b[*ng-1], x[*ng-1]); **/
/**   check_hypre_ierr(ierr, __LINE__, __FILE__); **/
/**   printf("doing solve"); **/
/**   /\* do the solve *\/ **/
/**   ierr += HYPRE_SStructGMRESSolve(solver[*ng-1], A[*ng-1], b[*ng-1], x[*ng-1]); **/
/**   //HYPRE_DescribeError(ierr,*descr); **/
/**   check_hypre_ierr(ierr, __LINE__, __FILE__); **/

/**   /\* get some info *\/ **/
/**   ierr += HYPRE_SStructGMRESGetFinalRelativeResidualNorm(solver[*ng-1], &final_res_norm); **/
/**   check_hypre_ierr(ierr, __LINE__, __FILE__); **/
/**   ierr += HYPRE_SStructGMRESGetNumIterations(solver[*ng-1], its); **/
/**   check_hypre_ierr(ierr, __LINE__, __FILE__); **/

/**   /\* clean up *\/ **/
/**   ierr += HYPRE_SStructGMRESDestroy(solver[*ng-1]); **/
/**   check_hypre_ierr(ierr, __LINE__, __FILE__); **/
            
/**   // HYPRE_StructJacobiDestroy(pc[*ng-1]); **/

/** #endif  **/

//}

void hypre_parcsr_solve_implicit_time_(int *nGridsGlobal, int *iGridGlobal, int *ng, int *region_nGrids, int *myrank, int *numproc, MPI_Fint *fcomm, int *fndim, int *is, int *ie, int *stencil_start, int *stencil_end, int *its, int *buildPC, int *lowerdtau, int *HypreParamsInt, double *HypreParamsReal)
{
  /*void hypre_parcsr_solve_implicit_time_(int *nGridsGlobal, int *iGridGlobal, int *ng, int *region_nGrids, int *myrank, int *numproc, MPI_Fint *fcomm, int *fndim, int *is, int *ie, int *stencil_start, int *stencil_end, int *its, int *buildPC, int *lowerdtau, double *GMRESTol, int *EuclidLevel, int *KDim, int *GMRESiters)
    {*/
  
#ifdef USE_HYPRE
  /* fill in the values */
  char                  *descr;
  int                   ierr = 0;
  int                   var, part;
  double               *values = NULL;
  int                   i, j;
  int                  *indices = NULL;
  int                   ndim, nentries, stencil_size, nvalues;
  MPI_Comm              comm;
  double                final_res_norm;
  int                   n_pre, n_post;
  static int            allocd = 0;

  /* input parameters */
  int                   precondnum;
  int                   numsolveriters;
  int                   solverparam1; // kdim for gmres, coarsening/relaxation for BoomerAMG
  int                   level;
  int                   solverparam3;
  int                   solverparam2; // parasailsnlevels for gmres, number sweeps for BoomerAMG
  int                   solverprintlevel;
  int                   pcprintlevel;
  
  double                solvertol;
  double                parasailsthresh;
  double                parasailsfilter;

  /* set parameters */
  precondnum       = HypreParamsInt[0];
  numsolveriters   = HypreParamsInt[1];
  solverparam1     = HypreParamsInt[2];
  level            = HypreParamsInt[3];
  solverparam3     = HypreParamsInt[4];
  solverprintlevel = HypreParamsInt[5];
  pcprintlevel     = HypreParamsInt[6];
  solverparam2     = HypreParamsInt[7];

  solvertol         = HypreParamsReal[0];
  parasailsthresh  = HypreParamsReal[1];
  parasailsfilter  = HypreParamsReal[2];
  
  /* set the dimension */
  ndim = (*fndim);

  /* convert communicator */
  comm = (MPI_Comm)MPI_Comm_f2c(*fcomm);

  //if(precondnum != 4){
  /* get the CSR-representations of A, b, x */
  HYPRE_SStructMatrixGetObject(A[*ng-1], (void **) &(par_A[*ng-1]));
  HYPRE_SStructVectorGetObject(b[*ng-1], (void **) &(par_b[*ng-1]));
  HYPRE_SStructVectorGetObject(x[*ng-1], (void **) &(par_x[*ng-1]));
  //}

  // comm = hypre_ParCSRMatrixComm(par_A[*ng-1]); 

  if (*buildPC == 1) {

    /* destroy existing solver, if present */
    if (allocd != 0) {
      if (*myrank == 0) {
	printf("Rebuilding solver ... \n");}

      if (precondnum != 4){
	HYPRE_ParCSRGMRESDestroy(par_solver[*ng-1]);
      }
      if (precondnum == 1){
	HYPRE_EuclidDestroy(eu[*ng-1]);
      }else if (precondnum == 2){
	HYPRE_ParaSailsDestroy(eu[*ng-1]);
      }else if (precondnum == 3){
	HYPRE_BoomerAMGDestroy(eu[*ng-1]);
      }else if (precondnum == 4){
	//	HYPRE_StructJacobiDestroy(pc[*ng-1]);
	printf("Destroying BoomerSolver\n");
	HYPRE_BoomerAMGDestroy(par_solver[*ng-1]);
      }
	
      //
    }

    /* GMRES */
    /* set solver */
    if (precondnum != 4){
      HYPRE_ParCSRGMRESCreate(comm, &(par_solver[*ng-1]));
    }
/**  else { **/
/**       HYPRE_StructGMRESCreate(comm, &(solver[*ng-1])); **/
/**     } **/
    //fprintf(stderr,"hypre.c: setting params");

    /* set the GMRES paramaters */
    if (precondnum != 4){
      HYPRE_ParCSRGMRESSetKDim(par_solver[*ng-1], solverparam1);
      HYPRE_ParCSRGMRESSetMaxIter(par_solver[*ng-1], numsolveriters);
      HYPRE_ParCSRGMRESSetTol(par_solver[*ng-1], solvertol);
      HYPRE_ParCSRGMRESSetAbsoluteTol(par_solver[*ng-1], solvertol);
      HYPRE_ParCSRGMRESSetPrintLevel(par_solver[*ng-1], solverprintlevel);
      //HYPRE_ParCSRGMRESSetLogging(par_solver, 1);
    }
/**  else { **/
/**       HYPRE_StructGMRESSetKDim(solver[*ng-1], kdim); **/
/**       HYPRE_StructGMRESSetMaxIter(solver[*ng-1], numgmresiters); **/
/**       HYPRE_StructGMRESSetTol(solver[*ng-1], gmrestol); **/
/**       HYPRE_StructGMRESSetAbsoluteTol(solver[*ng-1], gmrestol); **/
/**       HYPRE_StructGMRESSetPrintLevel(solver[*ng-1], gmresprintlevel); **/
/**       //HYPRE_StructGMRESSetLogging(solver, 1); **/
/**    }
    /* print stuff? */
#if 0
    HYPRE_SStructMatrixPrint("A.hypre",A[*ng-1],0);
    HYPRE_SStructVectorPrint("x.hypre",x[*ng-1],0);
    HYPRE_SStructVectorPrint("b.hypre",b[*ng-1],0);
#endif

      if (precondnum == 1){
	
	if (*myrank == 0) {
	  printf("Using Euclid Preconditioner\n");}
	
	HYPRE_EuclidCreate(comm, &(eu[*ng-1]));
	HYPRE_EuclidSetLevel(eu[*ng-1], level);
	HYPRE_EuclidSetBJ(eu[*ng-1], solverparam3);
	HYPRE_EuclidSetStats(eu[*ng-1], pcprintlevel);
	HYPRE_EuclidSetMem(eu[*ng-1], pcprintlevel);
	HYPRE_GMRESSetPrecond(par_solver[*ng-1], (HYPRE_PtrToSolverFcn) HYPRE_EuclidSolve, (HYPRE_PtrToSolverFcn) HYPRE_EuclidSetup, eu[*ng-1]);  

	
      }else if (precondnum == 2) {
	
	/* try ParaSails */
	if (*myrank == 0) {
	  printf("Using ParaSails Preconditioner\n");}
	HYPRE_ParaSailsCreate(comm,&(eu[*ng-1]));
	HYPRE_ParaSailsSetParams(eu[*ng-1], parasailsthresh, solverparam2);
	HYPRE_ParaSailsSetFilter(eu[*ng-1], parasailsfilter);
	HYPRE_ParaSailsSetSym(eu[*ng-1],0);
	HYPRE_ParaSailsSetLogging(eu[*ng-1], pcprintlevel);
	HYPRE_GMRESSetPrecond(par_solver[*ng-1],(HYPRE_PtrToSolverFcn) HYPRE_ParaSailsSolve, (HYPRE_PtrToSolverFcn) HYPRE_ParaSailsSetup, eu[*ng-1]);   
	

      }else if (precondnum == 3) {


      /* BoomerAMG*/
      if(*myrank == 0){
	printf("Using BoomerAMG Preconditioner\n");}

      /* Now set up the AMG preconditioner and specify any parameters */
      HYPRE_BoomerAMGCreate(&eu[*ng-1]);
      HYPRE_BoomerAMGSetPrintLevel(eu[*ng-1], pcprintlevel); /* print amg solution info */
      HYPRE_BoomerAMGSetCoarsenType(eu[*ng-1], 6);
      HYPRE_BoomerAMGSetRelaxType(eu[*ng-1], 6); /* Sym G.S./Jacobi hybrid */ 
      HYPRE_BoomerAMGSetNumSweeps(eu[*ng-1], 1); /* Sweeps on each level */
      HYPRE_BoomerAMGSetTol(eu[*ng-1], 0.0); /* conv. tolerance zero */
      HYPRE_BoomerAMGSetMaxIter(eu[*ng-1], 1); /* do only one iteration! */
      HYPRE_GMRESSetPrecond(par_solver[*ng-1], (HYPRE_PtrToSolverFcn) HYPRE_BoomerAMGSolve, (HYPRE_PtrToSolverFcn) HYPRE_BoomerAMGSetup, eu[*ng-1]);



      }else if (precondnum == 4) {
      if(*myrank == 0){
 	printf("Solving with BoomerAMG\n");}

      /* Set up BoomerAMG solver */
      HYPRE_BoomerAMGCreate(&par_solver[*ng-1]);
      HYPRE_BoomerAMGSetDebugFlag(par_solver[*ng-1],2);
      HYPRE_BoomerAMGSetSmoothType(par_solver[*ng-1],9);
      HYPRE_BoomerAMGSetRelaxWt(par_solver[*ng-1],0.001);
      HYPRE_BoomerAMGSetPrintLevel(par_solver[*ng-1], solverprintlevel); /* print amg solution info */
      HYPRE_BoomerAMGSetCoarsenType(par_solver[*ng-1], solverparam1); /* Falgout coarsening */
      HYPRE_BoomerAMGSetRelaxType(par_solver[*ng-1], solverparam3); /* Sym G.S./Jacobi hybrid */ 
      HYPRE_BoomerAMGSetMaxLevels(par_solver[*ng-1], level); /* maximum number of levels */
      HYPRE_BoomerAMGSetNumSweeps(par_solver[*ng-1], solverparam2); /* Sweeps at each level */
      HYPRE_BoomerAMGSetTol(par_solver[*ng-1], solvertol); /* conv. tolerance zero */
      HYPRE_BoomerAMGSetMaxIter(par_solver[*ng-1], numsolveriters); /* do only one iteration! */

/** 	printf("Jacobi Preconditioner\n");} **/

/** 	/\* use two-step Jacobi as preconditioner *\/ **/
/** 	HYPRE_StructJacobiCreate(comm, &(pc[*ng-1])); **/
/** 	HYPRE_StructJacobiSetMaxIter(pc[*ng-1], 2); **/
/** 	HYPRE_StructJacobiSetTol(pc[*ng-1], 0.0); **/
/** 	HYPRE_StructJacobiSetZeroGuess(pc[*ng-1]); **/
/** 	HYPRE_StructGMRESSetPrecond(solver[*ng-1],HYPRE_StructJacobiSolve, HYPRE_StructJacobiSetup, pc[*ng-1]); **/



      }


      if(*myrank == 0){
	fprintf(stderr,"hypre.c: setting up system\n");}
      /* do the setup */
      if(precondnum !=4){
	ierr += HYPRE_ParCSRGMRESSetup(par_solver[*ng-1], par_A[*ng-1], par_b[*ng-1], par_x[*ng-1]);
      } else {
	ierr += HYPRE_BoomerAMGSetup(par_solver[*ng-1], par_A[*ng-1], par_b[*ng-1], par_x[*ng-1]);
	//    exit(-1);


      }
      //    exit(0);
      //if(ierr!=0){
      //      HYPRE_DescribeError(ierr,*descr);
      //fprintf(stderr,"error = %s",*descr);}
      /* flag */
      allocd = 1;
  }
  
 
  // fprintf(stderr,"hypre.c: solving system\n");
  /* do the solve */
  *lowerdtau = 0;
  if(precondnum !=4){
    ierr += HYPRE_ParCSRGMRESSolve(par_solver[*ng-1], par_A[*ng-1], par_b[*ng-1], par_x[*ng-1]);
  } else {
    ierr += HYPRE_BoomerAMGSolve(par_solver[*ng-1], par_A[*ng-1], par_b[*ng-1], par_x[*ng-1]);
  }
  if(ierr != 0){
    *lowerdtau = 1;
    //printf("RFLOCM: GMRES Error -- Lowering dtau for retry, lowerdtau = %d.\n",*lowerdtau);

    //fprintf(stderr,"hypre.c: done with solve");
  }
  //check_hypre_ierr(ierr, __LINE__, __FILE__);

  /* get some info */
  if (precondnum !=4){
    HYPRE_GMRESGetNumIterations(par_solver[*ng-1], its);
    HYPRE_GMRESGetFinalRelativeResidualNorm(par_solver[*ng-1], &final_res_norm);
  }else{
    HYPRE_BoomerAMGGetNumIterations(par_solver[*ng-1], its);
    HYPRE_BoomerAMGGetFinalRelativeResidualNorm(par_solver[*ng-1], &final_res_norm);
  }
  /* printf("Done with solve\n"); */
  /* exit(-1); */

  /* clean up */
  // HYPRE_ParCSRGMRESDestroy(par_solver[*ng-1]);
  // HYPRE_EuclidDestroy(eu[*ng-1]);

  /* Gather the solution vector. */
  HYPRE_SStructVectorGather(x[*ng-1]);

    

#endif

}


/*
 * follow code from HYPRE to create a more Matlab-friendly matrix output
 */
void print_hypre_sstruct_matrix_(int *nGridsGlobal, int *iGridGlobal, int *ng, int *region_nGrids, int *myrank, int *numproc, MPI_Fint *fcomm, int *fndim, char *filename)
{

#ifdef USE_HYPRE
  HYPRE_SStructMatrixPrint("A.hypre",A[*ng-1],0);
  HYPRE_SStructVectorPrint("x.hypre",x[*ng-1],0);
  HYPRE_SStructVectorPrint("b.hypre",b[*ng-1],0);

#if 0
  int part, nvars, vi, vj;
  int nparts;
  hypre_SStructPMatrix *pmatrix = NULL;
  hypre_StructMatrix   *smatrix = NULL;
  hypre_StructStencil  *stencil = NULL;
  hypre_Index          *stencil_shape = NULL;
  int                   stencil_size = 0;

  /* loop over all of the parts */
  nparts = hypre_SStructMatrixNParts(A[*ng-1]);
  if (nparts == 1) printf("RFLOCM: HYPRE: Matrix A[%d] has %d part.\n", *ng-1, nparts);
  if (nparts >  1) printf("RFLOCM: HYPRE: Matrix A[%d] has %d parts.\n", *ng-1, nparts);

  for (part = 0; part < nparts; part++) {

    /* part-specific matrix */
    pmatrix = hypre_SStructMatrixPMatrix(A[*ng-1], part);
    if (pmatrix == NULL) {
      printf("RFLOCM: HYPRE: ERROR: hypre_SStructMatrixPMatrix(A[*ng-1], part) returned NULL part = %d.\n", part);
      exit(-1);
    }

    /* find number of nvars */
    nvars = hypre_SStructPMatrixNVars(pmatrix);
    printf("RFLOCM: HYPRE: Part %d of matrix A[%d] has %d variables.\n", part, *ng-1, nvars);

    for (vi = 0; vi < nvars; vi++)
    {
      for (vj = 0; vj < nvars; vj++)
      {
         smatrix = hypre_SStructPMatrixSMatrix(pmatrix, vi, vj);
         if (smatrix != NULL) {
           stencil       = hypre_StructMatrixStencil(smatrix);
           stencil_size  = hypre_StructStencilSize(stencil);
           printf("RFLOCM: HYPRE: Matrix A[%d]: Part = %d: lhs_var = %d: rhs_var = %d: stencil_size = %d\n", *ng-1, part, vi, vj, stencil_size);
         } else {
           printf("RFLOCM: HYPRE: ERROR: hypre_SStructPMatrixSMatrix(pmatrix, vi, vj) returned NULL with (vi,vj) = (%d,%d).\n", vi, vj);
         }
      }
    }
  }
#endif

#endif

}

void assemble_hypre_implicit_jacobian_matrix_(int *nGridsGlobal, int *iGridGlobal, int *ng, int *region_nGrids, 
                                              int *myrank, int *numproc, MPI_Fint *fcomm)
{

#ifdef USE_HYPRE

  int ierr = 0;

  /* assemble the matrix */
  ierr += HYPRE_SStructMatrixAssemble(A[*ng-1]);  
  // printf("RFLOCM: HYPRE: Assembled implicit Jacobian matrix A on processor %d for grid %d.\n", *myrank, *iGridGlobal);
  check_hypre_ierr(ierr, __LINE__, __FILE__);

#endif

}

void hypre_update_implicit_soln_vector_(int *nGridsGlobal, int *iGridGlobal, int *ng, int *region_nGrids, int *myrank, int *numproc, MPI_Fint *fcomm, int *fndim, int *is, int *ie, 
                                        int *stencil_start, int *stencil_end, double *q, double *jac)
{

#ifdef USE_HYPRE

  /* fill in the values */
  int     ierr = 0;
  int     var, part;
  double *values = NULL;
  int     i, j;
  int    *indices = NULL;
  int     ndim, nentries, stencil_size, nvalues;

  /* set the indices */
  ndim = (*fndim);

  /* set the values */
  nvalues = 1;
  for (i = 0; i < ndim; i++) nvalues *= (ie[i]-is[i]+1);
  values = (double *)malloc(nvalues*sizeof(double));

  /* set the grid extents */
  int *ilower = NULL, *iupper = NULL;
  ilower = (int *)malloc(ndim*sizeof(int));
  iupper = (int *)malloc(ndim*sizeof(int));
  for (i = 0; i < ndim; i++) {
    ilower[i] = is[i]-1;
    iupper[i] = ie[i]-1;
  }

  /* fill in the values for b */
  for (part = 0; part < nparts[*ng-1]; part++) {
    for (var = 0; var < nvars[*ng-1]; var++) {
      ierr += HYPRE_SStructVectorGetBoxValues(x[*ng-1], part, ilower, iupper, var, values);
      for (i = 0; i < nvalues; i++) {
        // q[var*nvalues + i] = q[var*nvalues+i] + jac[i] * values[i]; 
        q[var*nvalues+i] = values[i] * jac[i];
      }
    }
  }
  // printf("RFLOCM: HYPRE: Updated solution on processor %d for grid %d.\n", *myrank, *iGridGlobal);
  check_hypre_ierr(ierr, __LINE__, __FILE__);
  
  free(values);
  free(ilower);
  free(iupper);

#endif

}
