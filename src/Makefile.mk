# Copyright (c) 2015, University of Illinois at Urbana-Champaign, XPACC
# License: MIT, http://opensource.org/licenses/MIT
plascomcm_common_fpp_src = ModGlobal.fpp \
         ModDataStruct.fpp \
         ModMPI.fpp \
         ModString.fpp \
         ModParam.fpp \
         ModPLOT3D_IO.fpp \
         ModHDF5_IO.fpp \
         ModMatrixVectorOps.fpp \
         ModSpline.fpp \
         ModPSAAP2.fpp \
         ModPSAAP2_cp.fpp \
         ModInitialCondition.fpp \
         ModGeometry.fpp \
         ModEOS.fpp \
         ModDataUtils.fpp \
         ModPenta.fpp \
         ModDeriv.fpp \
	 ModIOUtil.fpp \
         ModIO.fpp \
         ModSBI.fpp \
         ModDerivBuildOps.fpp \
         ModMetrics.fpp \
         ModFVSetup.fpp \
         ModFiniteVolume.fpp \
         ModLighthill.fpp \
         ModRiemannSolver.fpp \
         ModNASARotor.fpp \
         ModEulerFV.fpp \
         ModNavierStokesBC.fpp \
         ModNavierStokesRHS.fpp \
         ModCombustion.fpp \
         ModOneStep.fpp \
         ModModel0.fpp \
         ModModel1.fpp \
         ModNavierStokesImplicitSATArtDiss.fpp \
         ModNavierStokesImplicit.fpp \
         ModLinNavierStokesRHS.fpp \
         ModLinNavierStokesBC.fpp \
         ModAdjointNS.fpp \
         ModCyl1D.fpp \
         ModQ1D.fpp \
         ModIBLANK.fpp \
         ModInterp.fpp \
         ModTimemarch.fpp \
         ModOptimization.fpp \
         ModRungeKutta.fpp \
         ModActuator.fpp \
         ModInput.fpp \
         ModRegion.fpp \
         ModPostProcess.fpp \
         ModStat.fpp \
         ModPlasComCM.fpp

# C Source Files
plascomcm_common_c_src = plot3d_format.c readbuffer.c mesh_io.c
if HAVE_HDF5
plascomcm_common_c_src += hdf5_format.c
endif

# tmsolve files
plascomcm_common_tmsolver_fpp_src = ModTMIO.fpp \
         ModTMInt.fpp \
         ModElasticity.fpp \
         ModTMEOM.fpp \
         ModFTMCoupling.fpp \
         ModTMEOMBC.fpp \
         ModPETSc.fpp \
         ModTMSingleStep.fpp \
         ModInitTM.fpp

plascomcm_dist =

plascomcm_dist += petsc_fortran.h

if BUILD_TMSOLVER
plascomcm_common_fpp_src += $(plascomcm_common_tmsolver_fpp_src)
else
plascomcm_dist += $(plascomcm_common_tmsolver_fpp_src)
endif

# Cantera files
plascomcm_common_cantera_fpp_src =  ModXMLCantera.fpp ModLaserIgnition.fpp
plascomcm_cantera_header_src = read_from_buffer.inc read_xml_array.inc read_xml_scalar.inc

plascomcm_dist += $(plascomcm_cantera_header_src)
if HAVE_CANTERA
plascomcm_common_fpp_src += $(plascomcm_common_cantera_fpp_src)
else
plascomcm_dist += $(plascomcm_common_cantera_fpp_src)
endif

# efsolver files
plascomcm_efsolver_c_src = ef_solver.c \
	ef_bc.c \
	ef_boundary.c \
	ef_dirichlet.c \
	ef_neumann.c \
	ef_dmap.c \
	ef_fd.c \
	ef_grid.c \
	ef_interface.c \
	ef_io.c \
	ef_metric.c \
	ef_operator.c \
	ef_stencil.c \
	ef_mat.c \
	ef_pc.c

plascomcm_efsolver_headers = ef_solver.h \
  ef_bc.h \
  ef_boundary.h \
  ef_dirichlet.h \
  ef_neumann.h \
  ef_dmap.h \
  ef_fd.h \
  ef_grid.h \
  ef_interface.h \
  ef_io.h \
  ef_level.h \
  ef_metric.h \
  ef_operator.h \
  ef_state.h \
  ef_stencil.h \
  ef_mat.h \
  ef_pc.h

plascomcm_efsolver_fpp_src = ModElectric.fpp \
	ModElectricBC.fpp \
	ModElectricState.fpp \
	ModElectricInterface.fpp \
	ModElectricSchwarz.fpp

plascomcm_dist += $(plascomcm_efsolver_headers)
if BUILD_ELECTRIC
plascomcm_common_fpp_src += $(plascomcm_efsolver_fpp_src)
plascomcm_common_c_src   += $(plascomcm_efsolver_c_src)
plascomcm_common_fpp_src += $(plascomcm_plasma_fpp_src)
else
plascomcm_dist += $(plascomcm_efsolver_fpp_src) $(plascomcm_efsolver_c_src) \
  $(plascomcm_plasma_fpp_src)
endif

plascomcm_fpp_src = ModMain.fpp $(plascomcm_common_fpp_src)
plascomcm_c_src = $(plascomcm_common_c_src)
