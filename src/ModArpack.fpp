! Copyright (c) 2015, University of Illinois at Urbana-Champaign, XPACC
! License: MIT, http://opensource.org/licenses/MIT
!-----------------------------------------------------------------------
!
! ModArpack.f90
! 
! - evaluate the eigenvalues of the discretized system
!
! Revision history
! - 06 Feb 2008 : DJB : initial code
!
! $Header: /cvsroot/genx/Codes/RocfloCM/Source/ModArpack.f90,v 1.3 2010/08/06 15:58:08 mtcampbe Exp $
!
!-----------------------------------------------------------------------
MODULE ModArpack

CONTAINS

  subroutine Arpack(region)

    USE ModGlobal
    USE ModDataStruct
    USE ModEOS
    USE ModDeriv
    USE ModIO
    USE ModMPI
    USE ModNavierStokes

    ! ... local variables
    type(t_grid), pointer :: grid
    type(t_mixt), pointer :: state
    type(t_mixt_input), pointer :: input
    type(t_region), pointer :: region
    type(t_patch), pointer :: patch
    integer :: ng

    ! ... ARPACK variables (copied from external_libs/ARPACK/PARPACK/EXAMPLES/MPI/pdndrv1.f)
    !     %-----------------------------%
    !     | Define maximum dimensions   |
    !     | for all arrays.             |
    !     | MAXN:   Maximum dimension   |
    !     |         of the distributed  |
    !     |         block of A allowed. |
    !     | MAXNEV: Maximum NEV allowed |
    !     | MAXNCV: Maximum NCV allowed |
    !     %-----------------------------%
    integer, parameter :: maxnloc=256, maxnev=12, maxncv=30, ldv=maxnloc
    !
    !     %--------------%
    !     | Local Arrays |
    !     %--------------%
    !
    integer      ::   iparam(11), ipntr(14)
    logical      ::   select(maxncv)
    real(rfreal) ::   ax(maxnloc), d(maxncv,3), resid(maxnloc), &
         v(ldv,maxncv), workd(3*maxnloc), &
         workev(3*maxncv), &
         workl(3*maxncv*maxncv+6*maxncv)
    !
    !     %---------------%
    !     | Local Scalars |
    !     %---------------%
    !
    character    :: bmat*1, which*2
    integer      :: ido, n, nx, nev, ncv, lworkl, info, j, nconv, maxitr, ishfts, mode
    real(rfreal) :: tol, sigmar, sigmai
    logical      :: first, rvec
    !
    !     %----------------------------------------------%
    !     | Local Buffers needed for MPI communication |
    !     %----------------------------------------------%
    !
    real(rfreal) :: mv_buf(maxnloc)
    !
    !     %------------%
    !     | Parameters |
    !     %------------%
    !
    real(rfreal), parameter :: zero = 0.0_rfreal
    !
    !     %-----------------------------%
    !     | BLAS & LAPACK routines used |
    !     %-----------------------------%
    !
    real(rfreal) ::  dlapy2, pdnorm2
    external         dlapy2, daxpy, pdnorm2
    !
    ndigit = -3
    logfil = 6
    mnaupd = 1
    !
    !     %--------------------------------------------------%
    !     | The number NX is the number of interior points   |
    !     | in the discretization of the 2-dimensional       |
    !     | convection-diffusion operator on the unit        |
    !     | square with zero Dirichlet boundary condition.   | 
    !     | The number N(=NX*NX) is the dimension of the     |
    !     | matrix.  A standard eigenvalue problem is        |
    !     | solved (BMAT = 'I').  NEV is the number of       |
    !     | eigenvalues to be approximated.  The user can    |
    !     | modify NX, NEV, NCV, WHICH to solve problems of  |
    !     | different sizes, and to get different parts of   |
    !     | the spectrum.  However, The following            |
    !     | conditions must be satisfied:                    |
    !     |                   N <= MAXN                      |
    !     |                 NEV <= MAXNEV                    |
    !     |           NEV + 2 <= NCV <= MAXNCV               | 
    !     %--------------------------------------------------% 
    !
    nx    = 10 
    n     = nx*nx 
    nev   = 4
    ncv   = 20 
    !
    bmat  = 'I'
    which = 'SM'
    !
    !     %-----------------------------------------------------%
    !     | The work array WORKL is used in PDNAUPD as          |  
    !     | workspace.  Its dimension LWORKL is set as          |
    !     | illustrated below.  The parameter TOL determines    |
    !     | the stopping criterion. If TOL<=0, machine          |
    !     | precision is used.  The variable IDO is used for    |
    !     | reverse communication, and is initially set to 0.   |
    !     | Setting INFO=0 indicates that a random vector is    |
    !     | generated in PDNAUPD to start the Arnoldi iteration.| 
    !     %-----------------------------------------------------%
    !
    ! ... compute the eigenvalues of the non-symmetric A
    ! ... where A is the discretized Navier-Stokes operator
    lworkl  = 3*ncv**2+6*ncv 
    tol    = zero 
    ido    = 0
    info   = 0
    !
    !     %---------------------------------------------------%
    !     | This program uses exact shifts with respect to    |
    !     | the current Hessenberg matrix (IPARAM(1) = 1).    |
    !     | IPARAM(3) specifies the maximum number of Arnoldi |
    !     | iterations allowed.  Mode 1 of PDNAUPD is used    |
    !     | (IPARAM(7) = 1). All these options can be changed |
    !     | by the user. For details see the documentation in |
    !     | PDNAUPD.                                          |
    !     %---------------------------------------------------%
    !
    ishfts = 1
    maxitr = 300
    mode   = 1
    !
    iparam(1) = ishfts
    iparam(3) = maxitr 
    iparam(7) = mode
    !
    !     %-------------------------------------------%
    !     | M A I N   L O O P (Reverse communication) | 
    !     %-------------------------------------------%
    !
10  continue
    !
    !        %---------------------------------------------%
    !        | Repeatedly call the routine PDNAUPD and take|
    !        | actions indicated by parameter IDO until    |
    !        | either convergence is indicated or maxitr   |
    !        | has been exceeded.                          |
    !        %---------------------------------------------%
    !
    call pdnaupd(comm, ido, bmat, nloc, which, nev, tol, resid, &
         ncv, v, ldv, iparam, ipntr, workd, workl, lworkl, info )
    !
    if (ido .eq. -1 .or. ido .eq. 1) then
      !
      !           %-------------------------------------------%
      !           | Perform matrix vector multiplication      |
      !           |                y <--- OP*x                |
      !           | The user should supply his/her own        |
      !           | matrix vector multiplication routine here |
      !           | that takes workd(ipntr(1)) as the input   |
      !           | vector, and return the matrix vector      |
      !           | product to workd(ipntr(2)).               | 
      !           %-------------------------------------------%
      !
      !
      !           %-----------------------------------------%
      !           | L O O P   B A C K to call PDNAUPD again.|
      !           %-----------------------------------------%
      !
      go to 10
      !
    end if
    ! 
    !     %----------------------------------------%
    !     | Either we have convergence or there is |
    !     | an error.                              |
    !     %----------------------------------------%
    !
    if ( info .lt. 0 ) then
      !
      !        %--------------------------%
      !        | Error message, check the |
      !        | documentation in PDNAUPD.|
      !        %--------------------------%
      !
      if ( region%myrank == 0 ) then
        print *, ' '
        print *, ' Error with _naupd, info = ', info
        print *, ' Check the documentation of _naupd'
        print *, ' '
      endif
      !
    else 
      !
      !        %-------------------------------------------%
      !        | No fatal errors occurred.                 |
      !        | Post-Process using PDNEUPD.               |
      !        |                                           |
      !        | Computed eigenvalues may be extracted.    |
      !        |                                           |
      !        | Eigenvectors may also be computed now if  |
      !        | desired.  (indicated by rvec = .true.)    |
      !        %-------------------------------------------%
      !
      rvec = .true.
      !
      call pdneupd ( comm, rvec, 'A', select, d, d(1,2), v, ldv, &
           sigmar, sigmai, workev, bmat, nloc, which, nev, tol, &
           resid, ncv, v, ldv, iparam, ipntr, workd, workl, &
           lworkl, ierr )
      !
      !        %-----------------------------------------------%
      !        | The real part of the eigenvalue is returned   |
      !        | in the first column of the two dimensional    |
      !        | array D, and the imaginary part is returned   |
      !        | in the second column of D.  The corresponding |
      !        | eigenvectors are returned in the first NEV    |
      !        | columns of the two dimensional array V if     |
      !        | requested.  Otherwise, an orthogonal basis    |
      !        | for the invariant subspace corresponding to   |
      !        | the eigenvalues in D is returned in V.        |
      !        %-----------------------------------------------%
      !
      if ( ierr .ne. 0) then
        !
        !           %------------------------------------%
        !           | Error condition:                   |
        !           | Check the documentation of PDNEUPD.|
        !           %------------------------------------%
        !
        if ( region%myrank .eq. 0 ) then
          print *, ' '
          print *, ' Error with _neupd, info = ', ierr
          print *, ' Check the documentation of _neupd. '
          print *, ' '
        endif
      else 
        !
        first  = .true.
        nconv  = iparam(5)
        do 20 j=1, nconv
          !
          !               %---------------------------%
          !               | Compute the residual norm |
          !               |                           |
          !               |   ||  A*x - lambda*x ||   |
          !               |                           |
          !               | for the NCONV accurately  |
          !               | computed eigenvalues and  |
          !               | eigenvectors.  (iparam(5) |
          !               | indicates how many are    |
          !               | accurate to the requested |
          !               | tolerance)                |
          !               %---------------------------%
          !
          if (d(j,2) .eq. zero)  then
            !
            !                  %--------------------%
            !                  | Ritz value is real |
            !                  %--------------------%
            !
            call av(comm, nloc, nx, mv_buf, v(1,j), ax)
            call daxpy(nloc, -d(j,1), v(1,j), 1, ax, 1)
            d(j,3) = pdnorm2( comm, nloc, ax, 1)                   
            !
          else if (first) then
            !
            !                  %------------------------%
            !                  | Ritz value is complex. |
            !                  | Residual of one Ritz   |
            !                  | value of the conjugate |
            !                  | pair is computed.      | 
            !                  %------------------------%
            !        
            call av(comm, nloc, nx, mv_buf, v(1,j), ax)
            call daxpy(nloc, -d(j,1), v(1,j), 1, ax, 1)
            call daxpy(nloc, d(j,2), v(1,j+1), 1, ax, 1)
            d(j,3) = pdnorm2( comm, nloc, ax, 1)
            call av(comm, nloc, nx, mv_buf, v(1,j+1), ax)
            call daxpy(nloc, -d(j,2), v(1,j), 1, ax, 1)
            call daxpy(nloc, -d(j,1), v(1,j+1), 1, ax, 1)
            d(j,3) = dlapy2(d(j,3), pdnorm2(comm,nloc,ax,1) )
            d(j+1,3) = d(j,3)
            first = .false.
          else
            first = .true.
          end if
          !
20        continue
          !
          !            %-----------------------------%
          !            | Display computed residuals. |
          !            %-----------------------------%
          !
          call pdmout(comm, 6, nconv, 3, d, maxncv, -6, &
               'Ritz values (Real,Imag) and direct residuals')
        end if
        !
        !        %-------------------------------------------%
        !        | Print additional convergence information. |
        !        %-------------------------------------------%
        !
        if (region%myrank .eq. 0)then
          if ( info .eq. 1) then
            print *, ' '
            print *, ' Maximum number of iterations reached.'
            print *, ' '
          else if ( info .eq. 3) then
            print *, ' ' 
            print *, ' No shifts could be applied during implicit Arnoldi update, try increasing NCV.'
            print *, ' '
          end if
          !
          print *, ' '
          print *, '_NDRV1 '
          print *, '====== '
          print *, ' ' 
          print *, ' Size of the matrix is ', n
          print *, ' The number of processors is ', nprocs
          print *, ' The number of Ritz values requested is ', nev
          print *, ' The number of Arnoldi vectors generated', &
               ' (NCV) is ', ncv
          print *, ' What portion of the spectrum: ', which
          print *, ' The number of converged Ritz values is ', &
               nconv 
          print *, ' The number of Implicit Arnoldi update', &
               ' iterations taken is ', iparam(3)
          print *, ' The number of OP*x is ', iparam(9)
          print *, ' The convergence criterion is ', tol
          print *, ' '
          !
        endif
      end if
    End Subroutine Arpack

END MODULE ModArpack
