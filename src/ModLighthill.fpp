! Copyright (c) 2015, University of Illinois at Urbana-Champaign, XPACC
! License: MIT, http://opensource.org/licenses/MIT
!-----------------------------------------------------------------------
!
! ModLighthill.f90
! 
! - basic code for the evaluation of the Lighthill stress tensor
!
! Revision history
! - 01 Feb 2008 : DJB : placed into own file
! 
!-----------------------------------------------------------------------
![UNUSED]
MODULE ModLighthill

! CONTAINS

!   subroutine Lighthill_Force(region, ng)

!     USE ModGlobal
!     USE ModDataStruct
!     USE ModDeriv
!     USE ModMPI
!     USE ModSpline

!     TYPE (t_region), POINTER :: region
!     INTEGER :: ng
!     INTEGER :: flags

!     ! ... local variables
!     Real(rfreal), Dimension(:,:,:,:,:), Pointer :: X
!     Character(LEN=2) :: prec, gf, vf, ib
!     Integer :: ngrid, I, J, K, L0, Nx, Ny, Nz, NDIM, M, ftype, ii
!     Integer, Dimension(:,:), Pointer :: ND
!     Type (t_mixt_input), pointer :: input
!     Type (t_mixt), pointer :: state
!     Type (t_grid), pointer :: grid
!     Integer :: parDir_loc(1), N(MAX_ND), vds(MAX_ND)
!     Integer, Parameter :: grid_unit = 20
!     Integer :: io_err
!     Real(rfreal) :: s1, s2, s3, s4, c(0:4), dt0(3), dt1(3), tau(4)
!     real(rfreal), dimension(:), pointer :: gridTime
!     real(rfreal), dimension(:,:,:), pointer :: Qspline
!     Character(LEN=80) :: string

!     ! ... simplicity
!     input => region%input
!     grid => region%grid(ng)
!     state => region%state(ng)
!     
!     ! ... allocate for one grid
!     if (.not. region%lighthillInit) then
!       allocate(region%lighthill)

!       ! ... count the names of the grids
!       region%lighthill%numGridFiles = 0; io_err = FALSE
!       open (unit=grid_unit, file='lighthill.force', status='unknown')
!       do while (io_err == FALSE) 
!         read (grid_unit, '(A)', iostat=io_err) string
!         region%lighthill%numGridFiles = region%lighthill%numGridFiles + 1
!       end do
!       rewind(grid_unit)
!       region%lighthill%numGridFiles = region%lighthill%numGridFiles - 1
!       if (region%myrank == 0) &
!            write (*,'(A,I5,A)') '>> Found ', region%lighthill%numGridFiles, &
!            ' Lighthill files in "lighthill.force" <<'

!       ! ... now read in the names
!       allocate(region%lighthill%grid_fname(region%lighthill%numGridFiles))
!       do i = 1, region%lighthill%numGridFiles
!         read (grid_unit, '(A)') region%lighthill%grid_fname(i)
!         !print *, trim(region%lighthill%grid_fname(i))
!       end do
!       close (grid_unit)

!       ! ... read the time from each file
!       ! ... scale wrt c_inf = 340.29m/s and L_inf = 1m
!       allocate(gridTime(region%lighthill%numGridFiles))
!       do i = 1, region%lighthill%numGridFiles
!         gridTime(i) = dble(i-1)*0.0004_rfreal*340.29_rfreal ! specified
!       end do

!       ! ... read the grid points from each file and save
!       do i = 1, grid%ND
!         N(i) = grid%ie(i)-grid%is(i)+1
!       end do

!       ! ... allocate memory for spline
!       allocate(Qspline(PRODUCT(N(1:grid%ND)),region%lighthill%numGridFiles,3))
!      
!       do ii = 1, region%lighthill%numGridFiles

!         ! ... read the entire file (dumb!)
!        Call Read_Soln(NDIM, ngrid, ND, X, tau, prec, gf, vf, region%lighthill%grid_fname(ii), FALSE)

!         ! ... save our portion
!         ! ... if Chimera, pick our grid and leave
!         if (grid%parDir == FALSE) then
!           do k = grid%is(3), grid%ie(3)
!             do j = grid%is(2), grid%ie(2)
!               do i = grid%is(1), grid%ie(1)
!                 l0 = (k-1)*(grid%ie(1)-grid%is(1)+1)*(grid%ie(2)-grid%is(2)+1) &
!                      + (j-1)*(grid%ie(1)-grid%is(1)+1) + i
!                 Qspline(l0,ii,1) = X(ng,i,j,k,1)
!                 Qspline(l0,ii,2) = X(ng,i,j,k,2)
!                 Qspline(l0,ii,3) = X(ng,i,j,k,3)
!                 ! Qspline(l0,ii,4) = X(ng,i,j,k,5)
!               end do
!             end do
!           end do
!         else ! ... single grid and only save our portion
!           do k = MAX(grid%is(3),1), MIN(grid%ie(3),ND(1,3))
!             do j = MAX(grid%is(2),1), MIN(grid%ie(2),ND(1,2))
!               do i = MAX(grid%is(1),1), MIN(grid%ie(1),ND(1,1))
!                 l0 = (k-grid%is(3))*(grid%ie(1)-grid%is(1)+1) &
!                      *(grid%ie(2)-grid%is(2)+1) &
!                      + (j-grid%is(2))*(grid%ie(1)-grid%is(1)+1) + i - grid%is(1) + 1
!                 Qspline(l0,ii,1) = X(1,i,j,k,1)
!                 Qspline(l0,ii,2) = X(1,i,j,k,2)
!                 Qspline(l0,ii,3) = X(1,i,j,k,3)
!                 ! Qspline(l0,ii,4) = X(l,i,j,k,5)
!               end do
!             end do
!           end do
!         end if

!         ! ... clean up
!         deallocate(X)

!       end Do

!       ! ... construct the spline
!       do i = 1, PRODUCT(N(1:grid%ND))

!         ! derivative at time(1)
!         s1 = 1.0_rfreal * (gridTime(2)-gridTime(1))
!         s2 = 2.0_rfreal * s1
!         s3 = 3.0_rfreal * s1
!         s4 = 4.0_rfreal * s1
!         c(1) = -s2*s3*s4/s1/(s1-s2)/(s1-s3)/(s1-s4);
!         c(2) = -s1*s3*s4/s2/(s2-s1)/(s2-s3)/(s2-s4);
!         c(3) = -s1*s2*s4/s3/(s3-s1)/(s3-s2)/(s3-s4);
!         c(4) = -s1*s2*s3/s4/(s4-s1)/(s4-s2)/(s4-s3);
!         c(0) = -(c(1) + c(2) + c(3) + c(4));
!         dt0  = 0D0
!         Do j = 1, 5
!           dt0(:) = dt0(:) + c(j-1)*Qspline(i,j,:)
!         End Do

!         ! derivative at time(end)
!         c(0) =  s1*s2*s3/s4/(s4-s1)/(s4-s2)/(s4-s3);
!         c(1) =  s1*s2*s4/s3/(s3-s1)/(s3-s2)/(s3-s4);
!         c(2) =  s1*s3*s4/s2/(s2-s1)/(s2-s3)/(s2-s4);
!         c(3) =  s2*s3*s4/s1/(s1-s2)/(s1-s3)/(s1-s4);
!         c(4) = -(c(0) + c(1) + c(2) + c(3));
!         dt1  = 0.0_rfreal
!         Do j = 0, 4
!           dt1(:) = dt1(:) + c(j)*Qspline(i,region%lighthill%numGridFiles-j,:)
!         End Do

!         ! ... construct the spline
!         do j = 1, 3
!           call GenerateSpline(gridTime, Qspline(i,:,j), region%lighthill%spline, &
!             DerivLower=dt0(j), DerivUpper=dt1(j))
!         end do

!       end do

!       ! ... allocate space for flow and force
!       allocate(region%lighthill%QLighthill(PRODUCT(N(1:grid%ND)),3))
!       allocate(region%lighthill%Force(PRODUCT(N(1:grid%ND)),2))

!       ! ... finish by setting the grid to the initial file
!       do i = 1, PRODUCT(N(1:grid%ND))
!         do j = 1, 3
!           region%lighthill%QLighthill(i,j) = SplineInterp(region%lighthill%spline, gridTime(1))
!         end do
!       end do

!       region%lighthillInit = .true.

!     else

!       ! ... number of grid points
!       do i = 1, grid%ND
!         N(i) = grid%ie(i)-grid%is(i)+1
!       end do

!       ! ... evalulate the grid and its time derivative at the current time
!       do i = 1, PRODUCT(N(1:grid%ND))
!         do j = 1, 3
!           region%lighthill%QLighthill(i,j) = SplineInterp(region%lighthill%spline, state%time(i))
!         end do
!       end do

!     end if

!     ! ... compute the lighthill ''force''

!     ! ... convert to primative variables
!     region%lighthill%QLighthill(:,2) = region%lighthill%QLighthill(:,2) / region%lighthill%QLighthill(:,1);
!     region%lighthill%QLighthill(:,3) = region%lighthill%QLighthill(:,3) / region%lighthill%QLighthill(:,1);

!     ! ... memory needed
!     allocate(region%lighthill%dQ(size(region%lighthill%QLighthill,1))); region%lighthill%dQ = 0.0_rfreal
!     allocate( region%lighthill%Q(size(region%lighthill%QLighthill,1)));  region%lighthill%Q = 0.0_rfreal

!     ! ... compute the x-force = -rho_inf u_x diff(u_x,x) - rho_inf u_y diff(u_x,y)
!     region%lighthill%Q(:) = region%lighthill%QLighthill(:,2); region%lighthill%Force(:,:) = 0.0_rfreal
!     ! Change FIRST_DERIV to take a t_lighthill ?
!     !Call FIRST_DERIV(region, ng, 1, Q, dQ, 1.0_rfreal, .FALSE.)
!     Call FIRST_DERIV(region, ng, 1, region%lighthill%Q, region%lighthill%dQ, 1.0_rfreal, .FALSE.)
!     region%lighthill%Force(:,1) = - region%lighthill%QLighthill(:,1) * region%lighthill%QLighthill(:,2) * region%lighthill%dQ(:); region%lighthill%dQ(:) = 0.0_rfreal
!     !Call FIRST_DERIV(region, ng, 2, Q, dQ, 1.0_rfreal, .FALSE.)
!     Call FIRST_DERIV(region, ng, 2, region%lighthill%Q, region%lighthill%dQ, 1.0_rfreal, .FALSE.)
!     region%lighthill%Force(:,1) = region%lighthill%Force(:,1) - region%lighthill%QLighthill(:,1) * region%lighthill%QLighthill(:,3) * region%lighthill%dQ(:); region%lighthill%dQ(:) = 0.0_rfreal

!     ! ... compute the y-force = -rho_inf u_x diff(u_y,x) - rho_inf u_y diff(u_y,y)
!     region%lighthill%Q(:) = region%lighthill%QLighthill(:,3)
!     !Call FIRST_DERIV(region, ng, 1, Q, dQ, 1.0_rfreal, .FALSE.)
!     Call FIRST_DERIV(region, ng, 1, region%lighthill%Q, region%lighthill%dQ, 1.0_rfreal, .FALSE.)
!     region%lighthill%Force(:,2) = - region%lighthill%QLighthill(:,1) * region%lighthill%QLighthill(:,2) * region%lighthill%dQ(:); region%lighthill%dQ(:) = 0.0_rfreal
!     !Call FIRST_DERIV(region, ng, 2, Q, dQ, 1.0_rfreal, .FALSE.)
!     Call FIRST_DERIV(region, ng, 2, region%lighthill%Q, region%lighthill%dQ, 1.0_rfreal, .FALSE.)
!     region%lighthill%Force(:,2) = region%lighthill%Force(:,2) - region%lighthill%QLighthill(:,1) * region%lighthill%QLighthill(:,3) * region%lighthill%dQ(:)

!     ! ... add to the rhs
!     state%rhs(:,2) = state%rhs(:,2) + region%lighthill%Force(:,1)
!     state%rhs(:,3) = state%rhs(:,3) + region%lighthill%Force(:,2)

!     deallocate(region%lighthill%dQ, region%lighthill%Q)

!   end subroutine Lighthill_Force

END MODULE ModLighthill
![/UNUSED]
