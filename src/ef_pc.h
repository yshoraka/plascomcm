#ifndef EF_PC_H
#define EF_PC_H

#include "petscksp.h"

#ifdef WITH_BOXMG
#include <boxmg/capi.h>
#endif

/**
 * @file: ef_pc.h
 *
 * Preconditioner object
 */

typedef struct {
	#ifdef WITH_BOXMG
	bmg2_solver solver;
	#endif
} ef_pc;

PetscErrorCode ef_pc_create(ef_pc**);

PetscErrorCode ef_pc_setup(PC);

PetscErrorCode ef_pc_apply(PC pc, Vec x, Vec y);

PetscErrorCode ef_pc_destroy(PC);

#endif
