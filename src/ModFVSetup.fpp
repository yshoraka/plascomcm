! Copyright (c) 2015, University of Illinois at Urbana-Champaign, XPACC
! License: MIT, http://opensource.org/licenses/MIT
![UNUSED]
Module ModFVSetup

Contains

  Subroutine Initialize_FV(region,gridID)

    USE ModGlobal
    USE ModDataStruct
    USE ModInput
    USE ModMPI

    Implicit None

    Type(t_region), pointer :: region
    Integer :: gridID

    ! ... local
    Type(t_grid), pointer :: grid
    Type(t_mixt_input), pointer :: input
    Type(t_fvgrid), pointer :: fv
    Real(rfreal), pointer :: vXYZ(:,:)
    Integer, pointer :: ND(:)
    Integer :: Nc(MAX_ND), Nf(MAX_ND), N(MAX_ND), Ng(MAX_ND)

    integer :: i, j, k, lc, lf
    integer :: l1, l2, l3, l4
    integer :: dir


       ! ... simplicity
       grid => region%grid(gridID)
       fv => grid%fv
       
       ! ... allocated in ModRegion
       !       allocate(grid%fv%Nc(MAX_ND))
       N = 1; Nf = 1; Nc = 1; Ng = 1;
       do dir = 1, grid%ND
          N(dir) = grid%ie(dir)-grid%is(dir)+1
          Ng(dir) = N(dir) + SUM(grid%fv%nGhost(dir,:))
       end do

       ! ... dual-cell discretization:

       ! ... number of faces in each direction
       ! ... is one less than number of grid points
       ! ... including ghost points
       do i = 1, grid%ND
          Nf(i) = Ng(i) - 1
       end do
        
       ! ... first step, locate cell vertices
       allocate(vXYZ(product(NF(:)),grid%ND))

       do i = 1, size(vXYZ,1)
          do j = 1, grid%ND
             vXYZ(i,j) = 0.0_rfreal
          end do
       end do

       call CellVertices(region,gridID,vXYZ)
       
       if(grid%ND == 2) then
          call CellFace2D(grid,vXYZ)
       elseif(grid%ND == 3) then
          call CellFace3D(grid,vXYZ)
       else
          call graceful_exit(region%myrank, 'FV not written for 1D, or 4D problems...')
       end if

       ! ... set up index map
       call FVVectorization(region,GridID)
       
  End Subroutine Initialize_FV
  
  Subroutine CellFace2D(grid,vXYZ)
    
    USE ModGlobal
    USE ModDataStruct
    USE ModInput
    USE ModMPI
    USE ModMatrixVectorOps

    Implicit None
    
    Type(t_grid), pointer :: grid
    Real(rfreal), pointer :: vXYZ(:,:)

    ! ... local
    Type(t_fvgrid), pointer :: fv
    Integer, pointer :: Nc(:)
    Real(rfreal) :: Vol(3), v13(3), v24(3)
    Real(rfreal) :: L12, L23, L34, L14 
    Integer :: indices(4), Nf(3)
    Integer :: i, j, k, dir, m
    Integer :: lc, lf, l1, l2, l3, l4

    ! ... simplicity
    fv => grid%fv
    Nc => fv%Nc

    do i = 1, grid%ND
       Nf(i) = Nc(i)+1
    end do

    !!!!!!!!! ... 2D Cell ... !!!!!!!!!
    !                                 !
    !            4------3             !
    !            |      |             !
    !            |      |             !
    !            1------2             !
    !                                 !
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

    ! ... loop through the cells (not including ghost)
    do j = 1, fv%Nc(2)
       do i = 1, fv%Nc(1)
          ! ... cell index
          lc = (j-1)*Nc(1) + i
             
          ! ... find areas in each direction
          dir = 1
          ! ... id the indices of cell vertices
          ! ... right handed coordinate system
          lf = (j-1)*Nf(1) + i
          l1 = (j-1)*Nf(1) + i
          l2 = (j-1)*Nf(1) + i + 1
          l3 = (j)*Nf(1)   + i + 1
          l4 = (j)*Nf(1)   + i
             
          ! ... normal
          fv%face(dir)%normal_vec(lf,1) = vXYZ(l4,2) - vXYZ(l1,2)
          fv%face(dir)%normal_vec(lf,2) = -(vXYZ(l4,1) - vXYZ(l1,1))
          
          ! ... tangent
          fv%face(dir)%tangent_vec(lf,1,1) =  vXYZ(l4,1) - vXYZ(l1,1)
          fv%face(dir)%tangent_vec(lf,2,1) =  vXYZ(l4,2) - vXYZ(l1,2)
          ! ... second, out of plane tangent
          fv%face(dir)%tangent_vec(lf,:,2) = 0.0_rfreal
          
          ! ... edge length and one diagonal
          L14 = 0.0_rfreal
          do k = 1, grid%ND
             L14 = L14 + fv%face(dir)%tangent_vec(lf,k,1)*fv%face(dir)%tangent_vec(lf,k,1)
          end do
          L14 = sqrt(L14)
          
          fv%face(dir)%area(lf) = L14
          
          ! ... divide normal and tangent by lenth
          fv%face(dir)%normal_vec(lf,:) = fv%face(dir)%normal_vec(lf,:)/L14
          fv%face(dir)%tangent_vec(lf,:,1) = fv%face(dir)%tangent_vec(lf,:,1)/L14

          if(i == Nc(1)) then
             lf = (j-1)*Nf(1) + i + 1
 
             ! ... normal
             fv%face(dir)%normal_vec(lf,1) = vXYZ(l3,2) - vXYZ(l2,2)
             fv%face(dir)%normal_vec(lf,2) = -(vXYZ(l3,1) - vXYZ(l2,1))

             ! ... tangent
             fv%face(dir)%tangent_vec(lf,1,1) =  vXYZ(l3,1) - vXYZ(l2,1)
             fv%face(dir)%tangent_vec(lf,2,1) =  vXYZ(l3,2) - vXYZ(l2,2)
             ! ... second, out of plane tangent
             fv%face(dir)%tangent_vec(lf,:,2) = 0.0_rfreal

             ! ... edge length and one diagonal
             L23 = 0.0_rfreal
             do k = 1, grid%ND
                L23 = L23 + fv%face(dir)%tangent_vec(lf,k,1)*fv%face(dir)%tangent_vec(lf,k,1)
             end do
             L23 = sqrt(L23)

             fv%face(dir)%area(lf) = L23

             ! ... divide normal and tangent by lenth
             fv%face(dir)%normal_vec(lf,:) = fv%face(dir)%normal_vec(lf,:)/L23
             fv%face(dir)%tangent_vec(lf,:,1) = fv%face(dir)%tangent_vec(lf,:,1)/L23
          end if

          dir = 2
          lf = (j-1)*Nc(1) + i

          ! ... normal
          fv%face(dir)%normal_vec(lf,1) = vXYZ(l1,2) - vXYZ(l2,2)
          fv%face(dir)%normal_vec(lf,2) = -(vXYZ(l1,1) - vXYZ(l2,1))
          
          ! ... tangent
          fv%face(dir)%tangent_vec(lf,1,1) =  vXYZ(l1,1) - vXYZ(l2,1)
          fv%face(dir)%tangent_vec(lf,2,1) =  vXYZ(l1,2) - vXYZ(l2,2)
          ! ... second, out of plane tangent
          fv%face(dir)%tangent_vec(lf,:,2) = 0.0_rfreal
          
          ! ... edge length and one diagonal
          L12 = 0.0_rfreal
          do k = 1, grid%ND
             L12 = L12 + fv%face(dir)%tangent_vec(lf,k,1)*fv%face(dir)%tangent_vec(lf,k,1)
          end do
          L12 = sqrt(L12)
          
          fv%face(dir)%area(lf) = L12
          
          ! ... divide normal and tangent by length
          fv%face(dir)%normal_vec(lf,:) = fv%face(dir)%normal_vec(lf,:)/L12
          fv%face(dir)%tangent_vec(lf,:,1) = fv%face(dir)%tangent_vec(lf,:,1)/L12

          if(j == Nc(2)) then
             lf = (j)*Nc(1) + i

             ! ... normal
             fv%face(dir)%normal_vec(lf,1) = vXYZ(l4,2) - vXYZ(l3,2)
             fv%face(dir)%normal_vec(lf,2) = -(vXYZ(l4,1) - vXYZ(l3,1))
          
             ! ... tangent
             fv%face(dir)%tangent_vec(lf,1,1) =  vXYZ(l4,1) - vXYZ(l3,1)
             fv%face(dir)%tangent_vec(lf,2,1) =  vXYZ(l4,2) - vXYZ(l3,2)
             ! ... second, out of plane tangent
             fv%face(dir)%tangent_vec(lf,:,2) = 0.0_rfreal
             
             ! ... edge length
             L34 = 0.0_rfreal
             do k = 1, grid%ND
                L34 = L34 + fv%face(dir)%tangent_vec(lf,k,1)*fv%face(dir)%tangent_vec(lf,k,1)
             end do
             L34 = sqrt(L34)
             
             fv%face(dir)%area(lf) = L34
          
             ! ... divide normal and tangent by length
             fv%face(dir)%normal_vec(lf,:) = fv%face(dir)%normal_vec(lf,:)/L34
             fv%face(dir)%tangent_vec(lf,:,1) = fv%face(dir)%tangent_vec(lf,:,1)/L34
          end if

          ! ... calculate the cell area
          v13 = 0.0_rfreal
          v24 = 0.0_rfreal
          do k = 1, grid%ND
             v13(k) = vXYZ(l3,k)-vXYZ(l1,k)
             v24(k) = vXYZ(l4,k)-vXYZ(l2,k)
          end do

          call cross_product(v13,v24,Vol)
          
          fv%vol(lc) = 0.5_rfreal*sqrt(dot_product(Vol,Vol))
          
       end do ! ... i
    end do ! ... j
 

  end Subroutine CellFace2D
  
  Subroutine CellFace3D(grid,vXYZ)

    USE ModGlobal
    USE ModDataStruct
    USE ModInput
    USE ModMPI

    Implicit None
    
    Type(t_grid), pointer :: grid
    Real(rfreal), pointer :: vXYZ(:,:)

    ! ... local
    Type(t_fvgrid), pointer :: fv
    Type(t_fvface), pointer :: face
    Integer, pointer :: Nc(:)
    Real(rfreal) :: Area(3,3), Vol(3), v17(3)
    Integer :: indices(4), Nf(3)
    Integer :: i, j, k, dir, m
    Integer :: lc, lf, l1, l2, l3, l4, l7

    ! ... simplicity
    fv => grid%fv
    Nc => fv%Nc

    do i = 1, grid%ND
       Nf(i) = Nc(i)+1
    end do

    !!!!!!!!! ... 3D Cell ... !!!!!!!!!
    !                                 !
    !            4------3             !
    !            |\     |\            !
    !            | 8------7           !
    !            1-|----2 |           !
    !             \|     \|           !
    !              5------6 
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

    ! ... loop through the cells (not including ghost)
    do k = 1, fv%Nc(3)
       do j = 1, fv%Nc(2)
          do i = 1, fv%Nc(1)
             ! ... cell index
             lc = (k-1)*Nc(2)*Nc(1) + (j-1)*Nc(1) + i
             
             ! ... find areas in each direction
             dir = 1
             ! ... id the indices of face vertices
             ! ... right handed coordinate system
             lf = (k-1)*Nc(2)*Nf(1) + (j-1)*Nf(1) + i
             l1 = (k-1)*Nf(2)*Nf(1) + (j-1)*Nf(1) + i
             l2 = (k-1)*Nf(2)*Nf(1) + (j)*Nf(1)   + i
             l3 = (k)*Nf(2)*Nf(1)   + (j)*Nf(1)   + i
             l4 = (k)*Nf(2)*Nf(1)   + (j-1)*Nf(1) + i
             
             ! ... calculate the areas, normals, tangents
             indices(1:4) = (/l1, l2, l3, l4/)
             face => fv%face(dir)
             call FaceArea3D(face,lf,vXYZ,indices)

             ! ... record area for the cell volume calculation
             do m = 1, 3
                Area(dir,m) = fv%face(dir)%normal_vec(lf,m)*fv%face(dir)%area(lf)
             end do

             if(i == fv%Nc(1)) then
                ! ... id the indices of face vertices
                ! ... right handed coordinate system
                lf = (k-1)*Nc(2)*Nf(1) + (j-1)*Nf(1) + i+1
                l1 = (k-1)*Nf(2)*Nf(1) + (j-1)*Nf(1) + i+1
                l2 = (k-1)*Nf(2)*Nf(1) + (j)*Nf(1)   + i+1
                l3 = (k)*Nf(2)*Nf(1)   + (j)*Nf(1)   + i+1
                l4 = (k)*Nf(2)*Nf(1)   + (j-1)*Nf(1) + i+1

                ! ... calculate the areas, normals, tangents
                indices(1:4) = (/l1, l2, l3, l4/)
                call FaceArea3D(face,lf,vXYZ,indices)
             end if

             ! ... find areas in each direction
             dir = 2
             ! ... id the indices of face vertices
             ! ... right handed coordinate system
             lf = (k-1)*Nf(2)*Nc(1) + (j-1)*Nc(1) + i
             l1 = (k-1)*Nf(2)*Nf(1) + (j-1)*Nf(1) + i
             l2 = (k)*Nf(2)*Nf(1)   + (j-1)*Nf(1) + i
             l3 = (k)*Nf(2)*Nf(1)   + (j-1)*Nf(1) + i+1
             l4 = (k-1)*Nf(2)*Nf(1) + (j-1)*Nf(1) + i+1
             
             ! ... calculate the areas, normals, tangents
             indices(1:4) = (/l1, l2, l3, l4/)
             face => fv%face(dir)
             call FaceArea3D(face,lf,vXYZ,indices)

             ! ... record area for the cell volume calculation
             do m = 1, 3
                Area(dir,m) = fv%face(dir)%normal_vec(lf,m)*fv%face(dir)%area(lf)
             end do


             if(j == fv%Nc(2)) then
                ! ... id the indices of face vertices
                ! ... right handed coordinate system
                lf = (k-1)*Nf(2)*Nc(1) + (j)*Nc(1) + i
                l1 = (k-1)*Nf(2)*Nf(1) + (j)*Nf(1) + i
                l2 = (k)*Nf(2)*Nf(1)   + (j)*Nf(1) + i
                l3 = (k)*Nf(2)*Nf(1)   + (j)*Nf(1) + i+1
                l4 = (k-1)*Nf(2)*Nf(1) + (j)*Nf(1) + i+1

                ! ... calculate the areas, normals, tangents
                indices(1:4) = (/l1, l2, l3, l4/)
                call FaceArea3D(face,lf,vXYZ,indices)
             end if

             dir = 3
             ! ... id the indices of face vertices
             ! ... right handed coordinate system
             lf = (k-1)*Nc(2)*Nc(1) + (j-1)*Nc(1) + i
             l1 = (k-1)*Nf(2)*Nf(1) + (j-1)*Nf(1) + i
             l2 = (k-1)*Nf(2)*Nf(1) + (j-1)*Nf(1) + i+1
             l3 = (k-1)*Nf(2)*Nf(1) + (j)*Nf(1)   + i+1
             l4 = (k-1)*Nf(2)*Nf(1) + (j)*Nf(1)   + i
             
             ! ... calculate the areas, normals, tangents
             indices(1:4) = (/l1, l2, l3, l4/)
             face => fv%face(dir)
             call FaceArea3D(face,lf,vXYZ,indices)

             ! ... record area for the cell volume calculation
             do m = 1, 3
                Area(dir,m) = fv%face(dir)%normal_vec(lf,m)*fv%face(dir)%area(lf)
             end do


             if(k == fv%Nc(3)) then
                ! ... id the indices of face vertices
                ! ... right handed coordinate system
                lf = (k)*Nc(2)*Nc(1) + (j-1)*Nc(1) + i
                l1 = (k)*Nf(2)*Nf(1) + (j-1)*Nf(1) + i
                l2 = (k)*Nf(2)*Nf(1) + (j-1)*Nf(1) + i+1
                l3 = (k)*Nf(2)*Nf(1) + (j)*Nf(1)   + i+1
                l4 = (k)*Nf(2)*Nf(1) + (j)*Nf(1)   + i

                ! ... calculate the areas, normals, tangents
                indices(1:4) = (/l1, l2, l3, l4/)
                call FaceArea3D(face,lf,vXYZ,indices)
             end if

             ! ... calculate the volume
             l1 = (k-1)*Nf(2)*Nf(1) + (j-1)*Nf(1) + i
             l7 = (k)*Nf(2)*Nf(1) + (j)*Nf(1) + i+1
             v17 = (vXYZ(l7,:)-vXYZ(l1,:))
             Vol(:) = (Area(1,:) + Area(2,:) + Area(3,:))*1/(3.0_rfreal)

             fv%vol(lc) = dot_product(Vol,v17)
                     
          end do ! ... i
       end do ! ... j
    end do ! ... k

  end Subroutine CellFace3D

  Subroutine FaceArea3D(face,lf,vXYZ,indices)

    USE ModGlobal
    USE ModDataStruct
    USE ModMPI
    USE ModMatrixVectorOps

    Implicit None
    
    Type(t_fvface), pointer :: face
    Real(rfreal), pointer :: vXYZ(:,:)
    Integer :: indices(4)
    Integer :: lf

    ! ... local
    Real(rfreal), dimension(3) :: v12, v13, v14, v24, v23, v43
    Real(rfreal), dimension(3) :: A, normal, tan1, tan2
    Real(rfreal) :: L12, L14, L43, L23
    Real(rfreal) :: DotProd1, DotProd2, VecMag
    Integer :: l1, l2, l3, l4
    Integer :: k
    
    l1 = indices(1)
    l2 = indices(2)
    l3 = indices(3)
    l4 = indices(4)
    
    L12 = 0.0_rfreal
    L14 = 0.0_rfreal
    L43 = 0.0_rfreal
    L23 = 0.0_rfreal
    
    ! ... calculate the cell edges
    do k = 1, 3
       v12(k) = vXYZ(l2,k) - vXYZ(l1,k)
       v13(k) = vXYZ(l3,k) - vXYZ(l1,k)
       v14(k) = vXYZ(l4,k) - vXYZ(l1,k)
       v24(k) = vXYZ(l4,k) - vXYZ(l2,k)
       
       v43(k) = vXYZ(l3,k) - vXYZ(l4,k)
       v23(k) = vXYZ(l3,k) - vXYZ(l2,k)

       L12 = L12 + v12(k)*v12(k)
       L14 = L14 + v14(k)*v14(k)
       L43 = L43 + v43(k)*v43(k)
       L23 = L23 + v23(k)*v23(k)
    end do

    L12 = sqrt(L12)
    L14 = sqrt(L14)
    L43 = sqrt(L43)
    L23 = sqrt(L23)

    do k = 1, 3
       tan1(k) = (v12(k)/L12 + v43(k)/L43)*0.5_rfreal
       tan2(k) = (v14(k)/L14 + v23(k)/L23)*0.5_rfreal
    end do
    
    ! ... Area = 1/2*|diag_1 x diag_2|
    call cross_product(v13,v24,A)
    face%area(lf) = sqrt(dot_product(A,A))

    ! ... unit normal
    normal(:) = A(:)/face%area(lf)
    
    ! ... now orthogonalize the tangent vector
    DotProd1 = Dot_Product(normal,tan1)
    VecMag = 0.0_rfreal
    do k = 1,3
       tan1(k) = tan1(k) - DotProd1*normal(k)
       VecMag = VecMag + tan1(k)*tan1(k)
    end do
    ! ... normalize
    tan1(:) = tan1(:)/sqrt(VecMag)

    ! ... now orthogonalize the other tangent vector
    DotProd1 = Dot_Product(normal,tan2)
    DotProd2 = Dot_Product(tan1,tan2)
    VecMag = 0.0_rfreal
    do k = 1,3
       tan2(k) = tan2(k) - DotProd1*normal(k) - DotProd2*tan1(k)
       VecMag = VecMag + tan2(k)*tan2(k)
    end do
    ! ... normalize
    tan2(:) = tan2(:)/sqrt(VecMag)

    ! ... store the data
    do k = 1, 3
       face%normal_vec(lf,k) = normal(k)
       face%tangent_vec(lf,k,1) = tan1(k)
       face%tangent_vec(lf,k,2) = tan2(k)
    end do
       
    face%area(lf) = face%area(lf)*0.5_rfreal

  end Subroutine FaceArea3D

  Subroutine CellVertices(region,gridID,vXYZ)

    USE ModGlobal
    USE ModDataStruct
    USE ModMPI
    USE ModDeriv

    Implicit None

    Type(t_region), pointer :: region
    Real(rfreal), pointer :: vXYZ(:,:)
    Integer :: gridID

    ! ... local
    Type(t_grid), pointer :: grid
    Type(t_mixt_input), pointer :: input
    Type(t_fvgrid), pointer :: fv
    Real(rfreal), pointer :: XYZAux(:,:), F(:)
    Integer, pointer :: ND(:)
    Integer :: Nf(MAX_ND), N(MAX_ND), Ng(MAX_ND)
    Integer, pointer :: indices(:)
    
    integer :: i, j, k, lv, dir, index, nVert, l0, m
    integer :: l1, l2, l3, l4, l5, l6, l7, l8

    ! ... setup the finite volume discretization 
    ! ... for each grid
    grid  => region%grid(gridID)
    fv    => grid%fv
    input => grid%input
    
    N = 1; Ng = 1; Nf = 1
    do dir = 1, grid%ND
       N(dir) = grid%ie(dir)-grid%is(dir)+1
       Ng(dir) = N(dir) + SUM(grid%fv%nGhost(dir,:))
    end do

    ! ... dual-cell discretization:

    ! ... number of faces in each direction
    ! ... is one more than number of cells
    do i = 1, grid%ND
       Nf(i) = fv%Nc(i) + 1
    end do

    ! ... allocate auxiliary coordinates array
    ! ... need to know neighboring coordinates in 
    ! ... order to calculate cell vertices
    allocate(XYZAux(product(Ng),grid%ND))
    
    XYZAux(:,:) = 0.0_rfreal

    ! ... fill the auxiliary array with known data
    do k = 1, N(3)
       do j = 1, N(2)
          do i = 1, N(1)
             l0 = (k-1)*N(2)*N(1) + (j-1)*N(1) + i
             l1 = (k-1+grid%fv%nGhost(3,1))*Ng(2)*Ng(1) + &
                  (j-1+grid%fv%nGhost(2,1))*Ng(1) + &
                  i + grid%fv%nGhost(1,1)
             XYZAux(l1,:) = grid%XYZ(l0,:)
          end do
       end do
    end do

    allocate(F(product(Ng)))

    ! ... fill XYZAux ghost data
    do k = 1, grid%ND

       do i = 1, size(XYZAux,1)
          F(i) = 0.0_rfreal
          F(i) = XYZAux(i,k)
       end do

       call Ghost_Cell_Exchange_FV(region,F,gridID)

       do i = 1, size(XYZAux,1)
          XYZAux(i,k) = F(i)
       end do

    end do
    
    deallocate(F)

    ! ... number of vertices
    if(grid%ND == 2) then
       nVert = 4
    elseif(grid%ND == 3) then
       nVert = 8
    else
       call graceful_exit(region%myrank, 'FV not set up for 1D!')
    end if

    ! ... set up the volumes
    ! ... loop through the vertices
    do k = 1, Nf(3)
       do j = 1, Nf(2)
          do i = 1, Nf(1)

             ! ... vertex number
             lv = (k-1)*Nf(2)*Nf(1) + (j-1)*Nf(1) + i

             ! ... node indices (8 of them in 3D)
             l1 = (k-1)*Ng(2)*Ng(1) + (j-1)*Ng(1) + i
             l2 = (k-1)*Ng(2)*Ng(1) + (j-1)*Ng(1) + i + 1
             l3 = (k-1)*Ng(2)*Ng(1) + (j)*Ng(1) + i + 1
             l4 = (k-1)*Ng(2)*Ng(1) + (j)*Ng(1) + i
             l5 = (k)*Ng(2)*Ng(1) + (j-1)*Ng(1) + i
             l6 = (k)*Ng(2)*Ng(1) + (j-1)*Ng(1) + i + 1
             l7 = (k)*Ng(2)*Ng(1) + (j)*Ng(1) + i + 1
             l8 = (k)*Ng(2)*Ng(1) + (j)*Ng(1) + i

             if (grid%ND == 2) then
                allocate(indices(4))
                indices(1:4) = (/l1, l2, l3, l4/)
             elseif(grid%ND == 3) then
                allocate(indices(8))
                indices(1:8) = (/l1, l2, l3, l4, l5, l6, l7, l8/)
             end if

             do dir = 1, grid%ND
                do index = 1, nVert
                   l0 = indices(index)
                   vXYZ(lv,dir) = vXYZ(lv,dir) + XYZAux(l0,dir)
                end do
                vXYZ(lv,dir) = vXYZ(lv,dir)/dble(nVert)
             end do

          end do ! ... i
       end do ! ... j
    end do ! ... k

    deallocate(XYZAux)

  end Subroutine CellVertices

  Subroutine OutputFVData(region)

    USE ModGlobal
    USE ModDataStruct
    USE ModMPI

    Implicit None

    Type(t_region), pointer :: region

    ! ... local
    Type(t_grid), pointer :: grid
    Type(t_mixt_input), pointer :: input
    Type(t_fvgrid), pointer :: fv
    Real(rfreal), pointer :: XYZAux(:,:)
    Integer, pointer :: ND(:)
    Integer, pointer :: Nf(:), N(:), Ng(:)
    Integer, pointer :: indices(:)

    integer :: i, j, k, lc, l0, lf, la
    integer :: dir, m, p, gridID, ierr

    call MPI_BARRIER(mycomm, ierr)
    do gridID = 1, region%nGrids
       grid => region%grid(gridID)
       fv => grid%fv
       do p = 0, grid%numproc_inComm
          if(region%myrank == p) then 
             if(region%myrank == 0) then
                open(unit=10, file='fvData.txt',status='unknown')
             else
                open(unit=10, file='fvData.txt',status='unknown', position='append')
             end if
             write(10,'(A,X,I4)') 'Rank',region%myrank
             do k = 1, fv%Nc(3)
                do j = 1, fv%Nc(2)
                   do i = 1, fv%Nc(1)
                      lc = (k-1)*fv%Nc(2)*fv%Nc(1)+(j-1)*fv%Nc(1) + i
                      write(10,'(A,3(X,I3))') 'i,j,k =', i,j,k
                      write(10,'(A,I5,A,D13.6)') 'Volume(',lc,')= ',fv%vol(lc)
                      do dir = 1, grid%ND
                         if (dir == 1) la = (k-1)*(fv%Nc(1)+1)*fv%Nc(2)+(j-1)*(fv%Nc(1)+1)+i 
                         if (dir == 2) la = (k-1)*(fv%Nc(2)+1)*fv%Nc(1)+(j-1)*fv%Nc(1)+i 
                         if (dir == 3) la = (k-1)*fv%Nc(2)*fv%Nc(1)+(j-1)*fv%Nc(1)+i 
                         write(10,'(A,I3,A,D13.6)') '  Area(',dir,')= ',fv%face(dir)%area(la)
                         !do m = 1, grid%ND
                            write(10,'(A,3(D13.6))') '       Normal = ',(fv%face(dir)%normal_vec(la,m),m=1,grid%ND)
                            write(10,'(A,3(D13.6))') '    Tangent 1 = ',(fv%face(dir)%tangent_vec(la,m,1),m=1,grid%ND)
                            write(10,'(A,3(D13.6))') '    Tangent 2 = ',(fv%face(dir)%tangent_vec(la,m,2),m=1,grid%ND)
                      end do
                   end do
                end do
             end do
             close(10)
           end if
          call MPI_BARRIER(mycomm,ierr) 
      end do
    end do

  end Subroutine OutputFVData


  Subroutine FVVectorization(region,ng)
    
    USE ModGlobal
    USE ModDataStruct
    USE ModMPI
    
    Implicit None

    Type(t_region), pointer :: region
    Integer :: ng
    
    ! ... local
    type(t_grid), pointer :: grid
    type(t_mixt_input), pointer :: input
    type(t_fvgrid), pointer :: fv
    type(t_mixt), pointer :: state
    type(t_fvface), pointer :: face
    Integer, pointer :: LRindex(:,:)

    Integer :: N(MAX_ND), Ncg(MAX_ND), Nf(MAX_ND), Nc(MAX_ND), Nfdir(MAX_ND)
    Integer :: i, j, k, dir, m, l0, lfl, lfr, lc, lf, N12pts
    
    ! ... simplicity
    grid => region%grid(ng)
    input => grid%input

    N = 1; Ncg = 1; Nf = 1; Nc = 1
    do i = 1, grid%ND
       N(i) = grid%ie(i)-grid%is(i) + 1
       Ncg(i) = N(i) + SUM(grid%fv%nGhost(i,:))
       Nf(i) = Ncg(i) - 1
       Nc(i) = Nf(i) - 1
    end do

    
    N12pts = Ncg(1)*Ncg(2)
    if(grid%ND < 3) then
       Nc(3) = 1
       N12pts = 0
    end if
    
    do dir = 1, grid%ND
       
       Nfdir(:) = Nc(:)
       Nfdir(dir) = Nf(dir)

       ! ... simplicity
       face => grid%fv%face(dir)
       allocate(grid%fv%face(dir)%LRindex(product(Nfdir(:)),2))
       LRindex => face%LRIndex

       do k = 1, Nfdir(3)
          do j = 1, Nfdir(2)
             do i = 1, Nfdir(1)
                lf = (k-1)*Nfdir(1)*Nfdir(2) + (j-1)*Nfdir(1) + (i-1) + 1
                
                if(dir == 1) then
                   LRindex(lf,1) = (k)*N12pts + (j)*Ncg(1) + (i-1) + 1
                   LRindex(lf,2) = (k)*N12pts + (j)*Ncg(1) + (i) + 1
                elseif(dir == 2) then
                   LRindex(lf,1) = (k)*N12pts + (j-1)*Ncg(1) + (i) + 1
                   LRindex(lf,2) = (k)*N12pts + (j)*Ncg(1) + (i) + 1
                elseif(dir == 3) then
                   LRindex(lf,1) = (k-1)*N12pts + (j)*Ncg(1) + (i) + 1
                   LRindex(lf,2) = (k)*N12pts + (j)*Ncg(1) + (i) + 1
                end if
    
             end do ! ... i
          end do ! ... j
       end do ! ... k
    end do ! ... dir

  end Subroutine FVVectorization


End Module ModFVSetup
![/UNUSED]
