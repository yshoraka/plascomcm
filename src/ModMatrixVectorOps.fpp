! Copyright (c) 2015, University of Illinois at Urbana-Champaign, XPACC
! License: MIT, http://opensource.org/licenses/MIT
!-----------------------------------------------------------------------
! ModMatrixVectorOps.f90
!  
! - basic code to manipulate matrices
!
! Revision history
! - 29 Dec 2006 : DJB : initial code
! - 12 Jan 2007 : DJB : modification for vectorized ops
!
!-----------------------------------------------------------------------

MODULE ModMatrixVectorOps

  !USE ModDataStruct

  !Interface MV_Mult
  !  Module Procedure MV_Mult_Sparse_CDS, MV_Mult_Sparse_CRS
  !End Interface

CONTAINS

  !---------------------------------------------------------------------
  ! MV_Mult_Sparse: 
  !
  ! - Perform a matrix-vector multiply of a banded matrix C with a
  !   vector f
  !
  ! - Matrix C is stored in Compressed Diagonal Storage or in
  !   Compressed Row Storage
  !
  ! - All data is assumed local
  !
  !---------------------------------------------------------------------
  subroutine MV_Mult_Sparse(grid, dir, C, F, dF)

    USE ModGlobal
    USE ModDataStruct

    Type (t_matrix_sparse), intent(in) :: C
    Real(RFREAL), POINTER :: F(:), dF(:)
    integer :: dir
    Type (t_grid), pointer :: grid

    ! ... local variables
    integer :: diag, loc, l0, l1, i, j, k, is, ie, vdsGhost
    real(rfreal) :: local_val
    integer      :: local_ind

    ! ... initialize
    do i = 1, size(dF)
      dF(i) = 0.0_rfreal
    end do

    vdsGhost = grid%vdsGhost(dir)

    do j = 1, C%n
      do k = 1, vdsGhost
        l1 = grid%vec_indGhost(k,j,dir)
        is = C%row_ptr(k,j)
        ie = C%row_ptr(k,j+1)-1
        do i = is, ie
          local_val = C%val(i,k)
          local_ind = C%col_ind(i,k)
          l0 = grid%vec_indGhost(k,local_ind,dir)
          dF(l1) = dF(l1) + local_val * F(l0)
        end do
      end do
    end do

  end subroutine MV_Mult_Sparse

  Subroutine Dense_To_CRS(A, Asparse)

    USE ModGlobal
    USE ModDataStruct

    Real(rfreal), pointer :: A(:,:)
    Type(t_matrix_sparse), pointer :: Asparse

    ! ... local variables
    integer :: nz, i, j, count, counti

    ! ... count non-zero elements
    nz = 0; counti = 0
    do i = 1, size(A,1)
      if (all(abs(A(i,:)) .lt. TINY)) then 
        nz = nz+1 
      else 
        do j = 1, size(A,2)
          if (abs(A(i,j)) .ge. TINY) nz = nz + 1
        end do
      end if
    end do

    ! ... allocate
    allocate(Asparse%val(1,nz), Asparse%col_ind(1,nz))
    allocate(Asparse%row_ptr(1,size(A,1)+1))
    allocate(Asparse%nnz(1))
    Asparse%row_ptr = (size(A,1)+1)**2

    ! ... populate
    count = 0; counti = 0;
    do i = 1, size(A,1)
      if (all(abs(A(i,:)) .lt. TINY)) then 
        count = count + 1
        Asparse%val(1,count) = 0.0_rfreal
        Asparse%col_ind(1,count) = i
        Asparse%row_ptr(1,i) = MIN(Asparse%row_ptr(1,i),count)
      else
        do j = 1, size(A,2)
          if (abs(A(i,j)) .ge. TINY) then
            count = count + 1
            Asparse%val(1,count) = A(i,j)
            Asparse%col_ind(1,count) = j
            Asparse%row_ptr(1,i) = MIN(Asparse%row_ptr(1,i),count)
          end if
        end do
      end if
    end do

    Asparse%row_ptr(1,size(A,1)+1) = nz+1
    Asparse%nnz(1) = nz

  end subroutine Dense_To_CRS

  Subroutine CRS_To_Dense(Asparse, k, A)

    USE ModGlobal
    USE ModDataStruct

    Real(rfreal), pointer :: A(:,:)
    Type(t_matrix_sparse) :: Asparse

    ! ... local variables
    integer :: nz, i, j, k, count, n, local_ind, vds
    real(rfreal) :: local_val

    ! ... allocate
    A(:,:) = 0.0_rfreal

    do j = 1, Asparse%n
      do i = Asparse%row_ptr(k,j), Asparse%row_ptr(k,j+1)-1
        local_ind = Asparse%col_ind(k,i)
        if (local_ind /= 0) then
          A(j,local_ind) = Asparse%val(k,i)
        end if
      end do
    end do

  end subroutine CRS_To_Dense

  Subroutine Write_CRS_To_File(Asparse, k, fname)

    USE ModGlobal
    USE ModDataStruct

    ! ... incoming variables
    Character(len=*) :: fname
    Type(t_matrix_sparse) :: Asparse
    Integer :: k

    ! ... local variables
    integer :: nz, i, j, count, n, local_ind, vds
    real(rfreal) :: local_val
    integer, parameter :: funit = 10

    open(unit=funit,file=trim(fname),status='unknown')
    write(funit,'(A,I4,A,I4,A)') 'A = zeros(',Asparse%n,',',Asparse%n,');'
    do j = 1, Asparse%n
      do i = Asparse%row_ptr(k,j), Asparse%row_ptr(k,j+1)-1
        local_ind = Asparse%col_ind(k,i)
        if (local_ind /= 0) then
          write (funit,'(A,I4,A,I4,A,E20.8,A)') 'A(',j,',',local_ind,') = ', Asparse%val(k,i),';'
        end if
      end do
    end do
    close(funit)

  end subroutine Write_CRS_To_File

  Subroutine CRS_Set_Val(A, ip, jp, val, vds_s, vds_e)

    USE ModGlobal
    USE ModDataStruct

    Type (t_matrix_sparse), pointer :: A
    Integer :: ip, jp, vds_s, vds_e, vds
    Real (rfreal) :: val

    ! ... local variables
    Integer :: i, j, k, new_nnz, n, i1, i2, kp
    Real(rfreal), pointer :: new_val(:,:), new_col_ind(:,:), new_row_ptr(:)

    ! ... size of matrix (n x n)
    n = A%n

    ! ... number of vectorized points
    vds = size(A%row_ptr,1)

    Do k = vds_s, vds_e

      i1 = A%row_ptr(k,ip); 
      i2 = A%row_ptr(k,ip+1);

      If (ANY(A%col_ind(k,i1:i2) == jp)) Then  ! value exists, change it

        do i = i1, i2
          if (A%col_ind(k,i) == jp) Then
            A%val(k,i) = val
          end if
        end do

      Else                                    ! entry does not exist, add it

        !print *, 'entry does not exist.'

        new_nnz = A%nnz(k) + 1

        If (new_nnz > MAXVAL(A%nnz)) Then         ! must reallocat

          !print *, 'must reallocate'

          ! ... space for old values
          allocate(new_val(vds,new_nnz), new_col_ind(vds,new_nnz))

          do kp = 1, vds

            new_val(kp,1:A%nnz(kp)) = A%val(kp,:)
            new_col_ind(kp,1:A%nnz(kp)) = A%col_ind(kp,:)

          end do

          ! ... deallocate old data
          deallocate(A%val, A%col_ind)

          ! ... reallocate
          allocate(A%val(vds,new_nnz), A%col_ind(vds,new_nnz))

          ! ... copy to new array
          do kp = 1, vds

            A%val(kp,1:A%nnz(kp)) = new_val(kp,1:A%nnz(kp))
            A%col_ind(kp,1:A%nnz(kp)) = new_col_ind(kp,1:A%nnz(kp))

          end do

          ! ... deallocate memory
          deallocate(new_val, new_col_ind)

        End if

        allocate(new_val(1,new_nnz), new_col_ind(1,new_nnz), new_row_ptr(n+1))

        ! ... inserting a new value of (ip,jp) will move col_ind, val
        i1 = A%row_ptr(k,ip+1)
        i2 = A%nnz(k)
        new_col_ind(1,i1:i2) = A%col_ind(k,i1:i2)
        new_val(1,i1:i2) =     A%val(k,i1:i2)
        A%col_ind(k,(i1+1):(i2+1)) = new_col_ind(1,i1:i2)
        A%val(k,(i1+1):(i2+1))     = new_val(1,i1:i2)
        A%val(k,i1) = val
        A%col_ind(k,i1) = jp;

        !print *, A%val(k,new_nnz)

        ! ... inserting a new value at (ip,jp) will affect row_ptr(ip+1:end)
        do kp = ip+1, n
          A%row_ptr(k,kp) = A%row_ptr(k,kp)+1
        end do

        ! ... update the nnz
        A%nnz(k) = new_nnz
        A%row_ptr(k,n+1) = new_nnz+1

        !print *, A%col_ind(k,1:5)

      End If

    End Do

  End Subroutine CRS_Set_Val

  Subroutine Compress_CRS_Matrix(A, B)

    USE ModGlobal
    USE ModDataStruct

    ! ... function call variables
    Type(t_matrix_sparse) :: A
    Type(t_matrix_sparse), Pointer :: B

    ! ... local variables
    integer :: nz, i, j, k, count, n, local_ind, vds, cs, ce
    real(rfreal) :: local_val
    real(rfreal), allocatable :: val(:)
    integer, allocatable :: loc(:), row_ptr(:)

    ! ... allocate
    allocate(val(size(A%val,2)), loc(size(A%val,2)), row_ptr(size(A%row_ptr,2)))
    val(:) = 0.0_WP
    loc(:) = 0
    row_ptr(:) = 0

    ! ... count variables
    count = 0
    row_ptr(1) = 1
    do i = 1, A%m
      cs = A%row_ptr(1,i)
      ce = A%row_ptr(1,i+1)-1
      if (all(abs(A%val(1,cs:ce)) .le. TINY)) then
        count = count + 1
        val(count) = 0.0_WP
        loc(count) = A%col_ind(1,cs)
        row_ptr(i+1) = count + 1        
      else
        do k = cs, ce
          if (abs(A%val(1,k)) > TINY .AND. (A%col_ind(1,k) > 0)) then
            count = count + 1
            val(count) = A%val(1,k)
            loc(count) = A%col_ind(1,k)
            row_ptr(i+1) = count + 1
          end if
        end do
      end if
    end do

    ! ... copy to new array
    B%m = A%m
    B%n = A%n
    allocate(B%val(1,count), B%col_ind(1,count), B%row_ptr(1,B%m+1))
    B%row_ptr(1,:) = row_ptr(:)
    do i = 1, count
      B%val(1,i) = val(i)
      B%col_ind(1,i) = loc(i)
    end do
    
    ! ... boundary rows
    allocate(B%nbc_row(1),B%bc_row(1,A%nbc_row(1)))
    B%nbc_row(1) = A%nbc_row(1)
    do i = 1, B%nbc_row(1)
      B%bc_row(1,i) = A%bc_row(1,i)
    end do

    ! ... clean and leave
    deallocate(val,loc,row_ptr)

  end subroutine Compress_CRS_Matrix

  subroutine cross_product(v1, v2, y)

    USE ModGlobal

    Real(rfreal) :: v1(MAX_ND), v2(MAX_ND), y(MAX_ND)

    y(1) = v1(2)*v2(3) - v1(3)*v2(2)
    y(2) = v1(3)*v2(1) - v1(1)*v2(3)
    y(3) = v1(1)*v2(2) - v1(2)*v2(1)

  end subroutine cross_product

  subroutine MV_Mult_Sparse_New(grid, dir, C, F, dF)

    USE ModGlobal
    USE ModDataStruct

    Type (t_matrix_sparse), intent(in) :: C
    Real(RFREAL), POINTER :: F(:), dF(:)
    integer :: dir
    Type (t_grid), pointer :: grid

    ! ... local variables
    integer :: diag, loc, l0, l1, i, j, jj, k, is, ie, vdsGhost
    real(rfreal) :: local_val
    integer      :: local_ind

    ! ... declarations for pointer dereference, WZhang 05/2014
    integer, pointer :: nbc_row(:), bc_row(:,:), vec_indGhost(:,:,:), row_ptr(:,:), col_ind(:,:)
    real(rfreal), pointer :: val(:,:)

    ! ... pointer dereference, WZhang 05/2014
    nbc_row      => C%nbc_row
    bc_row       => C%bc_row
    vec_indGhost => grid%vec_indGhost
    row_ptr      => C%row_ptr
    val          => C%val
    col_ind      => C%col_ind
    
!!$    ! ... initialize
!!$    do i = 1, size(dF)
!!$      dF(i) = 0.0_rfreal
!!$    end do

    vdsGhost = grid%vdsGhost(dir)

    do k = 1, vdsGhost
      do jj = 1, nbc_row(k)
         j = bc_row(k,jj)
        l1 = vec_indGhost(k,j,dir)
        is = row_ptr(k,j)
        ie = row_ptr(k,j+1)-1
        dF(l1) = 0.0_8
        do i = is, ie
          local_val = val(i,k)
          local_ind = col_ind(i,k)
          l0 = vec_indGhost(k,local_ind,dir)
          dF(l1) = dF(l1) + local_val * F(l0)
        end do
      end do
    end do

  end subroutine MV_Mult_Sparse_New


  subroutine MV_Mult_Sparse_ZERO(grid, dir, C, F, dF)

    USE ModGlobal
    USE ModDataStruct

    Type (t_matrix_sparse), intent(in) :: C
    Real(RFREAL), POINTER ::  F(:), dF(:)
    integer :: dir
    Type (t_grid), pointer :: grid

    ! ... local variables
    integer :: diag, loc, l0, l1, i, j, jj, k, vdsGhost
    real(rfreal) :: local_val
    integer      :: local_ind

!!$    ! ... initialize
!!$    do i = 1, size(dF)
!!$      dF(i) = 0.0_rfreal
!!$    end do

    vdsGhost = grid%vdsGhost(dir)

    do k = 1, vdsGhost
      do jj = 1, C%nbc_row(k)
         j = C%bc_row(k,jj)
        l1 = grid%vec_indGhost(k,j,dir)
!        is = C%row_ptr(k,j)
!        ie = C%row_ptr(k,j+1)-1
        dF(l1) = 0.0_8
      end do
    end do

  end subroutine MV_Mult_Sparse_ZERO

  subroutine MV_Mult_Sparse_Simple(grid, dir, order, C, F, dF)
    
    USE ModGlobal
    USE ModDataStruct
    USE ModMPI

    type(t_matrix_sparse), intent(in) :: C
    real(RFREAL), pointer :: F(:), dF(:)
    integer :: dir, order
    type(t_grid), pointer :: grid
    type(t_mixt_input), pointer :: input

    ! ... local variables
    integer :: i, j, k, vdsGhost, goffset(2), l1, l0, bndry_width, bndry_depth, N, NGhost, Ng
    integer :: offset, bndry_diff, N1, N2, N3, dist, offset_diff, offset_depth
    integer, pointer :: vec_indGhost(:,:,:)
    real(RFREAL), pointer :: rhs_block1(:,:,:), rhs_block2(:,:,:)

    input       => grid%input
    vdsGhost    = grid%vdsGhost(dir)
    goffset(1)  = grid%nGhostRHS(dir,1)
    goffset(2)  = grid%nGhostRHS(dir,2)
    bndry_width = input%operator_bndry_width(order)
    bndry_depth = input%operator_bndry_depth(order)
    N           = grid%ie(dir) - grid%is(dir) + 1
    NGhost      = goffset(1) + goffset(2)
    Ng          = N + NGhost
    N1         = grid%ie(1) - grid%is(1) + 1
    N2         = grid%ie(2) - grid%is(2) + 1
    N3         = grid%ie(3) - grid%is(3) + 1    

    vec_indGhost => grid%vec_indGhost
    rhs_block1   => input%rhs_block1
    rhs_block2   => input%rhs_block2
    bndry_diff = bndry_width - bndry_depth
  
    Select Case (dir)
      Case (1)
        dist = 1
      Case (2)
        dist = N1
      Case (3)
        dist = N1 * N2
    End Select

    offset_diff  = bndry_diff * dist
    offset_depth = Ng - bndry_depth

    ! ... if we have a left boundary, WZhang 06/2014
    if (goffset(1)  == 0) then
      do k = 1, vdsGhost
        do j = 1, bndry_depth
          l1     = vec_indGhost(k, j, dir)
          offset = l1 - j * dist
          dF(l1) = 0.0_8
          do i = 1, bndry_width
            dF(l1) = dF(l1) + rhs_block1(order,j,i) * F(i * dist + offset)
          end do
        end do        
      end do
    end if

    ! ... if we have a right boundary, WZhang 06/2014
    if (goffset(2)  == 0) then
      do k = 1, vdsGhost
        do j = 1, bndry_depth
          l1     = vec_indGhost(k, offset_depth + j, dir)
          dF(l1) = 0.0_8
          offset = l1 - j * dist - offset_diff
          do i = bndry_width, 1, -1
            dF(l1) = dF(l1) + rhs_block2(order,j,i) * F(i * dist + offset)
          end do
        end do
      end do
    end if

  end subroutine MV_Mult_Sparse_Simple

  subroutine MV_Mult_Sparse_Simple_with_holes(grid, dir, order, C, F, dF)
    
    USE ModGlobal
    USE ModDataStruct
    USE ModMPI

    type(t_matrix_sparse), intent(in) :: C
    real(RFREAL), pointer :: F(:), dF(:)
    integer :: dir, order
    type(t_grid), pointer :: grid
    type(t_mixt_input), pointer :: input

    ! ... local variables
    integer :: i, j, k, m, vdsGhost, goffset(2), l1, l0, bndry_width, N, NGhost, Ng, is, ie
    integer :: offset, bndry_diff, N1, N2, N3, dist, offset_diff, offset_depth, bndry_location, bndry_start, bndry_affected
    integer, pointer :: vec_indGhost(:,:,:)
    real(RFREAL), pointer :: rhs_block1(:,:,:), rhs_block2(:,:,:)
    integer, pointer:: l_ctr_lc(:), l_bndry_lc(:,:), r_ctr_lc(:), r_bndry_lc(:,:)
    integer, pointer:: l_extended(:), l_extended_size(:), r_extended(:), r_extended_size(:)
    integer :: bndry_depth_max, bndry_depth_local, bndry_interior, iis, iie
    real(rfreal) :: vals(-MAX_INTERIOR_STENCIL_WIDTH:MAX_INTERIOR_STENCIL_WIDTH)
    integer, pointer :: rhs_interior_stencil_start(:), rhs_interior_stencil_end(:)
    real(rfreal), pointer :: rhs_interior(:,:)


    input       => grid%input
    vdsGhost    = grid%vdsGhost(dir)
    goffset(1)  = grid%nGhostRHS(dir,1)
    goffset(2)  = grid%nGhostRHS(dir,2)
    bndry_width = input%operator_bndry_width(order)
    bndry_depth_local = input%operator_bndry_depth(order)
    bndry_depth_max =   maxval(input%operator_bndry_depth(:))
    N           = grid%ie(dir) - grid%is(dir) + 1
    NGhost      = goffset(1) + goffset(2)
    Ng          = N + NGhost
    N1         = grid%ie(1) - grid%is(1) + 1
    N2         = grid%ie(2) - grid%is(2) + 1
    N3         = grid%ie(3) - grid%is(3) + 1    
    is         = grid%is(dir)
    ie         = grid%ie(dir)

    vec_indGhost => grid%vec_indGhost
    rhs_block1   => input%rhs_block1
    rhs_block2   => input%rhs_block2
    bndry_diff = bndry_width - bndry_depth_local
    offset_depth = Ng - bndry_depth_local

    l_ctr_lc    => grid%l_ctr_lc(:,dir)
    l_bndry_lc  => grid%l_bndry_lc(:,:,dir)
    r_ctr_lc    => grid%r_ctr_lc(:,dir)
    r_bndry_lc  => grid%r_bndry_lc(:,:,dir)

    l_extended  => grid%l_extended(:,dir)
    l_extended_size  => grid%l_extended_size(:,dir)
    r_extended  => grid%r_extended(:,dir)
    r_extended_size  => grid%r_extended_size(:,dir)

    ! ... pointer dereference, WZhang 05/2014
    rhs_interior_stencil_start => input%rhs_interior_stencil_start
    rhs_interior_stencil_end   => input%rhs_interior_stencil_end
    rhs_interior               => input%rhs_interior

    ! ... size of interior stencil
    iis        = rhs_interior_stencil_start(order)
    iie        = rhs_interior_stencil_end(order)

    ! ... store values of interior stencil
    do i = iis, iie
      vals(i) = rhs_interior(order,i)
    end do
  
    bndry_interior = bndry_depth_max - bndry_depth_local

    Select Case (dir)
      Case (1)
        dist = 1
      Case (2)
        dist = N1
      Case (3)
        dist = N1 * N2
    End Select

    ! ... left boundaries, WZhang 09/2014
    do m = 1, vdsGhost

      if (l_ctr_lc(m) > 0) then
        do k = 1, l_ctr_lc(m)
          bndry_location = l_bndry_lc(k,m)
          bndry_start = bndry_location - is + 1 + goffset(1)
          if ((ie - bndry_location + 1) < bndry_depth_max) then
            bndry_affected = ie - bndry_location + 1
          else          
            bndry_affected = bndry_depth_max
          end if
          do j = 1, bndry_affected
            l1     = vec_indGhost(m, bndry_start+j-1, dir)
            dF(l1) = 0.0_8
            if (j + bndry_interior > bndry_depth_max) then
              do i = iis, iie
                l0 = l1 + i*dist
                dF(l1) = dF(l1) + vals(i) * F(l0)
              end do  ! ... i
            else
              do i = 1, bndry_width
                l0 = l1 + (i-j)*dist
                dF(l1) = dF(l1) + rhs_block1(order,j,i) * F(l0)
              end do  ! ... i
            end if
          end do  ! ... j
        end do  ! ... k  
      end if

      ! ... must take care of boundaries extended from neighboring processor
      if (l_extended(m) > 0) then
        bndry_start = 1 + goffset(1)
        bndry_affected = l_extended_size(m)
        offset = bndry_depth_max - bndry_affected

        do j = 1, bndry_affected
          l1     = vec_indGhost(m, bndry_start+j-1, dir)
          dF(l1) = 0.0_8
          if (j + offset + bndry_interior > bndry_depth_max) then
            do i = iis, iie
              l0 = l1 + i*dist
              dF(l1) = dF(l1) + vals(i) * F(l0)
            end do  ! ... i
          else
            do i = 1, bndry_width
              l0 = l1 +(i-j-offset)*dist
              dF(l1) = dF(l1) + rhs_block1(order,j+offset,i) * F(l0)
            end do ! ... i
          end if
        end do ! ... j
      end if
    end do ! ... m

    ! ... right boundaries, WZhang 08/2014
    do m = 1, vdsGhost

      if (r_ctr_lc(m) > 0) then
        do k = 1, r_ctr_lc(m)
          bndry_location = r_bndry_lc(k,m)
          if ((bndry_location - is + 1) < bndry_depth_max) then
            bndry_affected = bndry_location - is + 1
          else          
            bndry_affected = bndry_depth_max
          end if
          offset = bndry_depth_max - bndry_affected
          bndry_start = bndry_location - is + 1 + goffset(1) - bndry_affected + 1
          do j = 1, bndry_affected
            l1     = vec_indGhost(m, bndry_start+j-1, dir)
            dF(l1) = 0.0_8
            if ((j+offset) <= bndry_interior ) then
              do i = iis, iie
                l0 = l1 + i*dist
                dF(l1) = dF(l1) + vals(i) * F(l0)
              end do  ! ... i
            else
              do i = bndry_width, 1, -1
                l0 = l1 + (i-(j+offset-bndry_interior)-bndry_diff)*dist
                dF(l1) = dF(l1) + rhs_block2(order,j-bndry_interior+offset,i) * F(l0)
              end do ! ... i
            end if
          end do  ! ... j
        end do  ! ... k
      end if

      ! ... must take care of boundaries extended from neighboring processor
      if (r_extended(m) > 0) then
        bndry_affected = r_extended_size(m)
        bndry_start = N + goffset(1) - bndry_affected + 1
        do j = 1, bndry_affected
          l1     = vec_indGhost(m, bndry_start+j-1, dir)
          dF(l1) = 0.0_8
          if (j <= bndry_interior) then
            do i = iis, iie
              l0 = l1 + i*dist
              dF(l1) = dF(l1) + vals(i) * F(l0)
            end do  ! ... i
          else
            do i = bndry_width, 1, -1
              l0 = l1 + (i-(j-bndry_interior)-bndry_diff)*dist
              dF(l1) = dF(l1) + rhs_block2(order,j-bndry_interior,i) * F(l0)
            end do ! ... i
          end if
        end do ! ... j
      end if

    end do  ! ... m

  end subroutine MV_Mult_Sparse_Simple_with_holes

END MODULE ModMatrixVectorOps
