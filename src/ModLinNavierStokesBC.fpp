! Copyright (c) 2015, University of Illinois at Urbana-Champaign, XPACC
! License: MIT, http://opensource.org/licenses/MIT

!----------------------------------------------------------------------
!
! ModNavierStokes.f90
! 
! - basic code for the evaluation of the Navier-Stokes equations
!
! Revision history
! - 19 Dec 2006 : DJB : initial code
! - 20 Dec 2006 : DJB : continued evolution
! - 20 Mar 2007 : DJB : initial working Poinsot-Lele BC on gen. meshes
! - 20 Apr 2007 : DJB : initial working Poinsot-Lele BC on moving meshes
! - 10 Jul 2007 : DJB : initial CVS submit
! - 11 Jul 2007 : DJB : initial work on ``multi-dimensional'' Poinsot-Lele
! - 01 Feb 2008 : DJB : merged in JKim's LES & 3D bc routines
! - 29 Jul 2008 : DJB : Initial SAT boundary conditions
! 
! flags == FALSE    : compute inviscid fluxes only
! flags == TRUE     : compute dissipative fluxes only
! flags == COMBINED : inviscid + dissipative fluxes
!
! $Header: /cvsroot/genx/Codes/RocfloCM/Source/ModLinNavierStokesBC.f90,v 1.6 2011/02/02 04:12:10 bodony Exp $
!
!-----------------------------------------------------------------------
MODULE ModLinNavierStokesBC

CONTAINS

  !
  ! Also includes SAT boundary conditions of Kreiss, Nordstrom, Carpenter, ...
  !
  subroutine LinNS_BC(region, ng, flags)

    USE ModGlobal
    USE ModDataStruct
    USE ModDeriv
    USE ModMPI
    USE ModNavierStokesBC

    implicit none

    TYPE (t_region), POINTER :: region
    INTEGER :: ng
    INTEGER :: flags

    ! ... local variables
    type(t_patch), pointer :: patch
    type(t_grid), pointer :: grid
    type(t_mixt), pointer :: state, mean
    type(t_mixt_input), pointer :: input
    real(rfreal), pointer :: gvec(:), SC(:), Pinv(:,:), Pfor(:,:), Tmat(:,:), Tinv(:,:), cvPredictor(:), rhsPredictor(:)
    real(rfreal), pointer :: cons_inviscid_flux(:,:), L_vector(:), new_normal_flux(:), ViscFluxJac(:)
    integer :: ND, Nc, N(MAX_ND), npatch, i, j, l0, k, ii, jj, l1, l2, isign, lp, kk
    real(rfreal) :: UU(MAX_ND), norm(MAX_ND), mag_norm, tang(MAX_ND-1,MAX_ND)
    real(rfreal) :: mag_tang, constant, del, eta, rel_pos(MAX_ND), dsign, denom
    integer :: normDir, tangDir(MAX_ND,MAX_ND-1), Np(MAX_ND)
    real(rfreal) :: d(5), wave_amp(5), wave_spd(5), spd_snd
    real(rfreal) :: un, ut(MAX_ND-1)
    real(rfreal) :: norm_vec(MAX_ND), tang_vec(2,MAX_ND), fvec(MAX_ND,2)
    real(rfreal) :: metricInverse(MAX_ND,MAX_ND), invProj(MAX_ND, MAX_ND)
    real(rfreal) :: sponge_amp, bndry_q(MAX_ND+2), bndry_p(MAX_ND+2), bndry_rhs(MAX_ND+2)
    integer :: sgn, handedness(MAX_ND)
    integer, pointer :: inorm(:)
    real(rfreal) :: dundn, drdn, dpdn, drundn, dut1dn, dut2dn, sigma, SpecRad, KrnkrDlta
    real(rfreal) :: bndry_norm(MAX_ND), p_inf, u_inf, du_inf, un_old, metric_fac, T_stag, T_inf, dT_inf, theta, P_stag
    real(rfreal) :: charVar(MAX_ND+2), primVar(MAX_ND+2), dsgn
    real(rfreal) :: V_wall, V_wall_vec(MAX_ND), V_wall_target, V_wall_target_vec(MAX_ND), A_wall, drhodn, dp_inf, T_wall
    real(rfreal) :: Uhat_alpha(MAX_ND), alpha_x(MAX_ND,MAX_ND), alpha_t(MAX_ND)
    real(rfreal) :: x_alpha(MAX_ND,MAX_ND), primVar_RHS(MAX_ND+2), M2(2,2), M3(3,3)
    real(rfreal) :: SPVOL, RHO, UX, UY, UZ, PRESSURE, XI_X, XI_Y, XI_Z, SPD_SND_INV, KE, ENTHALPY, MACH2, GAMMA, V_DOT_XI, MU,LAMBDAvisc
    real(rfreal) :: XI_X_TILDE, XI_Y_TILDE, XI_Z_TILDE, epsilon, omega, dUdt_inflow, dTdt_inflow, Mi, Ui, Ti, u_inflow, T_inflow
    real(rfreal) :: w_of_t, dwdt, t0, wi, w_inflow, v_inflow, dVdt_inflow, vi, dWdt_inflow
    real(rfreal), pointer :: uB(:), gI1(:), gI2(:), Aprime(:,:), Lambda(:,:), matX(:,:), matXTrans(:,:), penalty(:), gI1w(:)
    real(rfreal) :: radNozz, Tc, u0, r0, coefVel, sigmaI1, sigmaI2, sigma_inter
    real(rfreal) :: junk ! JKim 11/2007
    real(rfreal) :: RHO_TARGET, UX_TARGET, UY_TARGET, UZ_TARGET, PRESSURE_TARGET, U_wall, UVWhat(MAX_ND), UVW(MAX_ND)
    real(rfreal) :: alpha, beta, phi2, ucon, PRESSURE2, bndry_h, gamm1i, T, RHO_P, RHO_T
    real(rfreal), pointer :: pnorm_vec(:), Lambda_in(:,:), Lambda_out(:,:), BI1(:,:), gI1mean(:)
    real(rfreal) :: p_in, p_out, sig_v, sigR(MAX_ND+2), sigL(MAX_ND+2), tmp(MAX_ND+2), tmp2(MAX_ND+2), sigma_scale, ibfac

    tangDir(1,:) = (/2, 3/)
    tangDir(2,:) = (/1, 3/)
    tangDir(3,:) = (/1, 2/)
    handedness = (/1, -1, 1/)

    ! ... simplicity
    ND = region%input%ND

    !allocate(gvec(size(input%bndry_explicit_deriv)))
    !allocate(inorm(size(gvec)))
    allocate(SC(ND+2),Pinv(ND+2,ND+2),Pfor(ND+2,ND+2),Tmat(ND+2,ND+2),Tinv(ND+2,ND+2))
    allocate(cons_inviscid_flux(ND,ND+2),L_vector(ND+2),new_normal_flux(ND+2), pnorm_vec(ND))
    allocate(uB(ND+2), gI1(ND+2), gI2(ND+2), Aprime(ND+2,ND+2), Lambda(ND+2,ND+2), matX(ND+2,ND+2), matXTrans(ND+2,ND+2), penalty(ND+2))
    allocate(Lambda_in(ND+2,ND+2), Lambda_out(ND+2,ND+2),ViscFluxJac(ND+2),BI1(ND+2,ND+2), gI1mean(ND+2))

    do npatch = 1, region%nPatches

      patch => region%patch(npatch)
      grid => region%grid(patch%gridID)
      state => region%state(patch%gridID)
      input => grid%input
      mean  => region%mean(patch%gridID)
      normDir = abs(patch%normDir)
      sgn = normDir / patch%normDir
      dsgn = dble(sgn)

      Nc = grid%nCells
      N  = 1; Np = 1;
      do j = 1, grid%ND
        N(j)  =  grid%ie(j) -  grid%is(j) + 1
        Np(j) = patch%ie(j) - patch%is(j) + 1
      end do;

      if ( patch%bcType /= SPONGE ) then

        Do k = patch%is(3), patch%ie(3)
          Do j = patch%is(2), patch%ie(2)
            Do i = patch%is(1), patch%ie(1)

              l0 = (k- grid%is(3))* N(1)* N(2) + (j- grid%is(2))* N(1) +  i- grid%is(1)+1
              lp = (k-patch%is(3))*Np(1)*Np(2) + (j-patch%is(2))*Np(1) + (i-patch%is(1)+1)

              if ( grid%iblank(l0) == 0 ) CYCLE

              ! ... iblank factor (either 1 or zero)
              ibfac = grid%ibfac(i)

              ! ... primative variables
              GAMMA = mean%gv(l0,1)
              RHO = mean%cv(l0,1)
              SPVOL = 1.0_rfreal / RHO
              UX  = SPVOL * mean%cv(l0,2); UY = 0.0_rfreal; UZ = 0.0_rfreal;
              if (ND >= 2) UY = SPVOL * mean%cv(l0,3)
              if (ND == 3) UZ = SPVOL * mean%cv(l0,4)
              PRESSURE = mean%dv(l0,1)
              SPD_SND = sqrt(GAMMA * PRESSURE * SPVOL)
              SPD_SND_INV = 1.0_rfreal / SPD_SND
              KE = 0.5_rfreal * (UX**2 + UY**2 + UZ**2)
              ENTHALPY = KE + SPD_SND**2 / (GAMMA - 1.0_rfreal)
              MACH2 = (2.0_rfreal * KE) * SPD_SND_INV**2

              ! ... construct the normalized metrics
              XI_X = grid%MT1(l0,region%global%t2Map(normDir,1)); XI_Y = 0.0_rfreal; XI_Z = 0.0_rfreal
              if (ND >= 2) XI_Y = grid%MT1(l0,region%global%t2Map(normDir,2))
              if (ND == 3) XI_Z = grid%MT1(l0,region%global%t2Map(normDir,3))

              ! ... multiply by the Jacobian
              XI_X = XI_X * grid%JAC(l0)
              XI_Y = XI_Y * grid%JAC(l0)
              XI_Z = XI_Z * grid%JAC(l0)
              denom = 1.0_rfreal / sqrt(XI_X**2+XI_Y**2+XI_Z**2)
              XI_X_TILDE = XI_X * denom
              XI_Y_TILDE = XI_Y * denom
              XI_Z_TILDE = XI_Z * denom
              bndry_h = denom

              ! ... wall-normal velocity
              V_DOT_XI = UX * XI_X_TILDE + UY * XI_Y_TILDE + UZ * XI_Z_TILDE

              if ( patch%bcType == SAT_FAR_FIELD ) then

                ! ... wall-normal vector
                norm_vec(1:MAX_ND) = (/ XI_X_TILDE, XI_Y_TILDE, XI_Z_TILDE /)

                ! ... compute the current wall-normal velocity
                V_wall_vec(1:ND) = V_DOT_XI * norm_vec(1:ND)

                ! ... target normal wall velocity
                V_wall_target_vec(1:ND) = dot_product(state%cvTarget(l0,2:ND+1),norm_vec(1:ND)) * norm_vec(1:ND) / RHO

                ! ... inviscid penalty parameter
                sigmaI1 = -input%SAT_sigmaI1_FF * dsgn

                ! ... pull off the boundary data 
                uB(:) = state%cv(l0,:)

                ! ... target data
                gI1(:) = state%cvTarget(l0,:)

                ! ... mean
                gI1mean(:) = mean%cv(l0,:)

                ! ... follow Magnus's algorithm (norm_vec has unit length)
                pnorm_vec(:) = norm_vec(1:ND) * sqrt(XI_X**2 + XI_Y**2 + XI_Z**2)

                ! ... use target data to make implicit work better?
                Call SAT_Form_Roe_Matrices(gI1mean, gI1mean, gamma, pnorm_vec, Tmat, Tinv, Lambda)

                ! ... save ucon, speed_sound
                ucon    = Lambda(1,1)
                spd_snd = Lambda(ND+1,ND+1) - ucon

                ! ... only pick those Lambda that are incoming
                if ( sgn == 1 ) then
                  do jj = 1, ND+2
                    Lambda(jj,jj) = max(Lambda(jj,jj),0.0_rfreal)
                  end do
                else 
                  do jj = 1, ND+2
                    Lambda(jj,jj) = min(Lambda(jj,jj),0.0_rfreal)
                  end do
                end if

                ! ... modify Lambda if subsonic outflow
                ! ... left boundary
                if (sgn == 1 .and. ucon < 0.0_rfreal .and. ucon + spd_snd > 0.0_rfreal) then
                  Lambda(ND+2,ND+1) = Lambda(ND+2,ND+1) - ucon - spd_snd
                  ! right boundary
                else if (sgn == -1 .and. ucon > 0.0_rfreal .and. ucon - spd_snd < 0.0_rfreal) then
                  Lambda(ND+1,ND+2) = Lambda(ND+1,ND+2) - ucon + spd_snd
                end if

                ! ... multiply Lambda' into X
                matX = MATMUL(Lambda,Tinv)

                ! ... compute the characteristic matrix
                Aprime = MATMUL(Tmat,matX)

                ! ... subtract off the target
                ub(1:ND+2) = ub(1:ND+2) - gI1(1:ND+2)

                ! ... compute the characteristic penalty vector
                gI2(1:ND+2) = MATMUL(Aprime,uB)

                ! ... compute penalty (Bndry_h is included in Lambda already)
                do jj = 1, ND+2
                  penalty(jj) = sigmaI1 * grid%SBP_invPdiag_block1(1) * gI2(jj)
                end do

                ! ... add the rhs
                do jj = 1, ND+2
                  state%rhs(l0,jj) = state%rhs(l0,jj) + ibfac * penalty(jj)
                end do

                ! ... store Aprime
                if (input%timeScheme == BDF_GMRES_IMPLICIT) then

                  ! ... intermediate values
                  gamm1i  = 1.0_8 / (gamma - 1.0_8)
                  T       = state%dv(lp,2)
                  RHO_P   = gamma * gamm1i / T
                  RHO_T   = -PRESSURE / T * RHO_P

                  ! ... construct BI1 = Gamma_e
                  If (ND == 2) then
                    BI1(1,1) = RHO_P
                    BI1(1,2) = 0.0_8
                    BI1(1,3) = 0.0_8
                    BI1(1,4) = RHO_T
                    BI1(2,1) = UX * RHO_P
                    BI1(2,2) = RHO
                    BI1(2,3) = 0.0_8
                    BI1(2,4) = UX * RHO_T
                    BI1(3,1) = UY * RHO_P
                    BI1(3,2) = 0.0_8
                    BI1(3,3) = RHO
                    BI1(3,4) = UY * RHO_T
                    BI1(4,1) = gamm1i + KE * RHO_P
                    BI1(4,2) = RHO * UX
                    BI1(4,3) = RHO * UY
                    BI1(4,4) = KE * RHO_T
                  End if

                  Aprime = MATMUL(Aprime,BI1)

                  do jj = 1, ND+2
                    do kk = 1, ND+2  
                      patch%implicit_sat_mat(lp,jj,kk) = patch%implicit_sat_mat(lp,jj,kk) + ibfac * sigmaI1 * grid%SBP_invPdiag_block1(1) * Aprime(jj,kk)
                    end do
                  end do
                end if

                if ( state%REinv > 0.0_rfreal ) then

                  ! ... fill vector for viscous flux Jacobian diagonal entries
                  ! ... MU and LAMBDAvisc already multiplied by REinv
                  MU = 0.0_8
                  LAMBDAvisc = 0.0_8
                  ViscFluxJac(:) = 0.0_rfreal
                  ViscFluxJac(2) = XI_X * XI_X * (2*MU + LAMBDAvisc)
                  ViscFluxJac(ND+2) = XI_X * XI_X

                  if (ND >= 2) then
                    ViscFluxJac(2) = ViscFluxJac(2) + MU * (XI_Y * XI_Y)
                    ViscFluxJac(3) = XI_Y * XI_Y * (2*MU + LAMBDAvisc) + MU * (XI_X * XI_X)
                    ViscFluxJac(ND+2) = ViscFluxJac(ND+2) + XI_Y * XI_Y
                  end if

                  if (ND == 3)  then
                    ViscFluxJac(4) = 0.0_rfreal
                    ViscFluxJac(2) = ViscFluxJac(2) + MU * (XI_Z * XI_Z)
                    ViscFluxJac(3) = ViscFluxJac(3) + MU * (XI_Z * XI_Z)
                    ViscFluxJac(4) = XI_Z * XI_Z * (2*MU + LAMBDAvisc) + MU * (XI_X * XI_X + XI_Y * XI_Y)
                    ViscFluxJac(ND+2) = ViscFluxJac(ND+2) + XI_Z * XI_Z
                  end if

                  ! ... multiply r_(5,5) by mu/Re/Pr
                  ViscFluxJac(ND+2) = ViscFluxJac(ND+2) * MU * state%REinv * state%PRinv

                  ! ... factor
                  sigmaI2 = dsgn * input%SAT_sigmaI2_FF * grid%SBP_invPdiag_block1(1) / bndry_h ! * grid%JAC(l0)

                  ! ... target state
                  do jj = 1, ND+2
                    gI2(jj) = 0.0_rfreal
                  end do

                  ! ... boundary data (already includes 1/Re factor)
                  uB(:) = patch%ViscousFlux(:,lp,1) * XI_X_TILDE
                  if (ND >= 2) uB(:) = uB(:) + patch%ViscousFlux(:,lp,2) * XI_Y_TILDE
                  if (ND == 3) uB(:) = uB(:) + patch%ViscousFlux(:,lp,3) * XI_Z_TILDE

                  ! penalty term
                  penalty(:) = sigmaI2 * (uB(:) - gI2(:))

                  ! ... add to RHS
                  do jj = 1, ND+2
                    state%rhs(l0,jj) = state%rhs(l0,jj) + ibfac * penalty(jj)
                  end do
                  !                  if (i == 100 .and. j == 5) then
                  !                      print*,'jj,i,j,x,y,mat_old,before',4,i,j,grid%XYZ(l0,1),grid%XYZ(l0,2),patch%implicit_sat_mat_old(lp,4,4)
                  !                   endif

                  if (input%timeScheme == BDF_GMRES_IMPLICIT) then
                    do jj = 1, ND+2
                      patch%implicit_sat_mat_old(lp,jj,jj) = patch%implicit_sat_mat_old(lp,jj,jj) + ibfac * sigmaI2 * ViscFluxJac(jj) 
                    end do
                  end if
                  !                   if (i == 100 .and. j == 5) then
                  !                      print*,'jj,i,j,x,y,mat_old',4,i,j,grid%XYZ(l0,1),grid%XYZ(l0,2),patch%implicit_sat_mat_old(lp,4,4),state%REinv
                  !                   endif


                end if

              end if

              if ( patch%bcType == SAT_BLOCK_INTERFACE ) then

                ! ... wall-normal vector
                norm_vec(1:MAX_ND) = (/ XI_X_TILDE, XI_Y_TILDE, XI_Z_TILDE /)

                ! ... inviscid penalty parameter
                sigmaI1 = 1.0_rfreal ! -input%SAT_sigmaI1_BI * dsgn

                ! ... pull off the boundary data 
                uB(:) = state%cv(l0,:)

                ! ... target data
                gI1(:) = patch%cv_out(lp,:)

                ! ... mean
                gI1mean(:) = mean%cv(l0,:)

                ! ... follow Magnus's algorithm (norm_vec has unit length)
                pnorm_vec(:) = norm_vec(1:ND) * sqrt(XI_X**2 + XI_Y**2 + XI_Z**2)
                Call SAT_Form_Roe_Matrices(gI1mean, gI1mean, gamma, pnorm_vec, Tmat, Tinv, Lambda)

                ! ... minimal dissipation if sigma_scale = 1
                ! ... complete upwinding if sigma_scale = 2
                sigma_scale = 2.0
                do jj = 1, ND+2
                  sigR(jj) = 0.5_rfreal * Lambda(jj,jj) * sigma_scale
                  sigL(jj) = sigR(jj) - Lambda(jj,jj)
                end do

                ! ... only pick those Lambda that are incoming
                Lambda_in(:,:)  = 0.0_rfreal
                Lambda_out(:,:) = 0.0_rfreal
                if ( sgn == 1 ) then
                  do jj = 1, ND+2
                    if (Lambda(jj,jj) > 0.0_rfreal) then
                      Lambda_in(jj,jj)  = -sigR(jj)
                    else
                      Lambda_out(jj,jj) = sigL(jj) 
                    end if
                  end do
                else 
                  do jj = 1, ND+2
                    if (Lambda(jj,jj) < 0.0_rfreal) then
                      Lambda_in(jj,jj)  = sigR(jj)
                    else
                      Lambda_out(jj,jj) = -sigL(jj) 
                    end if
                  end do
                end if

                ! ... scalings across interface
                ! ... assumes same FD scheme on both sides
                p_in  = 1.0_rfreal / grid%SBP_invPdiag_block1(1);
                p_out = 1.0_rfreal / grid%SBP_invPdiag_block1(1);

                if ( sgn == 1 ) then
                  sig_v = 1.0_rfreal - p_in / (p_in + p_out);
                else
                  sig_v = -p_out / (p_in + p_out);
                end if

                ! ... compute extra contribution to inviscid term
                if (state%REinv > 0.0_rfreal) then

                  sigma_inter = - 0.25_rfreal / (p_in + p_out) * state%REinv

                  ! ... for block interface, add sigma_interface to Lambda
                  do jj = 1, ND+2
                    Lambda_in(jj,jj)  = Lambda_in(jj,jj)  + sigma_inter * (XI_X**2 + XI_Y**2 + XI_Z**2)
                    Lambda_out(jj,jj) = Lambda_out(jj,jj) + sigma_inter * (XI_X**2 + XI_Y**2 + XI_Z**2)
                  end do

                end if


                ! ... incoming characteristics

                ! ... multiply Lambda' into X
                matX = MATMUL(Lambda_in,Tinv)

                ! ... compute the characteristic matrix
                Aprime = MATMUL(Tmat,matX)

                ! ... subtract off the target
                tmp(1:ND+2) = ub(1:ND+2) - gI1(1:ND+2)

                ! ... compute the characteristic penalty vector
                tmp2(1:ND+2) = MATMUL(Aprime,tmp(1:ND+2))

                ! ... compute penalty (Bndry_h is included in Lambda already)
                penalty(1:ND+2) = sigmaI1 * grid%SBP_invPdiag_block1(1) * tmp2(1:ND+2)


                ! ... outgoing characteristcs

                ! ... multiply Lambda' into X
                matX = MATMUL(Lambda_out,Tinv)

                ! ... compute the characteristic matrix
                Aprime = MATMUL(Tmat,matX)

                ! ... subtract off the target
                tmp(1:ND+2) = ub(1:ND+2) - gI1(1:ND+2)

                ! ... compute the characteristic penalty vector
                tmp2(1:ND+2) = MATMUL(Aprime,tmp(1:ND+2))

                ! ... compute penalty (Bndry_h is included in Lambda already)
                penalty(1:ND+2) = penalty(1:ND+2) + sigmaI1 * grid%SBP_invPdiag_block1(1) * tmp2(1:ND+2)

                ! ... add the rhs
                state%rhs(l0,:) = state%rhs(l0,:) + penalty(:)

                if ( state%REinv > 0.0_rfreal ) then

                  ! ... factor
                  sigmaI2 = dsgn * grid%SBP_invPdiag_block1(1) !/ bndry_h ! * grid%JAC(l0)

                  ! ... target state
                  gI2(:) = patch%ViscousFlux_out(:,lp,1) * XI_X_TILDE
                  if (ND >= 2) gI2(:) = gI2(:) + patch%ViscousFlux_out(:,lp,2) * XI_Y_TILDE
                  if (ND == 3) gI2(:) = gI2(:) + patch%ViscousFlux_out(:,lp,3) * XI_Z_TILDE

                  ! ... boundary data (already includes 1/Re factor)
                  uB(:) = patch%ViscousFlux(:,lp,1) * XI_X_TILDE
                  if (ND >= 2) uB(:) = uB(:) + patch%ViscousFlux(:,lp,2) * XI_Y_TILDE
                  if (ND == 3) uB(:) = uB(:) + patch%ViscousFlux(:,lp,3) * XI_Z_TILDE

                  ! if (normDir == 1 .and. sgn == -1) print *, uB(2), XI_X_TILDE, XI_Y_TILDE

                  ! penalty term
                  penalty(:) = penalty(:) + sigmaI2 * (uB(:) - gI2(:))

                  ! ... add to RHS
                  state%rhs(l0,:) = state%rhs(l0,:) + ibfac * penalty(:)

                end if

              end if


              ! ... 
              ! ... SAT boundary conditions as given in 
              ! ... Svard & Nordstrom, JCP, Vol. 227, 2008
              if ( patch%bcType == SAT_SLIP_ADIABATIC_WALL .OR. &
                   patch%bcType == SAT_NOSLIP_ISOTHERMAL_WALL ) then

                ! ... wall-normal vector
                norm_vec(1:MAX_ND) = (/ XI_X_TILDE, XI_Y_TILDE, XI_Z_TILDE /)

                ! ... compute the current wall-normal velocity
                V_wall_vec(1:ND) = V_DOT_XI * norm_vec(1:ND)

                ! ... target normal wall velocity
                V_wall_target_vec(1:ND) = dot_product(grid%XYZ_TAU(l0,1:ND),norm_vec(1:ND)) * norm_vec(1:ND)

                ! ... penalty parameter
                sigmaI1 = -input%SAT_sigmaI1 * dsgn

                ! ... pull off the boundary data 
                uB(:) = state%cv(l0,:)

                ! ... mean
                gI1mean(:) = mean%cv(l0,:)

                ! ... target data
                RHO_TARGET = state%cv(l0,1)
                UX_TARGET  = UX - (V_wall_vec(1)-V_wall_target_vec(1))
                if (ND >= 2) then
                  UY_TARGET  = UY - (V_wall_vec(2)-V_wall_target_vec(2))
                end if
                if (ND == 3) then
                  UZ_TARGET  = UZ - (V_wall_vec(3)-V_wall_target_vec(3))
                end if
                PRESSURE_TARGET = PRESSURE

                ! ... compute the target data for the inviscid conditions
                gI1(1) = RHO_TARGET
                gI1(2) = RHO_TARGET * UX_TARGET
                gI1(ND+2) = PRESSURE_TARGET / (GAMMA - 1.0_rfreal) & 
                     + 0.5_rfreal * RHO_TARGET * UX_TARGET**2
                If (ND >= 2) Then
                  gI1(3) = RHO_TARGET * UY_TARGET
                  gI1(ND+2) = gI1(ND+2) + 0.5_rfreal * RHO_TARGET * UY_TARGET**2
                End If
                If (ND == 3) Then
                  gI1(4) = RHO_TARGET * UZ_TARGET
                  gI1(ND+2) = gI1(ND+2) + 0.5_rfreal * RHO_TARGET * UZ_TARGET**2
                End If

                ! ... follow Magnus's algorithm (norm_vec has unit length)
                pnorm_vec(:) = norm_vec(1:ND) * sqrt(XI_X**2 + XI_Y**2 + XI_Z**2)

                ! ... use target state to define matrix for better implicit performance?
                Call SAT_Form_Roe_Matrices(gI1mean, gI1mean, gamma, pnorm_vec, Tmat, Tinv, Lambda)

                SpecRad = 0.0_rfreal
                ! ... only pick those Lambda that are incoming
                ! ... also find spectral radius
                if ( sgn == 1 ) then
                  do jj = 1, ND+2
                    Lambda(jj,jj) = max(Lambda(jj,jj),0.0_rfreal)
                    SpecRad = max(abs(Lambda(jj,jj)),abs(SpecRad))
                  end do
                else 
                  do jj = 1, ND+2
                    Lambda(jj,jj) = min(Lambda(jj,jj),0.0_rfreal)
                    SpecRad = max(abs(Lambda(jj,jj)),abs(SpecRad))
                  end do
                end if

                ! ... multiply Lambda' into X
                matX = MATMUL(Lambda,Tinv)

                ! ... compute the characteristic matrix
                Aprime = MATMUL(Tmat,matX)

                ! ... subtract off the target
                ub(1:ND+2) = ub(1:ND+2) - gI1(1:ND+2)

                ! ... compute the characteristic penalty vector
                gI2(1:ND+2) = MATMUL(Aprime,uB)

                ! ... compute penalty (Bndry_h is included in Lambda already)
                penalty(:) = sigmaI1 * grid%SBP_invPdiag_block1(1) * gI2(:)

                ! ... add the rhs
                do jj = 1, ND+2
                  state%rhs(l0,jj) = state%rhs(l0,jj) + ibfac * penalty(jj)
                end do

                ! ... find spectral radius of Aprime
                !                SpecRad = Abs(SpecRad)

                ! ... store Aprime if implicit
                if ( input%timeScheme == BDF_GMRES_IMPLICIT) then

                  ! ... intermediate values
                  gamm1i  = 1.0_8 / (gamma - 1.0_8)
                  T       = state%dv(lp,2)
                  RHO_P   = gamma * gamm1i / T
                  RHO_T   = -PRESSURE / T * RHO_P

                  ! ... construct BI1
                  If (ND == 2) then
                    BI1(1,1:ND+2) = 0.0_8
                    BI1(2,1) = RHO_P * (V_DOT_XI) * XI_X_TILDE
                    BI1(2,2) = RHO_TARGET * XI_X_TILDE * XI_X_TILDE
                    BI1(2,3) = RHO_TARGET * XI_Y_TILDE * XI_X_TILDE
                    BI1(2,4) = RHO_T * (V_DOT_XI) * XI_X_TILDE
                    BI1(3,1) = RHO_P * (V_DOT_XI) * XI_Y_TILDE
                    BI1(3,2) = RHO_TARGET * XI_X_TILDE * XI_Y_TILDE
                    BI1(3,3) = RHO_TARGET * XI_Y_TILDE * XI_Y_TILDE
                    BI1(3,4) = RHO_T * (V_DOT_XI) * XI_Y_TILDE
                    BI1(4,1) = 0.5_8 * RHO_P * (V_DOT_XI) * (V_DOT_XI)
                    BI1(4,2) = V_DOT_XI * XI_X_TILDE
                    BI1(4,3) = V_DOT_XI * XI_Y_TILDE
                    BI1(4,4) = 0.5_8 * RHO_T * (V_DOT_XI) * (V_DOT_XI)
                  End if

                  ! ... multiply A and BI1
                  Aprime = MATMUL(Aprime,BI1)

                  do jj = 1, ND+2
                    do kk = 1, ND+2
                      patch%implicit_sat_mat(lp,jj,kk) = patch%implicit_sat_mat(lp,jj,kk) + ibfac * sigmaI1 * grid%SBP_invPdiag_block1(1) * Aprime(jj,kk)
                    end do
                  end do
                end if

                if ( patch%bcType == SAT_NOSLIP_ISOTHERMAL_WALL ) then

                  ! ... boundary data
                  uB(:) = state%cv(l0,:)

                  ! ... wall temperature
                  T_wall = 1.0_rfreal / ( GAMMA - 1.0_rfreal )!*400.0_rfreal/input%TempRef

                  ! ... target state
                  gI2(1)      = RHO_TARGET
                  gI2(2:ND+1) = grid%XYZ_TAU(l0,1:ND) * RHO_TARGET
                  gI2(ND+2)   = RHO_TARGET * T_wall / GAMMA + 0.5_rfreal * SUM(gI2(2:ND+1)**2) / RHO_TARGET

                  ! ... penalty parameter
                  sigmaI2 = -(input%SAT_sigmaI2 / 4.0_rfreal) * (grid%SBP_invPdiag_block1(1)/bndry_h)**2 * state%tv(l0,1) / RHO_TARGET * MAX(GAMMA / state%Pr, 5.0_rfreal / 3.0_rfreal)
                  sigmaI2 = sigmaI2 * state%REinv

                  ! ... compute penalty
                  penalty(:) = sigmaI2 * (uB(1:ND+2) - gI2(1:ND+2))

                  ! ... add to the rhs
                  if ( input%timeScheme == IMEX_RK4 ) then

                    patch%sat_fac(lp) = ibfac * sigmaI2 
                    do jj = 1, ND+2
                      state%rhs(l0,jj) = state%rhs(l0,jj) - ibfac * sigmaI2 * gI2(jj)
                    end do

                  else if ( input%timeScheme == BDF_GMRES_IMPLICIT ) then

                    ! ... intermediate values
                    gamm1i  = 1.0_8 / (gamma - 1.0_8)
                    T       = state%dv(lp,2)
                    RHO_P   = gamma * gamm1i / T
                    RHO_T   = -PRESSURE / T * RHO_P
                    KE      = 0.5_8 * (UX * UX + UY * UY)

                    ! ... construct BI1
                    If (ND == 2) then
                      BI1(1,1:ND+2) = 0.0_8
                      BI1(2,1) = UX * RHO_P
                      BI1(2,2) = RHO_TARGET
                      BI1(2,3) = 0.0_8
                      BI1(2,4) = UX * RHO_T
                      BI1(3,1) = UY * RHO_P
                      BI1(3,2) = 0.0_8
                      BI1(3,3) = RHO_TARGET
                      BI1(3,4) = UY * RHO_T
                      BI1(4,1) = gamm1i + RHO_P * (KE - T_wall * gamm1i/GAMMA)
                      BI1(4,2) = RHO_TARGET * UX
                      BI1(4,3) = RHO_TARGET * UY
                      BI1(4,4) = RHO_T * (KE - T_wall * gamm1i/GAMMA)
                    End if

                    do jj = 1, ND+2
                      state%rhs(l0,jj) = state%rhs(l0,jj) + ibfac * penalty(jj)
                      ! patch%implicit_sat_mat(lp,jj,jj) = patch%implicit_sat_mat(lp,jj,jj) + ibfac * sigmaI2
                      do kk = 1, ND+2
                        patch%implicit_sat_mat(lp,jj,kk) = patch%implicit_sat_mat(lp,jj,kk) + ibfac * sigmaI2 * BI1(jj,kk)
                      end do
                    end do

                  else

                    do jj = 1, ND+2
                      state%rhs(l0,jj) = state%rhs(l0,jj) + ibfac * penalty(jj)
                    end do

                  end if

                end if

              end if
            end do
          end do
        end do

      else if (patch%bcType == SPONGE) then

        Do k = patch%is(3), patch%ie(3)
          Do j = patch%is(2), patch%ie(2)
            Do i = patch%is(1), patch%ie(1)

              l0 = (k- grid%is(3))* N(1)* N(2) + (j- grid%is(2))* N(1) +  i- grid%is(1)+1
              lp = (k-patch%is(3))*Np(1)*Np(2) + (j-patch%is(2))*Np(1) + (i-patch%is(1)+1)

              ! ... distance from boundary to inner sponge start
              rel_pos(1:grid%ND) = patch%sponge_xs(lp,1:grid%ND) - patch%sponge_xe(lp,1:grid%ND)
              del = sqrt(dot_product(rel_pos(1:grid%ND),rel_pos(1:grid%ND)))

              ! ... distance from inner sponge start to point
              rel_pos(1:grid%ND) = patch%sponge_xs(lp,1:grid%ND) - grid%XYZ(l0,1:grid%ND)
              eta = sqrt(dot_product(rel_pos(1:grid%ND),rel_pos(1:grid%ND))) / del

              sponge_amp = 0.0_rfreal
              if (eta .le. 1.0_rfreal) sponge_amp = (input%sponge_amp) * eta**(input%sponge_pow)

              if (grid%iblank(l0) /= 0) then
                state%rhs(l0,:) = state%rhs(l0,:) - sponge_amp * (state%cv(l0,:) - state%cvTarget(l0,:))
              else
                state%rhs(l0,:) = 0.0_rfreal
              end if

!!$              !if ( input%timeScheme == BDF_GMRES_IMPLICIT ) then
!!$
!!$                ! ... add to LHS
!!$                do jj = 1, grid%ND+2
!!$                  patch%implicit_sat_mat(lp,jj,jj) = patch%implicit_sat_mat(lp,jj,jj) + sponge_amp
!!$                end do
!!$
!!$              end if

            end do
          end do
        end do

      end if

    end do

    deallocate(SC,Pinv,Pfor)
    deallocate(cons_inviscid_flux,L_vector,new_normal_flux, pnorm_vec)
    deallocate(uB, gI1, gI2, Aprime, Lambda, matX, matXTrans, penalty)
    deallocate(Lambda_in, Lambda_out,ViscFluxJac,BI1, gI1mean)


    return

  end subroutine LinNS_BC

  subroutine LinNS_Update_Target(region, ng)

    USE ModGlobal
    USE ModDataStruct
    USE ModDeriv
    USE ModMPI

    implicit none

    TYPE (t_region), POINTER :: region
    INTEGER :: ng
    INTEGER :: flags

    ! ... local variables
    type(t_grid), pointer :: grid
    type(t_mixt), pointer :: state
    type(t_mixt_input), pointer :: input
    type(t_patch), pointer :: patch
    integer :: i, j, k, l0, lp, npatch, ND, N(MAX_ND), Np(MAX_ND), jj, l, mm, nn, ii
    real(rfreal) :: RHO, UX, UY, UZ, GAMMA, PRESSURE, TEMPERATURE
    real(rfreal) :: BASE_RHO, BASE_UX, BASE_UY, BASE_UZ, BASE_PRESSURE, BASE_TEMPERATURE, SPD_SND
    real(rfreal) :: epsilon, omega, sincos_arg, to_rad, angle, Heaviside, heaviside_arg
    real(rfreal) :: phi, theta, time, nhat(MAX_ND), x0(MAX_ND), nhat_dot_x, wt
    complex(kind=rfreal) :: eye, e_fac1, e_fac2, e_fac3, e_fac4, amp

    ! ... simplicity
    state => region%state(ng)
    grid  => region%grid(ng)
    input => grid%input
    ND    = input%ND
    to_rad = TWOPI / 360.0_rfreal
    eye   = (0.0_8, 1.0_8)

    ! ... grid sizes
    N(:) = 1
    Np(:) = 1
    Do j = 1, grid%ND
      N(j) = grid%GlobalSize(j)
      Np(j) = grid%ie(j)-grid%is(j)+1
    End Do

    ! ... by default, set the current target to the 'saved' version
    Do k = 1, size(state%cv,2)
      Do l0 = 1, size(state%cv,1)
        state%cvTarget(l0,k) = state%cvTargetOld(l0,k)
      End Do
    End Do

    ! ... INSTABILITY WAVE EXCITATION
    If ( input%bcic_eigenfunction == TRUE ) then

      ! ... add to target
      Do k = grid%is(3), grid%ie(3)
        Do j = grid%is(2), grid%ie(2)
          Do i = grid%is(1), grid%ie(1)

            If (i > input%bcic_eigenfunction_imax) CYCLE

            l0 = (k- grid%is(3))* Np(1)* Np(2) + (j- grid%is(2))* Np(1) + (i- grid%is(1)+1) 

            ! ... weighting function
            wt = MIN(state%time(l0) / 200.0_8, 1.0_8)

            Do mm = 1, input%m_mode
              Do nn = 1, input%n_mode

                e_fac1 = cdexp(eye * (input%alpha_pert(1,mm,nn) * grid%XYZ(l0,1) - &
                                      input%freq_pert(mm,nn) * state%time(l0)) )

                e_fac2 = cdexp(eye * (input%alpha_pert(1,mm,nn) * grid%XYZ(l0,1) - &
                                      input%freq_pert(mm,nn) * state%time(l0)) )

                ! ... if non-zero spanwise
                e_fac3 = e_fac1 * wt
                e_fac4 = e_fac2 * wt

                ! ... density
                state%cvTarget(l0,1) = state%cvTarget(l0,1) + input%amp_pert(mm,nn) * &
                                       Dble( e_fac3 * input%qp_hat(1,j,4,2*mm-1,nn) + & 
                                             e_fac4 * input%qp_hat(1,j,4,2*mm  ,nn) )

                ! ... rho u_x
                state%cvTarget(l0,2) = state%cvTarget(l0,2) + input%amp_pert(mm,nn) * &
                                       Dble( e_fac3 * input%qp_hat(1,j,1,2*mm-1,nn) + & 
                                             e_fac4 * input%qp_hat(1,j,1,2*mm  ,nn) )

                ! ... rho u_y
                state%cvTarget(l0,3) = state%cvTarget(l0,3) + input%amp_pert(mm,nn) * &
                                       Dble( e_fac3 * input%qp_hat(1,j,2,2*mm-1,nn) + & 
                                             e_fac4 * input%qp_hat(1,j,2,2*mm  ,nn) )

                ! ... rho E
                state%cvTarget(l0,4) = state%cvTarget(l0,4) + input%amp_pert(mm,nn) * &
                                       Dble( e_fac3 * input%qp_hat(1,j,5,2*mm-1,nn) + & 
                                             e_fac4 * input%qp_hat(1,j,5,2*mm  ,nn) )

              End Do
            End Do
          End Do
        End Do
      End Do

    End If

    ! ... PSE INSTABILITY WAVE EXCITATION
    If ( input%bcic_eigenfunction == PSE ) then

      ! ... add to target
      Do k = grid%is(3), grid%ie(3)
        Do j = grid%is(2), grid%ie(2)
          Do i = grid%is(1), grid%ie(1)

            If (i > input%bcic_eigenfunction_imax) CYCLE

            l0 = (k- grid%is(3))* Np(1)* Np(2) + (j- grid%is(2))* Np(1) + (i- grid%is(1)+1) 

            ! ... weighting function
            wt = MIN(state%time(l0) / 200.0_8, 1.0_8)

            Do mm = 1, input%m_mode
              Do nn = 1, input%n_mode

                ! ... time phase
                e_fac1 = cdexp(eye * (-input%freq_pert(mm,nn) * state%time(l0)) )

                ! ... if non-zero spanwise
                e_fac3 = e_fac1 * wt

                ! ... density
                state%cvTarget(l0,1) = state%cvTarget(l0,1) + input%amp_pert(mm,nn) * &
                                       Dble( e_fac3 * input%qp_hat(i,j,1,mm,nn) )

                ! ... rho u_x
                state%cvTarget(l0,2) = state%cvTarget(l0,2) + input%amp_pert(mm,nn) * &
                                       Dble( e_fac3 * input%qp_hat(i,j,2,mm,nn) )

                ! ... rho u_y
                state%cvTarget(l0,3) = state%cvTarget(l0,3) + input%amp_pert(mm,nn) * &
                                       Dble( e_fac3 * input%qp_hat(i,j,3,mm,nn) )

                ! ... rho E
                state%cvTarget(l0,4) = state%cvTarget(l0,4) + input%amp_pert(mm,nn) * &
                                       Dble( e_fac3 * input%qp_hat(i,j,5,mm,nn) )

              End Do
            End Do
          End Do
        End Do
      End Do

    End If

    ! ... PLANE WAVE EXCITATION --- update target
    If ( input%bcic_planewave == TRUE ) then

      ! ... parameters
      epsilon = 10.0_rfreal**(input%bcic_amplitude/20.0_rfreal - 9.701_rfreal)
      omega   = input%bcic_frequency * TWOPI * input%LengRef / input%SndSpdRef
      theta   = input%bcic_angle_theta * to_rad
      phi     = input%bcic_angle_phi   * to_rad

      ! ... wavenumber orientation
      nhat(1)  = sin(phi) * cos(theta)
      nhat(2)  = sin(phi) * sin(theta)
      nhat(3)  = cos(phi)

      ! ... point defining location of t-(nhat . (x-x0)/c0) = 0 at time = 0
      x0(1) = 0.0_rfreal
      x0(2) = 0.0_rfreal
      x0(3) = 1.0_rfreal

      ! ... update the interior target data
      Do l0 = 1, size(state%cv,1)

        ! ... ratio of specific heats
        GAMMA = state%gv(l0,1)

        ! ... time
        time  = state%time(l0)

        ! ... base values
        BASE_RHO = state%cvTargetOld(l0,1)
        BASE_UX  = state%cvTargetOld(l0,2) / BASE_RHO; BASE_UY = 0.0_rfreal; BASE_UZ = 0.0_rfreal;
        if (ND >= 2) BASE_UY  = state%cvTargetOld(l0,3) / BASE_RHO
        if (ND >= 3) BASE_UZ  = state%cvTargetOld(l0,4) / BASE_RHO
        BASE_PRESSURE = (GAMMA-1.0_rfreal) * (state%cvTargetOld(l0,ND+2) - 0.5_rfreal * (BASE_UX**2+BASE_UY**2+BASE_UZ**2) * BASE_RHO)
        BASE_TEMPERATURE = GAMMA * BASE_PRESSURE / (GAMMA-1.0_rfreal) / BASE_RHO
        SPD_SND = Sqrt(GAMMA * BASE_PRESSURE / BASE_RHO)

        ! ... wave direction dotted into position
        nhat_dot_x = 0.0_rfreal
        do j = 1, grid%ND
          nhat_dot_x = nhat_dot_x + nhat(j) * grid%XYZ(l0,j)
        end do
      
        ! ... limit plane wave by Heaviside
        heaviside_arg = time
        do j = 1, grid%ND
          heaviside_arg = heaviside_arg - nhat(j) * (grid%XYZ(l0,j) - x0(j)) / SPD_SND
        end do
        Heaviside = 0.0_rfreal
        if (heaviside_arg >= 0.0_rfreal) Heaviside = 1.0_rfreal

        ! ... plane wave
        sincos_arg = omega * (time - nhat_dot_x / SPD_SND)
        RHO        = epsilon * cos(sincos_arg) * Heaviside
        PRESSURE   = RHO * SPD_SND**2
        UX         = nhat(1) * PRESSURE / (BASE_RHO * SPD_SND)
        UY         = nhat(2) * PRESSURE / (BASE_RHO * SPD_SND)
        UZ         = nhat(3) * PRESSURE / (BASE_RHO * SPD_SND)

        ! ... add in the wave
        state%cvTarget(l0,1) = BASE_RHO + RHO
        state%cvTarget(l0,2) = state%cvTarget(l0,1) * ( UX + BASE_UX )
        if (ND >= 2) state%cvTarget(l0,3) = state%cvTarget(l0,1) * ( UY + BASE_UY )
        if (ND == 3) state%cvTarget(l0,4) = state%cvTarget(l0,1) * ( UZ + BASE_UZ )
        state%cvTarget(l0,ND+2) = (BASE_PRESSURE + PRESSURE) / (GAMMA-1.0_rfreal) + 0.5_rfreal * ((BASE_UX+UX)**2+(BASE_UY+UY)**2+(BASE_UZ+UZ)**2) * state%cvTarget(l0,1)

      End Do

    End If

    ! ... update the boundary patch data
    Do npatch = 1, region%npatches

      patch => region%patch(npatch)
      grid => region%grid(patch%gridID)
      state => region%state(patch%gridID)

      N  = 1; Np = 1;
      do j = 1, grid%ND
        N(j)  = grid%ie(j)-grid%is(j)+1
        Np(j) = patch%ie(j) - patch%is(j) + 1
      end do;

      If ( (patch%bcType == NSCBC_INFLOW_VELOCITY_TEMPERATURE_FROM_SPONGE) .OR. &
           (patch%bcType == NSCBC_INFLOW_VELOCITY_TEMPERATURE) ) Then

        Do k = patch%is(3), patch%ie(3)
          Do j = patch%is(2), patch%ie(2)
            Do i = patch%is(1), patch%ie(1)

              l0 = (k- grid%is(3))* N(1)* N(2) + (j- grid%is(2))* N(1) + (i- grid%is(1)+1)
              lp = (k-patch%is(3))*Np(1)*Np(2) + (j-patch%is(2))*Np(1) + (i-patch%is(1)+1)

              ! ... ratio of specific heats
              GAMMA = state%gv(l0,1)

              ! ... time
              time  = state%time(l0)

              ! ... base values
              BASE_RHO = state%cvTargetOld(l0,1)
              BASE_UX  = state%cvTargetOld(l0,2) / BASE_RHO; BASE_UY = 0.0_rfreal; BASE_UZ = 0.0_rfreal;
              if (ND >= 2) BASE_UY  = state%cvTargetOld(l0,3) / BASE_RHO
              if (ND >= 3) BASE_UZ  = state%cvTargetOld(l0,4) / BASE_RHO
              BASE_PRESSURE = (GAMMA-1.0_rfreal) * (state%cvTargetOld(l0,ND+2) - 0.5_rfreal * (BASE_UX**2+BASE_UY**2+BASE_UZ**2)*BASE_RHO)
              BASE_TEMPERATURE = GAMMA * BASE_PRESSURE / (GAMMA-1.0_rfreal) / BASE_RHO
              SPD_SND = Sqrt(GAMMA * BASE_PRESSURE / BASE_RHO)

              ! ... wave direction dotted into position
              nhat_dot_x = 0.0_rfreal
              do jj = 1, grid%ND
                nhat_dot_x = nhat_dot_x + nhat(jj) * grid%XYZ(l0,jj)
              end do
      
              ! ... limit plane wave by Heaviside
              heaviside_arg = time
              do jj = 1, grid%ND
                heaviside_arg = heaviside_arg - nhat(jj) * (grid%XYZ(l0,jj) - x0(jj)) / SPD_SND
              end do
              Heaviside = 0.0_rfreal
              if (heaviside_arg >= 0.0_rfreal) Heaviside = 1.0_rfreal
       

              patch%dudt_inflow(lp) = 0.0_rfreal
              If (ND >= 2) patch%dvdt_inflow(lp) = 0.0_rfreal
              If (ND == 3) patch%dwdt_inflow(lp) = 0.0_rfreal
              patch%dTdt_inflow(lp) = 0.0_rfreal

              If ( (input%bcic_planewave == TRUE) ) then

                ! ... plane wave
                ! ... plane wave
                sincos_arg = omega * (time - nhat_dot_x / SPD_SND)
                RHO        = epsilon * cos(sincos_arg) * Heaviside
                PRESSURE   = RHO * SPD_SND**2
                UX         = nhat(1) * PRESSURE / (RHO * SPD_SND)
                UY         = nhat(2) * PRESSURE / (RHO * SPD_SND)
                UZ         = nhat(3) * PRESSURE / (RHO * SPD_SND)

                ! ... time derivative
                patch%dudt_inflow(lp) = -omega * UX * Heaviside
                if (ND >= 2) patch%dvdt_inflow(lp) = -omega * UY * Heaviside
                if (ND == 3) patch%dwdt_inflow(lp) = -omega * UZ * Heaviside
                patch%dTdt_inflow(lp) = -omega * BASE_TEMPERATURE * ( PRESSURE / BASE_PRESSURE - RHO / BASE_RHO ) * Heaviside

              End If

              if ( patch%bcType == NSCBC_INFLOW_VELOCITY_TEMPERATURE_FROM_SPONGE ) then
                patch%u_inflow(lp) = state%cvTarget(l0,2) / state%cvTarget(l0,1)
                if (ND >= 2) patch%v_inflow(lp) = state%cvTarget(l0,3) / state%cvTarget(l0,1)
                if (ND == 3) patch%w_inflow(lp) = state%cvTarget(l0,4) / state%cvTarget(l0,1)
                PRESSURE = (GAMMA-1.0_rfreal) * (state%cvTarget(l0,ND+2) - 0.5_rfreal * state%cvTarget(l0,1) * patch%u_inflow(lp)**2)
                if (ND >= 2) PRESSURE = PRESSURE - (GAMMA-1.0_rfreal) * 0.5_rfreal * state%cvTarget(l0,1)* patch%v_inflow(lp)**2
                if (ND == 3) PRESSURE = PRESSURE - (GAMMA-1.0_rfreal) * 0.5_rfreal * state%cvTarget(l0,1)* patch%w_inflow(lp)**2
                patch%T_inflow(lp) = GAMMA * PRESSURE / (GAMMA - 1.0_rfreal) / state%cvTarget(l0,1)
              else
                patch%u_inflow(lp) = 0.1_rfreal
                if (ND >= 2) patch%v_inflow(lp) = 0.0_rfreal
                if (ND == 3) patch%w_inflow(lp) = 0.0_rfreal
                patch%T_inflow(lp) = 1.0_rfreal / (GAMMA - 1.0_rfreal)
              end if

            End Do
          End Do
        End Do

      End If
    End Do

    !           ! ... add time-dependence to 'ramp' up values
    !           t0     = 5.0_rfreal
    !           region%global%delta  = 1.0_rfreal
    !           w_of_t = 0.5_rfreal * (1.0_rfreal + tanh((state%time(l0) - t0)/region%global%delta))
    !           dwdt   = 0.5_rfreal / (region%global%delta * (cosh((state%time(l0)-t0)/region%global%delta))**2)
    !           w_of_t = 1.0_rfreal
    !           dwdt   = 0.0_rfreal

  end subroutine LinNS_Update_Target


END MODULE ModLinNavierStokesBC
