# Copyright (c) 2015, University of Illinois at Urbana-Champaign, XPACC
# License: MIT, http://opensource.org/licenses/MIT
utils_fpp_src = \
  ModBC.fpp \
  ModCompare.fpp \
  ModIEEEArithmetic.fpp \
  ModParseArguments.fpp \
  ModStdIO.fpp
