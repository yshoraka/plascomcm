! Copyright (c) 2015, University of Illinois at Urbana-Champaign, XPACC
! License: MIT, http://opensource.org/licenses/MIT
!===================================================================================================
!
! ModParseArguments: GNU-style command line argument parsing
!
! Usage:
!
!   (1) Read raw arguments into an array of strings via get_command_argument
!   (2) (Optional) Create an array of type(t_cmd_opt) and fill it with the desired option
!       specifications, e.g.:
!         Options(1) = t_cmd_opt_("verbose", "v", OPT_VALUE_TYPE_NONE)
!         Options(2) = t_cmd_opt_("path", "p", OPT_VALUE_TYPE_STRING)
!   (3) (Optional) Create an unallocated array for non-option arguments
!   (4) Call ParseArguments, passing the raw arguments and either/both of the option and non-option
!       argument arrays
!   (5) Option information will be stored in the corresponding t_cmd_opt%is_present and
!       t_cmd_opt%value fields, and non-option arguments will be stored in the allocatable array
!
!===================================================================================================

module ModParseArguments

  use ModStdIO
  implicit none

  private

  public :: CMD_ARG_LENGTH
  public :: t_cmd_opt
  public :: t_cmd_opt_
  public :: ParseArguments
  public :: OPT_VALUE_TYPE_NONE, OPT_VALUE_TYPE_INTEGER, OPT_VALUE_TYPE_REAL, OPT_VALUE_TYPE_STRING

  integer, parameter :: rk = selected_real_kind(15, 307)
  integer, parameter :: ik = selected_int_kind(9)

  integer(ik), parameter :: CMD_ARG_LENGTH = 256

  type t_cmd_opt
    character(len=CMD_ARG_LENGTH) :: long_name
    character(len=1) :: short_name
    integer(ik) :: value_type
    logical :: is_present
    character(len=CMD_ARG_LENGTH) :: value
  end type t_cmd_opt

  ! Trailing _ added for compatibility with compilers that don't support F2003 constructors
  interface t_cmd_opt_
    module procedure t_cmd_opt_Default
    module procedure t_cmd_opt_Specified
  end interface t_cmd_opt_

  integer(ik), parameter :: OPT_VALUE_TYPE_NONE = 1
  integer(ik), parameter :: OPT_VALUE_TYPE_INTEGER = 2
  integer(ik), parameter :: OPT_VALUE_TYPE_REAL = 3
  integer(ik), parameter :: OPT_VALUE_TYPE_STRING = 4

contains

  function t_cmd_opt_Default() result(Option)

    type(t_cmd_opt) :: Option

    Option%long_name = ""
    Option%short_name = ""
    Option%value_type = OPT_VALUE_TYPE_NONE
    Option%is_present = .false.
    Option%value = ""

  end function t_cmd_opt_Default

  function t_cmd_opt_Specified(LongName, ShortName, ValueType) result(Option)

    character(len=*), intent(in) :: LongName
    character(len=*), intent(in) :: ShortName
    integer(ik), intent(in) :: ValueType
    type(t_cmd_opt) :: Option

    Option%long_name = LongName
    Option%short_name = ShortName
    Option%value_type = ValueType
    Option%is_present = .false.
    Option%value = ""

  end function t_cmd_opt_Specified

  subroutine ParseArguments(RawArguments, Options, Arguments, MinArguments, MaxArguments)

    character(len=CMD_ARG_LENGTH), dimension(:), intent(in) :: RawArguments
    type(t_cmd_opt), dimension(:), intent(inout), optional :: Options
    character(len=CMD_ARG_LENGTH), dimension(:), allocatable, intent(out), optional :: Arguments
    integer(ik), intent(in), optional :: MinArguments, MaxArguments

    integer(ik) :: i, j, k, l
    character(len=CMD_ARG_LENGTH) :: RawArgument, NextRawArgument
    logical :: SkipNext
    character(len=CMD_ARG_LENGTH) :: OptionName, OptionValue
    logical :: ValidOptionValue
    character(len=CMD_ARG_LENGTH), dimension(:), allocatable :: ArgumentsTemp
    integer(ik) :: nArguments

    if (present(Arguments)) then
      allocate(ArgumentsTemp(size(RawArguments)))
      nArguments = 0
    end if

    i = 1
    do while (i <= size(RawArguments))

      RawArgument = RawArguments(i)
      if (i < size(RawArguments)) then
        NextRawArgument = RawArguments(i+1)
      else
        NextRawArgument = ""
      end if

      SkipNext = .false.

      if (RawArgument(1:2) == "--") then

        j = index(RawArgument, "=")
        j = merge(j, len_trim(RawArgument)+1, j /= 0)
        OptionName = RawArgument(3:j-1)
        OptionValue = RawArgument(j+1:)
        l = 0
        if (present(Options)) then
          do k = 1, size(Options)
            if (Options(k)%long_name == OptionName) then
              l = k
              exit
            end if
          end do
        end if
        if (l == 0) then
          write (ERROR_UNIT, '(3a)') "ERROR: Unrecognized option '", trim(OptionName), "'."
          stop 1
        end if
        if (OptionValue == "" .and. Options(l)%value_type /= OPT_VALUE_TYPE_NONE) then
          if (NextRawArgument /= "") then
            OptionValue = NextRawArgument
            SkipNext = .true.
          else
            write (ERROR_UNIT, '(3a)') "ERROR: Missing value for option '", trim(OptionName), &
              "'."
            stop 1
          end if
        end if
        ValidOptionValue = ValidateOptionValue(OptionValue, Options(l)%value_type)
        if (ValidOptionValue) then
          Options(l)%is_present = .true.
          Options(l)%value = OptionValue
        else
          write (ERROR_UNIT, '(3a)') "ERROR: Unrecognized value for option '", &
            trim(OptionName), "'."
          stop 1
        end if

      else if (RawArgument(1:1) == "-") then

        do j = 2, len_trim(RawArgument)
          OptionName = RawArgument(j:j)
          l = 0
          if (present(Options)) then
            do k = 1, size(Options)
              if (Options(k)%short_name == OptionName) then
                l = k
                exit
              end if
            end do
          end if
          if (l == 0) then
            write (ERROR_UNIT, '(3a)') "ERROR: Unrecognized option '", trim(OptionName), "'."
            stop 1
          end if
          if (Options(l)%value_type == OPT_VALUE_TYPE_NONE) then
            Options(l)%is_present = .true.
            Options(l)%value = ""
          else
            OptionValue = RawArgument(j+1:)
            if (OptionValue == "") then
              if (NextRawArgument /= "") then
                OptionValue = NextRawArgument
                SkipNext = .true.
              else
                write (ERROR_UNIT, '(3a)') "ERROR: Missing value for option '", &
                  trim(OptionName), "'."
                stop 1
              end if
            end if
            ValidOptionValue = ValidateOptionValue(OptionValue, Options(l)%value_type)
            if (ValidOptionValue) then
              Options(l)%is_present = .true.
              Options(l)%value = OptionValue
            else
              write (ERROR_UNIT, '(3a)') "ERROR: Unrecognized value for option '", &
                trim(OptionName), "'."
              stop 1
            end if
            exit
          end if
        end do

      else

        if (present(Arguments)) then
          ArgumentsTemp(nArguments+1) = RawArgument
          nArguments = nArguments + 1
        else
          write (ERROR_UNIT, '(3a)') "ERROR: Unrecognized argument '", trim(RawArgument), "'."
          stop 1
        end if

      end if

      i = merge(i+2, i+1, SkipNext)

    end do

    if (present(Arguments)) then

      if (present(MinArguments)) then
        if (nArguments < MinArguments) then
          write (ERROR_UNIT, '(a)') "ERROR: Not enough command-line arguments."
          stop 1
        end if
      end if

      if (present(MaxArguments)) then
        if (nArguments > MaxArguments) then
          write (ERROR_UNIT, '(a)') "ERROR: Too many command-line arguments."
          stop 1
        end if
      end if

      allocate(Arguments(nArguments))
      Arguments = ArgumentsTemp(:nArguments)

    end if

  end subroutine ParseArguments

  function ValidateOptionValue(OptionValue, OptionValueType) result(ValidOptionValue)

    character(len=CMD_ARG_LENGTH), intent(in) :: OptionValue
    integer(ik), intent(in) :: OptionValueType
    logical :: ValidOptionValue

    integer(ik) :: Error
    integer(ik) :: IntegerValue
    real(rk) :: RealValue

    select case (OptionValueType)
    case (OPT_VALUE_TYPE_NONE)
      ValidOptionValue = OptionValue == ""
    case (OPT_VALUE_TYPE_INTEGER)
      read (OptionValue, *, iostat=Error) IntegerValue
      ValidOptionValue = Error == 0
    case (OPT_VALUE_TYPE_REAL)
      read (OptionValue, *, iostat=Error) RealValue
      ValidOptionValue = Error == 0
    case (OPT_VALUE_TYPE_STRING)
      ValidOptionValue = OptionValue /= ""
    end select

  end function ValidateOptionValue

end module ModParseArguments
