clear all
set(0,'defaultaxesfontname','times');
set(0,'defaultaxesfontsize',20);

% air properties
Ru = 8314.5; % universal gas constant
MW = 28.97;  % molecular weight
Rs = Ru/MW;  % specific gas constant

% temperature points from NACA data
temp2 = [500:500:15000]';

% compressibility factor
Z2 = [1.000, 1.000, 1.000, 1.000, 1.003, 1.026, 1.091, 1.164, ...
     1.191, 1.217, 1.248, 1.316, 1.436, 1.605, 1.775, 1.891, 1.950, ...
     1.990, 2.020, 2.048, 2.074, 2.112, 2.176, 2.234, 2.320, 2.426, ...
     2.554, 2.700, 2.912, 3.030].';
 
% specific heat at constant volume
ZCv_R = [2.59, 2.96, 3.20, 3.33, 4.37, 7.99, 12.32, 8.64, 4.66, ...
         7.56, 13.27, 22.5, 33.5, 39.5, 33.8, 21.8, 13.08, 6.52, ...
         8.52, 12.49, 15.79, 19.82, 24.8, 30.3, 36.1, 42.3, 47.9, ...
         53.1, 57.5, 56.8].';
Cv2 = ZCv_R * Ru ./ Z2 / MW;

% specific heat at constant pressure
ZCp_R = [3.59, 3.96, 4.20, 4.33, 5.37, 9.55, 14.79, 10.68, 6.06, ...
         9.18, 15.69, 26.8, 40.8, 49.2, 42.1, 27.2, 16.52, 8.79, ...
         10.96, 15.49, 19.40, 24.2, 30.2, 37.4, 45.1, 53.9, 62.2, ...
         69.5, 76.0, 76.1].';
Cp2 = ZCp_R * Ru ./ Z2 / MW;

% data from Vargaftik
temp3 = [1400:200:6000].';
Cp3 = [1.207, 1.248, 1.286, 1.337, 1.417, 1.558, 1.803, 2.191, 2.726, ...
       3.340, 3.852, 4.024, 3.747, 3.207, 2.705, 2.411, 2.347, 2.488, ...
       2.816, 3.333, 4.049, 4.980, 6.131, 7.491].';

% ratio of specific heats
gamma2 = Cp2 ./ Cv2;

% lower temperature points
temp1 = [0:50:450]';
Z1 = ones(size(temp1));
for n = 1:length(temp1)
    [Cp1(n,1),R] = Cpfit(temp1(n),391);
    Cv1(n,1) = Cp1(n,1) - R;
    gamma1(n,1) = Cp1(n,1)/Cv1(n,1);
end

% merge them
temp = [temp1 ; temp2];
Cv = [Cv1 ; Cv2];
Cp = [Cp1 ; Cp2]; 
gamma = [gamma1 ; gamma2];
Z = [Z1 ; Z2];

% interpolate them onto a finer, uniformly spaced temperature
temp_final  = linspace(temp(1),temp(end),1000);
Cv_final    = spline(temp, Cv, temp_final);
Cp_final    = spline(temp, Cp, temp_final);
gamma_final = spline(temp, gamma, temp_final);
Z_final     = spline(temp, Z, temp_final);

% rename everything
temp  = temp_final;
Cv    = Cv_final;
Cp    = Cp_final;
gamma = gamma_final;
Z     = Z_final;

figure(1), clf
plot(temp,Cv,'k-'), hold on
plot(temp,Cp,'r--');
xlabel('Temperature [K]'); ylabel('Specific heat [J/(kg . K)]');
legend('C_v','C_p');
print -depsc -f1 Cv_Cp.eps

figure(2), clf
plot(temp,gamma,'k-'), hold on
plot(temp,Z,'r--');
xlabel('Temperature [K]');
legend('\gamma','Z');
axis([min(temp) max(temp) 0.9 4.1]);
print -depsc -f2 gamma_Z.eps

% construct internal energy
eint = cumtrapz(temp, Cv);

% construct splines
Cv_cs    = spline(temp,Cv);
Cp_cs    = spline(temp,Cp);
gamma_cs = spline(temp,gamma);
Z_cs     = spline(temp,Z);
eint_cs  = spline(temp,eint);

% invert eint spline
eint_eq  = linspace(eint(1),eint(end),length(eint));
temp_eq  = spline(eint,temp,eint_eq);
temp_cs  = spline(eint_eq,temp_eq);

% output
fid = fopen('Air_dv_spline.tbl','wb','ieee-be');
sizeof_int = 4;
sizeof_dbl = 8;

% output gas constant
fwrite(fid,sizeof_dbl,'int32');
fwrite(fid,Rs,'double');
fwrite(fid,sizeof_dbl,'int32');

% output number of break points
fwrite(fid,sizeof_int,'int32');
fwrite(fid,length(temp),'int32');
fwrite(fid,sizeof_int,'int32');

% output break points
fwrite(fid,length(temp)*sizeof_dbl,'int32');
fwrite(fid,temp,'double');
fwrite(fid,length(temp)*sizeof_dbl,'int32');

% output Cp
fwrite(fid,(length(temp)-1)*4*sizeof_dbl,'int32');
fwrite(fid,Cp_cs.coefs,'double');
fwrite(fid,(length(temp)-1)*4*sizeof_dbl,'int32');

% output Cv
fwrite(fid,(length(temp)-1)*4*sizeof_dbl,'int32');
fwrite(fid,Cv_cs.coefs,'double');
fwrite(fid,(length(temp)-1)*4*sizeof_dbl,'int32');

% output Eint
fwrite(fid,(length(temp)-1)*4*sizeof_dbl,'int32');
fwrite(fid,eint_cs.coefs,'double');
fwrite(fid,(length(temp)-1)*4*sizeof_dbl,'int32');

% output gamma
fwrite(fid,(length(temp)-1)*4*sizeof_dbl,'int32');
fwrite(fid,gamma_cs.coefs,'double');
fwrite(fid,(length(temp)-1)*4*sizeof_dbl,'int32');

% output compressibility
fwrite(fid,(length(temp)-1)*4*sizeof_dbl,'int32');
fwrite(fid,Z_cs.coefs,'double');
fwrite(fid,(length(temp)-1)*4*sizeof_dbl,'int32');

% output break points for eint reverse lookup
fwrite(fid,length(eint_eq)*sizeof_dbl,'int32');
fwrite(fid,eint_eq,'double');
fwrite(fid,length(eint_eq)*sizeof_dbl,'int32');

% output reverse lookup
fwrite(fid,(length(eint_eq)-1)*4*sizeof_dbl,'int32');
fwrite(fid,temp_cs.coefs,'double');
fwrite(fid,(length(eint_eq)-1)*4*sizeof_dbl,'int32');

% done
fclose(fid);

