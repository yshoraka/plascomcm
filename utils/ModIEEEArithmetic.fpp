!===================================================================================================
!
! ModIEEEArithmetic: Defines special operations for IEEE floating point numbers
!
!===================================================================================================

module ModIEEEArithmetic

#ifdef f2003
  use, intrinsic ieee_arithmetic, only : ieee_is_nan
#endif
  implicit none

  private

  public :: ieee_is_nan

  integer, parameter :: SP = kind(1.0)
  integer, parameter :: DP = kind(1.D0)

#ifndef f2003

  interface ieee_is_nan
    module procedure ieee_is_nan_sp
    module procedure ieee_is_nan_dp
  end interface ieee_is_nan

#endif

#ifndef f2003

contains

  pure elemental function ieee_is_nan_sp(X) result(IsNan)

    real(SP), intent(in) :: X
    logical :: IsNan

    IsNan = X /= X

  end function ieee_is_nan_sp

  pure elemental function ieee_is_nan_dp(X) result(IsNan)

    real(DP), intent(in) :: X
    logical :: IsNan

    IsNan = X /= X

  end function ieee_is_nan_dp

#endif

end module ModIEEEArithmetic
