! Copyright (c) 2015, University of Illinois at Urbana-Champaign, XPACC
! License: MIT, http://opensource.org/licenses/MIT
module ModGridUtils

  use ModGlobal
  implicit none

  private

  public :: TupleToIndex
  public :: IndexToTuple
  public :: PeriodicAdjust
!   public :: PeriodicProject
  public :: PeriodicExtend

contains

  pure function TupleToIndex(nDims, iStart, iEnd, Tuple) result(Ind)

    integer(ik), intent(in) :: nDims
    integer(ik), dimension(nDims), intent(in) :: iStart, iEnd
    integer(ik), dimension(nDims), intent(in) :: Tuple
    integer(ik) :: Ind

    integer(ik) :: i
    integer(ik), dimension(nDims) :: N
    integer(ik), dimension(nDims) :: Stride

    N = iEnd - iStart + 1
    Stride = [(product(N(:i)),i=0,nDims-1)]

    Ind = 1 + sum(Stride * (Tuple - iStart))

  end function TupleToIndex

  pure function IndexToTuple(nDims, iStart, iEnd, Ind) result(Tuple)

    integer(ik), intent(in) :: nDims
    integer(ik), dimension(nDims), intent(in) :: iStart, iEnd
    integer(ik), intent(in) :: Ind
    integer(ik), dimension(nDims) :: Tuple

    integer(ik) :: i
    integer(ik), dimension(nDims) :: N
    integer(ik), dimension(nDims) :: Stride

    N = iEnd - iStart + 1
    Stride = [(product(N(:i)),i=0,nDims-1)]

    Tuple = iStart + modulo((Ind-1)/Stride, N)

  end function IndexToTuple

  pure function PeriodicAdjust(nDims, Periodic, PeriodicStorage, iStart, iEnd, Tuple) &
    result(AdjustedTuple)

    integer(ik), intent(in) :: nDims
    integer(ik), dimension(nDims), intent(in) :: Periodic
    integer(ik), intent(in) :: PeriodicStorage
    integer(ik), dimension(nDims), intent(in) :: iStart, iEnd
    integer(ik), dimension(nDims), intent(in) :: Tuple
    integer(ik), dimension(nDims) :: AdjustedTuple

    integer(ik), dimension(nDims) :: iEndActual
    integer(ik), dimension(nDims) :: AdjustAmount
    integer(ik), dimension(nDims) :: PositiveAdjustment, NegativeAdjustment

    iEndActual = merge(iEnd, iEnd+1, Periodic == FALSE .or. PeriodicStorage /= NO_OVERLAP_PERIODIC)
    AdjustAmount = iEndActual - iStart

    PositiveAdjustment = merge(AdjustAmount, 0, Periodic /= FALSE .and. Tuple < iStart)
    NegativeAdjustment = merge(AdjustAmount, 0, Periodic /= FALSE .and. Tuple >= iEndActual)

    AdjustedTuple = Tuple + PositiveAdjustment - NegativeAdjustment

  end function PeriodicAdjust

!   pure function PeriodicProject(nDims, Periodic, PeriodicOrigin, PeriodicLength, Coords) &
!     result(ProjectedCoords)

!     integer(ik), intent(in) :: nDims
!     integer(ik), dimension(nDims), intent(in) :: Periodic
!     real(rk), dimension(nDims), intent(in) :: PeriodicOrigin
!     real(rk), dimension(nDims), intent(in) :: PeriodicLength
!     real(rk), dimension(nDims), intent(in) :: Coords
!     real(rk), dimension(nDims) :: ProjectedCoords

!     real(rk), dimension(nDims) :: PositiveAdjustment, NegativeAdjustment

!     PositiveAdjustment = merge(PeriodicLength, 0._rk, Periodic == PLANE_PERIODIC .and. &
!       Coords < PeriodicOrigin)
!     NegativeAdjustment = merge(PeriodicLength, 0._rk, Periodic == PLANE_PERIODIC .and. &
!       Coords >= PeriodicOrigin + PeriodicLength)

!     ProjectedCoords = Coords + PositiveAdjustment - NegativeAdjustment

!   end function PeriodicProject

  pure function PeriodicExtend(nDims, Periodic, PeriodicStorage, PeriodicLength, iStart, iEnd, &
    Tuple, Coords) result(ExtendedCoords)

    integer(ik), intent(in) :: nDims
    integer(ik), dimension(nDims), intent(in) :: Periodic
    integer(ik), intent(in) :: PeriodicStorage
    real(rk), dimension(nDims), intent(in) :: PeriodicLength
    integer(ik), dimension(nDims), intent(in) :: iStart, iEnd
    integer(ik), dimension(nDims), intent(in) :: Tuple
    real(rk), dimension(nDims), intent(in) :: Coords
    real(rk), dimension(nDims) :: ExtendedCoords

    integer(ik), dimension(nDims) :: iEndActual
    real(rk), dimension(nDims) :: PositiveAdjustment, NegativeAdjustment

    iEndActual = merge(iEnd, iEnd+1, Periodic == FALSE .or. PeriodicStorage /= NO_OVERLAP_PERIODIC)

    PositiveAdjustment = merge(PeriodicLength, 0._rk, Periodic == PLANE_PERIODIC .and. &
      Tuple >= iEndActual)
    NegativeAdjustment = merge(PeriodicLength, 0._rk, Periodic == PLANE_PERIODIC .and. &
      Tuple < iStart)

    ExtendedCoords = Coords + PositiveAdjustment - NegativeAdjustment

  end function PeriodicExtend

end module ModGridUtils
