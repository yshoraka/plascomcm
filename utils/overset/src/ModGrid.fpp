! Copyright (c) 2015, University of Illinois at Urbana-Champaign, XPACC
! License: MIT, http://opensource.org/licenses/MIT
module ModGrid

  use ModBoundingBox
  use ModGeometry
  use ModGlobal
  use ModGridUtils
  use ModMask
  implicit none

  private

  public :: t_grid
  public :: t_grid_
  public :: MakeGrid
  public :: DestroyGrid

  public :: OverlapsCell
  public :: CoordsInCell
  public :: CellSize
  public :: AvgCellSizeAroundPoint

  public :: GenerateGridMask
  public :: GenerateBoundaryMask

  public :: GRID_TYPE_CARTESIAN, GRID_TYPE_CARTESIAN_ROTATED, GRID_TYPE_RECTILINEAR, &
    GRID_TYPE_RECTILINEAR_ROTATED, GRID_TYPE_CURVILINEAR

  type t_grid
    integer(ik) :: nd
    integer(ik), dimension(MAX_ND) :: is, ie
    integer(ik), dimension(MAX_ND) :: npoints
    integer(ik) :: npoints_total
    real(rk), dimension(:,:), allocatable :: xyz
    type(t_bbox) :: bounds
    integer(ik), dimension(MAX_ND) :: periodic
    integer(ik) :: periodic_storage
    real(rk), dimension(MAX_ND) :: periodic_length
    integer(ik), dimension(:), allocatable :: iblank
    real(rk), dimension(:), allocatable :: cell_sizes
    integer(ik) :: grid_type
    integer(ik) :: id
  end type t_grid

  ! Trailing _ added for compatibility with compilers that don't support F2003 constructors
  interface t_grid_
    module procedure t_grid_Default
  end interface t_grid_

  interface MakeGrid
    module procedure MakeGrid_Rank2
    module procedure MakeGrid_Rank3
  end interface MakeGrid

  integer(ik), parameter :: GRID_TYPE_CARTESIAN = 1
  integer(ik), parameter :: GRID_TYPE_CARTESIAN_ROTATED = 2
  integer(ik), parameter :: GRID_TYPE_RECTILINEAR = 3
  integer(ik), parameter :: GRID_TYPE_RECTILINEAR_ROTATED = 4
  integer(ik), parameter :: GRID_TYPE_CURVILINEAR = 5

contains

  function t_grid_Default(nDims) result(Grid)

    integer(ik), intent(in) :: nDims
    type(t_grid) :: Grid

    Grid%nd = nDims
    Grid%is = 1
    Grid%ie(:nDims) = 0
    Grid%ie(nDims+1:) = 1
    Grid%npoints(:nDims) = 0
    Grid%npoints(nDims+1:) = 1
    Grid%npoints_total = 0
    Grid%bounds = t_bbox_(nDims)
    Grid%periodic = FALSE
    Grid%periodic_storage = NO_OVERLAP_PERIODIC
    Grid%periodic_length = 0._rk
    Grid%grid_type = GRID_TYPE_CURVILINEAR
    Grid%id = 1

  end function t_grid_Default

  subroutine MakeGrid_Rank2(Grid, nDims, iStart, iEnd, Coords, CoordsLayout, Periodic, &
    PeriodicStorage, PeriodicLength, IBlank, GridType, ID)

    type(t_grid), intent(out) :: Grid
    integer(ik), intent(in) :: nDims
    integer(ik), dimension(nDims), intent(in) :: iStart, iEnd
    real(rk), dimension(:,:,:), intent(in) :: Coords
    integer(ik), intent(in), optional :: CoordsLayout
    integer(ik), dimension(nDims), intent(in), optional :: Periodic
    integer(ik), intent(in), optional :: PeriodicStorage
    real(rk), dimension(nDims), intent(in), optional :: PeriodicLength
    integer(ik), dimension(:,:), intent(in), optional :: IBlank
    integer(ik), intent(in), optional :: GridType
    integer(ik), intent(in), optional :: ID

    integer(ik) :: CoordsLayout_
    integer(ik) :: i, j, l
    integer(ik), dimension(nDims) :: nCells

    Grid%nd = nDims
    Grid%is(:nDims) = iStart
    Grid%is(nDims+1:) = 1
    Grid%ie(:nDims) = iEnd
    Grid%ie(nDims+1:) = 1
    Grid%npoints(:nDims) = iEnd - iStart + 1
    Grid%npoints(nDims+1:) = 1
    Grid%npoints_total = product(iEnd - iStart + 1)

    allocate(Grid%xyz(nDims,Grid%npoints_total))

    if (present(CoordsLayout)) then
      CoordsLayout_ = CoordsLayout
    else
      CoordsLayout_ = COMPONENTS_FIRST
    end if

    select case (CoordsLayout_)
    case (COMPONENTS_FIRST)
      l = 1
      do j = Grid%is(2), Grid%ie(2)
        do i = Grid%is(1), Grid%ie(1)
          Grid%xyz(:,l) = Coords(:,i,j)
          l = l + 1
        end do
      end do
    case (COMPONENTS_LAST)
      l = 1
      do j = Grid%is(2), Grid%ie(2)
        do i = Grid%is(1), Grid%ie(1)
          Grid%xyz(:,l) = Coords(i,j,:)
          l = l + 1
        end do
      end do
    end select

    if (present(Periodic)) then
      Grid%periodic(:nDims) = Periodic
      Grid%periodic(nDims+1:) = FALSE
    else
      Grid%periodic = FALSE
    end if

    if (present(PeriodicStorage)) then
      Grid%periodic_storage = PeriodicStorage
    else
      Grid%periodic_storage = NO_OVERLAP_PERIODIC
    end if

    if (present(PeriodicLength)) then
      Grid%periodic_length(:nDims) = PeriodicLength
      Grid%periodic_length(nDims+1:) = 0._rk
    else
      Grid%periodic_length = 0._rk
    end if

    Grid%bounds = ComputeBounds(Grid%nd, Grid%is(:Grid%nd), Grid%ie(:Grid%nd), &
      Grid%periodic(:Grid%nd), Grid%periodic_storage, Grid%periodic_length(:Grid%nd), Grid%xyz)

    allocate(Grid%iblank(Grid%npoints_total))

    if (present(IBlank)) then
      l = 1
      do j = Grid%is(2), Grid%ie(2)
        do i = Grid%is(1), Grid%ie(1)
          Grid%iblank(l) = IBlank(i,j)
          l = l + 1
        end do
      end do
    else
      Grid%iblank = 1
    end if

    if (present(GridType)) then
      Grid%grid_type = GridType
    else
      Grid%grid_type = GRID_TYPE_CURVILINEAR
    end if

    nCells = merge(Grid%npoints(:Grid%nd)-1, Grid%npoints(:Grid%nd), &
      Grid%periodic(:Grid%nd) == FALSE .or. Grid%periodic_storage /= NO_OVERLAP_PERIODIC)

    allocate(Grid%cell_sizes(product(nCells)))

    call ComputeCellSizes(Grid%nd, Grid%is, Grid%ie, Grid%periodic, Grid%periodic_storage, &
      Grid%periodic_length, Grid%xyz, Grid%grid_type, Grid%cell_sizes)

    if (present(ID)) then
      Grid%id = ID
    else
      Grid%id = 1
    end if

  end subroutine MakeGrid_Rank2

  subroutine MakeGrid_Rank3(Grid, nDims, iStart, iEnd, Coords, CoordsLayout, Periodic, &
    PeriodicStorage, PeriodicLength, IBlank, GridType, ID)

    type(t_grid), intent(out) :: Grid
    integer(ik), intent(in) :: nDims
    integer(ik), dimension(nDims), intent(in) :: iStart, iEnd
    real(rk), dimension(:,:,:,:), intent(in) :: Coords
    integer(ik), intent(in), optional :: CoordsLayout
    integer(ik), dimension(nDims), intent(in), optional :: Periodic
    integer(ik), intent(in), optional :: PeriodicStorage
    real(rk), dimension(nDims), intent(in), optional :: PeriodicLength
    integer(ik), dimension(:,:,:), intent(in), optional :: IBlank
    integer(ik), intent(in), optional :: GridType
    integer(ik), intent(in), optional :: ID

    integer(ik) :: CoordsLayout_
    integer(ik) :: i, j, k, l
    integer(ik), dimension(nDims) :: nCells

    Grid%nd = nDims
    Grid%is(:nDims) = iStart
    Grid%is(nDims+1:) = 1
    Grid%ie(:nDims) = iEnd
    Grid%ie(nDims+1:) = 1
    Grid%npoints(:nDims) = iEnd - iStart + 1
    Grid%npoints(nDims+1:) = 1
    Grid%npoints_total = product(iEnd - iStart + 1)

    allocate(Grid%xyz(nDims,Grid%npoints_total))

    if (present(CoordsLayout)) then
      CoordsLayout_ = CoordsLayout
    else
      CoordsLayout_ = COMPONENTS_FIRST
    end if

    select case (CoordsLayout_)
    case (COMPONENTS_FIRST)
      l = 1
      do k = Grid%is(3), Grid%ie(3)
        do j = Grid%is(2), Grid%ie(2)
          do i = Grid%is(1), Grid%ie(1)
            Grid%xyz(:,l) = Coords(:,i,j,k)
            l = l + 1
          end do
        end do
      end do
    case (COMPONENTS_LAST)
      l = 1
      do k = Grid%is(3), Grid%ie(3)
        do j = Grid%is(2), Grid%ie(2)
          do i = Grid%is(1), Grid%ie(1)
            Grid%xyz(:,l) = Coords(i,j,k,:)
            l = l + 1
          end do
        end do
      end do
    end select

    Grid%bounds = ComputeBounds(Grid%nd, Grid%is, Grid%ie, Grid%periodic, Grid%periodic_storage, &
      Grid%periodic_length, Grid%xyz)

    if (present(Periodic)) then
      Grid%periodic(:nDims) = Periodic
      Grid%periodic(nDims+1:) = FALSE
    else
      Grid%periodic = FALSE
    end if

    if (present(PeriodicStorage)) then
      Grid%periodic_storage = PeriodicStorage
    else
      Grid%periodic_storage = NO_OVERLAP_PERIODIC
    end if

    if (present(PeriodicLength)) then
      Grid%periodic_length(:nDims) = PeriodicLength
      Grid%periodic_length(nDims+1:) = 0._rk
    else
      Grid%periodic_length = 0._rk
    end if

    allocate(Grid%iblank(Grid%npoints_total))

    if (present(IBlank)) then
      l = 1
      do k = Grid%is(3), Grid%ie(3)
        do j = Grid%is(2), Grid%ie(2)
          do i = Grid%is(1), Grid%ie(1)
            Grid%iblank(l) = IBlank(i,j,k)
            l = l + 1
          end do
        end do
      end do
    else
      Grid%iblank = 1
    end if

    if (present(GridType)) then
      Grid%grid_type = GridType
    else
      Grid%grid_type = GRID_TYPE_CURVILINEAR
    end if

    nCells = merge(Grid%npoints(:Grid%nd)-1, Grid%npoints(:Grid%nd), &
      Grid%periodic(:Grid%nd) == FALSE .or. Grid%periodic_storage /= NO_OVERLAP_PERIODIC)

    allocate(Grid%cell_sizes(product(nCells)))

    call ComputeCellSizes(Grid%nd, Grid%is, Grid%ie, Grid%periodic, Grid%periodic_storage, &
      Grid%periodic_length, Grid%xyz, Grid%grid_type, Grid%cell_sizes)

    if (present(ID)) then
      Grid%id = ID
    else
      Grid%id = 1
    end if

  end subroutine MakeGrid_Rank3

  subroutine DestroyGrid(Grid)

    type(t_grid), intent(inout) :: Grid

    deallocate(Grid%xyz)
    deallocate(Grid%iblank)
    deallocate(Grid%cell_sizes)

  end subroutine DestroyGrid

  function ComputeBounds(nDims, iStart, iEnd, Periodic, PeriodicStorage, PeriodicLength, Coords) &
    result(Bounds)

    integer(ik), intent(in) :: nDims
    integer(ik), dimension(MAX_ND), intent(in) :: iStart
    integer(ik), dimension(MAX_ND), intent(in) :: iEnd
    integer(ik), dimension(MAX_ND), intent(in) :: Periodic
    integer(ik), intent(in) :: PeriodicStorage
    real(rk), dimension(MAX_ND), intent(in) :: PeriodicLength
    real(rk), dimension(:,:), intent(in) :: Coords
    type(t_bbox) :: Bounds

    integer(ik) :: i, j, k, l, m
    integer(ik), dimension(MAX_ND) :: nPoints
    real(rk), dimension(:,:), allocatable :: PeriodicEndCoords
    integer(ik), dimension(MAX_ND) :: SourcePoint
    integer(ik), dimension(MAX_ND) :: PeriodicPoint
    real(rk), dimension(nDims) :: PrincipalCoords

    nPoints = iEnd - iStart + 1

    Bounds = BBFromPoints(Coords)

    if (Periodic(1) /= FALSE .and. PeriodicStorage == NO_OVERLAP_PERIODIC) then
      allocate(PeriodicEndCoords(nDims,nPoints(2)*nPoints(3)))
      m = 1
      do k = iStart(3), iEnd(3)
        do j = iStart(2), iEnd(2)
          SourcePoint = [iStart(1),j,k]
          PeriodicPoint = [iEnd(1)+1,j,k]
          l = TupleToIndex(nDims, iStart(:nDims), iEnd(:nDims), SourcePoint(:nDims))
          PeriodicEndCoords(:,m) = PeriodicExtend(nDims, Periodic(:nDims), PeriodicStorage, &
            PeriodicLength(:nDims), iStart(:nDims), iEnd(:nDims), PeriodicPoint(:nDims), &
            Coords(:,l))
          m = m + 1
        end do
      end do
      Bounds = BBUnion(Bounds, BBFromPoints(PeriodicEndCoords))
      deallocate(PeriodicEndCoords)
    end if

    if (Periodic(2) /= FALSE .and. PeriodicStorage == NO_OVERLAP_PERIODIC) then
      allocate(PeriodicEndCoords(nDims,nPoints(1)*nPoints(3)))
      m = 1
      do k = iStart(3), iEnd(3)
        do i = iStart(1), iEnd(1)
          SourcePoint = [i,iStart(2),k]
          PeriodicPoint = [i,iEnd(2)+1,k]
          l = TupleToIndex(nDims, iStart(:nDims), iEnd(:nDims), SourcePoint(:nDims))
          PeriodicEndCoords(:,m) = PeriodicExtend(nDims, Periodic(:nDims), PeriodicStorage, &
            PeriodicLength(:nDims), iStart(:nDims), iEnd(:nDims), PeriodicPoint(:nDims), &
            Coords(:,l))
          m = m + 1
        end do
      end do
      Bounds = BBUnion(Bounds, BBFromPoints(PeriodicEndCoords))
      deallocate(PeriodicEndCoords)
    end if

    if (Periodic(3) /= FALSE .and. PeriodicStorage == NO_OVERLAP_PERIODIC) then
      allocate(PeriodicEndCoords(nDims,nPoints(1)*nPoints(2)))
      m = 1
      do j = iStart(2), iEnd(2)
        do i = iStart(1), iEnd(1)
          SourcePoint = [i,j,iStart(3)]
          PeriodicPoint = [i,j,iEnd(3)+1]
          l = TupleToIndex(nDims, iStart(:nDims), iEnd(:nDims), SourcePoint(:nDims))
          PeriodicEndCoords(:,m) = PeriodicExtend(nDims, Periodic(:nDims), PeriodicStorage, &
            PeriodicLength(:nDims), iStart(:nDims), iEnd(:nDims), PeriodicPoint(:nDims), &
            Coords(:,l))
          m = m + 1
        end do
      end do
      Bounds = BBUnion(Bounds, BBFromPoints(PeriodicEndCoords))
      deallocate(PeriodicEndCoords)
    end if

  end function ComputeBounds

  subroutine ComputeCellSizes(nDims, iStart, iEnd, Periodic, PeriodicStorage, PeriodicLength, &
    Coords, GridType, CellSizes)

    integer(ik), intent(in) :: nDims
    integer(ik), dimension(MAX_ND), intent(in) :: iStart
    integer(ik), dimension(MAX_ND), intent(in) :: iEnd
    integer(ik), dimension(MAX_ND), intent(in) :: Periodic
    integer(ik), intent(in) :: PeriodicStorage
    real(rk), dimension(MAX_ND), intent(in) :: PeriodicLength
    real(rk), dimension(:,:), intent(in) :: Coords
    integer(ik), intent(in) :: GridType
    real(rk), dimension(:), intent(out) :: CellSizes

    integer(ik) :: i, j, k, l, m, n, o
    integer(ik), dimension(MAX_ND) :: iStartCell, iEndCell
    integer(ik), dimension(MAX_ND) :: Vertex
    integer(ik), dimension(MAX_ND) :: AdjustedVertex
    real(rk), dimension(nDims) :: PrincipalCoords
    real(rk), dimension(nDims,2**nDims) :: VertexCoords
    logical :: AxisAligned

    iStartCell = iStart
    iEndCell(:nDims) = merge(iEnd(:nDims)-1, iEnd(:nDims), Periodic(:nDims) == FALSE .or. &
      PeriodicStorage /= NO_OVERLAP_PERIODIC)
    iEndCell(nDims+1:) = 1

    l = 1
    do k = iStartCell(3), iEndCell(3)
      do j = iStartCell(2), iEndCell(2)
        do i = iStartCell(1), iEndCell(1)
          do m = 1, 2**nDims
            Vertex = [i,j,k] + [(modulo((m-1)/2**n,2),n=0,MAX_ND-1)]
            AdjustedVertex(:nDims) = PeriodicAdjust(nDims, Periodic(:nDims), PeriodicStorage, &
              iStart(:nDims), iEnd(:nDims), Vertex(:nDims))
            AdjustedVertex(nDims+1:) = 1
            o = TupleToIndex(nDims, iStart(:nDims), iEnd(:nDims), AdjustedVertex(:nDims))
            VertexCoords(:,m) = PeriodicExtend(nDims, Periodic(:nDims), PeriodicStorage, &
              PeriodicLength(:nDims), iStart(:nDims), iEnd(:nDims), Vertex(:nDims), Coords(:,o))
          end do
          AxisAligned = GridType == GRID_TYPE_CARTESIAN .or. GridType == GRID_TYPE_RECTILINEAR
          if (nDims == 3) then
            CellSizes(l) = HexahedronSize(VertexCoords, AxisAligned=AxisAligned)
          else if (nDims == 2) then
            CellSizes(l) = QuadSize(VertexCoords, AxisAligned=AxisAligned)
          end if




!           do n = 0, 1
!             do m = 0, 1
!               Vertex = [i+m,j+n]
!               AdjustedVertex = PeriodicAdjust(nDims, Periodic(:nDims), PeriodicStorage, &
!                 iStart(:nDims), iEnd(:nDims), Vertex)
!               p = TupleToIndex(nDims, iStart(:nDims), iEnd(:nDims), AdjustedVertex)
!               QuadCoords(:,m,n) = PeriodicExtend(nDims, Periodic(:nDims), PeriodicStorage, &
!                 PeriodicLength(:nDims), iStart(:nDims), iEnd(:nDims), Vertex, Coords(:,p))
!               CellSizes(l) = QuadSize(QuadCoords, AxisAligned=AxisAligned)
!             end do
!           end do

!           do o = 0, 1
!             do n = 0, 1
!               do m = 0, 1
!                 Vertex = [i+m,j+n,k+o]
!                 AdjustedVertex = PeriodicAdjust(nDims, Periodic(:nDims), PeriodicStorage, &
!                   iStart(:nDims), iEnd(:nDims), Vertex)
!                 p = TupleToIndex(nDims, iStart(:nDims), iEnd(:nDims), AdjustedVertex)
!                 HexahedronCoords(:,m,n,o) = PeriodicExtend(nDims, Periodic(:nDims), PeriodicStorage, &
!                   PeriodicLength(:nDims), iStart(:nDims), iEnd(:nDims), Vertex, Coords(:,p))
!                 CellSizes(l) = HexahedronSize(HexahedronCoords, AxisAligned=AxisAligned)
!               end do
!             end do
!           end do





          l = l + 1
        end do
      end do
    end do

  end subroutine ComputeCellSizes

  function OverlapsCell(Grid, Cell, Coords) result(Overlaps)

    type(t_grid), intent(in) :: Grid
    integer(ik), dimension(Grid%nd), intent(in) :: Cell
    real(rk), dimension(Grid%nd), intent(in) :: Coords
    logical :: Overlaps

    integer(ik) :: i, j, l
    integer(ik), dimension(Grid%nd) :: Vertex
    integer(ik), dimension(Grid%nd) :: AdjustedVertex
    integer(ik), dimension(2**Grid%nd) :: VertexIBlanks
    real(rk), dimension(Grid%nd) :: PrincipalCoords
    real(rk), dimension(Grid%nd,2**Grid%nd) :: VertexCoords
    logical :: AxisAligned

    do i = 1, 2**Grid%nd
      Vertex = Cell + [(modulo((i-1)/2**j,2),j=0,Grid%nd-1)]
      AdjustedVertex = PeriodicAdjust(Grid%nd, Grid%periodic(:Grid%nd), Grid%periodic_storage, &
        Grid%is(:Grid%nd), Grid%ie(:Grid%nd), Vertex)
      l = TupleToIndex(Grid%nd, Grid%is(:Grid%nd), Grid%ie(:Grid%nd), AdjustedVertex)
      VertexIBlanks(i) = Grid%iblank(l)
      VertexCoords(:,i) = PeriodicExtend(Grid%nd, Grid%periodic(:Grid%nd), Grid%periodic_storage, &
        Grid%periodic_length(:Grid%nd), Grid%is(:Grid%nd), Grid%ie(:Grid%nd), Vertex, &
        Grid%xyz(:,l))
    end do

    if (any(VertexIBlanks == 0)) then
      Overlaps = .false.
      return
    end if

    AxisAligned = Grid%grid_type == GRID_TYPE_CARTESIAN .or. Grid%grid_type == GRID_TYPE_RECTILINEAR

    if (Grid%nd == 2) then
      Overlaps = OverlapsQuad(Coords, VertexCoords, AxisAligned=AxisAligned)
    else if (Grid%nd == 3) then
      Overlaps = OverlapsHexahedron(Coords, VertexCoords, AxisAligned=AxisAligned)
    end if

  end function OverlapsCell

  function CoordsInCell(Grid, Cell, Coords)

    type(t_grid), intent(in) :: Grid
    integer(ik), dimension(Grid%nd), intent(in) :: Cell
    real(rk), dimension(Grid%nd), intent(in) :: Coords
    real(rk), dimension(Grid%nd) :: CoordsInCell

    integer(ik) :: i, j, l
    integer(ik), dimension(Grid%nd) :: Vertex
    integer(ik), dimension(Grid%nd) :: AdjustedVertex
    real(rk), dimension(Grid%nd) :: PrincipalCoords
    real(rk), dimension(Grid%nd,2**Grid%nd) :: VertexCoords
    logical :: AxisAligned

    do i = 1, 2**Grid%nd
      Vertex = Cell + [(modulo((i-1)/2**j,2),j=0,Grid%nd-1)]
      AdjustedVertex = PeriodicAdjust(Grid%nd, Grid%periodic(:Grid%nd), Grid%periodic_storage, &
        Grid%is(:Grid%nd), Grid%ie(:Grid%nd), Vertex)
      l = TupleToIndex(Grid%nd, Grid%is(:Grid%nd), Grid%ie(:Grid%nd), AdjustedVertex)
      VertexCoords(:,i) = PeriodicExtend(Grid%nd, Grid%periodic(:Grid%nd), Grid%periodic_storage, &
        Grid%periodic_length(:Grid%nd), Grid%is(:Grid%nd), Grid%ie(:Grid%nd), Vertex, Grid%xyz(:,l))
    end do

    AxisAligned = Grid%grid_type == GRID_TYPE_CARTESIAN .or. Grid%grid_type == GRID_TYPE_RECTILINEAR

    if (Grid%nd == 2) then
      CoordsInCell = CoordsInQuad(Coords, VertexCoords, AxisAligned=AxisAligned)
    else if (Grid%nd == 3) then
      CoordsInCell = CoordsInHexahedron(Coords, VertexCoords, AxisAligned=AxisAligned)
    end if

  end function CoordsInCell

  function CellSize(Grid, Cell)

    type(t_grid), intent(in) :: Grid
    integer(ik), dimension(Grid%nd), intent(in) :: Cell
    real(rk) :: CellSize

    integer(ik) :: l
    integer(ik), dimension(Grid%nd) :: iStartCell, iEndCell

    iStartCell = Grid%is(:Grid%nd)
    iEndCell = merge(Grid%ie(:Grid%nd)-1, Grid%ie(:Grid%nd), Grid%periodic(:Grid%nd) == FALSE .or. &
      Grid%periodic_storage /= NO_OVERLAP_PERIODIC)

    l = TupleToIndex(Grid%nd, iStartCell, iEndCell, Cell)

    CellSize = Grid%cell_sizes(l)

  end function CellSize

  function AvgCellSizeAroundPoint(Grid, Point) result(AvgCellSize)

    type(t_grid), intent(in) :: Grid
    integer(ik), dimension(Grid%nd), intent(in) :: Point
    real(rk) :: AvgCellSize

    integer(ik) :: i, j, k, l
    integer(ik) :: nCells
    integer(ik), dimension(Grid%nd) :: iStart, iEnd
    integer(ik), dimension(MAX_ND) :: NeighborCellBegin, NeighborCellEnd
    integer(ik), dimension(MAX_ND) :: NeighborCell
    integer(ik), dimension(Grid%nd) :: Cell

    iStart = Grid%is(:Grid%nd)
    iEnd = merge(Grid%ie(:Grid%nd)-1, Grid%ie(:Grid%nd), Grid%periodic(:Grid%nd) == FALSE .or. &
      Grid%periodic_storage /= NO_OVERLAP_PERIODIC)

    NeighborCellBegin(:Grid%nd) = -1
    NeighborCellBegin(Grid%nd+1:) = 0
    NeighborCellEnd = 0

    AvgCellSize = 0._rk
    nCells = 0

    do k = NeighborCellBegin(3), NeighborCellEnd(3)
      do j = NeighborCellBegin(2), NeighborCellEnd(2)
        do i = NeighborCellBegin(1), NeighborCellEnd(1)
          NeighborCell(:Grid%nd) = Point
          NeighborCell(Grid%nd+1:) = Grid%is(Grid%nd+1:)
          NeighborCell = NeighborCell + [i,j,k]
          Cell = PeriodicAdjust(Grid%nd, Grid%periodic(:Grid%nd), Grid%periodic_storage, &
            Grid%is(:Grid%nd), Grid%ie(:Grid%nd), NeighborCell(:Grid%nd))
          if (all(Cell >= iStart) .and. all(Cell <= iEnd)) then
            l = TupleToIndex(Grid%nd, iStart, iEnd, Cell)
            AvgCellSize = AvgCellSize + Grid%cell_sizes(l)
            nCells = nCells + 1
          end if
        end do
      end do
    end do

    AvgCellSize = AvgCellSize/real(nCells, kind=rk)

  end function AvgCellSizeAroundPoint

  subroutine GenerateGridMask(Grid, GridMask)

    type(t_grid), intent(in) :: Grid
    type(t_mask), intent(out) :: GridMask

    integer :: i, j, k, l

    call MakeMask(GridMask, nDims=Grid%nd, iStart=Grid%is(:Grid%nd), iEnd=Grid%ie(:Grid%nd), &
      Periodic=Grid%periodic(:Grid%nd), PeriodicStorage=Grid%periodic_storage)

    l = 1
    do k = Grid%is(3), Grid%ie(3)
      do j = Grid%is(2), Grid%ie(2)
        do i = Grid%is(1), Grid%ie(1)
          GridMask%values(i,j,k) = Grid%iblank(l) /= 0
          l = l + 1
        end do
      end do
    end do

  end subroutine GenerateGridMask

  subroutine GenerateBoundaryMask(Grid, BoundaryIBlank, BoundaryMask)

    type(t_grid), intent(in) :: Grid
    integer(ik), intent(in) :: BoundaryIBlank
    type(t_mask), intent(out) :: BoundaryMask

    integer :: i, j, k, l

    call MakeMask(BoundaryMask, nDims=Grid%nd, iStart=Grid%is(:Grid%nd), iEnd=Grid%ie(:Grid%nd), &
      Periodic=Grid%periodic(:Grid%nd), PeriodicStorage=Grid%periodic_storage)

    l = 1
    do k = Grid%is(3), Grid%ie(3)
      do j = Grid%is(2), Grid%ie(2)
        do i = Grid%is(1), Grid%ie(1)
          BoundaryMask%values(i,j,k) = Grid%iblank(l) == BoundaryIBlank
          l = l + 1
        end do
      end do
    end do

  end subroutine GenerateBoundaryMask

end module ModGrid
