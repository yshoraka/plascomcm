! Copyright (c) 2015, University of Illinois at Urbana-Champaign, XPACC
! License: MIT, http://opensource.org/licenses/MIT
module ModGeometry

  use ModGlobal
  implicit none

  private

  public :: OverlapsQuad, OverlapsHexahedron
  public :: CoordsInQuad, CoordsInHexahedron
  public :: QuadSize, HexahedronSize

  public :: Isoparametric2D, Isoparametric3D
  public :: IsoparametricInverse2D, IsoparametricInverse3D
  public :: InterpBasisLinear, InterpBasisLinearDeriv
  public :: InterpBasisCubic, InterpBasisCubicDeriv

  public :: CELL_TYPE_REGULAR, CELL_TYPE_IRREGULAR

  integer(ik), parameter :: CELL_TYPE_REGULAR = 1
  integer(ik), parameter :: CELL_TYPE_IRREGULAR = 2

contains

  function OverlapsQuad(Coords, Vertices, AxisAligned)

    real(rk), dimension(2), intent(in) :: Coords
    real(rk), dimension(2,4), intent(in) :: Vertices
    logical, intent(in), optional :: AxisAligned
    logical :: OverlapsQuad

    logical :: AxisAligned_
    integer(ik), dimension(3,2) :: Triangles
    integer(ik) :: i
    real(rk), dimension(2,2) :: Basis
    real(rk), dimension(2) :: RelativeCoords
    real(rk), dimension(2) :: LocalCoords

    if (present(AxisAligned)) then
      AxisAligned_ = AxisAligned
    else
      AxisAligned_ = .false.
    end if

    if (AxisAligned_) then

      OverlapsQuad = all(Coords >= Vertices(:,1)) .and. all(Coords <= Vertices(:,4))

    else

      ! Decompose quad into 2 triangles

      Triangles(:,1) = [1,2,3]
      Triangles(:,2) = [4,3,2]

      do i = 1, 2
        Basis(:,1) = Vertices(:,Triangles(2,i)) - Vertices(:,Triangles(1,i))
        Basis(:,2) = Vertices(:,Triangles(3,i)) - Vertices(:,Triangles(1,i))
        RelativeCoords = Coords - Vertices(:,Triangles(1,i))
        LocalCoords = CoordsInBasis2D(RelativeCoords, Basis)
        if (any(LocalCoords < 0._rk)) then
          OverlapsQuad = .false.
          return
        else if (sum(LocalCoords) <= 1._rk) then
          OverlapsQuad = .true.
          return
        end if
      end do

      OverlapsQuad = .true.

    end if

  end function OverlapsQuad

  function OverlapsHexahedron(Coords, Vertices, AxisAligned)

    real(rk), dimension(3), intent(in) :: Coords
    real(rk), dimension(3,8), intent(in) :: Vertices
    logical, intent(in), optional :: AxisAligned
    logical :: OverlapsHexahedron

    logical :: AxisAligned_
    integer(ik) :: i
    integer(ik), dimension(4,4) :: Tetrahedra
    real(rk), dimension(3,3) :: Basis
    real(rk), dimension(3) :: RelativeCoords
    real(rk), dimension(3) :: LocalCoords

    if (present(AxisAligned)) then
      AxisAligned_ = AxisAligned
    else
      AxisAligned_ = .false.
    end if

    if (AxisAligned_) then

      OverlapsHexahedron = all(Coords >= Vertices(:,1)) .and. all(Coords <= Vertices(:,8))

    else

      ! Decompose hexahedron into 4 tetrahedra (technically there are 5, but only 4 are
      ! necessary to test)

      Tetrahedra(:,1) = [1,2,3,5]
      Tetrahedra(:,2) = [4,3,2,8]
      Tetrahedra(:,3) = [6,5,8,2]
      Tetrahedra(:,4) = [7,8,5,3]

      do i = 1, 4
        Basis(:,1) = Vertices(:,Tetrahedra(2,i)) - Vertices(:,Tetrahedra(1,i))
        Basis(:,2) = Vertices(:,Tetrahedra(3,i)) - Vertices(:,Tetrahedra(1,i))
        Basis(:,3) = Vertices(:,Tetrahedra(4,i)) - Vertices(:,Tetrahedra(1,i))
        RelativeCoords = Coords - Vertices(:,Tetrahedra(1,i))
        LocalCoords = CoordsInBasis3D(RelativeCoords, Basis)
        if (any(LocalCoords < 0._rk)) then
          OverlapsHexahedron = .false.
          return
        else if (sum(LocalCoords) <= 1._rk) then
          OverlapsHexahedron = .true.
          return
        end if
      end do

      OverlapsHexahedron = .true.

    end if

  end function OverlapsHexahedron

  function CoordsInQuad(Coords, Vertices, AxisAligned)

    real(rk), dimension(2), intent(in) :: Coords
    real(rk), dimension(2,4), intent(in) :: Vertices
    logical, intent(in), optional :: AxisAligned
    real(rk), dimension(2) :: CoordsInQuad

    integer(ik) :: CellType

    if (present(AxisAligned)) then
      CellType = merge(CELL_TYPE_REGULAR, CELL_TYPE_IRREGULAR, AxisAligned)
    else
      CellType = CELL_TYPE_IRREGULAR
    end if

    CoordsInQuad = IsoparametricInverse2D(1, Vertices, Coords, CellType=CellType)

  end function CoordsInQuad

  function CoordsInHexahedron(Coords, Vertices, AxisAligned)

    real(rk), dimension(3), intent(in) :: Coords
    real(rk), dimension(3,8), intent(in) :: Vertices
    logical, intent(in), optional :: AxisAligned
    real(rk), dimension(3) :: CoordsInHexahedron

    integer(ik) :: CellType

    if (present(AxisAligned)) then
      CellType = merge(CELL_TYPE_REGULAR, CELL_TYPE_IRREGULAR, AxisAligned)
    else
      CellType = CELL_TYPE_IRREGULAR
    end if

    CoordsInHexahedron = IsoparametricInverse3D(1, Vertices, Coords, CellType=CellType)

  end function CoordsInHexahedron

  function QuadSize(Vertices, AxisAligned)

    real(rk), dimension(2,4), intent(in) :: Vertices
    logical, intent(in), optional :: AxisAligned
    real(rk) :: QuadSize

    logical :: AxisAligned_
    integer(ik), dimension(3,2) :: Triangles
    integer(ik) :: i
    real(rk), dimension(2,2) :: Basis
    real(rk) :: TriangleSize

    if (present(AxisAligned)) then
      AxisAligned_ = AxisAligned
    else
      AxisAligned_ = .false.
    end if

    if (AxisAligned_) then

      QuadSize = product(Vertices(:,4) - Vertices(:,1))

    else

      ! Decompose quad into 2 triangles

      Triangles(:,1) = [1,2,3]
      Triangles(:,2) = [4,3,2]

      QuadSize = 0._rk

      do i = 1, 2
        Basis(:,1) = Vertices(:,Triangles(2,i)) - Vertices(:,Triangles(1,i))
        Basis(:,2) = Vertices(:,Triangles(3,i)) - Vertices(:,Triangles(1,i))
        TriangleSize = 0.5_rk * abs(Basis(1,1) * Basis(2,2) - Basis(1,2) * Basis(2,1))
        QuadSize = QuadSize + TriangleSize
      end do

    end if

  end function QuadSize

  function HexahedronSize(Vertices, AxisAligned)

    real(rk), dimension(3,8), intent(in) :: Vertices
    logical, intent(in), optional :: AxisAligned
    real(rk) :: HexahedronSize

    logical :: AxisAligned_
    integer(ik), dimension(4,5) :: Tetrahedra
    integer(ik) :: i
    real(rk), dimension(3,3) :: Basis
    real(rk) :: TetrahedronSize

    if (present(AxisAligned)) then
      AxisAligned_ = AxisAligned
    else
      AxisAligned_ = .false.
    end if

    if (AxisAligned_) then

      HexahedronSize = product(Vertices(:,8) - Vertices(:,1))

    else

      ! Decompose hexahedron into 5 tetrahedra

      Tetrahedra(:,1) = [1,2,3,5]
      Tetrahedra(:,2) = [4,3,2,8]
      Tetrahedra(:,3) = [6,5,8,2]
      Tetrahedra(:,4) = [7,8,5,3]
      Tetrahedra(:,5) = [2,3,5,8]

      HexahedronSize = 0._rk

      do i = 1, 5
        Basis(:,1) = Vertices(:,Tetrahedra(2,i)) - Vertices(:,Tetrahedra(1,i))
        Basis(:,2) = Vertices(:,Tetrahedra(3,i)) - Vertices(:,Tetrahedra(1,i))
        Basis(:,3) = Vertices(:,Tetrahedra(4,i)) - Vertices(:,Tetrahedra(1,i))
        TetrahedronSize = abs( &
          Basis(1,1) * (Basis(2,2)*Basis(3,3) - Basis(2,3)*Basis(3,2)) + &
          Basis(1,2) * (Basis(2,3)*Basis(3,1) - Basis(2,1)*Basis(3,3)) + &
          Basis(1,3) * (Basis(2,1)*Basis(3,2) - Basis(2,2)*Basis(3,1)))/6._rk
        HexahedronSize = HexahedronSize + TetrahedronSize
      end do

    end if

  end function HexahedronSize

  pure function CoordsInBasis2D(Coords, Basis) result(CoordsInBasis)

    real(rk), dimension(2), intent(in) :: Coords
    real(rk), dimension(2,2), intent(in) :: Basis
    real(rk), dimension(2) :: CoordsInBasis

    CoordsInBasis = Solve2D(Basis, Coords)

  end function CoordsInBasis2D

  pure function CoordsInBasis3D(Coords, Basis) result(CoordsInBasis)

    real(rk), dimension(3), intent(in) :: Coords
    real(rk), dimension(3,3), intent(in) :: Basis
    real(rk), dimension(3) :: CoordsInBasis

    CoordsInBasis = Solve3D(Basis, Coords)

  end function CoordsInBasis3D

  pure function Solve2D(A, b) result(x)

    real(rk), dimension(2,2), intent(in) :: A
    real(rk), dimension(2), intent(in) :: b
    real(rk), dimension(2) :: x

    real(rk) :: Det

    Det = A(1,1) * A(2,2) - A(2,1) * A(1,2)

    x(1) = (  b(1) * A(2,2) -   b(2) * A(1,2)) / Det
    x(2) = (A(1,1) *   b(2) - A(2,1) *   b(1)) / Det

  end function Solve2D

  pure function Solve3D(A, b) result(x)

    real(rk), dimension(3,3), intent(in) :: A
    real(rk), dimension(3), intent(in) :: b
    real(rk), dimension(3) :: x

    real(rk) :: Det

    Det = &
      A(1,1) * (A(2,2)*A(3,3) - A(2,3)*A(3,2)) + &
      A(1,2) * (A(2,3)*A(3,1) - A(2,1)*A(3,3)) + &
      A(1,3) * (A(2,1)*A(3,2) - A(2,2)*A(3,1))

    x(1) = ( &
        b(1) * (A(2,2)*A(3,3) - A(2,3)*A(3,2)) + &
      A(1,2) * (A(2,3)*  b(3) -   b(2)*A(3,3)) + &
      A(1,3) * (  b(2)*A(3,2) - A(2,2)*  b(3)) &
      ) / Det

    x(2) = ( &
      A(1,1) * (  b(2)*A(3,3) - A(2,3)*  b(3)) + &
        b(1) * (A(2,3)*A(3,1) - A(2,1)*A(3,3)) + &
      A(1,3) * (A(2,1)*  b(3) -   b(2)*A(3,1)) &
      ) / Det

    x(3) = ( &
      A(1,1) * (A(2,2)*  b(3) -   b(2)*A(3,2)) + &
      A(1,2) * (  b(2)*A(3,1) - A(2,1)*  b(3)) + &
        b(1) * (A(2,1)*A(3,2) - A(2,2)*A(3,1)) &
      ) / Det

  end function Solve3D

  function Isoparametric2D(Degree, Vertices, LocalCoords, CellType) result(Coords)

    integer(ik), intent(in) :: Degree
    real(rk), dimension(2,(Degree+1)**2), intent(in) :: Vertices
    real(rk), dimension(2), intent(in) :: LocalCoords
    integer(ik), intent(in), optional :: CellType
    real(rk), dimension(2) :: Coords

    integer(ik) :: CellType_
    integer(ik) :: i, j, l
    real(rk), dimension(2,Degree+1) :: Basis

    Coords = 0._rk

    if (present(CellType)) then
      CellType_ = CellType
    else
      CellType_ = CELL_TYPE_IRREGULAR
    end if

    select case (CellType_)
    case (CELL_TYPE_REGULAR)

      select case (Degree)
      case (1)
        Coords = (1._rk - LocalCoords) * Vertices(:,1) + LocalCoords * Vertices(:,4)
      case (3)
        Coords = (1._rk - LocalCoords) * Vertices(:,6) + LocalCoords * Vertices(:,11)
      end select

    case (CELL_TYPE_IRREGULAR)

      select case (Degree)
      case (1)
        Basis(1,:) = InterpBasisLinear(LocalCoords(1))
        Basis(2,:) = InterpBasisLinear(LocalCoords(2))
        do j = 1, 2
          do i = 1, 2
            l = 1 + (i-1) + 2*(j-1)
            Coords = Coords + Vertices(:,l) * Basis(1,i) * Basis(2,j)
          end do
        end do
      case (3)
        Basis(1,:) = InterpBasisCubic(LocalCoords(1))
        Basis(2,:) = InterpBasisCubic(LocalCoords(2))
        do j = 1, 4
          do i = 1, 4
            l = 1 + (i-1) + 4*(j-1)
            Coords = Coords + Vertices(:,l) * Basis(1,i) * Basis(2,j)
          end do
        end do
      end select

    end select

  end function Isoparametric2D

  function Isoparametric3D(Degree, Vertices, LocalCoords, CellType) result(Coords)

    integer(ik), intent(in) :: Degree
    real(rk), dimension(3,(Degree+1)**3), intent(in) :: Vertices
    real(rk), dimension(3), intent(in) :: LocalCoords
    integer(ik), intent(in), optional :: CellType
    real(rk), dimension(3) :: Coords

    integer(ik) :: CellType_
    integer(ik) :: i, j, k, l
    real(rk), dimension(3,Degree+1) :: Basis

    Coords = 0._rk

    if (present(CellType)) then
      CellType_ = CellType
    else
      CellType_ = CELL_TYPE_IRREGULAR
    end if

    select case (CellType_)
    case (CELL_TYPE_REGULAR)

      select case (Degree)
      case (1)
        Coords = (1._rk - LocalCoords) * Vertices(:,1) + LocalCoords * Vertices(:,8)
      case (3)
        Coords = (1._rk - LocalCoords) * Vertices(:,22) + LocalCoords * Vertices(:,43)
      end select

    case (CELL_TYPE_IRREGULAR)

      select case (Degree)
      case (1)
        Basis(1,:) = InterpBasisLinear(LocalCoords(1))
        Basis(2,:) = InterpBasisLinear(LocalCoords(2))
        Basis(3,:) = InterpBasisLinear(LocalCoords(3))
        do k = 1, 2
          do j = 1, 2
            do i = 1, 2
              l = 1 + (i-1) + 2*(j-1) + 4*(k-1)
              Coords = Coords + Vertices(:,l) * Basis(1,i) * Basis(2,j) * Basis(3,k)
            end do
          end do
        end do
      case (3)
        Basis(1,:) = InterpBasisCubic(LocalCoords(1))
        Basis(2,:) = InterpBasisCubic(LocalCoords(2))
        Basis(3,:) = InterpBasisCubic(LocalCoords(3))
        do k = 1, 4
          do j = 1, 4
            do i = 1, 4
              l = 1 + (i-1) + 4*(j-1) + 16*(k-1)
              Coords = Coords + Vertices(:,l) * Basis(1,i) * Basis(2,j) * Basis(3,k)
            end do
          end do
        end do
      end select

    end select

  end function Isoparametric3D

  function IsoparametricInverse2D(Degree, Vertices, Coords, CellType, Guess) result(LocalCoords)

    integer(ik), intent(in) :: Degree
    real(rk), dimension(2,(Degree+1)**2), intent(in) :: Vertices
    real(rk), dimension(2), intent(in) :: Coords
    integer(ik), intent(in), optional :: CellType
    real(rk), dimension(2), intent(in), optional :: Guess
    real(rk), dimension(2) :: LocalCoords

    integer(ik) :: CellType_
    integer(ik) :: i
    real(rk), dimension(2) :: Error
    real(rk), dimension(2,2) :: Jacobian

    integer(ik), parameter :: MAX_STEPS = 100
    real(rk), parameter :: MAX_ERROR = 1.e-10_rk

    if (present(CellType)) then
      CellType_ = CellType
    else
      CellType_ = CELL_TYPE_IRREGULAR
    end if

    if (present(Guess)) then
      LocalCoords = Guess
    else
      LocalCoords = 0.5_rk
    end if

    select case (CellType_)
    case (CELL_TYPE_REGULAR)

      select case (Degree)
      case (1)
        LocalCoords = (Coords - Vertices(:,1))/(Vertices(:,4) - Vertices(:,1))
      case (3)
        LocalCoords = (Coords - Vertices(:,6))/(Vertices(:,11) - Vertices(:,6))
      end select

    case (CELL_TYPE_IRREGULAR)

      Error = Coords - Isoparametric2D(Degree, Vertices, LocalCoords, CellType=CellType_)

      i = 1
      do while (any(abs(Error) > MAX_ERROR) .and. i <= MAX_STEPS)
        Jacobian = IsoparametricJacobian2D(Degree, Vertices, LocalCoords)
        LocalCoords = LocalCoords + Solve2D(Jacobian, Error)
        Error = Coords - Isoparametric2D(Degree, Vertices, LocalCoords, CellType=CellType_)
        i = i + 1
      end do

      if (OVERSET_VERBOSE_ENABLED) then
        if (any(abs(Error) > MAX_ERROR)) then
          write (ERROR_UNIT, '(a)') "WARNING: IsoparametricInverse2D failed to converge"
        end if
      end if

    end select

  end function IsoparametricInverse2D

  function IsoparametricInverse3D(Degree, Vertices, Coords, CellType, Guess) result(LocalCoords)

    integer(ik), intent(in) :: Degree
    real(rk), dimension(3,(Degree+1)**3), intent(in) :: Vertices
    real(rk), dimension(3), intent(in) :: Coords
    integer(ik), intent(in), optional :: CellType
    real(rk), dimension(3), intent(in), optional :: Guess
    real(rk), dimension(3) :: LocalCoords

    integer(ik) :: CellType_
    integer(ik) :: i
    real(rk), dimension(3) :: Error
    real(rk), dimension(3,3) :: Jacobian

    integer(ik), parameter :: MAX_STEPS = 100
    real(rk), parameter :: MAX_ERROR = 1.e-10_rk

    if (present(CellType)) then
      CellType_ = CellType
    else
      CellType_ = CELL_TYPE_IRREGULAR
    end if

    if (present(Guess)) then
      LocalCoords = Guess
    else
      LocalCoords = 0.5_rk
    end if

    select case (CellType_)
    case (CELL_TYPE_REGULAR)

      select case (Degree)
      case (1)
        LocalCoords = (Coords - Vertices(:,1))/(Vertices(:,8) - Vertices(:,1))
      case (3)
        LocalCoords = (Coords - Vertices(:,22))/(Vertices(:,43) - Vertices(:,22))
      end select

    case (CELL_TYPE_IRREGULAR)

      Error = Coords - Isoparametric3D(Degree, Vertices, LocalCoords, CellType=CellType_)

      i = 1
      do while (any(abs(Error) > MAX_ERROR) .and. i <= MAX_STEPS)
        Jacobian = IsoparametricJacobian3D(Degree, Vertices, LocalCoords)
        LocalCoords = LocalCoords + Solve3D(Jacobian, Error)
        Error = Coords - Isoparametric3D(Degree, Vertices, LocalCoords, CellType=CellType_)
        i = i + 1
      end do

      if (OVERSET_VERBOSE_ENABLED) then
        if (any(abs(Error) > MAX_ERROR)) then
          write (ERROR_UNIT, '(a)') "WARNING: IsoparametricInverse3D failed to converge"
        end if
      end if

    end select

  end function IsoparametricInverse3D

  function IsoparametricJacobian2D(Degree, Vertices, LocalCoords) result(Jacobian)

    integer(ik), intent(in) :: Degree
    real(rk), dimension(2,(Degree+1)**2), intent(in) :: Vertices
    real(rk), dimension(2), intent(in) :: LocalCoords
    real(rk), dimension(2,2) :: Jacobian

    integer(ik) :: i, j, l
    real(rk), dimension(Degree+1,2) :: Basis
    real(rk), dimension(Degree+1,2) :: BasisDeriv
    real(rk), dimension(2,Degree+1) :: JX, JY

    select case (Degree)
    case (1)
      Basis(:,1) = InterpBasisLinear(LocalCoords(1))
      Basis(:,2) = InterpBasisLinear(LocalCoords(2))
      BasisDeriv(:,1) = InterpBasisLinearDeriv(LocalCoords(1))
      BasisDeriv(:,2) = InterpBasisLinearDeriv(LocalCoords(2))
    case (3)
      Basis(:,1) = InterpBasisCubic(LocalCoords(1))
      Basis(:,2) = InterpBasisCubic(LocalCoords(2))
      BasisDeriv(:,1) = InterpBasisCubicDeriv(LocalCoords(1))
      BasisDeriv(:,2) = InterpBasisCubicDeriv(LocalCoords(2))
    end select

    ! Interpolate in y direction
    JX = 0._rk
    do i = 1, Degree+1
      do j = 1, Degree+1
        l = 1 + (i-1) + (Degree+1)*(j-1)
        JX(:,i) = JX(:,i) + Basis(j,2) * Vertices(:,l)
      end do
    end do

    ! Derivative of interpolation in x direction
    Jacobian(:,1) = 0._rk
    do i = 1, Degree+1
      Jacobian(:,1) = Jacobian(:,1) + BasisDeriv(i,1) * JX(:,i)
    end do

    ! Interpolate in x direction
    JY = 0._rk
    do j = 1, Degree+1
      do i = 1, Degree+1
        l = 1 + (i-1) + (Degree+1)*(j-1)
        JY(:,j) = JY(:,j) + Basis(i,2) * Vertices(:,l)
      end do
    end do

    ! Derivative of interpolation in y direction
    Jacobian(:,2) = 0._rk
    do j = 1, Degree+1
      Jacobian(:,2) = Jacobian(:,2) + BasisDeriv(j,1) * JY(:,j)
    end do

  end function IsoparametricJacobian2D

  function IsoparametricJacobian3D(Degree, Vertices, LocalCoords) result(Jacobian)

    integer(ik), intent(in) :: Degree
    real(rk), dimension(3,(Degree+1)**3), intent(in) :: Vertices
    real(rk), dimension(3), intent(in) :: LocalCoords
    real(rk), dimension(3,3) :: Jacobian

    integer(ik) :: i, j, k, l
    real(rk), dimension(Degree+1,3) :: Basis
    real(rk), dimension(Degree+1,3) :: BasisDeriv
    real(rk), dimension(3,Degree+1,Degree+1) :: JXY, JYZ, JZX
    real(rk), dimension(3,Degree+1) :: JX, JY, JZ

    select case (Degree)
    case (1)
      Basis(:,1) = InterpBasisLinear(LocalCoords(1))
      Basis(:,2) = InterpBasisLinear(LocalCoords(2))
      Basis(:,3) = InterpBasisLinear(LocalCoords(3))
      BasisDeriv(:,1) = InterpBasisLinearDeriv(LocalCoords(1))
      BasisDeriv(:,2) = InterpBasisLinearDeriv(LocalCoords(2))
      BasisDeriv(:,3) = InterpBasisLinearDeriv(LocalCoords(3))
    case (3)
      Basis(:,1) = InterpBasisCubic(LocalCoords(1))
      Basis(:,2) = InterpBasisCubic(LocalCoords(2))
      Basis(:,3) = InterpBasisCubic(LocalCoords(3))
      BasisDeriv(:,1) = InterpBasisCubicDeriv(LocalCoords(1))
      BasisDeriv(:,2) = InterpBasisCubicDeriv(LocalCoords(2))
      BasisDeriv(:,3) = InterpBasisCubicDeriv(LocalCoords(3))
    end select

    ! Interpolate in y direction
    JZX = 0._rk
    do i = 1, Degree+1
      do k = 1, Degree+1
        do j = 1, Degree+1
          l = 1 + (i-1) + (Degree+1)*(j-1) + (Degree+1)*(Degree+1)*(k-1)
          JZX(:,k,i) = JZX(:,k,i) + Basis(j,2) * Vertices(:,l)
        end do
      end do
    end do

    ! Interpolate in z direction
    JX = 0._rk
    do i = 1, Degree+1
      do k = 1, Degree+1
        JX(:,i) = JX(:,i) + Basis(k,3) * JZX(:,k,i)
      end do
    end do

    ! Derivative of interpolation in x direction
    Jacobian(:,1) = 0._rk
    do i = 1, Degree+1
      Jacobian(:,1) = Jacobian(:,1) + BasisDeriv(i,1) * JX(:,i)
    end do

    ! Interpolate in z direction
    JXY = 0._rk
    do j = 1, Degree+1
      do i = 1, Degree+1
        do k = 1, Degree+1
          l = 1 + (i-1) + (Degree+1)*(j-1) + (Degree+1)*(Degree+1)*(k-1)
          JXY(:,i,j) = JXY(:,i,j) + Basis(k,3) * Vertices(:,l)
        end do
      end do
    end do

    ! Interpolate in x direction
    JY = 0._rk
    do j = 1, Degree+1
      do i = 1, Degree+1
        JY(:,j) = JY(:,j) + Basis(i,1) * JXY(:,i,j)
      end do
    end do

    ! Derivative of interpolation in y direction
    Jacobian(:,2) = 0._rk
    do j = 1, Degree+1
      Jacobian(:,2) = Jacobian(:,2) + BasisDeriv(j,2) * JY(:,j)
    end do

    ! Interpolate in x direction
    JYZ = 0._rk
    do k = 1, Degree+1
      do j = 1, Degree+1
        do i = 1, Degree+1
          l = 1 + (i-1) + (Degree+1)*(j-1) + (Degree+1)*(Degree+1)*(k-1)
          JYZ(:,j,k) = JYZ(:,j,k) + Basis(i,1) * Vertices(:,l)
        end do
      end do
    end do

    ! Interpolate in y direction
    JZ = 0._rk
    do k = 1, Degree+1
      do j = 1, Degree+1
        JZ(:,k) = JZ(:,k) + Basis(j,2) * JYZ(:,j,k)
      end do
    end do

    ! Derivative of interpolation in z direction
    Jacobian(:,3) = 0._rk
    do k = 1, Degree+1
      Jacobian(:,3) = Jacobian(:,3) + BasisDeriv(k,3) * JZ(:,k)
    end do

  end function IsoparametricJacobian3D

  pure function InterpBasisLinear(T) result(Basis)

    real(rk), intent(in) :: T
    real(rk), dimension(2) :: Basis

    Basis(1) = 1._rk - T
    Basis(2) = T

  end function InterpBasisLinear

  pure function InterpBasisLinearDeriv(T) result(BasisDeriv)

    real(rk), intent(in) :: T
    real(rk), dimension(2) :: BasisDeriv

    BasisDeriv(1) = -1._rk
    BasisDeriv(2) = 1._rk

  end function InterpBasisLinearDeriv

  pure function InterpBasisCubic(T) result(Basis)

    real(rk), intent(in) :: T
    real(rk), dimension(4) :: Basis

    Basis(1) = (-T**3 + 3._rk * T**2 - 2._rk * T)/6._rk
    Basis(2) = (3._rk * T**3 - 6._rk * T**2 - 3._rk * T + 6._rk)/6._rk
    Basis(3) = (-3._rk * T**3 + 3._rk * T**2 + 6._rk * T)/6._rk
    Basis(4) = (T**3 - T)/6._rk

  end function InterpBasisCubic

  pure function InterpBasisCubicDeriv(T) result(BasisDeriv)

    real(rk), intent(in) :: T
    real(rk), dimension(4) :: BasisDeriv

    BasisDeriv(1) = (-3._rk * T**2 + 6._rk * T - 2._rk)/6._rk
    BasisDeriv(2) = (9._rk * T**2 - 12._rk * T - 3._rk)/6._rk
    BasisDeriv(3) = (-9._rk * T**2 + 6._rk * T + 6._rk)/6._rk
    BasisDeriv(4) = (3._rk * T**2 - 1._rk)/6._rk

  end function InterpBasisCubicDeriv

end module ModGeometry
