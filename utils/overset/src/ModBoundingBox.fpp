! Copyright (c) 2015, University of Illinois at Urbana-Champaign, XPACC
! License: MIT, http://opensource.org/licenses/MIT
module ModBoundingBox

  use ModGlobal
  implicit none

  private

  public :: t_bbox
  public :: t_bbox_
  public :: BBOverlaps
  public :: BBContains
  public :: BBContainsPoint
  public :: BBIsEmpty
  public :: BBSize
  public :: BBMove
  public :: BBGrow
  public :: BBScale
  public :: BBExtend
  public :: BBUnion
  public :: BBIntersect
  public :: BBFromPoints

  type t_bbox
    integer(ik) :: nd
    real(rk), dimension(MAX_ND) :: b, e
  end type t_bbox

  ! Trailing _ added for compatibility with compilers that don't support F2003 constructors
  interface t_bbox_
    module procedure t_bbox_Default
    module procedure t_bbox_Assigned
  end interface t_bbox_

contains

  pure function t_bbox_Default(nDims) result(BBox)

    integer(ik), intent(in) :: nDims
    type(t_bbox) :: BBox

    BBox%nd = nDims
    BBox%b = 0._rk
    BBox%e(:nDims) = -1._rk
    BBox%e(nDims+1:) = 0._rk

  end function t_bbox_Default

  pure function t_bbox_Assigned(nDims, B, E) result(BBox)

    integer(ik), intent(in) :: nDims
    real(rk), dimension(nDims), intent(in) :: B, E
    type(t_bbox) :: BBox

    BBox%nd = nDims
    BBox%b(:BBox%nd) = B
    BBox%b(BBox%nd+1:) = 0._rk
    BBox%e(:BBox%nd) = E
    BBox%e(BBox%nd+1:) = 0._rk

  end function t_bbox_Assigned

  pure function BBOverlaps(LeftBBox, RightBBox)

    type(t_bbox), intent(in) :: LeftBBox, RightBBox
    logical :: BBOverlaps

    BBOverlaps = &
      all(LeftBBox%e(:LeftBBox%nd) >= LeftBBox%b(:LeftBBox%nd)) .and. &
      all(RightBBox%e(:RightBBox%nd) >= RightBBox%b(:RightBBox%nd)) .and. &
      all(RightBBox%e(:RightBBox%nd) >= LeftBBox%b(:LeftBBox%nd)) .and. &
      all(LeftBBox%e(:LeftBBox%nd) >= RightBBox%b(:RightBBox%nd))

  end function BBOverlaps

  pure function BBContains(LeftBBox, RightBBox)

    type(t_bbox), intent(in) :: LeftBBox, RightBBox
    logical :: BBContains

    BBContains = &
      all(LeftBBox%e(:LeftBBox%nd) >= LeftBBox%b(:LeftBBox%nd)) .and. &
      all(RightBBox%e(:RightBBox%nd) >= RightBBox%b(:RightBBox%nd)) .and. &
      all(RightBBox%b(:RightBBox%nd) >= LeftBBox%b(:LeftBBox%nd)) .and. &
      all(LeftBBox%e(:LeftBBox%nd) >= RightBBox%e(:RightBBox%nd))

  end function BBContains

  pure function BBContainsPoint(BBox, Point)

    type(t_bbox), intent(in) :: BBox
    real(rk), dimension(BBox%nd), intent(in) :: Point
    logical :: BBContainsPoint

    BBContainsPoint = all(Point >= BBox%b(:BBox%nd) .and. Point <= BBox%e(:BBox%nd))

  end function BBContainsPoint

  pure function BBIsEmpty(BBox)

    type(t_bbox), intent(in) :: BBox
    logical :: BBIsEmpty

    BBIsEmpty = any(BBox%e(:BBox%nd) < BBox%b(:BBox%nd))

  end function BBIsEmpty

  pure function BBSize(BBox)

    type(t_bbox), intent(in) :: BBox
    real(rk), dimension(BBox%nd) :: BBSize

    BBSize = max(BBox%e(:BBox%nd) - BBox%b(:BBox%nd), 0._rk)

  end function BBSize

  pure function BBMove(BBox, Amount)

    type(t_bbox), intent(in) :: BBox
    real(rk), dimension(BBox%nd), intent(in) :: Amount
    type(t_bbox) :: BBMove

    BBMove%nd = BBox%nd
    BBMove%b(:BBox%nd) = BBox%b(:BBox%nd) + Amount
    BBMove%b(BBox%nd+1:) = BBox%b(BBox%nd+1:)
    BBMove%e(:BBox%nd) = BBox%e(:BBox%nd) + Amount
    BBMove%e(BBox%nd+1:) = BBox%e(BBox%nd+1:)

  end function BBMove

  pure function BBGrow(BBox, Amount)

    type(t_bbox), intent(in) :: BBox
    real(rk), intent(in) :: Amount
    type(t_bbox) :: BBGrow

    BBGrow%nd = BBox%nd
    BBGrow%b(:BBox%nd) = BBox%b(:BBox%nd) - Amount
    BBGrow%b(BBox%nd+1:) = BBox%b(BBox%nd+1:)
    BBGrow%e(:BBox%nd) = BBox%e(:BBox%nd) + Amount
    BBGrow%e(BBox%nd+1:) = BBox%e(BBox%nd+1:)

  end function BBGrow

  pure function BBScale(BBox, Factor)

    type(t_bbox), intent(in) :: BBox
    real(rk), intent(in) :: Factor
    type(t_bbox) :: BBScale

    real(rk), dimension(BBox%nd) :: BBCenter, BBHalfSize

    BBCenter = 0.5_rk * (BBox%b(:BBox%nd) + BBox%e(:BBox%nd))
    BBHalfSize = 0.5_rk * Factor * (BBox%e(:BBox%nd) - BBox%b(:BBox%nd))

    BBScale%nd = BBox%nd
    BBScale%b(:BBox%nd) = BBCenter - BBHalfSize
    BBScale%b(BBox%nd+1:) = BBox%b(BBox%nd+1:)
    BBScale%e(:BBox%nd) = BBCenter + BBHalfSize
    BBScale%e(BBox%nd+1:) = BBox%e(BBox%nd+1:)

  end function BBScale

  pure function BBExtend(BBox, Point)

    type(t_bbox), intent(in) :: BBox
    real(rk), dimension(BBox%nd), intent(in) :: Point
    type(t_bbox) :: BBExtend

    BBExtend%nd = BBox%nd
    BBExtend%b(:BBox%nd) = min(BBox%b(:BBox%nd), Point)
    BBExtend%b(BBox%nd+1:) = BBox%b(BBox%nd+1:)
    BBExtend%e(:BBox%nd) = max(BBox%e(:BBox%nd), Point)
    BBExtend%e(BBox%nd+1:) = BBox%e(BBox%nd+1:)

  end function BBExtend

  pure function BBUnion(LeftBBox, RightBBox)

    type(t_bbox), intent(in) :: LeftBBox
    type(t_bbox), intent(in) :: RightBBox
    type(t_bbox) :: BBUnion

    BBUnion%nd = LeftBBox%nd
    BBUnion%b(:LeftBBox%nd) = min(LeftBBox%b(:LeftBBox%nd), RightBBox%b(:RightBBox%nd))
    BBUnion%b(LeftBBox%nd+1:) = LeftBBox%b(LeftBBox%nd+1:)
    BBUnion%e(:LeftBBox%nd) = max(LeftBBox%e(:LeftBBox%nd), RightBBox%e(:RightBBox%nd))
    BBUnion%e(LeftBBox%nd+1:) = LeftBBox%e(LeftBBox%nd+1:)

  end function BBUnion

  pure function BBIntersect(LeftBBox, RightBBox)

    type(t_bbox), intent(in) :: LeftBBox
    type(t_bbox), intent(in) :: RightBBox
    type(t_bbox) :: BBIntersect

    BBIntersect%nd = LeftBBox%nd
    BBIntersect%b(:LeftBBox%nd) = max(LeftBBox%b(:LeftBBox%nd), RightBBox%b(:RightBBox%nd))
    BBIntersect%b(LeftBBox%nd+1:) = LeftBBox%b(LeftBBox%nd+1:)
    BBIntersect%e(:LeftBBox%nd) = min(LeftBBox%e(:LeftBBox%nd), RightBBox%e(:RightBBox%nd))
    BBIntersect%e(LeftBBox%nd+1:) = LeftBBox%e(LeftBBox%nd+1:)

  end function BBIntersect

  pure function BBFromPoints(Points, PointsLayout)

    real(rk), dimension(:,:), intent(in) :: Points
    integer(ik), intent(in), optional :: PointsLayout
    type(t_bbox) :: BBFromPoints

    integer(ik) :: PointsLayout_
    integer(ik) :: i
    integer(ik) :: nDims

    if (present(PointsLayout)) then
      PointsLayout_ = PointsLayout
    else
      PointsLayout_ = COMPONENTS_FIRST
    end if

    if (PointsLayout_ == COMPONENTS_FIRST) then
      nDims = size(Points,1)
      BBFromPoints%nd = nDims
      if (size(Points,2) == 0) then
        BBFromPoints%b(:nDims) = 0._rk
        BBFromPoints%b(nDims+1:) = 0._rk
        BBFromPoints%e(:nDims) = -1._rk
        BBFromPoints%e(nDims+1:) = 0._rk
        return
      end if
      BBFromPoints%b(:nDims) = [(minval(Points(i,:)),i=1,nDims)]
      BBFromPoints%b(nDims+1:) = 0._rk
      BBFromPoints%e(:nDims) = [(maxval(Points(i,:)),i=1,nDims)]
      BBFromPoints%e(nDims+1:) = 0._rk
    else if (PointsLayout_ == COMPONENTS_LAST) then
      nDims = size(Points,2)
      BBFromPoints%nd = nDims
      if (size(Points,1) == 0) then
        BBFromPoints%b(:nDims) = 0._rk
        BBFromPoints%b(nDims+1:) = 0._rk
        BBFromPoints%e(:nDims) = -1._rk
        BBFromPoints%e(nDims+1:) = 0._rk
        return
      end if
      BBFromPoints%b(:nDims) = [(minval(Points(:,i)),i=1,nDims)]
      BBFromPoints%b(nDims+1:) = 0._rk
      BBFromPoints%e(:nDims) = [(maxval(Points(:,i)),i=1,nDims)]
      BBFromPoints%e(nDims+1:) = 0._rk
    end if

  end function BBFromPoints

end module ModBoundingBox
