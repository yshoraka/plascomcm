! Copyright (c) 2015, University of Illinois at Urbana-Champaign, XPACC
! License: MIT, http://opensource.org/licenses/MIT
module ModInterp

  use ModDonors
  use ModGeometry
  use ModGlobal
  use ModGrid
  use ModGridUtils
  use ModMask
  implicit none

  private

  public :: t_interp
  public :: t_interp_
  public :: MakeInterpData
  public :: GenerateInterpData
  public :: DestroyInterpData

  public :: ExpandDonorCells

!   public :: Interpolate

  public :: INTERP_LINEAR
  public :: INTERP_CUBIC

  type t_interp
    type(t_mask) :: valid_mask
    integer(ik), dimension(:,:,:), allocatable :: donor_grid_ids
    integer(ik), dimension(:,:,:), allocatable :: donor_cells
    real(rk), dimension(:,:,:,:), allocatable :: donor_cell_coords
    integer(ik), dimension(:,:,:), allocatable :: schemes
    real(rk), dimension(:,:,:,:,:), allocatable :: coefs
  end type t_interp

  ! Trailing _ added for compatibility with compilers that don't support F2003 constructors
  interface t_interp_
    module procedure t_interp_Default
  end interface t_interp_

  integer(ik), parameter :: INTERP_LINEAR = 1
  integer(ik), parameter :: INTERP_CUBIC = 2

contains

  function t_interp_Default(nDims) result(InterpData)

    integer(ik), intent(in) :: nDims
    type(t_interp) :: InterpData

    InterpData%valid_mask = t_mask_(nDims)

  end function t_interp_Default

  subroutine MakeInterpData(InterpData, nDims, iStart, iEnd, Periodic, PeriodicStorage, &
      InterpScheme)

    type(t_interp), intent(out) :: InterpData
    integer(ik), intent(in) :: nDims
    integer(ik), dimension(nDims), intent(in) :: iStart, iEnd
    integer(ik), dimension(nDims), intent(in), optional :: Periodic
    integer(ik), intent(in), optional :: PeriodicStorage
    integer(ik), intent(in), optional :: InterpScheme

    integer(ik) :: nInterpStencil
    integer(ik), dimension(MAX_ND) :: iS, iE

    call MakeMask(InterpData%valid_mask, nDims=nDims, iStart=iStart, iEnd=iEnd, Periodic=Periodic, &
      PeriodicStorage=PeriodicStorage)

    if (present(InterpScheme)) then
      select case (InterpScheme)
      case (INTERP_LINEAR)
        nInterpStencil = 2
      case (INTERP_CUBIC)
        nInterpStencil = 4
      end select
    else
      nInterpStencil = 2
    end if

    iS(:nDims) = iStart
    iS(nDims+1:) = 1
    iE(:nDims) = iEnd
    iE(nDims+1:) = 1

    allocate(InterpData%donor_grid_ids(iS(1):iE(1),iS(2):iE(2),iS(3):iE(3)))
    allocate(InterpData%donor_cells(iS(1):iE(1),iS(2):iE(2),iS(3):iE(3)))
    allocate(InterpData%donor_cell_coords(nDims,iS(1):iE(1),iS(2):iE(2),iS(3):iE(3)))
    allocate(InterpData%schemes(iS(1):iE(1),iS(2):iE(2),iS(3):iE(3)))
    allocate(InterpData%coefs(nInterpStencil,nDims,iS(1):iE(1),iS(2):iE(2),iS(3):iE(3)))

  end subroutine MakeInterpData

  subroutine GenerateInterpData(Grid, Donors, InterpData, InterpScheme)

    type(t_grid), intent(in) :: Grid
    type(t_donors), intent(in) :: Donors
    type(t_interp), intent(out) :: InterpData
    integer(ik), intent(in), optional :: InterpScheme

    integer(ik) :: InterpScheme_
    integer(ik) :: i, j, k, l

    if (present(InterpScheme)) then
      InterpScheme_ = InterpScheme
    else
      InterpScheme_ = INTERP_LINEAR
    end if

    call MakeInterpData(InterpData, Grid%nd, Grid%is(:Grid%nd), Grid%ie(:Grid%nd), &
      Periodic=Grid%periodic(:Grid%nd), PeriodicStorage=Grid%periodic_storage, &
      InterpScheme=InterpScheme)

    InterpData%valid_mask%values = .false.

    select case (InterpScheme_)
    case (INTERP_LINEAR)
      do k = Grid%is(3), Grid%ie(3)
        do j = Grid%is(2), Grid%ie(2)
          do i = Grid%is(1), Grid%ie(1)
            if (Donors%valid_mask%values(i,j,k)) then
              InterpData%valid_mask%values(i,j,k) = .true.
              InterpData%donor_grid_ids(i,j,k) = Donors%grid_ids(i,j,k)
              InterpData%donor_cells(i,j,k) = Donors%cells(i,j,k)
              InterpData%donor_cell_coords(:,i,j,k) = Donors%cell_coords(:,i,j,k)
              InterpData%schemes(i,j,k) = INTERP_LINEAR
              do l = 1, Grid%nd
                InterpData%coefs(:,l,i,j,k) = InterpBasisLinear(Donors%cell_coords(l,i,j,k))
              end do
            end if
          end do
        end do
      end do
    case (INTERP_CUBIC)
      do k = Grid%is(3), Grid%ie(3)
        do j = Grid%is(2), Grid%ie(2)
          do i = Grid%is(1), Grid%ie(1)
            if (Donors%valid_mask%values(i,j,k)) then
              InterpData%valid_mask%values(i,j,k) = .true.
              InterpData%donor_grid_ids(i,j,k) = Donors%grid_ids(i,j,k)
              InterpData%donor_cells(i,j,k) = Donors%cells(i,j,k)
              InterpData%donor_cell_coords(:,i,j,k) = Donors%cell_coords(:,i,j,k)
              if (Donors%expanded_mask%values(i,j,k)) then
                InterpData%schemes(i,j,k) = INTERP_CUBIC
                do l = 1, Grid%nd
                  InterpData%coefs(:,l,i,j,k) = InterpBasisCubic(Donors%cell_coords(l,i,j,k))
                end do
              else
                InterpData%schemes(i,j,k) = INTERP_LINEAR
                do l = 1, Grid%nd
                  InterpData%coefs(:2,l,i,j,k) = InterpBasisLinear(Donors%cell_coords(l,i,j,k))
                  InterpData%coefs(3:,l,i,j,k) = 0._rk
                end do
              end if
            end if
          end do
        end do
      end do
    case default
      write (ERROR_UNIT, '(a)') "ERROR: Unrecognized interpolation scheme."
      stop 1
    end select

  end subroutine GenerateInterpData

  subroutine DestroyInterpData(InterpData)

    type(t_interp), intent(inout) :: InterpData

    call DestroyMask(InterpData%valid_mask)

    deallocate(InterpData%donor_grid_ids)
    deallocate(InterpData%donor_cells)
    deallocate(InterpData%donor_cell_coords)
    deallocate(InterpData%schemes)
    deallocate(InterpData%coefs)

  end subroutine DestroyInterpData

  subroutine ExpandDonorCells(Grid, DonorGrids, Donors, InterpScheme)

    type(t_grid), intent(in) :: Grid
    type(t_grid), dimension(:), intent(in) :: DonorGrids
    type(t_donors), intent(inout) :: Donors
    integer(ik), intent(in) :: InterpScheme

    integer(ik) :: i, j, k, l
    integer(ik), dimension(MAX_ND) :: ReceiverPoint
    integer(ik), dimension(Grid%nd) :: DonorCell
    integer(ik), dimension(Grid%nd) :: OldDonorCell

    if (InterpScheme /= INTERP_CUBIC) return

    do k = Grid%is(3), Grid%ie(3)
      do j = Grid%is(2), Grid%ie(2)
        do i = Grid%is(1), Grid%ie(1)
          ReceiverPoint = [i,j,k]
          if (Donors%valid_mask%values(i,j,k)) then
            l = Donors%grid_ids(i,j,k)
            DonorCell = IndexToTuple(DonorGrids(l)%nd, DonorGrids(l)%is(:DonorGrids(l)%nd), &
              DonorGrids(l)%ie(:DonorGrids(l)%nd), Donors%cells(i,j,k))
            OldDonorCell = DonorCell
            call ExpandDonorCell(Grid, DonorGrids(l), ReceiverPoint(:Grid%nd), &
              DonorCell, Donors%cell_coords(:,i,j,k))
            Donors%cells(i,j,k) = TupleToIndex(DonorGrids(l)%nd, &
              DonorGrids(l)%is(:DonorGrids(l)%nd), DonorGrids(l)%ie(:DonorGrids(l)%nd), DonorCell)
            Donors%expanded_mask%values(i,j,k) = any(DonorCell /= OldDonorCell)
          end if
        end do
      end do
    end do

  end subroutine ExpandDonorCells

  subroutine ExpandDonorCell(Grid, DonorGrid, ReceiverPoint, DonorCell, DonorCellCoords)

    type(t_grid), intent(in) :: Grid
    type(t_grid), intent(in) :: DonorGrid
    integer(ik), dimension(Grid%nd), intent(in) :: ReceiverPoint
    integer(ik), dimension(DonorGrid%nd), intent(inout) :: DonorCell
    real(rk), dimension(DonorGrid%nd), intent(inout) :: DonorCellCoords

    integer(ik) :: i, l, m, n
    integer(ik), dimension(MAX_ND) :: Cell
    integer(ik), dimension(MAX_ND) :: ExpandedDonorCellLower, ExpandedDonorCellUpper
    integer(ik), dimension(DonorGrid%nd) :: Vertex
    integer(ik), dimension(DonorGrid%nd) :: AdjustedVertex
    real(rk), dimension(DonorGrid%nd,4**DonorGrid%nd) :: VertexCoords

    Cell(:DonorGrid%nd) = DonorCell-1
    Cell(DonorGrid%nd+1:) = 1
    ExpandedDonorCellLower(:DonorGrid%nd) = PeriodicAdjust(DonorGrid%nd, &
      DonorGrid%periodic(:DonorGrid%nd), DonorGrid%periodic_storage, DonorGrid%is(:DonorGrid%nd), &
      DonorGrid%ie(:DonorGrid%nd), Cell)
    ExpandedDonorCellLower(DonorGrid%nd+1:) = 1

    Cell(:DonorGrid%nd) = DonorCell+2
    Cell(DonorGrid%nd+1:) = 1
    ExpandedDonorCellUpper(:DonorGrid%nd) = PeriodicAdjust(DonorGrid%nd, &
      DonorGrid%periodic(:DonorGrid%nd), DonorGrid%periodic_storage, DonorGrid%is(:DonorGrid%nd), &
      DonorGrid%ie(:DonorGrid%nd), Cell)
    ExpandedDonorCellUpper(DonorGrid%nd+1:) = 1

    if (all(ExpandedDonorCellLower >= DonorGrid%is) .and. &
      all(ExpandedDonorCellUpper <= DonorGrid%ie)) then
      DonorCell = ExpandedDonorCellLower(:DonorGrid%nd)
      do i = 1, 4**DonorGrid%nd
        Vertex = DonorCell + [(modulo((i-1)/4**n,4),n=0,DonorGrid%nd-1)]
        AdjustedVertex = PeriodicAdjust(DonorGrid%nd, DonorGrid%periodic(:DonorGrid%nd), &
          DonorGrid%periodic_storage, DonorGrid%is(:DonorGrid%nd), DonorGrid%ie(:DonorGrid%nd), &
          Vertex)
        l = TupleToIndex(DonorGrid%nd, DonorGrid%is(:DonorGrid%nd), DonorGrid%ie(:DonorGrid%nd), &
          AdjustedVertex)
        VertexCoords(:,i) = PeriodicExtend(DonorGrid%nd, DonorGrid%periodic(:DonorGrid%nd), &
          DonorGrid%periodic_storage, DonorGrid%periodic_length(:DonorGrid%nd), &
          DonorGrid%is(:DonorGrid%nd), DonorGrid%ie(:DonorGrid%nd), Vertex, DonorGrid%xyz(:,l))
      end do
      m = TupleToIndex(Grid%nd, Grid%is(:Grid%nd), Grid%ie(:Grid%nd), ReceiverPoint)
      if (DonorGrid%nd == 3) then
        DonorCellCoords = IsoparametricInverse3D(3, VertexCoords, Grid%xyz(:,m), &
          Guess=DonorCellCoords)
      else
        DonorCellCoords = IsoparametricInverse2D(3, VertexCoords, Grid%xyz(:,m), &
          Guess=DonorCellCoords)
      end if
    end if

  end subroutine ExpandDonorCell

end module ModInterp
