! Copyright (c) 2015, University of Illinois at Urbana-Champaign, XPACC
! License: MIT, http://opensource.org/licenses/MIT
module ModDonors

  use ModBoundingBox
  use ModGlobal
  use ModGrid
  use ModGridUtils
  use ModHashGrid
  use ModMask
  implicit none

  private

  public :: t_donors
  public :: t_donors_
  public :: MakeDonors
  public :: DestroyDonors
  public :: FindDonors
  public :: ChooseDonors
  public :: MergeDonors

  public :: GenerateReceiverMask
  public :: GenerateDonorMask
  public :: GenerateOverlapMask
  public :: GenerateCoarseToFineMask
  public :: GenerateCyclicMask
  public :: GenerateOrphanMask

  public :: t_donor_accel
  public :: t_donor_accel_
  public :: GenerateDonorAccel
  public :: DestroyDonorAccel
  public :: FindDonorCell

  type t_donors
    type(t_mask) :: valid_mask
    integer(ik), dimension(:,:,:), allocatable :: grid_ids
    integer(ik), dimension(:,:,:), allocatable :: cells
    real(rk), dimension(:,:,:,:), allocatable :: cell_coords
    real(rk), dimension(:,:,:), allocatable :: cell_diff_params
    type(t_mask) :: expanded_mask
  end type t_donors

  ! Trailing _ added for compatibility with compilers that don't support F2003 constructors
  interface t_donors_
    module procedure t_donors_Default
  end interface t_donors_

  type t_donor_accel
    type(t_hashgrid) :: hashgrid
  end type t_donor_accel

  ! Trailing _ added for compatibility with compilers that don't support F2003 constructors
  interface t_donor_accel_
    module procedure t_donor_accel_Default
  end interface t_donor_accel_

contains

  function t_donors_Default(nDims) result(Donors)

    integer(ik), intent(in) :: nDims
    type(t_donors) :: Donors

    Donors%valid_mask = t_mask_(nDims)
    Donors%expanded_mask = t_mask_(nDims)

  end function t_donors_Default

  subroutine MakeDonors(Donors, nDims, iStart, iEnd, Periodic, PeriodicStorage)

    type(t_donors), intent(out) :: Donors
    integer(ik), intent(in) :: nDims
    integer(ik), dimension(nDims), intent(in) :: iStart, iEnd
    integer(ik), dimension(nDims), intent(in), optional :: Periodic
    integer(ik), intent(in), optional :: PeriodicStorage

    integer(ik), dimension(MAX_ND) :: iS, iE

    call MakeMask(Donors%valid_mask, nDims=nDims, iStart=iStart, iEnd=iEnd, Periodic=Periodic, &
      PeriodicStorage=PeriodicStorage)

    iS(:nDims) = iStart
    iS(nDims+1:) = 1
    iE(:nDims) = iEnd
    iE(nDims+1:) = 1

    allocate(Donors%grid_ids(iS(1):iE(1),iS(2):iE(2),iS(3):iE(3)))
    allocate(Donors%cells(iS(1):iE(1),iS(2):iE(2),iS(3):iE(3)))
    allocate(Donors%cell_coords(nDims,iS(1):iE(1),iS(2):iE(2),iS(3):iE(3)))
    allocate(Donors%cell_diff_params(iS(1):iE(1),iS(2):iE(2),iS(3):iE(3)))

    call MakeMask(Donors%expanded_mask, nDims=nDims, iStart=iStart, iEnd=iEnd, Periodic=Periodic, &
      PeriodicStorage=PeriodicStorage)

  end subroutine MakeDonors

  subroutine DestroyDonors(Donors)

    type(t_donors), intent(inout) :: Donors

    call DestroyMask(Donors%valid_mask)

    deallocate(Donors%grid_ids)
    deallocate(Donors%cells)
    deallocate(Donors%cell_coords)
    deallocate(Donors%cell_diff_params)

    call DestroyMask(Donors%expanded_mask)

  end subroutine DestroyDonors

  subroutine FindDonors(DonorGrid, ReceiverGrid, Donors)

    type(t_grid), intent(in) :: DonorGrid, ReceiverGrid
    type(t_donors), intent(out) :: Donors

    integer(ik) :: i, j, k, l
    type(t_bbox) :: Bounds
    type(t_donor_accel) :: DonorAccel
    integer(ik), dimension(DonorGrid%nd) :: DonorCell
    integer(ik), dimension(MAX_ND) :: ReceiverPoint
    real(rk), dimension(ReceiverGrid%nd) :: ReceiverCoords
    real(rk) :: DonorCellSize, ReceiverCellSize

    call MakeDonors(Donors, nDims=ReceiverGrid%nd, iStart=ReceiverGrid%is(:ReceiverGrid%nd), &
      iEnd=ReceiverGrid%ie(:ReceiverGrid%nd), Periodic=ReceiverGrid%periodic(:ReceiverGrid%nd), &
      PeriodicStorage=ReceiverGrid%periodic_storage)
    Donors%valid_mask%values = .false.
    Donors%expanded_mask%values = .false.

    Bounds = BBScale(BBIntersect(DonorGrid%bounds, ReceiverGrid%bounds), 1.001_rk)

    if (BBIsEmpty(Bounds)) then
      return
    end if

    call GenerateDonorAccel(DonorGrid, DonorAccel, Bounds=Bounds)

    do k = ReceiverGrid%is(3), ReceiverGrid%ie(3)
      do j = ReceiverGrid%is(2), ReceiverGrid%ie(2)
        do i = ReceiverGrid%is(1), ReceiverGrid%ie(1)
          ReceiverPoint = [i,j,k]
          l = TupleToIndex(ReceiverGrid%nd, ReceiverGrid%is(:ReceiverGrid%nd), &
            ReceiverGrid%ie(:ReceiverGrid%nd), ReceiverPoint(:ReceiverGrid%nd))
          ReceiverCoords = ReceiverGrid%xyz(:,l)
          if (ReceiverGrid%iblank(l) /= 0 .and. BBContainsPoint(Bounds, ReceiverCoords)) then
            DonorCell = FindDonorCell(DonorGrid, DonorAccel, ReceiverCoords)
            if (all(DonorCell /= DonorGrid%is(:DonorGrid%nd)-1)) then
              Donors%valid_mask%values(i,j,k) = .true.
              Donors%grid_ids(i,j,k) = DonorGrid%id
              Donors%cells(i,j,k) = TupleToIndex(DonorGrid%nd, DonorGrid%is(:DonorGrid%nd), &
                DonorGrid%ie(:DonorGrid%nd), DonorCell)
              Donors%cell_coords(:,i,j,k) = CoordsInCell(DonorGrid, DonorCell, ReceiverCoords)
              DonorCellSize = CellSize(DonorGrid, DonorCell)
              ReceiverCellSize = AvgCellSizeAroundPoint(ReceiverGrid, &
                ReceiverPoint(:ReceiverGrid%nd))
              Donors%cell_diff_params(i,j,k) = log(DonorCellSize/ReceiverCellSize)/(log(2._rk) * &
                real(ReceiverGrid%nd,kind=rk))
            end if
          end if
        end do
      end do
    end do

  end subroutine FindDonors

  subroutine ChooseDonors(Grid, CandidateDonors, Subset)

    type(t_grid), intent(in) :: Grid
    type(t_donors), dimension(:), intent(inout) :: CandidateDonors
    type(t_mask), intent(in), optional :: Subset

    integer(ik) :: i, j, k, m
    logical :: IncludePoint
    real(rk) :: CandidateCellDiff, BestCellDiff
    integer(ik) :: BestGrid

    do k = Grid%is(3), Grid%ie(3)
      do j = Grid%is(2), Grid%ie(2)
        do i = Grid%is(1), Grid%ie(1)
          if (present(Subset)) then
            IncludePoint = Subset%values(i,j,k)
          else
            IncludePoint = .true.
          end if
          if (IncludePoint) then
            BestCellDiff = huge(0._rk)
            BestGrid = 0
            do m = 1, size(CandidateDonors)
              if (CandidateDonors(m)%valid_mask%values(i,j,k)) then
                CandidateCellDiff = CandidateDonors(m)%cell_diff_params(i,j,k)
                if ((BestCellDiff >= 0._rk .and. CandidateCellDiff < BestCellDiff) .or. &
                  (BestCellDiff < 0._rk .and. CandidateCellDiff > BestCellDiff)) then
                  if (BestGrid /= 0) then
                    CandidateDonors(BestGrid)%valid_mask%values(i,j,k) = .false.
                  end if
                  BestCellDiff = CandidateCellDiff
                  BestGrid = m
                else
                  CandidateDonors(m)%valid_mask%values(i,j,k) = .false.
                end if
              end if
            end do
          end if
        end do
      end do
    end do

  end subroutine ChooseDonors

  subroutine MergeDonors(Grid, CandidateDonors, MergedDonors)

    type(t_grid), intent(in) :: Grid
    type(t_donors), dimension(:), intent(in) :: CandidateDonors
    type(t_donors), intent(out) :: MergedDonors

    integer(ik) :: i, j, k, m

    call MakeDonors(MergedDonors, nDims=Grid%nd, iStart=Grid%is(:Grid%nd), iEnd=Grid%ie(:Grid%nd), &
      Periodic=Grid%periodic(:Grid%nd), PeriodicStorage=Grid%periodic_storage)
    MergedDonors%valid_mask%values = .false.
    MergedDonors%expanded_mask%values = .false.

    do k = Grid%is(3), Grid%ie(3)
      do j = Grid%is(2), Grid%ie(2)
        do i = Grid%is(1), Grid%ie(1)
          do m = 1, size(CandidateDonors)
            if (CandidateDonors(m)%valid_mask%values(i,j,k)) then
              MergedDonors%valid_mask%values(i,j,k) = .true.
              MergedDonors%grid_ids(i,j,k) = CandidateDonors(m)%grid_ids(i,j,k)
              MergedDonors%cells(i,j,k) = CandidateDonors(m)%cells(i,j,k)
              MergedDonors%cell_coords(:,i,j,k) = CandidateDonors(m)%cell_coords(:,i,j,k)
              MergedDonors%cell_diff_params(i,j,k) = CandidateDonors(m)%cell_diff_params(i,j,k)
              MergedDonors%expanded_mask%values(i,j,k) = &
                CandidateDonors(m)%expanded_mask%values(i,j,k)
              exit
            end if
          end do
        end do
      end do
    end do

  end subroutine MergeDonors

  subroutine GenerateReceiverMask(ReceiverGrid, DonorGrid, Donors, ReceiverMask, ReceiverSubset, &
    DonorSubset)

    type(t_grid), intent(in) :: ReceiverGrid
    type(t_grid), intent(in) :: DonorGrid
    type(t_donors), intent(in) :: Donors
    type(t_mask), intent(out) :: ReceiverMask
    type(t_mask), intent(in), optional :: ReceiverSubset
    type(t_mask), intent(in), optional :: DonorSubset

    integer(ik) :: i, j, k, l, m
    logical :: IncludePoint
    integer(ik), dimension(MAX_ND) :: DonorCell
    integer(ik), dimension(MAX_ND) :: Vertex
    integer(ik), dimension(MAX_ND) :: AdjustedVertex

    call MakeMask(ReceiverMask, nDims=ReceiverGrid%nd, iStart=ReceiverGrid%is(:ReceiverGrid%nd), &
      iEnd=ReceiverGrid%ie(:ReceiverGrid%nd), Periodic=ReceiverGrid%periodic(:ReceiverGrid%nd), &
      PeriodicStorage=ReceiverGrid%periodic_storage)
    ReceiverMask%values = .false.

    do k = ReceiverGrid%is(3), ReceiverGrid%ie(3)
      do j = ReceiverGrid%is(2), ReceiverGrid%ie(2)
        do i = ReceiverGrid%is(1), ReceiverGrid%ie(1)
          if (present(ReceiverSubset)) then
            IncludePoint = ReceiverSubset%values(i,j,k)
          else
            IncludePoint = .true.
          end if
          if (IncludePoint .and. Donors%valid_mask%values(i,j,k)) then
            if (Donors%grid_ids(i,j,k) == DonorGrid%id) then
              if (present(DonorSubset)) then
                DonorCell = IndexToTuple(MAX_ND, DonorGrid%is, DonorGrid%ie, Donors%cells(i,j,k))
                do l = 1, 2**DonorGrid%nd
                  Vertex = DonorCell + [(modulo((l-1)/2**m,2),m=0,MAX_ND-1)]
                  AdjustedVertex = PeriodicAdjust(MAX_ND, DonorGrid%periodic, &
                    DonorGrid%periodic_storage, DonorGrid%is, DonorGrid%ie, Vertex)
                  if (DonorSubset%values(AdjustedVertex(1),AdjustedVertex(2),&
                    AdjustedVertex(3))) then
                    ReceiverMask%values(i,j,k) = .true.
                    exit
                  end if
                end do
              else
                ReceiverMask%values(i,j,k) = .true.
              end if
            end if
          end if
        end do
      end do
    end do

  end subroutine GenerateReceiverMask

  subroutine GenerateDonorMask(DonorGrid, ReceiverGrid, Donors, DonorMask, DonorSubset, &
    ReceiverSubset)

    type(t_grid), intent(in) :: DonorGrid
    type(t_grid), intent(in) :: ReceiverGrid
    type(t_donors), intent(in) :: Donors
    type(t_mask), intent(out) :: DonorMask
    type(t_mask), intent(in), optional :: DonorSubset
    type(t_mask), intent(in), optional :: ReceiverSubset

    integer(ik) :: i, j, k, l, m
    logical :: IncludePoint
    integer(ik), dimension(MAX_ND) :: DonorCell
    integer(ik), dimension(MAX_ND) :: Vertex
    integer(ik), dimension(MAX_ND) :: AdjustedVertex

    call MakeMask(DonorMask, nDims=DonorGrid%nd, iStart=DonorGrid%is(:DonorGrid%nd), &
      iEnd=DonorGrid%ie(:DonorGrid%nd), Periodic=DonorGrid%periodic(:DonorGrid%nd), &
      PeriodicStorage=DonorGrid%periodic_storage)
    DonorMask%values = .false.

    do k = ReceiverGrid%is(3), ReceiverGrid%ie(3)
      do j = ReceiverGrid%is(2), ReceiverGrid%ie(2)
        do i = ReceiverGrid%is(1), ReceiverGrid%ie(1)
          if (present(ReceiverSubset)) then
            IncludePoint = ReceiverSubset%values(i,j,k)
          else
            IncludePoint = .true.
          end if
          if (IncludePoint) then
            if (Donors%valid_mask%values(i,j,k)) then
              if (Donors%grid_ids(i,j,k) == DonorGrid%id) then
                DonorCell = IndexToTuple(MAX_ND, DonorGrid%is, DonorGrid%ie, Donors%cells(i,j,k))
                do l = 1, 2**DonorGrid%nd
                  Vertex = DonorCell + [(modulo((l-1)/2**m,2),m=0,MAX_ND-1)]
                  AdjustedVertex = PeriodicAdjust(MAX_ND, DonorGrid%periodic, &
                    DonorGrid%periodic_storage, DonorGrid%is, DonorGrid%ie, Vertex)
                  if (present(DonorSubset)) then
                    if (DonorSubset%values(AdjustedVertex(1),AdjustedVertex(2), &
                      AdjustedVertex(3))) then
                      DonorMask%values(AdjustedVertex(1),AdjustedVertex(2), &
                        AdjustedVertex(3)) = .true.
                    end if
                  else
                    DonorMask%values(AdjustedVertex(1),AdjustedVertex(2), &
                      AdjustedVertex(3)) = .true.
                  end if
                end do
              end if
            end if
          end if
        end do
      end do
    end do

    if (DonorGrid%periodic(1) /= FALSE .and. DonorMask%periodic_storage == OVERLAP_PERIODIC) then
      DonorMask%values(DonorGrid%ie(1),:,:) = DonorMask%values(DonorGrid%is(1),:,:)
    end if

    if (DonorGrid%periodic(2) /= FALSE .and. DonorMask%periodic_storage == OVERLAP_PERIODIC) then
      DonorMask%values(:,DonorGrid%ie(2),:) = DonorMask%values(:,DonorGrid%is(2),:)
    end if

    if (DonorGrid%periodic(3) /= FALSE .and. DonorMask%periodic_storage == OVERLAP_PERIODIC) then
      DonorMask%values(:,:,DonorGrid%ie(3)) = DonorMask%values(:,:,DonorGrid%is(3))
    end if

  end subroutine GenerateDonorMask

  subroutine GenerateOverlapMask(Grid, CandidateDonors, OverlapMask)

    type(t_grid), intent(in) :: Grid
    type(t_donors), dimension(:), intent(in) :: CandidateDonors
    type(t_mask), intent(out) :: OverlapMask

    integer(ik) :: i

    call MakeMask(OverlapMask, nDims=Grid%nd, iStart=Grid%is(:Grid%nd), iEnd=Grid%ie(:Grid%nd), &
      Periodic=Grid%periodic(:Grid%nd), PeriodicStorage=Grid%periodic_storage)
    OverlapMask%values = .false.

    do i = 1, size(CandidateDonors)
      OverlapMask%values = OverlapMask%values .or. CandidateDonors(i)%valid_mask%values
    end do

  end subroutine GenerateOverlapMask

  subroutine GenerateCoarseToFineMask(Grid, Donors, CoarseToFineMask, Subset)

    type(t_grid), intent(in) :: Grid
    type(t_donors), intent(in) :: Donors
    type(t_mask), intent(out) :: CoarseToFineMask
    type(t_mask), intent(in), optional :: Subset

    integer(ik) :: i, j, k
    logical :: IncludePoint

    call MakeMask(CoarseToFineMask, nDims=Grid%nd, iStart=Grid%is(:Grid%nd), &
      iEnd=Grid%ie(:Grid%nd), Periodic=Grid%periodic(:Grid%nd), &
      PeriodicStorage=Grid%periodic_storage)
    CoarseToFineMask%values = .false.

    do k = Grid%is(3), Grid%ie(3)
      do j = Grid%is(2), Grid%ie(2)
        do i = Grid%is(1), Grid%ie(1)
          if (present(Subset)) then
            IncludePoint = Subset%values(i,j,k)
          else
            IncludePoint = .true.
          end if
          if (IncludePoint) then
            if (Donors%valid_mask%values(i,j,k)) then
              CoarseToFineMask%values(i,j,k) = Donors%cell_diff_params(i,j,k) > 0._rk .or. &
                (Donors%cell_diff_params(i,j,k) == 0._rk .and. Donors%grid_ids(i,j,k) < Grid%id)
            end if
          end if
        end do
      end do
    end do

  end subroutine GenerateCoarseToFineMask

  subroutine GenerateCyclicMask(Grid1, Grid2, Donors1, Donors2, CyclicMask, Subset1, Subset2)

    type(t_grid), intent(in) :: Grid1, Grid2
    type(t_donors), intent(in) :: Donors1, Donors2
    type(t_mask), intent(out) :: CyclicMask
    type(t_mask), intent(in), optional :: Subset1, Subset2

    type(t_mask) :: ReceiverMask
    type(t_mask) :: DonorMask

    call GenerateReceiverMask(Grid1, Grid2, Donors1, ReceiverMask, ReceiverSubset=Subset1, &
      DonorSubset=Subset2)

    call GenerateDonorMask(Grid1, Grid2, Donors2, DonorMask, DonorSubset=Subset1, &
      ReceiverSubset=Subset2)

    call MakeMask(CyclicMask, nDims=Grid1%nd, iStart=Grid1%is(:Grid1%nd), &
      iEnd=Grid1%ie(:Grid1%nd), Periodic=Grid1%periodic(:Grid1%nd), &
      PeriodicStorage=Grid1%periodic_storage)

    CyclicMask%values = ReceiverMask%values .and. DonorMask%values

  end subroutine GenerateCyclicMask

  subroutine GenerateOrphanMask(Grid, ReceiverMask, Donors, OrphanMask)

    type(t_grid), intent(in) :: Grid
    type(t_mask), intent(in) :: ReceiverMask
    type(t_donors), intent(in) :: Donors
    type(t_mask), intent(out) :: OrphanMask

    call MakeMask(OrphanMask, nDims=Grid%nd, iStart=Grid%is(:Grid%nd), &
      iEnd=Grid%ie(:Grid%nd), Periodic=Grid%periodic(:Grid%nd), &
      PeriodicStorage=Grid%periodic_storage)
    OrphanMask%values = ReceiverMask%values .and. .not. Donors%valid_mask%values

  end subroutine GenerateOrphanMask

  function t_donor_accel_Default(nDims) result(DonorAccel)

    integer(ik), intent(in) :: nDims
    type(t_donor_accel) :: DonorAccel

    DonorAccel%hashgrid = t_hashgrid_(nDims)

  end function t_donor_accel_Default

  subroutine GenerateDonorAccel(Grid, DonorAccel, Bounds)

    type(t_grid), intent(in) :: Grid
    type(t_donor_accel), intent(out) :: DonorAccel
    type(t_bbox), intent(in), optional :: Bounds

    call GenerateHashGrid(Grid, DonorAccel%hashgrid, Bounds=Bounds)

  end subroutine GenerateDonorAccel

  subroutine DestroyDonorAccel(DonorAccel)

    type(t_donor_accel), intent(inout) :: DonorAccel

    call DestroyHashGrid(DonorAccel%hashgrid)

  end subroutine DestroyDonorAccel

  function FindDonorCell(DonorGrid, DonorAccel, Coords) result(DonorCell)

    type(t_grid), intent(in) :: DonorGrid
    type(t_donor_accel), intent(in) :: DonorAccel
    real(rk), dimension(DonorGrid%nd), intent(in) :: Coords
    integer(ik), dimension(DonorGrid%nd) :: DonorCell

    DonorCell = HashGridFind(DonorGrid, DonorAccel%hashgrid, Coords)

  end function FindDonorCell

end module ModDonors
