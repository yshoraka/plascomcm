! Copyright (c) 2015, University of Illinois at Urbana-Champaign, XPACC
! License: MIT, http://opensource.org/licenses/MIT
module ModPegasus

  use ModGlobal
  use ModGrid
  use ModGridUtils
  use ModInterp
  implicit none

  private

  public :: t_pegasus
  public :: t_pegasus_
  public :: MakePegasusData
  public :: DestroyPegasusData
  public :: WritePegasusData

  ! PEGASUS variable name key
  ! =========================
  !
  ! Grid data
  ! ---------
  ! ngrd - number of grids
  ! ieng(ng) - number of points in 1-dir of grid ng
  ! jeng(ng) - number of points in 2-dir of grid ng
  ! keng(ng) - number of points in 3-dir of grid ng

  ! Global interp data
  ! ------------------
  ! ipall - total number of donor points over all grids
  ! igall - maximum number of grid points, taken over all grids
  ! ipip - maximum number of donor points, taken over all grids
  ! ipbp - maximum number of receiver points, taken over all grids
  ! iisptr(ng) - starting index of global linear index of donor points on grid ng
  ! iieptr(ng) - ending index of global linear index of donor points on grid ng
  
  ! Interp data
  ! -----------
  ! ibpnts(ng) - number of receiver points on grid ng
  ! iipnts(ng) - number of donor points on grid ng
  ! ibct(m,ng) - global linear (over all grids' donor points) index of the donor point for the m-th receiver point on grid ng
  ! iit(m,ng) - local 1-dir index of m-th donor point on grid ng (in stencil’s most SW corner)
  ! jit(m,ng) - local 2-dir index of m-th donor point on grid ng (in stencil’s most SW corner)
  ! kit(m,ng) - local 3-dir index of m-th donor point on grid ng (in stencil’s most SW corner)
  ! dxit(m,ng) - 1-dir computational offset of m-th donor point on grid ng [only used in OGEN]
  ! dyit(m,ng) - 2-dir computational offset of m-th donor point on grid ng [only used in OGEN]
  ! dzit(m,ng) - 3-dir computational offset of m-th donor point on grid ng [only used in OGEN]
  ! ibt(m,ng) - local 1-dir index of m-th receiver point on grid ng
  ! jbt(m,ng) - local 2-dir index of m-th receiver point on grid ng
  ! kbt(m,ng) - local 3-dir index of m-th receiver point on grid ng
  ! nit(m,ng) - 1-dir stencil size of m-th donor point on grid ng
  ! njt(m,ng) - 2-dir stencil size of m-th donor point on grid ng
  ! nkt(m,ng) - 3-dir stencil size of m-th donor point on grid ng
  ! coeffit(m,io,1,ng) - polynomial coefficients of m-th donor point on grid ng in 1-dir, io = 1, ..., nit(m,ng)
  ! coeffit(m,io,2,ng) - polynomial coefficients of m-th donor point on grid ng in 2-dir, io = 1, ..., njt(m,ng)
  ! coeffit(m,io,3,ng) - polynomial coefficients of m-th donor point on grid ng in 3-dir, io = 1, ..., nkt(m,ng)

  type t_pegasus
    integer(ik) :: ngrd
    integer(ik), dimension(:), allocatable :: ieng, jeng, keng
    integer(ik) :: ipall
    integer(ik) :: igall
    integer(ik) :: ipip
    integer(ik) :: ipbp
    integer(ik), dimension(:), allocatable :: iisptr
    integer(ik), dimension(:), allocatable :: iieptr
    integer(ik), dimension(:), allocatable :: ibpnts
    integer(ik), dimension(:), allocatable :: iipnts
    integer(ik), dimension(:,:), allocatable :: ibct
    integer(ik), dimension(:,:), allocatable :: iit, jit, kit
    real(rk), dimension(:,:), allocatable :: dxit, dyit, dzit
    integer(ik), dimension(:,:), allocatable :: ibt, jbt, kbt
    integer(ik), dimension(:,:), allocatable :: nit, njt, nkt
    real(rk), dimension(:,:,:,:), allocatable :: coeffit
  end type t_pegasus

  ! Trailing _ added for compatibility with compilers that don't support F2003 constructors
  interface t_pegasus_
    module procedure t_pegasus_Default
  end interface t_pegasus_

contains

  function t_pegasus_Default() result(PegasusData)

    type(t_pegasus) :: PegasusData

    PegasusData%ngrd = 0
    PegasusData%ipall = 0
    PegasusData%igall = 0
    PegasusData%ipip = 0
    PegasusData%ipbp = 0

  end function t_pegasus_Default

  subroutine MakePegasusData(Grids, InterpData, PegasusData)

    type(t_grid), dimension(:), intent(in) :: Grids
    type(t_interp), dimension(:), intent(in) :: InterpData
    type(t_pegasus), intent(out) :: PegasusData

    integer(ik) :: i, j, k, m, n
    integer(ik) :: nGrids
    integer(ik) :: nDims
    integer(ik), dimension(:), allocatable :: nDonors, nReceivers
    integer(ik) :: nDonorsMax, nReceiversMax
    integer(ik), dimension(:), allocatable :: nInterpStencil
    integer(ik) :: nInterpStencilMax
    integer(ik) :: Offset
    integer(ik), dimension(:), allocatable :: NextReceiver
    integer(ik), dimension(:), allocatable :: NextDonor
    integer(ik), dimension(MAX_ND) :: ReceiverPoint
    integer(ik), dimension(MAX_ND) :: DonorCell
    real(rk), dimension(MAX_ND) :: DonorCellCoords
    integer(ik) :: InterpScheme
    real(rk), dimension(:,:), allocatable :: InterpCoefs

    nGrids = size(Grids)

    if (nGrids == 0) then
      PegasusData = t_pegasus_()
      return
    end if

    nDims = Grids(1)%nd

    allocate(nDonors(nGrids))
    allocate(nReceivers(nGrids))

    nDonors = 0
    nReceivers = 0

    do m = 1, nGrids
      do k = Grids(m)%is(3), Grids(m)%ie(3)
        do j = Grids(m)%is(2), Grids(m)%ie(2)
          do i = Grids(m)%is(1), Grids(m)%ie(1)
            if (InterpData(m)%valid_mask%values(i,j,k)) then
              n = InterpData(m)%donor_grid_ids(i,j,k)
              nReceivers(m) = nReceivers(m) + 1
              nDonors(n) = nDonors(n) + 1
            end if
          end do
        end do
      end do
    end do

    nDonorsMax = maxval(nDonors)
    nReceiversMax = maxval(nReceivers)

    allocate(nInterpStencil(nGrids))

    nInterpStencil = [(size(InterpData(m)%coefs,1),m=1,nGrids)]
    nInterpStencilMax = maxval(nInterpStencil)

    allocate(InterpCoefs(nInterpStencilMax,MAX_ND))

    allocate(PegasusData%ieng(nGrids))
    allocate(PegasusData%jeng(nGrids))
    allocate(PegasusData%keng(nGrids))
    allocate(PegasusData%iisptr(nGrids))
    allocate(PegasusData%iieptr(nGrids))
    allocate(PegasusData%ibpnts(nGrids))
    allocate(PegasusData%iipnts(nGrids))
    allocate(PegasusData%ibct(nReceiversMax,nGrids))
    allocate(PegasusData%iit(nDonorsMax,nGrids))
    allocate(PegasusData%jit(nDonorsMax,nGrids))
    allocate(PegasusData%kit(nDonorsMax,nGrids))
    allocate(PegasusData%dxit(nDonorsMax,nGrids))
    allocate(PegasusData%dyit(nDonorsMax,nGrids))
    allocate(PegasusData%dzit(nDonorsMax,nGrids))
    allocate(PegasusData%ibt(nReceiversMax,nGrids))
    allocate(PegasusData%jbt(nReceiversMax,nGrids))
    allocate(PegasusData%kbt(nReceiversMax,nGrids))
    allocate(PegasusData%nit(nDonorsMax,nGrids))
    allocate(PegasusData%njt(nDonorsMax,nGrids))
    allocate(PegasusData%nkt(nDonorsMax,nGrids))
    allocate(PegasusData%coeffit(nDonorsMax,nInterpStencilMax,MAX_ND,nGrids))

    PegasusData%ngrd = nGrids

    PegasusData%ieng = [(Grids(m)%npoints(1),m=1,nGrids)]
    PegasusData%jeng = [(Grids(m)%npoints(2),m=1,nGrids)]
    PegasusData%keng = [(Grids(m)%npoints(3),m=1,nGrids)]

    PegasusData%ipall = sum([(nDonors(m),m=1,nGrids)])
    PegasusData%igall = maxval([(Grids(m)%npoints_total,m=1,nGrids)])

    PegasusData%ipip = nDonorsMax
    PegasusData%ipbp = nReceiversMax

    Offset = 0
    do m = 1, nGrids
      PegasusData%iisptr(m) = Offset + 1
      PegasusData%iieptr(m) = Offset + nDonors(m)
      Offset = Offset + nDonors(m)
    end do

    PegasusData%ibpnts = nReceivers
    PegasusData%iipnts = nDonors

    allocate(NextReceiver(nGrids))
    allocate(NextDonor(nGrids))
    NextReceiver = 1
    NextDonor = 1

    do m = 1, nGrids
      do k = Grids(m)%is(3), Grids(m)%ie(3)
        do j = Grids(m)%is(2), Grids(m)%ie(2)
          do i = Grids(m)%is(1), Grids(m)%ie(1)
            if (InterpData(m)%valid_mask%values(i,j,k)) then
              n = InterpData(m)%donor_grid_ids(i,j,k)
              DonorCell = IndexToTuple(MAX_ND, Grids(n)%is, Grids(n)%ie, &
                InterpData(m)%donor_cells(i,j,k))
              DonorCellCoords(:nDims) = InterpData(m)%donor_cell_coords(:,i,j,k)
              DonorCellCoords(nDims+1:) = 0._rk
              InterpScheme = InterpData(m)%schemes(i,j,k)
              InterpCoefs(:nInterpStencil(m),:nDims) = InterpData(m)%coefs(:,:,i,j,k)
              InterpCoefs(nInterpStencil(m)+1:,:nDims) = 0._rk
              InterpCoefs(1,nDims+1:) = 1._rk
              InterpCoefs(2:,nDims+1:) = 0._rk
              PegasusData%ibct(NextReceiver(m),m) = PegasusData%iisptr(n) + NextDonor(n) - 1
              PegasusData%iit(NextDonor(n),n) = DonorCell(1)
              PegasusData%jit(NextDonor(n),n) = DonorCell(2)
              PegasusData%kit(NextDonor(n),n) = DonorCell(3)
              PegasusData%dxit(NextDonor(n),n) = DonorCellCoords(1)
              PegasusData%dyit(NextDonor(n),n) = DonorCellCoords(2)
              PegasusData%dzit(NextDonor(n),n) = DonorCellCoords(3)
              PegasusData%ibt(NextReceiver(m),m) = i
              PegasusData%jbt(NextReceiver(m),m) = j
              PegasusData%kbt(NextReceiver(m),m) = k
              if (InterpScheme == INTERP_LINEAR) then
                PegasusData%nit(NextDonor(n),n) = 2
                PegasusData%njt(NextDonor(n),n) = 2
                PegasusData%nkt(NextDonor(n),n) = merge(2, 1, nDims == MAX_ND)
              else if (InterpScheme == INTERP_CUBIC) then
                PegasusData%nit(NextDonor(n),n) = 4
                PegasusData%njt(NextDonor(n),n) = 4
                PegasusData%nkt(NextDonor(n),n) = merge(4, 1, nDims == MAX_ND)
              end if
              PegasusData%coeffit(NextDonor(n),:,:,n) = InterpCoefs
              NextReceiver(m) = NextReceiver(m) + 1
              NextDonor(n) = NextDonor(n) + 1
            end if
          end do
        end do
      end do
    end do

  end subroutine MakePegasusData

  subroutine DestroyPegasusData(PegasusData)

    type(t_pegasus), intent(inout) :: PegasusData

    deallocate(PegasusData%ieng)
    deallocate(PegasusData%jeng)
    deallocate(PegasusData%keng)
    deallocate(PegasusData%iisptr)
    deallocate(PegasusData%iieptr)
    deallocate(PegasusData%ibpnts)
    deallocate(PegasusData%iipnts)
    deallocate(PegasusData%ibct)
    deallocate(PegasusData%iit)
    deallocate(PegasusData%jit)
    deallocate(PegasusData%kit)
    deallocate(PegasusData%dxit)
    deallocate(PegasusData%dyit)
    deallocate(PegasusData%dzit)
    deallocate(PegasusData%ibt)
    deallocate(PegasusData%jbt)
    deallocate(PegasusData%kbt)
    deallocate(PegasusData%nit)
    deallocate(PegasusData%njt)
    deallocate(PegasusData%nkt)
    deallocate(PegasusData%coeffit)

  end subroutine DestroyPegasusData

  subroutine WritePegasusData(PegasusData, HOFilename, XFilename)

    type(t_pegasus), intent(in) :: PegasusData
    character(len=*), intent(in) :: HOFilename
    character(len=*), intent(in) :: XFilename

    integer(ik), parameter :: HOUnit = 4510695
    integer(ik), parameter :: XUnit = 4510696

    integer(ik) :: i, l, p

    open (HOUnit, file=HOFilename, status='unknown', form='unformatted')
    write (HOUnit) PegasusData%ngrd, PegasusData%ipall, PegasusData%igall, PegasusData%ipip, &
      PegasusData%ipbp
    do i = 1, PegasusData%ngrd
      write (HOUnit) PegasusData%ibpnts(i), PegasusData%iipnts(i), PegasusData%iieptr(i), &
        PegasusData%iisptr(i), PegasusData%ieng(i), PegasusData%jeng(i), PegasusData%keng(i)
      write (HOUnit) &
        (PegasusData%iit(l,i),l=1,PegasusData%iipnts(i)), &
        (PegasusData%jit(l,i),l=1,PegasusData%iipnts(i)), &
        (PegasusData%kit(l,i),l=1,PegasusData%iipnts(i)), &
        (PegasusData%dxit(l,i),l=1,PegasusData%iipnts(i)), &
        (PegasusData%dyit(l,i),l=1,PegasusData%iipnts(i)), &
        (PegasusData%dzit(l,i),l=1,PegasusData%iipnts(i))
      write (HOUnit) &
        (PegasusData%ibt(l,i),l=1,PegasusData%ibpnts(i)), &
        (PegasusData%jbt(l,i),l=1,PegasusData%ibpnts(i)), &
        (PegasusData%kbt(l,i),l=1,PegasusData%ibpnts(i)), &
        (PegasusData%ibct(l,i),l=1,PegasusData%ibpnts(i))
      write (HOUnit) (1,l=1,PegasusData%ieng(i)*PegasusData%jeng(i)*PegasusData%keng(i))
    end do
    close (HOUnit)

    open (XUnit, file=XFilename, status='unknown', form='unformatted')
    write (XUnit) PegasusData%ngrd, PegasusData%ipall, PegasusData%igall, PegasusData%ipip, &
      PegasusData%ipbp
    do i = 1, PegasusData%ngrd
      write (XUnit) &
        (PegasusData%nit(l,i),l=1,PegasusData%iipnts(i)), &
        (PegasusData%njt(l,i),l=1,PegasusData%iipnts(i)), & 
        (PegasusData%nkt(l,i),l=1,PegasusData%iipnts(i))
      write (XUnit) &
        ((PegasusData%coeffit(l,p,1,i),p=1,PegasusData%nit(l,i)),l=1,PegasusData%iipnts(i)), &
        ((PegasusData%coeffit(l,p,2,i),p=1,PegasusData%njt(l,i)),l=1,PegasusData%iipnts(i)), &
        ((PegasusData%coeffit(l,p,3,i),p=1,PegasusData%nkt(l,i)),l=1,PegasusData%iipnts(i))
      write (XUnit) 0, 0, 0
    end do
    close (XUnit)

  end subroutine WritePegasusData

end module ModPegasus
