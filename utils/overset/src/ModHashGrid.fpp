! Copyright (c) 2015, University of Illinois at Urbana-Champaign, XPACC
! License: MIT, http://opensource.org/licenses/MIT
module ModHashGrid

  use ModBoundingBox
  use ModGlobal
  use ModGrid
  use ModGridUtils
  use ModMask
  implicit none

  private

  public :: t_hashgrid
  public :: t_hashgrid_
  public :: GenerateHashGrid
  public :: DestroyHashGrid
  public :: HashGridBin
  public :: HashGridFind

  type t_hashgrid
    integer(ik) :: nd
    integer(ik), dimension(MAX_ND) :: nbins
    type(t_bbox) :: bounds
    real(rk), dimension(MAX_ND) :: bin_size
    integer(ik), dimension(:), allocatable :: bin_start
    integer(ik), dimension(:), allocatable :: bin_contents
  end type t_hashgrid

  ! Trailing _ added for compatibility with compilers that don't support F2003 constructors
  interface t_hashgrid_
    module procedure t_hashgrid_Default
  end interface t_hashgrid_

contains

  function t_hashgrid_Default(nDims) result(HashGrid)

    integer(ik), intent(in) :: nDims
    type(t_hashgrid) :: HashGrid

    HashGrid%nd = nDims
    HashGrid%nbins(:nDims) = 0
    HashGrid%nbins(nDims+1:) = 1
    HashGrid%bounds = t_bbox_(nDims)
    HashGrid%bin_size = 1._rk

  end function t_hashgrid_Default

  subroutine GenerateHashGrid(Grid, HashGrid, Bounds, BinScale)

    type(t_grid), intent(in) :: Grid
    type(t_hashgrid), intent(out) :: HashGrid
    type(t_bbox), intent(in), optional :: Bounds
    real(rk), intent(in), optional :: BinScale

    type(t_bbox) :: Bounds_
    real(rk) :: BinScale_
    integer(ik) :: i, j, k, l, m, n, o, p
    integer(ik), dimension(MAX_ND) :: iStartCell, iEndCell
    real(rk), dimension(Grid%nd) :: BoundsSize
    real(rk), dimension(Grid%nd) :: AvgCellSize
    integer(ik), dimension(MAX_ND) :: Cell
    integer(ik), dimension(MAX_ND) :: Vertex
    integer(ik), dimension(MAX_ND) :: AdjustedVertex
    real(rk), dimension(Grid%nd) :: PrincipalCoords
    real(rk), dimension(Grid%nd,2**Grid%nd) :: VertexCoords
    type(t_bbox), dimension(:,:,:), allocatable :: GridCellBounds
    type(t_mask) :: GridCellOverlapMask
    integer(ik) :: nOverlappingCells
    integer(ik), dimension(:), allocatable :: nHashBinEntries, NextHashBinEntry
    integer(ik), dimension(MAX_ND) :: HashBinLower, HashBinUpper
    integer(ik), dimension(MAX_ND) :: HashBin

    integer(ik), dimension(MAX_ND), parameter :: One = 1

    if (present(Bounds)) then
      Bounds_ = Bounds
    else
      Bounds_ = BBScale(Grid%bounds, 1.001_rk)
    end if

    if (present(BinScale)) then
      BinScale_ = BinScale
    else
      BinScale_ = 0.5_rk
    end if

    if (any(Grid%is > Grid%ie) .or. .not. BBOverlaps(Bounds_, Grid%bounds)) then
      HashGrid = t_hashgrid_(Grid%nd)
      return
    end if

    iStartCell = Grid%is
    iEndCell(:Grid%nd) = merge(Grid%ie(:Grid%nd)-1, Grid%ie(:Grid%nd), &
      Grid%periodic(:Grid%nd) == FALSE .or. Grid%periodic_storage /= NO_OVERLAP_PERIODIC)
    iEndCell(Grid%nd+1:) = Grid%ie(Grid%nd+1:)

    allocate(GridCellBounds(iStartCell(1):iEndCell(1),iStartCell(2):iEndCell(2), &
      iStartCell(3):iEndCell(3)))

    call MakeMask(GridCellOverlapMask, nDims=Grid%nd, iStart=iStartCell(:Grid%nd), &
      iEnd=iEndCell(:Grid%nd), Periodic=Grid%periodic(:Grid%nd), &
      PeriodicStorage=Grid%periodic_storage)

    AvgCellSize = 0._rk
    nOverlappingCells = 0

    do k = iStartCell(3), iEndCell(3)
      do j = iStartCell(2), iEndCell(2)
        do i = iStartCell(1), iEndCell(1)
          Cell = [i,j,k]
          do m = 1, 2**Grid%nd
            Vertex = [i,j,k] + [(modulo((m-1)/2**n,2),n=0,MAX_ND-1)]
            AdjustedVertex(:Grid%nd) = PeriodicAdjust(Grid%nd, Grid%periodic(:Grid%nd), &
              Grid%periodic_storage, Grid%is(:Grid%nd), Grid%ie(:Grid%nd), Vertex(:Grid%nd))
            l = TupleToIndex(Grid%nd, Grid%is(:Grid%nd), Grid%ie(:Grid%nd), &
              AdjustedVertex(:Grid%nd))
            VertexCoords(:,m) = PeriodicExtend(Grid%nd, Grid%periodic(:Grid%nd), &
              Grid%periodic_storage, Grid%periodic_length(:Grid%nd), Grid%is(:Grid%nd), &
              Grid%ie(:Grid%nd), Vertex(:Grid%nd), Grid%xyz(:,l))
          end do
          GridCellBounds(i,j,k) = BBFromPoints(VertexCoords)
          GridCellOverlapMask%values(i,j,k) = BBOverlaps(Bounds_, GridCellBounds(i,j,k))
          if (GridCellOverlapMask%values(i,j,k)) then
            AvgCellSize = AvgCellSize + BBSize(GridCellBounds(i,j,k))
            nOverlappingCells = nOverlappingCells + 1
          end if
        end do
      end do
    end do

    if (nOverlappingCells == 0) then
      HashGrid = t_hashgrid_(Grid%nd)
      return
    end if

    BoundsSize = BBSize(Bounds_)
    AvgCellSize = AvgCellSize/real(nOverlappingCells, kind=rk)

    HashGrid%nd = Grid%nd
    HashGrid%nbins(:Grid%nd) = max(int(BoundsSize/(BinScale_ * AvgCellSize)), 1)
    HashGrid%nbins(Grid%nd+1:) = 1
    HashGrid%bounds = Bounds_
    HashGrid%bin_size(:Grid%nd) = BoundsSize/real(HashGrid%nbins(:Grid%nd), kind=rk)
    HashGrid%bin_size(Grid%nd+1:) = 1._rk

    allocate(nHashBinEntries(product(HashGrid%nbins)))
    nHashBinEntries = 0

    do k = iStartCell(3), iEndCell(3) 
      do j = iStartCell(2), iEndCell(2)
        do i = iStartCell(1), iEndCell(1) 
          if (GridCellOverlapMask%values(i,j,k)) then
            HashBinLower(:Grid%nd) = HashGridBin(HashGrid, GridCellBounds(i,j,k)%b(:Grid%nd))
            HashBinLower(Grid%nd+1:) = 1
            HashBinLower = min(max(HashBinLower, 1), HashGrid%nbins)
            HashBinUpper(:Grid%nd) = HashGridBin(HashGrid, GridCellBounds(i,j,k)%e(:Grid%nd))
            HashBinUpper(Grid%nd+1:) = 1
            HashBinUpper = min(max(HashBinUpper, 1), HashGrid%nbins)
            do o = HashBinLower(3), HashBinUpper(3)
              do n = HashBinLower(2), HashBinUpper(2)
                do m = HashBinLower(1), HashBinUpper(1)
                  HashBin = [m,n,o]
                  p = TupleToIndex(Grid%nd, One(:Grid%nd), HashGrid%nbins(:Grid%nd), &
                    HashBin(:Grid%nd))
                  nHashBinEntries(p) = nHashBinEntries(p) + 1
                end do
              end do
            end do
          end if
        end do
      end do
    end do

    allocate(HashGrid%bin_start(product(HashGrid%nbins)+1))
    allocate(HashGrid%bin_contents(sum(nHashBinEntries)))

    p = 1
    do k = 1, HashGrid%nbins(3)
      do j = 1, HashGrid%nbins(2)
        do i = 1, HashGrid%nbins(1)
          HashBin = [i,j,k]
          l = TupleToIndex(Grid%nd, One(:Grid%nd), HashGrid%nbins(:Grid%nd), HashBin(:Grid%nd))
          HashGrid%bin_start(l) = p
          p = p + nHashBinEntries(l)
        end do
      end do
    end do
    HashGrid%bin_start(size(HashGrid%bin_start)) = p

    allocate(NextHashBinEntry(product(HashGrid%nbins)))
    NextHashBinEntry = HashGrid%bin_start(:size(HashGrid%bin_start)-1)

    do k = iStartCell(3), iEndCell(3)
      do j = iStartCell(2), iEndCell(2)
        do i = iStartCell(1), iEndCell(1)
          Cell = [i,j,k]
          l = TupleToIndex(Grid%nd, Grid%is(:Grid%nd), Grid%ie(:Grid%nd), Cell(:Grid%nd))
          if (GridCellOverlapMask%values(i,j,k)) then
            HashBinLower(:Grid%nd) = HashGridBin(HashGrid, GridCellBounds(i,j,k)%b(:Grid%nd))
            HashBinLower(Grid%nd+1:) = 1
            HashBinLower = min(max(HashBinLower, 1), HashGrid%nbins)
            HashBinUpper(:Grid%nd) = HashGridBin(HashGrid, GridCellBounds(i,j,k)%e(:Grid%nd))
            HashBinUpper(Grid%nd+1:) = 1
            HashBinUpper = min(max(HashBinUpper, 1), HashGrid%nbins)
            do o = HashBinLower(3), HashBinUpper(3)
              do n = HashBinLower(2), HashBinUpper(2)
                do m = HashBinLower(1), HashBinUpper(1)
                  HashBin = [m,n,o]
                  p = TupleToIndex(Grid%nd, One(:Grid%nd), HashGrid%nbins(:Grid%nd), &
                    HashBin(:Grid%nd))
                  HashGrid%bin_contents(NextHashBinEntry(p)) = l
                  NextHashBinEntry(p) = NextHashBinEntry(p) + 1
                end do
              end do
            end do
          end if
        end do
      end do
    end do

  end subroutine GenerateHashGrid

  subroutine DestroyHashGrid(HashGrid)

    type(t_hashgrid), intent(inout) :: HashGrid

    deallocate(HashGrid%bin_start)
    deallocate(HashGrid%bin_contents)

  end subroutine DestroyHashGrid

  pure function HashGridBin(HashGrid, Coords) result(Bin)

    type(t_hashgrid), intent(in) :: HashGrid
    real(rk), dimension(HashGrid%nd), intent(in) :: Coords
    integer(ik), dimension(HashGrid%nd) :: Bin

    Bin(:HashGrid%nd) = int(floor((Coords-HashGrid%bounds%b(:HashGrid%nd))/ &
      HashGrid%bin_size(:HashGrid%nd))) + 1

  end function HashGridBin

  function HashGridFind(Grid, HashGrid, Coords) result(Cell)

    type(t_grid), intent(in) :: Grid
    type(t_hashgrid), intent(in) :: HashGrid
    real(rk), dimension(Grid%nd), intent(in) :: Coords
    integer(ik), dimension(Grid%nd) :: Cell

    integer(ik), dimension(Grid%nd) :: HashBinTuple
    integer(ik) :: HashBin
    integer(ik) :: HashBinContentsStart, HashBinContentsEnd
    integer(ik) :: i
    logical :: FoundCell

    integer(ik), dimension(MAX_ND), parameter :: One = 1

    if (.not. BBContainsPoint(HashGrid%bounds, Coords)) then
      Cell = Grid%is(:Grid%nd) - 1
      return
    end if

    HashBinTuple = HashGridBin(HashGrid, Coords)
    HashBin = TupleToIndex(Grid%nd, One(:Grid%nd), HashGrid%nbins(:Grid%nd), &
      HashBinTuple)

    HashBinContentsStart = HashGrid%bin_start(HashBin)
    HashBinContentsEnd = HashGrid%bin_start(HashBin+1) - 1

    FoundCell = .false.

    do i = HashBinContentsStart, HashBinContentsEnd
      Cell = IndexToTuple(Grid%nd, Grid%is(:Grid%nd), Grid%ie(:Grid%nd), HashGrid%bin_contents(i))
      if (OverlapsCell(Grid, Cell, Coords)) then
        FoundCell = .true.
        exit
      end if
    end do

    if (.not. FoundCell) then
      Cell = Grid%is(:Grid%nd) - 1
      return
    end if

  end function HashGridFind

end module ModHashGrid
