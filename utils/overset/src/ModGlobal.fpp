! Copyright (c) 2015, University of Illinois at Urbana-Champaign, XPACC
! License: MIT, http://opensource.org/licenses/MIT
module ModGlobal

#ifdef f2003
  use, intrinsic iso_fortran_env, only : INPUT_UNIT, OUTPUT_UNIT, ERROR_UNIT
#endif

  implicit none

  private

  public :: INPUT_UNIT, OUTPUT_UNIT, ERROR_UNIT
  public :: rk, ik
  public :: OVERSET_DEBUG_ENABLED
  public :: OVERSET_VERBOSE_ENABLED
  public :: MAX_ND
  public :: COMPONENTS_FIRST, COMPONENTS_LAST
  public :: FALSE, TRUE
  public :: PLANE_PERIODIC
  public :: NO_OVERLAP_PERIODIC, OVERLAP_PERIODIC
  public :: CASEID_LENGTH
  public :: CASEID

#ifndef f2003
  integer, parameter :: INPUT_UNIT = 5
  integer, parameter :: OUTPUT_UNIT = 6
  integer, parameter :: ERROR_UNIT = 0
#endif

  integer, parameter :: rk = selected_real_kind(15, 307)
  integer, parameter :: ik = selected_int_kind(9)

#ifdef OVERSET_DEBUG
  logical, parameter :: OVERSET_DEBUG_ENABLED = .true.
#else
  logical, parameter :: OVERSET_DEBUG_ENABLED = .false.
#endif

#ifdef OVERSET_VERBOSE
  logical, parameter :: OVERSET_VERBOSE_ENABLED = .true.
#else
  logical, parameter :: OVERSET_VERBOSE_ENABLED = .false.
#endif

  integer(ik), parameter :: MAX_ND = 3

  integer(ik), parameter :: COMPONENTS_FIRST = 1
  integer(ik), parameter :: COMPONENTS_LAST = 2

  integer(ik), parameter :: FALSE = 0
  integer(ik), parameter :: TRUE = 1
  integer(ik), parameter :: PLANE_PERIODIC = 2

  integer(ik), parameter :: NO_OVERLAP_PERIODIC = 0
  integer(ik), parameter :: OVERLAP_PERIODIC = 1

  integer(ik), parameter :: CASEID_LENGTH = 256
  character(len=CASEID_LENGTH) :: CASEID = ""

end module ModGlobal
