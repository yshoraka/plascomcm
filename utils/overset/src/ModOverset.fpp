! Copyright (c) 2015, University of Illinois at Urbana-Champaign, XPACC
! License: MIT, http://opensource.org/licenses/MIT
module ModOverset

  use ModBoundingBox
  use ModDonors
  use ModGeometry
  use ModGlobal
  use ModGrid
  use ModGridUtils
  use ModInterp
  use ModMask
  implicit none

  private

  ! Local
  public :: AssembleOverset
  public :: PartitionReceivers
  public :: OptimizeOverlap
  public :: AssignIBlank
  public :: PrintIBlank

  ! ModBoundingBox
  public :: t_bbox
  public :: t_bbox_
  public :: BBOverlaps
  public :: BBContains
  public :: BBContainsPoint
  public :: BBIsEmpty
  public :: BBSize
  public :: BBMove
  public :: BBGrow
  public :: BBScale
  public :: BBExtend
  public :: BBUnion
  public :: BBIntersect
  public :: BBFromPoints

  ! ModDonors
  public :: t_donors
  public :: t_donors_
  public :: MakeDonors
  public :: DestroyDonors
  public :: FindDonors
  public :: ChooseDonors
  public :: MergeDonors
  public :: GenerateReceiverMask
  public :: GenerateDonorMask
  public :: GenerateOverlapMask
  public :: GenerateCoarseToFineMask
  public :: GenerateCyclicMask
  public :: GenerateOrphanMask

  ! ModGeometry
  public :: OverlapsQuad, OverlapsHexahedron
  public :: CoordsInQuad, CoordsInHexahedron
  public :: QuadSize, HexahedronSize
  public :: Isoparametric2D, Isoparametric3D
  public :: IsoparametricInverse2D, IsoparametricInverse3D
  public :: InterpBasisLinear, InterpBasisLinearDeriv
  public :: InterpBasisCubic, InterpBasisCubicDeriv
  public :: CELL_TYPE_REGULAR, CELL_TYPE_IRREGULAR

  ! ModGlobal
  public :: INPUT_UNIT, OUTPUT_UNIT, ERROR_UNIT
  public :: rk, ik
  public :: OVERSET_DEBUG_ENABLED
  public :: OVERSET_VERBOSE_ENABLED
  public :: MAX_ND
  public :: COMPONENTS_FIRST, COMPONENTS_LAST
  public :: FALSE, TRUE
  public :: PLANE_PERIODIC
  public :: NO_OVERLAP_PERIODIC, OVERLAP_PERIODIC
  public :: CASEID_LENGTH
  public :: CASEID

  ! ModGrid
  public :: t_grid
  public :: t_grid_
  public :: MakeGrid
  public :: DestroyGrid
  public :: OverlapsCell
  public :: CoordsInCell
  public :: CellSize
  public :: AvgCellSizeAroundPoint
  public :: GenerateGridMask
  public :: GenerateBoundaryMask
  public :: GRID_TYPE_CARTESIAN, GRID_TYPE_CARTESIAN_ROTATED, GRID_TYPE_RECTILINEAR, &
    GRID_TYPE_RECTILINEAR_ROTATED, GRID_TYPE_CURVILINEAR

  ! ModInterp
  public :: t_interp
  public :: t_interp_
  public :: MakeInterpData
  public :: DestroyInterpData
  public :: GenerateInterpData
  public :: ExpandDonorCells
!   public :: Interpolate
  public :: INTERP_LINEAR
  public :: INTERP_CUBIC

  ! ModMask
  public :: t_mask
  public :: t_mask_
  public :: MakeMask
  public :: DestroyMask
  public :: GenerateOuterEdgeMask
  public :: GrowMask
  public :: CountMask
  public :: MaskToIBlank
  public :: PrintMask

  ! ModPegasus
!   public :: t_pegasus
!   public :: t_pegasus_
!   public :: MakePegasusData
!   public :: DestroyPegasusData
!   public :: WritePegasusData

contains

  subroutine AssembleOverset(Grids, InterpData, FringeSize, InterpScheme, BoundaryIBlank, &
    OrphanIBlank)

    type(t_grid), dimension(:), intent(inout) :: Grids
    type(t_interp), dimension(:), intent(out) :: InterpData
    integer(ik), intent(in), optional :: FringeSize
    integer(ik), intent(in), optional :: InterpScheme
    integer(ik), intent(in), optional :: BoundaryIBlank
    integer(ik), intent(in), optional :: OrphanIBlank

    integer(ik) :: FringeSize_
    integer(ik) :: InterpScheme_
    integer(ik) :: i, j, k, l, m, n
    integer(ik) :: ClockInitial, ClockFinal, ClockRate
    integer(ik) :: nReceivers
    integer(ik) :: nOuterReceivers, nInnerReceivers
    integer(ik) :: nInvalidatedDonors
    integer(ik) :: nOrphans
    integer(ik) :: nRemovedPoints
    type(t_donors), dimension(:,:), allocatable :: PairwiseDonors
    type(t_donors), dimension(:), allocatable :: Donors
    type(t_mask), dimension(:), allocatable :: OuterReceiverMasks
    type(t_mask), dimension(:), allocatable :: InnerReceiverMasks
    type(t_mask) :: CoarseToFineMask
    type(t_mask) :: CyclicMask1, CyclicMask2
    type(t_mask), dimension(:), allocatable :: OrphanMasks

    if (present(FringeSize)) then
      FringeSize_ = FringeSize
    else
      FringeSize_ = 2
    end if

    if (present(InterpScheme)) then
      InterpScheme_ = InterpScheme
    else
      InterpScheme_ = INTERP_LINEAR
    end if

    if (OVERSET_VERBOSE_ENABLED) then
      call system_clock(ClockInitial, ClockRate)
      write (*, '(a)') "Overset grid assembly started..."
    end if

    if (OVERSET_VERBOSE_ENABLED) then
      write (*, '(a)') "Grid info:"
      write (*, '(a,i0,a)') "* Dimension: ", Grids(1)%nd, "D"
      write (*, '(a,i0)') "* Number of grids: ", size(Grids)
      do m = 1, size(Grids)
        write (*, '(2(a,i0),a)', advance="no") "* Grid ", m, ": "
        write (*, '(i0,a)', advance="no") Grids(m)%npoints_total, " points "
        if (Grids(m)%nd == 3) then
          write (*, '(6(a,i0),a)', advance="no") "(m=", Grids(m)%is(1), ",", Grids(m)%ie(1), ", n=", &
            Grids(m)%is(2), ",", Grids(m)%ie(2), ", k=", Grids(m)%is(3), ",", Grids(m)%ie(3), ")"
        else
          write (*, '(4(a,i0),a)', advance="no") "(m=", Grids(m)%is(1), ",", Grids(m)%ie(1), ", n=", &
            Grids(m)%is(2), ",", Grids(m)%ie(2), ")"
        end if
        write (*, '(a)') ""
      end do
    end if

    do m = 1, size(Grids)
      if (Grids(m)%id /= m) then
        write (ERROR_UNIT, '(2a)') "ERROR: found grid whose ID doesn't match its array index in ", &
          "call to GenerateOversetData"
        stop 1
      end if
    end do

    if (OVERSET_VERBOSE_ENABLED) then
      write (*, '(a)') "Searching for candidate donor/receiver pairs..."
    end if

    allocate(PairwiseDonors(size(Grids),size(Grids)))

    do n = 1, size(Grids)
      do m = 1, size(Grids)
        if (m /= n) then
          call FindDonors(Grids(m), Grids(n), PairwiseDonors(m,n))
          if (OVERSET_VERBOSE_ENABLED) then
            nReceivers = CountMask(PairwiseDonors(m,n)%valid_mask)
            write (*, '(3(a,i0),a)') "* ", nReceivers, &
              " candidate donor/receiver pairs from grid ", m, " to grid ", n, " found."
          end if
        else
          call MakeDonors(PairwiseDonors(m,n), nDims=Grids(n)%nd, &
            iStart=Grids(n)%is(:Grids(n)%nd), iEnd=Grids(n)%ie(:Grids(n)%nd), &
            Periodic=Grids(n)%periodic(:Grids(n)%nd), PeriodicStorage=Grids(n)%periodic_storage)
          PairwiseDonors(m,n)%valid_mask%values = .false.
          PairwiseDonors(m,n)%expanded_mask%values = .false.
        end if
      end do
    end do

    if (OVERSET_VERBOSE_ENABLED) then
      write (*, '(a)') "Finished searching for candidate donor/receiver pairs."
    end if

    if (OVERSET_VERBOSE_ENABLED) then
      write (*, '(a)') "Partitioning receivers..."
    end if

    allocate(OuterReceiverMasks(size(Grids)))
    allocate(InnerReceiverMasks(size(Grids)))

    do m = 1, size(Grids)
      call PartitionReceivers(Grids(m), PairwiseDonors(:,m), OuterReceiverMasks(m), &
        InnerReceiverMasks(m), FringeSize_, BoundaryIBlank=BoundaryIBlank)
      if (OVERSET_VERBOSE_ENABLED) then
        nOuterReceivers = CountMask(OuterReceiverMasks(m))
        nInnerReceivers = CountMask(InnerReceiverMasks(m))
        write (*, '(3(a,i0),a)') "* Partitioned receivers on grid ", m, " into ", &
          nOuterReceivers, " near-boundary points and ", nInnerReceivers, " interior points."
      end if
    end do

    if (OVERSET_VERBOSE_ENABLED) then
      write (*, '(a)') "Finished partitioning receivers."
    end if

    if (OVERSET_VERBOSE_ENABLED) then
      write (*, '(a)') "Invalidating coarse-to-fine donors..."
    end if

    do n = 1, size(Grids)
      do m = 1, size(Grids)
        if (m /= n) then
          call GenerateCoarseToFineMask(Grids(n), PairwiseDonors(m,n), CoarseToFineMask, &
            Subset=InnerReceiverMasks(n))
          PairwiseDonors(m,n)%valid_mask%values = PairwiseDonors(m,n)%valid_mask%values .and. &
            .not. CoarseToFineMask%values
          if (OVERSET_VERBOSE_ENABLED) then
            nInvalidatedDonors = CountMask(CoarseToFineMask)
            write (*, '(3(a,i0),a)') "* ", nInvalidatedDonors, " donors from grid ", m, &
              " to grid ", n, " invalidated."
          end if
        end if
      end do
    end do

    if (OVERSET_VERBOSE_ENABLED) then
      write (*, '(a)') "Finished invalidating coarse-to-fine donors."
    end if

    if (OVERSET_VERBOSE_ENABLED) then
      write (*, '(a)') "Choosing best donor/receiver pairs near grid boundaries..."
    end if

    do n = 1, size(Grids)
      call ChooseDonors(Grids(n), PairwiseDonors(:,n), Subset=OuterReceiverMasks(n))
      do m = 1, size(Grids)
        if (m /= n) then
          call GenerateCyclicMask(Grids(m), Grids(n), PairwiseDonors(n,m), PairwiseDonors(m,n), &
            CyclicMask1, Subset2=OuterReceiverMasks(n))
          PairwiseDonors(n,m)%valid_mask%values = PairwiseDonors(n,m)%valid_mask%values .and. &
            .not. CyclicMask1%values
        end if
      end do
    end do

    if (OVERSET_VERBOSE_ENABLED) then
      write (*, '(a)') "Finished choosing best donor/receiver pairs near grid boundaries."
    end if

    if (OVERSET_VERBOSE_ENABLED) then
      write (*, '(a)') "Invalidating cyclic donors..."
    end if

    do n = 1, size(Grids)
      do m = n+1, size(Grids)
        call GenerateCyclicMask(Grids(m), Grids(n), PairwiseDonors(n,m), PairwiseDonors(m,n), &
          CyclicMask1)
        call GenerateCyclicMask(Grids(n), Grids(m), PairwiseDonors(m,n), PairwiseDonors(n,m), &
          CyclicMask2)
        PairwiseDonors(n,m)%valid_mask%values = PairwiseDonors(n,m)%valid_mask%values .and. .not. &
          CyclicMask1%values
        PairwiseDonors(m,n)%valid_mask%values = PairwiseDonors(m,n)%valid_mask%values .and. .not. &
          CyclicMask2%values
        if (OVERSET_VERBOSE_ENABLED) then
          nInvalidatedDonors = CountMask(CyclicMask1)
          write (*, '(3(a,i0),a)') "* ", nInvalidatedDonors, &
            " donor/receiver pairs from grid ", n, " to ", m, " invalidated."
          nInvalidatedDonors = CountMask(CyclicMask2)
          write (*, '(3(a,i0),a)') "* ", nInvalidatedDonors, &
            " donor/receiver pairs from grid ", m, " to ", n, " invalidated."
        end if
      end do
    end do

    if (OVERSET_VERBOSE_ENABLED) then
      write (*, '(a)') "Finished invalidating cyclic donors."
    end if

    if (OVERSET_VERBOSE_ENABLED) then
      write (*, '(a)') "Choosing best donor/receiver pairs on grid interior..."
    end if

    do n = 1, size(Grids)
      call ChooseDonors(Grids(n), PairwiseDonors(:,n), Subset=InnerReceiverMasks(n))
      do m = 1, size(Grids)
        if (m /= n) then
          call GenerateCyclicMask(Grids(m), Grids(n), PairwiseDonors(n,m), PairwiseDonors(m,n), &
            CyclicMask1, Subset2=InnerReceiverMasks(n))
          PairwiseDonors(n,m)%valid_mask%values = PairwiseDonors(n,m)%valid_mask%values .and. &
            .not. CyclicMask1%values
        end if
      end do
    end do

    if (OVERSET_VERBOSE_ENABLED) then
      write (*, '(a)') "Finished choosing best donor/receiver pairs on grid interior."
    end if

    if (OVERSET_VERBOSE_ENABLED) then
      write (*, '(a)') "Merging remaining candidate donor/receiver pairs into final set..."
    end if

    allocate(Donors(size(Grids)))

    do m = 1, size(Grids)
      call MergeDonors(Grids(m), PairwiseDonors(:,m), Donors(m))
      if (OVERSET_VERBOSE_ENABLED) then
        nReceivers = CountMask(Donors(m)%valid_mask)
        write (*, '(2(a,i0),a)') "* ", nReceivers, " receiver points on grid ", m, "."
      end if
    end do

    deallocate(PairwiseDonors)

    if (OVERSET_VERBOSE_ENABLED) then
      write (*, '(a)') "Finished merging candidate donor/receiver pairs."
    end if

    if (OVERSET_VERBOSE_ENABLED) then
      write (*, '(a)') "Optimizing overlap..."
    end if

    do m = 1, size(Grids)
      if (OVERSET_VERBOSE_ENABLED) then
        nRemovedPoints = CountMask(Donors(m)%valid_mask)
      end if
      call OptimizeOverlap(Grids(m), Donors(m), FringeSize=FringeSize_)
      l = 1
      do k = Grids(m)%is(3), Grids(m)%ie(3)
        do j = Grids(m)%is(2), Grids(m)%ie(2)
          do i = Grids(m)%is(1), Grids(m)%ie(1)
            OuterReceiverMasks(m)%values(i,j,k) = OuterReceiverMasks(m)%values(i,j,k) .and. .not. &
              Grids(m)%iblank(l) == 0
            InnerReceiverMasks(m)%values(i,j,k) = InnerReceiverMasks(m)%values(i,j,k) .and. .not. &
              Grids(m)%iblank(l) == 0
            l = l + 1
          end do
        end do
      end do
      if (OVERSET_VERBOSE_ENABLED) then
        nRemovedPoints = nRemovedPoints - CountMask(Donors(m)%valid_mask)
        write (*, '(2(a,i0),a)') "* ", nRemovedPoints, " points removed from grid ", m, "."
      end if
    end do

    if (OVERSET_VERBOSE_ENABLED) then
      write (*, '(a)') "Finished optimizing overlap."
    end if

    if (OVERSET_VERBOSE_ENABLED) then
      write (*, '(a)') "Searching for orphan points..."
    end if

    allocate(OrphanMasks(size(Grids)))

    do m = 1, size(Grids)
      call GenerateOrphanMask(Grids(m), OuterReceiverMasks(m), Donors(m), OrphanMasks(m))
      if (OVERSET_VERBOSE_ENABLED) then
        do k = Grids(m)%is(3), Grids(m)%ie(3)
          do j = Grids(m)%is(2), Grids(m)%ie(2)
            do i = Grids(m)%is(1), Grids(m)%ie(1)
              if (OrphanMasks(m)%values(i,j,k)) then
                if (Grids(m)%nd == 2) then
                  write (ERROR_UNIT, '(3(a,i0))') "WARNING: orphan detected at point (", &
                    i, ",", j, ") of grid ", m
                else if (Grids(m)%nd == 3) then
                  write (ERROR_UNIT, '(4(a,i0))') "WARNING: orphan detected at point (", &
                    i, ",", j, ",", k, ") of grid ", m
                end if
              end if
            end do
          end do
        end do
        nOrphans = CountMask(OrphanMasks(m))
        write (*, '(2(a,i0),a)') "* ", nOrphans, " orphan points found on grid ", m, "."
      end if
    end do

    deallocate(OuterReceiverMasks)
    deallocate(InnerReceiverMasks)

    if (OVERSET_VERBOSE_ENABLED) then
      write (*, '(a)') "Finished searching for orphan points."
    end if

    if (OVERSET_VERBOSE_ENABLED) then
      write (*, '(a)') "Updating grid IBlank..."
    end if

    do m = 1, size(Grids)
      call AssignIBlank(Grids(m), Donors(m), OrphanIBlank=OrphanIBlank, OrphanMask=OrphanMasks(m))
      if (OVERSET_VERBOSE_ENABLED) then
        write (*, '(a,i0,a)') "* Updated IBlank for grid ", m, "."
      end if
    end do

    if (OVERSET_VERBOSE_ENABLED) then
      write (*, '(a)') "Finished updating grid IBlank."
    end if

    if (OVERSET_VERBOSE_ENABLED) then
      write (*, '(a)') "Generating interpolation data..."
    end if

    do m = 1, size(Grids)

      call ExpandDonorCells(Grids(m), Grids, Donors(m), InterpScheme_)
      call GenerateInterpData(Grids(m), Donors(m), InterpData(m), InterpScheme=InterpScheme_)
      if (OVERSET_VERBOSE_ENABLED) then
        write (*, '(a,i0,a)') "* Generated interpolation data for grid ", m, "."
      end if

    end do

    if (OVERSET_VERBOSE_ENABLED) then
      write (*, '(a)') "Finished generating interpolation data."
    end if

    if (OVERSET_VERBOSE_ENABLED) then
      call system_clock(ClockFinal, ClockRate)
      write (*, '(a,f0.3,a)') "Overset grid assembly finished (time: ", &
        real(ClockFinal-ClockInitial,kind=rk)/real(ClockRate,kind=rk), " seconds)."
    end if

  end subroutine AssembleOverset

  subroutine PartitionReceivers(Grid, CandidateDonors, OuterReceiverMask, InnerReceiverMask, &
    OuterWidth, BoundaryIBlank)

    type(t_grid), intent(in) :: Grid
    type(t_donors), dimension(:), intent(in) :: CandidateDonors
    type(t_mask), intent(out) :: OuterReceiverMask
    type(t_mask), intent(out) :: InnerReceiverMask
    integer(ik), intent(in) :: OuterWidth
    integer(ik), intent(in), optional :: BoundaryIBlank

    type(t_mask) :: GridMask
    type(t_mask) :: GridOuterEdgeMask
    type(t_mask) :: OverlapMask
    type(t_mask) :: OverlapOuterEdgeMask
    type(t_mask) :: BoundaryMask
    type(t_mask) :: BoundaryOuterEdgeMask
    type(t_mask) :: NonBoundaryMask
    type(t_mask) :: NonBoundaryOuterEdgeMask

    call GenerateGridMask(Grid, GridMask)
    call GenerateOuterEdgeMask(GridMask, GridOuterEdgeMask)

    if (present(BoundaryIBlank)) then

      call GenerateBoundaryMask(Grid, BoundaryIBlank, BoundaryMask)

      call MakeMask(NonBoundaryMask, nDims=Grid%nd, iStart=Grid%is(:Grid%nd), &
        iEnd=Grid%ie(:Grid%nd), Periodic=Grid%periodic(:Grid%nd), &
        PeriodicStorage=Grid%periodic_storage)
      NonBoundaryMask%values = .not. BoundaryMask%values

      call GenerateOuterEdgeMask(BoundaryMask, BoundaryOuterEdgeMask)
      call GenerateOuterEdgeMask(NonBoundaryMask, NonBoundaryOuterEdgeMask)

      GridOuterEdgeMask%values = GridOuterEdgeMask%values .and. .not. &
        (BoundaryOuterEdgeMask%values .and. .not. NonBoundaryOuterEdgeMask%values)

    end if

    call GenerateOverlapMask(Grid, CandidateDonors, OverlapMask)
    call GenerateOuterEdgeMask(OverlapMask, OverlapOuterEdgeMask)

    OverlapOuterEdgeMask%values = OverlapOuterEdgeMask%values .and. GridOuterEdgeMask%values

    call GrowMask(OverlapOuterEdgeMask, OuterWidth)

    call MakeMask(OuterReceiverMask, nDims=Grid%nd, iStart=Grid%is(:Grid%nd), &
      iEnd=Grid%ie(:Grid%nd), Periodic=Grid%periodic(:Grid%nd), &
      PeriodicStorage=Grid%periodic_storage)
    OuterReceiverMask%values = OverlapMask%values .and. OverlapOuterEdgeMask%values( &
      Grid%is(1):Grid%ie(1),Grid%is(2):Grid%ie(2),Grid%is(3):Grid%ie(3))

    call MakeMask(InnerReceiverMask, nDims=Grid%nd, iStart=Grid%is(:Grid%nd), &
      iEnd=Grid%ie(:Grid%nd), Periodic=Grid%periodic(:Grid%nd), &
      PeriodicStorage=Grid%periodic_storage)
    InnerReceiverMask%values = OverlapMask%values .and. .not. OverlapOuterEdgeMask%values( &
      Grid%is(1):Grid%ie(1),Grid%is(2):Grid%ie(2),Grid%is(3):Grid%ie(3))

  end subroutine PartitionReceivers

  subroutine OptimizeOverlap(Grid, Donors, FringeSize)

    type(t_grid), intent(inout) :: Grid
    type(t_donors), intent(inout) :: Donors
    integer(ik), intent(in), optional :: FringeSize

    integer(ik) :: FringeSize_
    integer(ik) :: i, j, k, l
    type(t_mask) :: CutMask

    if (present(FringeSize)) then
      FringeSize_ = FringeSize
    else
      FringeSize_ = 2
    end if

    call MakeMask(CutMask, nDims=Grid%nd, iStart=Grid%is(:Grid%nd), iEnd=Grid%ie(:Grid%nd), &
      Periodic=Grid%periodic(:Grid%nd), PeriodicStorage=Grid%periodic_storage)

    l = 1
    do k = Grid%is(3), Grid%ie(3)
      do j = Grid%is(2), Grid%ie(2)
        do i = Grid%is(1), Grid%ie(1)
          CutMask%values(i,j,k) = Grid%iblank(l) == 0 .or. Donors%valid_mask%values(i,j,k)
          l = l + 1
        end do
      end do
    end do

    call GrowMask(CutMask, -FringeSize_)

    Donors%valid_mask%values = Donors%valid_mask%values .and. .not. CutMask%values

    l = 1
    do k = Grid%is(3), Grid%ie(3)
      do j = Grid%is(2), Grid%ie(2)
        do i = Grid%is(1), Grid%ie(1)
          if (CutMask%values(i,j,k)) then
            Grid%iblank(l) = 0
          end if
          l = l + 1
        end do
      end do
    end do

  end subroutine OptimizeOverlap

  subroutine AssignIBlank(Grid, Donors, OrphanIBlank, OrphanMask)

    type(t_grid), intent(inout) :: Grid
    type(t_donors), intent(inout) :: Donors
    integer(ik), intent(in), optional :: OrphanIBlank
    type(t_mask), intent(in), optional :: OrphanMask

    integer(ik) :: i, j, k, l

    if (present(OrphanIBlank) .and. present(OrphanMask)) then

      l = 1
      do k = Grid%is(3), Grid%ie(3)
        do j = Grid%is(2), Grid%ie(2)
          do i = Grid%is(1), Grid%ie(1)
            if (Donors%valid_mask%values(i,j,k)) then
              Grid%iblank(l) = -Donors%grid_ids(i,j,k)
            else if (OrphanMask%values(i,j,k)) then
              Grid%iblank(l) = OrphanIBlank
            end if
            l = l + 1
          end do
        end do
      end do

    else

      l = 1
      do k = Grid%is(3), Grid%ie(3)
        do j = Grid%is(2), Grid%ie(2)
          do i = Grid%is(1), Grid%ie(1)
            if (Donors%valid_mask%values(i,j,k)) then
              Grid%iblank(l) = -Donors%grid_ids(i,j,k)
            end if
            l = l + 1
          end do
        end do
      end do

    end if

  end subroutine AssignIBlank

  subroutine PrintIBlank(IBlank)

    integer(ik), dimension(:,:), intent(in) :: IBlank

    integer(ik) :: j
    integer(ik), dimension(2) :: nPoints
    character(len=80) :: FormatString

    nPoints = shape(IBlank)

    write (FormatString, '(a,i0,a)') '(', nPoints(1), 'i3)'

    do j = nPoints(2), 1, -1
      write (*, trim(FormatString)) IBlank(:,j)
    end do
    write (*, '(a)') ""

  end subroutine PrintIBlank

end module ModOverset
