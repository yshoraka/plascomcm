! Copyright (c) 2015, University of Illinois at Urbana-Champaign, XPACC
! License: MIT, http://opensource.org/licenses/MIT
@MPITest(npes=[1])
subroutine ModGridUtilsTest_tuple_to_index_2d(this)

  use pFUnit_mod
  use ModGridUtils
  use ModGlobal
  implicit none

  class(MPITestCase), intent(inout) :: this

  integer(ik), dimension(2) :: iStart, iEnd
  integer(ik) :: Ind

  iStart = [2,2]
  iEnd = [3,4]

  ! Lower i, lower j corner
  Ind = TupleToIndex(2, iStart, iEnd, [2,2])
  @AssertEqual(1, Ind)

  ! Upper i, lower j corner
  Ind = TupleToIndex(2, iStart, iEnd, [3,2])
  @AssertEqual(2, Ind)

  ! Lower i, upper j corner
  Ind = TupleToIndex(2, iStart, iEnd, [2,4])
  @AssertEqual(5, Ind)
  
  ! Upper i, upper j corner
  Ind = TupleToIndex(2, iStart, iEnd, [3,4])
  @AssertEqual(6, Ind)

end subroutine ModGridUtilsTest_tuple_to_index_2d

@MPITest(npes=[1])
subroutine ModGridUtilsTest_tuple_to_index_3d(this)

  use pFUnit_mod
  use ModGridUtils
  use ModGlobal
  implicit none

  class(MPITestCase), intent(inout) :: this

  integer(ik), dimension(3) :: iStart, iEnd
  integer(ik) :: Ind

  iStart = [2,2,2]
  iEnd = [3,4,5]

  ! Lower i, lower j, lower k corner
  Ind = TupleToIndex(3, iStart, iEnd, [2,2,2])
  @AssertEqual(1, Ind)

  ! Upper i, lower j, lower k corner
  Ind = TupleToIndex(3, iStart, iEnd, [3,2,2])
  @AssertEqual(2, Ind)

  ! Lower i, upper j, lower k corner
  Ind = TupleToIndex(3, iStart, iEnd, [2,4,2])
  @AssertEqual(5, Ind)
  
  ! Upper i, upper j, lower k corner
  Ind = TupleToIndex(3, iStart, iEnd, [3,4,2])
  @AssertEqual(6, Ind)

  ! Lower i, lower j, upper k corner
  Ind = TupleToIndex(3, iStart, iEnd, [2,2,5])
  @AssertEqual(19, Ind)

  ! Upper i, lower j, upper k corner
  Ind = TupleToIndex(3, iStart, iEnd, [3,2,5])
  @AssertEqual(20, Ind)

  ! Lower i, upper j, upper k corner
  Ind = TupleToIndex(3, iStart, iEnd, [2,4,5])
  @AssertEqual(23, Ind)
  
  ! Upper i, upper j, upper k corner
  Ind = TupleToIndex(3, iStart, iEnd, [3,4,5])
  @AssertEqual(24, Ind)

end subroutine ModGridUtilsTest_tuple_to_index_3d

@MPITest(npes=[1])
subroutine ModGridUtilsTest_index_to_tuple_2d(this)

  use pFUnit_mod
  use ModGridUtils
  use ModGlobal
  implicit none

  class(MPITestCase), intent(inout) :: this

  integer(ik), dimension(2) :: iStart, iEnd
  integer(ik), dimension(2) :: Tuple

  iStart = [2,2]
  iEnd = [3,4]

  ! Lower i, lower j corner
  Tuple = IndexToTuple(2, iStart, iEnd, 1)
  @AssertEqual([2,2], Tuple)

  ! Upper i, lower j corner
  Tuple = IndexToTuple(2, iStart, iEnd, 2)
  @AssertEqual([3,2], Tuple)

  ! Lower i, upper j corner
  Tuple = IndexToTuple(2, iStart, iEnd, 5)
  @AssertEqual([2,4], Tuple)
  
  ! Upper i, upper j corner
  Tuple = IndexToTuple(2, iStart, iEnd, 6)
  @AssertEqual([3,4], Tuple)

end subroutine ModGridUtilsTest_index_to_tuple_2d

@MPITest(npes=[1])
subroutine ModGridUtilsTest_index_to_tuple_3d(this)

  use pFUnit_mod
  use ModGridUtils
  use ModGlobal
  implicit none

  class(MPITestCase), intent(inout) :: this

  integer(ik), dimension(3) :: iStart, iEnd
  integer(ik), dimension(3) :: Tuple

  iStart = [2,2,2]
  iEnd = [3,4,5]

  ! Lower i, lower j, lower k corner
  Tuple = IndexToTuple(3, iStart, iEnd, 1)
  @AssertEqual([2,2,2], Tuple)

  ! Upper i, lower j, lower k corner
  Tuple = IndexToTuple(3, iStart, iEnd, 2)
  @AssertEqual([3,2,2], Tuple)

  ! Lower i, upper j, lower k corner
  Tuple = IndexToTuple(3, iStart, iEnd, 5)
  @AssertEqual([2,4,2], Tuple)
  
  ! Upper i, upper j, lower k corner
  Tuple = IndexToTuple(3, iStart, iEnd, 6)
  @AssertEqual([3,4,2], Tuple)

  ! Lower i, lower j, upper k corner
  Tuple = IndexToTuple(3, iStart, iEnd, 19)
  @AssertEqual([2,2,5], Tuple)

  ! Upper i, lower j, upper k corner
  Tuple = IndexToTuple(3, iStart, iEnd, 20)
  @AssertEqual([3,2,5], Tuple)

  ! Lower i, upper j, upper k corner
  Tuple = IndexToTuple(3, iStart, iEnd, 23)
  @AssertEqual([2,4,5], Tuple)
  
  ! Upper i, upper j, upper k corner
  Tuple = IndexToTuple(3, iStart, iEnd, 24)
  @AssertEqual([3,4,5], Tuple)

end subroutine ModGridUtilsTest_index_to_tuple_3d

@MPITest(npes=[1])
subroutine ModGridUtilsTest_periodic_adjust_non_periodic_2d(this)

  use pFUnit_mod
  use ModGridUtils
  use ModGlobal
  implicit none

  class(MPITestCase), intent(inout) :: this

  integer(ik), dimension(2) :: Tuple
  integer(ik), dimension(2) :: Periodic
  integer(ik) :: PeriodicStorage
  integer(ik), dimension(2) :: iStart, iEnd

  Periodic = [FALSE,FALSE]
  PeriodicStorage = OVERLAP_PERIODIC
  iStart = [1,1]
  iEnd = [5,5]

  ! Lower x boundary
  Tuple = PeriodicAdjust(2, Periodic, PeriodicStorage, iStart, iEnd, [1,3])
  @AssertEqual([1,3], Tuple)

  ! Upper x boundary
  Tuple = PeriodicAdjust(2, Periodic, PeriodicStorage, iStart, iEnd, [5,3])
  @AssertEqual([5,3], Tuple)

  ! Lower y boundary
  Tuple = PeriodicAdjust(2, Periodic, PeriodicStorage, iStart, iEnd, [3,1])
  @AssertEqual([3,1], Tuple)

  ! Upper y boundary
  Tuple = PeriodicAdjust(2, Periodic, PeriodicStorage, iStart, iEnd, [3,5])
  @AssertEqual([3,5], Tuple)

end subroutine ModGridUtilsTest_periodic_adjust_non_periodic_2d

@MPITest(npes=[1])
subroutine ModGridUtilsTest_periodic_adjust_non_periodic_3d(this)

  use pFUnit_mod
  use ModGridUtils
  use ModGlobal
  implicit none

  class(MPITestCase), intent(inout) :: this

  integer(ik), dimension(3) :: Tuple
  integer(ik), dimension(3) :: Periodic
  integer(ik) :: PeriodicStorage
  integer(ik), dimension(3) :: iStart, iEnd

  Periodic = [FALSE,FALSE,FALSE]
  PeriodicStorage = OVERLAP_PERIODIC
  iStart = [1,1,1]
  iEnd = [5,5,5]

  ! Lower x boundary
  Tuple = PeriodicAdjust(3, Periodic, PeriodicStorage, iStart, iEnd, [1,3,3])
  @AssertEqual([1,3,3], Tuple)

  ! Upper x boundary
  Tuple = PeriodicAdjust(3, Periodic, PeriodicStorage, iStart, iEnd, [5,3,3])
  @AssertEqual([5,3,3], Tuple)

  ! Lower y boundary
  Tuple = PeriodicAdjust(3, Periodic, PeriodicStorage, iStart, iEnd, [3,1,3])
  @AssertEqual([3,1,3], Tuple)

  ! Upper y boundary
  Tuple = PeriodicAdjust(3, Periodic, PeriodicStorage, iStart, iEnd, [3,5,3])
  @AssertEqual([3,5,3], Tuple)

  ! Lower z boundary
  Tuple = PeriodicAdjust(3, Periodic, PeriodicStorage, iStart, iEnd, [3,3,1])
  @AssertEqual([3,3,1], Tuple)

  ! Upper z boundary
  Tuple = PeriodicAdjust(3, Periodic, PeriodicStorage, iStart, iEnd, [3,3,5])
  @AssertEqual([3,3,5], Tuple)

end subroutine ModGridUtilsTest_periodic_adjust_non_periodic_3d

@MPITest(npes=[1])
subroutine ModGridUtilsTest_periodic_adjust_overlap_2d(this)

  use pFUnit_mod
  use ModGridUtils
  use ModGlobal
  implicit none

  class(MPITestCase), intent(inout) :: this

  integer(ik), dimension(2) :: Tuple
  integer(ik), dimension(2) :: PeriodicX, PeriodicY
  integer(ik) :: PeriodicStorage
  integer(ik), dimension(2) :: iStart, iEnd

  PeriodicX = [TRUE,FALSE]
  PeriodicY = [FALSE,TRUE]
  PeriodicStorage = OVERLAP_PERIODIC
  iStart = [1,1]
  iEnd = [5,5]

  ! Lower x boundary
  Tuple = PeriodicAdjust(2, PeriodicX, PeriodicStorage, iStart, iEnd, [1,3])
  @AssertEqual([1,3], Tuple)

  ! Upper x boundary
  Tuple = PeriodicAdjust(2, PeriodicX, PeriodicStorage, iStart, iEnd, [5,3])
  @AssertEqual([1,3], Tuple)

  ! Lower y boundary
  Tuple = PeriodicAdjust(2, PeriodicY, PeriodicStorage, iStart, iEnd, [3,1])
  @AssertEqual([3,1], Tuple)

  ! Upper y boundary
  Tuple = PeriodicAdjust(2, PeriodicY, PeriodicStorage, iStart, iEnd, [3,5])
  @AssertEqual([3,1], Tuple)

  ! Lower x off-grid
  Tuple = PeriodicAdjust(2, PeriodicX, PeriodicStorage, iStart, iEnd, [0,3])
  @AssertEqual([4,3], Tuple)

  ! Upper x off-grid
  Tuple = PeriodicAdjust(2, PeriodicX, PeriodicStorage, iStart, iEnd, [6,3])
  @AssertEqual([2,3], Tuple)

  ! Lower y off-grid
  Tuple = PeriodicAdjust(2, PeriodicY, PeriodicStorage, iStart, iEnd, [3,0])
  @AssertEqual([3,4], Tuple)

  ! Upper y off-grid
  Tuple = PeriodicAdjust(2, PeriodicY, PeriodicStorage, iStart, iEnd, [3,6])
  @AssertEqual([3,2], Tuple)

end subroutine ModGridUtilsTest_periodic_adjust_overlap_2d

@MPITest(npes=[1])
subroutine ModGridUtilsTest_periodic_adjust_overlap_3d(this)

  use pFUnit_mod
  use ModGridUtils
  use ModGlobal
  implicit none

  class(MPITestCase), intent(inout) :: this

  integer(ik), dimension(3) :: Tuple
  integer(ik), dimension(3) :: PeriodicX, PeriodicY, PeriodicZ
  integer(ik) :: PeriodicStorage
  integer(ik), dimension(3) :: iStart, iEnd

  PeriodicX = [TRUE,FALSE,FALSE]
  PeriodicY = [FALSE,TRUE,FALSE]
  PeriodicZ = [FALSE,FALSE,TRUE]
  PeriodicStorage = OVERLAP_PERIODIC
  iStart = [1,1,1]
  iEnd = [5,5,5]

  ! Lower x boundary
  Tuple = PeriodicAdjust(3, PeriodicX, PeriodicStorage, iStart, iEnd, [1,3,3])
  @AssertEqual([1,3,3], Tuple)

  ! Upper x boundary
  Tuple = PeriodicAdjust(3, PeriodicX, PeriodicStorage, iStart, iEnd, [5,3,3])
  @AssertEqual([1,3,3], Tuple)

  ! Lower y boundary
  Tuple = PeriodicAdjust(3, PeriodicY, PeriodicStorage, iStart, iEnd, [3,1,3])
  @AssertEqual([3,1,3], Tuple)

  ! Upper y boundary
  Tuple = PeriodicAdjust(3, PeriodicY, PeriodicStorage, iStart, iEnd, [3,5,3])
  @AssertEqual([3,1,3], Tuple)

  ! Lower z boundary
  Tuple = PeriodicAdjust(3, PeriodicZ, PeriodicStorage, iStart, iEnd, [3,3,1])
  @AssertEqual([3,3,1], Tuple)

  ! Upper z boundary
  Tuple = PeriodicAdjust(3, PeriodicZ, PeriodicStorage, iStart, iEnd, [3,3,5])
  @AssertEqual([3,3,1], Tuple)

  ! Lower x off-grid
  Tuple = PeriodicAdjust(3, PeriodicX, PeriodicStorage, iStart, iEnd, [0,3,3])
  @AssertEqual([4,3,3], Tuple)

  ! Upper x off-grid
  Tuple = PeriodicAdjust(3, PeriodicX, PeriodicStorage, iStart, iEnd, [6,3,3])
  @AssertEqual([2,3,3], Tuple)

  ! Lower y off-grid
  Tuple = PeriodicAdjust(3, PeriodicY, PeriodicStorage, iStart, iEnd, [3,0,3])
  @AssertEqual([3,4,3], Tuple)

  ! Upper y off-grid
  Tuple = PeriodicAdjust(3, PeriodicY, PeriodicStorage, iStart, iEnd, [3,6,3])
  @AssertEqual([3,2,3], Tuple)

  ! Lower z off-grid
  Tuple = PeriodicAdjust(3, PeriodicZ, PeriodicStorage, iStart, iEnd, [3,3,0])
  @AssertEqual([3,3,4], Tuple)

  ! Upper z off-grid
  Tuple = PeriodicAdjust(3, PeriodicZ, PeriodicStorage, iStart, iEnd, [3,3,6])
  @AssertEqual([3,3,2], Tuple)

end subroutine ModGridUtilsTest_periodic_adjust_overlap_3d

@MPITest(npes=[1])
subroutine ModGridUtilsTest_periodic_adjust_no_overlap_2d(this)

  use pFUnit_mod
  use ModGridUtils
  use ModGlobal
  implicit none

  class(MPITestCase), intent(inout) :: this

  integer(ik), dimension(2) :: Tuple
  integer(ik), dimension(2) :: PeriodicX, PeriodicY
  integer(ik) :: PeriodicStorage
  integer(ik), dimension(2) :: iStart, iEnd

  PeriodicX = [TRUE,FALSE]
  PeriodicY = [FALSE,TRUE]
  PeriodicStorage = NO_OVERLAP_PERIODIC
  iStart = [1,1]
  iEnd = [4,4]

  ! Lower x boundary
  Tuple = PeriodicAdjust(2, PeriodicX, PeriodicStorage, iStart, iEnd, [1,3])
  @AssertEqual([1,3], Tuple)

  ! Upper x boundary
  Tuple = PeriodicAdjust(2, PeriodicX, PeriodicStorage, iStart, iEnd, [5,3])
  @AssertEqual([1,3], Tuple)

  ! Lower y boundary
  Tuple = PeriodicAdjust(2, PeriodicY, PeriodicStorage, iStart, iEnd, [3,1])
  @AssertEqual([3,1], Tuple)

  ! Upper y boundary
  Tuple = PeriodicAdjust(2, PeriodicY, PeriodicStorage, iStart, iEnd, [3,5])
  @AssertEqual([3,1], Tuple)

  ! Lower x off-grid
  Tuple = PeriodicAdjust(2, PeriodicX, PeriodicStorage, iStart, iEnd, [0,3])
  @AssertEqual([4,3], Tuple)

  ! Upper x off-grid
  Tuple = PeriodicAdjust(2, PeriodicX, PeriodicStorage, iStart, iEnd, [6,3])
  @AssertEqual([2,3], Tuple)

  ! Lower y off-grid
  Tuple = PeriodicAdjust(2, PeriodicY, PeriodicStorage, iStart, iEnd, [3,0])
  @AssertEqual([3,4], Tuple)

  ! Upper y off-grid
  Tuple = PeriodicAdjust(2, PeriodicY, PeriodicStorage, iStart, iEnd, [3,6])
  @AssertEqual([3,2], Tuple)

end subroutine ModGridUtilsTest_periodic_adjust_no_overlap_2d

@MPITest(npes=[1])
subroutine ModGridUtilsTest_periodic_adjust_no_overlap_3d(this)

  use pFUnit_mod
  use ModGridUtils
  use ModGlobal
  implicit none

  class(MPITestCase), intent(inout) :: this

  integer(ik), dimension(3) :: Tuple
  integer(ik), dimension(3) :: PeriodicX, PeriodicY, PeriodicZ
  integer(ik) :: PeriodicStorage
  integer(ik), dimension(3) :: iStart, iEnd

  PeriodicX = [TRUE,FALSE,FALSE]
  PeriodicY = [FALSE,TRUE,FALSE]
  PeriodicZ = [FALSE,FALSE,TRUE]
  PeriodicStorage = NO_OVERLAP_PERIODIC
  iStart = [1,1,1]
  iEnd = [4,4,4]

  ! Lower x boundary
  Tuple = PeriodicAdjust(3, PeriodicX, PeriodicStorage, iStart, iEnd, [1,3,3])
  @AssertEqual([1,3,3], Tuple)

  ! Upper x boundary
  Tuple = PeriodicAdjust(3, PeriodicX, PeriodicStorage, iStart, iEnd, [5,3,3])
  @AssertEqual([1,3,3], Tuple)

  ! Lower y boundary
  Tuple = PeriodicAdjust(3, PeriodicY, PeriodicStorage, iStart, iEnd, [3,1,3])
  @AssertEqual([3,1,3], Tuple)

  ! Upper y boundary
  Tuple = PeriodicAdjust(3, PeriodicY, PeriodicStorage, iStart, iEnd, [3,5,3])
  @AssertEqual([3,1,3], Tuple)

  ! Lower z boundary
  Tuple = PeriodicAdjust(3, PeriodicZ, PeriodicStorage, iStart, iEnd, [3,3,1])
  @AssertEqual([3,3,1], Tuple)

  ! Upper z boundary
  Tuple = PeriodicAdjust(3, PeriodicZ, PeriodicStorage, iStart, iEnd, [3,3,5])
  @AssertEqual([3,3,1], Tuple)

  ! Lower x off-grid
  Tuple = PeriodicAdjust(3, PeriodicX, PeriodicStorage, iStart, iEnd, [0,3,3])
  @AssertEqual([4,3,3], Tuple)

  ! Upper x off-grid
  Tuple = PeriodicAdjust(3, PeriodicX, PeriodicStorage, iStart, iEnd, [6,3,3])
  @AssertEqual([2,3,3], Tuple)

  ! Lower y off-grid
  Tuple = PeriodicAdjust(3, PeriodicY, PeriodicStorage, iStart, iEnd, [3,0,3])
  @AssertEqual([3,4,3], Tuple)

  ! Upper y off-grid
  Tuple = PeriodicAdjust(3, PeriodicY, PeriodicStorage, iStart, iEnd, [3,6,3])
  @AssertEqual([3,2,3], Tuple)

  ! Lower z off-grid
  Tuple = PeriodicAdjust(3, PeriodicZ, PeriodicStorage, iStart, iEnd, [3,3,0])
  @AssertEqual([3,3,4], Tuple)

  ! Upper z off-grid
  Tuple = PeriodicAdjust(3, PeriodicZ, PeriodicStorage, iStart, iEnd, [3,3,6])
  @AssertEqual([3,3,2], Tuple)

end subroutine ModGridUtilsTest_periodic_adjust_no_overlap_3d

@MPITest(npes=[1])
subroutine ModGridUtilsTest_periodic_extend_non_periodic_2d(this)

  use pFUnit_mod
  use ModGridUtils
  use ModGlobal
  implicit none

  class(MPITestCase), intent(inout) :: this

  real(rk), dimension(2) :: Coords
  integer(ik), dimension(2) :: Periodic
  integer(ik) :: PeriodicStorage
  real(rk), dimension(2) :: PeriodicLength
  integer(ik), dimension(2) :: iStart, iEnd

  Periodic = [FALSE,FALSE]
  PeriodicStorage = OVERLAP_PERIODIC
  PeriodicLength = 4._rk
  iStart = [1,1]
  iEnd = [5,5]

  ! Lower x boundary
  Coords = PeriodicExtend(2, Periodic, PeriodicStorage, PeriodicLength, iStart, iEnd, [1,3], &
    [1._rk, 3._rk])
  @AssertEqual([1._rk, 3._rk], Coords)

  ! Upper x boundary
  Coords = PeriodicExtend(2, Periodic, PeriodicStorage, PeriodicLength, iStart, iEnd, [5,3], &
    [5._rk, 3._rk])
  @AssertEqual([5._rk, 3._rk], Coords)

  ! Lower y boundary
  Coords = PeriodicExtend(2, Periodic, PeriodicStorage, PeriodicLength, iStart, iEnd, [3,1], &
    [3._rk, 1._rk])
  @AssertEqual([3._rk, 1._rk], Coords)

  ! Upper y boundary
  Coords = PeriodicExtend(2, Periodic, PeriodicStorage, PeriodicLength, iStart, iEnd, [3,5], &
    [3._rk, 5._rk])
  @AssertEqual([3._rk, 5._rk], Coords)

end subroutine ModGridUtilsTest_periodic_extend_non_periodic_2d

@MPITest(npes=[1])
subroutine ModGridUtilsTest_periodic_extend_overlap_periodic_2d(this)

  use pFUnit_mod
  use ModGridUtils
  use ModGlobal
  implicit none

  class(MPITestCase), intent(inout) :: this

  integer(ik), dimension(2) :: PeriodicX, PeriodicY
  real(rk), dimension(2) :: Coords
  integer(ik) :: PeriodicStorage
  real(rk), dimension(2) :: PeriodicLength
  integer(ik), dimension(2) :: iStart, iEnd

  PeriodicX = [PLANE_PERIODIC,FALSE]
  PeriodicY = [FALSE,PLANE_PERIODIC]
  PeriodicStorage = OVERLAP_PERIODIC
  PeriodicLength = 4._rk
  iStart = [1,1]
  iEnd = [5,5]

  ! Lower x boundary
  Coords = PeriodicExtend(2, PeriodicX, PeriodicStorage, PeriodicLength, iStart, iEnd, [1,3], &
    [1._rk, 3._rk])
  @AssertEqual([1._rk, 3._rk], Coords)

  ! Upper x boundary
  Coords = PeriodicExtend(2, PeriodicX, PeriodicStorage, PeriodicLength, iStart, iEnd, [5,3], &
    [1._rk, 3._rk])
  @AssertEqual([5._rk, 3._rk], Coords)

  ! Lower y boundary
  Coords = PeriodicExtend(2, PeriodicY, PeriodicStorage, PeriodicLength, iStart, iEnd, [3,1], &
    [3._rk, 1._rk])
  @AssertEqual([3._rk, 1._rk], Coords)

  ! Upper y boundary
  Coords = PeriodicExtend(2, PeriodicY, PeriodicStorage, PeriodicLength, iStart, iEnd, [3,5], &
    [3._rk, 1._rk])
  @AssertEqual([3._rk, 5._rk], Coords)

  ! Lower x off-grid
  Coords = PeriodicExtend(2, PeriodicX, PeriodicStorage, PeriodicLength, iStart, iEnd, [0,3], &
    [4._rk, 3._rk])
  @AssertEqual([0._rk, 3._rk], Coords)

  ! Upper x off-grid
  Coords = PeriodicExtend(2, PeriodicX, PeriodicStorage, PeriodicLength, iStart, iEnd, [6,3], &
    [2._rk, 3._rk])
  @AssertEqual([6._rk, 3._rk], Coords)

  ! Lower y off-grid
  Coords = PeriodicExtend(2, PeriodicY, PeriodicStorage, PeriodicLength, iStart, iEnd, [3,0], &
    [3._rk, 4._rk])
  @AssertEqual([3._rk, 0._rk], Coords)

  ! Upper y off-grid
  Coords = PeriodicExtend(2, PeriodicY, PeriodicStorage, PeriodicLength, iStart, iEnd, [3,6], &
    [3._rk, 2._rk])
  @AssertEqual([3._rk, 6._rk], Coords)

end subroutine ModGridUtilsTest_periodic_extend_overlap_periodic_2d

@MPITest(npes=[1])
subroutine ModGridUtilsTest_periodic_extend_no_overlap_periodic_2d(this)

  use pFUnit_mod
  use ModGridUtils
  use ModGlobal
  implicit none

  class(MPITestCase), intent(inout) :: this

  real(rk), dimension(2) :: Coords
  integer(ik), dimension(2) :: PeriodicX, PeriodicY
  integer(ik) :: PeriodicStorage
  real(rk), dimension(2) :: PeriodicLength
  integer(ik), dimension(2) :: iStart, iEnd

  PeriodicX = [PLANE_PERIODIC,FALSE]
  PeriodicY = [FALSE,PLANE_PERIODIC]
  PeriodicStorage = NO_OVERLAP_PERIODIC
  PeriodicLength = 4._rk
  iStart = [1,1]
  iEnd = [4,4]

  ! Lower x boundary
  Coords = PeriodicExtend(2, PeriodicX, PeriodicStorage, PeriodicLength, iStart, iEnd, &
    [1,3], [1._rk, 3._rk])
  @AssertEqual([1._rk, 3._rk], Coords)

  ! Upper x boundary
  Coords = PeriodicExtend(2, PeriodicX, PeriodicStorage, PeriodicLength, iStart, iEnd, &
    [5,3], [1._rk, 3._rk])
  @AssertEqual([5._rk, 3._rk], Coords)

  ! Lower y boundary
  Coords = PeriodicExtend(2, PeriodicY, PeriodicStorage, PeriodicLength, iStart, iEnd, &
    [3,1], [3._rk, 1._rk])
  @AssertEqual([3._rk, 1._rk], Coords)

  ! Upper y boundary
  Coords = PeriodicExtend(2, PeriodicY, PeriodicStorage, PeriodicLength, iStart, iEnd, &
    [3,5], [3._rk, 1._rk])
  @AssertEqual([3._rk, 5._rk], Coords)

  ! Lower x off-grid
  Coords = PeriodicExtend(2, PeriodicX, PeriodicStorage, PeriodicLength, iStart, iEnd, &
    [0,3], [4._rk, 3._rk])
  @AssertEqual([0._rk, 3._rk], Coords)

  ! Upper x off-grid
  Coords = PeriodicExtend(2, PeriodicX, PeriodicStorage, PeriodicLength, iStart, iEnd, &
    [6,3], [2._rk, 3._rk])
  @AssertEqual([6._rk, 3._rk], Coords)

  ! Lower y off-grid
  Coords = PeriodicExtend(2, PeriodicY, PeriodicStorage, PeriodicLength, iStart, iEnd, &
    [3,0], [3._rk, 4._rk])
  @AssertEqual([3._rk, 0._rk], Coords)

  ! Upper y off-grid
  Coords = PeriodicExtend(2, PeriodicY, PeriodicStorage, PeriodicLength, iStart, iEnd, &
    [3,6], [3._rk, 2._rk])
  @AssertEqual([3._rk, 6._rk], Coords)

end subroutine ModGridUtilsTest_periodic_extend_no_overlap_periodic_2d

@MPITest(npes=[1])
subroutine ModGridUtilsTest_periodic_extend_non_periodic_3d(this)

  use pFUnit_mod
  use ModGridUtils
  use ModGlobal
  implicit none

  class(MPITestCase), intent(inout) :: this

  real(rk), dimension(3) :: Coords
  integer(ik), dimension(3) :: Periodic
  integer(ik) :: PeriodicStorage
  real(rk), dimension(3) :: PeriodicLength
  integer(ik), dimension(3) :: iStart, iEnd

  Periodic = [FALSE,FALSE,FALSE]
  PeriodicStorage = OVERLAP_PERIODIC
  PeriodicLength = 4._rk
  iStart = [1,1,1]
  iEnd = [5,5,5]

  ! Lower x boundary
  Coords = PeriodicExtend(3, Periodic, PeriodicStorage, PeriodicLength, iStart, iEnd, [1,3,3], &
    [1._rk, 3._rk, 3._rk])
  @AssertEqual([1._rk, 3._rk, 3._rk], Coords)

  ! Upper x boundary
  Coords = PeriodicExtend(3, Periodic, PeriodicStorage, PeriodicLength, iStart, iEnd, [5,3,3], &
    [5._rk, 3._rk, 3._rk])
  @AssertEqual([5._rk, 3._rk, 3._rk], Coords)

  ! Lower y boundary
  Coords = PeriodicExtend(3, Periodic, PeriodicStorage, PeriodicLength, iStart, iEnd, [3,1,3], &
    [3._rk, 1._rk, 3._rk])
  @AssertEqual([3._rk, 1._rk, 3._rk], Coords)

  ! Upper y boundary
  Coords = PeriodicExtend(3, Periodic, PeriodicStorage, PeriodicLength, iStart, iEnd, [3,5,3], &
    [3._rk, 5._rk, 3._rk])
  @AssertEqual([3._rk, 5._rk, 3._rk], Coords)

  ! Lower z boundary
  Coords = PeriodicExtend(3, Periodic, PeriodicStorage, PeriodicLength, iStart, iEnd, [3,3,1], &
    [3._rk, 3._rk, 1._rk])
  @AssertEqual([3._rk, 3._rk, 1._rk], Coords)

  ! Upper z boundary
  Coords = PeriodicExtend(3, Periodic, PeriodicStorage, PeriodicLength, iStart, iEnd, [3,3,5], &
    [3._rk, 3._rk, 5._rk])
  @AssertEqual([3._rk, 3._rk, 5._rk], Coords)

end subroutine ModGridUtilsTest_periodic_extend_non_periodic_3d

@MPITest(npes=[1])
subroutine ModGridUtilsTest_periodic_extend_overlap_periodic_3d(this)

  use pFUnit_mod
  use ModGridUtils
  use ModGlobal
  implicit none

  class(MPITestCase), intent(inout) :: this

  integer(ik), dimension(3) :: PeriodicX, PeriodicY, PeriodicZ
  real(rk), dimension(3) :: Coords
  integer(ik) :: PeriodicStorage
  real(rk), dimension(3) :: PeriodicLength
  integer(ik), dimension(3) :: iStart, iEnd

  PeriodicX = [PLANE_PERIODIC,FALSE,FALSE]
  PeriodicY = [FALSE,PLANE_PERIODIC,FALSE]
  PeriodicZ = [FALSE,FALSE,PLANE_PERIODIC]
  PeriodicStorage = OVERLAP_PERIODIC
  PeriodicLength = 4._rk
  iStart = [1,1,1]
  iEnd = [5,5,5]

  ! Lower x boundary
  Coords = PeriodicExtend(3, PeriodicX, PeriodicStorage, PeriodicLength, iStart, iEnd, [1,3,3], &
    [1._rk, 3._rk, 3._rk])
  @AssertEqual([1._rk, 3._rk, 3._rk], Coords)

  ! Upper x boundary
  Coords = PeriodicExtend(3, PeriodicX, PeriodicStorage, PeriodicLength, iStart, iEnd, [5,3,3], &
    [1._rk, 3._rk, 3._rk])
  @AssertEqual([5._rk, 3._rk, 3._rk], Coords)

  ! Lower y boundary
  Coords = PeriodicExtend(3, PeriodicY, PeriodicStorage, PeriodicLength, iStart, iEnd, [3,1,3], &
    [3._rk, 1._rk, 3._rk])
  @AssertEqual([3._rk, 1._rk, 3._rk], Coords)

  ! Upper y boundary
  Coords = PeriodicExtend(3, PeriodicY, PeriodicStorage, PeriodicLength, iStart, iEnd, [3,5,3], &
    [3._rk, 1._rk, 3._rk])
  @AssertEqual([3._rk, 5._rk, 3._rk], Coords)

  ! Lower z boundary
  Coords = PeriodicExtend(3, PeriodicZ, PeriodicStorage, PeriodicLength, iStart, iEnd, [3,3,1], &
    [3._rk, 3._rk, 1._rk])
  @AssertEqual([3._rk, 3._rk, 1._rk], Coords)

  ! Upper z boundary
  Coords = PeriodicExtend(3, PeriodicZ, PeriodicStorage, PeriodicLength, iStart, iEnd, [3,3,5], &
    [3._rk, 3._rk, 1._rk])
  @AssertEqual([3._rk, 3._rk, 5._rk], Coords)

  ! Lower x off-grid
  Coords = PeriodicExtend(3, PeriodicX, PeriodicStorage, PeriodicLength, iStart, iEnd, [0,3,3], &
    [4._rk, 3._rk, 3._rk])
  @AssertEqual([0._rk, 3._rk, 3._rk], Coords)

  ! Upper x off-grid
  Coords = PeriodicExtend(3, PeriodicX, PeriodicStorage, PeriodicLength, iStart, iEnd, [6,3,3], &
    [2._rk, 3._rk, 3._rk])
  @AssertEqual([6._rk, 3._rk, 3._rk], Coords)

  ! Lower y off-grid
  Coords = PeriodicExtend(3, PeriodicY, PeriodicStorage, PeriodicLength, iStart, iEnd, [3,0,3], &
    [3._rk, 4._rk, 3._rk])
  @AssertEqual([3._rk, 0._rk, 3._rk], Coords)

  ! Upper y off-grid
  Coords = PeriodicExtend(3, PeriodicY, PeriodicStorage, PeriodicLength, iStart, iEnd, [3,6,3], &
    [3._rk, 2._rk, 3._rk])
  @AssertEqual([3._rk, 6._rk, 3._rk], Coords)

  ! Lower z off-grid
  Coords = PeriodicExtend(3, PeriodicZ, PeriodicStorage, PeriodicLength, iStart, iEnd, [3,3,0], &
    [3._rk, 3._rk, 4._rk])
  @AssertEqual([3._rk, 3._rk, 0._rk], Coords)

  ! Upper z off-grid
  Coords = PeriodicExtend(3, PeriodicZ, PeriodicStorage, PeriodicLength, iStart, iEnd, [3,3,6], &
    [3._rk, 3._rk, 2._rk])
  @AssertEqual([3._rk, 3._rk, 6._rk], Coords)

end subroutine ModGridUtilsTest_periodic_extend_overlap_periodic_3d

@MPITest(npes=[1])
subroutine ModGridUtilsTest_periodic_extend_no_overlap_periodic_3d(this)

  use pFUnit_mod
  use ModGridUtils
  use ModGlobal
  implicit none

  class(MPITestCase), intent(inout) :: this

  real(rk), dimension(3) :: Coords
  integer(ik), dimension(3) :: PeriodicX, PeriodicY, PeriodicZ
  integer(ik) :: PeriodicStorage
  real(rk), dimension(3) :: PeriodicLength
  integer(ik), dimension(3) :: iStart, iEnd

  PeriodicX = [PLANE_PERIODIC,FALSE,FALSE]
  PeriodicY = [FALSE,PLANE_PERIODIC,FALSE]
  PeriodicZ = [FALSE,FALSE,PLANE_PERIODIC]
  PeriodicStorage = NO_OVERLAP_PERIODIC
  PeriodicLength = 4._rk
  iStart = [1,1,1]
  iEnd = [4,4,4]

  ! Lower x boundary
  Coords = PeriodicExtend(3, PeriodicX, PeriodicStorage, PeriodicLength, iStart, iEnd, &
    [1,3,3], [1._rk, 3._rk, 3._rk])
  @AssertEqual([1._rk, 3._rk, 3._rk], Coords)

  ! Upper x boundary
  Coords = PeriodicExtend(3, PeriodicX, PeriodicStorage, PeriodicLength, iStart, iEnd, &
    [5,3,3], [1._rk, 3._rk, 3._rk])
  @AssertEqual([5._rk, 3._rk, 3._rk], Coords)

  ! Lower y boundary
  Coords = PeriodicExtend(3, PeriodicY, PeriodicStorage, PeriodicLength, iStart, iEnd, &
    [3,1,3], [3._rk, 1._rk, 3._rk])
  @AssertEqual([3._rk, 1._rk, 3._rk], Coords)

  ! Upper y boundary
  Coords = PeriodicExtend(3, PeriodicY, PeriodicStorage, PeriodicLength, iStart, iEnd, &
    [3,5,3], [3._rk, 1._rk, 3._rk])
  @AssertEqual([3._rk, 5._rk, 3._rk], Coords)

  ! Lower z boundary
  Coords = PeriodicExtend(3, PeriodicZ, PeriodicStorage, PeriodicLength, iStart, iEnd, &
    [3,3,1], [3._rk, 3._rk, 1._rk])
  @AssertEqual([3._rk, 3._rk, 1._rk], Coords)

  ! Upper z boundary
  Coords = PeriodicExtend(3, PeriodicZ, PeriodicStorage, PeriodicLength, iStart, iEnd, &
    [3,3,5], [3._rk, 3._rk, 1._rk])
  @AssertEqual([3._rk, 3._rk, 5._rk], Coords)

  ! Lower x off-grid
  Coords = PeriodicExtend(3, PeriodicX, PeriodicStorage, PeriodicLength, iStart, iEnd, &
    [0,3,3], [4._rk, 3._rk, 3._rk])
  @AssertEqual([0._rk, 3._rk, 3._rk], Coords)

  ! Upper x off-grid
  Coords = PeriodicExtend(3, PeriodicX, PeriodicStorage, PeriodicLength, iStart, iEnd, &
    [6,3,3], [2._rk, 3._rk, 3._rk])
  @AssertEqual([6._rk, 3._rk, 3._rk], Coords)

  ! Lower y off-grid
  Coords = PeriodicExtend(3, PeriodicY, PeriodicStorage, PeriodicLength, iStart, iEnd, &
    [3,0,3], [3._rk, 4._rk, 3._rk])
  @AssertEqual([3._rk, 0._rk, 3._rk], Coords)

  ! Upper y off-grid
  Coords = PeriodicExtend(3, PeriodicY, PeriodicStorage, PeriodicLength, iStart, iEnd, &
    [3,6,3], [3._rk, 2._rk, 3._rk])
  @AssertEqual([3._rk, 6._rk, 3._rk], Coords)

  ! Lower z off-grid
  Coords = PeriodicExtend(3, PeriodicZ, PeriodicStorage, PeriodicLength, iStart, iEnd, &
    [3,3,0], [3._rk, 3._rk, 4._rk])
  @AssertEqual([3._rk, 3._rk, 0._rk], Coords)

  ! Upper z off-grid
  Coords = PeriodicExtend(3, PeriodicZ, PeriodicStorage, PeriodicLength, iStart, iEnd, &
    [3,3,6], [3._rk, 3._rk, 2._rk])
  @AssertEqual([3._rk, 3._rk, 6._rk], Coords)

end subroutine ModGridUtilsTest_periodic_extend_no_overlap_periodic_3d
