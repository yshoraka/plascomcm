! Copyright (c) 2015, University of Illinois at Urbana-Champaign, XPACC
! License: MIT, http://opensource.org/licenses/MIT
@MPITest(npes=[1])
subroutine ModGeometryTest_overlaps_quad(this)

  use pFUnit_mod
  use ModGeometry
  use ModGlobal
  implicit none

  class(MPITestCase), intent(inout) :: this

  real(rk), dimension(2,4) :: Vertices

  ! Axis-aligned quad
  Vertices(1,:) = [2._rk, 3._rk, 2._rk, 3._rk]
  Vertices(2,:) = [4._rk, 4._rk, 5._rk, 5._rk]

  ! Vertex 1
  @AssertTrue(OverlapsQuad([2._rk, 4._rk], Vertices, AxisAligned=.true.))

  ! Vertex 2
  @AssertTrue(OverlapsQuad([3._rk, 4._rk], Vertices, AxisAligned=.true.))

  ! Vertex 3
  @AssertTrue(OverlapsQuad([2._rk, 5._rk], Vertices, AxisAligned=.true.))

  ! Vertex 4
  @AssertTrue(OverlapsQuad([3._rk, 5._rk], Vertices, AxisAligned=.true.))

  ! Center
  @AssertTrue(OverlapsQuad([2.5_rk, 4.5_rk], Vertices, AxisAligned=.true.))

  ! Outside
  @AssertFalse(OverlapsQuad([0._rk, 0._rk], Vertices, AxisAligned=.true.))

  ! General quad
  Vertices(1,:) = [2._rk, 3._rk, 1._rk, 4._rk]
  Vertices(2,:) = [4._rk, 4._rk, 5._rk, 5._rk]

  ! Vertex 1
  @AssertTrue(OverlapsQuad([2._rk, 4._rk], Vertices))

  ! Vertex 2
  @AssertTrue(OverlapsQuad([3._rk, 4._rk], Vertices))

  ! Vertex 3
  @AssertTrue(OverlapsQuad([1._rk, 5._rk], Vertices))

  ! Vertex 4
  @AssertTrue(OverlapsQuad([4._rk, 5._rk], Vertices))

  ! Center
  @AssertTrue(OverlapsQuad([2.5_rk, 4.5_rk], Vertices))

  ! Outside
  @AssertFalse(OverlapsQuad([0._rk, 0._rk], Vertices))

end subroutine ModGeometryTest_overlaps_quad

@MPITest(npes=[1])
subroutine ModGeometryTest_overlaps_hexahedron(this)

  use pFUnit_mod
  use ModGeometry
  use ModGlobal
  implicit none

  class(MPITestCase), intent(inout) :: this

  real(rk), dimension(3,8) :: Vertices

  ! Axis-aligned hexahedron
  Vertices(1,:) = [2._rk, 3._rk, 2._rk, 3._rk, 2._rk, 3._rk, 2._rk, 3._rk]
  Vertices(2,:) = [4._rk, 4._rk, 5._rk, 5._rk, 4._rk, 4._rk, 5._rk, 5._rk]
  Vertices(3,:) = [6._rk, 6._rk, 6._rk, 6._rk, 7._rk, 7._rk, 7._rk, 7._rk]

  ! Vertex 1
  @AssertTrue(OverlapsHexahedron([2._rk, 4._rk, 6._rk], Vertices, AxisAligned=.true.))

  ! Vertex 2
  @AssertTrue(OverlapsHexahedron([3._rk, 4._rk, 6._rk], Vertices, AxisAligned=.true.))

  ! Vertex 3
  @AssertTrue(OverlapsHexahedron([2._rk, 5._rk, 6._rk], Vertices, AxisAligned=.true.))

  ! Vertex 4
  @AssertTrue(OverlapsHexahedron([3._rk, 5._rk, 6._rk], Vertices, AxisAligned=.true.))

  ! Vertex 5
  @AssertTrue(OverlapsHexahedron([2._rk, 4._rk, 7._rk], Vertices, AxisAligned=.true.))

  ! Vertex 6
  @AssertTrue(OverlapsHexahedron([3._rk, 4._rk, 7._rk], Vertices, AxisAligned=.true.))

  ! Vertex 7
  @AssertTrue(OverlapsHexahedron([2._rk, 5._rk, 7._rk], Vertices, AxisAligned=.true.))

  ! Vertex 8
  @AssertTrue(OverlapsHexahedron([3._rk, 5._rk, 7._rk], Vertices, AxisAligned=.true.))

  ! Center
  @AssertTrue(OverlapsHexahedron([2.5_rk, 4.5_rk, 6.5_rk], Vertices, AxisAligned=.true.))

  ! Outside
  @AssertFalse(OverlapsHexahedron([0._rk, 0._rk, 0._rk], Vertices, AxisAligned=.true.))

  ! General hexahedron
  Vertices(1,:) = [2._rk, 3._rk, 2._rk, 3._rk, 1._rk, 3._rk, 1._rk, 3._rk]
  Vertices(2,:) = [4._rk, 4._rk, 5._rk, 5._rk, 3._rk, 3._rk, 6._rk, 6._rk]
  Vertices(3,:) = [6._rk, 6._rk, 6._rk, 6._rk, 7._rk, 7._rk, 7._rk, 7._rk]

  ! Vertex 1
  @AssertTrue(OverlapsHexahedron([2._rk, 4._rk, 6._rk], Vertices))

  ! Vertex 2
  @AssertTrue(OverlapsHexahedron([3._rk, 4._rk, 6._rk], Vertices))

  ! Vertex 3
  @AssertTrue(OverlapsHexahedron([2._rk, 5._rk, 6._rk], Vertices))

  ! Vertex 4
  @AssertTrue(OverlapsHexahedron([3._rk, 5._rk, 6._rk], Vertices))

  ! Vertex 5
  @AssertTrue(OverlapsHexahedron([1._rk, 3._rk, 7._rk], Vertices))

  ! Vertex 6
  @AssertTrue(OverlapsHexahedron([3._rk, 3._rk, 7._rk], Vertices))

  ! Vertex 7
  @AssertTrue(OverlapsHexahedron([1._rk, 6._rk, 7._rk], Vertices))

  ! Vertex 8
  @AssertTrue(OverlapsHexahedron([3._rk, 6._rk, 7._rk], Vertices))

  ! Center
  @AssertTrue(OverlapsHexahedron([2.5_rk, 4.5_rk, 6.5_rk], Vertices))

  ! Outside
  @AssertFalse(OverlapsHexahedron([0._rk, 0._rk, 0._rk], Vertices))

end subroutine ModGeometryTest_overlaps_hexahedron

@MPITest(npes=[1])
subroutine ModGeometryTest_coords_in_quad(this)

  use pFUnit_mod
  use ModGeometry
  use ModGlobal
  implicit none

  class(MPITestCase), intent(inout) :: this

  real(rk), dimension(2,4) :: Vertices
  real(rk), dimension(2) :: Coords

  ! Axis-aligned quad
  Vertices(1,:) = [2._rk, 3._rk, 2._rk, 3._rk]
  Vertices(2,:) = [4._rk, 4._rk, 5._rk, 5._rk]

  ! Vertex 1
  Coords = CoordsInQuad([2._rk, 4._rk], Vertices, AxisAligned=.true.)
  @AssertEqual([0._rk, 0._rk], Coords, 1.e-10_rk)

  ! Vertex 2
  Coords = CoordsInQuad([3._rk, 4._rk], Vertices, AxisAligned=.true.)
  @AssertEqual([1._rk, 0._rk], Coords, 1.e-10_rk)

  ! Vertex 3
  Coords = CoordsInQuad([2._rk, 5._rk], Vertices, AxisAligned=.true.)
  @AssertEqual([0._rk, 1._rk], Coords, 1.e-10_rk)

  ! Vertex 4
  Coords = CoordsInQuad([3._rk, 5._rk], Vertices, AxisAligned=.true.)
  @AssertEqual([1._rk, 1._rk], Coords, 1.e-10_rk)

  ! Center
  Coords = CoordsInQuad([2.5_rk, 4.5_rk], Vertices, AxisAligned=.true.)
  @AssertEqual([0.5_rk, 0.5_rk], Coords, 1.e-10_rk)

  ! General quad
  Vertices(1,:) = [2._rk, 3._rk, 1._rk, 4._rk]
  Vertices(2,:) = [4._rk, 4._rk, 5._rk, 5._rk]

  ! Vertex 1
  Coords = CoordsInQuad([2._rk, 4._rk], Vertices)
  @AssertEqual([0._rk, 0._rk], Coords, 1.e-10_rk)

  ! Vertex 2
  Coords = CoordsInQuad([3._rk, 4._rk], Vertices)
  @AssertEqual([1._rk, 0._rk], Coords, 1.e-10_rk)

  ! Vertex 3
  Coords = CoordsInQuad([1._rk, 5._rk], Vertices)
  @AssertEqual([0._rk, 1._rk], Coords, 1.e-10_rk)

  ! Vertex 4
  Coords = CoordsInQuad([4._rk, 5._rk], Vertices)
  @AssertEqual([1._rk, 1._rk], Coords, 1.e-10_rk)

  ! Center
  Coords = CoordsInQuad([2.5_rk, 4.5_rk], Vertices)
  @AssertEqual([0.5_rk, 0.5_rk], Coords, 1.e-10_rk)

end subroutine ModGeometryTest_coords_in_quad

@MPITest(npes=[1])
subroutine ModGeometryTest_coords_in_hexahedron(this)

  use pFUnit_mod
  use ModGeometry
  use ModGlobal
  implicit none

  class(MPITestCase), intent(inout) :: this

  real(rk), dimension(3,8) :: Vertices
  real(rk), dimension(3) :: Coords

  ! Axis-aligned hexahedron
  Vertices(1,:) = [2._rk, 3._rk, 2._rk, 3._rk, 2._rk, 3._rk, 2._rk, 3._rk]
  Vertices(2,:) = [4._rk, 4._rk, 5._rk, 5._rk, 4._rk, 4._rk, 5._rk, 5._rk]
  Vertices(3,:) = [6._rk, 6._rk, 6._rk, 6._rk, 7._rk, 7._rk, 7._rk, 7._rk]

  ! Vertex 1
  Coords = CoordsInHexahedron([2._rk, 4._rk, 6._rk], Vertices, AxisAligned=.true.)
  @AssertEqual([0._rk, 0._rk, 0._rk], Coords, 1.e-10_rk)

  ! Vertex 2
  Coords = CoordsInHexahedron([3._rk, 4._rk, 6._rk], Vertices, AxisAligned=.true.)
  @AssertEqual([1._rk, 0._rk, 0._rk], Coords, 1.e-10_rk)

  ! Vertex 3
  Coords = CoordsInHexahedron([2._rk, 5._rk, 6._rk], Vertices, AxisAligned=.true.)
  @AssertEqual([0._rk, 1._rk, 0._rk], Coords, 1.e-10_rk)

  ! Vertex 4
  Coords = CoordsInHexahedron([3._rk, 5._rk, 6._rk], Vertices, AxisAligned=.true.)
  @AssertEqual([1._rk, 1._rk, 0._rk], Coords, 1.e-10_rk)

  ! Vertex 5
  Coords = CoordsInHexahedron([2._rk, 4._rk, 7._rk], Vertices, AxisAligned=.true.)
  @AssertEqual([0._rk, 0._rk, 1._rk], Coords, 1.e-10_rk)

  ! Vertex 6
  Coords = CoordsInHexahedron([3._rk, 4._rk, 7._rk], Vertices, AxisAligned=.true.)
  @AssertEqual([1._rk, 0._rk, 1._rk], Coords, 1.e-10_rk)

  ! Vertex 7
  Coords = CoordsInHexahedron([2._rk, 5._rk, 7._rk], Vertices, AxisAligned=.true.)
  @AssertEqual([0._rk, 1._rk, 1._rk], Coords, 1.e-10_rk)

  ! Vertex 8
  Coords = CoordsInHexahedron([3._rk, 5._rk, 7._rk], Vertices, AxisAligned=.true.)
  @AssertEqual([1._rk, 1._rk, 1._rk], Coords, 1.e-10_rk)

  ! Center
  Coords = CoordsInHexahedron([2.5_rk, 4.5_rk, 6.5_rk], Vertices, AxisAligned=.true.)
  @AssertEqual([0.5_rk, 0.5_rk, 0.5_rk], Coords, 1.e-10_rk)

  ! General hexahedron
  Vertices(1,:) = [2._rk, 3._rk, 2._rk, 3._rk, 1._rk, 4._rk, 1._rk, 4._rk]
  Vertices(2,:) = [4._rk, 4._rk, 5._rk, 5._rk, 3._rk, 3._rk, 6._rk, 6._rk]
  Vertices(3,:) = [6._rk, 6._rk, 6._rk, 6._rk, 7._rk, 7._rk, 7._rk, 7._rk]

  ! Vertex 1
  Coords = CoordsInHexahedron([2._rk, 4._rk, 6._rk], Vertices)
  @AssertEqual([0._rk, 0._rk, 0._rk], Coords, 1.e-10_rk)

  ! Vertex 2
  Coords = CoordsInHexahedron([3._rk, 4._rk, 6._rk], Vertices)
  @AssertEqual([1._rk, 0._rk, 0._rk], Coords, 1.e-10_rk)

  ! Vertex 3
  Coords = CoordsInHexahedron([2._rk, 5._rk, 6._rk], Vertices)
  @AssertEqual([0._rk, 1._rk, 0._rk], Coords, 1.e-10_rk)

  ! Vertex 4
  Coords = CoordsInHexahedron([3._rk, 5._rk, 6._rk], Vertices)
  @AssertEqual([1._rk, 1._rk, 0._rk], Coords, 1.e-10_rk)

  ! Vertex 5
  Coords = CoordsInHexahedron([1._rk, 3._rk, 7._rk], Vertices)
  @AssertEqual([0._rk, 0._rk, 1._rk], Coords, 1.e-10_rk)

  ! Vertex 6
  Coords = CoordsInHexahedron([4._rk, 3._rk, 7._rk], Vertices)
  @AssertEqual([1._rk, 0._rk, 1._rk], Coords, 1.e-10_rk)

  ! Vertex 7
  Coords = CoordsInHexahedron([1._rk, 6._rk, 7._rk], Vertices)
  @AssertEqual([0._rk, 1._rk, 1._rk], Coords, 1.e-10_rk)

  ! Vertex 8
  Coords = CoordsInHexahedron([4._rk, 6._rk, 7._rk], Vertices)
  @AssertEqual([1._rk, 1._rk, 1._rk], Coords, 1.e-10_rk)

  ! Center
  Coords = CoordsInHexahedron([2.5_rk, 4.5_rk, 6.5_rk], Vertices)
  @AssertEqual([0.5_rk, 0.5_rk, 0.5_rk], Coords, 1.e-10_rk)

end subroutine ModGeometryTest_coords_in_hexahedron

@MPITest(npes=[1])
subroutine ModGeometryTest_quad_size(this)

  use pFUnit_mod
  use ModGeometry
  use ModGlobal
  implicit none

  class(MPITestCase), intent(inout) :: this

  real(rk), dimension(2,4) :: Vertices

  ! Axis-aligned quad
  Vertices(1,:) = [2._rk, 3._rk, 2._rk, 3._rk]
  Vertices(2,:) = [4._rk, 4._rk, 5._rk, 5._rk]

  @AssertEqual(1._rk, QuadSize(Vertices, AxisAligned=.true.), 1.e-10_rk)

  ! General quad
  Vertices(1,:) = [2._rk, 3._rk, 4._rk, 5._rk]
  Vertices(2,:) = [4._rk, 4._rk, 5._rk, 5._rk]

  @AssertEqual(1._rk, QuadSize(Vertices), 1.e-10_rk)

end subroutine ModGeometryTest_quad_size

@MPITest(npes=[1])
subroutine ModGeometryTest_hexahedron_size(this)

  use pFUnit_mod
  use ModGeometry
  use ModGlobal
  implicit none

  class(MPITestCase), intent(inout) :: this

  real(rk), dimension(3,8) :: Vertices

  ! Axis-aligned hexahedron
  Vertices(1,:) = [2._rk, 3._rk, 2._rk, 3._rk, 2._rk, 3._rk, 2._rk, 3._rk]
  Vertices(2,:) = [4._rk, 4._rk, 5._rk, 5._rk, 4._rk, 4._rk, 5._rk, 5._rk]
  Vertices(3,:) = [6._rk, 6._rk, 6._rk, 6._rk, 7._rk, 7._rk, 7._rk, 7._rk]

  @AssertEqual(1._rk, HexahedronSize(Vertices, AxisAligned=.true.), 1.e-10_rk)

  ! General hexahedron
  Vertices(1,:) = [2._rk, 3._rk, 2._rk, 3._rk, 4._rk, 5._rk, 4._rk, 5._rk]
  Vertices(2,:) = [4._rk, 4._rk, 5._rk, 5._rk, 6._rk, 6._rk, 7._rk, 7._rk]
  Vertices(3,:) = [6._rk, 6._rk, 6._rk, 6._rk, 7._rk, 7._rk, 7._rk, 7._rk]

  @AssertEqual(1._rk, HexahedronSize(Vertices), 1.e-10_rk)

end subroutine ModGeometryTest_hexahedron_size

@MPITest(npes=[1])
subroutine ModGeometryTest_isoparametric_2d_linear(this)

  use pFUnit_mod
  use ModGeometry
  use ModGlobal
  implicit none

  class(MPITestCase), intent(inout) :: this

  real(rk), dimension(2,4) :: Vertices
  real(rk), dimension(2) :: Coords

  Vertices(1,:) = [2._rk, 3._rk, 1._rk, 4._rk]
  Vertices(2,:) = [4._rk, 4._rk, 5._rk, 5._rk]

  ! Vertex 1
  Coords = Isoparametric2D(1, Vertices, [0._rk, 0._rk])
  @AssertEqual([2._rk, 4._rk], Coords, 1.e-10_rk)

  ! Vertex 2
  Coords = Isoparametric2D(1, Vertices, [1._rk, 0._rk])
  @AssertEqual([3._rk, 4._rk], Coords, 1.e-10_rk)

  ! Vertex 3
  Coords = Isoparametric2D(1, Vertices, [0._rk, 1._rk])
  @AssertEqual([1._rk, 5._rk], Coords, 1.e-10_rk)

  ! Vertex 4
  Coords = Isoparametric2D(1, Vertices, [1._rk, 1._rk])
  @AssertEqual([4._rk, 5._rk], Coords, 1.e-10_rk)

end subroutine ModGeometryTest_isoparametric_2d_linear

@MPITest(npes=[1])
subroutine ModGeometryTest_isoparametric_2d_cubic(this)

  use pFUnit_mod
  use ModGeometry
  use ModGlobal
  implicit none

  class(MPITestCase), intent(inout) :: this

  integer(ik) :: i, j, l
  real(rk), dimension(2,16) :: Vertices
  real(rk), dimension(2) :: Coords

  do j = 1, 4
    do i = 1, 4
      l = 1 + (i-1) + 4*(j-1)
      Vertices(:,l) = [2._rk, 4._rk] + real([i+j,j], kind=rk)
    end do
  end do

  ! Central cell vertex 1
  Coords = Isoparametric2D(3, Vertices, [0._rk, 0._rk])
  @AssertEqual(Vertices(:,6), Coords, 1.e-10_rk)

  ! Central cell vertex 2
  Coords = Isoparametric2D(3, Vertices, [1._rk, 0._rk])
  @AssertEqual(Vertices(:,7), Coords, 1.e-10_rk)

  ! Central cell vertex 3
  Coords = Isoparametric2D(3, Vertices, [0._rk, 1._rk])
  @AssertEqual(Vertices(:,10), Coords, 1.e-10_rk)

  ! Central cell vertex 4
  Coords = Isoparametric2D(3, Vertices, [1._rk, 1._rk])
  @AssertEqual(Vertices(:,11), Coords, 1.e-10_rk)

end subroutine ModGeometryTest_isoparametric_2d_cubic

@MPITest(npes=[1])
subroutine ModGeometryTest_isoparametric_3d_linear(this)

  use pFUnit_mod
  use ModGeometry
  use ModGlobal
  implicit none

  class(MPITestCase), intent(inout) :: this

  real(rk), dimension(3,8) :: Vertices
  real(rk), dimension(3) :: Coords

  Vertices(1,:) = [2._rk, 3._rk, 2._rk, 3._rk, 1._rk, 4._rk, 1._rk, 4._rk]
  Vertices(2,:) = [4._rk, 4._rk, 5._rk, 5._rk, 3._rk, 3._rk, 6._rk, 6._rk]
  Vertices(3,:) = [6._rk, 6._rk, 6._rk, 6._rk, 7._rk, 7._rk, 7._rk, 7._rk]

  ! Vertex 1
  Coords = Isoparametric3D(1, Vertices, [0._rk, 0._rk, 0._rk])
  @AssertEqual([2._rk, 4._rk, 6._rk], Coords, 1.e-10_rk)

  ! Vertex 2
  Coords = Isoparametric3D(1, Vertices, [1._rk, 0._rk, 0._rk])
  @AssertEqual([3._rk, 4._rk, 6._rk], Coords, 1.e-10_rk)

  ! Vertex 3
  Coords = Isoparametric3D(1, Vertices, [0._rk, 1._rk, 0._rk])
  @AssertEqual([2._rk, 5._rk, 6._rk], Coords, 1.e-10_rk)

  ! Vertex 4
  Coords = Isoparametric3D(1, Vertices, [1._rk, 1._rk, 0._rk])
  @AssertEqual([3._rk, 5._rk, 6._rk], Coords, 1.e-10_rk)

  ! Vertex 5
  Coords = Isoparametric3D(1, Vertices, [0._rk, 0._rk, 1._rk])
  @AssertEqual([1._rk, 3._rk, 7._rk], Coords, 1.e-10_rk)

  ! Vertex 6
  Coords = Isoparametric3D(1, Vertices, [1._rk, 0._rk, 1._rk])
  @AssertEqual([4._rk, 3._rk, 7._rk], Coords, 1.e-10_rk)

  ! Vertex 7
  Coords = Isoparametric3D(1, Vertices, [0._rk, 1._rk, 1._rk])
  @AssertEqual([1._rk, 6._rk, 7._rk], Coords, 1.e-10_rk)

  ! Vertex 8
  Coords = Isoparametric3D(1, Vertices, [1._rk, 1._rk, 1._rk])
  @AssertEqual([4._rk, 6._rk, 7._rk], Coords, 1.e-10_rk)

end subroutine ModGeometryTest_isoparametric_3d_linear

@MPITest(npes=[1])
subroutine ModGeometryTest_isoparametric_3d_cubic(this)

  use pFUnit_mod
  use ModGeometry
  use ModGlobal
  implicit none

  class(MPITestCase), intent(inout) :: this

  integer(ik) :: i, j, k, l
  real(rk), dimension(3,64) :: Vertices
  real(rk), dimension(3) :: Coords

  do k = 1, 4
    do j = 1, 4
      do i = 1, 4
        l = 1 + (i-1) + 4*(j-1) + 16*(k-1)
        Vertices(:,l) = [2._rk, 4._rk, 6._rk] + real([i+k,j+k,k], kind=rk)
      end do
    end do
  end do

  ! Central cell vertex 1
  Coords = Isoparametric3D(3, Vertices, [0._rk, 0._rk, 0._rk])
  @AssertEqual(Vertices(:,22), Coords, 1.e-10_rk)

  ! Central cell vertex 2
  Coords = Isoparametric3D(3, Vertices, [1._rk, 0._rk, 0._rk])
  @AssertEqual(Vertices(:,23), Coords, 1.e-10_rk)

  ! Central cell vertex 3
  Coords = Isoparametric3D(3, Vertices, [0._rk, 1._rk, 0._rk])
  @AssertEqual(Vertices(:,26), Coords, 1.e-10_rk)

  ! Central cell vertex 4
  Coords = Isoparametric3D(3, Vertices, [1._rk, 1._rk, 0._rk])
  @AssertEqual(Vertices(:,27), Coords, 1.e-10_rk)

  ! Central cell vertex 5
  Coords = Isoparametric3D(3, Vertices, [0._rk, 0._rk, 1._rk])
  @AssertEqual(Vertices(:,38), Coords, 1.e-10_rk)

  ! Central cell vertex 6
  Coords = Isoparametric3D(3, Vertices, [1._rk, 0._rk, 1._rk])
  @AssertEqual(Vertices(:,39), Coords, 1.e-10_rk)

  ! Central cell vertex 7
  Coords = Isoparametric3D(3, Vertices, [0._rk, 1._rk, 1._rk])
  @AssertEqual(Vertices(:,42), Coords, 1.e-10_rk)

  ! Central cell vertex 8
  Coords = Isoparametric3D(3, Vertices, [1._rk, 1._rk, 1._rk])
  @AssertEqual(Vertices(:,43), Coords, 1.e-10_rk)

end subroutine ModGeometryTest_isoparametric_3d_cubic

@MPITest(npes=[1])
subroutine ModGeometryTest_isoparametric_inverse_2d_linear(this)

  use pFUnit_mod
  use ModGeometry
  use ModGlobal
  implicit none

  class(MPITestCase), intent(inout) :: this

  real(rk), dimension(2,4) :: Vertices
  real(rk), dimension(2) :: LocalCoords

  Vertices(1,:) = [2._rk, 3._rk, 1._rk, 4._rk]
  Vertices(2,:) = [4._rk, 4._rk, 5._rk, 5._rk]

  ! Vertex 1
  LocalCoords = IsoparametricInverse2D(1, Vertices, [2._rk, 4._rk])
  @AssertEqual([0._rk, 0._rk], LocalCoords, 1.e-10_rk)

  ! Vertex 2
  LocalCoords = IsoparametricInverse2D(1, Vertices, [3._rk, 4._rk])
  @AssertEqual([1._rk, 0._rk], LocalCoords, 1.e-10_rk)

  ! Vertex 3
  LocalCoords = IsoparametricInverse2D(1, Vertices, [1._rk, 5._rk])
  @AssertEqual([0._rk, 1._rk], LocalCoords, 1.e-10_rk)

  ! Vertex 4
  LocalCoords = IsoparametricInverse2D(1, Vertices, [4._rk, 5._rk])
  @AssertEqual([1._rk, 1._rk], LocalCoords, 1.e-10_rk)

end subroutine ModGeometryTest_isoparametric_inverse_2d_linear

@MPITest(npes=[1])
subroutine ModGeometryTest_isoparametric_inverse_2d_cubic(this)

  use pFUnit_mod
  use ModGeometry
  use ModGlobal
  implicit none

  class(MPITestCase), intent(inout) :: this

  integer(ik) :: i, j, l
  real(rk), dimension(2,16) :: Vertices
  real(rk), dimension(2) :: LocalCoords

  do j = 1, 4
    do i = 1, 4
      l = 1 + (i-1) + 4*(j-1)
      Vertices(:,l) = [2._rk, 4._rk] + real([i+j,j], kind=rk)
    end do
  end do

  ! Central cell vertex 1
  LocalCoords = IsoparametricInverse2D(3, Vertices, Vertices(:,6))
  @AssertEqual([0._rk, 0._rk], LocalCoords, 1.e-10_rk)

  ! Central cell vertex 2
  LocalCoords = IsoparametricInverse2D(3, Vertices, Vertices(:,7))
  @AssertEqual([1._rk, 0._rk], LocalCoords, 1.e-10_rk)

  ! Central cell vertex 3
  LocalCoords = IsoparametricInverse2D(3, Vertices, Vertices(:,10))
  @AssertEqual([0._rk, 1._rk], LocalCoords, 1.e-10_rk)

  ! Central cell vertex 4
  LocalCoords = IsoparametricInverse2D(3, Vertices, Vertices(:,11))
  @AssertEqual([1._rk, 1._rk], LocalCoords, 1.e-10_rk)

end subroutine ModGeometryTest_isoparametric_inverse_2d_cubic

@MPITest(npes=[1])
subroutine ModGeometryTest_isoparametric_inverse_3d_linear(this)

  use pFUnit_mod
  use ModGeometry
  use ModGlobal
  implicit none

  class(MPITestCase), intent(inout) :: this

  real(rk), dimension(3,8) :: Vertices
  real(rk), dimension(3) :: LocalCoords

  Vertices(1,:) = [2._rk, 3._rk, 2._rk, 3._rk, 1._rk, 4._rk, 1._rk, 4._rk]
  Vertices(2,:) = [4._rk, 4._rk, 5._rk, 5._rk, 3._rk, 3._rk, 6._rk, 6._rk]
  Vertices(3,:) = [6._rk, 6._rk, 6._rk, 6._rk, 7._rk, 7._rk, 7._rk, 7._rk]

  ! Vertex 1
  LocalCoords = IsoparametricInverse3D(1, Vertices, [2._rk, 4._rk, 6._rk])
  @AssertEqual([0._rk, 0._rk, 0._rk], LocalCoords, 1.e-10_rk)

  ! Vertex 2
  LocalCoords = IsoparametricInverse3D(1, Vertices, [3._rk, 4._rk, 6._rk])
  @AssertEqual([1._rk, 0._rk, 0._rk], LocalCoords, 1.e-10_rk)

  ! Vertex 3
  LocalCoords = IsoparametricInverse3D(1, Vertices, [2._rk, 5._rk, 6._rk])
  @AssertEqual([0._rk, 1._rk, 0._rk], LocalCoords, 1.e-10_rk)

  ! Vertex 4
  LocalCoords = IsoparametricInverse3D(1, Vertices, [3._rk, 5._rk, 6._rk])
  @AssertEqual([1._rk, 1._rk, 0._rk], LocalCoords, 1.e-10_rk)

  ! Vertex 5
  LocalCoords = IsoparametricInverse3D(1, Vertices, [1._rk, 3._rk, 7._rk])
  @AssertEqual([0._rk, 0._rk, 1._rk], LocalCoords, 1.e-10_rk)

  ! Vertex 6
  LocalCoords = IsoparametricInverse3D(1, Vertices, [4._rk, 3._rk, 7._rk])
  @AssertEqual([1._rk, 0._rk, 1._rk], LocalCoords, 1.e-10_rk)

  ! Vertex 7
  LocalCoords = IsoparametricInverse3D(1, Vertices, [1._rk, 6._rk, 7._rk])
  @AssertEqual([0._rk, 1._rk, 1._rk], LocalCoords, 1.e-10_rk)

  ! Vertex 8
  LocalCoords = IsoparametricInverse3D(1, Vertices, [4._rk, 6._rk, 7._rk])
  @AssertEqual([1._rk, 1._rk, 1._rk], LocalCoords, 1.e-10_rk)

end subroutine ModGeometryTest_isoparametric_inverse_3d_linear

@MPITest(npes=[1])
subroutine ModGeometryTest_isoparametric_inverse_3d_cubic(this)

  use pFUnit_mod
  use ModGeometry
  use ModGlobal
  implicit none

  class(MPITestCase), intent(inout) :: this

  integer(ik) :: i, j, k, l
  real(rk), dimension(3,64) :: Vertices
  real(rk), dimension(3) :: LocalCoords

  do k = 1, 4
    do j = 1, 4
      do i = 1, 4
        l = 1 + (i-1) + 4*(j-1) + 16*(k-1)
        Vertices(:,l) = [2._rk, 4._rk, 6._rk] + real([i+k,j+k,k], kind=rk)
      end do
    end do
  end do

  ! Central cell vertex 1
  LocalCoords = IsoparametricInverse3D(3, Vertices, Vertices(:,22))
  @AssertEqual([0._rk, 0._rk, 0._rk], LocalCoords, 1.e-10_rk)

  ! Central cell vertex 2
  LocalCoords = IsoparametricInverse3D(3, Vertices, Vertices(:,23))
  @AssertEqual([1._rk, 0._rk, 0._rk], LocalCoords, 1.e-10_rk)

  ! Central cell vertex 3
  LocalCoords = IsoparametricInverse3D(3, Vertices, Vertices(:,26))
  @AssertEqual([0._rk, 1._rk, 0._rk], LocalCoords, 1.e-10_rk)

  ! Central cell vertex 4
  LocalCoords = IsoparametricInverse3D(3, Vertices, Vertices(:,27))
  @AssertEqual([1._rk, 1._rk, 0._rk], LocalCoords, 1.e-10_rk)

  ! Central cell vertex 5
  LocalCoords = IsoparametricInverse3D(3, Vertices, Vertices(:,38))
  @AssertEqual([0._rk, 0._rk, 1._rk], LocalCoords, 1.e-10_rk)

  ! Central cell vertex 6
  LocalCoords = IsoparametricInverse3D(3, Vertices, Vertices(:,39))
  @AssertEqual([1._rk, 0._rk, 1._rk], LocalCoords, 1.e-10_rk)

  ! Central cell vertex 7
  LocalCoords = IsoparametricInverse3D(3, Vertices, Vertices(:,42))
  @AssertEqual([0._rk, 1._rk, 1._rk], LocalCoords, 1.e-10_rk)

  ! Central cell vertex 8
  LocalCoords = IsoparametricInverse3D(3, Vertices, Vertices(:,43))
  @AssertEqual([1._rk, 1._rk, 1._rk], LocalCoords, 1.e-10_rk)

end subroutine ModGeometryTest_isoparametric_inverse_3d_cubic

@MPITest(npes=[1])
subroutine ModGeometryTest_interp_basis_linear(this)

  use pFUnit_mod
  use ModGeometry
  use ModGlobal
  implicit none

  class(MPITestCase), intent(inout) :: this

  real(rk), dimension(2) :: Basis
  real(rk), dimension(2) :: BasisDeriv

  Basis = InterpBasisLinear(0._rk)
  BasisDeriv = InterpBasisLinearDeriv(0._rk)

  @AssertEqual(1._rk, Basis(1), 1.e-10_rk)
  @AssertEqual(0._rk, Basis(2), 1.e-10_rk)
  @AssertEqual(-1._rk, BasisDeriv(1), 1.e-10_rk)
  @AssertEqual(1._rk, BasisDeriv(2), 1.e-10_rk)

  Basis = InterpBasisLinear(1._rk)
  BasisDeriv = InterpBasisLinearDeriv(1._rk)

  @AssertEqual(0._rk, Basis(1), 1.e-10_rk)
  @AssertEqual(1._rk, Basis(2), 1.e-10_rk)
  @AssertEqual(-1._rk, BasisDeriv(1), 1.e-10_rk)
  @AssertEqual(1._rk, BasisDeriv(2), 1.e-10_rk)

  Basis = InterpBasisLinear(0.5_rk)
  BasisDeriv = InterpBasisLinearDeriv(0.5_rk)

  @AssertEqual(0.5_rk, Basis(1), 1.e-10_rk)
  @AssertEqual(0.5_rk, Basis(2), 1.e-10_rk)
  @AssertEqual(-1._rk, BasisDeriv(1), 1.e-10_rk)
  @AssertEqual(1._rk, BasisDeriv(2), 1.e-10_rk)

end subroutine ModGeometryTest_interp_basis_linear

@MPITest(npes=[1])
subroutine ModGeometryTest_interp_basis_cubic(this)

  use pFUnit_mod
  use ModGeometry
  use ModGlobal
  implicit none

  class(MPITestCase), intent(inout) :: this

  real(rk), dimension(4) :: Basis
  real(rk), dimension(4) :: BasisDeriv

  Basis = InterpBasisCubic(0._rk)
  BasisDeriv = InterpBasisCubicDeriv(0._rk)

  @AssertEqual(0._rk, Basis(1), 1.e-10_rk)
  @AssertEqual(1._rk, Basis(2), 1.e-10_rk)
  @AssertEqual(0._rk, Basis(3), 1.e-10_rk)
  @AssertEqual(0._rk, Basis(4), 1.e-10_rk)
  @AssertEqual(-1._rk/3._rk, BasisDeriv(1), 1.e-10_rk)
  @AssertEqual(-0.5_rk, BasisDeriv(2), 1.e-10_rk)
  @AssertEqual(1._rk, BasisDeriv(3), 1.e-10_rk)
  @AssertEqual(-1._rk/6._rk, BasisDeriv(4), 1.e-10_rk)

  Basis = InterpBasisCubic(1._rk)
  BasisDeriv = InterpBasisCubicDeriv(1._rk)

  @AssertEqual(0._rk, Basis(1), 1.e-10_rk)
  @AssertEqual(0._rk, Basis(2), 1.e-10_rk)
  @AssertEqual(1._rk, Basis(3), 1.e-10_rk)
  @AssertEqual(0._rk, Basis(4), 1.e-10_rk)
  @AssertEqual(1._rk/6._rk, BasisDeriv(1), 1.e-10_rk)
  @AssertEqual(-1._rk, BasisDeriv(2), 1.e-10_rk)
  @AssertEqual(0.5_rk, BasisDeriv(3), 1.e-10_rk)
  @AssertEqual(1._rk/3._rk, BasisDeriv(4), 1.e-10_rk)

  Basis = InterpBasisCubic(0.5_rk)
  BasisDeriv = InterpBasisCubicDeriv(0.5_rk)

  @AssertEqual(-0.0625_rk, Basis(1), 1.e-10_rk)
  @AssertEqual(0.5625_rk, Basis(2), 1.e-10_rk)
  @AssertEqual(0.5625_rk, Basis(3), 1.e-10_rk)
  @AssertEqual(-0.0625_rk, Basis(4), 1.e-10_rk)
  @AssertEqual(1._rk/24._rk, BasisDeriv(1), 1.e-10_rk)
  @AssertEqual(-1.125_rk, BasisDeriv(2), 1.e-10_rk)
  @AssertEqual(1.125_rk, BasisDeriv(3), 1.e-10_rk)
  @AssertEqual(-1._rk/24._rk, BasisDeriv(4), 1.e-10_rk)

end subroutine ModGeometryTest_interp_basis_cubic
