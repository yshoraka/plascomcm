! Copyright (c) 2015, University of Illinois at Urbana-Champaign, XPACC
! License: MIT, http://opensource.org/licenses/MIT
@MPITest(npes=[1])
subroutine ModGridTest_default(this)

  use pFUnit_mod
  use ModBoundingBox
  use ModGrid
  use ModGlobal
  implicit none

  class(MPITestCase), intent(inout) :: this

  type(t_grid) :: Grid

  Grid = t_grid_(2)

  @AssertEqual(2, Grid%nd)
  @AssertEqual([1,1,1], Grid%is)
  @AssertEqual([0,0,1], Grid%ie)
  @AssertEqual([0,0,1], Grid%npoints)
  @AssertEqual(0, Grid%npoints_total)
  @AssertFalse(allocated(Grid%xyz))
  @AssertEqual(2, Grid%bounds%nd)
  @AssertEqual(0._rk, Grid%bounds%b(:2))
  @AssertEqual(-1._rk, Grid%bounds%e(:2))
  @AssertEqual(FALSE, Grid%periodic)
  @AssertEqual(NO_OVERLAP_PERIODIC, Grid%periodic_storage)
  @AssertEqual(0._rk, Grid%periodic_length)
  @AssertFalse(allocated(Grid%iblank))
  @AssertFalse(allocated(Grid%cell_sizes))
  @AssertEqual(GRID_TYPE_CURVILINEAR, Grid%grid_type)
  @AssertEqual(1, Grid%id)

  Grid = t_grid_(3)

  @AssertEqual(3, Grid%nd)
  @AssertEqual([1,1,1], Grid%is)
  @AssertEqual([0,0,0], Grid%ie)
  @AssertEqual([0,0,0], Grid%npoints)
  @AssertEqual(0, Grid%npoints_total)
  @AssertFalse(allocated(Grid%xyz))
  @AssertEqual(3, Grid%bounds%nd)
  @AssertEqual(0._rk, Grid%bounds%b)
  @AssertEqual(-1._rk, Grid%bounds%e)
  @AssertEqual(FALSE, Grid%periodic)
  @AssertEqual(NO_OVERLAP_PERIODIC, Grid%periodic_storage)
  @AssertEqual(0._rk, Grid%periodic_length)
  @AssertFalse(allocated(Grid%iblank))
  @AssertFalse(allocated(Grid%cell_sizes))
  @AssertEqual(GRID_TYPE_CURVILINEAR, Grid%grid_type)
  @AssertEqual(1, Grid%id)

end subroutine ModGridTest_default

@MPITest(npes=[1])
subroutine ModGridTest_make_2d(this)

  use pFUnit_mod
  use ModBoundingBox
  use ModGrid
  use ModGlobal
  implicit none

  class(MPITestCase), intent(inout) :: this

  type(t_grid) :: Grid

  integer(ik) :: i, j
  real(rk), dimension(2,3,2) :: Coords1
  real(rk), dimension(3,2,2) :: Coords2
  integer(ik), dimension(3,2) :: IBlank

  do j = 1, 2
    do i = 1, 3
      Coords1(:,i,j) = real([i-1,j-1], kind=rk)
    end do
  end do

  do j = 1, 2
    do i = 1, 3
      Coords2(i,j,:) = real([i-1,j-1], kind=rk)
    end do
  end do

  IBlank = -1

  call MakeGrid(Grid, nDims=2, iStart=[1,1], iEnd=[3,2], Coords=Coords1, &
    Periodic=[PLANE_PERIODIC,FALSE], PeriodicStorage=OVERLAP_PERIODIC, &
    PeriodicLength=[2._rk, 0._rk], IBlank=IBlank, GridType=GRID_TYPE_CARTESIAN, ID=2)

  @AssertEqual(2, Grid%nd)
  @AssertEqual([1,1,1], Grid%is)
  @AssertEqual([3,2,1], Grid%ie)
  @AssertEqual([3,2,1], Grid%npoints)
  @AssertEqual(6, Grid%npoints_total)
  @AssertTrue(allocated(Grid%xyz))
  @AssertEqual([2,6], shape(Grid%xyz))
  @AssertEqual(reshape(Coords1, [2,6]), Grid%xyz)
  @AssertEqual(2, Grid%bounds%nd)
  @AssertEqual([0._rk, 0._rk], Grid%bounds%b(:2))
  @AssertEqual([2._rk, 1._rk], Grid%bounds%e(:2))
  @AssertEqual([PLANE_PERIODIC,FALSE,FALSE], Grid%periodic)
  @AssertEqual(OVERLAP_PERIODIC, Grid%periodic_storage)
  @AssertEqual([2._rk, 0._rk, 0._rk], Grid%periodic_length)
  @AssertTrue(allocated(Grid%iblank))
  @AssertEqual(6, size(Grid%iblank))
  @AssertEqual(-1, Grid%iblank)
  @AssertTrue(allocated(Grid%cell_sizes))
  @AssertEqual(2, size(Grid%cell_sizes))
  @AssertEqual([1._rk, 1._rk], Grid%cell_sizes, 1.e-10_rk)
  @AssertEqual(GRID_TYPE_CARTESIAN, Grid%grid_type)
  @AssertEqual(2, Grid%id)

  ! Accepts grid with different coordinate layout
  call MakeGrid(Grid, nDims=2, iStart=[1,1], iEnd=[3,2], Coords=Coords2, &
    CoordsLayout=COMPONENTS_LAST)

  @AssertTrue(allocated(Grid%xyz))
  @AssertEqual([2,6], shape(Grid%xyz))
  @AssertEqual(reshape(Coords1, [2,6]), Grid%xyz)
  @AssertEqual([0._rk, 0._rk], Grid%bounds%b(:2))
  @AssertEqual([2._rk, 1._rk], Grid%bounds%e(:2))
  @AssertTrue(allocated(Grid%cell_sizes))
  @AssertEqual(2, size(Grid%cell_sizes))
  @AssertEqual([1._rk, 1._rk], Grid%cell_sizes, 1.e-10_rk)

end subroutine ModGridTest_make_2d

@MPITest(npes=[1])
subroutine ModGridTest_make_3d(this)

  use pFUnit_mod
  use ModBoundingBox
  use ModGrid
  use ModGlobal
  implicit none

  class(MPITestCase), intent(inout) :: this

  type(t_grid) :: Grid

  integer(ik) :: i, j, k
  real(rk), dimension(3,3,2,2) :: Coords1
  real(rk), dimension(3,2,2,3) :: Coords2
  integer(ik), dimension(3,2,2) :: IBlank

  do k = 1, 2
    do j = 1, 2
      do i = 1, 3
        Coords1(:,i,j,k) = real([i-1,j-1,k-1], kind=rk)
      end do
    end do
  end do

  do k = 1, 2
    do j = 1, 2
      do i = 1, 3
        Coords2(i,j,k,:) = real([i-1,j-1,k-1], kind=rk)
      end do
    end do
  end do

  IBlank = -1

  call MakeGrid(Grid, nDims=3, iStart=[1,1,1], iEnd=[3,2,2], Coords=Coords1, &
    Periodic=[PLANE_PERIODIC,FALSE,FALSE], PeriodicStorage=OVERLAP_PERIODIC, &
    PeriodicLength=[2._rk, 0._rk, 0._rk], IBlank=IBlank, GridType=GRID_TYPE_CARTESIAN, ID=2)

  @AssertEqual(3, Grid%nd)
  @AssertEqual([1,1,1], Grid%is)
  @AssertEqual([3,2,2], Grid%ie)
  @AssertEqual([3,2,2], Grid%npoints)
  @AssertEqual(12, Grid%npoints_total)
  @AssertTrue(allocated(Grid%xyz))
  @AssertEqual([3,12], shape(Grid%xyz))
  @AssertEqual(reshape(Coords1, [3,12]), Grid%xyz)
  @AssertEqual(3, Grid%bounds%nd)
  @AssertEqual([0._rk, 0._rk, 0._rk], Grid%bounds%b)
  @AssertEqual([2._rk, 1._rk, 1._rk], Grid%bounds%e)
  @AssertEqual([PLANE_PERIODIC,FALSE,FALSE], Grid%periodic)
  @AssertEqual(OVERLAP_PERIODIC, Grid%periodic_storage)
  @AssertEqual([2._rk, 0._rk, 0._rk], Grid%periodic_length)
  @AssertTrue(allocated(Grid%iblank))
  @AssertEqual(12, size(Grid%iblank))
  @AssertEqual(-1, Grid%iblank)
  @AssertTrue(allocated(Grid%cell_sizes))
  @AssertEqual(2, size(Grid%cell_sizes))
  @AssertEqual([1._rk, 1._rk], Grid%cell_sizes, 1.e-10_rk)
  @AssertEqual(GRID_TYPE_CARTESIAN, Grid%grid_type)
  @AssertEqual(2, Grid%id)

  ! Accepts grid with different coordinate layout
  call MakeGrid(Grid, nDims=3, iStart=[1,1,1], iEnd=[3,2,2], Coords=Coords2, &
    CoordsLayout=COMPONENTS_LAST)

  @AssertTrue(allocated(Grid%xyz))
  @AssertEqual([3,12], shape(Grid%xyz))
  @AssertEqual(reshape(Coords1, [3,12]), Grid%xyz)
  @AssertTrue(allocated(Grid%cell_sizes))
  @AssertEqual(2, size(Grid%cell_sizes))
  @AssertEqual([1._rk, 1._rk], Grid%cell_sizes, 1.e-10_rk)

end subroutine ModGridTest_make_3d

@MPITest(npes=[1])
subroutine ModGridTest_destroy(this)

  use pFUnit_mod
  use ModBoundingBox
  use ModGrid
  use ModGlobal
  implicit none

  class(MPITestCase), intent(inout) :: this

  type(t_grid) :: Grid

  real(rk), dimension(2,3,1) :: Coords

  Coords = reshape([ &
    0._rk, 1._rk, &
    1._rk, 1._rk, &
    2._rk, 1._rk &
  ], [2,3,1])

  call MakeGrid(Grid, nDims=2, iStart=[1,1], iEnd=[3,1], Coords=Coords)
  call DestroyGrid(Grid)

  @AssertFalse(allocated(Grid%xyz))
  @AssertFalse(allocated(Grid%iblank))
  @AssertFalse(allocated(Grid%cell_sizes))

end subroutine ModGridTest_destroy

@MPITest(npes=[1])
subroutine ModGridTest_overlaps_cell_uniform_2d(this)

  use pFUnit_mod
  use ModGlobal
  use ModGrid
  use ModOverset
  implicit none

  class(MPITestCase), intent(inout) :: this

  real(rk), dimension(2,2,3) :: Coords
  integer(ik), dimension(2,3) :: IBlank
  type(t_grid) :: Grid

  Coords = reshape([ &
    2._rk, 4._rk, &
    3._rk, 4._rk, &
    2._rk, 5._rk, &
    3._rk, 5._rk, &
    2._rk, 6._rk, &
    3._rk, 6._rk &
  ], [2,2,3])

  IBlank = 1
  IBlank(2,3) = 0

  call MakeGrid(Grid, nDims=2, iStart=[1,1], iEnd=[2,3], Coords=Coords, IBlank=IBlank, &
    GridType=GRID_TYPE_CARTESIAN)

  ! Grid point (1,1)
  @AssertTrue(OverlapsCell(Grid, [1,1], [2._rk, 4._rk]))

  ! Grid point (2,1)
  @AssertTrue(OverlapsCell(Grid, [1,1], [3._rk, 4._rk]))

  ! Grid point (1,2)
  @AssertTrue(OverlapsCell(Grid, [1,1], [2._rk, 5._rk]))

  ! Grid point (2,2)
  @AssertTrue(OverlapsCell(Grid, [1,1], [3._rk, 5._rk]))

  ! Grid point (1,3) -- part of cell containing blanked point
  @AssertFalse(OverlapsCell(Grid, [1,2], [2._rk, 6._rk]))

  ! Grid point (2,3) -- blanked point
  @AssertFalse(OverlapsCell(Grid, [1,2], [3._rk, 6._rk]))

  ! Cell 1 center
  @AssertTrue(OverlapsCell(Grid, [1,1], [2.5_rk, 4.5_rk]))

  ! Cell 2 center -- cell contains blanked point
  @AssertFalse(OverlapsCell(Grid, [1,2], [2.5_rk, 5.5_rk]))

  ! Off grid
  @AssertFalse(OverlapsCell(Grid, [1,1], [0._rk, 0._rk]))
  @AssertFalse(OverlapsCell(Grid, [1,2], [0._rk, 0._rk]))

end subroutine ModGridTest_overlaps_cell_uniform_2d

@MPITest(npes=[1])
subroutine ModGridTest_overlaps_cell_uniform_3d(this)

  use pFUnit_mod
  use ModGlobal
  use ModGrid
  use ModOverset
  implicit none

  class(MPITestCase), intent(inout) :: this

  real(rk), dimension(3,2,2,3) :: Coords
  integer(ik), dimension(2,2,3) :: IBlank
  type(t_grid) :: Grid

  Coords = reshape([ &
    2._rk, 4._rk, 6._rk, &
    3._rk, 4._rk, 6._rk, &
    2._rk, 5._rk, 6._rk, &
    3._rk, 5._rk, 6._rk, &
    2._rk, 4._rk, 7._rk, &
    3._rk, 4._rk, 7._rk, &
    2._rk, 5._rk, 7._rk, &
    3._rk, 5._rk, 7._rk, &
    2._rk, 4._rk, 8._rk, &
    3._rk, 4._rk, 8._rk, &
    2._rk, 5._rk, 8._rk, &
    3._rk, 5._rk, 8._rk &
  ], [3,2,2,3])

  IBlank = 1
  IBlank(2,2,3) = 0

  call MakeGrid(Grid, nDims=3, iStart=[1,1,1], iEnd=[2,2,3], Coords=Coords, IBlank=IBlank, &
    GridType=GRID_TYPE_CARTESIAN)

  ! Grid point (1,1,1)
  @AssertTrue(OverlapsCell(Grid, [1,1,1], [2._rk, 4._rk, 6._rk]))

  ! Grid point (2,1,1)
  @AssertTrue(OverlapsCell(Grid, [1,1,1], [3._rk, 4._rk, 6._rk]))

  ! Grid point (1,2,1)
  @AssertTrue(OverlapsCell(Grid, [1,1,1], [2._rk, 5._rk, 6._rk]))

  ! Grid point (2,2,1)
  @AssertTrue(OverlapsCell(Grid, [1,1,1], [3._rk, 5._rk, 6._rk]))

  ! Grid point (1,1,2)
  @AssertTrue(OverlapsCell(Grid, [1,1,1], [2._rk, 4._rk, 7._rk]))

  ! Grid point (2,1,2)
  @AssertTrue(OverlapsCell(Grid, [1,1,1], [3._rk, 4._rk, 7._rk]))

  ! Grid point (1,2,2)
  @AssertTrue(OverlapsCell(Grid, [1,1,1], [2._rk, 5._rk, 7._rk]))

  ! Grid point (2,2,2)
  @AssertTrue(OverlapsCell(Grid, [1,1,1], [3._rk, 5._rk, 7._rk]))

  ! Grid point (1,1,3) -- part of cell containing blanked point
  @AssertFalse(OverlapsCell(Grid, [1,1,2], [2._rk, 4._rk, 8._rk]))

  ! Grid point (2,1,3) -- part of cell containing blanked point
  @AssertFalse(OverlapsCell(Grid, [1,1,2], [3._rk, 4._rk, 8._rk]))

  ! Grid point (1,2,3) -- part of cell containing blanked point
  @AssertFalse(OverlapsCell(Grid, [1,1,2], [2._rk, 5._rk, 8._rk]))

  ! Grid point (2,2,3) -- blanked point
  @AssertFalse(OverlapsCell(Grid, [1,1,2], [3._rk, 5._rk, 8._rk]))

  ! Cell 1 center
  @AssertTrue(OverlapsCell(Grid, [1,1,1], [2.5_rk, 4.5_rk, 6.5_rk]))

  ! Cell 2 center -- cell contains blanked point
  @AssertFalse(OverlapsCell(Grid, [1,1,2], [2.5_rk, 4.5_rk, 7.5_rk]))

  ! Off grid
  @AssertFalse(OverlapsCell(Grid, [1,1,1], [0._rk, 0._rk, 0._rk]))
  @AssertFalse(OverlapsCell(Grid, [1,1,2], [0._rk, 0._rk, 0._rk]))

end subroutine ModGridTest_overlaps_cell_uniform_3d

@MPITest(npes=[1])
subroutine ModGridTest_overlaps_cell_non_uniform_2d(this)

  use pFUnit_mod
  use ModGlobal
  use ModGrid
  use ModOverset
  implicit none

  class(MPITestCase), intent(inout) :: this

  real(rk), dimension(2,2,3) :: Coords
  integer(ik), dimension(2,3) :: IBlank
  type(t_grid) :: Grid

  Coords = reshape([ &
    2._rk, 4._rk, &
    3._rk, 4._rk, &
    1._rk, 5._rk, &
    4._rk, 5._rk, &
    0._rk, 6._rk, &
    5._rk, 6._rk &
  ], [2,2,3])

  IBlank = 1
  IBlank(2,3) = 0

  call MakeGrid(Grid, nDims=2, iStart=[1,1], iEnd=[2,3], Coords=Coords, IBlank=IBlank, &
    GridType=GRID_TYPE_CURVILINEAR)

  ! Grid point (1,1)
  @AssertTrue(OverlapsCell(Grid, [1,1], [2._rk, 4._rk]))

  ! Grid point (2,1)
  @AssertTrue(OverlapsCell(Grid, [1,1], [3._rk, 4._rk]))

  ! Grid point (1,2)
  @AssertTrue(OverlapsCell(Grid, [1,1], [1._rk, 5._rk]))

  ! Grid point (2,2)
  @AssertTrue(OverlapsCell(Grid, [1,1], [4._rk, 5._rk]))

  ! Grid point (1,3) -- part of cell containing blanked point
  @AssertFalse(OverlapsCell(Grid, [1,2], [0._rk, 6._rk]))

  ! Grid point (2,3) -- blanked point
  @AssertFalse(OverlapsCell(Grid, [1,2], [5._rk, 6._rk]))

  ! Cell 1 center
  @AssertTrue(OverlapsCell(Grid, [1,1], [2.5_rk, 4.5_rk]))

  ! Cell 2 center -- cell contains blanked point
  @AssertFalse(OverlapsCell(Grid, [1,2], [2.5_rk, 5.5_rk]))

  ! Off grid
  @AssertFalse(OverlapsCell(Grid, [1,1], [0._rk, 0._rk]))
  @AssertFalse(OverlapsCell(Grid, [1,2], [0._rk, 0._rk]))

end subroutine ModGridTest_overlaps_cell_non_uniform_2d

@MPITest(npes=[1])
subroutine ModGridTest_overlaps_cell_non_uniform_3d(this)

  use pFUnit_mod
  use ModGlobal
  use ModGrid
  use ModOverset
  implicit none

  class(MPITestCase), intent(inout) :: this

  real(rk), dimension(3,2,2,3) :: Coords
  integer(ik), dimension(2,2,3) :: IBlank
  type(t_grid) :: Grid

  Coords = reshape([ &
    2._rk, 4._rk, 6._rk, &
    3._rk, 4._rk, 6._rk, &
    2._rk, 5._rk, 6._rk, &
    3._rk, 5._rk, 6._rk, &
    1._rk, 3._rk, 7._rk, &
    4._rk, 3._rk, 7._rk, &
    1._rk, 6._rk, 7._rk, &
    4._rk, 6._rk, 7._rk, &
    0._rk, 2._rk, 8._rk, &
    5._rk, 2._rk, 8._rk, &
    0._rk, 7._rk, 8._rk, &
    5._rk, 7._rk, 8._rk &
  ], [3,2,2,3])

  IBlank = 1
  IBlank(2,2,3) = 0

  call MakeGrid(Grid, nDims=3, iStart=[1,1,1], iEnd=[2,2,3], Coords=Coords, IBlank=IBlank, &
    GridType=GRID_TYPE_CURVILINEAR)

  ! Grid point (1,1,1)
  @AssertTrue(OverlapsCell(Grid, [1,1,1], [2._rk, 4._rk, 6._rk]))

  ! Grid point (2,1,1)
  @AssertTrue(OverlapsCell(Grid, [1,1,1], [3._rk, 4._rk, 6._rk]))

  ! Grid point (1,2,1)
  @AssertTrue(OverlapsCell(Grid, [1,1,1], [2._rk, 5._rk, 6._rk]))

  ! Grid point (2,2,1)
  @AssertTrue(OverlapsCell(Grid, [1,1,1], [3._rk, 5._rk, 6._rk]))

  ! Grid point (1,1,2)
  @AssertTrue(OverlapsCell(Grid, [1,1,1], [1._rk, 3._rk, 7._rk]))

  ! Grid point (2,1,2)
  @AssertTrue(OverlapsCell(Grid, [1,1,1], [4._rk, 3._rk, 7._rk]))

  ! Grid point (1,2,2)
  @AssertTrue(OverlapsCell(Grid, [1,1,1], [1._rk, 6._rk, 7._rk]))

  ! Grid point (2,2,2)
  @AssertTrue(OverlapsCell(Grid, [1,1,1], [4._rk, 6._rk, 7._rk]))

  ! Grid point (1,1,3) -- part of cell containing blanked point
  @AssertFalse(OverlapsCell(Grid, [1,1,2], [0._rk, 2._rk, 8._rk]))

  ! Grid point (2,1,3) -- part of cell containing blanked point
  @AssertFalse(OverlapsCell(Grid, [1,1,2], [5._rk, 2._rk, 8._rk]))

  ! Grid point (1,2,3) -- part of cell containing blanked point
  @AssertFalse(OverlapsCell(Grid, [1,1,2], [0._rk, 7._rk, 8._rk]))

  ! Grid point (2,2,3) -- blanked point
  @AssertFalse(OverlapsCell(Grid, [1,1,2], [5._rk, 7._rk, 8._rk]))

  ! Cell 1 center
  @AssertTrue(OverlapsCell(Grid, [1,1,1], [2.5_rk, 4.5_rk, 6.5_rk]))

  ! Cell 2 center -- cell contains blanked point
  @AssertFalse(OverlapsCell(Grid, [1,1,2], [2.5_rk, 4.5_rk, 7.5_rk]))

  ! Off grid
  @AssertFalse(OverlapsCell(Grid, [1,1,1], [0._rk, 0._rk, 0._rk]))
  @AssertFalse(OverlapsCell(Grid, [1,1,2], [0._rk, 0._rk, 0._rk]))

end subroutine ModGridTest_overlaps_cell_non_uniform_3d

@MPITest(npes=[1])
subroutine ModGridTest_cell_coords_uniform_2d(this)

  use pFUnit_mod
  use ModGlobal
  use ModGrid
  use ModOverset
  implicit none

  class(MPITestCase), intent(inout) :: this

  real(rk), dimension(2,2,3) :: Coords
  type(t_grid) :: Grid
  real(rk), dimension(2) :: CellCoords

  Coords = reshape([ &
    2._rk, 4._rk, &
    3._rk, 4._rk, &
    2._rk, 5._rk, &
    3._rk, 5._rk, &
    2._rk, 6._rk, &
    3._rk, 6._rk &
  ], [2,2,3])

  call MakeGrid(Grid, nDims=2, iStart=[1,1], iEnd=[2,3], Coords=Coords, GridType=GRID_TYPE_CARTESIAN)

  ! Grid point (1,1)
  CellCoords = CoordsInCell(Grid, [1,1], [2._rk, 4._rk])
  @AssertEqual([0._rk, 0._rk], CellCoords, 1.e-10_rk)

  ! Grid point (2,1)
  CellCoords = CoordsInCell(Grid, [1,1], [3._rk, 4._rk])
  @AssertEqual([1._rk, 0._rk], CellCoords, 1.e-10_rk)

  ! Grid point (1,2)
  CellCoords = CoordsInCell(Grid, [1,1], [2._rk, 5._rk])
  @AssertEqual([0._rk, 1._rk], CellCoords, 1.e-10_rk)

  ! Grid point (2,2)
  CellCoords = CoordsInCell(Grid, [1,1], [3._rk, 5._rk])
  @AssertEqual([1._rk, 1._rk], CellCoords, 1.e-10_rk)

  ! Grid point (1,3)
  CellCoords = CoordsInCell(Grid, [1,2], [2._rk, 6._rk])
  @AssertEqual([0._rk, 1._rk], CellCoords, 1.e-10_rk)

  ! Grid point (2,3)
  CellCoords = CoordsInCell(Grid, [1,2], [3._rk, 6._rk])
  @AssertEqual([1._rk, 1._rk], CellCoords, 1.e-10_rk)

  ! Cell 1 center
  CellCoords = CoordsInCell(Grid, [1,1], [2.5_rk, 4.5_rk])
  @AssertEqual([0.5_rk, 0.5_rk], CellCoords, 1.e-10_rk)

  ! Cell 2 center
  CellCoords = CoordsInCell(Grid, [1,2], [2.5_rk, 5.5_rk])
  @AssertEqual([0.5_rk, 0.5_rk], CellCoords, 1.e-10_rk)

end subroutine ModGridTest_cell_coords_uniform_2d

@MPITest(npes=[1])
subroutine ModGridTest_cell_coords_uniform_3d(this)

  use pFUnit_mod
  use ModGlobal
  use ModGrid
  use ModOverset
  implicit none

  class(MPITestCase), intent(inout) :: this

  real(rk), dimension(3,2,2,3) :: Coords
  type(t_grid) :: Grid
  real(rk), dimension(3) :: CellCoords

  Coords = reshape([ &
    2._rk, 4._rk, 6._rk, &
    3._rk, 4._rk, 6._rk, &
    2._rk, 5._rk, 6._rk, &
    3._rk, 5._rk, 6._rk, &
    2._rk, 4._rk, 7._rk, &
    3._rk, 4._rk, 7._rk, &
    2._rk, 5._rk, 7._rk, &
    3._rk, 5._rk, 7._rk, &
    2._rk, 4._rk, 8._rk, &
    3._rk, 4._rk, 8._rk, &
    2._rk, 5._rk, 8._rk, &
    3._rk, 5._rk, 8._rk &
  ], [3,2,2,3])

  call MakeGrid(Grid, nDims=3, iStart=[1,1,1], iEnd=[2,2,3], Coords=Coords, &
    GridType=GRID_TYPE_CARTESIAN)

  ! Grid point (1,1,1)
  CellCoords = CoordsInCell(Grid, [1,1,1], [2._rk, 4._rk, 6._rk])
  @AssertEqual([0._rk, 0._rk, 0._rk], CellCoords, 1.e-10_rk)

  ! Grid point (2,1,1)
  CellCoords = CoordsInCell(Grid, [1,1,1], [3._rk, 4._rk, 6._rk])
  @AssertEqual([1._rk, 0._rk, 0._rk], CellCoords, 1.e-10_rk)

  ! Grid point (1,2,1)
  CellCoords = CoordsInCell(Grid, [1,1,1], [2._rk, 5._rk, 6._rk])
  @AssertEqual([0._rk, 1._rk, 0._rk], CellCoords, 1.e-10_rk)

  ! Grid point (2,2,1)
  CellCoords = CoordsInCell(Grid, [1,1,1], [3._rk, 5._rk, 6._rk])
  @AssertEqual([1._rk, 1._rk, 0._rk], CellCoords, 1.e-10_rk)

  ! Grid point (1,1,2)
  CellCoords = CoordsInCell(Grid, [1,1,1], [2._rk, 4._rk, 7._rk])
  @AssertEqual([0._rk, 0._rk, 1._rk], CellCoords, 1.e-10_rk)

  ! Grid point (2,1,2)
  CellCoords = CoordsInCell(Grid, [1,1,1], [3._rk, 4._rk, 7._rk])
  @AssertEqual([1._rk, 0._rk, 1._rk], CellCoords, 1.e-10_rk)

  ! Grid point (1,2,2)
  CellCoords = CoordsInCell(Grid, [1,1,1], [2._rk, 5._rk, 7._rk])
  @AssertEqual([0._rk, 1._rk, 1._rk], CellCoords, 1.e-10_rk)

  ! Grid point (2,2,2)
  CellCoords = CoordsInCell(Grid, [1,1,1], [3._rk, 5._rk, 7._rk])
  @AssertEqual([1._rk, 1._rk, 1._rk], CellCoords, 1.e-10_rk)

  ! Grid point (1,1,3)
  CellCoords = CoordsInCell(Grid, [1,1,2], [2._rk, 4._rk, 8._rk])
  @AssertEqual([0._rk, 0._rk, 1._rk], CellCoords, 1.e-10_rk)

  ! Grid point (2,1,3)
  CellCoords = CoordsInCell(Grid, [1,1,2], [3._rk, 4._rk, 8._rk])
  @AssertEqual([1._rk, 0._rk, 1._rk], CellCoords, 1.e-10_rk)

  ! Grid point (1,2,3)
  CellCoords = CoordsInCell(Grid, [1,1,2], [2._rk, 5._rk, 8._rk])
  @AssertEqual([0._rk, 1._rk, 1._rk], CellCoords, 1.e-10_rk)

  ! Grid point (2,2,3)
  CellCoords = CoordsInCell(Grid, [1,1,2], [3._rk, 5._rk, 8._rk])
  @AssertEqual([1._rk, 1._rk, 1._rk], CellCoords, 1.e-10_rk)

  ! Cell 1 center
  CellCoords = CoordsInCell(Grid, [1,1,1], [2.5_rk, 4.5_rk, 6.5_rk])
  @AssertEqual([0.5_rk, 0.5_rk, 0.5_rk], CellCoords, 1.e-10_rk)

  ! Cell 2 center
  CellCoords = CoordsInCell(Grid, [1,1,2], [2.5_rk, 4.5_rk, 7.5_rk])
  @AssertEqual([0.5_rk, 0.5_rk, 0.5_rk], CellCoords, 1.e-10_rk)

end subroutine ModGridTest_cell_coords_uniform_3d

@MPITest(npes=[1])
subroutine ModGridTest_cell_coords_non_uniform_2d(this)

  use pFUnit_mod
  use ModGlobal
  use ModGrid
  use ModOverset
  implicit none

  class(MPITestCase), intent(inout) :: this

  real(rk), dimension(2,2,3) :: Coords
  type(t_grid) :: Grid
  real(rk), dimension(2) :: CellCoords

  Coords = reshape([ &
    2._rk, 4._rk, &
    3._rk, 4._rk, &
    1._rk, 5._rk, &
    4._rk, 5._rk, &
    0._rk, 6._rk, &
    5._rk, 6._rk &
  ], [2,2,3])

  call MakeGrid(Grid, nDims=2, iStart=[1,1], iEnd=[2,3], Coords=Coords, &
    GridType=GRID_TYPE_CURVILINEAR)

  ! Grid point (1,1)
  CellCoords = CoordsInCell(Grid, [1,1], [2._rk, 4._rk])
  @AssertEqual([0._rk, 0._rk], CellCoords, 1.e-10_rk)

  ! Grid point (2,1)
  CellCoords = CoordsInCell(Grid, [1,1], [3._rk, 4._rk])
  @AssertEqual([1._rk, 0._rk], CellCoords, 1.e-10_rk)

  ! Grid point (1,2)
  CellCoords = CoordsInCell(Grid, [1,1], [1._rk, 5._rk])
  @AssertEqual([0._rk, 1._rk], CellCoords, 1.e-10_rk)

  ! Grid point (2,2)
  CellCoords = CoordsInCell(Grid, [1,1], [4._rk, 5._rk])
  @AssertEqual([1._rk, 1._rk], CellCoords, 1.e-10_rk)

  ! Grid point (1,3)
  CellCoords = CoordsInCell(Grid, [1,2], [0._rk, 6._rk])
  @AssertEqual([0._rk, 1._rk], CellCoords, 1.e-10_rk)

  ! Grid point (2,3)
  CellCoords = CoordsInCell(Grid, [1,2], [5._rk, 6._rk])
  @AssertEqual([1._rk, 1._rk], CellCoords, 1.e-10_rk)

  ! Cell 1 center
  CellCoords = CoordsInCell(Grid, [1,1], [2.5_rk, 4.5_rk])
  @AssertEqual([0.5_rk, 0.5_rk], CellCoords, 1.e-10_rk)

  ! Cell 2 center
  CellCoords = CoordsInCell(Grid, [1,2], [2.5_rk, 5.5_rk])
  @AssertEqual([0.5_rk, 0.5_rk], CellCoords, 1.e-10_rk)

end subroutine ModGridTest_cell_coords_non_uniform_2d

@MPITest(npes=[1])
subroutine ModGridTest_cell_coords_non_uniform_3d(this)

  use pFUnit_mod
  use ModGlobal
  use ModGrid
  use ModOverset
  implicit none

  class(MPITestCase), intent(inout) :: this

  real(rk), dimension(3,2,2,3) :: Coords
  type(t_grid) :: Grid
  real(rk), dimension(3) :: CellCoords

  Coords = reshape([ &
    2._rk, 4._rk, 6._rk, &
    3._rk, 4._rk, 6._rk, &
    2._rk, 5._rk, 6._rk, &
    3._rk, 5._rk, 6._rk, &
    1._rk, 3._rk, 7._rk, &
    4._rk, 3._rk, 7._rk, &
    1._rk, 6._rk, 7._rk, &
    4._rk, 6._rk, 7._rk, &
    0._rk, 2._rk, 8._rk, &
    5._rk, 2._rk, 8._rk, &
    0._rk, 7._rk, 8._rk, &
    5._rk, 7._rk, 8._rk &
  ], [3,2,2,3])

  call MakeGrid(Grid, nDims=3, iStart=[1,1,1], iEnd=[2,2,3], Coords=Coords, &
    GridType=GRID_TYPE_CURVILINEAR)

  ! Grid point (1,1,1)
  CellCoords = CoordsInCell(Grid, [1,1,1], [2._rk, 4._rk, 6._rk])
  @AssertEqual([0._rk, 0._rk, 0._rk], CellCoords, 1.e-10_rk)

  ! Grid point (2,1,1)
  CellCoords = CoordsInCell(Grid, [1,1,1], [3._rk, 4._rk, 6._rk])
  @AssertEqual([1._rk, 0._rk, 0._rk], CellCoords, 1.e-10_rk)

  ! Grid point (1,2,1)
  CellCoords = CoordsInCell(Grid, [1,1,1], [2._rk, 5._rk, 6._rk])
  @AssertEqual([0._rk, 1._rk, 0._rk], CellCoords, 1.e-10_rk)

  ! Grid point (2,2,1)
  CellCoords = CoordsInCell(Grid, [1,1,1], [3._rk, 5._rk, 6._rk])
  @AssertEqual([1._rk, 1._rk, 0._rk], CellCoords, 1.e-10_rk)

  ! Grid point (1,1,2)
  CellCoords = CoordsInCell(Grid, [1,1,1], [1._rk, 3._rk, 7._rk])
  @AssertEqual([0._rk, 0._rk, 1._rk], CellCoords, 1.e-10_rk)

  ! Grid point (2,1,2)
  CellCoords = CoordsInCell(Grid, [1,1,1], [4._rk, 3._rk, 7._rk])
  @AssertEqual([1._rk, 0._rk, 1._rk], CellCoords, 1.e-10_rk)

  ! Grid point (1,2,2)
  CellCoords = CoordsInCell(Grid, [1,1,1], [1._rk, 6._rk, 7._rk])
  @AssertEqual([0._rk, 1._rk, 1._rk], CellCoords, 1.e-10_rk)

  ! Grid point (2,2,2)
  CellCoords = CoordsInCell(Grid, [1,1,1], [4._rk, 6._rk, 7._rk])
  @AssertEqual([1._rk, 1._rk, 1._rk], CellCoords, 1.e-10_rk)

  ! Grid point (1,1,3)
  CellCoords = CoordsInCell(Grid, [1,1,2], [0._rk, 2._rk, 8._rk])
  @AssertEqual([0._rk, 0._rk, 1._rk], CellCoords, 1.e-10_rk)

  ! Grid point (2,1,3)
  CellCoords = CoordsInCell(Grid, [1,1,2], [5._rk, 2._rk, 8._rk])
  @AssertEqual([1._rk, 0._rk, 1._rk], CellCoords, 1.e-10_rk)

  ! Grid point (1,2,3)
  CellCoords = CoordsInCell(Grid, [1,1,2], [0._rk, 7._rk, 8._rk])
  @AssertEqual([0._rk, 1._rk, 1._rk], CellCoords, 1.e-10_rk)

  ! Grid point (2,2,3)
  CellCoords = CoordsInCell(Grid, [1,1,2], [5._rk, 7._rk, 8._rk])
  @AssertEqual([1._rk, 1._rk, 1._rk], CellCoords, 1.e-10_rk)

  ! Cell 1 center
  CellCoords = CoordsInCell(Grid, [1,1,1], [2.5_rk, 4.5_rk, 6.5_rk])
  @AssertEqual([0.5_rk, 0.5_rk, 0.5_rk], CellCoords, 1.e-10_rk)

  ! Cell 2 center
  CellCoords = CoordsInCell(Grid, [1,1,2], [2.5_rk, 4.5_rk, 7.5_rk])
  @AssertEqual([0.5_rk, 0.5_rk, 0.5_rk], CellCoords, 1.e-10_rk)

end subroutine ModGridTest_cell_coords_non_uniform_3d

@MPITest(npes=[1])
subroutine ModGridTest_cell_size_uniform_2d(this)

  use pFUnit_mod
  use ModGlobal
  use ModGrid
  use ModOverset
  implicit none

  class(MPITestCase), intent(inout) :: this

  real(rk), dimension(2,2,3) :: Coords
  type(t_grid) :: Grid
  real(rk), dimension(2) :: CellCoords

  Coords = reshape([ &
    2._rk, 4._rk, &
    3._rk, 4._rk, &
    2._rk, 5._rk, &
    3._rk, 5._rk, &
    2._rk, 6._rk, &
    3._rk, 6._rk &
  ], [2,2,3])

  call MakeGrid(Grid, nDims=2, iStart=[1,1], iEnd=[2,3], Coords=Coords, &
    GridType=GRID_TYPE_CARTESIAN)

  ! Cell 1
  @AssertEqual(1._rk, CellSize(Grid, [1,1]), 1.e-10_rk)

  ! Cell 2
  @AssertEqual(1._rk, CellSize(Grid, [1,2]), 1.e-10_rk)

end subroutine ModGridTest_cell_size_uniform_2d

@MPITest(npes=[1])
subroutine ModGridTest_cell_size_uniform_3d(this)

  use pFUnit_mod
  use ModGlobal
  use ModGrid
  use ModOverset
  implicit none

  class(MPITestCase), intent(inout) :: this

  real(rk), dimension(3,2,2,3) :: Coords
  type(t_grid) :: Grid
  real(rk), dimension(3) :: CellCoords

  Coords = reshape([ &
    2._rk, 4._rk, 6._rk, &
    3._rk, 4._rk, 6._rk, &
    2._rk, 5._rk, 6._rk, &
    3._rk, 5._rk, 6._rk, &
    2._rk, 4._rk, 7._rk, &
    3._rk, 4._rk, 7._rk, &
    2._rk, 5._rk, 7._rk, &
    3._rk, 5._rk, 7._rk, &
    2._rk, 4._rk, 8._rk, &
    3._rk, 4._rk, 8._rk, &
    2._rk, 5._rk, 8._rk, &
    3._rk, 5._rk, 8._rk &
  ], [3,2,2,3])

  call MakeGrid(Grid, nDims=3, iStart=[1,1,1], iEnd=[2,2,3], Coords=Coords, &
    GridType=GRID_TYPE_CARTESIAN)

  ! Cell 1
  @AssertEqual(1._rk, CellSize(Grid, [1,1,1]), 1.e-10_rk)

  ! Cell 2
  @AssertEqual(1._rk, CellSize(Grid, [1,1,2]), 1.e-10_rk)

end subroutine ModGridTest_cell_size_uniform_3d

@MPITest(npes=[1])
subroutine ModGridTest_cell_size_non_uniform_2d(this)

  use pFUnit_mod
  use ModGlobal
  use ModGrid
  use ModOverset
  implicit none

  class(MPITestCase), intent(inout) :: this

  real(rk), dimension(2,2,3) :: Coords
  type(t_grid) :: Grid
  real(rk), dimension(2) :: CellCoords

  Coords = reshape([ &
    2._rk, 4._rk, &
    3._rk, 4._rk, &
    1._rk, 5._rk, &
    4._rk, 5._rk, &
    0._rk, 6._rk, &
    5._rk, 6._rk &
  ], [2,2,3])

  call MakeGrid(Grid, nDims=2, iStart=[1,1], iEnd=[2,3], Coords=Coords, &
    GridType=GRID_TYPE_CURVILINEAR)

  ! Cell 1
  @AssertEqual(2._rk, CellSize(Grid, [1,1]), 1.e-10_rk)

  ! Cell 2
  @AssertEqual(4._rk, CellSize(Grid, [1,2]), 1.e-10_rk)

end subroutine ModGridTest_cell_size_non_uniform_2d

@MPITest(npes=[1])
subroutine ModGridTest_cell_size_non_uniform_3d(this)

  use pFUnit_mod
  use ModGlobal
  use ModGrid
  use ModOverset
  implicit none

  class(MPITestCase), intent(inout) :: this

  real(rk), dimension(3,2,2,3) :: Coords
  type(t_grid) :: Grid
  real(rk), dimension(3) :: CellCoords

  Coords = reshape([ &
    2._rk, 4._rk, 6._rk, &
    3._rk, 4._rk, 6._rk, &
    2._rk, 5._rk, 6._rk, &
    3._rk, 5._rk, 6._rk, &
    1._rk, 3._rk, 7._rk, &
    4._rk, 3._rk, 7._rk, &
    1._rk, 6._rk, 7._rk, &
    4._rk, 6._rk, 7._rk, &
    0._rk, 2._rk, 8._rk, &
    5._rk, 2._rk, 8._rk, &
    0._rk, 7._rk, 8._rk, &
    5._rk, 7._rk, 8._rk &
  ], [3,2,2,3])

  call MakeGrid(Grid, nDims=3, iStart=[1,1,1], iEnd=[2,2,3], Coords=Coords, &
    GridType=GRID_TYPE_CURVILINEAR)

  ! Cell 1
  @AssertEqual(13._rk/3._rk, CellSize(Grid, [1,1,1]), 1.e-10_rk)

  ! Cell 2
  @AssertEqual(49._rk/3._rk, CellSize(Grid, [1,1,2]), 1.e-10_rk)

end subroutine ModGridTest_cell_size_non_uniform_3d

@MPITest(npes=[1])
subroutine ModGridTest_avg_cell_size_around_pt_uniform_2d(this)

  use pFUnit_mod
  use ModGlobal
  use ModGrid
  use ModOverset
  implicit none

  class(MPITestCase), intent(inout) :: this

  integer(ik) :: i, j
  real(rk), dimension(2,3,3) :: Coords
  type(t_grid) :: Grid
  real(rk), dimension(2) :: CellCoords

  do j = 1, 3
    do i = 1, 3
      Coords(:,i,j) = [2._rk, 4._rk] + real([i,j]-1,kind=rk)
    end do
  end do

  ! Non-periodic
  call MakeGrid(Grid, nDims=2, iStart=[1,1], iEnd=[3,3], Coords=Coords, &
    GridType=GRID_TYPE_CARTESIAN)

  ! Center
  @AssertEqual(1._rk, AvgCellSizeAroundPoint(Grid, [2,2]), 1.e-10_rk)

  ! Edge
  @AssertEqual(1._rk, AvgCellSizeAroundPoint(Grid, [3,2]), 1.e-10_rk)

  ! Corner
  @AssertEqual(1._rk, AvgCellSizeAroundPoint(Grid, [3,3]), 1.e-10_rk)

  ! Periodic
  call MakeGrid(Grid, nDims=2, iStart=[1,1], iEnd=[3,3], Coords=Coords, &
    Periodic=[PLANE_PERIODIC,PLANE_PERIODIC], PeriodicStorage=OVERLAP_PERIODIC, &
    PeriodicLength=[2._rk, 2._rk], GridType=GRID_TYPE_CARTESIAN)

  ! Center
  @AssertEqual(1._rk, AvgCellSizeAroundPoint(Grid, [2,2]), 1.e-10_rk)

  ! Edge
  @AssertEqual(1._rk, AvgCellSizeAroundPoint(Grid, [3,2]), 1.e-10_rk)

  ! Corner
  @AssertEqual(1._rk, AvgCellSizeAroundPoint(Grid, [3,3]), 1.e-10_rk)

end subroutine ModGridTest_avg_cell_size_around_pt_uniform_2d

@MPITest(npes=[1])
subroutine ModGridTest_avg_cell_size_around_pt_uniform_3d(this)

  use pFUnit_mod
  use ModGlobal
  use ModGrid
  use ModOverset
  implicit none

  class(MPITestCase), intent(inout) :: this

  integer(ik) :: i, j, k
  real(rk), dimension(3,3,3,3) :: Coords
  type(t_grid) :: Grid
  real(rk), dimension(3) :: CellCoords

  do k = 1, 3
    do j = 1, 3
      do i = 1, 3
        Coords(:,i,j,k) = [2._rk, 4._rk, 6._rk] + real([i,j,k]-1,kind=rk)
      end do
    end do
  end do

  ! Non-periodic
  call MakeGrid(Grid, nDims=3, iStart=[1,1,1], iEnd=[3,3,3], Coords=Coords, &
    GridType=GRID_TYPE_CARTESIAN)

  ! Interior
  @AssertEqual(1._rk, AvgCellSizeAroundPoint(Grid, [2,2,2]), 1.e-10_rk)

  ! Face
  @AssertEqual(1._rk, AvgCellSizeAroundPoint(Grid, [3,2,2]), 1.e-10_rk)

  ! Edge
  @AssertEqual(1._rk, AvgCellSizeAroundPoint(Grid, [3,3,2]), 1.e-10_rk)

  ! Corner
  @AssertEqual(1._rk, AvgCellSizeAroundPoint(Grid, [3,3,3]), 1.e-10_rk)

  ! Periodic
  call MakeGrid(Grid, nDims=3, iStart=[1,1,1], iEnd=[3,3,3], Coords=Coords, &
    Periodic=[PLANE_PERIODIC,PLANE_PERIODIC,PLANE_PERIODIC], PeriodicStorage=OVERLAP_PERIODIC, &
    PeriodicLength=[2._rk, 2._rk, 2._rk], GridType=GRID_TYPE_CARTESIAN)

  ! Interior
  @AssertEqual(1._rk, AvgCellSizeAroundPoint(Grid, [2,2,2]), 1.e-10_rk)

  ! Face
  @AssertEqual(1._rk, AvgCellSizeAroundPoint(Grid, [3,2,2]), 1.e-10_rk)

  ! Edge
  @AssertEqual(1._rk, AvgCellSizeAroundPoint(Grid, [3,3,2]), 1.e-10_rk)

  ! Corner
  @AssertEqual(1._rk, AvgCellSizeAroundPoint(Grid, [3,3,3]), 1.e-10_rk)

end subroutine ModGridTest_avg_cell_size_around_pt_uniform_3d

@MPITest(npes=[1])
subroutine ModGridTest_avg_cell_size_around_pt_non_uniform_2d(this)

  use pFUnit_mod
  use ModGlobal
  use ModGrid
  use ModOverset
  implicit none

  class(MPITestCase), intent(inout) :: this

  integer(ik) :: i, j
  real(rk), dimension(2,3,3) :: Coords
  type(t_grid) :: Grid
  real(rk), dimension(2) :: CellCoords

  ! Cell sizes are:
  ! (1,1) -> 3/2
  ! (2,1) -> 3/2
  ! (1,2) -> 5/2
  ! (2,2) -> 5/2

  do j = 1, 3
    do i = 1, 3
      Coords(:,i,j) = [2._rk, 4._rk] + real([i,j]-1,kind=rk) + real([(i-2)*(j-1),0],kind=rk)
    end do
  end do

  ! Non-periodic
  call MakeGrid(Grid, nDims=2, iStart=[1,1], iEnd=[3,3], Coords=Coords, &
    GridType=GRID_TYPE_CURVILINEAR)

  ! Center
  @AssertEqual(2._rk, AvgCellSizeAroundPoint(Grid, [2,2]), 1.e-10_rk)

  ! Edge
  @AssertEqual(2._rk, AvgCellSizeAroundPoint(Grid, [3,2]), 1.e-10_rk)

  ! Corner
  @AssertEqual(2.5_rk, AvgCellSizeAroundPoint(Grid, [3,3]), 1.e-10_rk)

end subroutine ModGridTest_avg_cell_size_around_pt_non_uniform_2d

@MPITest(npes=[1])
subroutine ModGridTest_avg_cell_size_around_pt_non_uniform_3d(this)

  use pFUnit_mod
  use ModGlobal
  use ModGrid
  use ModOverset
  implicit none

  class(MPITestCase), intent(inout) :: this

  integer(ik) :: i, j, k
  real(rk), dimension(3,3,3,3) :: Coords
  type(t_grid) :: Grid
  real(rk), dimension(3) :: CellCoords

  ! Cell sizes are:
  ! (1,1,1) -> 7/3
  ! (2,1,1) -> 7/3
  ! (1,2,1) -> 7/3
  ! (2,2,1) -> 7/3
  ! (1,1,2) -> 19/3
  ! (2,1,2) -> 19/3
  ! (1,2,2) -> 19/3
  ! (2,2,2) -> 19/3

  do k = 1, 3
    do j = 1, 3
      do i = 1, 3
        Coords(:,i,j,k) = [2._rk, 4._rk, 6._rk] + real([i,j,k]-1,kind=rk) + &
          real([(i-2)*(k-1),(j-2)*(k-1),0],kind=rk)
      end do
    end do
  end do

  ! Non-periodic
  call MakeGrid(Grid, nDims=3, iStart=[1,1,1], iEnd=[3,3,3], Coords=Coords, &
    GridType=GRID_TYPE_CURVILINEAR)

  ! Interior
  @AssertEqual(13._rk/3._rk, AvgCellSizeAroundPoint(Grid, [2,2,2]), 1.e-10_rk)

  ! Face
  @AssertEqual(13._rk/3._rk, AvgCellSizeAroundPoint(Grid, [3,2,2]), 1.e-10_rk)

  ! Edge
  @AssertEqual(13._rk/3._rk, AvgCellSizeAroundPoint(Grid, [3,3,2]), 1.e-10_rk)

  ! Corner
  @AssertEqual(19._rk/3._rk, AvgCellSizeAroundPoint(Grid, [3,3,3]), 1.e-10_rk)

end subroutine ModGridTest_avg_cell_size_around_pt_non_uniform_3d

@MPITest(npes=[1])
subroutine ModGridTest_grid_mask(this)

  use pFUnit_mod
  use ModGlobal
  use ModGrid
  use ModMask
  implicit none

  class(MPITestCase), intent(inout) :: this

  integer(ik) :: i, j
  real(rk), dimension(2,3,3) :: Coords
  integer(ik), dimension(3,3) :: IBlank
  type(t_grid) :: Grid
  type(t_mask) :: GridMask
  logical, dimension(3,3,1) :: ExpectedValues

  do j = 1, 3
    do i = 1, 3
      Coords(:,i,j) = real([i,j]-1, kind=rk)
    end do
  end do

  IBlank(1,:) = 1
  IBlank(2,:) = -2
  IBlank(3,:) = 0

  call MakeGrid(Grid, nDims=2, iStart=[1,1], iEnd=[3,3], Coords=Coords, IBlank=IBlank, &
    GridType=GRID_TYPE_CARTESIAN, ID=1)

  call GenerateGridMask(Grid, GridMask)

  ExpectedValues = .true.
  ExpectedValues(3,:,1) = .false.

  @AssertTrue(all(ExpectedValues .eqv. GridMask%values))

end subroutine ModGridTest_grid_mask

@MPITest(npes=[1])
subroutine ModGridTest_boundary_mask(this)

  use pFUnit_mod
  use ModGlobal
  use ModGrid
  use ModMask
  implicit none

  class(MPITestCase), intent(inout) :: this

  integer(ik) :: i, j
  real(rk), dimension(2,3,3) :: Coords
  integer(ik), dimension(3,3) :: IBlank
  type(t_grid) :: Grid
  type(t_mask) :: BoundaryMask
  logical, dimension(3,3,1) :: ExpectedValues

  do j = 1, 3
    do i = 1, 3
      Coords(:,i,j) = real([i,j]-1, kind=rk)
    end do
  end do

  IBlank(:2,:) = 1
  IBlank(3,:) = 4

  call MakeGrid(Grid, nDims=2, iStart=[1,1], iEnd=[3,3], Coords=Coords, IBlank=IBlank, &
    GridType=GRID_TYPE_CARTESIAN, ID=1)

  call GenerateBoundaryMask(Grid, 4, BoundaryMask)

  ExpectedValues = .false.
  ExpectedValues(3,:,1) = .true.

  @AssertTrue(all(ExpectedValues .eqv. BoundaryMask%values))

end subroutine ModGridTest_boundary_mask
