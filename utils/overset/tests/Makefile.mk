# Copyright (c) 2015, University of Illinois at Urbana-Champaign, XPACC
# License: MIT, http://opensource.org/licenses/MIT
tests_pf_src = \
  tests/ModBoundingBoxTest.pf \
  tests/ModDonorsTest.pf \
  tests/ModGeometryTest.pf \
  tests/ModGridTest.pf \
  tests/ModGridUtilsTest.pf \
  tests/ModHashGridTest.pf \
  tests/ModInterpTest.pf \
  tests/ModMaskTest.pf \
  tests/ModOversetTest.pf \
  tests/ModPegasusTest.pf
