clear all
set(0,'defaultaxesfontname','times');
set(0,'defaultaxesfontsize',20);

% air properties
Ru = 8314.5; % universal gas constant
MW = 28.97;  % molecular weight
Rs = Ru/MW;  % specific gas constant

% temperature points from NACA data
temp2 = [500:500:15000]';

% reference viscosity
eta0 = [267, 416, 527, 619, 700, 772, 838, 899, 957, 1011, 1062, ...
        1112, 1159, 1204, 1247, 1289, 1330, 1370, 1408, 1446, 1482, ...
        1518, 1552, 1586, 1620, 1652, 1684, 1716, 1747, 1777].';
eta0 = eta0 * 1e-7;
    
% viscosity ratio
eta_ratio = [1, 1, 1, 1, 1, 1, 1.003, 1.016, 1.029, 1.043, 1.060, ...
             1.090, 1.139, 1.208, 1.283, 1.342, 1.386, 1.425, ...
             1.447, 1.460, 1.467, 1.464, 1.450, 1.425, 1.376, 1.312, ...
             1.230, 1.129, 0.968, 0.882].';
         
eta2 = eta_ratio .* eta0;

% reference conductivity
k0 = [364, 567, 719, 844, 954, 1053, 1143, 1227, 1305, 1379, 1449, ...
      1516, 1580, 1642, 1701, 1759, 1814, 1868, 1921, 1972, 2020, 2070, ...
      2120, 2160, 2210, 2250, 2300, 2340, 2380, 2420].';
k0 = k0 * 1e-4;
 
% conductivity ratio
k_ratio = [1.021, 1.100, 1.150, 1.177, 1.619, 3.20, 4.72, 2.99, 1.714, ...
           3.29, 5.99, 10.19, 14.50, 15.69, 12.24, 7.80, 5.10, 3.26, ...
           10.09, 15.44, 19.84, 25.1, 31.2, 38.2, 45.0, 52.2, 58.2, ...
           62.6, 65.3, 64.2].';
k2 = k_ratio .* k0;

% lower temperature points
temp1 = [0:50:450]';
Z1 = ones(size(temp1));
for n = 1:length(temp1)
    eta1 = 1.716e-5*(temp1/273.15).^(3/2)*(273.15+110.4)./(temp1+110.4);
      k1 = 2.414e-2*(temp1/273.15).^(3/2)*(273.15+194.4)./(temp1+194.4);
end

% merge them
temp   = [temp1 ; temp2];
eta    = [eta1 ; eta2];
k      = [k1; k2+3e-3];
lambda = (0.60 - 2/3)*eta;

% interpolate them onto a finer, uniformly spaced temperature
temp_final   = linspace(temp(1),temp(end),1000);
eta_final    = spline(temp, eta, temp_final);
lambda_final = spline(temp, lambda, temp_final);
k_final      = spline(temp, k, temp_final);

% rename everything
temp   = temp_final;
eta    = eta_final;
lambda = lambda_final;
k      = k_final;

figure(3), clf
plot(temp,eta/spline(temp, eta, 300),'k- '); hold on
plot(temp,0.1*k/spline(temp, k, 300),'r--');
xlabel('Temperature [K]');
ylabel('\mu/\mu_0, k/k_0 \times 10^{-1}');
legend('\mu/\mu_0','k/k_0');
print -depsc -f3 tv.eps

% now construct spline
eta_cs    = spline(temp,eta);
lambda_cs = spline(temp,lambda);
k_cs      = spline(temp,k);

% output
fid = fopen('Air_tv_spline.tbl','wb','ieee-be');
sizeof_int = 4;
sizeof_dbl = 8;

% output number of break points
fwrite(fid,sizeof_int,'int32');
fwrite(fid,length(temp),'int32');
fwrite(fid,sizeof_int,'int32');

% output break points
fwrite(fid,length(temp)*sizeof_dbl,'int32');
fwrite(fid,temp,'double');
fwrite(fid,length(temp)*sizeof_dbl,'int32');

% output eta
fwrite(fid,(length(temp)-1)*4*sizeof_dbl,'int32');
fwrite(fid,eta_cs.coefs,'double');
fwrite(fid,(length(temp)-1)*4*sizeof_dbl,'int32');

% output lambda
fwrite(fid,(length(temp)-1)*4*sizeof_dbl,'int32');
fwrite(fid,lambda_cs.coefs,'double');
fwrite(fid,(length(temp)-1)*4*sizeof_dbl,'int32');

% output k
fwrite(fid,(length(temp)-1)*4*sizeof_dbl,'int32');
fwrite(fid,k_cs.coefs,'double');
fwrite(fid,(length(temp)-1)*4*sizeof_dbl,'int32');

% done
fclose(fid);

