! Copyright (c) 2015, University of Illinois at Urbana-Champaign, XPACC
! License: MIT, http://opensource.org/licenses/MIT
!===================================================================================================
!
! ModBC: Utilities for writing/reading boundary condition files
!
! Usage:
!
! * Writing BCs:
!   (1) Create an array of type(t_bc) and fill it with the desired boundary conditions, e.g.:
!         BCs(1) = t_bc_(1, BC_TYPE_SAT_NOSLIP_ISOTHERMAL_WALL, 1, [1,1], [1,-1], [1,-1])
!         BCs(2) = t_bc_(1, BC_TYPE_SAT_FAR_FIELD, -1, [-1,-1], [1,-1], [1,-1])
!                        ^grid    ^BC type          ^dir  ^is,ie  ^js,je  ^ks,ke
!   (2) Call WriteBCs to write these boundary conditions to a file
!
! * Reading BCs:
!   (1) Create an allocatable array of type(t_bc)
!   (2) Call ReadBCs to allocate and fill this array with boundary conditions from a file
!
!===================================================================================================

module ModBC

  use ModStdIO
  implicit none

  private

  public :: t_bc
  public :: t_bc_

  public :: WriteBCs
  public :: ReadBCs

  public :: BC_TYPE_OUTER_BOUNDARY
  public :: BC_TYPE_SAT_SLIP_ADIABATIC_WALL
  public :: BC_TYPE_SAT_NOSLIP_ISOTHERMAL_WALL
  public :: BC_TYPE_SAT_NOSLIP_ADIABATIC_WALL
  public :: BC_TYPE_SAT_FAR_FIELD
  public :: BC_TYPE_SAT_BLOCK_INTERFACE
  public :: BC_TYPE_NSCBC_INFLOW_VELOCITY_TEMPERATURE
  public :: BC_TYPE_NSCBC_INFLOW_VELOCITY_TEMPERATURE_FROM_SPONGE
  public :: BC_TYPE_NSCBC_OUTFLOW_PERFECT_NONREFLECTION
  public :: BC_TYPE_NSCBC_OUTFLOW_PERFECT_REFLECTION
  public :: BC_TYPE_NSCBC_WALL_ADIABATIC_SLIP
  public :: BC_TYPE_NSCBC_WALL_ISOTHERMAL_NOSLIP
  public :: BC_TYPE_NSCBC_SUBSONIC_INFLOW
  public :: BC_TYPE_NSCBC_SUPERSONIC_INFLOW
  public :: BC_TYPE_PERIODICITY
  public :: BC_TYPE_FV_DIRICHLET
  public :: BC_TYPE_FV_NEUMANN
  public :: BC_TYPE_FV_SYMMETRY
  public :: BC_TYPE_FV_BLOCK_INTERFACE
  public :: BC_TYPE_FV_BLOCK_INTERFACE_PERIODIC
  public :: BC_TYPE_SPONGE
  public :: BC_TYPE_EF_DIRICHLET_G
  public :: BC_TYPE_EF_DIRICHLET_P
  public :: BC_TYPE_EF_NEUMANN
  public :: BC_TYPE_EF_SCHWARZ
  public :: BC_TYPE_EF_DIELECTRIC

  integer, parameter :: ik = selected_int_kind(9)

  integer(ik), parameter :: PATH_LENGTH = 256
  integer(ik), parameter :: LINE_LENGTH = 256

  integer(ik), parameter :: BCUnit = 480

  type t_bc
    integer(ik) :: grid_id
    integer(ik) :: bc_type
    integer(ik) :: dir
    integer(ik), dimension(2) :: irange
    integer(ik), dimension(2) :: jrange
    integer(ik), dimension(2) :: krange
  end type t_bc

  ! Trailing _ added for compatibility with compilers that don't support F2003 constructors
  interface t_bc_
    module procedure t_bc_Default
    module procedure t_bc_Assigned2D
    module procedure t_bc_Assigned3D
  end interface t_bc_

  integer(ik), parameter :: BC_TYPE_OUTER_BOUNDARY = 00
  integer(ik), parameter :: BC_TYPE_SAT_SLIP_ADIABATIC_WALL = 21
  integer(ik), parameter :: BC_TYPE_SAT_NOSLIP_ISOTHERMAL_WALL = 22
  integer(ik), parameter :: BC_TYPE_SAT_NOSLIP_ADIABATIC_WALL = 23
  integer(ik), parameter :: BC_TYPE_SAT_FAR_FIELD = 24
  integer(ik), parameter :: BC_TYPE_SAT_BLOCK_INTERFACE = 25
  integer(ik), parameter :: BC_TYPE_NSCBC_INFLOW_VELOCITY_TEMPERATURE = 31
  integer(ik), parameter :: BC_TYPE_NSCBC_INFLOW_VELOCITY_TEMPERATURE_FROM_SPONGE = 32
  integer(ik), parameter :: BC_TYPE_NSCBC_OUTFLOW_PERFECT_NONREFLECTION = 41
  integer(ik), parameter :: BC_TYPE_NSCBC_OUTFLOW_PERFECT_REFLECTION = 42
  integer(ik), parameter :: BC_TYPE_NSCBC_WALL_ADIABATIC_SLIP = 51
  integer(ik), parameter :: BC_TYPE_NSCBC_WALL_ISOTHERMAL_NOSLIP = 52
  integer(ik), parameter :: BC_TYPE_NSCBC_SUBSONIC_INFLOW = 61
  integer(ik), parameter :: BC_TYPE_NSCBC_SUPERSONIC_INFLOW = 62
  integer(ik), parameter :: BC_TYPE_PERIODICITY = 73
  integer(ik), parameter :: BC_TYPE_FV_DIRICHLET = 91
  integer(ik), parameter :: BC_TYPE_FV_NEUMANN = 92
  integer(ik), parameter :: BC_TYPE_FV_SYMMETRY = 93
  integer(ik), parameter :: BC_TYPE_FV_BLOCK_INTERFACE = 94
  integer(ik), parameter :: BC_TYPE_FV_BLOCK_INTERFACE_PERIODIC = 95
  integer(ik), parameter :: BC_TYPE_SPONGE = 99
  integer(ik), parameter :: BC_TYPE_EF_DIRICHLET_G = 600
  integer(ik), parameter :: BC_TYPE_EF_DIRICHLET_P = 601
  integer(ik), parameter :: BC_TYPE_EF_NEUMANN = 602
  integer(ik), parameter :: BC_TYPE_EF_SCHWARZ = 698
  integer(ik), parameter :: BC_TYPE_EF_DIELECTRIC = 699

contains

  function t_bc_Default() result(BC)

    type(t_bc) :: BC

    BC%grid_id = 1
    BC%bc_type = BC_TYPE_SAT_FAR_FIELD
    BC%dir = 2
    BC%irange = [1,1]
    BC%jrange = [1,-1]
    BC%krange = [1,-1]

  end function t_bc_Default

  function t_bc_Assigned2D(GridID, BCType, Direction, IRange, JRange) result(BC)

    integer(ik), intent(in) :: GridID
    integer(ik), intent(in) :: BCType
    integer(ik), intent(in) :: Direction
    integer(ik), dimension(2), intent(in) :: IRange, JRange

    type(t_bc) :: BC

    BC%grid_id = GridID
    BC%bc_type = BCType
    BC%dir = Direction
    BC%irange = IRange
    BC%jrange = JRange
    BC%krange = [1,-1]

  end function t_bc_Assigned2D

  function t_bc_Assigned3D(GridID, BCType, Direction, IRange, JRange, KRange) result(BC)

    integer(ik), intent(in) :: GridID
    integer(ik), intent(in) :: BCType
    integer(ik), intent(in) :: Direction
    integer(ik), dimension(2), intent(in) :: IRange, JRange, KRange

    type(t_bc) :: BC

    BC%grid_id = GridID
    BC%bc_type = BCType
    BC%dir = Direction
    BC%irange = IRange
    BC%jrange = JRange
    BC%krange = KRange

  end function t_bc_Assigned3D

  subroutine WriteBCs(BCs, BCFile)

    type(t_bc), dimension(:), intent(in) :: BCs
    character(len=*), intent(in) :: BCFile

    integer(ik) :: i
    integer(ik) :: Error

    open(BCUnit, file=BCFile, form='formatted', iostat=Error)

    if (Error /= 0) then
      write (ERROR_UNIT, '(3a)') "ERROR: Could not open BC file '", trim(BCFile), "'."
      stop 1
    end if

    write (BCUnit, '(a)') "#"
    write (BCUnit, '(a)') "# Basic boundary condition file for PlasComCM"
    write (BCUnit, '(a)') "#"
    write (BCUnit, '(a)') "# FD = finite difference"
    write (BCUnit, '(a)') "# FV = finite volume"
    write (BCUnit, '(a)') "#"
    write (BCUnit, '(a)') "# ibType"
    write (BCUnit, '(a)') "# ======"
    write (BCUnit, '(a)') "#     00     OUTER_BOUNDARY (FD)"
    write (BCUnit, '(a)') "#     21     SAT_SLIP_ADIABATIC_WALL (FD)"
    write (BCUnit, '(a)') "#     22     SAT_NOSLIP_ISOTHERMAL_WALL (FD)"
    write (BCUnit, '(a)') "#     23     SAT_NOSLIP_ADIABATIC_WALL (FD)"
    write (BCUnit, '(a)') "#     24     SAT_FAR_FIELD (FD)"
    write (BCUnit, '(a)') "#     25     SAT_BLOCK_INTERFACE (FD)"
    write (BCUnit, '(a)') "#     31     NSCBC_INFLOW_VELOCITY_TEMPERATURE (FD)"
    write (BCUnit, '(a)') "#     32     NSCBC_INFLOW_VELOCITY_TEMPERATURE_FROM_SPONGE (FD)"
    write (BCUnit, '(a)') "#     41     NSCBC_OUTFLOW_PERFECT_NONREFLECTION (FD, FV)"
    write (BCUnit, '(a)') "#     42     NSCBC_OUTFLOW_PERFECT_REFLECTION (FD, FV)"
    write (BCUnit, '(a)') "#     51     NSCBC_WALL_ADIABATIC_SLIP (FD, FV)"
    write (BCUnit, '(a)') "#     52     NSCBC_WALL_ISOTHERMAL_NOSLIP (FD, FV)"
    write (BCUnit, '(a)') "#     61     NSCBC_SUBSONIC_INFLOW (FD)"
    write (BCUnit, '(a)') "#     62     NSCBC_SUPERSONIC_INFLOW (FD, FV)"
    write (BCUnit, '(a)') "#     73     PERIODICITY (FV)"
    write (BCUnit, '(a)') "#     91     FV_DIRICHLET (FV)"
    write (BCUnit, '(a)') "#     92     FV_NEUMANN (FV)"
    write (BCUnit, '(a)') "#     93     FV_SYMMETRY (FV)"
    write (BCUnit, '(a)') "#     94     FV_BLOCK_INTERFACE (FV)"
    write (BCUnit, '(a)') "#     95     FV_BLOCK_INTERFACE_PERIODIC (FV)"
    write (BCUnit, '(a)') "#     99     SPONGE (FD, FV)"
    write (BCUnit, '(a)') "#"
    write (BCUnit, '(a)') "# Grid ibType ibDir   is    ie    js    je     ks    ke"
    write (BCUnit, '(a)') "# ==== ====== =====  ====  ====  ====  ====   ====  ===="

    do i = 1, size(BCs)
      write (BCUnit, '(9i6)') BCs(i)%grid_id, BCs(i)%bc_type, BCs(i)%dir, BCs(i)%irange, &
        BCs(i)%jrange, BCs(i)%krange
    end do

    close(BCUnit)

  end subroutine WriteBCs

  subroutine ReadBCs(BCFile, BCs)

    character(len=*), intent(in) :: BCFile
    type(t_bc), dimension(:), allocatable, intent(out) :: BCs

    integer(ik) :: i
    integer(ik) :: Error
    integer(ik) :: LineNumber
    integer(ik) :: nEntries
    character(len=LINE_LENGTH) :: EntryString
    integer(ik) :: EntryGridID
    integer(ik) :: EntryBCType
    integer(ik) :: EntryDirection
    integer(ik), dimension(2) :: EntryIRange, EntryJRange, EntryKRange

    open(BCUnit, file=BCFile, form='formatted', status='old', iostat=Error)

    if (Error /= 0) then
      write (ERROR_UNIT, '(3a)') "ERROR: Could not open BC file '", trim(BCFile), "'."
      stop 1
    end if

    LineNumber = 1
    nEntries = 0
    Error = 0
    do while (Error == 0)
      read (BCUnit, '(a)', iostat=Error) EntryString
      if (Error == 0) then
        if (EntryString(1:1) /= '#' .and. len_trim(EntryString) > 0) then
          read (EntryString, *, iostat=Error) EntryGridID, EntryBCType, EntryDirection, &
            EntryIRange, EntryJRange, EntryKRange
          if (Error == 0) then
            nEntries = nEntries + 1
          else
            write (ERROR_UNIT, '(a,i0,3a)') "ERROR: Invalid entry in line ", LineNumber, &
              " of BC file '", trim(BCFile), "'."
            stop 1
          end if
        end if
        LineNumber = LineNumber + 1
      end if
    end do

    allocate(BCs(nEntries))

    rewind(BCUnit)

    i = 1
    Error = 0
    do while (Error == 0)
      read (BCUnit, '(a)', iostat=Error) EntryString
      if (Error == 0) then
        if (EntryString(1:1) /= '#' .and. len_trim(EntryString) > 0) then
          read (EntryString, *, iostat=Error) EntryGridID, EntryBCType, EntryDirection, &
            EntryIRange, EntryJRange, EntryKRange
          BCs(i) = t_bc_(EntryGridID, EntryBCType, EntryDirection, EntryIRange, EntryJRange, &
            EntryKRange)
          i = i + 1
        end if
      end if
    end do

    close(BCUnit)

  end subroutine ReadBCs

end module ModBC
