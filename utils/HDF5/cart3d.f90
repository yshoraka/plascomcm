! Copyright (c) 2015, University of Illinois at Urbana-Champaign, XPACC
! License: MIT, http://opensource.org/licenses/MIT
Program Cart3D
  USE ModHDF5_IO, filename=>filename_HDF5, vars_to_write=>vars_to_write_HDF5, NGRID=>NGRID_HDF5, &
    NDIM=>NDIM_HDF5, nAuxVars=>nAuxVars_HDF5, ND=>ND_HDF5, X=>X_HDF5, IBLANK=>IBLANK_HDF5, &
    Q=>Q_HDF5, Qaux=>Qaux_HDF5, CONFIG=>CONFIG_HDF5
  Implicit None
  
  ! Call the routines
  Call Cart3D_Init
  Call Cart3D_Grid
  Call Cart3D_BC
  Call Write_HDF5
  Call Finalize

Contains


  ! ============== !
  ! Initialization !
  ! ============== !
  Subroutine Cart3D_Init
    Implicit None

    print *, '! ======================================================= !'
    print *, '!                                                         !'
    print *, '!            HDF5 CART3D GRID GENERATION                  !'
    print *, '!    Generates a Cartesian grid and boundary conditions   !'
    print *, '!    in HDF5 format                                       !'
    print *, '!                                                         !'
    print *, '! ======================================================= !'

    ! HDF5 I/O details
    Write(filename(1:18),'(A10,I8.8)') 'RocFlo-CM.', 0
    Write(filename(19:21),'(A3)') '.h5'

    Allocate(vars_to_write(1))
    vars_to_write(1:1) = (/ 'xyz' /)

    Return
  End Subroutine Cart3D_Init


  ! ==================== !
  ! Create the grid/mesh !
  ! ==================== !
  Subroutine Cart3D_Grid
    Implicit None

    Integer :: I, J, K

    ! Initialize the grids
    ngrid = 1
    NDIM = 3
    Allocate(ND(ngrid,3)); ND(:,3) = 1

    ! Number of grid points
    Write (*,'(A)') ''
    Write (*,'(A)') 'Input NX: '
    Read (*,*) ND(1,1)
    Write (*,'(A)') 'Input NY: '
    Read (*,*) ND(1,2)
    Write (*,'(A)') 'Input NZ: '
    Read (*,*) ND(1,3)

    ! Allocate the arrays and assign values
    ALLOCATE(X(ngrid,MAXVAL(ND(:,1)),MAXVAL(ND(:,2)),MAXVAL(ND(:,3)),3))

    ! Generate the grid
    Do K = 1, ND(1,3)
       DO J = 1, ND(1,2)
          Do I = 1, ND(1,1)
             ! Create X
             If (ND(1,1) .GT. 1) Then
                X(1,I,J,K,1) = 2.0_8*DBLE(I-1)/DBLE(ND(1,1)-1) - 1.0_8
             Else
                X(1,I,J,K,1) = 0D0
             End If
             ! Create Y
             If (ND(1,2) .GT. 1) Then
                X(1,I,J,K,2) = 2.0_8*DBLE(J-1)/DBLE(ND(1,2)-1) - 1.0_8
             Else
                X(1,I,J,K,2) = 0D0
             End If
             ! Create Z
             If (ND(1,3) .GT. 1) Then
                X(1,I,J,K,3) = 2.0_8*DBLE(K-1)/DBLE(ND(1,3)-1) - 1.0_8
             Else
                X(1,I,J,K,3) = 0D0
             End If
          End Do
       End Do
    End Do

    ! Assign values to the config array
    Allocate(CONFIG(1:NGRID))
    CONFIG(1)%t_iter = 0
    CONFIG(1)%NDIM   = NDIM
    CONFIG(1)%ND(1)  = ND(1,1)
    CONFIG(1)%ND(2)  = ND(1,2)
    CONFIG(1)%ND(3)  = ND(1,3)
    CONFIG(1)%Use_IB = 0
    CONFIG(1)%time   = 0D0
    CONFIG(1)%Re     = 1000.0_8
    CONFIG(1)%Pr     = 0.72_8
    CONFIG(1)%Sc     = 1.0D0

    Return
  End Subroutine Cart3D_Grid


  ! =================== !
  ! Boundary conditions !
  ! =================== !
  Subroutine Cart3D_BC
    Implicit None

    Integer :: iunit

    ! Open the file
    iunit=11
    open(iunit,file="bc.dat")
    
    Write (*,'(A)') ''
    print *, 'Writing boundary conditions'

    ! Write the header
    write(iunit,'(1a1 )') "#"
    write(iunit,'(1a45)') "# Basic boundary condition file for PlasComCM"
    write(iunit,'(1a1 )') "#"
    write(iunit,'(1a24)') "# FD = finite difference"
    write(iunit,'(1a20)') "# FV = finite volume"
    write(iunit,'(1a1 )') "#"
    write(iunit,'(1a8 )') "# ibType"
    write(iunit,'(1a8 )') "# ======"
    write(iunit,'(1a32)') "#     00     OUTER_BOUNDARY (FD)"
    write(iunit,'(1a41)') "#     21     SAT_SLIP_ADIABATIC_WALL (FD)"
    write(iunit,'(1a44)') "#     22     SAT_NOSLIP_ISOTHERMAL_WALL (FD)"
    write(iunit,'(1a33)') "#     23     SAT_NOSLIP_ADIABATIC_WALL (FD)"
    write(iunit,'(1a31)') "#     24     SAT_FAR_FIELD (FD)"
    write(iunit,'(1a37)') "#     25     SAT_BLOCK_INTERFACE (FD)"
    write(iunit,'(1a51)') "#     31     NSCBC_INFLOW_VELOCITY_TEMPERATURE (FD)"
    write(iunit,'(1a63)') "#     32     NSCBC_INFLOW_VELOCITY_TEMPERATURE_FROM_SPONGE (FD)"
    write(iunit,'(1a57)') "#     41     NSCBC_OUTFLOW_PERFECT_NONREFLECTION (FD, FV)"
    write(iunit,'(1a54)') "#     42     NSCBC_OUTFLOW_PERFECT_REFLECTION (FD, FV)"
    write(iunit,'(1a47)') "#     51     NSCBC_WALL_ADIABATIC_SLIP (FD, FV)"
    write(iunit,'(1a50)') "#     52     NSCBC_WALL_ISOTHERMAL_NOSLIP (FD, FV)"
    write(iunit,'(1a39)') "#     61     NSCBC_SUBSONIC_INFLOW (FD)"
    write(iunit,'(1a45)') "#     62     NSCBC_SUPERSONIC_INFLOW (FD, FV)"
    write(iunit,'(1a29)') "#     73     PERIODICITY (FV)"
    write(iunit,'(1a30)') "#     91     FV_DIRICHLET (FV)"
    write(iunit,'(1a28)') "#     92     FV_NEUMANN (FV)"
    write(iunit,'(1a29)') "#     93     FV_SYMMETRY (FV)"
    write(iunit,'(1a36)') "#     94     FV_BLOCK_INTERFACE (FV)"
    write(iunit,'(1a45)') "#     95     FV_BLOCK_INTERFACE_PERIODIC (FV)"
    write(iunit,'(1a28)') "#     99     SPONGE (FD, FV)"
    write(iunit,'(1a1 )') "#"
    write(iunit,'(1a56)') "# Grid ibType ibDir   is    ie    js    je     ks    ke "
    write(iunit,'(1a56)') "# ==== ====== =====  ====  ====  ====  ====   ====  ===="

    ! Input the boundary conditions
    ! SAT far-field
    If (ND(1,1).gt.1) Then
       write(iunit,'(9I6)')      1,   24,     1,    1,     1,    1,   -1,    1,    -1
       write(iunit,'(9I6)')      1,   24,    -1,   -1,    -1,    1,   -1,    1,    -1
    End iF
    If (ND(1,2).gt.1) Then
       write(iunit,'(9I6)')      1,   24,     2,    1,    -1,    1,    1,    1,    -1
       write(iunit,'(9I6)')      1,   24,    -2,    1,    -1,   -1,   -1,    1,    -1
    End If
    If (ND(1,3).gt.1) Then
       write(iunit,'(9I6)')      1,   24,     3,    1,    -1,    1,   -1,    1,     1
       write(iunit,'(9I6)')      1,   24,    -3,    1,    -1,    1,   -1,   -1,    -1
    End if

    ! Close the file
    close(iunit)

    Return
  End Subroutine Cart3D_BC

  ! ================== !
  ! Clean up and leave !
  ! ================== !
  Subroutine Finalize
    Implicit None
  
    Deallocate(ND,X)

    Return
  End Subroutine Finalize

End Program Cart3D
