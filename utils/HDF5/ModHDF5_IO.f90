! Copyright (c) 2015, University of Illinois at Urbana-Champaign, XPACC
! License: MIT, http://opensource.org/licenses/MIT
Module ModHDF5_IO
  USE ModStdIO
  USE hdf5
  Implicit None

  ! Global I/O variables
  Character(len=3), Dimension(:), Pointer :: vars_to_write_HDF5
  Character(len=80) :: filename_HDF5

  ! Global simulation variables
  Integer :: NGRID_HDF5, NDIM_HDF5, nAuxVars_HDF5
  Integer, Dimension(:,:), Pointer :: ND_HDF5
  Integer, Dimension(:,:,:,:), Pointer :: IBLANK_HDF5
  Real(KIND=8), Dimension(:,:,:,:,:), Pointer :: X_HDF5, Q_HDF5, Qaux_HDF5
  TYPE :: t_CONFIG_HDF5
     Integer :: t_iter
     Integer :: NDIM
     Integer :: ND(3)
     Integer :: Use_IB
     Real(KIND=8) :: time
     Real(KIND=8) :: Re
     Real(KIND=8) :: Pr
     Real(KIND=8) :: Sc
  END TYPE t_CONFIG_HDF5
  TYPE(t_CONFIG_HDF5), POINTER :: CONFIG_HDF5(:)

  integer, parameter :: XDMF_VAR_TYPE_INTEGER = 1
  integer, parameter :: XDMF_VAR_TYPE_FLOAT = 2

  private :: rk
  integer, parameter :: rk = selected_real_kind(15, 307)

  private :: m_file_id
  Integer(hid_t)   :: m_file_id

  private :: MAX_ND
  integer, parameter :: MAX_ND = 3

  private :: XYZNames
  character(len=1), dimension(MAX_ND), parameter :: XYZNames = ["X", "Y", "Z"]

Contains

  ! =================== !
  ! WRITE THE HDF5 FILE !
  ! =================== !
  Subroutine Write_HDF5
    Implicit None

    integer :: i
    logical :: WriteXYZ, WriteIBlank, WriteCV, WriteCVTarget, WriteAux, WriteAuxTarget
    real(rk), dimension(:,:,:,:,:), pointer :: QTarget
    real(rk), dimension(:,:,:,:,:), pointer :: QAuxTarget
    integer :: TimestepIndex
    real(rk) :: Time
    real(rk) :: Re, Pr, Sc
    integer :: idot
    Character(LEN=80) :: xmlFileName
    character(len=3) :: var_to_write
    integer :: nXDMFVars
    character(len=3), dimension(:), pointer :: VarNames
    integer, dimension(:), pointer :: VarTypes
    integer, dimension(:), pointer :: nVarComponents
    integer :: NextVar

    ! Nullify pointers if their corresponding var name is not in vars_to_write

    WriteXYZ = .false.
    WriteIBlank = .false.
    WriteCV = .false.
    WriteCVTarget = .false.
    WriteAux = .false.
    WriteAuxTarget = .false.

    do i = 1, size(vars_to_write_HDF5)
      select case (vars_to_write_HDF5(i))
      case ("xyz")
        WriteXYZ = .true.
        if (CONFIG_HDF5(1)%Use_IB == 1) then
          WriteIBlank = .true.
        end if
      case ("cv ")
        WriteCV = .true.
      case ("cvt")
        WriteCVTarget = .true.
      case ("aux")
        WriteAux = .true.
      case ("aut")
        WriteAuxTarget = .true.
      end select
    end do

    if (.not. WriteXYZ) then
      nullify(X_HDF5)
    end if
    if (.not. WriteIBlank) then
      nullify(IBLANK_HDF5)
    end if
    if (.not. WriteCV) then
      nullify(Q_HDF5)
    end if
    if (.not. WriteAux) then
      nullify(Qaux_HDF5)
    end if

    ! Point QTarget and QAuxTarget at Q and Qaux if their var name is found in vars_to_write
    nullify(QTarget)
    nullify(QAuxTarget)
    if (WriteCVTarget) then
      QTarget => Q_HDF5
    end if
    if (WriteAuxTarget) then
      QAuxTarget => Qaux_HDF5
    end if

    ! Assume these are the same for all grids
    TimestepIndex = CONFIG_HDF5(1)%t_iter
    Time = CONFIG_HDF5(1)%time
    Re = CONFIG_HDF5(1)%Re
    Pr = CONFIG_HDF5(1)%Pr
    Sc = CONFIG_HDF5(1)%Sc

    call WriteHDF5File(filename_HDF5, nDims=NDIM_HDF5, nGrids=NGRID_HDF5, nPoints=ND_HDF5, &
      nAuxVars=nAuxVars_HDF5, X=X_HDF5, IBlank=IBLANK_HDF5, Q=Q_HDF5, QTarget=QTarget, &
      QAux=Qaux_HDF5, QAuxTarget=QauxTarget, TimestepIndex=TimestepIndex, Time=Time, Re=Re, &
      Pr=Pr, Sc=Sc)

    ! Add .xmf extension to file name
    idot = index(filename_HDF5,'.',BACK=.true.)
    xmlFileName = trim(filename_HDF5(1:idot-1)) // '.xmf'

    nXDMFVars = size(vars_to_write_HDF5)
    if (WriteXYZ) then
      nXDMFVars = nXDMFVars - 1
    end if

    allocate(VarNames(nXDMFVars))
    allocate(VarTypes(nXDMFVars))
    allocate(nVarComponents(nXDMFVars))

    NextVar = 1
    do i = 1, size(vars_to_write_HDF5)
      var_to_write = vars_to_write_HDF5(i)
      select case (var_to_write)
      case ('cv', 'cvt')
        VarNames(NextVar) = var_to_write
        VarTypes(NextVar) = XDMF_VAR_TYPE_FLOAT
        nVarComponents(NextVar) = NDIM_HDF5+2
        NextVar = NextVar + 1
      case ('aux', 'aut')
        VarNames(NextVar) = var_to_write
        VarTypes(NextVar) = XDMF_VAR_TYPE_FLOAT
        nVarComponents(NextVar) = nAuxVars_HDF5
        NextVar = NextVar + 1
      end select
    end do

    call WriteXDMFFile(xmlFilename, nDims=NDIM_HDF5, nGrids=NGRID_HDF5, nPoints=ND_HDF5, &
      HDF5File=filename_HDF5, HasIBlank=WriteIBlank, Time=Time, VarNames=VarNames, &
      VarTypes=VarTypes, nVarComponents=nVarComponents)

    deallocate(VarNames)
    deallocate(VarTypes)
    deallocate(nVarComponents)

    Return
  End Subroutine Write_HDF5


  subroutine WriteHDF5File(HDF5File, nDims, nGrids, nPoints, nAuxVars, X, IBlank, Q, QTarget, &
    QAux, QAuxTarget, TimestepIndex, Time, Re, Pr, Sc)

    character(len=*), intent(in) :: HDF5File
    integer, intent(in) :: nDims
    integer, intent(in) :: nGrids
    integer, dimension(:,:), pointer, intent(in) :: nPoints
    integer, intent(in), optional :: nAuxVars
    real(rk), dimension(:,:,:,:,:), pointer, intent(in), optional :: X
    integer, dimension(:,:,:,:), pointer, intent(in), optional :: IBlank
    real(rk), dimension(:,:,:,:,:), pointer, intent(in), optional :: Q
    real(rk), dimension(:,:,:,:,:), pointer, intent(in), optional :: QTarget
    real(rk), dimension(:,:,:,:,:), pointer, intent(in), optional :: QAux
    real(rk), dimension(:,:,:,:,:), pointer, intent(in), optional :: QAuxTarget
    integer, intent(in), optional :: TimestepIndex
    real(rk), intent(in), optional :: Time
    real(rk), intent(in), optional :: Re, Pr, Sc

    integer :: i, m
    integer :: nAuxVars_
    logical :: WriteXYZ, WriteIBlank, WriteCV, WriteCVTarget, WriteAux, WriteAuxTarget
    integer :: Error
    integer(hid_t) :: IntegerTypeID
    integer(hid_t) :: RealTypeID
    integer(hid_t) :: FileID
    integer(hid_t) :: GroupID
    integer(hid_t) :: AttributeID
    integer(hid_t) :: DatasetID
    integer(hid_t) :: DataspaceID
    integer(hsize_t) :: nData(MAX_ND)
    real(rk) :: TimestepIndexReal
    integer :: useIB
    real(rk), dimension(4) :: GroupHeader
    integer, dimension(MAX_ND) :: nPointsBuffer
    character(len=16) :: GroupName
    character(len=16) :: DatasetName
    real(rk), dimension(:,:,:), allocatable :: RealBuffer
    integer, dimension(:,:,:), allocatable :: IntegerBuffer

    if (present(X)) then
      WriteXYZ = associated(X)
    else
      WriteXYZ = .false.
    end if

    if (present(IBlank)) then
      WriteIBlank = associated(IBlank)
    else
      WriteIBlank = .false.
    end if

    if (present(Q)) then
      WriteCV = associated(Q)
    else
      WriteCV = .false.
    end if

    if (present(QTarget)) then
      WriteCVTarget = associated(QTarget)
    else
      WriteCVTarget = .false.
    end if

    if (present(QAux) .or. present(QAuxTarget)) then
      if (present(nAuxVars)) then
        nAuxVars_ = nAuxVars
      else if (present(QAux)) then
        nAuxVars_ = size(QAux,5)
      else
        nAuxVars_ = size(QAuxTarget,5)
      end if
    else
      nAuxVars_ = 0
    end if

    if (present(QAux)) then
      WriteAux = associated(QAux)
    else
      WriteAux = .false.
    end if

    if (present(QAuxTarget)) then
      WriteAuxTarget = associated(QAuxTarget)
    else
      WriteAuxTarget = .false.
    end if

    GroupHeader = 0._rk
    if (present(Sc)) then
      GroupHeader(1) = Sc
    end if
    if (present(Pr)) then
      GroupHeader(2) = Pr
    end if
    if (present(Re)) then
      GroupHeader(3) = Re
    end if
    if (present(Time)) then
      GroupHeader(4) = Time
    end if

    write (*, '(3a)') "Writing ", trim(HDF5File), "..."
    write (*, '(a,i0,a)', advance='no') "Format: ", nDims, "D, "
    if (WriteXYZ) then
      write (*, '(a)', advance='no') "xyz "
    end if
    if (WriteIBlank) then
      write (*, '(a)', advance='no') "iblank "
    end if
    if (WriteCV) then
      write (*, '(a)', advance='no') "cv "
    end if
    if (WriteCVTarget) then
      write (*, '(a)', advance='no') "cvt "
    end if
    if (WriteAux) then
      write (*, '(a)', advance='no') "aux "
    end if
    if (WriteAuxTarget) then
      write (*, '(a)', advance='no') "aut "
    end if
    write (*, '(a)') ""

    call h5open_f(Error)
    if (Error /= 0) then
      write (ERROR_UNIT, '(a)') "ERROR: Could not initialize HDF5 Fortran interface."
      stop 2
    end if

    IntegerTypeID = H5T_NATIVE_INTEGER
    RealTypeID = H5T_NATIVE_DOUBLE

    call h5fcreate_f(HDF5File, H5F_ACC_TRUNC_F, FileID, Error)
    if (Error /= 0) then
      write (ERROR_UNIT, '(3a)') "ERROR: Could not create HDF5 file '", trim(HDF5File), "'."
      stop 2
    end if

    ! Write timestep index
    if (present(TimestepIndex)) then
      TimestepIndexReal = real(TimestepIndex, kind=rk)
    else
      TimestepIndexReal = 0._rk
    end if
    call h5screate_simple_f(1, [1_hsize_t], DataspaceID, Error)
    call h5acreate_f(FileID, "HEADER", RealTypeID, DataspaceID, AttributeID, Error)
    call h5awrite_f(AttributeID, RealTypeID, TimestepIndexReal, [1_hsize_t], Error)
    call h5aclose_f(AttributeID, Error)
    call h5sclose_f(DataspaceID, Error)

    ! Write nGrids
    call h5screate_simple_f(1, [1_hsize_t], DataspaceID, Error)
    call h5acreate_f(FileID, "numberOfGrids", IntegerTypeID, DataspaceID, AttributeID, Error)
    call h5awrite_f(AttributeID, IntegerTypeID, nGrids, [1_hsize_t], Error)
    call h5aclose_f(AttributeID, Error)
    call h5sclose_f(DataspaceID, Error)

    do m = 1, nGrids

      write (*, '(a,i0,a)', advance='no') "Grid ", m, ": "

      write (GroupName, '(a,i3.3)') "Group", m
      call h5gcreate_f(FileID, GroupName, GroupID, Error)

      ! Write the group header (Sc, Pr, Re, Time)
      call h5screate_simple_f(1, [4_hsize_t], DataspaceID, Error)
      call h5acreate_f(GroupID, "HEADER", RealTypeID, DataspaceID, AttributeID, Error)
      call h5awrite_f(AttributeID, RealTypeID, GroupHeader, [4_hsize_t], Error)
      call h5aclose_f(AttributeID, Error)
      call h5sclose_f(DataspaceID, Error)

      ! Write flag specifying whether file contains IBlank
      useIB = merge(1, 0, WriteIBlank)
      call h5screate_simple_f(1, [1_hsize_t], DataspaceID, Error)
      call h5acreate_f(GroupID, "useIB", IntegerTypeID, DataspaceID, AttributeID, Error)
      call h5awrite_f(AttributeID, IntegerTypeID, UseIB, [1_hsize_t], Error)
      call h5aclose_f(AttributeID, Error)
      call h5sclose_f(DataspaceID, Error)

      ! Write nPoints
      call h5screate_simple_f(1, [int(nDims,kind=hsize_t)], DataspaceID, Error)
      call h5acreate_f(GroupID, "gridSize", IntegerTypeID, DataspaceID, AttributeID, Error)
      nPointsBuffer = nPoints(m,:)
      call h5awrite_f(AttributeID, IntegerTypeID, nPointsBuffer, [int(nDims,kind=hsize_t)], Error)
      call h5aclose_f(AttributeID, Error)
      call h5sclose_f(DataspaceID, Error)

      ! Write number of auxiliary variables
      call h5screate_simple_f(1, [1_hsize_t], DataspaceID, Error)
      call h5acreate_f(GroupID, "numberOfAuxVars", IntegerTypeID, DataspaceID, AttributeID, Error)
      call h5awrite_f(AttributeID, IntegerTypeID, nAuxVars_, [1_hsize_t], Error)
      call h5aclose_f(AttributeID, Error)
      call h5sclose_f(DataspaceID, Error)

      allocate(RealBuffer(nPoints(m,1),nPoints(m,2),nPoints(m,3)))
      allocate(IntegerBuffer(nPoints(m,1),nPoints(m,2),nPoints(m,3)))

      nData = nPoints(m,:)
      call h5screate_simple_f(nDims, nData, DataspaceID, Error)

      ! Write grid coordinates if present
      if (WriteXYZ) then
        do i = 1, nDims
          RealBuffer = X(m,:nPoints(m,1),:nPoints(m,2),:nPoints(m,3),i)
          call h5dcreate_f(GroupID, XYZNames(i), RealTypeID, DataspaceID, DatasetID, Error)
          call h5dwrite_f(DatasetID, RealTypeID, RealBuffer, nData, Error)
          call h5dclose_f(DatasetID, Error)
        end do
        write (*, '(a)', advance='no') "xyz "
      end if

      ! Write IBlank if present
      if (WriteIBlank) then
        IntegerBuffer = IBlank(m,:nPoints(m,1),:nPoints(m,2),:nPoints(m,3))
        call h5dcreate_f(GroupID, "IBLANK", IntegerTypeID, DataspaceID, DatasetID, Error)
        call h5dwrite_f(DatasetID, IntegerTypeID, IntegerBuffer, nData, Error)
        call h5dclose_f(DatasetID, Error)
        write (*, '(a)', advance='no') "iblank "
      end if

      ! Write solution if present
      if (WriteCV) then
        do i = 1, nDims+2
          write (DatasetName, '(a,i2.2)') "cv", i
          RealBuffer = Q(m,:nPoints(m,1),:nPoints(m,2),:nPoints(m,3),i)
          call h5dcreate_f(GroupID, DatasetName, RealTypeID, DataspaceID, DatasetID, Error)
          call h5dwrite_f(DatasetID, RealTypeID, RealBuffer, nData, Error)
          call h5dclose_f(DatasetID, Error)
        end do
        write (*, '(a)', advance='no') "cv "
      end if

      ! Write target solution if present
      if (WriteCVTarget) then
        do i = 1, nDims+2
          write (DatasetName, '(a,i2.2)') "cvt", i
          RealBuffer = QTarget(m,:nPoints(m,1),:nPoints(m,2),:nPoints(m,3),i)
          call h5dcreate_f(GroupID, DatasetName, RealTypeID, DataspaceID, DatasetID, Error)
          call h5dwrite_f(DatasetID, RealTypeID, RealBuffer, nData, Error)
          call h5dclose_f(DatasetID, Error)
        end do
        write (*, '(a)', advance='no') "cvt "
      end if

      ! Write auxiliary variables if present
      if (WriteAux) then
        do i = 1, nAuxVars_
          write (DatasetName, '(a,i2.2)') "aux", i
          RealBuffer = QAux(m,:nPoints(m,1),:nPoints(m,2),:nPoints(m,3),i)
          call h5dcreate_f(GroupID, DatasetName, RealTypeID, DataspaceID, DatasetID, Error)
          call h5dwrite_f(DatasetID, RealTypeID, RealBuffer, nData, Error)
          call h5dclose_f(DatasetID, Error)
        end do
        write (*, '(a)', advance='no') "aux "
      end if

      ! Write auxiliary target if present
      if (WriteAuxTarget) then
        do i = 1, nAuxVars_
          write (DatasetName, '(a,i2.2)') "aut", i
          RealBuffer = QAuxTarget(m,:nPoints(m,1),:nPoints(m,2),:nPoints(m,3),i)
          call h5dcreate_f(GroupID, DatasetName, RealTypeID, DataspaceID, DatasetID, Error)
          call h5dwrite_f(DatasetID, RealTypeID, RealBuffer, nData, Error)
          call h5dclose_f(DatasetID, Error)
        end do
        write (*, '(a)', advance='no') "aut "
      end if

      write (*, '(a)') ""

      call h5sclose_f(DataspaceID, Error)

      deallocate(RealBuffer)
      deallocate(IntegerBuffer)

      call h5gclose_f(GroupID, Error)

    end do

    call h5fclose_f(FileID, Error)

  end subroutine WriteHDF5File


  subroutine ReadHDF5File(HDF5File, nDims, nGrids, nPoints, nAuxVars, X, IBlank, Q, QTarget, QAux, &
    QAuxTarget, TimestepIndex, Time, Re, Pr, Sc)

    character(len=*), intent(in) :: HDF5File
    integer, intent(out) :: nDims
    integer, intent(out) :: nGrids
    integer, dimension(:,:), pointer, intent(out) :: nPoints
    integer, intent(out), optional :: nAuxVars
    real(rk), dimension(:,:,:,:,:), pointer, intent(out), optional :: X
    integer, dimension(:,:,:,:), pointer, intent(out), optional :: IBlank
    real(rk), dimension(:,:,:,:,:), pointer, intent(out), optional :: Q
    real(rk), dimension(:,:,:,:,:), pointer, intent(out), optional :: QTarget
    real(rk), dimension(:,:,:,:,:), pointer, intent(out), optional :: QAux
    real(rk), dimension(:,:,:,:,:), pointer, intent(out), optional :: QAuxTarget
    integer, intent(out), optional :: TimestepIndex
    real(rk), intent(out), optional :: Time
    real(rk), intent(out), optional :: Re, Pr, Sc

    integer :: i, m
    integer :: Error
    integer(hid_t) :: IntegerTypeID
    integer(hid_t) :: RealTypeID
    integer(hid_t) :: FileID
    integer(hid_t) :: GroupID
    integer(hid_t) :: AttributeID
    integer(hid_t) :: DatasetID
    integer(hid_t) :: DataspaceID
    integer(hsize_t) :: nData(MAX_ND)
    integer(hsize_t) :: nDataMax(MAX_ND)
    real(rk) :: TimestepIndexReal
    integer :: nAuxVars_
    logical :: ReadX, ReadIBlank, ReadCV, ReadCVTarget, ReadAux, ReadAuxTarget
    real(rk), dimension(4) :: GroupHeader
    integer, dimension(MAX_ND) :: nPointsBuffer
    character(len=16) :: GroupName
    character(len=16) :: DatasetName
    real(rk), dimension(:,:,:), allocatable :: RealBuffer
    integer, dimension(:,:,:), allocatable :: IntegerBuffer

    nullify(nPoints)
    if (present(X)) nullify(X)
    if (present(IBlank)) nullify(IBlank)
    if (present(Q)) nullify(Q)
    if (present(QTarget)) nullify(QTarget)
    if (present(QAux)) nullify(QAux)
    if (present(QAuxTarget)) nullify(QAuxTarget)

    call h5open_f(Error)
    if (Error /= 0) then
      write (ERROR_UNIT, '(a)') "ERROR: Could not initialize HDF5 Fortran interface."
      stop 2
    end if

    IntegerTypeID = H5T_NATIVE_INTEGER
    RealTypeID = H5T_NATIVE_DOUBLE

    call h5fopen_f(HDF5File, H5F_ACC_RDONLY_F, FileID, Error)
    if (Error /= 0) then
      write (ERROR_UNIT, '(3a)') "ERROR: Could not open HDF5 file '", trim(HDF5File), "'."
      stop 2
    end if

    ! Read timestep index
    call h5aopen_f(FileID, "HEADER", AttributeID, Error)
    call h5aread_f(AttributeID, RealTypeID, TimestepIndexReal, [1_hsize_t], Error)
    call h5aclose_f(AttributeID, Error)
    if (present(TimestepIndex)) then
      TimestepIndex = int(TimestepIndexReal)
    end if

    ! Read nGrids
    call h5aopen_f(FileID, "numberOfGrids", AttributeID, Error)
    call h5aread_f(AttributeID, IntegerTypeID, nGrids, [1_hsize_t], Error)
    call h5aclose_f(AttributeID, Error)

    call h5gopen_f(FileID, "Group001", GroupID, Error)

    ! Read Re, Pr, Sc, and time
    call h5aopen_f(GroupID, "HEADER", AttributeID, Error)
    call h5aread_f(AttributeID, RealTypeID, GroupHeader, [4_hsize_t], Error)
    call h5aclose_f(AttributeID, Error)
    if (present(Sc)) then
      Sc = GroupHeader(1)
    end if
    if (present(Pr)) then
      Pr = GroupHeader(2)
    end if
    if (present(Re)) then
      Re = GroupHeader(3)
    end if
    if (present(Time)) then
      Time = GroupHeader(4)
    end if

    ! Figure out nDims
    call h5aopen_f(GroupID, "gridSize", AttributeID, Error)
    call h5aget_space_f(AttributeID, DataspaceID, Error)
    call h5sget_simple_extent_dims_f(DataspaceID, nData, nDataMax, Error)
    nDims = nData(1)
    call h5sclose_f(DataspaceID, Error)
    call h5aclose_f(AttributeID, Error)

    ! Check if grid coordinates are present
    call h5lexists_f(GroupID, XYZNames(1), ReadX, Error)
    ReadX = ReadX .and. present(X)

    ! Check if IBlank values are present
    call h5lexists_f(GroupID, "IBLANK", ReadIBlank, Error)
    ReadIBlank = ReadIBlank .and. present(IBlank)

    ! Check if solution is present
    call h5lexists_f(GroupID, "cv01", ReadCV, Error)
    ReadCV = ReadCV .and. present(Q)

    ! Check if target solution is present
    call h5lexists_f(GroupID, "cvt01", ReadCVTarget, Error)
    ReadCVTarget = ReadCVTarget .and. present(QTarget)

    ! Check if auxiliary variables are present
    call h5lexists_f(GroupID, "aux01", ReadAux, Error)
    ReadAux = ReadAux .and. present(QAux)

    ! Check if auxiliary target is present
    call h5lexists_f(GroupID, "aut01", ReadAuxTarget, Error)
    ReadAuxTarget = ReadAuxTarget .and. present(QAuxTarget)

    ! Read nAuxVars
    call h5aopen_f(GroupID, "numberOfAuxVars", AttributeID, Error)
    call h5aread_f(AttributeID, IntegerTypeID, nAuxVars_, [1_hsize_t], Error)
    call h5aclose_f(AttributeID, Error)
    if (present(nAuxVars)) then
      nAuxVars = nAuxVars_
    end if

    call h5gclose_f(GroupID, Error)

    ! Read nPoints
    allocate(nPoints(nGrids,MAX_ND))
    nPoints = 1
    do m = 1, nGrids
      write (GroupName, '(a,i3.3)') "Group", m
      call h5gopen_f(FileID, GroupName, GroupID, Error)
      call h5aopen_f(GroupID, "gridSize", AttributeID, Error)
      call h5aread_f(AttributeID, IntegerTypeID, nPointsBuffer, [int(nDims,kind=hsize_t)], Error)
      nPoints(m,:nDims) = nPointsBuffer(:nDims)
      call h5aclose_f(AttributeID, Error)
      call h5gclose_f(GroupID, Error)
    end do

    if (ReadX) then
      allocate(X(nGrids,maxval(nPoints(:,1)),maxval(nPoints(:,2)),maxval(nPoints(:,3)),nDims))
    end if

    if (ReadIBlank) then
      allocate(IBlank(nGrids,maxval(nPoints(:,1)),maxval(nPoints(:,2)),maxval(nPoints(:,3))))
    end if

    if (ReadCV) then
      allocate(Q(nGrids,maxval(nPoints(:,1)),maxval(nPoints(:,2)),maxval(nPoints(:,3)),nDims+2))
    end if

    if (ReadCVTarget) then
      allocate(QTarget(nGrids,maxval(nPoints(:,1)),maxval(nPoints(:,2)),maxval(nPoints(:,3)),nDims+2))
    end if

    if (ReadAux) then
      allocate(QAux(nGrids,maxval(nPoints(:,1)),maxval(nPoints(:,2)),maxval(nPoints(:,3)),nAuxVars_))
    end if

    if (ReadAuxTarget) then
      allocate(QAuxTarget(nGrids,maxval(nPoints(:,1)),maxval(nPoints(:,2)),maxval(nPoints(:,3)),nAuxVars_))
    end if

    do m = 1, nGrids

      write (GroupName, '(a,i3.3)') "Group", m
      call h5gopen_f(FileID, GroupName, GroupID, Error)

      allocate(RealBuffer(nPoints(m,1),nPoints(m,2),nPoints(m,3)))
      allocate(IntegerBuffer(nPoints(m,1),nPoints(m,2),nPoints(m,3)))

      ! Read grid coordinates if present
      if (ReadX) then
        X(m,:,:,:,:) = 0._rk
        do i = 1, nDims
          call h5dopen_f(GroupID, XYZNames(i), DatasetID, Error)
          nData = nPoints(m,:)
          call h5dread_f(DatasetID, RealTypeID, RealBuffer, nData, Error)
          call h5dclose_f(DatasetID, Error)
          X(m,:nPoints(m,1),:nPoints(m,2),:nPoints(m,3),i) = RealBuffer
        end do
      end if

      ! Read IBlank if present
      if (ReadIBlank) then
        IBlank(m,:,:,:) = 0
        call h5dopen_f(GroupID, "IBLANK", DatasetID, Error)
        nData = nPoints(m,:)
        call h5dread_f(DatasetID, IntegerTypeID, IntegerBuffer, nData, Error)
        call h5dclose_f(DatasetID, Error)
        IBlank(m,:nPoints(m,1),:nPoints(m,2),:nPoints(m,3)) = IntegerBuffer
      end if

      ! Read solution if present
      if (ReadCV) then
        Q(m,:,:,:,:) = 0._rk
        do i = 1, nDims+2
          write (DatasetName, '(a,i2.2)') "cv", i
          call h5dopen_f(GroupID, DatasetName, DatasetID, Error)
          nData = nPoints(m,:)
          call h5dread_f(DatasetID, RealTypeID, RealBuffer, nData, Error)
          call h5dclose_f(DatasetID, Error)
          Q(m,:nPoints(m,1),:nPoints(m,2),:nPoints(m,3),i) = RealBuffer
        end do
      end if

      ! Read target solution if present
      if (ReadCVTarget) then
        QTarget(m,:,:,:,:) = 0._rk
        do i = 1, nDims+2
          write (DatasetName, '(a,i2.2)') "cvt", i
          call h5dopen_f(GroupID, DatasetName, DatasetID, Error)
          nData = nPoints(m,:)
          call h5dread_f(DatasetID, RealTypeID, RealBuffer, nData, Error)
          call h5dclose_f(DatasetID, Error)
          QTarget(m,:nPoints(m,1),:nPoints(m,2),:nPoints(m,3),i) = RealBuffer
        end do
      end if

      ! Read auxiliary variables if present
      if (ReadAux) then
        QAux(m,:,:,:,:) = 0._rk
        do i = 1, nAuxVars_
          write (DatasetName, '(a,i2.2)') "aux", i
          call h5dopen_f(GroupID, DatasetName, DatasetID, Error)
          nData = nPoints(m,:)
          call h5dread_f(DatasetID, RealTypeID, RealBuffer, nData, Error)
          call h5dclose_f(DatasetID, Error)
          QAux(m,:nPoints(m,1),:nPoints(m,2),:nPoints(m,3),i) = RealBuffer
        end do
      end if

      ! Read auxiliary target if present
      if (ReadAuxTarget) then
        QAuxTarget(m,:,:,:,:) = 0._rk
        do i = 1, nAuxVars_
          write (DatasetName, '(a,i2.2)') "aut", i
          call h5dopen_f(GroupID, DatasetName, DatasetID, Error)
          nData = nPoints(m,:)
          call h5dread_f(DatasetID, RealTypeID, RealBuffer, nData, Error)
          call h5dclose_f(DatasetID, Error)
          QAuxTarget(m,:nPoints(m,1),:nPoints(m,2),:nPoints(m,3),i) = RealBuffer
        end do
      end if

      deallocate(RealBuffer)
      deallocate(IntegerBuffer)

      call h5gclose_f(GroupID, Error)

    end do

    call h5fclose_f(FileID, Error)

  end subroutine ReadHDF5File

  subroutine WriteXDMFFile(XDMFFile, nDims, nGrids, nPoints, HDF5File, HDF5GridFile, HasIBlank, &
    Time, VarNames, VarTypes, nVarComponents)

    character(len=*), intent(in) :: XDMFFile
    integer, intent(in) :: nDims
    integer, intent(in) :: nGrids
    integer, dimension(:,:), pointer, intent(in) :: nPoints
    character(len=*), intent(in) :: HDF5File
    character(len=*), intent(in), optional :: HDF5GridFile
    logical, intent(in), optional :: HasIBlank
    real(rk), intent(in), optional :: Time
    character(len=3), dimension(:), pointer, intent(in), optional :: VarNames
    integer, dimension(:), pointer, intent(in), optional :: VarTypes
    integer, dimension(:), pointer, intent(in), optional :: nVarComponents

    integer :: i, j, m
    character(len=80) :: HDF5GridFile_
    logical :: HasIBlank_
    real(rk) :: Time_
    integer, dimension(MAX_ND) :: nPointsReversed
    character(len=16) :: GroupName
    character(len=16) :: VarName
    character(len=16) :: TopologyType
    character(len=16) :: GeometryType
    character(len=16) :: GridType
    character(len=16) :: NumberType
    character(len=32) :: pformat
    integer :: Prec

    integer, parameter :: XMFUnit = 8736

    if (present(HDF5GridFile)) then
      HDF5GridFile_ = HDF5GridFile
    else
      HDF5GridFile_ = HDF5File
    end if

    if (present(HasIBlank)) then
      HasIBlank_ = HasIBlank
    else
      HasIBlank_ = .false.
    end if

    if (present(Time)) then
      Time_ = Time
    else
      Time_ = 0._rk
    end if

    select case (nDims)
    case (2)
      TopologyType = '2DSMesh'
      GeometryType = 'X_Y'
    case (3)
      TopologyType = '3DSMesh'
      GeometryType = 'X_Y_Z'
    end select

    ! Write format string for DataItem tags
    write (pformat,'("(a,",i1,"(i0,1x),i0,3a,i0,a)")') nDims-1

    open (XMFUnit, file=XDMFFile)

    ! Write the file header
    write (XMFUnit, '(a)') '<?xml version="1.0" ?>'
    write (XMFUnit, '(a)') '<!DOCTYPE Xdmf SYSTEM "Xdmf.dtd" []>'
    write (XMFUnit, '(a)') '<Xdmf Version="2.0">'

    ! Begin describing the domain
    write (XMFUnit, '(a)') ' <Domain>'

    do m = 1, nGrids

      write (GroupName, '(a,i3.3)') "Group", m

      nPointsReversed = 1
      nPointsReversed(:nDims) = nPoints(m,nDims:1:-1)

      ! Begin describing the current grid
      write (XMFUnit, '(a,i0,1x,a)') '   <Grid Name="mesh',m,'" GridType="Uniform">'

      ! Write the time value
      write (XMFUnit, '(a,1pe13.5,a)') '       <Time Value="',Time_,'" />'

      ! Write the geometry info
      write (XMFUnit,'(3a,2(i0,1x),i0,a)') '     <Topology TopologyType="',trim(TopologyType), &
        '" NumberOfElements="',nPointsReversed,'"/>'
      write (XMFUnit,'(3a)') '     <Geometry GeometryType="',trim(GeometryType),'">'
      do i = 1, nDims
        write (XMFUnit,trim(pformat)) '       <DataItem Dimensions="',nPointsReversed(1:nDims), &
          '" NumberType="', 'Float', '" Precision="', 8, '" Format="HDF">'
        write (XMFUnit,'(6a)') '        ',trim(HDF5GridFile_),':/',trim(GroupName),'/',trim(XYZNames(i))
        write (XMFUnit,'(a)') '       </DataItem>'
      end do
      write (XMFUnit,'(a)') '     </Geometry>'

      ! Write the IBlank tag if needed
      if (HasIBlank_) then
        write (XMFUnit,'(a)') '     <Attribute Name="IBLANK" AttributeType="Scalar" Center="Node">'
        write (XMFUnit,trim(pformat)) '       <DataItem Dimensions="',nPointsReversed(1:nDims), &
          '" NumberType="', 'Int', '" Precision="', 4, '" Format="HDF">'
        write (XMFUnit,'(5a)') '        ',trim(HDF5GridFile_),':/',trim(GroupName),'/IBLANK'
        write (XMFUnit,'(a)') '       </DataItem>'
        write (XMFUnit,'(a)') '     </Attribute>'
      end if

      ! Describe variables defined on the grid (cv, aux, etc.)
      if (present(VarNames)) then

        do i = 1, size(VarNames)

          select case (VarTypes(i))
          case (XDMF_VAR_TYPE_INTEGER)
            NumberType = 'Int'
            Prec = 4
          case (XDMF_VAR_TYPE_FLOAT)
            NumberType = 'Float'
            Prec = 8
          end select

          do j = 1, nVarComponents(i)
            write (VarName,'(a,I2.2)') trim(VarNames(i)), j
            write (XMFUnit,'(3a)') '     <Attribute Name="',trim(VarName),'" AttributeType="Scalar" Center="Node">'
            write (XMFUnit,trim(pformat)) '       <DataItem Dimensions="',nPointsReversed(1:nDims), &
              '" NumberType="', trim(NumberType), '" Precision="', Prec, '" Format="HDF">'
            write (XMFUnit,'(6a)') '        ',trim(HDF5File),':/',trim(GroupName),'/',trim(VarName)
            write (XMFUnit,'(a)') '       </DataItem>'
            write (XMFUnit,'(a)') '     </Attribute>'
          end do

        end do

      end if

      ! Finished describing the grid
      write (XMFUnit,'(a)') '   </Grid>'

    end do

    ! Finished describing the domain
    write (XMFUnit,'(a)') ' </Domain>'

    ! Write file footer
    write (XMFUnit,'(a)') '</Xdmf>'

    close (XMFUnit)

  end subroutine WriteXDMFFile

End Module ModHDF5_IO
