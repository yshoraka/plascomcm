! Copyright (c) 2015, University of Illinois at Urbana-Champaign, XPACC
! License: MIT, http://opensource.org/licenses/MIT
!===================================================================================================
!
! HDF5ToPLOT3D: HDF5 to PLOT3D file conversion utility
!
! Usage: ./h5-to-p3d <Options> <HDF5 file>
!
! Options:
!   --base-name (-b) Base name of resulting PLOT3D files
!
!===================================================================================================

program HDF5ToPLOT3D

  use ModParseArguments
  use ModStdIO
  use ModPLOT3D_IO
  use ModHDF5_IO
  implicit none

  integer, parameter :: rk = selected_real_kind(15, 307)

  integer, parameter :: MAX_ND = 3

  integer, parameter :: PATH_LENGTH = 256
  integer, parameter :: VAR_NAME_LENGTH = 32

  integer :: i, j
  character(len=CMD_ARG_LENGTH), dimension(:), allocatable :: RawArguments
  type(t_cmd_opt), dimension(1) :: Options
  character(len=CMD_ARG_LENGTH), dimension(:), allocatable :: Arguments
  character(len=PATH_LENGTH) :: HDF5File
  character(len=PATH_LENGTH) :: PLOT3DGridFile
  character(len=PATH_LENGTH) :: PLOT3DSolutionFile
  character(len=PATH_LENGTH) :: PLOT3DTargetFile
  character(len=PATH_LENGTH) :: PLOT3DAuxFile
  character(len=PATH_LENGTH) :: PLOT3DAuxTargetFile
  logical :: Exists
  integer :: nDims
  integer :: nGrids
  integer :: nAuxVars
  integer, dimension(:,:), pointer :: nPoints
  real(rk), dimension(:,:,:,:,:), pointer :: X
  real(rk), dimension(:,:,:,:,:), pointer :: XCopy
  integer, dimension(:,:,:,:), pointer :: IBlank
  real(rk), dimension(:,:,:,:,:), pointer :: Q
  real(rk), dimension(:,:,:,:,:), pointer :: QTarget
  real(rk), dimension(:,:,:,:,:), pointer :: QAux
  real(rk), dimension(:,:,:,:,:), pointer :: QAuxTarget
  real(rk), dimension(:,:,:,:,:), pointer :: QCopy
  integer :: TimestepIndex
  real(rk) :: Time
  real(rk) :: Re
  integer :: BaseNameBegin, BaseNameEnd
  character(len=PATH_LENGTH) :: BaseName
  character(len=2) :: UseIBlank, CoordPrecision, GridFormat, VolumeFormat
  real(rk), dimension(4) :: Tau

  allocate(RawArguments(command_argument_count()))
  do i = 1, size(RawArguments)
    call get_command_argument(i, RawArguments(i))
  end do

  Options(1) = t_cmd_opt_("base-name", "b", OPT_VALUE_TYPE_STRING)

  call ParseArguments(RawArguments, Options=Options, Arguments=Arguments, MinArguments=1, &
    MaxArguments=1)

  HDF5File = Arguments(1)
  inquire(file=HDF5File, exist=Exists)
  if (.not. Exists) then
    write (ERROR_UNIT, '(3a)') "ERROR: File '", trim(HDF5File), "' does not exist."
    stop 1
  end if

  if (Options(1)%is_present) then
    BaseName = Options(1)%value
  else
    BaseNameBegin = scan(trim(HDF5File), '/', back=.true.) + 1
    BaseNameEnd = scan(trim(HDF5File), '.', back=.true.) - 1
    BaseName = HDF5File(BaseNameBegin:BaseNameEnd)
  end if

  PLOT3DGridFile = trim(BaseName) // ".xyz"
  PLOT3DSolutionFile = trim(BaseName) // ".q"
  PLOT3DTargetFile = trim(BaseName) // ".target.q"
  PLOT3DAuxFile = trim(BaseName) // ".aux.q"
  PLOT3DAuxTargetFile = trim(BaseName) // ".target.aux.q"

  call ReadHDF5File(HDF5File, nDims=nDims, nGrids=nGrids, nPoints=nPoints, nAuxVars=nAuxVars, X=X, &
    IBlank=IBlank, Q=Q, QTarget=QTarget, QAux=QAux, QAuxTarget=QAuxTarget, &
    TimestepIndex=TimestepIndex, Time=Time, Re=Re)

  ! Always write PLOT3D in 3D format, even if the problem is 2D
  if (nDims == 2) then

    ! Add z coordinate to X
    if (associated(X)) then
      allocate(XCopy(nGrids,size(X,2),size(X,3),size(X,4),2))
      XCopy = X
      deallocate(X)
      allocate(X(nGrids,size(XCopy,2),size(XCopy,3),size(XCopy,4),3))
      X(:,:,:,:,:2) = XCopy
      X(:,:,:,:,3) = 0._rk
      deallocate(XCopy)
    end if

    ! Add z momentum to Q
    if (associated(Q)) then
      allocate(QCopy(nGrids,size(Q,2),size(Q,3),size(Q,4),4))
      QCopy = Q
      deallocate(Q)
      allocate(Q(nGrids,size(QCopy,2),size(QCopy,3),size(QCopy,4),5))
      Q(:,:,:,:,:3) = QCopy(:,:,:,:,:3)
      Q(:,:,:,:,4) = 0._rk
      Q(:,:,:,:,5) = QCopy(:,:,:,:,4)
      deallocate(QCopy)
    end if

    ! Add z momentum to QTarget
    if (associated(QTarget)) then
      allocate(QCopy(nGrids,size(QTarget,2),size(QTarget,3),size(QTarget,4),4))
      QCopy = QTarget
      deallocate(QTarget)
      allocate(QTarget(nGrids,size(QCopy,2),size(QCopy,3),size(QCopy,4),5))
      QTarget(:,:,:,:,:3) = QCopy(:,:,:,:,:3)
      QTarget(:,:,:,:,4) = 0._rk
      QTarget(:,:,:,:,5) = QCopy(:,:,:,:,4)
      deallocate(QCopy)
    end if

  end if

  CoordPrecision = "d"
  GridFormat = "m"
  VolumeFormat = "w"

  if (associated(X)) then

    if (associated(IBlank)) then
      UseIBlank = "y"
    else
      UseIBlank = "n"
    end if

    call Write_Grid(MAX_ND, nGrids, nPoints, X, IBlank, CoordPrecision, GridFormat, VolumeFormat, &
      UseIBlank, PLOT3DGridFile)

  end if

  Tau(1) = real(TimestepIndex, kind=rk)
  Tau(2) = 0._rk
  Tau(3) = Re
  Tau(4) = Time

  if (associated(Q)) then
    call Write_Soln(MAX_ND, nGrids, nPoints, Q, Tau, CoordPrecision, GridFormat, VolumeFormat, &
      PLOT3DSolutionFile)
  end if

  if (associated(QTarget)) then
    call Write_Soln(MAX_ND, nGrids, nPoints, QTarget, Tau, CoordPrecision, GridFormat, VolumeFormat, &
      PLOT3DTargetFile)
  end if

  if (associated(QAux)) then
    call Write_Func(nGrids, nPoints, QAux, PLOT3DAuxFile)
  end if

  if (associated(QAuxTarget)) then
    call Write_Func(nGrids, nPoints, QAuxTarget, PLOT3DAuxTargetFile)
  end if

  if (associated(nPoints)) deallocate(nPoints)
  if (associated(X)) deallocate(X)
  if (associated(IBlank)) deallocate(IBlank)
  if (associated(Q)) deallocate(Q)
  if (associated(QTarget)) deallocate(QTarget)
  if (associated(QAux)) deallocate(QAux)
  if (associated(QAuxTarget)) deallocate(QAuxTarget)

end program HDF5ToPLOT3D
