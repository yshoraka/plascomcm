! Copyright (c) 2015, University of Illinois at Urbana-Champaign, XPACC
! License: MIT, http://opensource.org/licenses/MIT
!===================================================================================================
!
! CompareHDF5: PlasComCM HDF5 file comparison utility
!
! Usage: ./compare-h5 <Options> <File1> <File2>
!
! Options:
!   --compare-mode (-m) Method for comparison of floating point data sets ('inf' or a number >= 1,
!                       corresponding to the type of norm to be used; default: 'inf')
!   --tolerance (-t)    Maximum error value allowed in floating point data comparisons
!
! Notes:
!   * Returns 0 and prints nothing if no differences are found; returns 1 and prints differences if
!     any are found; returns 2 on error
!   * Uses AlmostEqual for real-valued dataset comparisons (see ModCompare for details)
!
! Limitations:
!   * Currently ignores HEADER
!
!===================================================================================================

program CompareHDF5

  use ModCompare
  use ModParseArguments
  use ModStdIO
  use ModHDF5_IO
  implicit none

  integer, parameter :: rk = selected_real_kind(15, 307)
  integer, parameter :: ik = selected_int_kind(9)

  integer(ik), parameter :: MAX_ND = 3

  integer(ik), parameter :: PATH_LENGTH = 256
  integer(ik), parameter :: VAR_NAME_LENGTH = 32

  integer(ik) :: i, j, k
  character(len=CMD_ARG_LENGTH), dimension(:), allocatable :: RawArguments
  type(t_cmd_opt), dimension(2) :: Options
  character(len=CMD_ARG_LENGTH), dimension(:), allocatable :: Arguments
  integer(ik) :: CompareNorm
  integer(ik) :: P
  integer(ik) :: Error
  character(len=PATH_LENGTH) :: File1, File2
  real(rk) :: Tolerance
  logical :: UseTolerance
  logical :: Exists
  integer(ik) :: nDims1, nDims2
  integer(ik) :: nGrids1, nGrids2
  integer(ik) :: nAuxVars1, nAuxVars2
  integer(ik), dimension(:,:), pointer :: nPoints1, nPoints2
  real(rk), dimension(:,:,:,:,:), pointer :: X1, X2
  integer(ik), dimension(:,:,:,:), pointer :: IBlank1, IBlank2
  real(rk), dimension(:,:,:,:,:), pointer :: Q1, Q2
  real(rk), dimension(:,:,:,:,:), pointer :: QTarget1, QTarget2
  real(rk), dimension(:,:,:,:,:), pointer :: QAux1, QAux2
  real(rk), dimension(:,:,:,:,:), pointer :: QAuxTarget1, QAuxTarget2
  character(len=VAR_NAME_LENGTH), dimension(MAX_ND) :: XNames
  character(len=VAR_NAME_LENGTH), dimension(MAX_ND+2) :: QNames
  real(rk), dimension(:,:,:), allocatable :: Values1, Values2
  integer(ik), dimension(:,:,:), allocatable :: IBlankValues1, IBlankValues2
  real(rk) :: Difference
  logical :: Equal

  XNames = ['x', 'y', 'z']

  allocate(RawArguments(command_argument_count()))
  do i = 1, size(RawArguments)
    call get_command_argument(i, RawArguments(i))
  end do

  Options(1) = t_cmd_opt_("compare-mode", "m", OPT_VALUE_TYPE_STRING)
  Options(2) = t_cmd_opt_("tolerance", "t", OPT_VALUE_TYPE_REAL)

  call ParseArguments(RawArguments, Options=Options, Arguments=Arguments, MinArguments=2, &
    MaxArguments=2)

  if (Options(1)%is_present) then
    select case (Options(1)%value)
    case ("inf")
      CompareNorm = COMPARE_NORM_INF
    case default
      CompareNorm = COMPARE_NORM_P
      read (Options(1)%value, *, iostat=Error) P
      if (Error /= 0) then
        write (ERROR_UNIT, '(3a)') "ERROR: Unrecognized comparison mode '", &
          trim(Options(1)%value), "'."
        stop 2
      end if
    end select
  else
    CompareNorm = COMPARE_NORM_INF
  end if

  if (Options(2)%is_present) then
    read (Options(2)%value, *, iostat=Error) Tolerance
    UseTolerance = .true.
  else
    UseTolerance = .false.
  end if

  File1 = Arguments(1)
  inquire(file=File1, exist=Exists)
  if (.not. Exists) then
    write (ERROR_UNIT, '(3a)') "ERROR: File '", trim(File1), "' does not exist."
    stop 2
  end if

  File2 = Arguments(2)
  inquire(file=File2, exist=Exists)
  if (.not. Exists) then
    write (ERROR_UNIT, '(3a)') "ERROR: File '", trim(File2), "' does not exist."
    stop 2
  end if

  call ReadHDF5File(File1, nDims=nDims1, nGrids=nGrids1, nPoints=nPoints1, nAuxVars=nAuxVars1, &
    X=X1, IBlank=IBlank1, Q=Q1, QTarget=QTarget1, QAux=QAux1, QAuxTarget=QAuxTarget1)
  call ReadHDF5File(File2, nDims=nDims2, nGrids=nGrids2, nPoints=nPoints2, nAuxVars=nAuxVars2, &
    X=X2, IBlank=IBlank2, Q=Q2, QTarget=QTarget2, QAux=QAux2, QAuxTarget=QAuxTarget2)

  if (nDims1 /= nDims2) then
    write (*, '(a)') "Files have different dimension."
    write (*, '(a,i0)') "File 1: ", nDims1
    write (*, '(a,i0)') "File 2: ", nDims2
    stop 1
  end if

  if (nGrids1 /= nGrids2) then
    write (*, '(a)') "Files have different numbers of grids."
    write (*, '(a,i0)') "File 1: ", nGrids1
    write (*, '(a,i0)') "File 2: ", nGrids2
    stop 1
  end if

  if (nAuxVars1 /= nAuxVars2) then
    write (*, '(a)') "Files have different numbers of auxiliary variables."
    write (*, '(a,i0)') "File 1: ", nAuxVars1
    write (*, '(a,i0)') "File 2: ", nAuxVars2
    stop 1
  end if

  do i = 1, nGrids1
    if (any(nPoints1(i,:) /= nPoints2(i,:))) then
      write (*, '(a)') "Files have different numbers of points."
      write (*, '(a,i0,a,i0,a,i0)') "File 1: ", nPoints1(i,1), ", ", nPoints1(i,2), ", ", &
        nPoints1(i,3)
      write (*, '(a,i0,a,i0,a,i0)') "File 2: ", nPoints2(i,1), ", ", nPoints2(i,2), ", ", &
        nPoints2(i,3)
      stop 1
    end if
  end do

  if (associated(X1) .neqv. associated(X2)) then
    write (*, '(a)') "Files do not have the same format."
    if (associated(X1)) then
      write (*, '(a)') "File 1: with grid"
    else
      write (*, '(a)') "File 1: without grid"
    end if
    if (associated(X2)) then
      write (*, '(a)') "File 2: with grid"
    else
      write (*, '(a)') "File 2: without grid"
    end if
    stop 1
  end if

  if (associated(IBlank1) .neqv. associated(IBlank2)) then
    write (*, '(a)') "Files do not have the same format."
    if (associated(IBlank1)) then
      write (*, '(a)') "File 1: with IBlank"
    else
      write (*, '(a)') "File 1: without IBlank"
    end if
    if (associated(IBlank2)) then
      write (*, '(a)') "File 2: with IBlank"
    else
      write (*, '(a)') "File 2: without IBlank"
    end if
    stop 1
  end if

  if (associated(Q1) .neqv. associated(Q2)) then
    write (*, '(a)') "Files do not have the same format."
    if (associated(Q1)) then
      write (*, '(a)') "File 1: with solution"
    else
      write (*, '(a)') "File 1: without solution"
    end if
    if (associated(Q2)) then
      write (*, '(a)') "File 2: with solution"
    else
      write (*, '(a)') "File 2: without solution"
    end if
    stop 1
  end if

  if (associated(QTarget1) .neqv. associated(QTarget2)) then
    write (*, '(a)') "Files do not have the same format."
    if (associated(QTarget1)) then
      write (*, '(a)') "File 1: with target"
    else
      write (*, '(a)') "File 1: without target"
    end if
    if (associated(QTarget2)) then
      write (*, '(a)') "File 2: with target"
    else
      write (*, '(a)') "File 2: without target"
    end if
    stop 1
  end if

  if (associated(QAux1) .neqv. associated(QAux2)) then
    write (*, '(a)') "Files do not have the same format."
    if (associated(QAux1)) then
      write (*, '(a)') "File 1: with auxiliary variables"
    else
      write (*, '(a)') "File 1: without auxiliary variables"
    end if
    if (associated(QAux2)) then
      write (*, '(a)') "File 2: with auxiliary variables"
    else
      write (*, '(a)') "File 2: without auxiliary variables"
    end if
    stop 1
  end if

  if (associated(QAuxTarget1) .neqv. associated(QAuxTarget2)) then
    write (*, '(a)') "Files do not have the same format."
    if (associated(QAuxTarget1)) then
      write (*, '(a)') "File 1: with auxiliary target"
    else
      write (*, '(a)') "File 1: without auxiliary target"
    end if
    if (associated(QAuxTarget2)) then
      write (*, '(a)') "File 2: with auxiliary target"
    else
      write (*, '(a)') "File 2: without auxiliary target"
    end if
    stop 1
  end if

  if (associated(X1)) then
    do i = 1, nGrids1
      allocate(Values1(nPoints1(i,1),nPoints1(i,2),nPoints1(i,3)))
      allocate(Values2(nPoints2(i,1),nPoints2(i,2),nPoints2(i,3)))
      do j = 1, nDims1
        Values1 = X1(i,:nPoints1(i,1),:nPoints1(i,2),:nPoints1(i,3),j)
        Values2 = X2(i,:nPoints2(i,1),:nPoints2(i,2),:nPoints2(i,3),j)
        if (UseTolerance) then
          Equal = AlmostEqual(Values1, Values2, CompareNorm=CompareNorm, P=P, Tolerance=Tolerance, &
            Difference=Difference)
        else
          Equal = AlmostEqual(Values1, Values2, CompareNorm=CompareNorm, P=P, Difference=Difference)
        end if
        if (.not. Equal) then
          write (*, '(3a,i0,a)') "Files have different ", trim(XNames(j)), " coordinates on grid ", i, "."
          write (*, '(a,e17.10)') "Difference: ", Difference
          stop 1
        end if
      end do
      deallocate(Values1)
      deallocate(Values2)
    end do
  end if

  if (associated(IBlank1)) then
    do i = 1, nGrids1
      allocate(IBlankValues1(nPoints1(i,1),nPoints1(i,2),nPoints1(i,3)))
      allocate(IBlankValues2(nPoints2(i,1),nPoints2(i,2),nPoints2(i,3)))
      IBlankValues1 = IBlank1(i,:nPoints1(i,1),:nPoints1(i,2),:nPoints1(i,3))
      IBlankValues2 = IBlank2(i,:nPoints2(i,1),:nPoints2(i,2),:nPoints2(i,3))
      Equal = all(IBlankValues1 == IBlankValues2)
      if (.not. Equal) then
        write (*, '(a,i0,a)') "Files have different IBlank values on grid ", i, "."
        stop 1
      end if
      deallocate(IBlankValues1)
      deallocate(IBlankValues2)
    end do
  end if

  if (nDims1 == 2) then
    QNames(1) = "densities"
    QNames(2:3) = ["x momenta", "y momenta"]
    QNames(4) = "energies"
  else
    QNames(1) = "densities"
    QNames(2:4) = ["x momenta", "y momenta", "z momenta"]
    QNames(5) = "energies"
  end if

  if (associated(Q1)) then
    do i = 1, nGrids1
      allocate(Values1(nPoints1(i,1),nPoints1(i,2),nPoints1(i,3)))
      allocate(Values2(nPoints2(i,1),nPoints2(i,2),nPoints2(i,3)))
      do j = 1, nDims1+2
        Values1 = Q1(i,:nPoints1(i,1),:nPoints1(i,2),:nPoints1(i,3),j)
        Values2 = Q2(i,:nPoints2(i,1),:nPoints2(i,2),:nPoints2(i,3),j)
        if (UseTolerance) then
          Equal = AlmostEqual(Values1, Values2, CompareNorm=CompareNorm, P=P, Tolerance=Tolerance, &
            Difference=Difference)
        else
          Equal = AlmostEqual(Values1, Values2, CompareNorm=CompareNorm, P=P, Difference=Difference)
        end if
        if (.not. Equal) then
          write (*, '(3a,i0,a)') "Files have different ", trim(QNames(j)), " on grid ", i, "."
          write (*, '(a,e17.10)') "Difference: ", Difference
          stop 1
        end if
      end do
      deallocate(Values1)
      deallocate(Values2)
    end do
  end if

  if (associated(QTarget1)) then
    do i = 1, nGrids1
      allocate(Values1(nPoints1(i,1),nPoints1(i,2),nPoints1(i,3)))
      allocate(Values2(nPoints2(i,1),nPoints2(i,2),nPoints2(i,3)))
      do j = 1, nDims1+2
        Values1 = QTarget1(i,:nPoints1(i,1),:nPoints1(i,2),:nPoints1(i,3),j)
        Values2 = QTarget2(i,:nPoints2(i,1),:nPoints2(i,2),:nPoints2(i,3),j)
        if (UseTolerance) then
          Equal = AlmostEqual(Values1, Values2, CompareNorm=CompareNorm, P=P, Tolerance=Tolerance, &
            Difference=Difference)
        else
          Equal = AlmostEqual(Values1, Values2, CompareNorm=CompareNorm, P=P, Difference=Difference)
        end if
        if (.not. Equal) then
          write (*, '(3a,i0,a)') "Files have different target ", trim(QNames(j)), " on grid ", i, "."
          write (*, '(a,e17.10)') "Difference: ", Difference
          stop 1
        end if
      end do
      deallocate(Values1)
      deallocate(Values2)
    end do
  end if

  if (associated(QAux1)) then
    do i = 1, nGrids1
      allocate(Values1(nPoints1(i,1),nPoints1(i,2),nPoints1(i,3)))
      allocate(Values2(nPoints2(i,1),nPoints2(i,2),nPoints2(i,3)))
      do j = 1, nAuxVars1
        Values1 = QAux1(i,:nPoints1(i,1),:nPoints1(i,2),:nPoints1(i,3),j)
        Values2 = QAux2(i,:nPoints2(i,1),:nPoints2(i,2),:nPoints2(i,3),j)
        if (UseTolerance) then
          Equal = AlmostEqual(Values1, Values2, CompareNorm=CompareNorm, P=P, Tolerance=Tolerance, &
            Difference=Difference)
        else
          Equal = AlmostEqual(Values1, Values2, CompareNorm=CompareNorm, P=P, Difference=Difference)
        end if
        if (.not. Equal) then
          write (*, '(a,i0,a,i0,a)') "Files have different auxiliary variable ", j, " on grid ", i, "."
          write (*, '(a,e17.10)') "Difference: ", Difference
          stop 1
        end if
      end do
      deallocate(Values1)
      deallocate(Values2)
    end do
  end if

  if (associated(QAuxTarget1)) then
    do i = 1, nGrids1
      allocate(Values1(nPoints1(i,1),nPoints1(i,2),nPoints1(i,3)))
      allocate(Values2(nPoints2(i,1),nPoints2(i,2),nPoints2(i,3)))
      do j = 1, nAuxVars1
        Values1 = QAuxTarget1(i,:nPoints1(i,1),:nPoints1(i,2),:nPoints1(i,3),j)
        Values2 = QAuxTarget2(i,:nPoints2(i,1),:nPoints2(i,2),:nPoints2(i,3),j)
        if (UseTolerance) then
          Equal = AlmostEqual(Values1, Values2, CompareNorm=CompareNorm, P=P, Tolerance=Tolerance, &
            Difference=Difference)
        else
          Equal = AlmostEqual(Values1, Values2, CompareNorm=CompareNorm, P=P, Difference=Difference)
        end if
        if (.not. Equal) then
          write (*, '(a,i0,a,i0,a)') "Files have different auxiliary target ", j, " on grid ", i, "."
          write (*, '(a,e17.10)') "Difference: ", Difference
          stop 1
        end if
      end do
      deallocate(Values1)
      deallocate(Values2)
    end do
  end if

  if (associated(nPoints1)) deallocate(nPoints1)
  if (associated(X1)) deallocate(X1)
  if (associated(IBlank1)) deallocate(IBlank1)
  if (associated(Q1)) deallocate(Q1)
  if (associated(QTarget1)) deallocate(QTarget1)
  if (associated(QAux1)) deallocate(QAux1)
  if (associated(QAuxTarget1)) deallocate(QAuxTarget1)

  if (associated(nPoints2)) deallocate(nPoints2)
  if (associated(X2)) deallocate(X2)
  if (associated(IBlank2)) deallocate(IBlank2)
  if (associated(Q2)) deallocate(Q2)
  if (associated(QTarget2)) deallocate(QTarget2)
  if (associated(QAux2)) deallocate(QAux2)
  if (associated(QAuxTarget2)) deallocate(QAuxTarget2)

end program CompareHDF5
