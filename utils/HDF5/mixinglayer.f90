! Copyright (c) 2015, University of Illinois at Urbana-Champaign, XPACC
! License: MIT, http://opensource.org/licenses/MIT
Program MixingLayer
  USE ModHDF5_IO, filename=>filename_HDF5, vars_to_write=>vars_to_write_HDF5, NGRID=>NGRID_HDF5, &
    NDIM=>NDIM_HDF5, nAuxVars=>nAuxVars_HDF5, ND=>ND_HDF5, X=>X_HDF5, IBLANK=>IBLANK_HDF5, &
    Q=>Q_HDF5, Qaux=>Qaux_HDF5, CONFIG=>CONFIG_HDF5
  USE ModParser
  Implicit None

  ! I/O variables
  Character(len=80) :: input_name

  ! Sponge information
  Integer :: I1, I2, J1, J2

  ! Density (computed in Aux_Q, used in Q)
  Real(KIND=8), Dimension(:,:,:), Pointer :: RHO
  
  ! Call the routines
  Call MixingLayer_Init
  Call MixingLayer_Grid
  Call MixingLayer_Aux_Q
  Call MixingLayer_Q
  Call MixingLayer_BC
  Call Write_HDF5
  Call Finalize

Contains


  ! ============== !
  ! Initialization !
  ! ============== !
  Subroutine MixingLayer_Init
    Implicit None

    print *, '! ======================================================= !'
    print *, '!                                                         !'
    print *, '!             HDF5 2D MIXING LAYER GENERATOR              !'
    print *, '!    Generates a Cartesian grid and boundary conditions   !'
    print *, '!    for a spatially-evolving jet in HDF5 format          !'
    print *, '!                                                         !'
    print *, '! ======================================================= !'

    ! Parse the input file
    input_name="plascomcm.inp"
    call parser_init
    call parser_parsefile(trim(input_name))

    ! HDF5 I/O details
    Write(filename(1:18),'(A10,I8.8)') 'RocFlo-CM.', 0
    Write(filename(19:21),'(A3)') '.h5'

    Allocate(vars_to_write(6))
    vars_to_write(1:6) = (/ 'xyz', 'cv ','cvt', 'aux', 'aut', 'rst' /)

    Return
  End Subroutine MixingLayer_Init


  ! ==================== !
  ! Create the grid/mesh !
  ! ==================== !
  Subroutine MixingLayer_Grid
    Implicit None

    Integer :: I, J, K, N
    Real(KIND=8) :: xmini, xmaxi, ymini, ymaxi
    Real(KIND=8) :: xmino, xmaxo, ymino, ymaxo
    Real(KIND=8) :: g0, b, c, sig, dy_min, dy_max
    Real(KIND=8), Allocatable :: s(:), g(:)
    Logical :: stretch_y

    ! Initialize the grids
    ngrid = 1
    NDIM = 3
    Allocate(ND(ngrid,3)); ND(:,3) = 1

    ! Number of grid points
    call parser_read('GRID1_NX',ND(1,1))
    call parser_read('GRID1_NY',ND(1,2))
    Write (*,'(A)') ''
    print *, 'Grid: ',ND(1,1),'x',ND(1,2),'x',ND(1,3)

    ! Allocate the arrays and assign values
    ALLOCATE(X(ngrid,MAXVAL(ND(:,1)),MAXVAL(ND(:,2)),MAXVAL(ND(:,3)),3))

    ! Grid size
    call parser_read('XMIN_INTERIOR',xmini)
    call parser_read('XMAX_INTERIOR',xmaxi)
    call parser_read('YMIN_INTERIOR',ymini)
    call parser_read('YMAX_INTERIOR',ymaxi)
    call parser_read('XMIN_OUTER',xmino)
    call parser_read('XMAX_OUTER',xmaxo)
    call parser_read('YMIN_OUTER',ymino)
    call parser_read('YMAX_OUTER',ymaxo)

    ! Stretching parameter
    call parser_read('STRETCH_Y',stretch_y,.false.)

    ! Generate the grid
    Do K = 1, ND(1,3)
       Do J = 1, ND(1,2)
          Do I = 1, ND(1,1)
             ! Create X
             X(1,I,J,K,1) = (XMAXo-XMINo)*DBLE(I-1)/DBLE(ND(1,1)-1) + XMINo
             ! Create Y
             If (.NOT.STRETCH_Y) Then
                X(1,I,J,K,2) = (YMAXo-YMINo)*DBLE(J-1)/DBLE(ND(1,2)-1) + YMINo
             End If
             ! Create Z
             X(1,I,J,K,3) = 0D0
          End Do
       End Do
    End Do

    ! Stretch grid
    If (STRETCH_Y) Then
       ! Parameters
       sig=0.2_8
       b=20.0_8
       c=0.62_8
       n=100

       ! Create uniform spacing
       Allocate(s(ND(1,2)))
       Do J = 1, ND(1,2)
          s(j) = DBLE(J-1)/DBLE(ND(1,2)-1)
       End Do

       ! Compute g(s)
       Allocate(g(ND(1,2)))
       Call g_of_s(1.0_8,b,c,sig,n,g0)
       Do j=1,ND(1,2)
          Call g_of_s(s(j),b,c,sig,n,g(J));
       End Do
       ! Compute y
       Do K = 1, ND(1,3)
          DO J = 1, ND(1,2)
             Do I = 1, ND(1,1)
                X(1,I,J,K,2) = 0.5_8*(YMAXo-YMINo)*g(J)/g0
             End Do
          End Do
       End Do
   
       ! Find min/max spacing
       dy_min=huge(1.0_8)
       dy_max=-huge(1.0_8)
       do J=1,ND(1,2)-1
          dy_min=min(dy_min,X(1,1,J+1,1,2)-X(1,1,J,1,2))
          dy_max=max(dy_max,X(1,1,J+1,1,2)-X(1,1,J,1,2))
       end do
       Write (*,'(A)') ''
       print *, 'min/max y-spacing:',dy_min,dy_max
       Write (*,'(A)') ''
    End If

    ! Find extents of outer region
    I1=1
    Do I = 1, ND(1,1)
       If (X(1,I,1,1,1) <= XMINi) I1=I
    End Do
    I2=ND(1,1)
    Do I = ND(1,1),1,-1
       If (X(1,I,1,1,1) >= XMAXi) I2=I
    End Do
    J1=1
    Do J = 1, ND(1,2)
       If (X(1,1,J,1,2) <= YMINi) J1=J
    End Do
    J2=ND(1,2)
    Do J = ND(1,2),1,-1
       If (X(1,1,J,1,2) >= YMAXi) J2=J
    End Do

    print *, 'Extents:',I1,I2,J1,J2

    Return
  End Subroutine MixingLayer_Grid


  ! ======================== !
  ! Grid stretching function !
  ! ======================== !
  Subroutine g_of_s(s,b,c,sig,n,g)
    Implicit None

    Integer, intent(in) :: n
    Real(KIND=8), intent(in) :: s,b,c,sig
    Real(KIND=8), intent(out) :: g

    Integer :: i
    Real(KIND=8) :: dx1,dx2,int1,int2
    Real(KIND=8), Dimension(n) :: x1,x2

    ! Compute stretched grid for mixing layer
    Do i = 1, N
       x1(i) = ((s-0.5_8)/sig)*DBLE(i-1)/DBLE(N-1) - c/sig
       x2(i) = ((s-0.5_8)/sig)*DBLE(i-1)/DBLE(N-1) + c/sig
    End Do
    dx1=x1(2)-x1(1)
    dx2=x2(2)-x2(1)

    int1=0.0_8
    int2=0.0_8
    do i=1,n
       int1 = int1 + erf(x1(i))*dx1
       int2 = int2 + erf(x2(i))*dx2
    end do

    g = (s-0.5_8)*(1.0_8+2.0_8*b)+b*sig*(int1-int2)

    Return
  End Subroutine g_of_s


  ! ============================= !
  ! Initial (target) solution Aux !
  ! ============================= !
  Subroutine MixingLayer_Aux_Q
    Implicit None

    Integer :: I, J, K, Nspecies
    Real(WP), Dimension(:,:,:), Pointer :: Fuel, Oxidizer, Z
    Real(WP), Pointer :: W(:)
    Real(WP) :: W_ref, Rho0, T0, P0, Ru, YF0, YO0
    Integer :: Air
    Integer, parameter :: H2=1
    Integer, parameter :: O2=2
    Logical :: variable_density

    ! Allocate density
    Allocate(RHO(MAXVAL(ND(:,1)),MAXVAL(ND(:,2)),MAXVAL(ND(:,3))))

    ! Read reference parameters from input
    call parser_read('NUMBER_OF_SCALARS',nspecies,0)
    nAuxVars = nspecies

    ! Only implemented for 1 species
    If (nspecies.gt.2) print *, 'WARNING, max of 2 species (for now)'

    ! Return if no species
    If (nspecies.EQ.0) Then
       RHO = 1.0_WP
       Return
    End If

    ! Read reference quantities
    Call parser_read('DENSITY_REFERENCE',rho0,1.0D0)
    Call parser_read('TEMPERATURE_REFERENCE',T0,293.15_WP)
    Call parser_read('PRESSURE_REFERENCE',P0,101325.0_WP)
    Call parser_read('UNIVERSAL_GAS_CONSTANT',Ru,8314.0_WP)
    Call parser_read('YF0',YF0,1.0_WP)
    Call parser_read('YO0',YO0,1.0_WP)

    ! Check if the mixture has variable density
    Call parser_read('VARIABLE_DENSITY',variable_density,.FALSE.)

    ! Allocate auxilary array
    Allocate(Qaux(ngrid,MAXVAL(ND(:,1)),MAXVAL(ND(:,2)),MAXVAL(ND(:,3)),nspecies))

    ! Link the pointers
    Fuel =>  Qaux(1,:,:,:,H2)
    Oxidizer =>  Qaux(1,:,:,:,O2)

    ! Mixture fraction
    Allocate(Z(MAXVAL(ND(:,1)),MAXVAL(ND(:,2)),MAXVAL(ND(:,3))))

    ! Assign mixture fraction profile
    Do K=1,ND(1,3)
       Do J=1,ND(1,2)
          Do I=1,ND(1,1)
             Z(I,J,K) = 0.5_WP*(1.0_WP-erf(X(1,I,J,K,2)))
          End Do
       End Do
    End Do

    ! Get mass fractions
    Fuel = YF0*Z
    Oxidizer = YO0*(1.0_WP-Z)

    ! Compute rho
    If (variable_density) then
       ! Molecular weights
       Allocate(W(Nspecies+1))
       Air = Nspecies+1
       Call parser_read('Mw1',W(H2),2.0_WP)
       Call parser_read('Mw_ref',W(Air),28.97_WP)

       ! Get Density assuming ideal gas
       Do K=1,ND(1,3)
          Do J=1,ND(1,2)
             Do I=1,ND(1,1)
                Rho(I,J,K) = Oxidizer(I,J,K)*P0*W(Air)/Ru/T0 + Fuel(I,J,K)*P0*W(H2)/Ru/T0
             End Do
          End Do
       End Do
       Rho = Rho/Rho0
    Else
       Rho = 1.0_WP
    End If

    ! Change mass fractions into mass densities
    Do I=1, nspecies
       Qaux(1,:,:,:,I) = Qaux(1,:,:,:,I)*Rho
    End Do

  End Subroutine MixingLayer_Aux_Q


  ! ========================= !
  ! Initial (target) solution !
  ! ========================= !
  Subroutine MixingLayer_Q
    Implicit None

    Integer :: I, J, K, EQ_STATE
    Real(KIND=8), Dimension(:,:,:), Pointer :: rhoU,rhoV,rhoE
    Real(KIND=8) :: Rho0, mu0, delta, gamma, C, Re, Pr, Sc
    Real(KIND=8) :: M1, M2, Mc, U1, U2, Uc

    ! Allocate solution array
    Allocate(Q(ngrid,MAXVAL(ND(:,1)),MAXVAL(ND(:,2)),MAXVAL(ND(:,3)),4))

    ! Density has already been computed
    Q(1,:,:,:,1) = RHO

    ! Link the pointers
    rhoU  =>  Q(1,:,:,:,2)
    rhoV  =>  Q(1,:,:,:,3)
    rhoE  =>  Q(1,:,:,:,4)

    ! Read reference parameters from input
    Call parser_read('REYNOLDS_NUMBER',Re,0D0)
    Call parser_read('PRANDTL_NUMBER',Pr,0.72D0)
    Call parser_read('SCHMIDT_NUMBER',Sc,1.0D0)
    Call parser_read('DENSITY_REFERENCE',rho0,1.0D0)
    Call parser_read('GAMMA_REFERENCE',gamma,1.4D0)
    Call parser_read('SNDSPD_REFERENCE',C,347.0D0)
    Call parser_read('GAS_EQUATION_OF_STATE',EQ_STATE,0)
    Call parser_read('LENGTH_REFERENCE',delta)

    ! Non-dim velocities
    Call parser_read('M1',M1)
    Call parser_read('M2',M2)
    Mc = 0.5D0*(M1+M2)

    ! Compute dimensional parameters
    U1 = M1*C
    U2 = M2*C
    Uc = 0.5D0*(U1+U2)
    mu0 = rho0*C*delta/Re

    ! Assign values to the config array
    Allocate(CONFIG(1:NGRID))
    CONFIG(1)%t_iter = 0
    CONFIG(1)%NDIM   = NDIM
    CONFIG(1)%ND(1)  = ND(1,1)
    CONFIG(1)%ND(2)  = ND(1,2)
    CONFIG(1)%ND(3)  = ND(1,3)
    CONFIG(1)%Use_IB = 0
    CONFIG(1)%time   = 0.0_8
    CONFIG(1)%Re     = Re
    CONFIG(1)%Pr     = Pr
    CONFIG(1)%Sc     = Sc

    ! Number of auxilary variables
    Call parser_read('NUMBER_OF_SCALARS',nauxvars)

    ! Initialize momentum
    rhoU = 0.0D0
    rhoV = 0.0D0

    ! Initialize the velocity field (hyperbolic tangent function)
    Do K=1,ND(1,3)
       Do J=1,ND(1,2)
          Do I=1,ND(1,1)
             rhoU(I,J,K) = rho(I,J,K)*(M2+0.5D0*(M1-M2)*(1.0D0+tanh(2.0D0*X(1,I,J,K,2))))
          End Do
       End Do
    End Do

    ! Compute energy
    if (EQ_STATE == 0 .or. EQ_STATE==3) then
       rhoE = rho/gamma/(gamma-1.0_WP) + 0.5_WP*(rhoU**2+rhoV**2)/rho
    else
       Write (*,'(A)') 'Mixing_layer only implemented using ideal gas law'
       Stop
    end if

    Return
  End Subroutine MixingLayer_Q


  ! =================== !
  ! Boundary conditions !
  ! =================== !
  Subroutine MixingLayer_BC
    Implicit None

    Integer :: iunit

    ! Open the file
    iunit=11
    open(iunit,file="bc.dat")
    
    Write (*,'(A)') ''
    Write (*,'(A)') 'Writing boundary conditions'

    ! Write the header
    write(iunit,'(1a1 )') "#"
    write(iunit,'(1a45)') "# Basic boundary condition file for PlasComCM"
    write(iunit,'(1a1 )') "#"
    write(iunit,'(1a24)') "# FD = finite difference"
    write(iunit,'(1a20)') "# FV = finite volume"
    write(iunit,'(1a1 )') "#"
    write(iunit,'(1a8 )') "# ibType"
    write(iunit,'(1a8 )') "# ======"
    write(iunit,'(1a32)') "#     00     OUTER_BOUNDARY (FD)"
    write(iunit,'(1a41)') "#     21     SAT_SLIP_ADIABATIC_WALL (FD)"
    write(iunit,'(1a44)') "#     22     SAT_NOSLIP_ISOTHERMAL_WALL (FD)"
    write(iunit,'(1a33)') "#     23     SAT_NOSLIP_ADIABATIC_WALL (FD)"
    write(iunit,'(1a31)') "#     24     SAT_FAR_FIELD (FD)"
    write(iunit,'(1a37)') "#     25     SAT_BLOCK_INTERFACE (FD)"
    write(iunit,'(1a51)') "#     31     NSCBC_INFLOW_VELOCITY_TEMPERATURE (FD)"
    write(iunit,'(1a63)') "#     32     NSCBC_INFLOW_VELOCITY_TEMPERATURE_FROM_SPONGE (FD)"
    write(iunit,'(1a57)') "#     41     NSCBC_OUTFLOW_PERFECT_NONREFLECTION (FD, FV)"
    write(iunit,'(1a54)') "#     42     NSCBC_OUTFLOW_PERFECT_REFLECTION (FD, FV)"
    write(iunit,'(1a47)') "#     51     NSCBC_WALL_ADIABATIC_SLIP (FD, FV)"
    write(iunit,'(1a50)') "#     52     NSCBC_WALL_ISOTHERMAL_NOSLIP (FD, FV)"
    write(iunit,'(1a39)') "#     61     NSCBC_SUBSONIC_INFLOW (FD)"
    write(iunit,'(1a45)') "#     62     NSCBC_SUPERSONIC_INFLOW (FD, FV)"
    write(iunit,'(1a29)') "#     73     PERIODICITY (FV)"
    write(iunit,'(1a30)') "#     91     FV_DIRICHLET (FV)"
    write(iunit,'(1a28)') "#     92     FV_NEUMANN (FV)"
    write(iunit,'(1a29)') "#     93     FV_SYMMETRY (FV)"
    write(iunit,'(1a36)') "#     94     FV_BLOCK_INTERFACE (FV)"
    write(iunit,'(1a45)') "#     95     FV_BLOCK_INTERFACE_PERIODIC (FV)"
    write(iunit,'(1a28)') "#     99     SPONGE (FD, FV)"
    write(iunit,'(1a1 )') "#"
    write(iunit,'(1a56)') "# Grid ibType ibDir   is    ie    js    je     ks    ke "
    write(iunit,'(1a56)') "# ==== ====== =====  ====  ====  ====  ====   ====  ===="

    ! Input the boundary conditions
    ! SAT far-field / sponge
    write(iunit,'(9I6)')      1,   24,     1,    1,     1,    1,   -1,    1,    -1
    write(iunit,'(9I6)')      1,   24,    -1,   -1,    -1,    1,   -1,    1,    -1
    write(iunit,'(9I6)')      1,   99,     1,    1,    I1,    1,   -1,    1,    -1
    write(iunit,'(9I6)')      1,   99,    -1,   I2,    -1,    1,   -1,    1,    -1
    write(iunit,'(9I6)')      1,   24,     2,    1,    -1,    1,    1,    1,    -1
    write(iunit,'(9I6)')      1,   24,    -2,    1,    -1,   -1,   -1,    1,    -1
    write(iunit,'(9I6)')      1,   99,     2,    1,    -1,    1,   J1,    1,    -1
    write(iunit,'(9I6)')      1,   99,    -2,    1,    -1,   J2,   -1,    1,    -1

    ! Close the file
    close(iunit)

    Return
  End Subroutine MixingLayer_BC

  ! ================== !
  ! Clean up and leave !
  ! ================== !
  Subroutine Finalize
    Implicit None
  
    Deallocate(ND,X,Q)

    Return
  End Subroutine Finalize

End Program MixingLayer
