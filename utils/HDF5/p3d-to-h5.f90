! Copyright (c) 2015, University of Illinois at Urbana-Champaign, XPACC
! License: MIT, http://opensource.org/licenses/MIT
!===================================================================================================
!
! PLOT3DToHDF5: PLOT3D to HDF5 file conversion utility
!
! Usage: ./p3d-to-h5 <Options> <PLOT3D file(s)>
!
! Options:
!   --base-name (-b) Base name of resulting HDF5/XDMF files
!
!===================================================================================================

program PLOT3DToHDF5

  use ModParseArguments
  use ModStdIO
  use ModPLOT3D_IO
  use ModHDF5_IO
  implicit none

  integer, parameter :: rk = selected_real_kind(15, 307)

  integer, parameter :: MAX_ND = 3

  integer, parameter :: PATH_LENGTH = 256
  integer, parameter :: VAR_NAME_LENGTH = 32

  integer :: i, j
  character(len=CMD_ARG_LENGTH), dimension(:), allocatable :: RawArguments
  type(t_cmd_opt), dimension(1) :: Options
  character(len=CMD_ARG_LENGTH), dimension(:), allocatable :: Arguments
  character(len=PATH_LENGTH) :: PLOT3DGridFile
  character(len=PATH_LENGTH) :: PLOT3DSolutionFile
  character(len=PATH_LENGTH) :: PLOT3DTargetFile
  character(len=PATH_LENGTH) :: PLOT3DAuxFile
  character(len=PATH_LENGTH) :: PLOT3DAuxTargetFile
  character(len=PATH_LENGTH) :: HDF5File
  character(len=PATH_LENGTH) :: XDMFFile
  logical :: HasGrid, HasSolution, HasTarget, HasAux, HasAuxTarget
  logical :: WriteIBlank
  logical :: Exists
  integer :: nDims, nDimsPLOT3D
  integer :: nGrids
  integer :: nAuxVars
  integer, dimension(:,:), pointer :: nPoints, nPointsAux
  real(rk), dimension(:,:,:,:,:), pointer :: X
  real(rk), dimension(:,:,:,:,:), pointer :: XCopy
  integer, dimension(:,:,:,:), pointer :: IBlank
  real(rk), dimension(:,:,:,:,:), pointer :: Q
  real(rk), dimension(:,:,:,:,:), pointer :: QTarget
  real(rk), dimension(:,:,:,:,:), pointer :: QAux
  real(rk), dimension(:,:,:,:,:), pointer :: QAuxTarget
  real(rk), dimension(:,:,:,:,:), pointer :: QCopy
  integer :: TimestepIndex
  real(rk) :: Time
  real(rk) :: Re
  character(len=PATH_LENGTH) :: FilePath
  integer :: FileNameBegin
  character(len=PATH_LENGTH) :: FileName
  character(len=PATH_LENGTH) :: BaseName
  character(len=PATH_LENGTH) :: Extension
  integer :: BaseNameBegin, BaseNameEnd
  character(len=2) :: UseIBlank, CoordPrecision, GridFormat, VolumeFormat
  real(rk), dimension(4) :: Tau, TauIgnore
  integer :: nXDMFVars
  character(len=3), dimension(:), pointer :: VarNames
  integer, dimension(:), pointer :: VarTypes
  integer, dimension(:), pointer :: nVarComponents
  integer :: NextVar

  allocate(RawArguments(command_argument_count()))
  do i = 1, size(RawArguments)
    call get_command_argument(i, RawArguments(i))
  end do

  Options(1) = t_cmd_opt_("base-name", "b", OPT_VALUE_TYPE_STRING)

  call ParseArguments(RawArguments, Options=Options, Arguments=Arguments, MinArguments=1, &
    MaxArguments=5)

  HasGrid = .false.
  HasSolution = .false.
  HasTarget = .false.
  HasAux = .false.
  HasAuxTarget = .false.

  do i = 1, size(Arguments)
    FilePath = Arguments(i)
    FileNameBegin = scan(trim(FilePath), '/', back=.true.) + 1
    FileName = FilePath(FileNameBegin:)
    call SplitPLOT3DFileName(FileName, BaseName, Extension)
    select case (Extension)
    case ('xyz')
      if (.not. HasGrid) then
        PLOT3DGridFile = FilePath
        HasGrid = .true.
      else
        write (ERROR_UNIT, '(a)') "ERROR: Specified more than one grid file."
        stop 1
      end if
    case ('q')
      if (.not. HasSolution) then
        PLOT3DSolutionFile = FilePath
        HasSolution = .true.
      else
        write (ERROR_UNIT, '(a)') "ERROR: Specified more than one solution file."
        stop 1
      end if
    case ('target.q')
      if (.not. HasTarget) then
        PLOT3DTargetFile = FilePath
        HasTarget = .true.
      else
        write (ERROR_UNIT, '(a)') "ERROR: Specified more than one target file."
        stop 1
      end if
    case ('aux.q')
      if (.not. HasAux) then
        PLOT3DAuxFile = FilePath
        HasAux = .true.
      else
        write (ERROR_UNIT, '(a)') "ERROR: Specified more than one auxiliary variable file."
        stop 1
      end if
    case ('target.aux.q')
      if (.not. HasAuxTarget) then
        PLOT3DAuxTargetFile = FilePath
        HasAuxTarget = .true.
      else
        write (ERROR_UNIT, '(a)') "ERROR: Specified more than one auxiliary target file."
        stop 1
      end if
    end select
  end do

  if (HasGrid) then
    inquire(file=PLOT3DGridFile, exist=Exists)
    if (.not. Exists) then
      write (ERROR_UNIT, '(3a)') "ERROR: File '", trim(PLOT3DGridFile), "' does not exist."
      stop 1
    end if
  end if

  if (HasSolution) then
    inquire(file=PLOT3DSolutionFile, exist=Exists)
    if (.not. Exists) then
      write (ERROR_UNIT, '(3a)') "ERROR: File '", trim(PLOT3DSolutionFile), "' does not exist."
      stop 1
    end if
  end if

  if (HasTarget) then
    inquire(file=PLOT3DTargetFile, exist=Exists)
    if (.not. Exists) then
      write (ERROR_UNIT, '(3a)') "ERROR: File '", trim(PLOT3DTargetFile), "' does not exist."
      stop 1
    end if
  end if

  if (HasAux) then
    inquire(file=PLOT3DAuxFile, exist=Exists)
    if (.not. Exists) then
      write (ERROR_UNIT, '(3a)') "ERROR: File '", trim(PLOT3DAuxFile), "' does not exist."
      stop 1
    end if
  end if

  if (HasAuxTarget) then
    inquire(file=PLOT3DAuxTargetFile, exist=Exists)
    if (.not. Exists) then
      write (ERROR_UNIT, '(3a)') "ERROR: File '", trim(PLOT3DAuxTargetFile), "' does not exist."
      stop 1
    end if
  end if

  if (Options(1)%is_present) then
    BaseName = Options(1)%value
  else
    if (HasSolution) then
      FilePath = PLOT3DSolutionFile
    else if (HasTarget) then
      FilePath = PLOT3DTargetFile
    else if (HasAux) then
      FilePath = PLOT3DAuxFile
    else if (HasAuxTarget) then
      FilePath = PLOT3DAuxTargetFile
    else
      FilePath = PLOT3DGridFile
    end if
    FileNameBegin = scan(trim(FilePath), '/', back=.true.) + 1
    FileName = FilePath(FileNameBegin:)
    call SplitPLOT3DFileName(FileName, BaseName, Extension)
  end if

  HDF5File = trim(BaseName) // ".h5"
  XDMFFile = trim(BaseName) // ".xmf"

  nullify(nPoints)
  nullify(nPointsAux)
  nullify(X)
  nullify(IBlank)
  nullify(Q)
  nullify(QTarget)
  nullify(QAux)
  nullify(QAuxTarget)

  if (HasGrid) then
    call Read_Grid(nDimsPLOT3D, nGrids, nPoints, X, IBlank, CoordPrecision, GridFormat, &
      VolumeFormat, UseIBlank, PLOT3DGridFile, 0)
    if (UseIBlank == "n" .and. associated(IBlank)) then
      deallocate(IBlank)
    end if
    WriteIBlank = associated(IBlank)
  end if

  if (HasSolution) then
    call Read_Soln(nDimsPLOT3D, nGrids, nPoints, Q, Tau, CoordPrecision, GridFormat, VolumeFormat, &
      PLOT3DSolutionFile, 0)
  end if

  if (HasTarget) then
    call Read_Soln(nDimsPLOT3D, nGrids, nPoints, QTarget, TauIgnore, CoordPrecision, GridFormat, &
      VolumeFormat, PLOT3DTargetFile, 0)
  end if

  nAuxVars = 0

  if (HasAux) then
    call Read_Func(MAX_ND, nGrids, nPointsAux, QAux, PLOT3DAuxFile)
    nAuxVars = size(QAux,5)
  end if

  if (HasAuxTarget) then
    call Read_Func(MAX_ND, nGrids, nPointsAux, QAuxTarget, PLOT3DAuxTargetFile)
    nAuxVars = size(QAuxTarget,5)
  end if

  ! Read_Func defines nPoints differently from other PLOT3D routines (has size nGrids by MAX_ND+1
  ! rather than MAX_ND), so we ignore it unless other routines don't get called
  if (.not. associated(nPoints) .and. associated(nPointsAux)) then
    allocate(nPoints(nGrids,MAX_ND))
    nPoints = nPointsAux(:,:MAX_ND)
  end if

  ! PLOT3D files are usually formatted as 3D, even if the problem is 2D -- not HDF5 though
  if (all(nPoints(:,3) == 1)) then
    nDims = 2
  else
    nDims = 3
  end if

  ! If problem is 2D, convert data to 2D format
  if (nDimsPLOT3D == 3 .and. nDims == 2) then

    ! Remove z coordinate from X
    if (associated(X)) then
      allocate(XCopy(nGrids,size(X,2),size(X,3),size(X,4),3))
      XCopy = X
      deallocate(X)
      allocate(X(nGrids,size(XCopy,2),size(XCopy,3),size(XCopy,4),2))
      X = XCopy(:,:,:,:,:2)
      deallocate(XCopy)
    end if

    ! Remove z momentum from Q
    if (associated(Q)) then
      allocate(QCopy(nGrids,size(Q,2),size(Q,3),size(Q,4),5))
      QCopy = Q
      deallocate(Q)
      allocate(Q(nGrids,size(QCopy,2),size(QCopy,3),size(QCopy,4),4))
      Q(:,:,:,:,:3) = QCopy(:,:,:,:,:3)
      Q(:,:,:,:,4) = QCopy(:,:,:,:,5)
      deallocate(QCopy)
    end if

    ! Remove z momentum from QTarget
    if (associated(QTarget)) then
      allocate(QCopy(nGrids,size(QTarget,2),size(QTarget,3),size(QTarget,4),5))
      QCopy = QTarget
      deallocate(QTarget)
      allocate(QTarget(nGrids,size(QCopy,2),size(QCopy,3),size(QCopy,4),4))
      QTarget(:,:,:,:,:3) = QCopy(:,:,:,:,:3)
      QTarget(:,:,:,:,4) = QCopy(:,:,:,:,5)
      deallocate(QCopy)
    end if

  end if

  TimestepIndex = int(Tau(1))
  Time = Tau(4)
  Re = Tau(3)

  call WriteHDF5File(HDF5File, nDims=nDims, nGrids=nGrids, nPoints=nPoints, nAuxVars=nAuxVars, &
    X=X, IBlank=IBlank, Q=Q, QTarget=QTarget, QAux=QAux, QAuxTarget=QAuxTarget, &
    TimestepIndex=TimestepIndex, Time=Time, Re=Re)

  nXDMFVars = 0
  if (HasSolution) then
    nXDMFVars = nXDMFVars + 1
  end if
  if (HasTarget) then
    nXDMFVars = nXDMFVars + 1
  end if
  if (HasAux) then
    nXDMFVars = nXDMFVars + 1
  end if
  if (HasAuxTarget) then
    nXDMFVars = nXDMFVars + 1
  end if

  allocate(VarNames(nXDMFVars))
  allocate(VarTypes(nXDMFVars))
  allocate(nVarComponents(nXDMFVars))

  NextVar = 1
  if (HasSolution) then
    VarNames(NextVar) = 'cv'
    VarTypes(NextVar) = XDMF_VAR_TYPE_FLOAT
    nVarComponents(NextVar) = nDims+2
    NextVar = NextVar + 1
  end if
  if (HasTarget) then
    VarNames(NextVar) = 'cvt'
    VarTypes(NextVar) = XDMF_VAR_TYPE_FLOAT
    nVarComponents(NextVar) = nDims+2
    NextVar = NextVar + 1
  end if
  if (HasAux) then
    VarNames(NextVar) = 'aux'
    VarTypes(NextVar) = XDMF_VAR_TYPE_FLOAT
    nVarComponents(NextVar) = nAuxVars
    NextVar = NextVar + 1
  end if
  if (HasAuxTarget) then
    VarNames(NextVar) = 'aut'
    VarTypes(NextVar) = XDMF_VAR_TYPE_FLOAT
    nVarComponents(NextVar) = nAuxVars
    NextVar = NextVar + 1
  end if

  call WriteXDMFFile(XDMFFile, nDims=nDims, nGrids=nGrids, nPoints=nPoints, HDF5File=HDF5File, &
    HasIBlank=WriteIBlank, Time=Time, VarNames=VarNames, VarTypes=VarTypes, &
    nVarComponents=nVarComponents)

  deallocate(VarNames)
  deallocate(VarTypes)
  deallocate(nVarComponents)

  if (associated(nPoints)) deallocate(nPoints)
  if (associated(nPointsAux)) deallocate(nPointsAux)
  if (associated(X)) deallocate(X)
  if (associated(IBlank)) deallocate(IBlank)
  if (associated(Q)) deallocate(Q)
  if (associated(QTarget)) deallocate(QTarget)
  if (associated(QAux)) deallocate(QAux)
  if (associated(QAuxTarget)) deallocate(QAuxTarget)

contains

  subroutine SplitPLOT3DFileName(FileName, BaseName, Extension)

    character(len=*), intent(in) :: FileName
    character(len=*), intent(out) :: BaseName
    character(len=*), intent(out) :: Extension

    integer :: ExtensionStart

    ExtensionStart = index(trim(FileName), '.xyz', back=.true.)
    if (ExtensionStart /= 0) then
      BaseName = FileName(:ExtensionStart-1)
      Extension = "xyz"
      return
    end if

    ExtensionStart = index(trim(FileName), '.target.q', back=.true.)
    if (ExtensionStart /= 0) then
      BaseName = FileName(:ExtensionStart-1)
      Extension = "target.q"
      return
    end if

    ! Checking aux target before aux because scanning for '.aux.q' also matches '.target.aux.q'
    ExtensionStart = index(trim(FileName), '.target.aux.q', back=.true.)
    if (ExtensionStart /= 0) then
      BaseName = FileName(:ExtensionStart-1)
      Extension = "target.aux.q"
      return
    end if

    ExtensionStart = index(trim(FileName), '.aux.q', back=.true.)
    if (ExtensionStart /= 0) then
      BaseName = FileName(:ExtensionStart-1)
      Extension = "aux.q"
      return
    end if

    ! Checking solution last because scanning for '.q' also matches '.target.q', '.aux.q', etc.
    ExtensionStart = index(trim(FileName), '.q', back=.true.)
    if (ExtensionStart /= 0) then
      BaseName = FileName(:ExtensionStart-1)
      Extension = "q"
      return
    end if

  end subroutine SplitPLOT3DFileName

end program PLOT3DToHDF5
