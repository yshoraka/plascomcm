#include <string>
#include <vector>
#include <map>
#include <sstream>
#include <istream>
#include <ostream>
#include <fstream>
#include <list>
#include <cstdlib>
#include <cstdio>
#include <cassert>

namespace apptools {

  ///
  /// \brief Simple GnuPlot plotter util
  ///
  inline int GNUPlot(const std::string &commands)  
  {                                                                           
    FILE *fp = popen("gnuplot -persist","w");
    fprintf(fp,commands.c_str());               
    pclose(fp);                  
    return(0);    
  }      

  ///                                                                      
  /// \brief Simple key-value pair
  ///
  template<typename K,typename V>
  class keyvaluepair : public std::pair<K,V>
  {
  public:
    keyvaluepair(const std::pair<K,V> &p){
      this->first = p.first;
      this->second = p.second;
    };
    keyvaluepair(const K &k,const V &v){
      this->first = k;
      this->second = v;
    };
    keyvaluepair(){};
    K Key() const{ return this->first; };
    V Value() const{ return this->second; };
    K &Key() { return this->first; };
    V &Value() { return this->second; };
    K &Key(const K &k) { this->first = k; return(this->first); };
    V &Value(const V &v) { this->second = v; return(this->second); };
    //  virtual std::ostream &Dump(std::ostream &Ostr){
    //    Ostr << Key() << ":" << Value();
    //    return(Ostr);
    //  };
  };
  
  inline std::string GetNextContent(std::istream &In)
  {
    std::string line;
    std::string ret;
    while(ret.empty() && std::getline(In,line)){
      line = line.substr(0,line.find("#"));
      std::istringstream Istr(line);
      std::string token;
      Istr >> token;
      if(!token.empty() && token[0] != '#')
	ret.assign(line);
    }
    return(ret);
  }
  
  inline void GetContentUntil(std::istream &In,
			      std::string ret,
			      const std::string &etag)
  {
    std::ostringstream Ostr;
    std::string line;
    while(std::getline(In,line) && ret.empty()){
      line = line.substr(0,line.find("#"));
      if(!line.empty()){
	std::istringstream Istr(line);
	std::string token;
	Istr >> token;
	if(!token.empty()){
	  if(token == etag){
	    //	    Ostr << line << std::endl;
	    ret = Ostr.str();
	  }
	  else
	    Ostr << line << std::endl;
	}
      }
    }
  }
  
  ///
  /// \brief Strip absolute path
  /// 
  /// Strips the absolute path to recover the base file name or
  /// the executable file name.
  ///
  inline const std::string 
  stripdirs(const std::string &pname)
  {
    std::string retval;
    std::string::size_type x = pname.find("/");
    if(x == std::string::npos)
      return(pname);
    return(pname.substr(pname.find_last_of("/")+1));
  }
  
  
  ///
  /// \brief Dump container contents
  ///
  /// Dumps the contents of the container to the specified std::ostream 
  /// object.
  ///
  template<typename ContainerType>
  void DumpContents(std::ostream &Ostr,const ContainerType &c,std::string del = "\n"){
    if(!c.empty()){
      typename ContainerType::const_iterator ci = c.begin();
      Ostr << *ci++;
      while(ci != c.end())
	Ostr << del << *ci++;
    }
  }
  
  ///
  /// \brief Tokenize string
  ///
  /// Breaks source string up into a vector of space delimited tokens
  ///
  inline void
  TokenizeString(std::vector<std::string> &tokens,const std::string &source)
  {
    tokens.resize(0);
    std::istringstream Istr(source);
    std::string token;
    while(Istr >> token)
      tokens.push_back(token);
  }
  
  ///
  /// \brief File opener
  ///
  inline int OpenFile(std::ifstream &Inf,const std::string &filename)
  {
    Inf.open(filename.c_str());
    if(!Inf)
      return(-1);
    return 0;
  }
  
  typedef keyvaluepair<std::string,std::string> ParamType;
  
  std::ostream &operator<<(std::ostream &Ostr,
			   const ParamType &param);
  
  class parameters : public std::vector<ParamType>
  
  {
    friend std::ostream &operator<<(std::ostream &oSt,
				    const parameters &pv);
    friend std::istream &operator>>(std::istream &iSt,
				    parameters &pv);
  public:
    std::string GetValue(const std::string &key) const;
    std::vector<std::string> GetValueVector(const std::string &key) const;
    std::list<std::string> GetValueList(const std::string &key) const;

    ParamType *ParamPtr(const std::string &key);
      
    bool GetFlagValue(const std::string &key) const
    {
      bool retVal = false;
      std::string value(this->Param(key));
      if(!value.empty()){
	if(value=="on"   || value=="yes"    ||
	   value=="ON"   || value=="YES"    ||
	   value=="Yes"  || value=="True"   ||
	   value=="1"    || value=="true"   ||
	   value=="TRUE" || value==".true." ||
	   value==".TRUE.")
	  retVal = true;
      }
      return(retVal);
    };
      
    template<typename T>
    T GetValue(const std::string &key) const
    {
      T retval = T();
      std::string value(this->Param(key));
      if(!value.empty()){
	std::istringstream Istr(value);
	Istr >> retval;
      }
      return(retval);
    };
      
    template<typename T>
    std::vector<T> GetValueVector(const std::string &key) const
    {
      std::vector<T> retval;
      std::string value(this->Param(key));
      if(!value.empty()){
	std::istringstream Istr(value);
	T tmpval;
	while(Istr >> tmpval)
	  retval.push_back(tmpval);
      }
      return(retval);
    };

    template<typename T>
    std::list<T> GetValueList(const std::string &key) const
    {
      std::list<T> retval;
      std::string value(this->Param(key));
      if(!value.empty()){
	std::istringstream Istr(value);
	T tmpval;
	while(Istr >> tmpval)
	  retval.push_back(tmpval);
      }
      return(retval);
    };

    virtual int SetValue(const std::string &key,const std::string &value){
      if(ParamPtr(key)){
	ParamPtr(key)->Value(value);
	return(0);
      }
      return(1);
    };
    virtual std::istream &ReadFromStream(std::istream &Is);
    virtual std::ostream &WriteToStream(std::ostream &Os) const;
    std::string Param(const std::string &Key) const;
    bool IsSet(const std::string &Key) const;
    virtual ~parameters(){};
  };
  std::ostream &operator<<(std::ostream &oSt,const parameters &pv);
  std::istream &operator>>(std::istream &iSt,parameters &pv);
  ParamType *parameters::ParamPtr(const std::string &key)
  {
    parameters::const_iterator pi = this->begin();
    while(pi != this->end()){
      unsigned int index = pi - this->begin();
      if(pi->first == key)
	return(&((*this)[index]));
      pi++;
    }
    return(NULL);
  }
    
  std::string parameters::GetValue(const std::string &key) const
  {
    std::string retval;
    std::string value(this->Param(key));
    if(!value.empty()){
      std::istringstream Istr(value);
      Istr >> retval;
    }
    return(retval);
  }

  std::vector<std::string> parameters::GetValueVector(const std::string &key) const
  {
    std::vector<std::string> retval;
    std::string value(this->Param(key));
    if(!value.empty()){
      std::istringstream Istr(value);
      std::string tmpval;
      while(Istr >> tmpval)
	retval.push_back(tmpval);
    }
    return(retval);
  }

  std::list<std::string> parameters::GetValueList(const std::string &key) const
  {
    std::list<std::string> retval;
    std::string value(this->Param(key));
    if(!value.empty()){
      std::istringstream Istr(value);
      std::string tmpval;
      while(Istr >> tmpval)
	retval.push_back(tmpval);
    }
    return(retval);
  }

  std::istream &parameters::ReadFromStream(std::istream &Is)
  {
    std::string line;
    int n = 0;
    while(Is){
      std::getline(Is,line);
      n++;
      // Removes any leading whitespace
      std::string::size_type x = line.find('#');
      line = line.substr(0,x);
      if(!line.empty()){
	x = line.find('=');
	if(x == std::string::npos)
	  return(Is);
	ParamType param;
	std::istringstream Instr(line.substr(0,x));
	Instr >> param.Key();
	std::vector<std::string> tokens;
	line = line.substr(x+1,line.size());
	TokenizeString(tokens,line);
	std::ostringstream Ostr;
	std::vector<std::string>::iterator ti = tokens.begin();
	if(ti != tokens.end())
	  Ostr << *ti++;
	while(ti != tokens.end())
	  Ostr << " " << *ti++;
	param.Value() = Ostr.str();
	this->push_back(param);
      }
    }
    return(Is);
  }

  std::string parameters::Param(const std::string &key) const
  {
    std::string empty;
    parameters::const_iterator pi = this->begin();
    while(pi != this->end()){
      if(pi->first == key)
	return(pi->second);
      pi++;
    }
    return(empty);
  }
  
  bool parameters::IsSet(const std::string &key) const
  {
    return(!Param(key).empty());
  };
    

  std::ostream &parameters::WriteToStream(std::ostream &oSt) const
  {
    parameters::const_iterator pi = this->begin();
    while(pi != this->end()){
      oSt << *pi++;
      if(pi != this->end())
	oSt << std::endl;
    }
    return(oSt);
  };

  std::ostream &operator<<(std::ostream &oSt,
			   const parameters &pv)
  {
    return(pv.WriteToStream(oSt));
  }
  
  std::istream &operator>>(std::istream &iSt,
			   parameters &pv)
  {
    return(pv.ReadFromStream(iSt));
  }
  
  
  std::ostream &operator<<(std::ostream &Ostr,const ParamType &param)
  {
    Ostr << param.Key() << " = " << param.Value();
    return(Ostr);
  }


  /// 
  /// Command line processing.
  ///
  /// Provides a quick and easy way to deal with the arguments
  /// coming into an application from the command line.
  /// Several useful methods for parsing the command line tokens and  
  /// generation of detailed usage information are provided for the 
  /// application.
  ///
  /// Command line tokens are separated into three kinds.
  /// The zeroth kind is the first command line token which is the 
  /// name of the program as it was invoked on the command line. This
  /// token is stripped of any absolute path information and stored.
  /// The first kind are _options_.
  /// Any command line token which begins with a "-" is interpreted as
  /// an option. There are several types of options:
  /// * Type 0) A simple flag.
  /// * Type 1) May accept an _option argument_. An option argument is the 
  /// command line token immediatly following the option token.
  /// * Type 2) Requires an option argument.
  /// * Type 3) Non-optional option.  Application requires this option to 
  /// function.
  /// 
  /// Option usage is summarized by the following table.
  ///
  /// | Type  |     Description    |      Usage     |    Value        |
  /// | ----: | :----------------: | :------------: | :-------------- |
  /// |  0    | simple flag        | -t             | .true.          |
  /// |  1    | argument optional  | -t or -t [arg] | .true. or [arg] |
  /// |  2    | argument required  | -t <arg>       | <arg>           |
  /// |  3    | required w/arg     | -t <arg>       | <arg>           |
  ///
  /// The second kind of command line tokens are _arguments_ to the application.
  /// These kind of tokens are simply all the tokens not used up in the 
  /// processing of the zeroth and first kinds of command line tokens. A 
  /// mechanism for specifying whether these kind of tokens are required or 
  /// optional is also provided.
  ///
  class comlinehandler : public std::vector< std::pair<char,std::string> >
  {
    friend std::ostream &operator<<(std::ostream &Out,const comlinehandler &cl);
    friend std::istream &operator>>(std::istream &In,comlinehandler &cl);
  protected:
    ///           
    /// application description.
    ///
    /// This description will be dumped as the first section 
    /// of the output of LongUsage().
    ///
    std::string _description; 
    /// Notes to be displayed at the end of LongUsage().
    std::string _notes;
    /// the name of the program
    std::string _program_name; 
    /// unformatted command line
    std::string _line;         
    /// stores error messages
    std::vector<std::string> _error_messages; 
    /// stores non-option arguments
    std::vector<std::string> _nonops; 
    /// stores the value of each option
    std::map<char,std::string> _options;
    /// stores the help string for each op
    std::map<char,std::string> _help;     
    /// stores a name for arguments
    std::map<char,std::string> _argname;
    /// stores the type of option   
    std::map<char,int> _type;       
    /// application arguments;
    std::vector<std::pair<std::string,int> > _args;
    /// help string for args
    std::map<std::string,std::string> _arghelp;
    /// commandline tokens
    std::vector<std::string> _toks;
  public:
    ///
    /// Default constructor.
    ///
    comlinehandler(){};
    ///
    /// Constructor.
    ///
    /// Typically, args comes directly from \e argv.  One must be careful about
    /// environments in which extra arguments are stored in argv.  A good example
    /// is MPI.  MPI_Init(&argc,&argv) should be invoked before any comlinehandler is 
    /// constructed.
    ///
    comlinehandler(const char *args[])
    {
      Record(args);
    };
    ///
    /// Copy constructor.
    ///
    comlinehandler(comlinehandler &incom);
    ///
    /// Copy method.
    ///
    void Copy(comlinehandler &incom);
    ///
    /// One fell swoop processing of command line.
    ///
    int ProcessCommandLine(const char *args[])
    {
      Record(args);
      return(ProcessOptions());
    };
    ///
    /// \brief Raw Command Line Access.
    ///
    std::string GetRawComLine() const { return (_program_name+std::string(" ")+_line);};
    ///
    /// \brief Raw Command Line Access.
    ///
    void SetRawComLine(const std::string &incl);
    ///
    /// \brief Processes all command line tokens.
    ///
    /// This function examines all the command line tokens, sorts them into
    /// their respective types, and assigns their values.  The number of 
    /// errors encountered during processing is returned.
    ///
    int ProcessOptions();
    ///
    /// \brief Minimal recording of command line.
    ///
    /// This function simply parses the command line to extract the executble
    /// name and record the full command line in a string.
    ///
    void Record(const char *args[]);
    /// 
    /// \brief User interface to describe simple option.
    ///
    /// This function is provided for the specification of a simple option.
    /// The arguments are the short, single letter option, the long "word"
    /// version, and the type of the option.  If the option is of the type
    /// which may have or requires an argument itself, then the argument
    /// will be given the default name, "arg".
    ///
    void AddOption(char s,const std::string &l,int = 0);
    /// 
    /// \brief User interface to describe option with argument.
    ///
    /// This function is provided for the specification of an option along
    /// with a name for it's argument.  Argument names are useful for the
    /// generation of usage information to the user.
    /// The arguments are the short, single letter option, the long "word"
    /// version, the type, and the argument name.
    ///
    void AddOption(char s,const std::string &l,int,const std::string argname);
    /// 
    /// \brief User interface to describe an application argument.
    ///
    /// This function is provided for the specification of an argument
    /// to the application (i.e. a non-option argument), and whether the
    /// argument is required for the application to function.
    /// The function arguments are the name of the argument, and an 
    /// integer which is > 0 for required application arguments. 
    ///
    void AddArgument(const std::string &a,int reqd = 0)
    {
      //_args.push_back(std::make_pair<std::string,int>(a,reqd));
      _args.push_back(std::make_pair(a,reqd));
    };
    /// 
    /// \brief Specify the usage info for application argument.
    ///
    /// This function is provided for the specification of the usage
    /// information for application arguments.  This is called after
    /// the argument has been set by AddArgument(...).  The string
    /// specified in "help" string is displayed in the output of 
    /// LongUsage().
    ///
    void AddArgHelp(const std::string &a,const std::string &help)
    {
      _arghelp[a] = help;
    };
    /// 
    /// \brief Specify name of an option argument.
    ///
    /// Function is used to set the argument name for an option 
    /// if the option can use an argument.
    ///
    void SetArgName(const std::string opstr,const std::string argname)
    {
      _argname[GetShort(opstr)] = argname;
    };
    /// 
    /// \brief Specify name of an option argument.
    ///
    /// Function is used to set the argument name for an option 
    /// if the option can use an argument.
    ///
    void SetArgName(char s,const std::string &argname)
    {
      _argname[s] = argname;
    };
    /// 
    /// \brief Specify usage for an option.
    ///
    /// Function is used to set the usage string for an option 
    /// which is displayed in the output of LongUsage().
    ///
    void AddHelp(char s,const std::string &help){ _help[s] = help;};
    /// 
    /// \brief Specify usage for an option.
    ///
    /// Function is used to set the usage string for an option 
    /// which is displayed in the output of LongUsage().
    ///
    void AddHelp(const std::string &l,const std::string &help)
    {
      _help[GetShort(l)] = help;
    };
    ///
    /// \brief Obtains option strings by type.
    ///
    /// Internal utility function for building a string of 
    /// option characters which are of a certain type.
    ///
    std::string GetOpStringByType(int mintype,int maxtype); 
    ///
    /// \brief Obtain the usage string for an option.
    ///
    std::string GetHelp(char s){return(_help[s]);};
    ///
    /// \brief Obtain the usage string for an option.
    ///
    std::string GetHelp(const std::string &l){return(_help[GetShort(l)]);};
    ///
    /// \brief Obtain the short one char option from the long word version.
    ///
    char GetShort(const std::string &l);
    ///
    /// \brief Obtain the long word option from the short char version.
    ///
    std::string GetLong(const char &s);
    ///
    /// \brief Generate short usage string.
    ///
    /// Generates a short usage string similar to what one would see
    /// in the typical short command line usage output for a UNIX 
    /// command.  Note that the format of this usage string follows
    /// the convention that optional quantities are enclosed in []'s
    /// while required quantities are enclosed in <>'s.
    ///
    std::string ShortUsage();
    ///
    /// \brief Generate long usage string.
    ///
    /// Generates a long usage string similar to what one would see
    /// in the typical UNIX man-like description.  Each option is 
    /// shown with it's short and long version, along with usage 
    /// information for each option and argument if such information 
    /// has been specified with AddHelp, or AddArgHelp, respectively. 
    /// Note that the format of this usage string follows
    /// the convention that optional quantities are enclosed in []'s
    /// while required quantities are enclosed in <>'s.
    ///
    std::string LongUsage();
    ///
    /// \brief Get the value of an option.
    ///
    /// Returns the value of the specified option.  If the option 
    /// has not been set on the command line, then the value will
    /// be an empty string.
    ///
    std::string GetOption(const char &s){return(_options[s]);};
    ///
    /// \brief Get the value of an option.
    ///
    /// Returns the value of the specified option.  If the option 
    /// has not been set on the command line, then the value will
    /// be an empty string.
    ///
    std::string GetOption(const std::string &l){return(_options[GetShort(l)]);};
    ///
    /// \brief Error reporting.
    ///
    /// Returns a report of the errors encountered in the ProcessOptions() 
    /// function.
    ///
    std::string ErrorReport();
    ///
    /// \brief Program name access.
    ///
    std::string ProgramName() const { return _program_name;};
    ///
    /// \brief Argument access.
    ///
    std::vector<std::string> GetArgs() const { return _nonops;};
    ///
    /// \brief Set description string.
    ///
    void SetDescription(const std::string &desc){ _description.assign(desc); };
    ///
    /// \brief Set notes string.
    ///
    void SetNotes(const std::string &notes){ _notes.assign(notes); };
    ///
    /// \brief Write an RC string that can be used for config.
    ///
    void WriteRC(std::ostream &Ostr) const;
    ///
    /// \brief Read a config from RC string.
    ///
    void ReadRC(const std::string &RCString);
    ///
    /// \brief Process a range in the format "t1:t2".
    ///
    template<typename NumType>
    void ProcessRange(NumType &t1,NumType &t2,const std::string stinter)
    {
      if(!stinter.empty()){
	std::string::size_type x = stinter.find(":");
	std::string t1s = stinter;
	std::string t2s = stinter;
	if(x != std::string::npos){
	  t1s = stinter.substr(0,x);
	  t2s = stinter.substr(x+1);
	  if(!t2s.empty()){
	    std::istringstream Istr(t2s);
	    Istr >> t2;
	  }
	}
	else {
	  t1s.erase();
	}
	if(!t1s.empty()){
	  std::istringstream Istr(t1s);
	  Istr >> t1;
	}
	if(!t2s.empty()){
	  std::istringstream Istr(t2s);
	  Istr >> t2;
	}
  
      }
    };
    ///
    /// \brief Resolve an option (i.e. obtain it's "value").
    ///
    template<typename NumType>
    void ResolveOption(NumType &t2,const std::string stinter)
    {
      if(stinter == ".true." || stinter == ".TRUE."){
	t2 = 1;
	return;
      }
      if(stinter == ".false." || stinter == ".FALSE."){
	t2 = 0;
	return;
      }
      if(!stinter.empty()){
	std::istringstream Istr(stinter);
	Istr >> t2;
      }
    };
    ///
    /// \brief virtual function for program specific Initialization.
    ///
    virtual void Initialize(void){};
    ///
    /// \brief virtual function for program specific Initialization.
    ///
    virtual void UserInitialize(void){};

    virtual ~comlinehandler(){};
  };


  std::istream &operator>>(std::istream &In,comlinehandler &cl)
  {
    std::string line;
    std::string::size_type x;
    line = GetNextContent(In);
    x = line.find("<description>");
    if(x != std::string::npos){
      GetContentUntil(In,cl._description,"</description>");
      line = GetNextContent(In);
    }
    x = line.find("<options>");
    if(x == std::string::npos){
      cl._error_messages.push_back("Config file format error - options section misplaced?.");
      return(In);
    }
    std::string options_content;
    GetContentUntil(In,options_content,"</options>");
    std::istringstream Istr(options_content);
    while(std::getline(Istr,line)){
      std::istringstream OpIn(line);
      std::string token;
      char opchar;
      int optype;
      OpIn >> opchar;
      OpIn >> token;
      OpIn >> optype;
      cl.push_back(std::make_pair(opchar,token));
      cl._type[opchar] = optype;
      if(optype > 0){
	std::string token;
	OpIn >> token;
	cl._argname[opchar] = token;
      }
      std::getline(Istr,line);
      x = line.find("<help>");
      if(x == std::string::npos){
	cl._error_messages.push_back("Configuration input format error in option help section.");
	return(In);
      }
      cl._help[opchar] = "";
      GetContentUntil(Istr,cl._help[opchar],"</help>");
    }
    line = GetNextContent(In);
    if(line.empty())
      return(In);
    x = line.find("<arguments>");
    if(x == std::string::npos){
      cl._error_messages.push_back("Configuration intput format error in argument section.");
      return(In);
    }
    std::string argsection;
    GetContentUntil(In,argsection,"</arguments>");
    Istr.str(argsection);
    while(std::getline(Istr,line)){
      std::istringstream ArgIn(line);
      std::string argname;
      int argtype;
      ArgIn >> argname >> argtype;
      cl._arghelp[argname] = argtype;
      std::string tag;
      ArgIn >> tag;
      std::string::size_type x = tag.find("<help>");
      if(x == std::string::npos){
	cl._error_messages.push_back("Configuration input format error in arghelp section.");
	return(In);
      }
      cl._arghelp[argname] = "";
      GetContentUntil(In,cl._arghelp[argname],"</help>");
    }
    return(In);
  }
  
  std::ostream &operator<<(std::ostream &Out,const comlinehandler &cl)
  {
    if(!cl._description.empty())
      Out << "<description>\n" << cl._description << "</description>\n";
    Out << "<options>\n";
    comlinehandler::const_iterator cli = cl.begin();
    while(cli != cl.end()){
      char opchar = cli->first;
      std::map<char,int>::const_iterator ti = cl._type.find(opchar);
      Out << cli->first << " " << cli->second << " " 
	  << (ti == cl._type.end() ? 0 : ti->second);
      if(ti != cl._type.end()){
	std::map<char,std::string>::const_iterator ai = cl._argname.find(opchar);
	Out << " " << (ai == cl._argname.end() ? "(unspecified)" : ai->second);
      }
      std::map<char,std::string>::const_iterator hi = cl._help.find(opchar);
      Out << "\n<help>\n" << (hi == cl._help.end() ? "(unspecified)" : hi->second) 
	  << "\n</help>\n";
      cli++;
    }
    Out << "</options>\n";
    if(!cl._args.empty()){
      Out << "<arguments>\n";
      std::vector<std::pair<std::string,int> >::const_iterator ai = cl._args.begin();
      while(ai != cl._args.end()){
	std::map<std::string,std::string>::const_iterator ahi = cl._arghelp.find(ai->first);
	Out << ai->first << " " << ai->second
	    << "\n<help>\n" << (ahi == cl._arghelp.end() ? "(unspecified)" : ahi->second) 
	    << "\n</help>\n";
	ai++;
      }
      Out << "</arguments>\n";
    }
    return(Out);
  }
  
  
  comlinehandler::comlinehandler(comlinehandler &incom)
  {
    Copy(incom);
  };

  void comlinehandler::Copy(comlinehandler &incom)
  {
    _description.erase();
    _notes.erase();
    _program_name.erase();
    _line.erase();
    _error_messages.resize(0);
    _nonops.resize(0);
    _options.clear();
    _help.clear();
    _argname.clear();
    _type.clear();
    _args.resize(0);
    _arghelp.clear();
    this->resize(0);

    _description.assign(incom._description);
    _notes.assign(incom._notes);
    _program_name.assign(incom._program_name);
    _line.assign(incom._line); 
    std::vector<std::string>::iterator ici = incom._error_messages.begin();
    while(ici != incom._error_messages.end())
      _error_messages.push_back(*ici++);
    ici = incom._nonops.begin();
    while(ici != incom._nonops.end())
      _nonops.push_back(*ici++);
    std::map<char,std::string>::iterator icmi = incom._options.begin();
    while(icmi != incom._options.end()){
      _options[icmi->first] = icmi->second;
      icmi++;
    }
    icmi = incom._help.begin();
    while(icmi != incom._help.end()){
      _help[icmi->first] = icmi->second;
      icmi++;
    }
    icmi = incom._argname.begin();
    while(icmi != incom._argname.end()){
      _argname[icmi->first] = icmi->second;
      icmi++;
    }
    std::map<char,int>::iterator icti = incom._type.begin();
    while(icti != incom._type.end()){
      _type[icti->first] = icti->second;
      icti++;
    }
    std::vector<std::pair<std::string,int> >::iterator icai = incom._args.begin();
    while(icai != incom._args.end())
      _args.push_back(*icai++);

    std::map<std::string,std::string>::iterator icahi = incom._arghelp.begin();
    while(icahi != incom._arghelp.end()){
      _arghelp[icahi->first] = icahi->second;
      icahi++;
    }
    comlinehandler::iterator cli = incom.begin();
    while(cli != incom.end())
      this->push_back(*cli++);
  };

  std::string comlinehandler::LongUsage()
  {
    std::ostringstream Ostr;
    if(!_description.empty())
      Ostr << _description << std::endl << std::endl;
    Ostr << "Usage: " << std::endl
	 << std::endl << ShortUsage() << std::endl << std::endl;
    std::vector<std::pair<char,std::string> >::const_iterator ti = this->begin();
    while(ti != this->end()){
      Ostr << "\t" << "-" << ti->first << ",--" << ti->second 
	   << (_type[ti->first] > 0 ? (_type[ti->first]==1 ? " [" : " <") : "")
	   << _argname[ti->first]
	   << (_type[ti->first] > 0 ? (_type[ti->first]==1 ? "]" : ">") : "")
	   << std::endl << "\t\t" << _help[ti->first];
      ti++;
      if(ti != this->end())
	Ostr << std::endl << std::endl;
    }
    Ostr << std::endl << std::endl;
    std::vector<std::pair<std::string,int> >::const_iterator ai = _args.begin();
    while(ai != _args.end()){
      Ostr << "\t" << (ai->second ? "<" : "[") << ai->first  
	   << (ai->second ? ">" : "]") << std::endl << "\t\t" 
	   << _arghelp[ai->first];
      ai++;
      if(ai != _args.end())
	Ostr << std::endl << std::endl;
    }
    if(!_notes.empty()) 
      Ostr << std::endl << _notes;
    return(Ostr.str());
  }
  
  std::string comlinehandler::GetLong(const char &s)
  {
    std::vector<std::pair<char,std::string> >::iterator ti = this->begin();
    while(ti != this->end()){
      if(ti->first == s)
	return(ti->second);
      ti++;
    }
    return("");
  }
  
  std::string comlinehandler::GetOpStringByType(int mintype,int maxtype)
  {
    std::ostringstream Ostr;
    std::vector<std::pair<char,std::string> >::iterator oi = this->begin();
    while(oi != this->end()){
      if(_type[oi->first] >= mintype && _type[oi->first] <= maxtype)
	Ostr << oi->first;
      oi++;
    }
    return(Ostr.str());
  }
  
  std::string comlinehandler::ShortUsage()
  {
    std::ostringstream Ostr;
    std::string flagstring = GetOpStringByType(0,0); 
    Ostr << _program_name << " ";
    if(!flagstring.empty())
      Ostr << "[-" << flagstring << "] ";
    std::string optionals = GetOpStringByType(1,2);
    if(!optionals.empty()){
      std::string::iterator oi = optionals.begin();
      Ostr << "[";
      while(oi != optionals.end()){
	Ostr << "-" << *oi 
	     << (_type[*oi] == 1 ? " [" : " <")
	     << _argname[*oi]  
	     << (_type[*oi] == 1 ? "] " : "> ");
	oi++;
      }
      Ostr << "] ";
    }
    std::string reqd = GetOpStringByType(3,3);
    if(!reqd.empty()){
      std::string::iterator oi = reqd.begin();
      Ostr << "<";
      while(oi != reqd.end()){
	Ostr << "-" << *oi << " <" << _argname[*oi]  
	     << "> ";
	oi++;
      }
      Ostr << "> ";
    }
    std::vector<std::pair<std::string,int> >::iterator ai = _args.begin();
    while(ai != _args.end()){
      Ostr << (ai->second > 0 ? "<" : "[") << ai->first 
	   << (ai->second > 0 ? "> ": "] ");
      ai++;
    }
    return(Ostr.str());
  };
  
  char comlinehandler::GetShort(const std::string &l)
  {
    std::vector<std::pair<char,std::string> >::iterator ti = this->begin();
    while(ti != this->end()){
      if(ti->second == l)
	return(ti->first);
      ti++;
    }
    return('\0');
  };
  
  void comlinehandler::AddOption(char s,const std::string &l,int atype)
  {
    //this->push_back(std::make_pair<char,std::string>(s,l));
    this->push_back(std::make_pair(s,l));
    _options[s] = std::string("");
    _type[s] = atype;
    if(atype > 0)
      _argname[s] = "arg";
    _help[s] = std::string("");
  }
    
  void comlinehandler::AddOption(char s,const std::string &l,int type,const std::string argname)
  {
    //this->push_back(std::make_pair<char,std::string>(s,l));
    this->push_back(std::make_pair(s,l));
    _options[s] = std::string("");
    _type[s] = type;
    _argname[s] = argname;
    _help[s] = std::string("");
  }
  
  void comlinehandler::Record(const char *args[])
  {
    int i = 1;
    std::ostringstream Ostr;
    _program_name.assign(stripdirs(args[0]));
    while(args[i]){
      _toks.push_back(std::string(args[i]));
      Ostr << args[i++];
      if(args[i])
	Ostr << " ";
    }
    _line.assign(Ostr.str());
  }
  
  int comlinehandler::ProcessOptions() 
  {
    bool end_of_ops = false;
    int errs = 0;
    std::vector<std::string> &args(_toks);
    //      TokenizeString(args,_line);
    std::vector<std::string>::iterator ai = args.begin();
    while(ai != args.end()){
      std::string this_argument = *ai++;
      std::string next_argument;
      if(ai != args.end())
	next_argument = *ai--;
      std::string::iterator si = this_argument.begin();
      if(*si == '-' && !end_of_ops){
	si++;
	if(si == this_argument.end())
	  end_of_ops = true;
	if(*si != '-') { // then we are processing a short option
	  while(si != this_argument.end()){
	    char flag_char = *si++;
	    std::vector<std::pair<char,std::string> >::iterator oi = this->begin();
	    bool found = false;
	    while((oi != this->end()) && !found){
	      if(flag_char == oi->first){
		found = true;
		if(si == this_argument.end()){  // means it's not a string of flags
		  if(_type[flag_char] != 0){
		    if(!next_argument.empty() && next_argument[0] != '-'){
		      _options[flag_char] = next_argument;
		      ai++;
		    }
		    else if(_type[flag_char] > 1){ // next arg wasn't valid, err if reqd
		      errs++;
		      std::ostringstream Ostr;
		      Ostr << "Option -" << flag_char << " requires an argument.";
		      _error_messages.push_back(Ostr.str());
		    }
		    else
		      _options[flag_char] = ".true.";
		  }
		  else
		    _options[flag_char] = ".true.";
		}
		else{ // it's a string of flags
		  if(_type[flag_char] > 1){ // it requires an argument, can't be in a flag string
		    errs++;
		    std::ostringstream Ostr;
		    Ostr << "Option -" << flag_char << " requires an argument.";
		    _error_messages.push_back(Ostr.str());
		  }
		  else
		    _options[flag_char] = ".true.";
		}
	      }
	      oi++;
	    }
	    if(!found){
	      errs++;
	      std::ostringstream Ostr;
	      Ostr << "Option -" << flag_char << " is unrecognized.";
	      _error_messages.push_back(Ostr.str());
	    }
	  }
	}
	else { // we are processing a long option
	  std::string opstring = this_argument.substr(2);
	  char flag_char = GetShort(opstring);
	  if(flag_char != '\0'){
	    if(_type[flag_char] != 0){
	      if(!next_argument.empty() && next_argument[0] != '-'){
		_options[flag_char] = next_argument;
		ai++;
	      }
	      else if(_type[flag_char] > 1){
		errs++;
		std::ostringstream Ostr;
		Ostr << "Option --" << GetLong(flag_char)
		     << " requires an argument.";
		_error_messages.push_back(Ostr.str());
	      }
	      else
		_options[flag_char] = ".true.";
	    }
	    else
	      _options[flag_char] = ".true.";
	  }
	  else{
	    errs++;
	    std::ostringstream Ostr;
	    Ostr << "Option --" << opstring << " is unrecognized.";
	    _error_messages.push_back(Ostr.str());
	  }
	}
      }
      else { // non option arguments
	_nonops.push_back(this_argument);
      }
      if(ai != args.end())
	ai++;
    }
    // After finished parsing all arguments, go back and determine
    // whether any required input was missing.
    // 
    // First, process the options
    std::vector<std::pair<char,std::string> >::iterator oi = this->begin();
    while(oi != this->end()){
      if(GetOption(oi->first).empty() && _type[oi->first]==3){
	errs++;
	std::ostringstream Ostr;
	Ostr << "Required option: -" << oi->first << ",--" << oi->second 
	     << " <" << _argname[oi->first] << "> was not specified.";
	_error_messages.push_back(Ostr.str());
      }
      oi++;
    }
    // Next, make sure there were enough nonop args
    unsigned int nreqd_args = 0;
    std::vector<std::pair<std::string,int> >::iterator aai = _args.begin();
    while(aai != _args.end()){
      if(aai->second > 0)
	nreqd_args++;
      aai++;
    }
    if(nreqd_args > _nonops.size()){
      errs++;
      std::ostringstream Ostr;
      Ostr << "Missing required arguments.";
      _error_messages.push_back(Ostr.str());
    }
    return(errs);
  }
  
  void comlinehandler::SetRawComLine(const std::string &incl)
  {
    std::istringstream Istr(incl);
    Istr >> _program_name;
    std::string token;
    Istr >> _line;
    while(Istr >> token)
      _line += (std::string(" ")+token);
  }
  
  
  std::string comlinehandler::ErrorReport()
  {
    std::ostringstream Ostr;
    Ostr << _program_name << " command line errors: " << std::endl;
    DumpContents(Ostr,_error_messages);
    return(Ostr.str());
  }
  
  void comlinehandler::WriteRC(std::ostream &Ostr) const {return;}
  void comlinehandler::ReadRC(const std::string &RCString) {return;}
}
       
