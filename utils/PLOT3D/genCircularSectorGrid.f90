! Copyright (c) 2015, University of Illinois at Urbana-Champaign, XPACC
! License: MIT, http://opensource.org/licenses/MIT
Program genCircularSectorGrid
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!                                                               !
! Generate a circular sector grid in PLOT3D format              !
!                                                               !
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!                                                               !
! Written by Daniel J. Bodony (bodony@stanford.edu)             !
! 26 January 2007                                               !
!                                                               !
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  USE ModPLOT3D_IO
  Implicit None

  !  Arrays
  Real(KIND=8), Dimension(:,:,:,:,:), Pointer :: X1
  Real(KIND=8) :: XMIN(3), XMAX(3), twopi, RMIN, RMAX, angle, radius, sector_angle

  !  Integers
  Integer :: ngrid(3), I, J, K
  Integer, Dimension(:,:), Pointer :: ND1
  Integer, Dimension(:,:,:,:), Pointer :: IBLANK1

  !  Logical
  Logical :: gf_exists(3), sf_exists(3)

  !  Characters
  Character(LEN=2)  :: prec(3), grid_format(3), volume_format(3), ib(3)
  Character(LEN=80) :: grid_file(3), cmd

  !  Step zero, read in the command line
  Integer :: iargc, nargc, ng, perDir, perPlane

  nargc = iargc()
  If (nargc /= 1) Then
    Write (*,'(A)') 'USAGE: genCircularSectorGrid {grid file 1}'
    Stop
  Else
    Call Getarg(1,grid_file(1));
    prec(1) = 'd'
    grid_format(1) = 'm'
    volume_format(1) = 'w'
    ib(1) = 'y'
    If ((prec(1) .NE. 's') .AND. (prec(1) .NE. 'd')) Then
      Write (*,'(A)') 'Invalid value for precision: use either "s" (single) or "d" (double)'
      Stop
    Else If ((grid_format(1) .NE. 's') .AND. (grid_format(1) .NE. 'm')) Then
      Write (*,'(A)') 'Invalid value for grid format 1: use either "s" (single) or "m" (multi)'
      Write (*,'(A)') '"'//grid_format(1)//'" was given.'
      Stop
    Else If ((volume_format(1) .NE. 'w') .AND. (volume_format(1) .NE. 'p')) Then
      Write (*,'(A)') 'Invalid value for volume format: use either "w" (whole) or "p" (planes)'
      Stop
    Else If ((ib(1) .NE. 'n') .AND. (ib(1) .NE. 'y')) Then
      Write (*,'(A)') 'Invalid value for IBLANK format: use either "n" (no) or "y" (yes)'
      Stop
    End If
  End If

  !  Step one, get parameters from user
  ngrid(1) = 1; Allocate(ND1(1,3))
  Write (*,'(A)',ADVANCE='NO') 'Input NX: '
  Read (*,*) ND1(1,1)
  Write (*,'(A)',ADVANCE='NO') 'Input NY: '
  Read (*,*) ND1(1,2)
  Write (*,'(A)',ADVANCE='NO') 'Input NZ: '
  Read (*,*) ND1(1,3)
  Write (*,'(A)',ADVANCE='NO') 'Input Sector Angle (degrees): '
  Read (*,*) sector_angle

  !  Step two, which direction is periodic
  Write (*,'(A)', ADVANCE='NO') 'Which direction is periodic: '
  Read (*,*) perDir

  !  Step three, include periodic plane twice?
  Write (*,'(A)', ADVANCE='NO') 'Replicate periodic plane (0=no, 1=yes): '
  Read (*,*) perPlane

  twopi = 2.0_8 * dacos(-1.0_8)
  XMIN(:) = 0.0_8; XMAX(:) = 0.0_8
  sector_angle = sector_angle * twopi / 360.0_8
  
  if (perPlane == 0) then
    XMAX(perDir) = sector_angle * dble(ND1(1,perDir)-1)/dble(ND1(1,perDir))
  else
    XMAX(perDir) = sector_angle * dble(ND1(1,perDir)-1)/dble(ND1(1,perDir)-1)
  end if

  If ((ND1(1,1) .GT. 1) .and. (perDir /= 1)) THEN
    Write (*,'(A)',ADVANCE='NO') 'Input XMIN, XMAX: '
    Read (*,*) XMIN(1), XMAX(1)
  End if
  If ((ND1(1,2) .GT. 1) .and. (perDir /= 2)) THEN
    Write (*,'(A)',ADVANCE='NO') 'Input YMIN, YMAX: '
    Read (*,*) XMIN(2), XMAX(2)
  End if
  If ((ND1(1,3) .GT. 1) .and. (perDir /= 3)) THEN
    Write (*,'(A)',ADVANCE='NO') 'Input ZMIN, ZMAX: '
    Read (*,*) XMIN(3), XMAX(3)
  End if

  If (perDir == 1) Then
    RMIN = XMIN(2); RMAX = XMAX(2);
  Else If (perDir == 2) Then
    RMIN = XMIN(1); RMAX = XMAX(1);
  Else
    Stop 'perDir == 3 not supported.'
  End If
   
  ALLOCATE(X1(1,ND1(1,1),ND1(1,2),ND1(1,3),3))
  ALLOCATE(IBLANK1(1,ND1(1,1),ND1(1,2),ND1(1,3)))
  
  Do I = 1, ND1(1,1)
    DO J = 1, ND1(1,2)
      Do K = 1, ND1(1,3)
        If (perDir == 1) Then
          if (perPlane == 0) angle = sector_angle * dble(I-1)/dble(ND1(1,1))
          if (perPlane == 1) angle = sector_angle * dble(I-1)/dble(ND1(1,1)-1)
          radius = RMIN + DBLE(J-1)/DBLE(ND1(1,2)-1)*(RMAX-RMIN)
        Else If (perDir == 2) Then
          if (perPlane == 0) angle = sector_angle * dble(J-1)/dble(ND1(1,2))
          if (perPlane == 1) angle = sector_angle * dble(J-1)/dble(ND1(1,2)-1)
          radius = RMIN + DBLE(I-1)/DBLE(ND1(1,1)-1)*(RMAX-RMIN)
        End If

        X1(1,I,J,K,1) = radius * dcos(-angle)
        X1(1,I,J,K,2) = radius * dsin(-angle)

        If (ND1(1,3) .GT. 1) Then
          X1(1,I,J,K,3) = XMIN(3) + DBLE(K-1)/DBLE(ND1(1,3)-1)*(XMAX(3)-XMIN(3))
        Else
          X1(1,I,J,K,3) = XMIN(3)
        End If
      End Do
    End Do
  End Do
  IBLANK1 = 1

  !  Step two, read in the mesh1 grid file
  Write (*,'(A)') ''
  Call Write_Grid(3,ngrid(1),ND1,X1,IBLANK1, &
                  prec(1),grid_format(1),volume_format(1),ib(1),grid_file(1))
  Write (*,'(A)') ''

  ! Step three, clean up & leave
  Deallocate(ND1, X1)

End Program genCircularSectorGrid
