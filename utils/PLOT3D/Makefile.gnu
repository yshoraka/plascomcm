# Copyright (c) 2015, University of Illinois at Urbana-Champaign, XPACC
# License: MIT, http://opensource.org/licenses/MIT
# Makefile for PLOT3D_Utils

# basic compilers
F90C = gfortran
CC   = gcc
CXX  = g++

# top level directories and binaries
LIBPLOT3D = libPLOT3D.a
OBJDIR    = obj

# Useful binaries for operating on PLOT3D files
BINS_F90 = detectFormat cart3d appendGrids diffSoln readSolnTime \
           extrudeMesh reverseDir Ogrid subsetMesh genOgrid subsetSoln \
           setIBLANK extractValue mergeGrids hRefine computeXZAverage boundary-layer.f90\
           cart2d moveNodes searchAndReplaceIBLANK assembleSoln assembleGrid \
           processPIV computeMean extrudeSoln extractProbe computeFunc genCircularSectorGrid
BINS_C   = swapEndian

# Object files
OBJS_F90 = $(addsuffix .o, $(BINS_F90))
OBJS_C   = $(addsuffix .o, $(BINS_C))
OBJ      = $(addprefix $(OBJDIR)/, $(OBJS_C) $(OBJS_F90))

# Source files
SRC_F90 = $(addsuffix .f90, $(BINS_F90))
SRC_C   = $(addsuffix .c, $(BINS_C))

# compile options
CFLAGS = -Wall -DLE
FFLAGS = -J$(OBJDIR) -pipe -std=gnu -ffree-line-length-0 -fconvert=big-endian

# Build
all : $(LIBPLOT3D) $(BINS_F90) $(BINS_C) | $(OBJDIR)

$(OBJDIR):
	@mkdir $(OBJDIR)

$(LIBPLOT3D): ModPLOT3D_IO.f90 plot3d_format.c
	$(CC) $(CFLAGS) -c plot3d_format.c -o $(OBJDIR)/plot3d_format.o
	$(F90C) $(FFLAGS) -c ModPLOT3D_IO.f90 -o $(OBJDIR)/ModPLOT3D_IO.o
	ar cr libPLOT3D.a $(OBJDIR)/plot3d_format.o $(OBJDIR)/ModPLOT3D_IO.o
	ranlib libPLOT3D.a

$(BINS_F90): $(addprefix $(OBJDIR)/, $(OBJS_F90)) $(LIBPLOT3D)
	 $(F90C) -o $@ $(OBJDIR)/$@.o $(LIBPLOT3D)

$(BINS_C): $(addprefix $(OBJDIR)/, $(OBJS_C))
	$(CC) $(CFLAGS) -o $@ $(OBJDIR)/$@.o

# implicit rules for fortran files
$(OBJDIR)/%.o: %.f90
	$(F90C) -c $(FFLAGS) $< -o $@	

# implicit rules for C files
$(OBJDIR)/%.o: %.c
	$(CC) -c $(CFLAGS) $(CPPFLAGS) $< -o $@	

#==================== Clean =========================
.PHONY: clean
clean:
	@rm -f $(DEPS) $(OBJDIR)/*.o $(OBJDIR)/*.mod $(BINS_F90) $(BINS_C) $(LIBPLOT3D)

# generate dependencies
CCDEP      = $(CC)
CCDEPARGS  = -M -c $<
CCMAKEDEP  = @echo "Generating dependency file $@ from $<" ; $(CCDEP) $(CPPFLAGS) $(CCDEPARGS) 

# Filter for changing "foo.o:" into "foo.o foo.d :"
FILTER_C = sed 's/\(.*\)\.o[ :]*/$(OBJDIR)\/\1.o $(OBJDIR)\/\1.d : /g'

$(OBJDIR)/%.d : %.f90
	@echo "Generating dependency file $(OBJDIR)/$*.d from $<"
	@(echo "$(OBJDIR)/$*.o: \\"; grep "^ *USE " $^ | \
	sed -e "s/[ \t]*USE  *\([0-9a-zA-Z_]*\).*/  $(OBJDIR)\/\1.o \\\/" | sort -u; \
	echo) > $@

$(OBJDIR)/%.d : %.F90
	@echo "Generating dependency file $(OBJDIR)/$*.d from $<"
	@(echo "$(OBJDIR)/$*.o: \\"; grep "^ *USE " $^ | \
	sed -e "s/[ \t]*USE  *\([0-9a-zA-Z_]*\).*/  $(OBJDIR)\/\1.o \\\/" | sort -u; \
	echo) > $@

$(OBJDIR)/%.d : %.c
	$(CCMAKEDEP) | $(FILTER_C) > $@

DEPS = $(OBJ:.o=.d)
$(DEPS): | $(OBJDIR)
NODEPS := clean
ifeq (0, $(words $(findstring $(MAKECMDGOALS), $(NODEPS))))
  -include $(DEPS)
endif
