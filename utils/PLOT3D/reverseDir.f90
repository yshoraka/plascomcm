! Copyright (c) 2015, University of Illinois at Urbana-Champaign, XPACC
! License: MIT, http://opensource.org/licenses/MIT
Program reverseDir
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!                                                               !
! Reverse the direction of one direction                        !
!                                                               !
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!                                                               !
! Written by Daniel J. Bodony (bodony@stanford.edu)             !
! 26 April 2006                                                 !
!                                                               !
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  USE ModPLOT3D_IO
  Implicit None

  !  Arrays
  Real(KIND=8), Dimension(:,:,:,:,:), Pointer :: X1, X2
  Real(KIND=8) :: XMIN, XMAX, YMIN, YMAX, ZMIN, ZMAX

  !  Integers
  Integer :: ngrid(3), I, J, K, dir, NX, NY, NZ, allblock, thisblock
  Integer, Dimension(:,:), Pointer :: ND1, ND2
  Integer, Dimension(:,:,:,:), Pointer :: IBLANK1, IBLANK2

  !  Logical
  Logical :: gf_exists(3), sf_exists(3)

  !  Characters
  Character(LEN=2)  :: prec(3), grid_format(3), volume_format(3), ib_format(3)
  Character(LEN=80) :: grid_file(3), cmd

  !  Step zero, read in the command line
  Integer :: iargc, nargc, ng, isperiodic, NDIM(2)
  Integer, Parameter :: TRUE = 1

  nargc = iargc()
  If (nargc /= 2) Then
    Write (*,'(A)') 'USAGE: reverseDir {grid file 1} {reversed grid file}'
    Stop
  Else
    Call Getarg(1,grid_file(1));
    Call Getarg(2,grid_file(2));
  End If

  !  Step one, read in the mesh1 grid file
  Write (*,'(A)') ''
  Call Read_Grid(NDIM(1),ngrid(1),ND1,X1,IBLANK1, &
       prec(1),grid_format(1),volume_format(1),ib_format(1),grid_file(1),TRUE)
  Write (*,'(A)') ''

  !  Step two, find reverse dir
  Write (*,'(A)', ADVANCE='NO') 'Enter reversion dir: ' 
  Read (*,*) dir

  !  Step three, reverse all dirs?
  Write (*,'(A)', ADVANCE='NO') 'Reverse all blocks?: '
  Read (*,*) allblock

  !  create temporary array
  Allocate(X2(size(X1,1), size(X1,2), size(X1,3), size(X1,4), size(X1,5)))
  Allocate(IBLANK2(size(X1,1),size(X1,2),size(X1,3),size(X1,4)))
  X2 = X1
  IBLANK2 = IBLANK1
  NDIM(2) = NDIM(1)
  
  !  Step three, reverse the dir
  Do ng = 1, ngrid(1)
    If (allblock .eq. 0) Then
      Write (*,'(A,I2,A,3(I3,1X))') 'Block ', ng, ' has size ', &
            (ND1(ng,i),i=1,3)
      Write (*,'(A)', ADVANCE='NO') 'Reverse this block?: '
      Read (*,*) thisblock
      If (thisblock .eq. 0) CYCLE
    End If
    If (dir .eq. 1) Then
      Do I = 1, ND1(ng,1)
        Do J = 1, ND1(ng,2)
          Do K = 1, ND1(ng,3)
            X1(ng,I,J,K,:) = X2(ng,ND1(ng,1)-I+1,J,K,:)
            IBLANK1(ng,I,J,K) = IBLANK2(ng,ND1(ng,1)-I+1,J,K)
          End Do
        End Do
      End Do
    Else If (dir .eq. 2) Then
      Do I = 1, ND1(ng,1)
        Do J = 1, ND1(ng,2)
          Do K = 1, ND1(ng,3)
            X1(ng,I,J,K,:) = X2(ng,I,ND1(ng,2)-J+1,K,:)
            IBLANK1(ng,I,J,K) = IBLANK2(ng,I,ND1(ng,2)-J+1,K)
          End Do
        End Do
      End Do
    Else
      Do I = 1, ND1(ng,1)
        Do J = 1, ND1(ng,2)
          Do K = 1, ND1(ng,3)
            X1(ng,I,J,K,:) = X2(ng,I,J,ND1(ng,3)-K+1,:)
            IBLANK1(ng,I,J,K) = IBLANK2(ng,I,J,ND1(ng,3)-K+1)
          End Do
        End Do
      End Do
    End If
  End Do

  Call Write_Grid(NDIM(2),ngrid(1),ND1,X1,IBLANK1, &
       prec(2),grid_format(2),volume_format(2),ib_format(2),grid_file(2))
  Write (*,'(A)') ''

  ! Step three, clean up & leave
  Deallocate(ND1, X1, IBLANK1, X2, IBLANK2)


End Program reverseDir
