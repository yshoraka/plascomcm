! Copyright (c) 2015, University of Illinois at Urbana-Champaign, XPACC
! License: MIT, http://opensource.org/licenses/MIT
Program plot3dToGridgen
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!                                                               !
! Read a PLOT3D mesh and convert it a GridGen mesh              !
!                                                               !
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!                                                               !
! Written by Daniel J. Bodony (bodony@uiuc.edu)                 !
! 01 October 2008
!                                                               !
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  USE ModPLOT3D_IO
  Implicit None

  !  Arrays
  Real(KIND=8), Dimension(:,:,:,:,:), Pointer :: X1, X2
  Real(KIND=8) :: XMIN, XMAX, YMIN, YMAX, ZMIN, ZMAX

  !  Integers
  Integer :: ngrid(3), I, J, K, dir, NX, NY, NZ, NDIM(3)
  Integer, Dimension(:,:), Pointer :: ND1, ND2
  Integer, Dimension(:,:,:,:), Pointer :: IBLANK1, IBLANK2

  !  Logical
  Logical :: gf_exists(3), sf_exists(3)

  !  Characters
  Character(LEN=2)  :: prec, gf, vf, ib
  Character(LEN=80) :: grid_file, cmd, file

  !  Step zero, read in the command line
  Integer :: iargc, nargc, ng, isperiodic, ftype
  Integer :: IS, IE, JS, JE, KS, KE, ISKIP, JSKIP, KSKIP
  Integer :: II, JJ, KK

  nargc = iargc()
  If (nargc /= 2) Then
    Write (*,'(A)') 'USAGE: plot3dToGridgen {grid file 1} {grid file 2}'
    Stop
  Endif

  Call Getarg(1,file);
  Call Getarg(2,grid_file);

  Call p3d_detect(LEN(Trim(file)),Trim(file),prec,NDIM,gf,vf,ib,ftype)

  !  Step one, read in the mesh1 grid file
  Write (*,'(A)') ''
  Call Read_Grid(NDIM(1),ngrid(1),ND1,X1,IBLANK1, &
       prec,gf,vf,ib,file,.TRUE.)
  Write (*,'(A)') ''

  ! ... step 2, write the new file
  open (unit=11, file=Trim(grid_file), status='unknown', form='formatted')

  ! ... write the number of blocks
  write (11,*) ngrid(1)

  ! ... write the size of each block
  Do ng = 1, ngrid(1)
    write (11, *) (ND1(ng,i),i=1,3)
  End Do

  Do ng = 1, ngrid(1)

    Write (11,*) (((X1(ng,i,j,k,1),i=1,ND1(ng,1)),j=1,ND1(ng,2)),k=1,ND1(ng,3))
    Write (11,*) (((X1(ng,i,j,k,2),i=1,ND1(ng,1)),j=1,ND1(ng,2)),k=1,ND1(ng,3))
    Write (11,*) (((X1(ng,i,j,k,3),i=1,ND1(ng,1)),j=1,ND1(ng,2)),k=1,ND1(ng,3))

  End Do

  Close (11)

  ! Step three, clean up & leave
  Deallocate(ND1, X1, IBLANK1)


  Stop
End Program plot3dToGridgen
