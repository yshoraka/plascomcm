! Copyright (c) 2015, University of Illinois at Urbana-Champaign, XPACC
! License: MIT, http://opensource.org/licenses/MIT
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!                                                               !
! Extrude a PLOT3D mesh in the periodic direction(s)            !
!                                                               !
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!                                                               !
! Written by Daniel J. Bodony (bodony@illinois.edu)             !
! 02 February 2009                                              !
!                                                               !
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

Program extrudePeriodicMesh

  USE ModPLOT3D_IO
  Implicit None

  !  Arrays
  Real(KIND=8), Dimension(:,:,:,:,:), Pointer :: X1, X2
  Real(KIND=8) :: XMIN, XMAX, YMIN, YMAX, ZMIN, ZMAX

  !  Integers
  Integer :: ngrid(3), I, J, K, dir, NX, NY, NZ, NDIM(3)
  Integer, Dimension(:,:), Pointer :: ND1, ND2
  Integer, Dimension(:,:,:,:), Pointer :: IBLANK1, IBLANK2

  !  Logical
  Logical :: gf_exists(3), sf_exists(3)

  !  Characters
  Character(LEN=2)  :: prec(3), gf(3), vf(3), ib(3)
  Character(LEN=80) :: grid_file(3), cmd

  !  Step zero, read in the command line
  Integer :: iargc, nargc, ng, isperiodic, ftype

  nargc = iargc()
  If (nargc /= 2) Then
    Write (*,'(A)') 'USAGE: extrudePeriodicMesh {grid file 1} {extruded grid file}'
    Stop
  Endif

  Call Getarg(1,grid_file(1));
  Call Getarg(2,grid_file(2));

  Call p3d_detect(LEN(Trim(grid_file(1))),Trim(grid_file(1)),prec,NDIM,gf,vf,ib,ftype)

  !  Step one, read in the mesh1 grid file
  Write (*,'(A)') ''
  Call Read_Grid(NDIM(1),ngrid(1),ND1,X1,IBLANK1, &
       prec(1),gf(1),vf(1),ib(1),grid_file(1),0)
  Write (*,'(A)') ''

  !  Step two, find extrusion dir
  Write (*,'(A)', ADVANCE='NO') 'Enter periodic extrusion dir: ' 
  Read (*,*) dir

  !  Step two, get parameters from user
  ngrid(2) = ngrid(1)
  NDIM(2) = NDIM(1)
  Allocate(ND2(ngrid(2),3))
  ND2 = ND1
  ND2(:,dir) = ND2(:,dir) + 1

  NX = MAXVAL(ND2(:,1))
  NY = MAXVAL(ND2(:,2))
  NZ = MAXVAL(ND2(:,3))
  ALLOCATE(X2(ngrid(2),NX,NY,NZ,3),IBLANK2(ngrid(2),NX,NY,NZ))

  Do ng = 1, ngrid(1)
    Do I = 1, ND1(ng,1)
      Do J = 1, ND1(ng,2)
        Do K = 1, ND1(ng,3)
          X2(ng,I,J,K,1:NDIM(1)) = X1(ng,I,J,K,1:NDIM(1))
          IBLANK2(ng,I,J,K) = IBLANK1(ng,I,J,K)
        End Do
      End Do
    End Do
  End Do

  If (dir .eq. 1) Then
    X2(:,NX,:,:,:) = X2(:,1,:,:,:)
    IBLANK2(:,NX,:,:) = IBLANK2(:,1,:,:)
  Else If (dir .eq. 2) Then
    X2(:,:,NY,:,:) = X2(:,:,1,:,:)
    IBLANK2(:,:,NY,:) = IBLANK2(:,:,1,:)
  Else
    X2(:,:,:,NZ,:) = X2(:,:,:,1,:)
    IBLANK2(:,:,:,NZ) = IBLANK2(:,:,:,1)
  End If

  Call Write_Grid(NDIM(2),ngrid(2),ND2,X2,IBLANK2, &
       prec(1),gf(1),vf(1),ib(1),grid_file(2))
  Write (*,'(A)') ''

  ! Step three, clean up & leave
  Deallocate(ND1, X1, IBLANK1, ND2, X2, IBLANK2)


  Stop
End Program extrudePeriodicMesh
