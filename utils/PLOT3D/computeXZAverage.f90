! Copyright (c) 2015, University of Illinois at Urbana-Champaign, XPACC
! License: MIT, http://opensource.org/licenses/MIT
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!                                                               !
! Compute the time average of velocity fields.                  !
!                                                               !
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!                                                               !
! Written by Daniel J. Bodony (bodony@illinois.edu)             !
! 30 January 2011                                               !
!                                                               !
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

Program computeXZAverage

  USE ModPLOT3D_IO
  Implicit None

  !  Arrays
  Real(KIND=8), Dimension(:,:,:,:,:), Pointer :: X1, X2, time(:)
  Real(KIND=8) :: XMIN, XMAX, YMIN, YMAX, ZMIN, ZMAX, TAU(4)

  !  Integers
  Integer :: ngrid, I, J, K, dir, NX, NY, NZ, NDIM
  Integer, Dimension(:,:), Pointer :: ND1, ND2
  Integer, Dimension(:,:,:,:), Pointer :: IBLANK1, IBLANK2
  Integer :: startIter, stopIter, skipIter, iter

  !  Logical
  Logical :: gf_exists(3), sf_exists(3)

  !  Characters
  Character(LEN=2)  :: prec, gf, vf, ib
  Character(LEN=80) :: infile, outfile, cmd

  !  Doubles
  Real(KIND=8) :: startTime, stopTime, interval, dt
  Real(KIND=8), Pointer :: cvAve(:,:,:)

  !  Step zero, read in the command line
  Integer :: iargc, nargc, ng, isperiodic, ftype, num, numFiles

  nargc = iargc()
  If (nargc /= 3) Then
    Write (*,'(A)') 'USAGE: computeXZAverage <startIter> <stopIter> <skipIter>'
    Stop
  Endif

  Nullify(X1,X2)

  ! ... starting, stopping, skip iteration
  Call Getarg(1,cmd); read (cmd,*) startIter
  Call Getarg(2,cmd); read (cmd,*) stopIter
  Call Getarg(3,cmd); read (cmd,*) skipIter

  Do iter = startIter, stopIter, skipIter

    Write(infile,'(A,I8.8,A)') 'RocFlo-CM.', iter, '.q'

    Call Read_Soln(NDIM,ngrid,ND1,X1,TAU,prec,gf,vf,infile,0)
    Write (*,'(A)') infile

    ! ... allocate
    If (iter == startIter) Allocate(cvAve(ngrid,MAXVAL(ND1(:,2)),5))

    ! ... initialize
    cvAve = 0.0_8 

    ! ... average current solution
    Do ng = 1, ngrid
      Do I = 1, ND1(ng,1)
        Do J = 1, ND1(ng,2)
          Do K = 1, ND1(ng,3)
            cvAve(ng,J,1:5) = cvAve(ng,J,1:5) + X1(ng,I,J,K,1:5)
          End Do
        End Do
      End Do
      cvAve(ng,:,:) = cvAve(ng,:,:) / dble(ND1(ng,1) * ND1(ng,3))
    End Do

    ! ... average current solution
    Do ng = 1, ngrid
      Do I = 1, ND1(ng,1)
        Do J = 1, ND1(ng,2)
          Do K = 1, ND1(ng,3)
            X1(ng,I,J,K,1:5) = cvAve(ng,J,1:5)
          End Do
        End Do
      End Do
    End Do

    ! ... write solution
    Write(outfile,'(A,I8.8,A)') 'unsteady.', iter, '.mean.q.xz-average'
    Call Write_Soln(NDIM,ngrid,ND1,X1,tau,prec,gf,vf,outfile)

    ! ... free memory
    Deallocate(ND1, X1)

  End Do

  Deallocate(cvAve) 

  Stop 'computeXZAverage completed.'

End Program computeXZAverage
