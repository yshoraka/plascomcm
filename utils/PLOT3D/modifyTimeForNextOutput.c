/* Copyright (c) 2015, University of Illinois at Urbana-Champaign, XPACC */
/* License: MIT, http://opensource.org/licenses/MIT */
/* 
 * Automatically detect the type of PLOT3D file to be read
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define S_PREC 4
#define D_PREC 8

void endianSwap(long int, int, int);
size_t safe_fread(long int, int, int, FILE *);
size_t safe_fwrite(long int, int, int, FILE *);

int main(int argc, char *argv[])
{

  FILE *in;
  int i, record, nzones, *N, el_size, ND;
  float ftau[4];
  double dtau[4];

  /* error checking */
  if (argc != 3) {
    fprintf(stderr, "Usage: %s <infile> <new-time>\n",argv[0]);
    exit(EXIT_FAILURE);
  }

  if ((in = fopen(argv[1],"rb+")) == NULL) {
    fprintf(stderr, "Unable to open file %s",argv[1]);
    exit(EXIT_FAILURE);
  }

  /* read first record: if record == sizeof(int): multizone, else single zone */
  safe_fread((long int)&record,sizeof(int),1,in);

  /* if we have a multi-zone file */
  if (record == sizeof(int)) {
    safe_fread((long int)&nzones,sizeof(int),1,in);
    safe_fread((long int)&record,sizeof(int),1,in);
    safe_fread((long int)&record,sizeof(int),1,in);

  } else { /* we have a single-zone file */    
    nzones = 1;
  }
 
  /* check number of dimensions: ND = record / (nzones * sizeof(int)) */
  ND = record / (nzones * sizeof(int));

  /* read the zonal dimensions */
  N = (int *)malloc((size_t)record);
  safe_fread((long int)N, sizeof(int), (size_t)(record/sizeof(int)), in);
  safe_fread((long int)&record,sizeof(int),1,in);

  /* are we a single- or double-precision solution file */
  safe_fread((long int)&record,sizeof(int),1,in);

  if ((record == (4 * sizeof(float)))) {

    safe_fread((long int)ftau,sizeof(float),4,in);
    for (i = 0; i < 4; i++) dtau[i] = (double)ftau[i];
    el_size = S_PREC;

  } else if ((record == (4 * sizeof(double)))) {

    safe_fread((long int)dtau,sizeof(double),4,in);
    el_size = D_PREC;

  } else {
 
    fprintf(stderr, "ERROR: unknown record size.\n");
    exit(EXIT_FAILURE);

  }

  fprintf(stdout,"Modifying %s time for next output from %lf to %lf.\n",argv[1],dtau[1],argv[2]);

  /* modify second element */
  dtau[1] = (double)atof(argv[2]);

  /* re-write this data */
  fseek(in,(long)(-4*el_size),SEEK_CUR);

  if (el_size == S_PREC) {

    ftau[3] = (float)dtau[3];
    safe_fwrite((long int)ftau,sizeof(float),4,in);

  } else {

    safe_fwrite((long int)dtau,sizeof(double),4,in);

  }

  /* re-write this data */
  fseek(in,(long)(-4*el_size),SEEK_CUR);
  safe_fread((long int)dtau,sizeof(double),4,in);

  fclose (in);

  return EXIT_SUCCESS;

}

size_t safe_fread(long int addr, int size_el, int num_el, FILE *in)
{
  size_t retval;
  char *a;

  a = (char *)addr;
  
  retval = fread(a, size_el, num_el, in);
 
  #ifdef LE
  endianSwap((long int)a, num_el, size_el);
  #endif

  return(retval);
}

size_t safe_fwrite(long int addr, int size_el, int num_el, FILE *in)
{
  size_t retval;
  char *a;

  a = (char *)addr;

 
  #ifdef LE
  endianSwap((long int)a, num_el, size_el);
  #endif

  retval = fwrite(a, size_el, num_el, in);

  return(retval);
}

/*********************************************************************
 *
 * endianSwap(addr,num_el,size_el): Changes between big- and 
 * little-endian by address-swapping.
 *
 * addr: (long int) address of variable to be swapped.
 * num_el: (int) number of elements in array pointed to by addr
 * size_el: (int) size of individual elements of *addr
 *
 * Written by Daniel J. Bodony (bodony@Stanford.EDU)
 * Copyright (c) 2001
 *
 * WARNING: This is only works on arrays that contiguous in memory.
 *
 *********************************************************************/

#define SWAP(a,b) temp=(a); (a)=(b); (b)=temp;

void endianSwap (long int addr, int num_el, int size_el) 
{
   int i, x;
   char *a;
   char temp;

   a = (char *)addr;

   for (i = 0; i < num_el; i++) {
      a = (char *)(addr + i*size_el);
      for (x = 0; x < size_el/2; x++) {
         SWAP(a[x],a[size_el-x-1]);
      }
   }
}
