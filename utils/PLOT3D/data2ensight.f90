! Copyright (c) 2015, University of Illinois at Urbana-Champaign, XPACC
! License: MIT, http://opensource.org/licenses/MIT
Program data2ensight
! ============================================================= !
!                                                               !
! Converts PlasComCM data file to EnSight format                !
!                                                               !
! ============================================================= !
!                                                               !
! Written by Jesse Capecelatro (jcaps@illinois.edu)             !
! 9 August 2014                                                 !
!                                                               !
! ============================================================= !
  USE ModPLOT3D_IO
  Implicit None

  !  Arrays
  Real(KIND=8), Dimension(:,:,:,:,:), Pointer :: X1, Q1, Q2
  Real(KIND=8), Dimension(:), Allocatable :: time
  Real(KIND=8) :: TAU(4)
  Real(KIND=4), Dimension(:,:,:), Allocatable :: rbuffer
  Real(KIND=4) :: XMIN, XMAX, YMIN, YMAX, ZMIN, ZMAX

  !  Integers
  Integer :: ngrid, I, J, K, N, NX, NY, NZ, NDIM, npart, reclength
  Integer, Dimension(:,:), Pointer :: ND1
  Integer, Dimension(:,:,:,:), Pointer :: IBLANK1
  Integer :: startIter, stopIter, skipIter, iter, var, nvar, ierr, ibuffer

  !  Characters
  character(LEN=80), dimension(:), Allocatable :: names
  Character(LEN=2)  :: prec, gf, vf, ib, ib2, use_aux
  Character(LEN=80) :: fname,directory,grid_name
  character(LEN=80) :: binary_form
  character(LEN=80) :: file_description1,file_description2
  character(LEN=80) :: node_id,element_id
  character(LEN=80) :: part,description_part,cblock,extents,cbuffer

  !  Step zero, read in the command line
  Integer :: ng, num, numFiles, naux

  ! Read information from standard input
  print*,'=============================================='
  print*,'| PlasComCM - data to ENSIGHT GOLD converter |'
  print*,'=============================================='
  print*
  print "(a14,$)", " start iter : "
  read "(i6)", startIter
  print "(a13,$)", " stop iter : "
  read "(i6)", stopIter
  print "(a13,$)", " skip iter : "
  read "(i6)", skipIter
  print "(a23,$)", " read auxilary (y/n) : "
  read "(a)", use_aux
  !print "(a13,$)", " grid name : "
  !read "(a)", grid_name
  !print "(a14,$)", " use iblank : "
  !read "(a)", ib
  directory='ensight-3D'
  grid_name='grid.xyz'
  ib='y'

  ! Check for errors
  If ((ib .NE. 'y') .AND. (ib .NE. 'n')) Then
     Write (*,'(A)') 'Invalid value for IBLANK: use either "y" (yes) or "n" (no)'
     Stop
  End If
  If ((use_aux .NE. 'y') .AND. (use_aux .NE. 'n')) Then
     Write (*,'(A)') 'Invalid value for read auxilary: use either "y" (yes) or "n" (no)'
     Stop
  End If

  ! Initialize
  numFiles = 0
  Do iter = startIter, stopIter, skipIter
     numFiles = numFiles + 1
  End Do

  ! Find number of variables (should be 5+)
  Write(fname,'(A,I8.8,A)') 'RocFlo-CM.', startIter, '.q'
  Call Read_Soln(NDIM,ngrid,ND1,Q1,TAU,prec,gf,vf,fname,0)
  If (use_aux.eq.'y') Then
     Write(fname,'(A,I8.8,A)') 'RocFlo-CM.', startIter, '.aux.q'
     Call Read_Func(NDIM, ngrid, ND1, Q2, fname)
     naux =  SIZE(Q2(1,1,1,1,:))
  Else
     naux=0
  End If
  nvar=SIZE(Q1(1,1,1,1,:)) + naux
  If (nvar < 5) then
     Write (*,'(A)') 'Number of variables less than 5'
     Stop
  Else
     Print *, 'Number of variables:',nvar
     Write (*,'(A)') ''
  End If

  ! Assign variable names
  allocate(names(nvar))
  names(1) = 'RHO'
  names(2) = 'U'
  names(3) = 'V'
  names(4) = 'W'
  names(5) = 'E'
  if (naux > 0) then
    names(6) = 'H2'
    names(7) = 'O2'
    if(naux >= 3) names(8) = 'H'
  end if

  ! Loop over all files, find time array first
  allocate(time(numFiles))
  time = 0.0_8
  num=0
  Do iter = startIter, stopIter, skipIter
    Write(fname,'(A,I8.8,A)') 'RocFlo-CM.', iter, '.q'

    ! ... read solution header to find time
    Call Read_Soln_Header(NDIM, ngrid, ND1, fname, tau)

    ! ... save time
    num = num + 1
    time(num) = tau(4)

  End Do

  ! Create the EnSight directory
  call system("mkdir -p "//trim(directory))


  ! ======================= !
  ! Write the geometry file !
  ! ======================= !

  !  Step one, read in the mesh1 grid file
  Write (*,'(A)') ''
  Call Read_Grid(NDIM,ngrid,ND1,X1,IBLANK1, &
       prec,gf,vf,ib2,grid_name,1)
  Write (*,'(A)') ''

  ! Loop through number of grids
  Do ng=1, ngrid
     ! Get mesh extents (single precision)
     NX = ND1(ng,1)
     NY = ND1(ng,2)
     NZ = ND1(ng,3)

     XMIN = minval(X1(ng,1:NX,1:NY,1:NZ,1))
     XMAX = maxval(X1(ng,1:NX,1:NY,1:NZ,1))
     YMIN = minval(X1(ng,1:NX,1:NY,1:NZ,2))
     YMAX = maxval(X1(ng,1:NX,1:NY,1:NZ,2))
     ZMIN = minval(X1(ng,1:NX,1:NY,1:NZ,3))
     ZMAX = maxval(X1(ng,1:NX,1:NY,1:NZ,3))

     ! Write EnSight geometry
     binary_form      ='C Binary'
     file_description1='Ensight Gold Geometry File'
     file_description2='WriteRectEnsightGeo Routine'
     node_id          ='node id is off'
     element_id       ='element id off'
     part             ='part'
     npart            =1
     Write(description_part,'(A,I1)') 'Grid', ng
     If (ib .EQ. 'y') then
        cblock        ='block iblanked'
     else
        cblock        ='block'
     End If
     extents          ='extents'

     ! Size of file
     If (ib .EQ. 'y') Then
        reclength=80*8+4*(4+4*NX*NY*NZ)
     Else
        reclength=80*9+4*(4+3*NX*NY*NZ)
     End If

     ! Write file
     Write(fname,'(2A,I1)') trim(directory), '/geometry', ng
     print *, 'Writing: ',fname
     !fname = trim(directory) // '/geometry'
     Open (unit=10, file=trim(fname), form='unformatted', access="direct", recl=reclength)
     If (ib .EQ. 'y') then
        write(unit=10,rec=1) binary_form, &
             file_description1, &
             file_description2, &
             node_id, &
             element_id, &
             part,npart, &
             description_part, &
             cblock, &
             NX,NY,NZ, &
             (((sngl(X1(ng,I,J,K,1)), I=1,NX), J=1,NY), K=1,NZ), &
             (((sngl(X1(ng,I,J,K,2)), I=1,NX), J=1,NY), K=1,NZ), &
             (((sngl(X1(ng,I,J,K,3)), I=1,NX), J=1,NY), K=1,NZ), &
             (((IBLANK1(ng,I,J,K), I=1,NX), J=1,NY), K=1,NZ)
     Else
        write(unit=10,rec=1) binary_form, &
             file_description1, &
             file_description2, &
             node_id, &
             element_id, &
             part,npart, &
             description_part, &
             cblock, &
             NX,NY,NZ, &
             (((sngl(X1(ng,I,J,K,1)), I=1,NX), J=1,NY), K=1,NZ), &
             (((sngl(X1(ng,I,J,K,2)), I=1,NX), J=1,NY), K=1,NZ), &
             (((sngl(X1(ng,I,J,K,3)), I=1,NX), J=1,NY), K=1,NZ)
     End If
  
     ! Close the file
     Close (10)

  End Do


  ! ===================================== !
  ! Loop through and write the data files !
  ! ===================================== !

  ! Allocate single-precision array
  Allocate(rbuffer(1:maxval(ND1(:,1)),1:maxval(ND1(:,2)),1:maxval(ND1(:,3))))

  ! Loop through files and write
  num = 1
  Do iter = startIter, stopIter, skipIter
    print *, 'Writing timestep',iter
    
     Write(fname,'(A,I8.8,A)') 'RocFlo-CM.', iter, '.q'
     Write (*,'(A)') fname
     Call Read_Soln(NDIM,ngrid,ND1,Q1,TAU,prec,gf,vf,fname,0)
     If (naux.gt.0) Then
        Write(fname,'(A,I8.8,A)') 'RocFlo-CM.', iter, '.aux.q'
        Call Read_Func(NDIM, ngrid, ND1, Q2, fname)
     End If

     ! Loop through each grid
     Do ng=1, ngrid

        ! Get domain size
        NX = ND1(ng,1)
        NY = ND1(ng,2)
        NZ = ND1(ng,3)

        ! Binary file length
        reclength=80*3+4*(1+NX*NY*NZ)

        Do var=1, nvar

           ! Convert the data to single precision
           If (var <= 5) Then
              rbuffer(1:NX,1:NY,1:NZ) = sngl(Q1(ng,1:NX,1:NY,1:NZ,var))
           Else
              rbuffer(1:NX,1:NY,1:NZ) = sngl(Q2(ng,1:NX,1:NY,1:NZ,var-5)/Q1(ng,1:NX,1:NY,1:NZ,1))
           End If

           ! Divide by rho for rhoU, rhoV, and rhoW
           If (var>1 .and. var<=5) then
              rbuffer(1:NX,1:NY,1:NZ)=sngl(Q1(ng,1:NX,1:NY,1:NZ,var)/Q1(ng,1:NX,1:NY,1:NZ,1))
           End If

           ! Write Ensight scalar file
           cbuffer=trim(names(var))
           Write(fname,'(3A,I1,A,I6.6)') trim(directory),'/',trim(names(var)),ng,'.',num
           Open (unit=10, file=trim(fname), form='unformatted', access="direct", recl=reclength)
           write(unit=10, rec=1) cbuffer,part,npart,cblock,(((rbuffer(i,j,k),i=1,NX),j=1,NY),k=1,NZ)
           Close(10)

        End Do

        ! Write IBLANK
        rbuffer(1:NX,1:NY,1:NZ) = Real(IBLANK1(ng,1:NX,1:NY,1:NZ),4)
        Write(fname,'(2A,I1,A,I6.6)') trim(directory),'/IB',ng,'.',num
        Open (unit=10, file=trim(fname), form='unformatted', access="direct", recl=reclength)
        write(unit=10, rec=1) cbuffer,part,npart,cblock,(((rbuffer(i,j,k),i=1,NX),j=1,NY),k=1,NZ)
        Close(10)
     End Do

     ! ... counter
     num = num + 1
  End Do
  

  ! =================== !
  ! Write the case file !
  ! =================== !
  Do ng=1, ngrid
     Write(fname,'(2A,I1,A)') trim(directory), '/plascomcm', ng, '.case'
     open(10,file=trim(fname),form="formatted",iostat=ierr,status="REPLACE")

     ! Write the case
     cbuffer='FORMAT'
     write(10,'(a80)') cbuffer
     cbuffer='type: ensight gold'
     write(10,'(a80)') cbuffer
     cbuffer='GEOMETRY'
     write(10,'(a80)') cbuffer
     Write(cbuffer,'(A,I1)') 'model: geometry', ng
     write(10,'(a80)') cbuffer

     cbuffer='VARIABLE'
     write(10,'(a)') cbuffer
     Do var=1,nvar
        Write(cbuffer,'(2A,I1,2A,I1,A)') 'scalar per node: 1 ',  trim(names(var)), ng, ' ', trim(names(var)), ng, '.******'
        write(10,'(a80)') cbuffer
     End Do

     ! Write IB
     Write(cbuffer,'(1A,I1,1A,I1,A)') 'scalar per node: 1 IB', ng, ' IB', ng, '.******'
     write(10,'(a80)') cbuffer
  
     cbuffer='TIME'
     write(10,'(a80)') cbuffer
     cbuffer='time set: 1'
     write(10,'(a80)') cbuffer
     write(10,'(a16,x,i12)') 'number of steps:',numFiles
     cbuffer='filename start number: 1'
     write(10,'(a80)') cbuffer
     cbuffer='filename increment: 1'
     write(10,'(a80)') cbuffer
     write(10,'(a12,x,10000000(3(ES12.5,x),/))') 'time values:',time

     ! Close the file
     close(10)

  End Do

  ! Clean up & leave
  Deallocate(ND1, X1, Q1, IBLANK1, time)
  If (naux.gt.0) Deallocate(Q2)

End Program data2ensight
