! Copyright (c) 2015, University of Illinois at Urbana-Champaign, XPACC
! License: MIT, http://opensource.org/licenses/MIT
Program analyzer
! ============================================================= !
!                                                               !
! Reads in PLOT3D data and averages at a distance in x          !
!                                                               !
! ============================================================= !
!                                                               !
! Written by Jesse Capecelatro (jcaps@illinois.edu)             !
! 1 October 2014                                                !
!                                                               !
! ============================================================= !
  USE ModPLOT3D_IO
  Implicit None

  !  Arrays
  Real(KIND=8), Dimension(:,:,:,:,:), Pointer :: X, Q
  Real(KIND=8), Dimension(:), Allocatable :: time
  Real(KIND=8), Dimension(:,:,:), Allocatable :: RHO,U,V,W,P
  Real(KIND=8), Dimension(:,:,:,:), Allocatable :: U0,V0,W0,P0
  Real(KIND=8), Dimension(:), Allocatable :: meanU,meanV,meanW,meanP
  Real(KIND=8), Dimension(:), Allocatable :: meanUU,meanVV,meanWW,meanUV,meanPP
  Real(KIND=8), Dimension(:,:), Allocatable :: meanU2D,meanV2D,meanW2D,meanP2D
  Real(KIND=8), Dimension(:,:), Allocatable :: R11,R22,R33,Rpp
  Real(KIND=8), Dimension(:,:), Allocatable :: T11,T22,T33,Tpp
  Integer, Dimension(:,:), Allocatable :: countT
  Real(KIND=8) :: TAU(4),T0
  Real(KIND=8) :: X1,X2

  !  Integers
  Integer :: ngrid, I, J, K, N, NN, NX, NY, NZ, NDIM, I1,I2
  Integer, Dimension(:,:), Pointer :: ND1
  Integer, Dimension(:,:,:,:), Pointer :: IBLANK
  Integer :: startIter, stopIter, skipIter, iter, var, nvar, numfiles, num, numt, num2d, ierr, ibuffer

  !  Characters
  character(LEN=80), dimension(:), Allocatable :: names
  Character(LEN=2)  :: prec, gf, vf, ib
  Character(LEN=80) :: fname,grid_name

  ! Read information from standard input
  print*,'================================================='
  print*,'| PlasComCM - Boundary Layer Analyzer           |'
  print*,'| Compute mean/RMS velocity & 2-pt correlations |'
  print*,'================================================='
  print*
  print "(a14,$)", " start iter : "
  read "(i6)", startIter
  print "(a13,$)", " stop iter : "
  read "(i6)", stopIter
  print "(a13,$)", " skip iter : "
  read "(i6)", skipIter
  !print "(a13,$)", " grid name : "
  !read "(a)", grid_name
  grid_name='grid.xyz'
  X1 = 0.345D0
  X2 = 0.385D0

  ! Initialize
  numFiles = 0
  Do iter = startIter, stopIter, skipIter
     numFiles = numFiles + 1
  End Do

  ! Find number of variables (should be 5)
  Write(fname,'(A,I8.8,A)') 'RocFlo-CM.', startIter, '.q'
  Call Read_Soln(NDIM,ngrid,ND1,Q,TAU,prec,gf,vf,fname,0)
  nvar=SIZE(Q(1,1,1,1,:))
  If (nvar /= 5) then
     Write (*,'(A)') 'Number of variables not equal 5'
     Stop
  End If

  ! Assign variable names
  allocate(names(nvar))
  names(1) = 'RHO'
  names(2) = 'U'
  names(3) = 'V'
  names(4) = 'W'
  names(5) = 'E'

  ! Loop over all files, store time info
  allocate(time(numFiles))
  num=0
  Do iter = startIter, stopIter, skipIter
    Write(fname,'(A,I8.8,A)') 'RocFlo-CM.', iter, '.q'

    ! ... read solution header to find time
    Call Read_Soln_Header(NDIM, ngrid, ND1, fname, tau)

    ! ... save time
    num = num + 1
    time(num) = tau(4)

  End Do
  t0 = time(1)

  ! Read grid
  Write (*,'(A)') ''
  Call Read_Grid(NDIM,ngrid,ND1,X,IBLANK, &
       prec,gf,vf,ib,grid_name,1)
  Write (*,'(A)') ''
  NX = ND1(1,1)
  NY = ND1(1,2)
  NZ = ND1(1,3)

  ! Find measurement location
  Do I=1,NX
      If (X(1,I,1,1,1).LE.X1) I1=I
  End Do
  Do I=NX,1,-1
      If (X(1,I,1,1,1).GE.X2) I2=I
  End Do
  print *, 'Measurement at grid:',I1,':',I2

  ! Allocate / initialize arrays
  Allocate(RHO(1:NX,1:NY,1:NZ))
  Allocate(U(1:NX,1:NY,1:NZ))
  Allocate(V(1:NX,1:NY,1:NZ))
  Allocate(W(1:NX,1:NY,1:NZ))
  Allocate(P(1:NX,1:NY,1:NZ))
  Allocate(U0(I1:I2,1:NY,1:NZ,numFiles))
  Allocate(V0(I1:I2,1:NY,1:NZ,numFiles))
  Allocate(W0(I1:I2,1:NY,1:NZ,numFiles))
  Allocate(P0(I1:I2,1:NY,1:NZ,numFiles))
  Allocate(meanU(1:NY)); meanU=0.0D0
  Allocate(meanV(1:NY)); meanV=0.0D0
  Allocate(meanW(1:NY)); meanW=0.0D0
  Allocate(meanP(1:NY)); meanP=0.0D0
  Allocate(meanUU(1:NY)); meanUU=0.0D0
  Allocate(meanVV(1:NY)); meanVV=0.0D0
  Allocate(meanWW(1:NY)); meanWW=0.0D0
  Allocate(meanUV(1:NY)); meanUV=0.0D0
  Allocate(meanPP(1:NY)); meanPP=0.0D0
  Allocate(meanU2D(I1:I2,1:NY)); meanU2D=0.0D0
  Allocate(meanV2D(I1:I2,1:NY)); meanV2D=0.0D0
  Allocate(meanW2D(I1:I2,1:NY)); meanW2D=0.0D0
  Allocate(meanP2D(I1:I2,1:NY)); meanP2D=0.0D0
  Allocate(R11(1:(NZ-1)/2,NY)); R11=0.0D0
  Allocate(R22(1:(NZ-1)/2,NY)); R22=0.0D0
  Allocate(R33(1:(NZ-1)/2,NY)); R33=0.0D0
  Allocate(Rpp(1:(NZ-1)/2,NY)); Rpp=0.0D0
  Allocate(T11(1:numFiles,NY)); T11=0.0D0
  Allocate(T22(1:numFiles,NY)); T22=0.0D0
  Allocate(T33(1:numFiles,NY)); T33=0.0D0
  Allocate(Tpp(1:numFiles,NY)); Tpp=0.0D0
  Allocate(countT(1:numFiles,NY)); countT=0

  ! ============================ !
  ! Loop through andaverage data !
  ! ============================ !

  ! Loop through solution files
  num  =0
  numt =0
  num2d=0
  Do iter = startIter, stopIter, skipIter
     numt=numt+1

     ! Get filename
     Write(fname,'(A,I8.8,A)') 'RocFlo-CM.', iter, '.q'
     print *, 'Analyzing',fname

     ! Read data
     Call Read_Soln(NDIM,ngrid,ND1,Q,TAU,prec,gf,vf,fname,0)

     ! Get variables
     rho = Q(1,:,:,:,1)
     U   = Q(1,:,:,:,2)/rho
     V   = Q(1,:,:,:,3)/rho
     W   = Q(1,:,:,:,4)/rho
     P   = 0.4D0*(Q(1,:,:,:,5)-0.5D0*rho*(U*U+V*V+W*W))

     ! Compute average
     Do K=1,NZ
        num2d=num2d+1
        Do I=I1,I2
           num=num+1
           Do J=1,NY
              meanU(J) = meanU(J) + U(I,J,K)
              meanV(J) = meanV(J) + V(I,J,K)
              meanW(J) = meanW(J) + W(I,J,K)
              meanP(J) = meanP(J) + P(I,J,K)
              meanUU(J) = meanUU(J) + U(I,J,K)**2
              meanVV(J) = meanVV(J) + V(I,J,K)**2
              meanWW(J) = meanWW(J) + W(I,J,K)**2
              meanUV(J) = meanUV(J) + U(I,J,K)*V(I,J,K)
              meanPP(J) = meanPP(J) + P(I,J,K)**2
              meanU2D(I,J) = meanU2D(I,J) + U(I,J,K)
              meanV2D(I,J) = meanV2D(I,J) + V(I,J,K)
              meanW2D(I,J) = meanW2D(I,J) + W(I,J,K)
              meanP2D(I,J) = meanP2D(I,J) + P(I,J,K)

              ! Store initial data
              U0(I,J,K,numt) = U(I,J,K)
              V0(I,J,K,numt) = V(I,J,K)
              W0(I,J,K,numt) = W(I,J,K)
              P0(I,J,K,numt) = P(I,J,K)
           End Do
        End Do
     End Do

  End Do

  ! Normalize
  meanU = meanU/real(num,8)
  meanV = meanV/real(num,8)
  meanW = meanW/real(num,8)
  meanP = meanP/real(num,8)
  meanUU = meanUU/real(num,8)
  meanVV = meanVV/real(num,8)
  meanWW = meanWW/real(num,8)
  meanUV = meanUV/real(num,8)
  meanPP = meanPP/real(num,8)
  meanU2D = meanU2D/real(num2d,8)
  meanV2D = meanV2D/real(num2d,8)
  meanW2D = meanW2D/real(num2d,8)
  meanP2D = meanP2D/real(num2d,8)

  ! Write to file
  print *, 'Writing meanVel.txt'
  open(10,file='meanVel.txt',form="formatted",iostat=ierr,status="REPLACE")

  ! Write header
  write(10,'(6a15)') "y","meanU","meanV","varU","varV","varUV"

  ! Write statistics
  Do J=1,NY
     write(10,'(6ES15.6)') X(1,I1,J,1,2),meanU(J),meanV(J),meanUU(J)-meanU(J)*meanU(J),meanVV(J)-meanV(J)*meanV(J),meanUV(J)-meanU(J)*meanV(J)
  End Do

  ! Close the file
  close(10)

  ! Compute two-point correlations
  ! Loop through solution files
  num =0
  numt=0
  Do iter = startIter, stopIter, skipIter
     numt=numt+1

     ! Get filename
     Write(fname,'(A,I8.8,A)') 'RocFlo-CM.', iter, '.q'
     print *, 'Computing 2-point stats on',fname

     ! Read data
     Call Read_Soln(NDIM,ngrid,ND1,Q,TAU,prec,gf,vf,fname,0)

     ! Get variables
     rho = Q(1,:,:,:,1)
     U   = Q(1,:,:,:,2)/rho
     V   = Q(1,:,:,:,3)/rho
     W   = Q(1,:,:,:,4)/rho
     P   = 0.4D0*(Q(1,:,:,:,5)-0.5D0*rho*(U*U+V*V+W*W))

     ! Compute two-point velocity correlation
     Do K=1,NZ
        Do I=I1,I2
           num=num+1
           Do J=1,NY
              ! Time correlation
              Do N=numt,numFiles
                 T11(N-numt+1,J) = T11(N-numt+1,J) + (U(I,J,K)-meanU2D(I,J))*(U0(I,J,K,N)-meanU2D(I,J))
                 T22(N-numt+1,J) = T22(N-numt+1,J) + (V(I,J,K)-meanV2D(I,J))*(V0(I,J,K,N)-meanV2D(I,J))
                 T33(N-numt+1,J) = T33(N-numt+1,J) + (W(I,J,K)-meanW2D(I,J))*(W0(I,J,K,N)-meanW2D(I,J))
                 Tpp(N-numt+1,J) = Tpp(N-numt+1,J) + (P(I,J,K)-meanP2D(I,J))*(P0(I,J,K,N)-meanP2D(I,J))
                 countT(N-numt+1,J) = countT(N-numt+1,J) + 1
              End Do

              ! Spatial correlations
              Do N=1,(NZ-1)/2
                 ! Look to the right
                 NN = K+N-1
                 If (NN.GT.NZ) NN = NN-NZ
                 R11(N,J) = R11(N,J) + (U(I,J,K)-meanU2D(I,J))*(U(I,J,NN)-meanU2D(I,J))
                 R22(N,J) = R22(N,J) + (V(I,J,K)-meanV2D(I,J))*(V(I,J,NN)-meanV2D(I,J))
                 R33(N,J) = R33(N,J) + (W(I,J,K)-meanW2D(I,J))*(W(I,J,NN)-meanW2D(I,J))
                 Rpp(N,J) = Rpp(N,J) + (P(I,J,K)-meanP2D(I,J))*(P(I,J,NN)-meanP2D(I,J))
                 ! Look to the left
                 NN = K-N+1
                 If (NN.LT.1) NN = NZ+NN
                 R11(N,J) = R11(N,J) + (U(I,J,K)-meanU2D(I,J))*(U(I,J,NN)-meanU2D(I,J))
                 R22(N,J) = R22(N,J) + (V(I,J,K)-meanV2D(I,J))*(V(I,J,NN)-meanV2D(I,J))
                 R33(N,J) = R33(N,J) + (W(I,J,K)-meanW2D(I,J))*(W(I,J,NN)-meanW2D(I,J))
                 Rpp(N,J) = Rpp(N,J) + (P(I,J,K)-meanP2D(I,J))*(P(I,J,NN)-meanP2D(I,J))
              End Do
           End Do
        End Do
     End Do

  End Do

  ! Normalize
  Do N=1,(NZ-1)/2
     Do J=2,NY
        R11(N,J) = R11(N,J)/real(2*num,8)/(meanUU(J)-meanU(J)*meanU(J))
        R22(N,J) = R22(N,J)/real(2*num,8)/(meanVV(J)-meanV(J)*meanV(J))
        R33(N,J) = R33(N,J)/real(2*num,8)/(meanWW(J)-meanW(J)*meanW(J))
        Rpp(N,J) = Rpp(N,J)/real(2*num,8)/(meanPP(J)-meanP(J)*meanP(J))
     End Do
  End Do

  Do N=1,numFiles
     Do J=2,NY
        T11(N,J) = T11(N,J)/real(countT(N,J),8)/(meanUU(J)-meanU(J)*meanU(J))
        T22(N,J) = T22(N,J)/real(countT(N,J),8)/(meanVV(J)-meanV(J)*meanV(J))
        T33(N,J) = T33(N,J)/real(countT(N,J),8)/(meanWW(J)-meanW(J)*meanW(J))
        Tpp(N,J) = Tpp(N,J)/real(countT(N,J),8)/(meanPP(J)-meanP(J)*meanP(J))
     End Do
  End Do

  ! Write to file
  print *, 'Writing 2pt stats'

  ! R11
  open(10,file='R11.txt',form="formatted",iostat=ierr,status="REPLACE")
  write(10,'(1a15,1000Es15.6)') "r",(X(1,I1,J,1,2),J=1,NY)
  Do N=1,(NZ-1)/2
     write(10,'(1000ES15.6)') X(1,I1,1,N,3),(R11(N,J),J=1,NY)
  End Do
  close(10)

  ! R22
  open(10,file='R22.txt',form="formatted",iostat=ierr,status="REPLACE")
  write(10,'(1a15,1000Es15.6)') "r",(X(1,I1,J,1,2),J=1,NY)
  Do N=1,(NZ-1)/2
     write(10,'(1000ES15.6)') X(1,I1,1,N,3),(R22(N,J),J=1,NY)
  End Do
  close(10)

  ! R33
  open(10,file='R33.txt',form="formatted",iostat=ierr,status="REPLACE")
  write(10,'(1a15,1000Es15.6)') "r",(X(1,I1,J,1,2),J=1,NY)
  Do N=1,(NZ-1)/2
     write(10,'(1000ES15.6)') X(1,I1,1,N,3),(R33(N,J),J=1,NY)
  End Do
  close(10)

  ! Rpp
  open(10,file='Rpp.txt',form="formatted",iostat=ierr,status="REPLACE")
  write(10,'(1a15,1000Es15.6)') "r",(X(1,I1,J,1,2),J=1,NY)
  Do N=1,(NZ-1)/2
     write(10,'(1000ES15.6)') X(1,I1,1,N,3),(Rpp(N,J),J=1,NY)
  End Do
  close(10)

  ! T11
  open(10,file='T11.txt',form="formatted",iostat=ierr,status="REPLACE")
  write(10,'(1a15,1000Es15.6)') "t",(X(1,I1,J,1,2),J=1,NY)
  Do N=1,numFiles
     write(10,'(1000ES15.6)') time(N)-t0,(T11(N,J),J=1,NY)
  End Do
  close(10)

  ! T22
  open(10,file='T22.txt',form="formatted",iostat=ierr,status="REPLACE")
  write(10,'(1a15,1000Es15.6)') "t",(X(1,I1,J,1,2),J=1,NY)
  Do N=1,numFiles
     write(10,'(1000ES15.6)') time(N)-t0,(T22(N,J),J=1,NY)
  End Do
  close(10)

  ! T33
  open(10,file='T33.txt',form="formatted",iostat=ierr,status="REPLACE")
  write(10,'(1a15,1000Es15.6)') "t",(X(1,I1,J,1,2),J=1,NY)
  Do N=1,numFiles
     write(10,'(1000ES15.6)') time(N)-t0,(T33(N,J),J=1,NY)
  End Do
  close(10)

  ! Tpp
  open(10,file='Tpp.txt',form="formatted",iostat=ierr,status="REPLACE")
  write(10,'(1a15,1000Es15.6)') "t",(X(1,I1,J,1,2),J=1,NY)
  Do N=1,numFiles
     write(10,'(1000ES15.6)') time(N)-t0,(Tpp(N,J),J=1,NY)
  End Do
  close(10)

  ! Clean up & leave
  Deallocate(ND1, X, Q, IBLANK, &
       RHO, U, V, W, &
       meanU,meanV,meanUU, meanVV, meanUV,&
       R11, R22, R33, Rpp, &
       T11, T22, T33, Tpp, &
       time, countT)

End Program analyzer
