! Copyright (c) 2015, University of Illinois at Urbana-Champaign, XPACC
! License: MIT, http://opensource.org/licenses/MIT
Program Ogrid
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!                                                               !
! Make one direction O-grid periodic                            !
!                                                               !
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!                                                               !
! Written by Daniel J. Bodony (bodony@stanford.edu)             !
! 26 April 2006                                                 !
!                                                               !
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  USE ModPLOT3D_IO
  Implicit None

  !  Arrays
  Real(KIND=8), Dimension(:,:,:,:,:), Pointer :: X1, X2
  Real(KIND=8) :: XMIN, XMAX, YMIN, YMAX, ZMIN, ZMAX

  !  Integers
  Integer :: ngrid(3), I, J, K, dir, NX, NY, NZ, allblock, thisblock
  Integer, Dimension(:,:), Pointer :: ND1, ND2
  Integer, Dimension(:,:,:,:), Pointer :: IBLANK1, IBLANK2

  !  Logical
  Logical :: gf_exists(3), sf_exists(3)

  !  Characters
  Character(LEN=2)  :: prec(3), grid_format(3), volume_format(3), ib_format(3)
  Character(LEN=80) :: grid_file(3), cmd

  !  Step zero, read in the command line
  Integer :: iargc, nargc, ng, isperiodic, NDIM(3)
  Integer, Parameter :: TRUE = 1

  nargc = iargc()
  If (nargc /= 2) Then
    Write (*,'(A)') 'USAGE: Ogrid {grid file 1} {periodized grid file}'
    Stop
  Else

    Call Getarg(1,grid_file(1));
    Call Getarg(2,grid_file(2));

  End If

  !  Step one, read in the mesh1 grid file
  Call Read_Grid(NDIM(1),ngrid(1),ND1,X1,IBLANK1, &
       prec(1),grid_format(1),volume_format(1),ib_format(1),grid_file(1),TRUE)
  Write (*,'(A)') ''

  !  Step two, find reverse dir
  Write (*,'(A)', ADVANCE='NO') 'Enter periodic dir: ' 
  Read (*,*) dir

  !  Step three, reverse all dirs?
  Write (*,'(A)', ADVANCE='NO') 'Periodize all blocks?: '
  Read (*,*) allblock

  !  create temporary array
  ngrid(2) = ngrid(1)
  Allocate(ND2(size(ND1,1),size(ND1,2)))
  ND2 = ND1
  NDIM(2) = NDIM(1)

  !  Step three, reverse the dir
  Do ng = 1, ngrid(1)
    If (allblock .eq. 0) Then
      Write (*,'(A,I2,A,3(I3,1X))') 'Block ', ng, ' has size ', &
            (ND1(ng,i),i=1,3)
      Write (*,'(A)', ADVANCE='NO') 'Periodize this block?: '
      Read (*,*) thisblock
      If (thisblock .eq. 0) CYCLE
      ND2(ng,dir) = ND2(ng,dir) + 1
    Else
      ND2(:,dir) = ND2(:,dir) + 1
    End If
  End Do

  Allocate(X2(size(ND2,1), maxval(nd2(:,1)), maxval(nd2(:,2)), maxval(nd2(:,3)), size(X1,5)))
  Allocate(IBLANK2(size(ND2,1), maxval(nd2(:,1)), maxval(nd2(:,2)), maxval(nd2(:,3))))

  Do ng = 1, ngrid(2)
    X2(ng,1:ND1(ng,1),1:ND1(ng,2),1:ND1(ng,3),:) = X1(ng,1:ND1(ng,1),1:ND1(ng,2),1:ND1(ng,3),:)
    IBLANK2(ng,1:ND1(ng,1),1:ND1(ng,2),1:ND1(ng,3)) = IBLANK1(ng,1:ND1(ng,1),1:ND1(ng,2),1:ND1(ng,3))
  End Do

  Do ng = 1, ngrid(2)
    If (ND1(ng,1) .ne. ND2(ng,1)) Then
      Do J = 1, ND1(ng,2)
        Do K = 1, ND1(ng,3)
          X2(ng,ND2(ng,1),J,K,:) = X2(ng,1,J,K,:)
          IBLANK2(ng,ND2(ng,1),J,K) = IBLANK2(ng,1,J,K)
        End Do
      End Do
    Else If (ND1(ng,2) .ne. ND2(ng,2)) Then
      Do I = 1, ND1(ng,1)
        Do K = 1, ND1(ng,3)
          X2(ng,I,ND2(ng,2),K,:) = X2(ng,I,1,K,:)
          IBLANK2(ng,I,ND2(ng,2),K) = IBLANK2(ng,I,1,K)
        End Do
      End Do    
    Else If (ND1(ng,3) .ne. ND2(ng,3)) Then
      Do I = 1, ND1(ng,1)
        Do J = 1, ND1(ng,2)
          X2(ng,I,J,ND2(ng,3),:) = X2(ng,I,J,1,:)
          IBLANK2(ng,I,J,ND2(ng,3)) = IBLANK2(ng,I,J,1)
        End Do
      End Do
    End If
  End Do

  Call Write_Grid(NDIM(2),ngrid(2),ND2,X2,IBLANK2, &
       prec(2),grid_format(2),volume_format(2),ib_format(2),grid_file(2))
  Write (*,'(A)') ''

  ! Step three, clean up & leave
  Deallocate(ND1, X1, IBLANK1, ND2, X2, IBLANK2)


  Stop
End Program Ogrid
