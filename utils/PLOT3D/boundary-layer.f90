! Copyright (c) 2015, University of Illinois at Urbana-Champaign, XPACC
! License: MIT, http://opensource.org/licenses/MIT
Program Boundary_Layer
  ! ============================================================= !
  !                                                               !
  ! BOUNDARY LAYER ON FLAT PLATE: FLOW INITIALIZATION ROUTINE     !
  ! Generates a grid and initial/boundary conditions for          !
  ! simulating a flat-plate boundary layer                        !
  !                                                               !
  ! ============================================================= !
  !                                                               !
  ! Written by Jesse Capecelatro (jcaps@illinois.edu)             !
  ! 12 August 2014                                                !
  !                                                               !
  ! ============================================================= !
  USE ModPLOT3D_IO
  USE ModParser
  Implicit None

  ! I/O variables
  Character(len=2)  :: prec(3), grid_format(3), volume_format(3), ib(3)
  Character(len=80) :: grid_file(3), sol_file(1)
  character(len=64) :: input_name

  !  Mesh variables
  Integer :: ngrid, ndim, I2, I3, J2
  Integer, Dimension(:,:), Pointer :: ND
  Integer, Dimension(:,:,:,:), Pointer :: IBLANK
  Real(KIND=8), Dimension(:,:,:,:,:), Pointer :: X, Q, Qaux
  Real(KIND=8) :: Lx, Ly, Lz
  
  Call Boundary_Layer_Init
  Call Boundary_Layer_Grid
  Call Boundary_Layer_BC
  Call Boundary_Layer_Data
  Call Finalize

Contains


  ! ============== !
  ! Initialization !
  ! ============== !
  Subroutine Boundary_Layer_Init
    Implicit None

    ! Parse the input file
    input_name="plascomcm.inp"
    call parser_init
    call parser_parsefile(trim(input_name))

    Return
  End Subroutine Boundary_Layer_Init


  ! ==================== !
  ! Create the grid/mesh !
  ! ==================== !
  Subroutine Boundary_Layer_Grid
    Implicit None

    Integer :: I, J, K, N, NGRIT, iTrip1, iTrip2
    Real(KIND=8) :: dx, dy, dz, ytilde, r, delta0, delta, alpha
    Real(KIND=8) :: trip_loc, trip_width, trip_height
    Real(KIND=8) :: grit_height, grit_width, peak_height, total_height
    REAL(KIND=8) :: rnd, GAUSS, AMP, SIG, x0, y0, z0, z12
    Logical :: use_trip, use_grit, use_stretching

    ! Initialize the grids
    ngrid = 1
    Call parser_read('ND', NDIM, 3)
    Allocate(ND(ngrid,3)); ND(:,3) = 1

    ! Set grid parameters
    prec(1) = 'd' ! Precision = double
    grid_format(1) = 'm' ! Grid format = single
    volume_format(1) = 'w' ! Volume format = whole

    ! Number of grid points
    call parser_read('GRID1_NX',ND(1,1))
    call parser_read('GRID1_NY',ND(1,2))
    call parser_read('GRID1_NZ',ND(1,3))
    print *, 'Grid: ', ND(1,1), 'x', ND(1,2), 'x', ND(1,3)

    ! Allocate the arrays and assign values
    ALLOCATE(X(ngrid,MAXVAL(ND(:,1)),MAXVAL(ND(:,2)),MAXVAL(ND(:,3)),3))
    ALLOCATE(IBLANK(ngrid,MAXVAL(ND(:,1)),MAXVAL(ND(:,2)),MAXVAL(ND(:,3))))

    ! Grid size
    call parser_read('GRID1_LX',Lx)
    call parser_read('GRID1_LY',Ly)
    If (ND(1,3) > 1) call parser_read('GRID1_LZ',Lz)
    dx = Lx / DBLE(ND(1,1))
    dy = Ly / DBLE(ND(1,2))
    dz = Lz / DBLE(ND(1,3))

    ! Grid Stretching
    Call Parser_Read('USE STRETCHING',Use_Stretching,.FALSE.)

    ! Stretching parameters
    If (Use_Stretching) r=2.0D0

    ! Generate the grid
    Do K = 1, ND(1,3)
       DO J = 1, ND(1,2)
          Do I = 1, ND(1,1)
             If (ND(1,1) .GT. 1) Then
                ! Create X
                X(1,I,J,K,1) = DBLE(I-1)/DBLE(ND(1,1)-1)*Lx
             Else
                X(1,I,J,K,1) = 0D0
             End If
             If (ND(1,2) .GT. 1) Then
                ! Create Y
                If (Use_Stretching) Then
                   ytilde = DBLE(ND(1,2)-J)/DBLE(ND(1,2)-1)
                   X(1,I,J,K,2) = Ly * (1.0D0-tanh(r*ytilde)/tanh(r))
                Else
                   X(1,I,J,K,2) = DBLE(J-1)/DBLE(ND(1,2)-1)*Ly
                End If
             Else
                X(1,I,J,K,2) = 0D0
             End If
             If (ND(1,3) .GT. 1) Then
                ! Create Z. Periodic direction, correct for length
                X(1,I,J,K,3) = DBLE(K-1)/DBLE(ND(1,3)-1)*(Lz-dz)
             Else
                X(1,I,J,K,3) = 0D0
             End If
          End Do
       End Do
    End Do

    ! Read trip strip parameters
    IBLANK = 1
    ib(1) = 'n'

    ! Generate the sandpaper
    Call parser_read('USE_TRIP', Use_trip, .False.)
    If (Use_trip) Then
       call parser_read('TRIP_LOCATION', trip_loc)
       call parser_read('TRIP_WIDTH', trip_width)
       call parser_read('TRIP_MEAN_PLANE_HEIGHT', trip_height)
       call parser_read('USE_GRIT', use_grit, .False.)
       If (use_grit) then
          call parser_read('GRIT_HEIGHT', grit_height)
          call parser_read('GRIT_WIDTH', grit_width)
          call parser_read('NUMBER_OF_PARTICLES', NGRIT)
       End If

       ! Find the sandpaper extents
       iTrip1 = 1; iTrip2 = ND(1,1)
       J = 1; K = 1
       do i = 1, ND(1,1)
          if (X(1,I,J,K,1) < trip_loc) iTrip1 = I + 1
       end do
       do I = ND(1,1), 1, -1
          if (X(1,I,J,K,1) > trip_loc + trip_width) iTrip2 = I - 1
       end do
       Write (*,'(A)') ''
       Write (*,'(A, i0.0, A, i0.0, A)') 'Sandpaper extents: [', iTrip1, ', ', iTrip2, ']'

       ! Deform the mesh to the sandpaper height
       do K = 1, ND(1,3)
          do I = iTrip1 - 20, iTrip2 + 20
             J = 1

             ! Create a smooth step
             sig = 2000.0_WP
             X(1,I,J,K,2) = 0.5_WP * trip_height * (tanh(sig * (X(1,I,J,K,1) - trip_loc)) - &
                  tanh(sig*(X(1,I,J,K,1) - trip_loc - trip_width)))

             ! Shift grid points above (smoothly)
             do J = 2, ND(1,2)
                ! Get unperturbed height variation
                y0 = X(1,1,J,K,2)
                delta = y0 - X(1,1,J-1,K,2)

                ! Adjust the current height
                ytilde = X(1,I,J-1,K,2) + delta
                alpha = tanh(0.025_WP * Real(J - 2, WP) / Real(ND(1,2) - 2, WP))
                X(1,I,J,K,2) = ytilde * (1.0_WP - alpha) + X(1,I,J,K,2) * alpha
             end do
          end do
       end do

       ! Embed the particles
       If (use_grit) Then

          ! Standard deviation
          sig = 1.0_WP * grit_width

          ! Loop through number of particles
          Write (*,'(A)') ''
          Write (*,'(A, i4.4, A)') 'Adding ', nGrit, ' particles...'
          Do N = 1, nGrit

             ! Compute amplitude
             amp = grit_height
             Call random_number(rnd)
             If (rnd < 0.5_WP) amp = -amp !... peak or valley

             ! Get a random location
             Call random_number(rnd)
             x0 = trip_loc + 5.0_WP * sig + (trip_width - 10.0_WP * sig) * rnd
             Call random_number(rnd)
             z0 = (Lz - dz) * rnd
             If (ND(1,3) == 1) z0 = 0.0_WP

             ! Modify the grid
             Do K = 1, ND(1,3)
                Do I = iTrip1, iTrip2
                   J = 1
                      
                   ! Represent sandpaper particles as Gaussian
                   Gauss = amp * Exp(-((X(1,I,J,K,1) - x0)**2 / (2.0_WP * sig**2) + &
                        (X(1,I,J,K,3) - z0)**2 / (2.0_wp * sig**2)))

                   ! Account for periodicity in z
                   z12 = (Lz-dz) - abs(X(1,I,J,K,3) - z0)
                   If (ND(1,3) > 1) Gauss = Gauss + amp * &
                        Exp(-((X(1,I,J,K,1) - x0)**2 / (2.0_wp * sig**2) + z12**2 / (2.0_wp * sig**2)))

                   ! Update the vertical coordinate
                   X(1,I,J,K,2) = X(1,I,J,K,2) + Gauss
                   
                   ! Shift grid points above (smoothly)
                   Do J = 2, ND(1,2)
                      ! Get unperturbed height variation
                      y0 = X(1,1,J,K,2)
                      delta = y0 - X(1,1,J-1,K,2)
                      
                      ! Adjust the current height
                      ytilde = X(1,I,J-1,K,2) + delta
                      alpha = tanh(4.0_wp * real((J - 2),WP) / real((ND(1,2) - 2),WP))
                      X(1,I,J,K,2) = ytilde * (1.0_WP - alpha) + X(1,I,J,K,2) * alpha
                   End Do

                End Do
             End Do

             ! Output progress
             If (Mod(Real(N,WP),Real(nGrit,WP)/10.0_WP) == 0) &
                  Write(*,'(f5.1, A)') real(N,WP)/real(nGrit,WP)*100.0_WP,'% complete'

          End Do
       End If !... If (use_grit)

       ! Output surface roughness dimentions
       total_height = 0.0_WP
       peak_height = 0.0_WP
       J = 1
       Do K = 1, ND(1,3)
          Do I = iTrip1, iTrip2
             total_height = max(total_height, X(1,I,J,K,2))
             peak_height = max(peak_height, X(1,I,J,K,2) - trip_height)
          End Do
       End Do
       Write (*,'(A)') ''
       print *, 'Max surface height:', real(total_height * 1000.0_WP, 4),'mm'
       print *, 'Peak height:', real(peak_height * 1000.0_WP, 4),'mm'
       Write (*,'(A)') ''

    End If !...  If (use_trip)

    ! Write the grid file
    grid_file(1)="grid.xyz"
    Write (*,'(A)') ''
    Call Write_Grid(3,ngrid,ND,X,IBLANK, &
         prec(1),grid_format(1),volume_format(1),ib(1),grid_file(1))
    Write (*,'(A)') ''

    Return

  End Subroutine Boundary_Layer_Grid


  ! =================== !
  ! Boundary conditions !
  ! =================== !
  Subroutine Boundary_Layer_BC
    Implicit None

    Integer :: nc,iunit
    Real(KIND=8) :: buf

    ! Get sponge zone thickness
    Call parser_read('NCELL_SPONGE',nc)

    ! Open the file
    iunit=11
    open(iunit,file="bc.dat")

    print *, 'Writing boundary conditions'
    Write (*,'(A)') ''

    ! Write the header
    write(iunit,'(1a1 )') "#"
    write(iunit,'(1a45)') "# Basic boundary condition file for PlasComCM"
    write(iunit,'(1a1 )') "#"
    write(iunit,'(1a24)') "# FD = finite difference"
    write(iunit,'(1a20)') "# FV = finite volume"
    write(iunit,'(1a1 )') "#"
    write(iunit,'(1a8 )') "# ibType"
    write(iunit,'(1a8 )') "# ======"
    write(iunit,'(1a32)') "#     00     OUTER_BOUNDARY (FD)"
    write(iunit,'(1a41)') "#     21     SAT_SLIP_ADIABATIC_WALL (FD)"
    write(iunit,'(1a44)') "#     22     SAT_NOSLIP_ISOTHERMAL_WALL (FD)"
    write(iunit,'(1a33)') "#     23     SAT_NOSLIP_ADIABATIC_WALL (FD)"
    write(iunit,'(1a31)') "#     24     SAT_FAR_FIELD (FD)"
    write(iunit,'(1a37)') "#     25     SAT_BLOCK_INTERFACE (FD)"
    write(iunit,'(1a51)') "#     31     NSCBC_INFLOW_VELOCITY_TEMPERATURE (FD)"
    write(iunit,'(1a63)') "#     32     NSCBC_INFLOW_VELOCITY_TEMPERATURE_FROM_SPONGE (FD)"
    write(iunit,'(1a57)') "#     41     NSCBC_OUTFLOW_PERFECT_NONREFLECTION (FD, FV)"
    write(iunit,'(1a54)') "#     42     NSCBC_OUTFLOW_PERFECT_REFLECTION (FD, FV)"
    write(iunit,'(1a47)') "#     51     NSCBC_WALL_ADIABATIC_SLIP (FD, FV)"
    write(iunit,'(1a50)') "#     52     NSCBC_WALL_ISOTHERMAL_NOSLIP (FD, FV)"
    write(iunit,'(1a39)') "#     61     NSCBC_SUBSONIC_INFLOW (FD)"
    write(iunit,'(1a45)') "#     62     NSCBC_SUPERSONIC_INFLOW (FD, FV)"
    write(iunit,'(1a29)') "#     73     PERIODICITY (FV)"
    write(iunit,'(1a30)') "#     91     FV_DIRICHLET (FV)"
    write(iunit,'(1a28)') "#     92     FV_NEUMANN (FV)"
    write(iunit,'(1a29)') "#     93     FV_SYMMETRY (FV)"
    write(iunit,'(1a36)') "#     94     FV_BLOCK_INTERFACE (FV)"
    write(iunit,'(1a45)') "#     95     FV_BLOCK_INTERFACE_PERIODIC (FV)"
    write(iunit,'(1a28)') "#     99     SPONGE (FD, FV)"
    write(iunit,'(1a1 )') "#"
    write(iunit,'(1a56)') "# Grid ibType ibDir   is    ie    js    je     ks    ke "
    write(iunit,'(1a56)') "# ==== ====== =====  ====  ====  ====  ====   ====  ===="

    ! Inflow / outflow
    write(iunit,'(9I6)')      1,   24,     1,    1,     1,    1,   -1,    1,    -1
    write(iunit,'(9I6)')      1,   24,    -1,   -1,    -1,    1,   -1,    1,    -1
    write(iunit,'(9I6)')      1,   24,    -2,    1,    -1,   -1,   -1,    1,    -1

    ! No-slip wall (isothermal or Robin condition)
    write(iunit,'(9I6)')      1,   22,     2,    1,    -1,    1,    1,    1,    -1

    ! Add spounge layers
    write(iunit,'(9I6)')      1,   99,     1,1         ,NC,    1,    -1,    1,    -1
    write(iunit,'(9I6)')      1,   99,    -1,ND(1,1)-NC,-1,    1,    -1,    1,    -1
    write(iunit,'(9I6)')      1,   99,    -2,    1,    -1,ND(1,2)-NC,-1,    1,    -1

    ! Close the file
    close(iunit)

    Return
  End Subroutine Boundary_Layer_BC


  ! ========================= !
  ! Initial (target) solution !
  ! ========================= !
  Subroutine Boundary_Layer_Data
    Implicit None

    integer :: I, J, K, L, EQ_STATE, kk, nspecies
    Real(KIND=8), Dimension(:,:,:), Pointer :: rho, rhoU, rhoV, rhoW, rhoE, Fuel, Air, Radical
    Real(KIND=8) :: U0, rho0, P0, mu0, L0, gamma, C, Re, Pr, TAU(4), delta99, x0, T, T0, YF0, YR0, Tw
    Real(WP), parameter :: YO = 1.0_WP/(1.0_WP+14.00674_WP/15.9994_WP*3.76_WP)
    Real(WP), Pointer :: Mwcoeff(:)
    Real(WP) :: MwRef
    Integer :: CHEM_MODEL, N2
    Integer, parameter :: H2=1
    Integer, parameter :: O2=2
    Integer, parameter :: R =3
    Character(len=80) :: MODEL
    Logical :: multicomponent

    ! Solution to Blasius boundary layer
    Real(KIND=8) :: blasius0,blasius1, delta, eta, xx, yy
    Real(KIND=8) :: f2l,f2r,f0l,f0r
    Real(KIND=8), dimension(0:9) :: by0 = (/ &
         0.0_8, 0.165571818583440_8, 0.650024518764203_8, 1.39680822972500_8, &
         2.30574664618049_8, 3.28327391871370_8, 4.27962110517696_8, &
         5.27923901129384_8, 6.27921363832835_8, 7.27921257797747_8 /)
    Real(KIND=8), dimension(0:9) :: by1 = (/ &
         0.0_8, 0.329780063306651_8, 0.629765721178679_8, 0.84604458266019_8, &
         0.95551827831671_8, 0.99154183259084_8, 0.99897290050990_8, &
         0.9999216098795_8, 0.99999627301467_8, 0.99999989265063_8 /)
    Real(KIND=8), dimension(0:9) :: by2 = (/ &
         0.332057384255589_8, 0.323007152241930_8, 0.266751564401387_8, 0.161360240845588_8, &
         0.06423404047594_8, 0.01590689966410_8, 0.00240199722109_8, &
         0.00022016340923_8, 0.00001224984692_8, 0.00000041090325_8 /)
    Character(len=64) :: flow

    ! Read from input
    call parser_read('CHEMISTRY_MODEL',MODEL,'')
    call parser_read('REYNOLDS_NUMBER',Re,0D0)
    call parser_read('PRANDTL_NUMBER',Pr,0.72D0)
    call parser_read('DENSITY_REFERENCE',rho0,1.0D0)
    call parser_read('PRESSURE_REFERENCE',P0)
    call parser_read('LENGTH_REFERENCE',L0)
    call parser_read('GAMMA_REFERENCE',gamma,1.4D0)
    call parser_read('SNDSPD_REFERENCE',C,343.6D0)
    call parser_read('GAS_EQUATION_OF_STATE',EQ_STATE,0)
    call parser_read('CROSSFLOW_TYPE',flow)
    call parser_read('TEMPERATURE_REFERENCE',T0,293.15_WP)
    call parser_read('CROSSFLOW_MACH_NUMBER',U0)
    call parser_read('INITIAL_WALL_TEMPERATURE',Tw,T0)
    call parser_read('NUMBER_OF_SCALARS',nspecies,0)

    ! Determine the chemistry model
    Select case (trim(MODEL))
       
    case ('', 'NONE')
       multicomponent = .false.

    case ('ONESTEP')
       multicomponent = .false.

    case ('MODEL0')
       multicomponent = .true.

    case ('MODEL1')
       multicomponent = .true.

    end select

    ! Allocate solution array
    Allocate(Q(ngrid,MAXVAL(ND(:,1)),MAXVAL(ND(:,2)),MAXVAL(ND(:,3)),5))

    ! Link the pointers
    rho   =>  Q(1,:,:,:,1)
    rhoU  =>  Q(1,:,:,:,2)
    rhoV  =>  Q(1,:,:,:,3)
    rhoW  =>  Q(1,:,:,:,4)
    rhoE  =>  Q(1,:,:,:,5)

    ! Initialize the timing
    TAU(1) = 0D0
    TAU(2) = Pr
    TAU(3) = Re
    TAU(4) = 0D0

    ! Initialize the variable array
    rho  = 1.0_WP
    rhoU = 0.0_WP
    rhoV = 0.0_WP
    rhoW = 0.0_WP

    If (nSpecies > 0) Then

       If (nSpecies > 3) Then
          Print *, 'WARNING: Boundary layer utility only implemented for <= 3 species...'
          Stop
       End If

       ! Read from the input
       call parser_read('INITIAL_FUEL_CONCENTRATION',YF0,0.0_WP)
       call parser_read('INITIAL_RADICAL_CONCENTRATION',YR0,0.0_WP)

       Allocate(Qaux(ngrid,MAXVAL(ND(:,1)),MAXVAL(ND(:,2)),MAXVAL(ND(:,3)),nspecies))
       Fuel    =>  Qaux(1,:,:,:,H2)
       Air     =>  Qaux(1,:,:,:,O2)
       If (nSpecies > 2) Radical =>  Qaux(1,:,:,:,R)

       ! Initialize the components
       Fuel = YF0
       Air = (1.0_WP - Fuel) * YO
       If (nSpecies > 2) Radical = YR0

       ! Nitrogen index
       N2 = nspecies + 1

       ! Molecular weights
       If (multicomponent) then
          Allocate(Mwcoeff(nspecies+1))
          Mwcoeff(H2) = 2.0_WP * 1.00794
          Mwcoeff(O2) = 2.0_WP * 15.9994_WP
          If (nSpecies > 2) Mwcoeff(R)  = 1.00794
          Mwcoeff(N2) = 2.0_WP * 14.00674

          ! Transform into the Mw^-1
          Mwcoeff = 1.0_WP / Mwcoeff

          ! Inverse of the reference Mw
          MwRef = YO*Mwcoeff(O2) + (1.0_WP-YO)*Mwcoeff(N2)

          ! Divide Mw by MwRef
          Mwcoeff = Mwcoeff/MwRef

          ! Subtract off the N2 contribution
          Mwcoeff(1:nSpecies) = Mwcoeff(1:nSpecies) - Mwcoeff(nSpecies+1)

       End If

    End If

    select case(trim(flow))

    case ('UNIFORM')

       Do K=1,ND(1,3)
          Do J=1,ND(1,2)
             Do I=1,ND(1,1)
                rhoU(i,j,k) = U0
             End Do
          End Do
       End Do

    case ('BLASIUS')

       call parser_read('BLASIUS_ORIGIN', x0)

       ! Compute viscosity
       mu0 = rho0 * C * L0 / Re

       ! Initialize the velocity field (Blasius solution for laminar flat plate)
       Do K=1,ND(1,3)
          Do J=2,ND(1,2)
             Do I=1,ND(1,1)
                ! Get the corrdinates (start slighty downstream)
                xx = X(1,I,J,K,1) + x0
                yy = X(1,I,J,K,2) - X(1,I,1,K,2)

                ! Return the first derivative of the Blasius function
                delta=sqrt(mu0*xx/(C*U0)/rho0)
                eta=yy/delta
                if (eta.le.0.0D0) then
                   blasius0 = 0.0D0
                   blasius1 = 0.0D0
                else if (eta.ge.9.0D0) then
                   blasius0 = by0(9) + (eta-9.0D0)
                   blasius1 = 1.0D0
                else
                   kk = int(eta)

                   f0l = by0(kk)
                   f0r = by0(kk+1)
                   f2l = by2(kk)
                   f2r = by2(kk+1)
                   blasius0 = &
                        1.0D0/6.0D0*f2l*(real(kk+1,8)-eta)**3 + &
                        1.0D0/6.0D0*f2r*(eta-real(kk,8))**3 + &
                        (f0l-1.0D0/6.0D0*f2l)*(real(kk+1,8)-eta) + &
                        (f0r-1.0D0/6.0D0*f2r)*(eta-real(kk,8))

                   f0l = by1(kk)
                   f0r = by1(kk+1)
                   f2l = -0.5D0*by0(kk)*by2(kk)
                   f2r = -0.5D0*by0(kk+1)*by2(kk+1)
                   blasius1 = &
                        1.0D0/6.0D0*f2l*(real(kk+1,8)-eta)**3 + &
                        1.0D0/6.0D0*f2r*(eta-real(kk,8))**3 + &
                        (f0l-1.0D0/6.0D0*f2l)*(real(kk+1,8)-eta) + &
                        (f0r-1.0D0/6.0D0*f2r)*(eta-real(kk,8))
                end if

                ! Update the velocity
                rhoU(I,J,K) = U0 * blasius1
                rhoV(I,J,K) = 0.5_WP * sqrt(mu0 * U0 / rho0 / xx) * (eta * blasius1 - blasius0)

             End Do
          End Do
       End Do

       ! Reinforce zero-slip at the wall
       rhoU(:,1,:) = 0.0_WP
       rhoV(:,1,:) = 0.0_WP
       rhoW(:,1,:) = 0.0_WP

    case default

       Write (*,'(3A)') "Unknown crossflow type `", trim(flow), "'"
       Stop

    end select

    ! Non-dimensionalize the temperatures
    Tw = Tw / (GAMMA - 1.0_WP) / T0
    T0 = 1.0_WP / (GAMMA - 1.0_WP)

    ! Non-dimensional reference pressure
    P0 = 1.0_WP / GAMMA

    ! Compute density from varying temperature profile assuming constant pressure
    Do K=1,ND(1,3)
       Do J=1,ND(1,2)
          Do I=1,ND(1,1)

             ! An arbitrary temperature profile (Golden Rule: when in doubt, use a Gaussian profile!)
             T = (Tw - T0) * exp(-X(1,I,J,K,2)**2) + T0

             ! Get the density
             rho(I,J,K) = GAMMA * P0 / (GAMMA - 1.0_WP) / T
             If (multicomponent) Then
                rho(I,J,K) = rho(I,J,K) / (sum(Qaux(1,I,J,K,:) * MWcoeff(1:nSpecies)) + MWcoeff(nSpecies+1))
             End If
          End Do
       End Do
    End Do

    ! Multiply through by density
    rhoU = rho * rhoU
    rhoV = rho * rhoV
    rhoW = rho * rhoW
    Do L=1,nSpecies
       Qaux(1,:,:,:,L) = rho * Qaux(1,:,:,:,L)
    End Do

    ! Compute energy assuming constant pressure
    rhoE = P0 / (GAMMA - 1.0_WP) + 0.5_WP*(rhoU**2+rhoV**2+rhoW**2) / rho

    ! Write the solution file
    sol_file(1)="RocFlo-CM.00000000.target.q"
    Call Write_Soln(3,ngrid,ND,Q,TAU,prec(1),grid_format(1),volume_format(1),sol_file(1))
    Write (*,'(A)') ''

    ! Write the auxilary file
    If (nspecies > 0) Then
       sol_file(1)="RocFlo-CM.00000000.target.aux.q"
       Call Write_Func(ngrid,ND,Qaux,sol_file(1))
       Write (*,'(A)') ''
    End If

    Return

  End Subroutine Boundary_Layer_Data


  ! ================== !
  ! Clean up and leave !
  ! ================== !
  Subroutine Finalize
    Implicit None
  
    Deallocate(ND,IBLANK,X,Q)

    Return
    
  End Subroutine Finalize

End Program Boundary_Layer
