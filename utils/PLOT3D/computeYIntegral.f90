! Copyright (c) 2015, University of Illinois at Urbana-Champaign, XPACC
! License: MIT, http://opensource.org/licenses/MIT
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!                                                               !
! Compute the y-integral of a field                             !
!                                                               !
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!                                                               !
! Written by Daniel J. Bodony (bodony@illinois.edu)             !
! 24 August 2011                                                !
!                                                               !
! $Header: /cvsroot/genx/Codes/RocfloCM/plot3d_utils/computeYIntegral.f90,v 1.2 2011/08/24 20:00:24 bodony Exp $
!                                                               !
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

Program computeYIntegral

  USE ModPLOT3D_IO
  Implicit None

  !  Arrays
  Real(KIND=8), Dimension(:,:,:,:,:), Pointer :: X1, Q1, time(:), Mean
  Real(KIND=8) :: XMIN, XMAX, YMIN, YMAX, ZMIN, ZMAX, TAU(4)
  Real(Kind=8), Dimension(:,:,:,:), Pointer ::  VAR

  !  Integers
  Integer :: ngrid, I, J, K, dir, NX, NY, NZ, NDIM, L
  Integer, Dimension(:,:), Pointer :: ND1, ND2
  Integer, Dimension(:,:,:,:), Pointer :: IBLANK1, IBLANK2
  Integer :: startIter, stopIter, skipIter, iter

  !  Logical
  Logical :: gf_exists(3), sf_exists(3)

  !  Characters
  Character(LEN=2)  :: prec, gf, vf, ib
  Character(LEN=80) :: infile, outfile, cmd, meanfile, gridfile

  !  Doubles
  Real(KIND=8) :: startTime, stopTime, interval, dt, gamma, value

  !  Step zero, read in the command line
  Integer :: iargc, nargc, ng, isperiodic, ftype, num, numFiles

  nargc = iargc()
  If (nargc /= 2) Then
    Write (*,'(A)') 'USAGE: computeYIntegral <gridFile> <inFile>'
    Stop
  Endif

  Nullify(X1,Q1)

  ! ... input file
  Call Getarg(1,gridfile);
  Call Getarg(2,infile);

  ! ... read the infile
  Call Read_Soln(NDIM,ngrid,ND1,Q1,TAU,prec,gf,vf,infile,0)

  ! ... read the grid
  Call Read_Grid(NDIM,ngrid,ND1,X1,IBLANK1,prec,gf,vf,ib,gridfile,0)


  ! ... allocate space
  allocate(VAR(ngrid,MAXVAL(ND1(:,1)),MAXVAL(ND1(:,3)),5))
  VAR = 0.0_8

  ! ... add current solution
  Do ng = 1, ngrid
    Do I = 1, ND1(ng,1)
      Do K = 1, ND1(ng,3)
        Do L = 1, 5
          Do J = 50, ND1(ng,2)-50
            var(ng,I,K,L) = var(ng,I,K,L) + 0.5_8 * (X1(ng,I,J+1,K,2) - X1(ng,I,J,K,2)) * (Q1(ng,I,J+1,K,L)+Q1(ng,I,J,K,L))
          End Do
        End Do
      End Do
    End Do
  End Do

  Do ng = 1, ngrid
    Do I = 1, ND1(ng,1)
      Write (*,*) X1(ng,I,1,1,1), Var(ng,I,1,5)
    End Do
  End Do

  Deallocate(X1,Q1,var,ND1)

  Stop 

End Program computeYIntegral
