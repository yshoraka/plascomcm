! Copyright (c) 2015, University of Illinois at Urbana-Champaign, XPACC
! License: MIT, http://opensource.org/licenses/MIT
Program readSolnTime
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!                                                               !
! Extract the time from a PLOT3D solution file                  !
!                                                               !
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!                                                               !
! Written by Daniel J. Bodony (bodony@illinois.edu)             !
! 11 July 2014                                                  !
!                                                               !
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  USE ModPLOT3D_IO
  Implicit None

  !  Arrays
  Real(KIND=8), Dimension(:,:,:,:,:), Pointer :: X1, Q1
  Real(KIND=8), Dimension(:,:,:,:,:), Pointer :: X2, Q2
  Real(KIND=8), Dimension(:,:,:,:,:), Pointer :: X3, Q3
  Real(KIND=8) :: tau(4), P, RHO, T, Ux, Uy, Uz, gamma

  !  Integers
  Integer :: ngrid, Nx, Ny, Nz, ng, NDIM, grid_num, ii, jj, kk, var_index
  Integer, Dimension(:,:), Pointer :: ND1

  !  Logical
  Logical :: gf_exists(3), sf_exists(3)

  !  Characters
  Character(LEN=2)  :: prec, gf, vf, ibf
  Character(LEN=80) :: file, cmd, ftype

  !  Step zero, read in the command line
  Integer :: iargc, nargc
  Integer, Parameter :: TRUE = 1, FALSE = 0

  nargc = iargc()
  If (nargc /= 1) Then
    Write (*,'(A)') 'USAGE: readSolnTime {soln_file}'
    Stop
  Else
    Call Getarg(1,file); ! print *, file
  End If

  ! ... read based on filetype
  Call Read_Soln_Header(3, 1, ND1, file, tau) 
  write (*,'(1(E20.12,1X))') tau(4)

End Program readSolnTime
