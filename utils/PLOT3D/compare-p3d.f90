! Copyright (c) 2015, University of Illinois at Urbana-Champaign, XPACC
! License: MIT, http://opensource.org/licenses/MIT
!===================================================================================================
!
! ComparePLOT3D: PlasComCM PLOT3D file comparison utility
!
! Usage: ./compare-p3d <Options> <File1> <File2>
!
! Options:
!   --file-type (-f)    Type of PLOT3D file ('grid', 'solution', or 'func')
!   --compare-mode (-m) Method for comparison of floating point data sets ('inf' or a number >= 1,
!                       corresponding to the type of norm to be used; default: 'inf')
!   --tolerance (-t)    Maximum error value allowed in floating point data comparisons
!
! Notes:
!   * Returns 0 and prints nothing if no differences are found; returns 1 and prints differences if
!     any are found; returns 2 on error
!   * Uses AlmostEqual for real-valued dataset comparisons (see ModCompare for details)
!
! Limitations:
!   * Currently ignores Tau
!
!===================================================================================================

program ComparePLOT3D

  use ModCompare
  use ModParseArguments
  use ModStdIO
  use ModPLOT3D_IO
  implicit none

  integer, parameter :: rk = selected_real_kind(15, 307)
  integer, parameter :: ik = selected_int_kind(9)

  integer(ik), parameter :: MAX_ND = 3

  integer(ik), parameter :: PATH_LENGTH = 256
  integer(ik), parameter :: VAR_NAME_LENGTH = 32

  integer(ik), parameter :: FILE_TYPE_GRID = 1
  integer(ik), parameter :: FILE_TYPE_SOLUTION = 2
  integer(ik), parameter :: FILE_TYPE_FUNC = 3

  integer(ik) :: i
  character(len=CMD_ARG_LENGTH), dimension(:), allocatable :: RawArguments
  type(t_cmd_opt), dimension(3) :: Options
  character(len=CMD_ARG_LENGTH), dimension(:), allocatable :: Arguments
  integer(ik) :: FileType
  integer(ik) :: CompareNorm
  integer(ik) :: P
  integer(ik) :: Error
  character(len=PATH_LENGTH) :: File1, File2
  real(rk) :: Tolerance
  logical :: UseTolerance
  logical :: Exists

  allocate(RawArguments(command_argument_count()))
  do i = 1, size(RawArguments)
    call get_command_argument(i, RawArguments(i))
  end do

  Options(1) = t_cmd_opt_("file-type", "f", OPT_VALUE_TYPE_STRING)
  Options(2) = t_cmd_opt_("compare-mode", "m", OPT_VALUE_TYPE_STRING)
  Options(3) = t_cmd_opt_("tolerance", "t", OPT_VALUE_TYPE_REAL)

  call ParseArguments(RawArguments, Options=Options, Arguments=Arguments, MinArguments=2, &
    MaxArguments=2)

  if (Options(1)%is_present) then
    select case (Options(1)%value)
    case ("grid")
      FileType = FILE_TYPE_GRID
    case ("solution")
      FileType = FILE_TYPE_SOLUTION
    case ("func")
      FileType = FILE_TYPE_FUNC
    case default
      write (ERROR_UNIT, '(3a)') "ERROR: Unrecognized file type '", &
        trim(Options(1)%value), "'."
      stop 2
    end select
  else
    FileType = FILE_TYPE_SOLUTION
  end if

  if (Options(2)%is_present) then
    select case (Options(2)%value)
    case ("inf")
      CompareNorm = COMPARE_NORM_INF
    case default
      CompareNorm = COMPARE_NORM_P
      read (Options(2)%value, *, iostat=Error) P
      if (Error /= 0) then
        write (ERROR_UNIT, '(3a)') "ERROR: Unrecognized comparison mode '", &
          trim(Options(2)%value), "'."
        stop 2
      end if
    end select
  else
    CompareNorm = COMPARE_NORM_INF
  end if

  if (Options(3)%is_present) then
    read (Options(3)%value, *, iostat=Error) Tolerance
    UseTolerance = .true.
  else
    UseTolerance = .false.
  end if

  File1 = Arguments(1)
  inquire(file=File1, exist=Exists)
  if (.not. Exists) then
    write (ERROR_UNIT, '(3a)') "ERROR: Could not open file '", trim(File1), "'."
    stop 2
  end if

  File2 = Arguments(2)
  inquire(file=File2, exist=Exists)
  if (.not. Exists) then
    write (ERROR_UNIT, '(3a)') "ERROR: Could not open file '", trim(File2), "'."
    stop 2
  end if

  select case (FileType)
  case (FILE_TYPE_GRID)
    if (UseTolerance) then
      call ComparePLOT3DGrid(File1, File2, CompareNorm, P, Tolerance=Tolerance)
    else
      call ComparePLOT3DGrid(File1, File2, CompareNorm, P)
    end if
  case (FILE_TYPE_SOLUTION)
    if (UseTolerance) then
      call ComparePLOT3DSolution(File1, File2, CompareNorm, P, Tolerance=Tolerance)
    else
      call ComparePLOT3DSolution(File1, File2, CompareNorm, P)
    end if
  case (FILE_TYPE_FUNC)
    if (UseTolerance) then
      call ComparePLOT3DFunc(File1, File2, CompareNorm, P, Tolerance=Tolerance)
    else
      call ComparePLOT3DFunc(File1, File2, CompareNorm, P)
    end if
  end select

contains

  subroutine ComparePLOT3DGrid(File1, File2, CompareNorm, P, Tolerance)

    character(len=*), intent(in) :: File1, File2
    integer(ik), intent(in) :: CompareNorm
    integer(ik), intent(in) :: P
    real(rk), intent(in), optional :: Tolerance

    integer(ik) :: i, j
    integer(ik) :: nDims1, nDims2
    integer(ik) :: nGrids1, nGrids2
    integer(ik), dimension(:,:), pointer :: nPoints1, nPoints2
    character(len=2) :: Precision1, Precision2
    character(len=2) :: GridFormat1, GridFormat2
    character(len=2) :: VolumeFormat1, VolumeFormat2
    character(len=2) :: UseIBlank1, UseIBlank2
    real(rk), dimension(:,:,:,:,:), pointer :: X1, X2
    integer, dimension(:,:,:,:), pointer :: IBlank1, IBlank2
    character(len=VAR_NAME_LENGTH), dimension(MAX_ND) :: XNames
    real(rk), dimension(:,:,:), allocatable :: Values1, Values2
    integer(ik), dimension(:,:,:), allocatable :: IBlankValues1, IBlankValues2
    real(rk) :: Difference

    nullify(nPoints1)
    nullify(nPoints2)
    nullify(X1)
    nullify(X2)
    nullify(IBlank1)
    nullify(IBlank2)

    call Read_Grid(nDims1, nGrids1, nPoints1, X1, IBlank1, Precision1, GridFormat1, VolumeFormat1, &
      UseIBlank1, File1, 0)
    call Read_Grid(nDims2, nGrids2, nPoints2, X2, IBlank2, Precision2, GridFormat2, VolumeFormat2, &
      UseIBlank2, File2, 0)

    if (nDims1 /= nDims2) then
      write (*, '(a)') "Grid files have different dimension."
      write (*, '(a,i0)') "File 1: ", nDims1
      write (*, '(a,i0)') "File 2: ", nDims2
      stop 1
    end if

    if (nGrids1 /= nGrids2) then
      write (*, '(a)') "Grid files have different numbers of grids."
      write (*, '(a,i0)') "File 1: ", nGrids1
      write (*, '(a,i0)') "File 2: ", nGrids2
      stop 1
    end if

    do i = 1, nGrids1
      if (any(nPoints1(i,:) /= nPoints2(i,:))) then
        write (*, '(a)') "Grid files have different numbers of points."
        write (*, '(a,i0,a,i0,a,i0)') "File 1: ", nPoints1(i,1), ", ", nPoints1(i,2), ", ", &
          nPoints1(i,3)
        write (*, '(a,i0,a,i0,a,i0)') "File 2: ", nPoints2(i,1), ", ", nPoints2(i,2), ", ", &
          nPoints2(i,3)
        stop 1
      end if
    end do

    if (Precision1 /= Precision2) then
      write (*, '(a)') "Grid files have different precision."
      write (*, '(2a)') "File 1: ", Precision1
      write (*, '(2a)') "File 2: ", Precision2
      stop 1
    end if

    if (GridFormat1 /= GridFormat2) then
      write (*, '(a)') "Grid files have different grid format."
      write (*, '(2a)') "File 1: ", GridFormat1
      write (*, '(2a)') "File 2: ", GridFormat2
      stop 1
    end if

    if (VolumeFormat1 /= VolumeFormat2) then
      write (*, '(a)') "Grid files have different volume format."
      write (*, '(2a)') "File 1: ", VolumeFormat1
      write (*, '(2a)') "File 2: ", VolumeFormat2
      stop 1
    end if

    if (UseIBlank1 /= UseIBlank2) then
      write (*, '(a)') "Grid files have different IBlank format."
      write (*, '(2a)') "File 1: ", UseIBlank1
      write (*, '(2a)') "File 2: ", UseIBlank2
      stop 1
    end if

    XNames = ["x coordinates", "y coordinates", "z coordinates"]

    do i = 1, nGrids1
      allocate(Values1(nPoints1(i,1),nPoints1(i,2),nPoints1(i,3)))
      allocate(Values2(nPoints2(i,1),nPoints2(i,2),nPoints2(i,3)))
      do j = 1, nDims1
        Values1 = X1(i,:nPoints1(i,1),:nPoints1(i,2),:nPoints1(i,3),j)
        Values2 = X2(i,:nPoints2(i,1),:nPoints2(i,2),:nPoints2(i,3),j)
        if (.not. AlmostEqual(Values1, Values2, CompareNorm=CompareNorm, P=P, &
          Tolerance=Tolerance, Difference=Difference)) then
          write (*, '(3a,i0,a)') "Grid files have different ", trim(XNames(j)), " on grid ", i, "."
          write (*, '(a,e17.10)') "Difference: ", Difference
          stop 1
        end if
      end do
      deallocate(Values1)
      deallocate(Values2)
    end do

    if (UseIBlank1 == "y") then
      do i = 1, nGrids1
        allocate(IBlankValues1(nPoints1(i,1),nPoints1(i,2),nPoints1(i,3)))
        allocate(IBlankValues2(nPoints2(i,1),nPoints2(i,2),nPoints2(i,3)))
        IBlankValues1 = IBlank1(i,:nPoints1(i,1),:nPoints1(i,2),:nPoints1(i,3))
        IBlankValues2 = IBlank2(i,:nPoints2(i,1),:nPoints2(i,2),:nPoints2(i,3))
        if (any(IBlankValues1 /= IBlankValues2)) then
          write (*, '(a,i0,a)') "Grid files have different IBlank values on grid ", i, "."
          stop 1
        end if
        deallocate(IBlankValues1)
        deallocate(IBlankValues2)
      end do
    end if 

    deallocate(nPoints1, nPoints2)
    deallocate(X1, X2)
    if (associated(IBlank1)) deallocate(IBlank1)
    if (associated(IBlank2)) deallocate(IBlank2)

  end subroutine ComparePLOT3DGrid

  subroutine ComparePLOT3DSolution(File1, File2, CompareNorm, P, Tolerance)

    character(len=*), intent(in) :: File1, File2
    integer(ik), intent(in) :: CompareNorm
    integer(ik), intent(in) :: P
    real(rk), intent(in), optional :: Tolerance

    integer(ik) :: i, j
    integer(ik) :: nDims1, nDims2
    integer(ik) :: nGrids1, nGrids2
    integer(ik), dimension(:,:), pointer :: nPoints1, nPoints2
    character(len=2) :: Precision1, Precision2
    character(len=2) :: GridFormat1, GridFormat2
    character(len=2) :: VolumeFormat1, VolumeFormat2
    real(rk), dimension(4) :: Tau1, Tau2
    real(rk), dimension(:,:,:,:,:), pointer :: Q1, Q2
    character(len=VAR_NAME_LENGTH), dimension(MAX_ND+2) :: QNames
    real(rk), dimension(:,:,:), allocatable :: Values1, Values2
    real(rk) :: Difference

    nullify(nPoints1)
    nullify(nPoints2)
    nullify(Q1)
    nullify(Q2)

    call Read_Soln(nDims1, nGrids1, nPoints1, Q1, Tau1, Precision1, GridFormat1, VolumeFormat1, &
      File1, 0)
    call Read_Soln(nDims2, nGrids2, nPoints2, Q2, Tau2, Precision2, GridFormat2, VolumeFormat2, &
      File2, 0)

    if (nDims1 /= nDims2) then
      write (*, '(a)') "Solution files have different dimension."
      write (*, '(a,i0)') "File 1: ", nDims1
      write (*, '(a,i0)') "File 2: ", nDims2
      stop 1
    end if

    if (nGrids1 /= nGrids2) then
      write (*, '(a)') "Solution files have different numbers of grids."
      write (*, '(a,i0)') "File 1: ", nGrids1
      write (*, '(a,i0)') "File 2: ", nGrids2
      stop 1
    end if

    do i = 1, nGrids1
      if (any(nPoints1(i,:) /= nPoints2(i,:))) then
        write (*, '(a)') "Solution files have different numbers of points."
        write (*, '(a,i0,a,i0,a,i0)') "File 1: ", nPoints1(i,1), ", ", nPoints1(i,2), ", ", &
          nPoints1(i,3)
        write (*, '(a,i0,a,i0,a,i0)') "File 2: ", nPoints2(i,1), ", ", nPoints2(i,2), ", ", &
          nPoints2(i,3)
        stop 1
      end if
    end do

    if (Precision1 /= Precision2) then
      write (*, '(a)') "Solution files have different precision."
      write (*, '(2a)') "File 1: ", Precision1
      write (*, '(2a)') "File 2: ", Precision2
      stop 1
    end if

    if (GridFormat1 /= GridFormat2) then
      write (*, '(a)') "Solution files have different grid format."
      write (*, '(2a)') "File 1: ", GridFormat1
      write (*, '(2a)') "File 2: ", GridFormat2
      stop 1
    end if

    if (VolumeFormat1 /= VolumeFormat2) then
      write (*, '(a)') "Solution files have different volume format."
      write (*, '(2a)') "File 1: ", VolumeFormat1
      write (*, '(2a)') "File 2: ", VolumeFormat2
      stop 1
    end if

    if (nDims1 == 3) then
      QNames(1) = "densities"
      QNames(2:4) = ["x momenta", "y momenta", "z momenta"]
      QNames(5) = "energies"
    else
      QNames(1) = "densities"
      QNames(2:3) = ["x momenta", "y momenta"]
      QNames(4) = "energies"
      QNames(5) = ""
    end if

    do i = 1, nGrids1
      allocate(Values1(nPoints1(i,1),nPoints1(i,2),nPoints1(i,3)))
      allocate(Values2(nPoints2(i,1),nPoints2(i,2),nPoints2(i,3)))
      do j = 1, nDims1+2
        Values1 = Q1(i,:nPoints1(i,1),:nPoints1(i,2),:nPoints1(i,3),j)
        Values2 = Q2(i,:nPoints2(i,1),:nPoints2(i,2),:nPoints2(i,3),j)
        if (.not. AlmostEqual(Values1, Values2, CompareNorm=CompareNorm, P=P, &
          Tolerance=Tolerance, Difference=Difference)) then
          write (*, '(3a,i0,a)') "Solution files have different ", trim(QNames(j)), " on grid ", i, "."
          write (*, '(a,e17.10)') "Difference: ", Difference
          stop 1
        end if
      end do
      deallocate(Values1)
      deallocate(Values2)
    end do

    deallocate(nPoints1, nPoints2)
    deallocate(Q1, Q2)

  end subroutine ComparePLOT3DSolution

  subroutine ComparePLOT3DFunc(File1, File2, CompareNorm, P, Tolerance)

    character(len=*), intent(in) :: File1, File2
    integer(ik), intent(in) :: CompareNorm
    integer(ik), intent(in) :: P
    real(rk), intent(in), optional :: Tolerance

    integer(ik) :: i, j
    integer(ik) :: nDims1, nDims2
    integer(ik) :: nGrids1, nGrids2
    integer(ik), dimension(:,:), pointer :: nPoints1, nPoints2
    integer(ik) :: nVars1, nVars2
    real(rk), dimension(:,:,:,:,:), pointer :: F1, F2
    real(rk), dimension(:,:,:), allocatable :: Values1, Values2
    real(rk) :: Difference

    nullify(nPoints1)
    nullify(nPoints2)
    nullify(F1)
    nullify(F2)

    call Read_Func(3, nGrids1, nPoints1, F1, File1)
    call Read_Func(3, nGrids2, nPoints2, F2, File2)

    nVars1 = size(F1,5)
    nVars2 = size(F2,5)

    if (nGrids1 /= nGrids2) then
      write (*, '(a)') "Function files have different numbers of grids."
      write (*, '(a,i0)') "File 1: ", nGrids1
      write (*, '(a,i0)') "File 2: ", nGrids2
      stop 1
    end if

    if (nVars1 /= nVars2) then
      write (*, '(a)') "Function files have different numbers of variables."
      write (*, '(a,i0)') "File 1: ", nVars1
      write (*, '(a,i0)') "File 2: ", nVars2
      stop 1
    end if

    do i = 1, nGrids1
      if (any(nPoints1(i,:) /= nPoints2(i,:))) then
        write (*, '(a)') "Function files have different numbers of points."
        write (*, '(a,i0,a,i0,a,i0)') "File 1: ", nPoints1(i,1), ", ", nPoints1(i,2), ", ", &
          nPoints1(i,3)
        write (*, '(a,i0,a,i0,a,i0)') "File 2: ", nPoints2(i,1), ", ", nPoints2(i,2), ", ", &
          nPoints2(i,3)
        stop 1
      end if
    end do

    do i = 1, nGrids1
      allocate(Values1(nPoints1(i,1),nPoints1(i,2),nPoints1(i,3)))
      allocate(Values2(nPoints2(i,1),nPoints2(i,2),nPoints2(i,3)))
      do j = 1, nVars1
        Values1 = F1(i,:nPoints1(i,1),:nPoints1(i,2),:nPoints1(i,3),j)
        Values2 = F2(i,:nPoints2(i,1),:nPoints2(i,2),:nPoints2(i,3),j)
        if (.not. AlmostEqual(Values1, Values2, CompareNorm=CompareNorm, P=P, &
          Tolerance=Tolerance, Difference=Difference)) then
          write (*, '(2(a,i0),a)') "Function files have different variable ", &
            j, " on grid ", i, "."
          write (*, '(a,e17.10)') "Difference: ", Difference
          stop 1
        end if
      end do
      deallocate(Values1)
      deallocate(Values2)
    end do

    deallocate(nPoints1, nPoints2)
    deallocate(F1, F2)

  end subroutine ComparePLOT3DFunc

end program ComparePLOT3D
