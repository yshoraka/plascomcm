! Copyright (c) 2015, University of Illinois at Urbana-Champaign, XPACC
! License: MIT, http://opensource.org/licenses/MIT
Program mergeGrids
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!                                                               !
! merge two PLOT3D grids into one grid                          !
!                                                               !
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!                                                               !
! Written by Daniel J. Bodony (bodony@stanford.edu)             !
! Tuesday, June 12, 2007 15:50:57 -0500                         !
!                                                               !
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  USE ModPLOT3D_IO
  Implicit None

  !  Arrays
  Real(KIND=8), Dimension(:,:,:,:,:), Pointer :: X1, Q1
  Real(KIND=8), Dimension(:,:,:,:,:), Pointer :: X2, Q2
  Real(KIND=8), Dimension(:,:,:,:,:), Pointer :: X3, Q3

  !  Integers
  Integer :: ngrid(3), Nx, Ny, Nz, ng, NDIM(3), append_dir, exclude_joint
  Integer, Dimension(:,:), Pointer :: ND1, ND2, ND3
  Integer, Dimension(:,:,:,:), Pointer :: IBLANK1, IBLANK2, IBLANK3
  Integer :: is1(3), ie1(3), is2(3), ie2(3), is3(3), ie3(3), io_stat
  Integer :: i, j, k

  !  Logical
  Logical :: gf_exists(3), sf_exists(3)

  !  Characters
  Character(LEN=2)  :: prec(3), grid_format(3), volume_format(3), ib_format(3)
  Character(LEN=80) :: grid_file(3), cmd

  !  Step zero, read in the command line
  Integer :: iargc, nargc
  Integer, Parameter :: TRUE = 1, FALSE = 0


  nargc = iargc()
  If (nargc /= 6) Then
    Write (*,'(A)') 'USAGE: mergeGrids {grid file 1}  {grid file 2} {s|d} {w|p} {y|n} {merged grid file}'
    Stop
  Else
    Call Getarg(1,grid_file(1)); !print *, grid_file(1)
    Call Getarg(2,grid_file(2)); !print *, grid_file(2:2)
    Call Getarg(3,prec(3))
    Call Getarg(4,volume_format(3))
    Call Getarg(5,ib_format(3))
    Call Getarg(6,grid_file(3)); !print *, soln_file(1:1)

    grid_format(3) = 'm'
    If ((volume_format(3) .NE. 'w') .AND. (volume_format(3) .NE. 'p')) Then
      Write (*,'(A)') 'Invalid value for volume format: use either "w" (whole) or "p" (planes)'
      Stop
    Else If ((prec(3) .NE. 's') .AND. (prec(3) .NE. 'd')) Then
      Write (*,'(A)') 'Invalid value for precision: use either "s" (single) or "d" (double)'
      Stop
    Else If ((ib_format(3) .NE. 'y') .AND. (ib_format(3) .NE. 'n')) Then
      Write (*,'(A)') 'Invalid value for IBLANK: use either "y" (yes) or "d" (no)'
      Stop
    End If

  End If


  !  Step one, read in the mesh1 grid file
  Write (*,'(A)') ''
  Call Read_Grid(NDIM(1),ngrid(1),ND1,X1,IBLANK1, &
       prec(1),grid_format(1),volume_format(1),ib_format(1),grid_file(1),TRUE)
  Write (*,'(A)') ''

  !  Step two, read in the mesh2 grid file
  Call Read_Grid(NDIM(2),ngrid(2),ND2,X2,IBLANK2, &
       prec(2),grid_format(2),volume_format(2),ib_format(2),grid_file(2),TRUE)
  Write (*,'(A)') ''

  If (NDIM(1) .ne. NDIM(2)) Then
    Stop 'Files are not of the same spatial dimension.'
  End If
  NDIM(3) = NDIM(1)

  If (ngrid(1) .ne. ngrid(2)) Then
    Stop 'Can not append more than two grids.'
  End If
  ngrid(3) = 1

  ! Step three, along which dimension should we appended?
  Write (*,'(A)',advance='no') 'Input append direction: '
  Read (*,*) append_dir
  If (append_dir .gt. NDIM(3)) Then
    Stop 'Cannot append in direction that is greater than spatial dimension.'
  End If

  ! Step four, should we exclude the joint?
  Write (*,'(A)',advance='no') 'Should we EXCLUDE the joint? '
  Read (*,*) exclude_joint

  ! Step five, determine new grid size
  Allocate(ND3(ngrid(3),3))
  ND3(:,:) = ND1(:,:)
  ND3(1,append_dir) = ND3(1,append_dir) + ND2(1,append_dir)
  If (exclude_joint == 1) ND3(1,append_dir) = ND3(1,append_dir) - 1

  ! Step six, allocate storage for merged file
  NX = MAXVAL(ND3(:,1));
  NY = MAXVAL(ND3(:,2));
  NZ = MAXVAL(ND3(:,3));
  Allocate(X3(ngrid(3),NX,NY,NZ,3),stat=io_stat)
  if (io_stat /= 0) stop 'Unable to allocate X3.'
  Allocate(IBLANK3(ngrid(3),NX,NY,NZ),stat=io_stat)
  if (io_stat /= 0) stop 'Unable to allocate IBLANK3.'

  ! Step four, copy grids 1 & 2 into 3
  is1(:) = 1; ie1(:) = ND1(1,:)
  is3(:) = 1; ie3(:) = ND1(1,:)
  if (exclude_joint == 1) Then
    ie1(append_dir) = ie1(append_dir) - 1
    ie3(append_dir) = ie3(append_dir) - 1
  end if
  Do ng = 1, ngrid(3)
    Do k = 0, ie3(3)-is3(3)
      Do j = 0, ie3(2)-is3(2)
        Do i = 0, ie3(1)-is3(1)
          X3(ng,i+is3(1),j+is3(2),k+is3(3),:) = &
          X1(ng,i+is1(1),j+is3(2),k+is3(3),:)
          IBLANK3(ng,i+is3(1),j+is3(2),k+is3(3)) = &
          IBLANK1(ng,i+is1(1),j+is3(2),k+is3(3))
        End Do
      End Do
    End Do
  End Do

  is2(:) = 1; ie2(:) = ND2(1,:)
  is3(:) = 1; ie3(:) = ND3(1,:)
  is3(append_dir) = is3(append_dir) + ie1(append_dir)

  Do ng = 1, ngrid(3)
    Do k = 0, ie3(3)-is3(3)
      Do j = 0, ie3(2)-is3(2)
        Do i = 0, ie3(1)-is3(1)
          X3(ng,i+is3(1),j+is3(2),k+is3(3),:) = &
          X2(ng,i+is2(1),j+is2(2),k+is2(3),:)
          IBLANK3(ng,i+is3(1),j+is3(2),k+is3(3)) = &
          IBLANK2(ng,i+is2(1),j+is2(2),k+is2(3))
        End Do
      End Do
    End Do
  End Do

  !  Step five, write the file
  Call Write_Grid(NDIM(3),ngrid(3),ND3,X3,IBLANK3, &
       prec(3),grid_format(3),volume_format(3),ib_format(3),grid_file(3))
  Write (*,'(A)') ''

  ! Step six, clean up & leave
  Deallocate(ND1, X1, IBLANK1)
  Deallocate(ND2, X2, IBLANK2)
  Deallocate(ND3, X3, IBLANK3)

End Program mergeGrids
