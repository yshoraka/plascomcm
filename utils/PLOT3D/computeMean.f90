! Copyright (c) 2015, University of Illinois at Urbana-Champaign, XPACC
! License: MIT, http://opensource.org/licenses/MIT
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!                                                               !
! Compute the time average of velocity fields.                  !
!                                                               !
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!                                                               !
! Written by Daniel J. Bodony (bodony@illinois.edu)             !
! 30 January 2011                                               !
!                                                               !
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

Program computeMean

  USE ModPLOT3D_IO
  Implicit None

  !  Arrays
  Real(KIND=8), Dimension(:,:,:,:,:), Pointer :: X1, X2, time(:)
  Real(KIND=8) :: XMIN, XMAX, YMIN, YMAX, ZMIN, ZMAX, TAU(4)

  !  Integers
  Integer :: ngrid, I, J, K, dir, NX, NY, NZ, NDIM
  Integer, Dimension(:,:), Pointer :: ND1, ND2
  Integer, Dimension(:,:,:,:), Pointer :: IBLANK1, IBLANK2
  Integer :: startIter, stopIter, skipIter, iter

  !  Logical
  Logical :: gf_exists(3), sf_exists(3)

  !  Characters
  Character(LEN=2)  :: prec, gf, vf, ib
  Character(LEN=80) :: infile, outfile, cmd

  !  Doubles
  Real(KIND=8) :: startTime, stopTime, interval, dt

  !  Step zero, read in the command line
  Integer :: iargc, nargc, ng, isperiodic, ftype, num, numFiles

  nargc = iargc()
  If (nargc /= 4) Then
    Write (*,'(A)') 'USAGE: computeMean <startIter> <stopIter> <skipIter> <outFile>'
    Stop
  Endif

  Nullify(X1,X2)

  ! ... starting, stopping, skip iteration
  Call Getarg(1,cmd); read (cmd,*) startIter
  Call Getarg(2,cmd); read (cmd,*) stopIter
  Call Getarg(3,cmd); read (cmd,*) skipIter

  ! ... output file
  Call Getarg(4,outfile);

  ! ... initialize
  num  = 1

  Do iter = startIter, stopIter, skipIter
    Write(infile,'(A,I8.8,A)') 'RocFlo-CM.', iter, '.q'

    Call Read_Soln(NDIM,ngrid,ND1,X1,TAU,prec,gf,vf,infile,0)
    Write (*,'(A)') infile

    ! ... allocate average if not already
    if (associated(X2) .eqv. .false.) then
      Allocate(X2(ngrid,MAXVAL(ND1(:,1)),MAXVAL(ND1(:,2)),MAXVAL(ND1(:,3)),NDIM+2))
      X2 = 0.0_8
    end if

    ! ... add current solution
    Do ng = 1, ngrid
      Do I = 1, ND1(ng,1)
        Do J = 1, ND1(ng,2)
          Do K = 1, ND1(ng,3)
            X2(ng,I,J,K,1) = ( X2(ng,I,J,K,1) * dble(num-1) + X1(ng,I,J,K,1) ) / dble(num)
            X2(ng,I,J,K,2) = ( X2(ng,I,J,K,2) * dble(num-1) + X1(ng,I,J,K,2) / X1(ng,I,J,K,1)) / dble(num)
            X2(ng,I,J,K,3) = ( X2(ng,I,J,K,3) * dble(num-1) + X1(ng,I,J,K,3) / X1(ng,I,J,K,1)) / dble(num)
            X2(ng,I,J,K,4) = ( X2(ng,I,J,K,4) * dble(num-1) + X1(ng,I,J,K,4) / X1(ng,I,J,K,1) ) / dble(num)
            X2(ng,I,J,K,5) = ( X2(ng,I,J,K,5) * dble(num-1) + X1(ng,I,J,K,5) ) / dble(num)
          End Do
        End Do
      End Do
    End Do

    ! ... counter
    num = num + 1

  End Do

  ! ... write solution
  if (associated(X2) .eqv. .true.) then
    Call Write_Soln(NDIM,ngrid,ND1,X2,tau,prec,gf,vf,outfile)
    Deallocate(ND1, X1, X2)
  end if

  Stop 'computeMean completed.'

End Program computeMean
