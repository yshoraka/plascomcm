! Copyright (c) 2015, University of Illinois at Urbana-Champaign, XPACC
! License: MIT, http://opensource.org/licenses/MIT
Program refineMesh
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  !                                                               !
  ! Extrude a PLOT3D mesh in one direction                        !
  !                                                               !
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  !                                                               !
  ! Written by Daniel J. Bodony (bodony@stanford.edu)             !
  ! 26 April 2006                                                 !
  ! 24 January 2007
  ! Monday, June 25, 2007 15:43:20 -0500
  !                                                               !
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  USE ModPLOT3D_IO
  Implicit None

  !  Arrays
  Real(KIND=8), Dimension(:,:,:,:,:), Pointer :: X1, X2
  Real(KIND=8) :: XMIN, XMAX, YMIN, YMAX, ZMIN, ZMAX

  !  Integers
  Integer :: ngrid(3), I, J, K, dir, NX, NY, NZ, NDIM(3)
  Integer, Dimension(:,:), Pointer :: ND1, ND2
  Integer, Dimension(:,:,:,:), Pointer :: IBLANK1, IBLANK2
  Integer, Parameter :: TRUE = 1, FALSE = 0


  !  Logical
  Logical :: gf_exists(3), sf_exists(3)

  !  Characters
  Character(LEN=2)  :: prec(3), gf(3), vf(3), ib(3)
  Character(LEN=80) :: grid_file(3), cmd

  !  Step zero, read in the command line
  Integer :: iargc, nargc, ng, isperiodic, ftype, alter
  Integer, allocatable :: refine(:,:)

  nargc = iargc()
  If (nargc /= 2) Then
    Write (*,'(A)') 'USAGE: refineMesh {grid file 1} {extruded grid file}'
    Stop
  Endif

  Call Getarg(1,grid_file(1));
  Call Getarg(2,grid_file(2));

  Call p3d_detect(LEN(Trim(grid_file(1))),Trim(grid_file(1)),prec,NDIM,gf,vf,ib,ftype)

  !  Step one, read in the mesh1 grid file
  Write (*,'(A)') ''
  Call Read_Grid(NDIM(1),ngrid(1),ND1,X1,IBLANK1, &
       prec(1),gf(1),vf(1),ib(1),grid_file(1),TRUE)
  Write (*,'(A)') ''

  allocate(refine(size(ND1,1),NDIM(1)))
  allocate(ND2(size(ND1,1),size(ND1,2)))

  !  Step two, find the refinement direction(s)
  refine(:,:) = 1
  Do ng = 1, ngrid(1)
    Write (*,'(A,I2,A,3(I3,1X))') 'Block ', ng, ' has size ', &
         (ND1(ng,i),i=1,3)
    Write (*,'(A)',advance='no') 'Alter this block? (0=no, 1=yes): '
    Read (*,*) alter
    If (alter == 1) Then
      Write (*,'(A)') 'Enter refinement factor: (integer >= 1, 1 gives no refinement): ' 
      Do dir = 1, NDIM(1)
        If (ND1(ng,dir) .ne. 1) Then
          If (dir == 1) Write (*,'(A)',advance='no') 'Refinement factor for the 1-dir: '
          If (dir == 2) Write (*,'(A)',advance='no') 'Refinement factor for the 2-dir: '
          If (dir == 3) Write (*,'(A)',advance='no') 'Refinement factor for the 3-dir: '
          read (*,*) refine(ng,dir)
        End If
      End Do
    End If
  End Do

  ! ... set the grid sizes
  ngrid(2) = ngrid(1)
  ND2(:,:) = ND1(:,:)
  NDIM(2)  = NDIM(1)
  Do ng = 1, ngrid(1)
    Do dir = 1, NDIM(1)
      ND2(ng,dir) = refine(ng,dir)*(ND1(ng,dir) - 1)
    End Do
  End Do

  Write (*,'(A)') 'The new grid sizes are: '
  Do I = 1, ngrid(2)
    Write (*,'(A,I2,A,3(I4,1X))') 'Grid ', I, ' is to be ', (ND2(I,J),J=1,NDIM(2))
  End Do

  NX = MAXVAL(ND2(:,1))
  NY = MAXVAL(ND2(:,2))
  NZ = MAXVAL(ND2(:,3))
  ALLOCATE(X2(ngrid(2),NX,NY,NZ,3),IBLANK2(ngrid(2),NX,NY,NZ))

  Stop 'refineMesh not yet complete.'

  Do ng = 1, ngrid(1)
    Do I = 1, ND1(ng,1)
      Do J = 1, ND1(ng,2)
        Do K = 1, ND1(ng,3)
          X2(ng,I,J,K,:) = X1(ng,I,J,K,:)
          IBLANK2(ng,I,J,K) = IBLANK1(ng,I,J,K)
        End Do
      End Do
    End Do
  End Do

  If (dir .eq. 1) Then
    Do ng = 1, ngrid(2)
      Do I = 1, ND2(ng,1)
        Do J = 1, ND2(ng,2)
          Do K = 1, ND2(ng,3)
            X2(ng,I,J,K,1) = XMIN + DBLE(I-1)/DBLE(ND2(ng,1)-(1-isperiodic)) &
                 * (XMAX-XMIN)
            X2(ng,I,J,K,2) = X2(ng,1,J,K,2)
            X2(ng,I,J,K,3) = X2(ng,1,J,K,3)
            IBLANK2(ng,I,J,K) = IBLANK2(ng,1,J,K)
          End Do
        End Do
      End Do
    End Do
  Else If (dir .eq. 2) Then
    Do ng = 1, ngrid(2)
      Do I = 1, ND2(ng,1)
        Do J = 1, ND2(ng,2)
          Do K = 1, ND2(ng,3)
            X2(ng,I,J,K,2) = YMIN + DBLE(J-1)/DBLE(ND2(ng,2)-(1-isperiodic)) &
                 * (YMAX-YMIN)
            X2(ng,I,J,K,1) = X2(ng,I,1,K,1)
            X2(ng,I,J,K,3) = X2(ng,I,1,K,3)
            IBLANK2(ng,I,J,K) = IBLANK2(ng,I,1,K)
          End Do
        End Do
      End Do
    End Do
  Else
    Do ng = 1, ngrid(2)
      Do I = 1, ND2(ng,1)
        Do J = 1, ND2(ng,2)
          Do K = 1, ND2(ng,3)
            X2(ng,I,J,K,3) = ZMIN + DBLE(K-1)/DBLE(ND2(ng,3)-(1-isperiodic)) &
                 * (ZMAX-ZMIN)
            X2(ng,I,J,K,1) = X2(ng,I,J,1,1)
            X2(ng,I,J,K,2) = X2(ng,I,J,1,2)
            IBLANK2(ng,I,J,K) = IBLANK2(ng,I,J,1)
          End Do
        End Do
      End Do
    End Do
  End If

  Call Write_Grid(NDIM(2),ngrid(2),ND2,X2,IBLANK2, &
       prec(1),gf(1),vf(1),ib(1),grid_file(2))
  Write (*,'(A)') ''

  ! Step three, clean up & leave
  Deallocate(ND1, X1, IBLANK1, ND2, X2, IBLANK2)


  Stop
End Program refineMesh
