#!/bin/bash
# Copyright (c) 2015, University of Illinois at Urbana-Champaign, XPACC
# License: MIT, http://opensource.org/licenses/MIT
# $Header: /cvsroot/genx/Codes/RocfloCM/plot3d_utils/assembleSoln.sh,v 1.1 2010/01/18 18:32:54 bodony Exp $

numstart=0
numstop=20
numskip=10

num=$numstart

while [ $num -le $numstop ]; 
do
   filenum=`echo $num | awk -- '{ printf "%08d", $1 }'`
   ./genx/Codes/RocfloCM/plot3d_utils/assembleSoln soln/RocFlo-CM.$filenum.q
   num=`expr $num + $numskip`
done
