#
# Build PLOT3D library and auxillary tools
#
set(plot3dF90-Sources ModPLOT3D_IO.f90 ModParser.f90)
set(plot3dC-Sources plot3d_format.c)
#
# setup a place for the module files to reside
#
set(moduleDirectory ${CMAKE_CURRENT_BINARY_DIR}/modules)
file(MAKE_DIRECTORY ${moduleDirectory})
#include_directories(${moduleDirectory})

set_source_files_properties(${plot3dF90-Sources} PROPERTIES COMPILE_FLAGS
  "${F90-Flags} ${MPI_FORTRAN_COMPILE_FLAGS}")
set_source_files_properties(${plot3dC-Sources} PROPERTIES COMPILE_FLAGS
  "${C-Flags} ${MPI_C_COMPILE_FLAGS}")
add_library(plot3d STATIC ${plot3dF90-Sources} ${plot3dC-Sources})
set_target_properties(plot3d PROPERTIES Fortran_MODULE_DIRECTORY ${moduleDirectory})
target_include_directories(plot3d PUBLIC ${moduleDirectory})
install(TARGETS plot3d RUNTIME DESTINATION bin LIBRARY DESTINATION lib ARCHIVE DESTINATION lib)

set(allC-Executables 
    swapEndian modifyIter modifyTime modifyTimeForNextOutput)

set(allF90-Executables 
    detectFormat cart3d appendGrids diffSoln readSolnTime
    extrudeMesh reverseDir Ogrid subsetMesh genOgrid subsetSoln
    setIBLANK extractValue mergeGrids hRefine computeXZAverage
    cart2d moveNodes searchAndReplaceIBLANK assembleSoln
    assembleGrid processPIV computeMean extrudeSoln extractProbe
    computeFunc genCircularSectorGrid data2ensight compare-p3d 
    boundary-layer jet-crossflow)

if(OversetTool)
  list(APPEND allF90-Executables overset-sandbox)
endif()

foreach(exec ${allF90-Executables})
  add_executable(${exec} ${exec}.f90)
  set_target_properties(${exec} PROPERTIES COMPILE_FLAGS "${F90-Flags} ${MPI_FORTRAN_COMPILE_FLAGS}")
  target_link_libraries(${exec} plot3d)
  install(TARGETS ${exec} RUNTIME DESTINATION bin/plot3d LIBRARY DESTINATION lib ARCHIVE DESTINATION lib)
endforeach()
target_link_libraries(compare-p3d utils)

if(OversetTool)
  target_link_libraries(overset-sandbox utils overset)
endif()

foreach(exec ${allC-Executables})
  add_executable(${exec} ${exec}.c)
  install(TARGETS ${exec} RUNTIME DESTINATION bin/plot3d LIBRARY DESTINATION lib ARCHIVE DESTINATION lib)
endforeach()
