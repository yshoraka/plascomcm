function evaluateSpline(nxb, dxb, xb, coeffs, nvar, x)

  USE ModParser
  Implicit None

! ... global variables
  Integer :: nxb, nvar
  Real(WP) :: dxb, x, evaluateSpline, xx
  Real(WP) :: xb(nxb), coeffs(nxb-1,4,nvar)

! ... local variables
  Integer :: index, k
  Real(WP) :: val

! ... find lower bound of breakpoint interval
  index = floor((x-xb(1))/dxb)+1

! ... local coordinate
  xx = x - xb(index)

! evaluate spline
  val = coeffs(index,1,nvar)
  do k = 2, 4
    val = val * xx + coeffs(index,k,nvar)
  end do

  evaluateSpline = val

end function evaluateSpline
!
Program flame1D
  USE ModPLOT3D_IO
  USE ModParser
  Implicit None

! I/O variables
  Character(LEN=2)  :: prec(3), grid_format(3), volume_format(3), ib(3)
  Character(LEN=80) :: grid_file(3), sol_file(1)

!  Mesh variables
  Integer :: ngrid, ndim, flag, Iwave
  Integer, Dimension(:,:), Pointer :: ND
  Integer, Dimension(:,:,:,:), Pointer :: IBLANK
  Real(WP), Dimension(:,:,:,:,:), Pointer :: X, Q, Qaux
  Real(WP) :: Lx, Ly, Lz, error, rhoEreac, rhoEprod
  Real(WP), parameter :: m_Zwave = 1.5d0
  Real(WP), parameter :: nitrogenOxygenMoleRatio = 3.76_WP
  real(WP), parameter :: m_wO=15.9994_WP, m_wH=1.00794_WP, m_wN=14.00674_WP
  Real(WP), parameter :: YO = 1.0_WP/(1.0_WP+m_wN/m_wO*nitrogenOxygenMoleRatio)
  Integer, parameter :: H2=1
  Integer, parameter :: O2=2
  Integer, parameter :: R =3
  Integer, parameter :: H2O=4

  Write (*,'(A)') ''
  Print *, '!=========================================== !'
  Print *, '! Homogeneous mixture initialization utility !'
  Print *, '!=========================================== !'
  Write (*,'(A)') ''

  print "(a34,$)", " preprocess(0) or postprocess(1)? "
  read "(i6)", flag
  if (flag == 0) then
! Call the routines
    Call flame1D_Init
    Call flame1D_Grid
    Call flame1D_Q
    Call flame1D_BC
    Call Finalize
  else
    Call checkIgnitionDelay(error)
    print*,'error(%)=',error
  endif

Contains


  Subroutine flame1D_Init
    Implicit None

    Character(len=80) :: input_name

! Parse the input file
    input_name="plascomcm.inp"
    call parser_init
    call parser_parsefile(trim(input_name))

    Return
  End Subroutine flame1D_Init


! ======================================== !
! Create the grid files                    !
! - Generate 3 grids for jet in cross flow !
! - IBlank solid boundaries                !
! - Identify receiver points               !
! ======================================== !
  Subroutine flame1D_Grid
    Implicit None

! Local variables
    Integer :: i, j, K, N
    Real(WP) :: dx, dy, dz, dxx, stretchFactor, alphaP, alphaM

! Initialize the grid (1 grid 2D)
    ngrid = 1
    ndim = 2
    Allocate(ND(ngrid,3))

! Set grid parameters
    prec(:)          = 'd' ! Precision: double
    grid_format(:)   = 'm' ! Grid format: single
    volume_format(:) = 'w' ! Volume format: whole
    ib(:)            = 'y' ! IBlank

! Number of grid points (Grid 1)
    Call Parser_read('GRID_NX',ND(1,1))
    Call Parser_read('GRID_NY',ND(1,2))
    Call Parser_read('GRID_NZ',ND(1,3))
    Print *, 'Grid: ',ND(1,1),'x',ND(1,2),'x',ND(1,3)

! Grid size
    call parser_read('GRID_LX',Lx)
    call parser_read('GRID_LY',Ly)
    call parser_read('GRID_LZ',Lz)
    !call parser_read('STRETCH_FACTOR', stretchFactor, 1d0)
    dx = Lx / DBLE(ND(1,1))
    dy = Ly / DBLE(ND(1,2))
    dz = Lz / DBLE(ND(1,3))

    !IWAVE = CEILING(DBLE(ND(1,1))/m_Zwave)
    
    !dxx = dx/stretchFactor
    !N =  ND(1,1)-IWAVE+1
    !Call GeometricStretchFactor(0d0,Lx/2d0, N,dxx,1.05_8,5D-1,alphaP)
    !N =  IWAVE-1+1
    !Call GeometricStretchFactor(0d0,Lx/2d0, N,dxx,1.05_8,5D-1,alphaM)
    !print*,'alphaP = ',alphaP, 'alphaM = ', alphaM

! Allocate the arrays and assign values
    ALLOCATE(X(ngrid,MAXVAL(ND(:,1)),MAXVAL(ND(:,2)),MAXVAL(ND(:,3)),3))
    ALLOCATE(IBLANK(ngrid,MAXVAL(ND(:,1)),MAXVAL(ND(:,2)),MAXVAL(ND(:,3))))
    Do K = 1, ND(1,3)
      Do J = 1, ND(1,2)
        Do I = 1, ND(1,1)
          X(1,I,J,K,1) = DBLE(I-1)/DBLE(ND(1,1)-1) * (Lx - dx) - 1.6_wp
          X(1,I,J,K,2) = DBLE(J-1)/DBLE(ND(1,2)-1) * (Ly - dy)
          X(1,I,J,K,3) = 0.0_wp !DBLE(k-1)/DBLE(ND(1,3)-1) * (Lz - dz)         
       End Do
      End Do
    End Do
    IBLANK = 1

! Write the grid file
    grid_file(1) = "grid.xyz"
    Write (*,'(A)') ''
    print*,'ngrid=',ngrid
    Call Write_Grid(3,ngrid,ND,X,IBLANK, &
         prec(1),grid_format(1),volume_format(1),ib(1),grid_file(1))
    Write (*,'(A)') ''

    Return
  End Subroutine flame1D_Grid


! ========================= !
! Initial (target) solution !
! ========================= !
  Subroutine flame1D_Q
    Implicit None

    integer :: I, J, K, L, nspecies
    Real(WP), Dimension(:,:,:,:), Pointer :: rho, rhoU, rhoV, rhoW, rhoE
    Real(WP), Dimension(:,:,:,:), Pointer :: Fuel, Air, Radical, Water
    Real(WP) :: rho0, P0, T0, Tref, mu0, L0, YF0, YO0, gamma, C, Re, Pr, TAU(4)
    Real(WP), Pointer :: Mwcoeff(:)
    Real(WP) :: MwRef, T0_Tref, FACTOR
    Integer :: N2

! Read reference parameters from input
    call parser_read('REYNOLDS_NUMBER', Re, 0.0_WP)
    call parser_read('PRANDTL_NUMBER', Pr, 0.72_WP)
    call parser_read('DENSITY_REFERENCE', rho0, 1.0_WP)
    call parser_read('PRESSURE_REFERENCE', P0)
    call parser_read('TEMPERATURE_REFERENCE', Tref)
    call parser_read('GAMMA_REFERENCE', gamma, 1.4_WP)
    call parser_read('SNDSPD_REFERENCE', C, 347.0_WP)
    call parser_read('LENGTH_REFERENCE',L0)

! Mixture properties
    call parser_read('MIXTURE_TEMPERATURE',T0)
    call parser_read('MIXTURE_FUEL_CONCENTRATION',YF0)
    call parser_read('MIXTURE_OXIDIZER_CONCENTRATION',YO0,-1.0_WP)

! Read reference parameters from input
    call parser_read('NUMBER_OF_SCALARS',nspecies,0)
    If (nspecies.EQ.0) Then
      Write (*,'(A)') 'This utility requires nSpecies > 0!'
      Stop
    End If

! Nitrogen
    N2 = nspecies+1

! Initialize the timing
    TAU(1) = 0.0_WP
    TAU(2) = 0.0_WP
    TAU(3) = Re
    TAU(4) = 0.0_WP

! Allocate solution array
    Allocate(Q(ngrid,MAXVAL(ND(:,1)),MAXVAL(ND(:,2)),MAXVAL(ND(:,3)),5))
    Allocate(Qaux(ngrid,MAXVAL(ND(:,1)),MAXVAL(ND(:,2)),MAXVAL(ND(:,3)),nspecies))

    Qaux = 0d0
! Link the pointers
    rho   =>  Q(:,:,:,:,1)
    rhoU  =>  Q(:,:,:,:,2)
    rhoV  =>  Q(:,:,:,:,3)
    rhoW  =>  Q(:,:,:,:,4)
    rhoE  =>  Q(:,:,:,:,5)
    Fuel  =>  Qaux(:,:,:,:,H2)
    Air   =>  Qaux(:,:,:,:,O2)
    If (nspecies.GE.3) Radical => Qaux(:,:,:,:,R)
    If (nspecies.GE.H2O) Water => Qaux(:,:,:,:,H2O)

! Non-dimensionalize
    C = sqrt(gamma*P0/rho0)
    P0 = P0 / rho0 / C**2
    T0_Tref = T0 / Tref

! Initialize the momentum
    rhoU = 1d-4
    rhoV = 0.0_WP
    rhoW = 0.0_WP

! Molecular weights
    Allocate(Mwcoeff(nspecies+1))
    Mwcoeff = 0d0
    Mwcoeff(H2) = 2.0_WP*m_wH
    Mwcoeff(O2) = 2.0_WP*m_wO
    If (nspecies.GE.R) Mwcoeff(R) = m_wH
    If (nspecies.GE.H2O) Mwcoeff(H2O) = 2.0_WP*m_wH+m_wO
    Mwcoeff(N2) = 2.0_WP*m_wN

! Transform into the Mw^-1
    Mwcoeff = 1.0_WP/Mwcoeff

! Inverse of the reference Mw
    MwRef = YO*Mwcoeff(O2) + (1.0_WP-YO)*Mwcoeff(N2)
    print*,'MwRef=',1d0/MwRef

! Divide Mw by MwRef
    Mwcoeff = Mwcoeff/MwRef

! Remove the nitrogen
    Mwcoeff(1:nspecies) = Mwcoeff(1:nspecies) - Mwcoeff(N2)

! Initialize fuel and radicals on all grids
    
    If (nspecies.GE.R) Radical = 0.0_WP
    If (nspecies.GE.H2O) Water = 0.0_WP

    ! Compute energy
    rhoE = P0/(gamma-1.0_WP)! - 3.56*P0

! Get Density assuming isobaric isothermal gas
    Rho = Mwcoeff(N2)
    Do K = 1, ND(1,3)
      Do J = 1, ND(1,2)
        Do I = 1, ND(1,1)
          FACTOR = 1d0 + (tanh(X(1,I,J,K,1)*100.0d0) + 1d0)*(0d0-1d0)/2d0
          Fuel(1,I,J,K) = YF0 *FACTOR
! Air fraction
          Air(1,I,J,K) = (1.0_WP - YF0)*FACTOR
! Change Air into Oxygen
          Air(1,I,J,K) = Air(1,I,J,K)*YO
! Water
          If (nspecies.GE.H2O) Water(1,I,J,K)  = (1.0_WP - FACTOR)*(YF0 + (1d0-YF0)* YO)
          if (nspecies.GE.H2O) rhoE(1,I,J,K) = rhoE(1,I,J,K) + (1.0_WP - FACTOR)*(-0.0_WP)*P0
          !if(J == 1) print*,1d0 - sum(Qaux(1,I,J,K,:))
          Do L = 1, nspecies
            Rho(1,I,J,K) = Rho(1,I,J,K) + Mwcoeff(L)*Qaux(1,I,J,K,L)
          End Do
          FACTOR = 1d0 + (tanh(X(1,I,J,K,1)*100.0d0) + 1d0)*(T0_Tref-1d0)/2d0
          Rho(1,I,J,K) = Rho(1,I,J,K)*FACTOR
          !rhoE(1,I,J,K) = rhoE(1,I,J,K)/(FACTOR**0.005d0) !0.173
        End Do
      End Do
    End Do

    !Radical = 0.001d0*Fuel

    !Fuel = 0.999d0*Fuel

    Rho = 1d0/Rho

! Change mass fractions into mass densities
    Do I = 1, nspecies
      Qaux(:,:,:,:,I) = Qaux(:,:,:,:,I)*Rho
    End Do



! Write the solution file (state variables)
    sol_file(1)="RocFlo-CM.00000000.target.q"
    Call Write_Soln(3,ngrid,ND,Q,TAU,prec(1),grid_format(1),volume_format(1),sol_file(1))
    Write (*,'(A)') ''

! Write the solution file (auxilary variables)
    sol_file(1)="RocFlo-CM.00000000.target.aux.q"
    Call Write_Func(ngrid,ND,Qaux,sol_file(1))
    Write (*,'(A)') ''

    Return
  End Subroutine flame1D_Q


! ================== !
! Clean up and leave !
! ================== !
  Subroutine Finalize
    Implicit None

    Deallocate(ND,IBLANK,X,Q)

    Return
  End Subroutine Finalize

  Subroutine checkIgnitionDelay(error)
    Implicit None
    Integer :: i,nentries, itimes, io
    Real(WP) :: error, val(2), x, tgradU, tign, tgradM
    Real(WP), pointer :: data(:,:)
    Character(len=80) :: fname(2)

    fname(1) = 'plascom.dat'
    fname(2) = 'model1000_0.dat'  !!cantera file


    do itimes = 1,2
      nentries = 0
      open(123,file=trim(fname(itimes)))
      DO
        READ(123,*,IOSTAT=io)  x
        IF (io > 0) THEN
          WRITE(*,*) 'Check input.  Something was wrong'
          EXIT
        ELSE IF (io < 0) THEN
          WRITE(*,*)  'total entries ', nentries
          EXIT
        ELSE
          nentries = nentries + 1
        END IF
      END DO
      allocate(data(nentries,2))
      rewind(123)
      DO i=1,nentries
        READ(123,*)  data(i,1:2)
      END DO
      close(123)
      tgradU=0d0;tign=0; tgradM=0;
      DO i=2,nentries
        tgradU = (data(i,2)-data(i-1,2))/(data(i,1)-data(i-1,1));
        if(tgradU > tgradM) then
          tgradM = tgradU;
          tign=(data(i,1)+data(i-1,1))/2.0;
        endif
      ENDDO
      val(itimes) = log10(tign)
      nullify(data)
    enddo

    error = abs(val(2)-val(1))/abs(val(2)+val(1))*200
    Return
  End Subroutine checkIgnitionDelay


! =================== !
! Boundary conditions !
! =================== !
  Subroutine flame1D_BC
    Implicit None

    Integer :: NC,iunit,allb(2),ng
    INTEGER, PARAMETER :: OUTER_BOUNDARY                              = 00,  &
         CUTTING_SURFACE                                 = 01,  &
         O_GRID_PERIODICITY                              = 10,  &
         C_MESH                                          = 11,  &
         NSCBC_INFLOW_VELOCITY_TEMPERATURE               = 31,  &
         NSCBC_INFLOW_VELOCITY_TEMPERATURE_FROM_SPONGE   = 32,  &
         NSCBC_INFLOW_VELOCITY_TEMPERATURE_FORCING_SLOT  = 33,  & ! JKim 06/2008
         NSCBC_OUTFLOW_PERFECT_NONREFLECTION             = 41,  &
         NSCBC_OUTFLOW_PERFECT_REFLECTION                = 42,  &
         NSCBC_WALL_ADIABATIC_SLIP                       = 51,  &
         NSCBC_WALL_ISOTHERMAL_NOSLIP                    = 52,  &
         NSCBC_WALL_ADIABATIC_SLIP_COUPLED               = 53,  &
         NSCBC_WALL_ISOTHERMAL_NOSLIP_COUPLED            = 54,  &
         NSCBC_WALL_NOSLIP_THERMALLY_COUPLED             = 55,  &
         SAT_SLIP_ADIABATIC_WALL_COUPLED                 = 56,  &
         NSCBC_SUBSONIC_INFLOW                           = 61,  &
         NSCBC_SUPERSONIC_INFLOW                         = 62,  &
         SAT_SLIP_ADIABATIC_WALL                         = 21,  &
         SAT_NOSLIP_ISOTHERMAL_WALL                      = 22,  &
         SAT_NOSLIP_ADIABATIC_WALL                       = 23,  &
         SAT_FAR_FIELD                                   = 24,  &
         SAT_BLOCK_INTERFACE                             = 25,  &
         SAT_WALL_NOSLIP_THERMALLY_COUPLED               = 26,  &
         SAT_BLOCK_INTERFACE_PERIODIC                    = 27,  &
         SAT_PRESSURE                                    = 28,  & !Luca1 09/2014
         SAT_INJECTION                                   = 29,  & !Luca1 09/2014
         SPONGE                                          = 99,  &
         SPONGE_WITH_LINEARIZED_DISTURBANCE              = 98,  &
         FV_TRANSMISSIVE_EXTRAPOLATION_0TH_ORDER         = 41,  &
         FV_PERFECT_REFLECTION                           = 42,  &
         FV_WALL_SLIP_EXTRAPOLATION_0TH_ORDER            = 51,  &
         FV_WALL_ISOTHERMAL_NOSLIP                       = 52,  &
         FV_SUPERSONIC_INFLOW                            = 62,  &
         FV_PERIODICITY                                  = 10,  &
         FV_DIRICHLET                                    = 91,  &
         FV_NEUMANN                                      = 92,  &
         FV_SYMMETRY                                     = 93,  &
         FV_BLOCK_INTERFACE                              = 94,  &
         FV_BLOCK_INTERFACE_PERIODIC                     = 95,  &
         THERMAL_TEMPERATURE                             = 101, &
         THERMAL_HEAT_FLUX                               = 102, &
         STRUCTURAL_DISPLACEMENT                         = 103, &
         STRUCTURAL_TRACTION                             = 104, &
         STRUCTURAL_PRESSURE                             = 105, &
         STRUCTURAL_INTERACTING                          = 106, &
         INTEGRAL_SURFACE                                = 200, &
         EF_DIRICHLET_G                                  = 300, &
         EF_DIRICHLET_P                                  = 301, &
         EF_NEUMANN                                      = 302, &
         EF_DIELECTRIC                                   = 399, &!LM added this one for charged particles BC at the walls. The 300's will span Plasma BCs &
         EF_ELECTRODE                                    = 301, &!LM::overloading EF_DIRICHLET_P
         SAT_AXISYMMETRIC                                = 400

! Get sponge zone thickness
    Call parser_read('NCELL_SPONGE',NC,0)

! Open the file
    iunit=11
    open(iunit,file="bc.dat")

    print *, 'Writing boundary conditions'
    Write (*,'(A)') ''

! Write the header
    write(iunit,'(1a1 )') "#"
    write(iunit,'(1a45)') "# Basic boundary condition file for PlasComCM"
    write(iunit,'(1a1 )') "#"
    write(iunit,'(1a24)') "# FD = finite difference"
    write(iunit,'(1a20)') "# FV = finite volume"
    write(iunit,'(1a1 )') "#"
    write(iunit,'(1a8 )') "# ibType"
    write(iunit,'(1a8 )') "# ======"
    write(iunit,'(1a32)') "#     00     OUTER_BOUNDARY (FD)"
    write(iunit,'(1a41)') "#     21     SAT_SLIP_ADIABATIC_WALL (FD)"
    write(iunit,'(1a44)') "#     22     SAT_NOSLIP_ISOTHERMAL_WALL (FD)"
    write(iunit,'(1a33)') "#     23     SAT_NOSLIP_ADIABATIC_WALL (FD)"
    write(iunit,'(1a31)') "#     24     SAT_FAR_FIELD (FD)"
    write(iunit,'(1a37)') "#     25     SAT_BLOCK_INTERFACE (FD)"
    write(iunit,'(1a51)') "#     31     NSCBC_INFLOW_VELOCITY_TEMPERATURE (FD)"
    write(iunit,'(1a63)') "#     32     NSCBC_INFLOW_VELOCITY_TEMPERATURE_FROM_SPONGE (FD)"
    write(iunit,'(1a57)') "#     41     NSCBC_OUTFLOW_PERFECT_NONREFLECTION (FD, FV)"
    write(iunit,'(1a54)') "#     42     NSCBC_OUTFLOW_PERFECT_REFLECTION (FD, FV)"
    write(iunit,'(1a47)') "#     51     NSCBC_WALL_ADIABATIC_SLIP (FD, FV)"
    write(iunit,'(1a50)') "#     52     NSCBC_WALL_ISOTHERMAL_NOSLIP (FD, FV)"
    write(iunit,'(1a39)') "#     61     NSCBC_SUBSONIC_INFLOW (FD)"
    write(iunit,'(1a45)') "#     62     NSCBC_SUPERSONIC_INFLOW (FD, FV)"
    write(iunit,'(1a29)') "#     73     PERIODICITY (FV)"
    write(iunit,'(1a30)') "#     91     FV_DIRICHLET (FV)"
    write(iunit,'(1a28)') "#     92     FV_NEUMANN (FV)"
    write(iunit,'(1a29)') "#     93     FV_SYMMETRY (FV)"
    write(iunit,'(1a36)') "#     94     FV_BLOCK_INTERFACE (FV)"
    write(iunit,'(1a45)') "#     95     FV_BLOCK_INTERFACE_PERIODIC (FV)"
    write(iunit,'(1a28)') "#     99     SPONGE (FD, FV)"
    write(iunit,'(1a27)') "#     300    EF_DIRICHLET_G"
    write(iunit,'(1a25)') "#     301    EF_DIRICHLET_P"
    write(iunit,'(1a23)') "#     302    EF_NEUMANN"
    write(iunit,'(1a26)') "#     399    EF_DIELECTRIC"
    write(iunit,'(1a1 )') "#"
    write(iunit,'(1a56)') "# Grid ibType ibDir   is    ie    js    je     ks    ke "
    write(iunit,'(1a56)') "# ==== ====== =====  ====  ====  ====  ====   ====  ===="


! Burak Korkut; 09092015
    allb = [1,-1]
    ng = 1
! Flow
    write(iunit,'(9I6)')      ng,   SAT_FAR_FIELD,      1,     1,     1,    allb,    allb
    write(iunit,'(9I6)')      ng,   SAT_FAR_FIELD,     -1,    -1,    -1,    allb,    allb
    write(iunit,'(9I6)')      ng,   SPONGE,             1,     1,     1+NC,    allb,    allb
    write(iunit,'(9I6)')      ng,   SPONGE,            -1,    -1-NC,    -1,    allb,    allb
    !write(iunit,'(9I6)')      ng,   SAT_PRESSURE,      2,     allb,     1,    1,    allb
    !write(iunit,'(9I6)')      ng,   SAT_PRESSURE,     -2,     allb,    -1,   -1,    allb

! Close the file
    close(iunit)

    Return
  End Subroutine flame1D_BC

  
  ! ================================ !
  ! Two-point boundary value problem !
  ! for optimal grid stretching      !
  ! ================================ !
  Subroutine GeometricStretchFactor(x1,xn,n,dx,guess,safety_fac,alpha)
    Implicit None

      ! ... input data
      Real(KIND=8) :: x1,xn,dx
      Integer :: n
      Real(KIND=8) :: guess, safety_fac

      ! ... output data
      Real(KIND=8) :: alpha

      ! ... local variables
      Integer :: i
      Real(KIND=8) :: alpha_old, alpha_new, f_old, f_new, err, slope
      Real(KIND=8), Parameter :: err_tol = 1D-12

      ! ... use secant method to solve the equation dx = (alpha - 1)/(alpha^{n-1}-1)
      alpha_old = guess
      alpha_new = 1.01_8 * guess

      f_old = (xn-x1) - (alpha_old**(n-1) - 1.0_8) / (alpha_old-1.0_8) * dx
      f_new = (xn-x1) - (alpha_new**(n-1) - 1.0_8) / (alpha_new-1.0_8) * dx
      err = dabs(f_new)

      ! ... error check that we got it right by guess
      If (err < err_tol) Then
         alpha = alpha_new
         Return
      End If

      ! ... iterate
      Do While (err > err_tol)

         ! ... secant line slope
         If (dabs(alpha_new - alpha_old) < err_tol) Then
            alpha = alpha_new
            Return
         End If
         slope = (f_new-f_old)/(alpha_new-alpha_old)

         ! ... save
         alpha_old = alpha_new
         f_old = f_new

         ! ... estimate new
         If (dabs(slope) >= err_tol) Then
            alpha_new = alpha_new - safety_fac * f_new / slope
         Else
            alpha = alpha_new
            Return
         End If

         ! ... evaluate new
         f_new = (xn-x1) - (alpha_new**(n-1) - 1.0_8) / (alpha_new-1.0_8) * dx
         err = dabs(f_new)

      End Do

      alpha = alpha_new
      Return
      
    End Subroutine GeometricStretchFactor


End Program flame1D
