! Copyright (c) 2015, University of Illinois at Urbana-Champaign, XPACC
! License: MIT, http://opensource.org/licenses/MIT
Program detectFormat
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!                                                               !
! Detect the plot3d format of a file                            !
!                                                               !
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!                                                               !
! Written by Daniel J. Bodony (bodony@stanford.edu)             !
! 17 May 2006                                                   !
! 20 Dec 2006 updated by DJB (bodony@uiuc.edu)                  !
!                                                               !
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  USE ModPLOT3D_IO
  Implicit None

  !  Logical
  Logical :: gf_exists(1)

  !  Characters
  Character(LEN=2)  :: prec, gf, vf, ib
  Character(LEN=80) :: file, cmd

  !  Step zero, read in the command line
  Integer :: iargc, nargc, NDIM, ftype, i, j, ngrid
  Integer, Dimension(:,:), Pointer :: ND

  nargc = iargc()
  If (nargc /= 1) Then
    Write (*,'(A)') 'USAGE: detectFormat {file} '
    Stop
  Else
    Call Getarg(1,file);
  End If

  Call p3d_detect(LEN(Trim(file)),Trim(file),prec,NDIM,gf,vf,ib,ftype)

  Call output_format(file,prec,NDIM,gf,vf,ib,ftype)

  Open (unit=10, file=trim(file), form='unformatted', status='old')

  !  Read number of grids
  If (gf(1:1) .eq. 's') Then
    ngrid = 1
  Else
    Read (10) ngrid
  End If
  Allocate(ND(ngrid,3))
  Write (*,'(A)') ' '
  Write (*,'(A,I2)') 'Number of grids: ', ngrid

  !  Read grid sizes
  ND(:,3) = 1
  Read (10) ((ND(I,J),J=1,NDIM),I=1,ngrid)
  Do I = 1, ngrid
    Write (*,'(A,I2,A,3(I4,1X))') 'Grid ', I, ' is ', (ND(I,J),J=1,NDIM)
  End Do

  close (10)

End Program detectFormat
