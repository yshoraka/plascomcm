! Copyright (c) 2015, University of Illinois at Urbana-Champaign, XPACC
! License: MIT, http://opensource.org/licenses/MIT
Program Jet_Crossflow
  ! ============================================================= !
  ! JET IN CROSS FLOW: FLOW INITIALIZATION ROUTINE                !
  ! Generates a grid, initial/boundary conditions,                !
  ! and interpolation coefficients between grids                  !
  ! for simulating a turbulent jet in crossflow                   !
  !                                                               !
  ! ============================================================= !
  !                                                               !
  ! Written by Jesse Capecelatro (jcaps@illinois.edu)             !
  ! Began: 26 August 2014                                         !
  ! Grid interpolation: 20 November 2014                          !
  !                                                               !
  ! ============================================================= !
  USE ModPLOT3D_IO
  USE ModParser
  Implicit None

  ! I/O variables
  Character(LEN=2)  :: prec(3), grid_format(3), volume_format(3), ib(3)
  Character(LEN=80) :: grid_file(3), sol_file(1)

  !  Mesh variables
  Integer :: ngrid, ndim
  Integer :: NX1   ! Number of points in x on grid 1
  Integer :: NY1   ! NUmber of points in y on grid 1
  Integer :: NZ1   ! Number of points in z on grid 1
  Integer :: NR2   ! Number of radial points in jet of grid 2
  Integer :: NT2   ! Number of azimuthal points on grid 2
  Integer :: NY23  ! Number of axial points for grid 2 and grid 3 (not including overlap with grid 1)
  Integer :: NS3   ! Number of points along side of grid 3 (inner rectangle)
  Integer :: NY_O  ! Number of axial points overlapping grids 2 and 3 with grid 1
  Integer :: NR_O  ! Number of overlapping points in radial direction for grid 2 jet
  Integer :: NS_O  ! Number of points along side of blanked out square on grid 1
  Integer :: NR4   ! Number of radial points in rounded corner for grid 4
  Integer :: NP4   ! Number of points in phi for grid 4
  Integer :: NR24x ! Number of points on grid 2 overlapping grid 4 in x
  Integer :: NR24y ! Number of points on grid 2 overlapping grid 4 in y
  Integer :: nspecies ! Number of species
  Integer, parameter :: JP = 64 ! Number of points from bottom of jet to end of velocity decay
  Integer :: I2, I3, J2, EI, EJ
  Integer, Dimension(:,:), Pointer :: ND
  Integer, Dimension(:,:,:,:), Pointer :: IBLANK
  Real(WP), Dimension(:,:,:,:,:), Pointer :: X
  Real(WP) :: Lx, Ly, Lz, Lx0
  Real(WP) :: ODJet, IDJet, XJET, EDEPTH, R0
  Logical :: Use_Trip, Use_Electrode, Round_Corners, Interpolate

  ! Solution variables
  Real(WP), Dimension(:,:,:,:), Pointer :: RHO
  Real(WP), Dimension(:,:,:,:,:), Pointer :: Q, Qaux

  ! Interpolation variables
  Integer, Parameter :: NBFringe = 2   ! Depth of interpolation fringe
  Integer :: id_in, ipall, igall, ipip, ipbp
  Integer, Pointer :: iipnts(:), ibpnts(:), ibpnts_(:,:), iisptr(:), iieptr(:), ieng(:), jeng(:), keng(:)
  Integer, Pointer :: ibt(:,:), jbt(:,:), kbt(:,:), ibct(:,:), kit(:,:), iit(:,:), jit(:,:), nit(:,:), njt(:,:), nkt(:,:)
  Integer, Pointer :: donor(:,:)
  Real(WP), Pointer :: dxit(:,:), dyit(:,:), dzit(:,:), coeffit(:,:,:,:)

  ! Useful parameters
  Real(WP), parameter :: Pi    = 3.1415926535897932385_8
  Real(WP), parameter :: twoPi = 6.2831853071795864770_8

  Write (*,'(A)') ''
  Print *, '!========================================= !'
  Print *, '! Jet in cross flow initialization utility !'
  Print *, '!========================================= !'
  Write (*,'(A)') ''
  
  ! Call the routines
  Call Jet_Crossflow_Init
  Call Jet_Crossflow_Grid
  Call Jet_Crossflow_Aux_Q
  Call Jet_Crossflow_Q
  Call Jet_Crossflow_BC
  If (Interpolate) Call Jet_Crossflow_Interp
  Call Finalize

Contains


  ! ============================ !
  ! Initialize jet in cross flow !
  ! ============================ !
  Subroutine Jet_Crossflow_Init
    Implicit None

    Character(len=80) :: input_name

    ! Parse the input file
    input_name="plascomcm.inp"
    call parser_init
    call parser_parsefile(trim(input_name))

    call parser_read('GENERATE_INTERPOLATION_FILES', Interpolate, .false.)

    Return
  End Subroutine Jet_Crossflow_Init


  ! ======================================== !
  ! Create the grid files                    !
  ! - Generate 3 grids for jet in cross flow !
  ! - IBlank solid boundaries                !
  ! - Identify receiver points               !
  ! ======================================== !
  Subroutine Jet_Crossflow_Grid
    Implicit None

    ! Grid 1: Cartesian boundary layer
    ! Grid 2: Cylindrical jet
    ! Grid 3: Cartesian rectangle to remove pole singularity in grid 2
    !
    !                             GRID 1
    !
    ! J=J2 >             _~~~~~~~~~~~~~~~~~~~~~~~~~~_
    !                   |                            |
    ! J=1  > ___________|                            |_________________|___:___|_____
    !
    !        ^          ^                            ^                     ^         ^
    !       I=1        I=I2                         I=I3                 I=IJet     I=NX1
    !
    !
    !
    !
    !                              GRID 2
    !
    ! J=ND(2,2) >      ||          |        :        |          ||
    !                  ||          |        :        |          ||
    !                  ||          |        :        |          ||
    ! J=NY23 > ________||==========|========:========|==========||________ < Location at bottom of grid 1
    !                              |        :        |
    !                              |        :        |
    !                              |        :        |
    !                              |        :        |
    ! J=EJ >                       |___     :     ___|
    !                                  |    :    |
    !                                  |    :    |
    !                                  |    :    |
    !                                  |    :    |
    !                                  |    :    |
    !                                  |    :    |
    !                                  |    :    |
    !                                  |    :    |
    ! J=1 >                            |    :    |
    !                                       ^    ^    ^          ^
    !                                     I=1  I=EI  I=NR2    I=ND(2,1)

    ! Local variables
    Integer :: I, J, K, N, JJ, NGRIT, IJet, KJet, IC1, IC2, IC3, IC4, NO_pole
    Real(WP) :: dx, dy, dz, dr, ytilde, s, r, Dcollar
    Real(WP) :: trip_loc, trip_width, trip_height
    Real(WP) :: grit_size, rnd, GAUSS, AMP, x0, z0, z12
    Real(WP), parameter :: rpole=0.000001_WP
    Real(WP), dimension(:,:), allocatable :: rbuf1,rbuf2,pbuf1,pbuf2
    Logical :: use_grit, use_stretching

    ! The Greek alphabet
    Real(WP) :: alpha, delta, delta0, phi, sig, theta

    ! Initialize the grids
    Call Parser_read('NGRID',NGRID)

    ! Number of dimensions
    call Parser_read('ND',NDIM,3)
    Allocate(ND(ngrid,3)); ND(:,3) = 1

    ! Set grid parameters
    prec(:)          = 'd' ! Precision: double
    grid_format(:)   = 'm' ! Grid format: multiple
    volume_format(:) = 'w' ! Volume format: whole
    ib(:)            = 'y' ! IBlank

    ! Number of grid points (Grid 1)
    Call Parser_read('GRID1_NX',NX1)
    Call Parser_read('GRID1_NY',NY1)
    Call Parser_read('GRID1_NZ',NZ1)
    ND(1,1)=NX1
    ND(1,2)=NY1
    ND(1,3)=NZ1
    Print *, 'Grid 1: ',ND(1,1),'x',ND(1,2),'x',ND(1,3)

    ! Grid overlap regions
    NY_O = ND(1,2) - 1                  ! Overlap grids 2 and 3 into grid 1
    Call Parser_read('JET_COLLAR',NR_O) ! Number of radial points in jet collar

    ! Number of grid points (Grid 2)
    Call Parser_read('GRID2_NR',NR2)
    Call Parser_read('GRID2_NY',NY23)
    Call Parser_read('GRID2_NT',NT2)
    ND(2,1)=NR2+NR_O
    ND(2,2)=NY23+NY_O
    ND(2,3)=NT2
    Print *, 'Grid 2: ',ND(2,1),'x',ND(2,2),'x',ND(2,3)

    ! Number of grid points (Grid 3)
    Call Parser_read('GRID3_NS',NS3)
    ND(3,1)=NS3
    ND(3,2)=NY23+NY_O
    ND(3,3)=NS3
    Print *, 'Grid 3: ',ND(3,1),'x',ND(3,2),'x',ND(3,3)

    ! Number of grid points (Grid 4)
    If (ngrid.gt.3) Then
       Round_Corners = .True.
       Call Parser_read('CORNER_RADIUS',R0)
       Call Parser_read('GRID4_NR',NR4)
       Call Parser_read('GRID4_NP',NP4)
       ND(4,2) = NR4
       ND(4,1) = NP4
       ND(4,3) = NT2
       Print *, 'Grid 4: ',ND(4,1),'x',ND(4,2),'x',ND(4,3)
    Else
       Round_Corners = .False.
    End If

    Write (*,'(A)') ''
    Print *, 'Total number of grid points: ', Real((PRODUCT(ND(1,:))+PRODUCT(ND(2,:))+PRODUCT(ND(3,:)))*10**(-6.0_8),4),'M'

    ! Grid Stretching
    Call Parser_Read('STRETCH_Y',Use_Stretching,.FALSE.)

    ! Grid 1 size
    call parser_read('GRID1_LX',Lx)
    call parser_read('GRID1_LX0',Lx0)
    call parser_read('GRID1_LY',Ly)
    call parser_read('GRID1_LZ',Lz)
    dx = (Lx - Lx0) / DBLE(ND(1,1))
    dy = Ly / DBLE(ND(1,2))
    dz = Lz / DBLE(ND(1,3))

    ! Read jet parameters
    Call Parser_Read('JET_OUTER_DIAMETER',ODJET)
    Call Parser_Read('JET_POSITION',XJET)

    ! Electrode
    Call Parser_Read('USE_ELECTRODE',Use_Electrode)
    If (Use_Electrode) Then
       Call Parser_Read('JET_INNER_DIAMETER',IDJET)
       Call Parser_Read('ELECTRODE_DEPTH',EDEPTH)
    End If

    ! Allocate the arrays and assign values
    ALLOCATE(X(ngrid,MAXVAL(ND(:,1)),MAXVAL(ND(:,2)),MAXVAL(ND(:,3)),3))
    ALLOCATE(IBLANK(ngrid,MAXVAL(ND(:,1)),MAXVAL(ND(:,2)),MAXVAL(ND(:,3))))

    ! Initialize IBLANK
    IBLANK = 1

    ! --------------------
    ! CREATE GRID 1

    ! Initialize stretching in y
    If (Use_Stretching) Then
       ! To avoid corner issue near jet, grid spacing in y
       ! is held constant at dr for first 11 points
       dr = 0.5_WP*ODJET/DBLE(NR2-1)
       Call GeometricStretchFactor(10.0_WP*dr,Ly,ND(1,2)-11,dr,1.05_8,5D-1,alpha)
    End If

    ! Cartesian box
    Do K = 1, ND(1,3)
       Do J = 1, ND(1,2)
          Do I = 1, ND(1,1)
             ! Create X
             X(1,I,J,K,1) = DBLE(I-1)/DBLE(ND(1,1)-1)*(Lx-Lx0) + Lx0
             ! Create Y
             If (Use_Stretching) Then
                If (J==1) Then
                   X(1,I,J,K,2) = 0.0_WP
                Elseif (J.GT.1 .AND. J.LE.11) Then
                   X(1,I,J,K,2) = X(1,I,J-1,K,2) +  dr
                Else
                   X(1,I,J,K,2) = X(1,I,11,K,2) + (alpha**(J-11)-1.0_WP)/(alpha-1.0_WP)*dr
                End If
             Else
                X(1,I,J,K,2) = DBLE(J-1)/DBLE(ND(1,2)-1)*Ly
             End If

             ! Create Z. Periodic direction, correct for length
             X(1,I,J,K,3) = DBLE(K-1)/DBLE(ND(1,3)-1)*(Lz-dz)
          End Do
       End Do
    End Do

    ! Trip strip
    Call parser_read('USE_TRIP',Use_trip,.False.)
    If (Use_trip) Then
       call parser_read('Trip_location',trip_loc)
       call parser_read('Trip_width',trip_width)
       call parser_read('Trip_height',trip_height)
       call parser_read('USE_GRIT',use_grit,.FALSE.)
       call parser_read('GRIT_DIAMETER',grit_size,0.0_WP)
       call parser_read('NUMBER_OF_PARTICLES',NGRIT,0)

       ! Account for grit size
       I2=1
       If (use_grit) trip_height=trip_height-grit_size

       ! Blank out trip-strip location
       Do I = 1, ND(1,1)-1
          If (X(1,I,1,1,1).LT.trip_loc) I2=I+1
       End Do
       Do I = ND(1,1),2,-1
          If (X(1,I,1,1,1).GT.trip_loc+trip_width) I3=I-1
       End Do
       Do J = 1, ND(1,2)
          If (X(1,1,J,1,2).LE.trip_height) J2=J
       End Do
       IBLANK(1,I2:I3,1:J2,:)=0

       Write (*,'(A)') ''
       print *, 'Trip I2:',I2
       print *, 'Trip I3:',I3
       print *, 'Trip J2:',J2
       Write (*,'(A)') ''
  
       ! Add grit
       If (use_grit) then

          ! Standard deviation
          sig = 1.5_WP * grit_size

          ! Loop through number of particles
          Do N = 1,NGRIT

             ! Compute amplitude
             Call random_number(rnd)
             AMP =  grit_size * (1.0D0 + 0.1D0 * (rnd-0.5D0))

             ! Get a random location
             Call random_number(rnd)
             x0 = trip_loc + 4.0_WP*sig + (trip_width - 8.0D0*sig) * rnd
             Call random_number(rnd)
             z0 = (Lz - dz) * rnd
             If (NDIM.eq.2) z0 = 0.0D0

             ! Modify the grid
             Do K = 1, ND(1,3)
                Do I = I2, I3

                   ! Check if this location was already modified
                   If (X(1,I,J2+1,K,2) .LT. 1.01D0*X(1,1,J2+1,1,2)) Then
                      
                      ! Represent grit as Gaussian
                      GAUSS = AMP * Exp(-((X(1,I,J2+1,K,1)-x0)**2/(2.0D0*sig**2)+(X(1,I,J2+1,K,3)-z0)**2/(2.0D0*sig**2)))

                      ! Account for periodicity in Z
                      z12 = Lz - abs(X(1,I,J2+1,K,3) - z0)
                      If (NDIM.eq.3) GAUSS = GAUSS + &
                           AMP * Exp(-((X(1,I,J2+1,K,1)-x0)**2/(2.0D0*sig**2)+z12**2/(2.0D0*sig**2)))

                      X(1,I,J2+1,K,2) = X(1,I,J2+1,K,2) + GAUSS
                   
                      ! Shift cells above (smoothly)
                      Do J=J2+2,ND(1,2)
                         ! Initial delta
                         delta0 = X(1,1,J,1,2) - X(1,1,J-1,1,2)

                         ! Delta after displacement
                         delta = X(1,I,J-1,K,2) + delta0 - X(1,1,J,1,2)

                         ! Adjust grid
                         X(1,I,J,K,2) = X(1,I,J-1,K,2) + delta0 - 0.02D0*delta

                     End Do
                      
                   End If
                End Do
             End Do
          End Do
          ! Make sure first and last 2 rows of cells on trip strip are not deformed (for stability)
          X(1,I2:I2+1,J2+1,:,2) = X(1,1:2,J2+1,:,2)
          X(1,I3-1:I3,J2+1,:,2) = X(1,1:2,J2+1,:,2)
       End If

    End If

    ! Find jet axis on Grid 1
    If (.not.Use_trip) I3=1
    Do I=I3,ND(1,1)
       If (X(1,I,1,1,1)<=XJet) IJet=I
    End Do
    Do K=1,ND(1,3)
       If (X(1,1,1,K,3)<=0.5_WP*Lz) KJet=K
    End Do

    ! Blank out Grid 1 region occupied by jet
    NS_O = 67 ! Make sure this is an odd number
    IBlank(1,IJet-(NS_O-1)/2:IJet+(NS_O-1)/2,:,KJet-(NS_O-1)/6:KJet+(NS_O-1)/6) = 0

    Print *, 'Grid 1 generation complete'
    Write (*,'(A)') ''

    ! ------------------------
    ! CREATE JET GRID (GRID 2)

    ! Cylinder (below grid 1)
    Do K = 1, ND(2,3)
       theta = DBLE(K-1)/DBLE(ND(2,3)-1)*twoPi
       JJ = NY23
       Do J = 1, NY23
          Do I = 1, NR2

             ! Radius
             r = DBLE(I-1)/DBLE(NR2-1)*(0.5_WP*ODJET-rpole) + rpole

             If (k==1 .or. k==ND(2,3)) Then

                ! Create X
                X(2,I,J,K,1) = r + XJET

                ! Create Y (mirror grid 1)
                X(2,I,J,K,2) = -X(1,1,JJ,1,2)

                ! Create Z
                X(2,I,J,K,3) = 0.5_WP*Lz

             Else

                ! Create X
                X(2,I,J,K,1) = r*Cos(theta) + XJET

                ! Create Y (mirror grid 1)
                X(2,I,J,K,2) = -X(1,1,JJ,1,2)

                ! Create Z
                X(2,I,J,K,3) = r*Sin(theta) + 0.5_WP*Lz

             End If

          End Do
          JJ=JJ-1
       End Do
    End Do

    ! Create collar (stretched)
    Dcollar =  X(1,I,1,ND(1,3)-8,3) ! ... Extent of cylindrical grid is 8 grid points in from z face of GRID1
    if (.not. use_stretching) dr = 0.5_WP*Dcollar / real(ND(2,1), WP)
    Call GeometricStretchFactor(0.5_WP*ODJet+10.0_WP*dr,0.5_WP*Dcollar,ND(2,1)-NR2+1-10,dr,1.05_8,5D-1,alpha)
    Do K = 1, ND(2,3)
       theta = DBLE(K-1)/DBLE(ND(2,3)-1)*twoPi
       JJ=NY23
       Do J = 1, NY23
          Do I = NR2+1, ND(2,1)

             ! Stretch radius
             If (I.LE.NR2+10) Then
                r = 0.5_WP*ODJet + dr*DBLE(I-NR2) ! ... uniform stretching in r near jet exit (for stability)
             Else
                r = 0.5_WP*ODJet + 10.0_WP*dr + (alpha**(I-NR2-10)-1.0_WP)/(alpha-1.0_WP)*dr
             End If

             If (k==1 .or. k==ND(2,3)) Then

                ! Create X
                X(2,I,J,K,1) = r + XJET

                ! Create Y (mirror grid 1)
                X(2,I,J,K,2) = -X(1,1,JJ,1,2)

                ! Create Z
                X(2,I,J,K,3) = 0.5_WP*Lz

             Else

                ! Create X
                X(2,I,J,K,1) = r*Cos(theta) + XJET

                ! Create Y (mirror grid 1)
                X(2,I,J,K,2) = -X(1,1,JJ,1,2)

                ! Create Z
                X(2,I,J,K,3) = r*Sin(theta) + 0.5_WP*Lz

             End If

          End Do
          JJ=JJ-1
       End Do
    End Do

    ! Add axial overlap region
    Do K = 1, ND(2,3)
       theta = DBLE(K-1)/DBLE(ND(2,3)-1)*twoPi
       JJ = 2
       Do J = NY23+1, ND(2,2)
          Do I = 1, NR2

             ! Radius
             r = DBLE(I-1)/DBLE(NR2-1)*(0.5_WP*ODJET-rpole) + rpole

             If (k==1 .or. k==ND(2,3)) Then

                ! Create X
                X(2,I,J,K,1) = r + XJET

                ! Create Y
                X(2,I,J,K,2) = X(1,1,JJ,1,2)

                ! Create Z
                X(2,I,J,K,3) = 0.5_WP*Lz

             Else

                ! Create X
                X(2,I,J,K,1) = r*Cos(theta) + XJET

                ! Create Y
                X(2,I,J,K,2) = X(1,1,JJ,1,2)

                ! Create Z
                X(2,I,J,K,3) = r*Sin(theta) + 0.5_WP*Lz

             End If

          End Do
          JJ = JJ+1
       End Do
    End Do

    ! Add axial overlap region for the collar
    Do K = 1, ND(2,3)
       theta = DBLE(K-1)/DBLE(ND(2,3)-1)*twoPi
       JJ = 2
       Do J = NY23+1, ND(2,2)
          Do I = NR2+1, ND(2,1)

             ! Stretch radius
             If (I.LE.NR2+10) Then
                r = 0.5_WP*ODJet + dr*DBLE(I-NR2)
             Else
                r = 0.5_WP*ODJet + 10.0_WP*dr + (alpha**(I-NR2-10)-1.0_WP)/(alpha-1.0_WP)*dr
             End If

             If (k==1 .or. k==ND(2,3)) Then

                ! Create X
                X(2,I,J,K,1) = r + XJET

                ! Create Y
                X(2,I,J,K,2) = X(1,1,JJ,1,2)

                ! Create Z
                X(2,I,J,K,3) = 0.5_WP*Lz

             Else

                ! Create X
                X(2,I,J,K,1) = r*Cos(theta) + XJET

                ! Create Y
                X(2,I,J,K,2) = X(1,1,JJ,1,2)

                ! Create Z
                X(2,I,J,K,3) = r*Sin(theta) + 0.5_WP*Lz

             End If

          End Do
          JJ = JJ+1
       End Do
    End Do

    ! Blank out collar region on Grid 2
    IBLANK(2,NR2+1:ND(2,1),1:NY23-1,:) = 0

    ! Blank out electrode tip on Grid 2
    If (Use_Electrode) Then
       ! Find indeces defining extents of the electrode
       Do I = 1, NR2
          r = DBLE(I-1)/DBLE(NR2-1)*(0.5_WP*ODJET-rpole) + rpole
          If (r.LE.0.5_WP*IDJET) EI = I
       End Do
       Do J = 1, NY23
          If (X(2,1,J,1,2).LE.-EDEPTH) EJ = J
       End Do
       IBLANK(2,EI+1:ND(2,1),1:EJ-1,:) = 0
    End If

    ! Blank out pole region
    NO_pole=7
    IBLANK(2,1:NO_pole,:,:) = 0

    Print *, 'Grid 2 generation complete'
    Write (*,'(A)') ''

    ! -------------------------------
    ! CREATE INNER RECTANGLE (GRID 3)

    ! Below grid 1
    Do K = 1, ND(3,3)
       JJ=NY23
       Do J = 1, NY23
          Do I = 1, ND(3,1)
             ! Create X
             X(3,I,J,K,1) = DBLE(I-1)/DBLE(ND(3,1)-1)*0.4_WP*ODJET + XJET - 0.2_WP*ODJET

             ! Create Y (mirror grid 1)
             X(3,I,J,K,2) = -X(1,1,JJ,1,2)

             ! Create Z
             X(3,I,J,K,3) = DBLE(K-1)/DBLE(ND(3,3)-1)*0.4_WP*ODJET + 0.5_WP*Lz - 0.2_WP*ODJET
          End Do
          JJ = JJ-1
       End Do
    End Do

    ! Inner rectangle (overlap)
    Do K = 1, ND(3,3)
       JJ = 2
       DO J = NY23+1, ND(3,2)
          Do I = 1, ND(3,1)
             ! Create X
             X(3,I,J,K,1) = DBLE(I-1)/DBLE(ND(3,1)-1)*0.4_WP*ODJET + XJET - 0.2_WP*ODJET

             ! Create Y
             X(3,I,J,K,2) = X(1,1,JJ,K,2)

             If (ND(3,3) .GT. 1) Then
                ! Create Z
                X(3,I,J,K,3) = DBLE(K-1)/DBLE(ND(3,3)-1)*0.4_WP*ODJET + 0.5_WP*Lz - 0.2_WP*ODJET
             Else
                X(3,I,J,K,3) = 0.0_WP
             End If
          End Do
          JJ = JJ+1
       End Do
    End Do

    Print *, 'Grid 3 generation complete'
    Write (*,'(A)') ''

    ! ----------------------------
    ! CREATE ROUNDED CORNER GRID 4

    ! Basically a torus that straightens out at the ends...
    If (Round_Corners) Then
       IC1=1; IC2=Int(ND(4,1)/4); IC3=Int(3*ND(4,1)/4); IC4=ND(4,1)
       Allocate(rbuf1(ND(4,1),ND(4,2)),rbuf2(ND(4,1),ND(4,2)))
       Allocate(pbuf1(ND(4,1),ND(4,2)),pbuf2(ND(4,1),ND(4,2)))
       Do K = 1, ND(4,3)
          ! Define azimuthal angle
          theta = DBLE(K-1)/DBLE(ND(4,3)-1)*twoPi
          Do J = 1, ND(4,2)
             ! First create the torus
             Do I = IC2, IC3

                ! Define radius and angle of the torus
                r = DBLE(J-1)/DBLE(ND(4,2)-1)*R0 + R0
                phi = DBLE(Int(I-IC2))/DBLE(Int(IC3-IC2))*0.5_WP*Pi

                ! Create X
                X(4,I,J,K,1) = (0.5_WP*ODJET+R0-r*cos(phi))*cos(theta) + XJET

                ! Create Y
                X(4,I,J,K,2) = r*sin(phi) - R0

                ! Create Z
                X(4,I,J,K,3) = (0.5_WP*ODJET+R0-r*cos(phi))*sin(theta) + 0.5_WP*Lz
             End Do
             ! Now straighten the right side
             Do I=IC3+1, IC4
                ! Store points at theta=0 plane
                If (K==1) Then
                   ! Set x and y points
                   X(4,I,J,K,1) = X(4,I-1,J,K,1) + (X(4,I-1,J,K,1)-X(4,I-2,J,K,1))
                   X(4,I,J,K,2) = X(4,I-1,J,K,2)

                   ! Save r and phi for later
                   r = sqrt((X(4,I,J,K,1)-XJET-0.5_WP*ODJET-R0)**2 + (X(4,I,J,K,2)+R0)**2)
                   rbuf1(I,J) = r
                   phi = 0.5_WP*Pi + acos((X(4,I,J,K,2)+R0)/r)
                   pbuf1(I,J) = phi
                   X(4,I,J,K,3) = (0.5_WP*ODJET+R0-r*cos(phi))*sin(theta) + 0.5_WP*Lz
                Else
                   r = rbuf1(I,J)
                   phi = pbuf1(I,J)
                   X(4,I,J,K,1) = (0.5_WP*ODJET+R0-r*cos(phi))*cos(theta) + XJET
                   X(4,I,J,K,2) = r*sin(phi) - R0
                   X(4,I,J,K,3) = (0.5_WP*ODJET+R0-r*cos(phi))*sin(theta) + 0.5_WP*Lz
                End If
             End Do
             ! Now straighten the bottom
             Do I=IC2-1,IC1,-1
                ! Store points at theta=0 plane
                If (K==1) Then
                   ! Set x and y points
                   X(4,I,J,K,1) = X(4,I+1,J,K,1)
                   X(4,I,J,K,2) = X(4,I+1,J,K,2) - (X(4,I+2,J,K,2)-X(4,I+1,J,K,2))

                   ! Save r and phi for later
                   r = sqrt((X(4,I,J,K,1)-XJET-0.5_WP*ODJET-R0)**2 + (X(4,I,J,K,2)+R0)**2)
                   rbuf2(I,J) = r
                   phi = asin((X(4,I,J,K,2)+R0)/r)
                   pbuf2(I,J) = phi
                   X(4,I,J,K,3) = (0.5_WP*ODJET+R0-r*cos(phi))*sin(theta) + 0.5_WP*Lz
                Else
                   r = rbuf2(I,J)
                   phi = pbuf2(I,J)
                   X(4,I,J,K,1) = (0.5_WP*ODJET+R0-r*cos(phi))*cos(theta) + XJET
                   X(4,I,J,K,2) = r*sin(phi) - R0
                   X(4,I,J,K,3) = (0.5_WP*ODJET+R0-r*cos(phi))*sin(theta) + 0.5_WP*Lz
                End If
             End Do
          End Do
       End Do

       Print *, 'Grid 4 generation complete'
       Write (*,'(A)') ''

       ! Determine number of overlapping points between Grid 2 and Grid 4
       Do I=NR2,ND(2,1)
          If (X(2,I,NY23,1,1).LE.XJET+0.5_WP*ODJet+R0) NR24x=I
       End Do
       Do J=NY23,1,-1
          If (X(2,NR2,J,1,2).GE.-R0) NR24y=J
       End Do
       NR24x = NR24x+1; NR24y = NR24y-1
       Print *, 'NR24x=',NR24x
       Print *, 'NR24y=',NR24y
       Write (*,'(A)') ''
    End If

    ! ----------------------------------------------
    ! Set the IBLANK for interpolation for each grid
    IBlank(1,IJet-(NS_O-1)/2-NBFringe:IJet-(NS_O-1)/2-1,2:ND(1,2),KJet-(NS_O-1)/6-NBFringe:KJet+(NS_O-1)/6+NBFringe) = -2
    IBlank(1,IJet+(NS_O-1)/2+1:IJet+(NS_O-1)/2+NBFringe,2:ND(1,2),KJet-(NS_O-1)/6-NBFringe:KJet+(NS_O-1)/6+NBFringe) = -2
    IBlank(1,IJet-(NS_O-1)/2-NBFringe:IJet+(NS_O-1)/2+NBFringe,2:ND(1,2),KJet-(NS_O-1)/6-NBFringe:KJet-(NS_O-1)/6-1) = -2
    IBlank(1,IJet-(NS_O-1)/2-NBFringe:IJet+(NS_O-1)/2+NBFringe,2:ND(1,2),KJet+(NS_O-1)/6+1:KJet+(NS_O-1)/6+NBFringe) = -2
    IBLANK(2,ND(2,1)-NBFringe+1:ND(2,1),NY23+1:ND(2,2),:) = -1
    IBLANK(2,NO_pole+1:NO_pole+NBFringe,:,:) = -3
    If (Round_Corners) Then
       IBLANK(2,NR2-NBFringe+1:NR2,NR24y:NY23,:) = -4
       IBLANK(2,NR2-1:NR24x,NY23:NY23+NBFringe-1,:) = -4
    End If
    IBLANK(3,1:NBFringe,:,:) = -2
    IBLANK(3,:,:,1:NBFringe) = -2
    IBLANK(3,ND(3,1)-NBFringe+1:ND(3,1),:,:) = -2
    IBLANK(3,:,:,ND(3,3)-NBFringe+1:ND(3,3)) = -2
    If (Round_Corners) Then
       IBLANK(4,:,ND(4,2)-NBFringe+1:ND(4,2),:) = -2
       IBLANK(4,1:NBFringe,2:ND(4,2),:) = -2
       IBLANK(4,ND(4,1)-NBFringe+1:ND(4,1),2:ND(4,2),:) = -2
    End If

    ! Write the grid file
    grid_file(1)="grid.xyz"
    Write (*,'(A)') ''
    Call Write_Grid(3,ngrid,ND,X,IBLANK, &
         prec(1),grid_format(1),volume_format(1),ib(1),grid_file(1))
    Write (*,'(A)') ''

    Return
  End Subroutine Jet_Crossflow_Grid

  
  ! ================================ !
  ! Two-point boundary value problem !
  ! for optimal grid stretching      !
  ! ================================ !
  Subroutine GeometricStretchFactor(x1,xn,n,dx,guess,safety_fac,alpha)
    Implicit None

      ! ... input data
      Real(KIND=8) :: x1,xn,dx
      Integer :: n
      Real(KIND=8) :: guess, safety_fac

      ! ... output data
      Real(KIND=8) :: alpha

      ! ... local variables
      Integer :: i
      Real(KIND=8) :: alpha_old, alpha_new, f_old, f_new, err, slope
      Real(KIND=8), Parameter :: err_tol = 1D-12

      ! ... use secant method to solve the equation dx = (alpha - 1)/(alpha^{n-1}-1)
      alpha_old = guess
      alpha_new = 1.01_8 * guess

      f_old = (xn-x1) - (alpha_old**(n-1) - 1.0_8) / (alpha_old-1.0_8) * dx
      f_new = (xn-x1) - (alpha_new**(n-1) - 1.0_8) / (alpha_new-1.0_8) * dx
      err = dabs(f_new)

      ! ... error check that we got it right by guess
      If (err < err_tol) Then
         alpha = alpha_new
         Return
      End If

      ! ... iterate
      Do While (err > err_tol)

         ! ... secant line slope
         If (dabs(alpha_new - alpha_old) < err_tol) Then
            alpha = alpha_new
            Return
         End If
         slope = (f_new-f_old)/(alpha_new-alpha_old)

         ! ... save
         alpha_old = alpha_new
         f_old = f_new

         ! ... estimate new
         If (dabs(slope) >= err_tol) Then
            alpha_new = alpha_new - safety_fac * f_new / slope
         Else
            alpha = alpha_new
            Return
         End If

         ! ... evaluate new
         f_new = (xn-x1) - (alpha_new**(n-1) - 1.0_8) / (alpha_new-1.0_8) * dx
         err = dabs(f_new)

      End Do

      alpha = alpha_new
      Return
      
    End Subroutine GeometricStretchFactor


    ! ============================= !
    ! Initial (target) solution Aux !
    ! ============================= !
    Subroutine Jet_Crossflow_Aux_Q
      USE ModParser
      USE ModPLOT3D_IO
      Implicit None

      Integer :: I, J, K, L, ng
      Real(WP), Dimension(:,:,:,:), Pointer :: Fuel, Air, Radical, Water
      Real(WP), parameter :: nitrogenOxygenMoleRatio = 3.76_WP
      real(WP), parameter :: m_wO=15.9994_WP, m_wH=1.00794_WP, m_wN=14.00674_WP
      Real(WP), parameter :: YO = 1.0_WP/(1.0_WP+m_wN/m_wO*nitrogenOxygenMoleRatio)
      Real(WP), Pointer :: Mwcoeff(:)
      Real(WP) :: MwRef
      Integer :: N2
      Integer, parameter :: H2  = 1
      Integer, parameter :: O2  = 2
      Integer, parameter :: R   = 3
      Integer, parameter :: H2O = 4

      ! Allocate density
      Allocate(RHO(ngrid,MAXVAL(ND(:,1)),MAXVAL(ND(:,2)),MAXVAL(ND(:,3))))

      ! Read reference parameters from input
      call parser_read('NUMBER_OF_SCALARS',nspecies,0)

      ! Return if no species
      If (nspecies.EQ.0) Then
        RHO = 1.0_WP
        print *, 'CONSTANT RHO'
        Return
      End If

      ! Nitrogen
      N2 = nspecies+1

      ! Allocate auxilary array
      Allocate(Qaux(ngrid,MAXVAL(ND(:,1)),MAXVAL(ND(:,2)),MAXVAL(ND(:,3)),nspecies))

      ! Link the pointers
      Fuel =>  Qaux(:,:,:,:,H2)
      Air  =>  Qaux(:,:,:,:,O2)
      If (nspecies.GE.3) Radical => Qaux(:,:,:,:,R)
      If (nspecies.GE.4) Water => Qaux(:,:,:,:,H2O)

      ! Molecular weights
      Allocate(Mwcoeff(nspecies+1))
      Mwcoeff = 0d0
      Mwcoeff(H2) = 2.0_WP*m_wH
      Mwcoeff(O2) = 2.0_WP*m_wO
      If (nspecies.GE.3) Mwcoeff(R) = m_wH
      If (nspecies.GE.4) Mwcoeff(H2O) = 2.0_WP*m_wH+m_wO
      Mwcoeff(N2) = 2.0_WP*m_wN

      ! Transform into the Mw^-1
      Mwcoeff = 1.0_WP/Mwcoeff

      ! Inverse of the reference Mw
      MwRef = YO*Mwcoeff(O2) + (1.0_WP-YO)*Mwcoeff(N2)

      ! Divide Mw by MwRef
      Mwcoeff = Mwcoeff/MwRef

      ! Remove the nitrogen
      Mwcoeff(1:nspecies) = Mwcoeff(1:nspecies) - Mwcoeff(N2)

      ! Initialize fuel and radicals on all grids
      Fuel = 0.0_WP
      If (nspecies.GE.3) Radical = 0.0_WP
      If (nspecies.GE.4) Water = 0.0_WP


      ! -------------------------
      ! Grid 2

      ! Assign value (linear decay)
      Do K=1,ND(2,3)
         Do I = 1, NR2
            Do J=1, JP
               Fuel(2,I,J,K) = 1.0_WP
               If (J.GT.JP/2) Then
                  Fuel(2,I,J,K) = Min(Max(1.0_WP - (DBLE(J)-DBLE(JP/2))/(DBLE(JP)-DBLE(JP/2)) , 0.0_WP),1.0_WP)
               End If
            End Do
         End Do
      End Do


      ! -------------------------
      ! Grid 3

      ! Assign value (linear decay)
      Do K = 1, ND(3,3)
         Do I = 1, ND(3,1)
            Do J=1, JP
               Fuel(3,I,J,K) = 1.0_WP
                If (J.GT.JP/2) Then
                  Fuel(3,I,J,K) = Min(Max(1.0_WP - (DBLE(J)-DBLE(JP/2))/(DBLE(JP)-DBLE(JP/2)), 0.0_WP),1.0_WP)
               End If
            End Do
         End Do
      End Do

      ! Air fraction
      do ng=1,ngrid
         do K = 1,maxval(ND(:,3))
            do J = 1,maxval(ND(:,2))
               do I = 1,maxval(ND(:,1))
                  Air(ng,I,J,K) = 1.0_WP - Fuel(ng,I,J,K)
               end do
            end do
         end do
      end do

      ! Change Air into Oxygen
      Air = Air*YO

      ! Get Density assuming isobaric isothermal gas

      do ng=1,ngrid
         do K = 1,maxval(ND(:,3))
            do J = 1,maxval(ND(:,2))
               do I = 1,maxval(ND(:,1))
                  Rho(ng,I,J,K) = Mwcoeff(N2)
               end do
            end do
         end do
      end do
      ! Rho = Mwcoeff(N2) 

      Do ng=1,ngrid
         Do K=1,ND(ng,3)
            Do J=1,ND(ng,2)
               Do I=1,ND(ng,1)
                  Do L=1, nspecies
                     Rho(ng,I,J,K) = Rho(ng,I,J,K) + Mwcoeff(L)*Qaux(ng,I,J,K,L)
                  End Do
               End Do
            End Do
         End Do
      End Do
      Rho = 1.0_WP/Rho

      ! Change mass fractions into mass densities
      Do L=1, nspecies
         do ng=1,ngrid
            do K = 1,maxval(ND(:,3))
               do J = 1,maxval(ND(:,2))
                  do I = 1,maxval(ND(:,1))
                     Qaux(ng,I,J,K,L) = Qaux(ng,I,J,K,L)*Rho(ng,I,J,K)
                  end do
               end do
            end do
         end do          
      End Do

      ! Write the solution file
      sol_file(1)="RocFlo-CM.00000000.target.aux.q"
      Call Write_Func(ngrid,ND,Qaux,sol_file(1))
      Write (*,'(A)') ''

      Return
    End Subroutine Jet_Crossflow_Aux_Q


  ! ========================= !
  ! Initial (target) solution !
  ! ========================= !
    Subroutine Jet_Crossflow_Q
    Implicit None

    integer :: I, J, K, ng
    Real(WP), Dimension(:,:,:,:), Pointer :: rhoU, rhoV, rhoW, rhoE
    Real(WP) :: U0, rho0, mu0, L0, gamma, C, Re, Pr, TAU(4), P0
    Real(WP) :: UJET, r

    ! Solution to Blasius boundary layer
    Integer :: kk
    Real(WP) :: blasius0,blasius1, delta, eta, xx, yy
    Real(WP) :: f2l,f2r,f0l,f0r
    Real(WP), dimension(0:9) :: by0 = (/ &
         0.0_8, 0.165571818583440_8, 0.650024518764203_8, 1.39680822972500_8, &
         2.30574664618049_8, 3.28327391871370_8, 4.27962110517696_8, &
         5.27923901129384_8, 6.27921363832835_8, 7.27921257797747_8 /)
    Real(WP), dimension(0:9) :: by1 = (/ &
         0.0_8, 0.329780063306651_8, 0.629765721178679_8, 0.84604458266019_8, &
         0.95551827831671_8, 0.99154183259084_8, 0.99897290050990_8, &
         0.9999216098795_8, 0.99999627301467_8, 0.99999989265063_8 /)
    Real(WP), dimension(0:9) :: by2 = (/ &
         0.332057384255589_8, 0.323007152241930_8, 0.266751564401387_8, 0.161360240845588_8, &
         0.06423404047594_8, 0.01590689966410_8, 0.00240199722109_8, &
         0.00022016340923_8, 0.00001224984692_8, 0.00000041090325_8 /)

    ! Read reference parameters from input
    call parser_read('REYNOLDS_NUMBER',Re,0.0_WP)
    call parser_read('PRANDTL_NUMBER',Pr,0.72_WP)
    call parser_read('DENSITY_REFERENCE',rho0,1.0D0)
    call parser_read('GAMMA_REFERENCE',gamma,1.4_WP)
    call parser_read('SNDSPD_REFERENCE',C,347.0_WP)
    call parser_read('LENGTH_REFERENCE',L0)

    ! Read fluid properties from input
    call parser_read('CROSSFLOW_VELOCITY',U0)
    Call Parser_Read('JET_VELOCITY',UJET)

    ! Initialize the timing
    TAU(1) = 0.0_WP
    TAU(2) = 0.0_WP
    TAU(3) = Re
    TAU(4) = 0.0_WP

    ! Allocate solution array
    Allocate(Q(ngrid,MAXVAL(ND(:,1)),MAXVAL(ND(:,2)),MAXVAL(ND(:,3)),5))

    ! Assign the density
    
    do ng=1,ngrid
         do K = 1,maxval(ND(:,3))
            do J = 1,maxval(ND(:,2))
               do I = 1,maxval(ND(:,1))
                  Q(ng,I,J,K,1) = Rho(ng,I,J,K)
               end do
            end do
         end do
      end do
    !Q(:,:,:,:,1) = Rho

    ! Link the pointers
    rhoU  =>  Q(:,:,:,:,2)
    rhoV  =>  Q(:,:,:,:,3)
    rhoW  =>  Q(:,:,:,:,4)
    rhoE  =>  Q(:,:,:,:,5)
    
    ! Initialize the momentum
    rhoU = 0.0_WP
    rhoV = 0.0_WP
    rhoW = 0.0_WP

    ! -------------------------
    ! Grid 1

    ! Compute viscosity
    mu0 = rho0*C*L0/Re

    ! Initialize the velocity field (Blasius solution for laminar flat plate)
    Do J=2,ND(1,2)
       Do I=1,ND(1,1)
          ! Get the corrdinates (start slighty downstream)
          xx=X(1,I,J,1,1) + 0.0254_WP
          yy=X(1,I,J,1,2)

          ! Return the first derivative of the Blasius function
          delta=sqrt(mu0*xx/U0/rho0)
          eta=yy/delta
          if (eta.le.0.0_WP) then
             blasius0 = 0.0_WP
             blasius1 = 0.0_WP
          else if (eta.ge.9.0_WP) then
             blasius0 = by0(9) + (eta-9.0_WP)
             blasius1 = 1.0_WP
          else
             k = int(eta)

             f0l = by0(k)
             f0r = by0(k+1)
             f2l = by2(k)
             f2r = by2(k+1)
             blasius0 = &
                  1.0_WP/6.0_WP*f2l*(real(k+1,8)-eta)**3 + &
                  1.0_WP/6.0_WP*f2r*(eta-real(k,8))**3 + &
                  (f0l-1.0_WP/6.0_WP*f2l)*(real(k+1,8)-eta) + &
                  (f0r-1.0_WP/6.0_WP*f2r)*(eta-real(k,8))

             f0l = by1(k)
             f0r = by1(k+1)
             f2l = -0.5_WP*by0(k)*by2(k)
             f2r = -0.5_WP*by0(k+1)*by2(k+1)
             blasius1 = &
                  1.0_WP/6.0_WP*f2l*(real(k+1,8)-eta)**3 + &
                  1.0_WP/6.0_WP*f2r*(eta-real(k,8))**3 + &
                  (f0l-1.0_WP/6.0_WP*f2l)*(real(k+1,8)-eta) + &
                  (f0r-1.0_WP/6.0_WP*f2r)*(eta-real(k,8))
          end if
    
          ! Update the velocity
          rhoU(1,I,J,:) = U0*blasius1/C
          rhoV(1,I,J,:) = 0.5_WP*sqrt(mu0*U0/rho0/xx)*(eta*blasius1-blasius0)/C

       End Do
    End Do

    ! Reinforce zero-slip
    rhoU(1,:,1,:) = 0.0_WP
    rhoV(1,:,1,:) = 0.0_WP
    rhoW(1,:,1,:) = 0.0_WP

    ! Compute delta99 at leading edge of sandpaper
    If (use_trip) Then
       delta=0.0_WP
       Do J=2,ND(1,2)
          I = I2-1
          K = 1
          yy=X(1,I,J,K,2)
          If (rhoU(1,I,J,K).le.0.99_WP*U0/C) delta=yy
       End Do
       print *, 'delta99 @ leading edge=',delta
    End If

    ! Zero variables in iblank region
    If (Use_trip) Then
       rhoU(1,I2-1:I3+1,1:J2+2,:)=0.0_WP
       rhoV(1,I2-1:I3+1,1:J2+2,:)=0.0_WP
       rhoW(1,I2-1:I3+1,1:J2+2,:)=0.0_WP
    End If

    ! -------------------------
    ! Grid 2

    ! Assign velocity (decaying laminar Poiseuille flow)
    Do K=1,ND(2,3)
       Do I = 1, NR2
          r = Sqrt((X(2,I,1,K,1)-XJet)**2 + (X(2,I,1,K,3)-0.5_WP*Lz)**2)
          Do J=1, JP
             rhoV(2,I,J,K) = 2.0_WP * Ujet * (1.0_WP - (r/(0.5_WP*IDJET))**2) / C
             If (J.GT.JP/2) Then
                rhoV(2,I,J,K) = rhoV(2,I,J,K)*(DBLE(JP)-DBLE(J))/(DBLE(JP)-DBLE(JP/2))
             End If
             rhoV(2,I,J,K) = max(rhoV(2,I,J,K),0.0_WP)
          End Do
       End Do
    End Do

    ! Replicate Blasius boundary layer on grid 2
    Do K=1,ND(2,3)
       Do J=NY23+1,ND(2,2)
          Do I=1,ND(2,1)
             ! Get the corrdinates (start slighty downstream)
             xx=X(2,I,J,K,1) + 0.104_WP
             yy=X(2,I,J,K,2)

             ! Return the first derivative of the Blasius function
             delta=sqrt(mu0*xx/U0/rho0)
             eta=yy/delta
             if (eta.le.0.0_WP) then
                blasius0 = 0.0_WP
                blasius1 = 0.0_WP
             else if (eta.ge.9.0_WP) then
                blasius0 = by0(9) + (eta-9.0_WP)
                blasius1 = 1.0_WP
             else
                kk = int(eta)

                f0l = by0(kk)
                f0r = by0(kk+1)
                f2l = by2(kk)
                f2r = by2(kk+1)
                blasius0 = &
                     1.0_WP/6.0_WP*f2l*(real(kk+1,8)-eta)**3 + &
                     1.0_WP/6.0_WP*f2r*(eta-real(kk,8))**3 + &
                     (f0l-1.0_WP/6.0_WP*f2l)*(real(kk+1,8)-eta) + &
                     (f0r-1.0_WP/6.0_WP*f2r)*(eta-real(kk,8))

                f0l = by1(kk)
                f0r = by1(kk+1)
                f2l = -0.5_WP*by0(kk)*by2(kk)
                f2r = -0.5_WP*by0(kk+1)*by2(kk+1)
                blasius1 = &
                     1.0_WP/6.0_WP*f2l*(real(kk+1,8)-eta)**3 + &
                     1.0_WP/6.0_WP*f2r*(eta-real(kk,8))**3 + &
                     (f0l-1.0_WP/6.0_WP*f2l)*(real(kk+1,8)-eta) + &
                     (f0r-1.0_WP/6.0_WP*f2r)*(eta-real(kk,8))
             end if
    
             ! Update the velocity
             rhoU(2,I,J,K) = U0*blasius1/C
             rhoV(2,I,J,K) = 0.5_WP*sqrt(mu0*U0/rho0/xx)*(eta*blasius1-blasius0)/C

          End Do
       End Do
    End Do

    ! -------------------------
    ! Grid 3

    ! Assign velocity (decaying laminar Poiseuille flow)
    Do K = 1, ND(3,3)
       Do I = 1, ND(3,1)
          r = Sqrt((X(3,I,1,K,1)-XJet)**2 + (X(3,I,1,K,3)-0.5_WP*Lz)**2)
          Do J=1, JP
             rhoV(3,I,J,K) = 2.0_WP * Ujet * (1.0_WP - (r/(0.5_WP*IDJET))**2) / C
             If (J.GT.(JP/2)) Then
                rhoV(3,I,J,K) = rhoV(3,I,J,K)*(DBLE(JP)-DBLE(J))/((DBLE(JP)-DBLE(JP/2)))
             End If
             rhoV(3,I,J,K) = max(rhoV(3,I,J,K),0.0_WP)
          End Do
       End Do
    End Do

    ! Replicate Blasius boundary layer on grid 3
    Do K=1,ND(3,3)
       Do J=NY23+1,ND(3,2)
          Do I=1,ND(3,1)
             ! Get the corrdinates (start slighty downstream)
             xx=X(3,I,J,K,1) + 0.104_WP
             yy=X(3,I,J,K,2)

             ! Return the first derivative of the Blasius function
             delta=sqrt(mu0*xx/U0/rho0)
             eta=yy/delta
             if (eta.le.0.0_WP) then
                blasius0 = 0.0_WP
                blasius1 = 0.0_WP
             else if (eta.ge.9.0_WP) then
                blasius0 = by0(9) + (eta-9.0_WP)
                blasius1 = 1.0_WP
             else
                kk = int(eta)

                f0l = by0(kk)
                f0r = by0(kk+1)
                f2l = by2(kk)
                f2r = by2(kk+1)
                blasius0 = &
                     1.0_WP/6.0_WP*f2l*(real(kk+1,8)-eta)**3 + &
                     1.0_WP/6.0_WP*f2r*(eta-real(kk,8))**3 + &
                     (f0l-1.0_WP/6.0_WP*f2l)*(real(kk+1,8)-eta) + &
                     (f0r-1.0_WP/6.0_WP*f2r)*(eta-real(kk,8))

                f0l = by1(kk)
                f0r = by1(kk+1)
                f2l = -0.5_WP*by0(kk)*by2(kk)
                f2r = -0.5_WP*by0(kk+1)*by2(kk+1)
                blasius1 = &
                     1.0_WP/6.0_WP*f2l*(real(kk+1,8)-eta)**3 + &
                     1.0_WP/6.0_WP*f2r*(eta-real(kk,8))**3 + &
                     (f0l-1.0_WP/6.0_WP*f2l)*(real(kk+1,8)-eta) + &
                     (f0r-1.0_WP/6.0_WP*f2r)*(eta-real(kk,8))
             end if
    
             ! Update the velocity
             rhoU(3,I,J,K) = U0*blasius1/C
             rhoV(3,I,J,K) = 0.5_WP*sqrt(mu0*U0/rho0/xx)*(eta*blasius1-blasius0)/C

          End Do
       End Do
    End Do

    ! -------------------------
    ! Grid 4

    If (Round_Corners) Then

       ! Replicate Blasius boundary layer on grid 2
       Do K=1,ND(4,3)
          Do J=1,ND(4,2)
             Do I=1,ND(4,1)
                ! Get the corrdinates (start slighty downstream)
                xx=X(4,I,J,K,1) + 0.104_WP
                yy=X(4,I,J,K,2)

                If (yy.gt.0.0D0) Then

                   ! Return the first derivative of the Blasius function
                   delta=sqrt(mu0*xx/U0/rho0)
                   eta=yy/delta
                   if (eta.le.0.0_WP) then
                      blasius0 = 0.0_WP
                      blasius1 = 0.0_WP
                   else if (eta.ge.9.0_WP) then
                      blasius0 = by0(9) + (eta-9.0_WP)
                      blasius1 = 1.0_WP
                   else
                      kk = int(eta)

                      f0l = by0(kk)
                      f0r = by0(kk+1)
                      f2l = by2(kk)
                      f2r = by2(kk+1)
                      blasius0 = &
                           1.0_WP/6.0_WP*f2l*(real(kk+1,8)-eta)**3 + &
                           1.0_WP/6.0_WP*f2r*(eta-real(kk,8))**3 + &
                           (f0l-1.0_WP/6.0_WP*f2l)*(real(kk+1,8)-eta) + &
                           (f0r-1.0_WP/6.0_WP*f2r)*(eta-real(kk,8))
                      
                      f0l = by1(kk)
                      f0r = by1(kk+1)
                      f2l = -0.5_WP*by0(kk)*by2(kk)
                      f2r = -0.5_WP*by0(kk+1)*by2(kk+1)
                      blasius1 = &
                           1.0_WP/6.0_WP*f2l*(real(kk+1,8)-eta)**3 + &
                           1.0_WP/6.0_WP*f2r*(eta-real(kk,8))**3 + &
                           (f0l-1.0_WP/6.0_WP*f2l)*(real(kk+1,8)-eta) + &
                           (f0r-1.0_WP/6.0_WP*f2r)*(eta-real(kk,8))
                   end if
    
                   ! Update the velocity
                   rhoU(4,I,J,K) = U0*blasius1/C
                   rhoV(4,I,J,K) = 0.5_WP*sqrt(mu0*U0/rho0/xx)*(eta*blasius1-blasius0)/C
                End If

             End Do
          End Do
       End Do
     End If

     ! Revert to momentum 
     do ng=1,ngrid
         do K = 1,maxval(ND(:,3))
            do J = 1,maxval(ND(:,2))
               do I = 1,maxval(ND(:,1))
                  rhoU(ng,I,J,K) = rhoU(ng,I,J,K)*rho(ng,I,J,K)
                  rhoV(ng,I,J,K) = rhoV(ng,I,J,K)*rho(ng,I,J,K)
                  rhoW(ng,I,J,K) = rhoW(ng,I,J,K)*rho(ng,I,J,K)
               end do
            end do
         end do
      end do
     !rhoU = rhoU*rho
     !rhoV = rhoV*rho
     !rhoW = rhoW*rho

     ! Compute energy (ensure constant pressure for all grids)
     P0 = 1.0_WP / gamma ! ... non-dimensional ambient pressure

     do ng=1,ngrid
         do K = 1,maxval(ND(:,3))
            do J = 1,maxval(ND(:,2))
               do I = 1,maxval(ND(:,1))
                  rhoE(ng,I,J,K) = P0 / (gamma-1.0_WP) + 0.5_WP*(rhoU(ng,I,J,K)**2+rhoV(ng,I,J,K)**2+rhoW(ng,I,J,K)**2)/rho(ng,I,J,K)
               end do
            end do
         end do
      end do
     !rhoE = P0 / (gamma-1.0_WP) + 0.5_WP*(rhoU**2+rhoV**2+rhoW**2)/rho

    ! Write the solution file
    sol_file(1)="RocFlo-CM.00000000.target.q"
    Call Write_Soln(3,ngrid,ND,Q,TAU,prec(1),grid_format(1),volume_format(1),sol_file(1))
    Write (*,'(A)') ''

    Return
  End Subroutine Jet_Crossflow_Q


  ! =================== !
  ! Boundary conditions !
  ! =================== !
  Subroutine Jet_Crossflow_BC
Implicit None

    Integer :: NC,iunit

    ! Get sponge zone thickness
    Call parser_read('NCELL_SPONGE',NC)

    ! Open the file
    iunit=11
    open(iunit,file="bc.dat")

    print *, 'Writing boundary conditions'
    Write (*,'(A)') ''

    ! Write the header
    write(iunit,'(1a1 )') "#"
    write(iunit,'(1a45)') "# Basic boundary condition file for PlasComCM"
    write(iunit,'(1a1 )') "#"
    write(iunit,'(1a24)') "# FD = finite difference"
    write(iunit,'(1a20)') "# FV = finite volume"
    write(iunit,'(1a1 )') "#"
    write(iunit,'(1a8 )') "# ibType"
    write(iunit,'(1a8 )') "# ======"
    write(iunit,'(1a32)') "#     00     OUTER_BOUNDARY (FD)"
    write(iunit,'(1a41)') "#     21     SAT_SLIP_ADIABATIC_WALL (FD)"
    write(iunit,'(1a44)') "#     22     SAT_NOSLIP_ISOTHERMAL_WALL (FD)"
    write(iunit,'(1a33)') "#     23     SAT_NOSLIP_ADIABATIC_WALL (FD)"
    write(iunit,'(1a31)') "#     24     SAT_FAR_FIELD (FD)"
    write(iunit,'(1a37)') "#     25     SAT_BLOCK_INTERFACE (FD)"
    write(iunit,'(1a51)') "#     31     NSCBC_INFLOW_VELOCITY_TEMPERATURE (FD)"
    write(iunit,'(1a63)') "#     32     NSCBC_INFLOW_VELOCITY_TEMPERATURE_FROM_SPONGE (FD)"
    write(iunit,'(1a57)') "#     41     NSCBC_OUTFLOW_PERFECT_NONREFLECTION (FD, FV)"
    write(iunit,'(1a54)') "#     42     NSCBC_OUTFLOW_PERFECT_REFLECTION (FD, FV)"
    write(iunit,'(1a47)') "#     51     NSCBC_WALL_ADIABATIC_SLIP (FD, FV)"
    write(iunit,'(1a50)') "#     52     NSCBC_WALL_ISOTHERMAL_NOSLIP (FD, FV)"
    write(iunit,'(1a39)') "#     61     NSCBC_SUBSONIC_INFLOW (FD)"
    write(iunit,'(1a45)') "#     62     NSCBC_SUPERSONIC_INFLOW (FD, FV)"
    write(iunit,'(1a29)') "#     73     PERIODICITY (FV)"
    write(iunit,'(1a30)') "#     91     FV_DIRICHLET (FV)"
    write(iunit,'(1a28)') "#     92     FV_NEUMANN (FV)"
    write(iunit,'(1a29)') "#     93     FV_SYMMETRY (FV)"
    write(iunit,'(1a36)') "#     94     FV_BLOCK_INTERFACE (FV)"
    write(iunit,'(1a45)') "#     95     FV_BLOCK_INTERFACE_PERIODIC (FV)"
    write(iunit,'(1a28)') "#     99     SPONGE (FD, FV)"
    write(iunit,'(1a1 )') "#"
    write(iunit,'(1a56)') "# Grid ibType ibDir   is    ie    js    je     ks    ke "
    write(iunit,'(1a56)') "# ==== ====== =====  ====  ====  ====  ====   ====  ===="

    ! Grid 1 boundary conditions
    ! Inflow / outflow
    write(iunit,'(9I6)')      1,   24,     1,    1,     1,    1,   -1,    1,    -1
    write(iunit,'(9I6)')      1,   24,    -1,   -1,    -1,    1,   -1,    1,    -1
    write(iunit,'(9I6)')      1,   24,    -2,    1,    -1,   -1,   -1,    1,    -1
    ! No-slip wall
    If (use_trip) Then
       write(iunit,'(9I6)')      1,   22,     2,    1,  I2-1,    1,    1,    1,    -1
       write(iunit,'(9I6)')      1,   22,     2, I3+1,    -1,    1,    1,    1,    -1
       write(iunit,'(9I6)')      1,   22,     2, I2-1,  I3+1, J2+1, J2+1,    1,    -1
       write(iunit,'(9I6)')      1,   22,    -1, I2-1,  I2-1,    1, J2+1,    1,    -1
       write(iunit,'(9I6)')      1,   22,     1, I3+1,  I3+1,    1, J2+1,    1,    -1
    Else
       write(iunit,'(9I6)')      1,   22,     2,    1,    -1,    1,    1,    1,    -1
    End If
    ! Add spounge layers
    write(iunit,'(9I6)')      1,   99,     1,1         ,NC,    1,    -1,    1,    -1
    write(iunit,'(9I6)')      1,   99,    -1,ND(1,1)-NC,-1,    1,    -1,    1,    -1
    write(iunit,'(9I6)')      1,   99,    -2,    1,    -1,ND(1,2)-NC,-1,    1,    -1

    ! Grid 2 boundary conditions
    If (Use_Electrode) Then
       ! Inflow
       write(iunit,'(9I6)')      2,   24,     2,    1,     EI,    1,    1,    1,    -1
       ! No-slip
       write(iunit,'(9I6)')      2,   22,    -1,  NR2,   NR2,   EJ,  NY23,    1,    -1
       write(iunit,'(9I6)')      2,   22,    -1,   EI,    EI,    1,   EJ,     1,    -1
       write(iunit,'(9I6)')      2,   22,     2,   EI,    NR2,   EJ,   EJ,    1,    -1
       ! Add spounge layers
       write(iunit,'(9I6)')      2,   99,     2,    1,    EI,    1,    NC,    1,    -1
    Else
       ! Inflow
       write(iunit,'(9I6)')      2,   24,     2,    1,    NR2,    1,    1,    1,    -1
       ! No-slip
       write(iunit,'(9I6)')      2,   22,    -1,  NR2,   NR2,    1,   EJ,    1,    -1
       write(iunit,'(9I6)')      2,   22,     2,  NR2,ND(2,1),NY23,  NY23,    1,    -1
       ! Add spounge layers
       write(iunit,'(9I6)')      2,   99,     2,    1,    NR2,    1,    NC,    1,    -1
    End If
    If (Round_Corners) Then
       ! No-Slip
       write(iunit,'(9I6)')      2,   22,     2,  NR24x+1,-1,NY23,  NY23,    1,    -1
    Else
       write(iunit,'(9I6)')      2,   22,     2,  NR2,-1,NY23,  NY23,    1,    -1
    End If
    ! Outflow
    write(iunit,'(9I6)')      2,   24,    -2,    1,    -1,   -1,   -1,    1,    -1
    write(iunit,'(9I6)')      2,   99,    -2,    1,    -1,ND(2,2)-NC, -1, 1,     -1

    ! Grid 3 boundary conditions
    ! Inflow
    write(iunit,'(9I6)')      3,   24,     2,    1,    -1,    1,    1,    1,    -1
    write(iunit,'(9I6)')      3,   99,     2,    1,    -1,    1,    NC,    1,    -1

    ! Outflow
    write(iunit,'(9I6)')      3,   24,    -2,    1,    -1,   -1,   -1,    1,    -1
    write(iunit,'(9I6)')      3,   99,    -2,    1,    -1,ND(3,2)-NC, -1, 1,     -1

    ! Grid 4 boundary conditions
    If (Round_Corners) write(iunit,'(9I6)')      4,   22,    2,    1,   -1,   1,    1,    1,    -1

    ! Close the file
    close(iunit)

    Return
  End Subroutine Jet_Crossflow_BC


  ! ============================== !
  ! Generate interpolation data    !
  ! - Assumes IBlank contains info !
  !   of receiver points           !
  ! ============================== !
  Subroutine Jet_Crossflow_Interp
    Implicit None

    Integer :: ng, i, j, k, irecv, irecv_(ngrid), ng_, io
    Integer :: xintout_x_unit, xintout_ho_unit
    Integer :: donor_mesh, recv_mesh
    Character(LEN=80) :: INTERP_TYPE

    ! Only consider linear interpolation for now
    Call Parser_read('GRID_INTERP',INTERP_TYPE)
    Select Case (trim(INTERP_TYPE))
    Case ('LINEAR')
       id_in = 2
    Case ('CUBIC')
       id_in = 4
    Case Default
       Write (*,'(A)') ''
       Print *, 'UNKNOWN INTERPOLATION TYPE'
       Stop
    End Select

    ! Allocate space for PEGASUS data
    Allocate(iipnts(ngrid), ibpnts(ngrid), ibpnts_(ngrid,ngrid), iisptr(ngrid), iieptr(ngrid), ieng(ngrid), jeng(ngrid), keng(ngrid))

    ! Determine the global data
    ! Maximum number of grid points, taken over all grids
    igall = 1
    Do ng = 1, ngrid
      ieng(ng) = ND(ng,1)
      jeng(ng) = ND(ng,2)
      keng(ng) = ND(ng,3)
      igall = MAX(igall,PRODUCT(ND(ng,:)))
    End Do

    ! Expected maximum number of receiver points (donar_mesh,recv_mesh)
    ibpnts_(:,:) = 0
    Do ng = 1, ngrid
       Do K = 1, ND(ng,3)
          Do J = 1, ND(ng,2)
             Do I = 1, ND(ng,1)
                If (IBlank(ng,I,J,K)<0) Then
                   ng_ = -IBlank(ng,I,J,K)
                   ibpnts_(ng_,ng) = ibpnts_(ng_,ng)+1
                End If
             End Do
          End Do
       End Do
    End Do

    ! Total number of receiver points on each grid
    Do ng=1,ngrid
       ibpnts(ng) = sum(ibpnts_(:,ng))
    End Do
    ipbp = MAXVAL(ibpnts(:))

    Write (*,'(A)') ''
    print *, 'Number of receiver points on grid 1:',ibpnts(1)
    print *, 'Number of receiver points on grid 2:',ibpnts(2)
    print *, 'Number of receiver points on grid 3:',ibpnts(3)
    If (Round_Corners) print *, 'Number of receiver points on grid 4:',ibpnts(4)
    Write (*,'(A)') ''

    ! Expected number of donor cells
    Do ng=1,ngrid
       iipnts(ng) = sum(ibpnts_(ng,:))
    End Do
    ipip = MAXVAL(iipnts(:))
    ipall = SUM(iipnts(:))
    iisptr(1) = 1
    iieptr(1) = iipnts(1)
    iisptr(2) = iieptr(1) + 1
    iieptr(2) = iisptr(2) + iipnts(2)
    iisptr(3) = iieptr(2) + 1
    iieptr(3) = iisptr(3) + iipnts(3)
    iisptr(4) = iieptr(3) + 1
    iieptr(4) = ipall

    Write (*,'(A)') ''
    print *, 'Number of donor points on grid 1:',iipnts(1)
    print *, 'Number of donor points on grid 2:',iipnts(2)
    print *, 'Number of donor points on grid 3:',iipnts(3)
    If (Round_Corners)  print *, 'Number of donor points on grid 4:',iipnts(4)
    Write (*,'(A)') ''

    ! Allocate more space for PEGASUS data
    Allocate(ibt(ipbp,ngrid),jbt(ipbp,ngrid),kbt(ipbp,ngrid),ibct(ipbp,ngrid),donor(ipbp,ngrid))
    Allocate(iit(ipip,ngrid),jit(ipip,ngrid),kit(ipip,ngrid))
    Allocate(nit(ipip,ngrid),njt(ipip,ngrid),nkt(ipip,ngrid))
    Allocate(dxit(ipip,ngrid),dyit(ipip,ngrid),dzit(ipip,ngrid),coeffit(ipip,id_in,3,ngrid))
    dxit = 0.0_WP; dyit=0.0_WP; dzit=0.0_WP; coeffit=0.0_WP

    ! Store indices of receiver points for each grid
    ibt = 0; jbt = 0; kbt = 0
    donor = 0
    Do ng = 1, ngrid
       irecv = 0
       irecv_ = 0
 
!!$       Do I = 1, ND(ng,1)
!!$          Do J = 1, ND(ng,2)
!!$             Do K = 1, ND(ng,3)
!!$                If (IBlank(ng,I,J,K)<0) Then
!!$                   ! Local index of m-th receiver point on grid ng
!!$                   irecv = irecv+1
!!$                   ibt(irecv,ng) = I
!!$                   jbt(irecv,ng) = J
!!$                   kbt(irecv,ng) = K
!!$
!!$                   ! Keep track of who we are receiving from
!!$                   ng_ = -IBlank(ng,I,J,K)
!!$                   donor(irecv,ng) = ng_
!!$                   irecv_(ng_) = irecv_(ng_)+1
!!$                End If
!!$             End Do
!!$          End Do
!!$       End Do

       Do K = 1, ND(ng,3)
          Do J = 1, ND(ng,2)
             Do I = 1, ND(ng,1)
                If (IBlank(ng,I,J,K)==-2) Then
                   ! Local index of m-th receiver point on grid ng
                   irecv = irecv+1
                   ibt(irecv,ng) = I
                   jbt(irecv,ng) = J
                   kbt(irecv,ng) = K

                   ! Keep track of who we are receiving from
                   ng_ = -IBlank(ng,I,J,K)
                   donor(irecv,ng) = ng_
                   irecv_(ng_) = irecv_(ng_)+1
                End If
             End Do
          End Do
       End Do

       Do K = 1, ND(ng,3)
          Do J = 1, ND(ng,2)
             Do I = 1, ND(ng,1)
                If (IBlank(ng,I,J,K)==-1) Then
                   ! Local index of m-th receiver point on grid ng
                   irecv = irecv+1
                   ibt(irecv,ng) = I
                   jbt(irecv,ng) = J
                   kbt(irecv,ng) = K

                   ! Keep track of who we are receiving from
                   ng_ = -IBlank(ng,I,J,K)
                   donor(irecv,ng) = ng_
                   irecv_(ng_) = irecv_(ng_)+1
                End If
             End Do
          End Do
       End Do

       Do K = 1, ND(ng,3)
          Do J = 1, ND(ng,2)
             Do I = 1, ND(ng,1)
                If (IBlank(ng,I,J,K)==-3) Then
                   ! Local index of m-th receiver point on grid ng
                   irecv = irecv+1
                   ibt(irecv,ng) = I
                   jbt(irecv,ng) = J
                   kbt(irecv,ng) = K

                   ! Keep track of who we are receiving from
                   ng_ = -IBlank(ng,I,J,K)
                   donor(irecv,ng) = ng_
                   irecv_(ng_) = irecv_(ng_)+1
                End If
             End Do
          End Do
       End Do

       If (Round_Corners) Then
          Do K = 1, ND(ng,3)
             Do J = 1, ND(ng,2)
                Do I = 1, ND(ng,1)
                   If (IBlank(ng,I,J,K)==-4) Then
                      ! Local index of m-th receiver point on grid ng
                      irecv = irecv+1
                      ibt(irecv,ng) = I
                      jbt(irecv,ng) = J
                      kbt(irecv,ng) = K

                      ! Keep track of who we are receiving from
                      ng_ = -IBlank(ng,I,J,K)
                      donor(irecv,ng) = ng_
                      irecv_(ng_) = irecv_(ng_)+1
                   End If
                End Do
             End Do
          End Do
       End If

       ! Make sure this was done correctly
       If (irecv.ne.ibpnts(ng)) Then
          print *, 'ERROR: Number of receiver points does not correspond to IBlank values!'
          print *, 'Grid:',ng,irecv,ibpnts(ng)
          Stop
       End If
       Do ng_=1,ngrid
          If (irecv_(ng_).ne.ibpnts_(ng_,ng)) Then
             print *, 'ERROR: Number of local receiver points does not correspond to IBlank values!'
             print *, 'Grid:',ng_,irecv_(ng_),ibpnts_(ng_,ng)
             Stop
          End If
       End Do
    End Do

    ! Identify donor cells on grid 2 corresponding to grid 1 recv points
    donor_mesh = 2
    recv_mesh  = 1
    Call Find_Donor_Cell_2Dy(donor_mesh, recv_mesh, 1)

    ! Identify donor cells on grid 2 corresponding to grid 3 recv points
    donor_mesh = 2
    recv_mesh  = 3
    Call Find_Donor_Cell_2Dy(donor_mesh, recv_mesh, ibpnts_(2,1)+1)

    ! Identify donor cells on grid 2 corresponding to grid 4 recv points
    If (Round_Corners) Then
       donor_mesh = 2
       recv_mesh  = 4
       Call Find_Donor_Cell_2Dtheta(donor_mesh, recv_mesh, ibpnts_(2,1)+ibpnts_(2,3)+1)
    End If

    ! Identify donor cells on grid 1 corresponding to grid 2 recv points
    donor_mesh = 1
    recv_mesh  = 2
    Call Find_Donor_Cell_2Dy(donor_mesh, recv_mesh, 1)

    ! Identify donor cells on grid 3 corresponding to grid 2 recv points
    donor_mesh = 3
    recv_mesh  = 2
    Call Find_Donor_Cell_2Dy(donor_mesh, recv_mesh, 1)

    ! Identify donor cells on grid 4 corresponding to grid 2 recv points
    If (Round_Corners) Then
       donor_mesh = 4
       recv_mesh  = 2
       Call Find_Donor_Cell_2Dtheta(donor_mesh, recv_mesh, 1)
    End If

    Write (*,'(A)') ''
    Print *, 'Donor search complete'
    Write (*,'(A)') ''

    ! Print a bunch of stuff to the screen
    print *, 'Interpolation header stuff:'
    print *, 'ipall:',ipall
    print *, 'igall:',igall
    print *, 'ippip:',ipip
    print *, 'ipbp:',ipbp
    print *, 'Number of recv/donor points'
    print *, 'min/max iipnts:',minval(iipnts),maxval(iipnts)
    print *, 'min/max ibpnts:',minval(ibpnts),maxval(ibpnts)
    print *, 'local index of donor point'
    print *, 'min/max iit on grid 1:',minval(iit(1:iipnts(1),1)),maxval(iit(1:iipnts(1),1))
    print *, 'min/max iit on grid 2:',minval(iit(1:iipnts(2),2)),maxval(iit(1:iipnts(2),2))
    print *, 'min/max iit on grid 3:',minval(iit(1:iipnts(3),3)),maxval(iit(1:iipnts(3),3))
    If (Round_Corners) print *, 'min/max iit on grid 4:',minval(iit(1:iipnts(4),4)),maxval(iit(1:iipnts(4),4))
    print *, 'min/max jit on grid 1:',minval(jit(1:iipnts(1),1)),maxval(jit(1:iipnts(1),1))
    print *, 'min/max jit on grid 2:',minval(jit(1:iipnts(2),2)),maxval(jit(1:iipnts(2),2))
    print *, 'min/max jit on grid 3:',minval(jit(1:iipnts(3),3)),maxval(jit(1:iipnts(3),3))
    If (Round_Corners) print *, 'min/max jit on grid 4:',minval(jit(1:iipnts(4),4)),maxval(jit(1:iipnts(4),4))
    print *, 'min/max kit on grid 1:',minval(kit(1:iipnts(1),1)),maxval(kit(1:iipnts(1),1))
    print *, 'min/max kit on grid 2:',minval(kit(1:iipnts(2),2)),maxval(kit(1:iipnts(2),2))
    print *, 'min/max kit on grid 3:',minval(kit(1:iipnts(3),3)),maxval(kit(1:iipnts(3),3))
    If (Round_Corners) print *, 'min/max kit on grid 4:',minval(kit(1:iipnts(4),4)),maxval(kit(1:iipnts(4),4))
    print *, 'Relative distance between donor/recv point'
    print *, 'min/max dxit on grid 1:',minval(dxit(1:iipnts(1),1)),maxval(dxit(1:iipnts(1),1))
    print *, 'min/max dxit on grid 2:',minval(dxit(1:iipnts(2),2)),maxval(dxit(1:iipnts(2),2))
    print *, 'min/max dxit on grid 3:',minval(dxit(1:iipnts(3),3)),maxval(dxit(1:iipnts(3),3))
    If (Round_Corners) print *, 'min/max dxit on grid 4:',minval(dxit(1:iipnts(4),4)),maxval(dxit(1:iipnts(4),4))
    print *, 'min/max dyit on grid 1:',minval(dyit(1:iipnts(1),1)),maxval(dyit(1:iipnts(1),1))
    print *, 'min/max dyit on grid 2:',minval(dyit(1:iipnts(2),2)),maxval(dyit(1:iipnts(2),2))
    print *, 'min/max dyit on grid 3:',minval(dyit(1:iipnts(3),3)),maxval(dyit(1:iipnts(3),3))
    If (Round_Corners) print *, 'min/max dyit on grid 4:',minval(dyit(1:iipnts(4),4)),maxval(dyit(1:iipnts(4),4))
    print *, 'min/max dzit on grid 1:',minval(dzit(1:iipnts(1),1)),maxval(dzit(1:iipnts(1),1))
    print *, 'min/max dzit on grid 2:',minval(dzit(1:iipnts(2),2)),maxval(dzit(1:iipnts(2),2))
    print *, 'min/max dzit on grid 3:',minval(dzit(1:iipnts(3),3)),maxval(dzit(1:iipnts(3),3))
    If (Round_Corners) print *, 'min/max dzit on grid 4:',minval(dzit(1:iipnts(4),4)),maxval(dzit(1:iipnts(4),4))
    print *, 'Global index of donor point'
    print *, 'min/max ibct on grid 1:',minval(ibct(1:ibpnts(1),1)),maxval(ibct(1:ibpnts(1),1))
    print *, 'min/max ibct on grid 2:',minval(ibct(1:ibpnts(2),2)),maxval(ibct(1:ibpnts(2),2))
    print *, 'min/max ibct on grid 3:',minval(ibct(1:ibpnts(3),3)),maxval(ibct(1:ibpnts(3),3))
    If (Round_Corners) print *, 'min/max ibct on grid 4:',minval(ibct(1:ibpnts(4),4)),maxval(ibct(1:ibpnts(4),4))
    print *, 'Stencil size'
    print *, 'min/max nit on grid 1:',minval(nit(1:iipnts(1),1)),maxval(nit(1:iipnts(1),1))
    print *, 'min/max nit on grid 2:',minval(nit(1:iipnts(2),2)),maxval(nit(1:iipnts(2),2))
    print *, 'min/max nit on grid 3:',minval(nit(1:iipnts(3),3)),maxval(nit(1:iipnts(3),3))
    If (Round_Corners) print *, 'min/max nit on grid 4:',minval(nit(1:iipnts(4),4)),maxval(nit(1:iipnts(4),4))
    print *, 'min/max njt on grid 1:',minval(njt(1:iipnts(1),1)),maxval(njt(1:iipnts(1),1))
    print *, 'min/max njt on grid 2:',minval(njt(1:iipnts(2),2)),maxval(njt(1:iipnts(2),2))
    print *, 'min/max njt on grid 3:',minval(njt(1:iipnts(3),3)),maxval(njt(1:iipnts(3),3))
    If (Round_Corners) print *, 'min/max njt on grid 4:',minval(njt(1:iipnts(4),4)),maxval(njt(1:iipnts(4),4))
    print *, 'min/max nkt on grid 1:',minval(nkt(1:iipnts(1),1)),maxval(nkt(1:iipnts(1),1))
    print *, 'min/max nkt on grid 2:',minval(nkt(1:iipnts(2),2)),maxval(nkt(1:iipnts(2),2))
    print *, 'min/max nkt on grid 3:',minval(nkt(1:iipnts(3),3)),maxval(nkt(1:iipnts(3),3))
    If (Round_Corners) print *, 'min/max nkt on grid 4:',minval(nkt(1:iipnts(4),4)),maxval(nkt(1:iipnts(4),4))
    print *, 'Interp coeff'
    print *, 'min/max x coeff on grid1 :',minval(coeffit(1:iipnts(1),:,1,1)),maxval(coeffit(1:iipnts(1),:,1,1))
    print *, 'min/max y coeff on grid1 :',minval(coeffit(1:iipnts(1),:,2,1)),maxval(coeffit(1:iipnts(1),:,2,1))
    print *, 'min/max z coeff on grid1 :',minval(coeffit(1:iipnts(1),:,3,1)),maxval(coeffit(1:iipnts(1),:,3,1))
    print *, 'min/max x coeff on grid2 :',minval(coeffit(2:iipnts(2),:,1,2)),maxval(coeffit(2:iipnts(2),:,1,2))
    print *, 'min/max y coeff on grid2 :',minval(coeffit(2:iipnts(2),:,2,2)),maxval(coeffit(2:iipnts(2),:,2,2))
    print *, 'min/max z coeff on grid2 :',minval(coeffit(2:iipnts(2),:,3,2)),maxval(coeffit(2:iipnts(2),:,3,2))
    print *, 'min/max x coeff on grid3 :',minval(coeffit(3:iipnts(3),:,1,3)),maxval(coeffit(3:iipnts(3),:,1,3))
    print *, 'min/max y coeff on grid3 :',minval(coeffit(3:iipnts(3),:,2,3)),maxval(coeffit(3:iipnts(3),:,2,3))
    print *, 'min/max z coeff on grid3 :',minval(coeffit(3:iipnts(3),:,3,3)),maxval(coeffit(3:iipnts(3),:,3,3))
    If (Round_Corners) print *, 'min/max x coeff on grid4 :',minval(coeffit(4:iipnts(4),:,1,4)),maxval(coeffit(4:iipnts(4),:,1,4))
    If (Round_Corners) print *, 'min/max y coeff on grid4 :',minval(coeffit(4:iipnts(4),:,2,4)),maxval(coeffit(4:iipnts(4),:,2,4))
    If (Round_Corners) print *, 'min/max z coeff on grid4 :',minval(coeffit(4:iipnts(4),:,3,4)),maxval(coeffit(4:iipnts(4),:,3,4))

    ! Now write out the data
    xintout_x_unit = 10
    xintout_ho_unit = 11
    Open (xintout_ho_unit, file='XINTOUT.HO.2D', status='unknown', form='unformatted')
    Write (xintout_ho_unit) ngrid, ipall, igall, ipip, ipbp
    Do ng = 1, ngrid
       Write (xintout_ho_unit) ibpnts(ng), iipnts(ng), iieptr(ng), iisptr(ng), ieng(ng), jeng(ng), keng(ng)
       Write (xintout_ho_unit) ( iit(i,ng), i = 1, iipnts(ng)), &
                               ( jit(i,ng), i = 1, iipnts(ng)), &
                               ( kit(i,ng), i = 1, iipnts(ng)), &
                               (dxit(i,ng), i = 1, iipnts(ng)), &
                               (dyit(i,ng), i = 1, iipnts(ng)), &
                               (dzit(i,ng), i = 1, iipnts(ng))
       Write (xintout_ho_unit) ( ibt(i,ng), i = 1, ibpnts(ng)), &
                               ( jbt(i,ng), i = 1, ibpnts(ng)), &
                               ( kbt(i,ng), i = 1, ibpnts(ng)), &
                               (ibct(i,ng), i = 1, ibpnts(ng))
       !Write (xintout_ho_unit) (1, i = 1, ieng(ng)*jeng(ng)*keng(ng))
    End Do
    Close (xintout_ho_unit)

    Open (xintout_x_unit, file='XINTOUT.X.2D', status='unknown', form='unformatted')
    Write (xintout_x_unit) ngrid, ipall, igall, ipip, ipbp
    Do ng = 1, ngrid
      Write (xintout_x_unit) (nit(i,ng), i = 1, iipnts(ng)), &
                             (njt(i,ng), i = 1, iipnts(ng)), & 
                             (nkt(i,ng), i = 1, iipnts(ng))
      Write (xintout_x_unit) ((coeffit(i,io,1,ng), io = 1, nit(i,ng)), i = 1, iipnts(ng)), &
                             ((coeffit(i,io,2,ng), io = 1, njt(i,ng)), i = 1, iipnts(ng)), &
                             ((coeffit(i,io,3,ng), io = 1, nkt(i,ng)), i = 1, iipnts(ng))
      Write (xintout_x_unit) 0, 0, 0
    End Do
    Close (xintout_x_unit)

    Return
  End Subroutine Jet_Crossflow_Interp


  ! =========================================== !
  ! Routine to find donor cells on grid a       !
  ! corresponding to receiver points on grid b  !
  ! - Assume grids are aligned in y-direction   !
  ! - Assume grids are structured               !
  ! =========================================== !
  Subroutine Find_Donor_Cell_2Dy(donor_mesh,recv_mesh,is)
    Implicit None

    ! Local variables
    Integer, Intent(in) :: donor_mesh, recv_mesh, is
    Integer :: i, j, k, irecv, idonor, ir, jr, kr, id, jd, kd, checksum, id_new, kd_new, ND2(2)
    Real(WP) :: xr,yr,zr,xd,xd2,xd3,xd4,yd,zd,zd2,zd3,zd4,xd_min,xd_max,zd_min,zd_max
    Real(WP) :: xrecv(2), xdon(2,2,2), xdon_cub(4,4,2)
    Logical :: Found_donor, Valid_Donor_Cell_2D

    ! Sanity check
    If (donor_mesh==recv_mesh) Then
       Print *, 'Donor mesh and receiver mesh cannot be the same!'
       Stop
    End If

    Write (*,'(A)') ''
    Print *, 'Begin donor search for grid',donor_mesh,'corresponding to recv points on grid',recv_mesh

    ! 2D, assume grids align along y-axis
    dyit = 0.0_WP
    ND2(1) = ND(donor_mesh,1)
    ND2(2) = ND(donor_mesh,3)

    ! Loop through receiver points belonging to receiving mesh
    checksum = 0
    idonor = is
    loop_recv: Do irecv = 1, ibpnts(recv_mesh)
       ! Check if receiving from corresponding donor mesh
       If (donor(irecv,recv_mesh).NE.donor_mesh) Cycle

       ! Get index of receiver points
       ir = ibt(irecv,recv_mesh)
       jr = jbt(irecv,recv_mesh)
       kr = kbt(irecv,recv_mesh)

       ! Get location of receiver point
       xr = X(recv_mesh,ir,jr,kr,1)
       yr = X(recv_mesh,ir,jr,kr,2)
       zr = X(recv_mesh,ir,jr,kr,3)

       ! Store receiver point Cartesian coordinates
       xrecv(1) = xr
       xrecv(2) = zr

       ! Localize along y (ensure yd=yr)
       Call Bisection(yr,jd,X(donor_mesh,1,1:ND(donor_mesh,2),1,2),1,ND(donor_mesh,2))
       yd = X(donor_mesh,1,jd,1,2)
       If (abs(yd-yr).gt.0.000000001_WP) Then
          Print *, "Something's wrong: yr: ",yr,"yd: ",yd
          Stop
       End If

       ! Locate the SW index of the cell that contains the receiver point (xr,zr)
       !
       ! (xd3,zd3)    (xd4,zd4)
       !   o------------o
       !   |            |
       !   |     x      |
       !   |  (xr,zr)   |
       !   |            |
       !   o------------o
       ! (xd,zd)      (xd2,zd2)
       !    ^
       ! SW corner
       !
       ! Brute-force for now
       Found_donor = .False.
       Do kd=1,ND(donor_mesh,3)-1
          Do id=1,ND(donor_mesh,1)-1
             ! Make sure this is not a blanked out node
             If (IBlank(donor_mesh,id,jd,kd).EQ.0) Cycle

             ! Get position of SW corner of donor cell
             xd = X(donor_mesh,id,jd,kd,1)
             zd = X(donor_mesh,id,jd,kd,3)

             ! Get neighboring donor points
             xd2 = X(donor_mesh,id+1,jd,kd  ,1)
             zd2 = X(donor_mesh,id+1,jd,kd  ,3)
             xd3 = X(donor_mesh,id  ,jd,kd+1,1)
             zd3 = X(donor_mesh,id  ,jd,kd+1,3)
             xd4 = X(donor_mesh,id+1,jd,kd+1,1)
             zd4 = X(donor_mesh,id+1,jd,kd+1,3)

             ! Determine min/max points in donor cell
             xd_min=min(xd,xd2); xd_min=min(xd_min,xd3); xd_min=min(xd_min,xd4)
             xd_max=max(xd,xd2); xd_max=max(xd_max,xd3); xd_max=max(xd_max,xd4)
             zd_min=min(zd,zd2); zd_min=min(zd_min,zd3); zd_min=min(zd_min,zd4)
             zd_max=max(zd,zd2); zd_max=max(zd_max,zd3); zd_max=max(zd_max,zd4)

             ! Check if corresponding cell contains receiving point
             If  (xd_min.le.xr .and. xd_max.ge.xr .and. zd_min.le.zr .and. zd_max.ge.zr) Then

                ! Store donor points
                xdon(:,:,1) = X(donor_mesh,id:id+1,jd,kd:kd+1,1)
                xdon(:,:,2) = X(donor_mesh,id:id+1,jd,kd:kd+1,3)

                ! Check if valid donor cell
                Call Compute_Coords_In_Donor_Cell_2D(2, xrecv, xdon, dxit(idonor,donor_mesh), dzit(idonor,donor_mesh))
                If (.NOT.VALID_DONOR_CELL_2D(id_in, dxit(idonor,donor_mesh), dzit(idonor,donor_mesh))) Cycle

                ! Save the stencil size
                nit(idonor,donor_mesh) = id_in
                njt(idonor,donor_mesh) = 1
                nkt(idonor,donor_mesh) = id_in

                ! ... expand the stencil for cubic interpolation
                if (id_in==4) then
                   Call Expand_Stencil_Interp_2D(id_in, id, kd, ND2, id_new, kd_new)
                   xdon_cub(:,:,1) = X(donor_mesh,id_new:id_new+3,jd,kd_new:kd_new+3,1)
                   xdon_cub(:,:,2) = X(donor_mesh,id_new:id_new+3,jd,kd_new:kd_new+3,3)
!!$                   id_new = id - 1
!!$                   kd_new = kd - 1
!!$                   ! Account for periodicity!
!!$                   if (kd_new.le.0) kd_new=kd_new+ND(donor_mesh,3)-1
!!$                   do k=1,4
!!$                      if (kd_new+k-1.ge.ND(donor_mesh,3)) then
!!$                         xdon_cub(:,k,1) = X(donor_mesh,id_new:id_new+3,jd,kd_new+k-ND(donor_mesh,3),1)
!!$                         xdon_cub(:,k,2) = X(donor_mesh,id_new:id_new+3,jd,kd_new+k-ND(donor_mesh,3),3)
!!$                      else
!!$                         xdon_cub(:,k,1) = X(donor_mesh,id_new:id_new+3,jd,kd_new+k-1,1)
!!$                         xdon_cub(:,k,2) = X(donor_mesh,id_new:id_new+3,jd,kd_new+k-1,3)
!!$                      end if
!!$                   end do
                   ! Local index of m-th donor point
                   iit(idonor,donor_mesh) = id_new
                   jit(idonor,donor_mesh) = jd
                   kit(idonor,donor_mesh) = kd_new
                else
                   ! Local index of m-th donor point
                   iit(idonor,donor_mesh) = id
                   jit(idonor,donor_mesh) = jd
                   kit(idonor,donor_mesh) = kd
                end if

                ! Global index of donor point on grid donor_mesh for m-th recv point on grid recv_mesh
                ibct(irecv,recv_mesh) = idonor + iisptr(donor_mesh) - 1

                ! ... compute the interpolation coefficients
                If (id_in == 4) Call Compute_Cubic_Interp_Coeffs_2Dy(xrecv, xdon_cub, coeffit(idonor,1:4,1:3,donor_mesh))
                If (id_in == 2) Call Compute_Linear_Interp_Coeffs_2Dy(xrecv, xdon, coeffit(idonor,1:2,1:3,donor_mesh))

                ! Output Matlab format for plotting donor/recv pairs at a specific vertical plane
!!$                If (recv_mesh==2 .and. donor_mesh==1 .and. jr.eq.175) Then
!!$                   print *, ''
!!$                   write (*,'(A,2(F15.8,A))') ' xr = [', xr, ',', zr, '];'
!!$                   Do j = 1, 2
!!$                      Do i = 1, 2
!!$                         write (*,'(A,I2.2,A,2(F15.8,A))') 'x', (j-1)*2+i, ' = [', xdon(i,j,1), ',', xdon(i,j,2), '];'
!!$                      End Do
!!$                   End Do
!!$                   Write (*,'(A)') "plot(xr(1),xr(2),'+r')"
!!$                   Write (*,'(A)') "hold all"
!!$                   Write (*,'(A)') "plot(x01(1),x01(2),'ob',x02(1),x02(2),'ob',x03(1),x03(2),'ob',x04(1),x04(2),'ob')"
!!$                   Write (*,'(A)') "hold all"
!!$                   Write (*,'(A)') ""
!!$                   Write (*,'(A,F15.8,A)') "dxit=",dxit(idonor,donor_mesh),";"
!!$                   Write (*,'(A,F15.8,A)') "dzit=",dzit(idonor,donor_mesh),";"
!!$                   Write (*,'(A)') ""
!!$                   Write (*,'(A,F15.8,A)') "coeff(1,1)=",coeffit(idonor,1,1,donor_mesh),";"
!!$                   Write (*,'(A,F15.8,A)') "coeff(2,1)=",coeffit(idonor,2,1,donor_mesh),";"
!!$                   Write (*,'(A,F15.8,A)') "coeff(1,3)=",coeffit(idonor,1,3,donor_mesh),";"
!!$                   Write (*,'(A,F15.8,A)') "coeff(2,3)=",coeffit(idonor,2,3,donor_mesh),";"
!!$                End If

                ! Set the flag
                Found_donor=.True.
                idonor = idonor+1
                checksum = checksum+1
                Exit
             End If
          End Do
          If (Found_donor) Exit
       End Do
       ! Sanity check
       If (.not.Found_donor) Print *, 'Donor cell for recv point:',irecv,'not found'
    End Do loop_recv

    ! Check number of donor cells are correct
    If (checksum.NE.ibpnts_(donor_mesh,recv_mesh)) Then
       Write (*,'(A)') ''
       Print *, 'Incorrect number of donor cells found',checksum,'/',ibpnts_(donor_mesh,recv_mesh)
       Stop
    End If

    Return
  End Subroutine Find_Donor_Cell_2Dy


  ! ============================================= !
  ! Routine to find donor cells on grid a         !
  ! corresponding to receiver points on grid b    !
  ! - Assume grids are aligned in theta-direction !
  ! - Assume grids are structured                 !
  ! ============================================= !
  Subroutine Find_Donor_Cell_2Dtheta(donor_mesh,recv_mesh,is)
    Implicit None

    ! Local variables
    Integer, Intent(in) :: donor_mesh, recv_mesh, is
    Integer :: i, j, ii, jj, kk, irecv, idonor, ir, jr, kr, id, jd, kd, checksum, id_guess, jd_guess, ND2(2)
    Real(WP) :: xr, yr, zr, rr, thetar, thetad
    Real(WP) :: xd, yd, zd, yd2, yd3, yd4, rd, rd2, rd3, rd4, rd_min, rd_max, yd_min, yd_max
    Real(WP), dimension(2,2,2) :: xdcell, ydcell, zdcell
    Real(WP) :: xrecv(2), xdon(2,2,2)
    Real(WP), Pointer :: drit(:,:), dtit(:,:)
    Logical :: Found_donor, Valid_Donor_Cell_2D

    ! Sanity check
    If (donor_mesh==recv_mesh) Then
       Print *, 'Donor mesh and receiver mesh cannot be the same!'
       Stop
    End If

    If (id_in==4) Then
       Print *, 'Cubic interpolation not yet implemented for grids aligned in theta...'
       Stop
    End If

    Write (*,'(A)') ''
    Print *, 'Begin donor search for grid',donor_mesh,'corresponding to recv points on grid',recv_mesh

    ! Allocate polar grid spacing (assume aligned in theta)
    Allocate(drit(ipip,ngrid),dtit(ipip,ngrid))
    drit = 0.0_WP; dyit = 0.0_WP; dtit = 0.0_WP
    ND2(1) = ND(donor_mesh,1)
    ND2(2) = ND(donor_mesh,2)

    ! Loop through receiver points belonging to receiving mesh
    checksum = 0
    idonor = is
    loop_recv: Do irecv = 1, ibpnts(recv_mesh)
       ! Check if receiving from corresponding donor mesh
       If (donor(irecv,recv_mesh).NE.donor_mesh) Cycle

       ! Get index of receiver points
       ir = ibt(irecv,recv_mesh)
       jr = jbt(irecv,recv_mesh)
       kr = kbt(irecv,recv_mesh)

       ! Get location of receiver point
       xr = X(recv_mesh,ir,jr,kr,1)
       yr = X(recv_mesh,ir,jr,kr,2)
       zr = X(recv_mesh,ir,jr,kr,3)

       ! Convert to polar coordinates
       rr = sqrt((xr-XJET)**2 + (zr-0.5_WP*Lz)**2)

       ! Store receiver point Cartesian coordinates
       xrecv(1) = rr
       xrecv(2) = yr

       ! Localize along theta
       kd = kr

       ! Make sure theta direction is aligned
       thetar = DBLE(kr-1)/DBLE(ND(recv_mesh,3)-1)*twoPi
       thetad = DBLE(kd-1)/DBLE(ND(donor_mesh,3)-1)*twoPi
       If (abs(thetad-thetar).gt.0.000000001_WP) Then
          Print *, "Something's wrong: thetar: ",thetar,"thetad: ",thetad
          Stop
       End If

       ! Locate the SW index of the cell that contains the receiver point (xr,zr)
       !
       ! (rd3,yd3)    (rd4,yd4)
       !   o------------o
       !   |            |
       !   |     x      |
       !   |  (rr,yr)   |
       !   |            |
       !   o------------o
       ! (rd,yd)      (rd2,yd2)
       !    ^
       ! SW corner
       !
       ! Brute-force for now
       Found_donor = .False.
       Do jd=1,ND(donor_mesh,2)-1
          Do id=1,ND(donor_mesh,1)-1
             ! Make sure this is not a blanked out node
             If (IBlank(donor_mesh,id,jd,kd).EQ.0) Cycle

             ! Get position of SW corner of donor cell
             xd = X(donor_mesh,id,jd,kd,1)
             yd = X(donor_mesh,id,jd,kd,2)
             zd = X(donor_mesh,id,jd,kd,3)

             ! Convert to polar coordinates
             rd = sqrt((xd-XJET)**2 + (zd-0.5_WP*Lz)**2)

             ! Get neighboring donor points
             ! Radial donor points
             rd2 = sqrt((X(donor_mesh,id+1,jd  ,kd,1)-XJET)**2 + (X(donor_mesh,id+1,jd  ,kd,3)-0.5_WP*Lz)**2)
             rd3 = sqrt((X(donor_mesh,id  ,jd+1,kd,1)-XJET)**2 + (X(donor_mesh,id  ,jd+1,kd,3)-0.5_WP*Lz)**2)
             rd4 = sqrt((X(donor_mesh,id+1,jd+1,kd,1)-XJET)**2 + (X(donor_mesh,id+1,jd+1,kd,3)-0.5_WP*Lz)**2)
             ! Vertical donor points
             yd2 = X(donor_mesh,id+1,jd  ,kd,2)
             yd3 = X(donor_mesh,id  ,jd+1,kd,2)
             yd4 = X(donor_mesh,id+1,jd+1,kd,2)
                
             ! Determine min/max points in donor cell
             rd_min=min(rd,rd2); rd_min=min(rd_min,rd3); rd_min=min(rd_min,rd4)
             rd_max=max(rd,rd2); rd_max=max(rd_max,rd3); rd_max=max(rd_max,rd4)
             yd_min=min(yd,yd2); yd_min=min(yd_min,yd3); yd_min=min(yd_min,yd4)
             yd_max=max(yd,yd2); yd_max=max(yd_max,yd3); yd_max=max(yd_max,yd4)

             ! Check if corresponding cell contains receiving point
             If  (rd_min.le.rr .and. rd_max.ge.rr .and. yd_min.le.yr .and. yd_max.ge.yr) Then

                ! Store donor points
                xdon(1,1,1)=rd; xdon(2,1,1)=rd2; xdon(1,2,1)=rd3; xdon(2,2,1)=rd4
                xdon(1,1,2)=yd; xdon(2,1,2)=yd2; xdon(1,2,2)=yd3; xdon(2,2,2)=yd4

                ! Check if valid donor cell
                Call Compute_Coords_In_Donor_Cell_2D(2, xrecv, xdon, drit(idonor,donor_mesh), dyit(idonor,donor_mesh))
                If (.NOT.VALID_DONOR_CELL_2D(id_in, drit(idonor,donor_mesh), dyit(idonor,donor_mesh))) Cycle

                ! Save the stencil size
                nit(idonor,donor_mesh) = id_in
                njt(idonor,donor_mesh) = id_in
                nkt(idonor,donor_mesh) = 1

                ! Local index of m-th donor point
                iit(idonor,donor_mesh) = id
                jit(idonor,donor_mesh) = jd
                kit(idonor,donor_mesh) = kd

                ! Global index of donor point on grid donor_mesh for m-th recv point on grid recv_mesh
                ibct(irecv,recv_mesh) = idonor + iisptr(donor_mesh) - 1

                ! Convert to Cartesian coordinates
                dxit(idonor,donor_mesh) = drit(idonor,donor_mesh)*abs(cos(thetad))
                dzit(idonor,donor_mesh) = drit(idonor,donor_mesh)*abs(sin(thetad))

                ! Compute linear interpolation coefficients
                Call Compute_Linear_Interp_Coeffs_2Dtheta(xrecv, xdon, coeffit(idonor,1:2,1:3,donor_mesh))

                ! Output Matlab format for plotting donor/recv pairs at a specific vertical plane
!!$                   If (donor_mesh==4 .and. kr.eq.1) Then
!!$                      print *, ''
!!$                      write (*,'(A,2(F15.8,A))') ' xr = [', rr, ',', yr, '];'
!!$                      Do j = 1, 2
!!$                         Do i = 1, 2
!!$                            Write (*,'(A,I2.2,A,2(F15.8,A))') 'x', (j-1)*2+i, ' = [', xdon(i,j,1), ',', xdon(i,j,2), '];'
!!$                         End Do
!!$                      End Do
!!$                      Write (*,'(A)') "plot(xr(1),xr(2),'+r')"
!!$                      Write (*,'(A)') "hold all"
!!$                      Write (*,'(A)') "plot(x01(1),x01(2),'ob',x02(1),x02(2),'ob',x03(1),x03(2),'ob',x04(1),x04(2),'ob')"
!!$                      Write (*,'(A)') "hold all"
!!$                      Write (*,'(A)') ""
!!$                      Write (*,'(A,F15.8,A)') "drit=",drit(idonor,donor_mesh),";"
!!$                      Write (*,'(A,F15.8,A)') "dyit=",dyit(idonor,donor_mesh),";"
!!$                      Write (*,'(A)') ""
!!$                      Write (*,'(A,F15.8,A)') "coeff(1,1)=",coeffit(idonor,1,1,donor_mesh),";"
!!$                      Write (*,'(A,F15.8,A)') "coeff(2,1)=",coeffit(idonor,2,1,donor_mesh),";"
!!$                      Write (*,'(A,F15.8,A)') "coeff(1,3)=",coeffit(idonor,1,3,donor_mesh),";"
!!$                      Write (*,'(A,F15.8,A)') "coeff(2,3)=",coeffit(idonor,2,3,donor_mesh),";"
!!$                   End If

                ! Set the flag
                Found_donor=.True.
                idonor = idonor+1
                checksum = checksum+1
                Exit
             End If
          End Do
          If (Found_donor) Exit
       End Do
       ! Sanity check
       If (.not.Found_donor .and. kr.eq.1) Then
          Print *, 'Donor cell for recv point:',irecv,'not found'
       End If
    End Do loop_recv

    ! Check number of donor cells are correct
    If (checksum.NE.ibpnts_(donor_mesh,recv_mesh)) Then
       Write (*,'(A)') ''
       Print *, 'Incorrect number of donor cells found',checksum,'/',ibpnts_(donor_mesh,recv_mesh)
       Stop
    End If

    Return
  End Subroutine Find_Donor_Cell_2Dtheta


  Subroutine Expand_Stencil_Interp_2D(stencil_width, id_in, jd_in, ND, id_out, jd_out)

    Implicit None

    ! ... Global Variables
    Integer :: stencil_width, id_in, jd_in, ND(2), id_out, jd_out

    ! ... Local variables
    Integer :: offset(4) = (/ 0, 0, 0, 1 /)

    ! ... default
    id_out = id_in - offset(stencil_width)
    jd_out = jd_in - offset(stencil_width)

    ! ... check left edge
    if (id_out <= 0) id_out = 1
    if (jd_out <= 0) jd_out = 1

    ! ... check right edge
    if (id_out + stencil_width - 1 > ND(1)) id_out = ND(1) - stencil_width + 1
    if (jd_out + stencil_width - 1 > ND(2)) jd_out = ND(2) - stencil_width + 1

  End Subroutine Expand_Stencil_Interp_2D


  ! ================================================== !
  ! Bisection routine                                  !
  ! Gets an array and its size as well as a position x !
  ! Returns the index between istart-1 and iend        !
  ! xarray(iloc)<=xloc<xarray(iloc+1)                  !
  ! Assuming xarray is monotonically increasing        !
  ! ================================================== !
  Subroutine Bisection(xloc,iloc,xarray,istart,iend)
    Implicit None
  
    Real(WP), Intent(in) :: xloc
    Integer, Intent(out) :: iloc
    Integer, Intent(in) :: istart
    Integer, Intent(in) :: iend
    Real(WP), Dimension(istart:iend), Intent(in) :: xarray
  
    Integer :: il,im,iu
  
    ! Initialize lower and upper limits
    il=istart-1
    iu=iend
  
    ! While not done
    do while (iu-il.gt.1)
       ! Compute a mid-point
       im=(iu+il)/2
       ! Replace lower of upper limit as appropriate
       if (xloc.ge.xarray(im)) then
          il=im
       else
          iu=im
       end if
    end do
  
    ! Finalize the output
    if (xloc.eq.xarray(istart)) then
       iloc=istart
    elseif (xloc.ge.xarray(iend)) then
       iloc=iend
    else
       iloc=il
    end if
  
    Return
  End Subroutine Bisection


  ! ================== !
  ! Clean up and leave !
  ! ================== !
  Subroutine Finalize
    Implicit None
    integer :: ierror
  
    Deallocate(ND,IBLANK,X,Q)

    Return
  End Subroutine Finalize

End Program Jet_Crossflow


! ================================================== !
! Functions and subroutines used in determining      !
! the interpolation coefficients used above.         !
! Taken from jetGridChimera.f90 written by D. Bodony !
! ================================================== !
Subroutine Compute_Linear_Interp_Coeffs_2Dy(xr, xd, coeff)
  Implicit None

  ! ... Global Variables
  Real(KIND=8) :: xr(2), xd(2,2,2), coeff(2,3)

  ! ... local variables
  Integer :: i, j
  Real(KIND=8) :: s, t, Linear_Lagrange_Polynomial
  Logical :: VALID_DONOR_CELL_2D
  External VALID_DONOR_CELL_2D, Linear_Lagrange_Polynomial

  ! ... step 1: find the (xi,eta) coordinates of the receiver point in this 4 x 4 grid of donor points
  Call Compute_Coords_In_Donor_Cell_2D(2, xr, xd, s, t)
  If (.NOT.VALID_DONOR_CELL_2D(2, s, t)) Then
     ! Output Matlab format
     print *, ''
     write (*,'(A,2(F15.8,A))') ' xr = [', xr(1), ',', xr(2), '];'
     Do j = 1, 2
        Do i = 1, 2
           write (*,'(A,I2.2,A,2(F15.8,A))') 'x', (j-1)*2+i, ' = [', xd(i,j,1), ',', xd(i,j,2), '];'
        End Do
     End Do
     Write (*,'(A)') "figure()"
     Write (*,'(A)') "plot(xr(1),xr(2),'or',x01(1),x01(2),'ob',x02(1),x02(2),'ob',x03(1),x03(2),'ob',x04(1),x04(2),'ob')"
     Stop 'ERROR finding valid linear polynomial donor cell'
  End If
  
  ! ... step 2: evaluate the polynomials
  Do i = 1, 2
     coeff(i,1) = Linear_Lagrange_Polynomial(0.0_8, 1.0_8, i, s)
     coeff(i,3) = Linear_Lagrange_Polynomial(0.0_8, 1.0_8, i, t)
  End Do
  coeff(1,2) = 1.0_8
  coeff(2,2) = 0.0_8
    
End Subroutine Compute_Linear_Interp_Coeffs_2Dy

Subroutine Compute_Linear_Interp_Coeffs_2Dtheta(xr, xd, coeff)
  Implicit None

  ! ... Global Variables
  Real(KIND=8) :: xr(2), xd(2,2,2), coeff(2,3)

  ! ... local variables
  Integer :: i, j
  Real(KIND=8) :: s, t, Linear_Lagrange_Polynomial
  Logical :: VALID_DONOR_CELL_2D
  External VALID_DONOR_CELL_2D, Linear_Lagrange_Polynomial

  ! ... step 1: find the (xi,eta) coordinates of the receiver point in this 4 x 4 grid of donor points
  Call Compute_Coords_In_Donor_Cell_2D(2, xr, xd, s, t)
  If (.NOT.VALID_DONOR_CELL_2D(2, s, t)) Then
     ! Output Matlab format
     print *, ''
     write (*,'(A,2(F15.8,A))') ' xr = [', xr(1), ',', xr(2), '];'
     Do j = 1, 2
        Do i = 1, 2
           write (*,'(A,I2.2,A,2(F15.8,A))') 'x', (j-1)*2+i, ' = [', xd(i,j,1), ',', xd(i,j,2), '];'
        End Do
     End Do
     Write (*,'(A)') "figure()"
     Write (*,'(A)') "plot(xr(1),xr(2),'or',x01(1),x01(2),'ob',x02(1),x02(2),'ob',x03(1),x03(2),'ob',x04(1),x04(2),'ob')"
     Stop 'ERROR finding valid linear polynomial donor cell'
  End If
  
  ! ... step 2: evaluate the polynomials
  Do i = 1, 2
     coeff(i,1) = Linear_Lagrange_Polynomial(0.0_8, 1.0_8, i, s)
     coeff(i,2) = Linear_Lagrange_Polynomial(0.0_8, 1.0_8, i, t)
  End Do
  coeff(1,3) = 1.0_8
  coeff(2,3) = 0.0_8
    
End Subroutine Compute_Linear_Interp_Coeffs_2Dtheta


Subroutine Compute_Cubic_Interp_Coeffs_2Dy(xr, xd, coeff)
  Implicit None

  ! ... Global Variables
  Real(KIND=8) :: xr(2), xd(4,4,2), coeff(4,3)

  ! ... local variables
  Integer :: i, j
  Real(KIND=8) :: s, t, Cubic_Lagrange_Polynomial
  Logical :: VALID_DONOR_CELL_2D
  External VALID_DONOR_CELL_2D, Cubic_Lagrange_Polynomial

  ! ... step 1: find the (xi,eta) coordinates of the receiver point in this 4 x 4 grid of donor points
  Call Compute_Coords_In_Donor_Cell_2D(4, xr, xd, s, t)
  If (.NOT.VALID_DONOR_CELL_2D(4, s, t)) Then
     print *, ''
     write (*,'(A,2(F15.8,A))') ' xr = {', xr(1), ',', xr(2), '};'
     Do j = 1, 4
        Do i = 1, 4
           write (*,'(A,I2.2,A,2(F15.8,A))') 'x', (j-1)*4+i, ' = {', xd(i,j,1), ',', xd(i,j,2), '};'
        End Do
     End Do
     Write (*,'(A)') 'fig1 = ListPlot[{xr}];'
     Write (*,'(A)') 'fig2 = ListLinePlot[{x01, x02, x03, x04, x08, x12, x16, x15, x14, x13,x09, x05, x01}, PlotRange -> Full];'
     Write (*,'(A)') 'Show[fig1, fig2]'
     Stop 'ERROR finding valid cubic polynomial donor cell'
  End If
  
  ! ... step 2: evaluate the polynomials
  Do i = 1, 4
     coeff(i,1) = Cubic_Lagrange_Polynomial(-1.0_8, 0.0_8, 1.0_8, 2.0_8, i, s)
     coeff(i,3) = Cubic_Lagrange_Polynomial(-1.0_8, 0.0_8, 1.0_8, 2.0_8, i, t)
  End Do
  coeff(1,2) = 1.0_8
  coeff(2:4,2) = 0.0_8
 
End Subroutine Compute_Cubic_Interp_Coeffs_2Dy


Subroutine Compute_Coords_In_Donor_Cell_2D(id, xr, xd, dx, dy)
  Implicit None

  ! ... Global variables
  Integer :: id
  Real(KIND=8) :: xr(2), xd(id,id,2), dx, dy
  
  ! ... Local variables
  Real(KIND=8) :: s_old, t_old, s_new, t_new, error, f(2), df(2,2,2)
  Real(KIND=8) :: Amat(2,2), invAmat(2,2), det
  Real(KIND=8) :: ds, dt
  Real(KIND=8), Parameter :: TINY = 1D-10
  Integer :: i, j

  ! ... start in the middle and take a guess
  s_old = 0.5_8
  t_old = 0.5_8
  Call Isoparametric_Error_Functions_2D(id, xr, xd, s_old, t_old, f)
  error = dabs(f(1)) + dabs(f(2))

  ! ... check if we were lucky
  If (error < TINY) Then
     dx = s_old
     dy = t_old
     Return
  End If

  ! ... we weren't lucky, continue
  Do While (error > TINY)

     ! ... set increment
     If (id == 2) Then
        ds = 1D-4
        dt = 1D-4
     Else If (id == 4) Then
        ds = 1D-2
        dt = 1D-2
     End If

     ! ... compute around current (s_old,t_old)
     Call Isoparametric_Error_Functions_2D(id, xr, xd, s_old-0.5_8*ds, t_old, df(1,1,:))
     Call Isoparametric_Error_Functions_2D(id, xr, xd, s_old+0.5_8*ds, t_old, df(2,1,:))
     Call Isoparametric_Error_Functions_2D(id, xr, xd, s_old, t_old-0.5_8*dt, df(2,2,:))
     Call Isoparametric_Error_Functions_2D(id, xr, xd, s_old, t_old+0.5_8*dt, df(1,2,:))

     ! ... compute the new search point
     Amat(:,1) = (df(2,1,:) - df(1,1,:)) / ds
     Amat(:,2) = (df(1,2,:) - df(2,2,:)) / dt

     ! ... compute the determinant
     det = Amat(1,1) * Amat(2,2) - Amat(1,2) * Amat(2,1)

     ! ... compute the inverse
     If (dabs(det) < TINY) Then
        print *, ''
        print *, 'id = ', id
        print *, 'xr = ', xr(1:2)
        Do j = 1, id
           Do i = 1, id
              write (*,'(A,I2.2,A,2(E20.8,1X))') 'x', (j-1)*id+i, ' = ', xd(i,j,:)
           End Do
        End Do
        print *, 'Amat = '
        Do j = 1, 2
           print *, Amat(j,:)
        End Do
        Stop 'ERROR: Zero determinant in Compute_Coords_In_Donor_Cell_2D'
     Else
        invAmat(1,1) = Amat(2,2) / det
        invAmat(2,2) = Amat(1,1) / det
        invAmat(1,2) = -Amat(1,2) / det
        invAmat(2,1) = -Amat(2,1) / det
     End If

     ! ... compute the increment
     ds = -invAmat(1,1) * f(1) - invAmat(1,2) * f(2)
     dt = -invAmat(2,1) * f(1) - invAmat(2,2) * f(2)

     ! ... compute the new point
     s_old = s_old + 0.5_8 * ds
     t_old = t_old + 0.5_8 * dt

     Call Isoparametric_Error_Functions_2D(id, xr, xd, s_old, t_old, f)
     error = dabs(f(1)) + dabs(f(2))

  End Do
 
  ! ... must be done
  dx = s_old
  dy = t_old

End Subroutine Compute_Coords_In_Donor_Cell_2D


Subroutine Isoparametric_Error_Functions_2D(id,xr,x,s,t,out)
  Implicit None

  ! ... Global variables
  Real(KIND=8) :: xr(2), x(id,id,2), s, t, out(2)
  Integer :: id

  ! ... Local variables
  Integer :: i, j
  Real(KIND=8) :: Cubic_Lagrange_Polynomial
  External Cubic_Lagrange_Polynomial

  ! ... out(:) = xr(:) - (x(1,1,:) * (1-s) * (1-t) + x(2,1,:) * (s) * (1-t) + x(2,2,:) * (s) * (t) + x(1,2,:) * (1-s) * (t))
  If (id == 2) Then
     out(:) = xr(:) - (x(1,1,:) * (1.0_8 - s) * (1.0_8 - t) + x(2,1,:) * s * (1.0_8 - t) &
          + x(2,2,:) * s * t + x(1,2,:) * (1.0_8 - s) * t)
  Else If (id == 4) Then
     out(:) = xr(:)
     Do i = 1, 4
        Do j = 1, 4
           out(:) = out(:) - x(i,j,:) * Cubic_Lagrange_Polynomial(-1.0_8, 0.0_8, 1.0_8, 2.0_8, i, s) &
                * Cubic_Lagrange_Polynomial(-1.0_8, 0.0_8, 1.0_8, 2.0_8, j, t) 
        End Do
     End Do
  Else
     Stop 'ERROR: Invalid id given in Isoparametric_Error_Functions_2D'
  End If

End Subroutine Isoparametric_Error_Functions_2D


Real(KIND=8) Function Linear_Lagrange_Polynomial(x0, x1, id, x)
  Implicit None

  ! ... Global Variables
  Real(KIND=8) :: x0, x1, x
  Integer :: id

  Select Case (id)
  Case (1)
     Linear_Lagrange_Polynomial = (x-x1) / (x0-x1) 
  Case (2)
     Linear_Lagrange_Polynomial = (x-x0) / (x1-x0)
  End Select

End Function Linear_Lagrange_Polynomial


Real(KIND=8) Function Cubic_Lagrange_Polynomial(x0, x1, x2, x3, id, x)

  Implicit None

  ! ... Global Variables
  Real(KIND=8) :: x0, x1, x2, x3, x
  Integer :: id

  Select Case (id)
  Case (1)
     Cubic_Lagrange_Polynomial = (x-x1) * (x-x2) * (x-x3) / ( (x0-x1) * (x0-x2) * (x0-x3) )
  Case (2)
     Cubic_Lagrange_Polynomial = (x-x0) * (x-x2) * (x-x3) / ( (x1-x0) * (x1-x2) * (x1-x3) )
  Case (3)
     Cubic_Lagrange_Polynomial = (x-x0) * (x-x1) * (x-x3) / ( (x2-x0) * (x2-x1) * (x2-x3) )
  Case (4)
     Cubic_Lagrange_Polynomial = (x-x0) * (x-x1) * (x-x2) / ( (x3-x0) * (x3-x1) * (x3-x2) )
  End Select

End Function Cubic_Lagrange_Polynomial


Logical Function Valid_Donor_Cell_2D (id, dx, dy)
  Implicit None
  
  ! ... Global variables
  Real(KIND=8) :: dx, dy
  Integer :: id

  ! ... Default
  Valid_Donor_Cell_2D = .TRUE.

  If (id == 2) Then

     ! ... Test x-dir
     If (dx < 0.0_8 .or. dx > 1.0_8) Then
        Valid_Donor_Cell_2D = .FALSE.
        Return
     End If

     ! ... Test y-dir
     If (dy < 0.0_8 .or. dy > 1.0_8) Then
        Valid_Donor_Cell_2D = .FALSE.
        Return
     End If

  Else If (id == 4) Then

     ! ... Test x-dir
     If (dx < -1.0_8 .or. dx > 2.0_8) Then
        Valid_Donor_Cell_2D = .FALSE.
        Return
     End If

     ! ... Test y-dir
     If (dy < -1.0_8 .or. dy > 2.0_8) Then
        Valid_Donor_Cell_2D = .FALSE.
        Return
     End If

  End If

  Return
End Function Valid_Donor_Cell_2D
