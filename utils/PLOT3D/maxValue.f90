! Copyright (c) 2015, University of Illinois at Urbana-Champaign, XPACC
! License: MIT, http://opensource.org/licenses/MIT
Program maxValue
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!                                                               !
! Extract maximum value from a PLOT3D solution or function file !
!                                                               !
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!                                                               !
! Written by Daniel J. Bodony (bodony@illinois.edu)             !
! 20 October 2011                                               !
!                                                               !
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  USE ModPLOT3D_IO
  Implicit None

  !  Arrays
  Real(KIND=8), Dimension(:,:,:,:,:), Pointer :: X1, Q1
  Real(KIND=8), Dimension(:,:,:,:,:), Pointer :: X2, Q2
  Real(KIND=8), Dimension(:,:,:,:,:), Pointer :: X3, Q3
  Real(KIND=8) :: tau(4), P, RHO, T, Ux, Uy, Uz, gamma

  !  Integers
  Integer :: ngrid, Nx, Ny, Nz, ng, NDIM, grid_num, ii, jj, kk, var_index
  Integer, Dimension(:,:), Pointer :: ND1

  !  Logical
  Logical :: gf_exists(3), sf_exists(3)

  !  Characters
  Character(LEN=2)  :: prec, gf, vf, ibf
  Character(LEN=80) :: file, cmd, ftype

  !  Step zero, read in the command line
  Integer :: iargc, nargc
  Integer, Parameter :: TRUE = 1, FALSE = 0

  nargc = iargc()
  If (nargc /= 7) Then
    Write (*,'(A)') 'USAGE: maxValue <soln|func> {soln_file} {grid #} {I} {J} {K} {var #}'
    Stop
  Else
    Call Getarg(1,ftype); ! print *, ftype
    Call Getarg(2,file); ! print *, file
    Call Getarg(3,cmd); read (cmd,*) grid_num
    Call Getarg(4,cmd); read (cmd,*) ii
    Call Getarg(5,cmd); read (cmd,*) jj
    Call Getarg(6,cmd); read (cmd,*) kk
    Call Getarg(7,cmd); read (cmd,*) var_index
  End If

  ! ... read based on filetype
  if (ftype(1:1) .eq. 's') then
    Call Read_Soln(NDIM, ngrid, ND1, X1, tau, prec, gf, vf, file, FALSE)
  else if (ftype(1:1) .eq. 'f') then
    Call Read_Func(NDIM, ngrid, ND1, X1, file)
  else
    STOP 'extractValue: unknown filetype '
  end if

  if (ftype(1:1) .eq. 's') then
    ! ... calorically-perfect temperature
    gamma = 1.4_8
    RHO = X1(grid_num,ii,jj,kk,1)
    Ux  = X1(grid_num,ii,jj,kk,2) / RHO
    Uy  = X1(grid_num,ii,jj,kk,3) / RHO
    Uz  = X1(grid_num,ii,jj,kk,4) / RHO
    P   = (gamma-1.0_8)*(X1(grid_num,ii,jj,kk,5) - 0.5_8 * RHO * (Ux*Ux + Uy*Uy + Uz*Uz))
    T   = P * gamma / ((gamma-1.0_8)*RHO)
    write (*,'(2(E20.12,1X))') tau(4), T
  end if

  if (ftype(1:1) .eq. 'f') then
    write (*,'(2(E20.12,1X))') MAXVAL(X1(grid_num,:,:,:,var_index))
  end if

  Deallocate(ND1, X1)

End Program maxValue
