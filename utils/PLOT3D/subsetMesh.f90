! Copyright (c) 2015, University of Illinois at Urbana-Champaign, XPACC
! License: MIT, http://opensource.org/licenses/MIT
Program subsetMesh
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!                                                               !
! take a subset of  a PLOT3D mesh in one direction              !
!                                                               !
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!                                                               !
! Written by Daniel J. Bodony (bodony@stanford.edu)             !
! 24 January 2007
!                                                               !
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  USE ModPLOT3D_IO
  Implicit None

  !  Arrays
  Real(KIND=8), Dimension(:,:,:,:,:), Pointer :: X1, X2
  Real(KIND=8) :: XMIN, XMAX, YMIN, YMAX, ZMIN, ZMAX

  !  Integers
  Integer :: ngrid(3), I, J, K, dir, NX, NY, NZ, NDIM(3)
  Integer, Dimension(:,:), Pointer :: ND1, ND2
  Integer, Dimension(:,:,:,:), Pointer :: IBLANK1, IBLANK2

  !  Logical
  Logical :: gf_exists(3), sf_exists(3)

  !  Characters
  Character(LEN=2)  :: prec, gf, vf, ib
  Character(LEN=80) :: grid_file, cmd, file

  !  Step zero, read in the command line
  Integer :: iargc, nargc, ng, isperiodic, ftype
  Integer :: IS, IE, JS, JE, KS, KE, ISKIP, JSKIP, KSKIP
  Integer :: II, JJ, KK
  Integer, Parameter :: TRUE = 1

  nargc = iargc()
  If (nargc /= 2) Then
    Write (*,'(A)') 'USAGE: subsetMesh {grid file 1} {subset grid file}'
    Stop
  Endif

  Call Getarg(1,file);
  Call Getarg(2,grid_file);

  Call p3d_detect(LEN(Trim(file)),Trim(file),prec,NDIM,gf,vf,ib,ftype)

  !  Step one, read in the mesh1 grid file
  Write (*,'(A)') ''
  Call Read_Grid(NDIM(1),ngrid(1),ND1,X1,IBLANK1, &
       prec,gf,vf,ib,file,TRUE)
  Write (*,'(A)') ''

  !  Step two, find subset dir
  !Write (*,'(A)', ADVANCE='NO') 'Enter subset dir: ' 
  !Read (*,*) dir
  !Do ng = 1, ngrid(1)
  !  If (ND1(ng,dir) .le. 1) Then
  !    Write (*,'(A,I2)') &
  !         'subset dir does not have more than one grid point on grid ', ng
  !    Stop
  !  End If
  !End Do

  !  Step two, get parameters from user
  ngrid(2) = ngrid(1)
  NDIM(2) = NDIM(1)
  Allocate(ND2(ngrid(2),3))

  NX = MAXVAL(ND1(:,1))
  NY = MAXVAL(ND1(:,2))
  NZ = MAXVAL(ND1(:,3))
  ALLOCATE(X2(ngrid(2),NX,NY,NZ,3),IBLANK2(ngrid(2),NX,NY,NZ))

  Do ng = 1, ngrid(1)

    Write (*,'(A,I2,A,3(I5,1X))') 'Block ', ng, ' has size ', &
            (ND1(ng,i),i=1,3)

    IS = 1; JS = 1; KS = 1
    ISKIP = 1; JSKIP = 1; KSKIP = 1
    Write (*,'(A)',ADVANCE='NO') 'Input IS, IE, ISKIP: '
    Read (*,*) IS, IE, ISKIP
    IF (IE < 0) IE = IE + ND1(ng,1) + 1
    ND2(ng,1) = CEILING(DBLE(IE - IS + 1)/DBLE(ISKIP))
    Write (*,'(A)',ADVANCE='NO') 'Input JS, JE, JSKIP: '
    Read (*,*) JS, JE, JSKIP
    IF (JE < 0) JE = JE + ND1(ng,2) + 1
    ND2(ng,2) = CEILING(DBLE(JE - JS + 1)/DBLE(JSKIP))
    Write (*,'(A)',ADVANCE='NO') 'Input KS, KE, KSKIP: '
    Read (*,*) KS, KE, KSKIP
    IF (KE < 0) KE = KE + ND1(ng,3) + 1
    ND2(ng,3) = CEILING(DBLE(KE - KS + 1)/DBLE(KSKIP))
    isperiodic = 0
    II = 0
    Do I = IS, IE, ISKIP
      II = II + 1
      JJ = 0
      Do J = JS, JE, JSKIP
        JJ = JJ + 1
        KK = 0
        Do K = KS, KE, KSKIP
          KK = KK + 1
          X2(ng,II,JJ,KK,:) = X1(ng,I,J,K,:)
          IBLANK2(ng,II,JJ,KK) = IBLANK1(ng,I,J,K)
        End Do
      End Do
    End Do
  End Do

  Call Write_Grid(NDIM(2),ngrid(2),ND2,X2,IBLANK2, &
       prec,gf,vf,ib,grid_file)
  Write (*,'(A)') ''

  ! Step three, clean up & leave
  Deallocate(ND1, X1, IBLANK1, ND2, X2, IBLANK2)


  Stop
End Program subsetMesh
