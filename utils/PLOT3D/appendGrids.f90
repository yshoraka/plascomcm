! Copyright (c) 2015, University of Illinois at Urbana-Champaign, XPACC
! License: MIT, http://opensource.org/licenses/MIT
Program appendGrids
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!                                                               !
! append two PLOT3D grids into one grid                         !
!                                                               !
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!                                                               !
! Written by Daniel J. Bodony (bodony@stanford.edu)             !
! 25 April 2006                                                 !
!                                                               !
! Updated by DJB (bodony@illinois.edu)                          !
! 28 July 2010                                                  !
!                                                               !
! $Header: /cvsroot/genx/Codes/RocfloCM/plot3d_utils/appendGrids.f90,v 1.7 2010/08/10 18:17:56 mtcampbe Exp $                                                      !
!                                                               !
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  USE ModPLOT3D_IO
  Implicit None

  !  Arrays
  Real(KIND=8), Dimension(:,:,:,:,:), Pointer :: X1, Q1
  Real(KIND=8), Dimension(:,:,:,:,:), Pointer :: X2, Q2
  Real(KIND=8), Dimension(:,:,:,:,:), Pointer :: X3, Q3

  !  Integers
  Integer :: ngrid(3), Nx, Ny, Nz, ng, NDIM(3)
  Integer, Dimension(:,:), Pointer :: ND1, ND2, ND3
  Integer, Dimension(:,:,:,:), Pointer :: IBLANK1, IBLANK2, IBLANK3
  Integer, Parameter :: TRUE = 1

  !  Logical
  Logical :: gf_exists(3), sf_exists(3)

  !  Characters
  Character(LEN=2)  :: prec(3), grid_format(3), volume_format(3), ib_format(3)
  Character(LEN=80) :: grid_file(3), cmd

  !  Step zero, read in the command line
  Integer :: iargc, nargc

  nargc = iargc()
  If (nargc /= 3) Then
    Write (*,'(A)') 'USAGE: appendGrids {grid file 1}  {grid file 2} {merged grid file}'
    Stop
  Else
    Call Getarg(1,grid_file(1)); !print *, grid_file(1)
    Call Getarg(2,grid_file(2)); !print *, grid_file(2:2)
    Call Getarg(3,grid_file(3)); !print *, soln_file(1:1)
  End If


  !  Step one, read in the mesh1 grid file
  Write (*,'(A)') ''
  Call Read_Grid(NDIM(1),ngrid(1),ND1,X1,IBLANK1, &
       prec(1),grid_format(1),volume_format(1),ib_format(1),grid_file(1),TRUE)
  Write (*,'(A)') ''

  !  Step two, read in the mesh2 grid file
  Call Read_Grid(NDIM(2),ngrid(2),ND2,X2,IBLANK2, &
       prec(2),grid_format(2),volume_format(2),ib_format(2),grid_file(2),TRUE)
  Write (*,'(A)') ''

  If (NDIM(1) .ne. NDIM(2)) Then
    Stop 'Files are not of the same spatial dimension.'
  End If
  NDIM(3) = NDIM(1)

  ! Step three, allocate storage for merged file
  ngrid(3) = ngrid(1) + ngrid(2)
  Allocate(ND3(ngrid(3),3))
  Do ng = 1, ngrid(1)
    ND3(ng,:) = ND1(ng,:)
  End Do
  Do ng = 1, ngrid(2)
    ND3(ngrid(1)+ng,:) = ND2(ng,:)
  End Do
  NX = MAXVAL(ND3(:,1));
  NY = MAXVAL(ND3(:,2));
  NZ = MAXVAL(ND3(:,3));
  Allocate(X3(ngrid(3),NX,NY,NZ,3))
  Allocate(IBLANK3(ngrid(3),NX,NY,NZ))

  ! Step four, copy grids 1 & 2 into 3
  Do ng = 1, ngrid(1)
    X3(ng,1:ND1(ng,1),1:ND1(ng,2),1:ND1(ng,3),:) = &
    X1(ng,1:ND1(ng,1),1:ND1(ng,2),1:ND1(ng,3),:)
    IBLANK3(ng,1:ND1(ng,1),1:ND1(ng,2),1:ND1(ng,3)) = &
    IBLANK1(ng,1:ND1(ng,1),1:ND1(ng,2),1:ND1(ng,3))
  End Do
  Do ng = 1, ngrid(2)
    X3(ng+ngrid(1),1:ND2(ng,1),1:ND2(ng,2),1:ND2(ng,3),:) = &
    X2(ng         ,1:ND2(ng,1),1:ND2(ng,2),1:ND2(ng,3),:)
    IBLANK3(ng+ngrid(1),1:ND2(ng,1),1:ND2(ng,2),1:ND2(ng,3)) = &
    IBLANK2(ng         ,1:ND2(ng,1),1:ND2(ng,2),1:ND2(ng,3))
  End Do

  !  Step five, write the file
  Call Write_Grid(NDIM(3),ngrid(3),ND3,X3,IBLANK3, &
       prec(1),grid_format(1),volume_format(1),ib_format(1),grid_file(3))
  Write (*,'(A)') ''

  ! Step six, clean up & leave
  Deallocate(ND1, X1, IBLANK1)
  Deallocate(ND2, X2, IBLANK2)
  Deallocate(ND3, X3, IBLANK3)

End Program appendGrids
