! Copyright (c) 2015, University of Illinois at Urbana-Champaign, XPACC
! License: MIT, http://opensource.org/licenses/MIT
Program plot3dToVtk
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!                                                               !
! Read a PLOT3D mesh and convert it a Structured VTK set of mesh!
! Each Block/Grid is written in a separate VTK file             !
!                                                               !
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!                                                               !
! Written by Daniel J. Bodony (bodony@uiuc.edu)                 !
! 01 October 2008
!                                                               !
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  USE ModPLOT3D_IO
  Implicit None

  !  Arrays
  Real(KIND=8), Dimension(:,:,:,:,:), Pointer :: X1, X2
  Real(KIND=8) :: XMIN, XMAX, YMIN, YMAX, ZMIN, ZMAX

  !  Integers
  Integer :: ngrid(3), I, J, K, dir, NX, NY, NZ, NDIM(3)
  Integer, Dimension(:,:), Pointer :: ND1, ND2
  Integer, Dimension(:,:,:,:), Pointer :: IBLANK1, IBLANK2

  !  Logical
  Logical :: gf_exists(3), sf_exists(3)

  !  Characters
  Character(LEN=2)  :: prec, gf, vf, ib
  Character(LEN=80) :: grid_file, cmd, file

  !  Step zero, read in the command line
  Integer :: iargc, nargc, ng, isperiodic, ftype
  Integer :: IS, IE, JS, JE, KS, KE, ISKIP, JSKIP, KSKIP
  Integer :: II, JJ, KK, l
  character(80)a
  nargc = iargc()
  If (nargc /= 2) Then
    Write (*,'(A)') 'USAGE: plot3dToGridgen {grid file 1} {grid file 2}'
    Stop
  Endif

  Call Getarg(1,file);
  Call Getarg(2,grid_file);

  Call p3d_detect(LEN(Trim(file)),Trim(file),prec,NDIM,gf,vf,ib,ftype)

  !  Step one, read in the mesh1 grid file
  Write (*,'(A)') ''
  !Call Read_Grid(NDIM(1),ngrid(1),ND1,X1,IBLANK1, &
  !     prec,gf,vf,ib,file,.true. )

  Call Read_Grid(NDIM(1),ngrid(1),ND1,X1,IBLANK1, &
       prec,gf,vf,ib,file, 1 )
  Write (*,'(A)') ''

  ! ... step 2, write each grid in a separate vtk file
  Do ng=1, size(X1,1)
    write(a,'(i10)') ng
    open (unit=11, file=Trim(Trim(grid_file)//Trim(a)//'.vtk'), status='unknown', form='formatted')
    ! ... write VTK header
    Write (11,*) '# vtk DataFile Version 3.0'
    Write (11,*) 'Converted PLOT3D multiblock grid'
    Write (11,*) 'ASCII'
    Write (11,*) 'DATASET STRUCTURED_GRID'
    Write (11,100) 'DIMENSIONS', ND1(ng,1), ND1(ng,2), ND1(ng,3)
  100 format (A,I,I,I)
  Write (11,200) 'POINTS ', ND1(ng,1)*ND1(ng,2)*ND1(ng,3), ' double'
  200 format (A,I,A)
    Do k=1, ND1(ng,3)
      Do j=1, ND1(ng,2)
        Do i=1, ND1(ng,1)
          Write(11,*) (X1(ng,i,j,k,l),l=1,3)
        End Do
      End Do
    End Do
    Close (11)
  End Do

  ! Step three, clean up & leave
  Deallocate(ND1, X1, IBLANK1)


  Stop
  End Program plot3dToVtk
