! Copyright (c) 2015, University of Illinois at Urbana-Champaign, XPACC
! License: MIT, http://opensource.org/licenses/MIT
Program hRefine

  USE ModPLOT3D_IO
  Implicit None

  Real(KIND=8), Dimension(:,:,:,:,:), Pointer :: X1, Q1
  Real(KIND=8), Dimension(:,:,:,:,:), Pointer :: X2, Q2
  Real(KIND=8) :: tau(4)

  !  Integers
  Integer :: ngrid(2), Nx, Ny, Nz, ng, NDIM(2)
  Integer, Dimension(:,:), Pointer :: ND1, ND1Copy, ND2
  Integer, Dimension(:,:,:,:), Pointer :: IBLANK1, IBLANK2
  Integer :: is1(3), ie1(3), is2(3), ie2(3), io_stat, rFac(3)
  Integer :: i, j, k, i1, j1, k1, i2, j2, k2, i3, j3, k3, m, n

  ! ... Logical
  Logical :: gf_exists(3), sf_exists(3)

  !  Characters
  Character(LEN=2)  :: prec(2), grid_format(2), volume_format(2), ib_format(2),rFacC(3)
  Character(LEN=80) :: grid_file1, soln_file1, grid_file2, soln_file2, cmd

  !  Step zero, read in the command line
  Integer :: iargc, nargc
  Integer, Parameter :: TRUE = 1, FALSE = 0

  nargc = iargc()
  If (nargc /= 5) Then
     Write (*,'(A)') 'USAGE: hRefine {grid file} {soln file} {ifactor} {jfactor} {kfactor}'
     Stop
  Else
     Call Getarg(1,grid_file1); !print *, grid_file(1)
     Call Getarg(2,soln_file1); !print *, grid_file(2:2)
     Call Getarg(3,rFacC(1))
     Call Getarg(4,rFacC(2))
     Call Getarg(5,rFacC(3))
  End If

  ! ... Nullify pointers
  Nullify(Q1)
  Nullify(X1)

  ! ... convert input to integer
  do i = 1, 3
     read(rFacC(i),'(I2)') rFac(i)
  end do

  Write (*,'(A)') ''
  Call Read_Grid(NDIM(1),ngrid(1),ND1,X1,IBLANK1, &
       prec(1),grid_format(1),volume_format(1),ib_format(1),grid_file1,TRUE)
  Write (*,'(A)') ''

  Allocate(ND1Copy(size(ND1,1),size(ND1,2)))

  ! ... new file names
  grid_file2 = trim(grid_file1) // ".refined.xyz"
  soln_file2 = trim(soln_file1) // ".refined.q"

  ! ... find the size of the refined grid
  allocate(ND2(size(ND1,1),3))
  do i = 1, 3
     ND2(1,i) = rFac(i)*(ND1(1,i)-1)+1
  end do
  
  write(*,'(A,I4,2(A,I4),A,I9,A)') 'Refined grid is ',ND2(1,1),' x ',ND2(1,2),' x ',ND2(1,3),' = ',product(ND2),' points.'
  write(*,'(A,f6.2,A)') 'Estimated ',dble(product(ND2))*5.0*8.0/(1024.0**3),' GB of memory needed to perform refinement.'
  write(*,'(A)',ADVANCE='NO') 'Continue? (0 = no, 1 = yes): ' 
  read(*,'(I1)') i
  if(i /= 1) stop

  ! ... proceed
  allocate(X2(size(X1,1),ND2(1,1),ND2(1,2),ND2(1,3),size(X1,5)))
  Allocate(IBLANK2(size(X1,1),ND2(1,1),ND2(1,2),ND2(1,3)))

  ! ... now refine the grid
  do ng = 1, size(X1,1)
     do k = 1, ND1(1,3)
        do j = 1, ND1(1,2)
           do i = 1, ND1(1,1)
             ! print*,i,j,k
              k2 = (k-1)*rFac(3) + 1
              j2 = (j-1)*rFac(2) + 1
              i2 = (i-1)*rFac(1) + 1
              do m = 1, size(X1,5)
                 X2(ng,i2,j2,k2,m) = X1(ng,i,j,k,m)
              end do
           end do
        end do
     end do
  end do

  ! ... free up memory
  deallocate(X1,IBLANK1)

  ! ... interpolate in direction 1
  do ng = 1, size(X2,1)
     do k = 1, ND2(1,3)
        do j = 1, ND2(1,2)
           do i = 1, ND1(1,1)-1
              do n = 1, rFac(1)
                 i2 = (i-1)*rFac(1) + n + 1
                 i1 = (i-1)*rFac(1) + 1
                 i3 = (i)*rFac(1) + 1
                 do m = 1, size(X2,5)
                    X2(ng,i2,j,k,m) = dble(n)/dble(rFac(1))*(X2(ng,i3,j,k,m)-X2(ng,i1,j,k,m))+X2(ng,i1,j,k,m)
                 end do
              end do
           end do
        end do
     end do
  end do

  ! ... interpolate in direction 2
  do ng = 1, size(X2,1)
     do k = 1, ND2(1,3)
        do j = 1, ND1(1,2)-1
           do i = 1, ND2(1,1)
              do n = 1, rFac(2)
                 j2 = (j-1)*rFac(2) + n + 1
                 j1 = (j-1)*rFac(2) + 1
                 j3 = (j)*rFac(2) + 1
                 do m = 1, size(X2,5)
                    X2(ng,i,j2,k,m) = dble(n)/dble(rFac(2))*(X2(ng,i,j3,k,m)-X2(ng,i,j1,k,m))+X2(ng,i,j1,k,m)
                 end do
              end do
           end do
        end do
     end do
  end do

  ! ... interpolate in direction 3
  do ng = 1, size(X2,1)
     do k = 1, ND1(1,3)-1
        do j = 1, ND2(1,2)
           do i = 1, ND2(1,1)
              do n = 1, rFac(3)
                 k2 = (k-1)*rFac(3) + n + 1
                 k1 = (k-1)*rFac(3) + 1
                 k3 = (k)*rFac(3) + 1
                 do m = 1, size(X2,5)
                    X2(ng,i,j,k2,m) = dble(n)/dble(rFac(3))*(X2(ng,i,j,k3,m)-X2(ng,i,j,k1,m))+X2(ng,i,j,k1,m)
                 end do
              end do
           end do
        end do
     end do
  end do

  Call Write_Grid(NDIM(1),ngrid(1),ND2,X2,IBLANK2, &
       prec(1),grid_format(1),volume_format(1),ib_format(1),grid_file2)
  Write (*,'(A)') ''

  ND1Copy = ND1

  ! ... free up memory
  Deallocate(ND1,X2,IBLANK2)

  Write (*,'(A)') 'Now interpolating solution'
  Call Read_Soln(NDIM(1), ngrid(1), ND1, Q1, tau, prec(1), grid_format(1), volume_format(1), soln_file1, TRUE)

  do i = 1, size(ND1,2)
     if (ND1(1,i)/=ND1Copy(1,i)) then
        write(*,'(A)') 'Input grid and solution sizes do not match'
        write(*,'(A,A,I4,2(A,I4),A,I9,A)') TRIM(grid_file1), ' is ',ND1Copy(1,1),' x ',ND1Copy(1,2),' x ',ND1Copy(1,3),' = ', &
             product(ND1Copy),' points.'
        write(*,'(A,A,I4,2(A,I4),A,I9,A)') TRIM(soln_file1), ' is ',ND1(1,1),' x ',ND1(1,2),' x ',ND1(1,3),' = ', &
             product(ND1),' points.'
        stop
     end if
  end do

  deallocate(ND1Copy)

  ! ... proceed
  allocate(Q2(size(Q1,1),ND2(1,1),ND2(1,2),ND2(1,3),size(Q1,5)))

  ! ... now refine the grid
  do ng = 1, size(Q1,1)
     do k = 1, ND1(1,3)
        do j = 1, ND1(1,2)
           do i = 1, ND1(1,1)
             ! print*,i,j,k
              k2 = (k-1)*rFac(3) + 1
              j2 = (j-1)*rFac(2) + 1
              i2 = (i-1)*rFac(1) + 1
              do m = 1, size(Q1,5)
                 Q2(ng,i2,j2,k2,m) = Q1(ng,i,j,k,m)
              end do
           end do
        end do
     end do
  end do

  ! ... free up memory
  deallocate(Q1)

  ! ... interpolate in direction 1
  do ng = 1, size(Q2,1)
     do k = 1, ND2(1,3)
        do j = 1, ND2(1,2)
           do i = 1, ND1(1,1)-1
              do n = 1, rFac(1)
                 i2 = (i-1)*rFac(1) + n + 1
                 i1 = (i-1)*rFac(1) + 1
                 i3 = (i)*rFac(1) + 1
                 do m = 1, size(Q2,5)
                    Q2(ng,i2,j,k,m) = dble(n)/dble(rFac(1))*(Q2(ng,i3,j,k,m)-Q2(ng,i1,j,k,m))+Q2(ng,i1,j,k,m)
                 end do
              end do
           end do
        end do
     end do
  end do

  ! ... interpolate in direction 2
  do ng = 1, size(Q2,1)
     do k = 1, ND2(1,3)
        do j = 1, ND1(1,2)-1
           do i = 1, ND2(1,1)
              do n = 1, rFac(2)
                 j2 = (j-1)*rFac(2) + n + 1
                 j1 = (j-1)*rFac(2) + 1
                 j3 = (j)*rFac(2) + 1
                 do m = 1, size(Q2,5)
                    Q2(ng,i,j2,k,m) = dble(n)/dble(rFac(2))*(Q2(ng,i,j3,k,m)-Q2(ng,i,j1,k,m))+Q2(ng,i,j1,k,m)
                 end do
              end do
           end do
        end do
     end do
  end do

  ! ... interpolate in direction 3
  do ng = 1, size(Q2,1)
     do k = 1, ND1(1,3)-1
        do j = 1, ND2(1,2)
           do i = 1, ND2(1,1)
              do n = 1, rFac(3)
                 k2 = (k-1)*rFac(3) + n + 1
                 k1 = (k-1)*rFac(3) + 1
                 k3 = (k)*rFac(3) + 1
                 do m = 1, size(Q1,5)
                    Q2(ng,i,j,k2,m) = dble(n)/dble(rFac(3))*(Q2(ng,i,j,k3,m)-Q2(ng,i,j,k1,m))+Q2(ng,i,j,k1,m)
                 end do
              end do
           end do
        end do
     end do
  end do

  Call Write_Soln(NDIM(1),ngrid(1),ND2,Q2,tau,&
       prec(1),grid_format(1),volume_format(1),soln_file2)
  
  Write (*,'(A)') ''  
  
  deallocate(ND1,ND2,Q2)


End Program HREFINE
         
