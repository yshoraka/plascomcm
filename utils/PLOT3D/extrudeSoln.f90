! Copyright (c) 2015, University of Illinois at Urbana-Champaign, XPACC
! License: MIT, http://opensource.org/licenses/MIT
Program extrudeSoln
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!                                                               !
! Extrude a PLOT3D mesh in one direction                        !
!                                                               !
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!                                                               !
! Written by Daniel J. Bodony (bodony@illinois.edu)             !
! 06 January 2012                                               !
!                                                               !
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  USE ModPLOT3D_IO
  Implicit None

  !  Arrays
  Real(KIND=8), Dimension(:,:,:,:,:), Pointer :: X1, X2
  Real(KIND=8) :: XMIN, XMAX, YMIN, YMAX, ZMIN, ZMAX, TAU(4)

  !  Integers
  Integer :: ngrid(3), I, J, K, dir, NX, NY, NZ, NDIM(3)
  Integer, Dimension(:,:), Pointer :: ND1, ND2
  Integer, Dimension(:,:,:,:), Pointer :: IBLANK1, IBLANK2

  !  Logical
  Logical :: gf_exists(3), sf_exists(3)

  !  Characters
  Character(LEN=2)  :: prec(3), gf(3), vf(3), ib(3)
  Character(LEN=80) :: soln_file(3), cmd

  !  Step zero, read in the command line
  Integer :: iargc, nargc, ng, isperiodic, ftype
  Integer, Parameter :: TRUE = 1

  nargc = iargc()
  If (nargc /= 2) Then
    Write (*,'(A)') 'USAGE: extrudeMesh {soln file 1} {extruded soln file}'
    Stop
  Endif

  Call Getarg(1,soln_file(1));
  Call Getarg(2,soln_file(2));

  Call p3d_detect(LEN(Trim(soln_file(1))),Trim(soln_file(1)),prec,NDIM,gf,vf,ib,ftype)

  !  Step one, read in the mesh1 grid file
  Write (*,'(A)') ''
  Call Read_Soln(NDIM(1),ngrid(1),ND1,X1, tau, prec(1), gf(1), vf(1), soln_file(1), TRUE)
  Write (*,'(A)') ''

  !  Step two, find extrusion dir
  Write (*,'(A)', ADVANCE='NO') 'Enter extrusion dir: ' 
  Read (*,*) dir
  Do ng = 1, ngrid(1)
    If (ND1(ng,dir) .ne. 1) Then
      Write (*,'(A,I2)') &
           'Extrusion dir does not have one grid point on grid ', ng
      Stop
    End If
  End Do

  !  Step two, get parameters from user
  ngrid(2) = ngrid(1)
  NDIM(2) = NDIM(1)
  Allocate(ND2(ngrid(2),3))
  ND2 = ND1
  If (dir .eq. 1) Then
    Write (*,'(A)',ADVANCE='NO') 'Input NX: '
    Read (*,*) ND2(1,1)
    ND2(:,1) = ND2(1,1)    
  Else If (dir .eq. 2) Then
    Write (*,'(A)',ADVANCE='NO') 'Input NY: '
    Read (*,*) ND2(1,2)
    ND2(:,2) = ND2(1,2)
  Else
    Write (*,'(A)',ADVANCE='NO') 'Input NZ: '
    Read (*,*) ND2(1,3)
    ND2(:,3) = ND2(1,3)
  End If

  NX = MAXVAL(ND2(:,1))
  NY = MAXVAL(ND2(:,2))
  NZ = MAXVAL(ND2(:,3))
  ALLOCATE(X2(ngrid(2),NX,NY,NZ,5))

  Do ng = 1, ngrid(1)
    Do I = 1, ND1(ng,1)
      Do J = 1, ND1(ng,2)
        Do K = 1, ND1(ng,3)
          X2(ng,I,J,K,:) = X1(ng,I,J,K,:)
        End Do
      End Do
    End Do
  End Do

  If (dir .eq. 1) Then
    Do ng = 1, ngrid(2)
      Do I = 1, ND2(ng,1)
        Do J = 1, ND2(ng,2)
          Do K = 1, ND2(ng,3)
            X2(ng,I,J,K,:) = X2(ng,1,J,K,:)
          End Do
        End Do
      End Do
    End Do
  Else If (dir .eq. 2) Then
    Do ng = 1, ngrid(2)
      Do I = 1, ND2(ng,1)
        Do J = 1, ND2(ng,2)
          Do K = 1, ND2(ng,3)
            X2(ng,I,J,K,:) = X2(ng,I,1,K,:)
          End Do
        End Do
      End Do
    End Do
  Else
    Do ng = 1, ngrid(2)
      Do I = 1, ND2(ng,1)
        Do J = 1, ND2(ng,2)
          Do K = 1, ND2(ng,3)
            X2(ng,I,J,K,:) = X2(ng,I,J,1,:)
          End Do
        End Do
      End Do
    End Do
  End If

  Call Write_Soln(NDIM(2), ngrid(2), ND2, X2, tau, prec(1), gf(1), vf(1), soln_file(2))

  Write (*,'(A)') ''

  ! Step three, clean up & leave
  Deallocate(ND1, X1, ND2, X2)


  Stop
End Program extrudeSoln
