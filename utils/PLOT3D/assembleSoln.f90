! Copyright (c) 2015, University of Illinois at Urbana-Champaign, XPACC
! License: MIT, http://opensource.org/licenses/MIT
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!                                                               !
! Assemble a PLOT3D file that has been split over processors    !
!                                                               !
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!                                                               !
! Written by Daniel J. Bodony (bodony@illinois.edu)             !
! 16 September 2009                                             !
!                                                               !
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

Program assembleSoln

  USE ModPLOT3D_IO
  Implicit None

  !  Arrays
  Real(KIND=8), Dimension(:,:,:,:,:), Pointer :: X
  Real(KIND=8) :: XMIN, XMAX, YMIN, YMAX, ZMIN, ZMAX, TAU(4)
  Real(KIND=8), Pointer :: dtmp(:,:)

  !  Integers
  Integer :: ngrid(3), I, J, K, M, dir, NX, NY, NZ, NDIM, NXp, NYp, NZp
  Integer, Dimension(:,:), Pointer :: ND, gridToProcMap(:,:)
  Integer, Dimension(:,:,:,:), Pointer :: IBLANK
  Integer :: nGridsGlobal, IS(3), IE(3), l0, iGridGlobal, p, itmp, pp
  Integer :: var(5)
  !  Logical
  Logical :: gf_exists(3), sf_exists(3)

  !  Characters
  Character(LEN=2)  :: prec(3), gf(3), vf(3), ib(3)
  Character(LEN=80) :: baseFile, individualFile, cmd

  !  Step zero, read in the command line
  Integer :: iargc, nargc, ng, isperiodic, ftype

  nargc = iargc()
  If (nargc /= 1) Then
    Write (*,'(A)') 'USAGE: assembleSoln {Base solution filename}'
    Write (*,'(A)') 'Example: assembleSoln RocFlo-CM.00000000.q'
    Stop
  Endif

  ! ... pointer
  Do m = 1, 5
    var(m) = m
  End Do

  Call Getarg(1,baseFile);

  ! ... step one
  ! ... read the first file and get the problem size
  write (individualFile,'(A,A,I5.5,A,I5.5)') trim(baseFile), '.', 00000, '.', 00001
  Open (unit=10, file=trim(individualFile), form='unformatted', status='old')
  Read (10) nGridsGlobal, NDIM
  Allocate(ND(nGridsGlobal,3))
  Allocate(gridToProcMap(nGridsGlobal,2))
  Read (10) (gridToProcMap(i,1:2),i=1,nGridsGlobal)
  Read (10) (ND(i,1:3),i=1,nGridsGlobal)
  Close (10)

  ! ... adjust for 1- and 2-D problems
  If (NDIM == 1) var(3) = 5
  If (NDIM == 2) var(4) = 5

  ! ... step two 
  ! ... allocate space
  allocate(X(nGridsGlobal,MAXVAL(ND(:,1)),MAXVAL(ND(:,2)),MAXVAL(ND(:,3)),5))

  ! ... step three
  ! ... read each grid
  do ng = 1, nGridsGlobal
    NX = ND(p,1)
    NY = ND(p,2)
    NZ = ND(p,3)
    do p = gridToProcMap(ng,1), gridToProcMap(ng,2)
      pp = p - gridToProcMap(ng,1)
      write (individualFile,'(A,A,I5.5,A,I5.5)') trim(baseFile), '.', pp, '.', ng
      print *, trim(individualFile)
      Open (unit=10, file=trim(individualFile), form='unformatted', status='old')
      Read (10) itmp, itmp
      Read (10) (itmp,i=1,nGridsGlobal*2)
      Read (10) (itmp,i=1,nGridsGlobal*3)
      Read (10) iGridGlobal
      if (iGridGlobal /= ng) STOP 'grid number mismatch'
      Read (10) IS(1:3), IE(1:3)
      Read (10) tau
      NXp = IE(1) - IS(1) + 1
      NYp = IE(2) - IS(2) + 1
      NZp = IE(3) - IS(3) + 1
      allocate(dtmp(NXp*NYp*NZp,NDIM+2))
      Read (10) dtmp
      Do m = 1, NDIM+2
        Do k = 1, NZp
          Do j = 1, NYp
            Do i = 1, NXp
              l0 = (k-1)*NXp*NYp + (j-1)*NXp + i
              X(ng,i+IS(1)-1,j+IS(2)-1,k+IS(3)-1,var(m)) = dtmp(l0,m)
            End Do
          End Do
        End Do
      End Do
      deallocate(dtmp)
    end do
  end do

  prec(1) = 'd '
  gf(1)   = 'm '
  vf(1)   = 'w '
  Call Write_Soln(3,nGridsGlobal,ND,X,tau,prec(1),gf(1),vf(1),baseFile)

  Write (*,'(A)') ''

  ! Step three, clean up & leave
  Deallocate(ND, X)


  Stop
End Program assembleSoln
