! Copyright (c) 2015, University of Illinois at Urbana-Champaign, XPACC
! License: MIT, http://opensource.org/licenses/MIT
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!                                                               !
! Compute the root-mean-square of a linearized field            !
!                                                               !
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!                                                               !
! Written by Daniel J. Bodony (bodony@illinois.edu)             !
! 24 August 2011                                                !
!                                                               !
! $Header: /cvsroot/genx/Codes/RocfloCM/plot3d_utils/computeLinRMS.f90,v 1.4 2011/08/24 20:00:23 bodony Exp $
!                                                               !
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

Program computeMean

  USE ModPLOT3D_IO
  Implicit None

  !  Arrays
  Real(KIND=8), Dimension(:,:,:,:,:), Pointer :: X1, X2, time(:), Mean
  Real(KIND=8) :: XMIN, XMAX, YMIN, YMAX, ZMIN, ZMAX, TAU(4), VAR(5)

  !  Integers
  Integer :: ngrid, I, J, K, dir, NX, NY, NZ, NDIM, L
  Integer, Dimension(:,:), Pointer :: ND1, ND2
  Integer, Dimension(:,:,:,:), Pointer :: IBLANK1, IBLANK2
  Integer :: startIter, stopIter, skipIter, iter

  !  Logical
  Logical :: gf_exists(3), sf_exists(3)

  !  Characters
  Character(LEN=2)  :: prec, gf, vf, ib
  Character(LEN=80) :: infile, outfile, cmd, meanfile

  !  Doubles
  Real(KIND=8) :: startTime, stopTime, interval, dt, gamma

  !  Step zero, read in the command line
  Integer :: iargc, nargc, ng, isperiodic, ftype, num, numFiles

  nargc = iargc()
  If (nargc /= 5) Then
    Write (*,'(A)') 'USAGE: computeRMSLin <startIter> <stopIter> <skipIter> <meanFile> <outFile>'
    Stop
  Endif

  Nullify(X1,X2)

  ! ... starting, stopping, skip iteration
  Call Getarg(1,cmd); read (cmd,*) startIter
  Call Getarg(2,cmd); read (cmd,*) stopIter
  Call Getarg(3,cmd); read (cmd,*) skipIter

  ! ... output file
  Call Getarg(4,meanfile);
  Call Getarg(5,outfile);

  ! ... read the mean
  Call Read_Soln(NDIM,ngrid,ND1,MEAN,TAU,prec,gf,vf,meanfile,0)

  ! ... convert mean to primative variables
  gamma = 1.4_8
  Do ng = 1, ngrid
    Do I = 1, ND1(ng,1)
      Do J = 1, ND1(ng,2)
        Do K = 1, ND1(ng,3)
          Mean(ng,I,J,K,2) = Mean(ng,I,J,K,2) / Mean(ng,I,J,K,1)
          Mean(ng,I,J,K,3) = Mean(ng,I,J,K,3) / Mean(ng,I,J,K,1)
          Mean(ng,I,J,K,4) = Mean(ng,I,J,K,4) / Mean(ng,I,J,K,1)
          Mean(ng,I,J,K,5) = (gamma-1.0_8)*(Mean(ng,I,J,K,5) - 0.5_8 * Mean(ng,I,J,K,1) * &
                                Mean(ng,I,J,K,2) * Mean(ng,I,J,K,2) + &
                                Mean(ng,I,J,K,3) * Mean(ng,I,J,K,3) + &
                                Mean(ng,I,J,K,4) * Mean(ng,I,J,K,4))
        End Do
      End Do
    End Do
  End Do
 

  ! ... initialize
  num  = 1

  Do iter = startIter, stopIter, skipIter
    Write(infile,'(A,I8.8,A)') 'RocFlo-CM.', iter, '.q'

    Call Read_Soln(NDIM,ngrid,ND1,X1,TAU,prec,gf,vf,infile,0)
    Write (*,'(A)') infile

    ! ... allocate average if not already
    if (associated(X2) .eqv. .false.) then
      Allocate(X2(ngrid,MAXVAL(ND1(:,1)),MAXVAL(ND1(:,2)),MAXVAL(ND1(:,3)),NDIM+2))
      X2 = 0.0_8
    end if

    ! ... add current solution
    Do ng = 1, ngrid
      Do I = 1, ND1(ng,1)
        Do J = 1, ND1(ng,2)
          Do K = 1, ND1(ng,3)
            var(1) = X1(ng,I,J,K,1)
            var(2) = (X1(ng,I,J,K,2) - var(1) * Mean(ng,I,J,K,2)) / Mean(ng,I,J,K,1)
            var(3) = (X1(ng,I,J,K,3) - var(1) * Mean(ng,I,J,K,3)) / Mean(ng,I,J,K,1)
            var(4) = (X1(ng,I,J,K,4) - var(1) * Mean(ng,I,J,K,4)) / Mean(ng,I,J,K,1)
            var(5) = (gamma-1.0_8)*(X1(ng,I,J,K,5) - 0.5_8 * var(1) * ( &
                      Mean(ng,I,J,K,2) * Mean(ng,I,J,K,2) + &
                      Mean(ng,I,J,K,3) * Mean(ng,I,J,K,3) + &
                      Mean(ng,I,J,K,4) * Mean(ng,I,J,K,4) ) - Mean(ng,I,J,K,3) * ( &
                      Mean(ng,I,J,K,2) * var(2) + &
                      Mean(ng,I,J,K,3) * var(3) + &
                      Mean(ng,I,J,K,4) * var(4)))

            X2(ng,I,J,K,1) = ( X2(ng,I,J,K,1) * dble(num-1) + var(1) * var(1) ) / dble(num)
            X2(ng,I,J,K,2) = ( X2(ng,I,J,K,2) * dble(num-1) + var(2) * var(2) ) / dble(num)
            X2(ng,I,J,K,3) = ( X2(ng,I,J,K,3) * dble(num-1) + var(3) * var(3) ) / dble(num)
            X2(ng,I,J,K,4) = ( X2(ng,I,J,K,4) * dble(num-1) + var(4) * var(4) ) / dble(num)
            X2(ng,I,J,K,5) = ( X2(ng,I,J,K,5) * dble(num-1) + var(5) * var(5) ) / dble(num)
          End Do
        End Do
      End Do
    End Do

    ! ... counter
    num = num + 1

    ! ... free memory
    deallocate(X1)

  End Do

  ! ... take square root
  Do ng = 1, ngrid
    Do I = 1, ND1(ng,1)
      Do J = 1, ND1(ng,2)
        Do K = 1, ND1(ng,3)
          Do L = 1, 5
            X2(ng,I,J,K,L) = dsqrt(X2(ng,I,J,K,L))
          End Do
        End Do
      End Do
    End Do
  End Do

  ! ... write solution
  if (associated(X2) .eqv. .true.) then
    Call Write_Soln(NDIM,ngrid,ND1,X2,tau,prec,gf,vf,outfile)
    Deallocate(ND1, X2)
  end if

  Stop 

End Program computeMean
