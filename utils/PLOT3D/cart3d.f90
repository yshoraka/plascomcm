! Copyright (c) 2015, University of Illinois at Urbana-Champaign, XPACC
! License: MIT, http://opensource.org/licenses/MIT
Program cart3d
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!                                                               !
! Generate a Cartesian grid in PLOT3D format                    !
!                                                               !
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!                                                               !
! Written by Daniel J. Bodony (bodony@stanford.edu)             !
! 26 April 2006                                                 !
!                                                               !
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  USE ModPLOT3D_IO
  Implicit None

  !  Arrays
  Real(KIND=8), Dimension(:,:,:,:,:), Pointer :: X1, Q1
  Real(KIND=8) :: XMIN, XMAX, YMIN, YMAX, ZMIN, ZMAX, Lx, Ly, Lz, dx, dy, dz

  !  Integers
  Integer :: ngrid(3), I, J, K, ISPERIODIC(3)
  Integer, Dimension(:,:), Pointer :: ND1
  Integer, Dimension(:,:,:,:), Pointer :: IBLANK1

  !  Logical
  Logical :: gf_exists(3), sf_exists(3)

  !  Characters
  Character(LEN=2)  :: prec(3), grid_format(3), volume_format(3), ib(3)
  Character(LEN=80) :: grid_file(3), cmd

  !  Step zero, read in the command line
  Integer :: iargc, nargc, ng

  nargc = iargc()
  If (nargc /= 1) Then
    Write (*,'(A)') 'USAGE: cart3d {grid file 1}'
    Stop
  Else
    Call Getarg(1,prec(1)); !print *, prec(1:1)
    Call Getarg(2,grid_format(1)); !print *, grid_format(1:1)
    Call Getarg(3,volume_format(1)); !print *, volume_format(1:1)
    Call Getarg(4,ib(1)); ! print *, ib(1:1)
    prec(1) = 'd'
    grid_format(1) = 'm'
    volume_format(1) = 'w'
    ib(1) = 'y'
    Call Getarg(1,grid_file(1)); !print *, grid_file(1)
    If ((prec(1) .NE. 's') .AND. (prec(1) .NE. 'd')) Then
      Write (*,'(A)') 'Invalid value for precision: use either "s" (single) or "d" (double)'
      Stop
    Else If ((grid_format(1) .NE. 's') .AND. (grid_format(1) .NE. 'm')) Then
      Write (*,'(A)') 'Invalid value for grid format 1: use either "s" (single) or "m" (multi)'
      Write (*,'(A)') '"'//grid_format(1)//'" was given.'
      Stop
    Else If ((volume_format(1) .NE. 'w') .AND. (volume_format(1) .NE. 'p')) Then
      Write (*,'(A)') 'Invalid value for volume format: use either "w" (whole) or "p" (planes)'
      Stop
    Else If ((ib(1) .NE. 'n') .AND. (ib(1) .NE. 'y')) Then
      Write (*,'(A)') 'Invalid value for IBLANK format: use either "n" (no) or "y" (yes)'
      Stop
    End If
  End If

  !  Step one, get parameters from user
  ngrid(1) = 1; Allocate(ND1(1,3))
  Write (*,'(A)',ADVANCE='NO') 'Input NX: '
  Read (*,*) ND1(1,1)
  Write (*,'(A)',ADVANCE='NO') 'Input NY: '
  Read (*,*) ND1(1,2)
  Write (*,'(A)',ADVANCE='NO') 'Input NZ: '
  Read (*,*) ND1(1,3)
  
  If (ND1(1,1) .GT. 1) THEN
    Write (*,'(A)',ADVANCE='NO') 'Input XMIN, XMAX: '
    Read (*,*) XMIN, XMAX
  Else
    XMIN = 0.0_8; XMAX = 0.0_8
  End if
  If (ND1(1,2) .GT. 1) THEN
    Write (*,'(A)',ADVANCE='NO') 'Input YMIN, YMAX: '
    Read (*,*) YMIN, YMAX
  Else
    YMIN = 0.0_8; YMAX = 0.0_8
  End if
  If (ND1(1,3) .GT. 1) THEN
    Write (*,'(A)',ADVANCE='NO') 'Input ZMIN, ZMAX: '
    Read (*,*) ZMIN, ZMAX
  Else
    ZMIN = 0.0_8; ZMAX = 0.0_8
  End if

  ! check for periodicity
  If (ND1(1,1) .GT. 1) THEN
    Write (*,'(A)',ADVANCE='NO') 'Is the x-dir periodic (0=no, 1=yes): '
    Read (*,*) ISPERIODIC(1)
    If (ISPERIODIC(1) == 1) Then
      Lx = XMAX - XMIN
      dx = Lx / DBLE(ND1(1,1))
      XMAX = XMAX - dx
      write (*,'(A,E14.6)') 'Taking periodic length to be Lx = ', Lx
      write (*,'(A,E14.6)') 'Adjusting XMAX to be ', XMAX
    End If
  End If
  If (ND1(1,2) .GT. 1) THEN
    Write (*,'(A)',ADVANCE='NO') 'Is the y-dir periodic (0=no, 1=yes): '
    Read (*,*) ISPERIODIC(2)
    If (ISPERIODIC(2) == 1) Then
      Ly = YMAX - YMIN
      dy = Ly / DBLE(ND1(1,2))
      YMAX = YMAX - dy
      write (*,'(A,E14.6)') 'Taking periodic length to be Ly = ', Ly
      write (*,'(A,E14.6)') 'Adjusting YMAX to be ', YMAX
    End If
  End If
  If (ND1(1,3) .GT. 1) THEN
    Write (*,'(A)',ADVANCE='NO') 'Is the z-dir periodic (0=no, 1=yes): '
    Read (*,*) ISPERIODIC(3)
    If (ISPERIODIC(3) == 1) Then
      Lz = ZMAX - ZMIN
      dz = Lz / DBLE(ND1(1,3))
      ZMAX = ZMAX - dz
      write (*,'(A,E14.6)') 'Taking periodic length to be Lz = ', Lz
      write (*,'(A,E14.6)') 'Adjusting ZMAX to be ', ZMAX
    End If
  End If
 
  ALLOCATE(X1(1,ND1(1,1),ND1(1,2),ND1(1,3),3))
  ALLOCATE(IBLANK1(1,ND1(1,1),ND1(1,2),ND1(1,3)))
  Do I = 1, ND1(1,1)
    DO J = 1, ND1(1,2)
      Do K = 1, ND1(1,3)
        If (ND1(1,1) .GT. 1) Then
          X1(1,I,J,K,1) = XMIN + DBLE(I-1)/DBLE(ND1(1,1)-1)*(XMAX-XMIN)
        Else
          X1(1,I,J,K,1) = XMIN
        End If
        If (ND1(1,2) .GT. 1) Then
          X1(1,I,J,K,2) = YMIN + DBLE(J-1)/DBLE(ND1(1,2)-1)*(YMAX-YMIN)
        Else
          X1(1,I,J,K,2) = YMIN
        End If
        If (ND1(1,3) .GT. 1) Then
          X1(1,I,J,K,3) = ZMIN + DBLE(K-1)/DBLE(ND1(1,3)-1)*(ZMAX-ZMIN)
        Else
          X1(1,I,J,K,3) = ZMIN
        End If
      End Do
    End Do
  End Do
  IBLANK1 = 1

  !  Step two, read in the mesh1 grid file
  Write (*,'(A)') ''
  Call Write_Grid(3,ngrid(1),ND1,X1,IBLANK1, &
                  prec(1),grid_format(1),volume_format(1),ib(1),grid_file(1))
  Write (*,'(A)') ''

  ! Step three, clean up & leave
  Deallocate(ND1, X1)

End Program cart3d
