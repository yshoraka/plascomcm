! Copyright (c) 2015, University of Illinois at Urbana-Champaign, XPACC
! License: MIT, http://opensource.org/licenses/MIT
program OversetSandbox

  use ModBC
  use ModOverset
  use ModPegasus
  use ModPLOT3D_IO
  implicit none

  integer(ik) :: i, j, k, m
  real(rk) :: U, V, W
  integer(ik) :: nDims
  integer(ik) :: nGrids
  integer(ik), dimension(:,:), allocatable :: nPoints
  type(t_bbox), dimension(:), allocatable :: Bounds
  real(rk), dimension(:,:), allocatable :: Lengths
  real(rk), dimension(:,:,:,:,:), allocatable :: X
  real(rk), dimension(:,:,:,:,:), allocatable :: Q
  integer(ik), dimension(:,:), allocatable :: Periodic
  integer(ik), dimension(:), allocatable :: PeriodicStorage
  real(rk), dimension(:,:), allocatable :: PeriodicLength
  integer(ik), dimension(:,:,:,:), allocatable :: IBlank
  integer(ik) :: Padding
  integer(ik), dimension(:), allocatable :: GridType
  type(t_grid), dimension(:), allocatable :: Grids
  type(t_interp), dimension(:), allocatable :: InterpData
  type(t_pegasus) :: PegasusData
  type(t_bc), dimension(:), allocatable :: BCs
  integer(ik), dimension(MAX_ND) :: iStartBC, iEndBC
  real(rk) :: Gamma
  real(rk) :: Temperature
  real(rk) :: Density
  real(rk), dimension(2) :: Velocity
  character(len=2)  :: prec, grid_format, volume_format, ib
  character(len=80) :: grid_file
  character(len=80) :: initial_file
  real(rk), dimension(4) :: tau
  integer(ik), dimension(:,:), pointer :: nPoints_PLOT3D
  real(rk), dimension(:,:,:,:,:), pointer :: X_PLOT3D
  integer(ik), dimension(:,:,:,:), pointer :: IBlank_PLOT3D
  real(rk), dimension(:,:,:,:,:), pointer :: Q_PLOT3D

  nDims = 2
  nGrids = 2

  allocate(nPoints(MAX_ND,nGrids))

  nPoints(:nDims,1) = [31,16]
  nPoints(:nDims,2) = [31,16]
  nPoints(nDims+1:,:) = 1

  allocate(Bounds(nGrids))
  allocate(Lengths(nDims,nGrids))

  Bounds(1) = t_bbox_(2, [-1._rk, -0.5_rk], [1._rk, 0.5_rk])
  Lengths(:,1) = BBSize(Bounds(1))

  Bounds(2) = t_bbox_(2, [-0.5_rk, -0.75_rk], [0.5_rk, -0.25_rk])
  Bounds(2)%b(1) = Bounds(2)%b(1) - 0.5_rk * Lengths(1,1)/real(nPoints(1,1)-1,kind=rk)
  Bounds(2)%e(1) = Bounds(2)%e(1) + 0.5_rk * Lengths(1,1)/real(nPoints(1,1)-1,kind=rk)
  Bounds(2)%b(2) = Bounds(2)%b(2) + 0.25_rk * Lengths(2,1)/real(nPoints(2,1)-1,kind=rk)
  Bounds(2)%e(2) = Bounds(2)%e(2) + 0.25_rk * Lengths(2,1)/real(nPoints(2,1)-1,kind=rk)

  Lengths(:,2) = BBSize(Bounds(2))

  allocate(X(nDims,maxval(nPoints(1,:)),maxval(nPoints(2,:)),maxval(nPoints(3,:)),nGrids))

  do k = 1, nPoints(3,1)
    do j = 1, nPoints(2,1)
      do i = 1, nPoints(1,1)
        U = real(i-1, kind=rk)/real(nPoints(1,1)-1, kind=rk)
        V = real(j-1, kind=rk)/real(nPoints(2,1)-1, kind=rk)
        X(:,i,j,k,1) = Bounds(1)%b(:2) + Lengths(:,1) * [U,V]
      end do
    end do
  end do

  do k = 1, nPoints(3,2)
    do j = 1, nPoints(2,2)
      do i = 1, nPoints(1,2)
        U = real(i-1, kind=rk)/real(nPoints(1,2)-1, kind=rk)
        V = real(j-1, kind=rk)/real(nPoints(2,2)-1, kind=rk)
        X(:,i,j,k,2) = Bounds(2)%b(:2) + Lengths(:,2) * [U,V]
      end do
    end do
  end do

  allocate(Periodic(MAX_ND,nGrids))
  allocate(PeriodicStorage(nGrids))
  allocate(PeriodicLength(MAX_ND,nGrids))

  Periodic = FALSE
  PeriodicStorage = NO_OVERLAP_PERIODIC
  PeriodicLength = 0._rk

  allocate(IBlank(maxval(nPoints(1,:)),maxval(nPoints(2,:)),maxval(nPoints(3,:)),nGrids))

  Padding = 8

  IBlank(:,:,:,1) = 1
  IBlank(:,:,:,2) = 1

  IBlank(1:Padding,:nPoints(2,2)/2-1,1,2) = 0
  IBlank(nPoints(1,2)-Padding+1:nPoints(1,2),:nPoints(2,2)/2-1,1,2) = 0

  allocate(BCs(11))

  BCs(1) = t_bc_(1, BC_TYPE_SAT_NOSLIP_ISOTHERMAL_WALL, 1, [1,1], [1,-1])
  BCs(2) = t_bc_(1, BC_TYPE_SAT_NOSLIP_ISOTHERMAL_WALL, 2, [1,-1], [1,1])
  BCs(3) = t_bc_(1, BC_TYPE_SAT_NOSLIP_ISOTHERMAL_WALL, -2, [1,-1], [-1,-1])
  BCs(4) = t_bc_(1, BC_TYPE_SAT_FAR_FIELD, -1, [-1,-1], [1,-1])
  BCs(5) = t_bc_(1, BC_TYPE_SPONGE, -1, [-5,-1], [1,-1])
  BCs(6) = t_bc_(2, BC_TYPE_SAT_NOSLIP_ISOTHERMAL_WALL, 2, [1,Padding+1], [8,8])
  BCs(7) = t_bc_(2, BC_TYPE_SAT_NOSLIP_ISOTHERMAL_WALL, 2, [-Padding-1,-1], [8,8])
  BCs(8) = t_bc_(2, BC_TYPE_SAT_NOSLIP_ISOTHERMAL_WALL, 1, [Padding+1,Padding+1], [1,7])
  BCs(9) = t_bc_(2, BC_TYPE_SAT_NOSLIP_ISOTHERMAL_WALL, -1, [-Padding-1,-Padding-1], [1,7])
  BCs(10) = t_bc_(2, BC_TYPE_SAT_FAR_FIELD, 2, [Padding+1,-Padding-1], [1,1])
  BCs(11) = t_bc_(2, BC_TYPE_SPONGE, 2, [Padding+1,-Padding-1], [1,5])

  do i = 1, size(BCs)
    if (BCs(i)%bc_type /= BC_TYPE_SPONGE) then
      m = BCs(i)%grid_id
      iStartBC = [BCs(i)%irange(1),BCs(i)%jrange(1),BCs(i)%krange(1)]
      iEndBC = [BCs(i)%irange(2),BCs(i)%jrange(2),BCs(i)%krange(2)]
      do j = 1, 3
        if (iStartBC(j) < 0) then
          iStartBC(j) = nPoints(j,m) + iStartBC(j) + 1
        end if
        if (iEndBC(j) < 0) then
          iEndBC(j) = nPoints(j,m) + iEndBC(j) + 1
        end if
      end do
      IBlank(iStartBC(1):iEndBC(1),iStartBC(2):iEndBC(2),iStartBC(3):iEndBC(3),m) = 4
    end if
  end do

  allocate(GridType(nGrids))

  GridType = GRID_TYPE_CARTESIAN

  allocate(Grids(nGrids))

  do m = 1, nGrids
    call MakeGrid(Grids(m), nDims=nDims, iStart=[(1,i=1,nDims)], iEnd=nPoints(:nDims,m), &
      Coords=X(:,:nPoints(1,m),:nPoints(2,m),:nPoints(3,m),m), Periodic=Periodic(:nDims,m), &
      PeriodicStorage=PeriodicStorage(m), PeriodicLength=PeriodicLength(:nDims,m), &
      IBlank=IBlank(:nPoints(1,m),:nPoints(2,m),:nPoints(3,m),m), ID=m, GridType=GridType(m))
  end do

  allocate(InterpData(nGrids))

  call AssembleOverset(Grids, InterpData, FringeSize=2, InterpScheme=INTERP_LINEAR, &
    BoundaryIBlank=4, OrphanIBlank=7)

  call MakePegasusData(Grids, InterpData, PegasusData)

  do m = 1, nGrids
    IBlank(:nPoints(1,m),:nPoints(2,m),:nPoints(3,m),m) = reshape(Grids(m)%iblank, &
      [nPoints(1,m),nPoints(2,m),nPoints(3,m)])
  end do

  do m = 1, nGrids
    do k = 1, nPoints(3,m)
      do j = 1, nPoints(2,m)
        do i = 1, nPoints(1,m)
          if (IBlank(i,j,k,m) == 4) then
            IBlank(i,j,k,m) = 1
          end if
        end do
      end do
    end do
  end do

  allocate(Q(nDims+2,maxval(nPoints(1,:)),maxval(nPoints(2,:)),maxval(nPoints(3,:)),nGrids))

  Gamma = 1.4_rk
  Temperature = 1._rk/(Gamma - 1._rk)
  Density = 1._rk
  Velocity = [0._rk, 0.2_rk]

  Q(1,:,:,:,:) = Density

  do m = 1, nGrids
    do k = 1, nPoints(3,m)
      do j = 1, nPoints(2,m)
        do i = 1, nPoints(1,m)
          if (m == 2 .and. j <= 5 .and. IBlank(i,j,k,m) == 1) then
            Q(2,i,j,k,m) = 0._rk
            Q(3,i,j,k,m) = Density * 0.2_rk
            Q(4,i,j,k,m) = Density * (Temperature/Gamma + 0.5_rk * sum(Velocity**2))
          else if (m == 1 .and. i >= nPoints(1,m)-4 .and. IBlank(i,j,k,m) == 1) then
            Q(2,i,j,k,m) = Density * 0.1_rk
            Q(3,i,j,k,m) = 0._rk
            Q(4,i,j,k,m) = Density * (Temperature/Gamma + 0.5_rk * sum(Velocity**2))
          else
            Q(2,i,j,k,m) = 0._rk
            Q(3,i,j,k,m) = 0._rk
            Q(4,i,j,k,m) = Density * Temperature/Gamma
          end if
        end do
      end do
    end do
  end do

  allocate(nPoints_PLOT3D(nGrids,MAX_ND))
  allocate(X_PLOT3D(nGrids,maxval(nPoints(1,:)),maxval(nPoints(2,:)),maxval(nPoints(3,:)),MAX_ND))
  allocate(IBlank_PLOT3D(nGrids,maxval(nPoints(1,:)),maxval(nPoints(2,:)),maxval(nPoints(3,:))))
  allocate(Q_PLOT3D(nGrids,maxval(nPoints(1,:)),maxval(nPoints(2,:)), maxval(nPoints(3,:)),MAX_ND+2))

  do m = 1, nGrids
    nPoints_PLOT3D(m,:) = nPoints(:,m)
  end do

  do m = 1, nGrids
    do k = 1, nPoints(3,m)
      do j = 1, nPoints(2,m)
        do i = 1, nPoints(1,m)
          X_PLOT3D(m,i,j,k,:nDims) = X(:,i,j,k,m)
          X_PLOT3D(m,i,j,k,nDims+1:) = 0._rk
          IBlank_PLOT3D(m,i,j,k) = IBlank(i,j,k,m)
        end do
      end do
    end do
  end do

  do m = 1, nGrids
    do k = 1, nPoints(3,m)
      do j = 1, nPoints(2,m)
        do i = 1, nPoints(1,m)
          Q_PLOT3D(m,i,j,k,1) = Q(1,i,j,k,m)
          Q_PLOT3D(m,i,j,k,2) = Q(2,i,j,k,m)
          Q_PLOT3D(m,i,j,k,3) = Q(3,i,j,k,m)
          if (nDims == 3) then
            Q_PLOT3D(m,i,j,k,4) = Q(4,i,j,k,m)
            Q_PLOT3D(m,i,j,k,5) = Q(5,i,j,k,m)
          else
            Q_PLOT3D(m,i,j,k,4) = 0._rk
            Q_PLOT3D(m,i,j,k,5) = Q(4,i,j,k,m)
          end if
        end do
      end do
    end do
  end do

  prec = "d" ! Precision: double
  grid_format = "m" ! Grid format: multiple
  volume_format = "w" ! Volume format: whole
  ib = "y" ! IBlank
  grid_file = "grid.xyz"
  initial_file = "initial.q"
  tau = 0._rk

  call Write_Grid(3, nGrids, nPoints_PLOT3D, X_PLOT3D, IBlank_PLOT3D, prec, grid_format, &
    volume_format, ib, grid_file)
  call Write_Soln(3, nGrids, nPoints_PLOT3D, Q_PLOT3D, tau, prec, grid_format, volume_format, &
    initial_file)

  call WriteBCs(BCs, "bc.dat")
  call WritePegasusData(PegasusData, "XINTOUT.HO.2D", "XINTOUT.X.2D")

  deallocate(nPoints_PLOT3D)
  deallocate(X_PLOT3D)
  deallocate(IBlank_PLOT3D)
  deallocate(Q_PLOT3D)

end program OversetSandbox
