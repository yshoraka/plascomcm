! Copyright (c) 2015, University of Illinois at Urbana-Champaign, XPACC
! License: MIT, http://opensource.org/licenses/MIT
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!                                                               !
! Compute [a] dependent variable[s] into a function file        !  
!                                                               !
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!                                                               !
! Written by Daniel J. Bodony (bodony@illinois.edu)             !
! 24 January 2013                                               !
!                                                               !
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

Program computeFunc

  USE ModPLOT3D_IO
  Implicit None

  !  Arrays
  Real(KIND=8), Dimension(:,:,:,:,:), Pointer :: Q, X, F
  Real(KIND=8) :: TAU(4), gam

  !  Integers
  Integer :: ngrid, I, J, K,  NX, NY, NZ, NDIM, READ_GRIDFILE
  Integer, Dimension(:,:), Pointer :: ND
  Integer, Dimension(:,:,:,:), Pointer :: IB

  !  Characters
  Character(LEN=2)  :: prec, gf, vf, ibf
  Character(LEN=80) :: gridFile, solnFile, funcFile

  !  Step zero, read in the command line
  Integer :: iargc, nargc, ng, isperiodic, ftype, num, numFiles

  nargc = iargc()
  If (nargc == 1 .or. nargc > 3) Then
    Write (*,'(A)') 'USAGE: computeFunc [gridFile] <solnFile> <funcFile>'
    Write (*,'(A)') 'USAGE: gridFile is optional and will cause program to avoid'
    Write (*,'(A)') 'USAGE: those areas with IBLANK = 0.'
    Stop
  End If

  ! ... read command line
  If (nargc == 2) Then
    READ_GRIDFILE = 0
    Call Getarg(1,solnFile); 
    Call Getarg(2,funcFile); 
  End If

  If (nargc == 3) Then
    READ_GRIDFILE = 1
    Call Getarg(1,gridFile);
    Call Getarg(2,solnFile);
    Call Getarg(3,funcFile);
  End If

  ! ... read grid, if desired
  If (READ_GRIDFILE == 1) Call Read_Grid(NDIM,ngrid,ND,X,IB,prec,gf,vf,ibf,gridFile,0)

  ! ... read soln
  Call Read_Soln(NDIM,ngrid,ND,Q,TAU,prec,gf,vf,solnFile,0)

  ! ... compute function
  NX = MAXVAL(ND(:,1))
  NY = MAXVAL(ND(:,2))
  NZ = MAXVAL(ND(:,3)) 
  allocate(X(ngrid,NX,NY,NZ,1))
  gam = 1.4_8
  Do ng = 1, ngrid
    Do K = 1, ND(ng,3)
      Do J = 1, ND(ng,2)
        Do I = 1, ND(ng,1)
          X(ng,I,J,K,1) = (gam - 1.0_8)*(Q(ng,I,J,K,5) - 0.5_8*(Q(ng,I,J,K,2)**2 + Q(ng,I,J,K,3)**2 + Q(ng,I,J,K,4)**2)/Q(ng,I,J,K,1)) - 1.0_8 / gam
        End Do
      End Do
    End Do
  End Do

  ! ... write function
  print *, ngrid,ND
  Call Write_Func(ngrid,ND,X,funcFile)
          
  Stop

End Program computeFunc
