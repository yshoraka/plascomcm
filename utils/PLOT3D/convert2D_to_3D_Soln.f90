! Copyright (c) 2015, University of Illinois at Urbana-Champaign, XPACC
! License: MIT, http://opensource.org/licenses/MIT
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!                                                               !
! Convert a 2-D PLOT3D file to a 3-D file                       !
!                                                               !
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!                                                               !
! Written by Daniel J. Bodony (bodony@illinois.edu)             !
! 31 May 2009                                                   !
!                                                               !
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

Program convert2D_to_3D_soln

  USE ModPLOT3D_IO
  Implicit None

  !  Arrays
  Real(KIND=8), Dimension(:,:,:,:,:), Pointer :: X1, X2
  Real(KIND=8) :: XMIN, XMAX, YMIN, YMAX, ZMIN, ZMAX, TAU(4)

  !  Integers
  Integer :: ngrid(3), I, J, K, dir, NX, NY, NZ, NDIM(3)
  Integer, Dimension(:,:), Pointer :: ND1, ND2
  Integer, Dimension(:,:,:,:), Pointer :: IBLANK1, IBLANK2

  !  Logical
  Logical :: gf_exists(3), sf_exists(3)

  !  Characters
  Character(LEN=2)  :: prec(3), gf(3), vf(3), ib(3)
  Character(LEN=80) :: grid_file(3), cmd

  !  Step zero, read in the command line
  Integer :: iargc, nargc, ng, isperiodic, ftype

  nargc = iargc()
  If (nargc /= 2) Then
    Write (*,'(A)') 'USAGE: convert2D_to_3D_Soln {2-D Soln file} {3-D Soln file}'
    Stop
  Endif

  Call Getarg(1,grid_file(1));
  Call Getarg(2,grid_file(2));

  Call p3d_detect(LEN(Trim(grid_file(1))),Trim(grid_file(1)),prec,NDIM,gf,vf,ib,ftype)

  !  Step one, read in the mesh1 grid file
  Write (*,'(A)') ''
  Call Read_Soln(NDIM(1),ngrid(1),ND1,X1,TAU, &
       prec(1),gf(1),vf(1),grid_file(1),.TRUE.)
  Write (*,'(A)') ''

  !  Step two, get parameters from user
  ngrid(2) = ngrid(1)
  NDIM(2) = 3
  Allocate(ND2(ngrid(2),3)); ND2(:,:) = 1
  ND2(1:NDIM(1),:) = ND1(1:NDIM(1),:)

  NX = MAXVAL(ND2(:,1))
  NY = MAXVAL(ND2(:,2))
  NZ = MAXVAL(ND2(:,3))
  ALLOCATE(X2(ngrid(2),NX,NY,NZ,5))

  Do ng = 1, ngrid(1)
    Do I = 1, ND1(ng,1)
      Do J = 1, ND1(ng,2)
        Do K = 1, ND1(ng,3)
          X2(ng,I,J,K,1) = X1(ng,I,J,K,1)
          X2(ng,I,J,K,2) = X1(ng,I,J,K,2)
          X2(ng,I,J,K,3) = X1(ng,I,J,K,3)
          X2(ng,I,J,K,5) = X1(ng,I,J,K,4)
        End Do
      End Do
    End Do
  End Do

  Call Write_Soln(NDIM(2),ngrid(2),ND2,X2,tau,&
       prec(1),gf(1),vf(1),grid_file(2))

  Write (*,'(A)') ''

  ! Step three, clean up & leave
  Deallocate(ND1, X1, ND2, X2)


  Stop
End Program convert2D_to_3D_Soln
