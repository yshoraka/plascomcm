These routines are distributed as part of SLATEC found here: http://netlib.org/slatec/guide

Section 4 of http://netlib.org/slatec/guide states:

"""
SECTION 4.  OBTAINING THE LIBRARY

The Library is in the public domain and distributed by the Energy Science
and Technology Software Center.

               Energy Science and Technology Software Center
               P.O. Box 1020
               Oak Ridge, TN  37831

               Telephone  615-576-2606
               E-mail  estsc%a1.adonis.mrouter@zeus.osti.gov
"""
