# Copyright (c) 2015, University of Illinois at Urbana-Champaign, XPACC
# License: MIT, http://opensource.org/licenses/MIT
system_tests = \
  AxisymmetricTests \
  ChemistryTests \
  CompareTests \
  ElectricTests \
  HDF5IOTests \
  HDF5UtilsTests \
  FluidTests \
  GeometryTests \
  InputTests \
  PLOT3DIOTests
