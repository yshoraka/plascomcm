#! /bin/sh
# Copyright (c) 2015, University of Illinois at Urbana-Champaign, XPACC
# License: MIT, http://opensource.org/licenses/MIT

# Configuration stuff
abs_top_builddir__="@abs_top_builddir@"
mpiexec__="@MPIEXEC@"
spawner__="@SPAWNER@"
HAVE_BATCH="@HAVE_BATCH@"
HAVE_HDF5="@HAVE_HDF5@"
HAVE_CANTERA="@HAVE_CANTERA@"
HAVE_PETSC="@HAVE_PETSC@"
AXISYMMETRIC="@AXISYMMETRIC@"
BUILD_ELECTRIC="@BUILD_ELECTRIC@"
BUILD_COVERAGE="@BUILD_COVERAGE@"
extra_args=""
export F_UFMTENDIAN=big
plot3d_function_extensions__="aux met jac phi"

# Useful locations
TESTDATA="$abs_top_builddir/tests/system/data"
TESTUTILS="$abs_top_builddir/tests/system/utils"
SRC="$abs_top_builddir/src"
UTILS="$abs_top_builddir/utils"
PLOT3DUTILS="$abs_top_builddir/utils/PLOT3D"
HDF5UTILS="$abs_top_builddir/utils/HDF5"

# File comparison utilities
compare_p3d__="$PLOT3DUTILS/compare-p3d"
compare_h5__="$HDF5UTILS/compare-h5"

# replace this with a list of how to run batch jobs on each platform
# consider a dedicated host file for this
# don't do anything if CMake set mpiexec
if [ "$HAVE_BATCH" = 0 ]; then
  if [ "${mpiexec__#*srun}" != "${mpiexec__}" ]; then
    extra_args="-ppdebug"
  fi
  if [ "${spawner__#*ibrun}" != "${spawner__}" ]; then
    extra_args="-pdevelopment --pty ibrun"
  fi
fi

RunSim() {

  if [ $# -eq 3 ]; then
    inputfile__="$1"
    restartfile__="$2"
    nprocs__=$3
  elif [ $# -eq 2 ]; then
    inputfile__="$1"
    restartfile__=""
    nprocs__=$2
  else
    PrintError "Incorrect number of arguments to RunSim"
    FailTest
  fi

  plascomcm__="$abs_top_builddir__/src/plascomcm"

  if [ "$BUILD_COVERAGE" -eq 1 ]; then
    nprocs__=1
  fi
 
  TryExecJob $nprocs__ "$plascomcm__" --input="$inputfile__" "$restartfile__"
    
  if [ $RESULT -ne 0 ]; then
    PrintError "RunSim: Error occurred while running simulation"
    PrintError "Input file = $inputfile__"
    if [ ! -z "$restartfile__" ]; then
      PrintError "Restart file = $restartfile__"
    fi
    PrintError "Number of processes = $nprocs__"
    FailTest
  fi

}

AssertFileExists() {

  if [ $# -eq 1 ]; then
    file__=$1
  else
    PrintError "Incorrect number of arguments to AssertFileExists"
    FailTest
  fi

  if [ ! -f "$file__" ]; then
    PrintError "AssertFileExists: File does not exist"
    PrintError "File = $file__"
    FailTest
  fi

}

AssertDirExists() {

  if [ $# -eq 1 ]; then
    dir__=$1
  else
    PrintError "Incorrect number of arguments to AssertDirExists"
    FailTest
  fi

  if [ ! -d "$dir__" ]; then
    PrintError "AssertDirectoryExists: Directory does not exist"
    PrintError "Directory = $dir__"
    FailTest
  fi

}

determine_plot3d_file_type__() {

  case "$1" in
    *.xyz) filetype__=grid;;
    *.q) filetype__=;;
    *.f) filetype__=func;;
  esac

  if [ -z $filetype__ ]; then
    filetype__=solution
    for ext__ in $plot3d_function_extensions__; do
      if [ "${1%.${ext__}.q}" != "$1" ]; then
        filetype__=func
      fi
    done
  fi

}

compare_equal__() {

  case "$1" in
    *.h5) fileformat__=HDF5;;
    *.xyz|*.q|*.f) fileformat__=PLOT3D;;
    *.txt,*.xmf,*.dat) fileformat__=TEXT;;
    *) fileformat__=;;
  esac

  if [ $fileformat__ == HDF5 ]; then
    TryExecJob 1 "$compare_h5__" --compare-mode=inf "$1" "$2"
  elif [ $fileformat__ == PLOT3D ]; then
    determine_plot3d_file_type__ "$1"
    TryExec "$compare_p3d__" --file-type=$filetype__ --compare-mode=inf "$1" "$2"
  elif [ $fileformat__ == TEXT ]; then
    TryExec diff "$1" "$2"
  else
    # Exit code 2 indicates failure to compare files
    TryExec "(exit 2)"
  fi

}

compare_almost_equal__() {

  case "$1" in
    *.h5) fileformat__=HDF5;;
    *.xyz|*.q|*.f) fileformat__=PLOT3D;;
    *) fileformat__=;;
  esac

  if [ $fileformat__ == HDF5 ]; then
    TryExecJob 1 "$compare_h5__" --compare-mode=$3 --tolerance=$4 "$1" "$2"
  elif [ $fileformat__ == PLOT3D ]; then
    determine_plot3d_file_type__ "$1"
    TryExec "$compare_p3d__" --file-type=$filetype__ --compare-mode=$3 --tolerance=$4 "$1" "$2"
  else
    # Exit code 2 indicates failure to compare files
    TryExec "(exit 2)"
  fi

}

AssertEqual() {

  if [ $# -eq 2 ]; then
    file1__="$1"
    file2__="$2"
  else
    PrintError "Incorrect number of arguments to AssertEqual"
    FailTest
  fi

  compare_equal__ "$file1__" "$file2__"

  if [ $RESULT -eq 1 ]; then
    PrintError "AssertEqual: Files are not equal"
    PrintError "File 1 = $file1__"
    PrintError "File 2 = $file2__"
    FailTest
  elif [ $RESULT -gt 1 ]; then
    PrintError "AssertEqual: Could not compare files"
    PrintError "File 1 = $file1__"
    PrintError "File 2 = $file2__"
    FailTest
  fi

}

AssertNotEqual() {

  if [ $# -eq 2 ]; then
    file1__="$1"
    file2__="$2"
  else
    PrintError "Incorrect number of arguments to AssertNotEqual"
    FailTest
  fi

  compare_equal__ "$file1__" "$file2__"

  if [ $RESULT -eq 0 ]; then
    PrintError "AssertNotEqual: Files are equal"
    PrintError "File 1 = $file1__"
    PrintError "File 2 = $file2__"
    FailTest
  elif [ $RESULT -gt 1 ]; then
    PrintError "AssertNotEqual: Could not compare files"
    PrintError "File 1 = $file1__"
    PrintError "File 2 = $file2__"
    FailTest
  fi

}

AssertAlmostEqual() {

  if [ $# -eq 4 ]; then
    file1__="$1"
    file2__="$2"
    tolerance__=$3
    comparemode__=$4
  elif [ $# -eq 3 ]; then
    file1__="$1"
    file2__="$2"
    tolerance__=$3
    comparemode__=inf
  else
    PrintError "Incorrect number of arguments to AssertAlmostEqual"
    FailTest
  fi

  compare_almost_equal__ "$file1__" "$file2__" $comparemode__ $tolerance__

  if [ $RESULT -eq 1 ]; then
    PrintError "AssertAlmostEqual: Files are not almost equal"
    PrintError "File 1 = $file1__"
    PrintError "File 2 = $file2__"
    PrintError "Tolerance = $tolerance__"
    PrintError "Comparison mode = $comparemode__"
    FailTest
  elif [ $RESULT -gt 1 ]; then
    PrintError "AssertAlmostEqual: Could not compare files"
    PrintError "File 1 = $file1__"
    PrintError "File 2 = $file2__"
    PrintError "Tolerance = $tolerance__"
    PrintError "Comparison mode = $comparemode__"
    FailTest
  fi

}

AssertNotAlmostEqual() {

  if [ $# -eq 4 ]; then
    file1__="$1"
    file2__="$2"
    tolerance__=$3
    comparemode__=$4
  elif [ $# -eq 3 ]; then
    file1__="$1"
    file2__="$2"
    tolerance__=$3
    comparemode__=inf
  else
    PrintError "Incorrect number of arguments to AssertNotAlmostEqual"
    FailTest
  fi

  compare_almost_equal__ "$file1__" "$file2__" $comparemode__ $tolerance__

  if [ $RESULT -eq 0 ]; then
    PrintError "AssertNotoAlmostEqual: Files are almost equal"
    PrintError "File 1 = $file1__"
    PrintError "File 2 = $file2__"
    PrintError "Tolerance = $tolerance__"
    PrintError "Comparison mode = $comparemode__"
    FailTest
  elif [ $RESULT -gt 1 ]; then
    PrintError "AssertAlmostEqual: Could not compare files"
    PrintError "File 1 = $file1__"
    PrintError "File 2 = $file2__"
    PrintError "Tolerance = $tolerance__"
    PrintError "Comparison mode = $comparemode__"
    FailTest
  fi

}

AssertTrue() {

  TryExec "$@"

  if [ $RESULT -eq 1 ]; then
    PrintError "AssertTrue: Command returned false"
    PrintError "Command: $*"
    FailTest
  elif [ $RESULT -gt 1 ]; then
    PrintError "AssertTrue: Error occurred during command execution"
    PrintError "Command: $*"
    FailTest
  fi

}

AssertFalse() {

  TryExec "$@"

  if [ $RESULT -eq 0 ]; then
    PrintError "AssertFalse: Command returned true"
    PrintError "Command: $*"
    FailTest
  elif [ $RESULT -gt 1 ]; then
    PrintError "AssertFalse: Error occurred during command execution"
    PrintError "Command: $*"
    FailTest
  fi

}
