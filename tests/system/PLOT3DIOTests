#! /bin/sh
# Copyright (c) 2015, University of Illinois at Urbana-Champaign, XPACC
# License: MIT, http://opensource.org/licenses/MIT

# Check that grid files are read and written properly in single-grid 2D case
ReadWriteGrid2DSingle() {
  Exec tar -xzvf $TESTDATA/$SUITE/2DSingle.tar.gz
  RunSim $TESTDATA/$SUITE/2DSingle_input.inp 4
  AssertEqual grid_expected.xyz RocFlo-CM.00000000.xyz
}
AddTest ReadWriteGrid2DSingle

# Check that grid files are read and written properly in multiple-grid 2D case
ReadWriteGrid2DMultiple() {
  Exec tar -xzvf $TESTDATA/$SUITE/2DMultiple.tar.gz
  RunSim $TESTDATA/$SUITE/2DMultiple_input.inp 4
  AssertEqual grid_expected.xyz RocFlo-CM.00000000.xyz
}
AddTest ReadWriteGrid2DMultiple

# Check that grid files are read and written properly in single-grid 3D case
ReadWriteGrid3DSingle() {
  Exec tar -xzvf $TESTDATA/$SUITE/3DSingle.tar.gz
  RunSim $TESTDATA/$SUITE/3DSingle_input.inp 4
  AssertEqual grid_expected.xyz RocFlo-CM.00000000.xyz
}
AddTest ReadWriteGrid3DSingle

# Check that grid files are read and written properly in multiple-grid 3D case
ReadWriteGrid3DMultiple() {
  Exec tar -xzvf $TESTDATA/$SUITE/3DMultiple.tar.gz
  RunSim $TESTDATA/$SUITE/3DMultiple_input.inp 4
  AssertEqual grid_expected.xyz RocFlo-CM.00000000.xyz
}
AddTest ReadWriteGrid3DMultiple

# Check that solution files are written properly in single-grid 2D case
WriteSolution2DSingle() {
  Exec tar -xzvf $TESTDATA/$SUITE/2DSingle.tar.gz
  RunSim $TESTDATA/$SUITE/2DSingle_input.inp 4
  AssertEqual soln_expected.q RocFlo-CM.00000000.q
}
AddTest WriteSolution2DSingle

# Check that solution files are written properly in multiple-grid 2D case
WriteSolution2DMultiple() {
  Exec tar -xzvf $TESTDATA/$SUITE/2DMultiple.tar.gz
  RunSim $TESTDATA/$SUITE/2DMultiple_input.inp 4
  AssertEqual soln_expected.q RocFlo-CM.00000000.q
}
AddTest WriteSolution2DMultiple

# Check that solution files are written properly in single-grid 3D case
WriteSolution3DSingle() {
  Exec tar -xzvf $TESTDATA/$SUITE/3DSingle.tar.gz
  RunSim $TESTDATA/$SUITE/3DSingle_input.inp 4
  AssertEqual soln_expected.q RocFlo-CM.00000000.q
}
AddTest WriteSolution3DSingle

# Check that solution files are written properly in multiple-grid 3D case
WriteSolution3DMultiple() {
  Exec tar -xzvf $TESTDATA/$SUITE/3DMultiple.tar.gz
  RunSim $TESTDATA/$SUITE/3DMultiple_input.inp 4
  AssertEqual soln_expected.q RocFlo-CM.00000000.q
}
AddTest WriteSolution3DMultiple

# Compiling in axisymmetric mode breaks these tests because they compare solutions that are computed
# after a timestep rather than initial solutions. Consider making axisymmetric a runtime option so
# these tests can run unconditionally?
if [ $AXISYMMETRIC -eq 0 ]; then

# Check that restart files are read and written properly in single-grid 2D case
ReadWriteRestart2DSingle() {
  Exec tar -xzvf $TESTDATA/$SUITE/2DSingle.tar.gz
  RunSim $TESTDATA/$SUITE/2DSingle_input.inp initial.q 4
  AssertAlmostEqual restart_expected.q RocFlo-CM.00000001.restart 1.e-10 2
}
AddTest ReadWriteRestart2DSingle

# Check that restart files are read and written properly in multiple-grid 2D case
ReadWriteRestart2DMultiple() {
  Exec tar -xzvf $TESTDATA/$SUITE/2DMultiple.tar.gz
  RunSim $TESTDATA/$SUITE/2DMultiple_input.inp initial.q 4
  AssertAlmostEqual restart_expected.q RocFlo-CM.00000001.restart 1.e-10 2
}
AddTest ReadWriteRestart2DMultiple

# Check that restart files are read and written properly in single-grid 3D case
ReadWriteRestart3DSingle() {
  Exec tar -xzvf $TESTDATA/$SUITE/3DSingle.tar.gz
  RunSim $TESTDATA/$SUITE/3DSingle_input.inp initial.q 4
  AssertAlmostEqual restart_expected.q RocFlo-CM.00000001.restart 1.e-10 2
}
AddTest ReadWriteRestart3DSingle

# Check that restart files are read and written properly in multiple-grid 3D case
ReadWriteRestart3DMultiple() {
  Exec tar -xzvf $TESTDATA/$SUITE/3DMultiple.tar.gz
  RunSim $TESTDATA/$SUITE/3DMultiple_input.inp initial.q 4
  AssertAlmostEqual restart_expected.q RocFlo-CM.00000001.restart 1.e-10 2
}
AddTest ReadWriteRestart3DMultiple

fi
