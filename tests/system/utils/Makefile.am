# Copyright (c) 2015, University of Illinois at Urbana-Champaign, XPACC
# License: MIT, http://opensource.org/licenses/MIT
# Initialize variables
EXTRA_DIST =
bin_PROGRAMS =

if BUILD_TESTS
# For axisymmetric verification test
bin_PROGRAMS += checkrate
# For Model-0 verification test
bin_PROGRAMS += checkignitiondelay
# Efield test case input generator
bin_PROGRAMS += ElectricTestCases
# Grid generation tests
bin_PROGRAMS += CheckGeometry
# Comparison utility test data generator (PLOT3D)
bin_PROGRAMS += CompareTestPLOT3DData
if HAVE_HDF5
# Comparison utility test data generator (HDF5)
bin_PROGRAMS += CompareTestHDF5Data
endif
endif

LDADD_UTILS = -L../../../utils -lutils
DEPS_UTILS = ../../../utils/libutils.a
LDADD_PLOT3D = -L../../../utils/PLOT3D -lplot3d
DEPS_PLOT3D = ../../../utils/PLOT3D/libplot3d.a
if HAVE_HDF5
LDADD_HDF5 = -L../../../utils/HDF5 -lhdf5io
DEPS_HDF5 = ../../../utils/HDF5/libhdf5io.a
endif

checkrate_SOURCES = checkRate.f90
checkrate_LDADD = $(LDADD) $(LDADD_UTILS) $(LDADD_PLOT3D)
checkrate_DEPENDENCIES = $(DEPS_UTILS) $(DEPS_PLOT3D)
checkignitiondelay_SOURCES = checkIgnitionDelay.f90
checkignitiondelay_LDADD = $(LDADD) $(LDADD_UTILS)
checkignitiondelay_DEPENDENCIES = $(DEPS_UTILS)
ElectricTestCases_SOURCES = ElectricTestCases.f90
ElectricTestCases_LDADD = $(LDADD) $(LDADD_UTILS) $(LDADD_PLOT3D)
ElectricTestCases_DEPENDENCIES = $(DEPS_UTILS) $(DEPS_PLOT3D)
CheckGeometry_SOURCES = CheckGeometry.f90
CheckGeometry_LDADD = $(LDADD) $(LDADD_UTILS) $(LDADD_PLOT3D)
CheckGeometry_DEPENDENCIES = $(DEPS_UTILS) $(DEPS_PLOT3D)
CompareTestPLOT3DData_SOURCES = CompareTestPLOT3DData.f90
CompareTestPLOT3DData_LDADD = $(LDADD) $(LDADD_UTILS) $(LDADD_PLOT3D)
CompareTestPLOT3DData_DEPENDENCIES = $(DEPS_UTILS) $(DEPS_PLOT3D)
if HAVE_HDF5
CompareTestHDF5Data_SOURCES = CompareTestHDF5Data.f90
CompareTestHDF5Data_LDADD = $(LDADD) $(LDADD_UTILS) $(LDADD_HDF5)
CompareTestHDF5Data_DEPENDENCIES = $(DEPS_UTILS) $(DEPS_HDF5)
endif

UTILSMODDIR = ../../../utils/mod
PLOT3DMODDIR = ../../../utils/PLOT3D

if HAVE_HDF5
HDF5MODDIR = ../../../utils/HDF5
endif

FC_MODEXT = @FC_MODEXT@
FC_MODINC   = @FC_MODINC@
FC_MODOUT   = @FC_MODOUT@
FC_MODCASE  = @FC_MODCASE@
AM_FCFLAGS = $(FC_MODOUT). $(FC_MODINC). $(FC_MODINC)$(UTILSMODDIR) $(FC_MODINC)$(PLOT3DMODDIR)

if HAVE_HDF5
AM_FCFLAGS += $(FC_MODINC)$(HDF5MODDIR)
endif

clean-local:
	rm -f *.$(FC_MODEXT)
	rm -f obj/*
	rm -rf *.dSYM
