program CompareTestPLOT3DData

  use ModBC
  use ModParseArguments
  use ModStdIO
  use ModPLOT3D_IO
  implicit none

  integer, parameter :: MAX_ND = 3
  integer, parameter :: rk = kind(0.d0)

  integer :: i
  character(len=CMD_ARG_LENGTH), dimension(:), allocatable :: RawArguments, Arguments

  allocate(RawArguments(command_argument_count()))
  do i = 1, size(RawArguments)
    call get_command_argument(i, RawArguments(i))
  end do

  call ParseArguments(RawArguments, Arguments=Arguments, MinArguments=1, MaxArguments=1)

  select case (trim(Arguments(1)))
  case ("ComparePLOT3DGrid2D")
    call ComparePLOT3DGrid2D()
  case ("ComparePLOT3DGrid3D")
    call ComparePLOT3DGrid3D()
  case ("ComparePLOT3DSolution2D")
    call ComparePLOT3DSolution2D()
  case ("ComparePLOT3DSolution3D")
    call ComparePLOT3DSolution3D()
  case ("ComparePLOT3DFunction2D")
    call ComparePLOT3DFunction2D()
  case ("ComparePLOT3DFunction3D")
    call ComparePLOT3DFunction3D()
  case default
    write (ERROR_UNIT, '(3a)') "ERROR: Unknown case ID '", trim(Arguments(1)), "'."
    stop 1
  end select

contains

  subroutine ComparePLOT3DGrid2D

    integer, parameter :: nGrids = 2
    integer, parameter :: nDims = 2

    integer :: i, j, m
    real(rk) :: U, V
    integer, dimension(:,:), pointer :: nPoints
    real(rk), dimension(nGrids,nDims) :: Lengths
    real(rk), dimension(:,:,:,:,:), pointer :: X
    real(rk), dimension(:,:,:,:,:), pointer :: XCompare
    integer, dimension(:,:,:,:), pointer :: IBlank
    integer, dimension(:,:,:,:), pointer :: IBlankCompare
    character(len=2)  :: GridFormat, VolumeFormat, CoordPrecision, UseIBlank
    character(len=80) :: GridFile

    CoordPrecision = "d"
    GridFormat = "m"
    VolumeFormat = "w"
    UseIBlank = "y"

    allocate(nPoints(nGrids,MAX_ND))

    nPoints = 1
    nPoints(1,:nDims) = [20,30]
    nPoints(2,:nDims) = [10,20]

    Lengths(1,:) = [2._rk, 3._rk]
    Lengths(2,:) = [1._rk, 2._rk]

    allocate(X(nGrids,maxval(nPoints(:,1)),maxval(nPoints(:,2)),maxval(nPoints(:,3)),nDims))
    X = 0._rk

    do m = 1, nGrids
      do j = 1, nPoints(m,2)
        do i = 1, nPoints(m,1)
          U = real(i-1, kind=rk)/real(nPoints(m,1)-1, kind=rk)
          V = real(j-1, kind=rk)/real(nPoints(m,2)-1, kind=rk)
          X(m,i,j,1,:) = Lengths(m,:) * ([U,V]-0.5_rk)
        end do
      end do
    end do

    allocate(IBlank(nGrids,maxval(nPoints(:,1)),maxval(nPoints(:,2)),maxval(nPoints(:,3))))
    IBlank = 1

    GridFile = "grid_2d.xyz"
    call Write_Grid(nDims, nGrids, nPoints, X, IBlank, CoordPrecision, GridFormat, &
      VolumeFormat, UseIBlank, GridFile)

    allocate(XCompare(nGrids,maxval(nPoints(:,1)),maxval(nPoints(:,2)),maxval(nPoints(:,3)),nDims))
    allocate(IBlankCompare(nGrids,maxval(nPoints(:,1)),maxval(nPoints(:,2)),maxval(nPoints(:,3))))

    ! Perturb the coordinates slightly
    XCompare = X
    do m = 1, nGrids
      do j = 1, nPoints(m,2)
        do i = 1, nPoints(m,1)
          XCompare(m,i,j,1,:) = XCompare(m,i,j,1,:) + 1.e-10_rk
        end do
      end do
    end do

    GridFile = "grid_2d_almost.xyz"
    call Write_Grid(nDims, nGrids, nPoints, XCompare, IBlank, CoordPrecision, GridFormat, &
      VolumeFormat, UseIBlank, GridFile)

    ! Perturb the coordinates by a large amount
    XCompare = X
    do m = 1, nGrids
      do j = 1, nPoints(m,2)
        do i = 1, nPoints(m,1)
          XCompare(m,i,j,1,:) = XCompare(m,i,j,1,:) + 1._rk
        end do
      end do
    end do

    GridFile = "grid_2d_different_xyz.xyz"
    call Write_Grid(nDims, nGrids, nPoints, XCompare, IBlank, CoordPrecision, GridFormat, &
      VolumeFormat, UseIBlank, GridFile)

    ! Modify the IBlank values
    IBlankCompare = 0

    GridFile = "grid_2d_different_iblank.xyz"
    call Write_Grid(nDims, nGrids, nPoints, X, IBlankCompare, CoordPrecision, GridFormat, &
      VolumeFormat, UseIBlank, GridFile)

    deallocate(X)
    deallocate(XCompare)
    deallocate(IBlank)
    deallocate(IBlankCompare)

  end subroutine ComparePLOT3DGrid2D

  subroutine ComparePLOT3DGrid3D

    integer, parameter :: nGrids = 2
    integer, parameter :: nDims = 3

    integer :: i, j, k, m
    real(rk) :: U, V, W
    integer, dimension(:,:), pointer :: nPoints
    real(rk), dimension(nGrids,nDims) :: Lengths
    real(rk), dimension(:,:,:,:,:), pointer :: X
    real(rk), dimension(:,:,:,:,:), pointer :: XCompare
    integer, dimension(:,:,:,:), pointer :: IBlank
    integer, dimension(:,:,:,:), pointer :: IBlankCompare
    character(len=2)  :: GridFormat, VolumeFormat, CoordPrecision, UseIBlank
    character(len=80) :: GridFile

    CoordPrecision = "d"
    GridFormat = "m"
    VolumeFormat = "w"
    UseIBlank = "y"

    allocate(nPoints(nGrids,MAX_ND))

    nPoints = 1
    nPoints(1,:nDims) = [20,30,40]
    nPoints(2,:nDims) = [10,20,30]

    Lengths(1,:) = [2._rk, 3._rk, 4._rk]
    Lengths(2,:) = [1._rk, 2._rk, 3._rk]

    allocate(X(nGrids,maxval(nPoints(:,1)),maxval(nPoints(:,2)),maxval(nPoints(:,3)),nDims))
    X = 0._rk

    do m = 1, nGrids
      do k = 1, nPoints(m,3)
        do j = 1, nPoints(m,2)
          do i = 1, nPoints(m,1)
            U = real(i-1, kind=rk)/real(nPoints(m,1)-1, kind=rk)
            V = real(j-1, kind=rk)/real(nPoints(m,2)-1, kind=rk)
            W = real(k-1, kind=rk)/real(nPoints(m,3)-1, kind=rk)
            X(m,i,j,k,:) = Lengths(m,:) * ([U,V,W]-0.5_rk)
          end do
        end do
      end do
    end do

    allocate(IBlank(nGrids,maxval(nPoints(:,1)),maxval(nPoints(:,2)),maxval(nPoints(:,3))))
    IBlank = 1

    GridFile = "grid_3d.xyz"
    call Write_Grid(nDims, nGrids, nPoints, X, IBlank, CoordPrecision, GridFormat, &
      VolumeFormat, UseIBlank, GridFile)

    allocate(XCompare(nGrids,maxval(nPoints(:,1)),maxval(nPoints(:,2)),maxval(nPoints(:,3)),nDims))
    allocate(IBlankCompare(nGrids,maxval(nPoints(:,1)),maxval(nPoints(:,2)),maxval(nPoints(:,3))))

    ! Perturb the coordinates slightly
    XCompare = X
    do m = 1, nGrids
      do k = 1, nPoints(m,3)
        do j = 1, nPoints(m,2)
          do i = 1, nPoints(m,1)
            XCompare(m,i,j,k,:) = XCompare(m,i,j,k,:) + 1.e-10_rk
          end do
        end do
      end do
    end do

    GridFile = "grid_3d_almost.xyz"
    call Write_Grid(nDims, nGrids, nPoints, XCompare, IBlank, CoordPrecision, GridFormat, &
      VolumeFormat, UseIBlank, GridFile)

    ! Perturb the coordinates by a large amount
    XCompare = X
    do m = 1, nGrids
      do k = 1, nPoints(m,3)
        do j = 1, nPoints(m,2)
          do i = 1, nPoints(m,1)
            XCompare(m,i,j,k,:) = XCompare(m,i,j,k,:) + 1._rk
          end do
        end do
      end do
    end do

    GridFile = "grid_3d_different_xyz.xyz"
    call Write_Grid(nDims, nGrids, nPoints, XCompare, IBlank, CoordPrecision, GridFormat, &
      VolumeFormat, UseIBlank, GridFile)

    ! Modify the IBlank values
    IBlankCompare = 0

    GridFile = "grid_3d_different_iblank.xyz"
    call Write_Grid(nDims, nGrids, nPoints, X, IBlankCompare, CoordPrecision, GridFormat, &
      VolumeFormat, UseIBlank, GridFile)

    deallocate(X)
    deallocate(XCompare)
    deallocate(IBlank)
    deallocate(IBlankCompare)

  end subroutine ComparePLOT3DGrid3D

  subroutine ComparePLOT3DSolution2D

    integer, parameter :: nGrids = 2
    integer, parameter :: nDims = 2

    integer :: i, j, m
    integer, dimension(:,:), pointer :: nPoints
    real(rk), dimension(:,:,:,:,:), pointer :: Q
    real(rk), dimension(:,:,:,:,:), pointer :: QCompare
    character(len=2)  :: GridFormat, VolumeFormat, CoordPrecision
    character(len=80) :: SolutionFile
    real(rk), dimension(4) :: Tau

    CoordPrecision = "d"
    GridFormat = "m"
    VolumeFormat = "w"

    Tau = 0._rk

    allocate(nPoints(nGrids,MAX_ND))

    nPoints = 1
    nPoints(1,:nDims) = [20,30]
    nPoints(2,:nDims) = [10,20]

    allocate(Q(nGrids,maxval(nPoints(:,1)),maxval(nPoints(:,2)),maxval(nPoints(:,3)),nDims+2))
    Q = 0._rk

    do m = 1, nGrids
      do j = 1, nPoints(m,2)
        do i = 1, nPoints(m,1)
          Q(m,i,j,1,:) = [1._rk, 2._rk, 3._rk, 4._rk]
        end do
      end do
    end do

    SolutionFile = "solution_2d.q"
    call Write_Soln(nDims, nGrids, nPoints, Q, Tau, CoordPrecision, GridFormat, VolumeFormat, &
      SolutionFile)

    allocate(QCompare(nGrids,maxval(nPoints(:,1)),maxval(nPoints(:,2)),maxval(nPoints(:,3)),nDims+2))

    ! Perturb the solution slightly
    QCompare = Q
    do m = 1, nGrids
      do j = 1, nPoints(m,2)
        do i = 1, nPoints(m,1)
          QCompare(m,i,j,1,:) = QCompare(m,i,j,1,:) + 1.e-10_rk
        end do
      end do
    end do

    SolutionFile = "solution_2d_almost.q"
    call Write_Soln(nDims, nGrids, nPoints, QCompare, Tau, CoordPrecision, GridFormat, VolumeFormat, &
      SolutionFile)

    ! Perturb the density by a large amount
    QCompare = Q
    do m = 1, nGrids
      do j = 1, nPoints(m,2)
        do i = 1, nPoints(m,1)
          QCompare(m,i,j,1,1) = QCompare(m,i,j,1,1) + 1._rk
        end do
      end do
    end do

    SolutionFile = "solution_2d_different_density.q"
    call Write_Soln(nDims, nGrids, nPoints, QCompare, Tau, CoordPrecision, GridFormat, VolumeFormat, &
      SolutionFile)

    ! Perturb the x momentum by a large amount
    QCompare = Q
    do m = 1, nGrids
      do j = 1, nPoints(m,2)
        do i = 1, nPoints(m,1)
          QCompare(m,i,j,1,2) = QCompare(m,i,j,1,2) + 1._rk
        end do
      end do
    end do

    SolutionFile = "solution_2d_different_x_momentum.q"
    call Write_Soln(nDims, nGrids, nPoints, QCompare, Tau, CoordPrecision, GridFormat, VolumeFormat, &
      SolutionFile)

    ! Perturb the y momentum by a large amount
    QCompare = Q
    do m = 1, nGrids
      do j = 1, nPoints(m,2)
        do i = 1, nPoints(m,1)
          QCompare(m,i,j,1,3) = QCompare(m,i,j,1,3) + 1._rk
        end do
      end do
    end do

    SolutionFile = "solution_2d_different_y_momentum.q"
    call Write_Soln(nDims, nGrids, nPoints, QCompare, Tau, CoordPrecision, GridFormat, VolumeFormat, &
      SolutionFile)

    ! Perturb the energy by a large amount
    QCompare = Q
    do m = 1, nGrids
      do j = 1, nPoints(m,2)
        do i = 1, nPoints(m,1)
          QCompare(m,i,j,1,4) = QCompare(m,i,j,1,4) + 1._rk
        end do
      end do
    end do

    SolutionFile = "solution_2d_different_energy.q"
    call Write_Soln(nDims, nGrids, nPoints, QCompare, Tau, CoordPrecision, GridFormat, VolumeFormat, &
      SolutionFile)

    deallocate(Q)
    deallocate(QCompare)

  end subroutine ComparePLOT3DSolution2D

  subroutine ComparePLOT3DSolution3D

    integer, parameter :: nGrids = 2
    integer, parameter :: nDims = 3

    integer :: i, j, k, m
    integer, dimension(:,:), pointer :: nPoints
    real(rk), dimension(:,:,:,:,:), pointer :: Q
    real(rk), dimension(:,:,:,:,:), pointer :: QCompare
    character(len=2)  :: GridFormat, VolumeFormat, CoordPrecision
    character(len=80) :: SolutionFile
    real(rk), dimension(4) :: Tau

    CoordPrecision = "d"
    GridFormat = "m"
    VolumeFormat = "w"

    Tau = 0._rk

    allocate(nPoints(nGrids,MAX_ND))

    nPoints = 1
    nPoints(1,:nDims) = [20,30,40]
    nPoints(2,:nDims) = [10,20,30]

    allocate(Q(nGrids,maxval(nPoints(:,1)),maxval(nPoints(:,2)),maxval(nPoints(:,3)),nDims+2))
    Q = 0._rk

    do m = 1, nGrids
      do k = 1, nPoints(m,3)
        do j = 1, nPoints(m,2)
          do i = 1, nPoints(m,1)
            Q(m,i,j,k,:) = [1._rk, 2._rk, 3._rk, 4._rk, 5._rk]
          end do
        end do
      end do
    end do

    SolutionFile = "solution_3d.q"
    call Write_Soln(nDims, nGrids, nPoints, Q, Tau, CoordPrecision, GridFormat, VolumeFormat, &
      SolutionFile)

    allocate(QCompare(nGrids,maxval(nPoints(:,1)),maxval(nPoints(:,2)),maxval(nPoints(:,3)),nDims+2))

    ! Perturb the solution slightly
    QCompare = Q
    do m = 1, nGrids
      do j = 1, nPoints(m,2)
        do i = 1, nPoints(m,1)
          QCompare(m,i,j,1,:) = QCompare(m,i,j,1,:) + 1.e-10_rk
        end do
      end do
    end do

    SolutionFile = "solution_3d_almost.q"
    call Write_Soln(nDims, nGrids, nPoints, QCompare, Tau, CoordPrecision, GridFormat, VolumeFormat, &
      SolutionFile)

    ! Perturb the density by a large amount
    QCompare = Q
    do m = 1, nGrids
      do k = 1, nPoints(m,3)
        do j = 1, nPoints(m,2)
          do i = 1, nPoints(m,1)
            QCompare(m,i,j,k,1) = QCompare(m,i,j,k,1) + 1._rk
          end do
        end do
      end do
    end do

    SolutionFile = "solution_3d_different_density.q"
    call Write_Soln(nDims, nGrids, nPoints, QCompare, Tau, CoordPrecision, GridFormat, VolumeFormat, &
      SolutionFile)

    ! Perturb the x momentum by a large amount
    QCompare = Q
    do m = 1, nGrids
      do k = 1, nPoints(m,3)
        do j = 1, nPoints(m,2)
          do i = 1, nPoints(m,1)
            QCompare(m,i,j,k,2) = QCompare(m,i,j,k,2) + 1._rk
          end do
        end do
      end do
    end do

    SolutionFile = "solution_3d_different_x_momentum.q"
    call Write_Soln(nDims, nGrids, nPoints, QCompare, Tau, CoordPrecision, GridFormat, VolumeFormat, &
      SolutionFile)

    ! Perturb the y momentum by a large amount
    QCompare = Q
    do m = 1, nGrids
      do k = 1, nPoints(m,3)
        do j = 1, nPoints(m,2)
          do i = 1, nPoints(m,1)
            QCompare(m,i,j,k,3) = QCompare(m,i,j,k,3) + 1._rk
          end do
        end do
      end do
    end do

    SolutionFile = "solution_3d_different_y_momentum.q"
    call Write_Soln(nDims, nGrids, nPoints, QCompare, Tau, CoordPrecision, GridFormat, VolumeFormat, &
      SolutionFile)

    ! Perturb the energy by a large amount
    QCompare = Q
    do m = 1, nGrids
      do k = 1, nPoints(m,3)
        do j = 1, nPoints(m,2)
          do i = 1, nPoints(m,1)
            QCompare(m,i,j,k,4) = QCompare(m,i,j,k,4) + 1._rk
          end do
        end do
      end do
    end do

    SolutionFile = "solution_3d_different_z_momentum.q"
    call Write_Soln(nDims, nGrids, nPoints, QCompare, Tau, CoordPrecision, GridFormat, VolumeFormat, &
      SolutionFile)

    ! Perturb the energy by a large amount
    QCompare = Q
    do m = 1, nGrids
      do k = 1, nPoints(m,3)
        do j = 1, nPoints(m,2)
          do i = 1, nPoints(m,1)
            QCompare(m,i,j,k,5) = QCompare(m,i,j,k,5) + 1._rk
          end do
        end do
      end do
    end do

    SolutionFile = "solution_3d_different_energy.q"
    call Write_Soln(nDims, nGrids, nPoints, QCompare, Tau, CoordPrecision, GridFormat, VolumeFormat, &
      SolutionFile)

    deallocate(Q)
    deallocate(QCompare)

  end subroutine ComparePLOT3DSolution3D

  subroutine ComparePLOT3DFunction2D

    integer, parameter :: nGrids = 2
    integer, parameter :: nDims = 2

    integer :: i, j, m
    integer, dimension(:,:), pointer :: nPoints
    real(rk), dimension(:,:,:,:,:), pointer :: F
    real(rk), dimension(:,:,:,:,:), pointer :: FCompare
    character(len=80) :: FunctionFile

    allocate(nPoints(nGrids,MAX_ND))

    nPoints = 1
    nPoints(1,:nDims) = [20,30]
    nPoints(2,:nDims) = [10,20]

    allocate(F(nGrids,maxval(nPoints(:,1)),maxval(nPoints(:,2)),maxval(nPoints(:,3)),2))
    F = 0._rk

    do m = 1, nGrids
      do j = 1, nPoints(m,2)
        do i = 1, nPoints(m,1)
          F(m,i,j,1,:) = [1._rk, 2._rk]
        end do
      end do
    end do

    FunctionFile = "function_2d.f"
    call Write_Func(nGrids, nPoints, F, FunctionFile)

    allocate(FCompare(nGrids,maxval(nPoints(:,1)),maxval(nPoints(:,2)),maxval(nPoints(:,3)),2))

    ! Perturb the values slightly
    FCompare = F
    do m = 1, nGrids
      do j = 1, nPoints(m,2)
        do i = 1, nPoints(m,1)
          FCompare(m,i,j,1,:) = FCompare(m,i,j,1,:) + 1.e-10_rk
        end do
      end do
    end do

    FunctionFile = "function_2d_almost.f"
    call Write_Func(nGrids, nPoints, FCompare, FunctionFile)

    ! Perturb the values by a large amount
    FCompare = F
    do m = 1, nGrids
      do j = 1, nPoints(m,2)
        do i = 1, nPoints(m,1)
          FCompare(m,i,j,1,:) = FCompare(m,i,j,1,:) + 1._rk
        end do
      end do
    end do

    FunctionFile = "function_2d_different.f"
    call Write_Func(nGrids, nPoints, FCompare, FunctionFile)

    deallocate(F)
    deallocate(FCompare)

  end subroutine ComparePLOT3DFunction2D

  subroutine ComparePLOT3DFunction3D

    integer, parameter :: nGrids = 2
    integer, parameter :: nDims = 3

    integer :: i, j, k, m
    integer, dimension(:,:), pointer :: nPoints
    real(rk), dimension(:,:,:,:,:), pointer :: F
    real(rk), dimension(:,:,:,:,:), pointer :: FCompare
    character(len=80) :: FunctionFile

    allocate(nPoints(nGrids,MAX_ND))

    nPoints = 1
    nPoints(1,:nDims) = [20,30,40]
    nPoints(2,:nDims) = [10,20,30]

    allocate(F(nGrids,maxval(nPoints(:,1)),maxval(nPoints(:,2)),maxval(nPoints(:,3)),2))
    F = 0._rk

    do m = 1, nGrids
      do k = 1, nPoints(m,3)
        do j = 1, nPoints(m,2)
          do i = 1, nPoints(m,1)
            F(m,i,j,k,:) = [1._rk, 2._rk]
          end do
        end do
      end do
    end do

    FunctionFile = "function_3d.f"
    call Write_Func(nGrids, nPoints, F, FunctionFile)

    allocate(FCompare(nGrids,maxval(nPoints(:,1)),maxval(nPoints(:,2)),maxval(nPoints(:,3)),2))

    ! Perturb the values slightly
    FCompare = F
    do m = 1, nGrids
      do k = 1, nPoints(m,3)
        do j = 1, nPoints(m,2)
          do i = 1, nPoints(m,1)
            FCompare(m,i,j,k,:) = FCompare(m,i,j,k,:) + 1.e-10_rk
          end do
        end do
      end do
    end do

    FunctionFile = "function_3d_almost.f"
    call Write_Func(nGrids, nPoints, FCompare, FunctionFile)

    ! Perturb the values by a large amount
    FCompare = F
    do m = 1, nGrids
      do k = 1, nPoints(m,3)
        do j = 1, nPoints(m,2)
          do i = 1, nPoints(m,1)
            FCompare(m,i,j,k,:) = FCompare(m,i,j,k,:) + 1._rk
          end do
        end do
      end do
    end do

    FunctionFile = "function_3d_different.f"
    call Write_Func(nGrids, nPoints, FCompare, FunctionFile)

    deallocate(F)
    deallocate(FCompare)

  end subroutine ComparePLOT3DFunction3D

end program CompareTestPLOT3DData
