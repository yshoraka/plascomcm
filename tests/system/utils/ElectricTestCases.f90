program ElectricTestCases

  use ModParseArguments
  use ModStdIO
  implicit none

  integer, parameter :: MAX_ND = 3
  integer, parameter :: DP = kind(0.d0)

  integer :: i
  character(len=CMD_ARG_LENGTH), dimension(:), allocatable :: RawArguments, Arguments

  allocate(RawArguments(command_argument_count()))
  do i = 1, size(RawArguments)
    call get_command_argument(i, RawArguments(i))
  end do

  call ParseArguments(RawArguments, Arguments=Arguments, MinArguments=1, MaxArguments=1)

  select case (trim(Arguments(1)))
  case ("Electric2DSingle")
    call Electric2DSingle
  case ("ElectricScreened")
    call ElectricScreened
  case default
    write (ERROR_UNIT, '(3a)') "ERROR: Unknown case ID '", trim(Arguments(1)), "'."
    stop 1
  end select

contains

  subroutine Electric2DSingle

    use ModBC
    use ModPLOT3D_IO
    implicit none

    integer :: i, j
    real(DP) :: U, V
    integer, dimension(:,:), pointer :: nPoints
    real(DP), dimension(MAX_ND) :: Length
    real(DP), dimension(:,:,:,:,:), pointer :: X
    integer, dimension(:,:,:,:), pointer :: IBlank
    real(DP), dimension(:,:,:,:,:), pointer :: Q
    real(DP), dimension(:,:,:,:,:), pointer :: Phi
    real(DP) :: Gamma
    real(DP) :: Temperature
    real(DP) :: Density
    integer, dimension(MAX_ND) :: nSponge
    type(t_bc), dimension(12) :: BCs
    character(len=2)  :: prec, grid_format, volume_format, ib
    character(len=80) :: grid_file
    character(len=80) :: initial_file
    character(len=80) :: phi_file
    real(DP), dimension(4) :: tau

    allocate(nPoints(1,MAX_ND))

    nPoints(1,:) = [21,21,1]
    Length = [1.d0, 1.d0, 0.d0]

    allocate(X(1,nPoints(1,1),nPoints(1,2),nPoints(1,3),3))

    do j = 1, nPoints(1,2)
      do i = 1, nPoints(1,1)
        U = real(i-1, kind=DP)/real(nPoints(1,1)-1, kind=DP)
        V = real(j-1, kind=DP)/real(nPoints(1,2)-1, kind=DP)
        X(1,i,j,1,:) = Length * [U, V, 0.d0]
      end do
    end do

    allocate(Q(1,nPoints(1,1),nPoints(1,2),nPoints(1,3),5))

    Gamma = 1.4d0
    Temperature = 1.d0/(Gamma - 1.d0)
    Density = 1.d0

    Q(1,:,:,:,1) = Density
    Q(1,:,:,:,2) = 0.d0
    Q(1,:,:,:,3) = 0.d0
    Q(1,:,:,:,4) = 0.d0
    Q(1,:,:,:,5) = Density * Temperature/Gamma

    ! Fluid BCs
    nSponge = 4
    BCs(1) = t_bc_(1, BC_TYPE_SAT_FAR_FIELD, 1, [1,1], [1,-1])
    BCs(2) = t_bc_(1, BC_TYPE_SPONGE, 1, [1,nSponge(1)], [1,-1])
    BCs(3) = t_bc_(1, BC_TYPE_SAT_FAR_FIELD, -1, [-1,-1], [1,-1])
    BCs(4) = t_bc_(1, BC_TYPE_SPONGE, -1, [-nSponge(1),-1], [1,-1])
    BCs(5) = t_bc_(1, BC_TYPE_SAT_FAR_FIELD, 2, [1,-1], [1,1])
    BCs(6) = t_bc_(1, BC_TYPE_SPONGE, 2, [1,-1], [1,nSponge(2)])
    BCs(7) = t_bc_(1, BC_TYPE_SAT_FAR_FIELD, -2, [1,-1], [-1,-1])
    BCs(8) = t_bc_(1, BC_TYPE_SPONGE, -2, [1,-1], [-nSponge(2),-1])

    ! Efield BCs (Neumann lower, Dirichlet 0 upper)
    BCs(9) = t_bc_(1, BC_TYPE_EF_NEUMANN, 1, [1,1], [1,-1])
    BCs(10) = t_bc_(1, BC_TYPE_EF_DIRICHLET_G, -1, [-1,-1], [1,-1])
    BCs(11) = t_bc_(1, BC_TYPE_EF_NEUMANN, 2, [1,-1], [1,1])
    BCs(12) = t_bc_(1, BC_TYPE_EF_DIRICHLET_G, -2, [1,-1], [-1,-1])

    call WriteBCs(BCs, "bc.dat")

    allocate(Phi(1,nPoints(1,1),nPoints(1,2),nPoints(1,3),1))

    ! Manufactured solution using RHS equal to 4 - 2 * (x^2 + y^2)
    do j = 1, nPoints(1,2)
      do i = 1, nPoints(1,1)
        Phi(1,i,j,1,1) = (X(1,i,j,1,1)+1.d0) * (X(1,i,j,1,1)-1.d0) * &
          (X(1,i,j,1,2)+1.d0) * (X(1,i,j,1,2)-1.d0)
      end do
    end do

    prec = "d" ! Precision: double
    grid_format = "m" ! Grid format: multiple
    volume_format = "w" ! Volume format: whole
    ib = "n" ! IBlank
    grid_file = "grid.xyz"
    initial_file = "initial.q"
    phi_file = "expected.phi.q"
    tau = 0.d0

    call Write_Grid(3, 1, nPoints, X, IBlank, prec, grid_format, volume_format, ib, grid_file)
    call Write_Soln(3, 1, nPoints, Q, tau, prec, grid_format, volume_format, initial_file)
    call Write_Func(1, nPoints, Phi, phi_file)

    deallocate(X)
    deallocate(Q)
    deallocate(Phi)

  end subroutine Electric2DSingle

  subroutine ElectricScreened

    use ModBC
    use ModPLOT3D_IO
    implicit none

    integer :: i, j
    real(DP) :: U, V
    integer, dimension(:,:), pointer :: nPoints
    real(DP), dimension(MAX_ND) :: Length
    real(DP), dimension(:,:,:,:,:), pointer :: X
    integer, dimension(:,:,:,:), pointer :: IBlank
    real(DP), dimension(:,:,:,:,:), pointer :: Q
    real(DP), dimension(:,:,:,:,:), pointer :: Phi
    real(DP) :: Gamma
    real(DP) :: Temperature
    real(DP) :: Density
    integer, dimension(MAX_ND) :: nSponge
    type(t_bc), dimension(12) :: BCs
    character(len=2)  :: prec, grid_format, volume_format, ib
    character(len=80) :: grid_file
    character(len=80) :: initial_file
    character(len=80) :: phi_file
    real(DP), dimension(4) :: tau

    allocate(nPoints(1,MAX_ND))

    nPoints(1,:) = [21,21,1]
    Length = [1.d0, 1.d0, 0.d0]

    allocate(X(1,nPoints(1,1),nPoints(1,2),nPoints(1,3),3))

    do j = 1, nPoints(1,2)
      do i = 1, nPoints(1,1)
        U = real(i-1, kind=DP)/real(nPoints(1,1)-1, kind=DP)
        V = real(j-1, kind=DP)/real(nPoints(1,2)-1, kind=DP)
        X(1,i,j,1,:) = Length * [U, V-0.5d0, 0.d0]
      end do
    end do

    allocate(Q(1,nPoints(1,1),nPoints(1,2),nPoints(1,3),5))

    Gamma = 1.4d0
    Temperature = 1.d0/(Gamma - 1.d0)
    Density = 1.d0

    Q(1,:,:,:,1) = Density
    Q(1,:,:,:,2) = 0.d0
    Q(1,:,:,:,3) = 0.d0
    Q(1,:,:,:,4) = 0.d0
    Q(1,:,:,:,5) = Density * Temperature/Gamma

    ! Fluid BCs
    nSponge = 4
    BCs(1) = t_bc_(1, BC_TYPE_SAT_FAR_FIELD, 1, [1,1], [1,-1])
    BCs(2) = t_bc_(1, BC_TYPE_SPONGE, 1, [1,nSponge(1)], [1,-1])
    BCs(3) = t_bc_(1, BC_TYPE_SAT_FAR_FIELD, -1, [-1,-1], [1,-1])
    BCs(4) = t_bc_(1, BC_TYPE_SPONGE, -1, [-nSponge(1),-1], [1,-1])
    BCs(5) = t_bc_(1, BC_TYPE_SAT_FAR_FIELD, 2, [1,-1], [1,1])
    BCs(6) = t_bc_(1, BC_TYPE_SPONGE, 2, [1,-1], [1,nSponge(2)])
    BCs(7) = t_bc_(1, BC_TYPE_SAT_FAR_FIELD, -2, [1,-1], [-1,-1])
    BCs(8) = t_bc_(1, BC_TYPE_SPONGE, -2, [1,-1], [-nSponge(2),-1])

    ! Efield BCs (Dirichlet in x (0 lower, 1 upper), Neumann in y)
    BCs(9) = t_bc_(1, BC_TYPE_EF_DIRICHLET_G, 1, [1,1], [1,-1])
    BCs(10) = t_bc_(1, BC_TYPE_EF_DIRICHLET_P, -1, [-1,-1], [1,-1])
    BCs(11) = t_bc_(1, BC_TYPE_EF_NEUMANN, 2, [1,-1], [1,1])
    BCs(12) = t_bc_(1, BC_TYPE_EF_NEUMANN, -2, [1,-1], [-1,-1])

    call WriteBCs(BCs, "bc.dat")

    allocate(Phi(1,nPoints(1,1),nPoints(1,2),nPoints(1,3),1))

    ! Manufactured solution using RHS equal to -2 + x^2/a^2, where a is the screening length
    do j = 1, nPoints(1,2)
      do i = 1, nPoints(1,1)
        Phi(1,i,j,1,1) = X(1,i,j,1,1)**2
      end do
    end do

    prec = "d" ! Precision: double
    grid_format = "m" ! Grid format: multiple
    volume_format = "w" ! Volume format: whole
    ib = "n" ! IBlank
    grid_file = "grid.xyz"
    initial_file = "initial.q"
    phi_file = "expected.phi.q"
    tau = 0.d0

    call Write_Grid(3, 1, nPoints, X, IBlank, prec, grid_format, volume_format, ib, grid_file)
    call Write_Soln(3, 1, nPoints, Q, tau, prec, grid_format, volume_format, initial_file)
    call Write_Func(1, nPoints, Phi, phi_file)

    deallocate(X)
    deallocate(Q)
    deallocate(Phi)

  end subroutine ElectricScreened

end program ElectricTestCases
