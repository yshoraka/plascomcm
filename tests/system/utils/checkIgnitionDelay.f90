Program checkIgnitionDelay

  use ModIEEEArithmetic
  use ModParseArguments
  use ModStdIO
  Implicit None

  integer, parameter :: DP = kind(1.0d0)
  integer, parameter :: WP = DP

  integer, parameter :: PATH_LENGTH = 256

  Integer :: i,nentries, itimes, io
  Real(WP) :: error, val(2), x, tgradU, tign, tgradM
  Real(WP), pointer :: data(:,:)
  Character(len=PATH_LENGTH) :: fname(2)
  character(len=CMD_ARG_LENGTH), dimension(:), allocatable :: RawArguments, Arguments
  logical :: exists
  real(kind=8) :: tolerance

  allocate(RawArguments(command_argument_count()))
  do i = 1, size(RawArguments)
    call get_command_argument(i, RawArguments(i))
  end do

  call ParseArguments(RawArguments, Arguments=Arguments, MinArguments=3, MaxArguments=3)

  fname(1) = Arguments(1)
  fname(2) = Arguments(2)

  read (Arguments(3), *) tolerance

  do i = 1, 2
    inquire(file=fname(i), exist=exists)
    if (.not. exists) then
      write (ERROR_UNIT, '(3a)') "ERROR: Could not locate file '", trim(fname(i)), "'."
      stop 2
    end if
  end do

  do itimes = 1,2
     nentries = 0
     open(123,file=trim(fname(itimes)))
     DO
        READ(123,*,IOSTAT=io)  x
        IF (io > 0) THEN
           WRITE(ERROR_UNIT,'(3a)') "ERROR: File '", trim(fname(itimes)), "' has unexpected format."
           stop 2
        ELSE IF (io < 0) THEN
           exit
        ELSE
           nentries = nentries + 1
        END IF
     END DO
     allocate(data(nentries,2))
     rewind(123)
     DO i=1,nentries
        READ(123,*)  data(i,1:2)
     END DO
     close(123)
     tgradU=0d0;tign=0; tgradM=0;
     DO i=2,nentries
        tgradU = (data(i,2)-data(i-1,2))/(data(i,1)-data(i-1,1));
         if(tgradU > tgradM) then
           tgradM = tgradU;
           tign=(data(i,1)+data(i-1,1))/2.0;
         endif
      ENDDO
      val(itimes) = log10(tign)
     nullify(data)
  enddo

  error = abs(val(2)-val(1))/abs(val(2)+val(1))*2

  print*,'error(%)=',error*100

  if (abs(error) > tolerance .or. ieee_is_nan(error)) then
    write (*, '(a)') "Error in ignition delay is greater than specified tolerance."
    stop 1
  end if

End Program checkIgnitionDelay
