Program checkRate
!
!LM modeled after Jcaps's data2ensight
!Used for axisymmetric verification test
!

  use ModIEEEArithmetic
  use ModParseArguments
  USE ModPLOT3D_IO
  use ModStdIO
  Implicit None

  !  Arrays
  Real(KIND=8), Dimension(:,:,:,:,:), Pointer :: Xg, Q0, Q1, Q2
  Real(KIND=8), Dimension(:), Allocatable :: time, out
  Real(KIND=8) :: TAU(4), CAX(3)
  Real(KIND=8), Dimension(:,:,:), Allocatable :: rbuffer
  Real(KIND=4) :: XMIN, XMAX, YMIN, YMAX, ZMIN, ZMAX

  !  Integers
  Integer :: ngrid, I, J, K, N, NX, NY, NZ, NDIM, npart, reclength
  Integer, Dimension(:,:), Pointer :: ND1
  Integer, Dimension(:,:,:,:), Pointer :: IBLANK1
  Integer :: startIter, stopIter, skipIter, iter, var, nvar, ierr, ibuffer

  !  Characters
  character(LEN=80), dimension(:), Allocatable :: names
  Character(LEN=2)  :: prec, gf, vf, ib, ib2
  Character(LEN=80) :: fname,grid_name
  character(LEN=80) :: binary_form
  character(LEN=80) :: file_description1,file_description2
  character(LEN=80) :: node_id,element_id
  character(LEN=80) :: part,description_part,cblock,extents,cbuffer

  !  Step zero, read in the command line
  Integer :: ng, num, numFiles, naux, testVar

  real(KIND=8)           ::  sumx  = 0.0d0
  real(KIND=8)           ::  sumx2 = 0.0d0
  real(KIND=8)           ::  sumxy = 0.0d0
  real(KIND=8)           ::  sumy  = 0.0d0
  real(KIND=8)           ::  sumy2 = 0.0d0
  real(kind=8)           ::  dslope, dintercept,dcorrCoeff 
  real(kind=8)           ::  derror

  real(kind=8)           ::  OmegaR, omegaI, period

  real(kind=8) :: tolerance

  character(len=CMD_ARG_LENGTH), dimension(:), allocatable :: RawArguments, Arguments
  logical :: exists

  ! Nullify pointers
  nullify(Xg, Q0, Q1, Q2)
  nullify(ND1)
  nullify(IBLANK1)

  allocate(RawArguments(command_argument_count()))
  do i = 1, size(RawArguments)
    call get_command_argument(i, RawArguments(i))
  end do

  call ParseArguments(RawArguments, Arguments=Arguments, MinArguments=4, MaxArguments=4)

  read (Arguments(1), *) startIter
  read (Arguments(2), *) stopIter
  read (Arguments(3), *) skipIter
  read (Arguments(4), *) tolerance

  do iter = startIter, stopIter, skipIter
    write (fname, '(A,I8.8,A)') 'RocFlo-CM.', iter, '.q'
    inquire(file=fname, exist=exists)
    if (.not. exists) then
      write (ERROR_UNIT, '(3a)') "ERROR: Could not locate file '", trim(fname), "'."
      stop 2
    end if
  end do

  grid_name='grid.xyz'
  ib='y'

  testVar = 2
  CAX = [15d0,50d0,10d0]

  !base flow
  fname = 'BaseflowMod.q'
  Call Read_Soln(NDIM,ngrid,ND1,Q0,TAU,prec,gf,vf,fname,1)

  ! Initialize
  numFiles = 0
  Do iter = startIter, stopIter, skipIter
     numFiles = numFiles + 1
  End Do

  ! Find number of variables (should be 5+)
  Write(fname,'(A,I8.8,A)') 'RocFlo-CM.', startIter, '.q'
  Call Read_Soln(NDIM,ngrid,ND1,Q1,TAU,prec,gf,vf,fname,0)
  naux=0
  nvar=SIZE(Q1(1,1,1,1,:)) + naux
  If (nvar < 5) then
     Write (*,'(A)') 'ERROR: Number of CVs less than 5.'
     Stop 2
  Else
     Print *, 'Number of variables:',nvar
     Write (*,'(A)') ''
  End If

  !  Step one, read in the mesh1 grid file
  Write (*,'(A)') ''
  Call Read_Grid(NDIM,ngrid,ND1,Xg,IBLANK1, prec,gf,vf,ib2,grid_name,1)
  Write (*,'(A)') ''

  ! Loop over all files, find time array first
  allocate(time(numFiles), out(numFiles))
  time = 0.0_8
  out = 0d0
  num=0
  Do iter = startIter, stopIter, skipIter
    Write(fname,'(A,I8.8,A)') 'RocFlo-CM.', iter, '.q'

    ! ... read solution header to find time
    Call Read_Soln_Header(NDIM, ngrid, ND1, fname, tau)

    ! ... save time
    num = num + 1
    time(num) = tau(4)

  End Do

  ! ===================================== !
  ! Loop through and write the data files !
  ! ===================================== !

  ! Allocate single-precision array
  Allocate(rbuffer(1:maxval(ND1(:,1)),1:maxval(ND1(:,2)),1:maxval(ND1(:,3))))

  ! Loop through files and write
  num = 1
  filesLoop: Do iter = startIter, stopIter, skipIter
    
     Write(fname,'(A,I8.8,A)') 'RocFlo-CM.', iter, '.q'
     !Write (*,'(A)') fname
     Call Read_Soln(NDIM,ngrid,ND1,Q1,TAU,prec,gf,vf,fname,0)


     ! Loop through each grid
     gridsLoop: Do ng=1, ngrid

        ! Get domain size
        NX = ND1(ng,1)
        NY = ND1(ng,2)
        NZ = ND1(ng,3)

        var = testVar
           If (var>1 .and. var<=5) then
              rbuffer(1:NX,1:NY,1:NZ)=dble((Q1(ng,1:NX,1:NY,1:NZ,var)/Q1(ng,1:NX,1:NY,1:NZ,1) -Q0(ng,1:NX,1:NY,1:NZ,var))**2*Xg(ng,1:NX,1:NY,1:NZ,1))
           else
              rbuffer(1:NX,1:NY,1:NZ)=dble((Q1(ng,1:NX,1:NY,1:NZ,var)-Q0(ng,1:NX,1:NY,1:NZ,var))**2*Xg(ng,1:NX,1:NY,1:NZ,1))
           End If
           
           where(Xg(ng,1:NX,1:NY,1:NZ,1) < 0d0 .or. Xg(ng,1:NX,1:NY,1:NZ,1) >= CAX(3) .or. Xg(ng,1:NX,1:NY,1:NZ,2) < CAX(1) .or. Xg(ng,1:NX,1:NY,1:NZ,2) >= CAX(2) .or. IBLANK1(ng,1:NX,1:NY,1:NZ) /= 1)
              rbuffer = 0d0
           end where
           out(num) = out(num) + sum(rbuffer)
        End Do gridsLoop
        out(num) = sqrt(out(num))
        
        write(*,*) time(num), out(num)
     ! ... counter
     num = num + 1
  End Do filesLoop
!Data from mahesh calculations
  omegaR = 6.466764349101957D-002 
  omegaI = 1.49419910221811d0;
  period = 2d0 *acos(-1d0) /OmegaI
  num = num-1
  time = time - time(1)
  do K = 1,num
     out(K) = out(K) * (exp(omegaR*period)-1d0)/omegaR*(1d0+sin(2*omegaI*time(K) +acos(-1d0)*9d0/10d0)*0.026d0)
  enddo
  out = log(out)

  do K = 1,numFiles
      sumx  = sumx + time(K)
      sumx2 = sumx2 + time(K) * time(K)
      sumxy = sumxy + time(K) * out(K)
      sumy  = sumy + out(K)
      print*,time(K),out(K)
  enddo
   
   dslope = (dble(num) * sumxy  -  sumx * sumy) / (dble(num) * sumx2 - sumx**2)
   dintercept = (sumy * sumx2  -  sumx * sumxy) / (dble(num) * sumx2  -  sumx**2)
   dcorrCoeff = (sumxy - sumx * sumy / dble(num)) / sqrt((sumx2 - sumx**2/dble(num)) * (sumy2 - sumy**2/dble(num)))

   derror = (dslope - omegaR)/omegaR
  
   print *,'Slope %',dslope
   print *,'Error %',derror*100d0

   if (abs(derror) > tolerance .or. ieee_is_nan(derror)) then
     write (*, '(a)') "Error in growth rate is greater than specified tolerance."
     stop 1
   end if 

  ! Clean up & leave
  Deallocate(ND1, Xg, Q1, Q0, IBLANK1, time, rbuffer)

End Program checkRate
