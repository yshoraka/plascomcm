! Copyright (c) 2015, University of Illinois at Urbana-Champaign, XPACC
! License: MIT, http://opensource.org/licenses/MIT
module TestFixtures

  use pFUnit_mod
  use ModDataStruct
  use ModGlobal
  use ModPlasComCM
  implicit none

contains

  subroutine InputSetup(TestCase, input_fname, region, restart_fname, restart_aux_fname)

    class(MPITestCase), intent(in) :: TestCase
    character(len=PATH_LENGTH), intent(in) :: input_fname
    type(t_region), pointer, intent(out) :: region
    character(len=PATH_LENGTH), intent(in), optional :: restart_fname, restart_aux_fname

    integer :: ierr

    allocate(region)

    call PlasComCM_Init_MPI(region, TestCase%getMPICommunicator())
    call PlasComCM_Init_Timings(region)
    call PlasComCM_Init_Input(region, input_fname, restart_fname=restart_fname, &
      restart_aux_fname=restart_fname)

  end subroutine InputSetup

  subroutine InputTeardown(TestCase, region)

    class(MPITestCase), intent(in) :: TestCase
    type(t_region), pointer, intent(inout) :: region

    integer :: ierr

    call PlasComCM_Finalize_Timings(region)

    deallocate(region)

  end subroutine InputTeardown

  subroutine GridSetup(TestCase, input_fname, region, restart_fname, restart_aux_fname)

    class(MPITestCase), intent(in) :: TestCase
    character(len=PATH_LENGTH), intent(in) :: input_fname
    type(t_region), pointer, intent(out) :: region
    character(len=PATH_LENGTH), intent(in), optional :: restart_fname, restart_aux_fname

    integer :: ierr

    allocate(region)

    call PlasComCM_Init_MPI(region, TestCase%getMPICommunicator())
    call PlasComCM_Init_Timings(region)
    call PlasComCM_Init_Input(region, input_fname, restart_fname=restart_fname, &
      restart_aux_fname=restart_aux_fname)
    call PlasComCM_Init_Grids(region)

  end subroutine GridSetup

  subroutine GridTeardown(TestCase, region)

    class(MPITestCase), intent(in) :: TestCase
    type(t_region), pointer, intent(inout) :: region

    integer :: ierr

    call PlasComCM_Finalize_Timings(region)

    deallocate(region)

  end subroutine GridTeardown

  subroutine StateSetup(TestCase, input_fname, region, restart_fname, restart_aux_fname)

    class(MPITestCase), intent(in) :: TestCase
    character(len=PATH_LENGTH), intent(in) :: input_fname
    type(t_region), pointer, intent(out) :: region
    character(len=PATH_LENGTH), intent(in), optional :: restart_fname, restart_aux_fname

    integer :: ierr

    allocate(region)

    call PlasComCM_Init_MPI(region, TestCase%getMPICommunicator())
    call PlasComCM_Init_Timings(region)
    call PlasComCM_Init_Input(region, input_fname, restart_fname=restart_fname, &
      restart_aux_fname=restart_aux_fname)
    call PlasComCM_Init_Grids(region)
    call PlasComCM_Init_State(region)

  end subroutine StateSetup

  subroutine StateTeardown(TestCase, region)

    class(MPITestCase), intent(in) :: TestCase
    type(t_region), pointer, intent(inout) :: region

    integer :: ierr

    call PlasComCM_Finalize_Timings(region)

    deallocate(region)

  end subroutine StateTeardown

  subroutine AdjointSetup(TestCase, input_fname, region, restart_fname, restart_aux_fname)

    class(MPITestCase), intent(in) :: TestCase
    character(len=PATH_LENGTH), intent(in) :: input_fname
    type(t_region), pointer, intent(out) :: region
    character(len=PATH_LENGTH), intent(in), optional :: restart_fname, restart_aux_fname

    integer :: ierr

    allocate(region)

    call PlasComCM_Init_MPI(region, TestCase%getMPICommunicator())
    call PlasComCM_Init_Timings(region)
    call PlasComCM_Init_Input(region, input_fname, restart_fname=restart_fname, &
      restart_aux_fname=restart_aux_fname)
    call PlasComCM_Init_Grids(region)
    call PlasComCM_Init_State(region)
    call PlasComCM_Init_Adjoint(region)

  end subroutine AdjointSetup

  subroutine AdjointTeardown(TestCase, region)

    class(MPITestCase), intent(in) :: TestCase
    type(t_region), pointer, intent(inout) :: region

    integer :: ierr

    call PlasComCM_Finalize_Timings(region)

    deallocate(region)

  end subroutine AdjointTeardown

  subroutine TMSetup(TestCase, input_fname, region, restart_fname, restart_aux_fname)

    class(MPITestCase), intent(in) :: TestCase
    character(len=PATH_LENGTH), intent(in) :: input_fname
    type(t_region), pointer, intent(out) :: region
    character(len=PATH_LENGTH), intent(in), optional :: restart_fname, restart_aux_fname

    integer :: ierr

    allocate(region)

    call PlasComCM_Init_MPI(region, TestCase%getMPICommunicator())
    call PlasComCM_Init_Timings(region)
    call PlasComCM_Init_Input(region, input_fname, restart_fname=restart_fname, &
      restart_aux_fname=restart_aux_fname)
    call PlasComCM_Init_Grids(region)
    call PlasComCM_Init_State(region)
    call PlasComCM_Init_Adjoint(region)
    call PlasComCM_Init_Probe(region)
    call PlasComCM_Init_LST(region)
    call PlasComCM_Init_TM(region)

  end subroutine TMSetup

  subroutine TMTeardown(TestCase, region)

    class(MPITestCase), intent(in) :: TestCase
    type(t_region), pointer, intent(inout) :: region

    integer :: ierr

    call PlasComCM_Finalize_TM(region)
    call PlasComCM_Finalize_Timings(region)

    deallocate(region)

  end subroutine TMTeardown

  subroutine FullSetup(TestCase, input_fname, region, restart_fname, restart_aux_fname)

    class(MPITestCase), intent(in) :: TestCase
    character(len=PATH_LENGTH), intent(in) :: input_fname
    type(t_region), pointer, intent(out) :: region
    character(len=PATH_LENGTH), intent(in), optional :: restart_fname, restart_aux_fname

    integer :: ierr

    allocate(region)

    call PlasComCM_Init_MPI(region, TestCase%getMPICommunicator())
    call PlasComCM_Init_Timings(region)
    call PlasComCM_Init_Input(region, input_fname, restart_fname=restart_fname, &
      restart_aux_fname=restart_aux_fname)
    call PlasComCM_Init_Grids(region)
    call PlasComCM_Init_State(region)
    call PlasComCM_Init_Adjoint(region)
    call PlasComCM_Init_Probe(region)
    call PlasComCM_Init_LST(region)
    call PlasComCM_Init_TM(region)
    call PlasComCM_Init_NASA_Rotor_Grid(region)
    call PlasComCM_Init_LES(region)
    call PlasComCM_Init_Stat(region)

  end subroutine FullSetup

  subroutine FullTeardown(TestCase, region)

    class(MPITestCase), intent(in) :: TestCase
    type(t_region), pointer, intent(inout) :: region

    integer :: ierr

    call PlasComCM_Finalize_TM(region)
    call PlasComCM_Finalize_Timings(region)

    deallocate(region)

  end subroutine FullTeardown

end module TestFixtures
