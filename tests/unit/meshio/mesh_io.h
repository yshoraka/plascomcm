#ifndef __MPI_MESH_IO_H__
#define __MPI_MESH_IO_H__

#include <mpi.h>


/* Read an n-dimensional mesh from a file.
   This is a collective call. All the ranks that opened the file 'fh'
   must call this collectively.
   The data in the file is assumed to be in contiguous, canonical order.

   The data read may be a subset of the mesh encoded in the file, and it
   may be a subset of the mesh encoded in memory. 

   For example, let the file contain a 6x10 mesh of data. We wish to
   read the upper-right quadrant into an in-memory array with 1 halo
   cell on each side.

   file:
   . . . . . d d d d d
   . . . . . d d d d d
   . . . . . d d d d d
   . . . . . . . . . .
   . . . . . . . . . .
   . . . . . . . . . .

   memory:
   . . . . . . .
   . d d d d d .
   . d d d d d .
   . d d d d d .
   . . . . . . .

   mesh_size = {3, 5};
   file_mesh_sizes = {6, 10};
   file_mesh_starts = {0, 5};
   memory_mesh_sizes = {5, 7};
   memory_mesh_starts = {1, 1}

   fh - Handle of the file from which data will be read.
   offset - Offset (in bytes) from the beginning of the file where the first
     byte of the full mesh can be found.
   etype - datatype of each element in the mesh. Only basic datatypes
     are supported.
   file_is_big_endian - boolean flag, nonzero iff the data in the file
     is in big-endian byte order. If this does not match the CPU format,
     the data will be byte-swapped.
   ndims - number of array dimensions (positive integer)
   buf - location of the mesh in memory
   mesh_sizes - number of elements in each dimension of the mesh being read
     (array of positive integers). These elements will be a subset of the
   file_mesh_sizes - number of elements in each dimension of the mesh
     as it is stored in the file.
   file_mesh_starts - number of elements in each dimension by which the
     submesh being read is inset from the origin of the file mesh.
   file_array_order - storage order of the mesh on disk.
     MPI_ORDER_C for row-major, MPI_ORDER_FORTRAN for column-major.
   memory_mesh_sizes - number of elements in each dimension of the full
     in-memory mesh
   memory_mesh_starts - number of elements in each dimension by which the
     submesh being written to memory is inset from the origin of the
     memory mesh.
   memory_array_order - storage order of the mesh in memory.
     MPI_ORDER_C for row-major, MPI_ORDER_FORTRAN for column-major.

  Returns:
    MPI_SUCCESS on success
    MPI_ERR_TYPE if etype is not a supported datatype
    MPI_ERR_BUFFER if buf is NULL
    MPI_ERR_DIMS if ndims is nonpositive
    MPI_ERR_ARG if any of the array bounds are invalid or the datatype
      has a nonpositive size or odd size (other than 1).
    MPI_ERR_TRUNCATE if no data is read from the file
    If any MPI call fails, this returns the error code from that call.
*/
int Mesh_IO_read
(MPI_File fh,
 MPI_Offset offset,
 MPI_Datatype etype,
 int file_is_big_endian,
 void *buf,
 int ndims,
 const int *mesh_size,
 const int *file_mesh_size,
 const int *file_mesh_starts,
 int file_array_order,
 const int *memory_mesh_size,
 const int *memory_mesh_starts,
 int memory_array_order);


#endif /* __MPI_MESH_IO_H__ */
