#include <stdio.h>
#include "endian_switch.h"
#include "mesh_io.h"
#include "mpi.h"

int rank, np;

#define FILENAME "test_mesh_io.tmp"
int FILE_HEADER_LEN = 16;
int simple_file_size[3] = {10, 10, 10};


int isBigEndian();
// create test datafile
void createFile();
void testReadSimple(MPI_File fh);
void testReadWithHalo(MPI_File fh);
const char *getErrorName(int mpi_err);
void reportError(const char *msg, int mpi_err);

int main(int argc, char **argv) {
  MPI_File fh;

  MPI_Init(&argc, &argv);
  MPI_Comm_size(MPI_COMM_WORLD, &np);
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);

  if (rank == 0) createFile();

  MPI_File_open(MPI_COMM_WORLD, FILENAME, MPI_MODE_RDONLY, MPI_INFO_NULL, &fh);
  testReadSimple(fh);
  testReadWithHalo(fh);
  
  MPI_File_close(&fh);
  if (rank == 0) remove(FILENAME);
  MPI_Finalize();
  return 0;
}


int isBigEndian() {
  int tmp = 1;
  return ! *(char*) &tmp;
}


const char *getErrorName(int mpi_err) {
  switch (mpi_err) {
  case MPI_SUCCESS: return "MPI_SUCCESS";
  case MPI_ERR_TYPE: return "MPI_ERR_TYPE";
  case MPI_ERR_BUFFER: return "MPI_ERR_BUFFER";
  case MPI_ERR_DIMS: return "MPI_ERR_DIMS";
  case MPI_ERR_ARG: return "MPI_ERR_ARG";
  case MPI_ERR_TRUNCATE: return "MPI_ERR_TRUNCATE";
  default: return NULL;
  }
}


void reportError(const char *msg, int mpi_err) {
  const char *description = getErrorName(mpi_err);
  if (description) {
    printf("[%d] %s: %s\n", rank, msg, description);
  } else {
    printf("[%d] %s: %d\n", rank, msg, mpi_err);
  }
}


/* Create a simple data file with a 16 byte header and a 10x10x10 cube
   of doubles who values are z*10000 + y*100 + x. */
void createFile() {
  double array[10][10][10];
  int i, j, k;
  FILE *outf;

  for (i=0; i < 10; i++)
    for (j=0; j < 10; j++)
      for (k=0; k < 10; k++)
        array[i][j][k] = i*10000 + j * 100 + k;

  outf = fopen(FILENAME, "wb");

  /* write 16 character header (FILE_HEADER_LEN) */
  fprintf(outf, "mesh test header");
  fwrite(array, sizeof(double), 10*10*10, outf);
  fclose(outf);
}


/* Have each process the first 5 elements of a row, and each process
   is reading a different row. */
void testReadSimple(MPI_File fh) {
  int layer = (rank/10) % 10;
  int row = rank % 10;
  int read_size[3] = {1, 1, 5}, read_starts[3] = {layer, row, 0};
  int write_starts[3] = {0, 0, 0};
  int err, i;
  double data[5] = {-1, -1, -1, -1, -1};
  
  err = Mesh_IO_read(fh, FILE_HEADER_LEN, MPI_DOUBLE, isBigEndian(),
                     data, 3, read_size,
                     simple_file_size, read_starts, MPI_ORDER_C,
                     read_size, write_starts, MPI_ORDER_C);

  if (err != MPI_SUCCESS) {
    reportError("Failed with error code", err);
    return;
  }

  /* printf("[%02d] %.0f %.0f %.0f %.0f %.0f\n",
     rank, data[0], data[1], data[2], data[3], data[4]); */

  err = 0;
  for (i=0; i < 5; i++) {
    double expected = layer*10000 + row*100 + i;
    if (data[i] != expected) {
      printf("[%d] error [%d] is %f, should be %f\n", rank, i, data[i],
             expected);
      err = 1;
    }
  }

  MPI_Reduce(&err, &i, 1, MPI_INT, MPI_SUM, 0, MPI_COMM_WORLD);
  if (rank == 0) {
    printf("testReadSimple: %s\n", i==0 ? "ok" : "FAIL");
  }
}


/* Read 3 elements into an array of 5: . X X X . */
void testReadWithHalo(MPI_File fh) {
  int layer = (rank/10) % 10;
  int row = rank % 10;
  int read_size[3] = {1, 1, 3}, read_starts[3] = {layer, row, 1};
  int write_buffer_size[3] = {1, 1, 5}, write_starts[3] = {0, 0, 1};
  int err, i;
  double data[5] = {-1, -1, -1, -1, -1};

  err = Mesh_IO_read(fh, FILE_HEADER_LEN, MPI_DOUBLE, isBigEndian(),
                     data, 3, read_size,
                     simple_file_size, read_starts, MPI_ORDER_C,
                     write_buffer_size, write_starts, MPI_ORDER_C);

  if (err != MPI_SUCCESS) {
    reportError("Failed with error code", err);
    err = 1;
    goto fail;
  }

  /* printf("[%02d] %.0f %.0f %.0f %.0f %.0f\n",
     rank, data[0], data[1], data[2], data[3], data[4]); */

  err = 0;
  if (data[0] != -1 || data[4] != -1) {
    printf("[%d] error halo cells modified\n", rank);
    err = 1;
  }
  
  for (i=1; i < 4; i++) {
    double expected = layer*10000 + row*100 + i;
    if (data[i] != expected) {
      printf("[%d] error [%d] is %f, should be %f\n", rank, i, data[i],
             expected);
      err = 1;
    }
  }

 fail:
  MPI_Reduce(&err, &i, 1, MPI_INT, MPI_SUM, 0, MPI_COMM_WORLD);
  if (rank == 0) {
    printf("testReadWithHalo: %s\n", i==0 ? "ok" : "FAIL");
  }

  
}
