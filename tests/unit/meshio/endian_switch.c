#include <stdlib.h>
#include "reverse_bytes.h"
#include "endian_switch.h"


/* to cut down argument length, package up the arguments for fixEndianOneDim */
typedef struct {
  int ndims;

  /* for all i > first_full_dim, sub_mesh_sizes[i] == full_mesh_sizes[i] */
  int first_full_dim;

  size_t element_size;
  const int *sub_mesh_sizes, *full_mesh_sizes, *sub_mesh_starts;

  /* {full,sub}_mesh_sizes[i] = product of {full,sub}_mesh_sizes[i+1..ndim-1] */
  const int *full_cum_sizes, *sub_cum_sizes;
} EndianParams;

static int *createReversedArrayCopy(int len, const int *array);


/* Returns 1 iff the current architectures is big-endian. */
static int isBigEndian() {
  int tmp = 1;
  return ! *(char*) &tmp;
}
  


/*
  endianSwitchOneDim - recursive function used to traverse mesh dimensions

  p->first_full_dim : index after which all dimensions are full.

  If sub_mesh_sizes[i] < full_mesh_sizes[i] for all i, first_full_dim
  will be ndims.

  ndims=1, value doesn't matter
  xxxxxxx - first_full_dim=0, one contiguous chunk

  ..xxx.. - first_full_dim=1, one contiguous chunk

  ndims=2

  full_mesh_sizes={3,7}, sub_mesh_sizes={3,7}
  xxxxxxx
  xxxxxxx
  xxxxxxx
  first_full_dim=0, one contiguous chunk
     
  full_mesh_sizes={3,7}, sub_mesh_sizes={2,7}
  xxxxxxx
  xxxxxxx
  .......
  first_full_dim=1, one contiguous chunk

  full_mesh_sizes={3,7}, sub_mesh_sizes={2,4}
  .......
  xxxx...
  xxxx...
  .......
  first_full_dim=2, sub_mesh_sizes[0] contiguous chunks
*/
static void endianSwitchOneDim(int dim_no, char *buf, const EndianParams *p) {
  int i, start=p->sub_mesh_starts[dim_no], count = p->sub_mesh_sizes[dim_no];
  buf += p->element_size * start * p->full_cum_sizes[dim_no];

  if (dim_no+1 >= p->first_full_dim) {
    /* if the remaining dimensions are contiguous, do it in one call */
    reverseBytes(buf, p->element_size, count * p->sub_cum_sizes[dim_no]);
  } else {
    for (i = start; i < start + count; i++) {
      endianSwitchOneDim(dim_no+1, buf, p);
      buf += p->element_size * p->full_cum_sizes[dim_no];
    }
  }
}
 
                            
void endianSwitch
(void *buf, size_t element_size, int data_is_big_endian, int ndims,
 const int *full_mesh_sizes, const int *sub_mesh_sizes,
 const int *sub_mesh_starts, int array_order) {

  int i, first_full_dim, *full_cum_sizes, *sub_cum_sizes;

  /* do nothing if the data is the right endianness or is only 1 byte long */
  if (isBigEndian() == data_is_big_endian
      || element_size == 1) return;

  /* reverse the dimension order if it's column-major */
  if (array_order != MPI_ORDER_C) {
    sub_mesh_sizes = createReversedArrayCopy(ndims, sub_mesh_sizes);
    full_mesh_sizes = createReversedArrayCopy(ndims, full_mesh_sizes);
    sub_mesh_starts = createReversedArrayCopy(ndims, sub_mesh_starts);
  }

  /* Summarize the size of each dimension as the product of the remaining
     dimensions. For example, with a C array foo[2][3][5]:
       size[0] == 15, size[1] == 5, size[2] == 1
  */
  full_cum_sizes = (int*) malloc(sizeof(int) * ndims);
  sub_cum_sizes  = (int*) malloc(sizeof(int) * ndims);
  assert(full_cum_sizes);
  assert(sub_cum_sizes);
  full_cum_sizes[ndims-1] = sub_cum_sizes[ndims-1] = 1;
  for (i=ndims-2; i >= 0; i--) {
    full_cum_sizes[i] = full_cum_sizes[i+1] * full_mesh_sizes[i+1];
    sub_cum_sizes[i]  = sub_cum_sizes[i+1]  * sub_mesh_sizes[i+1];
  }

  /* Figure out the dimension after which the storage is contiguous.
     For example, with full_mesh_sizes={5,5,5,5,5,5} and
     sub_mesh_sizes={4,3,2,1,5,5}
     the last two dimensions are contiguous.  first_full_dim is the
     index after which all dimensions are full so it will be 3.
 */
  for (i = ndims; i > 0; i--)
    if (sub_mesh_sizes[i-1] != full_mesh_sizes[i-1]) break;
  first_full_dim = i;

  /* use a recursive function to traverse the dimensions */
  {
    EndianParams params = {ndims, first_full_dim, element_size,
                           sub_mesh_sizes, full_mesh_sizes, sub_mesh_starts,
                           full_cum_sizes, sub_cum_sizes};
    endianSwitchOneDim(0, (char*)buf, &params);
  }

  /* free the copies I made of the shape arrays */
  if (array_order != MPI_ORDER_C) {
    /* strip off 'const' */
    free((int*)sub_mesh_sizes);
    free((int*)full_mesh_sizes);
    free((int*)sub_mesh_starts);
  }
  free(full_cum_sizes);
  free(sub_cum_sizes);
}




static int *createReversedArrayCopy(int len, const int *array) {
  int i, *copy = (int*) malloc(sizeof(int) * len);
  assert(copy);

  for (i=0; i < len; i++)
    copy[len - 1 - i] = array[i];

  return copy;
}

  
