#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <stdint.h>
#include <stddef.h>
#include <mpi.h>
#include "reverse_bytes.h"
#include "mesh_io.h"


#define DO_REVERSE_BYTES 1
#define VERBOSE 0

int rank, np;
MPI_Datatype cubelet_def_t;

typedef struct {
  int rank, ndims;
  long offset;
  int cube_size[3], subcube_offset[3], subcube_size[3];
  unsigned hash;
} Cubelet;

int distribute(int count, int splits, int split_idx);
unsigned simpleHash(const void *data_v, size_t len);
int readAndScatterCubeletFile(Cubelet **cubelet_list, int *cubelet_list_count,
			      int *max_count, const char *filename);
int readCubeletFile(Cubelet **cubelet_list, int *cubelet_list_count,
		    const char *filename);
int readCubelet(Cubelet *c, FILE *inf);
int Cubelet_get_cube_size(Cubelet *c);
int Cubelet_get_subcube_size(Cubelet *c);

void printCube(Cubelet *c, double *data);
void defineCubeletDefType();


void readCubeData(Cubelet *cubelet_list, int cubelet_list_count,
               MPI_File grid_inf, size_t *bytes_read);


void printHelp() {
  printf("\n  test_read_cubes_mpi <grid file> <cube list>\n"
	 "  Given a plot3d file (like grid.xyz) and a file containing a list\n"
	 "  of cube descriptions like:\n"
	 "    rank 46 NDIM 3 fileoffset 60 cubesize 1025,257,97 subcubeoffset 33,130,33 subcubesize 32,26,32 hash 57d00740\n"
	 "  read each of the cubes and verify the hash.\n\n");
  exit(1);
}


int main(int argc, char **argv) {
  char *grid_file, *cube_list_file;
  int amode, max_cubelets_per_rank;
  MPI_File grid_inf;
  Cubelet *cubelet_list;
  int cubelet_list_count;
  MPI_Info info;
  size_t bytes_read;
  double start_time, elapsed;
  
  MPI_Init(&argc, &argv);
  MPI_Comm_size(MPI_COMM_WORLD, &np);
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);

  if (argc != 3) printHelp();

  start_time = MPI_Wtime();  
  grid_file = argv[1];
  cube_list_file = argv[2];

  defineCubeletDefType();

  /* read the list of cubelets and scatter it to all the ranks
     if there is an error reading the file, all processors should exit */
  if (!readAndScatterCubeletFile(&cubelet_list, &cubelet_list_count,
				 &max_cubelets_per_rank, cube_list_file))
    goto fail;

  if (rank == 0)
    printf("%.3f ms to call readAndScatterCubeletFile\n",
	   1000 * (MPI_Wtime() - start_time));



  /* open the grid file */
  amode = MPI_MODE_RDONLY; /* | MPI_MODE_UNIQUE_OPEN; */

  MPI_Info_create(&info);
  /*
  MPI_Info_set(info, "striping_factor", "4");
  MPI_Info_set(info, "striping_unit", "1048576");
  MPI_Info_set(info, "collective_buffering", "true");
  MPI_Info_set(info, "cb_block_size", "1048576");
  */
  MPI_File_open(MPI_COMM_WORLD, grid_file, amode, info, &grid_inf);
  MPI_Info_free(&info);

  start_time = MPI_Wtime();

  readCubeData(cubelet_list, cubelet_list_count, grid_inf, &bytes_read);
  

  elapsed = MPI_Wtime() - start_time;

  if (rank == 0)
    printf("%lu bytes read in %.3f seconds = %.3f MiB/sec\n",
	   bytes_read, elapsed, bytes_read / (elapsed * 1024 * 1024));

  MPI_File_close(&grid_inf);

  free(cubelet_list);

 fail:
  MPI_Finalize();

  return 0;
}


void readCubeData(Cubelet *cubelet_list, int cubelet_list_count,
                  MPI_File grid_inf, size_t *bytes_read) {

  size_t subcube_bytes;
  double *subcube_buffer, *subcube_buffer_pos;
  int error_count = 0, success_count = 0, cube_list_pos;
  int i, cube_no=0, dim_no, cube_read_count = 0, max_cube_read_count;

  *bytes_read = 0;

  /* allocate a buffer big enough for all the subcubes I'll read */
  for (i = 0; i < cubelet_list_count; i++) {
    Cubelet *c = &cubelet_list[i];
    subcube_bytes = sizeof(double) * c->ndims * Cubelet_get_subcube_size(c);
    *bytes_read += subcube_bytes;
    cube_read_count += c->ndims;
  }
  subcube_buffer_pos = subcube_buffer = (double*) malloc(*bytes_read);

  /* find the most number of cubes any rank will be reading */
  MPI_Allreduce(&cube_read_count, &max_cube_read_count, 1, MPI_INT,
		MPI_MAX, MPI_COMM_WORLD);

  /* Some ranks may have more cubes to read than others.
     Loop as many times as the maximum. */

  cube_list_pos = dim_no = 0;
  for (cube_no = 0; cube_no < max_cube_read_count; cube_no++) {

    /* printf("[%d] iter %d, cube_list_pos=%d, dim_no=%d\n", rank, cube_no,
       cube_list_pos, dim_no); */
    
    /* If this rank has no more to read */
    if (cube_no >= cube_read_count) {
      /* do a dummy collective call */
      int dummysize[3] = {1, 1, 1}, dummybuf, dummyoffset[3] = {0,0,0};

      Mesh_IO_read(grid_inf, 0, MPI_INT, 1, &dummybuf,
                   3, dummysize, dummysize, dummyoffset,
                   MPI_ORDER_FORTRAN, dummysize, dummyoffset,
                   MPI_ORDER_FORTRAN);

    }

    /* This rank still has cubes to read */
    else {
      Cubelet *c = &cubelet_list[cube_list_pos];
      unsigned computed_hash;
      size_t subcube_size_count, subcube_size_bytes;
      size_t cube_size_count, cube_size_bytes;
      MPI_Offset cube_offset;
      int off0[3] = {0,0,0};

      /* compute the sizes of the full and partial cubes in bytes and elements
       */
      cube_size_count = Cubelet_get_cube_size(c);
      cube_size_bytes = sizeof(double) * cube_size_count;
      subcube_size_count = Cubelet_get_subcube_size(c);
      subcube_size_bytes = sizeof(double) * subcube_size_count;
      *bytes_read += subcube_size_bytes;

      cube_offset = c->offset + dim_no * cube_size_bytes;

      /* read a subcube for this cubelet */
      Mesh_IO_read(grid_inf, cube_offset, MPI_DOUBLE, 1, subcube_buffer_pos,
                   3, c->subcube_size, c->cube_size, c->subcube_offset,
                   MPI_ORDER_FORTRAN, c->subcube_size, off0,
                   MPI_ORDER_FORTRAN);


#if VERBOSE > 1
      printCube(c, subcube_buffer_pos);
#endif

      subcube_buffer_pos += subcube_size_count;

      dim_no++;
      if (dim_no == c->ndims) {
        
        /* check the hash of the data */
        computed_hash = simpleHash
          (subcube_buffer_pos - subcube_size_count * c->ndims,
           subcube_size_bytes * c->ndims);
        if (computed_hash != c->hash) {
#if VERBOSE
          printf("[%d] cube %d: expected hash %08x, got %08x\n",
                 rank, cube_list_pos, c->hash, computed_hash);
          fflush(stdout);
#endif
          error_count++;
        } else {
#if VERBOSE
          printf("[%d] cube %d: OK %08x = %08x\n",
                 rank, cube_list_pos, c->hash, computed_hash);
          fflush(stdout);
#endif
          success_count++;
        }

        dim_no = 0;
        cube_list_pos++;
      }  /* dim_no==ndims */
    }
  }  /* cube_no */

  printf("[%d] %d cubes read successfully, %d errors\n", rank, success_count, error_count);

  MPI_Reduce(rank==0 ? MPI_IN_PLACE : bytes_read, bytes_read, 1,
	     MPI_UNSIGNED_LONG, MPI_SUM, 0, MPI_COMM_WORLD);

  MPI_Reduce(rank==0 ? MPI_IN_PLACE : &success_count, &success_count, 1,
	     MPI_INT, MPI_SUM, 0, MPI_COMM_WORLD);
  MPI_Reduce(rank==0 ? MPI_IN_PLACE : &error_count, &error_count, 1,
	     MPI_INT, MPI_SUM, 0, MPI_COMM_WORLD);

  if (rank == 0) {
    if (error_count > 0) {
      printf("TEST FAIL: %d cubes read correctly, %d incorrectly\n",
             success_count, error_count);
    } else {
      printf("TEST PASS: %d cubes read correctly\n", success_count);
    }
  }
  
  free(subcube_buffer);
}
  



/* Given 'count' items distributed among 'splits' recipients,
   as equally as possible, return the number of items recipient
   'idx' will get.

   For example, 8 items split 5 ways:
     0,1  2,3  4,5  6    7
     0    1    2    3    4

   distribute(8, 5, 0) -> 2
   distribute(8, 5, 3) -> 1
*/
int distribute(int count, int splits, int idx) {
  return count / splits + (idx < count % splits);
}


int readCubeletFile(Cubelet **cubelet_list, int *cubelet_list_count,
		    const char *filename) {
  Cubelet c;
  int capacity = 100;
  FILE *inf;

  *cubelet_list_count = 0;

  inf = fopen(filename, "r");
  if (!inf) {
    printf("Cannot open %s\n", filename);
    return 0;
  }

  *cubelet_list = (Cubelet*) malloc(sizeof(Cubelet) * capacity);

  while (1) {
    if (!readCubelet(&c, inf)) break;

    if (*cubelet_list_count == capacity) {
      capacity *= 2;
      *cubelet_list =
	(Cubelet*) realloc(*cubelet_list, sizeof(Cubelet) * capacity);
    }

    (*cubelet_list)[*cubelet_list_count] = c;
    (*cubelet_list_count)++;
  }

  fclose(inf);

  return 1;
}


int readAndScatterCubeletFile(Cubelet **cubelet_list, int *cubelet_list_count,
			      int *max_count, const char *filename) {
  Cubelet *full_list;
  int full_count, *send_counts=0, *displacements=0;

  /* rank 0 reads the full list */
  if (rank == 0) {
    readCubeletFile(&full_list, &full_count, filename);
    /* printf("[%d] full_count = %d\n", rank, full_count); */
  }

  /* broadcast the total count; abort if 0 */
  MPI_Bcast(&full_count, 1, MPI_INT, 0, MPI_COMM_WORLD);
  if (full_count == 0) return 0;

  /* allocate memory for my subarray */
  *cubelet_list_count = distribute(full_count, np, rank);
  *cubelet_list = (Cubelet*) malloc(sizeof(Cubelet) * *cubelet_list_count);
  *max_count = (full_count + np - 1) / np;

  /* rank 0 computes how much to send to each */
  if (rank == 0) {
    int i, pos = 0;
    send_counts = (int*) malloc(sizeof(int) * np);
    displacements = (int*) malloc(sizeof(int) * np);
    
    for (i=0; i < np; i++) {
      send_counts[i] = distribute(full_count, np, i);
      displacements[i] = pos;
      pos += send_counts[i];
    }
  }

  /* do the scatter */
  MPI_Scatterv(full_list, send_counts, displacements,
	       cubelet_def_t, *cubelet_list, *cubelet_list_count,
	       cubelet_def_t, 0, MPI_COMM_WORLD);

  /* free the memory for the full list */
  if (rank == 0) free(full_list);

  printf("[%d] Reading %d of %d cubes\n", rank, *cubelet_list_count, full_count);

#if VERBOSE
  {int i;
  for (i=0; i < *cubelet_list_count; i++) {
    printf("[%d] cubelet %d,%d,%d %d,%d,%d\n",
	   rank,
	   (*cubelet_list)[i].subcube_offset[0],
	   (*cubelet_list)[i].subcube_offset[1],
	   (*cubelet_list)[i].subcube_offset[2],
	   (*cubelet_list)[i].subcube_size[0],
	   (*cubelet_list)[i].subcube_size[1],
	   (*cubelet_list)[i].subcube_size[2]);
  }}
#endif

  return 1;
}


int readCubelet(Cubelet *c, FILE *inf) {
  int conversions = fscanf
    (inf, " rank %d NDIM %d fileoffset %ld cubesize %d,%d,%d "
     "subcubeoffset %d,%d,%d subcubesize %d,%d,%d hash %08x",
     &c->rank, &c->ndims, &c->offset,
     &c->cube_size[0], &c->cube_size[1], &c->cube_size[2],
     &c->subcube_offset[0], &c->subcube_offset[1], &c->subcube_offset[2],
     &c->subcube_size[0], &c->subcube_size[1], &c->subcube_size[2],
     &c->hash);
  return conversions == 13;
}


int Cubelet_get_cube_size(Cubelet *c) {
  return c->cube_size[0] * c->cube_size[1] * c->cube_size[2];
}
   
int Cubelet_get_subcube_size(Cubelet *c) {
  return c->subcube_size[0] * c->subcube_size[1] * c->subcube_size[2];
}


void printCube(Cubelet *c, double *data) {
  int x, y, z, d;

  for (d=0; d < c->ndims; d++) {
    printf("Dim %d\n", d);
    for (z=0; z < c->subcube_size[2]; z++) {
      printf("Layer %d\n", z);
      for (y=0; y < c->subcube_size[1]; y++) {
	for (x=0; x < c->subcube_size[0]; x++) {
	  double e = data[d * c->subcube_size[2] * c->subcube_size[1] * c->subcube_size[0] +
			  z * c->subcube_size[1] * c->subcube_size[0] +
			  y * c->subcube_size[0] + x];
	  printf("%5.1f ", e);
	}
	printf("\n");
      }
    }
    printf("\n");
  }
}


void defineCubeletDefType() {
  int block_lengths[7] = {1, 1, 1, 3, 3, 3, 1};
  MPI_Aint displacements[7] = {
    offsetof(Cubelet, rank),
    offsetof(Cubelet, ndims),
    offsetof(Cubelet, offset),
    offsetof(Cubelet, cube_size),
    offsetof(Cubelet, subcube_offset),
    offsetof(Cubelet, subcube_size),
    offsetof(Cubelet, hash)
  };
  MPI_Datatype types[7] = {
    MPI_INT,
    MPI_INT,
    MPI_LONG,
    MPI_INT,
    MPI_INT,
    MPI_INT,
    MPI_UNSIGNED
  };

  MPI_Type_create_struct(7, block_lengths, displacements, types,
			 &cubelet_def_t);
  MPI_Type_commit(&cubelet_def_t);
}


unsigned simpleHash(const void *data_v, size_t len) {
  const unsigned char *data = (const unsigned char *) data_v;
  unsigned hash = 2166136261u;
  size_t i;
  for (i=0; i < len; i++) {
    hash = hash ^ data[i];
    hash = hash * 16777619;
  }
  return hash;
}
