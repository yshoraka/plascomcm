#include <stdio.h>
#include <assert.h>
#include "endian_switch.h"

int do_reverse_endian;
const unsigned AB = 0x12345678;
const unsigned BA = 0x78563412;


void test1DFull() {
  unsigned array[] = {AB, AB, AB, AB};
  int i, len = sizeof(array) / sizeof(array[0]);
  int sub_size[] = {len}, full_size[] = {len}, sub_start[] = {0};

  endianSwitch(array, sizeof(array[0]), do_reverse_endian, 1,
               full_size, sub_size, sub_start, MPI_ORDER_C);
  
  for (i=0; i < len; i++)
    assert(array[i] == BA);
}


void test1DPartial() {
  unsigned array[] = {BA, BA, AB, AB, AB, BA};
  int i, len = sizeof(array) / sizeof(array[0]);
  int sub_size[] = {3}, full_size[] = {len}, sub_start[] = {2};

  endianSwitch(array, sizeof(array[0]), do_reverse_endian, 1,
               full_size, sub_size, sub_start, MPI_ORDER_C);
  
  for (i=0; i < len; i++)
    assert(array[i] == BA);
}

void test2DFull() {
  unsigned array[3][5];
  int size[] = {3, 5}, start[] = {0, 0};
  int i, j;

  for (i=0; i < 3; i++)
    for (j=0; j<5; j++)
      array[i][j] = AB;
  
  endianSwitch(array, sizeof(unsigned), do_reverse_endian, 2,
               size, size, start, MPI_ORDER_C);

  for (i=0; i < 3; i++)
    for (j=0; j<5; j++)
      assert(array[i][j] == BA);
}


void test2DPartialRows() {
  unsigned array[3][5];
  int sub_size[] = {1, 5}, full_size[] = {3, 5}, start[] = {1, 0};
  int i, j;

  for (i=0; i < 3; i++)
    for (j=0; j<5; j++)
      array[i][j] = i==1 ? AB : BA;
  
  endianSwitch(array, sizeof(unsigned), do_reverse_endian, 2,
               full_size, sub_size, start, MPI_ORDER_C);

  for (i=0; i < 3; i++)
    for (j=0; j<5; j++)
      assert(array[i][j] == BA);
}
  

void test2DPartialCols() {
  unsigned array[3][5];
  int sub_size[] = {3, 2}, full_size[] = {3, 5}, start[] = {0, 3};
  int i, j;

  for (i=0; i < 3; i++)
    for (j=0; j<5; j++)
      array[i][j] = j >= 3 ? AB : BA;
  
  endianSwitch(array, sizeof(unsigned), do_reverse_endian, 2,
               full_size, sub_size, start, MPI_ORDER_C);

  for (i=0; i < 3; i++)
    for (j=0; j<5; j++)
      assert(array[i][j] == BA);
}
  

void test2DPartialBoth() {
  unsigned array[3][5];
  int sub_size[] = {1, 2}, full_size[] = {3, 5}, start[] = {2, 1};
  int i, j;

  for (i=0; i < 3; i++)
    for (j=0; j<5; j++)
      array[i][j] = (i==2 && j>=1 && j<3) ? AB : BA;
  
  endianSwitch(array, sizeof(unsigned), do_reverse_endian, 2,
               full_size, sub_size, start, MPI_ORDER_C);

  for (i=0; i < 3; i++)
    for (j=0; j<5; j++)
      assert(array[i][j] == BA);
}


void testSizes() {
  char buf[16];
  int i, full_size = 16, sub_size, offset = 0;
  for (i=0; i < 16; i++) buf[i] = i;

  // 1 byte, do nothing
  sub_size = 16;
  endianSwitch(buf, 1, do_reverse_endian, 1, &full_size, &sub_size, &offset,
               MPI_ORDER_C);
  for (i=0; i < 16; i++) assert(buf[i] == i);

  // reverse 2 byte pieces
  sub_size = 8;
  for (i=0; i < 16; i+=2) {
    buf[i] = i+1; buf[i+1] = i;
  }
  endianSwitch(buf, 2, do_reverse_endian, 1, &full_size, &sub_size, &offset,
               MPI_ORDER_C);
  for (i=0; i < 16; i++) assert(buf[i] == i);

  // reverse 4 byte pieces
  sub_size = 4;
  for (i=0; i < 16; i+=4) {
    buf[i] = i+3; buf[i+1] = i+2; buf[i+2] = i+1; buf[i+3] = i;
  }
  endianSwitch(buf, 4, do_reverse_endian, 1, &full_size, &sub_size, &offset,
               MPI_ORDER_C);
  for (i=0; i < 16; i++) assert(buf[i] == i);

  // reverse 8 byte pieces
  sub_size = 2;
  for (i=0; i < 8; i++) {
    buf[i] = 7-i;
    buf[i+8] = 15-i;
  }
  endianSwitch(buf, 8, do_reverse_endian, 1, &full_size, &sub_size, &offset,
               MPI_ORDER_C);
  for (i=0; i < 16; i++) assert(buf[i] == i);

  // reverse 6 byte pieces
  sub_size = 2;
  for (i=0; i < 6; i++) {
    buf[i] = 5-i;
    buf[i+6] = 11-i;
  }
  endianSwitch(buf, 6, do_reverse_endian, 1, &full_size, &sub_size, &offset,
               MPI_ORDER_C);
  for (i=0; i < 16; i++) assert(buf[i] == i);

  // reverse all 16 bytes
  sub_size = 1;
  for (i=0; i < 16; i++)
    buf[i] = 15-i;
  endianSwitch(buf, 16, do_reverse_endian, 1, &full_size, &sub_size, &offset,
               MPI_ORDER_C);
  for (i=0; i < 16; i++) assert(buf[i] == i);
}


void testFloatDouble() {
  float floats[2] = {3.14159, 2.71828}, *revfloat;
  double doubles[2] = {3.141592653589793, 2.7182818284590455}, *revdouble;
  unsigned char buf[24] = {
    0x40, 0x49, 0x0F, 0xD0,
    0x40, 0x2D, 0xF8, 0x4D,
    0x40, 0x09, 0x21, 0xFB, 0x54, 0x44, 0x2D, 0x18,
    0x40, 0x05, 0xBF, 0x0A, 0x8B, 0x14, 0x57, 0x6A
  };
  int size=2, offset=0;
  
  // fwrite(floats, 4, 2, stdout);
  // fwrite(doubles, 8, 2, stdout);

  revfloat = (float*)buf;
  revdouble = (double*)(buf+8);
  endianSwitch(revfloat, 4, do_reverse_endian, 1, &size, &size, &offset,
               MPI_ORDER_C);
  endianSwitch(revdouble, 8, do_reverse_endian, 1, &size, &size, &offset,
               MPI_ORDER_C);
  
  assert(floats[0] == revfloat[0]);
  assert(floats[1] == revfloat[1]);
  assert(doubles[0] == revdouble[0]);
  assert(doubles[1] == revdouble[1]);
}


void testManyDimensions() {
  unsigned buf[5][3][4][7][6];
  int full_sizes[] = {5, 3, 4, 7, 6};
  int sub_sizes[] = {4, 1, 3, 5, 2};
  int sub_starts[] = {0, 2, 1, 1, 3};
  int i0, i1, i2, i3, i4;

  for (i0=0; i0 < full_sizes[0]; i0++)
    for (i1=0; i1 < full_sizes[1]; i1++)
      for (i2=0; i2 < full_sizes[2]; i2++)
        for (i3=0; i3 < full_sizes[3]; i3++)
          for (i4=0; i4 < full_sizes[4]; i4++)

            if (i0 >= sub_starts[0] && i0 < sub_sizes[0] + sub_starts[0] &&
                i1 >= sub_starts[1] && i1 < sub_sizes[1] + sub_starts[1] &&
                i2 >= sub_starts[2] && i2 < sub_sizes[2] + sub_starts[2] &&
                i3 >= sub_starts[3] && i3 < sub_sizes[3] + sub_starts[3] &&
                i4 >= sub_starts[4] && i4 < sub_sizes[4] + sub_starts[4])
              buf[i0][i1][i2][i3][i4] = AB;
            else
              buf[i0][i1][i2][i3][i4] = BA;
  
  endianSwitch(buf, 4, do_reverse_endian, 5, full_sizes, sub_sizes, 
               sub_starts, MPI_ORDER_C);

  for (i0=0; i0 < full_sizes[0]; i0++)
    for (i1=0; i1 < full_sizes[1]; i1++)
      for (i2=0; i2 < full_sizes[2]; i2++)
        for (i3=0; i3 < full_sizes[3]; i3++)
          for (i4=0; i4 < full_sizes[4]; i4++)
            assert(buf[i0][i1][i2][i3][i4] == BA);
}


void testFortranOrder() {
  unsigned array[15];
  int i, full_sizes[2] = {3, 5}, sub_sizes[2] = {1, 5}, sub_starts[2] = {0, 0};

  for (i=0; i < 15; i++) array[i] = AB;

  // a column in C [5][3], a row in Fortran [3][5]
  for (i=0; i < 5; i++) array[i*3] = BA;

  endianSwitch(array, 4, do_reverse_endian, 2, full_sizes, sub_sizes,
               sub_starts, MPI_ORDER_FORTRAN);

  for (i=0; i < 15; i++) assert(array[i] == AB);
}
  


typedef void (*TestFn)();
TestFn test_functions[] = {
  test1DFull,
  test1DPartial,
  test2DFull,
  test2DPartialRows,
  test2DPartialCols,
  test2DPartialBoth,
  testSizes,
  testFloatDouble,
  testManyDimensions,
  testFortranOrder
};

  

int main() {
  int t, n_tests = sizeof(test_functions) / sizeof(test_functions[0]);
  do_reverse_endian = 1;
  do_reverse_endian = *(char*) &do_reverse_endian;

  for (t=0; t < n_tests; t++) {
    putchar('.');
    fflush(stdout);
    test_functions[t]();
  }
  printf(" %d tests ok\n", n_tests);
  
  return 0;
}
