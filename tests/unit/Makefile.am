# Copyright (c) 2015, University of Illinois at Urbana-Champaign, XPACC
# License: MIT, http://opensource.org/licenses/MIT
# Make sure automake knows where to look for the local macros for configure
ACLOCAL_AMFLAGS = -I m4
# Build the object files in the source directory instead of the top
# directory (subdir-objects) and don't act like a GNU tool (foreign)
# If you change the subdir-objects, change the FC_DEPDIR option to match
AUTOMAKE_OPTIONS = subdir-objects foreign

# Initialize variables
BUILT_SOURCES =
EXTRA_DIST =
check_PROGRAMS =
noinst_PROGRAMS =
dist_noinst_SCRIPTS =
noinst_HEADERS =
CLEAN_LOCAL_TARGETS =
DISTCLEAN_LOCAL_TARGETS =
deps_fpp_src =
deps_c_src =
FPPFLAGS =

# For suffix rules
SUFFIXES = .fpp .$(FC_MODEXT) .pf

# Include list of PlasComCM source files and process them to be relative to the
# current directory
include ../../src/Makefile.mk
plascomcm_common_rel_fpp_src = $(addprefix ../../src/,$(plascomcm_common_fpp_src))
plascomcm_common_rel_fc_src = $(plascomcm_common_rel_fpp_src:.fpp=.f90)
plascomcm_common_rel_c_src = $(addprefix ../../src/,$(plascomcm_common_c_src))

# Include unit test source filenames
include Makefile.mk
unit_tests_fpp_src = $(unit_tests_pf_src:.pf=.fpp)
unit_tests_fc_src = $(unit_tests_fpp_src:.fpp=.f90)

# Distribute .pf source files
EXTRA_DIST += $(unit_tests_pf_src)

# Distribute test data
EXTRA_DIST += data

# Distribute the test runner script
dist_noinst_SCRIPTS += runall.in autorun.in

# Distribute test suite include file
noinst_HEADERS += testSuites.inc

# Distribute the test fixtures
EXTRA_DIST += TestFixtures.fpp

.PHONY: unittests-clean

if BUILD_TESTS
if HAVE_PFUNIT

# Unit test driver
noinst_PROGRAMS += unittests
check_PROGRAMS += unittests
nodist_unittests_SOURCES = $(unit_tests_fc_src) driver.f90 TestFixtures.f90
unittests_DEPENDENCIES = $(plascomcm_common_rel_fpp_src:.fpp=.o) $(plascomcm_common_rel_c_src:.c=.o) ../../amos/libamos.a
unittests_LDADD = $(plascomcm_common_rel_fpp_src:.fpp=.o) $(plascomcm_common_rel_c_src:.c=.o) -L../../amos -lamos

# Generate dependencies
deps_fpp_src += $(plascomcm_common_rel_fpp_src) $(unit_tests_fpp_src) driver.fpp TestFixtures.fpp

# Copy the test driver source file over to a local directory before compiling
driver.fpp: @PFUNIT_DRIVER@
	cp @PFUNIT_DRIVER@ driver.fpp

# Make sure driver gets reprocessed if testSuites.inc changes
driver.f90: testSuites.inc

# Preprocess the .pf test files into Fortran .fpp files
.pf.fpp:
	@PFPARSE@ $^ $@

# Create directories and copy files for VPATH builds
BUILT_SOURCES += $(abs_builddir)/.datastamp
$(abs_builddir)/.datastamp: $(abs_builddir)/Makefile
	if [ "$(builddir)" != "$(srcdir)" ]; then \
    test -L data || @LN_S@ "$(abs_srcdir)/data" data; \
    test -L testSuites.inc || @LN_S@ "$(abs_srcdir)/testSuites.inc" testSuites.inc; \
  fi; \
  touch .datastamp

# Cleanup rules
CLEAN_LOCAL_TARGETS += unittests-clean
unittests-clean:
	rm -f $(unit_tests_fpp_src)
	rm -f $(unit_tests_fc_src)
	rm -f driver.f90 driver.fpp
	rm -f TestFixtures.f90
	rm -f .datastamp
	if [ "$(builddir)" != "$(srcdir)" ]; then \
    rm -f data; \
    rm -f testSuites.inc; \
  fi

endif
endif

#======
# Misc
#======

# Directories for module files
MODDIR = mod
SRCMODDIR = ../../src/mod
UTILSMODDIR = ../../utils/mod

# Fortran module characteristics
FC_MODEXT   = @FC_MODEXT@
FC_MODINC   = @FC_MODINC@
FC_MODOUT   = @FC_MODOUT@
FC_MODCASE  = @FC_MODCASE@
AM_FCFLAGS  = $(FC_MODOUT)$(MODDIR) $(FC_MODINC)$(MODDIR) $(FC_MODINC)$(SRCMODDIR) \
  $(FC_MODINC)$(UTILSMODDIR)

# aggressive optimization flags
# The strange form here protects these conditionals from automake,
# and ensures that the are executed by make, not automake.
@MK@ifeq "$(USE_AGGRESSIVE_OPTS)" "1"
@MK@ifeq "$(COMPILER_FAMILY)" "intel"
@MK@  AM_CFLAGS   += -O2
@MK@  AM_CXXFLAGS += -O2
@MK@  AM_FCFLAGS  += -O3 -ip -ipo -xHost
@MK@endif
@MK@ifeq "$(COMPILER_FAMILY)" "gnu"
@MK@  AM_CFLAGS   += -O2
@MK@  AM_FCFLAGS  += -fomit-frame-pointer -O3 -ftree-vectorize -ffast-math -funroll-loops
@MK@  AM_CXXFLAGS += -O2
@MK@endif
@MK@ifeq "$(COMPILER_FAMILY)" "cray"
@MK@  AM_CFLAGS   += -O
@MK@endif
@MK@endif

# Fortran preprocessing
FPPFLAGS += @FPPFLAGS@

# Rule for processing .fpp files to .f90
# Note that Make has trouble combining implicit with explicit rules.
# Thus, a change to an .fpp file will not force a recompilation of all of the
# modules that make use of the module(s) created by that .fpp file unless
# we include that as an explicit dependency
.fpp.f90:
	$(FPP) $(FPPFLAGS) $< >$@
	-@rm -f $*.s
	$(FC_DEPCOMP) -J$(MODDIR) -J$(SRCMODDIR) -J$(UTILSMODDIR) --modext=$(FC_MODEXT) \
    --modcase=$(FC_MODCASE) --depdir=$(DEPDIR) --moddir=$(MODDIR) --subdirobj --subdirmod \
    --modulelist=_fcmods --origsrc=$*.fpp $*.f90

# Cleanup rules
clean-local: $(CLEAN_LOCAL_TARGETS)
	rm -rf $(MODDIR)
	rm -rf *.dSYM

distclean-local: $(DISTCLEAN_LOCAL_TARGETS)

# Dependency generation script
FC_DEPCOMP = @FC_DEPCOMP@

# Dependency directory name
DEPDIR = @DEPDIR@

# Create module directory
BUILT_SOURCES += $(abs_builddir)/$(MODDIR)
$(abs_builddir)/$(MODDIR):
	test -d $(MODDIR) || @MKDIR_P@ $(MODDIR)

# FC dependencies
# Since automake won't generate one for each Fortran source file,
# we create one. (automake will add this for us if wel use C or C++)
# Do dependency generation first
BUILT_SOURCES += $(abs_builddir)/$(DEPDIR)/stamp_fclist
# If the module is not found, it is expected in MODDIR
# Include source files on the dependencies, but then always rescan
# everyfile on a change to any of them.
# Do need to add a rescan when a file is changed
# Note the two pass algorithm.  First find all of the module names defined
# by the package, then use those to create the dependency files.
# Updates to individual files will update the module list (_fcmods)
$(abs_builddir)/$(DEPDIR)/stamp_fclist: $(deps_fpp_src) $(deps_c_src)
	rm -f $(DEPDIR)/_fclist $(DEPDIR)/_fcmods
	touch $(DEPDIR)/_fclist
	for file in $(deps_fpp_src); do \
	    bfile=`echo $$file | sed 's/\.fpp/.f90/'`; \
	    $(MAKE) $$bfile; \
	    $(FC_DEPCOMP) -J$(MODDIR) -J$(SRCMODDIR) -J$(UTILSMODDIR) --modext=$(FC_MODEXT) \
        --modcase=$(FC_MODCASE) --depdir=$(DEPDIR) -moddir=$(MODDIR) --subdirobj --subdirmod \
        --modulelist=_fcmods --origsrc=$$file $$bfile; \
	done
	for file in $(deps_fpp_src:.fpp=.f90); do \
	    bfile=`basename $$file .f90`; \
	    sfile=`echo $$file | sed 's/\.f90/.fpp/'` ; \
	    $(FC_DEPCOMP) -J$(MODDIR) -J$(SRCMODDIR) -J$(UTILSMODDIR) --modext=$(FC_MODEXT) \
        --modcase=$(FC_MODCASE) --depdir=$(DEPDIR) --moddir=$(MODDIR) --subdirobj --subdirmod \
        --modulelist=_fcmods --origsrc=$$sfile $$file; \
	    echo "include $(DEPDIR)/$$bfile.Po" >> $(DEPDIR)/_fclist; \
	done
	echo "Stamp for FC dependencies" > $(DEPDIR)/stamp_fclist

.PHONY: deps-clean deps-distclean

# _fclist and C .Po files are created by configure, so 'clean' target shouldn't delete them
# (just empty them); Fortran .Po files are created by make, so they should be deleted
CLEAN_LOCAL_TARGETS += deps-clean
deps-clean:
	rm -f $(DEPDIR)/_fcmods
	rm -f $(DEPDIR)/stamp_fclist
	echo "# dummy" > $(DEPDIR)/_fclist
	(cd $(DEPDIR) && for file in $(deps_c_src:.c=.Po); do \
	  echo "# dummy" > "$$file"; \
	done)
	(cd $(DEPDIR) && for file in $(deps_fpp_src:.fpp=.Po); do \
	  rm -f "$$file"; \
	done)

DISTCLEAN_LOCAL_TARGETS += deps-distclean
deps-distclean:
	rm -rf $(DEPDIR)

# Include dependency files
@AMDEP_TRUE@@am__include@ @am__quote@$(DEPDIR)/_fclist@am__quote@
