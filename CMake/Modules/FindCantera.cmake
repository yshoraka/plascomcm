#
# module for Cantera, http://cantera.github.com/docs/sphinx/html/index.html
#
include(FindPackageHandleStandardArgs)

IF(NOT CANTERA_DIR)
   SET(CANTERA_DIR "$ENV{CANTERA_DIR}")
ENDIF()

find_path(CANTERA_INCLUDE_DIR cantera.mod 
  PATHS /usr/local/include/cantera 
        /usr/include/cantera
        ENV ${CANTERA_DIR}
  HINTS cantera
  PATH_SUFFIXES include/cantera
  DOC "Cantera include directory")

find_program (MAKE_EXECUTABLE NAMES make gmake)
#
#find_library(CANTERA_LIBRARY
#  NAMES cantera 
#  PATHS /usr/local/lib /usr/lib
#  HINTS ENV ${CANTERA_DIR}
#  PATH_SUFFIXES cantera lib 
#  DOC "Cantera libraries")
# 
#find_library(CANTERA_LIBRARY_SHARE
#  NAMES cantera_shared
#  PATHS /usr/local/lib /usr/lib
#  HINTS ENV ${CANTERA_DIR}
#  PATH_SUFFIXES cantera lib 
#  DOC "Cantera libraries")
#
#find_library(CANTERA_LIBRARY_FORTRAN
#  NAMES cantera_fortran
#  PATHS /usr/local/lib /usr/lib
#  HINTS ENV ${CANTERA_DIR}
#  PATH_SUFFIXES cantera lib 
#  DOC "Cantera Fortran libraries")
#
#find_library(CANTERA_LIBRARY_BLAS
#  NAMES blas
#  PATHS /usr/local/lib /usr/lib
#  HINTS ENV ${CANTERA_DIR}
#  PATH_SUFFIXES cantera lib
#  DOC "Cantera blas libraries")
#
#find_library(CANTERA_LIBRARY_LAPACK
#  NAMES lapack
#  PATHS /usr/local/lib /usr/lib
#  HINTS ENV ${CANTERA_DIR}
#  PATH_SUFFIXES cantera lib
#  DOC "Cantera blas libraries")

function(cantera_get_version)
  if (EXISTS "${CANTERA_INCLUDE_DIR}/base/config.h")
    file (STRINGS "${CANTERA_INCLUDE_DIR}/base/config.h" vstrings REGEX "define CANTERA_VERSION")
    string(REPLACE "\"" " " tmp ${vstrings})
    string(REGEX REPLACE " +" ";" fields ${tmp})
    list(GET fields 2 version)
    set(CANTERA_VERSION ${version} PARENT_SCOPE)
  endif()
endfunction()

set (cantera_conf_variables "${CANTERA_INCLUDE_DIR}/Cantera.mak")

if (cantera_conf_variables)

  # A temporary makefile to probe the Cantera configuration
  set (cantera_config_makefile "${PROJECT_BINARY_DIR}/Makefile.cantera")
  file (WRITE "${cantera_config_makefile}"
"## This file was autogenerated by FindCantera.cmake
include ${cantera_conf_variables}
show :
\t-@echo -n \${\${VARIABLE}}
")

  macro (cantera_get_variable name var)
    set (${var} "NOTFOUND" CACHE INTERNAL "Cleared" FORCE)
    execute_process (COMMAND ${MAKE_EXECUTABLE} --no-print-directory -f ${cantera_config_makefile} show VARIABLE=${name}
      OUTPUT_VARIABLE ${var}
      RESULT_VARIABLE cantera_return)
  endmacro ()

  cantera_get_variable (CANTERA_INCLUDES        cantera_inc)
  cantera_get_variable (CANTERA_TOTAL_LIBS      cantera_lib)
  cantera_get_variable (CANTERA_TOTAL_LIBS_DEP  cantera_lib_dep)
  cantera_get_variable (CANTERA_FORTRAN_LIBS    cantera_fortran_lib)
  # We are done with the temporary Makefile, calling CANTERA_GET_VARIABLE after this point is invalid!
  file (REMOVE ${cantera_config_makefile})

  # remove -lcvode from the libraries
  # this is an apparant bug in Cantera to include it even if not needed
  # todo: add check to see if we need it, don't just remove it always
  # only supported by cmake 3.1.3 and newer
  # write a C equivalent, this will at least give us a warm fuzzy
  #
  #    set(_CANTERA_TEST_SOURCE "
  #    program sundialstest
  #      use cantera
  #    end program sundialstest
  #")
  #
  #    set (CMAKE_REQUIRED_INCLUDES ${includes})
  #    set (CMAKE_REQUIRED_LIBRARIES ${libraries})
  #    check_fortran_source_runs ("${_CANTERA_TEST_SOURCE}" ${runs})
  #    if (${${runs}})
  #    else ()
  string(REPLACE "-lcvode" "" cantera_lib ${cantera_lib})
  string(REPLACE "-lcvode" "" cantera_fortran_lib ${cantera_fortran_lib})
  #    endif ()

  set(CANTERA_Fortran_LIBRARY ${cantera_fortran_lib})
  set(CANTERA_CORE_LIBRARY ${cantera_lib})
  set(CANTERA_EXTRA_LIBRARIES ${cantera_lib_dep})
  #set(CANTERA_SHARED_LIBRARY ${CANTERA_LIBRARY_SHARE})
  set(CANTERA_INCLUDE_DIRS ${CANTERA_INCLUDE_DIR})
  mark_as_advanced(CANTERA_INCLUDE_DIRS CANTERA_CORE_LIBRARY 
    CANTERA_Fortran_LIBRARY CANTERA_EXTRA_LIBRARIES)
  set(CANTERA_VERSION "")
  cantera_get_version(CANTERA_VERSION)
#
# test that we can build a simple application with cantera
# todo: add this test :-)
# only supported by cmake 3.1.3 and newer
# write a C equivalent, this will at least give us a warm fuzzy
#
#    set(_CANTERA_TEST_SOURCE "
#    program canteratest
#      use cantera
#    end program canteratest
#")
#
#    set (CMAKE_REQUIRED_INCLUDES ${includes})
#    set (CMAKE_REQUIRED_LIBRARIES ${libraries})
#    check_fortran_source_runs ("${_CANTERA_TEST_SOURCE}" ${runs})
#    if (${${runs}})
#      set (CANTERA_EXECUTABLE_RUNS "YES" CACHE BOOL
#        "Can the system successfully run a PETSc executable?  This variable can be manually set to \"YES\" to force CMake to accept a given PETSc configuration, but this will almost always result in a broken build.  If you change PETSC_DIR, PETSC_ARCH, or PETSC_CURRENT you would have to reset this variable." FORCE)
#    endif ()
ELSE()
  SET(CANTERA_DIR "" CACHE PATH
    "An optional hint to the cantera installation directory"
    )
ENDIF()

find_package_handle_standard_args(cantera DEFAULT_MSG
                                  CANTERA_CORE_LIBRARY CANTERA_Fortran_LIBRARY CANTERA_INCLUDE_DIRS)
