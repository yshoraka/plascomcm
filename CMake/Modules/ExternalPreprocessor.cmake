#
# Preprocess PlasComCM unprocessed sources using a third party tool 
# Default to the C Compiler if no tool is specified
#
function(SetupPreProcessor)
  get_directory_property(includeDirectories INCLUDE_DIRECTORIES)
  #message("includeDirectories ${includeDirectories}")
  set(includeFlags)
  foreach(inc ${includeDirectories})
    set(includeFlags ${includeFlags} -I${inc} PARENT_SCOPE)
  endforeach()
  #message("includeFlags ${includeFlags}")
  
  get_directory_property(defines COMPILE_DEFINITIONS)
  #message("defines ${defines}")
  set(defineFlags)
  foreach(def ${defines})
    set(defineFlags ${defineFlags} -D${def} PARENT_SCOPE)
  endforeach()
  #message("defineFlags ${defineFlags}")
  
  if(PreProcTool)
    set(FPP-PreProc ${PreProcTool} PARENT_SCOPE)
    set(FPP-PreProcFlags ${PreProcToolFlags} PARENT_SCOPE)
  else()
    set(FPP-PreProc ${CMAKE_C_COMPILER} PARENT_SCOPE)
    set(FPP-PreProcFlags -E -P ${includeFlags} ${defineFlags} PARENT_SCOPE)
  endif()
endfunction()

function(PreProcess inputFile destination outputFile)
    #message("inputFile ${inputFile}")
    set(srcFile ${inputFile})
    get_filename_component(filename "${srcFile}" NAME_WE)
    set(FPP-File ${srcFile})
    set(F90-File ${destination}/${filename}.f90)
    #message(STATUS "Procesing ${FPP-File} to ${F90-File}")
  
    add_custom_command(OUTPUT "${F90-File}" 
      COMMAND ${FPP-PreProc} ${FPP-PreProcFlags} ${FPP-File} > ${F90-File}
      IMPLICIT_DEPENDS C "${FPP-File}"
      COMMENT "\tPreprocessing ${srcFile}"
      VERBATIM)

    #message("F90-File ${F90-File}")
    set(${outputFile} ${F90-File} PARENT_SCOPE)
    set(outputFile ${F90-File})
    #message("output ${output}")
endfunction()
