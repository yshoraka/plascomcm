# installation

1. PETSc (required > 3.6)
```
wget http://ftp.mcs.anl.gov/pub/petsc/release-snapshots/petsc-3.6.3.tar.gz
tar xvfz petsc-3.6.3.tar.gz
./configure --with-mpiexec=mpiexec.openmpi --download-hypre --prefix=/home/lukeo/petsc
make PETSC_DIR=/home/lukeo/petsc-3.6.3 PETSC_ARCH=arch-linux2-c-debug all
```

Make sure to install hypre (and use the system MPI if possible)

2. Cantera.  Install as normal.

3. Run autogen

```
./autogen.sh
```

3. Add the solver to the configure:

```
./configure --enable-efsolver --with-petsc=/home/lukeo/petsc --prefix=/home/lukeo/plascom --with-cantera=/home/lukeo/cantera --enable-axisymmetric
```

# What the Efield solver looks like from the Fortan side

- ElectricRegionSetup
    - This module sets up phi in region
    - It is automatically included in ModRegion when enabled: `--enable-efsolver`

- ElectricSetup(region, ng)
  - Initilizes the Efield solver (allocating memory)
  - Set this for the grid on which you want an electric field
  - Calls ElectricInitState to initialize the state variables and pass them to the C routines
  - Calls ElectricInitBC to read boundary condition information to pass to C
  - Calls `ef_setup_op` to calculate metrics and assemble the operator

What to add to your .fpp with a zero right-hand side:

```fortran
Use ModElectric

ng = 1
ElectricSetup(region, ng) ! call this only once
electric%rhs(:) = 0.0
ElectricSolve(region, ng)
```

Note: For compatibility with other PlasComCM configurations, electric field code should be wrapped in `#ifdef BUILD_ELECTRIC`
and (for executable statements within a subroutine) `if (input%includeElectric)` blocks, .e.g.:
```
#ifdef BUILD_ELECTRIC
if (input%includeElectric) then
  call ElectricSetup(region, ng)
end if
#endif
```

# sample input

4. To run, change `bc.dat` to include the efield boundary conditions.  For example if this is your `bc.dat` file:
```
1     21     1      1     1      1    -1      1    -1
1     22    -1     61    61     57    63      1    -1
1     22     2     62    -1     64    64      1    -1
1     24    -1     -1    -1      1    -1      1    -1
1     99    -1    490    -1      1    -1      1    -1
1     28    -2      1    -1     -1    -1      1    -1
1     29     2      1    -1      1     1      1    -1
1     99     2      1    -1      1    12      1    -1
1     22    -1     49    49      1    56      1    -1
1     22     2     50    61     57    57      1    -1
```
then you would look up the id in the ModGlobal.fpp:
```
EF_DIRICHLET_G                                  = 600, &
EF_DIRICHLET_P                                  = 601, &
EF_NEUMANN                                      = 602, &
EF_SCHWARZ                                      = 698, &
EF_DIELECTRIC                                   = 699, &  ! added this one for charged particles BC at the walls. The 300's will span Plasma BCs &
EF_ELECTRODE                                    = 601, &  ! overloading EF_DIRICHLET_P
```
Notice the ground (`EF_DIRICHLET_G`), the applied potential (`EF_DIRICHLET_P`), 
and the neumann and dirichlet will be applied with these IDs in the following addition to `bc.dat`:
```
1    600     1      1    -1      1     1      1    -1
1    600     1      1    -1     -1    -1      1    -1
1    600     1      1     1      1    -1      1    -1
1    600     1     -1    -1      1    -1      1    -1
```

2. In `ModElectricState` modify the right-hand side to your values.  It is set at `0.0` as a default.

3. In `ModElectricState` modify the jump conditions in `jc`, the coefficient in `epsv`

4. Copy the example `inputs/PETScOptions-efield.txt` to `PETScOptions.txt` in the same directory as your `bc.dat` file.

5. In `plascomcm.inp`, set the option `INCLUDE_ELECTRIC` to `TRUE`.

6. (Optional) In `plascomcm.inp`, set the option `WRITE_PHI` to `TRUE` to enable writing the electric potential to a file.

# Things that need to be fixed

- cantera needs instructions
- add a check for petsc >= 3.6
- add a check for cantera when efieldsolve is enabled (needed by modelectricstate)

# Notes
`petscvariables` is in a new location for petsc 3.6
```
13:m4/ax_lib_petsc.m4:72: PETSC_VAR_FILE=${PETSC_DIR}/lib/petsc/conf/petscvariables
15:src/Makefile.am:135:@MK@include $(PETSC_DIR)/lib/petsc/conf/petscvariables
18:tests/Makefile.am:64:include $(PETSC_DIR)/lib/petsc/conf/petscvariables
```
