Copyright (c) 2015, University of Illinois at Urbana-Champaign, XPACC
License: MIT, [http://opensource.org/licenses/MIT](http://opensource.org/licenses/MIT)

PlasComCM - a multi-physics solver that can couple a compressible viscous fluid
            to a compressible, finite strain solid

Code was developed by Professor Daniel J. Bodony
[bodony@illinois.edu](mailto:bodony@illinois.edu) and his collaborators at the
University of Illinois.  Starting in 2006, the code has been supported by
several agencies and industrial partners, including:

- Department of Energy
- National Science Foundation
- Air Force Research Lab
- Air Force Office of Scientific Research
- Office of Naval Research
- NASA
- Rolls-Royce North America

Basic instructions about the code can be found in Manual/Manual.pdf while the
source code resides in src/

Additional instructions on how to interact with the plascomcm repository, including:

- bug reports
- code fixes
- code enhancements

can be found on the [wiki](https://bitbucket.org/xpacc-dev/plascomcm/wiki/Home).

Questions and comments should be directed towards the [XPACC forum](http://xpacc-cluster-0.ae.illinois.edu/phpBB3)

A brief tutorial is available on bitbucket [here](https://<YourBitBucketID>@bitbucket.org/xpacc-dev/plascomcm-tutorials.git)

Sample input datasets are available on bitbucket [here](https://<YourBitBucketID>@bitbucket.org/xpacc-dev/plascomcm-sample-inputs.git)

Detailed documentation is hosted on bitbucket [here](http://xpacc-dev.bitbucket.org/PlasComCM/)

# Index to files

File                 | Description
---                  | ---
AUTHORS              | listing of those contributing code to PlasComCM
ChangeLog            | listing of changes to code
INSTALL              | comments regarding installing PlasComCM
README               | this file
README.PETSC.COMPILE | comments regarding building PlasComCM with PETSc
README.TAU.COMPILE   | comments regarding building PlasComCM with TAU
TODO                 | listing of changes needed to code

# Index to directories

Directory | Description
---       | ---
  amos    | external library for Bessel functions
  doc     | documentation for PlasComCM
  inputs  | default input files
  src     | PlasComCM source code
  tests   | PlasComCM test code
  utils   | PlasComCM utilities to create grids, solutions, and restart files

# Compiling with PETSc and the TM Solver

Before compiling PlasComCM with the thermomechanical solver enabled,
a usuable implementation of PETSc is required.

If PETSc (version 3.3 or newer) is present, then compile with

```
make PETSC_DIR=${PETSC_DIR} PETSC_ARCH=${PETSC_ARCH} USE_TMSOLVER=1
```

If PETSc (version 3.2 or older) is present, then compile with

```
make PETSC_DIR=${PETSC_DIR} PETSC_ARCH=${PETSC_ARCH} OLD_PETSC=1 USE_TMSOVLER=1
```

# Compiling with the TAU profiler

These notes document how to compile PlasComCM with the TAU profiler on a
per-machine basis.  Please e-mail
[bodony@illinois.edu](mailto:bodony@illinois.edu) with instructions for other
matchines.

## NSF TACC Stampede

1. From trial-and-error I discovered that the PAPI libraries are incorrectly
   linked in the newer tau module, so you have to use the older modules:
```
module load intel/13.0.2.146 mvapich2/1.9a2
module load tau/2.22.1 papi/4.4.0
```
2. Once loaded, set the FC, F77, and CC environments as
```
export CC=`which tau_cc.sh`
export FC=`which tau_f90.sh`
export F77=`which tau_f77.sh`
```
3. then execute the configure and make commands:
```
sh ./autogen.sh
./configure —with-tau
make clean && make
```

You should be good-to-go.  (NOTE: I couldn’t use HDF5 because of a missing
symbol at linking.)

