#! /bin/sh

########################################################################
# This file contains material from autogen.sh in the mpich distribution,
# which is Copyright 2006 by Argonne National Laboratory
# That copyright permits the preparation of derivative works, of which
# this is one.
########################################################################


########################################################################
## Utility functions
########################################################################

recreate_tmp() {
    rm -rf .tmp
    mkdir .tmp 2>&1 >/dev/null
}

warn() {
    echo "===> WARNING: $@"
}

echo_n() {
    # "echo -n" isn't portable, must portably implement with printf
    printf "%s" "$*"
}

# Assume Program's install-dir is <install-dir>/bin/<prog>.
# Given program name as the 1st argument,
# the install-dir is returned is returned in 2nd argument.
# e.g., ProgHomeDir libtoolize libtooldir.
ProgHomeDir() {
    prog=$1
    progpath="`which $prog`"
    progbindir="`dirname $progpath`"
    proghome=`(cd $progbindir/.. && pwd)`
    eval $2=$proghome
}

autoreconf_args="-i -f"
export autoreconf_args

########################################################################
## Check for the location of autotools
########################################################################

    autoconf=${AUTOCONF:-autoconf}
    autoheader=${AUTOHEADER:-autoheader}
    autoreconf=${AUTORECONF:-autoreconf}
    autom4te=${AUTOM4TE:-autom4te}
    automake=${AUTOMAKE:-automake}
    aclocal=${ACLOCAL:-aclocal}

ProgHomeDir $autoconf   autoconfdir
ProgHomeDir $automake   automakedir

echo_n "Checking if autotools are in the same location... "
if [ "$autoconfdir" = "$automakedir" ] ; then
    same_atdir=yes
    echo "yes, all in $autoconfdir"
else
    same_atdir=no
    echo "no"
    echo "	autoconf is in $autoconfdir"
    echo "	automake is in $automakedir"
    # Emit a big warning message if $same_atdir = no.
    warn "Autotools are in different locations. In rare occasion,"
    warn "resulting configure or makefile may fail in some unexpected ways."
fi

########################################################################
## Verify autoconf version
########################################################################

echo_n "Checking for autoconf version... "
recreate_tmp
ver=2.69
# petsc.mcs.anl.gov's /usr/bin/autoreconf is version 2.65 which returns OK
# if configure.ac has AC_PREREQ() withOUT AC_INIT.
#
# ~/> hostname
# petsc
# ~> /usr/bin/autoconf --version
# autoconf (GNU Autoconf) 2.65
# ....
# ~/> cat configure.ac
# AC_PREREQ(2.68)
# ~/> /usr/bin/autoconf ; echo "rc=$?"
# configure.ac:1: error: Autoconf version 2.68 or higher is required
# configure.ac:1: the top level
# autom4te: /usr/bin/m4 failed with exit status: 63
# rc=63
# ~/> /usr/bin/autoreconf ; echo "rc=$?"
# rc=0
cat > .tmp/configure.ac<<EOF
AC_INIT
AC_PREREQ($ver)
AC_OUTPUT
EOF
if (cd .tmp && $autoreconf $autoreconf_args >/dev/null 2>&1 ) ; then
    echo ">= $ver"
else
    echo "Either autoconf failed or version is too old"
    cat <<EOF
You either do not have autoconf in your path or it is too old (version
$ver or higher required). You may be able to use

     autoconf --version

Unfortunately, there is no standard format for the version output and
it changes between autotools versions.  In addition, some versions of
autoconf choose among many versions and provide incorrect output).

You can find an up-to-date autoconf at https://www.gnu.org/software/autoconf/
EOF
    exit 1
fi


########################################################################
## Verify automake version
########################################################################

echo_n "Checking for automake version... "
recreate_tmp
ver=1.12.3
cat > .tmp/configure.ac<<EOF
AC_INIT(testver,1.0)
AC_CONFIG_AUX_DIR([m4])
AC_CONFIG_MACRO_DIR([m4])
m4_ifdef([AM_INIT_AUTOMAKE],,[m4_fatal([AM_INIT_AUTOMAKE not defined])])
AM_INIT_AUTOMAKE([$ver foreign])
AC_MSG_RESULT([A message])
AC_OUTPUT([Makefile])
EOF
cat <<EOF >.tmp/Makefile.am
ACLOCAL_AMFLAGS = -I m4
EOF
if [ ! -d .tmp/m4 ] ; then mkdir .tmp/m4 >/dev/null 2>&1 ; fi
if (cd .tmp && $autoreconf $autoreconf_args >/dev/null 2>&1 ) ; then
    echo ">= $ver"
else
    echo "Either automake failed or version is too old"
    cat <<EOF
You either do not have automake in your path or it is too old (version
$ver or higher required). You may be able to use

     automake --version

Unfortunately, there is no standard format for the version output and
it changes between autotools versions.  In addition, some versions of
autoconf choose among many versions and provide incorrect output).

You can find an up-to-date autoconf at https://www.gnu.org/software/automake/
If both autoconf and automake need to be installed, install autoconf
first and install both into the same installation directory, such as
/usr/local/autotools or /home/myname/installs/autotools.

EOF
    exit 1
fi


autoreconf -I m4 -ivf
